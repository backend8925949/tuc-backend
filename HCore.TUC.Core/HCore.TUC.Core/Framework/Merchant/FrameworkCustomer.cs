//==================================================================================
// FileName: FrameworkCustomer.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Resource;
using System.Security.Cryptography.X509Certificates;
using ClosedXML.Excel;
using System.Reflection;
using System.IO;
using Akka.Actor;
using System.Net;

namespace HCore.TUC.Core.Framework.Merchant
{
    public class FrameworkCustomer
    {
        OAccounts.ContactPerson _ContactPerson;
        HCoreContext _HCoreContext;
        List<OAccounts.Cashier.List> _Cashiers;
        List<OAccounts.SubAccount.List> _SubAccounts;
        List<OAccounts.Store.List> _Stores;
        List<OAccounts.Terminal.List> _Terminals;
        List<OAccounts.Customer.List> _Customers;
        OAccounts.Merchant.Response _MerchantDetails;
        OAccounts.Store.Details _StoreDetails;
        OAccounts.Cashier.Details _CashierDetails;
        OAccounts.SubAccount.Details _SubAccountDetails;
        OAccounts.Terminal.Details _TerminalDetails;
        OAddress _OAddress;
        OAccounts.Customer.Overview _CustomerDetails;
        OAccounts.Terminal.Overview _TerminalOverview;
        OAccounts.Cashier.Overview _CashiersOverview;
        HCUAccountParameter _HCUAccountParameter;

        internal OResponse GetCustomer(OList.Request _Request)
        {
            if (_Request.IsDownload)
            {
                var _Actor = ActorSystem.Create("ActorGetCustomerListDownload");
                var _ActorNotify = _Actor.ActorOf<ActorGetCustomerDownloadN>("ActorGetCustomerListDownload");
                _ActorNotify.Tell(_Request);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
            }
            #region Manage Exception
            try
            {
                if (_Request.StartDate == null)
                {
                    _Request.StartDate = new DateTime(2015, 01, 01, 00, 00, 00);
                }
                if (_Request.EndDate == null)
                {
                    _Request.EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                }
                _Customers = new List<OAccounts.Customer.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {

                    bool IsTucPlusEnable = _Request.IsTucPlus;
                    if (IsTucPlusEnable)
                    {
                        if (_Request.Type == "new")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any() || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))))
                                                                                                          && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                         .Select(x => new OAccounts.Customer.List
                                                         {
                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,
                                                             ReferralCode = x.ReferralCode,
                                                             DisplayName = x.DisplayName,
                                                             Name = x.Name,
                                                             FirstName = x.FirstName,
                                                             LastName = x.LastName,
                                                             EmailAddress = x.EmailAddress,
                                                             MobileNumber = x.MobileNumber,
                                                             CreateDate = x.CreateDate,
                                                             GenderCode = x.Gender.SystemName,
                                                             GenderName = x.Gender.Name,
                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name,
                                                             IconUrl = x.IconStorage.Path,
                                                             LastLoginDate = x.LastLoginDate,
                                                             LastActivityDate = x.LastActivityDate,
                                                             RegistrationSource = x.RegistrationSource.Name,
                                                             TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))
    ),
                                                             TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                             TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                             TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                             TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                             RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                             TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                             LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                         })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any() || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                      && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                      && m.ParentId == _Request.AccountId
                                                                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                                                                      && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))))
                                                                                                          && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           ReferralCode = x.ReferralCode,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           CreateDate = x.CreateDate,
                                                           GenderCode = x.Gender.SystemName,
                                                           GenderName = x.Gender.Name,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,
                                                           RegistrationSource = x.RegistrationSource.Name,
                                                           IconUrl = x.IconStorage.Path,
                                                           LastLoginDate = x.LastLoginDate,
                                                           LastActivityDate = x.LastActivityDate,
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))
    ),
                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                           TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                           TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                           TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                           LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Customers)
                            {
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                        else if (_Request.Type == "loyal")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any() || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))))
                                                                                                          && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                         .Select(x => new OAccounts.Customer.List
                                                         {
                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,
                                                             DisplayName = x.DisplayName,
                                                             Name = x.Name,
                                                             FirstName = x.FirstName,
                                                             LastName = x.LastName,
                                                             EmailAddress = x.EmailAddress,
                                                             MobileNumber = x.MobileNumber,
                                                             CreateDate = x.CreateDate,
                                                             GenderCode = x.Gender.SystemName,
                                                             GenderName = x.Gender.Name,
                                                             RegistrationSource = x.RegistrationSource.Name,
                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name,
                                                             IconUrl = x.IconStorage.Path,
                                                             LastLoginDate = x.LastLoginDate,
                                                             LastActivityDate = x.LastActivityDate,
                                                             TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))
    ),
                                                             TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                             TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                             TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                             TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                             RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                             TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                             LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                         })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any() || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))))
                                                                                                          && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           CreateDate = x.CreateDate,
                                                           GenderCode = x.Gender.SystemName,
                                                           GenderName = x.Gender.Name,
                                                           StatusId = x.StatusId,
                                                           RegistrationSource = x.RegistrationSource.Name,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,
                                                           IconUrl = x.IconStorage.Path,
                                                           LastLoginDate = x.LastLoginDate,
                                                           LastActivityDate = x.LastActivityDate,
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))
    ),
                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                           TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                           TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                           TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                           LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Customers)
                            {
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                        else if (_Request.Type == "lost")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any() || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                       && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                       && m.ParentId == _Request.AccountId
                                                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                                                       && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))))
                                                                                                          && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                         .Select(x => new OAccounts.Customer.List
                                                         {
                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,
                                                             DisplayName = x.DisplayName,
                                                             Name = x.Name,
                                                             FirstName = x.FirstName,
                                                             LastName = x.LastName,
                                                             EmailAddress = x.EmailAddress,
                                                             MobileNumber = x.MobileNumber,
                                                             CreateDate = x.CreateDate,
                                                             GenderCode = x.Gender.SystemName,
                                                             GenderName = x.Gender.Name,
                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name,
                                                             RegistrationSource = x.RegistrationSource.Name,
                                                             IconUrl = x.IconStorage.Path,
                                                             LastLoginDate = x.LastLoginDate,
                                                             LastActivityDate = x.LastActivityDate,
                                                             TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))
    ),
                                                             TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                             TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                             TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                             TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                             RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                             TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                             LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                         })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any() || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                       && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                       && m.ParentId == _Request.AccountId
                                                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                                                       && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))))
                                                                                                          && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           CreateDate = x.CreateDate,
                                                           GenderCode = x.Gender.SystemName,
                                                           GenderName = x.Gender.Name,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,
                                                           RegistrationSource = x.RegistrationSource.Name,
                                                           IconUrl = x.IconStorage.Path,
                                                           LastLoginDate = x.LastLoginDate,
                                                           LastActivityDate = x.LastActivityDate,
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))
    ),
                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                           TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                           TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                           TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                           LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Customers)
                            {
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                        else
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any() || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))))
                                                         .Select(x => new OAccounts.Customer.List
                                                         {
                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,
                                                             DisplayName = x.DisplayName,
                                                             Name = x.Name,
                                                             FirstName = x.FirstName,
                                                             LastName = x.LastName,
                                                             EmailAddress = x.EmailAddress,
                                                             MobileNumber = x.MobileNumber,
                                                             CreateDate = x.CreateDate,
                                                             GenderCode = x.Gender.SystemName,
                                                             GenderName = x.Gender.Name,
                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name,
                                                             RegistrationSource = x.RegistrationSource.Name,
                                                             IconUrl = x.IconStorage.Path,
                                                             LastLoginDate = x.LastLoginDate,
                                                             LastActivityDate = x.LastActivityDate,
                                                             TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))
    ),
                                                             TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                             TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                             TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                             TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                             RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                             TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                             LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                         })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any() || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))))
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           CreateDate = x.CreateDate,
                                                           GenderCode = x.Gender.SystemName,
                                                           GenderName = x.Gender.Name,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,
                                                           RegistrationSource = x.RegistrationSource.Name,
                                                           IconUrl = x.IconStorage.Path,
                                                           LastLoginDate = x.LastLoginDate,
                                                           LastActivityDate = x.LastActivityDate,
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))
    ),
                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                           TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                           TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                           TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                           LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Customers)
                            {
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                    }
                    else
                    {
                        if (_Request.SubReferenceId > 0)
                        {
                            if (_Request.Type == "new")
                            {
                                if (_Request.RefreshCount)
                                {
                                    #region Total Records
                                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && (x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                             ))
                                                                                                              && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.SubParentId == _Request.SubReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                               .Select(x => new OAccounts.Customer.List
                                                               {
                                                                   ReferenceId = x.Id,
                                                                   ReferenceKey = x.Guid,
                                                                   DisplayName = x.DisplayName,
                                                                   Name = x.Name,
                                                                   FirstName = x.FirstName,
                                                                   LastName = x.LastName,
                                                                   EmailAddress = x.EmailAddress,
                                                                   MobileNumber = x.MobileNumber,
                                                                   GenderCode = x.Gender.SystemName,
                                                                   GenderName = x.Gender.Name,
                                                                   CreateDate = x.CreateDate,
                                                                   StatusId = x.StatusId,
                                                                   StatusCode = x.Status.SystemName,
                                                                   StatusName = x.Status.Name,
                                                                   IconUrl = x.IconStorage.Path,
                                                                   RegistrationSource = x.RegistrationSource.Name,
                                                                   LoyaltyTypeName = "New",
                                                                   LastLoginDate = x.LastLoginDate,
                                                                   LastActivityDate = x.LastActivityDate,
                                                                   TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                                   TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                   TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                   RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                   LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                               })
                                                            .Where(_Request.SearchCondition)
                                                   .Count();
                                    #endregion
                                }
                                #region Data
                                _Customers = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && (x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                             ))
                                                                                                              && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.SubParentId == _Request.SubReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                                         .Select(x => new OAccounts.Customer.List
                                                                         {
                                                                             ReferenceId = x.Id,
                                                                             ReferenceKey = x.Guid,
                                                                             DisplayName = x.DisplayName,
                                                                             Name = x.Name,
                                                                             FirstName = x.FirstName,
                                                                             LastName = x.LastName,
                                                                             LoyaltyTypeName = "New",
                                                                             EmailAddress = x.EmailAddress,
                                                                             MobileNumber = x.MobileNumber,
                                                                             GenderCode = x.Gender.SystemName,
                                                                             GenderName = x.Gender.Name,
                                                                             CreateDate = x.CreateDate,
                                                                             StatusId = x.StatusId,
                                                                             StatusCode = x.Status.SystemName,
                                                                             StatusName = x.Status.Name,
                                                                             RegistrationSource = x.RegistrationSource.Name,
                                                                             IconUrl = x.IconStorage.Path,
                                                                             LastLoginDate = x.LastLoginDate,
                                                                             LastActivityDate = x.LastActivityDate,
                                                                             TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                                             TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                             TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                             RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                             LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                                         })
                                                        .Where(_Request.SearchCondition)
                                                        .OrderBy(_Request.SortExpression)
                                                        .Skip(_Request.Offset)
                                                        .Take(_Request.Limit)
                                                        .ToList();
                                #endregion
                                #region Create  Response Object
                                _HCoreContext.Dispose();
                                foreach (var DataItem in _Customers)
                                {
                                    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                    {
                                        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                    }
                                    else
                                    {
                                        DataItem.IconUrl = _AppConfig.Default_Icon;
                                    }
                                }
                                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                                #endregion
                            }
                            else if (_Request.Type == "loyal")
                            {
                                if (_Request.RefreshCount)
                                {
                                    #region Total Records
                                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && (x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                             ))
                                                                                                              && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.SubParentId == _Request.SubReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                               .Select(x => new OAccounts.Customer.List
                                                               {
                                                                   ReferenceId = x.Id,
                                                                   ReferenceKey = x.Guid,
                                                                   DisplayName = x.DisplayName,
                                                                   Name = x.Name,
                                                                   FirstName = x.FirstName,
                                                                   LastName = x.LastName,
                                                                   EmailAddress = x.EmailAddress,
                                                                   MobileNumber = x.MobileNumber,
                                                                   GenderCode = x.Gender.SystemName,
                                                                   GenderName = x.Gender.Name,
                                                                   CreateDate = x.CreateDate,
                                                                   StatusId = x.StatusId,
                                                                   StatusCode = x.Status.SystemName,
                                                                   StatusName = x.Status.Name,
                                                                   IconUrl = x.IconStorage.Path,
                                                                   RegistrationSource = x.RegistrationSource.Name,
                                                                   LoyaltyTypeName = "Loyal",
                                                                   LastLoginDate = x.LastLoginDate,
                                                                   LastActivityDate = x.LastActivityDate,
                                                                   TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                                   TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                   TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                   RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                   LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                               })
                                                            .Where(_Request.SearchCondition)
                                                   .Count();
                                    #endregion
                                }
                                #region Data
                                _Customers = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && (x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                             ))
                                                                                                              && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.SubParentId == _Request.SubReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                                         .Select(x => new OAccounts.Customer.List
                                                                         {
                                                                             ReferenceId = x.Id,
                                                                             ReferenceKey = x.Guid,
                                                                             DisplayName = x.DisplayName,
                                                                             Name = x.Name,
                                                                             FirstName = x.FirstName,
                                                                             LastName = x.LastName,
                                                                             EmailAddress = x.EmailAddress,
                                                                             MobileNumber = x.MobileNumber,
                                                                             GenderCode = x.Gender.SystemName,
                                                                             GenderName = x.Gender.Name,
                                                                             CreateDate = x.CreateDate,
                                                                             StatusId = x.StatusId,
                                                                             StatusCode = x.Status.SystemName,
                                                                             StatusName = x.Status.Name,
                                                                             IconUrl = x.IconStorage.Path,
                                                                             RegistrationSource = x.RegistrationSource.Name,
                                                                             LoyaltyTypeName = "Loyal",
                                                                             LastLoginDate = x.LastLoginDate,
                                                                             LastActivityDate = x.LastActivityDate,
                                                                             TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                                             TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                             TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                             RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                             LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                                         })
                                                        .Where(_Request.SearchCondition)
                                                        .OrderBy(_Request.SortExpression)
                                                        .Skip(_Request.Offset)
                                                        .Take(_Request.Limit)
                                                        .ToList();
                                #endregion
                                #region Create  Response Object
                                _HCoreContext.Dispose();
                                foreach (var DataItem in _Customers)
                                {
                                    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                    {
                                        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                    }
                                    else
                                    {
                                        DataItem.IconUrl = _AppConfig.Default_Icon;
                                    }
                                }
                                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                                #endregion
                            }
                            else if (_Request.Type == "lost")
                            {
                                if (_Request.RefreshCount)
                                {
                                    #region Total Records
                                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && (x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                             ))
                                                                                                              && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.SubParentId == _Request.SubReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                                  .Select(x => new OAccounts.Customer.List
                                                                  {
                                                                      ReferenceId = x.Id,
                                                                      ReferenceKey = x.Guid,
                                                                      DisplayName = x.DisplayName,
                                                                      Name = x.Name,
                                                                      FirstName = x.FirstName,
                                                                      LastName = x.LastName,
                                                                      EmailAddress = x.EmailAddress,
                                                                      MobileNumber = x.MobileNumber,
                                                                      GenderCode = x.Gender.SystemName,
                                                                      GenderName = x.Gender.Name,
                                                                      CreateDate = x.CreateDate,
                                                                      StatusId = x.StatusId,
                                                                      LoyaltyTypeName = "Lost",
                                                                      StatusCode = x.Status.SystemName,
                                                                      StatusName = x.Status.Name,
                                                                      RegistrationSource = x.RegistrationSource.Name,
                                                                      IconUrl = x.IconStorage.Path,
                                                                      LastLoginDate = x.LastLoginDate,
                                                                      LastActivityDate = x.LastActivityDate,
                                                                      TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                                      TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                      TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                      RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                      LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                                  })
                                                            .Where(_Request.SearchCondition)
                                                   .Count();
                                    #endregion
                                }
                                #region Data
                                _Customers = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && (x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                             ))
                                                                                                              && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.SubParentId == _Request.SubReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                                             .Select(x => new OAccounts.Customer.List
                                                                             {
                                                                                 ReferenceId = x.Id,
                                                                                 ReferenceKey = x.Guid,
                                                                                 DisplayName = x.DisplayName,
                                                                                 Name = x.Name,
                                                                                 FirstName = x.FirstName,
                                                                                 LastName = x.LastName,
                                                                                 EmailAddress = x.EmailAddress,
                                                                                 MobileNumber = x.MobileNumber,
                                                                                 GenderCode = x.Gender.SystemName,
                                                                                 GenderName = x.Gender.Name,
                                                                                 CreateDate = x.CreateDate,
                                                                                 StatusId = x.StatusId,
                                                                                 StatusCode = x.Status.SystemName,
                                                                                 LoyaltyTypeName = "Lost",
                                                                                 StatusName = x.Status.Name,
                                                                                 RegistrationSource = x.RegistrationSource.Name,
                                                                                 IconUrl = x.IconStorage.Path,
                                                                                 LastLoginDate = x.LastLoginDate,
                                                                                 LastActivityDate = x.LastActivityDate,
                                                                                 TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                                                 TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                                 TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                                 RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                                 LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                                             })
                                                        .Where(_Request.SearchCondition)
                                                        .OrderBy(_Request.SortExpression)
                                                        .Skip(_Request.Offset)
                                                        .Take(_Request.Limit)
                                                        .ToList();
                                #endregion
                                #region Create  Response Object
                                _HCoreContext.Dispose();
                                foreach (var DataItem in _Customers)
                                {
                                    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                    {
                                        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                    }
                                    else
                                    {
                                        DataItem.IconUrl = _AppConfig.Default_Icon;
                                    }
                                }
                                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                                #endregion
                            }
                            else
                            {
                                if (_Request.RefreshCount)
                                {
                                    #region Total Records
                                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && (x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                             )))
                                                               .Select(x => new OAccounts.Customer.List
                                                               {
                                                                   ReferenceId = x.Id,
                                                                   ReferenceKey = x.Guid,
                                                                   DisplayName = x.DisplayName,
                                                                   Name = x.Name,
                                                                   FirstName = x.FirstName,
                                                                   LastName = x.LastName,
                                                                   EmailAddress = x.EmailAddress,
                                                                   MobileNumber = x.MobileNumber,
                                                                   GenderCode = x.Gender.SystemName,
                                                                   GenderName = x.Gender.Name,
                                                                   CreateDate = x.CreateDate,
                                                                   StatusId = x.StatusId,
                                                                   StatusCode = x.Status.SystemName,
                                                                   StatusName = x.Status.Name,
                                                                   RegistrationSource = x.RegistrationSource.Name,
                                                                   IconUrl = x.IconStorage.Path,
                                                                   LastLoginDate = x.LastLoginDate,
                                                                   LastActivityDate = x.LastActivityDate,
                                                                   TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                                   TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                   TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                   RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                   LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                               })
                                                            .Where(_Request.SearchCondition)
                                                   .Count();
                                    #endregion
                                }
                                #region Data
                                _Customers = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && (x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                             )))
                                                               .Select(x => new OAccounts.Customer.List
                                                               {
                                                                   ReferenceId = x.Id,
                                                                   ReferenceKey = x.Guid,
                                                                   DisplayName = x.DisplayName,
                                                                   Name = x.Name,
                                                                   FirstName = x.FirstName,
                                                                   LastName = x.LastName,
                                                                   EmailAddress = x.EmailAddress,
                                                                   MobileNumber = x.MobileNumber,
                                                                   GenderCode = x.Gender.SystemName,
                                                                   GenderName = x.Gender.Name,
                                                                   CreateDate = x.CreateDate,
                                                                   StatusId = x.StatusId,
                                                                   StatusCode = x.Status.SystemName,
                                                                   StatusName = x.Status.Name,
                                                                   RegistrationSource = x.RegistrationSource.Name,
                                                                   IconUrl = x.IconStorage.Path,
                                                                   LastLoginDate = x.LastLoginDate,
                                                                   LastActivityDate = x.LastActivityDate,
                                                                   TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                                   TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                   TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                   RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                   LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.SubParentId == _Request.SubReferenceId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                               })
                                                        .Where(_Request.SearchCondition)
                                                        .OrderBy(_Request.SortExpression)
                                                        .Skip(_Request.Offset)
                                                        .Take(_Request.Limit)
                                                        .ToList();
                                #endregion
                                #region Create  Response Object
                                _HCoreContext.Dispose();
                                foreach (var DataItem in _Customers)
                                {
                                    if (DataItem.TotalTransaction == 0)
                                    {
                                        DataItem.LoyaltyTypeName = "New";
                                    }
                                    else if (DataItem.TotalTransaction == 1)
                                    {
                                        DataItem.LoyaltyTypeName = "New";
                                    }
                                    else if (DataItem.TotalTransaction > 1)
                                    {
                                        DataItem.LoyaltyTypeName = "Loyal";
                                    }



                                    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                    {
                                        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                    }
                                    else
                                    {
                                        DataItem.IconUrl = _AppConfig.Default_Icon;
                                    }
                                }
                                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                                #endregion
                            }
                        }
                        else
                        {
                            if (_Request.Type == "new")
                            {
                                if (_Request.RefreshCount)
                                {
                                    #region Total Records
                                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any()
                                                           || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                             ))
                                                                                                              && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                               .Select(x => new OAccounts.Customer.List
                                                               {
                                                                   ReferenceId = x.Id,
                                                                   ReferenceKey = x.Guid,
                                                                   DisplayName = x.DisplayName,
                                                                   Name = x.Name,
                                                                   FirstName = x.FirstName,
                                                                   LastName = x.LastName,
                                                                   EmailAddress = x.EmailAddress,
                                                                   MobileNumber = x.MobileNumber,
                                                                   GenderCode = x.Gender.SystemName,
                                                                   GenderName = x.Gender.Name,
                                                                   CreateDate = x.CreateDate,
                                                                   StatusId = x.StatusId,
                                                                   StatusCode = x.Status.SystemName,
                                                                   StatusName = x.Status.Name,
                                                                   IconUrl = x.IconStorage.Path,
                                                                   RegistrationSource = x.RegistrationSource.Name,
                                                                   LoyaltyTypeName = "New",
                                                                   LastLoginDate = x.LastLoginDate,
                                                                   LastActivityDate = x.LastActivityDate,
                                                                   TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                                   TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                   TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                   RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                   LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                               })
                                                            .Where(_Request.SearchCondition)
                                                   .Count();
                                    #endregion
                                }
                                #region Data
                                _Customers = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any()
                                                           || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                             ))
                                                                                                              && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                                         .Select(x => new OAccounts.Customer.List
                                                                         {
                                                                             ReferenceId = x.Id,
                                                                             ReferenceKey = x.Guid,
                                                                             DisplayName = x.DisplayName,
                                                                             Name = x.Name,
                                                                             FirstName = x.FirstName,
                                                                             LastName = x.LastName,
                                                                             LoyaltyTypeName = "New",
                                                                             EmailAddress = x.EmailAddress,
                                                                             MobileNumber = x.MobileNumber,
                                                                             GenderCode = x.Gender.SystemName,
                                                                             GenderName = x.Gender.Name,
                                                                             CreateDate = x.CreateDate,
                                                                             StatusId = x.StatusId,
                                                                             StatusCode = x.Status.SystemName,
                                                                             StatusName = x.Status.Name,
                                                                             RegistrationSource = x.RegistrationSource.Name,
                                                                             IconUrl = x.IconStorage.Path,
                                                                             LastLoginDate = x.LastLoginDate,
                                                                             LastActivityDate = x.LastActivityDate,
                                                                             TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                                             TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                             TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                             RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                             LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                                         })
                                                        .Where(_Request.SearchCondition)
                                                        .OrderBy(_Request.SortExpression)
                                                        .Skip(_Request.Offset)
                                                        .Take(_Request.Limit)
                                                        .ToList();
                                #endregion
                                #region Create  Response Object
                                _HCoreContext.Dispose();
                                foreach (var DataItem in _Customers)
                                {
                                    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                    {
                                        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                    }
                                    else
                                    {
                                        DataItem.IconUrl = _AppConfig.Default_Icon;
                                    }
                                }
                                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                                #endregion
                            }
                            else if (_Request.Type == "loyal")
                            {
                                if (_Request.RefreshCount)
                                {
                                    #region Total Records
                                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any()
                                                           || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                             ))
                                                                                                              && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                               .Select(x => new OAccounts.Customer.List
                                                               {
                                                                   ReferenceId = x.Id,
                                                                   ReferenceKey = x.Guid,
                                                                   DisplayName = x.DisplayName,
                                                                   Name = x.Name,
                                                                   FirstName = x.FirstName,
                                                                   LastName = x.LastName,
                                                                   EmailAddress = x.EmailAddress,
                                                                   MobileNumber = x.MobileNumber,
                                                                   GenderCode = x.Gender.SystemName,
                                                                   GenderName = x.Gender.Name,
                                                                   CreateDate = x.CreateDate,
                                                                   StatusId = x.StatusId,
                                                                   StatusCode = x.Status.SystemName,
                                                                   StatusName = x.Status.Name,
                                                                   IconUrl = x.IconStorage.Path,
                                                                   RegistrationSource = x.RegistrationSource.Name,
                                                                   LoyaltyTypeName = "Loyal",
                                                                   LastLoginDate = x.LastLoginDate,
                                                                   LastActivityDate = x.LastActivityDate,
                                                                   TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                                   TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                   TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                   RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                   LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                               })
                                                            .Where(_Request.SearchCondition)
                                                   .Count();
                                    #endregion
                                }
                                #region Data
                                _Customers = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any()
                                                           || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                             ))
                                                                                                              && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                                         .Select(x => new OAccounts.Customer.List
                                                                         {
                                                                             ReferenceId = x.Id,
                                                                             ReferenceKey = x.Guid,
                                                                             DisplayName = x.DisplayName,
                                                                             Name = x.Name,
                                                                             FirstName = x.FirstName,
                                                                             LastName = x.LastName,
                                                                             EmailAddress = x.EmailAddress,
                                                                             MobileNumber = x.MobileNumber,
                                                                             GenderCode = x.Gender.SystemName,
                                                                             GenderName = x.Gender.Name,
                                                                             CreateDate = x.CreateDate,
                                                                             StatusId = x.StatusId,
                                                                             StatusCode = x.Status.SystemName,
                                                                             StatusName = x.Status.Name,
                                                                             IconUrl = x.IconStorage.Path,
                                                                             RegistrationSource = x.RegistrationSource.Name,
                                                                             LoyaltyTypeName = "Loyal",
                                                                             LastLoginDate = x.LastLoginDate,
                                                                             LastActivityDate = x.LastActivityDate,
                                                                             TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                                             TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                             TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                             RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                             LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                                         })
                                                        .Where(_Request.SearchCondition)
                                                        .OrderBy(_Request.SortExpression)
                                                        .Skip(_Request.Offset)
                                                        .Take(_Request.Limit)
                                                        .ToList();
                                #endregion
                                #region Create  Response Object
                                _HCoreContext.Dispose();
                                foreach (var DataItem in _Customers)
                                {
                                    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                    {
                                        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                    }
                                    else
                                    {
                                        DataItem.IconUrl = _AppConfig.Default_Icon;
                                    }
                                }
                                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                                #endregion
                            }
                            else if (_Request.Type == "lost")
                            {
                                if (_Request.RefreshCount)
                                {
                                    #region Total Records
                                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any()
                                                           || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                             ))
                                                                                                              && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                                  .Select(x => new OAccounts.Customer.List
                                                                  {
                                                                      ReferenceId = x.Id,
                                                                      ReferenceKey = x.Guid,
                                                                      DisplayName = x.DisplayName,
                                                                      Name = x.Name,
                                                                      FirstName = x.FirstName,
                                                                      LastName = x.LastName,
                                                                      EmailAddress = x.EmailAddress,
                                                                      MobileNumber = x.MobileNumber,
                                                                      GenderCode = x.Gender.SystemName,
                                                                      GenderName = x.Gender.Name,
                                                                      CreateDate = x.CreateDate,
                                                                      StatusId = x.StatusId,
                                                                      LoyaltyTypeName = "Lost",
                                                                      StatusCode = x.Status.SystemName,
                                                                      StatusName = x.Status.Name,
                                                                      RegistrationSource = x.RegistrationSource.Name,
                                                                      IconUrl = x.IconStorage.Path,
                                                                      LastLoginDate = x.LastLoginDate,
                                                                      LastActivityDate = x.LastActivityDate,
                                                                      TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                                      TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                      TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                      RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                      LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                                  })
                                                            .Where(_Request.SearchCondition)
                                                   .Count();
                                    #endregion
                                }
                                #region Data
                                _Customers = _HCoreContext.HCUAccount
                                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                           && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any()
                                                           || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                             ))
                                                                                                              && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                                             .Select(x => new OAccounts.Customer.List
                                                                             {
                                                                                 ReferenceId = x.Id,
                                                                                 ReferenceKey = x.Guid,
                                                                                 DisplayName = x.DisplayName,
                                                                                 Name = x.Name,
                                                                                 FirstName = x.FirstName,
                                                                                 LastName = x.LastName,
                                                                                 EmailAddress = x.EmailAddress,
                                                                                 MobileNumber = x.MobileNumber,
                                                                                 GenderCode = x.Gender.SystemName,
                                                                                 GenderName = x.Gender.Name,
                                                                                 CreateDate = x.CreateDate,
                                                                                 StatusId = x.StatusId,
                                                                                 StatusCode = x.Status.SystemName,
                                                                                 LoyaltyTypeName = "Lost",
                                                                                 StatusName = x.Status.Name,
                                                                                 RegistrationSource = x.RegistrationSource.Name,
                                                                                 IconUrl = x.IconStorage.Path,
                                                                                 LastLoginDate = x.LastLoginDate,
                                                                                 LastActivityDate = x.LastActivityDate,
                                                                                 TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                                                 TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                                 TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                                 RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                                 LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.ParentId == _Request.AccountId
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                                             })
                                                        .Where(_Request.SearchCondition)
                                                        .OrderBy(_Request.SortExpression)
                                                        .Skip(_Request.Offset)
                                                        .Take(_Request.Limit)
                                                        .ToList();
                                #endregion
                                #region Create  Response Object
                                _HCoreContext.Dispose();
                                foreach (var DataItem in _Customers)
                                {
                                    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                    {
                                        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                    }
                                    else
                                    {
                                        DataItem.IconUrl = _AppConfig.Default_Icon;
                                    }
                                }
                                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                                #endregion
                            }
                            else
                            {
                                if (_Request.RefreshCount)
                                {
                                    #region Total Records
                                    _Request.TotalRecords = _HCoreContext.cmt_loyalty_customer
                                                            .Where(x => x.owner_id == _Request.AccountId
                                                            && ((x.create_date > _Request.StartDate && x.create_date < _Request.EndDate) || (x.customer.cmt_salecustomer.Any(y => y.merchant_id == _Request.AccountId && y.transaction_date > _Request.StartDate && y.transaction_date < _Request.EndDate)))
                                                            //&& x.customer.HCUAccountTransactionAccount.Any(m => m.AccountId == x.customer_id
                                                            //&& m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                                            //&& m.ParentId == _Request.AccountId
                                                            //&& m.StatusId == HelperStatus.Transaction.Success
                                                            //&& (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                            //&& m.TypeId != TransactionType.ThankUCashPlusCredit
                                                            //)
                                                            )
                                                               .Select(x => new OAccounts.Customer.List
                                                               {
                                                                   ReferenceId = x.customer.Id,
                                                                   ReferenceKey = x.customer.Guid,
                                                                   DisplayName = x.customer.DisplayName,
                                                                   Name = x.customer.Name,
                                                                   FirstName = x.customer.FirstName,
                                                                   LastName = x.customer.LastName,
                                                                   EmailAddress = x.customer.EmailAddress,
                                                                   MobileNumber = x.customer.MobileNumber,
                                                                   GenderCode = x.customer.Gender.SystemName,
                                                                   GenderName = x.customer.Gender.Name,
                                                                   CreateDate = x.customer.CreateDate,
                                                                   StatusId = x.status_id,
                                                                   StatusCode = x.status.SystemName,
                                                                   StatusName = x.status.Name,
                                                                   RegistrationSource = x.customer.RegistrationSource.Name,
                                                                   IconUrl = x.customer.IconStorage.Path,
                                                                   LastLoginDate = x.last_transaction_date,
                                                                   LastActivityDate = x.last_transaction_date,
                                                                   TotalTransaction = x.total_transactions,
                                                                   TotalInvoiceAmount = x.invoice_amount,
                                                                   TotalRewardAmount = x.credit,
                                                                   RedeemAmount = x.debit,
                                                                   LastTransactionDate = x.last_transaction_date,
                                                               })
                                                            .Where(_Request.SearchCondition)
                                                   .Count();
                                    #endregion
                                }
                                #region Data
                                _Customers = _HCoreContext.cmt_loyalty_customer
                                                              .Where(x => x.owner_id == _Request.AccountId
                                                            && ((x.create_date > _Request.StartDate && x.create_date < _Request.EndDate) || (x.customer.cmt_salecustomer.Any(y => y.merchant_id == _Request.AccountId && y.transaction_date > _Request.StartDate && y.transaction_date < _Request.EndDate)))
                                                            //&& x.customer.HCUAccountTransactionAccount.Any(m => m.AccountId == x.customer_id
                                                            //&& m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                                            //&& m.ParentId == _Request.AccountId
                                                            //&& m.StatusId == HelperStatus.Transaction.Success
                                                            //&& (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                            //&& m.TypeId != TransactionType.ThankUCashPlusCredit
                                                            //)
                                                            )
                                                               .Select(x => new OAccounts.Customer.List
                                                               {
                                                                   ReferenceId = x.customer.Id,
                                                                   ReferenceKey = x.customer.Guid,
                                                                   DisplayName = x.customer.DisplayName,
                                                                   Name = x.customer.Name,
                                                                   FirstName = x.customer.FirstName,
                                                                   LastName = x.customer.LastName,
                                                                   EmailAddress = x.customer.EmailAddress,
                                                                   MobileNumber = x.customer.MobileNumber,
                                                                   GenderCode = x.customer.Gender.SystemName,
                                                                   GenderName = x.customer.Gender.Name,
                                                                   CreateDate = x.customer.CreateDate,
                                                                   StatusId = x.status_id,
                                                                   StatusCode = x.status.SystemName,
                                                                   StatusName = x.status.Name,
                                                                   RegistrationSource = x.customer.RegistrationSource.Name,
                                                                   IconUrl = x.customer.IconStorage.Path,
                                                                   LastLoginDate = x.last_transaction_date,
                                                                   LastActivityDate = x.last_transaction_date,
                                                                   TotalTransaction = x.total_transactions,
                                                                   TotalInvoiceAmount = x.invoice_amount,
                                                                   TotalRewardAmount = x.credit,
                                                                   RedeemAmount = x.debit,
                                                                   LastTransactionDate = x.last_transaction_date,
                                                               }).Where(_Request.SearchCondition)
                                                        .OrderBy(_Request.SortExpression)
                                                        .Skip(_Request.Offset)
                                                        .Take(_Request.Limit)
                                                        .ToList();
                                #endregion
                                #region Create  Response Object
                                _HCoreContext.Dispose();
                                foreach (var DataItem in _Customers)
                                {
                                    if (DataItem.TotalTransaction == 0)
                                    {
                                        DataItem.LoyaltyTypeName = "No Tran";
                                    }
                                    else if (DataItem.TotalTransaction == 1)
                                    {
                                        DataItem.LoyaltyTypeName = "New";
                                    }
                                    else if (DataItem.TotalTransaction > 1)
                                    {
                                        DataItem.LoyaltyTypeName = "Loyal";
                                    }

                                    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                    {
                                        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                    }
                                    else
                                    {
                                        DataItem.IconUrl = _AppConfig.Default_Icon;
                                    }
                                }
                                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                                #endregion
                                //if (_Request.RefreshCount)
                                //{
                                //    #region Total Records
                                //    _Request.TotalRecords = _HCoreContext.HCUAccount
                                //                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                //                           && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customercustomer.Any(x => x.owner_id == _Request.AccountId)
                                //                           || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                //                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                //                                                                            && m.ParentId == _Request.AccountId
                                //                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                //                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                //                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                //                                                                             )))
                                //                               .Select(x => new OAccounts.Customer.List
                                //                               {
                                //                                   ReferenceId = x.Id,
                                //                                   ReferenceKey = x.Guid,
                                //                                   DisplayName = x.DisplayName,
                                //                                   Name = x.Name,
                                //                                   FirstName = x.FirstName,
                                //                                   LastName = x.LastName,
                                //                                   EmailAddress = x.EmailAddress,
                                //                                   MobileNumber = x.MobileNumber,
                                //                                   GenderCode = x.Gender.SystemName,
                                //                                   GenderName = x.Gender.Name,
                                //                                   CreateDate = x.CreateDate,
                                //                                   StatusId = x.StatusId,
                                //                                   StatusCode = x.Status.SystemName,
                                //                                   StatusName = x.Status.Name,
                                //                                   RegistrationSource = x.RegistrationSource.Name,
                                //                                   IconUrl = x.IconStorage.Path,
                                //                                   LastLoginDate = x.LastLoginDate,
                                //                                   LastActivityDate = x.LastActivityDate,
                                //                                   TotalTransaction = _HCoreContext.HCUAccountTransaction
                                //                                                                            .Count(m => m.AccountId == x.Id
                                //                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                //                                                                            && m.ParentId == _Request.AccountId
                                //                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                //                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                //                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                //                                   TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                //                                                                            .Where(m => m.AccountId == x.Id
                                //                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                //                                                                            && m.ParentId == _Request.AccountId
                                //                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                //                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                //                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                //                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                //                                   TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                //                                                                            .Where(m => m.AccountId == x.Id
                                //                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                //                                                                            && m.ParentId == _Request.AccountId
                                //                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                //                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                //                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                //                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                //                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                //                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                //                                   RedeemAmount = _HCoreContext.HCUAccountTransaction
                                //                                                                            .Where(m => m.AccountId == x.Id
                                //                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                //                                                                            && m.ParentId == _Request.AccountId
                                //                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                //                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                //                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                //                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                //                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                //                                   LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                //                                                                            .Where(m => m.AccountId == x.Id
                                //                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                //                                                                            && m.ParentId == _Request.AccountId
                                //                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                //                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                //                                                                            .OrderByDescending(m => m.TransactionDate)
                                //                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                //                               })
                                //                            .Where(_Request.SearchCondition)
                                //                   .Count();
                                //    #endregion
                                //}
                                //#region Data
                                //_Customers = _HCoreContext.HCUAccount
                                //                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                //                           && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customercustomer.Any(x => x.owner_id == _Request.AccountId)
                                //                           || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                //                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                //                                                                            && m.ParentId == _Request.AccountId
                                //                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                //                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                //                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                                //                                                                             )))
                                //                               .Select(x => new OAccounts.Customer.List
                                //                               {
                                //                                   ReferenceId = x.Id,
                                //                                   ReferenceKey = x.Guid,
                                //                                   DisplayName = x.DisplayName,
                                //                                   Name = x.Name,
                                //                                   FirstName = x.FirstName,
                                //                                   LastName = x.LastName,
                                //                                   EmailAddress = x.EmailAddress,
                                //                                   MobileNumber = x.MobileNumber,
                                //                                   GenderCode = x.Gender.SystemName,
                                //                                   GenderName = x.Gender.Name,
                                //                                   CreateDate = x.CreateDate,
                                //                                   StatusId = x.StatusId,
                                //                                   StatusCode = x.Status.SystemName,
                                //                                   StatusName = x.Status.Name,
                                //                                   RegistrationSource = x.RegistrationSource.Name,
                                //                                   IconUrl = x.IconStorage.Path,
                                //                                   LastLoginDate = x.LastLoginDate,
                                //                                   LastActivityDate = x.LastActivityDate,
                                //                                   TotalTransaction = _HCoreContext.HCUAccountTransaction
                                //                                                                            .Count(m => m.AccountId == x.Id
                                //                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                //                                                                            && m.ParentId == _Request.AccountId
                                //                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                //                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                //                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                //                                   TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                //                                                                            .Where(m => m.AccountId == x.Id
                                //                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                //                                                                            && m.ParentId == _Request.AccountId
                                //                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                //                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                //                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                //                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                //                                   TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                //                                                                            .Where(m => m.AccountId == x.Id
                                //                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                //                                                                            && m.ParentId == _Request.AccountId
                                //                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                //                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                //                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                //                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                //                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                //                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                //                                   RedeemAmount = _HCoreContext.HCUAccountTransaction
                                //                                                                            .Where(m => m.AccountId == x.Id
                                //                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                //                                                                            && m.ParentId == _Request.AccountId
                                //                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                //                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                //                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                //                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                //                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                //                                   LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                //                                                                            .Where(m => m.AccountId == x.Id
                                //                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                                //                                                                            && m.ParentId == _Request.AccountId
                                //                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                //                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                //                                                                            .OrderByDescending(m => m.TransactionDate)
                                //                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                //                               })
                                //                        .Where(_Request.SearchCondition)
                                //                        .OrderBy(_Request.SortExpression)
                                //                        .Skip(_Request.Offset)
                                //                        .Take(_Request.Limit)
                                //                        .ToList();
                                //#endregion
                                //#region Create  Response Object
                                //_HCoreContext.Dispose();
                                //foreach (var DataItem in _Customers)
                                //{
                                //    if (DataItem.TotalTransaction == 0)
                                //    {
                                //        DataItem.LoyaltyTypeName = "New";
                                //    }
                                //    else if (DataItem.TotalTransaction == 1)
                                //    {
                                //        DataItem.LoyaltyTypeName = "New";
                                //    }
                                //    else if (DataItem.TotalTransaction > 1)
                                //    {
                                //        DataItem.LoyaltyTypeName = "Loyal";
                                //    }

                                //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                //    {
                                //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                //    }
                                //    else
                                //    {
                                //        DataItem.IconUrl = _AppConfig.Default_Icon;
                                //    }
                                //}
                                //OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                                //#endregion
                                //#region Send Response
                                //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                                //#endregion
                            }

                        }
                    }

                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCustomer", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        internal OResponse GetCustomerOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _CustomerDetails = new OAccounts.Customer.Overview();
                if (_Request.StartDate == null)
                {
                    _Request.StartDate = new DateTime(2015, 01, 01, 00, 00, 00);
                }
                if (_Request.EndDate == null)
                {
                    _Request.EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    bool IsTucPlusEnable = _Request.IsTucPlus;
                    //long IsTucPlusEnable = Convert.ToInt64(HCoreHelper.GetConfigurationValueByUserAccount("thankucashplus", _Request.ReferenceId, _Request.UserReference));
                    if (IsTucPlusEnable)
                    {

                        #region Total Records
                        //_CustomerDetails.Total = _HCoreContext.HCUAccount
                        //                        .Count(x => x.AccountTypeId == UserAccountType.Appuser
                        //                        && ( x.OwnerId == _Request.AccountId || x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate)));


                        _CustomerDetails.Total = _HCoreContext.HCUAccount
                                            .Count(x => x.AccountTypeId == UserAccountType.Appuser && !x.cmt_loyalty_customercustomer.Any(a => a.owner_id == _Request.AccountId && a.status_id == HelperStatus.Default.Blocked)
                                           && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any()
                                           || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus
                                                                                            && m.ModeId == TransactionMode.Credit
                                                                                             )));

                        #endregion
                        #region New Records
                        //_CustomerDetails.New = _HCoreContext.HCUAccount
                        //                        .Count(x => x.AccountTypeId == UserAccountType.Appuser
                        //                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1);



                        _CustomerDetails.New = _HCoreContext.HCUAccount
                                               .Count(x => x.AccountTypeId == UserAccountType.Appuser
                                               && !x.cmt_loyalty_customercustomer.Any(a => a.owner_id == _Request.AccountId && a.status_id == HelperStatus.Default.Blocked)
                                              && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any()
                                              || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus
                                                                                            && m.ModeId == TransactionMode.Credit
                                                                                                ))
                                               && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1);


                        #endregion
                        #region Repeating Records
                        //_CustomerDetails.Repeating = _HCoreContext.HCUAccount
                        //                        .Count(x => x.AccountTypeId == UserAccountType.Appuser
                        //                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1);


                        _CustomerDetails.Repeating = _HCoreContext.HCUAccount
                                               .Count(x => x.AccountTypeId == UserAccountType.Appuser
                                               && !x.cmt_loyalty_customercustomer.Any(a => a.owner_id == _Request.AccountId && a.status_id == HelperStatus.Default.Blocked)
                                              && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any()
                                              || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus
                                                                                            && m.ModeId == TransactionMode.Credit
                                                                                                ))
                                               && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1);

                        #endregion
                        //_CustomerDetails.Lost = _HCoreContext.HCUAccount
                        //                        .Count(x => x.AccountTypeId == UserAccountType.Appuser
                        //                          && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate < _Request.StartDate)
                        //                          && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0);



                        #region Lost 
                        _CustomerDetails.Lost = _HCoreContext.HCUAccount
                                                .Count(x => x.AccountTypeId == UserAccountType.Appuser
                                                && !x.cmt_loyalty_customercustomer.Any(a => a.owner_id == _Request.AccountId && a.status_id == HelperStatus.Default.Blocked)
                                               && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any()
                                               || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus
                                                                                            && m.ModeId == TransactionMode.Credit
                                                                                                 ))
                                                && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0);
                        #endregion

                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerDetails, "HC0001");
                        #endregion
                    }
                    else
                    {


                        if (_Request.SubReferenceId > 0)
                        {
                            _CustomerDetails.Total = _HCoreContext.HCUAccount
                                                .Count(x =>
                                                x.AccountTypeId == UserAccountType.Appuser
                                                && !x.cmt_loyalty_customercustomer.Any(a => a.owner_id == _Request.AccountId && a.status_id == HelperStatus.Default.Blocked)
                                                //&& x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate
                                               && (x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId
                                                                                                && m.SubParentId == _Request.SubReferenceId
                                                                                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                                                && m.AccountId == x.Id
                                                                                                && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                 )));


                            #region New Records

                            _CustomerDetails.New = _HCoreContext.HCUAccount
                                                   .Count(x => x.AccountTypeId == UserAccountType.Appuser
                                                   && !x.cmt_loyalty_customercustomer.Any(a => a.owner_id == _Request.AccountId && a.status_id == HelperStatus.Default.Blocked)
                                                  && x.StatusId != HelperStatus.Default.Blocked
                                                  && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate
                                                  && (x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                   && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                   && m.ParentId == _Request.AccountId
                                                                                                   && m.SubParentId == _Request.SubReferenceId
                                                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                                                   && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                   && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                    ))
                                                   && x.HCUAccountTransactionAccount.Count(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.SubParentId == _Request.SubReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1);



                            #endregion
                            #region Repeating Records

                            _CustomerDetails.Repeating = _HCoreContext.HCUAccount
                                                   .Count(x => x.AccountTypeId == UserAccountType.Appuser
                                                   && !x.cmt_loyalty_customercustomer.Any(a => a.owner_id == _Request.AccountId && a.status_id == HelperStatus.Default.Blocked)
                                                  && x.StatusId != HelperStatus.Default.Blocked
                                                  //&& x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate
                                                  && (x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                   && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                   && m.ParentId == _Request.AccountId
                                                                                                   && m.SubParentId == _Request.SubReferenceId
                                                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                                                   && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                   && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                    ))
                                                   && x.HCUAccountTransactionAccount.Count(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.SubParentId == _Request.SubReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1);

                            #endregion                          

                            #region Lost 
                            _CustomerDetails.Lost = _HCoreContext.HCUAccount
                                                    .Count(x => x.AccountTypeId == UserAccountType.Appuser
                                                    && !x.cmt_loyalty_customercustomer.Any(a => a.owner_id == _Request.AccountId && a.status_id == HelperStatus.Default.Blocked)
                                                    //&& x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate
                                                    && (x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.SubParentId == _Request.SubReferenceId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                     ))
                                                    && x.HCUAccountTransactionAccount.Count(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.SubParentId == _Request.SubReferenceId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0);
                            #endregion
                        }
                        else
                        {
                            _CustomerDetails.Total = _HCoreContext.cmt_loyalty_customer
                                                        .Where(x => x.owner_id == _Request.AccountId
                                                        && (x.customer.OwnerId == _Request.AccountId || x.last_transaction_date >= _Request.StartDate && x.last_transaction_date <= _Request.EndDate))
                                                       .Count();

                            #region New Records
                            //_CustomerDetails.New = _HCoreContext.cmt_loyalty_customer
                            //                            .Where(x => x.status_id == HelperStatus.Default.Active
                            //                            && x.owner_id == _Request.AccountId
                            //                            && ((x.customer.CreateDate > _Request.StartDate && x.customer.CreateDate < _Request.EndDate) || x.last_transaction_date > _Request.StartDate && x.last_transaction_date < _Request.EndDate))
                            //                           .Count();

                            _CustomerDetails.New = _HCoreContext.cmt_loyalty_customer
                                                    .Where(x => x.owner_id == _Request.AccountId
                                                    && ((x.create_date > _Request.StartDate && x.create_date < _Request.EndDate))
                                                    //&& ((x.customer.CreateDate > _Request.StartDate && x.customer.CreateDate < _Request.EndDate)
                                                    //                      || x.customer.cmt_salecustomer.Any(m => m.customer_id == x.customer_id
                                                    //                                                                       && m.transaction_date > _Request.StartDate && m.transaction_date < _Request.EndDate
                                                    //                                                                       && m.merchant_id == _Request.AccountId
                                                    //                                                                       && m.status_id == HelperStatus.Transaction.Success))
                                                    //&& x.customer.cmt_salecustomer.Count(m => m.customer_id == x.customer_id && m.merchant_id == _Request.AccountId && m.transaction_date > _Request.StartDate && m.transaction_date < _Request.EndDate) == 1
                                                    )
                                                   .Count();
                            //&& ((x.customer.CreateDate > _Request.StartDate && x.customer.CreateDate < _Request.EndDate)
                            //                      || x.customer.HCUAccountTransactionAccount.Any(m => m.AccountId == x.customer_id
                            //                                                                       && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                            //                                                                       && m.ParentId == _Request.AccountId
                            //                                                                       && m.StatusId == HelperStatus.Transaction.Success
                            //                                                                       && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                            //                                                                       && m.TypeId != TransactionType.ThankUCashPlusCredit
                            //                                                                        ))
                            //&& x.customer.HCUAccountTransactionAccount.Count(m => m.AccountId == x.customer_id && m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                            //.Count();
                            //_CustomerDetails.New = _HCoreContext.HCUAccount
                            //                       .Count(x => x.AccountTypeId == UserAccountType.Appuser
                            //                      && x.StatusId != HelperStatus.Default.Blocked
                            //                      && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate
                            //                      && (x.OwnerId == _Request.AccountId
                            //                      || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                            //                                                                       && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                            //                                                                       && m.ParentId == _Request.AccountId
                            //                                                                       && m.StatusId == HelperStatus.Transaction.Success
                            //                                                                       && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                            //                                                                       && m.TypeId != TransactionType.ThankUCashPlusCredit
                            //                                                                        ))
                            //                       && x.HCUAccountTransactionAccount.Count(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1);
                            #endregion
                            #region Repeating Records
                            //_CustomerDetails.Repeating = _HCoreContext.HCUAccount
                            //                        .Count(x => x.AccountTypeId == UserAccountType.Appuser
                            //                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1);

                            //_CustomerDetails.Repeating = _HCoreContext.cmt_loyalty_customer
                            //                           .Where(x => x.status_id == HelperStatus.Default.Active
                            //                           && x.owner_id == _Request.AccountId
                            //                           && x.customer.HCUAccountTransactionAccount.Count(m => m.AccountId == x.customer_id && m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                            //                          .Count();
                            _CustomerDetails.Repeating = _HCoreContext.cmt_loyalty_customer
                                                    .Where(x => x.owner_id == _Request.AccountId
                                                    //&& ((x.customer.CreateDate > _Request.StartDate && x.customer.CreateDate < _Request.EndDate)
                                                    //                      || x.customer.cmt_salecustomer.Any(m => m.customer_id == x.customer_id
                                                    //                                                                       && m.transaction_date > _Request.StartDate && m.transaction_date < _Request.EndDate
                                                    //                                                                       && m.merchant_id == _Request.AccountId
                                                    //                                                                       && m.status_id == HelperStatus.Transaction.Success
                                                    //                                                                       && (m.source_id == TransactionSource.TUC || m.source_id == TransactionSource.TUCBlack)
                                                    //                                                                       && m.type_id != TransactionType.ThankUCashPlusCredit
                                                    //                                                                        ))
                                                    && x.customer.cmt_salecustomer.Count(m => m.merchant_id == _Request.AccountId && m.transaction_date > _Request.StartDate && m.transaction_date < _Request.EndDate) > 1)
                                                   .Count();
                            //_CustomerDetails.Repeating = _HCoreContext.HCUAccount
                            //                       .Count(x => x.AccountTypeId == UserAccountType.Appuser
                            //                       //&& !x.cmt_loyalty_customercustomer.Any(a => a.MerchantId == _Request.AccountId && a.StatusId == HelperStatus.Default.Blocked)
                            //                       //&& x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate
                            //                      && (x.OwnerId == _Request.AccountId
                            //                      //|| x.cmt_loyalty_customerowner.Any(x => x.MerchantId == _Request.AccountId)
                            //                      || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                            //                                                                       && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                            //                                                                       && m.ParentId == _Request.AccountId
                            //                                                                       && m.StatusId == HelperStatus.Transaction.Success
                            //                                                                       && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                            //                                                                       && m.TypeId != TransactionType.ThankUCashPlusCredit
                            //                                                                        ))
                            //                       && x.HCUAccountTransactionAccount.Count(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1);

                            #endregion

                            #region Lost
                            _CustomerDetails.Lost = _HCoreContext.cmt_loyalty_customer
                                                    .Where(x => x.owner_id == _Request.AccountId
                                                    && x.create_date < _Request.StartDate
                                                    && x.customer.cmt_salecustomer.Count(m => m.merchant_id == _Request.AccountId && m.transaction_date > _Request.StartDate && m.transaction_date < _Request.EndDate) == 0)
                                                   .Count();
                            //_CustomerDetails.Lost = _HCoreContext.HCUAccount
                            //                        .Count(x => x.AccountTypeId == UserAccountType.Appuser
                            //                       // && !x.cmt_loyalty_customercustomer.Any(a => a.MerchantId == _Request.AccountId && a.StatusId == HelperStatus.Default.Blocked)
                            //                       //&& x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate
                            //                       && (x.OwnerId == _Request.AccountId
                            //                       //|| x.cmt_loyalty_customerowner.Any(x => x.MerchantId == _Request.AccountId)
                            //                       || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                            //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                            //                                                                        && m.ParentId == _Request.AccountId
                            //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                            //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                            //                                                                        && m.TypeId != TransactionType.ThankUCashPlusCredit
                            //                                                                         ))
                            //                        && x.HCUAccountTransactionAccount.Count(m => m.AccountId == x.Id && m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0);
                            #endregion

                            ////var Transactions = _HCoreContext.HCUAccountTransaction
                            ////    .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                            ////    && x.StatusId == HelperStatus.Transaction.Success
                            ////    && x.ParentId == _Request.AccountId
                            ////    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack)
                            ////    && x.TypeId != TransactionType.ThankUCashPlusCredit)
                            ////    .Select(x => new
                            ////    {
                            ////        TransactionDate = x.TransactionDate,
                            ////        AccountId = x.AccountId
                            ////    }).ToList();

                            //_CustomerDetails.Total = _HCoreContext.HCUAccount
                            //                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                            //                            //&& !x.cmt_loyalty_customercustomer.Any(a => a.MerchantId == _Request.AccountId && a.StatusId == HelperStatus.Default.Blocked) && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate
                            //                           && (x.OwnerId == _Request.AccountId
                            //                           //|| x.cmt_loyalty_customercustomer.Any(x => x.MerchantId == _Request.AccountId)
                            //                           || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                            //                                                                            && m.TransactionDate >= _Request.StartDate && m.TransactionDate <= _Request.EndDate
                            //                                                                            && m.ParentId == _Request.AccountId
                            //                                                                            && m.StatusId == HelperStatus.Transaction.Success
                            //                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                            //                                                                            && m.TypeId != TransactionType.ThankUCashPlusCredit
                            //                                                                             )))
                            //                           .Count();


                            //#region New Records
                            ////_CustomerDetails.New = _HCoreContext.HCUAccount
                            ////                        .Count(x => x.AccountTypeId == UserAccountType.Appuser
                            ////                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1);
                            //_CustomerDetails.New = _HCoreContext.HCUAccount
                            //                       .Count(x => x.AccountTypeId == UserAccountType.Appuser
                            //                       //&& !x.cmt_loyalty_customercustomer.Any(a => a.MerchantId == _Request.AccountId && a.StatusId == HelperStatus.Default.Blocked)
                            //                      && x.StatusId != HelperStatus.Default.Blocked
                            //                      && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate
                            //                      && (x.OwnerId == _Request.AccountId
                            //                      //|| x.cmt_loyalty_customerowner.Any(x => x.MerchantId == _Request.AccountId)
                            //                      || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                            //                                                                       && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                            //                                                                       && m.ParentId == _Request.AccountId
                            //                                                                       && m.StatusId == HelperStatus.Transaction.Success
                            //                                                                       && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                            //                                                                       && m.TypeId != TransactionType.ThankUCashPlusCredit
                            //                                                                        ))
                            //                       && x.HCUAccountTransactionAccount.Count(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1);


                            //#endregion
                            //#region Repeating Records
                            ////_CustomerDetails.Repeating = _HCoreContext.HCUAccount
                            ////                        .Count(x => x.AccountTypeId == UserAccountType.Appuser
                            ////                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1);


                            //_CustomerDetails.Repeating = _HCoreContext.HCUAccount
                            //                       .Count(x => x.AccountTypeId == UserAccountType.Appuser
                            //                       //&& !x.cmt_loyalty_customercustomer.Any(a => a.MerchantId == _Request.AccountId && a.StatusId == HelperStatus.Default.Blocked)
                            //                       //&& x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate
                            //                      && (x.OwnerId == _Request.AccountId
                            //                      //|| x.cmt_loyalty_customerowner.Any(x => x.MerchantId == _Request.AccountId)
                            //                      || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                            //                                                                       && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                            //                                                                       && m.ParentId == _Request.AccountId
                            //                                                                       && m.StatusId == HelperStatus.Transaction.Success
                            //                                                                       && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                            //                                                                       && m.TypeId != TransactionType.ThankUCashPlusCredit
                            //                                                                        ))
                            //                       && x.HCUAccountTransactionAccount.Count(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1);

                            //#endregion

                            //#region Lost 
                            //_CustomerDetails.Lost = _HCoreContext.HCUAccount
                            //                        .Count(x => x.AccountTypeId == UserAccountType.Appuser
                            //                       // && !x.cmt_loyalty_customercustomer.Any(a => a.MerchantId == _Request.AccountId && a.StatusId == HelperStatus.Default.Blocked)
                            //                       //&& x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate
                            //                       && (x.OwnerId == _Request.AccountId
                            //                       //|| x.cmt_loyalty_customerowner.Any(x => x.MerchantId == _Request.AccountId)
                            //                       || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                            //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                            //                                                                        && m.ParentId == _Request.AccountId
                            //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                            //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                            //                                                                        && m.TypeId != TransactionType.ThankUCashPlusCredit
                            //                                                                         ))
                            //                        && x.HCUAccountTransactionAccount.Count(m => m.AccountId == x.Id && m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0);
                            //#endregion
                        }
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerDetails, "HC0001");
                        #endregion                       
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetCustomerOverview", _Exception, _Request.UserReference, _CustomerDetails, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion

        }
        internal void GetCustomerDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.StartDate == null)
                {
                    _Request.StartDate = new DateTime(2015, 01, 01, 00, 00, 00);
                }
                if (_Request.EndDate == null)
                {
                    _Request.EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                }
                _Customers = new List<OAccounts.Customer.List>();
                long StorageId = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                    _HCUAccountParameter.Name = "Customers_Sheet";
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Inactive;
                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    _HCoreContext.SaveChanges();
                    StorageId = _HCUAccountParameter.Id;
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                int RecordLimit = 2000;
                int iterations = 1;
                using (_HCoreContext = new HCoreContext())
                {
                    bool IsTucPlusEnable = _Request.IsTucPlus;
                    if (IsTucPlusEnable)
                    {
                        if (_Request.Type == "new")
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                    && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any() || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))))
                                                                                                      && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                     .Select(x => new OAccounts.Customer.List
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         DisplayName = x.DisplayName,
                                                         Name = x.Name,
                                                         FirstName = x.FirstName,
                                                         LastName = x.LastName,
                                                         EmailAddress = x.EmailAddress,
                                                         MobileNumber = x.MobileNumber,
                                                         CreateDate = x.CreateDate,
                                                         GenderCode = x.Gender.SystemName,
                                                         GenderName = x.Gender.Name,
                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name,
                                                         IconUrl = x.IconStorage.Path,
                                                         RegistrationSource = x.RegistrationSource.Name,
                                                         TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))
),
                                                         TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                         TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                         TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                         TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                         RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                         TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                     && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                         LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                    .OrderByDescending(m => m.TransactionDate)
                                                                                                    .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                     })
                                                    .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                            if (_Request.TotalRecords > RecordLimit)
                            {
                                iterations = (_Request.TotalRecords / RecordLimit) + 1;
                                _Request.Offset = 0;
                                _Request.Limit = 2000;
                            }
                            else
                            {
                                iterations = 1;
                                _Request.Offset = 0;
                                _Request.Limit = _Request.TotalRecords;
                            }
                            #region Data

                            for (int i = 0; i < iterations; i++)
                            {
                                _Customers.AddRange(_HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any() || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                      && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                      && m.ParentId == _Request.AccountId
                                                                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                                                                      && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))))
                                                                                                          && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           CreateDate = x.CreateDate,
                                                           GenderCode = x.Gender.SystemName,
                                                           GenderName = x.Gender.Name,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,
                                                           IconUrl = x.IconStorage.Path,
                                                           RegistrationSource = x.RegistrationSource.Name,
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))
    ),
                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                           TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                           TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                           TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                           LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList());
                                _Request.Offset = _Request.Offset + RecordLimit;
                            }

                            #endregion
                        }
                        else if (_Request.Type == "loyal")
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                    && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any() || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))))
                                                                                                      && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                     .Select(x => new OAccounts.Customer.List
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         DisplayName = x.DisplayName,
                                                         Name = x.Name,
                                                         FirstName = x.FirstName,
                                                         LastName = x.LastName,
                                                         EmailAddress = x.EmailAddress,
                                                         MobileNumber = x.MobileNumber,
                                                         CreateDate = x.CreateDate,
                                                         GenderCode = x.Gender.SystemName,
                                                         GenderName = x.Gender.Name,
                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name,
                                                         IconUrl = x.IconStorage.Path,
                                                         RegistrationSource = x.RegistrationSource.Name,
                                                         TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))
),
                                                         TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                         TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                         TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                         TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                         RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                         TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                     && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                         LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                    .OrderByDescending(m => m.TransactionDate)
                                                                                                    .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                     })
                                                    .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                            if (_Request.TotalRecords > RecordLimit)
                            {
                                iterations = (_Request.TotalRecords / RecordLimit) + 1;
                                _Request.Offset = 0;
                                _Request.Limit = 2000;
                            }
                            else
                            {
                                iterations = 1;
                                _Request.Offset = 0;
                                _Request.Limit = _Request.TotalRecords;
                            }
                            #region Data
                            for (int i = 0; i < iterations; i++)
                            {
                                _Customers.AddRange(_HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any() || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))))
                                                                                                          && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           CreateDate = x.CreateDate,
                                                           GenderCode = x.Gender.SystemName,
                                                           GenderName = x.Gender.Name,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,
                                                           IconUrl = x.IconStorage.Path,
                                                           RegistrationSource = x.RegistrationSource.Name,
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))
    ),
                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                           TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                           TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                           TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                           LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList());
                                _Request.Offset = _Request.Offset + RecordLimit;
                            }

                            #endregion
                        }
                        else if (_Request.Type == "lost")
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                    && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any() || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                   && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                   && m.ParentId == _Request.AccountId
                                                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                                                   && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))))
                                                                                                      && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                     .Select(x => new OAccounts.Customer.List
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         DisplayName = x.DisplayName,
                                                         Name = x.Name,
                                                         FirstName = x.FirstName,
                                                         LastName = x.LastName,
                                                         EmailAddress = x.EmailAddress,
                                                         MobileNumber = x.MobileNumber,
                                                         CreateDate = x.CreateDate,
                                                         GenderCode = x.Gender.SystemName,
                                                         GenderName = x.Gender.Name,
                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name,
                                                         IconUrl = x.IconStorage.Path,
                                                         RegistrationSource = x.RegistrationSource.Name,
                                                         TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))
),
                                                         TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                         TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                         TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                         TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                         RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                         TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                     && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                         LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                    .OrderByDescending(m => m.TransactionDate)
                                                                                                    .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                     })
                                                    .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                            if (_Request.TotalRecords > RecordLimit)
                            {
                                iterations = (_Request.TotalRecords / RecordLimit) + 1;
                                _Request.Offset = 0;
                                _Request.Limit = 2000;
                            }
                            else
                            {
                                iterations = 1;
                                _Request.Offset = 0;
                                _Request.Limit = _Request.TotalRecords;
                            }
                            #region Data
                            for (int i = 0; i < iterations; i++)
                            {
                                _Customers.AddRange(_HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any() || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                       && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                       && m.ParentId == _Request.AccountId
                                                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                                                       && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))))
                                                                                                          && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           CreateDate = x.CreateDate,
                                                           GenderCode = x.Gender.SystemName,
                                                           GenderName = x.Gender.Name,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,
                                                           IconUrl = x.IconStorage.Path,
                                                           RegistrationSource = x.RegistrationSource.Name,
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))
    ),
                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                           TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                           TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                           TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                           LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList());
                                _Request.Offset = _Request.Offset + RecordLimit;
                            }
                            #endregion
                        }
                        else
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                    && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any() || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))))
                                                     .Select(x => new OAccounts.Customer.List
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         DisplayName = x.DisplayName,
                                                         Name = x.Name,
                                                         FirstName = x.FirstName,
                                                         LastName = x.LastName,
                                                         EmailAddress = x.EmailAddress,
                                                         MobileNumber = x.MobileNumber,
                                                         CreateDate = x.CreateDate,
                                                         GenderCode = x.Gender.SystemName,
                                                         GenderName = x.Gender.Name,
                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name,
                                                         IconUrl = x.IconStorage.Path,
                                                         RegistrationSource = x.RegistrationSource.Name,
                                                         TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))
),
                                                         TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                         TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                         TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                         TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                         RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                         TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                     && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                         LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                    .OrderByDescending(m => m.TransactionDate)
                                                                                                    .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                     })
                                                    .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                            if (_Request.TotalRecords > RecordLimit)
                            {
                                iterations = (_Request.TotalRecords / RecordLimit) + 1;
                                _Request.Offset = 0;
                                _Request.Limit = 2000;
                            }
                            else
                            {
                                iterations = 1;
                                _Request.Offset = 0;
                                _Request.Limit = _Request.TotalRecords;
                            }
                            #region Data
                            for (int i = 0; i < iterations; i++)
                            {
                                _Customers.AddRange(_HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any() || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))))
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           CreateDate = x.CreateDate,
                                                           GenderCode = x.Gender.SystemName,
                                                           GenderName = x.Gender.Name,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,
                                                           IconUrl = x.IconStorage.Path,
                                                           RegistrationSource = x.RegistrationSource.Name,
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC))
    ),
                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && ((m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || (m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC)))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                           TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                           TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                           TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                           LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList());
                                _Request.Offset = _Request.Offset + RecordLimit;
                            }

                            #endregion
                        }
                    }
                    else
                    {

                        if (_Request.Type == "new")
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                   && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any()
                                                   || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                     ))
                                                                                                      && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           GenderCode = x.Gender.SystemName,
                                                           GenderName = x.Gender.Name,
                                                           CreateDate = x.CreateDate,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,
                                                           IconUrl = x.IconStorage.Path,
                                                           LoyaltyTypeName = "New",
                                                           RegistrationSource = x.RegistrationSource.Name,
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                           TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                    .OrderByDescending(m => m.TransactionDate)
                                                                                                    .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                       })
                                                    .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                            if (_Request.TotalRecords > RecordLimit)
                            {
                                iterations = (_Request.TotalRecords / RecordLimit) + 1;
                                _Request.Offset = 0;
                                _Request.Limit = 2000;
                            }
                            else
                            {
                                iterations = 1;
                                _Request.Offset = 0;
                                _Request.Limit = _Request.TotalRecords;
                            }
                            #region Data
                            for (int i = 0; i < iterations; i++)
                            {
                                _Customers.AddRange(_HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                       && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any()
                                                       || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                        && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                         ))
                                                                                                          && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                                     .Select(x => new OAccounts.Customer.List
                                                                     {
                                                                         ReferenceId = x.Id,
                                                                         ReferenceKey = x.Guid,
                                                                         DisplayName = x.DisplayName,
                                                                         Name = x.Name,
                                                                         FirstName = x.FirstName,
                                                                         LastName = x.LastName,
                                                                         LoyaltyTypeName = "New",
                                                                         EmailAddress = x.EmailAddress,
                                                                         MobileNumber = x.MobileNumber,
                                                                         GenderCode = x.Gender.SystemName,
                                                                         GenderName = x.Gender.Name,
                                                                         CreateDate = x.CreateDate,
                                                                         StatusId = x.StatusId,
                                                                         StatusCode = x.Status.SystemName,
                                                                         StatusName = x.Status.Name,
                                                                         IconUrl = x.IconStorage.Path,
                                                                         RegistrationSource = x.RegistrationSource.Name,
                                                                         TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                        && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                                         TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                        && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                         TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                         RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                         LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                                     })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList());
                                _Request.Offset = _Request.Offset + RecordLimit;
                            }

                            #endregion
                        }
                        else if (_Request.Type == "loyal")
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                   && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any()
                                                   || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                     ))
                                                                                                      && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           GenderCode = x.Gender.SystemName,
                                                           GenderName = x.Gender.Name,
                                                           CreateDate = x.CreateDate,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,
                                                           IconUrl = x.IconStorage.Path,
                                                           LoyaltyTypeName = "Loyal",
                                                           RegistrationSource = x.RegistrationSource.Name,
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                           TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                    .OrderByDescending(m => m.TransactionDate)
                                                                                                    .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                       })
                                                    .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                            if (_Request.TotalRecords > RecordLimit)
                            {
                                iterations = (_Request.TotalRecords / RecordLimit) + 1;
                                _Request.Offset = 0;
                                _Request.Limit = 2000;
                            }
                            else
                            {
                                iterations = 1;
                                _Request.Offset = 0;
                                _Request.Limit = _Request.TotalRecords;
                            }
                            #region Data
                            for (int i = 0; i < iterations; i++)
                            {
                                _Customers.AddRange(_HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                       && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any()
                                                       || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                        && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                         ))
                                                                                                          && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                                     .Select(x => new OAccounts.Customer.List
                                                                     {
                                                                         ReferenceId = x.Id,
                                                                         ReferenceKey = x.Guid,
                                                                         DisplayName = x.DisplayName,
                                                                         Name = x.Name,
                                                                         FirstName = x.FirstName,
                                                                         LastName = x.LastName,
                                                                         EmailAddress = x.EmailAddress,
                                                                         MobileNumber = x.MobileNumber,
                                                                         GenderCode = x.Gender.SystemName,
                                                                         GenderName = x.Gender.Name,
                                                                         CreateDate = x.CreateDate,
                                                                         StatusId = x.StatusId,
                                                                         StatusCode = x.Status.SystemName,
                                                                         StatusName = x.Status.Name,
                                                                         IconUrl = x.IconStorage.Path,
                                                                         LoyaltyTypeName = "Loyal",
                                                                         RegistrationSource = x.RegistrationSource.Name,
                                                                         TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                        && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                                         TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                        && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                         TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                         RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                         LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                                     })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList());
                                _Request.Offset = _Request.Offset + RecordLimit;
                            }
                            #endregion
                        }
                        else if (_Request.Type == "lost")
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                   && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any()
                                                   || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                     ))
                                                                                                      && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                          .Select(x => new OAccounts.Customer.List
                                                          {
                                                              ReferenceId = x.Id,
                                                              ReferenceKey = x.Guid,
                                                              DisplayName = x.DisplayName,
                                                              Name = x.Name,
                                                              FirstName = x.FirstName,
                                                              LastName = x.LastName,
                                                              EmailAddress = x.EmailAddress,
                                                              MobileNumber = x.MobileNumber,
                                                              GenderCode = x.Gender.SystemName,
                                                              GenderName = x.Gender.Name,
                                                              CreateDate = x.CreateDate,
                                                              StatusId = x.StatusId,
                                                              LoyaltyTypeName = "Lost",
                                                              StatusCode = x.Status.SystemName,
                                                              StatusName = x.Status.Name,
                                                              IconUrl = x.IconStorage.Path,
                                                              RegistrationSource = x.RegistrationSource.Name,
                                                              TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                              TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                    && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                              TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                              RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                              LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.ParentId == _Request.AccountId
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                    .OrderByDescending(m => m.TransactionDate)
                                                                                                    .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                          })
                                                    .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                            if (_Request.TotalRecords > RecordLimit)
                            {
                                iterations = (_Request.TotalRecords / RecordLimit) + 1;
                                _Request.Offset = 0;
                                _Request.Limit = 2000;
                            }
                            else
                            {
                                iterations = 1;
                                _Request.Offset = 0;
                                _Request.Limit = _Request.TotalRecords;
                            }
                            #region Data
                            for (int i = 0; i < iterations; i++)
                            {
                                _Customers.AddRange(_HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                       && (x.OwnerId == _Request.AccountId || x.cmt_loyalty_customerowner.Any()
                                                       || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                        && m.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                         ))
                                                                                                          && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                                         .Select(x => new OAccounts.Customer.List
                                                                         {
                                                                             ReferenceId = x.Id,
                                                                             ReferenceKey = x.Guid,
                                                                             DisplayName = x.DisplayName,
                                                                             Name = x.Name,
                                                                             FirstName = x.FirstName,
                                                                             LastName = x.LastName,
                                                                             EmailAddress = x.EmailAddress,
                                                                             MobileNumber = x.MobileNumber,
                                                                             GenderCode = x.Gender.SystemName,
                                                                             GenderName = x.Gender.Name,
                                                                             CreateDate = x.CreateDate,
                                                                             StatusId = x.StatusId,
                                                                             StatusCode = x.Status.SystemName,
                                                                             LoyaltyTypeName = "Lost",
                                                                             StatusName = x.Status.Name,
                                                                             IconUrl = x.IconStorage.Path,
                                                                             RegistrationSource = x.RegistrationSource.Name,
                                                                             TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                        && m.TypeId != TransactionType.ThankUCashPlusCredit),

                                                                             TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                                                                        && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                                             TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,



                                                                             RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                                             LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                                         })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList());
                                _Request.Offset = _Request.Offset + RecordLimit;
                            }
                            #endregion
                        }
                        else
                        {
                            #region Total Records

                            _Request.TotalRecords = _HCoreContext.cmt_loyalty_customer
                                                              .Where(x => x.owner_id == _Request.AccountId
                                                            && ((x.create_date > _Request.StartDate && x.create_date < _Request.EndDate) || (x.customer.cmt_salecustomer.Any(y => y.merchant_id == _Request.AccountId && y.transaction_date > _Request.StartDate && y.transaction_date < _Request.EndDate)))
                                                            )
                                                               .Select(x => new OAccounts.Customer.List
                                                               {
                                                                   ReferenceId = x.customer.Id,
                                                                   ReferenceKey = x.customer.Guid,
                                                                   DisplayName = x.customer.DisplayName,
                                                                   Name = x.customer.Name,
                                                                   FirstName = x.customer.FirstName,
                                                                   MiddleName = x.customer.MiddleName,
                                                                   LastName = x.customer.LastName,
                                                                   EmailAddress = x.customer.EmailAddress,
                                                                   MobileNumber = x.customer.MobileNumber,
                                                                   GenderCode = x.customer.Gender.SystemName,
                                                                   GenderName = x.customer.Gender.Name,
                                                                   CreateDate = x.customer.CreateDate,
                                                                   StatusId = x.status_id,
                                                                   StatusCode = x.status.SystemName,
                                                                   StatusName = x.status.Name,
                                                                   RegistrationSource = x.customer.RegistrationSource.Name,
                                                                   IconUrl = x.customer.IconStorage.Path,
                                                                   LastLoginDate = x.last_transaction_date,
                                                                   LastActivityDate = x.last_transaction_date,
                                                                   TotalTransaction = x.total_transactions,
                                                                   TotalInvoiceAmount = x.invoice_amount,
                                                                   TotalRewardAmount = x.credit,
                                                                   RedeemAmount = x.debit,
                                                                   LastTransactionDate = x.last_transaction_date,
                                                               }).Where(_Request.SearchCondition)
                                           .Count();
                            #endregion

                            if (_Request.TotalRecords > RecordLimit)
                            {
                                iterations = (_Request.TotalRecords / RecordLimit) + 1;
                                _Request.Offset = 0;
                                _Request.Limit = 2000;
                            }
                            else
                            {
                                iterations = 1;
                                _Request.Offset = 0;
                                _Request.Limit = _Request.TotalRecords;
                            }
                            #region Data
                            for (int i = 0; i < iterations; i++)
                            {
                                _Customers.AddRange(_HCoreContext.cmt_loyalty_customer
                                                              .Where(x => x.owner_id == _Request.AccountId
                                                            && ((x.create_date > _Request.StartDate && x.create_date < _Request.EndDate) || (x.customer.cmt_salecustomer.Any(y => y.merchant_id == _Request.AccountId && y.transaction_date > _Request.StartDate && y.transaction_date < _Request.EndDate)))
                                                            )
                                                               .Select(x => new OAccounts.Customer.List
                                                               {
                                                                   ReferenceId = x.customer.Id,
                                                                   ReferenceKey = x.customer.Guid,
                                                                   DisplayName = x.customer.DisplayName,
                                                                   Name = x.customer.Name,
                                                                   FirstName = x.customer.FirstName,
                                                                   MiddleName = x.customer.MiddleName,
                                                                   LastName = x.customer.LastName,
                                                                   EmailAddress = x.customer.EmailAddress,
                                                                   MobileNumber = x.customer.MobileNumber,
                                                                   GenderCode = x.customer.Gender.SystemName,
                                                                   GenderName = x.customer.Gender.Name,
                                                                   CreateDate = x.customer.CreateDate,
                                                                   StatusId = x.status_id,
                                                                   StatusCode = x.status.SystemName,
                                                                   StatusName = x.status.Name,
                                                                   RegistrationSource = x.customer.RegistrationSource.Name,
                                                                   IconUrl = x.customer.IconStorage.Path,
                                                                   LastLoginDate = x.last_transaction_date,
                                                                   LastActivityDate = x.last_transaction_date,
                                                                   TotalTransaction = x.total_transactions,
                                                                   TotalInvoiceAmount = x.invoice_amount,
                                                                   TotalRewardAmount = x.credit,
                                                                   RedeemAmount = x.debit,
                                                                   LastTransactionDate = x.last_transaction_date,
                                                               }).Where(_Request.SearchCondition)
                                    .OrderBy(_Request.SortExpression)
                                    .Skip(_Request.Offset)
                                    .Take(_Request.Limit)
                                    .ToList());
                                _Request.Offset = _Request.Offset + RecordLimit;
                            }
                            #endregion

                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Customers)
                            {
                                if (DataItem.TotalTransaction == 0)
                                {
                                    DataItem.LoyaltyTypeName = "New";
                                }
                                else if (DataItem.TotalTransaction == 1)
                                {
                                    DataItem.LoyaltyTypeName = "New";
                                }
                                else if (DataItem.TotalTransaction > 1)
                                {
                                    DataItem.LoyaltyTypeName = "Loyal";
                                }
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            #endregion
                        }
                    }
                    if (_Customers.Count > 0)
                    {
                        List<OAccounts.Customer.Download> _Cust = new List<OAccounts.Customer.Download>();
                        foreach (var x in _Customers)
                        {
                            _Cust.Add(new OAccounts.Customer.Download
                            {
                                CustomerId = x.ReferenceId,
                                DisplayName = x.DisplayName,
                                Name = x.Name,
                                FirstName = x.FirstName,
                                MiddleName = x.MiddleName,
                                LastName = x.LastName,
                                MobileNumber = x.MobileNumber,
                                EmailAddress = x.EmailAddress,
                                Gender = x.GenderName,

                                TotalRewardTransaction = x.TotalRewardTransaction,
                                TotalRewardAmount = x.TotalRewardAmount,
                                TotalRewardInvoiceAmount = x.TotalRewardInvoiceAmount,

                                TotalRedeemTransaction = x.RedeemTransaction,
                                TotalRedeemAmount = x.RedeemAmount,
                                TotalRedeemInvoiceAmount = x.RedeemInvoiceAmount,

                                LastTransactionDate = x.LastTransactionDate,
                                RegisteredOn = x.CreateDate,
                                RegistrationSource = x.RegistrationSource
                            });
                        }
                        using (var _XLWorkbook = new ClosedXML.Excel.XLWorkbook())
                        {
                            var _WorkSheet = _XLWorkbook.Worksheets.Add("Rewards_Sheet");
                            PropertyInfo[] properties = _Cust.First().GetType().GetProperties();
                            List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                            for (int i = 0; i < headerNames.Count; i++)
                            {
                                _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                            }
                            _WorkSheet.Cell(2, 1).InsertData(_Cust);
                            MemoryStream _MemoryStream = new MemoryStream();
                            _XLWorkbook.SaveAs(_MemoryStream);
                            long? _StorageId = HCoreHelper.SaveStorage("Customers_Sheet", "xlsx", _MemoryStream, _Request.UserReference);
                            if (_StorageId != null && _StorageId != 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                                    if (TItem != null)
                                    {
                                        TItem.IconStorageId = _StorageId;
                                        TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        TItem.ModifyById = _Request.UserReference.AccountId;
                                        TItem.StatusId = HelperStatus.Default.Active;
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                        }
                    }

                    //long IsTucPlusEnable = Convert.ToInt64(HCoreHelper.GetConfigurationValueByUserAccount("thankucashplus", _Request.ReferenceId, _Request.UserReference));
                    //if (IsTucPlusEnable == 1)
                    //{
                    //    if (_Request.Type == "new")
                    //    {
                    //        if (_Request.RefreshCount)
                    //        {
                    //            #region Total Records
                    //            _Request.TotalRecords = _HCoreContext.HCUAccount
                    //                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                    && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                    //                                    .Select(x => new OAccounts.Customer.List
                    //                                    {
                    //                                        ReferenceId = x.Id,
                    //                                        ReferenceKey = x.Guid,
                    //                                        DisplayName = x.DisplayName,
                    //                                        Name = x.Name,
                    //                                        FirstName = x.FirstName,
                    //                                        LastName = x.LastName,
                    //                                        EmailAddress = x.EmailAddress,
                    //                                        MobileNumber = x.MobileNumber,
                    //                                        LastTransactionDate = x.LastTransactionDate,
                    //                                        CreateDate = x.CreateDate,
                    //                                        StatusId = x.StatusId,
                    //                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                    //                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                    //                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                    //                                        TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.ReferenceId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                    //                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                    //                                    })
                    //                                    .Where(_Request.SearchCondition)
                    //                           .Count();
                    //            #endregion
                    //        }
                    //        #region Data
                    //        _Customers = _HCoreContext.HCUAccount
                    //                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                    && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                    //                                   .Select(x => new OAccounts.Customer.List
                    //                                   {
                    //                                       ReferenceId = x.Id,
                    //                                       ReferenceKey = x.Guid,
                    //                                       DisplayName = x.DisplayName,
                    //                                       Name = x.Name,
                    //                                       FirstName = x.FirstName,
                    //                                       LastName = x.LastName,
                    //                                       EmailAddress = x.EmailAddress,
                    //                                       MobileNumber = x.MobileNumber,
                    //                                       LastTransactionDate = x.LastTransactionDate,
                    //                                       CreateDate = x.CreateDate,
                    //                                       StatusId = x.StatusId,
                    //                                   })
                    //                                .Where(_Request.SearchCondition)
                    //                                .OrderBy(_Request.SortExpression)
                    //                                .Skip(_Request.Offset)
                    //                                .Take(_Request.Limit)
                    //                                .ToList();
                    //        #endregion
                    //        #region Create  Response Object
                    //        _HCoreContext.Dispose();
                    //        foreach (var DataItem in _Customers)
                    //        {
                    //            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //            {
                    //                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //            }
                    //            else
                    //            {
                    //                DataItem.IconUrl = _AppConfig.Default_Icon;
                    //            }
                    //        }
                    //        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                    //        #endregion

                    //    }
                    //    else if (_Request.Type == "loyal")
                    //    {
                    //        if (_Request.RefreshCount)
                    //        {
                    //            #region Total Records
                    //            _Request.TotalRecords = _HCoreContext.HCUAccount
                    //                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                    && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                    //                                    .Select(x => new OAccounts.Customer.List
                    //                                    {
                    //                                        ReferenceId = x.Id,
                    //                                        ReferenceKey = x.Guid,
                    //                                        DisplayName = x.DisplayName,
                    //                                        Name = x.Name,
                    //                                        FirstName = x.FirstName,
                    //                                        LastName = x.LastName,
                    //                                        EmailAddress = x.EmailAddress,
                    //                                        MobileNumber = x.MobileNumber,
                    //                                        LastTransactionDate = x.LastTransactionDate,
                    //                                        CreateDate = x.CreateDate,
                    //                                        StatusId = x.StatusId,
                    //                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                    //                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                    //                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                    //                                        TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.ReferenceId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                    //                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                    //                                    })
                    //                                    .Where(_Request.SearchCondition)
                    //                           .Count();
                    //            #endregion
                    //        }
                    //        #region Data
                    //        _Customers = _HCoreContext.HCUAccount
                    //                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                    && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                    //                                   .Select(x => new OAccounts.Customer.List
                    //                                   {
                    //                                       ReferenceId = x.Id,
                    //                                       ReferenceKey = x.Guid,
                    //                                       DisplayName = x.DisplayName,
                    //                                       Name = x.Name,
                    //                                       FirstName = x.FirstName,
                    //                                       LastName = x.LastName,
                    //                                       EmailAddress = x.EmailAddress,
                    //                                       MobileNumber = x.MobileNumber,
                    //                                       LastTransactionDate = x.LastTransactionDate,
                    //                                       CreateDate = x.CreateDate,
                    //                                       StatusId = x.StatusId,
                    //                                   })
                    //                                .Where(_Request.SearchCondition)
                    //                                .OrderBy(_Request.SortExpression)
                    //                                .Skip(_Request.Offset)
                    //                                .Take(_Request.Limit)
                    //                                .ToList();
                    //        #endregion
                    //        #region Create  Response Object
                    //        _HCoreContext.Dispose();
                    //        foreach (var DataItem in _Customers)
                    //        {
                    //            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //            {
                    //                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //            }
                    //            else
                    //            {
                    //                DataItem.IconUrl = _AppConfig.Default_Icon;
                    //            }
                    //        }
                    //        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                    //        #endregion

                    //    }
                    //    else if (_Request.Type == "lost")
                    //    {
                    //        if (_Request.RefreshCount)
                    //        {
                    //            #region Total Records
                    //            _Request.TotalRecords = _HCoreContext.HCUAccount
                    //                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                     && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate < _Request.StartDate)
                    //                                     && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                    //                                    .Select(x => new OAccounts.Customer.List
                    //                                    {
                    //                                        ReferenceId = x.Id,
                    //                                        ReferenceKey = x.Guid,
                    //                                        DisplayName = x.DisplayName,
                    //                                        Name = x.Name,
                    //                                        FirstName = x.FirstName,
                    //                                        LastName = x.LastName,
                    //                                        EmailAddress = x.EmailAddress,
                    //                                        MobileNumber = x.MobileNumber,
                    //                                        LastTransactionDate = x.LastTransactionDate,
                    //                                        CreateDate = x.CreateDate,
                    //                                        StatusId = x.StatusId,
                    //                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                    //                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                    //                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                    //                                        TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.ReferenceId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                    //                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                    //                                    })
                    //                                    .Where(_Request.SearchCondition)
                    //                           .Count();
                    //            #endregion
                    //        }
                    //        #region Data
                    //        _Customers = _HCoreContext.HCUAccount
                    //                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                     && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate < _Request.StartDate)
                    //                                     && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                    //                                    .Select(x => new OAccounts.Customer.List
                    //                                    {
                    //                                        ReferenceId = x.Id,
                    //                                        ReferenceKey = x.Guid,
                    //                                        DisplayName = x.DisplayName,
                    //                                        Name = x.Name,
                    //                                        FirstName = x.FirstName,
                    //                                        LastName = x.LastName,
                    //                                        EmailAddress = x.EmailAddress,
                    //                                        MobileNumber = x.MobileNumber,
                    //                                        LastTransactionDate = x.LastTransactionDate,
                    //                                        CreateDate = x.CreateDate,
                    //                                        StatusId = x.StatusId,
                    //                                    })
                    //                                .Where(_Request.SearchCondition)
                    //                                .OrderBy(_Request.SortExpression)
                    //                                .Skip(_Request.Offset)
                    //                                .Take(_Request.Limit)
                    //                                .ToList();
                    //        #endregion
                    //        #region Create  Response Object
                    //        _HCoreContext.Dispose();
                    //        foreach (var DataItem in _Customers)
                    //        {
                    //            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //            {
                    //                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //            }
                    //            else
                    //            {
                    //                DataItem.IconUrl = _AppConfig.Default_Icon;
                    //            }
                    //        }
                    //        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                    //        #endregion

                    //    }
                    //    else
                    //    {
                    //        if (_Request.RefreshCount)
                    //        {
                    //            #region Total Records
                    //            _Request.TotalRecords = _HCoreContext.HCUAccount
                    //                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                    && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                    //                                    .Select(x => new OAccounts.Customer.List
                    //                                    {
                    //                                        ReferenceId = x.Id,
                    //                                        ReferenceKey = x.Guid,
                    //                                        DisplayName = x.DisplayName,
                    //                                        Name = x.Name,
                    //                                        FirstName = x.FirstName,
                    //                                        LastName = x.LastName,
                    //                                        EmailAddress = x.EmailAddress,
                    //                                        MobileNumber = x.MobileNumber,
                    //                                        // LastTransactionDate = x.LastTransactionDate,
                    //                                        //TLastTransactionDate = x.HCUAccountTransactionAccount.Where(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).Select(d => d.TransactionDate).FirstOrDefault(),
                    //                                        CreateDate = x.CreateDate,
                    //                                        StatusId = x.StatusId,
                    //                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                    //                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                    //                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                    //                                        TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.ReferenceId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                    //                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                    //                                    })
                    //                                    .Where(_Request.SearchCondition)
                    //                           .Count();
                    //            #endregion
                    //        }
                    //        #region Data
                    //        _Customers = _HCoreContext.HCUAccount
                    //                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                 && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                    //                                   .Select(x => new OAccounts.Customer.List
                    //                                   {
                    //                                       ReferenceId = x.Id,
                    //                                       ReferenceKey = x.Guid,
                    //                                       DisplayName = x.DisplayName,
                    //                                       Name = x.Name,
                    //                                       FirstName = x.FirstName,
                    //                                       LastName = x.LastName,
                    //                                       EmailAddress = x.EmailAddress,
                    //                                       MobileNumber = x.MobileNumber,
                    //                                       LastTransactionDate = x.LastTransactionDate,
                    //                                       //TLastTransactionDate = x.HCUAccountTransactionAccount.Where(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).Select(d => d.TransactionDate).FirstOrDefault(),
                    //                                       CreateDate = x.CreateDate,
                    //                                       StatusId = x.StatusId,
                    //                                       TotalTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                    //                                       TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                    //                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                    //                                       TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.ReferenceId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                    //                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),

                    //                                   })
                    //                                .Where(_Request.SearchCondition)
                    //                                .OrderBy(_Request.SortExpression)
                    //                                .Skip(_Request.Offset)
                    //                                .Take(_Request.Limit)
                    //                                .ToList();
                    //        #endregion
                    //        #region Create  Response Object
                    //        _HCoreContext.Dispose();
                    //        foreach (var DataItem in _Customers)
                    //        {
                    //            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //            {
                    //                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //            }
                    //            else
                    //            {
                    //                DataItem.IconUrl = _AppConfig.Default_Icon;
                    //            }
                    //        }
                    //        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                    //        #endregion

                    //    }

                    //}
                    //else
                    //{
                    //    if (_Request.Type == "new")
                    //    {
                    //        if (_Request.RefreshCount)
                    //        {
                    //            #region Total Records
                    //            _Request.TotalRecords = _HCoreContext.HCUAccount
                    //                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                    && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                    //                                    .Select(x => new OAccounts.Customer.List
                    //                                    {
                    //                                        ReferenceId = x.Id,
                    //                                        ReferenceKey = x.Guid,
                    //                                        DisplayName = x.DisplayName,
                    //                                        Name = x.Name,
                    //                                        FirstName = x.FirstName,
                    //                                        LastName = x.LastName,
                    //                                        EmailAddress = x.EmailAddress,
                    //                                        MobileNumber = x.MobileNumber,
                    //                                        LastTransactionDate = x.LastTransactionDate,
                    //                                        CreateDate = x.CreateDate,
                    //                                        StatusId = x.StatusId,
                    //                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                    //                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                    //                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                    //                                        TucRewardAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                    //                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                    //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                    //                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                    //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                    //                                    })
                    //                                    .Where(_Request.SearchCondition)
                    //                           .Count();
                    //            #endregion
                    //        }
                    //        #region Data
                    //        _Customers = _HCoreContext.HCUAccount
                    //                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                    && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                    //                                   .Select(x => new OAccounts.Customer.List
                    //                                   {
                    //                                       ReferenceId = x.Id,
                    //                                       ReferenceKey = x.Guid,
                    //                                       DisplayName = x.DisplayName,
                    //                                       Name = x.Name,
                    //                                       FirstName = x.FirstName,
                    //                                       LastName = x.LastName,
                    //                                       EmailAddress = x.EmailAddress,
                    //                                       MobileNumber = x.MobileNumber,
                    //                                       LastTransactionDate = x.LastTransactionDate,
                    //                                       CreateDate = x.CreateDate,
                    //                                       StatusId = x.StatusId,
                    //                                       TotalTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                    //                                       TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                    //                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                    //                                       TucRewardAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                    //                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                    //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                    //                                       RedeemAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                    //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                    //                                   })
                    //                                .Where(_Request.SearchCondition)
                    //                                .OrderBy(_Request.SortExpression)
                    //                                .Skip(_Request.Offset)
                    //                                .Take(_Request.Limit)
                    //                                .ToList();
                    //        #endregion
                    //        #region Create  Response Object
                    //        _HCoreContext.Dispose();
                    //        //foreach (var DataItem in _Customers)
                    //        //{
                    //        //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //        //    {
                    //        //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //        //    }
                    //        //    else
                    //        //    {
                    //        //        DataItem.IconUrl = _AppConfig.Default_Icon;
                    //        //    }
                    //        //}
                    //        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                    //        #endregion

                    //    }
                    //    else if (_Request.Type == "loyal")
                    //    {
                    //        if (_Request.RefreshCount)
                    //        {
                    //            #region Total Records
                    //            _Request.TotalRecords = _HCoreContext.HCUAccount
                    //                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                    && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                    //                                    .Select(x => new OAccounts.Customer.List
                    //                                    {
                    //                                        ReferenceId = x.Id,
                    //                                        ReferenceKey = x.Guid,
                    //                                        DisplayName = x.DisplayName,
                    //                                        Name = x.Name,
                    //                                        FirstName = x.FirstName,
                    //                                        LastName = x.LastName,
                    //                                        EmailAddress = x.EmailAddress,
                    //                                        MobileNumber = x.MobileNumber,
                    //                                        LastTransactionDate = x.LastTransactionDate,
                    //                                        CreateDate = x.CreateDate,
                    //                                        StatusId = x.StatusId,
                    //                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                    //                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                    //                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                    //                                        TucRewardAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                    //                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                    //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                    //                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                    //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                    //                                    })
                    //                                    .Where(_Request.SearchCondition)
                    //                           .Count();
                    //            #endregion
                    //        }
                    //        #region Data
                    //        _Customers = _HCoreContext.HCUAccount
                    //                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                    && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                    //                                   .Select(x => new OAccounts.Customer.List
                    //                                   {
                    //                                       ReferenceId = x.Id,
                    //                                       ReferenceKey = x.Guid,
                    //                                       DisplayName = x.DisplayName,
                    //                                       Name = x.Name,
                    //                                       FirstName = x.FirstName,
                    //                                       LastName = x.LastName,
                    //                                       EmailAddress = x.EmailAddress,
                    //                                       MobileNumber = x.MobileNumber,
                    //                                       LastTransactionDate = x.LastTransactionDate,
                    //                                       CreateDate = x.CreateDate,
                    //                                       StatusId = x.StatusId,
                    //                                       TotalTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                    //                                       TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                    //                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                    //                                       TucRewardAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                    //                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                    //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                    //                                       RedeemAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                    //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                    //                                   })
                    //                                .Where(_Request.SearchCondition)
                    //                                .OrderBy(_Request.SortExpression)
                    //                                .Skip(_Request.Offset)
                    //                                .Take(_Request.Limit)
                    //                                .ToList();
                    //        #endregion
                    //        #region Create  Response Object
                    //        _HCoreContext.Dispose();
                    //        //foreach (var DataItem in _Customers)
                    //        //{
                    //        //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //        //    {
                    //        //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //        //    }
                    //        //    else
                    //        //    {
                    //        //        DataItem.IconUrl = _AppConfig.Default_Icon;
                    //        //    }
                    //        //}
                    //        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                    //        #endregion

                    //    }
                    //    else if (_Request.Type == "lost")
                    //    {
                    //        if (_Request.RefreshCount)
                    //        {
                    //            #region Total Records
                    //            _Request.TotalRecords = _HCoreContext.HCUAccount
                    //                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                    && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate < _Request.StartDate)
                    //                                     && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)

                    //                                     .Select(x => new OAccounts.Customer.List
                    //                                     {
                    //                                         ReferenceId = x.Id,
                    //                                         ReferenceKey = x.Guid,
                    //                                         DisplayName = x.DisplayName,
                    //                                         Name = x.Name,
                    //                                         FirstName = x.FirstName,
                    //                                         LastName = x.LastName,
                    //                                         EmailAddress = x.EmailAddress,
                    //                                         MobileNumber = x.MobileNumber,
                    //                                         LastTransactionDate = x.LastTransactionDate,
                    //                                         CreateDate = x.CreateDate,
                    //                                         StatusId = x.StatusId,
                    //                                         TotalTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                    //                                         TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                    //                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                    //                                         TucRewardAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                    //                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                    //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                    //                                         RedeemAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                    //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                    //                                     })
                    //                                    .Where(_Request.SearchCondition)
                    //                           .Count();
                    //            #endregion
                    //        }
                    //        #region Data
                    //        _Customers = _HCoreContext.HCUAccount
                    //                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate < _Request.StartDate)
                    //                                && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                    //                                    .Select(x => new OAccounts.Customer.List
                    //                                    {
                    //                                        ReferenceId = x.Id,
                    //                                        ReferenceKey = x.Guid,
                    //                                        DisplayName = x.DisplayName,
                    //                                        Name = x.Name,
                    //                                        FirstName = x.FirstName,
                    //                                        LastName = x.LastName,
                    //                                        EmailAddress = x.EmailAddress,
                    //                                        MobileNumber = x.MobileNumber,
                    //                                        LastTransactionDate = x.LastTransactionDate,
                    //                                        CreateDate = x.CreateDate,
                    //                                        StatusId = x.StatusId,
                    //                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                    //                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                    //                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                    //                                        TucRewardAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                    //                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                    //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                    //                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                    //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                    //                                    })
                    //                                .Where(_Request.SearchCondition)
                    //                                .OrderBy(_Request.SortExpression)
                    //                                .Skip(_Request.Offset)
                    //                                .Take(_Request.Limit)
                    //                                .ToList();
                    //        #endregion
                    //        #region Create  Response Object
                    //        _HCoreContext.Dispose();
                    //        foreach (var DataItem in _Customers)
                    //        {
                    //            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //            {
                    //                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //            }
                    //            else
                    //            {
                    //                DataItem.IconUrl = _AppConfig.Default_Icon;
                    //            }
                    //        }
                    //        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                    //        #endregion

                    //    }
                    //    else
                    //    {
                    //        if (_Request.RefreshCount)
                    //        {
                    //            #region Total Records
                    //            _Request.TotalRecords = _HCoreContext.HCUAccount
                    //                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                    && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                    //                                    .Select(x => new OAccounts.Customer.List
                    //                                    {
                    //                                        ReferenceId = x.Id,
                    //                                        ReferenceKey = x.Guid,
                    //                                        DisplayName = x.DisplayName,
                    //                                        Name = x.Name,
                    //                                        FirstName = x.FirstName,
                    //                                        LastName = x.LastName,
                    //                                        EmailAddress = x.EmailAddress,
                    //                                        MobileNumber = x.MobileNumber,
                    //                                        //LastTransactionDate = x.LastTransactionDate,
                    //                                        //TLastTransactionDate = x.HCUAccountTransactionAccount.Where(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).OrderByDescending(d => d.TransactionDate).Select(d => d.TransactionDate).FirstOrDefault(),
                    //                                        CreateDate = x.CreateDate,
                    //                                        StatusId = x.StatusId,
                    //                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                    //                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                    //                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                    //                                        TucRewardAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                    //                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                    //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                    //                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                    //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                    //                                    })
                    //                                    .Where(_Request.SearchCondition)
                    //                           .Count();
                    //            #endregion
                    //        }
                    //        #region Data
                    //        _Customers = _HCoreContext.HCUAccount
                    //                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                 && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                    //                                   .Select(x => new OAccounts.Customer.List
                    //                                   {
                    //                                       ReferenceId = x.Id,
                    //                                       ReferenceKey = x.Guid,
                    //                                       DisplayName = x.DisplayName,
                    //                                       Name = x.Name,
                    //                                       FirstName = x.FirstName,
                    //                                       LastName = x.LastName,
                    //                                       EmailAddress = x.EmailAddress,
                    //                                       MobileNumber = x.MobileNumber,
                    //                                       //TLastTransactionDate = x.HCUAccountTransactionAccount.Where(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).Select(d => d.TransactionDate).FirstOrDefault(),
                    //                                       CreateDate = x.CreateDate,
                    //                                       StatusId = x.StatusId,
                    //                                       //TLastTransactionDate = x.HCUAccountTransactionAccount.Where(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).OrderByDescending(d => d.TransactionDate).Select(d => d.TransactionDate).FirstOrDefault(),
                    //                                       TotalTransaction = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Count(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                    //                                       TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                    //                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                    //                                       TucRewardAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                    //                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                    //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                    //                                       RedeemAmount = _HCoreContext.HCUAccountTransaction
                    //                                                                        .Where(m => m.AccountId == x.Id
                    //                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                    //                                                                        && m.ParentId == _Request.AccountId
                    //                                                                        && m.StatusId == HelperStatus.Transaction.Success
                    //                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                    //                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                    //                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                    //                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                    //                                   })
                    //                                .Where(_Request.SearchCondition)
                    //                                .OrderBy(_Request.SortExpression)
                    //                                .Skip(_Request.Offset)
                    //                                .Take(_Request.Limit)
                    //                                .ToList();
                    //        #endregion
                    //        #region Create  Response Object
                    //        _HCoreContext.Dispose();
                    //        //foreach (var DataItem in _Customers)
                    //        //{
                    //        //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //        //    {
                    //        //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //        //    }
                    //        //    else
                    //        //    {
                    //        //        DataItem.IconUrl = _AppConfig.Default_Icon;
                    //        //    }
                    //        //}
                    //        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                    //        #endregion

                    //    }

                    //}
                    //if (_Customers.Count > 0)
                    //{
                    //    using (var _XLWorkbook = new ClosedXML.Excel.XLWorkbook())
                    //    {
                    //        var _WorkSheet = _XLWorkbook.Worksheets.Add("Rewards_Sheet");
                    //        PropertyInfo[] properties = _Customers.First().GetType().GetProperties();
                    //        List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                    //        for (int i = 0; i < headerNames.Count; i++)
                    //        {
                    //            _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                    //        }
                    //        _WorkSheet.Cell(2, 1).InsertData(_Customers);
                    //        MemoryStream _MemoryStream = new MemoryStream();
                    //        _XLWorkbook.SaveAs(_MemoryStream);
                    //        long? _StorageId = HCoreHelper.SaveStorage("Customers_Sheet", "xlsx", _MemoryStream, _Request.UserReference);
                    //        if (_StorageId != null && _StorageId != 0)
                    //        {
                    //            using (_HCoreContext = new HCoreContext())
                    //            {
                    //                var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                    //                if (TItem != null)
                    //                {
                    //                    TItem.IconStorageId = _StorageId;
                    //                    TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                    //                    TItem.ModifyById = _Request.UserReference.AccountId;
                    //                    TItem.StatusId = HelperStatus.Default.Active;
                    //                    _HCoreContext.SaveChanges();
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCustomer", _Exception, _Request.UserReference);
            }
            #endregion
        }
        internal OResponse GetCustomer(OAccounts.Customer.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.AccountId &&
                        x.Guid == _Request.AccountKey &&
                        x.StatusId == HelperStatus.Default.Active)
                        .Select(x => new OAccounts.Customer.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            Name = x.Name,
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            DisplayName = x.DisplayName,
                            EmailAddress = x.EmailAddress,
                            MobileNumber = x.MobileNumber,
                            DateOfBirth = x.DateOfBirth,
                            GenderName = x.Gender.Name,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                            IconUrl = x.IconStorage.Path,
                            WakaPointId = x.AccountCode
                        }).FirstOrDefault();
                    if (Details != null)
                    {
                        if (_HCoreContext.HCUAccountTransaction.Any(x => x.AccountId == _Request.AccountId && x.ParentId == _Request.MerchantId))
                        {
                            DateTime? FirstTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == _Request.AccountId && x.ParentId == _Request.MerchantId).Select(x => x.TransactionDate).FirstOrDefault();
                            if (FirstTransactionDate != null)
                            {
                                Details.FirstTransactionDate = FirstTransactionDate;
                            }
                            DateTime? LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == _Request.AccountId && x.ParentId == _Request.MerchantId).OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();
                            if (LastTransactionDate != null)
                            {
                                Details.LastTransactionDate = FirstTransactionDate;
                            }
                        }
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            Details.IconUrl = _AppConfig.Default_Icon;
                        }

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomer", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Delete Customer: This method updates the customer
        /// statusId from active to inactive
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteCustomer(OAccounts.Customer.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var customer = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.AccountKey && x.Id == _Request.AccountId).FirstOrDefault();
                    if (customer != null)
                    {
                        var customerStatus = _HCoreContext.cmt_loyalty_customer.Where(x => x.customer_id == customer.Id && x.owner_id == _Request.MerchantId).FirstOrDefault();
                        if (customerStatus != null)
                        {
                            if (customerStatus.status_id == HelperStatus.Default.Active)
                            {
                                customerStatus.status_id = HelperStatus.Default.Blocked;
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA0200, TUCCoreResource.CD0007);
                            }
                            else
                            {
                                customerStatus.status_id = HelperStatus.Default.Active;
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA0200, TUCCoreResource.CD0008);
                            }

                        }
                        else
                        {
                            var TUCGold = HCoreHelper.GetConfigurationDetails("thankucashgold", _Request.MerchantId);
                            // Save cmt_loyalty_customer
                            cmt_loyalty_customer _cmt_loyalty_customer = new cmt_loyalty_customer();
                            _cmt_loyalty_customer.guid = HCoreHelper.GenerateGuid();
                            _cmt_loyalty_customer.customer_id = customer.Id;
                            _cmt_loyalty_customer.owner_id = _Request.MerchantId;
                            if (TUCGold != null && TUCGold.Value == "1")
                            {
                                _cmt_loyalty_customer.source_id = TransactionSource.TUCBlack;
                            }
                            else
                            {
                                _cmt_loyalty_customer.source_id = TransactionSource.TUC;
                            }
                            _cmt_loyalty_customer.credit = 0;
                            _cmt_loyalty_customer.debit = 0;
                            _cmt_loyalty_customer.create_date = HCoreHelper.GetGMTDateTime();
                            _cmt_loyalty_customer.status_id = HelperStatus.Default.Blocked;
                            _HCoreContext.cmt_loyalty_customer.Add(_cmt_loyalty_customer);
                            _HCoreContext.SaveChanges();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA0200, TUCCoreResource.CD0007);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("DeleteCustomer", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
    }

    internal class ActorGetCustomerDownloadN : ReceiveActor
    {
        public ActorGetCustomerDownloadN()
        {
            FrameworkCustomer _FrameworkCustomer;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkCustomer = new FrameworkCustomer();
                _FrameworkCustomer.GetCustomerDownload(_Request);
            });
        }
    }
}
