//==================================================================================
// FileName: FrameworkOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Resource;

namespace HCore.TUC.Core.Framework.Merchant
{
    internal class FrameworkOperations
    {
        HCoreContext _HCoreContext;
        HCUAccountParameter _HCUAccountParameter;

        /// <summary>
        /// Description: Saves the configurations.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveConfigurations(OOperations.Configuration.BulkUpdate _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.Items == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1361, TUCCoreResource.CA1361M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var UserAccountDetails = _HCoreContext.HCUAccount
                                .Where(x => x.Guid == _Request.AccountKey && x.Id == _Request.AccountId)
                                .Select(x => new
                                {
                                    Id = x.Id,
                                    StatusId = x.StatusId,
                                }).FirstOrDefault();
                    if (UserAccountDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1072, TUCCoreResource.CA1072M);
                    }
                    #region Operation
                    foreach (var _RequestItem in _Request.Items)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x =>
                            x.SystemName == _RequestItem.ConfigurationKey
                            && x.StatusId == HelperStatus.Default.Active
                            && x.TypeId == HelperType.Configuration
                            ).Select(x => new
                            {
                                SystemName = x.SystemName,
                                HelperId = x.HelperId,
                                Value = x.Value,
                                ConfigurationId = x.Id,
                            }).FirstOrDefault();
                            if (ConfigurationDetails != null)
                            {
                                if (UserAccountDetails.StatusId != HelperStatus.Default.Blocked)
                                {
                                    var UserConfigurations = _HCoreContext.HCUAccountParameter.Where(x =>
                                                             x.TypeId == HelperType.ConfigurationValue
                                                             && x.StatusId == HelperStatus.Default.Active
                                                             && x.AccountId == UserAccountDetails.Id
                                                             && x.CommonId == ConfigurationDetails.ConfigurationId
                                                            ).ToList();
                                    if (UserConfigurations.Count > 0)
                                    {
                                        foreach (var UserConfiguration in UserConfigurations)
                                        {
                                            UserConfiguration.StatusId = HCoreConstant.HelperStatus.Default.Inactive;
                                            UserConfiguration.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            UserConfiguration.ModifyById = _Request.UserReference.AccountId;
                                            _HCoreContext.SaveChanges();
                                        }
                                    }
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        if (ConfigurationDetails.SystemName == "rewardpercentage")
                                        {
                                            var Account = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId).FirstOrDefault();
                                            if (Account != null)
                                            {
                                                Account.AccountPercentage = Convert.ToDouble(_RequestItem.Value);
                                            }
                                        }
                                        int HelperId = _HCoreContext.HCCore.Where(x => x.SystemName == _RequestItem.HelperCode).Select(x => x.Id).FirstOrDefault();
                                        _HCUAccountParameter = new HCUAccountParameter();
                                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                        _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                        _HCUAccountParameter.AccountId = UserAccountDetails.Id;
                                        _HCUAccountParameter.CommonId = ConfigurationDetails.ConfigurationId;
                                        _HCUAccountParameter.Value = _RequestItem.Value;
                                        if (HelperId != 0)
                                        {
                                            _HCUAccountParameter.HelperId = HelperId;
                                        }
                                        _HCUAccountParameter.Description = _RequestItem.Comment;
                                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.ModifyById = _Request.UserReference.AccountId;
                                        _HCUAccountParameter.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                        _HCoreContext.SaveChanges();
                                        //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC001");
                                    }
                                }
                                //else
                                //{
                                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1074, TUCCoreResource.CA1074M);
                                //}
                            }
                            //else
                            //{
                            //    #region Send Response
                            //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1073, TUCCoreResource.CA1073M);
                            //    #endregion
                            //}
                        }

                    }
                    #endregion
                }
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1362, TUCCoreResource.CA1362M);
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetMerchantImportList", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveConfiguration(OOperations.Configuration.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Operation
                bool isNum = double.TryParse(_Request.Value, out double num);
                if (!isNum)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1537, TUCCoreResource.CA1537M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var UserAccountDetails = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey && x.Id == _Request.AccountId)
                        .Select(x => new
                        {
                            Id = x.Id,
                            StatusId = x.StatusId,
                        }).FirstOrDefault();
                    if (UserAccountDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1072, TUCCoreResource.CA1072M);
                    }

                    var ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x =>
                    x.SystemName == _Request.ConfigurationKey
                    && x.StatusId == HelperStatus.Default.Active
                    && x.TypeId == HelperType.Configuration
                    ).Select(x => new
                    {
                        SystemName = x.SystemName,
                        HelperId = x.HelperId,
                        Value = x.Value,
                        ConfigurationId = x.Id,
                    }).FirstOrDefault();
                    if (ConfigurationDetails != null)
                    {
                        if (UserAccountDetails.StatusId != HelperStatus.Default.Blocked)
                        {
                            var UserConfigurations = _HCoreContext.HCUAccountParameter.Where(x =>
                                                     x.TypeId == HelperType.ConfigurationValue
                                                     && x.StatusId == HelperStatus.Default.Active
                                                     && x.AccountId == UserAccountDetails.Id
                                                     && x.CommonId == ConfigurationDetails.ConfigurationId
                                                    ).ToList();
                            if (UserConfigurations.Count > 0)
                            {
                                foreach (var UserConfiguration in UserConfigurations)
                                {
                                    UserConfiguration.StatusId = HCoreConstant.HelperStatus.Default.Inactive;
                                    UserConfiguration.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    UserConfiguration.ModifyById = _Request.UserReference.AccountId;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                            using (_HCoreContext = new HCoreContext())
                            {
                                if (ConfigurationDetails.SystemName == "rewardpercentage")
                                {
                                    var Account = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId).FirstOrDefault();
                                    if (Account != null)
                                    {
                                        Account.AccountPercentage = Convert.ToDouble(_Request.Value);
                                    }
                                }
                                int HelperId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.HelperCode).Select(x => x.Id).FirstOrDefault();
                                _HCUAccountParameter = new HCUAccountParameter();
                                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                _HCUAccountParameter.AccountId = UserAccountDetails.Id;
                                _HCUAccountParameter.CommonId = ConfigurationDetails.ConfigurationId;
                                _HCUAccountParameter.Value = _Request.Value;
                                if (HelperId != 0)
                                {
                                    _HCUAccountParameter.HelperId = HelperId;
                                }
                                _HCUAccountParameter.Description = _Request.Comment;
                                _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountParameter.ModifyById = _Request.UserReference.AccountId;
                                _HCUAccountParameter.ModifyDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC001");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1074, TUCCoreResource.CA1074M);
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1073, TUCCoreResource.CA1073M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetMerchantImportList", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the configuration details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetConfiguration(OOperations.Configuration.Config _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var UserAccountDetails = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey && x.Id == _Request.AccountId)
                        .Select(x => new
                        {
                            Id = x.Id,
                            StatusId = x.StatusId,
                        }).FirstOrDefault();
                    if (UserAccountDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1072, TUCCoreResource.CA1072M);
                    }
                    var ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x =>
                    x.SystemName == _Request.ConfigurationKey
                    && x.StatusId == HelperStatus.Default.Active
                    && x.TypeId == HelperType.Configuration
                    ).Select(x => new
                    {
                        SystemName = x.SystemName,
                        HelperId = x.HelperId,
                        Value = x.Value,
                        ConfigurationId = x.Id,
                    }).FirstOrDefault();
                    if (ConfigurationDetails != null)
                    {
                        string Value = "";
                        var UserConfiguration = _HCoreContext.HCUAccountParameter.Where(x =>
                                                     x.TypeId == HelperType.ConfigurationValue
                                                     && x.StatusId == HelperStatus.Default.Active
                                                     && x.AccountId == UserAccountDetails.Id
                                                     && x.CommonId == ConfigurationDetails.ConfigurationId
                                                    ).OrderByDescending(x => x.CreateDate)
                                                    .Select(x => new
                                                    {
                                                        Value = x.Value,
                                                    }).FirstOrDefault();
                        if (UserConfiguration != null)
                        {
                            Value = UserConfiguration.Value;
                        }
                        Value = Convert.ToString(_HCoreContext.HCUAccountParameter.Where(y => y.AccountId == _Request.AccountId
                                                              && y.Common.SystemName == "rewardpercentage").OrderByDescending(x => x.CreateDate)
                                                        .Select(y => y.Value).FirstOrDefault());
                        if (!string.IsNullOrEmpty(Value))
                        {
                            var ConfigDetails = new
                            {
                                Value
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ConfigDetails, "HC001", "Configuration loaded");
                        }
                        else
                        {
                            var ConfigDetails = new
                            {
                                ConfigurationDetails.Value
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ConfigDetails, "HC001", "Configuration loaded");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1073, TUCCoreResource.CA1073M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetConfiguration", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
        }
        /// <summary>
        /// Description: Gets the loyalty configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLoyaltyConfiguration(OOperations.Configuration.Config _Request)
        {
            try
            {


                using (_HCoreContext = new HCoreContext())
                {
                    var UserAccountDetails = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey && x.Id == _Request.AccountId)
                        .Select(x => new
                        {
                            Id = x.Id,
                            StatusId = x.StatusId,
                            MerchantCategoryId = x.PrimaryCategoryId,
                        }).FirstOrDefault();
                    if (UserAccountDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1072, TUCCoreResource.CA1072M);
                    }
                    var _LoyaltyResponse = new OOperations.Configuration.LoyaltyConfiguration
                    {
                        IsTucPlus = false,
                        IsTucGold = false,
                        CategoryName = "",
                        Reward = new OOperations.Configuration.Reward
                        {
                            IsCustomReward = false,
                            TucCommissionSource = "Invoice Amount",
                            RewardPercentage = 0,
                            UserPercentage = 0,
                            SystemPercentage = 0,
                            MerchantPercentage = 0,
                            PaymentMethod = "Prepay"

                        },
                        Redeem = new OOperations.Configuration.Redeem
                        {
                            TucCommissionSource = "Redeem Amount",
                            UserPercentage = 0,
                            SystemPercentage = 0,
                            MerchantPercentage = 0,
                        }
                    };


                    var TUCGold = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashgold", UserAccountDetails.Id));
                    if (TUCGold != 0)
                    {
                        _LoyaltyResponse.IsTucGold = true;
                    }
                    var TUCPlus = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", UserAccountDetails.Id));
                    if (TUCGold != 0)
                    {
                        _LoyaltyResponse.IsTucPlus = true;
                    }
                    //   _LoyaltyResponse.Reward.RewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", UserAccountDetails.Id)), _AppConfig.SystemRoundPercentage);
                    _LoyaltyResponse.Reward.RewardPercentage = Convert.ToDouble(_HCoreContext.HCUAccountParameter.Where(y => y.AccountId == UserAccountDetails.Id
                                                                && y.Common.SystemName == "rewardpercentage").OrderByDescending(x => x.CreateDate)
                                                           .Select(y => y.Value).FirstOrDefault());
                    int TAllowCustomReward = Convert.ToInt32(HCoreHelper.GetConfiguration("customreward", UserAccountDetails.Id));
                    if (TAllowCustomReward > 0)
                    {
                        _LoyaltyResponse.Reward.IsCustomReward = true;
                    }
                    else
                    {
                        _LoyaltyResponse.Reward.IsCustomReward = false;
                    }

                    var RewardDeductionType = HCoreHelper.GetConfigurationDetails("rewarddeductiontype", UserAccountDetails.Id);
                    _LoyaltyResponse.Reward.PaymentMethod = RewardDeductionType.Name;
                    int ThankUCashPercentageFromRewardAmount = Convert.ToInt32(HCoreHelper.GetConfiguration("rewardcommissionfromrewardamount", UserAccountDetails.Id));
                    if (ThankUCashPercentageFromRewardAmount == 1)
                    {
                        _LoyaltyResponse.Reward.TucCommissionSource = "Reward Amount";
                        _LoyaltyResponse.Reward.UserPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("userrewardpercentage", UserAccountDetails.Id)), _AppConfig.SystemRoundPercentage);
                        _LoyaltyResponse.Reward.SystemPercentage = 100 - _LoyaltyResponse.Reward.UserPercentage;
                        _LoyaltyResponse.Reward.MerchantPercentage = _LoyaltyResponse.Reward.RewardPercentage;
                    }
                    else
                    {
                        _LoyaltyResponse.Reward.TucCommissionSource = "Invoice Amount";
                        double SystemDefaultCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardcomissionpercentage")), _AppConfig.SystemRoundPercentage);
                        double SystemMerchantCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetAccountConfiguration("rewardcomissionpercentage", UserAccountDetails.Id)), _AppConfig.SystemRoundPercentage);
                        double SystemCategoryCommissionPercentage = 0;
                        double TUCRewardCommissionAmountPercentage = 0;
                        if (UserAccountDetails.MerchantCategoryId > 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var CategoryCommission = _HCoreContext.TUCMerchantCategory.Where(x => x.RootCategoryId == UserAccountDetails.MerchantCategoryId)
                                    .Select(x => new
                                    {
                                        Commission = x.Commission,
                                        Name = x.RootCategory.Name,
                                    }).FirstOrDefault();
                                _HCoreContext.Dispose();
                                if (CategoryCommission != null && CategoryCommission.Commission > 0)
                                {
                                    SystemCategoryCommissionPercentage = (double)CategoryCommission.Commission;
                                    _LoyaltyResponse.CategoryName = CategoryCommission.Name;
                                }
                            }
                        }
                        if (SystemMerchantCommissionPercentage > 0)
                        {
                            TUCRewardCommissionAmountPercentage = SystemMerchantCommissionPercentage;
                        }
                        else if (SystemCategoryCommissionPercentage > 0)
                        {
                            TUCRewardCommissionAmountPercentage = SystemCategoryCommissionPercentage;
                        }
                        else
                        {
                            TUCRewardCommissionAmountPercentage = SystemDefaultCommissionPercentage;
                        }
                        if (_LoyaltyResponse.IsTucGold == true)
                        {
                            _LoyaltyResponse.Reward.SystemPercentage = 0;
                        }
                        else
                        {
                            _LoyaltyResponse.Reward.SystemPercentage = TUCRewardCommissionAmountPercentage;
                        }
                        _LoyaltyResponse.Reward.UserPercentage = 100;
                        _LoyaltyResponse.Reward.MerchantPercentage = _LoyaltyResponse.Reward.SystemPercentage + _LoyaltyResponse.Reward.UserPercentage;

                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _LoyaltyResponse, "HC001", "Configuration loaded");
                    //var ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x =>
                    //x.SystemName == _Request.ConfigurationKey
                    //&& x.StatusId == HelperStatus.Default.Active
                    //&& x.TypeId == HelperType.Configuration
                    //).Select(x => new
                    //{
                    //    SystemName = x.SystemName,
                    //    HelperId = x.HelperId,
                    //    Value = x.Value,
                    //    ConfigurationId = x.Id,
                    //}).FirstOrDefault();
                    //if (ConfigurationDetails != null)
                    //{
                    //    string Value = "";
                    //    var UserConfiguration = _HCoreContext.HCUAccountParameter.Where(x =>
                    //                                 x.TypeId == HelperType.ConfigurationValue
                    //                                 && x.StatusId == HelperStatus.Default.Active
                    //                                 && x.AccountId == UserAccountDetails.Id
                    //                                 && x.CommonId == ConfigurationDetails.ConfigurationId
                    //                                ).OrderByDescending(x => x.CreateDate)
                    //                                .Select(x => new
                    //                                {
                    //                                    Value = x.Value,
                    //                                }).FirstOrDefault();
                    //    if (UserConfiguration != null)
                    //    {
                    //        Value = UserConfiguration.Value;
                    //    }
                    //    if (!string.IsNullOrEmpty(Value))
                    //    {
                    //        var ConfigDetails = new
                    //        {
                    //            Value
                    //        };
                    //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ConfigDetails, "HC001", "Configuration loaded");
                    //    }
                    //    else
                    //    {
                    //        var ConfigDetails = new
                    //        {
                    //            ConfigurationDetails.Value
                    //        };
                    //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ConfigDetails, "HC001", "Configuration loaded");
                    //    }
                    //}
                    //else
                    //{
                    //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1073, TUCCoreResource.CA1073M);
                    //}
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetConfiguration", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
        }
        /// <summary>
        /// Description: Gets the configuration list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetConfiguration(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "StartDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountParameter
                                                .Where(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey && x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new OOperations.Configuration.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Value = x.Value,
                                                    Comment = x.Description,


                                                    ConfigurationId = x.Common.Id,
                                                    ConfigurationKey = x.Common.Guid,
                                                    ConfigurationName = x.Common.Name,
                                                    ConfigurationSystemName = x.Common.SystemName,

                                                    HelperCode = x.Helper.SystemName,
                                                    HelperName = x.Helper.Name,
                                                    StartDate = x.CreateDate,
                                                    CreatedByReferenceId = x.CreatedById,
                                                    CreatedByReferenceKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    EndDate = x.ModifyDate,
                                                    ModifyByReferenceId = x.ModifyById,
                                                    ModifyByReferenceKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    var Data = _HCoreContext.HCUAccountParameter
                                                .Where(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey && x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new OOperations.Configuration.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Value = x.Value,
                                                    Comment = x.Description,

                                                    ConfigurationId = x.Common.Id,
                                                    ConfigurationKey = x.Common.Guid,
                                                    ConfigurationName = x.Common.Name,
                                                    ConfigurationSystemName = x.Common.SystemName,

                                                    HelperCode = x.Helper.SystemName,
                                                    HelperName = x.Helper.Name,
                                                    StartDate = x.CreateDate,
                                                    CreatedByReferenceId = x.CreatedById,
                                                    CreatedByReferenceKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    EndDate = x.ModifyDate,
                                                    ModifyByReferenceId = x.ModifyById,
                                                    ModifyByReferenceKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    //foreach (var DataItem in _Terminals)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.ProviderIconUrl))
                    //    {
                    //        DataItem.ProviderIconUrl = _AppConfig.StorageUrl + DataItem.ProviderIconUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.ProviderIconUrl = _AppConfig.Default_Icon;
                    //    }
                    //    if (!string.IsNullOrEmpty(DataItem.AcquirerIconUrl))
                    //    {
                    //        DataItem.AcquirerIconUrl = _AppConfig.StorageUrl + DataItem.AcquirerIconUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.AcquirerIconUrl = _AppConfig.Default_Icon;
                    //    }
                    //}
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetConfiguration", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the configuration history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetConfigurationHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "StartDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountParameter
                                                .Where(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey && x.Common.SystemName == _Request.SubReferenceKey)
                                                .Select(x => new OOperations.Configuration.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Value = x.Value,
                                                    Comment = x.Description,

                                                    HelperCode = x.Helper.SystemName,
                                                    HelperName = x.Helper.Name,
                                                    StartDate = x.CreateDate,
                                                    CreatedByReferenceId = x.CreatedById,
                                                    CreatedByReferenceKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    EndDate = x.ModifyDate,
                                                    ModifyByReferenceId = x.ModifyById,
                                                    ModifyByReferenceKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    var Data = _HCoreContext.HCUAccountParameter
                                                .Where(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey && x.Common.SystemName == _Request.SubReferenceKey)
                                                .Select(x => new OOperations.Configuration.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Value = x.Value,
                                                    Comment = x.Description,

                                                    HelperCode = x.Helper.SystemName,
                                                    HelperName = x.Helper.Name,
                                                    StartDate = x.CreateDate,
                                                    CreatedByReferenceId = x.CreatedById,
                                                    CreatedByReferenceKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    EndDate = x.ModifyDate,
                                                    ModifyByReferenceId = x.ModifyById,
                                                    ModifyByReferenceKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    //foreach (var DataItem in _Terminals)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.ProviderIconUrl))
                    //    {
                    //        DataItem.ProviderIconUrl = _AppConfig.StorageUrl + DataItem.ProviderIconUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.ProviderIconUrl = _AppConfig.Default_Icon;
                    //    }
                    //    if (!string.IsNullOrEmpty(DataItem.AcquirerIconUrl))
                    //    {
                    //        DataItem.AcquirerIconUrl = _AppConfig.StorageUrl + DataItem.AcquirerIconUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.AcquirerIconUrl = _AppConfig.Default_Icon;
                    //    }
                    //}
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetConfigurationHistory", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }
}
