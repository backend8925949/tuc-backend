//==================================================================================
// FileName: FrameworkMerchantOnboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to merchant onbaording
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Resource;
using HCore.Operations;
using HCore.Operations.Object;
using System.Net;
using Newtonsoft.Json;
using RestSharp;

namespace HCore.TUC.Core.Framework.Merchant
{
    internal class tokenResponse
    {
        public string? access_token { get; set; }
        public string? token_type { get; set; }
        public string? expires_in { get; set; }
        public string? scope { get; set; }
        public string? client_name { get; set; }
        public string? jti { get; set; }
    }
    public class WebPayResponse
    {
        public string? Reference { get; set; }
        public WebPayBusinessInfo Business { get; set; }
        public WebPayContactInfo Contact { get; set; }

        public class WebPayBusinessInfo
        {
            public string? Name { get; set; }
            public string? EmailAddress { get; set; }
            public string? MobileNumber { get; set; }
            public string? Address { get; set; }
        }
        public class WebPayContactInfo
        {
            public string? Name { get; set; }
            public string? EmailAddress { get; set; }
            public string? MobileNumber { get; set; }
        }
    }

    internal class FrameworkMerchantOnboarding
    {
        #region Declare
        Random _Random;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        //HCUAccountOwner _HCUAccountOwner;
        HCoreContext _HCoreContext;
        //HCUAccountParameter _HCUAccountParameter;
        //List<HCUAccount> _HCUAccounts;
        //List<HCUAccountOwner> _HCUAccountOwners;
        //List<HCUAccountParameter> _HCUAccountParameters;
        List<HCUAccountParameter> _HCUAccountParameters;
        TUCTerminal _TUCTerminal;
        ManageCoreVerification _ManageCoreVerification;
        OCoreVerificationManager.Request _VerificationRequest;
        OCoreVerificationManager.RequestVerify _VerifyRequest;
        List<TUCCategoryAccount> _TUCCategoryAccount;
        #endregion
        internal OResponse OnboardSafaricomMerchant(OAccounts.MerchantOnboarding.MerchantDetails _Request)
        {
            #region Manage Exception
            try
            {
                //if (string.IsNullOrEmpty(_Request.DisplayName))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1064, TUCCoreResource.CA1064M);
                //}
                //if (string.IsNullOrEmpty(_Request.Name))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1065, TUCCoreResource.CA1065M);
                //}
                //if (string.IsNullOrEmpty(_Request.MobileNumber))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1066, TUCCoreResource.CA1066M);
                //}
                //if (string.IsNullOrEmpty(_Request.EmailAddress))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1067, TUCCoreResource.CA1067M);
                //}
                //if (_Request.Latitude == 0 && _Request.Longitude == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1059, TUCCoreResource.CA1059M);
                //}
                //if (_Request.BranchId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1060, TUCCoreResource.CA1060M);
                //}
                //if (_Request.RmId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1061, TUCCoreResource.CA1061M);
                //}
                //if (string.IsNullOrEmpty(_Request.StatusCode))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                //}
                //long StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                //if (StatusId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                //}
                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.Address, _Request.UserReference);
                using (_HCoreContext = new HCoreContext())
                {

                    if (string.IsNullOrEmpty(_Request.Password))
                    {
                        _Request.Password = HCoreHelper.GenerateRandomNumber(6);
                    }
                    var UserNameCheck = _HCoreContext.HCUAccountAuth.Any(x => x.Username == _Request.EmailAddress);
                    if (UserNameCheck)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1097, TUCCoreResource.CA1097M);
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber))
                    {
                        _Request.MobileNumber = HCoreHelper.FormatMobileNumber("254", _Request.MobileNumber);
                        var UserMobileNumberCheck = _HCoreContext.HCUAccount.Any(x => x.MobileNumber == _Request.MobileNumber && x.AccountTypeId == UserAccountType.Merchant);
                        if (UserMobileNumberCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1314, TUCCoreResource.CA1314M);
                        }
                    }
                    if (!string.IsNullOrEmpty(_Request.EmailAddress))
                    {
                        var UserEmailAddressCheck = _HCoreContext.HCUAccount.Any(x => x.EmailAddress == _Request.EmailAddress && x.AccountTypeId == UserAccountType.Merchant);
                        if (UserEmailAddressCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1315, TUCCoreResource.CA1315M);
                        }
                    }

                    _Random = new Random();
                    string AccountCode = HCoreHelper.GenerateRandomNumber(15);
                    string ReferenceKey = HCoreHelper.GenerateGuid();
                    HCUAccountParameter _HCUAccountParameter;
                    _HCUAccountParameters = new List<HCUAccountParameter>();
                    _HCUAccountParameter = new HCUAccountParameter();
                    //_HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    //_HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                    //_HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage").Select(x => x.Id).FirstOrDefault();
                    //_HCUAccountParameter.Value = _Request.RewardPercentage.ToString();
                    //_HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                    //_HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    //if (_Request.UserReference.AccountId != 0)
                    //{
                    //    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    //}
                    //_HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                    //_HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                    //_HCUAccountParameters.Add(_HCUAccountParameter);
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                    _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewarddeductiontype").Select(x => x.Id).FirstOrDefault();
                    _HCUAccountParameter.Value = "Prepay";
                    _HCUAccountParameter.HelperId = 253;
                    _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                    _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccountParameters.Add(_HCUAccountParameter);


                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = _Request.EmailAddress;
                    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    #region Save UserAccount
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                    if (_Request.Categories != null && _Request.Categories.Count > 0)
                    {
                        _HCUAccount.PrimaryCategoryId = _Request.Categories.FirstOrDefault().ReferenceId;
                        //_HCUAccountParameters = new List<HCUAccountParameter>();
                        //foreach (var Category in _Request.Categories)
                        //{
                        //    HCUAccountParameter _HCUAccountParameterItem = new HCUAccountParameter
                        //    {
                        //        Guid = HCoreHelper.GenerateGuid(),
                        //        TypeId = HelperType.MerchantCategory,
                        //        CommonId = Category.ReferenceId,
                        //        CreateDate = HCoreHelper.GetGMTDateTime(),
                        //        StatusId = HelperStatus.Default.Active,
                        //    };
                        //    _HCUAccount.HCUAccountParameterAccount.Add(_HCUAccountParameterItem);
                        //}
                        _TUCCategoryAccount = new List<TUCCategoryAccount>();
                        if (_Request.UserReference.AccountId == 0)
                        {
                            _Request.UserReference.AccountId = 1;
                        }
                        foreach (var Category in _Request.Categories)
                        {
                            _TUCCategoryAccount.Add(new TUCCategoryAccount
                            {
                                Guid = HCoreHelper.GenerateGuid(),
                                TypeId = 1,
                                CategoryId = Category.ReferenceId,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                CreatedById = _Request.UserReference.AccountId,
                                StatusId = HelperStatus.Default.Active,
                            });
                        }
                        _HCUAccount.TUCCategoryAccountAccount = _TUCCategoryAccount;
                    }
                    _HCUAccount.Guid = ReferenceKey;
                    _HCUAccount.AccountTypeId = UserAccountType.Merchant;
                    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                    _HCUAccount.OwnerId = 3;
                    _HCUAccount.DisplayName = _Request.DisplayName;
                    //_HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                    _HCUAccount.Name = _Request.Name;
                    _HCUAccount.EmailAddress = _Request.EmailAddress;
                    _HCUAccount.ContactNumber = _Request.MobileNumber;
                    if (_HCUAccount.OwnerId != 0)
                    {
                        _HCUAccount.OwnerId = 3;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(_Request.ReferralCode))
                        {
                            long ReferrerId = _HCoreContext.HCUAccount
                                .Where(x => x.ReferralCode == _Request.ReferralCode
                                && (x.AccountTypeId == UserAccountType.Merchant || x.AccountTypeId == UserAccountType.Acquirer || x.AccountTypeId == UserAccountType.PgAccount || x.AccountTypeId == UserAccountType.PosAccount))
                                .Select(x => x.Id).FirstOrDefault();
                            if (ReferrerId > 0)
                            {
                                _HCUAccount.OwnerId = ReferrerId;
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1458, TUCCoreResource.CA1458M);
                            }
                        }
                    }
                    _HCUAccount.WebsiteUrl = _Request.WebsiteUrl;
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
                    _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                    _HCUAccount.Address = _AddressResponse.Address;
                    _HCUAccount.Latitude = _AddressResponse.Latitude;
                    _HCUAccount.Longitude = _AddressResponse.Longitude;
                    _HCUAccount.CountryId = 118;
                    if (_AddressResponse.StateId != 0)
                    {
                        _HCUAccount.StateId = _AddressResponse.StateId;
                    }
                    if (_AddressResponse.CityId != 0)
                    {
                        _HCUAccount.CityId = _AddressResponse.CityId;
                    }
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    if (_Request.RegistrationSourceId > 0)
                    {
                        _HCUAccount.RegistrationSourceId = _Request.RegistrationSourceId;
                    }
                    else
                    {
                        _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                    }
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.StatusId = HelperStatus.Default.Active;
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.AccountPercentage = 3;
                    _HCUAccount.User = _HCUAccountAuth;
                    #endregion
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    _HCoreContext.SaveChanges();
                    long MerchantId = _HCUAccount.Id;
                    var EmailObject = new
                    {
                        UserName = _HCUAccountAuth.Username,
                        Password = _Request.Password,
                        DisplayName = _HCUAccount.DisplayName,
                    };
                    using (_HCoreContext = new HCoreContext())
                    {
                        _HCUAccountAuth = new HCUAccountAuth();
                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(10);
                        _HCUAccountAuth.Password = HCoreHelper.GenerateRandomNumber(6);
                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountAuth.CreatedById = MerchantId;
                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                        #region Save UserAccount
                        _HCUAccount = new HCUAccount();
                        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                        _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                        _HCUAccount.OwnerId = MerchantId;
                        _HCUAccount.DisplayName = _Request.DisplayName;
                        //if (_HCUAccount.OwnerId != 0)
                        //{
                        //    _HCUAccount.OwnerId = 3;
                        //}
                        //else
                        //{
                        //    if (!string.IsNullOrEmpty(_Request.ReferralCode))
                        //    {
                        //        long ReferrerId = _HCoreContext.HCUAccount
                        //            .Where(x => x.ReferralCode == _Request.ReferralCode
                        //            && (x.AccountTypeId == UserAccountType.Merchant || x.AccountTypeId == UserAccountType.Acquirer || x.AccountTypeId == UserAccountType.PgAccount || x.AccountTypeId == UserAccountType.PosAccount))
                        //            .Select(x => x.Id).FirstOrDefault();
                        //        if (ReferrerId > 0)
                        //        {
                        //            _HCUAccount.OwnerId = ReferrerId;
                        //        }
                        //        else
                        //        {
                        //            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1458, TUCCoreResource.CA1458M);
                        //        }
                        //    }
                        //}
                        _HCUAccount.Name = _Request.DisplayName;
                        _HCUAccount.EmailAddress = _Request.EmailAddress;
                        _HCUAccount.ContactNumber = _Request.MobileNumber;
                        _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber("254", _Request.MobileNumber);
                        _HCUAccount.Address = _AddressResponse.Address;
                        _HCUAccount.Latitude = _AddressResponse.Latitude;
                        _HCUAccount.Longitude = _AddressResponse.Longitude;
                        _HCUAccount.Longitude = 118;
                        if (_AddressResponse.StateId != 0)
                        {
                            _HCUAccount.StateId = _AddressResponse.StateId;
                        }
                        if (_AddressResponse.CityId != 0)
                        {
                            _HCUAccount.CityId = _AddressResponse.CityId;
                        }
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                        _HCUAccount.AccountCode = _Random.Next(100000000, 999999999).ToString();
                        if (_Request.UserReference.AppVersionId != 0)
                        {
                            _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                        }
                        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                        _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                        }
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                        _HCUAccount.EmailVerificationStatus = 0;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.NumberVerificationStatus = 0;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.User = _HCUAccountAuth;
                        #endregion
                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                        _HCoreContext.SaveChanges();

                    }

                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Setup closed loyalty
                        double RewardPercentage = _Request.RewardPercentage;
                        if (_Request.RewardPercentage > 0)
                        {
                            RewardPercentage = Convert.ToDouble(_HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage" && x.TypeId == HelperType.Configuration).Select(x => x.Value).FirstOrDefault());
                        }
                        _HCUAccountParameter = new HCUAccountParameter();
                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountParameter.AccountId = MerchantId;
                        _HCUAccountParameter.TypeId = HelperType.ThankUCashLoyalty;
                        _HCUAccountParameter.SubTypeId = HelperType.ThankUCashLoyalties.ClosedLoyalty;
                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage").Select(x => x.Id).FirstOrDefault();
                        _HCUAccountParameter.Value = RewardPercentage.ToString();
                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreatedById = MerchantId;
                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);

                        #region  Close Loyalty
                        #region Set Closed Loyalty
                        _HCUAccountParameter = new HCUAccountParameter();
                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountParameter.AccountId = MerchantId;
                        _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                        _HCUAccountParameter.HelperId = Helpers.DataType.Number;
                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "thankucashgold").Select(x => x.Id).FirstOrDefault();
                        _HCUAccountParameter.Value = "1";
                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreatedById = MerchantId;
                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                        #endregion

                        #region Set User Reward % to 100
                        _HCUAccountParameter = new HCUAccountParameter();
                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountParameter.AccountId = MerchantId;
                        _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                        _HCUAccountParameter.HelperId = Helpers.DataType.Number;
                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "userrewardpercentage").Select(x => x.Id).FirstOrDefault();
                        _HCUAccountParameter.Value = "100";
                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreatedById = MerchantId;
                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                        #endregion

                        #region Set TUC Reward % to 0
                        _HCUAccountParameter = new HCUAccountParameter();
                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountParameter.AccountId = MerchantId;
                        _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                        _HCUAccountParameter.HelperId = Helpers.DataType.Number;
                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardcomissionpercentage").Select(x => x.Id).FirstOrDefault();
                        _HCUAccountParameter.Value = "0";
                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreatedById = MerchantId;
                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                        #endregion



                        #region Set TUC Reward % to 0
                        _HCUAccountParameter = new HCUAccountParameter();
                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountParameter.AccountId = MerchantId;
                        _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                        _HCUAccountParameter.HelperId = Helpers.DataType.Text;
                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "safaricomshortcode").Select(x => x.Id).FirstOrDefault();
                        _HCUAccountParameter.Value = _Request.ShortCode;
                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreatedById = MerchantId;
                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                        #endregion
                        #endregion
                        _HCoreContext.SaveChanges();

                        #endregion
                    }


                    HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantWelcomeEmail, _Request.DisplayName, _Request.EmailAddress, EmailObject, _Request.UserReference);
                    //#region Request Verification
                    //_VerificationRequest = new OCoreVerificationManager.Request();
                    //_VerificationRequest.CountryIsd = "254";
                    //_VerificationRequest.Type = 1;
                    //_VerificationRequest.MobileNumber = _Request.MobileNumber;
                    //_VerificationRequest.UserReference = _Request.UserReference;
                    //_ManageCoreVerification = new ManageCoreVerification();
                    //var VerificationResponse = _ManageCoreVerification.RequestOtp(_VerificationRequest);
                    //#endregion
                    //string Token = "";
                    //if (VerificationResponse.Status == StatusSuccess)
                    //{
                    //    OCoreVerificationManager.Response VerificationResponseItem = (OCoreVerificationManager.Response)VerificationResponse.Result;
                    //    using (_HCoreContext = new HCoreContext())
                    //    {
                    //        var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Id == MerchantId).FirstOrDefault();
                    //        MerchantDetails.ReferralUrl = VerificationResponseItem.RequestToken;
                    //        Token = VerificationResponseItem.RequestToken;
                    //        _HCoreContext.SaveChanges();
                    //    }
                    //}


                    #region  Post Safaricom Data
                    try
                    {
                        string Url = "https://safaricomtest.thankucash.com/api/C2B/c2b-register";
                        if (HostEnvironment == HostEnvironmentType.Live)
                        {
                            Url = "https://miniappconnect.thankucash.com/api/C2B/c2b-register";
                        }
                        var client = new RestSharp.RestClient(Url);
                        var request = new RestSharp.RestRequest();
                        request.Method = Method.Post;
                        request.AddHeader("accept", "application/json");
                        request.AddHeader("content-type", "application/json");
                        request.AddParameter("application/json", "{\"shortCode\":\"" + _Request.ShortCode + "\",\"accountCode\":\"" + AccountCode + "\",\"name\":\"" + _Request.DisplayName + "\"}", RestSharp.ParameterType.RequestBody);
                        var response = client.Execute(request);
                    }
                    catch (Exception ex)
                    {
                        return HCoreHelper.LogException("SaveMerchant-SAFARICOM-CONNECT", ex, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                    }
                    #endregion

                    var _Response = new
                    {
                        ReferenceId = MerchantId,
                        ReferenceKey = ReferenceKey,
                        //Token = Token,
                        AccountCode = AccountCode,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, TUCCoreResource.CA1063, TUCCoreResource.CA1063M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveMerchant-SAFARICOM", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                //return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1063, _Exception.ToString());
            }
            #endregion
        }

        /// <summary>
        /// Description: Called when onboard merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse OnboardMerchant(OAccounts.MerchantOnboarding.MerchantDetails _Request)
        {
            #region Manage Exception
            try
            {
                //return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "", "Registration not available from App. Please complete registration on thankucash.com website");

                //if (string.IsNullOrEmpty(_Request.DisplayName))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1064, TUCCoreResource.CA1064M);
                //}
                //if (string.IsNullOrEmpty(_Request.Name))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1065, TUCCoreResource.CA1065M);
                //}
                //if (string.IsNullOrEmpty(_Request.MobileNumber))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1066, TUCCoreResource.CA1066M);
                //}
                //if (string.IsNullOrEmpty(_Request.EmailAddress))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1067, TUCCoreResource.CA1067M);
                //}
                //if (_Request.Latitude == 0 && _Request.Longitude == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1059, TUCCoreResource.CA1059M);
                //}
                //if (_Request.BranchId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1060, TUCCoreResource.CA1060M);
                //}
                //if (_Request.RmId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1061, TUCCoreResource.CA1061M);
                //}
                //if (string.IsNullOrEmpty(_Request.StatusCode))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                //}
                //long StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                //if (StatusId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                //}
                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.Address, _Request.UserReference);
                using (_HCoreContext = new HCoreContext())
                {
                    var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Id == _AddressResponse.CountryId)
                        .Select(x => new
                        {
                            Isd = x.Isd,
                            MobileNumberLength = x.MobileNumberLength,
                        }).FirstOrDefault();
                    if (string.IsNullOrEmpty(_Request.Password))
                    {
                        _Request.Password = HCoreHelper.GenerateRandomNumber(6);
                    }
                    var UserNameCheck = _HCoreContext.HCUAccountAuth.Any(x => x.Username == _Request.EmailAddress);
                    if (UserNameCheck)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1097, TUCCoreResource.CA1097M);
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber))
                    {
                        _Request.MobileNumber = HCoreHelper.FormatMobileNumber(CountryDetails.Isd, _Request.MobileNumber, CountryDetails.MobileNumberLength);
                        var UserMobileNumberCheck = _HCoreContext.HCUAccount.Any(x => x.MobileNumber == _Request.MobileNumber && x.AccountTypeId == UserAccountType.Merchant);
                        if (UserMobileNumberCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1314, TUCCoreResource.CA1314M);
                        }
                    }
                    if (!string.IsNullOrEmpty(_Request.EmailAddress))
                    {
                        var UserEmailAddressCheck = _HCoreContext.HCUAccount.Any(x => x.EmailAddress == _Request.EmailAddress && x.AccountTypeId == UserAccountType.Merchant);
                        if (UserEmailAddressCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1315, TUCCoreResource.CA1315M);
                        }
                    }

                    _Random = new Random();
                    string AccountCode = _Random.Next(100, 999).ToString() + _Random.Next(000000000, 999999999).ToString();
                    string ReferenceKey = HCoreHelper.GenerateGuid();

                    HCUAccountParameter _HCUAccountParameter;
                    _HCUAccountParameters = new List<HCUAccountParameter>();
                    _HCUAccountParameter = new HCUAccountParameter();
                    //_HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    //_HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                    //_HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage").Select(x => x.Id).FirstOrDefault();
                    //_HCUAccountParameter.Value = _Request.RewardPercentage.ToString();
                    //_HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                    //_HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    //if (_Request.UserReference.AccountId != 0)
                    //{
                    //    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    //}
                    //_HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                    //_HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                    //_HCUAccountParameters.Add(_HCUAccountParameter);

                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                    _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewarddeductiontype").Select(x => x.Id).FirstOrDefault();
                    _HCUAccountParameter.Value = "Prepay";
                    _HCUAccountParameter.HelperId = 253;
                    _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                    _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccountParameters.Add(_HCUAccountParameter);


                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = _Request.EmailAddress;
                    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    #region Save UserAccount
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                    if (_Request.Categories != null && _Request.Categories.Count > 0)
                    {
                        _HCUAccount.PrimaryCategoryId = _Request.Categories.FirstOrDefault().ReferenceId;
                        //_HCUAccountParameters = new List<HCUAccountParameter>();
                        //foreach (var Category in _Request.Categories)
                        //{
                        //    HCUAccountParameter _HCUAccountParameterItem = new HCUAccountParameter
                        //    {
                        //        Guid = HCoreHelper.GenerateGuid(),
                        //        TypeId = HelperType.MerchantCategory,
                        //        CommonId = Category.ReferenceId,
                        //        CreateDate = HCoreHelper.GetGMTDateTime(),
                        //        StatusId = HelperStatus.Default.Active,
                        //    };
                        //    _HCUAccount.HCUAccountParameterAccount.Add(_HCUAccountParameterItem);
                        //}
                        _TUCCategoryAccount = new List<TUCCategoryAccount>();
                        if (_Request.UserReference.AccountId == 0)
                        {
                            _Request.UserReference.AccountId = 1;
                        }
                        foreach (var Category in _Request.Categories)
                        {
                            _TUCCategoryAccount.Add(new TUCCategoryAccount
                            {
                                Guid = HCoreHelper.GenerateGuid(),
                                TypeId = 1,
                                CategoryId = Category.ReferenceId,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                CreatedById = _Request.UserReference.AccountId,
                                StatusId = HelperStatus.Default.Active,
                            });
                        }
                        _HCUAccount.TUCCategoryAccountAccount = _TUCCategoryAccount;
                    }
                    _HCUAccount.Guid = ReferenceKey;
                    _HCUAccount.AccountTypeId = UserAccountType.Merchant;
                    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                    _HCUAccount.OwnerId = 3;
                    _HCUAccount.DisplayName = _Request.DisplayName;
                    //_HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                    _HCUAccount.Name = _Request.Name;
                    _HCUAccount.EmailAddress = _Request.EmailAddress;
                    _HCUAccount.ContactNumber = _Request.MobileNumber;
                    if (_HCUAccount.OwnerId != 0)
                    {
                        _HCUAccount.OwnerId = 3;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(_Request.ReferralCode))
                        {
                            long ReferrerId = _HCoreContext.HCUAccount
                                .Where(x => x.ReferralCode == _Request.ReferralCode
                                && (x.AccountTypeId == UserAccountType.Merchant || x.AccountTypeId == UserAccountType.Acquirer || x.AccountTypeId == UserAccountType.PgAccount || x.AccountTypeId == UserAccountType.PosAccount))
                                .Select(x => x.Id).FirstOrDefault();
                            if (ReferrerId > 0)
                            {
                                _HCUAccount.OwnerId = ReferrerId;
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1458, TUCCoreResource.CA1458M);
                            }
                        }
                    }
                    _HCUAccount.WebsiteUrl = _Request.WebsiteUrl;
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
                    _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                    _HCUAccount.Address = _AddressResponse.Address;
                    _HCUAccount.Latitude = _AddressResponse.Latitude;
                    _HCUAccount.Longitude = _AddressResponse.Longitude;
                    _HCUAccount.CountryId = _AddressResponse.CountryId;
                    if (_AddressResponse.StateId != 0)
                    {
                        _HCUAccount.StateId = _AddressResponse.StateId;
                    }
                    if (_AddressResponse.CityId != 0)
                    {
                        _HCUAccount.CityId = _AddressResponse.CityId;
                    }
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    if (_Request.RegistrationSourceId > 0)
                    {
                        _HCUAccount.RegistrationSourceId = _Request.RegistrationSourceId;
                    }
                    else
                    {
                        _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                    }
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.StatusId = HelperStatus.Default.Inactive;
                    if (_AddressResponse.CountryId > 0)
                    {
                        _HCUAccount.CountryId = _AddressResponse.CountryId;
                    }
                    else
                    {
                        _HCUAccount.CountryId = _Request.UserReference.SystemCountry;
                    }
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.AccountPercentage = 3;
                    _HCUAccount.User = _HCUAccountAuth;
                    #endregion
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    _HCoreContext.SaveChanges();
                    long MerchantId = _HCUAccount.Id;
                    var EmailObject = new
                    {
                        UserName = _HCUAccountAuth.Username,
                        Password = _Request.Password,
                        DisplayName = _HCUAccount.DisplayName,
                    };
                    using (_HCoreContext = new HCoreContext())
                    {
                        _HCUAccountAuth = new HCUAccountAuth();
                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(10);
                        _HCUAccountAuth.Password = HCoreHelper.GenerateRandomNumber(6);
                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountAuth.CreatedById = MerchantId;
                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                        #region Save UserAccount
                        _HCUAccount = new HCUAccount();
                        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                        _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                        _HCUAccount.OwnerId = MerchantId;
                        _HCUAccount.DisplayName = _Request.DisplayName;
                        //if (_HCUAccount.OwnerId != 0)
                        //{
                        //    _HCUAccount.OwnerId = 3;
                        //}
                        //else
                        //{
                        //    if (!string.IsNullOrEmpty(_Request.ReferralCode))
                        //    {
                        //        long ReferrerId = _HCoreContext.HCUAccount
                        //            .Where(x => x.ReferralCode == _Request.ReferralCode
                        //            && (x.AccountTypeId == UserAccountType.Merchant || x.AccountTypeId == UserAccountType.Acquirer || x.AccountTypeId == UserAccountType.PgAccount || x.AccountTypeId == UserAccountType.PosAccount))
                        //            .Select(x => x.Id).FirstOrDefault();
                        //        if (ReferrerId > 0)
                        //        {
                        //            _HCUAccount.OwnerId = ReferrerId;
                        //        }
                        //        else
                        //        {
                        //            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1458, TUCCoreResource.CA1458M);
                        //        }
                        //    }
                        //}
                        _HCUAccount.Name = _Request.DisplayName;
                        _HCUAccount.EmailAddress = _Request.EmailAddress;
                        _HCUAccount.ContactNumber = _Request.MobileNumber;
                        _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                        _HCUAccount.Address = _AddressResponse.Address;
                        _HCUAccount.Latitude = _AddressResponse.Latitude;
                        _HCUAccount.Longitude = _AddressResponse.Longitude;
                        if (_AddressResponse.CountryId > 0)
                        {
                            _HCUAccount.CountryId = _AddressResponse.CountryId;
                        }
                        else
                        {
                            _HCUAccount.CountryId = _Request.UserReference.SystemCountry;
                        }
                        if (_AddressResponse.StateId != 0)
                        {
                            _HCUAccount.StateId = _AddressResponse.StateId;
                        }
                        if (_AddressResponse.CityId != 0)
                        {
                            _HCUAccount.CityId = _AddressResponse.CityId;
                        }
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                        _HCUAccount.AccountCode = _Random.Next(100000000, 999999999).ToString();
                        if (_Request.UserReference.AppVersionId != 0)
                        {
                            _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                        }
                        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                        _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                        }
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                        _HCUAccount.EmailVerificationStatus = 0;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.NumberVerificationStatus = 0;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.User = _HCUAccountAuth;
                        #endregion
                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                        _HCoreContext.SaveChanges();

                    }
                    HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantWelcomeEmail, _Request.DisplayName, _Request.EmailAddress, EmailObject, _Request.UserReference);
                    #region Request Verification
                    _VerificationRequest = new OCoreVerificationManager.Request();
                    _VerificationRequest.CountryIsd = CountryDetails.Isd;
                    _VerificationRequest.Type = 1;
                    _VerificationRequest.MobileNumber = _Request.MobileNumber;
                    _VerificationRequest.UserReference = _Request.UserReference;
                    _ManageCoreVerification = new ManageCoreVerification();
                    var VerificationResponse = _ManageCoreVerification.RequestOtp(_VerificationRequest);
                    #endregion
                    string Token = "";
                    if (VerificationResponse.Status == StatusSuccess)
                    {
                        OCoreVerificationManager.Response VerificationResponseItem = (OCoreVerificationManager.Response)VerificationResponse.Result;
                        using (_HCoreContext = new HCoreContext())
                        {
                            var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Id == MerchantId).FirstOrDefault();
                            MerchantDetails.ReferralUrl = VerificationResponseItem.RequestToken;
                            Token = VerificationResponseItem.RequestToken;
                            _HCoreContext.SaveChanges();
                        }
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        #region  Close Loyalty
                        #region Set Closed Loyalty
                        _HCUAccountParameter = new HCUAccountParameter();
                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountParameter.AccountId = MerchantId;
                        _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                        _HCUAccountParameter.HelperId = Helpers.DataType.Number;
                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "thankucashgold").Select(x => x.Id).FirstOrDefault();
                        _HCUAccountParameter.Value = "1";
                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                        #endregion

                        #region Set User Reward % to 100
                        _HCUAccountParameter = new HCUAccountParameter();
                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountParameter.AccountId = MerchantId;
                        _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                        _HCUAccountParameter.HelperId = Helpers.DataType.Number;
                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "userrewardpercentage").Select(x => x.Id).FirstOrDefault();
                        _HCUAccountParameter.Value = "100";
                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                        #endregion

                        #region Set TUC Reward % to 0
                        _HCUAccountParameter = new HCUAccountParameter();
                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountParameter.AccountId = MerchantId;
                        _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                        _HCUAccountParameter.HelperId = Helpers.DataType.Number;
                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardcomissionpercentage").Select(x => x.Id).FirstOrDefault();
                        _HCUAccountParameter.Value = "0";
                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                        #endregion
                        _HCoreContext.SaveChanges();
                        #endregion
                    }
                    var _Response = new
                    {
                        ReferenceId = MerchantId,
                        ReferenceKey = ReferenceKey,
                        Token = Token
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, TUCCoreResource.CA1063, TUCCoreResource.CA1063M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveMerchant", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Onboard merchant verify number.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse OnboardMerchantVerifyNumber(OAccounts.MerchantOnboarding.VerifyOtp _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.Token))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1092, TUCCoreResource.CA1092M);
                }
                if (string.IsNullOrEmpty(_Request.AccessCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1093, TUCCoreResource.CA1093M);
                }
                //if (_Request.Latitude == 0 && _Request.Longitude == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1059, TUCCoreResource.CA1059M);
                //}
                //if (_Request.BranchId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1060, TUCCoreResource.CA1060M);
                //}
                //if (_Request.RmId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1061, TUCCoreResource.CA1061M);
                //}
                //if (string.IsNullOrEmpty(_Request.StatusCode))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                //}
                //long StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                //if (StatusId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                //}
                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.ReferralUrl == _Request.Token).FirstOrDefault();
                    if (MerchantDetails == null)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1094, TUCCoreResource.CA1094M);
                    }
                    #region Request Verification
                    _VerifyRequest = new OCoreVerificationManager.RequestVerify();
                    _VerifyRequest.RequestToken = MerchantDetails.ReferralUrl;
                    _VerifyRequest.AccessCode = _Request.AccessCode;
                    _VerifyRequest.UserReference = _Request.UserReference;
                    _ManageCoreVerification = new ManageCoreVerification();
                    var VerificationResponse = _ManageCoreVerification.VerifyOtp(_VerifyRequest);
                    #endregion
                    if (VerificationResponse.Status == StatusSuccess)
                    {
                        OCoreVerificationManager.Response VerificationResponseItem = (OCoreVerificationManager.Response)VerificationResponse.Result;
                        using (_HCoreContext = new HCoreContext())
                        {
                            var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == MerchantDetails.Id).FirstOrDefault();
                            AccountDetails.NumberVerificationStatus = 1;
                            AccountDetails.NumberVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                            AccountDetails.StatusId = HelperStatus.Default.Active;
                            var MerchantStores = _HCoreContext.HCUAccount.Where(x => x.OwnerId == AccountDetails.Id).ToList();
                            foreach (var MerchantStore in MerchantStores)
                            {
                                MerchantStore.ModifyDate = HCoreHelper.GetGMTDateTime();
                                MerchantStore.StatusId = HelperStatus.Default.Active;
                            }
                            _HCoreContext.SaveChanges();
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1095, TUCCoreResource.CA1095M);
                    }
                    else
                    {
                        return VerificationResponse;
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchantVerifyNumber", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Onboard merchant request verification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse OnboardMerchantRequestVerification(OAccounts.MerchantOnboarding.VerifyOtp _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                //if (_Request.Latitude == 0 && _Request.Longitude == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1059, TUCCoreResource.CA1059M);
                //}
                //if (_Request.BranchId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1060, TUCCoreResource.CA1060M);
                //}
                //if (_Request.RmId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1061, TUCCoreResource.CA1061M);
                //}
                //if (string.IsNullOrEmpty(_Request.StatusCode))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                //}
                //long StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                //if (StatusId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                //}
                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (MerchantDetails == null)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1094, TUCCoreResource.CA1094M);
                    }
                    if (MerchantDetails.NumberVerificationStatus == 1)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1096, TUCCoreResource.CA1096M);
                    }
                    string CountryIsd = _HCoreContext.HCCoreCountry.Where(x => x.Id == MerchantDetails.CountryId).Select(x => x.Isd).FirstOrDefault();

                    _VerificationRequest = new OCoreVerificationManager.Request();
                    _VerificationRequest.CountryIsd = CountryIsd;
                    _VerificationRequest.Type = 1;
                    _VerificationRequest.MobileNumber = MerchantDetails.MobileNumber;
                    _VerificationRequest.UserReference = _Request.UserReference;
                    _ManageCoreVerification = new ManageCoreVerification();
                    var VerificationResponse = _ManageCoreVerification.RequestOtp(_VerificationRequest);
                    string Token = "";
                    if (VerificationResponse.Status == StatusSuccess)
                    {
                        OCoreVerificationManager.Response VerificationResponseItem = (OCoreVerificationManager.Response)VerificationResponse.Result;
                        using (_HCoreContext = new HCoreContext())
                        {
                            var MerchantDetailIs = _HCoreContext.HCUAccount.Where(x => x.Id == MerchantDetails.Id).FirstOrDefault();
                            MerchantDetailIs.ReferralUrl = VerificationResponseItem.RequestToken;
                            MerchantDetailIs.ModifyDate = HCoreHelper.GetGMTDateTime();
                            Token = VerificationResponseItem.RequestToken;
                            _HCoreContext.SaveChanges();
                        }
                    }
                    var _Response = new
                    {
                        ReferenceId = MerchantDetails.Id,
                        ReferenceKey = MerchantDetails.Guid,
                        Token = Token
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, TUCCoreResource.CAA14155, TUCCoreResource.CAA14155M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchantRequestVerification", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Onboard merchant change verification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse OnboardMerchantChangeVerification(OAccounts.MerchantOnboarding.ChangeNumber _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCM3323", "Enter mobile number");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (MerchantDetails == null)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1094, TUCCoreResource.CA1094M);
                    }

                    string CountryIsd = _HCoreContext.HCCoreCountry.Where(x => x.Id == MerchantDetails.CountryId).Select(x => x.Isd).FirstOrDefault();
                    string MobileNumber = HCoreHelper.FormatMobileNumber(CountryIsd, _Request.MobileNumber);
                    if (!string.IsNullOrEmpty(MobileNumber))
                    {
                        var UserMobileNumberCheck = _HCoreContext.HCUAccount.Any(x => x.MobileNumber == MobileNumber && x.Id != MerchantDetails.Id && x.AccountTypeId == UserAccountType.Merchant);
                        if (UserMobileNumberCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1314, TUCCoreResource.CA1314M);
                        }
                    }
                    MerchantDetails.ContactNumber = MobileNumber;
                    MerchantDetails.MobileNumber = MobileNumber;
                    MerchantDetails.CpContactNumber = MobileNumber;
                    MerchantDetails.NumberVerificationStatus = 0;
                    MerchantDetails.NumberVerificationStatusDate = null;
                    _HCoreContext.SaveChanges();
                    #region Request Verification
                    _VerificationRequest = new OCoreVerificationManager.Request();
                    _VerificationRequest.CountryIsd = CountryIsd;
                    _VerificationRequest.Type = 1;
                    _VerificationRequest.MobileNumber = MobileNumber;
                    _VerificationRequest.UserReference = _Request.UserReference;
                    _ManageCoreVerification = new ManageCoreVerification();
                    var VerificationResponse = _ManageCoreVerification.RequestOtp(_VerificationRequest);
                    #endregion
                    string Token = "";
                    if (VerificationResponse.Status == StatusSuccess)
                    {
                        OCoreVerificationManager.Response VerificationResponseItem = (OCoreVerificationManager.Response)VerificationResponse.Result;
                        using (_HCoreContext = new HCoreContext())
                        {
                            var MerchantDetailIs = _HCoreContext.HCUAccount.Where(x => x.Id == MerchantDetails.Id).FirstOrDefault();
                            MerchantDetailIs.ReferralUrl = VerificationResponseItem.RequestToken;
                            MerchantDetailIs.ModifyDate = HCoreHelper.GetGMTDateTime();
                            Token = VerificationResponseItem.RequestToken;
                            _HCoreContext.SaveChanges();
                        }
                    }
                    var _Response = new
                    {
                        ReferenceId = MerchantDetails.Id,
                        ReferenceKey = MerchantDetails.Guid,
                        Token = Token,
                        MobileNumber = MobileNumber,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, TUCCoreResource.CAA14156, TUCCoreResource.CAA14156M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchantChangeVerification", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Web pay initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse WebPay_Initialize(OAccounts.OCoreMerchantWebPay.Initialize.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1334, TUCCoreResource.CA1334M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    string ApiKey = "7d502ca638fc4e80805c89a6be002759";
                    if (HostEnvironment == HostEnvironmentType.Test)
                    {
                        ApiKey = "e0dd58f4c09a403bb18797a2727eef4d";
                    }
                    HCoreHelper.LogData(HCoreConstant.LogType.Log, "WEBPAYINI", _Request.Reference, null, null);
                    string ReferenceData = null;
                    try
                    {
                        ReferenceData = HCoreEncryptAes.DecryptString(ApiKey, _Request.Reference);
                    }
                    catch (Exception _Exception)
                    {

                        HCoreHelper.LogException("WebPay_Initialize_Decrypt", _Exception);
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1333, TUCCoreResource.CA1333M);
                    }
                    string Reference = null;
                    string MerchantCode = null;
                    if (ReferenceData.Contains("|"))
                    {
                        string[] Dts = ReferenceData.Split("|");
                        Reference = Dts[0];
                        MerchantCode = Dts[1];
                    }
                    else
                    {
                        Reference = ReferenceData;
                    }
                    if (!string.IsNullOrEmpty(MerchantCode) && !string.IsNullOrEmpty(Reference))
                    {
                        var MerchantDetails = _HCoreContext.HCUAccount
                            .Where(x => x.AccountCode == MerchantCode && x.ReferenceNumber == Reference
                            && x.AccountTypeId == UserAccountType.Merchant)
                            .Select(x => new
                            {
                                ReferenceId = x.Id,
                                UserName = x.User.Username,
                                SystemPassword = x.User.SystemPassword,
                            }).FirstOrDefault();
                        if (MerchantDetails != null)
                        {
                            var _Response = new
                            {
                                Reference = Reference,
                                MerchantCode = MerchantCode,
                                IsMerchantConnected = true,
                                taccessid = MerchantDetails.UserName,
                                taccesstoken = HCoreEncrypt.DecryptHash(MerchantDetails.SystemPassword)
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, TUCCoreResource.CA1331, TUCCoreResource.CA1331M);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1324, TUCCoreResource.CA1324M);
                        }
                    }
                    else
                    {
                        try
                        {
                            string data = "scope=profile&grant_type=client_credentials";
                            using (WebClient _WebClient = new WebClient())
                            {
                                string TokenUrl = "https://api.interswitchng.com/passport/oauth/token";
                                if (HostEnvironment == HostEnvironmentType.Test)
                                {
                                    TokenUrl = "https://apps.qa.interswitchng.com/passport/oauth/token";
                                }
                                _WebClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                                _WebClient.Headers.Add("Authorization", "Basic SUtJQTBBQjIxRTZGRjhGMDM2NTBGQjkzQTMxNDQ2NEZGOTMwOTAxM0UzMEI6dGhhbmstdS1jYXNo");
                                string result = _WebClient.UploadString(TokenUrl, data);
                                if (!string.IsNullOrEmpty(result))
                                {
                                    tokenResponse _TokenResponse = JsonConvert.DeserializeObject<tokenResponse>(result);
                                    if (_TokenResponse != null)
                                    {
                                        string Url = "https://api.interswitchng.com/paymentgateway/api/v1/onboarding/tuc";
                                        if (HostEnvironment == HostEnvironmentType.Test)
                                        {
                                            Url = "https://testwebpay.interswitchng.com/paymentgateway/api/v1/onboarding/tuc";
                                        }
                                        var client = new RestSharp.RestClient(Url);
                                        RestSharp.RestRequest _RestRequest = new RestSharp.RestRequest();
                                        _RestRequest.Method = RestSharp.Method.Post;
                                        _RestRequest.AddHeader("accept", "application/json");
                                        _RestRequest.AddHeader("content-type", "application/json");
                                        string Part = "{\"merchantCode\":\"" + Reference + "\"}";
                                        _RestRequest.AddHeader("authorization", "Bearer " + _TokenResponse.access_token);
                                        _RestRequest.AddParameter("application/json", Part, RestSharp.ParameterType.RequestBody);
                                        RestSharp.RestResponse response = client.Execute(_RestRequest);
                                        if (response.StatusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(response.Content))
                                        {
                                            HCoreHelper.LogData(HCoreConstant.LogType.Log, "AAWWQQ", Part, JsonConvert.SerializeObject(response.Content), null);
                                            WebPayResponse _RequestDetails = JsonConvert.DeserializeObject<WebPayResponse>(response.Content);
                                            if (_RequestDetails != null && _RequestDetails.Business != null)
                                            {
                                                if (!string.IsNullOrEmpty(_RequestDetails.Business.Name) && !string.IsNullOrEmpty(_RequestDetails.Business.MobileNumber) && !string.IsNullOrEmpty(_RequestDetails.Business.EmailAddress))
                                                {
                                                    var _Response = new
                                                    {
                                                        IsMerchantConnected = false,
                                                        Reference = Reference,
                                                        MerchantCode = MerchantCode,
                                                        Business = _RequestDetails.Business,
                                                        Contact = _RequestDetails.Contact,
                                                        RewardPercentage = new
                                                        {
                                                            Minimum = 3,
                                                            Maximum = 20
                                                        }
                                                    };
                                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, TUCCoreResource.CA1324, TUCCoreResource.CA1324M);
                                                }
                                                else
                                                {
                                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1326, TUCCoreResource.CA1326M);
                                                }
                                            }
                                            else
                                            {
                                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1327, TUCCoreResource.CA1327M);
                                            }
                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1325, TUCCoreResource.CA1325M);
                                        }
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1328, TUCCoreResource.CA1328M);
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1329, TUCCoreResource.CA1329M);
                                }
                            }
                        }
                        catch (Exception _Exception)
                        {
                            HCoreHelper.LogException("WEBPAYCON_REQ", _Exception);
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1330, TUCCoreResource.CA1330M);
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("WebPay_Initialize", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1332, TUCCoreResource.CA1332M);
            }
        }
        /// <summary>
        /// Description: Web pay confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse WebPay_Confirm(OAccounts.OCoreMerchantWebPay.Confirm _Request)
        {
            #region Manage Exception
            try
            {

                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1335, TUCCoreResource.CA1335M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var UserNameCheck = _HCoreContext.HCUAccountAuth.Any(x => x.Username == _Request.Business.EmailAddress);
                    if (UserNameCheck)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1097, TUCCoreResource.CA1097M);
                    }
                    if (!string.IsNullOrEmpty(_Request.Business.MobileNumber))
                    {
                        _Request.Business.MobileNumber = HCoreHelper.FormatMobileNumber("234", _Request.Business.MobileNumber);
                        var UserMobileNumberCheck = _HCoreContext.HCUAccount.Any(x => x.MobileNumber == _Request.Business.MobileNumber && x.AccountTypeId == UserAccountType.Merchant);
                        if (UserMobileNumberCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1314, TUCCoreResource.CA1314M);
                        }
                    }
                    if (!string.IsNullOrEmpty(_Request.Business.EmailAddress))
                    {
                        var UserEmailAddressCheck = _HCoreContext.HCUAccount.Any(x => x.EmailAddress == _Request.Business.EmailAddress && x.AccountTypeId == UserAccountType.Merchant);
                        if (UserEmailAddressCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1315, TUCCoreResource.CA1315M);
                        }
                    }

                    _Random = new Random();
                    string AccountCode = _Random.Next(100, 999).ToString() + _Random.Next(000000000, 999999999).ToString();
                    string ReferenceKey = HCoreHelper.GenerateGuid();
                    string MerchantCode = HCoreHelper.GenerateRandomNumber(15);
                    HCUAccountParameter _HCUAccountParameter;
                    _HCUAccountParameters = new List<HCUAccountParameter>();
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                    _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewarddeductiontype").Select(x => x.Id).FirstOrDefault();
                    _HCUAccountParameter.Value = "Prepay";
                    _HCUAccountParameter.HelperId = 253;
                    _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                    _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccountParameters.Add(_HCUAccountParameter);

                    string UserName = _Request.Business.EmailAddress;
                    string Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = UserName;
                    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.Business.Password);
                    _HCUAccountAuth.SecondaryPassword = Password;
                    _HCUAccountAuth.SystemPassword = Password;
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    #region Save UserAccount
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                    //if (_Request.Business.Categories != null && _Request.Business.Categories.Count > 0)
                    //{
                    //    _HCUAccountParameters = new List<HCUAccountParameter>();
                    //    foreach (var Category in _Request.Business.Categories)
                    //    {
                    //        HCUAccountParameter _HCUAccountParameterItem = new HCUAccountParameter
                    //        {
                    //            Guid = HCoreHelper.GenerateGuid(),
                    //            TypeId = HelperType.MerchantCategory,
                    //            CommonId = Category.ReferenceId,
                    //            CreateDate = HCoreHelper.GetGMTDateTime(),
                    //            StatusId = HelperStatus.Default.Active,
                    //        };
                    //        _HCUAccount.HCUAccountParameterAccount.Add(_HCUAccountParameterItem);
                    //    }
                    //}

                    #region Set Categories
                    if (_Request.Business.Categories != null && _Request.Business.Categories.Count > 0)
                    {
                        _TUCCategoryAccount = new List<TUCCategoryAccount>();
                        foreach (var Category in _Request.Business.Categories)
                        {
                            _TUCCategoryAccount.Add(new TUCCategoryAccount
                            {
                                Guid = HCoreHelper.GenerateGuid(),
                                TypeId = 1,
                                CategoryId = Category.ReferenceId,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                CreatedById = _Request.UserReference.AccountId,
                                StatusId = HelperStatus.Default.Active,
                            });
                        }
                        _HCUAccount.TUCCategoryAccountAccount = _TUCCategoryAccount;
                    }
                    #endregion
                    _HCUAccount.Guid = ReferenceKey;
                    _HCUAccount.AccountTypeId = UserAccountType.Merchant;
                    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                    _HCUAccount.OwnerId = 3;
                    _HCUAccount.DisplayName = _Request.Business.Name;
                    _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                    _HCUAccount.Name = _Request.Business.Name;
                    _HCUAccount.EmailAddress = _Request.Business.EmailAddress;
                    _HCUAccount.ContactNumber = _Request.Business.MobileNumber;
                    _HCUAccount.AccountPercentage = _Request.Business.RewardPercentage;
                    //_HCUAccount.WebsiteUrl = _Request.WebsiteUrl;
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = MerchantCode;
                    _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.Business.MobileNumber);
                    _HCUAccount.Address = _Request.Business.Address;
                    _HCUAccount.ReferenceNumber = _Request.Reference;
                    //_HCUAccount.Latitude = _AddressResponse.Latitude;
                    //_HCUAccount.Longitude = _AddressResponse.Longitude;
                    //_HCUAccount.CountryId = _AddressResponse.CountryId;
                    //if (_AddressResponse.StateId != 0)
                    //{
                    //    _HCUAccount.RegionId = _AddressResponse.StateId;
                    //}
                    //if (_AddressResponse.CityId != 0)
                    //{
                    //    _HCUAccount.CityId = _AddressResponse.CityId;
                    //}
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.StatusId = HelperStatus.Default.Active;
                    _HCUAccount.CountryId = (int?)_Request.UserReference.CountryId;
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.AccountPercentage = 3;
                    _HCUAccount.User = _HCUAccountAuth;
                    #endregion
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    _HCoreContext.SaveChanges();
                    long MerchantId = _HCUAccount.Id;
                    var EmailObject = new
                    {
                        UserName = _HCUAccountAuth.Username,
                        Password = _Request.Business.Password,
                        DisplayName = _HCUAccount.DisplayName,
                    };
                    using (_HCoreContext = new HCoreContext())
                    {
                        _HCUAccountAuth = new HCUAccountAuth();
                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(10);
                        _HCUAccountAuth.Password = HCoreHelper.GenerateRandomNumber(6);
                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountAuth.CreatedById = MerchantId;
                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                        #region Save UserAccount
                        _HCUAccount = new HCUAccount();
                        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                        _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                        _HCUAccount.OwnerId = MerchantId;
                        _HCUAccount.DisplayName = _Request.Business.Name;
                        _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                        _HCUAccount.Name = _Request.Business.Name;
                        _HCUAccount.EmailAddress = _Request.Business.EmailAddress;
                        _HCUAccount.ContactNumber = _Request.Business.MobileNumber;
                        _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.Business.MobileNumber);
                        _HCUAccount.Address = _Request.Business.Address;
                        //_HCUAccount.Latitude = _AddressResponse.Latitude;
                        //_HCUAccount.Longitude = _AddressResponse.Longitude;
                        //_HCUAccount.CountryId = _AddressResponse.CountryId;
                        //if (_AddressResponse.StateId != 0)
                        //{
                        //    _HCUAccount.RegionId = _AddressResponse.StateId;
                        //}
                        //if (_AddressResponse.CityId != 0)
                        //{
                        //    _HCUAccount.CityId = _AddressResponse.CityId;
                        //}
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                        _HCUAccount.AccountCode = _Random.Next(100000000, 999999999).ToString();
                        if (_Request.UserReference.AppVersionId != 0)
                        {
                            _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                        }
                        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                        _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                        }
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                        _HCUAccount.EmailVerificationStatus = 0;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.NumberVerificationStatus = 0;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.User = _HCUAccountAuth;
                        #endregion
                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                        _HCoreContext.SaveChanges();

                    }
                    HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantWelcomeEmail, _Request.Business.Name, _Request.Business.EmailAddress, EmailObject, _Request.UserReference);
                    try
                    {
                        string data = "scope=profile&grant_type=client_credentials";
                        using (WebClient _WebClient = new WebClient())
                        {
                            string TokenUrl = "https://api.interswitchng.com/passport/oauth/token";
                            if (HostEnvironment == HostEnvironmentType.Test)
                            {
                                TokenUrl = "https://apps.qa.interswitchng.com/passport/oauth/token";
                            }
                            _WebClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                            _WebClient.Headers.Add("Authorization", "Basic SUtJQTBBQjIxRTZGRjhGMDM2NTBGQjkzQTMxNDQ2NEZGOTMwOTAxM0UzMEI6dGhhbmstdS1jYXNo");
                            string result = _WebClient.UploadString(TokenUrl, data);
                            if (!string.IsNullOrEmpty(result))
                            {
                                tokenResponse _TokenResponse = JsonConvert.DeserializeObject<tokenResponse>(result);
                                if (_TokenResponse != null)
                                {
                                    string Url = "https://webpay.interswitchng.com/collections/api/v1/tuc/configure";
                                    if (HostEnvironment == HostEnvironmentType.Test)
                                    {
                                        Url = "https://testwebpay.interswitchng.com/collections/api/v1/tuc/configure";
                                    }
                                    var client = new RestSharp.RestClient(Url);
                                    var request = new RestSharp.RestRequest();
                                    request.Method = RestSharp.Method.Post;
                                    request.AddHeader("accept", "application/json");
                                    request.AddHeader("content-type", "application/json");
                                    string Part = "{\"merchantCode\":\"" + _Request.Reference + "\",\"tucMerchantId\":\"" + MerchantCode + "\"}";
                                    //request.AddHeader("authorization", "Bearer eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiY2Flc2FyIiwiaXN3LWNvbGxlY3Rpb25zIiwiaXN3LWNvcmUiLCJpc3ctcGF5bWVudGdhdGV3YXkiLCJwYXNzcG9ydCIsInByb2plY3QteC1tZXJjaGFudCJdLCJzY29wZSI6WyJwcm9maWxlIl0sImV4cCI6MTYxODQxNTY2NSwiY2xpZW50X25hbWUiOiJUaGFuayBZb3UgY2FzaCIsImp0aSI6IjIzZGZhY2U1LTc5ZjEtNDkwNC05YTgzLTg2Y2FmNjcwODk5ZSIsImNsaWVudF9pZCI6IklLSUEwQUIyMUU2RkY4RjAzNjUwRkI5M0EzMTQ0NjRGRjkzMDkwMTNFMzBCIn0.SJ3jq6dk_GV7YgSPLaOECf5e2shldOB7DyV-4UgyUrcU7FGSuUpUMPjAb-VanqP14NgQl7aDiQBFA6QGWPxvkoWIfbenftPtpWvt6MBv60k1t6zgVkAkGaj_kphJX7DRteRia1pFJBnkeaxl61hs4vZRJuB1pTc9AJA_hxXCq1YzdAEpTXrF93JHJ0DC6LFZqtb75TFfiFPq_T3CCrX6gyecc-c1yW-0DMxEqhZa_uVOeKTUV-7oZC0EfEnJtfbDQBnyf9Bquzzg4PDX-S9M2qO8V268t4T7jfXzvSwpeiavnGU-d6WM5y1Dd6lmaF3133wif9koFIWxFYQohrxz3w");
                                    request.AddHeader("authorization", "Bearer " + _TokenResponse.access_token);
                                    request.AddParameter("application/json", Part, RestSharp.ParameterType.RequestBody);
                                    var response = client.Execute(request);
                                    //HCoreHelper.LogData(HCoreConstant.LogType.Log, "DDSD", Part, JsonConvert.SerializeObject(_Request), response.Content);
                                }
                            }
                        }
                    }
                    catch (Exception _Exception)
                    {
                        HCoreHelper.LogException("WEBPAYCON_REQ", _Exception);
                    }
                    var _Response = new
                    {
                        IsMerchantConnected = true,
                        taccessid = UserName,
                        taccesstoken = HCoreEncrypt.DecryptHash(Password)
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, TUCCoreResource.CA1063, TUCCoreResource.CA1063M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("WebPay_Confirm", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        //internal OResponse SaveMerchantWebPay(OAccounts.OCoreMerchantWebPay.Request _Request)
        //{
        //    //_MerchantResponse = new OCoreMerchant.Response
        //    //{
        //    //    Status = HCoreConstant.ResponseStatus.Error,
        //    //    StatusCode = TUCCoreResource.CA0500,
        //    //    Message = TUCCoreResource.CA0500M
        //    //};
        //    try
        //    {
        //        //if (string.IsNullOrEmpty(_Request.DisplayName))
        //        //{
        //        //    _MerchantResponse.StatusCode = TUCCoreResource.CA1126;
        //        //    _MerchantResponse.Message = TUCCoreResource.CA1126M;
        //        //    return _MerchantResponse;
        //        //}
        //        //if (string.IsNullOrEmpty(_Request.CompanyName))
        //        //{
        //        //    _MerchantResponse.StatusCode = TUCCoreResource.CA1127;
        //        //    _MerchantResponse.Message = TUCCoreResource.CA1127M;
        //        //    return _MerchantResponse;
        //        //}
        //        //if (string.IsNullOrEmpty(_Request.ContactNumber))
        //        //{
        //        //    _MerchantResponse.StatusCode = TUCCoreResource.CA1128;
        //        //    _MerchantResponse.Message = TUCCoreResource.CA1128M;
        //        //    return _MerchantResponse;
        //        //}
        //        //if (string.IsNullOrEmpty(_Request.EmailAddress))
        //        //{
        //        //    _MerchantResponse.StatusCode = TUCCoreResource.CA1129;
        //        //    _MerchantResponse.Message = TUCCoreResource.CA1129M;
        //        //    return _MerchantResponse;
        //        //}
        //        //if (_Request.Address == null)
        //        //{
        //        //    _MerchantResponse.StatusCode = TUCCoreResource.CA1130;
        //        //    _MerchantResponse.Message = TUCCoreResource.CA1130M;
        //        //    return _MerchantResponse;
        //        //}
        //        //if (_Request.Address.Latitude == 0 && _Request.Address.Longitude == 0)
        //        //{
        //        //    _MerchantResponse.StatusCode = TUCCoreResource.CA1130;
        //        //    _MerchantResponse.Message = TUCCoreResource.CA1130M;
        //        //    return _MerchantResponse;
        //        //}
        //        long StatusId = HelperStatus.Default.Active;
        //        //if (!string.IsNullOrEmpty(_Request.StatusCode))
        //        //{
        //        //    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
        //        //    if (StatusId == 0)
        //        //    {
        //        //        _MerchantResponse.StatusCode = TUCCoreResource.CAINSTATUS;
        //        //        _MerchantResponse.Message = TUCCoreResource.CAINSTATUSM;
        //        //        return _MerchantResponse;
        //        //    }
        //        //}
        //        //OAddressResponse _AddressResponse = HCoreHelper.GetAddressIds(_Request.Address, _Request.UserReference);
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
        //            && x.Name == _Request.Business.Name && x.EmailAddress == _Request.Business.EmailAddress)
        //               .Select(x => new
        //               {
        //                   ReferenceId = x.Id,
        //                   ReferenceKey = x.Guid,
        //                  Name = x.Name,
        //               }).FirstOrDefault();
        //            if (MerchantDetails != null)
        //            {

        //            }
        //            else
        //            {

        //            }
        //            //long AccountOperationType = _HCoreContext.hc
        //            if (string.IsNullOrEmpty(_Request.UserName))
        //            {
        //                _Request.UserName = _Request.EmailAddress;
        //            }
        //            bool ValidateUserName = _HCoreContext.HCUAccountAuth.Any(x => x.Username == _Request.UserName);
        //            if (ValidateUserName)
        //            {
        //                _MerchantResponse.StatusCode = TUCCoreResource.CA1131;
        //                _MerchantResponse.Message = TUCCoreResource.CA1131M;
        //                return _MerchantResponse;
        //            }
        //            bool ValidateCompanyName = _HCoreContext.HCUAccount.Any(x => x.Name == _Request.CompanyName && x.AccountTypeId == UserAccountType.Merchant);
        //            if (ValidateCompanyName)
        //            {
        //                _MerchantResponse.StatusCode = TUCCoreResource.CA1132;
        //                _MerchantResponse.Message = TUCCoreResource.CA1132M;
        //                return _MerchantResponse;
        //            }
        //            if (_Request.Configurations == null)
        //            {
        //                _MerchantConfiguration = new OCoreMerchant.Configuration();
        //                _MerchantConfiguration.RewardDeductionTypeCode = "rewarddeductiontype.prepay";
        //                _MerchantConfiguration.RewardPercentage = 0;
        //                _Request.Configurations = _MerchantConfiguration;
        //            }
        //            else
        //            {
        //                if (string.IsNullOrEmpty(_Request.Configurations.RewardDeductionTypeCode))
        //                {
        //                    _Request.Configurations.RewardDeductionTypeCode = "rewarddeductiontype.prepay";
        //                }

        //            }
        //            if (string.IsNullOrEmpty(_Request.AccountOperationTypeCode))
        //            {
        //                _Request.AccountOperationTypeCode = "accountoperationtype.onlineandoffline";
        //            }
        //            if (_Request.AccountOperationTypeCode != "accountoperationtype.online"
        //                && _Request.AccountOperationTypeCode != "accountoperationtype.offline"
        //                && _Request.AccountOperationTypeCode != "accountoperationtype.onlineandoffline")
        //            {
        //                _MerchantResponse.StatusCode = TUCCoreResource.CA1133;
        //                _MerchantResponse.Message = TUCCoreResource.CA1133M;
        //                return _MerchantResponse;
        //            }

        //            _Random = new Random();
        //            string MerchantKey = HCoreHelper.GenerateGuid();
        //            string AccountCode = _Random.Next(100, 999).ToString() + _Random.Next(000000000, 999999999).ToString();
        //            #region Parameters
        //            _HCUAccountParameters = new List<HCUAccountParameter>();
        //            #region Reward Percentage
        //            if (_Request.Configurations.RewardPercentage > 0)
        //            {
        //                _HCUAccountParameter = new HCUAccountParameter();
        //                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
        //                _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
        //                _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage").Select(x => x.Id).FirstOrDefault();
        //                _HCUAccountParameter.Value = _Request.Configurations.RewardPercentage.ToString();
        //                _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
        //                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
        //                if (_Request.UserReference.AccountId != 0)
        //                {
        //                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
        //                }
        //                _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
        //                _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
        //                _HCUAccountParameters.Add(_HCUAccountParameter);
        //            }
        //            #endregion
        //            #region Account Reward Deduction Type
        //            _HCUAccountParameter = new HCUAccountParameter();
        //            _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
        //            _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
        //            _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewarddeductiontype").Select(x => x.Id).FirstOrDefault();
        //            _HCUAccountParameter.Value = "Prepay";
        //            _HCUAccountParameter.HelperId = _HCoreContext.HCCoreHelper.Where(x => x.SystemName == _Request.AccountOperationTypeCode).Select(x => x.Id).FirstOrDefault();
        //            _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
        //            _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
        //            if (_Request.UserReference.AccountId != 0)
        //            {
        //                _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
        //            }
        //            _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
        //            _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
        //            _HCUAccountParameters.Add(_HCUAccountParameter);
        //            #endregion
        //            #region Set Categories
        //            if (_Request.Categories != null && _Request.Categories.Count > 0)
        //            {
        //                foreach (var Category in _Request.Categories)
        //                {
        //                    _HCUAccountParameters.Add(new HCUAccountParameter
        //                    {
        //                        Guid = HCoreHelper.GenerateGuid(),
        //                        TypeId = HelperType.MerchantCategory,
        //                        CommonId = Category.ReferenceId,
        //                        CreateDate = HCoreHelper.GetGMTDateTime(),
        //                        StatusId = HelperStatus.Default.Active,
        //                    });
        //                }
        //            }
        //            #endregion
        //            #endregion
        //            #region User Object
        //            _HCUAccountAuth = new HCUAccountAuth();
        //            _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
        //            if (!string.IsNullOrEmpty(_Request.UserName))
        //            {
        //                _HCUAccountAuth.Username = _Request.UserName;
        //            }
        //            else
        //            {
        //                _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(6);
        //            }
        //            if (!string.IsNullOrEmpty(_Request.Password))
        //            {
        //                _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.Password);
        //            }
        //            else
        //            {
        //                _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
        //            }
        //            _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
        //            _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
        //            _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
        //            if (_Request.UserReference.AccountId != 0)
        //            {
        //                _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
        //            }
        //            _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
        //            #endregion
        //            #region Save UserAccount
        //            _HCUAccount = new HCUAccount();
        //            _HCUAccount.Guid = MerchantKey;
        //            _HCUAccount.AccountTypeId = UserAccountType.Merchant;
        //            long AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
        //            if (_Request.AccountOperationTypeCode == "accountoperationtype.online")
        //            {
        //                AccountOperationTypeId = AccountOperationType.Online;
        //            }
        //            if (_Request.AccountOperationTypeCode == "accountoperationtype.offline")
        //            {
        //                AccountOperationTypeId = AccountOperationType.Offline;
        //            }
        //            if (_Request.AccountOperationTypeCode == "accountoperationtype.onlineandoffline")
        //            {
        //                AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
        //            }
        //            _HCUAccount.AccountOperationTypeId = AccountOperationTypeId;
        //            if (_Request.OwnerId != 0)
        //            {
        //                _HCUAccount.OwnerId = _Request.OwnerId;
        //            }
        //            _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
        //            _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
        //            //_HCUAccount.BankId = _Request.AccountId;
        //            _HCUAccount.DisplayName = _Request.DisplayName;
        //            _HCUAccount.Name = _Request.CompanyName;
        //            _HCUAccount.EmailAddress = _Request.EmailAddress;
        //            _HCUAccount.MobileNumber = _Request.MobileNumber;
        //            _HCUAccount.ContactNumber = _Request.ContactNumber;
        //            if (_Request.ContactPerson != null)
        //            {
        //                _HCUAccount.CpName = _Request.ContactPerson.FirstName + " " + _Request.ContactPerson.LastName;
        //                _HCUAccount.CpFirstName = _Request.ContactPerson.FirstName;
        //                _HCUAccount.CpLastName = _Request.ContactPerson.LastName;
        //                _HCUAccount.CpMobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber);
        //                _HCUAccount.CpEmailAddress = _Request.ContactPerson.EmailAddress;

        //                _HCUAccount.FirstName = _Request.ContactPerson.FirstName;
        //                _HCUAccount.LastName = _Request.ContactPerson.LastName;
        //                _HCUAccount.SecondaryEmailAddress = _Request.ContactPerson.EmailAddress;
        //                _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber);
        //            }
        //            if (_Request.StartDate != null)
        //            {
        //                _HCUAccount.DateOfBirth = _Request.StartDate;
        //            }

        //            if (_AddressResponse != null)
        //            {
        //                _HCUAccount.Address = _AddressResponse.Address;
        //                _HCUAccount.Latitude = _AddressResponse.Latitude;
        //                _HCUAccount.Longitude = _AddressResponse.Longitude;
        //                _HCUAccount.CountryId = _AddressResponse.CountryId;
        //                if (_AddressResponse.StateId != 0)
        //                {
        //                    _HCUAccount.RegionId = _AddressResponse.StateId;
        //                }
        //                if (_AddressResponse.CityId != 0)
        //                {
        //                    _HCUAccount.CityId = _AddressResponse.CityId;
        //                }
        //                if (_AddressResponse.CityAreaId != 0)
        //                {
        //                    _HCUAccount.CityAreaId = _AddressResponse.CityAreaId;
        //                }
        //            }
        //            _HCUAccount.WebsiteUrl = _Request.WebsiteUrl;
        //            _HCUAccount.Description = _Request.Description;
        //            _HCUAccount.EmailVerificationStatus = 0;
        //            _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
        //            _HCUAccount.NumberVerificationStatus = 0;
        //            _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
        //            _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
        //            if (_Request.UserReference.AppVersionId != 0)
        //            {
        //                _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
        //            }
        //            _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
        //            _HCUAccount.CountValue = 0;
        //            _HCUAccount.AverageValue = 0;
        //            _HCUAccount.Credit = 0;
        //            _HCUAccount.Debit = 0;
        //            _HCUAccount.MainBalance = 0;
        //            _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
        //            if (_Request.Configurations.RewardPercentage > 0)
        //            {
        //                _HCUAccount.AccountPercentage = _Request.Configurations.RewardPercentage;
        //            }
        //            if (_Request.UserReference.AccountId != 0)
        //            {
        //                _HCUAccount.CreatedById = _Request.UserReference.AccountId;
        //            }
        //            _HCUAccount.StatusId = StatusId;
        //            _HCUAccount.User = _HCUAccountAuth;
        //            _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
        //            #endregion
        //            _HCoreContext.HCUAccount.Add(_HCUAccount);
        //            _HCoreContext.SaveChanges();
        //            long MerchantId = _HCUAccount.Id;
        //            if (_Request.Stores != null && _Request.Stores.Count == 0)
        //            {
        //                using (_HCoreContext = new HCoreContext())
        //                {
        //                    #region Save Store
        //                    _Random = new Random();
        //                    string StoreAccountCode = _Random.Next(100000000, 999999999).ToString();
        //                    _HCUAccountAuth = new HCUAccountAuth();
        //                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
        //                    _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(6);
        //                    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
        //                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
        //                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
        //                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
        //                    if (_Request.UserReference.AccountId != 0)
        //                    {
        //                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
        //                    }
        //                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;

        //                    _HCUAccount = new HCUAccount();
        //                    _HCUAccount.User = _HCUAccountAuth;
        //                    if (_Request.Categories != null && _Request.Categories.Count > 0)
        //                    {
        //                        _HCUAccountParameters = new List<HCUAccountParameter>();
        //                        foreach (var Category in _Request.Categories)
        //                        {
        //                            _HCUAccountParameters.Add(new HCUAccountParameter
        //                            {
        //                                Guid = HCoreHelper.GenerateGuid(),
        //                                TypeId = HelperType.MerchantCategory,
        //                                CommonId = Category.ReferenceId,
        //                                CreateDate = HCoreHelper.GetGMTDateTime(),
        //                                StatusId = HelperStatus.Default.Active,
        //                            });
        //                        }
        //                        _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
        //                    }
        //                    _HCUAccount.Guid = HCoreHelper.GenerateGuid();
        //                    _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
        //                    _HCUAccount.AccountOperationTypeId = AccountOperationTypeId;
        //                    _HCUAccount.OwnerId = MerchantId;
        //                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
        //                    _HCUAccount.AccountCode = StoreAccountCode;
        //                    if (_Request.DisplayName.Length > 30)
        //                    {
        //                        _HCUAccount.DisplayName = _Request.DisplayName.Substring(0, 29);
        //                    }
        //                    else
        //                    {
        //                        _HCUAccount.DisplayName = _Request.DisplayName;
        //                    }
        //                    _HCUAccount.Name = _Request.CompanyName;
        //                    _HCUAccount.ContactNumber = _Request.ContactNumber;
        //                    _HCUAccount.EmailAddress = _Request.EmailAddress;
        //                    if (_Request.ContactPerson != null)
        //                    {
        //                        if (!string.IsNullOrEmpty(_Request.ContactPerson.FirstName))
        //                        {
        //                            _HCUAccount.FirstName = _Request.ContactPerson.FirstName;
        //                            _HCUAccount.CpFirstName = _Request.ContactPerson.FirstName;
        //                        }
        //                        if (!string.IsNullOrEmpty(_Request.ContactPerson.LastName))
        //                        {
        //                            _HCUAccount.LastName = _Request.ContactPerson.LastName;
        //                            _HCUAccount.CpLastName = _Request.ContactPerson.LastName;
        //                        }
        //                        _HCUAccount.CpName = _Request.ContactPerson.FirstName + " " + _Request.ContactPerson.LastName;
        //                        if (!string.IsNullOrEmpty(_Request.ContactPerson.MobileNumber))
        //                        {
        //                            _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber);
        //                            _HCUAccount.CpMobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber);
        //                        }
        //                        if (!string.IsNullOrEmpty(_Request.ContactPerson.EmailAddress))
        //                        {
        //                            _HCUAccount.SecondaryEmailAddress = _Request.ContactPerson.EmailAddress;
        //                            _HCUAccount.CpEmailAddress = _Request.ContactPerson.EmailAddress;
        //                        }
        //                    }
        //                    if (_Request.StartDate != null)
        //                    {
        //                        _HCUAccount.DateOfBirth = _Request.StartDate;
        //                    }
        //                    _HCUAccount.WebsiteUrl = _Request.WebsiteUrl;
        //                    _HCUAccount.Description = _Request.Description;
        //                    if (_AddressResponse != null)
        //                    {
        //                        if (!string.IsNullOrEmpty(_AddressResponse.Address))
        //                        {
        //                            _HCUAccount.Address = _AddressResponse.Address;
        //                        }
        //                        if (_AddressResponse.CityAreaId != 0)
        //                        {
        //                            _HCUAccount.CityAreaId = _AddressResponse.CityAreaId;
        //                        }
        //                        if (_AddressResponse.CityId != 0)
        //                        {
        //                            _HCUAccount.CityId = _AddressResponse.CityId;
        //                        }
        //                        if (_AddressResponse.StateId != 0)
        //                        {
        //                            _HCUAccount.RegionId = _AddressResponse.StateId;
        //                        }
        //                        if (_AddressResponse.CountryId != 0)
        //                        {
        //                            _HCUAccount.CountryId = _AddressResponse.CountryId;
        //                        }
        //                        if (_AddressResponse.Latitude != 0)
        //                        {
        //                            _HCUAccount.Latitude = _AddressResponse.Latitude;
        //                        }
        //                        if (_AddressResponse.Longitude != 0)
        //                        {
        //                            _HCUAccount.Longitude = _AddressResponse.Longitude;
        //                        }
        //                    }
        //                    _HCUAccount.EmailVerificationStatus = 0;
        //                    _HCUAccount.NumberVerificationStatus = 0;
        //                    _HCUAccount.RegistrationSourceId = RegistrationSource.System;
        //                    _HCUAccount.CountValue = 0;
        //                    _HCUAccount.AverageValue = 0;
        //                    _HCUAccount.Credit = 0;
        //                    _HCUAccount.Debit = 0;
        //                    _HCUAccount.MainBalance = 0;
        //                    if (_Request.UserReference.AppVersionId != 0)
        //                    {
        //                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
        //                    }
        //                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
        //                    if (_Request.UserReference.AccountId != 0)
        //                    {
        //                        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
        //                    }
        //                    _HCUAccount.StatusId = HelperStatus.Default.Inactive;
        //                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
        //                    _HCoreContext.HCUAccount.Add(_HCUAccount);
        //                    _HCoreContext.SaveChanges();
        //                    long StoreId = _HCUAccount.Id;
        //                    #endregion
        //                    using (_HCoreContext = new HCoreContext())
        //                    {
        //                        #region User
        //                        _HCUAccountAuth = new HCUAccountAuth();
        //                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
        //                        _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(6);
        //                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
        //                        _HCUAccountAuth.SecondaryPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
        //                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
        //                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
        //                        if (_Request.UserReference.AccountId != 0)
        //                        {
        //                            _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
        //                        }
        //                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
        //                        #endregion
        //                        #region Save Cashier Account
        //                        _Random = new Random();
        //                        string CashierAccountCode = _Random.Next(1000, 9999).ToString() + _Random.Next(000000000, 999999999).ToString();
        //                        _HCUAccount = new HCUAccount();
        //                        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
        //                        _HCUAccount.AccountTypeId = UserAccountType.MerchantCashier;
        //                        _HCUAccount.AccountOperationTypeId = Helpers.AccountOperationType.Offline;
        //                        _HCUAccount.OwnerId = MerchantId;
        //                        _HCUAccount.SubOwnerId = StoreId;
        //                        _HCUAccount.DisplayName = "0001";
        //                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
        //                        _HCUAccount.AccountCode = CashierAccountCode;
        //                        _HCUAccount.GenderId = Gender.Male;
        //                        if (_Request.UserReference.AppVersionId != 0)
        //                        {
        //                            _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
        //                        }
        //                        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
        //                        if (_Request.UserReference.AccountId != 0)
        //                        {
        //                            _HCUAccount.CreatedById = _Request.UserReference.AccountId;
        //                        }
        //                        _HCUAccount.Name = "0001";
        //                        _HCUAccount.EmailVerificationStatus = 0;
        //                        _HCUAccount.NumberVerificationStatus = 0;
        //                        _HCUAccount.RegistrationSourceId = RegistrationSource.System;
        //                        _HCUAccount.StatusId = HelperStatus.Default.Active;
        //                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
        //                        _HCUAccount.User = _HCUAccountAuth;
        //                        _HCoreContext.HCUAccount.Add(_HCUAccount);
        //                        _HCoreContext.SaveChanges();
        //                        #endregion
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                foreach (var _Store in _Request.Stores)
        //                {
        //                    OAddressResponse _StoreAddress = HCoreHelper.GetAddressIds(_Store.Address, _Request.UserReference);
        //                    using (_HCoreContext = new HCoreContext())
        //                    {
        //                        #region Save Store
        //                        _Random = new Random();
        //                        string StoreAccountCode = _Random.Next(100000000, 999999999).ToString();
        //                        _HCUAccountAuth = new HCUAccountAuth();
        //                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
        //                        _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(6);
        //                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
        //                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
        //                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
        //                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
        //                        if (_Request.UserReference.AccountId != 0)
        //                        {
        //                            _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
        //                        }
        //                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;

        //                        _HCUAccount = new HCUAccount();
        //                        _HCUAccount.User = _HCUAccountAuth;
        //                        if (_Store.Categories != null && _Store.Categories.Count > 0)
        //                        {
        //                            _HCUAccountParameters = new List<HCUAccountParameter>();
        //                            foreach (var Category in _Store.Categories)
        //                            {
        //                                _HCUAccountParameters.Add(new HCUAccountParameter
        //                                {
        //                                    Guid = HCoreHelper.GenerateGuid(),
        //                                    TypeId = HelperType.MerchantCategory,
        //                                    CommonId = Category.ReferenceId,
        //                                    CreateDate = HCoreHelper.GetGMTDateTime(),
        //                                    StatusId = HelperStatus.Default.Active,
        //                                });
        //                            }
        //                            _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
        //                        }
        //                        else
        //                        {
        //                            if (_Request.Categories != null && _Request.Categories.Count > 0)
        //                            {
        //                                _HCUAccountParameters = new List<HCUAccountParameter>();
        //                                foreach (var Category in _Request.Categories)
        //                                {
        //                                    _HCUAccountParameters.Add(new HCUAccountParameter
        //                                    {
        //                                        Guid = HCoreHelper.GenerateGuid(),
        //                                        TypeId = HelperType.MerchantCategory,
        //                                        CommonId = Category.ReferenceId,
        //                                        CreateDate = HCoreHelper.GetGMTDateTime(),
        //                                        StatusId = HelperStatus.Default.Active,
        //                                    });
        //                                }
        //                                _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
        //                            }
        //                        }
        //                        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
        //                        _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
        //                        _HCUAccount.AccountOperationTypeId = Helpers.AccountOperationType.OnlineAndOffline;
        //                        _HCUAccount.AccountOperationTypeId = AccountOperationTypeId;
        //                        //if (string.IsNullOrEmpty(_Store.AccountOperationTypeCode))
        //                        //{
        //                        //    _Store.AccountOperationTypeCode = _Request.AccountOperationTypeCode;
        //                        //}
        //                        //if (_Store.AccountOperationTypeCode == "accountoperationtype.online")
        //                        //{
        //                        //    _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
        //                        //}
        //                        //if (_Store.AccountOperationTypeCode == "accountoperationtype.offline")
        //                        //{
        //                        //    _HCUAccount.AccountOperationTypeId = AccountOperationType.Offline;
        //                        //}
        //                        //if (_Store.AccountOperationTypeCode == "accountoperationtype.onlineandoffline")
        //                        //{
        //                        //    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
        //                        //}
        //                        _HCUAccount.OwnerId = MerchantId;
        //                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
        //                        _HCUAccount.AccountCode = StoreAccountCode;
        //                        if (_Store.DisplayName.Length > 30)
        //                        {
        //                            _HCUAccount.DisplayName = _Store.DisplayName.Substring(0, 29);
        //                        }
        //                        else
        //                        {
        //                            _HCUAccount.DisplayName = _Store.DisplayName;
        //                        }
        //                        _HCUAccount.Name = _Store.Name;
        //                        _HCUAccount.ContactNumber = _Store.ContactNumber;
        //                        _HCUAccount.EmailAddress = _Store.EmailAddress;
        //                        if (_Store.ContactPerson != null)
        //                        {
        //                            if (!string.IsNullOrEmpty(_Store.ContactPerson.FirstName))
        //                            {
        //                                _HCUAccount.FirstName = _Store.ContactPerson.FirstName;
        //                                _HCUAccount.CpFirstName = _Store.ContactPerson.FirstName;
        //                            }
        //                            if (!string.IsNullOrEmpty(_Store.ContactPerson.LastName))
        //                            {
        //                                _HCUAccount.LastName = _Store.ContactPerson.LastName;
        //                                _HCUAccount.CpLastName = _Store.ContactPerson.LastName;
        //                            }
        //                            _HCUAccount.CpName = _Store.ContactPerson.FirstName + " " + _Store.ContactPerson.LastName;
        //                            if (!string.IsNullOrEmpty(_Store.ContactPerson.MobileNumber))
        //                            {
        //                                _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Store.ContactPerson.MobileNumber);
        //                                _HCUAccount.CpMobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Store.ContactPerson.MobileNumber);
        //                            }
        //                            if (!string.IsNullOrEmpty(_Store.ContactPerson.EmailAddress))
        //                            {
        //                                _HCUAccount.SecondaryEmailAddress = _Store.ContactPerson.EmailAddress;
        //                                _HCUAccount.CpEmailAddress = _Store.ContactPerson.EmailAddress;
        //                            }
        //                        }
        //                        if (_Store.StartDate != null)
        //                        {
        //                            _HCUAccount.DateOfBirth = _Store.StartDate;
        //                        }
        //                        _HCUAccount.WebsiteUrl = _Store.WebsiteUrl;
        //                        _HCUAccount.Description = _Store.Description;
        //                        if (_StoreAddress != null)
        //                        {
        //                            if (!string.IsNullOrEmpty(_StoreAddress.Address))
        //                            {
        //                                _HCUAccount.Address = _StoreAddress.Address;
        //                            }
        //                            if (_StoreAddress.CityAreaId != 0)
        //                            {
        //                                _HCUAccount.CityAreaId = _StoreAddress.CityAreaId;
        //                            }
        //                            if (_StoreAddress.CityId != 0)
        //                            {
        //                                _HCUAccount.CityId = _StoreAddress.CityId;
        //                            }
        //                            if (_StoreAddress.StateId != 0)
        //                            {
        //                                _HCUAccount.RegionId = _StoreAddress.StateId;
        //                            }
        //                            if (_StoreAddress.CountryId != 0)
        //                            {
        //                                _HCUAccount.CountryId = _StoreAddress.CountryId;
        //                            }
        //                            if (_StoreAddress.Latitude != 0)
        //                            {
        //                                _HCUAccount.Latitude = _StoreAddress.Latitude;
        //                            }
        //                            if (_StoreAddress.Longitude != 0)
        //                            {
        //                                _HCUAccount.Longitude = _StoreAddress.Longitude;
        //                            }
        //                        }
        //                        _HCUAccount.EmailVerificationStatus = 0;
        //                        _HCUAccount.NumberVerificationStatus = 0;
        //                        _HCUAccount.RegistrationSourceId = RegistrationSource.System;
        //                        _HCUAccount.CountValue = 0;
        //                        _HCUAccount.AverageValue = 0;
        //                        _HCUAccount.Credit = 0;
        //                        _HCUAccount.Debit = 0;
        //                        _HCUAccount.MainBalance = 0;
        //                        if (_Request.UserReference.AppVersionId != 0)
        //                        {
        //                            _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
        //                        }
        //                        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
        //                        if (_Request.UserReference.AccountId != 0)
        //                        {
        //                            _HCUAccount.CreatedById = _Request.UserReference.AccountId;
        //                        }
        //                        _HCUAccount.StatusId = HelperStatus.Default.Inactive;
        //                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
        //                        _HCoreContext.HCUAccount.Add(_HCUAccount);
        //                        _HCoreContext.SaveChanges();
        //                        long StoreId = _HCUAccount.Id;
        //                        #endregion
        //                        using (_HCoreContext = new HCoreContext())
        //                        {
        //                            _TUCBranchAccount = new TUCBranchAccount();
        //                            _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
        //                            _TUCBranchAccount.BranchId = _Request.BranchId;
        //                            _TUCBranchAccount.OwnerId = _HCoreContext.TUCBranchAccount.Where(x => x.Id == _Request.RmId).Select(x => x.AccountId).FirstOrDefault();
        //                            _TUCBranchAccount.AccountId = StoreId;
        //                            _TUCBranchAccount.AccountLevelId = 9;
        //                            _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
        //                            _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
        //                            _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
        //                            _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
        //                            _HCoreContext.TUCBranchAccount.Add(_TUCBranchAccount);
        //                            _HCoreContext.SaveChanges();
        //                        }
        //                        if (_Store.Terminals != null && _Store.Terminals.Count > 0)
        //                        {
        //                            foreach (var _Terminal in _Store.Terminals)
        //                            {
        //                                using (_HCoreContext = new HCoreContext())
        //                                {
        //                                    bool TerminalIdCheck = _HCoreContext.TUCTerminal.Any(x => x.IdentificationNumber == _Terminal.TerminalId);
        //                                    if (!TerminalIdCheck)
        //                                    {
        //                                        //_HCUAccountAuth = new HCUAccountAuth();
        //                                        //_HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
        //                                        //_HCUAccountAuth.Username = _Terminal.TerminalId;
        //                                        //_HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Terminal.TerminalId);
        //                                        //_HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
        //                                        //_HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
        //                                        //_HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                        //if (_Request.UserReference.AccountId != 0)
        //                                        //{
        //                                        //    _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
        //                                        //}
        //                                        //_HCUAccountAuth.StatusId = HelperStatus.Default.Active;
        //                                        //#region Save UserAccount
        //                                        //_HCUAccount = new HCUAccount();
        //                                        //_HCUAccount.Guid = HCoreHelper.GenerateGuid();
        //                                        //_HCUAccount.AccountTypeId = UserAccountType.TerminalAccount;
        //                                        //_HCUAccount.AccountOperationTypeId = AccountOperationType.Offline;
        //                                        //_HCUAccount.OwnerId = _Terminal.ProviderId;
        //                                        //_HCUAccount.BankId = _Terminal.AcquirerId;
        //                                        //_HCUAccount.SubOwnerId = StoreId;
        //                                        //_HCUAccount.DisplayName = _Terminal.TerminalId;
        //                                        //_HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
        //                                        //_HCUAccount.AccountCode = _Random.Next(100000, 999999).ToString() + _Random.Next(000000000, 999999999).ToString();
        //                                        //if (_Request.UserReference.AppVersionId != 0)
        //                                        //{
        //                                        //    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
        //                                        //}
        //                                        //_HCUAccount.RequestKey = _Request.UserReference.RequestKey;
        //                                        //_HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
        //                                        //_HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                        //if (_Request.UserReference.AccountId != 0)
        //                                        //{
        //                                        //    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
        //                                        //}
        //                                        //_HCUAccount.StatusId = HelperStatus.Default.Active;
        //                                        //_HCUAccount.Name = _Terminal.TerminalId;
        //                                        //_HCUAccount.CountryId = _Request.UserReference.CountryId;
        //                                        //_HCUAccount.EmailVerificationStatus = 0;
        //                                        //_HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
        //                                        //_HCUAccount.NumberVerificationStatus = 0;
        //                                        //_HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
        //                                        //_HCUAccount.User = _HCUAccountAuth;
        //                                        //#endregion
        //                                        _TUCTerminal = new TUCTerminal();
        //                                        _TUCTerminal.Guid = HCoreHelper.GenerateGuid();
        //                                        _TUCTerminal.IdentificationNumber = _Terminal.TerminalId;
        //                                        _TUCTerminal.SerialNumber = _Terminal.SerialNumber;
        //                                        //_TUCTerminal.Account = _HCUAccount;
        //                                        _TUCTerminal.MerchantId = MerchantId;
        //                                        _TUCTerminal.StoreId = StoreId;
        //                                        _TUCTerminal.ProviderId = _Terminal.ProviderId;
        //                                        _TUCTerminal.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                        _TUCTerminal.AcquirerId = _Terminal.AcquirerId;
        //                                        _HCoreContext.TUCTerminal.Add(_TUCTerminal);
        //                                        _HCoreContext.SaveChanges();
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //            HCore.TUC.Core.Framework.Operations.FrameworkSubscription _FrameworkSubscription = new Framework.Operations.FrameworkSubscription();
        //            _FrameworkSubscription.AddAccountFreeSubscription(MerchantId, UserAccountType.Merchant);

        //            if (!string.IsNullOrEmpty(_Request.EmailAddress))
        //            {
        //                using (_HCoreContext = new HCoreContext())
        //                {
        //                    var MerchantDetails = _HCoreContext.HCUAccount
        //                            .Where(x => x.Id == MerchantId)
        //                            .Select(x => new
        //                            {
        //                                UserName = x.User.Username,
        //                                Pasword = x.User.Password,
        //                                Name = x.Name,
        //                                DisplayName = x.DisplayName,
        //                                EmailAddress = x.EmailAddress,
        //                                MobileNumber = x.MobileNumber,
        //                            }).FirstOrDefault();
        //                    if (MerchantDetails != null)
        //                    {
        //                        try
        //                        {
        //                            PipedriveClient client = new PipedriveClient(new ProductHeaderValue("0001"), new Uri("https://thankucashsales.pipedrive.com"))
        //                            {
        //                                Credentials = new Credentials("b4cda3bb82c8af5b1ef71f9c1152fb155b27f81f", AuthenticationType.ApiToken),
        //                            };

        //                            NewActivity _NewActivity = new NewActivity("New Merchant Created " + MerchantDetails.Name, "Merchant");
        //                            _NewActivity.Subject = "New Merchant Registration : " + MerchantDetails.Name;
        //                            _NewActivity.Done = 0;
        //                            _NewActivity.Type = "call";
        //                            _NewActivity.OrgId = 1;
        //                            _NewActivity.Note = "Merchant Request <br> Name:" + MerchantDetails.Name + "<br>Contact Number : " + MerchantDetails.MobileNumber + "<br>Email Address : " + MerchantDetails.EmailAddress;
        //                            client.Activity.Create(_NewActivity);
        //                        }
        //                        catch (Exception _Exception)
        //                        {
        //                            HCoreHelper.LogException("PIPEDRIVE", _Exception);
        //                        }

        //                        var _EmailParameters = new
        //                        {
        //                            UserDisplayName = MerchantDetails.DisplayName,
        //                            UserName = MerchantDetails.UserName,
        //                            Password = HCoreEncrypt.DecryptHash(MerchantDetails.Pasword),
        //                            Name = MerchantDetails.Name,
        //                        };
        //                        HCoreHelper.BroadCastEmail("d-8a7bc7cd93d84cd7ac25684a3e851aea", MerchantDetails.DisplayName, MerchantDetails.EmailAddress, _EmailParameters, _Request.UserReference);
        //                    }
        //                }
        //            }
        //            _MerchantResponse.Status = HCoreConstant.ResponseStatus.Success;
        //            _MerchantResponse.StatusCode = TUCCoreResource.CA1134;
        //            _MerchantResponse.Message = TUCCoreResource.CA1134M;
        //            _MerchantResponse.ReferenceId = MerchantId;
        //            _MerchantResponse.ReferenceKey = MerchantKey;
        //            return _MerchantResponse;
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException("Core-SaveMerchant", _Exception, _Request.UserReference);
        //        return _MerchantResponse;
        //    }
        //}

        /// <summary>
        /// Description: Onboard merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse OnboardMerchantV4(OAccounts.MerchantOnboardingV4.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.Account == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCX4000", "Account information required");
                }
                else if (_Request.Owner == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCX4000", "Owner information required");
                }
                else if (_Request.Business == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCX4000", "Business information required");
                }
                else if (_Request.ContactPerson == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCX4000", "Contact person information required");
                }
                else if (_Request.Business.AddressComponent == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCX4000", "Address required");
                }
                //if (string.IsNullOrEmpty(_Request.Business.DisplayName))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1064, TUCCoreResource.CA1064M);
                //}
                //if (string.IsNullOrEmpty(_Request.Name))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1065, TUCCoreResource.CA1065M);
                //}
                //if (string.IsNullOrEmpty(_Request.MobileNumber))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1066, TUCCoreResource.CA1066M);
                //}
                //if (string.IsNullOrEmpty(_Request.EmailAddress))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1067, TUCCoreResource.CA1067M);
                //}
                //if (_Request.Latitude == 0 && _Request.Longitude == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1059, TUCCoreResource.CA1059M);
                //}
                //if (_Request.BranchId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1060, TUCCoreResource.CA1060M);
                //}
                //if (_Request.RmId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1061, TUCCoreResource.CA1061M);
                //}
                //if (string.IsNullOrEmpty(_Request.StatusCode))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                //}
                //long StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                //if (StatusId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                //}
                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.Business.AddressComponent, _Request.UserReference);
                using (_HCoreContext = new HCoreContext())
                {
                    _Request.Account.UserName = _Request.Account.UserName.Trim();
                    _Request.Account.Password = _Request.Account.Password.Trim();
                    string CountryIsd = _HCoreContext.HCCoreCountry.Where(x => x.Id == _AddressResponse.CountryId).Select(x => x.Isd).FirstOrDefault();
                    string MobileNumber = HCoreHelper.FormatMobileNumber(CountryIsd, _Request.Owner.MobileNumber);

                    var UserNameCheck = _HCoreContext.HCUAccountAuth.Any(x => x.Username == _Request.Business.EmailAddress);
                    if (UserNameCheck)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1097, TUCCoreResource.CA1097M);
                    }
                    if (!string.IsNullOrEmpty(_Request.Owner.MobileNumber))
                    {
                        var UserMobileNumberCheck = _HCoreContext.HCUAccount.Any(x => x.MobileNumber == MobileNumber && x.AccountTypeId == UserAccountType.Merchant);
                        if (UserMobileNumberCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1314, TUCCoreResource.CA1314M);
                        }
                    }
                    if (!string.IsNullOrEmpty(_Request.Business.EmailAddress))
                    {
                        var UserEmailAddressCheck = _HCoreContext.HCUAccount.Any(x => x.EmailAddress == _Request.Business.EmailAddress && x.AccountTypeId == UserAccountType.Merchant);
                        if (UserEmailAddressCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1315, TUCCoreResource.CA1315M);
                        }
                    }

                    //_Random = new Random();
                    //string AccountCode = _Random.Next(100, 999).ToString() + _Random.Next(000000000, 999999999).ToString();
                    string ReferenceKey = HCoreHelper.GenerateGuid();

                    HCUAccountParameter _HCUAccountParameter;
                    _HCUAccountParameters = new List<HCUAccountParameter>();
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                    _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewarddeductiontype").Select(x => x.Id).FirstOrDefault();
                    _HCUAccountParameter.Value = "Prepay";
                    _HCUAccountParameter.HelperId = 253;
                    _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                    _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccountParameters.Add(_HCUAccountParameter);

                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = _Request.Account.UserName.Trim();
                    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.Account.Password.Trim());
                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    #region Save UserAccount
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                    if (_Request.Business.Categories != null && _Request.Business.Categories.Count > 0)
                    {
                        _HCUAccount.PrimaryCategoryId = _Request.Business.Categories.FirstOrDefault().ReferenceId;
                        _TUCCategoryAccount = new List<TUCCategoryAccount>();
                        if (_Request.UserReference.AccountId == 0)
                        {
                            _Request.UserReference.AccountId = 1;
                        }
                        foreach (var Category in _Request.Business.Categories)
                        {
                            _TUCCategoryAccount.Add(new TUCCategoryAccount
                            {
                                Guid = HCoreHelper.GenerateGuid(),
                                TypeId = 1,
                                CategoryId = Category.ReferenceId,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                CreatedById = _Request.UserReference.AccountId,
                                StatusId = HelperStatus.Default.Active,
                            });
                        }
                        _HCUAccount.TUCCategoryAccountAccount = _TUCCategoryAccount;
                    }
                    _HCUAccount.Guid = ReferenceKey;
                    _HCUAccount.AccountTypeId = UserAccountType.Merchant;
                    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                    _HCUAccount.OwnerId = 3;
                    _HCUAccount.DisplayName = _Request.Business.DisplayName;
                    _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                    _HCUAccount.Name = _Request.Business.Name;
                    _HCUAccount.EmailAddress = _Request.Business.EmailAddress;
                    _HCUAccount.ContactNumber = _Request.Business.ContactNumber;
                    if (_HCUAccount.OwnerId != 0)
                    {
                        _HCUAccount.OwnerId = 3;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(_Request.Account.ReferralCode))
                        {
                            long ReferrerId = _HCoreContext.HCUAccount
                                .Where(x => x.ReferralCode == _Request.Account.ReferralCode
                                && (x.AccountTypeId == UserAccountType.Merchant || x.AccountTypeId == UserAccountType.Acquirer || x.AccountTypeId == UserAccountType.PgAccount || x.AccountTypeId == UserAccountType.PosAccount))
                                .Select(x => x.Id).FirstOrDefault();
                            if (ReferrerId > 0)
                            {
                                _HCUAccount.OwnerId = ReferrerId;
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1458, TUCCoreResource.CA1458M);
                            }
                        }
                    }
                    _HCUAccount.WebsiteUrl = _Request.Business.WebsiteUrl;
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
                    _HCUAccount.MobileNumber = MobileNumber;
                    _HCUAccount.Address = _AddressResponse.Address;
                    _HCUAccount.Latitude = _AddressResponse.Latitude;
                    _HCUAccount.Longitude = _AddressResponse.Longitude;
                    _HCUAccount.CountryId = _AddressResponse.CountryId;
                    if (_AddressResponse.StateId != 0)
                    {
                        _HCUAccount.StateId = _AddressResponse.StateId;
                    }
                    if (_AddressResponse.CityId != 0)
                    {
                        _HCUAccount.CityId = _AddressResponse.CityId;
                    }
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.App;
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.StatusId = HelperStatus.Default.Inactive;



                    if (_AddressResponse.CountryId > 0)
                    {
                        _HCUAccount.CountryId = _AddressResponse.CountryId;
                    }
                    else
                    {
                        _HCUAccount.CountryId = _Request.UserReference.SystemCountry;
                    }
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.AccountPercentage = 5;
                    _HCUAccount.User = _HCUAccountAuth;
                    #endregion
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    _HCoreContext.SaveChanges();
                    long MerchantId = _HCUAccount.Id;
                    var EmailObject = new
                    {
                        UserName = _HCUAccountAuth.Username,
                        Password = _Request.Account.Password,
                        DisplayName = _HCUAccount.DisplayName,
                        MobileNumber = _HCUAccount.MobileNumber,
                    };
                    using (_HCoreContext = new HCoreContext())
                    {
                        _HCUAccountAuth = new HCUAccountAuth();
                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(10);
                        _HCUAccountAuth.Password = HCoreHelper.GenerateRandomNumber(6);
                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountAuth.CreatedById = MerchantId;
                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                        #region Save UserAccount
                        _HCUAccount = new HCUAccount();
                        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                        _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                        _HCUAccount.OwnerId = MerchantId;
                        _HCUAccount.DisplayName = _Request.Business.DisplayName;
                        _HCUAccount.Name = _Request.Business.DisplayName;
                        _HCUAccount.EmailAddress = _Request.Business.EmailAddress;
                        _HCUAccount.ContactNumber = _Request.Business.ContactNumber;
                        _HCUAccount.MobileNumber = MobileNumber;
                        _HCUAccount.Address = _AddressResponse.Address;
                        _HCUAccount.Latitude = _AddressResponse.Latitude;
                        _HCUAccount.Longitude = _AddressResponse.Longitude;
                        if (_AddressResponse.CountryId > 0)
                        {
                            _HCUAccount.CountryId = _AddressResponse.CountryId;
                        }
                        else
                        {
                            _HCUAccount.CountryId = _Request.UserReference.SystemCountry;
                        }
                        if (_AddressResponse.StateId != 0)
                        {
                            _HCUAccount.StateId = _AddressResponse.StateId;
                        }
                        if (_AddressResponse.CityId != 0)
                        {
                            _HCUAccount.CityId = _AddressResponse.CityId;
                        }
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                        _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(9);
                        if (_Request.UserReference.AppVersionId != 0)
                        {
                            _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                        }
                        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                        _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                        }
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                        _HCUAccount.EmailVerificationStatus = 0;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.NumberVerificationStatus = 0;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.User = _HCUAccountAuth;
                        #endregion
                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                        _HCoreContext.SaveChanges();

                    }
                    HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantWelcomeEmail, _Request.Business.DisplayName, _Request.Account.UserName, EmailObject, _Request.UserReference);
                    #region Request Verification
                    _VerificationRequest = new OCoreVerificationManager.Request();
                    _VerificationRequest.CountryIsd = CountryIsd;
                    _VerificationRequest.Type = 1;
                    _VerificationRequest.MobileNumber = MobileNumber;
                    _VerificationRequest.UserReference = _Request.UserReference;
                    _ManageCoreVerification = new ManageCoreVerification();
                    var VerificationResponse = _ManageCoreVerification.RequestOtp(_VerificationRequest);
                    #endregion
                    string Token = "";
                    if (VerificationResponse.Status == StatusSuccess)
                    {
                        OCoreVerificationManager.Response VerificationResponseItem = (OCoreVerificationManager.Response)VerificationResponse.Result;
                        using (_HCoreContext = new HCoreContext())
                        {
                            var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Id == MerchantId).FirstOrDefault();
                            MerchantDetails.ReferralUrl = VerificationResponseItem.RequestToken;
                            Token = VerificationResponseItem.RequestToken;
                            _HCoreContext.SaveChanges();
                        }
                    }
                    var _Response = new
                    {
                        ReferenceId = MerchantId,
                        ReferenceKey = ReferenceKey,
                        Token = Token
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, TUCCoreResource.CA1063, TUCCoreResource.CA1063M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchantV4", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

    }
}
