//==================================================================================
// FileName: FrameworkCustomersOverview.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to customer overview
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods  
//
//==================================================================================

using System;
using System.Linq.Dynamic.Core;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Merchant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant;
using System.Collections.Generic;
using HCore.TUC.Core.Resource;

namespace HCore.TUC.Core.Framework.Merchant.Analytics
{
    public class FrameworkCustomersOverview
    {
        HCoreContext _HCoreContext;
        OCustomersOverview.OAnalytics.Response _CustomerAnalytics;
        OCustomersOverview.OAnalytics.Count _CustomerAnalyticsCount;
        OCustomersOverview.OAnalytics.Loyalty _LoyaltyOverview;
        List<OCustomersOverview.OAnalytics.Range> _CustomerAnalyticsRange;
        OCustomersOverview.OAnalytics.RewardsOverview _RewardsOverview;

        /// <summary>
        /// Description: Gets the customers gender.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomersGender(OCustomersOverview.OAnalytics.Request _Request)
        {
            _CustomerAnalytics = new OCustomersOverview.OAnalytics.Response();
            _CustomerAnalyticsCount = new OCustomersOverview.OAnalytics.Count();
            _LoyaltyOverview = new OCustomersOverview.OAnalytics.Loyalty();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime TodayTime = HCoreHelper.GetGMTDateTime();
                    _CustomerAnalyticsRange = new List<OCustomersOverview.OAnalytics.Range>();
                    _CustomerAnalyticsRange.Add(new OCustomersOverview.OAnalytics.Range
                    {
                        Title = "Male",
                        Count = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                                && x.CountryId == _Request.UserReference.CountryId && x.GenderId == Helpers.Gender.Male
                                && x.OwnerId == _Request.AccountId
                                && x.Owner.Guid == _Request.AccountKey
                                && (x.Owner.AccountTypeId == UserAccountType.Merchant || x.Owner.Owner.AccountTypeId == UserAccountType.Merchant)), //&& x.TUCSaleCustomer.Any(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate) == true),

                        Visits = _HCoreContext.HCUAccountTransaction.Count(x =>
                                 x.Account.AccountTypeId == UserAccountType.Appuser
                                 && x.Account.CountryId == _Request.UserReference.CountryId
                                 && x.ParentId == _Request.AccountId
                                 && x.Parent.Guid == _Request.AccountKey
                                 && x.Parent.AccountTypeId == UserAccountType.Merchant
                                 && x.Account.GenderId == Helpers.Gender.Male
                                 && x.StatusId == HelperStatus.Transaction.Success
                                 && x.TransactionDate > _Request.StartDate
                                 && x.TransactionDate < _Request.EndDate),

                        InvoiceAmount = _HCoreContext.HCUAccountTransaction.Where(x =>
                                        x.StatusId == HelperStatus.Transaction.Success
                                        && x.Customer.GenderId == Helpers.Gender.Male
                                        && x.ParentId == _Request.AccountId
                                        && x.Parent.Guid == _Request.AccountKey
                                        && x.Parent.AccountTypeId == UserAccountType.Merchant
                                        && x.TransactionDate > _Request.StartDate
                                        && x.TransactionDate < _Request.EndDate)
                                        .Sum(x => x.PurchaseAmount),
                    });
                    _CustomerAnalyticsRange.Add(new OCustomersOverview.OAnalytics.Range
                    {
                        Title = "Female",
                        Count = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                                && x.CountryId == _Request.UserReference.CountryId && x.GenderId == Helpers.Gender.Female
                                && x.OwnerId == _Request.AccountId
                                && x.Owner.Guid == _Request.AccountKey
                                && (x.Owner.AccountTypeId == UserAccountType.Merchant || x.Owner.Owner.AccountTypeId == UserAccountType.Merchant)),
                        //&& x.TUCSaleCustomer.Any(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate) == true),

                        Visits = _HCoreContext.HCUAccountTransaction.Count(x =>
                                 x.Account.AccountTypeId == UserAccountType.Appuser
                                 && x.Account.CountryId == _Request.UserReference.CountryId
                                 && x.ParentId == _Request.AccountId
                                 && x.Parent.Guid == _Request.AccountKey
                                 && x.Parent.AccountTypeId == UserAccountType.Merchant
                                 && x.Account.GenderId == Helpers.Gender.Female
                                 && x.StatusId == HelperStatus.Transaction.Success
                                 && x.TransactionDate > _Request.StartDate
                                 && x.TransactionDate < _Request.EndDate),

                        InvoiceAmount = _HCoreContext.HCUAccountTransaction.Where(x =>
                                        x.StatusId == HelperStatus.Transaction.Success
                                        && x.ParentId == _Request.AccountId
                                        && x.Parent.Guid == _Request.AccountKey
                                        && x.Parent.AccountTypeId == UserAccountType.Merchant
                                        && x.Customer.GenderId == Helpers.Gender.Male
                                        && x.TransactionDate > _Request.StartDate
                                        && x.TransactionDate < _Request.EndDate)
                                        .Sum(x => x.PurchaseAmount),
                    });
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerAnalyticsRange, "HC0001", "Details loaded");
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersGender", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

        /// <summary>
        /// Description: Gets the customers age group gender.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomersAgeGroupGender(OCustomersOverview.OAnalytics.Request _Request)
        {
            try
            {
                DateTime TodayTime = HCoreHelper.GetGMTDateTime();
                using (_HCoreContext = new HCoreContext())
                {
                    _CustomerAnalyticsRange = new List<OCustomersOverview.OAnalytics.Range>();
                    _CustomerAnalyticsRange.Add(new OCustomersOverview.OAnalytics.Range
                    {
                        Title = "18-29",
                        Count = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                                && (x.DateOfBirth > _Request.StartDate && x.DateOfBirth < _Request.EndDate)
                                && x.CountryId == _Request.UserReference.CountryId
                                && (x.OwnerId == _Request.AccountId || x.Owner.OwnerId == _Request.AccountId)
                                && x.Owner.AccountTypeId == UserAccountType.Merchant
                                && (TodayTime.Year - x.DateOfBirth.Value.Year) > 17),

                        Male = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                                && (x.DateOfBirth > _Request.StartDate && x.DateOfBirth < _Request.EndDate)
                                && x.CountryId == _Request.UserReference.CountryId
                                && (x.OwnerId == _Request.AccountId || x.Owner.OwnerId == _Request.AccountId)
                                && x.Owner.AccountTypeId == UserAccountType.Merchant
                                && x.GenderId == Gender.Male
                                && (TodayTime.Year - x.DateOfBirth.Value.Year) > 17),


                        Female = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                                && (x.DateOfBirth > _Request.StartDate && x.DateOfBirth < _Request.EndDate)
                                && x.CountryId == _Request.UserReference.CountryId
                                && (x.OwnerId == _Request.AccountId || x.Owner.OwnerId == _Request.AccountId)
                                && x.Owner.AccountTypeId == UserAccountType.Merchant
                                && x.GenderId == Gender.Female
                                && (TodayTime.Year - x.DateOfBirth.Value.Year) > 17),
                    });
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerAnalyticsRange, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersAgeGroupGender", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

        /// <summary>
        /// Description: Gets the customers top spenders.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomersTopSpenders(OCustomersOverview.OAnalytics.Request _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    List<OCustomersOverview.OAnalytics.User> Data = _HCoreContext.HCUAccount.Where(x =>
                                x.AccountTypeId == UserAccountType.Appuser
                                && x.OwnerId == _Request.AccountId
                                && x.Owner.Guid == _Request.AccountKey
                                && x.CountryId == _Request.UserReference.CountryId
                                && x.HCUAccountTransactionAccount.Any(a => a.StatusId == HelperStatus.Transaction.Success
                                                                 && a.ParentId == _Request.AccountId
                                                                 && a.TransactionDate > _Request.StartDate
                                                                 && a.TransactionDate < _Request.EndDate) == true)
                                .Select(x => new OCustomersOverview.OAnalytics.User
                                {
                                    ReferenceId = x.Id,
                                    ReferenceKey = x.Guid,
                                    DisplayName = x.DisplayName,
                                    MobileNumber = x.MobileNumber,
                                    IconUrl = x.IconStorage.Path,
                                    InvoiceAmount = x.HCUAccountTransactionAccount.Where(a => a.StatusId == HelperStatus.Transaction.Success
                                                                                  && a.ParentId == _Request.AccountId
                                                                                  && a.TransactionDate > _Request.StartDate
                                                                                  && a.TransactionDate < _Request.EndDate).Sum(a => a.PurchaseAmount),
                                }).OrderByDescending(x => x.InvoiceAmount)
                                .Skip(_Request.Offset)
                                .Take(_Request.Limit)
                                .ToList();
                    if (Data.Count > 0)
                    {
                        foreach (var TopSpender in Data)
                        {
                            if (!string.IsNullOrEmpty(TopSpender.IconUrl))
                            {
                                TopSpender.IconUrl = _AppConfig.StorageUrl + TopSpender.IconUrl;
                            }
                            else
                            {
                                TopSpender.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "HC0001", "Details loaded");
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersTopSpenders", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

        /// <summary>
        /// Description: Gets the customer rewards overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomerRewardsOverview(OReference _Request)
        {
            try
            {
                _RewardsOverview = new OCustomersOverview.OAnalytics.RewardsOverview();
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    _RewardsOverview.UnClaimedRewardsAmount = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                    && x.ParentId == _Request.AccountId
                                                    && x.ModeId == TransactionMode.Credit
                                                    && x.ParentTransaction.TotalAmount > 0
                                                    && x.CampaignId == null
                                                    && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                    || x.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount);

                    _RewardsOverview.PendingRewardsAmount = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                    && x.ParentId == _Request.AccountId
                                                    && x.StatusId == HelperStatus.Transaction.Pending
                                                    && x.ModeId == TransactionMode.Credit
                                                    && x.ParentTransaction.TotalAmount > 0
                                                    && x.CampaignId == null
                                                    && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                    || x.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount);

                    _RewardsOverview.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.ReferenceAmount);

                    _RewardsOverview.TotalRewards = _RewardsOverview.UnClaimedRewardsAmount + _RewardsOverview.PendingRewardsAmount;

                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _RewardsOverview, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetCustomerRewardsOverview", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
    }
}

