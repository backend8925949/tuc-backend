//==================================================================================
// FileName: FrameworkOverview.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to overview
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Operations;
using HCore.Helper;
using HCore.Operations;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Resource;
using Mono;
using System;
using System.Collections.Generic;
using System.Linq;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;


namespace HCore.TUC.Core.Framework.Merchant.Analytics
{
    internal class FrameworkOverview
    {
        public class OSalesItem
        {
            public long? UserAccountId { get; set; }
            //public DateTime TransactionDate { get; set; }
            public DateTime TransactionDateT { get; set; }
            public double? InvoiceAmount { get; set; }
            public long? CashierId { get; set; }
            public long TypeId { get; set; }
        }
        List<OSalesItem> _OSalesItem;
        public class CashierOverview
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? FistName { get; set; }
            public string? LastName { get; set; }
            public string? CashierId { get; set; }
            public string? IconUrl { get; set; }
            public long Transactions { get; set; }
            public long Customers { get; set; }
            public double InvoiceAmount { get; set; }
            public double CardInvoiceAmount { get; set; }
            public double CashInvoiceAmount { get; set; }
            public long WorkingDays { get; set; }
            public long InactiveDays { get; set; }
            public long AverageCustomers { get; set; }
            public long AverageTransactions { get; set; }
            public double PerformanceLevel { get; set; }
            public string? PerformanceName { get; set; }
        }
        public class CashiersOverview
        {
            public long Total { get; set; }
            public long Transactions { get; set; }
            public long Customers { get; set; }
            public double InvoiceAmount { get; set; }
            public double CardInvoiceAmount { get; set; }
            public double CashInvoiceAmount { get; set; }
            public long WorkingDays { get; set; }
            public long AverageCustomers { get; set; }
            public long AverageTransactions { get; set; }
            public List<CashierOverview> Cashiers { get; set; }
        }
        CashiersOverview _CashiersOverview;
        List<CashierOverview> _CashierOverviewDetails;

        private class OTrList
        {
            public DateTime TransactionDate { get; set; }
            public double InvoiceAmount { get; set; }
            public long? UserAccountId { get; set; }
        }
        HCoreContext _HCoreContext;
        HCoreContextOperations _HCoreContextOperations;
        OAnalytics.SalesOverview _SalesOverview;
        OAnalytics.CustomerOverview _CustomerOverview;
        OAnalytics.Loyalty _LoyaltyOverview;
        List<OAnalytics.Sale> _SalesHistory;
        List<OTrList> _OTrList;
        OAnalytics.Overview _AccOverview;
        /// <summary>
        /// Description: Gets the account summary.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountSummary(OAnalytics.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    _AccOverview = new OAnalytics.Overview();
                    _AccOverview.IsTransactionPresent = _HCoreContext.HCUAccountTransaction.Any(x => x.ParentId == _Request.AccountId);
                    _AccOverview.Balance = _HCoreContext.HCUAccountBalance.Where(x => x.AccountId == _Request.AccountId).Select(x => x.Balance).FirstOrDefault();
                    _AccOverview.RewardPercentage = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId).Select(x => x.AccountPercentage).FirstOrDefault() ?? 0;
                    _AccOverview.SubscriptionName = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId).Select(x => x.Subscription.Subscription.Name).FirstOrDefault();
                    //_AccOverview.PaymentModeType = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId).Select(x => x.Subscription.Name).FirstOrDefault() ;
                    _AccOverview.Stores = _HCoreContext.HCUAccount.Count(x => x.OwnerId == _Request.AccountId && x.AccountTypeId == UserAccountType.MerchantStore);
                    _AccOverview.Cashiers = _HCoreContext.HCUAccount.Count(x => (x.OwnerId == _Request.AccountId || x.Owner.OwnerId == _Request.AccountId || x.SubOwner.OwnerId == _Request.AccountId) && x.AccountTypeId == UserAccountType.MerchantCashier);
                    _AccOverview.Terminals = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.AccountId);
                    _AccOverview.Subaccounts = _HCoreContext.HCUAccount.Count(x => x.OwnerId == _Request.AccountId && x.AccountTypeId == UserAccountType.MerchantSubAccount);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AccOverview, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAccountSummary", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the customers overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomersOverview(OAnalytics.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    _CustomerOverview = new OAnalytics.CustomerOverview();
                    #region Sales Overiew
                    if (_Request.StoreReferenceId != 0)
                    {
                        long Transactions = (long?)_HCoreContext.HCUAccountTransaction
                                                  .Count(m => m.ParentId == _Request.AccountId
                                                        && m.SubParentId == _Request.StoreReferenceId
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && m.TransactionDate >= _Request.StartDate
                                                        && m.TransactionDate <= _Request.EndDate
                                                        && m.ModeId == TransactionMode.Credit
                                                        && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC) || m.SourceId == TransactionSource.TUCBlack
                                                        || m.SourceId == TransactionSource.ThankUCashPlus)
                                                  ) ?? 0;

                        _CustomerOverview.Total = _HCoreContext.HCUAccount
                                  .Count(m => m.AccountTypeId == UserAccountType.Appuser
                                  && ((m.OwnerId == _Request.AccountId && m.CreateDate >= _Request.StartDate && m.CreateDate <= _Request.EndDate)
                                  || m.HCUAccountTransactionAccount.Any(x => x.ParentId == _Request.AccountId && x.SubParentId == _Request.StoreReferenceId
                                  && x.TransactionDate > _Request.StartDate
                                  && x.TransactionDate < _Request.EndDate
                                  && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.SourceId == TransactionSource.TUC && x.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                          || ((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.SourceId == TransactionSource.ThankUCashPlus) || x.SourceId == TransactionSource.TUCBlack)
                                  && x.CampaignId == null)));
                        _CustomerOverview.New = _HCoreContext.HCUAccount
                           .Count(m => m.AccountTypeId == UserAccountType.Appuser
                           && m.HCUAccountTransactionAccount.Count(x => x.ParentId == _Request.AccountId && x.SubParentId == _Request.StoreReferenceId
                          && x.TransactionDate >= _Request.StartDate
                          && x.TransactionDate <= _Request.EndDate
                          && x.StatusId == HelperStatus.Transaction.Success
                          && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.SourceId == TransactionSource.TUC && x.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || ((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.SourceId == TransactionSource.ThankUCashPlus) || x.SourceId == TransactionSource.TUCBlack)
                          && x.CampaignId == null) == 1);

                        _CustomerOverview.Repeat = _HCoreContext.HCUAccount
                           .Count(m => m.AccountTypeId == UserAccountType.Appuser
                           && m.HCUAccountTransactionAccount.Count(x => x.ParentId == _Request.AccountId && x.SubParentId == _Request.StoreReferenceId
                          && x.TransactionDate >= _Request.StartDate
                          && x.TransactionDate <= _Request.EndDate
                          && x.StatusId == HelperStatus.Transaction.Success
                          && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.SourceId == TransactionSource.TUC && x.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || ((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.SourceId == TransactionSource.ThankUCashPlus) || x.SourceId == TransactionSource.TUCBlack)
                          && x.CampaignId == null) > 1);

                        if (_CustomerOverview.Total > 0)
                        {
                            _CustomerOverview.AverageVisit = Transactions / _CustomerOverview.Total;
                        }

                        if (Transactions > 0)
                        {
                            double InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                 .Where(m => m.ParentId == _Request.AccountId
                                                                                       && m.SubParentId == _Request.StoreReferenceId
                                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                                       && m.TransactionDate >= _Request.StartDate
                                                                                       && m.TransactionDate <= _Request.EndDate
                                                                                       && m.ModeId == TransactionMode.Credit
                                                                                       && ((m.TypeId != TransactionType.ThankUCashPlusCredit && ((m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) || m.SourceId == TransactionSource.TUCBlack))
                                                                                       || m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUCBlack)
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                            if (_CustomerOverview.Total > 0)
                            {
                                _CustomerOverview.AverageInvoiceAmount = InvoiceAmount / _CustomerOverview.Total;
                            }

                        }
                    }
                    else
                    {
                        long Transactions = (long?)_HCoreContext.HCUAccountTransaction
                                                  .Count(m => m.ParentId == _Request.AccountId
                                                        && m.Account.AccountTypeId == UserAccountType.Appuser
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && m.TransactionDate >= _Request.StartDate
                                                        && m.TransactionDate <= _Request.EndDate
                                                        && m.ModeId == TransactionMode.Credit
                                                        && ((m.TypeId != TransactionType.ThankUCashPlusCredit && ((m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) || m.SourceId == TransactionSource.TUCBlack)) || m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUCBlack)
                                                  ) ?? 0;

                        _CustomerOverview.Total = _HCoreContext.HCUAccount
                                  .Count(m => m.AccountTypeId == UserAccountType.Appuser
                                  && ((m.OwnerId == _Request.AccountId && m.CreateDate >= _Request.StartDate && m.CreateDate <= _Request.EndDate)
                                  || m.HCUAccountTransactionAccount.Any(x => x.ParentId == _Request.AccountId
                                  && x.TransactionDate >= _Request.StartDate
                                  && x.TransactionDate <= _Request.EndDate
                                  && x.StatusId == HelperStatus.Transaction.Success
                                  && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.SourceId == TransactionSource.TUC && x.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                          || ((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.SourceId == TransactionSource.ThankUCashPlus) || x.SourceId == TransactionSource.TUCBlack)
                                  && x.CampaignId == null)));
                        _CustomerOverview.New = _HCoreContext.HCUAccount
                           .Count(m => m.AccountTypeId == UserAccountType.Appuser
                           && m.HCUAccountTransactionAccount.Count(x => x.ParentId == _Request.AccountId
                          && x.TransactionDate >= _Request.StartDate
                          && x.TransactionDate <= _Request.EndDate
                          && x.StatusId == HelperStatus.Transaction.Success
                          && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.SourceId == TransactionSource.TUC && x.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || ((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.SourceId == TransactionSource.ThankUCashPlus) || x.SourceId == TransactionSource.TUCBlack)
                          && x.CampaignId == null) == 1);

                        _CustomerOverview.Repeat = _HCoreContext.HCUAccount
                           .Count(m => m.AccountTypeId == UserAccountType.Appuser
                           && m.HCUAccountTransactionAccount.Count(x => x.ParentId == _Request.AccountId
                          && x.TransactionDate >= _Request.StartDate
                          && x.TransactionDate <= _Request.EndDate
                          && x.StatusId == HelperStatus.Transaction.Success
                          && (((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.SourceId == TransactionSource.TUC && x.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || ((x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit) && x.SourceId == TransactionSource.ThankUCashPlus) || x.SourceId == TransactionSource.TUCBlack)
                          && x.CampaignId == null) > 1);

                        if (_CustomerOverview.Total > 0)
                        {
                            _CustomerOverview.AverageVisit = Transactions / _CustomerOverview.Total;
                        }
                        if (Transactions > 0)
                        {
                            double InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                 .Where(m => m.ParentId == _Request.AccountId
                                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                                       && m.TransactionDate >= _Request.StartDate
                                                                                       && m.TransactionDate <= _Request.EndDate
                                                                                       && m.ModeId == TransactionMode.Credit
                                                                                       && ((m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                       || m.SourceId == TransactionSource.ThankUCashPlus || m.SourceId == TransactionSource.TUCBlack)
                                                                                 ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                            if (_CustomerOverview.Total > 0)
                            {
                                _CustomerOverview.AverageInvoiceAmount = InvoiceAmount / _CustomerOverview.Total;
                            }
                        }
                    }
                    #endregion
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerOverview, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the sales overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSalesOverview(OAnalytics.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    _SalesOverview = new OAnalytics.SalesOverview();
                    #region Sales Overiew
                    if (_Request.StoreReferenceId != 0)
                    {
                        _SalesOverview.Transactions = (long?)_HCoreContext.HCUAccountTransaction
                                                 .Count(x => x.SubParentId == _Request.StoreReferenceId
                                             && x.ParentId == _Request.AccountId
                                             && x.TransactionDate.AddHours(1) > _Request.StartDate
                                             && x.TransactionDate.AddHours(1) < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                             && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                             && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)) ?? 0;
                        if (_SalesOverview.Transactions > 0)
                        {
                            _SalesOverview.Customers = (long?)_HCoreContext.HCUAccountTransaction
                                                   .Where(x => x.SubParentId == _Request.StoreReferenceId
                                                && x.ParentId == _Request.AccountId
                                                && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                && x.TransactionDate.AddHours(1) < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)).Select(x => x.AccountId).Distinct().Count() ?? 0;
                            _SalesOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.SubParentId == _Request.StoreReferenceId
                                                && x.ParentId == _Request.AccountId
                                                && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                && x.TransactionDate.AddHours(1) < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                            _SalesOverview.HighSales = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.SubParentId == _Request.StoreReferenceId
                                                && x.ParentId == _Request.AccountId
                                                && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                && x.TransactionDate.AddHours(1) < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)).OrderByDescending(m => m.PurchaseAmount).Select(m => m.PurchaseAmount).FirstOrDefault();
                            if (_Request.AmountDistribution)
                            {

                                _SalesOverview.CardTransaction = _HCoreContext.HCUAccountTransaction
                                                                      .Count(m => m.ParentId == _Request.AccountId
                                                                       && m.SubParentId == _Request.StoreReferenceId
                                                                       && m.TransactionDate.AddHours(1) > _Request.StartDate
                                                                       && m.TransactionDate.AddHours(1) < _Request.EndDate
                                                                       && m.TypeId == TransactionType.CardReward
                                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                     );
                                if (_SalesOverview.CardTransaction > 0)
                                {
                                    _SalesOverview.CardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.ParentId == _Request.AccountId
                                                                     && m.SubParentId == _Request.StoreReferenceId
                                                                     && m.TransactionDate.AddHours(1) > _Request.StartDate
                                                                     && m.TransactionDate.AddHours(1) < _Request.EndDate
                                                                     && m.TypeId == TransactionType.CardReward
                                                                     && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                     || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }
                                _SalesOverview.CashTransaction = _HCoreContext.HCUAccountTransaction
                                                                   .Count(m => m.ParentId == _Request.AccountId
                                                                    && m.SubParentId == _Request.StoreReferenceId
                                                                    && m.TransactionDate.AddHours(1) > _Request.StartDate
                                                                    && m.TransactionDate.AddHours(1) < _Request.EndDate
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  );
                                if (_SalesOverview.CashTransaction > 0)
                                {
                                    _SalesOverview.CashInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.ParentId == _Request.AccountId
                                                                     && m.SubParentId == _Request.StoreReferenceId
                                                                     && m.TransactionDate.AddHours(1) > _Request.StartDate
                                                                     && m.TransactionDate.AddHours(1) < _Request.EndDate
                                                                     && m.TypeId == TransactionType.CashReward
                                                                     && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                     || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }
                            }
                        }
                    }
                    else if (_Request.CashierReferenceId != 0)
                    {
                        _SalesOverview.Transactions = (long?)_HCoreContext.HCUAccountTransaction
                                                    .Count(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.CashierId == _Request.CashierReferenceId
                                                 && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                 && x.TransactionDate.AddHours(1) < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)) ?? 0;
                        if (_SalesOverview.Transactions > 0)
                        {
                            _SalesOverview.Customers = (long?)_HCoreContext.HCUAccountTransaction
                                                   .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.CashierId == _Request.CashierReferenceId
                                                && x.ParentId == _Request.AccountId
                                                && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                && x.TransactionDate.AddHours(1) < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)).Select(x => x.AccountId).Distinct().Count() ?? 0;
                            _SalesOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.CashierId == _Request.CashierReferenceId
                                                && x.ParentId == _Request.AccountId
                                                && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                && x.TransactionDate.AddHours(1) < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                            _SalesOverview.HighSales = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.CashierId == _Request.CashierReferenceId
                                                && x.ParentId == _Request.AccountId
                                                && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                && x.TransactionDate.AddHours(1) < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)).OrderByDescending(m => m.PurchaseAmount).Select(m => m.PurchaseAmount).FirstOrDefault();
                            if (_Request.AmountDistribution)
                            {
                                _SalesOverview.CardTransaction = _HCoreContext.HCUAccountTransaction
                                                                      .Count(m => m.ParentId == _Request.AccountId
                                                && m.CashierId == _Request.CashierReferenceId
                                                                       && m.TransactionDate.AddHours(1) > _Request.StartDate
                                                                       && m.TransactionDate.AddHours(1) < _Request.EndDate
                                                                       && m.TypeId == TransactionType.CardReward
                                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || m.SourceId == TransactionSource.TUCBlack)
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                     );
                                if (_SalesOverview.CardTransaction > 0)
                                {
                                    _SalesOverview.CardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.ParentId == _Request.AccountId
                                                && m.CashierId == _Request.CashierReferenceId
                                                                     && m.TransactionDate.AddHours(1) > _Request.StartDate
                                                                     && m.TransactionDate.AddHours(1) < _Request.EndDate
                                                                     && m.TypeId == TransactionType.CardReward
                                                                     && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                     || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || m.SourceId == TransactionSource.TUCBlack)
                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }
                                _SalesOverview.CashTransaction = _HCoreContext.HCUAccountTransaction
                                                                   .Count(m => m.ParentId == _Request.AccountId
                                                && m.CashierId == _Request.CashierReferenceId
                                                                    && m.TransactionDate.AddHours(1) > _Request.StartDate
                                                                    && m.TransactionDate.AddHours(1) < _Request.EndDate
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || m.SourceId == TransactionSource.TUCBlack)
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  );
                                if (_SalesOverview.CashTransaction > 0)
                                {
                                    _SalesOverview.CashInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.ParentId == _Request.AccountId
                                                && m.CashierId == _Request.CashierReferenceId
                                                                     && m.TransactionDate.AddHours(1) > _Request.StartDate
                                                                     && m.TransactionDate.AddHours(1) < _Request.EndDate
                                                                     && m.TypeId == TransactionType.CashReward
                                                                     && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                     || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || m.SourceId == TransactionSource.TUCBlack)
                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }
                            }
                        }
                    }
                    else if (_Request.CustomerReferenceId != 0)
                    {

                        _SalesOverview.Transactions = (long?)_HCoreContext.HCUAccountTransaction
                                                   .Count(x => x.AccountId == _Request.CustomerReferenceId
                                               && x.ParentId == _Request.AccountId
                                                && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                                      && x.TransactionDate.AddHours(1) < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                               && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                               && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)) ?? 0;
                        if (_SalesOverview.Transactions > 0)
                        {
                            _SalesOverview.Customers = (long?)_HCoreContext.HCUAccountTransaction
                                                   .Where(x => x.AccountId == _Request.CustomerReferenceId
                                                && x.ParentId == _Request.AccountId
                                                && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                && x.TransactionDate.AddHours(1) < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)).Select(x => x.AccountId).Distinct().Count() ?? 0;
                            _SalesOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.AccountId == _Request.CustomerReferenceId
                                                && x.ParentId == _Request.AccountId
                                                && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                && x.TransactionDate.AddHours(1) < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                            if (_Request.AmountDistribution)
                            {

                                _SalesOverview.CardTransaction = _HCoreContext.HCUAccountTransaction
                                                                      .Count(m => m.ParentId == _Request.AccountId
                                                                       && m.AccountId == _Request.CustomerReferenceId
                                                                       && m.TransactionDate.AddHours(1) > _Request.StartDate
                                                                       && m.TransactionDate.AddHours(1) < _Request.EndDate
                                                                       && m.TypeId == TransactionType.CardReward
                                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || m.SourceId == TransactionSource.TUCBlack)
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                     );
                                if (_SalesOverview.CardTransaction > 0)
                                {
                                    _SalesOverview.CardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.ParentId == _Request.AccountId
                                                                      && m.AccountId == _Request.CustomerReferenceId
                                                                     && m.TransactionDate.AddHours(1) > _Request.StartDate
                                                                     && m.TransactionDate.AddHours(1) < _Request.EndDate
                                                                     && m.TypeId == TransactionType.CardReward
                                                                     && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                     || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || m.SourceId == TransactionSource.TUCBlack)
                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }
                                _SalesOverview.CashTransaction = _HCoreContext.HCUAccountTransaction
                                                                   .Count(m => m.ParentId == _Request.AccountId
                                                                    && m.AccountId == _Request.CustomerReferenceId
                                                                    && m.TransactionDate.AddHours(1) > _Request.StartDate
                                                                    && m.TransactionDate.AddHours(1) < _Request.EndDate
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || m.SourceId == TransactionSource.TUCBlack)
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  );
                                if (_SalesOverview.CashTransaction > 0)
                                {
                                    _SalesOverview.CashInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.ParentId == _Request.AccountId
                                                                     && m.AccountId == _Request.CustomerReferenceId
                                                                     && m.TransactionDate.AddHours(1) > _Request.StartDate
                                                                     && m.TransactionDate.AddHours(1) < _Request.EndDate
                                                                     && m.TypeId == TransactionType.CashReward
                                                                     && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                     || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || m.SourceId == TransactionSource.TUCBlack)
                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }
                            }

                        }
                    }
                    else
                    {
                        _SalesOverview.Transactions = (long?)_HCoreContext.HCUAccountTransaction
                                                    .Count(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                && x.TransactionDate.AddHours(1) < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)) ?? 0;
                        if (_SalesOverview.Transactions > 0)
                        {
                            _SalesOverview.Customers = (long?)_HCoreContext.HCUAccountTransaction
                                                   .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                && x.TransactionDate.AddHours(1) < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)).Select(x => x.AccountId).Distinct().Count() ?? 0;
                            _SalesOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                && x.TransactionDate.AddHours(1) < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                            _SalesOverview.HighSales = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                && x.TransactionDate.AddHours(1) < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)).OrderByDescending(m => m.PurchaseAmount).Select(m => m.PurchaseAmount).FirstOrDefault();
                            if (_Request.AmountDistribution)
                            {
                                _SalesOverview.CardTransaction = _HCoreContext.HCUAccountTransaction
                                                                      .Count(m => m.ParentId == _Request.AccountId
                                                                       && m.TransactionDate > _Request.StartDate
                                                                       && m.TransactionDate < _Request.EndDate
                                                                       && m.TypeId == TransactionType.CardReward
                                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || m.SourceId == TransactionSource.TUCBlack)
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                     );
                                if (_SalesOverview.CardTransaction > 0)
                                {
                                    _SalesOverview.CardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.ParentId == _Request.AccountId
                                                                     && m.TransactionDate > _Request.StartDate
                                                                     && m.TransactionDate < _Request.EndDate
                                                                     && m.TypeId == TransactionType.CardReward
                                                                     && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                     || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || m.SourceId == TransactionSource.TUCBlack)
                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }
                                _SalesOverview.CashTransaction = _HCoreContext.HCUAccountTransaction
                                                                   .Count(m => m.ParentId == _Request.AccountId
                                                                    && m.TransactionDate > _Request.StartDate
                                                                    && m.TransactionDate < _Request.EndDate
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || m.SourceId == TransactionSource.TUCBlack)
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  );
                                if (_SalesOverview.CashTransaction > 0)
                                {
                                    _SalesOverview.CashInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                    .Where(m => m.ParentId == _Request.AccountId
                                                                     && m.TransactionDate > _Request.StartDate
                                                                     && m.TransactionDate < _Request.EndDate
                                                                     && m.TypeId == TransactionType.CashReward
                                                                     && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                     || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus) || m.SourceId == TransactionSource.TUCBlack)
                                                                     && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                                }
                            }
                        }
                    }
                    #endregion
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SalesOverview, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetSalesOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the sales history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSalesHistory(OAnalytics.Request _Request)
        {
            #region Manage Exception
            _SalesHistory = new List<OAnalytics.Sale>();
            try
            {
                //_Request.StartDate = _Request.StartDate.Value.Date;
                //_Request.EndDate = _Request.EndDate.Value.Date;
                #region Operation
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        _OSalesItem = new List<OSalesItem>();
                        if (_Request.StoreReferenceId != 0)
                        {
                            if (_Request.SourceId > 0)
                            {
                                _OSalesItem = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.SubParentId == _Request.StoreReferenceId
                                                && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                && x.TransactionDate.AddHours(1) < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                                && (x.SourceId == _Request.SourceId))
                                          .Select(x => new OSalesItem
                                          {
                                              UserAccountId = x.AccountId,
                                              //TransactionDate = x.TransactionDate,
                                              TransactionDateT = x.TransactionDate.AddHours(1),
                                              InvoiceAmount = x.PurchaseAmount
                                          }).ToList();
                            }
                            else
                            {
                                _OSalesItem = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.SubParentId == _Request.StoreReferenceId
                                                && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                && x.TransactionDate.AddHours(1) < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && x.TypeId != TransactionType.ThankUCashPlusCredit)
                                          .Select(x => new OSalesItem
                                          {
                                              UserAccountId = x.AccountId,
                                              //TransactionDate = x.TransactionDate,
                                              TransactionDateT = x.TransactionDate.AddHours(1),
                                              InvoiceAmount = x.PurchaseAmount
                                          }).ToList();
                            }
                        }
                        else
                        {
                            if (_Request.SourceId > 0)
                            {
                                _OSalesItem = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                && x.TransactionDate.AddHours(1) < _Request.EndDate
                                            && x.StatusId == HelperStatus.Transaction.Success
                                                && (x.SourceId == _Request.SourceId))
                                          .Select(x => new OSalesItem
                                          {
                                              UserAccountId = x.AccountId,
                                              //TransactionDate = x.TransactionDate,
                                              TransactionDateT = x.TransactionDate.AddHours(1),
                                              InvoiceAmount = x.PurchaseAmount
                                          }).ToList();
                            }
                            else
                            {
                                _OSalesItem = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.TransactionDate.AddHours(1) > _Request.StartDate
                                                && x.TransactionDate.AddHours(1) < _Request.EndDate
                                            && x.StatusId == HelperStatus.Transaction.Success
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && x.TypeId != TransactionType.ThankUCashPlusCredit)
                                          .Select(x => new OSalesItem
                                          {
                                              UserAccountId = x.AccountId,
                                              //TransactionDate = x.TransactionDate,
                                              TransactionDateT = x.TransactionDate.AddHours(1),
                                              InvoiceAmount = x.PurchaseAmount
                                          }).ToList();
                            }
                        }
                        _HCoreContext.Dispose();
                        #region Set Default Limit
                        #endregion
                        #region Get Data
                        if (_Request.Type == "hour")
                        {
                            for (int i = 0; i <= 23; i++)
                            {
                                _SalesHistory.Add(new OAnalytics.Sale
                                {
                                    Hour = i,
                                    TotalCustomer = 0,
                                    TotalTransaction = 0,
                                    TotalInvoiceAmount = 0,
                                    RepeatVisits = 0,
                                    NewVisits = 0
                                });
                            }
                            var _SalesDetails = _OSalesItem
                                                   .GroupBy(x => x.TransactionDateT.Hour)
                                                   .Select(x => new OAnalytics.Sale
                                                   {
                                                       Hour = x.Key,
                                                       TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                                       TotalTransaction = x.Count(),
                                                       TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                                       NewVisits = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() == 1).Count(),
                                                       RepeatVisits = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() > 1).Count(),
                                                   })
                                                   .ToList();
                            foreach (var _SalesHistoryItem in _SalesHistory)
                            {
                                var ItemInfo = _SalesDetails.Where(x => x.Hour == _SalesHistoryItem.Hour).FirstOrDefault();
                                if (ItemInfo != null)
                                {
                                    _SalesHistoryItem.TotalCustomer = ItemInfo.TotalCustomer;
                                    _SalesHistoryItem.TotalTransaction = ItemInfo.TotalTransaction;
                                    _SalesHistoryItem.TotalInvoiceAmount = ItemInfo.TotalInvoiceAmount;
                                    _SalesHistoryItem.NewVisits = ItemInfo.NewVisits;
                                    _SalesHistoryItem.RepeatVisits = ItemInfo.RepeatVisits;
                                }
                            }

                        }
                        else if (_Request.Type == "m")
                        {

                        }
                        else if (_Request.Type == "week")
                        {
                            string[] Days = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
                            var _SalesDetails = _OSalesItem
                                                 .GroupBy(x => x.TransactionDateT.DayOfWeek)
                                                 .Select(x => new OAnalytics.Sale
                                                 {
                                                     WeekDay = x.Key.ToString(),
                                                     TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                                     TotalTransaction = x.Count(),
                                                     TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                                     NewVisits = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() == 1).Count(),
                                                     RepeatVisits = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() > 1).Count(),
                                                 })
                                                 .ToList();
                            foreach (var DayItem in Days)
                            {
                                var ItemInfo = _SalesDetails.Where(x => x.WeekDay == DayItem).FirstOrDefault();
                                if (ItemInfo != null)
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        WeekDay = DayItem,
                                        TotalCustomer = ItemInfo.TotalCustomer,
                                        TotalTransaction = ItemInfo.TotalTransaction,
                                        TotalInvoiceAmount = ItemInfo.TotalInvoiceAmount,
                                        NewVisits = ItemInfo.NewVisits,
                                        RepeatVisits = ItemInfo.RepeatVisits
                                    });
                                }
                                else
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        WeekDay = DayItem,
                                        TotalCustomer = 0,
                                        TotalTransaction = 0,
                                        TotalInvoiceAmount = 0,
                                        NewVisits = 0,
                                        RepeatVisits = 0
                                    });
                                }
                            }
                        }
                        else if (_Request.Type == "month")
                        {

                            string[] Months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
                            int[] MonthIndexes = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

                            var _SalesDetails = _OSalesItem
                                                 .GroupBy(x => new
                                                 {
                                                     //x.UserAccountId,
                                                     x.TransactionDateT.Month,
                                                 })
                                                 .Select(x => new OAnalytics.Sale
                                                 {
                                                     Month = x.Key.Month,
                                                     TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                                     TotalTransaction = x.Count(),
                                                     TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                                     NewVisits = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() == 1).Count(),
                                                     RepeatVisits = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() > 1).Count(),
                                                 })
                                                 .ToList();

                            foreach (var MonthIndex in MonthIndexes)
                            {
                                var ItemInfo = _SalesDetails.Where(x => x.Month == MonthIndex).FirstOrDefault();
                                if (ItemInfo != null)
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        Month = MonthIndex,
                                        TotalCustomer = ItemInfo.TotalCustomer,
                                        TotalTransaction = ItemInfo.TotalTransaction,
                                        TotalInvoiceAmount = ItemInfo.TotalInvoiceAmount,
                                        NewVisits = ItemInfo.NewVisits,
                                        RepeatVisits = ItemInfo.RepeatVisits
                                    });
                                }
                                else
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        Month = MonthIndex,
                                        TotalCustomer = 0,
                                        TotalTransaction = 0,
                                        TotalInvoiceAmount = 0,
                                        RepeatVisits = 0,
                                        NewVisits = 0
                                    });
                                }
                            }
                        }
                        else if (_Request.Type == "year")
                        {


                            var start = _Request.StartDate;
                            var end = _Request.EndDate;

                            // set end-date to end of month
                            end = new DateTime(end.Value.Year, end.Value.Month, DateTime.DaysInMonth(end.Value.Year, end.Value.Month));

                            var diff = Enumerable.Range(0, Int32.MaxValue)
                                                 .Select(e => start.Value.AddYears(e))
                                                 .TakeWhile(e => e <= end)
                                                 .Select(e => e.Year).ToList();



                            var _SalesDetails = _OSalesItem
                                             .GroupBy(x => new
                                             {
                                                 x.TransactionDateT.Year,
                                                 //x.TransactionDateT.Month,
                                             })
                                             .Select(x => new OAnalytics.Sale
                                             {
                                                 //Month = x.Key.Month,
                                                 Year = x.Key.Year,
                                                 TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                                 TotalTransaction = x.Count(),
                                                 TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                                 NewVisits = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() == 1).Count(),
                                                 RepeatVisits = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() > 1).Count(),
                                             })
                                             .ToList();
                            foreach (var year in diff)
                            {
                                var ItemInfo = _SalesDetails.Where(x => x.Year == year).FirstOrDefault();
                                if (ItemInfo != null)
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        Year = year,
                                        TotalCustomer = ItemInfo.TotalCustomer,
                                        TotalTransaction = ItemInfo.TotalTransaction,
                                        TotalInvoiceAmount = ItemInfo.TotalInvoiceAmount,
                                        NewVisits = ItemInfo.NewVisits,
                                        RepeatVisits = ItemInfo.RepeatVisits
                                    });
                                }
                                else
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        Year = year,
                                        TotalCustomer = 0,
                                        TotalTransaction = 0,
                                        TotalInvoiceAmount = 0,
                                        RepeatVisits = 0,
                                        NewVisits = 0
                                    });
                                }
                            }

                        }
                        else
                        {
                            int Days = (_Request.EndDate - _Request.StartDate).Value.Days;
                            DateTime? StartDate = _Request.StartDate.Value.Date;
                            if (Days == 0)
                            {
                                _SalesHistory.Add(new OAnalytics.Sale
                                {
                                    Date = StartDate.Value.ToString("dd-MM-yyyy"),
                                    TotalCustomer = 0,
                                    TotalTransaction = 0,
                                    TotalInvoiceAmount = 0,
                                    RepeatVisits = 0,
                                    NewVisits = 0
                                });
                            }
                            for (int i = 0; i < Days; i++)
                            {
                                _SalesHistory.Add(new OAnalytics.Sale
                                {
                                    Date = StartDate.Value.ToString("dd-MM-yyyy"),
                                    TotalCustomer = 0,
                                    TotalTransaction = 0,
                                    TotalInvoiceAmount = 0,
                                    RepeatVisits = 0,
                                    NewVisits = 0
                                });
                                StartDate = StartDate.Value.AddDays(1);
                            }
                            var _SalesDetails = _OSalesItem
                                           .GroupBy(x => new
                                           {
                                               x.TransactionDateT.Date,
                                           })
                                           .Select(x => new OAnalytics.Sale
                                           {
                                               Date = x.Key.Date.ToString("dd-MM-yyyy"),
                                               TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                               TotalTransaction = x.Count(),
                                               TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                               NewVisits = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() == 1).Count(),
                                               RepeatVisits = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() > 1).Count(),
                                           })
                                           .ToList();
                            foreach (var _SalesHistoryItem in _SalesHistory)
                            {
                                var ItemInfo = _SalesDetails.Where(x => x.Date == _SalesHistoryItem.Date).FirstOrDefault();
                                if (ItemInfo != null)
                                {
                                    _SalesHistoryItem.TotalCustomer = ItemInfo.TotalCustomer;
                                    _SalesHistoryItem.TotalTransaction = ItemInfo.TotalTransaction;
                                    _SalesHistoryItem.TotalInvoiceAmount = ItemInfo.TotalInvoiceAmount;
                                    _SalesHistoryItem.NewVisits = ItemInfo.NewVisits;
                                    _SalesHistoryItem.RepeatVisits = ItemInfo.RepeatVisits;
                                }
                            }




                            //_SalesHistory = _HCoreContextOperations.HCOAccountSalesHistory
                            //                     .Where(x => x.AccountId == _Request.AccountId && x.Date > _Request.StartDate && x.Date < _Request.EndDate)
                            //                           .GroupBy(x => x.UserAccountId)
                            //                           .Select(x => new OAnalytics.Sale
                            //                           {
                            //                               //UserAccountId = x.Key,
                            //                               //UserAccountTypeId = x.Select(m => m.UserAccountTypeId).FirstOrDefault(),
                            //                               //MerchantId = x.Select(m => m.MerchantId).FirstOrDefault(),
                            //                               //StoreId = x.Select(m => m.StoreId).FirstOrDefault(),
                            //                               //CashierId = x.Select(m => m.CashierId).FirstOrDefault(),
                            //                               //TerminalId = x.Select(m => m.TerminalId).FirstOrDefault(),
                            //                               //BankId = x.Select(m => m.BankId).FirstOrDefault(),
                            //                               //PtspId = x.Select(m => m.PtspId).FirstOrDefault(),
                            //                               //ManagerId = x.Select(m => m.ManagerId).FirstOrDefault(),
                            //                               //RmId = x.Select(m => m.RmId).FirstOrDefault(),
                            //                               //TotalUser = x.Sum(m => m.TotalUser),
                            //                               TotalTransaction = x.Sum(m => m.TotalTransaction),
                            //                               TotalInvoiceAmount = x.Sum(m => m.TotalInvoiceAmount),
                            //                               //CardTransactionUser = x.Sum(m => m.CardTransactionUser),
                            //                               //CardTransaction = x.Sum(m => m.CardTransaction),
                            //                               //CardInvoiceAmount = x.Sum(m => m.CardInvoiceAmount),
                            //                               //CashTransactionUser = x.Sum(m => m.CashTransactionUser),
                            //                               //CashTransaction = x.Sum(m => m.CashTransaction),
                            //                               //CashInvoiceAmount = x.Sum(m => m.CashInvoiceAmount),
                            //                               //SuccessfulTransaction = x.Sum(m => m.SuccessfulTransaction),
                            //                               //SuccessfulTransactionInvoiceAmount = x.Sum(m => m.SuccessfulTransactionInvoiceAmount),
                            //                               //SuccessfulTransactionUser = x.Sum(m => m.SuccessfulTransactionUser),
                            //                               //FailedTransaction = x.Sum(m => m.FailedTransaction),
                            //                               //FailedTransactionInvoiceAmount = x.Sum(m => m.FailedTransactionInvoiceAmount),
                            //                               //FailedTransactionUser = x.Sum(m => m.FailedTransactionUser),
                            //                           })
                            //                          //.OrderBy(_Request.SortExpression)
                            //                          //.Skip(_Request.Offset)
                            //                          //.Take(_Request.Limit)
                            //                          .ToList();
                        }
                        #endregion
                        #region Create  Response Object
                        _HCoreContextOperations.Dispose();
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SalesHistory, "HC0001");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GeSalesSummary", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the loyalty overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLoyaltyOverview(OAnalytics.Request _Request)
        {
            _LoyaltyOverview = new OAnalytics.Loyalty();
            try
            {
                if (_Request.CustomerReferenceId > 0)
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        _LoyaltyOverview.Transaction = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.AccountId == _Request.CustomerReferenceId
                                                                && m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                              ).Count();
                        _LoyaltyOverview.TransactionInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.AccountId == _Request.CustomerReferenceId
                                                            && m.ParentId == _Request.AccountId
                                                            && m.TransactionDate > _Request.StartDate
                                                            && m.TransactionDate < _Request.EndDate
                                                            && m.TotalAmount > 0
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                        _LoyaltyOverview.RewardTransaction = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.AccountId == _Request.CustomerReferenceId
                                                            && m.ParentId == _Request.AccountId
                                                            && m.TransactionDate > _Request.StartDate
                                                            && m.TransactionDate < _Request.EndDate
                                                            && m.TotalAmount > 0
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                            && (((m.ModeId == TransactionMode.Credit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          ).Count();
                        if (_LoyaltyOverview.RewardTransaction > 0)
                        {
                            _LoyaltyOverview.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.AccountId == _Request.CustomerReferenceId
                                                            && m.ParentId == _Request.AccountId
                                                            && m.TransactionDate > _Request.StartDate
                                                            && m.TransactionDate < _Request.EndDate
                                                            && m.TotalAmount > 0
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                            && (((m.ModeId == TransactionMode.Credit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           ).Sum(x => (double?)x.TotalAmount) ?? 0;
                            _LoyaltyOverview.RewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.AccountId == _Request.CustomerReferenceId
                                                            && m.ParentId == _Request.AccountId
                                                            && m.TransactionDate > _Request.StartDate
                                                            && m.TransactionDate < _Request.EndDate
                                                            && m.TotalAmount > 0
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                            && (((m.ModeId == TransactionMode.Credit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                        }
                        _LoyaltyOverview.TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.AccountId == _Request.CustomerReferenceId
                                                            && m.ParentId == _Request.AccountId
                                                            && m.TransactionDate > _Request.StartDate
                                                            && m.TransactionDate < _Request.EndDate
                                                            && m.TotalAmount > 0
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                            && (m.ModeId == TransactionMode.Credit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                          ).Count();
                        if (_LoyaltyOverview.TucRewardTransaction > 0)
                        {
                            _LoyaltyOverview.TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.AccountId == _Request.CustomerReferenceId
                                                            && m.ParentId == _Request.AccountId
                                                            && m.TransactionDate > _Request.StartDate
                                                            && m.TransactionDate < _Request.EndDate
                                                            && m.TotalAmount > 0
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                            && (m.ModeId == TransactionMode.Credit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                          ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _LoyaltyOverview.TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.AccountId == _Request.CustomerReferenceId
                                                            && m.ParentId == _Request.AccountId
                                                            && m.TransactionDate > _Request.StartDate
                                                            && m.TransactionDate < _Request.EndDate
                                                            && m.TotalAmount > 0
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                            && (m.ModeId == TransactionMode.Credit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                          ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                        }
                        _LoyaltyOverview.TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.AccountId == _Request.CustomerReferenceId
                                                            && m.ParentId == _Request.AccountId
                                                            && m.TransactionDate > _Request.StartDate
                                                            && m.TransactionDate < _Request.EndDate
                                                            && m.TotalAmount > 0
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                          ).Count();
                        if (_LoyaltyOverview.TucPlusRewardTransaction > 0)
                        {
                            _LoyaltyOverview.TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.AccountId == _Request.CustomerReferenceId
                                                            && m.ParentId == _Request.AccountId
                                                            && m.TransactionDate > _Request.StartDate
                                                            && m.TransactionDate < _Request.EndDate
                                                            && m.TotalAmount > 0
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                         ).Sum(x => (double?)x.TotalAmount) ?? 0;
                            _LoyaltyOverview.TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.AccountId == _Request.CustomerReferenceId
                                                            && m.ParentId == _Request.AccountId
                                                            && m.TransactionDate > _Request.StartDate
                                                            && m.TransactionDate < _Request.EndDate
                                                            && m.TotalAmount > 0
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                         ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                        }


                        _LoyaltyOverview.TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                           .Count(m => m.AccountId == _Request.CustomerReferenceId
                                                            && m.ParentId == _Request.AccountId
                                                            && m.TransactionDate > _Request.StartDate
                                                            && m.TransactionDate < _Request.EndDate
                                                            && m.TotalAmount > 0
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.ThankUCashPlus
                                                         );
                        if (_LoyaltyOverview.TucPlusRewardClaimTransaction > 0)
                        {
                            _LoyaltyOverview.TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.AccountId == _Request.CustomerReferenceId
                                                            && m.ParentId == _Request.AccountId
                                                            && m.TransactionDate > _Request.StartDate
                                                            && m.TransactionDate < _Request.EndDate
                                                            && m.TotalAmount > 0
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.ThankUCashPlus
                                                         ).Sum(x => (double?)x.TotalAmount) ?? 0;

                            _LoyaltyOverview.TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.AccountId == _Request.CustomerReferenceId
                                                            && m.ParentId == _Request.AccountId
                                                            && m.TransactionDate > _Request.StartDate
                                                            && m.TransactionDate < _Request.EndDate
                                                            && m.TotalAmount > 0
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.ThankUCashPlus
                                                         ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                        }
                        _LoyaltyOverview.RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                           .Count(m => m.AccountId == _Request.CustomerReferenceId
                                                            && m.ParentId == _Request.AccountId
                                                            && m.TransactionDate > _Request.StartDate
                                                            && m.TransactionDate < _Request.EndDate
                                                            && m.TotalAmount > 0
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                            && m.ModeId == TransactionMode.Debit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                         );
                        if (_LoyaltyOverview.RedeemTransaction > 0)
                        {
                            _LoyaltyOverview.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.AccountId == _Request.CustomerReferenceId
                                                            && m.ParentId == _Request.AccountId
                                                            && m.TransactionDate > _Request.StartDate
                                                            && m.TransactionDate < _Request.EndDate
                                                            && m.TotalAmount > 0
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                            && m.ModeId == TransactionMode.Debit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                         ).Sum(x => (double?)x.TotalAmount) ?? 0;
                            _LoyaltyOverview.RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.AccountId == _Request.CustomerReferenceId
                                                            && m.ParentId == _Request.AccountId
                                                            && m.TransactionDate > _Request.StartDate
                                                            && m.TransactionDate < _Request.EndDate
                                                            && m.TotalAmount > 0
                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                             && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                            && m.ModeId == TransactionMode.Debit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                         ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                        }
                    }
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.StoreReferenceId != 0)
                        {
                            var Transactions = _HCoreContext.HCUAccountTransaction
                                         .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                         && x.ParentId == _Request.AccountId
                                         && x.SubParentId == _Request.StoreReferenceId
                                         && x.TransactionDate > _Request.StartDate
                                         && x.TransactionDate < _Request.EndDate
                                         && x.ModeId == TransactionMode.Credit
                                         && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack))
                                         .Select(x => new
                                         {
                                             UserAccountId = x.AccountId,
                                             InvoiceAmount = x.PurchaseAmount
                                         }).ToList();

                            var TM = Transactions
                           .GroupBy(x => x.UserAccountId)
                           .Select(x => new
                           {
                               UserAccountId = x.Key,
                               Transactions = x.Count(),
                               InvoiceAmount = x.Sum(a => a.InvoiceAmount),
                           }).ToList();
                            _LoyaltyOverview.NewCustomers = TM.Count(x => x.Transactions < 2);
                            _LoyaltyOverview.NewCustomerInvoiceAmount = TM.Where(x => x.Transactions < 2).Sum(x => x.InvoiceAmount);
                            _LoyaltyOverview.RepeatingCustomers = TM.Count(x => x.Transactions > 1);
                            _LoyaltyOverview.RepeatingCustomerInvoiceAmount = TM.Where(x => x.Transactions > 1).Sum(x => x.InvoiceAmount);
                            _LoyaltyOverview.VisitsByRepeatingCustomers = TM.Where(x => x.Transactions > 1).Count();
                            _LoyaltyOverview.VisitsByNewCustomers = TM.Where(x => x.Transactions < 2).Sum(x => x.Transactions);
                            _LoyaltyOverview.AllVisit = _LoyaltyOverview.VisitsByNewCustomers + _LoyaltyOverview.VisitsByRepeatingCustomers;
                            _LoyaltyOverview.TotalTimeofVisit = 0;

                            _LoyaltyOverview.Transaction = Transactions.Count();
                            _LoyaltyOverview.TransactionInvoiceAmount = Transactions.Sum(x => x.InvoiceAmount);
                            _LoyaltyOverview.RewardTransaction = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.SubParentId == _Request.StoreReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (((m.ModeId == TransactionMode.Credit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                              ).Count();
                            if (_LoyaltyOverview.RewardTransaction > 0)
                            {
                                _LoyaltyOverview.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.SubParentId == _Request.StoreReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (((m.ModeId == TransactionMode.Credit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                               ).Sum(x => (double?)x.TotalAmount) ?? 0;
                                _LoyaltyOverview.RewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.SubParentId == _Request.StoreReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (((m.ModeId == TransactionMode.Credit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                               ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }
                            _LoyaltyOverview.TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.SubParentId == _Request.StoreReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (m.ModeId == TransactionMode.Credit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                              ).Count();
                            if (_LoyaltyOverview.TucRewardTransaction > 0)
                            {
                                _LoyaltyOverview.TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.SubParentId == _Request.StoreReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (m.ModeId == TransactionMode.Credit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                              ).Sum(x => (double?)x.TotalAmount) ?? 0;

                                _LoyaltyOverview.TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.SubParentId == _Request.StoreReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (m.ModeId == TransactionMode.Credit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                              ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }
                            _LoyaltyOverview.TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.SubParentId == _Request.StoreReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                              ).Count();
                            if (_LoyaltyOverview.TucPlusRewardTransaction > 0)
                            {
                                _LoyaltyOverview.TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.SubParentId == _Request.StoreReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;
                                _LoyaltyOverview.TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.SubParentId == _Request.StoreReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                             ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }


                            _LoyaltyOverview.TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                               .Count(m => m.ParentId == _Request.AccountId
                                                                && m.SubParentId == _Request.StoreReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.ThankUCashPlus
                                                             );
                            if (_LoyaltyOverview.TucPlusRewardClaimTransaction > 0)
                            {
                                _LoyaltyOverview.TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.SubParentId == _Request.StoreReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.ThankUCashPlus
                                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;

                                _LoyaltyOverview.TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.SubParentId == _Request.StoreReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.ThankUCashPlus
                                                             ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }
                            _LoyaltyOverview.RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                               .Count(m => m.ParentId == _Request.AccountId
                                                                && m.SubParentId == _Request.StoreReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == TransactionMode.Debit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                             );
                            if (_LoyaltyOverview.RedeemTransaction > 0)
                            {
                                _LoyaltyOverview.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.SubParentId == _Request.StoreReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == TransactionMode.Debit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;
                                _LoyaltyOverview.RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.SubParentId == _Request.StoreReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == TransactionMode.Debit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                             ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }
                        }
                        else if (_Request.CashierReferenceId != 0)
                        {
                            var Transactions = _HCoreContext.HCUAccountTransaction
                                         .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                         && x.ParentId == _Request.AccountId
                                         && x.CashierId == _Request.CashierReferenceId
                                         && x.TransactionDate > _Request.StartDate
                                         && x.TransactionDate < _Request.EndDate
                                         && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                         && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay))
                                         .Select(x => new
                                         {
                                             UserAccountId = x.AccountId,
                                             InvoiceAmount = x.PurchaseAmount
                                         }).ToList();

                            var TM = Transactions.GroupBy(x => x.UserAccountId)
                           .Select(x => new
                           {
                               UserAccountId = x.Key,
                               Count = x.Count(),
                               InvoiceAmount = x.Sum(a => a.InvoiceAmount),

                           }).ToList();
                            _LoyaltyOverview.NewCustomers = TM.Count(x => x.Count < 2);
                            _LoyaltyOverview.NewCustomerInvoiceAmount = TM.Where(x => x.Count < 2).Sum(x => x.InvoiceAmount);
                            _LoyaltyOverview.RepeatingCustomers = TM.Count(x => x.Count > 1);
                            _LoyaltyOverview.VisitsByRepeatingCustomers = TM.Where(x => x.Count > 1).Count();
                            _LoyaltyOverview.VisitsByNewCustomers = TM.Where(x => x.Count < 2).Sum(x => x.Count);
                            _LoyaltyOverview.AllVisit = _LoyaltyOverview.VisitsByNewCustomers + _LoyaltyOverview.VisitsByRepeatingCustomers;
                            _LoyaltyOverview.TotalTimeofVisit = 0;
                            _LoyaltyOverview.RepeatingCustomerInvoiceAmount = TM.Where(x => x.Count > 1).Sum(x => x.InvoiceAmount);
                            _LoyaltyOverview.Transaction = Transactions.Count();
                            _LoyaltyOverview.TransactionInvoiceAmount = Transactions.Sum(x => x.InvoiceAmount);
                            _LoyaltyOverview.RewardTransaction = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                         && m.CashierId == _Request.CashierReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (((m.ModeId == TransactionMode.Credit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                              ).Count();
                            if (_LoyaltyOverview.RewardTransaction > 0)
                            {
                                _LoyaltyOverview.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                         && m.CashierId == _Request.CashierReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (((m.ModeId == TransactionMode.Credit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                               ).Sum(x => (double?)x.TotalAmount) ?? 0;

                                _LoyaltyOverview.RewardCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.ParentId == _Request.AccountId
                                         && m.CashierId == _Request.CashierReferenceId
                                                              && m.TransactionDate > _Request.StartDate
                                                              && m.TransactionDate < _Request.EndDate
                                                              && m.TotalAmount > 0
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                              && (((m.ModeId == TransactionMode.Credit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                             ).Sum(x => (double?)x.ParentTransaction.ComissionAmount) ?? 0;
                                _LoyaltyOverview.RewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                         && m.CashierId == _Request.CashierReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (((m.ModeId == TransactionMode.Credit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                               ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }
                            _LoyaltyOverview.TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                         && m.CashierId == _Request.CashierReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (m.ModeId == TransactionMode.Credit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                              ).Count();
                            if (_LoyaltyOverview.TucRewardTransaction > 0)
                            {
                                _LoyaltyOverview.TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                         && m.CashierId == _Request.CashierReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (m.ModeId == TransactionMode.Credit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                              ).Sum(x => (double?)x.TotalAmount) ?? 0;

                                _LoyaltyOverview.TucRewardCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.ParentId == _Request.AccountId
                                         && m.CashierId == _Request.CashierReferenceId
                                                               && m.TransactionDate > _Request.StartDate
                                                               && m.TransactionDate < _Request.EndDate
                                                               && m.TotalAmount > 0
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                               && (m.ModeId == TransactionMode.Credit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                             ).Sum(x => (double?)x.ParentTransaction.ComissionAmount) ?? 0;

                                _LoyaltyOverview.TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                         && m.CashierId == _Request.CashierReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (m.ModeId == TransactionMode.Credit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                              ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }

                            _LoyaltyOverview.TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                         && m.CashierId == _Request.CashierReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                              ).Count();
                            if (_LoyaltyOverview.TucPlusRewardTransaction > 0)
                            {
                                _LoyaltyOverview.TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;

                                _LoyaltyOverview.TucPlusRewardCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.ParentId == _Request.AccountId
                                         && m.CashierId == _Request.CashierReferenceId
                                                              && m.TransactionDate > _Request.StartDate
                                                              && m.TransactionDate < _Request.EndDate
                                                              && m.TotalAmount > 0
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                              && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                           ).Sum(x => (double?)x.ParentTransaction.ComissionAmount) ?? 0;
                                _LoyaltyOverview.TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                             ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }


                            _LoyaltyOverview.TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                               .Count(m => m.ParentId == _Request.AccountId
                                         && m.CashierId == _Request.CashierReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.ThankUCashPlus
                                                             );
                            if (_LoyaltyOverview.TucPlusRewardClaimTransaction > 0)
                            {
                                _LoyaltyOverview.TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.ThankUCashPlus
                                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;

                                _LoyaltyOverview.TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                         && m.CashierId == _Request.CashierReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.ThankUCashPlus
                                                             ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }




                            _LoyaltyOverview.RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                               .Count(m => m.ParentId == _Request.AccountId
                                         && m.CashierId == _Request.CashierReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == TransactionMode.Debit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                             );
                            if (_LoyaltyOverview.RedeemTransaction > 0)
                            {
                                _LoyaltyOverview.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                         && m.CashierId == _Request.CashierReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == TransactionMode.Debit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;
                                _LoyaltyOverview.RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                         && m.CashierId == _Request.CashierReferenceId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == TransactionMode.Debit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                             ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }
                        }
                        else
                        {
                            var Transactions = _HCoreContext.HCUAccountTransaction
                                         .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                         && x.ParentId == _Request.AccountId
                                         && x.TransactionDate > _Request.StartDate
                                         && x.TransactionDate < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                         && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                         && x.TypeId != TransactionType.ThankUCashPlusCredit)
                                         .Select(x => new
                                         {
                                             UserAccountId = x.AccountId,
                                             InvoiceAmount = x.PurchaseAmount
                                         }).ToList();
                            var TM = Transactions.GroupBy(x => x.UserAccountId)
                                       .Select(x => new
                                       {
                                           UserAccountId = x.Key,
                                           Count = x.Count(),
                                           InvoiceAmount = x.Sum(a => a.InvoiceAmount),
                                       }).ToList();
                            _LoyaltyOverview.NewCustomers = TM.Count(x => x.Count < 2);
                            _LoyaltyOverview.NewCustomerInvoiceAmount = TM.Where(x => x.Count < 2).Sum(x => x.InvoiceAmount);
                            _LoyaltyOverview.RepeatingCustomers = TM.Count(x => x.Count > 1);
                            _LoyaltyOverview.VisitsByRepeatingCustomers = TM.Where(x => x.Count > 1).Count();
                            _LoyaltyOverview.VisitsByNewCustomers = TM.Where(x => x.Count < 2).Sum(x => x.Count);
                            _LoyaltyOverview.RepeatingCustomerInvoiceAmount = TM.Where(x => x.Count > 1).Sum(x => x.InvoiceAmount);
                            _LoyaltyOverview.Transaction = Transactions.Count();
                            _LoyaltyOverview.AllVisit = _LoyaltyOverview.VisitsByNewCustomers + _LoyaltyOverview.VisitsByRepeatingCustomers;
                            _LoyaltyOverview.TotalTimeofVisit = 0;
                            _LoyaltyOverview.TransactionInvoiceAmount = Transactions.Sum(x => x.InvoiceAmount);
                            _LoyaltyOverview.RewardTransaction = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (((m.ModeId == TransactionMode.Credit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                              ).Count();
                            if (_LoyaltyOverview.RewardTransaction > 0)
                            {
                                _LoyaltyOverview.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (((m.ModeId == TransactionMode.Credit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                )
                                                               .Sum(x => (double?)x.ReferenceAmount) ?? 0;

                                _LoyaltyOverview.RewardCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.ParentId == _Request.AccountId
                                                              && m.TransactionDate > _Request.StartDate
                                                              && m.TransactionDate < _Request.EndDate
                                                              && m.TotalAmount > 0
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                              && (((m.ModeId == TransactionMode.Credit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                             ).Sum(x => (double?)x.ParentTransaction.ComissionAmount) ?? 0;
                                _LoyaltyOverview.RewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (((m.ModeId == TransactionMode.Credit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                               ).Sum(x => (double?)x.ReferenceInvoiceAmount) ?? 0;
                            }
                            _LoyaltyOverview.TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (m.ModeId == TransactionMode.Credit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                              ).Count();
                            if (_LoyaltyOverview.TucRewardTransaction > 0)
                            {
                                _LoyaltyOverview.TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (m.ModeId == TransactionMode.Credit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                              ).Sum(x => (double?)x.TotalAmount) ?? 0;

                                _LoyaltyOverview.TucRewardCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(m => m.ParentId == _Request.AccountId
                                                               && m.TransactionDate > _Request.StartDate
                                                               && m.TransactionDate < _Request.EndDate
                                                               && m.TotalAmount > 0
                                                               && m.StatusId == HelperStatus.Transaction.Success
                                                               && (m.ModeId == TransactionMode.Credit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                             ).Sum(x => (double?)x.ParentTransaction.ComissionAmount) ?? 0;

                                _LoyaltyOverview.TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (m.ModeId == TransactionMode.Credit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                              ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }

                            _LoyaltyOverview.TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                              ).Count();
                            if (_LoyaltyOverview.TucPlusRewardTransaction > 0)
                            {
                                _LoyaltyOverview.TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;

                                _LoyaltyOverview.TucPlusRewardCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.ParentId == _Request.AccountId
                                                              && m.TransactionDate > _Request.StartDate
                                                              && m.TransactionDate < _Request.EndDate
                                                              && m.TotalAmount > 0
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                              && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                           ).Sum(x => (double?)x.ParentTransaction.ComissionAmount) ?? 0;
                                _LoyaltyOverview.TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                             ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }


                            _LoyaltyOverview.TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                               .Count(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.ThankUCashPlus
                                                             );
                            if (_LoyaltyOverview.TucPlusRewardClaimTransaction > 0)
                            {
                                _LoyaltyOverview.TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.ThankUCashPlus
                                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;

                                _LoyaltyOverview.TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.ThankUCashPlus
                                                             ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }




                            _LoyaltyOverview.RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                               .Count(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == TransactionMode.Debit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                             );
                            if (_LoyaltyOverview.RedeemTransaction > 0)
                            {
                                _LoyaltyOverview.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == TransactionMode.Debit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;
                                _LoyaltyOverview.RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.ParentId == _Request.AccountId
                                                                && m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == TransactionMode.Debit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                             ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }
                        }
                    }
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _LoyaltyOverview, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetLoyaltyOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// This APi will return the Customers visits count s per date selected and it will show how many times customers has visited to taht merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLoyaltyVisitHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    long DaysDifference = (_Request.EndDate.Value.Date - _Request.StartDate.Value.Date).Days;
                    _OTrList = new List<OTrList>();
                    if (_Request.CustomerReferenceId > 0)
                    {
                        _OTrList = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                          && x.ParentId == _Request.AccountId
                                          && x.AccountId == _Request.CustomerReferenceId
                                          && x.TransactionDate > _Request.StartDate
                                          && x.TransactionDate < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                          && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                          && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay))
                                          .Select(x => new OTrList
                                          {
                                              TransactionDate = x.TransactionDate.AddHours(1),
                                              InvoiceAmount = x.PurchaseAmount
                                          }).ToList();
                    }
                    else if (_Request.StoreReferenceId > 0)
                    {
                        _OTrList = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                          && x.ParentId == _Request.AccountId
                                          && x.SubParentId == _Request.StoreReferenceId
                                          && x.TransactionDate > _Request.StartDate
                                          && x.TransactionDate < _Request.EndDate
                                                && x.StatusId == HelperStatus.Transaction.Success
                                          && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                          && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay))
                                          .Select(x => new OTrList
                                          {
                                              TransactionDate = x.TransactionDate.AddHours(1),
                                              UserAccountId = x.AccountId,
                                              InvoiceAmount = x.PurchaseAmount
                                          }).ToList();
                    }
                    else
                    {
                        _OTrList = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                        && x.ParentId == _Request.AccountId
                                        && x.TransactionDate > _Request.StartDate
                                        && x.TransactionDate < _Request.EndDate
                                       && x.StatusId == HelperStatus.Transaction.Success
                                        && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack))
                                       .Select(x => new OTrList
                                       {
                                           TransactionDate = x.TransactionDate.AddHours(1),
                                           UserAccountId = x.AccountId,
                                           InvoiceAmount = x.PurchaseAmount
                                       }).ToList();
                    }
                    if (_Request.CustomerReferenceId > 0)
                    {
                        if (DaysDifference > 365)
                        {
                            var TM = _OTrList.GroupBy(x => x.TransactionDate.Date.Year)
                             .Select(x => new OAnalytics.SalesMetrics
                             {
                                 Title = x.Key.ToString(),
                                 Year = x.Key,
                                 TotalTransaction = x.Count(),
                                 TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                             }).ToList();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                        }
                        else if (DaysDifference < 366 && DaysDifference > 31)
                        {
                            var TM = _OTrList.GroupBy(x => new
                            {
                                x.TransactionDate.Date.Year,
                                x.TransactionDate.Date.Month,
                            })
                           .Select(x => new OAnalytics.SalesMetrics
                           {
                               Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
                               Year = x.Key.Year,
                               Month = x.Key.Month,
                               TotalTransaction = x.Count(),
                               TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                           }).ToList();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                        }
                        else
                        {
                            var TM = _OTrList.GroupBy(x => x.TransactionDate.Date)
                             .Select(x => new OAnalytics.SalesMetrics
                             {
                                 Date = x.Key,
                                 TotalTransaction = x.Count(),
                                 TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                             }).ToList();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                        }
                    }
                    else
                    {
                        if (DaysDifference > 365)
                        {
                            //var TM = _OTrList.GroupBy(x => x.TransactionDate.Date.Year)
                            // .Select(x => new OAnalytics.SalesMetrics
                            // {
                            //     Title = x.Key.ToString(),
                            //     Year = x.Key,
                            //     TotalTransaction = x.Count(),
                            //     TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                            // }).ToList();
                            //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                            var TM = _OTrList.GroupBy(x => x.TransactionDate.Year)
                           .Select(x => new OAnalytics.SalesMetrics
                           {
                               Title = x.Key.ToString(),
                               Year = x.Key,
                               TotalTransaction = x.Count(),
                               TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                               TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                               NewCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() == 1).Count(),
                               RepeatingCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() > 1).Count(),
                           }).ToList();
                            foreach (var item in TM)
                            {
                                item.NewCustomer = _OTrList
                                    .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
                                    .GroupBy(x => x.UserAccountId)
                                    .Where(x => x.Count() < 2).Count();

                                item.NewCustomerInvoiceAmount = _OTrList
                                   .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
                                   .GroupBy(x => x.UserAccountId)
                                   .Where(x => x.Count() < 2)
                                   .Select(x => new
                                   {
                                       UserAccount = x.Key,
                                       InvoiceAmount = x.Sum(d => d.InvoiceAmount)
                                   }).Sum(g => g.InvoiceAmount);

                                item.RepeatingCustomer = _OTrList
                                    .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
                                    .GroupBy(x => x.UserAccountId)
                                    .Where(x => x.Count() > 1)
                                    .Count();

                                item.RepeatingCustomerInvoiceAmount = _OTrList
                                  .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
                                  .GroupBy(x => x.UserAccountId)
                                  .Where(x => x.Count() > 1)
                                  .Select(x => new
                                  {
                                      UserAccount = x.Key,
                                      InvoiceAmount = x.Sum(d => d.InvoiceAmount)
                                  }).Sum(g => g.InvoiceAmount);
                                item.VisitsByRepeatingCustomers = _OTrList
                                 .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
                                 .GroupBy(x => x.UserAccountId)
                                 .Where(x => x.Count() > 1)
                                 .Sum(x => x.Count());
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                            #endregion
                        }
                        else if (DaysDifference < 366 && DaysDifference > 31)
                        {
                            //var TM = _OTrList.GroupBy(x => new
                            //{
                            //    x.TransactionDate.Date.Year,
                            //    x.TransactionDate.Date.Month,
                            //})
                            //.Select(x => new OAnalytics.SalesMetrics
                            //{
                            //    Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
                            //    Year = x.Key.Year,
                            //    Month = x.Key.Month,
                            //    TotalTransaction = x.Count(),
                            //    TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                            //}).ToList();
                            // return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");


                            var TM = _OTrList.GroupBy(x => new
                            {
                                x.TransactionDate.Date.Year,
                                x.TransactionDate.Date.Month,
                            })
                         .Select(x => new OAnalytics.SalesMetrics
                         {
                             Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
                             Year = x.Key.Year,
                             Month = x.Key.Month,
                             TotalTransaction = x.Count(),
                             TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                             NewCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() == 1).Count(),
                             RepeatingCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() > 1).Count(),
                         }).ToList();
                            foreach (var item in TM)
                            {
                                item.NewCustomer = _OTrList
                                    .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
                                    .GroupBy(x => x.UserAccountId)
                                    .Where(x => x.Count() < 2).Count();

                                item.NewCustomerInvoiceAmount = _OTrList
                                    .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
                                   .GroupBy(x => x.UserAccountId)
                                   .Where(x => x.Count() < 2)
                                   .Select(x => new
                                   {
                                       UserAccount = x.Key,
                                       InvoiceAmount = x.Sum(d => d.InvoiceAmount)
                                   }).Sum(g => g.InvoiceAmount);

                                item.RepeatingCustomer = _OTrList
                                    .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
                                    .GroupBy(x => x.UserAccountId)
                                    .Where(x => x.Count() > 1)
                                    .Count();

                                item.RepeatingCustomerInvoiceAmount = _OTrList
                                    .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
                                  .GroupBy(x => x.UserAccountId)
                                  .Where(x => x.Count() > 1)
                                  .Select(x => new
                                  {
                                      UserAccount = x.Key,
                                      InvoiceAmount = x.Sum(d => d.InvoiceAmount)
                                  }).Sum(g => g.InvoiceAmount);
                                item.VisitsByRepeatingCustomers = _OTrList
                                    .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
                                 .GroupBy(x => x.UserAccountId)
                                 .Where(x => x.Count() > 1)
                                 .Sum(x => x.Count());
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                            #endregion
                        }
                        else if (DaysDifference < 2)
                        {
                            var TM = _OTrList.GroupBy(x => x.TransactionDate.Hour)
                             .Select(x => new OAnalytics.SalesMetrics
                             {
                                 Title = x.Key.ToString(),
                                 Hour = x.Key,
                                 TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                 TotalTransaction = x.Count(),
                                 TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                             }).ToList();
                            foreach (var item in TM)
                            {
                                var hours = Convert.ToInt32(item.Title);
                                int minutes = 0;
                                var amPmDesignator = "AM";
                                if (hours == 0)
                                    hours = 12;
                                else if (hours == 12)
                                    amPmDesignator = "PM";
                                else if (hours > 12)
                                {
                                    hours -= 12;
                                    amPmDesignator = "PM";
                                }
                                item.Title = String.Format("{0}:{1:00} {2}", hours, minutes, amPmDesignator);


                                item.NewCustomer = _OTrList
                                    .Where(x => x.TransactionDate.Hour == item.Hour)
                                    .GroupBy(x => x.UserAccountId)
                                    .Where(x => x.Count() < 2).Count();

                                item.NewCustomerInvoiceAmount = _OTrList
                                    .Where(x => x.TransactionDate.Hour == item.Hour)
                                   .GroupBy(x => x.UserAccountId)
                                   .Where(x => x.Count() < 2)
                                   .Select(x => new
                                   {
                                       UserAccount = x.Key,
                                       InvoiceAmount = x.Sum(d => d.InvoiceAmount)
                                   }).Sum(g => g.InvoiceAmount);

                                item.RepeatingCustomer = _OTrList
                                    .Where(x => x.TransactionDate.Hour == item.Hour)
                                    .GroupBy(x => x.UserAccountId)
                                    .Where(x => x.Count() > 1)
                                    .Count();

                                item.RepeatingCustomerInvoiceAmount = _OTrList
                                    .Where(x => x.TransactionDate.Hour == item.Hour)
                                  .GroupBy(x => x.UserAccountId)
                                  .Where(x => x.Count() > 1)
                                  .Select(x => new
                                  {
                                      UserAccount = x.Key,
                                      InvoiceAmount = x.Sum(d => d.InvoiceAmount)
                                  }).Sum(g => g.InvoiceAmount);
                                item.VisitsByRepeatingCustomers = _OTrList
                                    .Where(x => x.TransactionDate.Hour == item.Hour)
                                 .GroupBy(x => x.UserAccountId)
                                 .Where(x => x.Count() > 1)
                                 .Sum(x => x.Count());
                            }

                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                        }
                        else
                        {
                            var TM = _OTrList.GroupBy(x => x.TransactionDate.Date)
                             .Select(x => new OAnalytics.SalesMetrics
                             {
                                 Title = x.Key.ToString("dd-MM-yyyy"),
                                 Date = x.Key,
                                 TotalTransaction = x.Count(),
                                 TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                 TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                             }).ToList();

                            foreach (var item in TM)
                            {
                                item.NewCustomer = _OTrList
                                    .Where(x => x.TransactionDate.Date == item.Date)
                                    .GroupBy(x => x.UserAccountId)
                                    .Where(x => x.Count() < 2).Count();

                                item.NewCustomerInvoiceAmount = _OTrList
                                    .Where(x => x.TransactionDate.Date == item.Date)
                                   .GroupBy(x => x.UserAccountId)
                                   .Where(x => x.Count() < 2)
                                   .Select(x => new
                                   {
                                       UserAccount = x.Key,
                                       InvoiceAmount = x.Sum(d => d.InvoiceAmount)
                                   }).Sum(g => g.InvoiceAmount);

                                item.RepeatingCustomer = _OTrList
                                    .Where(x => x.TransactionDate.Date == item.Date)
                                    .GroupBy(x => x.UserAccountId)
                                    .Where(x => x.Count() > 1)
                                    .Count();

                                item.RepeatingCustomerInvoiceAmount = _OTrList
                                    .Where(x => x.TransactionDate.Date == item.Date)
                                  .GroupBy(x => x.UserAccountId)
                                  .Where(x => x.Count() > 1)
                                  .Select(x => new
                                  {
                                      UserAccount = x.Key,
                                      InvoiceAmount = x.Sum(d => d.InvoiceAmount)
                                  }).Sum(g => g.InvoiceAmount);
                                item.VisitsByRepeatingCustomers = _OTrList
                                    .Where(x => x.TransactionDate.Date == item.Date)
                                 .GroupBy(x => x.UserAccountId)
                                 .Where(x => x.Count() > 1)
                                 .Sum(x => x.Count());
                            }

                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetLoyaltyHistory", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the cashier pos history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCashierPosHistory(OAnalytics.Request _Request)
        {
            #region Manage Exception
            try
            {
                _SalesHistory = new List<OAnalytics.Sale>();
                #region Operation
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var TransactionsAll = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                            && x.TransactionDate > _Request.StartDate
                                                    && x.TransactionDate < _Request.EndDate
                                                    && x.TerminalId != null
                                                     && x.CashierId == _Request.CashierReferenceId
                                                && x.ParentId == _Request.AccountId
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward))
                                              .Select(x => new
                                              {
                                                  TerminalId = x.Terminal.IdentificationNumber,
                                                  StoreDisplayName = x.SubParent.DisplayName,
                                                  StoreAddress = x.SubParent.Address,
                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate.AddHours(1),
                                              }).ToList();
                        var Transactions = TransactionsAll
                                               .GroupBy(x => new
                                               {
                                                   x.TerminalId
                                               })
                                             .Select(x => new OAnalytics.Sale
                                             {
                                                 TerminalId = x.Key.TerminalId,
                                                 TotalTransaction = x.Count(),
                                                 TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                                 StoreDisplayName = x.FirstOrDefault().StoreDisplayName,
                                                 StoreAddress = x.FirstOrDefault().StoreAddress,
                                                 LastTransactionDate = x.OrderByDescending(a => a.TransactionDate).Select(a => a.TransactionDate).FirstOrDefault(),
                                             }).ToList();
                        //var _SalesHistory = Transactions
                        //                        .GroupBy(x => new
                        //                        {
                        //                            x.TerminalId
                        //                        })
                        //                        .Select(x => new OAnalytics.Sale
                        //                        {
                        //                            TerminalId = x.Key.TerminalId,
                        //                            TotalTransaction = x.Count(),
                        //                            TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                        //                         })
                        //                       .ToList();
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Transactions, "HC0001");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GeSalesSummary", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the cashiers overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCashiersOverview(OAnalytics.Request _Request)
        {
            #region Manage Exception
            _CashiersOverview = new CashiersOverview();
            _CashierOverviewDetails = new List<CashierOverview>();
            _SalesHistory = new List<OAnalytics.Sale>();
            try
            {
                //_Request.StartDate = _Request.StartDate.Value.Date;
                //_Request.EndDate = _Request.EndDate.Value.Date;
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    _OSalesItem = new List<OSalesItem>();
                    if (_Request.StoreReferenceId != 0)
                    {
                        _CashierOverviewDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.MerchantCashier
                                      && (x.OwnerId == _Request.StoreReferenceId || x.SubOwnerId == _Request.StoreReferenceId))
                                            .OrderBy(x => x.DisplayName)
                                           .Select(x => new CashierOverview
                                           {
                                               ReferenceId = x.Id,
                                               ReferenceKey = x.Guid,
                                               CashierId = x.DisplayName,
                                               FistName = x.FirstName,
                                               LastName = x.LastName,
                                               IconUrl = x.IconStorage.Path,
                                               Transactions = 0,
                                               InvoiceAmount = 0,
                                               CardInvoiceAmount = 0,
                                               CashInvoiceAmount = 0,
                                               Customers = 0,
                                               WorkingDays = 0,
                                               AverageCustomers = 0,
                                               AverageTransactions = 0,
                                               PerformanceLevel = 0,
                                           }).ToList();
                        _OSalesItem = _HCoreContext.HCUAccountTransaction
                                         .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                               && x.ParentId == _Request.AccountId
                                               && x.SubParentId == _Request.StoreReferenceId
                                               && x.CashierId != null
                                               && x.TransactionDate > _Request.StartDate
                                               && x.TransactionDate < _Request.EndDate
                                               && x.StatusId == HelperStatus.Transaction.Success
                                               && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                               && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward))
                                         .Select(x => new OSalesItem
                                         {
                                             UserAccountId = x.AccountId,
                                             TransactionDateT = x.TransactionDate,
                                             InvoiceAmount = x.PurchaseAmount,
                                             CashierId = x.CashierId,
                                             TypeId = x.TypeId,
                                         }).ToList();
                    }
                    else
                    {
                        _CashierOverviewDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.MerchantCashier
                                   && (x.Owner.OwnerId == _Request.AccountId || x.OwnerId == _Request.AccountId || x.SubOwner.OwnerId == _Request.AccountId))
                                          .OrderBy(x => x.DisplayName)
                                         .Select(x => new CashierOverview
                                         {
                                             ReferenceId = x.Id,
                                             ReferenceKey = x.Guid,
                                             CashierId = x.DisplayName,
                                             FistName = x.FirstName,
                                             LastName = x.LastName,
                                             IconUrl = x.IconStorage.Path,
                                             Transactions = 0,
                                             InvoiceAmount = 0,
                                             CardInvoiceAmount = 0,
                                             CashInvoiceAmount = 0,
                                             Customers = 0,
                                             WorkingDays = 0,
                                             AverageCustomers = 0,
                                             AverageTransactions = 0,
                                             PerformanceLevel = 0,
                                         }).ToList();
                        _OSalesItem = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                               && x.CashierId != null
                                              && x.ParentId == _Request.AccountId
                                              && x.TransactionDate > _Request.StartDate
                                              && x.TransactionDate < _Request.EndDate
                                          && x.StatusId == HelperStatus.Transaction.Success
                                              && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                              && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward))
                                        .Select(x => new OSalesItem
                                        {
                                            UserAccountId = x.AccountId,
                                            TransactionDateT = x.TransactionDate,
                                            InvoiceAmount = x.PurchaseAmount,
                                            CashierId = x.CashierId,
                                            TypeId = x.TypeId,
                                        }).ToList();
                    }
                    _HCoreContext.Dispose();
                    _CashiersOverview.Total = _CashierOverviewDetails.Count;
                    _CashiersOverview.Transactions = _OSalesItem.Count;
                    if (_CashiersOverview.Transactions > 0)
                    {
                        _CashiersOverview.Customers = _OSalesItem.Select(x => x.UserAccountId).Distinct().Count();
                        _CashiersOverview.InvoiceAmount = (double)_OSalesItem.Sum(x => x.InvoiceAmount);
                        _CashiersOverview.CardInvoiceAmount = (double)_OSalesItem.Where(x => x.TypeId == TransactionType.CardReward).Sum(x => x.InvoiceAmount);
                        _CashiersOverview.CashInvoiceAmount = (double)_OSalesItem.Where(x => x.TypeId == TransactionType.CashReward).Sum(x => x.InvoiceAmount);
                        _CashiersOverview.WorkingDays = (_Request.EndDate - _Request.StartDate).Value.Days;
                        if (_CashiersOverview.WorkingDays == 0)
                        {
                            _CashiersOverview.WorkingDays = 1;
                        }
                        _CashiersOverview.AverageCustomers = _CashiersOverview.Customers / _CashiersOverview.Total;
                        _CashiersOverview.AverageTransactions = _CashiersOverview.Transactions / _CashiersOverview.Total;
                        foreach (var _CashierOverviewDetail in _CashierOverviewDetails)
                        {
                            _CashierOverviewDetail.Transactions = _OSalesItem.Where(x => x.CashierId == _CashierOverviewDetail.ReferenceId).Count();
                            //if (_CashierOverviewDetail.Transactions > 0)
                            //{

                            _CashierOverviewDetail.Customers = _OSalesItem.Where(x => x.CashierId == _CashierOverviewDetail.ReferenceId).Select(x => x.UserAccountId).Distinct().Count();
                            _CashierOverviewDetail.InvoiceAmount = (double)_OSalesItem.Where(x => x.CashierId == _CashierOverviewDetail.ReferenceId).Sum(x => x.InvoiceAmount);
                            _CashierOverviewDetail.CardInvoiceAmount = (double)_OSalesItem.Where(x => x.CashierId == _CashierOverviewDetail.ReferenceId && x.TypeId == TransactionType.CardReward).Sum(x => x.InvoiceAmount);
                            _CashierOverviewDetail.CashInvoiceAmount = (double)_OSalesItem.Where(x => x.CashierId == _CashierOverviewDetail.ReferenceId && x.TypeId == TransactionType.CashReward).Sum(x => x.InvoiceAmount);
                            _CashierOverviewDetail.WorkingDays = _OSalesItem.Where(x => x.CashierId == _CashierOverviewDetail.ReferenceId).Select(x => x.TransactionDateT.Date).Distinct().Count();
                            _CashierOverviewDetail.InactiveDays = (_Request.EndDate - _Request.StartDate).Value.Days - _CashierOverviewDetail.WorkingDays;
                            if (_CashierOverviewDetail.WorkingDays > 0)
                            {
                                _CashierOverviewDetail.AverageCustomers = _CashierOverviewDetail.Customers / _CashierOverviewDetail.WorkingDays;
                                _CashierOverviewDetail.AverageTransactions = _CashierOverviewDetail.Transactions / _CashierOverviewDetail.WorkingDays;
                            }
                            else
                            {
                                _CashierOverviewDetail.AverageCustomers = _CashierOverviewDetail.Customers;
                                _CashierOverviewDetail.AverageTransactions = _CashierOverviewDetail.Transactions;
                            }
                            _CashierOverviewDetail.PerformanceLevel = Math.Round(HCoreHelper.GetAmountPercentage(_CashierOverviewDetail.Transactions, _CashiersOverview.Transactions), 2);
                            if (_CashierOverviewDetail.PerformanceLevel < 25.1)
                            {
                                _CashierOverviewDetail.PerformanceName = "Below Average";
                            }
                            if (_CashierOverviewDetail.PerformanceLevel > 25 && _CashierOverviewDetail.PerformanceLevel < 51)
                            {
                                _CashierOverviewDetail.PerformanceName = "Average";
                            }
                            if (_CashierOverviewDetail.PerformanceLevel > 50 && _CashierOverviewDetail.PerformanceLevel < 75)
                            {
                                _CashierOverviewDetail.PerformanceName = "Good";
                            }
                            if (_CashierOverviewDetail.PerformanceLevel > 75)
                            {
                                _CashierOverviewDetail.PerformanceName = "Excellent";
                            }
                            //}
                            //else
                            //{
                            //    _CashierOverviewDetail.PerformanceName = "Below Average";
                            //}
                        }
                        _CashiersOverview.Cashiers = _CashierOverviewDetails;
                    }
                    else
                    {
                        _CashiersOverview.Cashiers = _CashierOverviewDetails;
                    }
                    foreach (var _Cashier in _CashiersOverview.Cashiers)
                    {
                        if (!string.IsNullOrEmpty(_Cashier.IconUrl))
                        {
                            _Cashier.IconUrl = _AppConfig.StorageUrl + _Cashier.IconUrl;
                        }
                        else
                        {
                            _Cashier.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Create  Response Object
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CashiersOverview, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCashiersOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }

        //internal OResponse GetSalesHistory(OAnalytics.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        _SalesHistory = new List<OAnalytics.Sale>();
        //        #region Operation
        //        using (_HCoreContextOperations = new HCoreContextOperations())
        //        {
        //            #region Set Default Limit
        //            #endregion
        //            #region Get Data
        //            if (_Request.Type == "hour")
        //            {
        //                _SalesHistory = _HCoreContextOperations.HCOAccountSalesHistoryHourly
        //                                      .Where(x => x.AccountId == _Request.AccountId && x.Date > _Request.StartDate && x.Date < _Request.EndDate)
        //                                      .GroupBy(x => x.Hour)
        //                                      .Select(x => new OAnalytics.Sale
        //                                      {
        //                                          Hour = x.Key,
        //                                          TotalCustomer = x.Sum(m => m.TotalUser),
        //                                          TotalTransaction = x.Sum(m => m.SuccessfulTransaction),
        //                                          TotalInvoiceAmount = x.Sum(m => m.SuccessfulTransactionInvoiceAmount),
        //                                      })
        //                                      .ToList();
        //            }
        //            else if (_Request.Type == "m")
        //            {
        //                _SalesHistory = _HCoreContextOperations.HCOAccountSalesHistory
        //                                     .Where(x => x.AccountId == _Request.AccountId)
        //                                     .GroupBy(x => new
        //                                     {
        //                                         x.Date.Year,
        //                                     })
        //                                     .Select(x => new OAnalytics.Sale
        //                                     {
        //                                         Year = x.Key.Year,
        //                                         TotalTransaction = x.Sum(m => m.TotalTransaction),
        //                                         TotalInvoiceAmount = x.Sum(m => m.TotalInvoiceAmount),
        //                                         //CardTransactionUser = x.Sum(m => m.CardTransactionUser),
        //                                         //CardTransaction = x.Sum(m => m.CardTransaction),
        //                                         //CardInvoiceAmount = x.Sum(m => m.CardInvoiceAmount),
        //                                         //CashTransactionUser = x.Sum(m => m.CashTransactionUser),
        //                                         //CashTransaction = x.Sum(m => m.CashTransaction),
        //                                         //CashInvoiceAmount = x.Sum(m => m.CashInvoiceAmount),
        //                                         //SuccessfulTransaction = x.Sum(m => m.SuccessfulTransaction),
        //                                         //SuccessfulTransactionInvoiceAmount = x.Sum(m => m.SuccessfulTransactionInvoiceAmount),
        //                                         //SuccessfulTransactionUser = x.Sum(m => m.SuccessfulTransactionUser),
        //                                         //FailedTransaction = x.Sum(m => m.FailedTransaction),
        //                                         //FailedTransactionInvoiceAmount = x.Sum(m => m.FailedTransactionInvoiceAmount),
        //                                         //FailedTransactionUser = x.Sum(m => m.FailedTransactionUser),
        //                                     })
        //                                    //.OrderBy(_Request.SortExpression)
        //                                    //.Skip(_Request.Offset)
        //                                    //.Take(_Request.Limit)
        //                                    .ToList();
        //            }
        //            else if (_Request.Type == "week")
        //            {
        //                _SalesHistory = _HCoreContextOperations.HCOAccountSalesHistory
        //                                     .Where(x => x.AccountId == _Request.AccountId && x.Date > _Request.StartDate && x.Date < _Request.EndDate)
        //                                     .AsEnumerable()
        //                                     .GroupBy(x => new
        //                                     {
        //                                         x.Date.DayOfWeek,
        //                                     })
        //                                     .Select(x => new OAnalytics.Sale
        //                                     {
        //                                         WeekDay = x.Key.DayOfWeek.ToString(),
        //                                         TotalCustomer = x.Sum(m => m.TotalUser),
        //                                         TotalTransaction = x.Sum(m => m.TotalTransaction),
        //                                         TotalInvoiceAmount = x.Sum(m => m.TotalInvoiceAmount),
        //                                         //CardTransactionUser = x.Sum(m => m.CardTransactionUser),
        //                                         //CardTransaction = x.Sum(m => m.CardTransaction),
        //                                         //CardInvoiceAmount = x.Sum(m => m.CardInvoiceAmount),
        //                                         //CashTransactionUser = x.Sum(m => m.CashTransactionUser),
        //                                         //CashTransaction = x.Sum(m => m.CashTransaction),
        //                                         //CashInvoiceAmount = x.Sum(m => m.CashInvoiceAmount),
        //                                         //SuccessfulTransaction = x.Sum(m => m.SuccessfulTransaction),
        //                                         //SuccessfulTransactionInvoiceAmount = x.Sum(m => m.SuccessfulTransactionInvoiceAmount),
        //                                         //SuccessfulTransactionUser = x.Sum(m => m.SuccessfulTransactionUser),
        //                                         //FailedTransaction = x.Sum(m => m.FailedTransaction),
        //                                         //FailedTransactionInvoiceAmount = x.Sum(m => m.FailedTransactionInvoiceAmount),
        //                                         //FailedTransactionUser = x.Sum(m => m.FailedTransactionUser),
        //                                     })
        //                                    //.OrderBy(_Request.SortExpression)
        //                                    //.Skip(_Request.Offset)
        //                                    //.Take(_Request.Limit)
        //                                    .ToList();
        //            }
        //            else if (_Request.Type == "year")
        //            {
        //                _SalesHistory = _HCoreContextOperations.HCOAccountSalesHistory
        //                                     .Where(x => x.AccountId == _Request.AccountId && x.Date > _Request.StartDate && x.Date < _Request.EndDate)
        //                                     .GroupBy(x => new
        //                                     {
        //                                         x.UserAccountId,
        //                                         x.Date.Month,
        //                                         x.Date.Year,
        //                                     })
        //                                     .Select(x => new OAnalytics.Sale
        //                                     {
        //                                         Month = x.Key.Month,
        //                                         Year = x.Key.Year,
        //                                         //Date = x.Select(m => m.Date).FirstOrDefault(),
        //                                         //UserAccountTypeId = x.Select(m => m.UserAccountTypeId).FirstOrDefault(),
        //                                         //MerchantId = x.Select(m => m.MerchantId).FirstOrDefault(),
        //                                         //StoreId = x.Select(m => m.StoreId).FirstOrDefault(),
        //                                         //CashierId = x.Select(m => m.CashierId).FirstOrDefault(),
        //                                         //TerminalId = x.Select(m => m.TerminalId).FirstOrDefault(),
        //                                         //BankId = x.Select(m => m.BankId).FirstOrDefault(),
        //                                         //PtspId = x.Select(m => m.PtspId).FirstOrDefault(),
        //                                         //ManagerId = x.Select(m => m.ManagerId).FirstOrDefault(),
        //                                         //RmId = x.Select(m => m.RmId).FirstOrDefault(),

        //                                         //TotalUser = x.Sum(m => m.TotalUser),
        //                                         TotalTransaction = x.Sum(m => m.TotalTransaction),
        //                                         TotalInvoiceAmount = x.Sum(m => m.TotalInvoiceAmount),
        //                                         //CardTransactionUser = x.Sum(m => m.CardTransactionUser),
        //                                         //CardTransaction = x.Sum(m => m.CardTransaction),
        //                                         //CardInvoiceAmount = x.Sum(m => m.CardInvoiceAmount),
        //                                         //CashTransactionUser = x.Sum(m => m.CashTransactionUser),
        //                                         //CashTransaction = x.Sum(m => m.CashTransaction),
        //                                         //CashInvoiceAmount = x.Sum(m => m.CashInvoiceAmount),
        //                                         //SuccessfulTransaction = x.Sum(m => m.SuccessfulTransaction),
        //                                         //SuccessfulTransactionInvoiceAmount = x.Sum(m => m.SuccessfulTransactionInvoiceAmount),
        //                                         //SuccessfulTransactionUser = x.Sum(m => m.SuccessfulTransactionUser),
        //                                         //FailedTransaction = x.Sum(m => m.FailedTransaction),
        //                                         //FailedTransactionInvoiceAmount = x.Sum(m => m.FailedTransactionInvoiceAmount),
        //                                         //FailedTransactionUser = x.Sum(m => m.FailedTransactionUser),
        //                                     })
        //                                    //.OrderBy(_Request.SortExpression)
        //                                    //.Skip(_Request.Offset)
        //                                    //.Take(_Request.Limit)
        //                                    .ToList();
        //            }
        //            else if (_Request.Type == "month")
        //            {
        //                #region Get Data
        //                _SalesHistory = _HCoreContextOperations.HCOAccountSalesHistory
        //                                     .Where(x => x.AccountId == _Request.AccountId && x.Date > _Request.StartDate && x.Date < _Request.EndDate)
        //                                          .Select(x => new OAnalytics.Sale
        //                                          {

        //                                              //ReferenceId = x.Id,
        //                                              Date = x.Date,
        //                                              //UserAccountId = x.AccountId,
        //                                              //UserAccountTypeId = x.UserAccountTypeId,
        //                                              //TotalUser = x.TotalUser,
        //                                              TotalTransaction = x.TotalTransaction,
        //                                              TotalInvoiceAmount = x.TotalInvoiceAmount,
        //                                              //CardTransactionUser = x.CardTransactionUser,
        //                                              //CardTransaction = x.CardTransaction,
        //                                              //CardInvoiceAmount = x.CardInvoiceAmount,
        //                                              //CashTransactionUser = x.CashTransactionUser,
        //                                              //CashTransaction = x.CashTransaction,
        //                                              //CashInvoiceAmount = x.CashInvoiceAmount,
        //                                              //SuccessfulTransaction = x.SuccessfulTransaction,
        //                                              //SuccessfulTransactionInvoiceAmount = x.SuccessfulTransactionInvoiceAmount,
        //                                              //SuccessfulTransactionUser = x.SuccessfulTransactionUser,
        //                                              //FailedTransaction = x.FailedTransaction,
        //                                              //FailedTransactionInvoiceAmount = x.FailedTransactionInvoiceAmount,
        //                                              //FailedTransactionUser = x.FailedTransactionUser,
        //                                              //MerchantId = x.MerchantId,
        //                                              //StoreId = x.StoreId,
        //                                              //CashierId = x.CashierId,
        //                                              //TerminalId = x.TerminalId,
        //                                              //BankId = x.BankId,
        //                                              //PtspId = x.PtspId,
        //                                              //ManagerId = x.ManagerId,
        //                                              //RmId = x.RmId,
        //                                          })
        //                                          //.Where(_Request.SearchCondition)
        //                                          //.OrderBy(_Request.SortExpression)
        //                                          //.Skip(_Request.Offset)
        //                                          //.Take(_Request.Limit)
        //                                          .ToList();
        //                #endregion

        //            }
        //            else
        //            {
        //                _SalesHistory = _HCoreContextOperations.HCOAccountSalesHistory
        //                                     .Where(x => x.AccountId == _Request.AccountId && x.Date > _Request.StartDate && x.Date < _Request.EndDate)

        //                                           .GroupBy(x => x.UserAccountId)
        //                                           .Select(x => new OAnalytics.Sale
        //                                           {
        //                                               //UserAccountId = x.Key,
        //                                               //UserAccountTypeId = x.Select(m => m.UserAccountTypeId).FirstOrDefault(),
        //                                               //MerchantId = x.Select(m => m.MerchantId).FirstOrDefault(),
        //                                               //StoreId = x.Select(m => m.StoreId).FirstOrDefault(),
        //                                               //CashierId = x.Select(m => m.CashierId).FirstOrDefault(),
        //                                               //TerminalId = x.Select(m => m.TerminalId).FirstOrDefault(),
        //                                               //BankId = x.Select(m => m.BankId).FirstOrDefault(),
        //                                               //PtspId = x.Select(m => m.PtspId).FirstOrDefault(),
        //                                               //ManagerId = x.Select(m => m.ManagerId).FirstOrDefault(),
        //                                               //RmId = x.Select(m => m.RmId).FirstOrDefault(),
        //                                               //TotalUser = x.Sum(m => m.TotalUser),
        //                                               TotalTransaction = x.Sum(m => m.TotalTransaction),
        //                                               TotalInvoiceAmount = x.Sum(m => m.TotalInvoiceAmount),
        //                                               //CardTransactionUser = x.Sum(m => m.CardTransactionUser),
        //                                               //CardTransaction = x.Sum(m => m.CardTransaction),
        //                                               //CardInvoiceAmount = x.Sum(m => m.CardInvoiceAmount),
        //                                               //CashTransactionUser = x.Sum(m => m.CashTransactionUser),
        //                                               //CashTransaction = x.Sum(m => m.CashTransaction),
        //                                               //CashInvoiceAmount = x.Sum(m => m.CashInvoiceAmount),
        //                                               //SuccessfulTransaction = x.Sum(m => m.SuccessfulTransaction),
        //                                               //SuccessfulTransactionInvoiceAmount = x.Sum(m => m.SuccessfulTransactionInvoiceAmount),
        //                                               //SuccessfulTransactionUser = x.Sum(m => m.SuccessfulTransactionUser),
        //                                               //FailedTransaction = x.Sum(m => m.FailedTransaction),
        //                                               //FailedTransactionInvoiceAmount = x.Sum(m => m.FailedTransactionInvoiceAmount),
        //                                               //FailedTransactionUser = x.Sum(m => m.FailedTransactionUser),
        //                                           })
        //                                          //.OrderBy(_Request.SortExpression)
        //                                          //.Skip(_Request.Offset)
        //                                          //.Take(_Request.Limit)
        //                                          .ToList();
        //            }
        //            #endregion
        //            #region Create  Response Object
        //            _HCoreContextOperations.Dispose();
        //            //foreach (var _SalesHistoryItem in _SalesHistory)
        //            //{
        //            //    var Details = HCoreDataStore.Accounts.Where(x => x.ReferenceId == _SalesHistoryItem.UserAccountId).FirstOrDefault();
        //            //    if (Details != null)
        //            //    {
        //            //        _SalesHistoryItem.DisplayName = Details.DisplayName;
        //            //        _SalesHistoryItem.Name = Details.Name;
        //            //        _SalesHistoryItem.Address = Details.Address;
        //            //        _SalesHistoryItem.Latitude = Details.Latitude;
        //            //        _SalesHistoryItem.Longitude = Details.Longitude;
        //            //        _SalesHistoryItem.IconUrl = Details.IconUrl;
        //            //    }
        //            //    if (_SalesHistoryItem.MerchantId != 0)
        //            //    {
        //            //        var MerchantDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == _SalesHistoryItem.MerchantId).FirstOrDefault();
        //            //        if (MerchantDetails != null)
        //            //        {
        //            //            _SalesHistoryItem.MerchantDisplayName = MerchantDetails.DisplayName;
        //            //            _SalesHistoryItem.MerchantKey = MerchantDetails.ReferenceKey;
        //            //            _SalesHistoryItem.MerchantIconUrl = MerchantDetails.IconUrl;
        //            //        }
        //            //    }
        //            //    if (_SalesHistoryItem.BankId != 0)
        //            //    {
        //            //        var BankDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == _SalesHistoryItem.BankId).FirstOrDefault();
        //            //        if (BankDetails != null)
        //            //        {
        //            //            _SalesHistoryItem.BankDisplayName = BankDetails.DisplayName;
        //            //            _SalesHistoryItem.BankKey = BankDetails.ReferenceKey;
        //            //            _SalesHistoryItem.BankIconUrl = BankDetails.IconUrl;
        //            //        }
        //            //    }
        //            //    if (_SalesHistoryItem.PtspId != 0)
        //            //    {
        //            //        var PtspDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == _SalesHistoryItem.PtspId).FirstOrDefault();
        //            //        if (PtspDetails != null)
        //            //        {
        //            //            _SalesHistoryItem.PtspDisplayName = PtspDetails.DisplayName;
        //            //            _SalesHistoryItem.PtspKey = PtspDetails.ReferenceKey;
        //            //            _SalesHistoryItem.PtspIconUrl = PtspDetails.IconUrl;
        //            //        }
        //            //    }
        //            //    if (_SalesHistoryItem.StoreId != 0)
        //            //    {
        //            //        var StoreDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == _SalesHistoryItem.StoreId).FirstOrDefault();
        //            //        if (StoreDetails != null)
        //            //        {
        //            //            _SalesHistoryItem.StoreDisplayName = StoreDetails.DisplayName;
        //            //            _SalesHistoryItem.StoreKey = StoreDetails.ReferenceKey;
        //            //            _SalesHistoryItem.StoreIconUrl = StoreDetails.IconUrl;
        //            //            _SalesHistoryItem.StoreAddress = StoreDetails.Address;
        //            //            _SalesHistoryItem.Latitude = StoreDetails.Latitude;
        //            //            _SalesHistoryItem.Longitude = StoreDetails.Longitude;
        //            //        }
        //            //    }
        //            //}
        //            #endregion
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SalesHistory, "HC0001");
        //            #endregion
        //        }
        //        #endregion

        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region Log Bug
        //        HCoreHelper.LogException("GeSalesSummary", _Exception, _Request.UserReference);
        //        #endregion
        //        #region Create DataTable Response Object
        //        #endregion
        //        #region Send Response
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
        //        #endregion
        //    }
        //    #endregion
        //}


        /// <summary>
        /// Description: Gets the loyalty overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        ///
        ManageCoreTransaction _ManageCoreTransaction;
        internal OResponse GetGroupLoyaltyOverview(OAnalytics.Request _Request)
        {
            _LoyaltyOverview = new OAnalytics.Loyalty();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var Transactions = _HCoreContext.HCUAccountTransaction
                                      .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                      //&& x.Parent.OwnerId == _Request.AccountId
                                      && x.ProgramId == _Request.ProgramId
                                      && x.TransactionDate > _Request.StartDate
                                      && x.TransactionDate < _Request.EndDate
                                             && x.StatusId == HelperStatus.Transaction.Success
                                      && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack))
                                      //&& (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay))
                                      .Select(x => new
                                      {
                                          UserAccountId = x.AccountId,
                                          InvoiceAmount = x.PurchaseAmount
                                      }).ToList();
                    var TM = Transactions.GroupBy(x => x.UserAccountId)
                               .Select(x => new
                               {
                                   UserAccountId = x.Key,
                                   Count = x.Count(),
                                   InvoiceAmount = x.Sum(a => a.InvoiceAmount),
                               }).ToList();
                    _LoyaltyOverview.NewCustomers = TM.Count(x => x.Count < 2);
                    _LoyaltyOverview.NewCustomerInvoiceAmount = TM.Where(x => x.Count < 2).Sum(x => x.InvoiceAmount);
                    _LoyaltyOverview.RepeatingCustomers = TM.Count(x => x.Count > 1);
                    _LoyaltyOverview.VisitsByRepeatingCustomers = TM.Where(x => x.Count > 1).Count();
                    _LoyaltyOverview.VisitsByNewCustomers = TM.Where(x => x.Count < 2).Sum(x => x.Count);
                    _LoyaltyOverview.RepeatingCustomerInvoiceAmount = TM.Where(x => x.Count > 1).Sum(x => x.InvoiceAmount);
                    _LoyaltyOverview.Transaction = Transactions.Count();
                    _LoyaltyOverview.AllVisit = _LoyaltyOverview.VisitsByNewCustomers + _LoyaltyOverview.VisitsByRepeatingCustomers;
                    _LoyaltyOverview.TotalTimeofVisit = 0;
                    _LoyaltyOverview.TransactionInvoiceAmount = Transactions.Sum(x => x.InvoiceAmount);
                    _LoyaltyOverview.RewardTransaction = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ProgramId == _Request.ProgramId
                                                       && m.Account.AccountTypeId == UserAccountType.Appuser
                                                        //&& m.Parent.OwnerId == _Request.AccountId
                                                        && m.TransactionDate > _Request.StartDate
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && (((m.ModeId == TransactionMode.Credit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus)
                                                        && (m.TypeId == TransactionType.CardReward || m.TypeId == TransactionType.CashReward))
                                                      ).Count();
                    if (_LoyaltyOverview.RewardTransaction > 0)
                    {
                        _LoyaltyOverview.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ProgramId == _Request.ProgramId
                                                       && m.Account.AccountTypeId == UserAccountType.Appuser
                                                        //&& m.Parent.OwnerId == _Request.AccountId
                                                        && m.TransactionDate > _Request.StartDate
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.BankId == _Request.AccountId
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                       && m.ModeId == TransactionMode.Credit
                                                       && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.GiftCards || m.SourceId == TransactionSource.TUCBlack || m.SourceId == TransactionSource.ThankUCashPlus)
                                                       && (m.TypeId == TransactionType.CardReward || m.TypeId == TransactionType.CashReward || m.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || m.TypeId == TransactionType.GiftCard))
                                                       .Sum(x => (double?)x.ReferenceAmount) ?? 0;

                        _LoyaltyOverview.RewardCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.ProgramId == _Request.ProgramId
                                                     && m.Account.AccountTypeId == UserAccountType.Appuser
                                                      && m.BankId == _Request.AccountId
                                                      //&& m.Parent.OwnerId == _Request.AccountId
                                                      && m.TransactionDate > _Request.StartDate
                                                      && m.TransactionDate < _Request.EndDate
                                                      && m.TotalAmount > 0
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                      && (((m.ModeId == TransactionMode.Credit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                     ).Sum(x => (double?)x.ParentTransaction.ComissionAmount) ?? 0;
                        _LoyaltyOverview.RewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ProgramId == _Request.ProgramId
                                                       && m.Account.AccountTypeId == UserAccountType.Appuser
                                                      && m.BankId == _Request.AccountId
                                                        //&& m.Parent.OwnerId == _Request.AccountId
                                                        && m.TransactionDate > _Request.StartDate
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && (((m.ModeId == TransactionMode.Credit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                       ).Sum(x => (double?)x.ReferenceInvoiceAmount) ?? 0;
                    }


                    _LoyaltyOverview.RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                       .Count(m => m.ProgramId == _Request.ProgramId
                                                        //&& m.Parent.OwnerId == _Request.AccountId
                                                        && m.TransactionDate > _Request.StartDate
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                       //  && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                        && m.ModeId == TransactionMode.Debit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                     );
                    if (_LoyaltyOverview.RedeemTransaction > 0)
                    {
                        _LoyaltyOverview.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ProgramId == _Request.ProgramId
                                                        //&& m.Parent.OwnerId == _Request.AccountId
                                                        && m.TransactionDate > _Request.StartDate
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                         && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                        && m.ModeId == TransactionMode.Debit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                     ).Sum(x => (double?)x.TotalAmount) ?? 0;
                        _LoyaltyOverview.RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ProgramId == _Request.ProgramId
                                                        //&& m.Parent.OwnerId == _Request.AccountId
                                                        && m.TransactionDate > _Request.StartDate
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                         && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                        && m.ModeId == TransactionMode.Debit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
                                                     ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                    }
                }

                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _LoyaltyOverview, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetGroupLoyaltyOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// This APi will return the Customers visits count s per date selected and it will show how many times customers has visited to taht merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetGroupLoyaltyVisitHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    long DaysDifference = (_Request.EndDate.Value.Date - _Request.StartDate.Value.Date).Days;
                    _OTrList = new List<OTrList>();
                    _OTrList = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                        //&& x.Parent.OwnerId == _Request.AccountId
                                        && x.ProgramId == _Request.ProgramId
                                        && x.TransactionDate > _Request.StartDate
                                        && x.TransactionDate < _Request.EndDate
                                       && x.StatusId == HelperStatus.Transaction.Success
                                        && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                        && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay))
                                       .Select(x => new OTrList
                                       {
                                           TransactionDate = x.TransactionDate.AddHours(1),
                                           UserAccountId = x.AccountId,
                                           InvoiceAmount = x.PurchaseAmount
                                       }).ToList();
                    if (DaysDifference > 365)
                    {
                        //var TM = _OTrList.GroupBy(x => x.TransactionDate.Date.Year)
                        // .Select(x => new OAnalytics.SalesMetrics
                        // {
                        //     Title = x.Key.ToString(),
                        //     Year = x.Key,
                        //     TotalTransaction = x.Count(),
                        //     TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                        // }).ToList();
                        //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                        var TM = _OTrList.GroupBy(x => x.TransactionDate.Year)
                       .Select(x => new OAnalytics.SalesMetrics
                       {
                           Title = x.Key.ToString(),
                           Year = x.Key,
                           TotalTransaction = x.Count(),
                           TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                           TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                           NewCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() == 1).Count(),
                           RepeatingCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() > 1).Count(),
                       }).ToList();
                        foreach (var item in TM)
                        {
                            item.NewCustomer = _OTrList
                                .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
                                .GroupBy(x => x.UserAccountId)
                                .Where(x => x.Count() < 2).Count();

                            item.NewCustomerInvoiceAmount = _OTrList
                               .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
                               .GroupBy(x => x.UserAccountId)
                               .Where(x => x.Count() < 2)
                               .Select(x => new
                               {
                                   UserAccount = x.Key,
                                   InvoiceAmount = x.Sum(d => d.InvoiceAmount)
                               }).Sum(g => g.InvoiceAmount);

                            item.RepeatingCustomer = _OTrList
                                .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
                                .GroupBy(x => x.UserAccountId)
                                .Where(x => x.Count() > 1)
                                .Count();

                            item.RepeatingCustomerInvoiceAmount = _OTrList
                              .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
                              .GroupBy(x => x.UserAccountId)
                              .Where(x => x.Count() > 1)
                              .Select(x => new
                              {
                                  UserAccount = x.Key,
                                  InvoiceAmount = x.Sum(d => d.InvoiceAmount)
                              }).Sum(g => g.InvoiceAmount);
                            item.VisitsByRepeatingCustomers = _OTrList
                             .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
                             .GroupBy(x => x.UserAccountId)
                             .Where(x => x.Count() > 1)
                             .Sum(x => x.Count());
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                        #endregion
                    }
                    else if (DaysDifference < 366 && DaysDifference > 31)
                    {
                        //var TM = _OTrList.GroupBy(x => new
                        //{
                        //    x.TransactionDate.Date.Year,
                        //    x.TransactionDate.Date.Month,
                        //})
                        //.Select(x => new OAnalytics.SalesMetrics
                        //{
                        //    Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
                        //    Year = x.Key.Year,
                        //    Month = x.Key.Month,
                        //    TotalTransaction = x.Count(),
                        //    TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                        //}).ToList();
                        // return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");


                        var TM = _OTrList.GroupBy(x => new
                        {
                            x.TransactionDate.Date.Year,
                            x.TransactionDate.Date.Month,
                        })
                     .Select(x => new OAnalytics.SalesMetrics
                     {
                         Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
                         Year = x.Key.Year,
                         Month = x.Key.Month,
                         TotalTransaction = x.Count(),
                         TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                         TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         NewCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() == 1).Count(),
                         RepeatingCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() > 1).Count(),
                     }).ToList();
                        foreach (var item in TM)
                        {
                            item.NewCustomer = _OTrList
                                .Where(x => x.TransactionDate.Year == item.Year.Value && x.TransactionDate.Month == item.Month.Value)
                                .GroupBy(x => x.UserAccountId)
                                .Where(x => x.Count() < 2).Count();

                            item.NewCustomerInvoiceAmount = _OTrList
                               .Where(x => x.TransactionDate.Year == item.Year.Value && x.TransactionDate.Month == item.Month.Value)
                               .GroupBy(x => x.UserAccountId)
                               .Where(x => x.Count() < 2)
                               .Select(x => new
                               {
                                   UserAccount = x.Key,
                                   InvoiceAmount = x.Sum(d => d.InvoiceAmount)
                               }).Sum(g => g.InvoiceAmount);

                            item.RepeatingCustomer = _OTrList
                               .Where(x => x.TransactionDate.Year == item.Year.Value && x.TransactionDate.Month == item.Month.Value)
                                .GroupBy(x => x.UserAccountId)
                                .Where(x => x.Count() > 1)
                                .Count();

                            item.RepeatingCustomerInvoiceAmount = _OTrList
                               .Where(x => x.TransactionDate.Year == item.Year.Value && x.TransactionDate.Month == item.Month.Value)
                              .GroupBy(x => x.UserAccountId)
                              .Where(x => x.Count() > 1)
                              .Select(x => new
                              {
                                  UserAccount = x.Key,
                                  InvoiceAmount = x.Sum(d => d.InvoiceAmount)
                              }).Sum(g => g.InvoiceAmount);
                            item.VisitsByRepeatingCustomers = _OTrList
                                .Where(x => x.TransactionDate.Year == item.Year.Value && x.TransactionDate.Month == item.Month.Value)
                             .GroupBy(x => x.UserAccountId)
                             .Where(x => x.Count() > 1)
                             .Sum(x => x.Count());
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                        #endregion
                    }
                    else if (DaysDifference < 2)
                    {
                        var TM = _OTrList.GroupBy(x => x.TransactionDate.Hour)
                         .Select(x => new OAnalytics.SalesMetrics
                         {
                             Title = x.Key.ToString(),
                             Hour = x.Key,
                             TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                             TotalTransaction = x.Count(),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         }).ToList();
                        foreach (var item in TM)
                        {
                            var hours = Convert.ToInt32(item.Title);
                            int minutes = 0;
                            var amPmDesignator = "AM";
                            if (hours == 0)
                                hours = 12;
                            else if (hours == 12)
                                amPmDesignator = "PM";
                            else if (hours > 12)
                            {
                                hours -= 12;
                                amPmDesignator = "PM";
                            }
                            item.Title = String.Format("{0}:{1:00} {2}", hours, minutes, amPmDesignator);


                            item.NewCustomer = _OTrList
                                .Where(x => x.TransactionDate.Hour == item.Hour)
                                .GroupBy(x => x.UserAccountId)
                                .Where(x => x.Count() < 2).Count();

                            item.NewCustomerInvoiceAmount = _OTrList
                                .Where(x => x.TransactionDate.Hour == item.Hour)
                               .GroupBy(x => x.UserAccountId)
                               .Where(x => x.Count() < 2)
                               .Select(x => new
                               {
                                   UserAccount = x.Key,
                                   InvoiceAmount = x.Sum(d => d.InvoiceAmount)
                               }).Sum(g => g.InvoiceAmount);

                            item.RepeatingCustomer = _OTrList
                                .Where(x => x.TransactionDate.Hour == item.Hour)
                                .GroupBy(x => x.UserAccountId)
                                .Where(x => x.Count() > 1)
                                .Count();

                            item.RepeatingCustomerInvoiceAmount = _OTrList
                                .Where(x => x.TransactionDate.Hour == item.Hour)
                              .GroupBy(x => x.UserAccountId)
                              .Where(x => x.Count() > 1)
                              .Select(x => new
                              {
                                  UserAccount = x.Key,
                                  InvoiceAmount = x.Sum(d => d.InvoiceAmount)
                              }).Sum(g => g.InvoiceAmount);
                            item.VisitsByRepeatingCustomers = _OTrList
                                .Where(x => x.TransactionDate.Hour == item.Hour)
                             .GroupBy(x => x.UserAccountId)
                             .Where(x => x.Count() > 1)
                             .Sum(x => x.Count());
                        }

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                    }
                    else
                    {
                        var TM = _OTrList.GroupBy(x => x.TransactionDate.Date)
                         .Select(x => new OAnalytics.SalesMetrics
                         {
                             Title = x.Key.ToString("dd-MM-yyyy"),
                             Date = x.Key,
                             TotalTransaction = x.Count(),
                             TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         }).ToList();

                        foreach (var item in TM)
                        {
                            item.NewCustomer = _OTrList
                                .Where(x => x.TransactionDate.Date == item.Date)
                                .GroupBy(x => x.UserAccountId)
                                .Where(x => x.Count() < 2).Count();

                            item.NewCustomerInvoiceAmount = _OTrList
                                .Where(x => x.TransactionDate.Date == item.Date)
                               .GroupBy(x => x.UserAccountId)
                               .Where(x => x.Count() < 2)
                               .Select(x => new
                               {
                                   UserAccount = x.Key,
                                   InvoiceAmount = x.Sum(d => d.InvoiceAmount)
                               }).Sum(g => g.InvoiceAmount);

                            item.RepeatingCustomer = _OTrList
                                .Where(x => x.TransactionDate.Date == item.Date)
                                .GroupBy(x => x.UserAccountId)
                                .Where(x => x.Count() > 1)
                                .Count();

                            item.RepeatingCustomerInvoiceAmount = _OTrList
                                .Where(x => x.TransactionDate.Date == item.Date)
                              .GroupBy(x => x.UserAccountId)
                              .Where(x => x.Count() > 1)
                              .Select(x => new
                              {
                                  UserAccount = x.Key,
                                  InvoiceAmount = x.Sum(d => d.InvoiceAmount)
                              }).Sum(g => g.InvoiceAmount);
                            item.VisitsByRepeatingCustomers = _OTrList
                                .Where(x => x.TransactionDate.Date == item.Date)
                             .GroupBy(x => x.UserAccountId)
                             .Where(x => x.Count() > 1)
                             .Sum(x => x.Count());
                        }

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                    }

                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetGroupLoyaltyHistory", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
    }
}
