//==================================================================================
// FileName: FrameworkAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to analytics
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Merchant;

namespace HCore.TUC.Core.Framework.Merchant.Analytics
{
    public class FrameworkAnalytics
    {
        HCoreContext _HCoreContext;
        OAnalytics.Counts _Counts;
        OAnalytics.DateRangeResponse _DateRangeResponse;
        List<OAnalytics.DateRange> _DateRanges;
        OAnalytics.TerminalStatus _TerminalStatus;

        /// <summary>
        /// Description: Gets the terminal activity history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTerminalActivityHistory(OAnalytics.Request _Request)
        {
            _DateRangeResponse = new OAnalytics.DateRangeResponse();
            _DateRanges = new List<OAnalytics.DateRange>();
            try
            {
                int Days = ((DateTime)_Request.EndDate - (DateTime)_Request.StartDate).Days;
                DateTime StartTime = (DateTime)_Request.StartDate;
                DateTime EndTime = StartTime.AddDays(1);

                using (_HCoreContext = new HCoreContext())
                {
                    for (int i = 0; i < Days; i++)
                    {
                        _TerminalStatus = new OAnalytics.TerminalStatus();
                        _TerminalStatus.TransactionAmount = _HCoreContext.TUCTerminal.Where(x => x.Id == _Request.SubAccountId && x.MerchantId == _Request.AccountId).Select(a => a.HCUAccountTransaction.Where(z => z.TerminalId == _Request.SubAccountId && z.ParentId == _Request.AccountId && (z.TransactionDate > StartTime && z.TransactionDate < EndTime)).Select(m => m.PurchaseAmount).FirstOrDefault()).FirstOrDefault();
                        _DateRanges.Add(new OAnalytics.DateRange
                        {
                            Data = _TerminalStatus,
                            Date = StartTime,
                            StartDate = StartTime,
                            EndDate = EndTime,
                        });
                        StartTime = StartTime.AddDays(1);
                        EndTime = EndTime.AddDays(1);
                    }
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DateRanges, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetTerminalActivityHistory", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

        /// <summary>
        /// Description: Gets the account overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountOverview(OAnalytics.Request _Request)
        {
            _Counts = new OAnalytics.Counts();
            _Counts.TotalTransactions = 0;
            _Counts.TotalSale = 0;
            _Counts.AverageTransactions = 0;
            _Counts.AverageTransactionAmount = 0;
            _Counts.CardTransactions = 0;
            _Counts.CardTransactionsAmount = 0;
            _Counts.CashTransactions = 0;
            _Counts.CashTransactionAmount = 0;
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long SubUserAccountId = 0;

                    if (!string.IsNullOrEmpty(_Request.SubAccountKey) && _Request.SubAccountId > 0)
                    {
                        var SubAccountDetails = _HCoreContext.TUCTerminal.Where(x => x.Guid == _Request.SubAccountKey && x.Id == _Request.SubAccountId)
                         .Select(x => new
                         {
                             UserAccountId = x.Id,
                         }).FirstOrDefault();

                        if (SubAccountDetails != null)
                        {
                            SubUserAccountId = SubAccountDetails.UserAccountId;
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1086");
                            #endregion
                        }
                    }

                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey && x.AccountTypeId == UserAccountType.Merchant)
                       .Select(x => new
                       {
                           UserAccountId = x.Id,
                           AccountTypeId = x.AccountTypeId,
                           OwnerId = x.OwnerId,
                       }).FirstOrDefault();

                    if (AccountDetails != null)
                    {
                        if (SubUserAccountId > 0)
                        {

                            #region  Overview
                            _Counts.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ParentId == _Request.AccountId
                                                            && m.TerminalId == _Request.SubAccountId
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.TransactionDate > _Request.StartDate
                                                            && m.TransactionDate < _Request.EndDate
                                                           && (m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit)
                                                           && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack || m.SourceId == TransactionSource.ThankUCashPlus)
                                                           && (m.TypeId == TransactionType.CardReward || m.TypeId == TransactionType.CashReward || m.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
                                                           && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                           || m.SourceId == TransactionSource.ThankUCashPlus)
                                                       ).Count();

                            _Counts.TotalSale = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.ParentId == _Request.AccountId
                                                            && m.TerminalId == _Request.SubAccountId
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                          && m.TransactionDate > _Request.StartDate
                                                          && m.TransactionDate < _Request.EndDate
                                                          && (m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit)
                                                           && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack || m.SourceId == TransactionSource.ThankUCashPlus)
                                                           && (m.TypeId == TransactionType.CardReward || m.TypeId == TransactionType.CashReward || m.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
                                                          && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || m.SourceId == TransactionSource.ThankUCashPlus)
                                                      ).Sum(m => (double?)m.PurchaseAmount) ?? 0;


                            _Counts.AverageTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(a => a.ParentId == _Request.AccountId
                                                              && a.TerminalId == _Request.SubAccountId
                                                          && a.StatusId == HelperStatus.Transaction.Success
                                                          && a.TransactionDate > _Request.StartDate
                                                          && a.TransactionDate < _Request.EndDate
                                                          && (a.ModeId == TransactionMode.Credit || a.ModeId == TransactionMode.Debit)
                                                           && (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUCBlack || a.SourceId == TransactionSource.ThankUCashPlus)
                                                           && (a.TypeId == TransactionType.CardReward || a.TypeId == TransactionType.CashReward || a.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
                                                          && ((a.TypeId != TransactionType.ThankUCashPlusCredit && a.SourceId == TransactionSource.TUC)
                                                          || a.SourceId == TransactionSource.ThankUCashPlus))
                                                              .Average(m => (double?)m.PurchaseAmount) ?? 0;

                            _Counts.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ParentId == _Request.AccountId
                                                                    && m.TerminalId == _Request.SubAccountId
                                                                   && m.TransactionDate > _Request.StartDate
                                                                   && m.TransactionDate < _Request.EndDate
                                                                   && m.TypeId == TransactionType.CardReward
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();

                            _Counts.CardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ParentId == _Request.AccountId
                                                                    && m.TerminalId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartDate
                                                                    && m.TransactionDate < _Request.EndDate
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;


                            _Counts.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ParentId == _Request.AccountId
                                                                    && m.TerminalId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartDate
                                                                    && m.TransactionDate < _Request.EndDate
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Count();

                            _Counts.CashTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ParentId == _Request.AccountId
                                                                    && m.TerminalId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartDate
                                                                    && m.TransactionDate < _Request.EndDate
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            #endregion
                            _Counts.CardTypeSale = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                       .Select(x => new OAnalytics.CardTypeSale
                                       {
                                           ReferenceId = x.Id,
                                           Name = x.Name,
                                           Transactions = 0,
                                           Amount = 0
                                       }).ToList();

                            foreach (var CardBrand in _Counts.CardTypeSale)
                            {

                                CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.ParentId == _Request.AccountId
                                                          && m.TerminalId == _Request.SubAccountId
                                                          && m.TransactionDate > _Request.StartDate
                                                          && m.TransactionDate < _Request.EndDate
                                                          && m.CardBrandId == CardBrand.ReferenceId
                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                        ).Count();

                                CardBrand.Amount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.ParentId == _Request.AccountId
                                                                 && m.TerminalId == _Request.SubAccountId
                                                                 && m.TransactionDate > _Request.StartDate
                                                                 && m.TransactionDate < _Request.EndDate
                                                                 && m.CardBrandId == CardBrand.ReferenceId
                                                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                 && m.StatusId == HelperStatus.Transaction.Success
                                                               ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }
                        }

                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Counts, "HC0001", "Details loaded");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001", "Details not found");
                        #endregion
                    }

                }


            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
    }
}

