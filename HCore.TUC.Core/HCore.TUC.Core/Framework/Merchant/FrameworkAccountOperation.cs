//==================================================================================
// FileName: FrameworkAccountOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to account operations
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Resource;
using HCore.Operations;
using HCore.Operations.Object;
using static HCore.TUC.Core.Object.Console.OProfile;
using HCore.Data.Operations;

namespace HCore.TUC.Core.Framework.Merchant
{
    internal class FrameworkAccountOperation
    {
        #region Declare
        Random _Random;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        HCoreContext _HCoreContext;
        List<HCUAccount> _HCUAccounts;
        List<TUCCategoryAccount> _TUCCategoryAccount;
        List<HCUAccountParameter> _HCUAccountParameters;
        TUCTerminal _TUCTerminal;
        ManageCoreUserAccess _ManageCoreUserAccess;
        OAppProfile.Request _AppProfileRequest;
        HCoreContextOperations? _HCoreContextOperations;

        #endregion
        /// <summary>
        /// Description: Saves the customer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveCustomer(OAccountOperations.Customer.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                if (string.IsNullOrEmpty(_Request.FirstName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1392, TUCCoreResource.CA1392M);
                }
                if (string.IsNullOrEmpty(_Request.LastName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1390, TUCCoreResource.CA1390M);
                }
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1389, TUCCoreResource.CA1389M);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                    string TMobileNumber = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                    var AccountDetails = _HCoreContext.HCUAccount.Any(x => x.AccountTypeId == UserAccountType.Appuser
                    && x.User.Username == TMobileNumber);
                    if (AccountDetails)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1388, TUCCoreResource.CA1388M);
                    }
                    else
                    {
                        _AppProfileRequest = new OAppProfile.Request();
                        _AppProfileRequest.OwnerId = _Request.AccountId;
                        _AppProfileRequest.CreatedById = _Request.UserReference.AccountId;
                        _AppProfileRequest.ReferralCode = _Request.ReferralCode;
                        _AppProfileRequest.MobileNumber = _Request.MobileNumber;
                        _AppProfileRequest.FirstName = _Request.FirstName;
                        _AppProfileRequest.LastName = _Request.LastName;
                        _AppProfileRequest.EmailAddress = _Request.EmailAddress;
                        _AppProfileRequest.DateOfBirth = _Request.DateOfBirth;
                        _AppProfileRequest.Name = _Request.FirstName + " " + _Request.LastName;
                        if (!string.IsNullOrEmpty(_Request.FirstName))
                        {
                            _AppProfileRequest.DisplayName = _Request.FirstName;
                        }
                        if (!string.IsNullOrEmpty(_Request.GenderCode))
                        {
                            _AppProfileRequest.GenderCode = _Request.GenderCode.ToLower();
                        }
                        _AppProfileRequest.UserReference = _Request.UserReference;
                        _ManageCoreUserAccess = new ManageCoreUserAccess();
                        OAppProfile.Response _AppUserCreateResponse = _ManageCoreUserAccess.CreateAppUserAccount(_AppProfileRequest);
                        if (_AppUserCreateResponse != null && _AppUserCreateResponse.Status == ResponseStatus.Success)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1391, TUCCoreResource.CA1391M);
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _AppUserCreateResponse.StatusResponseCode, _AppUserCreateResponse.StatusMessage);
                            #endregion
                        }
                    }

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("SaveCustomer", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveStore(OAccountOperations.Store.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                if (string.IsNullOrEmpty(_Request.DisplayName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1079, TUCCoreResource.CA1079M);
                }
                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.AddressComponent, _Request.UserReference);
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    bool Details = _HCoreContext.HCUAccount
                        .Any(x => x.Owner.Guid == _Request.AccountKey && x.OwnerId == _Request.AccountId && x.AccountTypeId == UserAccountType.MerchantStore && x.DisplayName == _Request.DisplayName);
                    if (Details)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1077, TUCCoreResource.CA1077M);
                    }
                    else
                    {

                        _Random = new Random();
                        string AccountCode = _Random.Next(100000000, 999999999).ToString();
                        _HCUAccounts = new List<HCUAccount>();
                        _HCUAccount = new HCUAccount();
                        //if (_Request.Categories != null && _Request.Categories.Count > 0)
                        //{
                        //    _HCUAccountParameters = new List<HCUAccountParameter>();
                        //    foreach (var Category in _Request.Categories)
                        //    {
                        //        _HCUAccountParameters.Add(new HCUAccountParameter
                        //        {
                        //            Guid = HCoreHelper.GenerateGuid(),
                        //            TypeId = HelperType.MerchantCategory,
                        //            CommonId = Category.ReferenceId,
                        //            CreateDate = HCoreHelper.GetGMTDateTime(),
                        //            StatusId = HelperStatus.Default.Active,
                        //        });
                        //    }
                        //    _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                        //}
                        #region Set Categories
                        if (_Request.Categories != null && _Request.Categories.Count > 0)
                        {
                            _TUCCategoryAccount = new List<TUCCategoryAccount>();
                            foreach (var Category in _Request.Categories)
                            {
                                _TUCCategoryAccount.Add(new TUCCategoryAccount
                                {
                                    Guid = HCoreHelper.GenerateGuid(),
                                    TypeId = 1,
                                    CategoryId = Category.ReferenceId,
                                    CreateDate = HCoreHelper.GetGMTDateTime(),
                                    CreatedById = _Request.UserReference.AccountId,
                                    StatusId = HelperStatus.Default.Active,
                                });
                            }
                            _HCUAccount.TUCCategoryAccountAccount = _TUCCategoryAccount;
                        }
                        #endregion
                        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                        _HCUAccount.AccountOperationTypeId = Helpers.AccountOperationType.OnlineAndOffline;
                        _HCUAccount.OwnerId = _Request.AccountId;
                        _HCUAccount.SubOwnerId = _Request.StoreManagerId; // Change Request as per suraj sir,user should not manually enter store manager details
                        if (_Request.DisplayName.Length > 30)
                        {
                            _HCUAccount.DisplayName = _Request.DisplayName.Substring(0, 29);
                        }
                        else
                        {
                            _HCUAccount.DisplayName = _Request.DisplayName;
                        }
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                        _HCUAccount.AccountCode = AccountCode;
                        if (_Request.UserReference.AppVersionId != 0)
                        {
                            _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                        }
                        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                        }
                        _HCUAccount.Name = _Request.Name;
                        //_HCUAccount.ContactNumber = _Request.ContactNumber;
                        if (_Request.ContactNumber != null)
                        {
                            //   string VMobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactNumber, _Request.ContactNumber.Length);
                            _HCUAccount.ContactNumber = _Request.ContactNumber;
                        }
                        _HCUAccount.EmailAddress = _Request.EmailAddress;
                        if (_Request.ContactPerson != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.FirstName))
                            {
                                _HCUAccount.FirstName = _Request.ContactPerson.FirstName;
                                _HCUAccount.CpFirstName = _Request.ContactPerson.FirstName;
                            }
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.LastName))
                            {
                                _HCUAccount.LastName = _Request.ContactPerson.LastName;
                                _HCUAccount.CpLastName = _Request.ContactPerson.LastName;
                            }
                            _HCUAccount.CpName = _Request.ContactPerson.FirstName + " " + _Request.ContactPerson.LastName;
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.MobileNumber))
                            {
                                //_HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber, _Request.ContactPerson.MobileNumber.Length);
                                _HCUAccount.MobileNumber = _Request.ContactPerson.MobileNumber;
                            }
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.EmailAddress))
                            {
                                _HCUAccount.SecondaryEmailAddress = _Request.ContactPerson.EmailAddress;
                            }
                        }
                        if (_AddressResponse != null)
                        {
                            if (!string.IsNullOrEmpty(_AddressResponse.Address))
                            {
                                _HCUAccount.Address = _AddressResponse.Address;
                            }
                            if (_AddressResponse.CityAreaId != 0)
                            {
                                _HCUAccount.CityAreaId = _AddressResponse.CityAreaId;
                            }
                            if (_AddressResponse.CityId != 0)
                            {
                                _HCUAccount.CityId = _AddressResponse.CityId;
                            }
                            if (_AddressResponse.StateId != 0)
                            {
                                _HCUAccount.StateId = _AddressResponse.StateId;
                            }
                            if (_AddressResponse.CountryId != 0)
                            {
                                _HCUAccount.CountryId = _AddressResponse.CountryId;
                            }
                            if (_AddressResponse.Latitude != 0)
                            {
                                _HCUAccount.Latitude = _AddressResponse.Latitude;
                            }
                            if (_AddressResponse.Longitude != 0)
                            {
                                _HCUAccount.Longitude = _AddressResponse.Longitude;
                            }
                        }
                        _HCUAccount.EmailVerificationStatus = 0;
                        _HCUAccount.NumberVerificationStatus = 0;
                        _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccounts.Add(_HCUAccount);
                        _HCUAccountAuth = new HCUAccountAuth();
                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(6);
                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                        _HCUAccountAuth.SecondaryPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                        }
                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                        _HCUAccountAuth.HCUAccount = _HCUAccounts;
                        _HCoreContext.HCUAccountAuth.Add(_HCUAccountAuth);
                        _HCoreContext.SaveChanges();
                        var Response = new
                        {
                            ReferenceId = _HCUAccount.Id,
                            ReferenceKey = _HCUAccount.Guid,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, TUCCoreResource.CA1078, TUCCoreResource.CA1078M);
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("SaveStore", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the cashier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveCashier(OAccountOperations.Cashier.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                if (_Request.StoreId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.StoreKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.AddressComponent, _Request.UserReference);
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {



                    long? StoreMerchantId = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.StoreId).Select(x => x.OwnerId).FirstOrDefault();
                    if (!string.IsNullOrEmpty(_Request.EmailAddress))
                    {
                        bool DataCheck = _HCoreContext.HCUAccount.Any(x =>
                                x.Owner.OwnerId == StoreMerchantId &&
                                x.EmailAddress == _Request.EmailAddress &&
                                x.AccountTypeId == UserAccountType.MerchantCashier);
                        if (DataCheck)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1428, TUCCoreResource.CA1428M);
                        }
                    }
                    if (!string.IsNullOrEmpty(_Request.ContactNumber))
                    {
                        bool DataCheck = _HCoreContext.HCUAccount.Any(x =>
                                x.Owner.OwnerId == StoreMerchantId &&
                                x.ContactNumber == _Request.ContactNumber &&
                                x.AccountTypeId == UserAccountType.MerchantCashier);
                        if (DataCheck)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1429, TUCCoreResource.CA1429M);
                        }
                    }
                    if (!string.IsNullOrEmpty(_Request.EmployeeCode))
                    {
                        bool DataCheck = _HCoreContext.HCUAccount.Any(x =>
                                x.Owner.OwnerId == StoreMerchantId &&
                                x.ReferralCode == _Request.EmployeeCode &&
                                x.AccountTypeId == UserAccountType.MerchantCashier);
                        if (DataCheck)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1430, TUCCoreResource.CA1430M);
                        }
                    }
                    //string CashierId = "";
                    string CashierId = HCoreHelper.GenerateRandomNumber(4);
                    bool CashierCodeCheck = _HCoreContext.HCUAccount.Any(x =>
                                 x.Owner.OwnerId == StoreMerchantId &&
                                 x.DisplayName == CashierId &&
                                 x.AccountTypeId == UserAccountType.MerchantCashier);
                    if (CashierCodeCheck)
                    {
                        CashierId = HCoreHelper.GenerateRandomNumber(4);
                    }
                    _Random = new Random();
                    string AccountCode = _Random.Next(1000, 9999).ToString() + _Random.Next(000000000, 999999999).ToString();
                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(6);
                    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                    _HCUAccountAuth.SecondaryPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.User = _HCUAccountAuth;
                    _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccount.AccountTypeId = UserAccountType.MerchantCashier;
                    _HCUAccount.AccountOperationTypeId = Helpers.AccountOperationType.Offline;
                    _HCUAccount.OwnerId = _Request.StoreId;
                    _HCUAccount.DisplayName = CashierId;
                    _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = AccountCode;
                    if (_Request.GenderCode == "gender.male")
                    {
                        _HCUAccount.GenderId = Helpers.Gender.Male;
                    }
                    if (_Request.GenderCode == "gender.female")
                    {
                        _HCUAccount.GenderId = Helpers.Gender.Female;
                    }
                    if (_Request.GenderCode == "gender.other")
                    {
                        _HCUAccount.GenderId = Helpers.Gender.Other;
                    }
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccount.Name = _Request.FirstName + " " + _Request.LastName;
                    _HCUAccount.ContactNumber = _Request.ContactNumber;
                    _HCUAccount.EmailAddress = _Request.EmailAddress;
                    _HCUAccount.ReferralCode = _Request.EmployeeCode;
                    if (!string.IsNullOrEmpty(_Request.FirstName))
                    {
                        _HCUAccount.FirstName = _Request.FirstName;
                    }
                    if (!string.IsNullOrEmpty(_Request.LastName))
                    {
                        _HCUAccount.LastName = _Request.LastName;
                    }

                    if (_AddressResponse != null)
                    {
                        if (!string.IsNullOrEmpty(_AddressResponse.Address))
                        {
                            _HCUAccount.Address = _AddressResponse.Address;
                        }
                        if (_AddressResponse.CityAreaId != 0)
                        {
                            _HCUAccount.CityAreaId = _AddressResponse.CityAreaId;
                        }
                        if (_AddressResponse.CityId != 0)
                        {
                            _HCUAccount.CityId = _AddressResponse.CityId;
                        }
                        if (_AddressResponse.StateId != 0)
                        {
                            _HCUAccount.StateId = _AddressResponse.StateId;
                        }
                        if (_AddressResponse.CountryId != 0)
                        {
                            _HCUAccount.CountryId = _AddressResponse.CountryId;
                        }
                        if (_AddressResponse.Latitude != 0)
                        {
                            _HCUAccount.Latitude = _AddressResponse.Latitude;
                        }
                        if (_AddressResponse.Longitude != 0)
                        {
                            _HCUAccount.Longitude = _AddressResponse.Longitude;
                        }
                    }
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.RegistrationSourceId = RegistrationSource.System;
                    _HCUAccount.StatusId = HelperStatus.Default.Active;
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    _HCoreContext.SaveChanges();
                    long? IconStorageId = null;
                    if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                    {
                        IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                    }
                    if (IconStorageId != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var UserAccDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _HCUAccount.Id).FirstOrDefault();
                            if (UserAccDetails != null)
                            {
                                if (IconStorageId != null)
                                {
                                    UserAccDetails.IconStorageId = IconStorageId;
                                }
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    var Response = new
                    {
                        ReferenceId = _HCUAccount.Id,
                        ReferenceKey = _HCUAccount.Guid,
                    };

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, TUCCoreResource.CA1078, TUCCoreResource.CA1078M);
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("SaveCashier", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the sub account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveSubAccount(OAccountOperations.SubAccount.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                if (string.IsNullOrEmpty(_Request.RoleKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1366, TUCCoreResource.CA1366M);
                }

                //long RoleId = HCoreHelper.GetCoreParameterId(_Request.RoleKey);
                //if (RoleId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1197, TUCCoreResource.CA1197M);
                //}

                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.AddressComponent, _Request.UserReference);
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    if (!string.IsNullOrEmpty(_Request.EmailAddress))
                    {
                        var EmailCheck = _HCoreContext.HCUAccount.Any(x => x.AccountTypeId == UserAccountType.MerchantSubAccount && x.EmailAddress == _Request.EmailAddress);
                        if (EmailCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1394, TUCCoreResource.CA1394M);
                        }
                    }
                    if (!string.IsNullOrEmpty(_Request.ContactNumber))
                    {
                        var EmailCheck = _HCoreContext.HCUAccount.Any(x => x.AccountTypeId == UserAccountType.MerchantSubAccount && x.ContactNumber == _Request.ContactNumber);
                        if (EmailCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1395, TUCCoreResource.CA1395M);
                        }
                    }
                    _Random = new Random();
                    string AccountCode = _Random.Next(10000000, 999999999).ToString() + "" + _Random.Next(10000000, 999999999).ToString();
                    _HCUAccounts = new List<HCUAccount>();
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccount.AccountTypeId = UserAccountType.MerchantSubAccount;
                    _HCUAccount.AccountOperationTypeId = Helpers.AccountOperationType.OnlineAndOffline;
                    _HCUAccount.OwnerId = _Request.AccountId;
                    if (_Request.StoreReferenceId > 0)
                    {
                        _HCUAccount.SubOwnerId = _Request.StoreReferenceId;
                    }
                    _HCUAccount.RoleId = _Request.RoleId;
                    _HCUAccount.DisplayName = _Request.FirstName;
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = AccountCode;
                    if (_Request.GenderCode == "gender.male")
                    {
                        _HCUAccount.GenderId = Helpers.Gender.Male;
                    }
                    if (_Request.GenderCode == "gender.female")
                    {
                        _HCUAccount.GenderId = Helpers.Gender.Female;
                    }
                    if (_Request.GenderCode == "gender.other")
                    {
                        _HCUAccount.GenderId = Helpers.Gender.Other;
                    }
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccount.Name = _Request.FirstName + " " + _Request.LastName;
                    _HCUAccount.ContactNumber = _Request.ContactNumber;
                    _HCUAccount.EmailAddress = _Request.EmailAddress;
                    if (!string.IsNullOrEmpty(_Request.FirstName))
                    {
                        _HCUAccount.FirstName = _Request.FirstName;
                    }
                    if (!string.IsNullOrEmpty(_Request.LastName))
                    {
                        _HCUAccount.LastName = _Request.LastName;
                    }
                    if (_AddressResponse != null)
                    {
                        if (!string.IsNullOrEmpty(_AddressResponse.Address))
                        {
                            _HCUAccount.Address = _AddressResponse.Address;
                        }
                        if (_AddressResponse.CityAreaId != 0)
                        {
                            _HCUAccount.CityAreaId = _AddressResponse.CityAreaId;
                        }
                        if (_AddressResponse.CityId != 0)
                        {
                            _HCUAccount.CityId = _AddressResponse.CityId;
                        }
                        if (_AddressResponse.StateId != 0)
                        {
                            _HCUAccount.StateId = _AddressResponse.StateId;
                        }
                        if (_AddressResponse.CountryId != 0)
                        {
                            _HCUAccount.CountryId = _AddressResponse.CountryId;
                        }
                        if (_AddressResponse.Latitude != 0)
                        {
                            _HCUAccount.Latitude = _AddressResponse.Latitude;
                        }
                        if (_AddressResponse.Longitude != 0)
                        {
                            _HCUAccount.Longitude = _AddressResponse.Longitude;
                        }
                    }
                    else
                    {
                        _HCUAccount.CountryId = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.StoreReferenceId).Select(x => x.CountryId).FirstOrDefault();
                    }
                    if (_Request.StoreReferenceId == 0)
                    {
                        _HCUAccount.CountryId = _Request.UserReference.CountryId;
                    }
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.RegistrationSourceId = RegistrationSource.System;
                    _HCUAccount.StatusId = HelperStatus.Default.Active;
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccounts.Add(_HCUAccount);
                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(12);
                    string Password = HCoreHelper.GenerateRandomNumber(10);
                    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(Password);
                    _HCUAccountAuth.SecondaryPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    _HCUAccountAuth.HCUAccount = _HCUAccounts;
                    _HCoreContext.HCUAccountAuth.Add(_HCUAccountAuth);
                    _HCoreContext.SaveChanges();


                    long? ImageStorageId = null;
                    if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                    {
                        ImageStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                    }
                    if (ImageStorageId != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _HCUAccount.Guid).FirstOrDefault();
                            if (AccountDetails != null)
                            {
                                if (ImageStorageId != null)
                                {
                                    AccountDetails.IconStorageId = ImageStorageId;
                                }
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(_Request.EmailAddress))
                    {
                        var _EmailParameters = new
                        {
                            DisplayName = _Request.FirstName,
                            UserName = _HCUAccountAuth.Username,
                            Password = Password,
                            AccessPin = HCoreEncrypt.DecryptHash(_HCUAccount.AccessPin)
                        };
                        HCoreHelper.BroadCastEmail(NotificationTemplates.AccountCredentails, _Request.FirstName, _Request.EmailAddress, _EmailParameters, _Request.UserReference);
                    }
                    var Response = new
                    {
                        ReferenceId = _HCUAccount.Id,
                        ReferenceKey = _HCUAccount.Guid,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, TUCCoreResource.CA1078, TUCCoreResource.CA1078M);
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("SaveSubAccount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateMerchant(OAccountOperations.Merchant.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.AddressComponent, _Request.UserReference);
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey && x.Id == _Request.AccountId && x.AccountTypeId == UserAccountType.Merchant)
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.OwnerName) && Details.FirstName != _Request.OwnerName)
                        {
                            Details.FirstName = _Request.OwnerName;
                        }
                        if (!string.IsNullOrEmpty(_Request.DisplayName) && Details.DisplayName != _Request.DisplayName)
                        {
                            Details.DisplayName = _Request.DisplayName;
                        }
                        if (!string.IsNullOrEmpty(_Request.Name) && Details.Name != _Request.Name)
                        {
                            Details.Name = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.ContactNumber) && Details.ContactNumber != _Request.ContactNumber)
                        {
                            Details.ContactNumber = _Request.ContactNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.EmailAddress) && Details.EmailAddress != _Request.EmailAddress)
                        {
                            Details.EmailAddress = _Request.EmailAddress;
                        }
                        if (!string.IsNullOrEmpty(_Request.WebsiteUrl) && Details.WebsiteUrl != _Request.WebsiteUrl)
                        {
                            Details.WebsiteUrl = _Request.WebsiteUrl;
                        }
                        if (!string.IsNullOrEmpty(_Request.Description) && Details.Description != _Request.Description)
                        {
                            Details.Description = _Request.Description;
                        }
                        if (_Request.ContactPerson != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.FirstName) && Details.FirstName != _Request.ContactPerson.FirstName)
                            {
                                Details.CpFirstName = _Request.ContactPerson.FirstName;
                            }
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.LastName) && Details.LastName != _Request.ContactPerson.LastName)
                            {
                                Details.LastName = _Request.ContactPerson.LastName;
                                Details.CpLastName = _Request.ContactPerson.LastName;
                            }
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.MobileNumber) && Details.MobileNumber != _Request.ContactPerson.MobileNumber)
                            {
                                Details.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber, _Request.ContactPerson.MobileNumber.Length);
                                Details.CpMobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber, _Request.ContactPerson.MobileNumber.Length);
                            }
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.EmailAddress) && Details.SecondaryEmailAddress != _Request.ContactPerson.EmailAddress)
                            {
                                Details.SecondaryEmailAddress = _Request.ContactPerson.EmailAddress;
                                Details.CpEmailAddress = _Request.ContactPerson.EmailAddress;
                            }
                        }
                        if (_AddressResponse != null)
                        {
                            if (!string.IsNullOrEmpty(_AddressResponse.Address) && Details.Address != _AddressResponse.Address)
                            {
                                Details.Address = _AddressResponse.Address;
                            }
                            if (_AddressResponse.CityAreaId != 0)
                            {
                                Details.CityAreaId = _AddressResponse.CityAreaId;
                            }
                            if (_AddressResponse.CityId != 0)
                            {
                                Details.CityId = _AddressResponse.CityId;
                            }
                            if (_AddressResponse.StateId != 0)
                            {
                                Details.StateId = _AddressResponse.StateId;
                            }
                            if (_AddressResponse.CountryId != 0)
                            {
                                Details.CountryId = _AddressResponse.CountryId;
                            }
                            if (_AddressResponse.Latitude != 0)
                            {
                                Details.Latitude = _AddressResponse.Latitude;
                            }
                            if (_AddressResponse.Longitude != 0)
                            {
                                Details.Longitude = _AddressResponse.Longitude;
                            }
                        }
                        if (_Request.Categories != null && _Request.Categories.Count > 0)
                        {
                            Details.PrimaryCategoryId = _Request.Categories.FirstOrDefault().ReferenceId;
                        }
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        if (_Request.Categories != null && _Request.Categories.Count > 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var Iems = _HCoreContext.TUCCategoryAccount.Where(x => x.AccountId == _Request.AccountId).ToList();
                                _HCoreContext.TUCCategoryAccount.RemoveRange(Iems);
                                _HCoreContext.SaveChanges();
                            }
                            using (_HCoreContext = new HCoreContext())
                            {
                                _TUCCategoryAccount = new List<TUCCategoryAccount>();
                                _HCUAccountParameters = new List<HCUAccountParameter>();
                                foreach (var Category in _Request.Categories)
                                {
                                    _TUCCategoryAccount.Add(new TUCCategoryAccount
                                    {
                                        Guid = HCoreHelper.GenerateGuid(),
                                        AccountId = _Request.AccountId,
                                        TypeId = 1,
                                        CategoryId = Category.ReferenceId,
                                        CreateDate = HCoreHelper.GetGMTDateTime(),
                                        CreatedById = _Request.UserReference.AccountId,
                                        StatusId = HelperStatus.Default.Active,
                                    });
                                    //_HCUAccountParameters.Add(new HCUAccountParameter
                                    //{
                                    //    Guid = HCoreHelper.GenerateGuid(),
                                    //    AccountId = _Request.AccountId,
                                    //    TypeId = HelperType.MerchantCategory,
                                    //    CommonId = Category.ReferenceId,
                                    //    CreateDate = HCoreHelper.GetGMTDateTime(),
                                    //    StatusId = HelperStatus.Default.Active,
                                    //});
                                }
                                if (_TUCCategoryAccount.Count() > 0)
                                {
                                    _HCoreContext.TUCCategoryAccount.AddRange(_TUCCategoryAccount);
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        long? IconStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, Details.IconStorageId, _Request.UserReference);
                        }
                        if (IconStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var UserAccDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId).FirstOrDefault();
                                if (UserAccDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        UserAccDetails.IconStorageId = IconStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }

                        using (_HCoreContextOperations = new HCoreContextOperations())
                        {
                            var MerchantDetails = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => (x.EmailAddress == Details.EmailAddress || x.EmailAddress == _Request.EmailAddress) || x.AccountId == Details.Id).FirstOrDefault();
                            if (MerchantDetails != null)
                            {
                                MerchantDetails.EmailAddress = _Request.EmailAddress;
                                MerchantDetails.AccountId = Details.Id;
                                _HCoreContextOperations.SaveChanges();
                                _HCoreContextOperations.Dispose();
                            }
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1075, TUCCoreResource.CA1075M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateMerchant", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateStore(OAccountOperations.Store.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.AddressComponent, _Request.UserReference);
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey && x.Id == _Request.AccountId && x.AccountTypeId == UserAccountType.MerchantStore)
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        var UserDetails = _HCoreContext.HCUAccountAuth.Where(x => x.Id == Details.UserId).FirstOrDefault();
                        if (!string.IsNullOrEmpty(_Request.Username) && _Request.Username != UserDetails.Username)
                        {
                            bool UCheck = _HCoreContext.HCUAccountAuth.Any(x => x.Username == _Request.Username && x.Id != Details.Id);
                            if (UCheck)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1365, TUCCoreResource.CA1365M);
                            }
                            if (UserDetails != null)
                            {
                                UserDetails.Username = _Request.Username;
                                UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                UserDetails.ModifyById = _Request.UserReference.AccountId;
                            }
                        }
                        if (!string.IsNullOrEmpty(_Request.Password))
                        {
                            UserDetails.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                            UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            UserDetails.ModifyById = _Request.UserReference.AccountId;
                        }
                        if (!string.IsNullOrEmpty(_Request.DisplayName) && Details.DisplayName != _Request.DisplayName)
                        {
                            Details.DisplayName = _Request.DisplayName;
                        }
                        if (!string.IsNullOrEmpty(_Request.Name) && Details.Name != _Request.Name)
                        {
                            Details.Name = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.ContactNumber) && Details.ContactNumber != _Request.ContactNumber)
                        {
                            Details.ContactNumber = _Request.ContactNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.EmailAddress) && Details.EmailAddress != _Request.EmailAddress)
                        {
                            Details.EmailAddress = _Request.EmailAddress;
                        }
                        if (!string.IsNullOrEmpty(_Request.SecondaryEmailAddress) && Details.SecondaryEmailAddress != _Request.SecondaryEmailAddress)
                        {
                            Details.SecondaryEmailAddress = _Request.SecondaryEmailAddress;
                        }
                        if (_Request.ContactPerson != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.FirstName) && Details.FirstName != _Request.ContactPerson.FirstName)
                            {
                                Details.FirstName = _Request.ContactPerson.FirstName;
                            }
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.LastName) && Details.LastName != _Request.ContactPerson.LastName)
                            {
                                Details.LastName = _Request.ContactPerson.LastName;
                            }
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.MobileNumber) && Details.MobileNumber != _Request.ContactPerson.MobileNumber)
                            {
                                Details.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber, _Request.ContactPerson.MobileNumber.Length);
                            }
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.EmailAddress) && Details.SecondaryEmailAddress != _Request.ContactPerson.EmailAddress)
                            {
                                Details.SecondaryEmailAddress = _Request.ContactPerson.EmailAddress;
                            }
                        }
                        if (_AddressResponse != null)
                        {
                            if (!string.IsNullOrEmpty(_AddressResponse.Address) && Details.Address != _AddressResponse.Address)
                            {
                                Details.Address = _AddressResponse.Address;
                            }
                            if (_AddressResponse.CityAreaId != 0)
                            {
                                Details.CityAreaId = _AddressResponse.CityAreaId;
                            }
                            if (_AddressResponse.CityId != 0)
                            {
                                Details.CityId = _AddressResponse.CityId;
                            }
                            if (_AddressResponse.StateId != 0)
                            {
                                Details.StateId = _AddressResponse.StateId;
                            }
                            if (_AddressResponse.CountryId != 0)
                            {
                                Details.CountryId = _AddressResponse.CountryId;
                            }

                            if (_AddressResponse.Latitude != 0)
                            {
                                Details.Latitude = _AddressResponse.Latitude;
                            }
                            if (_AddressResponse.Longitude != 0)
                            {
                                Details.Longitude = _AddressResponse.Longitude;
                            }
                        }
                        if (_Request.Categories != null && _Request.Categories.Count > 0)
                        {
                            Details.PrimaryCategoryId = _Request.Categories.FirstOrDefault().ReferenceId;
                        }
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.ModifyById = _Request.UserReference.AccountId;
                        Details.SubOwnerId = _Request.StoreManagerId; // Change Request as per suraj sir,user should not manually enter store manager details
                        _HCoreContext.SaveChanges();
                        if (_Request.Categories != null && _Request.Categories.Count > 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var Iems = _HCoreContext.TUCCategoryAccount.Where(x => x.AccountId == _Request.AccountId).ToList();
                                _HCoreContext.TUCCategoryAccount.RemoveRange(Iems);
                                _HCoreContext.SaveChanges();
                            }
                            using (_HCoreContext = new HCoreContext())
                            {
                                _TUCCategoryAccount = new List<TUCCategoryAccount>();
                                _HCUAccountParameters = new List<HCUAccountParameter>();
                                foreach (var Category in _Request.Categories)
                                {
                                    //var CategoryCheck = _HCoreContext.HCUAccountParameter.Any(x => x.TypeId == 1 && x.AccountId == _Request.AccountId && x.CommonId == Category.ReferenceId);
                                    //if (!CategoryCheck)
                                    //{
                                    //    _HCUAccountParameters.Add(new HCUAccountParameter
                                    //    {
                                    //        Guid = HCoreHelper.GenerateGuid(),
                                    //        AccountId = _Request.AccountId,
                                    //        TypeId = 1,
                                    //        //TypeId = HelperType.MerchantCategory,
                                    //        CommonId = Category.ReferenceId,
                                    //        CreateDate = HCoreHelper.GetGMTDateTime(),
                                    //        StatusId = HelperStatus.Default.Active,
                                    //    });
                                    //}
                                    _TUCCategoryAccount.Add(new TUCCategoryAccount
                                    {
                                        Guid = HCoreHelper.GenerateGuid(),
                                        AccountId = _Request.AccountId,
                                        TypeId = 1,
                                        CategoryId = Category.ReferenceId,
                                        CreateDate = HCoreHelper.GetGMTDateTime(),
                                        CreatedById = _Request.UserReference.AccountId,
                                        StatusId = HelperStatus.Default.Active,
                                    });
                                }
                                if (_TUCCategoryAccount.Count() > 0)
                                {
                                    _HCoreContext.TUCCategoryAccount.AddRange(_TUCCategoryAccount);
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1076, TUCCoreResource.CA1076M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateStore", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the cashier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateCashier(OAccountOperations.Cashier.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.AddressComponent, _Request.UserReference);
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey
                        && x.Id == _Request.AccountId
                        && x.AccountTypeId == UserAccountType.MerchantCashier)
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.GenderCode))
                        {
                            if (_Request.GenderCode == "gender.male")
                            {
                                Details.GenderId = Gender.Male;
                            }
                            if (_Request.GenderCode == "gender.female")
                            {
                                Details.GenderId = Helpers.Gender.Female;
                            }
                            if (_Request.GenderCode == "gender.other")
                            {
                                Details.GenderId = Helpers.Gender.Other;
                            }
                        }
                        if (_Request.StoreId != 0)
                        {
                            Details.OwnerId = _Request.StoreId;
                        }
                        if (!string.IsNullOrEmpty(_Request.FirstName) && Details.Name != _Request.FirstName)
                        {
                            Details.FirstName = _Request.FirstName;
                        }
                        if (!string.IsNullOrEmpty(_Request.FirstName) && Details.Name != _Request.FirstName)
                        {
                            Details.LastName = _Request.LastName;
                        }
                        Details.Name = Details.FirstName + " " + Details.LastName;
                        if (!string.IsNullOrEmpty(_Request.ContactNumber) && Details.ContactNumber != _Request.ContactNumber)
                        {
                            Details.ContactNumber = _Request.ContactNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.EmailAddress) && Details.EmailAddress != _Request.EmailAddress)
                        {
                            Details.EmailAddress = _Request.EmailAddress;
                        }
                        if (!string.IsNullOrEmpty(_Request.EmployeeCode) && Details.ReferralCode != _Request.EmployeeCode)
                        {
                            Details.ReferralCode = _Request.EmployeeCode;
                        }
                        if (_AddressResponse != null)
                        {
                            if (!string.IsNullOrEmpty(_AddressResponse.Address) && Details.Address != _AddressResponse.Address)
                            {
                                Details.Address = _AddressResponse.Address;
                            }
                            if (_AddressResponse.CityAreaId != 0)
                            {
                                Details.CityAreaId = _AddressResponse.CityAreaId;
                            }
                            if (_AddressResponse.CityId != 0)
                            {
                                Details.CityId = _AddressResponse.CityId;
                            }
                            if (_AddressResponse.StateId != 0)
                            {
                                Details.StateId = _AddressResponse.StateId;
                            }
                            if (_AddressResponse.CountryId != 0)
                            {
                                Details.CountryId = _AddressResponse.CountryId;
                            }
                        }
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        long? IconStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, Details.IconStorageId, _Request.UserReference);
                        }
                        if (IconStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var UserAccDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId).FirstOrDefault();
                                if (UserAccDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        UserAccDetails.IconStorageId = IconStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1076, TUCCoreResource.CA1076M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateCashier", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the sub account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateSubAccount(OAccountOperations.SubAccount.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.AddressComponent, _Request.UserReference);
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey
                        && x.Id == _Request.AccountId
                        && x.AccountTypeId == UserAccountType.MerchantSubAccount)
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        bool isChangedEmail = false;
                        if (!string.IsNullOrEmpty(_Request.EmailAddress) && Details.EmailAddress != _Request.EmailAddress)
                        {
                            isChangedEmail = true;
                            var EmailCheck = _HCoreContext.HCUAccount.Any(x => x.AccountTypeId == UserAccountType.MerchantSubAccount && x.Id != Details.Id && x.EmailAddress == _Request.EmailAddress);
                            if (EmailCheck)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1394, TUCCoreResource.CA1394M);
                            }
                        }
                        if (!string.IsNullOrEmpty(_Request.ContactNumber) && Details.ContactNumber != _Request.ContactNumber)
                        {
                            var EmailCheck = _HCoreContext.HCUAccount.Any(x => x.AccountTypeId == UserAccountType.MerchantSubAccount && x.Id != Details.Id && x.ContactNumber == _Request.ContactNumber);
                            if (EmailCheck)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1395, TUCCoreResource.CA1395M);
                            }
                        }


                        if (Details.RoleId > 0)
                        {
                            Details.RoleId = _Request.RoleId;
                        }
                        if (_Request.StoreReferenceId > 0)
                        {
                            Details.SubOwnerId = _Request.StoreReferenceId;
                        }
                        if (!string.IsNullOrEmpty(_Request.GenderCode))
                        {
                            if (_Request.GenderCode == "gender.male")
                            {
                                Details.GenderId = Gender.Male;
                            }
                            if (_Request.GenderCode == "gender.female")
                            {
                                Details.GenderId = Helpers.Gender.Female;
                            }
                            if (_Request.GenderCode == "gender.other")
                            {
                                Details.GenderId = Helpers.Gender.Other;
                            }
                        }
                        if (!string.IsNullOrEmpty(_Request.FirstName) && Details.Name != _Request.FirstName)
                        {
                            Details.FirstName = _Request.FirstName;
                        }
                        if (!string.IsNullOrEmpty(_Request.FirstName) && Details.Name != _Request.FirstName)
                        {
                            Details.LastName = _Request.LastName;
                        }
                        Details.Name = Details.FirstName + " " + Details.LastName;
                        if (!string.IsNullOrEmpty(_Request.ContactNumber) && Details.ContactNumber != _Request.ContactNumber)
                        {
                            Details.ContactNumber = _Request.ContactNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.EmailAddress) && Details.EmailAddress != _Request.EmailAddress)
                        {
                            Details.EmailAddress = _Request.EmailAddress;
                        }
                        if (_AddressResponse != null)
                        {
                            if (!string.IsNullOrEmpty(_AddressResponse.Address) && Details.Address != _AddressResponse.Address)
                            {
                                Details.Address = _AddressResponse.Address;
                            }
                            if (_AddressResponse.CityAreaId != 0)
                            {
                                Details.CityAreaId = _AddressResponse.CityAreaId;
                            }
                            if (_AddressResponse.CityId != 0)
                            {
                                Details.CityId = _AddressResponse.CityId;
                            }
                            if (_AddressResponse.StateId != 0)
                            {
                                Details.StateId = _AddressResponse.StateId;
                            }
                            if (_AddressResponse.CountryId != 0)
                            {
                                Details.CountryId = _AddressResponse.CountryId;
                            }
                        }
                        _HCoreContext.SaveChanges();
                        if (isChangedEmail)
                        {
                            var details = _HCoreContext.HCUAccountAuth.Where(x => x.Id == Details.UserId)
                                .Select(x => new
                                {
                                    Username = x.Username,
                                    Password = x.Password
                                }).FirstOrDefault();
                            if (details != null)
                            {
                                var _EmailParameters = new
                                {
                                    DisplayName = _Request.FirstName,
                                    UserName = details.Username,
                                    Password = HCoreEncrypt.DecryptHash(details.Password),
                                    AccessPin = HCoreEncrypt.DecryptHash(Details.AccessPin)
                                };
                                HCoreHelper.BroadCastEmail(NotificationTemplates.AccountCredentails, _Request.FirstName, _Request.EmailAddress, _EmailParameters, _Request.UserReference);
                            }
                        }
                        long? IconStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, Details.IconStorageId, _Request.UserReference);
                        }
                        if (IconStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var UserAccDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId).FirstOrDefault();
                                if (UserAccDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        UserAccDetails.IconStorageId = IconStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1076, TUCCoreResource.CA1076M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateSubAccount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Resets the sub account password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ResetSubAccountPassword(OAccountOperations.SubAccount.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey
                        && x.Id == _Request.AccountId
                        && x.AccountTypeId == UserAccountType.MerchantSubAccount)
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        string NewPassword = HCoreHelper.GenerateRandomNumber(6);
                        var User = _HCoreContext.HCUAccountAuth.Where(x => x.Id == Details.UserId).FirstOrDefault();
                        if (_Request.IsAutoGenerated)
                        {
                            User.Password = HCoreEncrypt.EncryptHash(NewPassword);
                        }
                        else
                        {
                            User.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                            NewPassword = _Request.Password;
                        }
                        User.ModifyDate = HCoreHelper.GetGMTDateTime();
                        User.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        if (!string.IsNullOrEmpty(Details.EmailAddress))
                        {
                            var _EmailParameters = new
                            {
                                DisplayName = Details.FirstName,
                                UserName = User.Username,
                                Password = NewPassword,
                                AccessPin = HCoreEncrypt.DecryptHash(_HCUAccount.AccessPin)
                            };
                            HCoreHelper.BroadCastEmail(NotificationTemplates.AccountCredentails, Details.FirstName, Details.EmailAddress, _EmailParameters, _Request.UserReference);
                        }
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1076, TUCCoreResource.CA1076M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ResetSubAccountPassword", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the terminal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveTerminal(OAccountOperations.Terminal.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.TerminalId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1038, TUCCoreResource.CA1038M);
                }
                if (_Request.MerchantReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1039, TUCCoreResource.CA1039M);
                }
                if (string.IsNullOrEmpty(_Request.MerchantReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1040, TUCCoreResource.CA1040M);
                }
                if (_Request.StoreReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1041, TUCCoreResource.CA1041M);
                }
                if (string.IsNullOrEmpty(_Request.StoreReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1042, TUCCoreResource.CA1042);
                }
                if (_Request.AcquirerReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1043, TUCCoreResource.CA1043M);
                }
                if (string.IsNullOrEmpty(_Request.AcquirerReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1044, TUCCoreResource.CA1044M);
                }
                if (_Request.ProviderReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1045, TUCCoreResource.CA1045M);
                }
                if (string.IsNullOrEmpty(_Request.ProviderReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1046, TUCCoreResource.CA1046M);
                }

                //int? TypeId = 0;
                //if(!string.IsNullOrEmpty(_Request.TypeCode))
                //{
                int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                //    if(TypeId <= 0)
                //    {
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.POS0001, TUCCoreResource.POS0001M);
                //    }
                //}

                using (_HCoreContext = new HCoreContext())
                {
                    bool TerminalIdCheck = _HCoreContext.TUCTerminal.Any(x => x.IdentificationNumber == _Request.TerminalId);
                    if (TerminalIdCheck)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1051, TUCCoreResource.CA1051M);
                    }
                    //_HCUAccountAuth = new HCUAccountAuth();
                    //_HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    //_HCUAccountAuth.Username = _Request.TerminalId;
                    //_HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.TerminalId);
                    //_HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    //_HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    //_HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    //if (_Request.UserReference.AccountId != 0)
                    //{
                    //    _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    //}
                    //_HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    //#region Save UserAccount
                    //_HCUAccount = new HCUAccount();
                    //_HCUAccount.Guid = HCoreHelper.GenerateGuid();
                    //_HCUAccount.AccountTypeId = UserAccountType.TerminalAccount;
                    //_HCUAccount.AccountOperationTypeId = AccountOperationType.Offline;
                    //_HCUAccount.OwnerId = _Request.ProviderReferenceId;
                    //_HCUAccount.BankId = _Request.AcquirerReferenceId;
                    //_HCUAccount.SubOwnerId = _Request.StoreReferenceId;
                    //_HCUAccount.DisplayName = _Request.TerminalId;
                    //_HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    //_HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
                    //if (_Request.UserReference.AppVersionId != 0)
                    //{
                    //    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    //}
                    //_HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    //_HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                    //_HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    //if (_Request.UserReference.AccountId != 0)
                    //{
                    //    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    //}
                    //_HCUAccount.StatusId = HelperStatus.Default.Active;
                    //_HCUAccount.Name = _Request.TerminalId;
                    //_HCUAccount.CountryId = _Request.UserReference.CountryId;
                    //_HCUAccount.EmailVerificationStatus = 0;
                    //_HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    //_HCUAccount.NumberVerificationStatus = 0;
                    //_HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    //_HCUAccount.User = _HCUAccountAuth;
                    //#endregion
                    _TUCTerminal = new TUCTerminal();
                    _TUCTerminal.Guid = HCoreHelper.GenerateGuid();
                    _TUCTerminal.DisplayName = _Request.TerminalId;
                    _TUCTerminal.IdentificationNumber = _Request.TerminalId;
                    _TUCTerminal.SerialNumber = _Request.SerialNumber;
                    //_TUCTerminal.Account = _HCUAccount;
                    if (_Request.MerchantReferenceId != 0)
                    {
                        _TUCTerminal.MerchantId = _Request.MerchantReferenceId;
                    }
                    if (_Request.StoreReferenceId != 0)
                    {
                        _TUCTerminal.StoreId = _Request.StoreReferenceId;
                    }
                    if (_Request.CashierReferenceId != 0)
                    {
                        _TUCTerminal.CashierId = _Request.CashierReferenceId;
                    }
                    if (_Request.ProviderReferenceId != 0)
                    {
                        _TUCTerminal.ProviderId = _Request.ProviderReferenceId;
                    }
                    if (_Request.AcquirerReferenceId != 0)
                    {
                        _TUCTerminal.AcquirerId = _Request.AcquirerReferenceId;
                    }

                    if (TypeId != null)
                    {
                        _TUCTerminal.TypeId = (int)TypeId;
                    }
                    else
                    {
                        _TUCTerminal.TypeId = UserAccountType.PosAccount;
                    }

                    _TUCTerminal.CreateDate = HCoreHelper.GetGMTDateTime();
                    _TUCTerminal.CreatedById = _Request.UserReference.AccountId;
                    _TUCTerminal.StatusId = HelperStatus.Default.Active;
                    _HCoreContext.TUCTerminal.Add(_TUCTerminal);
                    _HCoreContext.SaveChanges();

                    long? ReceiptStorageId = null;
                    if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                    {
                        ReceiptStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                    }
                    if (ReceiptStorageId != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var UserAccDetails = _HCoreContext.TUCTerminal.Where(x => x.Id == _TUCTerminal.Id).FirstOrDefault();
                            if (UserAccDetails != null)
                            {
                                if (ReceiptStorageId != null)
                                {
                                    UserAccDetails.ReceiptStorageId = ReceiptStorageId;
                                }
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    var _Response = new
                    {
                        ReferenceId = _TUCTerminal.Id,
                        ReferenceKey = _TUCTerminal.Guid,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1052, TUCCoreResource.CA1052M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveTerminal", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Enables the terminal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse EnableTerminal(OAccountOperations.Terminal.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.TUCTerminal
                        .Where(x => x.Guid == _Request.AccountKey
                        && x.Id == _Request.AccountId)
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.StatusId == HelperStatus.Default.Active)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1082, TUCCoreResource.CA1082M);
                        }
                        Details.ModifyById = _Request.UserReference.AccountId;
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1083, TUCCoreResource.CA1083M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("DisableTerminal", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Disables the terminal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DisableTerminal(OAccountOperations.Terminal.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.TUCTerminal
                        .Where(x => x.Guid == _Request.AccountKey
                        && x.Id == _Request.AccountId)
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.StatusId == HelperStatus.Default.Blocked)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1080, TUCCoreResource.CA1080M);
                        }
                        Details.ModifyById = _Request.UserReference.AccountId;
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.StatusId = HelperStatus.Default.Blocked;
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1081, TUCCoreResource.CA1081M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("DisableTerminal", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Enables the store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse EnableStore(OAccountOperations.Store.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey
                        && x.Id == _Request.AccountId
                        && x.AccountTypeId == UserAccountType.MerchantStore)
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        var OwnerDetails = _HCoreContext.HCUAccount
                            .Where(x => x.Guid == _Request.AccountKey
                            && x.Id == _Request.AccountId
                            && x.AccountTypeId == UserAccountType.MerchantStore)
                            .Select(m => new { m.Owner, m.OwnerId, m.Owner.StatusId })
                            .FirstOrDefault();
                        if (OwnerDetails.OwnerId != null && OwnerDetails.Owner.StatusId == HelperStatus.Default.Blocked)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14158, TUCCoreResource.CAA14158M);
                        }
                        if (Details.StatusId == HelperStatus.Default.Active)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1082, TUCCoreResource.CA1082M);
                        }
                        Details.ModifyById = _Request.UserReference.AccountId;
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1083, TUCCoreResource.CA1083M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("DisableStore", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Disables the store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DisableStore(OAccountOperations.Store.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey
                        && x.Id == _Request.AccountId
                        && x.AccountTypeId == UserAccountType.MerchantStore)
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.StatusId == HelperStatus.Default.Blocked)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1532, TUCCoreResource.CA1532M);
                        }
                        Details.ModifyById = _Request.UserReference.AccountId;
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.StatusId = HelperStatus.Default.Blocked;
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1081, TUCCoreResource.CA1081M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("DisableTerminal", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Enables the cashier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse EnableCashier(OAccountOperations.Cashier.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey
                        && x.Id == _Request.AccountId
                        && x.AccountTypeId == UserAccountType.MerchantCashier)
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        var OwnerDetails = _HCoreContext.HCUAccount
                            .Where(x => x.Guid == _Request.AccountKey
                            && x.Id == _Request.AccountId
                            && x.AccountTypeId == UserAccountType.MerchantCashier)
                            .Select(m => new { m.Owner, m.OwnerId, m.Owner.StatusId })
                            .FirstOrDefault();
                        if (OwnerDetails.OwnerId != null && OwnerDetails.Owner.StatusId == HelperStatus.Default.Blocked)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14159, TUCCoreResource.CAA14159M);
                        }
                        if (Details.StatusId == HelperStatus.Default.Active)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1082, TUCCoreResource.CA1082M);
                        }
                        Details.ModifyById = _Request.UserReference.AccountId;
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1083, TUCCoreResource.CA1083M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("DisableTerminal", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Disables the cashier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DisableCashier(OAccountOperations.Cashier.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey
                        && x.Id == _Request.AccountId
                        && x.AccountTypeId == UserAccountType.MerchantCashier)
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.StatusId == HelperStatus.Default.Blocked)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1522, TUCCoreResource.CA1522M);
                        }
                        Details.ModifyById = _Request.UserReference.AccountId;
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.StatusId = HelperStatus.Default.Blocked;
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1081, TUCCoreResource.CA1081M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("DisableTerminal", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Resets the cashier password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ResetCashierPassword(OAccountOperations.Cashier.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey
                        && x.Id == _Request.AccountId
                        && x.AccountTypeId == UserAccountType.MerchantCashier)
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        string NewPassword = HCoreHelper.GenerateRandomNumber(8);
                        var User = _HCoreContext.HCUAccountAuth.Where(x => x.Id == Details.UserId).FirstOrDefault();
                        if (User != null)
                        {
                            if (_Request.IsAutoGenerated)
                            {
                                User.Password = HCoreEncrypt.EncryptHash(NewPassword);
                            }
                            else
                            {
                                User.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                                NewPassword = _Request.Password;
                            }
                            User.ModifyDate = HCoreHelper.GetGMTDateTime();
                            User.ModifyById = _Request.UserReference.AccountId;
                        }
                        //if (Details.StatusId == HelperStatus.Default.Active)
                        //{
                        //    _HCoreContext.Dispose();
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1082, TUCCoreResource.CA1082M);
                        //}
                        Details.ModifyById = _Request.UserReference.AccountId;
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            Password = NewPassword,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1083, TUCCoreResource.CA1083M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("DisableTerminal", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Enables the subaccount.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse EnableSubaccount(OAccountOperations.SubAccount.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey
                        && x.Id == _Request.AccountId
                        && x.AccountTypeId == UserAccountType.MerchantSubAccount)
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.StatusId == HelperStatus.Default.Active)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1082, TUCCoreResource.CA1082M);
                        }
                        Details.ModifyById = _Request.UserReference.AccountId;
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1083, TUCCoreResource.CA1083M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("DisableTerminal", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Disables the subaccount.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DisableSubaccount(OAccountOperations.SubAccount.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey
                        && x.Id == _Request.AccountId
                        && x.AccountTypeId == UserAccountType.MerchantSubAccount)
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.StatusId == HelperStatus.Default.Blocked)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1533, TUCCoreResource.CA1533M);
                        }
                        Details.ModifyById = _Request.UserReference.AccountId;
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.StatusId = HelperStatus.Default.Blocked;
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1081, TUCCoreResource.CA1081M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("DisableTerminal", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Resets the merchant password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ResetMerchantPassword(OAccountOperations.Merchant.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey
                        && x.Id == _Request.AccountId
                        && (x.AccountTypeId == UserAccountType.Merchant || x.AccountTypeId == UserAccountType.MerchantStore))
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        string NewPassword = HCoreHelper.GenerateRandomNumber(8);
                        var User = _HCoreContext.HCUAccountAuth.Where(x => x.Id == Details.UserId).FirstOrDefault();
                        if (User != null)
                        {
                            User.Password = HCoreEncrypt.EncryptHash(NewPassword);
                            User.SecondaryPassword = HCoreEncrypt.EncryptHash(NewPassword);
                            User.SystemPassword = HCoreEncrypt.EncryptHash(NewPassword);
                            User.ModifyDate = HCoreHelper.GetGMTDateTime();
                            User.ModifyById = _Request.UserReference.AccountId;
                        }
                        //if (Details.StatusId == HelperStatus.Default.Active)
                        //{
                        //    _HCoreContext.Dispose();
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1082, TUCCoreResource.CA1082M);
                        //}
                        Details.ModifyById = _Request.UserReference.AccountId;
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            Password = NewPassword,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1083, TUCCoreResource.CA1083M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ResetMerchantPassword", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
