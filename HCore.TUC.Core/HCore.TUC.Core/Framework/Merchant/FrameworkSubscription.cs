//==================================================================================
// FileName: FrameworkSubscription.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to subscription
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Integration.Paystack;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant.HelperStatus;

namespace HCore.TUC.Core.Framework.Merchant
{
    internal class FrameworkSubscription
    {
        HCoreContext _HCoreContext;
        OCoreTransaction.Request _CoreTransactionRequest;
        ManageCoreTransaction _ManageCoreTransaction;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        List<OSubscription.Topup.List> _TopupHistory;
        OSubscription.Balance.Response _BalanceResponse;
        HCUAccountSubscription _HCUAccountSubscription;
        /// <summary>
        /// Description: Topups the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse TopupAccount(OSubscription.Topup.Request _Request)
        {
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                if (_Request.Amount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1084, TUCCoreResource.CA1084M);
                }
                if (_Request.TransactionId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1086, TUCCoreResource.CA1086M);
                }
                if (string.IsNullOrEmpty(_Request.PaymentReference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1085, TUCCoreResource.CA1085M);
                }

                if (_Request.UserReference.AccountTypeId == UserAccountType.Admin || _Request.UserReference.AccountTypeId == UserAccountType.Controller)
                {
                    _CoreTransactionRequest = new OCoreTransaction.Request();
                    _CoreTransactionRequest.UserReference = _Request.UserReference;
                    _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                    _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                    _CoreTransactionRequest.ParentId = _Request.AccountId;
                    _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                    _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                    _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                    _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                    _CoreTransactionRequest.PaymentMethodId = Helpers.PaymentMethod.Bank;
                    _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                    {
                        UserAccountId = _Request.AccountId,
                        ModeId = TransactionMode.Credit,
                        TypeId = TransactionType.MerchantCredit,
                        SourceId = TransactionSource.Merchant,
                        Amount = _Request.Amount,
                        Comission = 0,
                        TotalAmount = _Request.Amount,
                    });
                    _CoreTransactionRequest.Transactions = _TransactionItems;
                    _ManageCoreTransaction = new ManageCoreTransaction();
                    OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                    if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1087, TUCCoreResource.CA1087M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1088, TUCCoreResource.CA1088M);
                        #endregion
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(_Request.PaymentSource) && _Request.PaymentSource == "quickteller")
                    {
                        //OPayStackResponseData _PayStackResponseData = PaystackTransaction.GetTransaction(_Request.PaymentReference, _AppConfig.PaystackPrivateKey);
                        //if (_PayStackResponseData != null && _PayStackResponseData.status == "success")
                        //{
                        _CoreTransactionRequest = new OCoreTransaction.Request();
                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                        _CoreTransactionRequest.ParentId = _Request.AccountId;
                        _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                        _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                        _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                        _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                        _CoreTransactionRequest.PaymentMethodId = Helpers.PaymentMethod.QuickTeller;
                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = _Request.AccountId,
                            ModeId = TransactionMode.Credit,
                            TypeId = TransactionType.MerchantCredit,
                            SourceId = TransactionSource.Merchant,
                            Amount = _Request.Amount,
                            Comission = 0,
                            TotalAmount = _Request.Amount,
                        });
                        _CoreTransactionRequest.Transactions = _TransactionItems;
                        _ManageCoreTransaction = new ManageCoreTransaction();
                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1087, TUCCoreResource.CA1087M);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1088, TUCCoreResource.CA1088M);
                            #endregion
                        }
                        //}
                        //else
                        //{
                        //    #region Send Response
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1089, TUCCoreResource.CA1089M);
                        //    #endregion
                        //}
                    }
                    else
                    {
                        OPayStackResponseData _PayStackResponseData = PaystackTransaction.GetTransaction(_Request.PaymentReference, _AppConfig.PaystackPrivateKey);
                        if (_PayStackResponseData != null && _PayStackResponseData.status == "success")
                        {

                            _CoreTransactionRequest = new OCoreTransaction.Request();
                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                            _CoreTransactionRequest.ParentId = _Request.AccountId;
                            _CoreTransactionRequest.InvoiceAmount = _PayStackResponseData.amount / 100;
                            _CoreTransactionRequest.ReferenceInvoiceAmount = _PayStackResponseData.amount/100;
                            _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                            _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                            _CoreTransactionRequest.PaymentMethodId = Helpers.PaymentMethod.Paystack;
                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = _Request.AccountId,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionType.MerchantCredit,
                                SourceId = TransactionSource.Merchant,
                                Amount = _PayStackResponseData.amount/100,
                                Comission = 0,
                                TotalAmount = _PayStackResponseData.amount/100,
                            });
                            _CoreTransactionRequest.Transactions = _TransactionItems;
                            _ManageCoreTransaction = new ManageCoreTransaction();
                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1087, TUCCoreResource.CA1087M);
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1088, TUCCoreResource.CA1088M);
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1089, TUCCoreResource.CA1089M);
                            #endregion
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("TopupAccount", _Exception);
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the topup history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTopupHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _TopupHistory = new List<OSubscription.Topup.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.HCUAccountTransaction
                                                 where x.AccountId == _Request.AccountId
                                                 && x.ModeId == TransactionMode.Credit
                                                 && x.TypeId == TransactionType.MerchantCredit
                                                 && x.SourceId == TransactionSource.Merchant
                                                 select new OSubscription.Topup.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     Amount = x.Amount,
                                                     TransactionDate = x.CreateDate,
                                                     PaymentReference = x.ReferenceNumber,
                                                     PaymentMethodCode = x.PaymentMethod.SystemName,
                                                     PaymentMethodName = x.PaymentMethod.Name,
                                                     CreatedByReferenceId = x.CreatedById,
                                                     CreatedByReferenceKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.Name,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                 })
                                         .Where(_Request.SearchCondition)
                                 .Count();
                    }
                    #endregion
                    #region Get Data
                    _TopupHistory = (from x in _HCoreContext.HCUAccountTransaction
                                     where x.AccountId == _Request.AccountId
                                     && x.ModeId == TransactionMode.Credit
                                     && x.TypeId == TransactionType.MerchantCredit
                                     && x.SourceId == TransactionSource.Merchant
                                     select new OSubscription.Topup.List
                                     {
                                         ReferenceId = x.Id,
                                         ReferenceKey = x.Guid,
                                         Amount = x.Amount,
                                         TransactionDate = x.CreateDate,
                                         PaymentReference = x.ReferenceNumber,
                                         PaymentMethodCode = x.PaymentMethod.SystemName,
                                         PaymentMethodName = x.PaymentMethod.Name,
                                         CreatedByReferenceId = x.CreatedById,
                                         CreatedByReferenceKey = x.CreatedBy.Guid,
                                         CreatedByDisplayName = x.CreatedBy.Name,
                                         StatusCode = x.Status.SystemName,
                                         StatusName = x.Status.Name,
                                     })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _TopupHistory)
                    {

                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _TopupHistory, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetTopupHistory", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the account balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountBalance(OSubscription.Balance.Request _Request)
        {

            _BalanceResponse = new OSubscription.Balance.Response();
            #region Manage Exception
            try
            {
                DateTime? LastTransactionDate = HCoreHelper.GetGMTDate();
                using (_HCoreContext = new HCoreContext())
                {
                    _BalanceResponse.Credit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                             .Where(x => x.AccountId == _Request.AccountId &&
                                                    x.SourceId == TransactionSource.Merchant &&
                                                    x.ModeId == TransactionMode.Credit &&
                                                    //x.TypeId != TransactionType.MerchantCredit &&
                                                    (x.StatusId == Transaction.Success || x.StatusId == Transaction.Pending))
                                             .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                    _BalanceResponse.Debit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                     .Where(x => x.AccountId == _Request.AccountId &&
                                                            x.SourceId == TransactionSource.Merchant &&
                                                             x.ModeId == TransactionMode.Debit &&
                                                             (x.StatusId == Transaction.Success || x.StatusId == Transaction.Pending))
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                    var LastTransaction = _HCoreContext.HCUAccountTransaction
                                             .Where(x => x.AccountId == _Request.AccountId &&
                                                    x.SourceId == TransactionSource.Merchant &&
                                                    x.ModeId == TransactionMode.Credit &&
                                                   (x.StatusId == Transaction.Success || x.StatusId == Transaction.Pending))
                                             .OrderByDescending(x => x.TransactionDate)
                                             .Select(x => new
                                             {
                                                 x.TotalAmount,
                                                 x.TransactionDate
                                             }).FirstOrDefault();
                    if (LastTransaction != null)
                    {
                        _BalanceResponse.LastTransactionAmount = HCoreHelper.RoundNumber(LastTransaction.TotalAmount, _AppConfig.SystemEntryRoundDouble);
                        _BalanceResponse.LastTransactionDate = LastTransaction.TransactionDate;
                    }
                    if (_BalanceResponse.LastTransactionDate != null)
                    {
                        double TCredit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                        .Where(x => x.AccountId == _Request.AccountId &&
                                        x.TransactionDate < _BalanceResponse.LastTransactionDate &&
                                               x.SourceId == TransactionSource.Merchant &&
                                               //x.TypeId != TransactionType.MerchantCredit &&
                                               x.ModeId == TransactionMode.Credit &&
                                                (x.StatusId == Transaction.Success || x.StatusId == Transaction.Pending))
                                        .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                        double TDebit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                         .Where(x => x.AccountId == _Request.AccountId &&
                                        x.TransactionDate < _BalanceResponse.LastTransactionDate &&
                                                                x.SourceId == TransactionSource.Merchant &&
                                                                 x.ModeId == TransactionMode.Debit &&
                                                                 (x.StatusId == Transaction.Success || x.StatusId == Transaction.Pending))
                                                          .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                        if ((_BalanceResponse.Credit - _BalanceResponse.Debit) != 0)
                        {
                            _BalanceResponse.LastTopupBalance = HCoreHelper.RoundNumber(((TCredit - TDebit) + _BalanceResponse.LastTransactionAmount), _AppConfig.SystemEntryRoundDouble);
                        }
                        else {
                            _BalanceResponse.LastTopupBalance = HCoreHelper.RoundNumber(((TCredit - TDebit)), _AppConfig.SystemEntryRoundDouble);
                        }
                    }
                    _BalanceResponse.Balance = HCoreHelper.RoundNumber((_BalanceResponse.Credit - _BalanceResponse.Debit), _AppConfig.SystemEntryRoundDouble);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, TUCCoreResource.CA1090, TUCCoreResource.CA1090M);
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAccountBalance", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the top up overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTopUpOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    OSubscription.Overview _Overview = new OSubscription.Overview();

                    _Overview.Amount = (from x in _HCoreContext.HCUAccountTransaction
                                        where x.AccountId == _Request.AccountId
                                        && x.ModeId == TransactionMode.Credit
                                        && (x.TypeId == TransactionType.MerchantCredit || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
                                        && x.SourceId == TransactionSource.Merchant
                                        select new OSubscription.Topup.List
                                        {
                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,
                                            Amount = x.Amount,
                                            TransactionDate = x.CreateDate,
                                            PaymentReference = x.ReferenceNumber,
                                            PaymentMethodCode = x.PaymentMethod.SystemName,
                                            PaymentMethodName = x.PaymentMethod.Name,
                                            CreatedByReferenceId = x.CreatedById,
                                            CreatedByReferenceKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.Name,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name,
                                            TransactionTypeId = x.TypeId,
                                            TransactionType = "Top-Up",
                                            AccountDisplayName = x.Account.DisplayName,
                                            WalletType = "Reward Wallet",
                                            SentFrom = "Card",
                                            Description = "Top-Up of Reward Wallet"
                                        })
                                      .Where(_Request.SearchCondition)
                                      .Sum(x => x.Amount);


                    _Overview.TotalAmount = (from x in _HCoreContext.HCUAccountTransaction
                                             where x.AccountId == _Request.AccountId
                                             && x.ModeId == TransactionMode.Credit
                                             && (x.TypeId == TransactionType.MerchantCredit || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
                                             && x.SourceId == TransactionSource.Merchant
                                             select new OSubscription.Topup.List
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,
                                                 Amount = x.TotalAmount,
                                                 TransactionDate = x.CreateDate,
                                                 PaymentReference = x.ReferenceNumber,
                                                 PaymentMethodCode = x.PaymentMethod.SystemName,
                                                 PaymentMethodName = x.PaymentMethod.Name,
                                                 CreatedByReferenceId = x.CreatedById,
                                                 CreatedByReferenceKey = x.CreatedBy.Guid,
                                                 CreatedByDisplayName = x.CreatedBy.Name,
                                                 StatusCode = x.Status.SystemName,
                                                 StatusName = x.Status.Name,
                                                 TransactionTypeId = x.TypeId,
                                                 TransactionType = "Top-Up",
                                                 AccountDisplayName = x.Account.DisplayName,
                                                 WalletType = "Reward Wallet",
                                                 SentFrom = "Card",
                                                 Description = "Top-Up of Reward Wallet"
                                             })
                                      .Where(_Request.SearchCondition)
                                      .Sum(x => x.Amount);
                    _Overview.RewardPercentage = _HCoreContext.HCUAccountParameter
                                                .Where(x => x.AccountId == _Request.AccountId
                                                        && x.Account.Guid == _Request.AccountKey
                                                        && x.Common.SystemName == "rewardpercentage")
                                                    .OrderByDescending(x => x.CreateDate)
                                                .Select(x => x.Value).FirstOrDefault();

                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Overview, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetTopUpOverview", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

    }
}
