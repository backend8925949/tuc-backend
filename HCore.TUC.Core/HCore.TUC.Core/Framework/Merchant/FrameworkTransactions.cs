//==================================================================================
// FileName: FrameworkTransactions.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to transactions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Store;
using HCore.Helper;
using HCore.TUC.Core.Object.Merchant;
using System;
using System.Collections.Generic;
using System.Linq;
using HCore.ThankUCash.Actors;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using System.Linq.Dynamic.Core;
using ClosedXML.Excel;
using System.Reflection;
using System.IO;
using HCore.TUC.Core.Resource;
using HCore.TUC.Core.Object.Operations;

namespace HCore.TUC.Core.Framework.Merchant
{
    internal class FrameworkTransactions
    {
        List<OTransaction.Sale> _Sales;
        List<OTransaction.SaleDownload> _SaleDownload;
        List<OTransaction.RewardDownload> _RewardDownload;
        List<OTransaction.RewardClaimDownload> _RewardClaimDownload;
        List<OTransaction.RedeemDownload> _RedeemDownload;
        List<OTransaction.All> _AllTransactions;
        OTransactionOverview _OTransactionOverview;
        HCoreContext _HCoreContext;
        HCUAccountParameter _HCUAccountParameter;
        List<OCashOut.RewardList> _RewardLists;
        /// <summary>
        /// Description: Formats the transactions.
        /// </summary>
        /// <param name="_Sales">The sales.</param>
        /// <returns>List&lt;OTransaction.Sale&gt;.</returns>
        private List<OTransaction.Sale> FormatTransactions(List<OTransaction.Sale> _Sales)
        {
            #region Create  Response Object
            foreach (var DataItem in _Sales)
            {
                DataItem.TypeName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.TypeId).Select(q => q.Name).FirstOrDefault();
                DataItem.CardBankName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBankId).Select(q => q.Name).FirstOrDefault();
                DataItem.CardBrandName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBrandId).Select(q => q.Name).FirstOrDefault();
                DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.StatusId).Select(q => q.Name).FirstOrDefault();
                //var ParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ParentId);
                //if (ParentDetails != null)
                //{
                //    DataItem.ParentKey = ParentDetails.ReferenceKey;
                //    DataItem.ParentDisplayName = ParentDetails.DisplayName;
                //    DataItem.ParentIconUrl = ParentDetails.IconUrl;
                //}
                var SubParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.StoreReferenceId);
                if (SubParentDetails != null)
                {
                    DataItem.StoreReferenceKey = SubParentDetails.ReferenceKey;
                    DataItem.StoreDisplayName = SubParentDetails.DisplayName;
                }
                var AcquirerDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.AcquirerId);
                if (AcquirerDetails != null)
                {
                    DataItem.AcquirerKey = AcquirerDetails.ReferenceKey;
                    DataItem.AcquirerDisplayName = AcquirerDetails.DisplayName;
                    DataItem.AcquirerIconUrl = AcquirerDetails.IconUrl;
                }
                var ProviderDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ProviderId);
                if (ProviderDetails != null)
                {
                    DataItem.ProviderKey = ProviderDetails.ReferenceKey;
                    DataItem.ProviderDisplayName = ProviderDetails.DisplayName;
                    DataItem.ProviderIconUrl = AcquirerDetails.IconUrl;
                }
                var CashierDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CashierId);
                if (CashierDetails != null)
                {
                    DataItem.CashierKey = CashierDetails.ReferenceKey;
                    DataItem.CashierCode = CashierDetails.DisplayName;
                    DataItem.CashierDisplayName = CashierDetails.Name;
                    DataItem.CashierMobileNumber = CashierDetails.MobileNumber;
                }

                var CreatedByDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CreatedById);
                if (CreatedByDetails != null)
                {
                    DataItem.CreatedByKey = CreatedByDetails.ReferenceKey;
                    DataItem.CreatedByDisplayName = CreatedByDetails.DisplayName;
                    DataItem.CreatedByAccountTypeCode = CreatedByDetails.AccountTypeCode;
                }
                if (!string.IsNullOrEmpty(DataItem.TypeName))
                {
                    DataItem.TypeName = DataItem.TypeName.Replace("Reward", "").Replace("Rewards", "");
                }
            }
            return _Sales;
            #endregion
        }
        /// <summary>
        /// Description: Formats the transactions.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <param name="_Sales">The sales.</param>
        /// <returns>OList.Response.</returns>
        private OList.Response FormatTransactions(OList.Request _Request, List<OTransaction.Sale> _Sales)
        {
            #region Create  Response Object
            foreach (var DataItem in _Sales)
            {
                if (DataItem.TotalAmount > 0)
                {
                    DataItem.LoyaltyTypeId = 1;
                }
                DataItem.TypeName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.TypeId).Select(q => q.Name).FirstOrDefault();
                DataItem.CardBankName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBankId).Select(q => q.Name).FirstOrDefault();
                DataItem.CardBrandName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBrandId).Select(q => q.Name).FirstOrDefault();
                DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.StatusId).Select(q => q.Name).FirstOrDefault();
                //var ParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ParentId);
                //if (ParentDetails != null)
                //{
                //    DataItem.ParentKey = ParentDetails.ReferenceKey;
                //    DataItem.ParentDisplayName = ParentDetails.DisplayName;
                //    DataItem.ParentIconUrl = ParentDetails.IconUrl;
                //}
                var SubParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.StoreReferenceId);
                if (SubParentDetails != null)
                {
                    DataItem.StoreReferenceKey = SubParentDetails.ReferenceKey;
                    DataItem.StoreDisplayName = SubParentDetails.DisplayName;
                }
                var AcquirerDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.AcquirerId);
                if (AcquirerDetails != null)
                {
                    DataItem.AcquirerKey = AcquirerDetails.ReferenceKey;
                    DataItem.AcquirerDisplayName = AcquirerDetails.DisplayName;
                    DataItem.AcquirerIconUrl = AcquirerDetails.IconUrl;
                }
                var ProviderDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ProviderId);
                if (ProviderDetails != null)
                {
                    DataItem.ProviderKey = ProviderDetails.ReferenceKey;
                    DataItem.ProviderDisplayName = ProviderDetails.DisplayName;
                    DataItem.ProviderIconUrl = AcquirerDetails.IconUrl;
                }
                var CashierDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CashierId);
                if (CashierDetails != null)
                {
                    DataItem.CashierKey = CashierDetails.ReferenceKey;
                    DataItem.CashierCode = CashierDetails.DisplayName;
                    DataItem.CashierDisplayName = CashierDetails.Name;
                    DataItem.CashierMobileNumber = CashierDetails.MobileNumber;
                }
                var CreatedByDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CreatedById);
                if (CreatedByDetails != null)
                {
                    DataItem.CreatedByKey = CreatedByDetails.ReferenceKey;
                    DataItem.CreatedByDisplayName = CreatedByDetails.DisplayName;
                    DataItem.CreatedByAccountTypeCode = CreatedByDetails.AccountTypeCode;
                }

                if (!string.IsNullOrEmpty(DataItem.TerminalId))
                {
                    var TerminalDetails = HCoreDataStore.Terminals.FirstOrDefault(q => q.DisplayName == DataItem.TerminalId);
                    if (TerminalDetails != null)
                    {
                        DataItem.TerminalId = TerminalDetails.DisplayName;
                        DataItem.TerminalReferenceId = TerminalDetails.ReferenceId;
                        DataItem.TerminalReferenceKey = TerminalDetails.ReferenceKey;
                    }
                }


                if (!string.IsNullOrEmpty(DataItem.TypeName))
                {
                    DataItem.TypeName = DataItem.TypeName.Replace("Reward", "").Replace("Rewards", "");
                }
            }
            return HCoreHelper.GetListResponse(_Request.TotalRecords, _Sales, _Request.Offset, _Request.Limit);
            #endregion
        }
        /// <summary>
        /// Description: Gets the sale transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSaleTransaction(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorSaleTransactionDownloader");
                    var _ActorNotify = _Actor.ActorOf<ActorSaleTransactionDownloader>("ActorSaleTransactionDownloader");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && (x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit)
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward))
                                                //&& (x.StatusId == HelperStatus.Default.Active || x.StatusId == HelperStatus.Default.Suspended || x.StatusId == HelperStatus.Default.Blocked || x.StatusId == HelperStatus.Default.Inactive))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    TotalAmount = x.TotalAmount,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    AccountId = x.Account.OwnerId,
                                                    AccountKey = x.Account.Owner.Guid,
                                                    AccountDisplayName = x.Account.Owner.DisplayName,
                                                    AccountMobileNumber = x.Account.Owner.MobileNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    CashierKey = x.Cashier.Guid,
                                                    CashierCode = x.Cashier.AccountCode,
                                                    CashierDisplayName = x.Cashier.DisplayName,
                                                    CashierMobileNumber = x.Cashier.MobileNumber,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    TerminalReferenceId = x.TerminalId,
                                                    CashierReward = x.CashierReward,
                                                    ParentId = x.ParentId
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    _Sales = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && (x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit)
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward))
                                                //&& (x.StatusId == HelperStatus.Default.Active || x.StatusId == HelperStatus.Default.Suspended || x.StatusId == HelperStatus.Default.Blocked || x.StatusId == HelperStatus.Default.Inactive))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    TotalAmount = x.TotalAmount,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    AccountId = x.Account.OwnerId,
                                                    AccountKey = x.Account.Owner.Guid,
                                                    AccountDisplayName = x.Account.Owner.DisplayName,
                                                    AccountMobileNumber = x.Account.Owner.MobileNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    CashierKey = x.Cashier.Guid,
                                                    CashierCode = x.Cashier.AccountCode,
                                                    CashierDisplayName = x.Cashier.DisplayName,
                                                    CashierMobileNumber = x.Cashier.MobileNumber,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    TerminalReferenceId = x.TerminalId,
                                                    CashierReward = x.CashierReward,
                                                    ParentId = x.ParentId
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                    #endregion
                    foreach (var _Sale in _Sales)
                    {
                        if (_Sale.StatusId == HelperStatus.Transaction.Pending)
                        {
                            _Sale.StatusCode = "transaction.pending";
                            _Sale.StatusId = HelperStatus.Transaction.Success;
                            //Added status name here because if it is pending it shows as success, so changed to pending.
                            _Sale.StatusName = "Pending";
                        }
                    }
                    #region Send Response
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetSaleTransactions", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the sale transaction download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetSaleTransactionDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _SaleDownload = new List<OTransaction.SaleDownload>();
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                long StorageId = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                    _HCUAccountParameter.Name = "Sales_Sheet";
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Inactive;
                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    _HCoreContext.SaveChanges();
                    StorageId = _HCUAccountParameter.Id;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    TotalAmount = x.TotalAmount,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    AccountId = x.Account.OwnerId,
                                                    AccountKey = x.Account.Owner.Guid,
                                                    AccountDisplayName = x.Account.Owner.DisplayName,
                                                    AccountMobileNumber = x.Account.Owner.MobileNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    CashierKey = x.Cashier.Guid,
                                                    CashierCode = x.Cashier.AccountCode,
                                                    CashierDisplayName = x.Cashier.DisplayName,
                                                    CashierMobileNumber = x.Cashier.MobileNumber,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    TerminalReferenceId = x.TerminalId,
                                                    CashierReward = x.CashierReward
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    double Iterations = Math.Round((double)_Request.TotalRecords / 1000, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        _Sales.AddRange(_HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    TotalAmount = x.TotalAmount,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    AccountId = x.Account.OwnerId,
                                                    AccountKey = x.Account.Owner.Guid,
                                                    AccountDisplayName = x.Account.Owner.DisplayName,
                                                    AccountMobileNumber = x.Account.Owner.MobileNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    CashierKey = x.Cashier.Guid,
                                                    CashierCode = x.Cashier.AccountCode,
                                                    CashierDisplayName = x.Cashier.DisplayName,
                                                    CashierMobileNumber = x.Cashier.MobileNumber,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalReferenceId = x.TerminalId,
                                                    CashierReward = x.CashierReward
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(1000)
                                                     .ToList());
                        _Request.Offset += 1000;
                    }
                    foreach (var _Sale in _Sales)
                    {
                        if (_Sale.StatusId == HelperStatus.Transaction.Pending)
                        {
                            _Sale.StatusCode = "transaction.pending";
                            _Sale.StatusId = HelperStatus.Transaction.Success;
                        }
                    }
                    #endregion
                    _Sales = FormatTransactions(_Sales);
                    foreach (var SalesItem in _Sales)
                    {
                        _SaleDownload.Add(new OTransaction.SaleDownload
                        {
                            ReferenceId = SalesItem.ReferenceId,
                            TransactionDate = HCoreHelper.GetGMTToNigeria(SalesItem.TransactionDate),
                            User = SalesItem.UserDisplayName,
                            MobileNumber = SalesItem.UserMobileNumber,
                            TypeName = SalesItem.TypeName,
                            InvoiceAmount = SalesItem.InvoiceAmount,
                            ReferenceNumber = SalesItem.ReferenceNumber,
                            CardNumber = SalesItem.AccountNumber,
                            CardBrand = SalesItem.CardBrandName,
                            CardBank = SalesItem.CardBankName,

                            Store = SalesItem.StoreDisplayName,
                            CashierId = SalesItem.CashierCode,
                            CashierDisplayName = SalesItem.CashierDisplayName,
                            Bank = SalesItem.AcquirerDisplayName,
                            Provider = SalesItem.ProviderDisplayName,
                            Issuer = SalesItem.CreatedByDisplayName,
                            TerminalId = SalesItem.TerminalId,
                            Status = SalesItem.StatusName,
                        });
                    }
                    using (var _XLWorkbook = new XLWorkbook())
                    {
                        var _WorkSheet = _XLWorkbook.Worksheets.Add("Sales_Sheet");
                        PropertyInfo[] properties = _SaleDownload.First().GetType().GetProperties();
                        List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                        for (int i = 0; i < headerNames.Count; i++)
                        {
                            _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                        }
                        _WorkSheet.Cell(2, 1).InsertData(_SaleDownload);
                        MemoryStream _MemoryStream = new MemoryStream();
                        _XLWorkbook.SaveAs(_MemoryStream);
                        long? _StorageId = HCoreHelper.SaveStorage("Sales_Sheet", "xlsx", _MemoryStream, _Request.UserReference);
                        if (_StorageId != null && _StorageId != 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                                if (TItem != null)
                                {
                                    TItem.IconStorageId = _StorageId;
                                    TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    TItem.ModifyById = _Request.UserReference.AccountId;
                                    TItem.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }
                    #region Send Response
                    _HCoreContext.Dispose();
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardClaimTransactionDownload", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the sale transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSaleTransactionOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    _OTransactionOverview = new OTransactionOverview();
                    //_OTransactionOverview.Customers = _HCoreContext.HCUAccountTransaction
                    //                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                    //                            && x.ParentId == _Request.AccountId
                    //                            && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus  || x.SourceId == TransactionSource.TUCBlack)
                    //                            && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward))
                    //                            .Select(x => new OTransaction.Sale
                    //                            {
                    //                                ReferenceId = x.Id,
                    //                                ReferenceKey = x.Guid,
                    //                                InvoiceAmount = x.PurchaseAmount,
                    //                                TransactionDate = x.TransactionDate,
                    //                                TypeId = x.TypeId,
                    //                                CardBankId = x.CardBankId,
                    //                                CardBrandId = x.CardBrandId,
                    //                                AccountNumber = x.AccountNumber,
                    //                                ReferenceNumber = x.ReferenceNumber,
                    //                                UserAccountId = x.AccountId,
                    //                                UserAccountKey = x.Account.Guid,
                    //                                UserDisplayName = x.Account.DisplayName,
                    //                                UserMobileNumber = x.Account.MobileNumber,
                    //                                StoreReferenceId = x.SubParentId,
                    //                                ProviderId = x.ProviderId,
                    //                                AcquirerId = x.BankId,
                    //                                CreatedById = x.CreatedById,
                    //                                CashierId = x.CashierId,
                    //                                StatusCode = x.Status.SystemName,
                    //                                StatusId = x.StatusId,
                    //                            })
                    //                            .Where(_Request.SearchCondition)
                    //                            .Select(x => x.AccountId)
                    //                            .Distinct()
                    //                            .Count();

                    _OTransactionOverview.Transactions = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && (x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit)
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    TotalAmount = x.TotalAmount,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    AccountId = x.Account.OwnerId,
                                                    AccountKey = x.Account.Owner.Guid,
                                                    AccountDisplayName = x.Account.Owner.DisplayName,
                                                    AccountMobileNumber = x.Account.Owner.MobileNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    CashierKey = x.Cashier.Guid,
                                                    CashierCode = x.Cashier.AccountCode,
                                                    CashierDisplayName = x.Cashier.DisplayName,
                                                    CashierMobileNumber = x.Cashier.MobileNumber,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalReferenceId = x.TerminalId,
                                                    ParentId = x.ParentId
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();

                    _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && (x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit)
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    TotalAmount = x.TotalAmount,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    AccountId = x.Account.OwnerId,
                                                    AccountKey = x.Account.Owner.Guid,
                                                    AccountDisplayName = x.Account.Owner.DisplayName,
                                                    AccountMobileNumber = x.Account.Owner.MobileNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    CashierKey = x.Cashier.Guid,
                                                    CashierCode = x.Cashier.AccountCode,
                                                    CashierDisplayName = x.Cashier.DisplayName,
                                                    CashierMobileNumber = x.Cashier.MobileNumber,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalReferenceId = x.TerminalId,
                                                    ParentId = x.ParentId
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);

                    _OTransactionOverview.CashierRewardTotal = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && (x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit)
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    TotalAmount = x.TotalAmount,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,
                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    AccountId = x.Account.OwnerId,
                                                    AccountKey = x.Account.Owner.Guid,
                                                    AccountDisplayName = x.Account.Owner.DisplayName,
                                                    AccountMobileNumber = x.Account.Owner.MobileNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    CashierKey = x.Cashier.Guid,
                                                    CashierCode = x.Cashier.AccountCode,
                                                    CashierDisplayName = x.Cashier.DisplayName,
                                                    CashierMobileNumber = x.Cashier.MobileNumber,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalReferenceId = x.TerminalId,
                                                    CashierReward = x.CashierReward,
                                                    ParentId = x.ParentId
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.CashierReward);

                    //_OTransactionOverview.CardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                    //                            && x.ParentId == _Request.AccountId
                    //                            && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus  || x.SourceId == TransactionSource.TUCBlack)
                    //                            && x.TypeId == TransactionType.CardReward)
                    //                            .Select(x => new OTransaction.Sale
                    //                            {
                    //                                ReferenceId = x.Id,
                    //                                ReferenceKey = x.Guid,
                    //                                InvoiceAmount = x.PurchaseAmount,
                    //                                TransactionDate = x.TransactionDate,
                    //                                TypeId = x.TypeId,
                    //                                CardBankId = x.CardBankId,
                    //                                CardBrandId = x.CardBrandId,
                    //                                AccountNumber = x.AccountNumber,
                    //                                ReferenceNumber = x.ReferenceNumber,
                    //                                UserAccountId = x.AccountId,
                    //                                UserAccountKey = x.Account.Guid,
                    //                                UserDisplayName = x.Account.DisplayName,
                    //                                UserMobileNumber = x.Account.MobileNumber,
                    //                                StoreReferenceId = x.SubParentId,
                    //                                ProviderId = x.ProviderId,
                    //                                AcquirerId = x.BankId,
                    //                                CreatedById = x.CreatedById,
                    //                                CashierId = x.CashierId,
                    //                                StatusCode = x.Status.SystemName,
                    //                                StatusId = x.StatusId,
                    //                            })
                    //                            .Where(_Request.SearchCondition)
                    //                            .Sum(x => x.InvoiceAmount);
                    //_OTransactionOverview.CashInvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                    //                            && x.ParentId == _Request.AccountId
                    //                            && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus  || x.SourceId == TransactionSource.TUCBlack)
                    //                            && x.TypeId == TransactionType.CashReward)
                    //                            .Select(x => new OTransaction.Sale
                    //                            {
                    //                                ReferenceId = x.Id,
                    //                                ReferenceKey = x.Guid,
                    //                                InvoiceAmount = x.PurchaseAmount,
                    //                                TransactionDate = x.TransactionDate,
                    //                                TypeId = x.TypeId,
                    //                                CardBankId = x.CardBankId,
                    //                                CardBrandId = x.CardBrandId,
                    //                                AccountNumber = x.AccountNumber,
                    //                                ReferenceNumber = x.ReferenceNumber,
                    //                                UserAccountId = x.AccountId,
                    //                                UserAccountKey = x.Account.Guid,
                    //                                UserDisplayName = x.Account.DisplayName,
                    //                                UserMobileNumber = x.Account.MobileNumber,
                    //                                StoreReferenceId = x.SubParentId,
                    //                                ProviderId = x.ProviderId,
                    //                                AcquirerId = x.BankId,
                    //                                CreatedById = x.CreatedById,
                    //                                CashierId = x.CashierId,
                    //                                StatusCode = x.Status.SystemName,
                    //                                StatusId = x.StatusId,
                    //                            })
                    //                            .Where(_Request.SearchCondition)
                    //                            .Sum(x => x.InvoiceAmount);
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetSaleTransactionOverviewOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }



        /// <summary>
        /// Description: Gets the reward transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRewardTransaction(OList.Request _Request)
        {
            if (_Request.IsDownload)
            {
                var _Actor = ActorSystem.Create("ActorRewardTransactionDownloader");
                var _ActorNotify = _Actor.ActorOf<ActorRewardTransactionDownloader>("ActorRewardTransactionDownloader");
                _ActorNotify.Tell(_Request);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
            }
            else
            {
                #region Manage Exception
                try
                {

                    _SaleDownload = new List<OTransaction.SaleDownload>();
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    if (string.IsNullOrEmpty(_Request.SortExpression))
                    {
                        HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                    }
                    if (_Request.Limit < 1)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                    && x.ParentId == _Request.AccountId
                                                    && x.ModeId == TransactionMode.Credit
                                                    && x.ParentTransaction.TotalAmount > 0
                                                    && x.CampaignId == null
                                                    && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                    || x.SourceId == TransactionSource.ThankUCashPlus))
                                                    .Select(x => new OTransaction.Sale
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        InvoiceAmount = x.PurchaseAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        TypeId = x.TypeId,
                                                        RewardAmount = x.ReferenceAmount,
                                                        CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                        UserAmount = x.TotalAmount,
                                                        CardBankId = x.CardBankId,
                                                        CardBrandId = x.CardBrandId,
                                                        AccountNumber = x.AccountNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        UserAccountId = x.AccountId,
                                                        UserAccountKey = x.Account.Guid,
                                                        UserDisplayName = x.Account.DisplayName,
                                                        UserMobileNumber = x.Account.MobileNumber,
                                                        ParentId = x.ParentId,
                                                        ParentDisplayName = x.Parent.DisplayName,
                                                        StoreReferenceId = x.SubParentId,
                                                        StoreDisplayName = x.SubParent.DisplayName,
                                                        ProviderId = x.ProviderId,
                                                        AcquirerId = x.BankId,
                                                        CreatedById = x.CreatedById,
                                                        CashierId = x.CashierId,
                                                        TerminalId = x.Terminal.IdentificationNumber,
                                                        TerminalReferenceId = x.TerminalId,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                    .Count(_Request.SearchCondition);
                            #endregion
                        }
                        #region Get Data
                        _Sales = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                    && x.ParentId == _Request.AccountId
                                                    && x.ModeId == TransactionMode.Credit
                                                    && x.CampaignId == null
                                                    && x.ParentTransaction.TotalAmount > 0
                                                    && ((x.TypeId != TransactionType.ThankUCashPlusCredit && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack)) || x.SourceId == TransactionSource.ThankUCashPlus))
                                                    .Select(x => new OTransaction.Sale
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        InvoiceAmount = x.PurchaseAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        TypeId = x.TypeId,
                                                        RewardAmount = x.ReferenceAmount,
                                                        UserAmount = x.TotalAmount,
                                                        CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                        CardBankId = x.CardBankId,
                                                        CardBrandId = x.CardBrandId,
                                                        AccountNumber = x.AccountNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        UserAccountId = x.AccountId,
                                                        UserAccountKey = x.Account.Guid,
                                                        UserDisplayName = x.Account.DisplayName,
                                                        UserMobileNumber = x.Account.MobileNumber,
                                                        ParentId = x.ParentId,
                                                        ParentDisplayName = x.Parent.DisplayName,
                                                        StoreReferenceId = x.SubParentId,
                                                        StoreDisplayName = x.SubParent.DisplayName,
                                                        ProviderId = x.ProviderId,
                                                        AcquirerId = x.BankId,
                                                        CreatedById = x.CreatedById,
                                                        CashierId = x.CashierId,
                                                        TerminalId = x.Terminal.IdentificationNumber,
                                                        TerminalReferenceId = x.TerminalId,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                         .Where(_Request.SearchCondition)
                                                         .OrderBy(_Request.SortExpression)
                                                         .Skip(_Request.Offset)
                                                         .Take(_Request.Limit)
                                                         .ToList();
                        #endregion
                        #region Send Response
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                        #endregion
                    }
                }
                catch (Exception _Exception)
                {
                    HCoreHelper.LogException("GetRewardTransaction", _Exception, _Request.UserReference);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                }
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the reward transaction download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetRewardTransactionDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _RewardDownload = new List<OTransaction.RewardDownload>();
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                long StorageId = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                    _HCUAccountParameter.Name = "Rewards_Sheet";
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Inactive;
                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    _HCoreContext.SaveChanges();
                    StorageId = _HCUAccountParameter.Id;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Credit
                                                && x.ParentTransaction.TotalAmount > 0
                                                && x.CampaignId == null
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    StoreReferenceId = x.SubParentId,
                                                    StoreDisplayName = x.SubParent.DisplayName,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    TerminalReferenceId = x.TerminalId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    StatusName = x.Status.Name,
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    double Iterations = Math.Round((double)_Request.TotalRecords / 1000, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        _Sales.AddRange(_HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId == null
                                                 && x.ParentTransaction.TotalAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    StoreReferenceId = x.SubParentId,
                                                    StoreDisplayName = x.SubParent.DisplayName,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    TerminalReferenceId = x.TerminalId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    StatusName = x.Status.Name,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(1000)
                                                     .ToList());
                        _Request.Offset += 1000;
                    }
                    #endregion
                    _Sales = FormatTransactions(_Sales);
                    foreach (var SalesItem in _Sales)
                    {
                        _RewardDownload.Add(new OTransaction.RewardDownload
                        {
                            ReferenceId = SalesItem.ReferenceId,
                            TransactionDate = HCoreHelper.GetGMTToNigeria(SalesItem.TransactionDate),
                            User = SalesItem.UserDisplayName,
                            MobileNumber = SalesItem.UserMobileNumber,
                            TypeName = SalesItem.TypeName,
                            InvoiceAmount = SalesItem.InvoiceAmount,
                            RewardAmount = SalesItem.RewardAmount,
                            ConvinenceCharge = SalesItem.CommissionAmount,
                            ReferenceNumber = SalesItem.ReferenceNumber,
                            CardNumber = SalesItem.AccountNumber,
                            CardBrand = SalesItem.CardBrandName ?? "",
                            CardBank = SalesItem.CardBankName,

                            Store = SalesItem.StoreDisplayName,
                            CashierId = SalesItem.CashierCode,
                            CashierDisplayName = SalesItem.CashierDisplayName,
                            Bank = SalesItem.AcquirerDisplayName,
                            Provider = SalesItem.ProviderDisplayName,
                            Issuer = SalesItem.CreatedByDisplayName,
                            TerminalId = SalesItem.TerminalId,
                            Status = SalesItem.StatusName,
                        });
                    }
                    using (var _XLWorkbook = new XLWorkbook())
                    {
                        var _WorkSheet = _XLWorkbook.Worksheets.Add("Rewards_Sheet");
                        PropertyInfo[] properties = _RewardDownload.First().GetType().GetProperties();
                        List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                        for (int i = 0; i < headerNames.Count; i++)
                        {
                            _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                        }
                        _WorkSheet.Cell(2, 1).InsertData(_RewardDownload);
                        MemoryStream _MemoryStream = new MemoryStream();
                        _XLWorkbook.SaveAs(_MemoryStream);
                        long? _StorageId = HCoreHelper.SaveStorage("Rewards_Sheet", "xlsx", _MemoryStream, _Request.UserReference);
                        if (_StorageId != null && _StorageId != 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                                if (TItem != null)
                                {
                                    TItem.IconStorageId = _StorageId;
                                    TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    TItem.ModifyById = _Request.UserReference.AccountId;
                                    TItem.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }
                    #region Send Response
                    _HCoreContext.Dispose();
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardTransactionDownload", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the reward transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRewardTransactionOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    _OTransactionOverview = new OTransactionOverview();
                    //_OTransactionOverview.Customers = _HCoreContext.HCUAccountTransaction
                    //                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                    //                            && x.ParentId == _Request.AccountId
                    //                            && x.ModeId == TransactionMode.Credit
                    //                            && x.CampaignId == null
                    //                             && x.ParentTransaction.TotalAmount > 0
                    //                            && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                    //                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                    //                            || x.SourceId == TransactionSource.ThankUCashPlus))
                    //                            .Select(x => new OTransaction.Sale
                    //                            {
                    //                                ReferenceId = x.Id,
                    //                                ReferenceKey = x.Guid,
                    //                                InvoiceAmount = x.PurchaseAmount,
                    //                                TransactionDate = x.TransactionDate,
                    //                                TypeId = x.TypeId,
                    //                                RewardAmount = x.ReferenceAmount,
                    //                                CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                    //                                UserAmount = x.TotalAmount,
                    //                                CardBankId = x.CardBankId,
                    //                                CardBrandId = x.CardBrandId,
                    //                                AccountNumber = x.AccountNumber,
                    //                                ReferenceNumber = x.ReferenceNumber,
                    //                                UserAccountId = x.AccountId,
                    //                                UserAccountKey = x.Account.Guid,
                    //                                UserDisplayName = x.Account.DisplayName,
                    //                                UserMobileNumber = x.Account.MobileNumber,
                    //                                StoreReferenceId = x.SubParentId,
                    //                                ProviderId = x.ProviderId,
                    //                                AcquirerId = x.BankId,
                    //                                CreatedById = x.CreatedById,
                    //                                CashierId = x.CashierId,
                    //                                StatusId = x.StatusId,
                    //                                StatusCode = x.Status.SystemName,
                    //                            })
                    //                            .Where(_Request.SearchCondition)
                    //                            .Select(x => x.AccountId)
                    //                            .Distinct()
                    //                            .Count();
                    _OTransactionOverview.Transactions = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.CampaignId == null
                                                && x.ModeId == TransactionMode.Credit
                                                && x.ParentTransaction.TotalAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountMobileNumber = x.Account.MobileNumber,

                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    StoreReferenceId = x.SubParentId,
                                                    StoreDisplayName = x.SubParent.DisplayName,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    TerminalReferenceId = x.TerminalId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    StatusName = x.Status.Name,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();

                    _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId == null
                                                 && x.ParentTransaction.TotalAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountMobileNumber = x.Account.MobileNumber,

                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    StoreReferenceId = x.SubParentId,
                                                    StoreDisplayName = x.SubParent.DisplayName,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    TerminalReferenceId = x.TerminalId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    StatusName = x.Status.Name,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);


                    _OTransactionOverview.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                              && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId == null
                                              && x.ParentTransaction.TotalAmount > 0
                                              && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                  && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                              || x.SourceId == TransactionSource.ThankUCashPlus))
                                              .Select(x => new OTransaction.Sale
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,

                                                  AccountId = x.AccountId,
                                                  AccountKey = x.Account.Guid,
                                                  AccountDisplayName = x.Account.DisplayName,
                                                  AccountMobileNumber = x.Account.MobileNumber,

                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate,
                                                  TypeId = x.TypeId,
                                                  RewardAmount = x.ReferenceAmount,
                                                  UserAmount = x.TotalAmount,
                                                  CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                  CardBankId = x.CardBankId,
                                                  CardBrandId = x.CardBrandId,
                                                  AccountNumber = x.AccountNumber,
                                                  ReferenceNumber = x.ReferenceNumber,
                                                  UserAccountId = x.AccountId,
                                                  UserAccountKey = x.Account.Guid,
                                                  UserDisplayName = x.Account.DisplayName,
                                                  UserMobileNumber = x.Account.MobileNumber,
                                                  ParentId = x.ParentId,
                                                  ParentDisplayName = x.Parent.DisplayName,
                                                  StoreReferenceId = x.SubParentId,
                                                  StoreDisplayName = x.SubParent.DisplayName,
                                                  ProviderId = x.ProviderId,
                                                  AcquirerId = x.BankId,
                                                  CreatedById = x.CreatedById,
                                                  CashierId = x.CashierId,
                                                  TerminalId = x.Terminal.IdentificationNumber,
                                                  TerminalReferenceId = x.TerminalId,
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name,
                                              })
                                              .Where(_Request.SearchCondition)
                                              .Sum(x => x.RewardAmount);

                    //_OTransactionOverview.CommissionAmount = _HCoreContext.HCUAccountTransaction
                    //                         .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                    //                            && x.ParentId == _Request.AccountId
                    //                         && x.ModeId == TransactionMode.Credit
                    //                           && x.CampaignId == null
                    //                          && x.ParentTransaction.TotalAmount > 0
                    //                         && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                    //                             && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                    //                         || x.SourceId == TransactionSource.ThankUCashPlus))
                    //                         .Select(x => new OTransaction.Sale
                    //                         {
                    //                             ReferenceId = x.Id,
                    //                             ReferenceKey = x.Guid,
                    //                             InvoiceAmount = x.PurchaseAmount,
                    //                             TransactionDate = x.TransactionDate,
                    //                             TypeId = x.TypeId,
                    //                             RewardAmount = x.ReferenceAmount,
                    //                             UserAmount = x.TotalAmount,
                    //                             CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                    //                             CardBankId = x.CardBankId,
                    //                             CardBrandId = x.CardBrandId,
                    //                             AccountNumber = x.AccountNumber,
                    //                             ReferenceNumber = x.ReferenceNumber,
                    //                             UserAccountId = x.AccountId,
                    //                             UserAccountKey = x.Account.Guid,
                    //                             UserDisplayName = x.Account.DisplayName,
                    //                             UserMobileNumber = x.Account.MobileNumber,
                    //                             StoreReferenceId = x.SubParentId,
                    //                             ProviderId = x.ProviderId,
                    //                             AcquirerId = x.BankId,
                    //                             CreatedById = x.CreatedById,
                    //                             CashierId = x.CashierId,
                    //                             StatusId = x.StatusId,
                    //                             StatusCode = x.Status.SystemName,
                    //                         })
                    //                         .Where(_Request.SearchCondition)
                    //                         .Sum(x => x.CommissionAmount);

                    _OTransactionOverview.UserAmount = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                              && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId == null
                                              && x.ParentTransaction.TotalAmount > 0
                                              && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                  && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                              || x.SourceId == TransactionSource.ThankUCashPlus))
                                              .Select(x => new OTransaction.Sale
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,

                                                  AccountId = x.AccountId,
                                                  AccountKey = x.Account.Guid,
                                                  AccountDisplayName = x.Account.DisplayName,
                                                  AccountMobileNumber = x.Account.MobileNumber,

                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate,
                                                  TypeId = x.TypeId,
                                                  RewardAmount = x.ReferenceAmount,
                                                  UserAmount = x.TotalAmount,
                                                  CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                  CardBankId = x.CardBankId,
                                                  CardBrandId = x.CardBrandId,
                                                  AccountNumber = x.AccountNumber,
                                                  ReferenceNumber = x.ReferenceNumber,
                                                  UserAccountId = x.AccountId,
                                                  UserAccountKey = x.Account.Guid,
                                                  UserDisplayName = x.Account.DisplayName,
                                                  UserMobileNumber = x.Account.MobileNumber,
                                                  ParentId = x.ParentId,
                                                  ParentDisplayName = x.Parent.DisplayName,
                                                  StoreReferenceId = x.SubParentId,
                                                  StoreDisplayName = x.SubParent.DisplayName,
                                                  ProviderId = x.ProviderId,
                                                  AcquirerId = x.BankId,
                                                  CreatedById = x.CreatedById,
                                                  CashierId = x.CashierId,
                                                  TerminalId = x.Terminal.IdentificationNumber,
                                                  TerminalReferenceId = x.TerminalId,
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name,
                                              })
                                             .Where(_Request.SearchCondition)
                                             .Sum(x => x.UserAmount);

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardTransactionOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the pending reward transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPendingRewardTransaction(OList.Request _Request)
        {
            if (_Request.IsDownload)
            {
                var _Actor = ActorSystem.Create("ActorPendingRewardTransactionDownloader");
                var _ActorNotify = _Actor.ActorOf<ActorPendingRewardTransactionDownloader>("ActorPendingRewardTransactionDownloader");
                _ActorNotify.Tell(_Request);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
            }
            else
            {
                #region Manage Exception
                try
                {
                    _SaleDownload = new List<OTransaction.SaleDownload>();
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    if (string.IsNullOrEmpty(_Request.SortExpression))
                    {
                        HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                    }
                    if (_Request.Limit < 1)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.CampaignId == null
                                                && x.ModeId == TransactionMode.Credit
                                                && x.ParentTransaction.TotalAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                    .Select(x => new OTransaction.Sale
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountMobileNumber = x.Account.MobileNumber,

                                                        InvoiceAmount = x.PurchaseAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        TypeId = x.TypeId,
                                                        RewardAmount = x.ReferenceAmount,
                                                        UserAmount = x.TotalAmount,
                                                        CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                        CardBankId = x.CardBankId,
                                                        CardBrandId = x.CardBrandId,
                                                        AccountNumber = x.AccountNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        UserAccountId = x.AccountId,
                                                        UserAccountKey = x.Account.Guid,
                                                        UserDisplayName = x.Account.DisplayName,
                                                        UserMobileNumber = x.Account.MobileNumber,
                                                        ParentId = x.ParentId,
                                                        ParentDisplayName = x.Parent.DisplayName,
                                                        StoreReferenceId = x.SubParentId,
                                                        StoreDisplayName = x.SubParent.DisplayName,
                                                        ProviderId = x.ProviderId,
                                                        AcquirerId = x.BankId,
                                                        CreatedById = x.CreatedById,
                                                        CashierId = x.CashierId,
                                                        TerminalId = x.Terminal.IdentificationNumber,
                                                        TerminalReferenceId = x.TerminalId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusId = x.StatusId,
                                                        StatusName = x.Status.Name,
                                                    })
                                                    .Count(_Request.SearchCondition);
                            #endregion
                        }
                        #region Get Data
                        _Sales = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.CampaignId == null
                                                && x.ModeId == TransactionMode.Credit
                                                && x.ParentTransaction.TotalAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                    .Select(x => new OTransaction.Sale
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountMobileNumber = x.Account.MobileNumber,

                                                        InvoiceAmount = x.PurchaseAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        TypeId = x.TypeId,
                                                        RewardAmount = x.ReferenceAmount,
                                                        UserAmount = x.TotalAmount,
                                                        CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                        CardBankId = x.CardBankId,
                                                        CardBrandId = x.CardBrandId,
                                                        AccountNumber = x.AccountNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        UserAccountId = x.AccountId,
                                                        UserAccountKey = x.Account.Guid,
                                                        UserDisplayName = x.Account.DisplayName,
                                                        UserMobileNumber = x.Account.MobileNumber,
                                                        ParentId = x.ParentId,
                                                        ParentDisplayName = x.Parent.DisplayName,
                                                        StoreReferenceId = x.SubParentId,
                                                        StoreDisplayName = x.SubParent.DisplayName,
                                                        ProviderId = x.ProviderId,
                                                        AcquirerId = x.BankId,
                                                        CreatedById = x.CreatedById,
                                                        CashierId = x.CashierId,
                                                        TerminalId = x.Terminal.IdentificationNumber,
                                                        TerminalReferenceId = x.TerminalId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusId = x.StatusId,
                                                        StatusName = x.Status.Name,
                                                    })
                                                         .Where(_Request.SearchCondition)
                                                         .OrderBy(_Request.SortExpression)
                                                         .Skip(_Request.Offset)
                                                         .Take(_Request.Limit)
                                                         .ToList();
                        #endregion
                        #region Send Response
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                        #endregion
                    }
                }
                catch (Exception _Exception)
                {
                    HCoreHelper.LogException("GetPendingRewardTransaction", _Exception, _Request.UserReference);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                }
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the pending reward transaction download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetPendingRewardTransactionDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _RewardDownload = new List<OTransaction.RewardDownload>();
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                long StorageId = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                    _HCUAccountParameter.Name = "Pending_Rewards_Sheet";
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Inactive;
                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    _HCoreContext.SaveChanges();
                    StorageId = _HCUAccountParameter.Id;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                    && x.ParentId == _Request.AccountId
                                                    && x.ModeId == TransactionMode.Credit
                                                    && x.ParentTransaction.TotalAmount > 0
                                                    && x.CampaignId == null
                                                    && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                    || x.SourceId == TransactionSource.ThankUCashPlus))
                        //_Request.TotalRecords = _HCoreContext.TUCLoyaltyPending
                        //                            .Where(x => x.MerchantId == _Request.AccountId
                        //                            && x.StatusId == HelperStatus.Transaction.Pending
                        //                            && x.LoyaltyTypeId == Loyalty.Reward)
                                                    .Select(x => new OTransaction.Sale
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        InvoiceAmount = x.PurchaseAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        TypeId = x.TypeId,
                                                        RewardAmount = x.TotalAmount,
                                                        CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                        UserAmount = x.Amount,
                                                        //CardBankId = x.CardBankId,
                                                        //CardBrandId = x.CardBrandId,
                                                        AccountNumber = x.AccountNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        UserAccountId = x.AccountId,
                                                        UserAccountKey = x.Account.Guid,
                                                        UserDisplayName = x.Account.DisplayName,
                                                        UserMobileNumber = x.Account.MobileNumber,
                                                        StoreReferenceId = x.SubParentId,
                                                        ProviderId = x.ProviderId,
                                                        AcquirerId = x.BankId,
                                                        TerminalId = x.Terminal.DisplayName,
                                                        CreatedById = x.CreatedById,
                                                        CashierId = x.CashierId,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        //ReferenceId = x.Id,
                                                        //ReferenceKey = x.Guid,
                                                        //InvoiceAmount = x.InvoiceAmount,
                                                        //TransactionDate = x.TransactionDate,
                                                        //TypeId = x.TypeId,
                                                        //RewardAmount = x.TotalAmount,
                                                        //CommissionAmount = x.CommissionAmount,
                                                        //UserAmount = x.Amount,
                                                        ////CardBankId = x.CardBankId,
                                                        ////CardBrandId = x.CardBrandId,
                                                        //AccountNumber = x.AccountNumber,
                                                        //ReferenceNumber = x.ReferenceNumber,
                                                        //UserAccountId = x.ToAccountId,
                                                        //UserAccountKey = x.ToAccount.Guid,
                                                        //UserDisplayName = x.ToAccount.DisplayName,
                                                        //UserMobileNumber = x.ToAccount.MobileNumber,
                                                        //StoreReferenceId = x.StoreId,
                                                        //ProviderId = x.ProviderId,
                                                        //AcquirerId = x.AcquirerId,
                                                        //TerminalId = x.Terminal.DisplayName,
                                                        //CreatedById = x.CreatedById,
                                                        //CashierId = x.CashierId,
                                                        //StatusId = x.StatusId,
                                                        //StatusCode = x.Status.SystemName,
                                                        //StatusName = x.Status.Name,
                                                    })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    double Iterations = Math.Round((double)_Request.TotalRecords / 1000, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        _Sales.AddRange(_HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                    && x.ParentId == _Request.AccountId
                                                    && x.ModeId == TransactionMode.Credit
                                                    && x.ParentTransaction.TotalAmount > 0
                                                    && x.CampaignId == null
                                                    && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                    || x.SourceId == TransactionSource.ThankUCashPlus))
                                                    .Select(x => new OTransaction.Sale
                                                    //_Sales.AddRange(_HCoreContext.TUCLoyaltyPending
                                                    //                            .Where(x => x.MerchantId == _Request.AccountId
                                                    //                            && x.StatusId == HelperStatus.Transaction.Pending
                                                    //                            && x.LoyaltyTypeId == Loyalty.Reward)
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        InvoiceAmount = x.PurchaseAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        TypeId = x.TypeId,
                                                        RewardAmount = x.TotalAmount,
                                                        CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                        UserAmount = x.Amount,
                                                        //CardBankId = x.CardBankId,
                                                        //CardBrandId = x.CardBrandId,
                                                        AccountNumber = x.AccountNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        UserAccountId = x.AccountId,
                                                        UserAccountKey = x.Account.Guid,
                                                        UserDisplayName = x.Account.DisplayName,
                                                        UserMobileNumber = x.Account.MobileNumber,
                                                        StoreReferenceId = x.SubParentId,
                                                        ProviderId = x.ProviderId,
                                                        AcquirerId = x.BankId,
                                                        TerminalId = x.Terminal.DisplayName,
                                                        CreatedById = x.CreatedById,
                                                        CashierId = x.CashierId,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        //ReferenceId = x.Id,
                                                        //ReferenceKey = x.Guid,
                                                        //InvoiceAmount = x.InvoiceAmount,
                                                        //TransactionDate = x.TransactionDate,
                                                        //TypeId = x.TypeId,
                                                        //RewardAmount = x.TotalAmount,
                                                        //CommissionAmount = x.CommissionAmount,
                                                        //UserAmount = x.Amount,
                                                        ////CardBankId = x.CardBankId,
                                                        ////CardBrandId = x.CardBrandId,
                                                        //AccountNumber = x.AccountNumber,
                                                        //ReferenceNumber = x.ReferenceNumber,
                                                        //UserAccountId = x.ToAccountId,
                                                        //UserAccountKey = x.ToAccount.Guid,
                                                        //UserDisplayName = x.ToAccount.DisplayName,
                                                        //UserMobileNumber = x.ToAccount.MobileNumber,
                                                        //StoreReferenceId = x.StoreId,
                                                        //ProviderId = x.ProviderId,
                                                        //AcquirerId = x.AcquirerId,
                                                        //TerminalId = x.Terminal.DisplayName,
                                                        //CreatedById = x.CreatedById,
                                                        //CashierId = x.CashierId,
                                                        //StatusId = x.StatusId,
                                                        //StatusCode = x.Status.SystemName,
                                                        //StatusName = x.Status.Name,
                                                    })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(1000)
                                                     .ToList());
                        _Request.Offset += 1000;
                    }
                    #endregion
                    _Sales = FormatTransactions(_Sales);
                    foreach (var SalesItem in _Sales)
                    {
                        _RewardDownload.Add(new OTransaction.RewardDownload
                        {
                            ReferenceId = SalesItem.ReferenceId,
                            TransactionDate = HCoreHelper.GetGMTToNigeria(SalesItem.TransactionDate),
                            User = SalesItem.UserDisplayName,
                            MobileNumber = SalesItem.UserMobileNumber,
                            TypeName = SalesItem.TypeName,
                            InvoiceAmount = SalesItem.InvoiceAmount,
                            RewardAmount = SalesItem.RewardAmount,
                            ConvinenceCharge = SalesItem.CommissionAmount,
                            ReferenceNumber = SalesItem.ReferenceNumber,
                            CardNumber = SalesItem.AccountNumber,
                            CardBrand = SalesItem.CardBrandName ?? "",
                            CardBank = SalesItem.CardBankName,

                            Store = SalesItem.StoreDisplayName,
                            CashierId = SalesItem.CashierCode,
                            CashierDisplayName = SalesItem.CashierDisplayName,
                            Bank = SalesItem.AcquirerDisplayName,
                            Provider = SalesItem.ProviderDisplayName,
                            Issuer = SalesItem.CreatedByDisplayName,
                            TerminalId = SalesItem.TerminalId,
                            Status = SalesItem.StatusName,
                        });
                    }
                    using (var _XLWorkbook = new XLWorkbook())
                    {
                        var _WorkSheet = _XLWorkbook.Worksheets.Add("Pending_Rewards_Sheet");
                        PropertyInfo[] properties = _RewardDownload.First().GetType().GetProperties();
                        List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                        for (int i = 0; i < headerNames.Count; i++)
                        {
                            _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                        }
                        _WorkSheet.Cell(2, 1).InsertData(_RewardDownload);
                        MemoryStream _MemoryStream = new MemoryStream();
                        _XLWorkbook.SaveAs(_MemoryStream);
                        long? _StorageId = HCoreHelper.SaveStorage("Pending_Rewards_Sheet", "xlsx", _MemoryStream, _Request.UserReference);
                        if (_StorageId != null && _StorageId != 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                                if (TItem != null)
                                {
                                    TItem.IconStorageId = _StorageId;
                                    TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    TItem.ModifyById = _Request.UserReference.AccountId;
                                    TItem.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }
                    #region Send Response
                    _HCoreContext.Dispose();
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetPendingRewardTransactionDownload", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the pending reward transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPendingRewardTransactionOverview(OList.Request _Request)
        {
            #region Manage Exception
            //try
            //{
            //    using (_HCoreContext = new HCoreContext())
            //    {
            //        if (string.IsNullOrEmpty(_Request.SearchCondition))
            //        {
            //            HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
            //        }
            //        _OTransactionOverview = new OTransactionOverview();
            //        //_OTransactionOverview.Customers = _HCoreContext.HCUAccountTransaction
            //        //                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
            //        //                            && x.ParentId == _Request.AccountId
            //        //                            && x.ModeId == TransactionMode.Credit
            //        //                            && x.CampaignId == null
            //        //                             && x.ParentTransaction.TotalAmount > 0
            //        //                            && ((x.TypeId != TransactionType.ThankUCashPlusCredit
            //        //                                && x.SourceId == TransactionSource.TUC)
            //        //                            || x.SourceId == TransactionSource.ThankUCashPlus))
            //        //                            .Select(x => new OTransaction.Sale
            //        //                            {
            //        //                                ReferenceId = x.Id,
            //        //                                ReferenceKey = x.Guid,
            //        //                                InvoiceAmount = x.PurchaseAmount,
            //        //                                TransactionDate = x.TransactionDate,
            //        //                                TypeId = x.TypeId,
            //        //                                RewardAmount = x.ReferenceAmount,
            //        //                                CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
            //        //                                UserAmount = x.TotalAmount,
            //        //                                CardBankId = x.CardBankId,
            //        //                                CardBrandId = x.CardBrandId,
            //        //                                AccountNumber = x.AccountNumber,
            //        //                                ReferenceNumber = x.ReferenceNumber,
            //        //                                UserAccountId = x.AccountId,
            //        //                                UserAccountKey = x.Account.Guid,
            //        //                                UserDisplayName = x.Account.DisplayName,
            //        //                                UserMobileNumber = x.Account.MobileNumber,
            //        //                                StoreReferenceId = x.SubParentId,
            //        //                                ProviderId = x.ProviderId,
            //        //                                AcquirerId = x.BankId,
            //        //                                CreatedById = x.CreatedById,
            //        //                                CashierId = x.CashierId,
            //        //                                StatusId = x.StatusId,
            //        //                                StatusCode = x.Status.SystemName,
            //        //                            })
            //        //                            .Where(_Request.SearchCondition)
            //        //                            .Select(x => x.AccountId)
            //        //                            .Distinct()
            //        //                            .Count();

            //        _OTransactionOverview.Transactions = _HCoreContext.TUCLoyaltyPending
            //                                        .Where(x => x.MerchantId == _Request.AccountId
            //                                        && x.StatusId == HelperStatus.Transaction.Pending
            //                                        && x.LoyaltyTypeId == Loyalty.Reward)
            //                                        .Select(x => new OTransaction.Sale
            //                                        {
            //                                            ReferenceId = x.Id,
            //                                            ReferenceKey = x.Guid,
            //                                            InvoiceAmount = x.InvoiceAmount,
            //                                            TransactionDate = x.TransactionDate,
            //                                            TypeId = x.TypeId,
            //                                            RewardAmount = x.TotalAmount,
            //                                            CommissionAmount = x.CommissionAmount,
            //                                            UserAmount = x.Amount,
            //                                            //CardBankId = x.CardBankId,
            //                                            //CardBrandId = x.CardBrandId,
            //                                            AccountNumber = x.AccountNumber,
            //                                            ReferenceNumber = x.ReferenceNumber,
            //                                            UserAccountId = x.ToAccountId,
            //                                            UserAccountKey = x.ToAccount.Guid,
            //                                            UserDisplayName = x.ToAccount.DisplayName,
            //                                            UserMobileNumber = x.ToAccount.MobileNumber,
            //                                            StoreReferenceId = x.StoreId,
            //                                            ProviderId = x.ProviderId,
            //                                            AcquirerId = x.AcquirerId,
            //                                            TerminalId = x.Terminal.DisplayName,
            //                                            CreatedById = x.CreatedById,
            //                                            CashierId = x.CashierId,
            //                                            StatusId = x.StatusId,
            //                                            StatusCode = x.Status.SystemName,
            //                                        })
            //                                    .Where(_Request.SearchCondition)
            //                                    .Count();

            //        _OTransactionOverview.InvoiceAmount = _HCoreContext.TUCLoyaltyPending
            //                                        .Where(x => x.MerchantId == _Request.AccountId
            //                                        && x.StatusId == HelperStatus.Transaction.Pending
            //                                        && x.LoyaltyTypeId == Loyalty.Reward)
            //                                        .Select(x => new OTransaction.Sale
            //                                        {
            //                                            ReferenceId = x.Id,
            //                                            ReferenceKey = x.Guid,
            //                                            InvoiceAmount = x.InvoiceAmount,
            //                                            TransactionDate = x.TransactionDate,
            //                                            TypeId = x.TypeId,
            //                                            RewardAmount = x.TotalAmount,
            //                                            CommissionAmount = x.CommissionAmount,
            //                                            UserAmount = x.Amount,
            //                                            //CardBankId = x.CardBankId,
            //                                            //CardBrandId = x.CardBrandId,
            //                                            AccountNumber = x.AccountNumber,
            //                                            ReferenceNumber = x.ReferenceNumber,
            //                                            UserAccountId = x.ToAccountId,
            //                                            UserAccountKey = x.ToAccount.Guid,
            //                                            UserDisplayName = x.ToAccount.DisplayName,
            //                                            UserMobileNumber = x.ToAccount.MobileNumber,
            //                                            StoreReferenceId = x.StoreId,
            //                                            ProviderId = x.ProviderId,
            //                                            AcquirerId = x.AcquirerId,
            //                                            TerminalId = x.Terminal.DisplayName,
            //                                            CreatedById = x.CreatedById,
            //                                            CashierId = x.CashierId,
            //                                            StatusId = x.StatusId,
            //                                            StatusCode = x.Status.SystemName,
            //                                        })
            //                                    .Where(_Request.SearchCondition)
            //                                    .Sum(x => x.InvoiceAmount);


            //        _OTransactionOverview.RewardAmount = _HCoreContext.TUCLoyaltyPending
            //                                        .Where(x => x.MerchantId == _Request.AccountId
            //                                        && x.StatusId == HelperStatus.Transaction.Pending
            //                                        && x.LoyaltyTypeId == Loyalty.Reward)
            //                                        .Select(x => new OTransaction.Sale
            //                                        {
            //                                            ReferenceId = x.Id,
            //                                            ReferenceKey = x.Guid,
            //                                            InvoiceAmount = x.InvoiceAmount,
            //                                            TransactionDate = x.TransactionDate,
            //                                            TypeId = x.TypeId,
            //                                            RewardAmount = x.TotalAmount,
            //                                            CommissionAmount = x.CommissionAmount,
            //                                            UserAmount = x.Amount,
            //                                            //CardBankId = x.CardBankId,
            //                                            //CardBrandId = x.CardBrandId,
            //                                            AccountNumber = x.AccountNumber,
            //                                            ReferenceNumber = x.ReferenceNumber,
            //                                            UserAccountId = x.ToAccountId,
            //                                            UserAccountKey = x.ToAccount.Guid,
            //                                            UserDisplayName = x.ToAccount.DisplayName,
            //                                            UserMobileNumber = x.ToAccount.MobileNumber,
            //                                            StoreReferenceId = x.StoreId,
            //                                            ProviderId = x.ProviderId,
            //                                            AcquirerId = x.AcquirerId,
            //                                            TerminalId = x.Terminal.DisplayName,
            //                                            CreatedById = x.CreatedById,
            //                                            CashierId = x.CashierId,
            //                                            StatusId = x.StatusId,
            //                                            StatusCode = x.Status.SystemName,
            //                                        })
            //                                  .Where(_Request.SearchCondition)
            //                                  .Sum(x => x.RewardAmount);

            //        //_OTransactionOverview.CommissionAmount = _HCoreContext.HCUAccountTransaction
            //        //                         .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
            //        //                            && x.ParentId == _Request.AccountId
            //        //                         && x.ModeId == TransactionMode.Credit
            //        //                           && x.CampaignId == null
            //        //                          && x.ParentTransaction.TotalAmount > 0
            //        //                         && ((x.TypeId != TransactionType.ThankUCashPlusCredit
            //        //                             && x.SourceId == TransactionSource.TUC)
            //        //                         || x.SourceId == TransactionSource.ThankUCashPlus))
            //        //                         .Select(x => new OTransaction.Sale
            //        //                         {
            //        //                             ReferenceId = x.Id,
            //        //                             ReferenceKey = x.Guid,
            //        //                             InvoiceAmount = x.PurchaseAmount,
            //        //                             TransactionDate = x.TransactionDate,
            //        //                             TypeId = x.TypeId,
            //        //                             RewardAmount = x.ReferenceAmount,
            //        //                             UserAmount = x.TotalAmount,
            //        //                             CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
            //        //                             CardBankId = x.CardBankId,
            //        //                             CardBrandId = x.CardBrandId,
            //        //                             AccountNumber = x.AccountNumber,
            //        //                             ReferenceNumber = x.ReferenceNumber,
            //        //                             UserAccountId = x.AccountId,
            //        //                             UserAccountKey = x.Account.Guid,
            //        //                             UserDisplayName = x.Account.DisplayName,
            //        //                             UserMobileNumber = x.Account.MobileNumber,
            //        //                             StoreReferenceId = x.SubParentId,
            //        //                             ProviderId = x.ProviderId,
            //        //                             AcquirerId = x.BankId,
            //        //                             CreatedById = x.CreatedById,
            //        //                             CashierId = x.CashierId,
            //        //                             StatusId = x.StatusId,
            //        //                             StatusCode = x.Status.SystemName,
            //        //                         })
            //        //                         .Where(_Request.SearchCondition)
            //        //                         .Sum(x => x.CommissionAmount);

            //        //_OTransactionOverview.UserAmount = _HCoreContext.HCUAccountTransaction
            //        //                         .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
            //        //                            && x.ParentId == _Request.AccountId
            //        //                         && x.ModeId == TransactionMode.Credit
            //        //                           && x.CampaignId == null
            //        //                          && x.ParentTransaction.TotalAmount > 0
            //        //                         && ((x.TypeId != TransactionType.ThankUCashPlusCredit
            //        //                             && x.SourceId == TransactionSource.TUC)
            //        //                         || x.SourceId == TransactionSource.ThankUCashPlus))
            //        //                         .Select(x => new OTransaction.Sale
            //        //                         {
            //        //                             ReferenceId = x.Id,
            //        //                             ReferenceKey = x.Guid,
            //        //                             InvoiceAmount = x.PurchaseAmount,
            //        //                             TransactionDate = x.TransactionDate,
            //        //                             TypeId = x.TypeId,
            //        //                             RewardAmount = x.ReferenceAmount,
            //        //                             UserAmount = x.TotalAmount,
            //        //                             CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
            //        //                             CardBankId = x.CardBankId,
            //        //                             CardBrandId = x.CardBrandId,
            //        //                             AccountNumber = x.AccountNumber,
            //        //                             ReferenceNumber = x.ReferenceNumber,
            //        //                             UserAccountId = x.AccountId,
            //        //                             UserAccountKey = x.Account.Guid,
            //        //                             UserDisplayName = x.Account.DisplayName,
            //        //                             UserMobileNumber = x.Account.MobileNumber,
            //        //                             StoreReferenceId = x.SubParentId,
            //        //                             ProviderId = x.ProviderId,
            //        //                             AcquirerId = x.BankId,
            //        //                             CreatedById = x.CreatedById,
            //        //                             CashierId = x.CashierId,
            //        //                             StatusId = x.StatusId,
            //        //                             StatusCode = x.Status.SystemName,
            //        //                         })
            //        //                         .Where(_Request.SearchCondition)
            //        //                         .Sum(x => x.UserAmount);

            //        #region Send Response
            //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
            //        #endregion
            //    }
            //}
            //catch (Exception _Exception)
            //{
            //    HCoreHelper.LogException("GetRewardTransactionOverview", _Exception, _Request.UserReference);
            //    OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
            //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            //}
            #endregion

            #region Manage Exception
            //try
            //{
            //    if (string.IsNullOrEmpty(_Request.SearchCondition))
            //    {
            //        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
            //    }
            //    if (string.IsNullOrEmpty(_Request.SortExpression))
            //    {
            //        HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
            //    }
            //    using (_HCoreContext = new HCoreContext())
            //    {
            //        _OTransactionOverview = new OTransactionOverview();
            //        _OTransactionOverview.Customers = _HCoreContext.HCUAccountTransaction
            //                                        .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
            //                                        && x.Account.AccountTypeId == UserAccountType.Appuser
            //                                    && x.Parent.CountryId == _Request.UserReference.SystemCountry
            //                                    && x.ModeId == TransactionMode.Credit
            //                                    && x.CampaignId == null
            //                                        && x.Type.SubParentId == TransactionTypeCategory.Reward
            //                                     && x.ParentTransaction.TotalAmount > 0
            //                                    && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash
            //                                        && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
            //                                    || x.SourceId == TransactionSource.ThankUCashPlus))
            //                                    .Select(x => new OTransaction.Sale
            //                                    {
            //                                        ReferenceId = x.Id,
            //                                        ReferenceKey = x.Guid,
            //                                        InvoiceAmount = x.PurchaseAmount,
            //                                        TransactionDate = x.TransactionDate,
            //                                        TypeId = x.TypeId,
            //                                        RewardAmount = x.ReferenceAmount,
            //                                        UserAmount = x.TotalAmount,
            //                                        CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
            //                                        CardBankId = x.CardBankId,
            //                                        CardBrandId = x.CardBrandId,
            //                                        AccountNumber = x.AccountNumber,
            //                                        ReferenceNumber = x.ReferenceNumber,
            //                                        UserAccountId = x.AccountId,
            //                                        UserAccountKey = x.Account.Guid,
            //                                        UserDisplayName = x.Account.DisplayName,
            //                                        UserMobileNumber = x.Account.MobileNumber,
            //                                        ParentId = x.ParentId,
            //                                        SubParentId = x.SubParentId,
            //                                        ProviderId = x.ProviderId,
            //                                        AcquirerId = x.BankId,
            //                                        CreatedById = x.CreatedById,
            //                                        CashierId = x.CashierId,
            //                                        StatusCode = x.Status.SystemName,
            //                                        StatusId = x.StatusId,
            //                                        TerminalReferenceId = x.TerminalId,
            //                                        TerminalReferenceKey = x.Terminal.Guid,
            //                                        TerminalId = x.Terminal.IdentificationNumber,
            //                                        Balance = x.Balance,
            //                                        SourceId = x.SourceId,
            //                                    })
            //                                    .Where(_Request.SearchCondition)
            //                                    .Select(x => x.UserAccountId)
            //                                    .Distinct()
            //                                    .Count();

            //        _OTransactionOverview.Transactions = _HCoreContext.HCUAccountTransaction
            //                                        .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
            //                                        && x.Account.AccountTypeId == UserAccountType.Appuser
            //                                    && x.Parent.CountryId == _Request.UserReference.SystemCountry
            //                                    && x.CampaignId == null
            //                                        && x.Type.SubParentId == TransactionTypeCategory.Reward
            //                                    && x.ModeId == TransactionMode.Credit
            //                                    && x.ParentTransaction.TotalAmount > 0
            //                                    && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash
            //                                        && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
            //                                    || x.SourceId == TransactionSource.ThankUCashPlus))
            //                                    .Select(x => new OTransaction.Sale
            //                                    {
            //                                        ReferenceId = x.Id,
            //                                        ReferenceKey = x.Guid,
            //                                        InvoiceAmount = x.PurchaseAmount,
            //                                        TransactionDate = x.TransactionDate,
            //                                        TypeId = x.TypeId,
            //                                        RewardAmount = x.ReferenceAmount,
            //                                        UserAmount = x.TotalAmount,
            //                                        CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
            //                                        CardBankId = x.CardBankId,
            //                                        CardBrandId = x.CardBrandId,
            //                                        AccountNumber = x.AccountNumber,
            //                                        ReferenceNumber = x.ReferenceNumber,
            //                                        UserAccountId = x.AccountId,
            //                                        UserAccountKey = x.Account.Guid,
            //                                        UserDisplayName = x.Account.DisplayName,
            //                                        UserMobileNumber = x.Account.MobileNumber,
            //                                        ParentId = x.ParentId,
            //                                        SubParentId = x.SubParentId,
            //                                        ProviderId = x.ProviderId,
            //                                        AcquirerId = x.BankId,
            //                                        CreatedById = x.CreatedById,
            //                                        CashierId = x.CashierId,
            //                                        StatusCode = x.Status.SystemName,
            //                                        StatusId = x.StatusId,
            //                                        TerminalReferenceId = x.TerminalId,
            //                                        TerminalReferenceKey = x.Terminal.Guid,
            //                                        TerminalId = x.Terminal.IdentificationNumber,
            //                                        Balance = x.Balance,
            //                                        SourceId = x.SourceId,
            //                                    })
            //                                    .Where(_Request.SearchCondition)
            //                                    .Count();

            //        _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
            //                                        .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
            //                                        && x.Account.AccountTypeId == UserAccountType.Appuser
            //                                    && x.Parent.CountryId == _Request.UserReference.SystemCountry
            //                                    && x.ModeId == TransactionMode.Credit
            //                                    && x.Type.SubParentId == TransactionTypeCategory.Reward
            //                                    && x.CampaignId == null
            //                                     && x.ParentTransaction.TotalAmount > 0
            //                                     && (((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash)
            //                                    && (x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.CardReward)
            //                                         && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
            //                                     || x.SourceId == TransactionSource.ThankUCashPlus))
            //                                    .Select(x => new OTransaction.Sale
            //                                    {
            //                                        ReferenceId = x.Id,
            //                                        ReferenceKey = x.Guid,
            //                                        InvoiceAmount = x.PurchaseAmount,
            //                                        TransactionDate = x.TransactionDate,
            //                                        TypeId = x.TypeId,
            //                                        RewardAmount = x.ReferenceAmount,
            //                                        UserAmount = x.TotalAmount,
            //                                        CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
            //                                        CardBankId = x.CardBankId,
            //                                        CardBrandId = x.CardBrandId,
            //                                        AccountNumber = x.AccountNumber,
            //                                        ReferenceNumber = x.ReferenceNumber,
            //                                        UserAccountId = x.AccountId,
            //                                        UserAccountKey = x.Account.Guid,
            //                                        UserDisplayName = x.Account.DisplayName,
            //                                        UserMobileNumber = x.Account.MobileNumber,
            //                                        ParentId = x.ParentId,
            //                                        SubParentId = x.SubParentId,
            //                                        ProviderId = x.ProviderId,
            //                                        AcquirerId = x.BankId,
            //                                        CreatedById = x.CreatedById,
            //                                        CashierId = x.CashierId,
            //                                        StatusCode = x.Status.SystemName,
            //                                        StatusId = x.StatusId,
            //                                        TerminalReferenceId = x.TerminalId,
            //                                        TerminalReferenceKey = x.Terminal.Guid,
            //                                        TerminalId = x.Terminal.IdentificationNumber,
            //                                        Balance = x.Balance,
            //                                        SourceId = x.SourceId,
            //                                    })
            //                                    .Where(_Request.SearchCondition)
            //                                    .Sum(x => x.InvoiceAmount);

            //        _OTransactionOverview.RewardAmount = _HCoreContext.HCUAccountTransaction
            //                                        .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
            //                                        && x.Account.AccountTypeId == UserAccountType.Appuser
            //                                    && x.Parent.CountryId == _Request.UserReference.SystemCountry
            //                                  && x.ModeId == TransactionMode.Credit
            //                                        && x.Type.SubParentId == TransactionTypeCategory.Reward
            //                                    && x.CampaignId == null
            //                                  && x.ParentTransaction.TotalAmount > 0
            //                                  && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash
            //                                      && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
            //                                  || x.SourceId == TransactionSource.ThankUCashPlus))
            //                                  .Select(x => new OTransaction.Sale
            //                                  {
            //                                      ReferenceId = x.Id,
            //                                      ReferenceKey = x.Guid,
            //                                      InvoiceAmount = x.PurchaseAmount,
            //                                      TransactionDate = x.TransactionDate,
            //                                      TypeId = x.TypeId,
            //                                      RewardAmount = x.ReferenceAmount,
            //                                      UserAmount = x.TotalAmount,
            //                                      CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
            //                                      CardBankId = x.CardBankId,
            //                                      CardBrandId = x.CardBrandId,
            //                                      AccountNumber = x.AccountNumber,
            //                                      ReferenceNumber = x.ReferenceNumber,
            //                                      UserAccountId = x.AccountId,
            //                                      UserAccountKey = x.Account.Guid,
            //                                      UserDisplayName = x.Account.DisplayName,
            //                                      UserMobileNumber = x.Account.MobileNumber,
            //                                      ParentId = x.ParentId,
            //                                      SubParentId = x.SubParentId,
            //                                      ProviderId = x.ProviderId,
            //                                      AcquirerId = x.BankId,
            //                                      CreatedById = x.CreatedById,
            //                                      CashierId = x.CashierId,
            //                                      StatusId = x.StatusId,
            //                                      StatusCode = x.Status.SystemName,
            //                                      TerminalReferenceId = x.TerminalId,
            //                                      TerminalReferenceKey = x.Terminal.Guid,
            //                                      TerminalId = x.Terminal.IdentificationNumber,
            //                                      Balance = x.Balance,
            //                                      SourceId = x.SourceId,
            //                                  })
            //                                  .Where(_Request.SearchCondition)
            //                                  .Sum(x => x.RewardAmount);

            //        _OTransactionOverview.CommissionAmount = _HCoreContext.HCUAccountTransaction
            //                                        .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
            //                                        && x.Account.AccountTypeId == UserAccountType.Appuser
            //                                    && x.Parent.CountryId == _Request.UserReference.SystemCountry
            //                                 && x.ModeId == TransactionMode.Credit
            //                                        && x.Type.SubParentId == TransactionTypeCategory.Reward
            //                                   && x.CampaignId == null
            //                                  && x.ParentTransaction.TotalAmount > 0
            //                                 && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash
            //                                     && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
            //                                 || x.SourceId == TransactionSource.ThankUCashPlus))
            //                                 .Select(x => new OTransaction.Sale
            //                                 {
            //                                     ReferenceId = x.Id,
            //                                     ReferenceKey = x.Guid,
            //                                     InvoiceAmount = x.PurchaseAmount,
            //                                     TransactionDate = x.TransactionDate,
            //                                     TypeId = x.TypeId,
            //                                     RewardAmount = x.ReferenceAmount,
            //                                     UserAmount = x.TotalAmount,
            //                                     CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
            //                                     CardBankId = x.CardBankId,
            //                                     CardBrandId = x.CardBrandId,
            //                                     AccountNumber = x.AccountNumber,
            //                                     ReferenceNumber = x.ReferenceNumber,
            //                                     UserAccountId = x.AccountId,
            //                                     UserAccountKey = x.Account.Guid,
            //                                     UserDisplayName = x.Account.DisplayName,
            //                                     UserMobileNumber = x.Account.MobileNumber,
            //                                     ParentId = x.ParentId,
            //                                     SubParentId = x.SubParentId,
            //                                     ProviderId = x.ProviderId,
            //                                     AcquirerId = x.BankId,
            //                                     CreatedById = x.CreatedById,
            //                                     CashierId = x.CashierId,
            //                                     StatusId = x.StatusId,
            //                                     StatusCode = x.Status.SystemName,
            //                                     TerminalReferenceId = x.TerminalId,
            //                                     TerminalReferenceKey = x.Terminal.Guid,
            //                                     TerminalId = x.Terminal.IdentificationNumber,
            //                                     Balance = x.Balance,
            //                                     SourceId = x.SourceId,
            //                                 })
            //                                 .Where(_Request.SearchCondition)
            //                                 .Sum(x => x.CommissionAmount);

            //        _OTransactionOverview.UserAmount = _HCoreContext.HCUAccountTransaction
            //                                        .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
            //                                        && x.Account.AccountTypeId == UserAccountType.Appuser
            //                                    && x.Parent.CountryId == _Request.UserReference.SystemCountry
            //                                 && x.ModeId == TransactionMode.Credit
            //                                        && x.Type.SubParentId == TransactionTypeCategory.Reward
            //                                   && x.CampaignId == null
            //                                  && x.ParentTransaction.TotalAmount > 0
            //                                 && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash
            //                                     && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
            //                                 || x.SourceId == TransactionSource.ThankUCashPlus))
            //                                 .Select(x => new OTransaction.Sale
            //                                 {
            //                                     ReferenceId = x.Id,
            //                                     ReferenceKey = x.Guid,
            //                                     InvoiceAmount = x.PurchaseAmount,
            //                                     TransactionDate = x.TransactionDate,
            //                                     TypeId = x.TypeId,
            //                                     RewardAmount = x.ReferenceAmount,
            //                                     UserAmount = x.TotalAmount,
            //                                     CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
            //                                     CardBankId = x.CardBankId,
            //                                     CardBrandId = x.CardBrandId,
            //                                     AccountNumber = x.AccountNumber,
            //                                     ReferenceNumber = x.ReferenceNumber,
            //                                     UserAccountId = x.AccountId,
            //                                     UserAccountKey = x.Account.Guid,
            //                                     UserDisplayName = x.Account.DisplayName,
            //                                     UserMobileNumber = x.Account.MobileNumber,
            //                                     ParentId = x.ParentId,
            //                                     SubParentId = x.SubParentId,
            //                                     ProviderId = x.ProviderId,
            //                                     AcquirerId = x.BankId,
            //                                     CreatedById = x.CreatedById,
            //                                     CashierId = x.CashierId,
            //                                     StatusId = x.StatusId,
            //                                     StatusCode = x.Status.SystemName,
            //                                     TerminalReferenceId = x.TerminalId,
            //                                     TerminalReferenceKey = x.Terminal.Guid,
            //                                     TerminalId = x.Terminal.IdentificationNumber,
            //                                     Balance = x.Balance,
            //                                     SourceId = x.SourceId,
            //                                 })
            //                                 .Where(_Request.SearchCondition)
            //                                 .Sum(x => x.UserAmount);

            //        #region Send Response
            //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
            //        #endregion
            //    }
            //}
            //catch (Exception _Exception)
            //{
            //    HCoreHelper.LogException("GetPendingRewardTransactionOverview", _Exception, _Request.UserReference);
            //    OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
            //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            //}
            #endregion

            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    _OTransactionOverview = new OTransactionOverview();
                    //_OTransactionOverview.Customers = _HCoreContext.HCUAccountTransaction
                    //                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                    //                            && x.ParentId == _Request.AccountId
                    //                            && x.ModeId == TransactionMode.Credit
                    //                            && x.CampaignId == null
                    //                             && x.ParentTransaction.TotalAmount > 0
                    //                            && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                    //                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                    //                            || x.SourceId == TransactionSource.ThankUCashPlus))
                    //                            .Select(x => new OTransaction.Sale
                    //                            {
                    //                                ReferenceId = x.Id,
                    //                                ReferenceKey = x.Guid,
                    //                                InvoiceAmount = x.PurchaseAmount,
                    //                                TransactionDate = x.TransactionDate,
                    //                                TypeId = x.TypeId,
                    //                                RewardAmount = x.ReferenceAmount,
                    //                                CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                    //                                UserAmount = x.TotalAmount,
                    //                                CardBankId = x.CardBankId,
                    //                                CardBrandId = x.CardBrandId,
                    //                                AccountNumber = x.AccountNumber,
                    //                                ReferenceNumber = x.ReferenceNumber,
                    //                                UserAccountId = x.AccountId,
                    //                                UserAccountKey = x.Account.Guid,
                    //                                UserDisplayName = x.Account.DisplayName,
                    //                                UserMobileNumber = x.Account.MobileNumber,
                    //                                StoreReferenceId = x.SubParentId,
                    //                                ProviderId = x.ProviderId,
                    //                                AcquirerId = x.BankId,
                    //                                CreatedById = x.CreatedById,
                    //                                CashierId = x.CashierId,
                    //                                StatusId = x.StatusId,
                    //                                StatusCode = x.Status.SystemName,
                    //                            })
                    //                            .Where(_Request.SearchCondition)
                    //                            .Select(x => x.AccountId)
                    //                            .Distinct()
                    //                            .Count();
                    _OTransactionOverview.Transactions = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.CampaignId == null
                                                && x.ModeId == TransactionMode.Credit
                                                && x.ParentTransaction.TotalAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountMobileNumber = x.Account.MobileNumber,

                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    StoreReferenceId = x.SubParentId,
                                                    StoreDisplayName = x.SubParent.DisplayName,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    TerminalReferenceId = x.TerminalId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    StatusName = x.Status.Name,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();

                    _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId == null
                                                 && x.ParentTransaction.TotalAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountMobileNumber = x.Account.MobileNumber,

                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,
                                                    StoreReferenceId = x.SubParentId,
                                                    StoreDisplayName = x.SubParent.DisplayName,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    TerminalReferenceId = x.TerminalId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    StatusName = x.Status.Name,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);


                    _OTransactionOverview.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                              && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId == null
                                              && x.ParentTransaction.TotalAmount > 0
                                              && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                  && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                              || x.SourceId == TransactionSource.ThankUCashPlus))
                                              .Select(x => new OTransaction.Sale
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,

                                                  AccountId = x.AccountId,
                                                  AccountKey = x.Account.Guid,
                                                  AccountDisplayName = x.Account.DisplayName,
                                                  AccountMobileNumber = x.Account.MobileNumber,

                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate,
                                                  TypeId = x.TypeId,
                                                  RewardAmount = x.ReferenceAmount,
                                                  UserAmount = x.TotalAmount,
                                                  CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                  CardBankId = x.CardBankId,
                                                  CardBrandId = x.CardBrandId,
                                                  AccountNumber = x.AccountNumber,
                                                  ReferenceNumber = x.ReferenceNumber,
                                                  UserAccountId = x.AccountId,
                                                  UserAccountKey = x.Account.Guid,
                                                  UserDisplayName = x.Account.DisplayName,
                                                  UserMobileNumber = x.Account.MobileNumber,
                                                  ParentId = x.ParentId,
                                                  ParentDisplayName = x.Parent.DisplayName,
                                                  StoreReferenceId = x.SubParentId,
                                                  StoreDisplayName = x.SubParent.DisplayName,
                                                  ProviderId = x.ProviderId,
                                                  AcquirerId = x.BankId,
                                                  CreatedById = x.CreatedById,
                                                  CashierId = x.CashierId,
                                                  TerminalId = x.Terminal.IdentificationNumber,
                                                  TerminalReferenceId = x.TerminalId,
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name,
                                              })
                                              .Where(_Request.SearchCondition)
                                              .Sum(x => x.RewardAmount);

                    //_OTransactionOverview.CommissionAmount = _HCoreContext.HCUAccountTransaction
                    //                         .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                    //                            && x.ParentId == _Request.AccountId
                    //                         && x.ModeId == TransactionMode.Credit
                    //                           && x.CampaignId == null
                    //                          && x.ParentTransaction.TotalAmount > 0
                    //                         && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                    //                             && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                    //                         || x.SourceId == TransactionSource.ThankUCashPlus))
                    //                         .Select(x => new OTransaction.Sale
                    //                         {
                    //                             ReferenceId = x.Id,
                    //                             ReferenceKey = x.Guid,
                    //                             InvoiceAmount = x.PurchaseAmount,
                    //                             TransactionDate = x.TransactionDate,
                    //                             TypeId = x.TypeId,
                    //                             RewardAmount = x.ReferenceAmount,
                    //                             UserAmount = x.TotalAmount,
                    //                             CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                    //                             CardBankId = x.CardBankId,
                    //                             CardBrandId = x.CardBrandId,
                    //                             AccountNumber = x.AccountNumber,
                    //                             ReferenceNumber = x.ReferenceNumber,
                    //                             UserAccountId = x.AccountId,
                    //                             UserAccountKey = x.Account.Guid,
                    //                             UserDisplayName = x.Account.DisplayName,
                    //                             UserMobileNumber = x.Account.MobileNumber,
                    //                             StoreReferenceId = x.SubParentId,
                    //                             ProviderId = x.ProviderId,
                    //                             AcquirerId = x.BankId,
                    //                             CreatedById = x.CreatedById,
                    //                             CashierId = x.CashierId,
                    //                             StatusId = x.StatusId,
                    //                             StatusCode = x.Status.SystemName,
                    //                         })
                    //                         .Where(_Request.SearchCondition)
                    //                         .Sum(x => x.CommissionAmount);

                    _OTransactionOverview.UserAmount = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                                                    && x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                              && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId == null
                                              && x.ParentTransaction.TotalAmount > 0
                                              && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                  && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                              || x.SourceId == TransactionSource.ThankUCashPlus))
                                              .Select(x => new OTransaction.Sale
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,

                                                  AccountId = x.AccountId,
                                                  AccountKey = x.Account.Guid,
                                                  AccountDisplayName = x.Account.DisplayName,
                                                  AccountMobileNumber = x.Account.MobileNumber,

                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate,
                                                  TypeId = x.TypeId,
                                                  RewardAmount = x.ReferenceAmount,
                                                  UserAmount = x.TotalAmount,
                                                  CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                  CardBankId = x.CardBankId,
                                                  CardBrandId = x.CardBrandId,
                                                  AccountNumber = x.AccountNumber,
                                                  ReferenceNumber = x.ReferenceNumber,
                                                  UserAccountId = x.AccountId,
                                                  UserAccountKey = x.Account.Guid,
                                                  UserDisplayName = x.Account.DisplayName,
                                                  UserMobileNumber = x.Account.MobileNumber,
                                                  ParentId = x.ParentId,
                                                  ParentDisplayName = x.Parent.DisplayName,
                                                  StoreReferenceId = x.SubParentId,
                                                  StoreDisplayName = x.SubParent.DisplayName,
                                                  ProviderId = x.ProviderId,
                                                  AcquirerId = x.BankId,
                                                  CreatedById = x.CreatedById,
                                                  CashierId = x.CashierId,
                                                  TerminalId = x.Terminal.IdentificationNumber,
                                                  TerminalReferenceId = x.TerminalId,
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name,
                                              })
                                             .Where(_Request.SearchCondition)
                                             .Sum(x => x.UserAmount);

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardTransactionOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the reward claim transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRewardClaimTransaction(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorRewardClaimTransactionDownloader");
                    var _ActorNotify = _Actor.ActorOf<ActorRewardClaimTransactionDownloader>("ActorRewardClaimTransactionDownloader");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Debit
                                                && x.SourceId == TransactionSource.ThankUCashPlus)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ClaimAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    _Sales = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Debit
                                                && x.SourceId == TransactionSource.ThankUCashPlus)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ClaimAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                    #endregion
                    #region Send Response
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardClaimTransaction", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the reward claim transaction download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetRewardClaimTransactionDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _RewardClaimDownload = new List<OTransaction.RewardClaimDownload>();
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                long StorageId = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                    _HCUAccountParameter.Name = "RewardClaim_Sheet";
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Inactive;
                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    _HCoreContext.SaveChanges();
                    StorageId = _HCUAccountParameter.Id;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                            && x.ParentId == _Request.AccountId
                                            && x.ModeId == TransactionMode.Debit
                                            && x.SourceId == TransactionSource.ThankUCashPlus)
                                            .Select(x => new OTransaction.Sale
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                InvoiceAmount = x.PurchaseAmount,
                                                RewardAmount = x.ReferenceAmount,
                                                TransactionDate = x.TransactionDate,
                                                TypeId = x.TypeId,
                                                ClaimAmount = x.TotalAmount,
                                                ReferenceNumber = x.ReferenceNumber,
                                                UserAccountId = x.AccountId,
                                                UserAccountKey = x.Account.Guid,
                                                UserDisplayName = x.Account.DisplayName,
                                                UserMobileNumber = x.Account.MobileNumber,
                                                StoreReferenceId = x.SubParentId,
                                                ProviderId = x.ProviderId,
                                                AcquirerId = x.BankId,
                                                CreatedById = x.CreatedById,
                                                CashierId = x.CashierId,
                                                StatusId = x.StatusId,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,
                                                TerminalId = x.Terminal.IdentificationNumber,
                                            })
                                            .Count(_Request.SearchCondition);
                    #endregion
                    #region Get Data
                    double Iterations = Math.Round((double)_Request.TotalRecords / 1000, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        _Sales.AddRange(_HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Debit
                                                && x.SourceId == TransactionSource.ThankUCashPlus)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ClaimAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(1000)
                                                     .ToList());
                        _Request.Offset += 1000;
                    }
                    #endregion
                    _Sales = FormatTransactions(_Sales);
                    foreach (var SalesItem in _Sales)
                    {
                        _RewardClaimDownload.Add(new OTransaction.RewardClaimDownload
                        {
                            ReferenceId = SalesItem.ReferenceId,
                            TransactionDate = HCoreHelper.GetGMTToNigeria(SalesItem.TransactionDate),
                            User = SalesItem.UserDisplayName,
                            MobileNumber = SalesItem.UserMobileNumber,
                            TypeName = SalesItem.TypeName,
                            InvoiceAmount = SalesItem.InvoiceAmount,
                            ClaimAmount = SalesItem.ClaimAmount,
                            ReferenceNumber = SalesItem.ReferenceNumber,
                            CardNumber = SalesItem.AccountNumber,
                            CardBrand = SalesItem.CardBrandName,
                            CardBank = SalesItem.CardBankName,

                            Store = SalesItem.StoreDisplayName,
                            CashierId = SalesItem.CashierCode,
                            CashierDisplayName = SalesItem.CashierDisplayName,
                            Bank = SalesItem.AcquirerDisplayName,
                            Provider = SalesItem.ProviderDisplayName,
                            Issuer = SalesItem.CreatedByDisplayName,
                            TerminalId = SalesItem.TerminalId,
                            Status = SalesItem.StatusName,
                        });
                    }
                    using (var _XLWorkbook = new XLWorkbook())
                    {
                        var _WorkSheet = _XLWorkbook.Worksheets.Add("RewardClaim_Sheet");
                        PropertyInfo[] properties = _RewardClaimDownload.First().GetType().GetProperties();
                        List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                        for (int i = 0; i < headerNames.Count; i++)
                        {
                            _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                        }
                        _WorkSheet.Cell(2, 1).InsertData(_RewardClaimDownload);
                        MemoryStream _MemoryStream = new MemoryStream();
                        _XLWorkbook.SaveAs(_MemoryStream);
                        long? _StorageId = HCoreHelper.SaveStorage("RewardClaim_Sheet", "xlsx", _MemoryStream, _Request.UserReference);
                        if (_StorageId != null && _StorageId != 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                                if (TItem != null)
                                {
                                    TItem.IconStorageId = _StorageId;
                                    TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    TItem.ModifyById = _Request.UserReference.AccountId;
                                    TItem.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                            //using (_HCoreContext = new HCoreContext())
                            //{
                            //    _HCUAccountParameter = new HCUAccountParameter();
                            //    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                            //    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                            //    _HCUAccountParameter.IconStorageId = _StorageId;
                            //    _HCUAccountParameter.Name = "RewardClaim_Sheet";
                            //    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                            //    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                            //    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                            //    _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                            //    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                            //    _HCoreContext.SaveChanges();
                            //}
                        }
                    }
                    #region Send Response
                    _HCoreContext.Dispose();
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardClaimTransactionDownload", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the reward claim transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRewardClaimTransactionOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                using (_HCoreContext = new HCoreContext())
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    _OTransactionOverview = new OTransactionOverview();
                    _OTransactionOverview.Customers = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Debit
                                                && x.SourceId == TransactionSource.ThankUCashPlus)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ClaimAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Select(x => x.UserAccountId)
                                                .Distinct()
                                                .Count();

                    _OTransactionOverview.Transactions = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Debit
                                                && x.SourceId == TransactionSource.ThankUCashPlus)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ClaimAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();

                    _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Debit
                                                && x.SourceId == TransactionSource.ThankUCashPlus)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ClaimAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);


                    _OTransactionOverview.RewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Debit
                                                && x.SourceId == TransactionSource.ThankUCashPlus)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ClaimAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                              .Where(_Request.SearchCondition)
                                              .Sum(x => x.ClaimAmount);


                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardClaimTransactionOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the redeem transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRedeemTransaction(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorRedeemTransactionDownloader");
                    var _ActorNotify = _Actor.ActorOf<ActorRedeemTransactionDownloader>("ActorRedeemTransactionDownloader");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RedeemAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    _Sales = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RedeemAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                    #endregion
                    #region Send Response
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRedeemTransaction", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the redeem transaction download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetRedeemTransactionDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _RedeemDownload = new List<OTransaction.RedeemDownload>();
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                long StorageId = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                    _HCUAccountParameter.Name = "Redeem_Sheet";
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Inactive;
                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    _HCoreContext.SaveChanges();
                    StorageId = _HCUAccountParameter.Id;
                }
                using (_HCoreContext = new HCoreContext())
                {

                    #region Total Records
                    _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                            && x.ParentId == _Request.AccountId
                                            && x.ModeId == TransactionMode.Debit
                                            && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                            && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                            .Select(x => new OTransaction.Sale
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                InvoiceAmount = x.PurchaseAmount,
                                                TransactionDate = x.TransactionDate,
                                                TypeId = x.TypeId,
                                                RedeemAmount = x.TotalAmount,
                                                ReferenceNumber = x.ReferenceNumber,
                                                UserAccountId = x.AccountId,
                                                UserAccountKey = x.Account.Guid,
                                                UserDisplayName = x.Account.DisplayName,
                                                UserMobileNumber = x.Account.MobileNumber,
                                                ParentId = x.ParentId,
                                                StoreReferenceId = x.SubParentId,
                                                ProviderId = x.ProviderId,
                                                AcquirerId = x.BankId,
                                                CreatedById = x.CreatedById,
                                                CashierId = x.CashierId,
                                                StatusId = x.StatusId,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,
                                                TerminalId = x.Terminal.IdentificationNumber,
                                            })
                                            .Count(_Request.SearchCondition);
                    #endregion
                    #region Get Data
                    double Iterations = Math.Round((double)_Request.TotalRecords / 1000, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        _Sales.AddRange(_HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RedeemAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(1000)
                                                     .ToList());
                        _Request.Offset += 1000;
                    }
                    #endregion
                    _Sales = FormatTransactions(_Sales);
                    foreach (var SalesItem in _Sales)
                    {
                        _RedeemDownload.Add(new OTransaction.RedeemDownload
                        {
                            ReferenceId = SalesItem.ReferenceId,
                            TransactionDate = HCoreHelper.GetGMTToNigeria(SalesItem.TransactionDate),
                            User = SalesItem.UserDisplayName,
                            MobileNumber = SalesItem.UserMobileNumber,
                            TypeName = SalesItem.TypeName,
                            InvoiceAmount = SalesItem.InvoiceAmount,
                            RedeemAmount = SalesItem.RedeemAmount,
                            ReferenceNumber = SalesItem.ReferenceNumber,
                            CardNumber = SalesItem.AccountNumber,
                            CardBrand = SalesItem.CardBrandName,
                            CardBank = SalesItem.CardBankName,

                            Store = SalesItem.StoreDisplayName,
                            CashierId = SalesItem.CashierCode,
                            CashierDisplayName = SalesItem.CashierDisplayName,
                            Bank = SalesItem.AcquirerDisplayName,
                            Provider = SalesItem.ProviderDisplayName,
                            Issuer = SalesItem.CreatedByDisplayName,
                            TerminalId = SalesItem.TerminalId,
                            Status = SalesItem.StatusName,
                        });
                    }
                    using (var _XLWorkbook = new XLWorkbook())
                    {
                        var _WorkSheet = _XLWorkbook.Worksheets.Add("Redeem_Sheet");
                        PropertyInfo[] properties = _RedeemDownload.First().GetType().GetProperties();
                        List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                        for (int i = 0; i < headerNames.Count; i++)
                        {
                            _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                        }
                        _WorkSheet.Cell(2, 1).InsertData(_RedeemDownload);
                        MemoryStream _MemoryStream = new MemoryStream();
                        _XLWorkbook.SaveAs(_MemoryStream);
                        long? _StorageId = HCoreHelper.SaveStorage("Redeem_Sheet", "xlsx", _MemoryStream, _Request.UserReference);
                        if (_StorageId != null && _StorageId != 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                                if (TItem != null)
                                {
                                    TItem.IconStorageId = _StorageId;
                                    TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    TItem.ModifyById = _Request.UserReference.AccountId;
                                    TItem.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }
                    #region Send Response
                    _HCoreContext.Dispose();
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRedeemTransactionDownload", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the redeem transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRedeemTransactionOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    _OTransactionOverview = new OTransactionOverview();
                    //_OTransactionOverview.Customers = _HCoreContext.HCUAccountTransaction
                    //                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                    //                            && x.ParentId == _Request.AccountId
                    //                            && x.ModeId == TransactionMode.Debit
                    //                            && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints))
                    //                            .Select(x => new OTransaction.Sale
                    //                            {
                    //                                ReferenceId = x.Id,
                    //                                ReferenceKey = x.Guid,
                    //                                InvoiceAmount = x.PurchaseAmount,
                    //                                TransactionDate = x.TransactionDate,
                    //                                TypeId = x.TypeId,
                    //                                RedeemAmount = x.TotalAmount,
                    //                                ReferenceNumber = x.ReferenceNumber,
                    //                                UserAccountId = x.AccountId,
                    //                                UserAccountKey = x.Account.Guid,
                    //                                UserDisplayName = x.Account.DisplayName,
                    //                                UserMobileNumber = x.Account.MobileNumber,
                    //                                StoreReferenceId = x.SubParentId,  
                    //                                ProviderId = x.ProviderId,
                    //                                AcquirerId = x.BankId,
                    //                                CreatedById = x.CreatedById,
                    //                                CashierId = x.CashierId,
                    //                                StatusId = x.StatusId,
                    //                                StatusCode = x.Status.SystemName,
                    //                            })
                    //                            .Where(_Request.SearchCondition)
                    //                            .Select(x => x.AccountId)
                    //                            .Distinct()
                    //                            .Count();

                    _OTransactionOverview.Transactions = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RedeemAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();

                    _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RedeemAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,


                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);


                    _OTransactionOverview.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                              .Select(x => new OTransaction.Sale
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,
                                                  //x.ProgramId if not null it is from third part
                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate,
                                                  TypeId = x.TypeId,
                                                  RedeemAmount = x.TotalAmount,
                                                  ReferenceNumber = x.ReferenceNumber,
                                                  UserAccountId = x.AccountId,
                                                  UserAccountKey = x.Account.Guid,
                                                  UserDisplayName = x.Account.DisplayName,
                                                  UserMobileNumber = x.Account.MobileNumber,
                                                  ParentId = x.ParentId,
                                                  StoreReferenceId = x.SubParentId,
                                                  ProviderId = x.ProviderId,
                                                  AcquirerId = x.BankId,
                                                  CreatedById = x.CreatedById,
                                                  CashierId = x.CashierId,
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name,
                                                  TerminalId = x.Terminal.IdentificationNumber,
                                              })
                                              .Where(_Request.SearchCondition)
                                              .Sum(x => x.RedeemAmount);
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRedeemTransactionOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the campaign transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCampaignTransaction(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Credit
                                                && x.ReferenceAmount > 0
                                                && x.CampaignId != null
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && x.SourceId == TransactionSource.TUC)
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,

                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    _Sales = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId != null
                                                && x.ReferenceAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && x.SourceId == TransactionSource.TUC)
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,

                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                    #endregion
                    #region Send Response
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCampaignTransaction", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the campaign transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCampaignTransactionOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    _OTransactionOverview = new OTransactionOverview();
                    _OTransactionOverview.Customers = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId != null
                                                && x.ReferenceAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && x.SourceId == TransactionSource.TUC)
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,

                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Select(x => x.UserAccountId)
                                                .Distinct()
                                                .Count();

                    _OTransactionOverview.Transactions = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.CampaignId != null
                                                && x.ModeId == TransactionMode.Credit
                                                && x.ReferenceAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && x.SourceId == TransactionSource.TUC)
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,

                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();

                    _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                                && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId != null
                                                && x.ReferenceAmount > 0
                                                && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                    && x.SourceId == TransactionSource.TUC)
                                                || x.SourceId == TransactionSource.ThankUCashPlus))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.TotalAmount,
                                                    CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,

                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);


                    _OTransactionOverview.RewardAmount = _HCoreContext.HCUAccountTransaction
                                              .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.AccountId
                                              && x.ModeId == TransactionMode.Credit
                                                && x.CampaignId != null
                                              && x.ReferenceAmount > 0
                                              && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                  && x.SourceId == TransactionSource.TUC)
                                              || x.SourceId == TransactionSource.ThankUCashPlus))
                                              .Select(x => new OTransaction.Sale
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,
                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate,
                                                  TypeId = x.TypeId,
                                                  RewardAmount = x.ReferenceAmount,
                                                  UserAmount = x.TotalAmount,
                                                  CommissionAmount = (x.ReferenceAmount - x.TotalAmount),
                                                  CardBankId = x.CardBankId,
                                                  CardBrandId = x.CardBrandId,
                                                  AccountNumber = x.AccountNumber,
                                                  ReferenceNumber = x.ReferenceNumber,
                                                  UserAccountId = x.AccountId,
                                                  UserAccountKey = x.Account.Guid,
                                                  UserDisplayName = x.Account.DisplayName,
                                                  UserMobileNumber = x.Account.MobileNumber,

                                                  StoreReferenceId = x.SubParentId,
                                                  ProviderId = x.ProviderId,
                                                  AcquirerId = x.BankId,
                                                  CreatedById = x.CreatedById,
                                                  CashierId = x.CashierId,
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name,
                                                  TerminalId = x.Terminal.IdentificationNumber,
                                              })
                                              .Where(_Request.SearchCondition)
                                              .Sum(x => x.RewardAmount);


                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCampaignTransactionOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the transactions.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTransactions(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _AllTransactions = new List<OTransaction.All>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _Request.TotalRecords = (from x in _HCoreContext.HCUAccountTransaction
                                                                         where x.ParentId == _Request.AccountId
                                                                         select new OTransaction.All
                                                                         {
                                                                             ReferenceId = x.Id,
                                                                             ReferenceKey = x.Guid,

                                                                             ParentId = x.ParentId,
                                                                             ParentKey = x.Parent.Guid,
                                                                             ParentDisplayName = x.Parent.DisplayName,

                                                                             UserAccountId = x.AccountId,
                                                                             UserAccountKey = x.Account.Guid,
                                                                             UserAccountDisplayName = x.Account.DisplayName,
                                                                             UserAccountMobileNumber = x.Account.MobileNumber,

                                                                             SubParentDisplayName = x.SubParent.DisplayName,
                                                                             SubParentKey = x.SubParent.Guid,
                                                                             OwnerId = x.Account.OwnerId,

                                                                             TypeId = x.TypeId,
                                                                             TypeKey = x.Type.SystemName,
                                                                             TypeName = x.Type.Name,
                                                                             TypeCategory = x.Type.Parent.SystemName,

                                                                             ModeId = x.ModeId,
                                                                             ModeKey = x.Mode.SystemName,
                                                                             ModeName = x.Mode.Name,

                                                                             SourceId = x.SourceId,
                                                                             SourceKey = x.Source.SystemName,
                                                                             SourceName = x.Source.Name,

                                                                             Amount = x.Amount,
                                                                             Charge = x.Charge,
                                                                             CommissionAmount = x.ComissionAmount,

                                                                             TotalAmount = x.TotalAmount,
                                                                             InvoiceAmount = x.PurchaseAmount,
                                                                             ReferenceInvoiceAmount = x.PurchaseAmount,
                                                                             TransactionDate = x.TransactionDate,

                                                                             ReferenceNumber = x.ReferenceNumber,
                                                                             GroupKey = x.GroupKey,

                                                                             CreatedById = x.CreatedById,
                                                                             CreatedByKey = x.CreatedBy.Guid,
                                                                             CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                                             CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                                                                             ParentTransactionAmount = x.ParentTransaction.Amount,
                                                                             ParentTransactionChargeAmount = x.ParentTransaction.Charge,
                                                                             ParentTransactionCommissionAmount = x.ParentTransaction.ComissionAmount,
                                                                             ParentTransactionTotalAmount = x.ParentTransaction.TotalAmount,

                                                                             StatusId = x.StatusId,
                                                                             StatusCode = x.Status.SystemName,
                                                                             StatusName = x.Status.Name,

                                                                             CashierId = x.CashierId,
                                                                             CashierKey = x.Cashier.Guid,
                                                                             CashierDisplayName = x.Cashier.Name,
                                                                             CashierCode = x.Cashier.DisplayName,
                                                                             CardBankId = x.CardBankId,
                                                                             CardBrandId = x.CardBrandId,
                                                                             SubParentId = x.SubParentId,
                                                                             ProviderId = x.ProviderId,
                                                                             AcquirerId = x.BankId,
                                                                             AccountNumber = x.AccountNumber,
                                                                             TerminalId = x.Terminal.IdentificationNumber,
                                                                             TerminalReferenceId = x.Terminal.Id,
                                                                         })

                                         .Where(_Request.SearchCondition)
                                 .Count();
                    }
                    #endregion
                    #region Get Data
                    _AllTransactions = (from x in _HCoreContext.HCUAccountTransaction
                                        where x.ParentId == _Request.AccountId
                                        select new OTransaction.All
                                        {
                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,

                                            ParentId = x.ParentId,
                                            ParentKey = x.Parent.Guid,
                                            ParentDisplayName = x.Parent.DisplayName,

                                            SubParentId = x.SubParentId,
                                            ProviderId = x.ProviderId,
                                            AcquirerId = x.BankId,

                                            UserAccountId = x.AccountId,
                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,
                                            UserAccountMobileNumber = x.Account.MobileNumber,

                                            SubParentDisplayName = x.SubParent.DisplayName,
                                            SubParentKey = x.SubParent.Guid,
                                            OwnerId = x.Account.OwnerId,

                                            TypeId = x.TypeId,
                                            TypeKey = x.Type.SystemName,
                                            TypeName = x.Type.Name,
                                            TypeCategory = x.Type.Parent.SystemName,

                                            ModeId = x.ModeId,
                                            ModeKey = x.Mode.SystemName,
                                            ModeName = x.Mode.Name,

                                            SourceId = x.SourceId,
                                            SourceKey = x.Source.SystemName,
                                            SourceName = x.Source.Name,

                                            Amount = x.Amount,
                                            Charge = x.Charge,
                                            CommissionAmount = x.ComissionAmount,

                                            TotalAmount = x.TotalAmount,
                                            InvoiceAmount = x.PurchaseAmount,
                                            ReferenceInvoiceAmount = x.PurchaseAmount,
                                            TransactionDate = x.TransactionDate,
                                            AccountNumber = x.AccountNumber,
                                            ReferenceNumber = x.ReferenceNumber,
                                            GroupKey = x.GroupKey,

                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                                            CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,

                                            ParentTransactionAmount = x.ParentTransaction.Amount,
                                            ParentTransactionChargeAmount = x.ParentTransaction.Charge,
                                            ParentTransactionCommissionAmount = x.ParentTransaction.ComissionAmount,
                                            ParentTransactionTotalAmount = x.ParentTransaction.TotalAmount,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name,

                                            CashierId = x.CashierId,
                                            CashierKey = x.Cashier.Guid,
                                            CashierDisplayName = x.Cashier.Name,
                                            CashierCode = x.Cashier.DisplayName,
                                            CardBankId = x.CardBankId,
                                            CardBrandId = x.CardBrandId,
                                            TerminalId = x.Terminal.IdentificationNumber,
                                            TerminalReferenceId = x.Terminal.Id,
                                        })

                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _AllTransactions)
                    {
                        DataItem.TypeName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.TypeId).Select(q => q.Name).FirstOrDefault();
                        DataItem.CardBankName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBankId).Select(q => q.Name).FirstOrDefault();
                        DataItem.CardBrandName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBrandId).Select(q => q.Name).FirstOrDefault();
                        DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.StatusId).Select(q => q.Name).FirstOrDefault();
                        var ParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ParentId);
                        if (ParentDetails != null)
                        {
                            DataItem.ParentKey = ParentDetails.ReferenceKey;
                            DataItem.ParentDisplayName = ParentDetails.DisplayName;
                            DataItem.ParentIconUrl = ParentDetails.IconUrl;
                        }
                        var SubParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.SubParentId);
                        if (SubParentDetails != null)
                        {
                            DataItem.SubParentKey = SubParentDetails.ReferenceKey;
                            DataItem.SubParentDisplayName = SubParentDetails.DisplayName;
                        }
                        var AcquirerDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.AcquirerId);
                        if (AcquirerDetails != null)
                        {
                            DataItem.AcquirerKey = AcquirerDetails.ReferenceKey;
                            DataItem.AcquirerDisplayName = AcquirerDetails.DisplayName;
                            //DataItem.AcquirerIconUrl = AcquirerDetails.IconUrl;
                        }
                        var ProviderDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ProviderId);
                        if (ProviderDetails != null)
                        {
                            DataItem.ProviderKey = ProviderDetails.ReferenceKey;
                            DataItem.ProviderDisplayName = ProviderDetails.DisplayName;
                            //DataItem.ProviderIconUrl = AcquirerDetails.IconUrl;
                        }
                        var CashierDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CashierId);
                        if (CashierDetails != null)
                        {
                            DataItem.CashierKey = CashierDetails.ReferenceKey;
                            DataItem.CashierCode = CashierDetails.DisplayName;
                            DataItem.CashierDisplayName = CashierDetails.Name;
                        }

                        var CreatedByDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CreatedById);
                        if (CreatedByDetails != null)
                        {
                            DataItem.CreatedByKey = CreatedByDetails.ReferenceKey;
                            DataItem.CreatedByDisplayName = CreatedByDetails.DisplayName;
                            DataItem.CreatedByAccountTypeCode = CreatedByDetails.AccountTypeCode;
                        }
                        if (!string.IsNullOrEmpty(DataItem.TypeName))
                        {
                            DataItem.TypeName = DataItem.TypeName.Replace("Reward", "").Replace("Rewards", "");
                        }
                    }

                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _AllTransactions, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetTransactions", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the pending reward.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPendingReward(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _AllTransactions = new List<OTransaction.All>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.TUCLoyaltyPending
                                                 where x.FromAccountId == _Request.AccountId && x.StatusId == HelperStatus.Transaction.Pending
                                                 select new OTransaction.All
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     UserAccountId = x.ToAccountId,
                                                     UserAccountKey = x.ToAccount.Guid,
                                                     UserAccountDisplayName = x.ToAccount.DisplayName,
                                                     UserAccountMobileNumber = x.ToAccount.MobileNumber,
                                                     TypeId = x.TypeId,
                                                     TypeKey = x.Type.SystemName,
                                                     TypeName = x.Type.Name,
                                                     TypeCategory = x.Type.Parent.SystemName,

                                                     ModeId = x.ModeId,
                                                     ModeKey = x.Mode.SystemName,
                                                     ModeName = x.Mode.Name,

                                                     SourceId = x.SourceId,
                                                     SourceKey = x.Source.SystemName,
                                                     SourceName = x.Source.Name,

                                                     Amount = x.Amount,
                                                     CommissionAmount = x.CommissionAmount,
                                                     TotalAmount = x.TotalAmount,
                                                     InvoiceAmount = x.InvoiceAmount,
                                                     TransactionDate = x.TransactionDate,
                                                     CreatedById = x.CreatedById,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,

                                                     CashierId = x.CashierId,
                                                     CashierKey = x.Cashier.Guid,
                                                     CashierDisplayName = x.Cashier.Name,
                                                     CashierCode = x.Cashier.DisplayName,
                                                     AccountNumber = x.AccountNumber,
                                                 })
                                         .Where(_Request.SearchCondition)
                                 .Count();
                    }
                    #endregion
                    #region Get Data
                    _AllTransactions = (from x in _HCoreContext.TUCLoyaltyPending
                                        where x.FromAccountId == _Request.AccountId && x.StatusId == HelperStatus.Transaction.Pending
                                        select new OTransaction.All
                                        {
                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,
                                            UserAccountId = x.ToAccountId,
                                            UserAccountKey = x.ToAccount.Guid,
                                            UserAccountDisplayName = x.ToAccount.DisplayName,
                                            UserAccountMobileNumber = x.ToAccount.MobileNumber,
                                            TypeId = x.TypeId,
                                            TypeKey = x.Type.SystemName,
                                            TypeName = x.Type.Name,
                                            TypeCategory = x.Type.Parent.SystemName,

                                            ModeId = x.ModeId,
                                            ModeKey = x.Mode.SystemName,
                                            ModeName = x.Mode.Name,

                                            SourceId = x.SourceId,
                                            SourceKey = x.Source.SystemName,
                                            SourceName = x.Source.Name,

                                            Amount = x.Amount,
                                            CommissionAmount = x.CommissionAmount,
                                            TotalAmount = x.TotalAmount,
                                            InvoiceAmount = x.InvoiceAmount,
                                            TransactionDate = x.TransactionDate,
                                            CreatedById = x.CreatedById,
                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                                            CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name,

                                            CashierId = x.CashierId,
                                            CashierKey = x.Cashier.Guid,
                                            CashierDisplayName = x.Cashier.Name,
                                            CashierCode = x.Cashier.DisplayName,
                                            AccountNumber = x.AccountNumber,
                                        })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _AllTransactions)
                    {
                        DataItem.TypeName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.TypeId).Select(q => q.Name).FirstOrDefault();
                        DataItem.CardBankName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBankId).Select(q => q.Name).FirstOrDefault();
                        DataItem.CardBrandName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBrandId).Select(q => q.Name).FirstOrDefault();
                        DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.StatusId).Select(q => q.Name).FirstOrDefault();
                        var ParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ParentId);
                        if (ParentDetails != null)
                        {
                            DataItem.ParentKey = ParentDetails.ReferenceKey;
                            DataItem.ParentDisplayName = ParentDetails.DisplayName;
                            DataItem.ParentIconUrl = ParentDetails.IconUrl;
                        }
                        var SubParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.SubParentId);
                        if (SubParentDetails != null)
                        {
                            DataItem.SubParentKey = SubParentDetails.ReferenceKey;
                            DataItem.SubParentDisplayName = SubParentDetails.DisplayName;
                        }
                        var AcquirerDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.AcquirerId);
                        if (AcquirerDetails != null)
                        {
                            DataItem.AcquirerKey = AcquirerDetails.ReferenceKey;
                            DataItem.AcquirerDisplayName = AcquirerDetails.DisplayName;
                            //DataItem.AcquirerIconUrl = AcquirerDetails.IconUrl;
                        }
                        var ProviderDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ProviderId);
                        if (ProviderDetails != null)
                        {
                            DataItem.ProviderKey = ProviderDetails.ReferenceKey;
                            DataItem.ProviderDisplayName = ProviderDetails.DisplayName;
                            //DataItem.ProviderIconUrl = AcquirerDetails.IconUrl;
                        }
                        var CashierDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CashierId);
                        if (CashierDetails != null)
                        {
                            DataItem.CashierKey = CashierDetails.ReferenceKey;
                            DataItem.CashierCode = CashierDetails.DisplayName;
                            DataItem.CashierDisplayName = CashierDetails.Name;
                        }

                        var CreatedByDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CreatedById);
                        if (CreatedByDetails != null)
                        {
                            DataItem.CreatedByKey = CreatedByDetails.ReferenceKey;
                            DataItem.CreatedByDisplayName = CreatedByDetails.DisplayName;
                            DataItem.CreatedByAccountTypeCode = CreatedByDetails.AccountTypeCode;
                        }
                        if (!string.IsNullOrEmpty(DataItem.TypeName))
                        {
                            DataItem.TypeName = DataItem.TypeName.Replace("Reward", "").Replace("Rewards", "");
                        }
                    }

                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _AllTransactions, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetTransactions", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        //internal OResponse GetAcquirerMerchantSales(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        List<OAccount.Account> Data = new List<OAccount.Account>();
        //        if (string.IsNullOrEmpty(_Request.SearchCondition))
        //        {
        //            HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
        //        }
        //        if (string.IsNullOrEmpty(_Request.SortExpression))
        //        {
        //            HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
        //        }
        //        if (_Request.Limit < 1)
        //        {
        //            _Request.Limit = HCoreConstant._AppConfig.DefaultRecordsLimit;
        //        }
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            long UserAccountId = 0;
        //            if (!string.IsNullOrEmpty(_Request.ReferenceKey))
        //            {
        //                UserAccountId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.ReferenceKey).Select(x => x.Id).FirstOrDefault();
        //            }
        //            switch (_Request.Type)
        //            {

        //                case ThankUCashConstant.ListType.SubOwner:
        //                    #region Total Records
        //                    if (_Request.RefreshCount)
        //                    {
        //                        _Request.TotalRecords = (from x in _HCoreContext.HCUAccount
        //                                                 where x.AccountTypeId == UserAccountType.Merchant
        //                                                 && x.HCUAccountOwnerOwner.Any(m => m.Account.AccountTypeId == UserAccountType.TerminalAccount && m.Account.BankId == UserAccountId)
        //                                                 select new OAccount.Account
        //                                                 {
        //                                                     ReferenceId = x.Id,
        //                                                     ReferenceKey = x.Guid,
        //                                                     DisplayName = x.DisplayName,
        //                                                     IconUrl = x.IconStorage.Path,
        //                                                     ContactNumber = x.ContactNumber,
        //                                                     EmailAddress = x.EmailAddress,
        //                                                     StatusId = x.StatusId,
        //                                                     CreateDate = x.CreateDate,
        //                                                     TotalTransaction = _HCoreContext.HCUAccountTransaction
        //                                                                 .Count(m => m.ParentId == x.Id
        //                                                                  && m.CreatedBy.BankId == UserAccountId
        //                                                                  && m.StatusId == HelperStatus.Transaction.Success
        //                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
        //                                                                         || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))),
        //                                                     TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
        //                                                                 .Where(m => m.ParentId == x.Id
        //                                                                  && m.CreatedBy.BankId == UserAccountId
        //                                                                  && m.StatusId == HelperStatus.Transaction.Success
        //                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
        //                                                                         || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))).Sum(m => m.PurchaseAmount),

        //                                                 })
        //                                 .Where(_Request.SearchCondition)
        //                                .Count();
        //                    }
        //                    #endregion
        //                    #region Get Data
        //                    Data = (from x in _HCoreContext.HCUAccount
        //                            where x.AccountTypeId == UserAccountType.Merchant
        //                            && x.HCUAccountOwnerOwner.Any(m => m.Account.AccountTypeId == UserAccountType.TerminalAccount && m.Account.BankId == UserAccountId)
        //                            select new OAccount.Account
        //                            {
        //                                ReferenceId = x.Id,
        //                                ReferenceKey = x.Guid,
        //                                DisplayName = x.DisplayName,
        //                                IconUrl = x.IconStorage.Path,
        //                                ContactNumber = x.ContactNumber,
        //                                EmailAddress = x.EmailAddress,
        //                                StatusId = x.StatusId,
        //                                CreateDate = x.CreateDate,
        //                                TotalTransaction = _HCoreContext.HCUAccountTransaction
        //                                                                 .Count(m => m.ParentId == x.Id
        //                                                                  && m.CreatedBy.BankId == UserAccountId
        //                                                                  && m.StatusId == HelperStatus.Transaction.Success
        //                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
        //                                                                         || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))),
        //                                TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
        //                                                                 .Where(m => m.ParentId == x.Id
        //                                                                  && m.CreatedBy.BankId == UserAccountId
        //                                                                  && m.StatusId == HelperStatus.Transaction.Success
        //                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
        //                                                                         || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))).Sum(m => m.PurchaseAmount),


        //                            })
        //                                             .Where(_Request.SearchCondition)
        //                                             .OrderBy(_Request.SortExpression)
        //                                             .Skip(_Request.Offset)
        //                                             .Take(_Request.Limit)
        //                                             .ToList();
        //                    #endregion
        //                    break;
        //                default:
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
        //            }
        //            #region Create  Response Object
        //            _HCoreContext.Dispose();
        //            foreach (var DataItem in Data)
        //            {
        //                if (!string.IsNullOrEmpty(DataItem.IconUrl))
        //                {
        //                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
        //                }
        //                else
        //                {
        //                    DataItem.IconUrl = _AppConfig.Default_Icon;
        //                }
        //            }
        //            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
        //            #endregion
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //            #endregion
        //        }

        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException("GetAcquirerMerchantSales", _Exception, _Request.UserReference);
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
        //    }
        //    #endregion
        //}
        /// <summary>
        /// Description: Updates the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateTransaction(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                //if (_Request.StatusCode != "transaction.success" ||  _Request.StatusCode != "transaction.rejected")
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1363, TUCCoreResource.CA1363M);
                //}
                if (string.IsNullOrEmpty(_Request.Comment) && _Request.StatusCode == "transaction.rejected")
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1364, TUCCoreResource.CA1364M);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var TransactionDetails = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.Guid == _Request.ReferenceKey
                        && x.Id == _Request.ReferenceId
                        && x.ParentId == _Request.AccountId
                        && x.Parent.Guid == _Request.AccountKey
                        ).FirstOrDefault();
                    if (TransactionDetails != null)
                    {

                        var TransactionGroup = _HCoreContext.HCUAccountTransaction.Where(x => x.GroupKey == TransactionDetails.GroupKey).ToList();
                        foreach (var item in TransactionGroup)
                        {
                            if (_Request.StatusCode == "transaction.success")
                            {
                                item.StatusId = HelperStatus.Transaction.Success;
                            }
                            if (_Request.StatusCode == "transaction.rejected")
                            {
                                item.StatusId = HelperStatus.Transaction.Rejected;
                            }
                            item.Comment = _Request.Comment;
                            item.ModifyById = _Request.UserReference.AccountId;
                            item.ModifyDate = HCoreHelper.GetGMTDateTime();
                        }
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1075, TUCCoreResource.CA1075M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateMerchant", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Author: Ugochukwu Oluo
        /// Descirption: this method gets all transaction from a third party
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetAllRedeemTransaction(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    _OTransactionOverview = new OTransactionOverview();

                    _OTransactionOverview.RedeemAmountByThirdParty = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == _Request.ReferenceId
                                                && x.ModeId == TransactionMode.Debit
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                && x.Type.SubParentId == TransactionTypeCategory.Redeem
                                                && x.ProgramId != null
                                                )
                                              .Select(x => new OTransaction.Sale
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,
                                                  InvoiceAmount = x.PurchaseAmount,
                                                  TransactionDate = x.TransactionDate,
                                                  TypeId = x.TypeId,
                                                  RedeemAmount = x.TotalAmount,
                                                  ReferenceNumber = x.ReferenceNumber,
                                                  UserAccountId = x.AccountId,
                                                  UserAccountKey = x.Account.Guid,
                                                  UserDisplayName = x.Account.DisplayName,
                                                  UserMobileNumber = x.Account.MobileNumber,
                                                  ParentId = x.ParentId,
                                                  StoreReferenceId = x.SubParentId,
                                                  ProviderId = x.ProviderId,
                                                  AcquirerId = x.BankId,
                                                  CreatedById = x.CreatedById,
                                                  CashierId = x.CashierId,
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name,
                                                  TerminalId = x.Terminal.IdentificationNumber,
                                              })
                                              .Where(_Request.SearchCondition)
                                              .Sum(x => x.RedeemAmount);

                    _OTransactionOverview.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                               .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                               && x.ParentId == _Request.ReferenceId
                                               && x.ModeId == TransactionMode.Debit
                                               && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                               && x.Type.SubParentId == TransactionTypeCategory.Redeem
                                               && x.ProgramId == null
                                               && x.SubParentId != null
                                               )
                                             .Select(x => new OTransaction.Sale
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,
                                                 InvoiceAmount = x.PurchaseAmount,
                                                 TransactionDate = x.TransactionDate,
                                                 TypeId = x.TypeId,
                                                 RedeemAmount = x.TotalAmount,
                                                 ReferenceNumber = x.ReferenceNumber,
                                                 UserAccountId = x.AccountId,
                                                 UserAccountKey = x.Account.Guid,
                                                 UserDisplayName = x.Account.DisplayName,
                                                 UserMobileNumber = x.Account.MobileNumber,
                                                 ParentId = x.ParentId,
                                                 StoreReferenceId = x.SubParentId,
                                                 ProviderId = x.ProviderId,
                                                 AcquirerId = x.BankId,
                                                 CreatedById = x.CreatedById,
                                                 CashierId = x.CashierId,
                                                 StatusId = x.StatusId,
                                                 StatusCode = x.Status.SystemName,
                                                 StatusName = x.Status.Name,
                                                 TerminalId = x.Terminal.IdentificationNumber,
                                             })
                                             .Where(_Request.SearchCondition)
                                             .Sum(x => x.RedeemAmount);
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRedeemTransactionOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

        /// <summary>
        /// Gets the sale transaction list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAcquirerTransaction(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorSaleTransactionDownloader");
                    var _ActorNotify = _Actor.ActorOf<ActorSaleTransactionDownloader>("ActorSaleTransactionDownloader");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus))
                                                //&& (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    CommissionAmount = x.ComissionAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    TransactionDate = x.TransactionDate,

                                                    TypeId = x.TypeId,
                                                    TypeName = x.Type.Name,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,

                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,

                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,

                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,

                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,

                                                    SubParentId = x.SubParentId,
                                                    SubParentDisplayName = x.SubParent.DisplayName,
                                                    SubParentStatusId = x.Account.StatusId,
                                                    SubParentStatusCode = x.Account.Status.SystemName,
                                                    SubParentStatusName = x.Account.Status.Name,

                                                    ProviderId = x.ProviderId,
                                                    ProviderDisplayName = x.Provider.DisplayName,

                                                    AcquirerId = x.BankId,
                                                    AcquirerDisplayName = x.Bank.DisplayName,

                                                    CreatedById = x.CreatedById,

                                                    CashierId = x.CashierId,
                                                    CashierDisplayName = x.Cashier.DisplayName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    StatusName = x.Status.Name,

                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,

                                                    RedeemAmount = x.TotalAmount,
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    _Sales = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.Parent.CountryId == _Request.UserReference.SystemCountry
                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftCards || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus))
                                                //&& (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay || x.TypeId == TransactionType.GiftCard))
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    CommissionAmount = x.ComissionAmount,
                                                    RewardAmount = x.ReferenceAmount,
                                                    UserAmount = x.ReferenceInvoiceAmount,
                                                    TransactionDate = x.TransactionDate,

                                                    TypeId = x.TypeId,
                                                    TypeName = x.Type.Name,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.SystemName,
                                                    SourceName = x.Source.Name,

                                                    CardBankId = x.CardBankId,
                                                    CardBankName = x.CardBank.Name,

                                                    CardBrandId = x.CardBrandId,
                                                    CardBrandName = x.CardBrand.Name,

                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,

                                                    ParentId = x.ParentId,
                                                    ParentDisplayName = x.Parent.DisplayName,

                                                    SubParentId = x.SubParentId,
                                                    SubParentDisplayName = x.SubParent.DisplayName,
                                                    SubParentStatusId = x.Account.StatusId,
                                                    SubParentStatusCode = x.Account.Status.SystemName,
                                                    SubParentStatusName = x.Account.Status.Name,

                                                    ProviderId = x.ProviderId,
                                                    ProviderDisplayName = x.Provider.DisplayName,

                                                    AcquirerId = x.BankId,
                                                    AcquirerDisplayName = x.Bank.DisplayName,

                                                    CreatedById = x.CreatedById,

                                                    CashierId = x.CashierId,
                                                    CashierDisplayName = x.Cashier.DisplayName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    StatusName = x.Status.Name,

                                                    TerminalReferenceId = x.TerminalId,
                                                    TerminalReferenceKey = x.Terminal.Guid,
                                                    TerminalId = x.Terminal.IdentificationNumber,

                                                    RedeemAmount = x.TotalAmount,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                    #endregion
                    foreach (var _Sale in _Sales)
                    {
                        if (_Sale.StatusId == HelperStatus.Transaction.Pending)
                        {
                            _Sale.StatusCode = "transaction.pending";
                            _Sale.StatusId = HelperStatus.Transaction.Success;
                        }
                    }
                    #region Send Response
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAcquirerTransaction", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
    }
}
