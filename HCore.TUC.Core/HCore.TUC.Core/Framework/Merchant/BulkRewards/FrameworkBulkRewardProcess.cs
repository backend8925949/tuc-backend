﻿using System;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Operations;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant;
using System.Transactions;
using Z.EntityFramework.Plus;
using HCore.Object;
using SendGrid;
using SendGrid.Helpers.Mail;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Akka.Actor;
//using HCore.TUC.Core.Framework.Merchant.Upload;
using Delivery.Object.Response.Shipments;

namespace HCore.TUC.Core.Framework.Merchant.BulkRewards
{
    public class ManagaeBulkRewardProcess
    {
        FrameworkBulkRewardProcess? _FrameworkBulkRewardProcess;
        public void ProcessBulkRewardCustomerReward()
        {
            var systemA = ActorSystem.Create("ActorProcessBulkRewardAccountId");
            var greeterA = systemA.ActorOf<ActorProcessBulkRewardAccountId>("ActorProcessBulkRewardAccountId");
            greeterA.Tell("call");


            var system = ActorSystem.Create("ActorProcessBulkReward");
            var greeter = system.ActorOf<ActorProcessBulkReward>("ActorProcessBulkReward");
            greeter.Tell("call");

            var systemN = ActorSystem.Create("ActorProcessBulkRewardNotification");
            var greeterN = systemN.ActorOf<ActorProcessBulkRewardNotification>("ActorProcessBulkRewardNotification");
            greeterN.Tell("call");
        }

    }
    public class ActorProcessBulkRewardAccountId : ReceiveActor
    {
        FrameworkBulkRewardProcess _FrameworkBulkRewardProcess;
        public ActorProcessBulkRewardAccountId()
        {
            Receive<string>(_Action =>
            {
                _FrameworkBulkRewardProcess = new FrameworkBulkRewardProcess();
                _FrameworkBulkRewardProcess.ProcessAccountId();
            });
        }
    }
    public class ActorProcessBulkReward : ReceiveActor
    {
        FrameworkBulkRewardProcess _FrameworkBulkRewardProcess;
        public ActorProcessBulkReward()
        {
            Receive<string>(_Action =>
            {
                _FrameworkBulkRewardProcess = new FrameworkBulkRewardProcess();
                _FrameworkBulkRewardProcess.ProcessRewardTransaction();
            });
        }
    }
    public class ActorProcessBulkRewardNotification : ReceiveActor
    {
        FrameworkBulkRewardProcess _FrameworkBulkRewardProcess;
        public ActorProcessBulkRewardNotification()
        {
            Receive<string>(_Action =>
            {
                _FrameworkBulkRewardProcess = new FrameworkBulkRewardProcess();
                _FrameworkBulkRewardProcess.ProcessRewardNotification();
            });
        }
    }


    public class FrameworkBulkRewardProcess
    {
        ManageCoreTransaction _ManageCoreTransaction;
        OAppProfileLite.Request _AppProfileRequest;
        HCoreContext _HCoreContext;
        HCoreContextOperations _HCoreContextOperations;
        ManageCoreUserAccess _ManageCoreUserAccess;
        //HCUAccountTransaction _HCUAccountTransaction;
        HCUAccountTransaction _HCUAccountTransactionChild;
        public async void ProcessAccountId()
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var Countries = await _HCoreContext.HCCoreCountry
                        .Select(x => new
                        {
                            x.Id,
                            x.MobileNumberLength,
                            x.Isd
                        })
                        .ToListAsync();
                    using (_HCoreContextOperations = new HCoreContextOperations())
                    {
                        var PendingItems = await _HCoreContextOperations.HCOUploadCustomerReward
                             .Where(x => x.AccountId == null && x.FileId > 80)
                             .Skip(0)
                             .Take(1000)
                             .ToListAsync();
                        if (PendingItems.Count > 0)
                        {
                            foreach (var Item in PendingItems)
                            {
                                int AccountCountryId = 0;
                                string AccountCountryIsd = "";
                                string AccountNumber = "";
                                long AccountId = 0;
                                #region Get Customer 
                                var CountryInformation = Countries.Where(x => x.Isd == Item.AccountCountryIsd)
                                                             .Select(x => new
                                                             {
                                                                 x.Id,
                                                                 x.MobileNumberLength,
                                                                 x.Isd
                                                             })
                                                             .FirstOrDefault();
                                string TUserName = _AppConfig.AppUserPrefix + Item.MobileNumber;
                                string TAccountNumber = Item.MobileNumber;
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var AccountDetails = await _HCoreContext.HCUAccount
                                       .Where(x => (x.AccountTypeId == UserAccountType.Appuser && (x.AccountCode == TAccountNumber || x.MobileNumber == TAccountNumber)))
                                       .Select(x => new
                                       {
                                           x.Id,
                                           x.CountryId,
                                           x.AccountCode,
                                       }).FirstOrDefaultAsync();
                                    if (AccountDetails != null)
                                    {
                                        AccountId = AccountDetails.Id;
                                        AccountCountryId = CountryInformation.Id;
                                        AccountCountryIsd = CountryInformation.Isd;
                                        AccountNumber = AccountDetails.AccountCode;
                                    }
                                    else
                                    {
                                        _AppProfileRequest = new OAppProfileLite.Request();
                                        _AppProfileRequest.OwnerId = Item.MerchantId;
                                        if (Item.CreatedById > 0)
                                        {
                                            _AppProfileRequest.CreatedById = (long)Item.CreatedById;
                                        }
                                        _AppProfileRequest.CountryId = CountryInformation.Id;
                                        _AppProfileRequest.MobileNumber = TAccountNumber;
                                        _AppProfileRequest.EmailAddress = Item.EmailAddress;
                                        _AppProfileRequest.DateOfBirth = Item.DateOfBirth;
                                        _AppProfileRequest.Name = Item.Name;
                                        _AppProfileRequest.FirstName = Item.FirstName;
                                        _AppProfileRequest.MiddleName = Item.MiddleName;
                                        _AppProfileRequest.LastName = Item.LastName;
                                        if (!string.IsNullOrEmpty(Item.FirstName))
                                        {
                                            _AppProfileRequest.DisplayName = Item.FirstName;
                                        }
                                        else
                                        {
                                            _AppProfileRequest.DisplayName = Item.MobileNumber;
                                        }
                                        if (!string.IsNullOrEmpty(Item.Gender))
                                        {
                                            _AppProfileRequest.GenderCode = Item.Gender.ToLower();
                                        }
                                        OUserReference _OUserReference = new OUserReference();
                                        _OUserReference.AccountId = Item.MerchantId;
                                        _OUserReference.AccountTypeId = UserAccountType.Merchant;
                                        _AppProfileRequest.UserReference = _OUserReference;
                                        _ManageCoreUserAccess = new ManageCoreUserAccess();
                                        OAppProfile.Response _AppUserCreateResponse = _ManageCoreUserAccess.CreateAppUserAccountLite(_AppProfileRequest);
                                        if (_AppUserCreateResponse != null && _AppUserCreateResponse.Status == ResponseStatus.Success)
                                        {
                                            AccountCountryIsd = CountryInformation.Isd;
                                            AccountCountryId = CountryInformation.Id;
                                            AccountNumber = _AppUserCreateResponse.AccountCode;
                                            AccountId = _AppUserCreateResponse.AccountId;
                                        }
                                        else
                                        {
                                            Item.StatusId = 4;
                                            Item.StatusMessage = "Account creation failed";
                                        }
                                    }
                                }
                                #endregion
                                if (AccountId > 0)
                                {
                                    Item.AccountId = AccountId;
                                }
                                Item.AccountCountryId = AccountCountryId;
                                Item.AccountCountryIsd = AccountCountryIsd;
                                Item.AccountNumber = AccountNumber;
                                Item.ModifyDate = HCoreHelper.GetGMTDateTime();
                            }
                            await _HCoreContextOperations.SaveChangesAsync();
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ActorUploadCustomerRewardProcess", _Exception, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        public async void ProcessRewardTransaction()
        {
            #region Manage Exception
            try
            {
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var Files = _HCoreContextOperations.HCOUploadFile.Where(x => (x.StatusId == 1 || x.StatusId == 2) && x.Id > 80).
                        Select(x => x.Id).ToList();
                    foreach (var FileId in Files)
                    {
                        using (_HCoreContextOperations = new HCoreContextOperations())
                        {
                            var RewardItems = _HCoreContextOperations
                            .HCOUploadCustomerReward
                            .Where(x => x.FileId == FileId && x.StatusId == 1 && x.AccountId != null && x.MerchantCountryId != null)
                            .Skip(0)
                            .Take(10000)
                            .ToList();
                            if (RewardItems.Count > 0)
                            {
                                foreach (var PendingItem in RewardItems)
                                {
                                    PendingItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    PendingItem.ModifyById = SystemAccounts.ThankUCashSystemId;
                                    PendingItem.StatusId = 2;
                                    PendingItem.StatusMessage = "Processing";
                                }
                                _HCoreContextOperations.SaveChanges();
                                int offset = 0;
                                for (int i = 0; i < 6; i++)
                                {
                                    using (_HCoreContextOperations = new HCoreContextOperations())
                                    {
                                        var PendingItems = _HCoreContextOperations.HCOUploadCustomerReward
                                            .Where(x => x.StatusId == 2
                                            && x.AccountId != null
                                            && x.MerchantCountryId != null)
                                            .Skip(offset)
                                            .Take(2000)
                                            .ToList();
                                        if (PendingItems.Count > 0)
                                        {
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                foreach (var Item in PendingItems)
                                                {
                                                    long ProcessStatusId = 1;
                                                    string ProcessMessage = "";
                                                    #region Merchant Transaction
                                                    HCUAccountTransaction _HCUAccountTransaction = new HCUAccountTransaction();
                                                    _HCUAccountTransaction.Guid = Item.TransactionReference;
                                                    _HCUAccountTransaction.ParentId = Item.MerchantId;
                                                    _HCUAccountTransaction.AccountId = Item.MerchantId;
                                                    _HCUAccountTransaction.ModeId = TransactionMode.Debit;
                                                    _HCUAccountTransaction.TypeId = (int)Item.MerchantTransactionTypeId;
                                                    _HCUAccountTransaction.SourceId = (int)Item.MerchantTransactionSourceId;
                                                    _HCUAccountTransaction.Amount = (int)Item.RewardAmount;
                                                    _HCUAccountTransaction.Charge = 0;
                                                    _HCUAccountTransaction.ComissionAmount = (int)Item.TucComissionAmount;
                                                    _HCUAccountTransaction.TotalAmount = (int)Item.RewardAmount;
                                                    _HCUAccountTransaction.Balance = 0;
                                                    _HCUAccountTransaction.PurchaseAmount = (double)Item.InvoiceAmount;
                                                    _HCUAccountTransaction.ReferenceAmount = (double)Item.InvoiceAmount;
                                                    _HCUAccountTransaction.ReferenceInvoiceAmount = (double)Item.InvoiceAmount;
                                                    _HCUAccountTransaction.CustomerId = Item.AccountId;
                                                    _HCUAccountTransaction.TransactionDate = HCoreHelper.GetGMTDateTime();
                                                    _HCUAccountTransaction.CreateDate = HCoreHelper.GetGMTDateTime();
                                                    _HCUAccountTransaction.ReferenceNumber = Item.ReferenceNumber;
                                                    _HCUAccountTransaction.CreatedById = Item.MerchantId;
                                                    _HCUAccountTransaction.GroupKey = Item.TransactionReference;
                                                    _HCUAccountTransaction.StatusId = (int)Item.TransactionStatusId;
                                                    _HCUAccountTransaction.TCode = Item.FileId.ToString();
                                                    #endregion
                                                    #region Customer Transaction
                                                    _HCUAccountTransactionChild = new HCUAccountTransaction();
                                                    _HCUAccountTransactionChild.Guid = HCoreHelper.GenerateGuid();
                                                    _HCUAccountTransactionChild.ParentId = Item.MerchantId;
                                                    _HCUAccountTransactionChild.AccountId = (int)Item.AccountId;
                                                    _HCUAccountTransactionChild.ModeId = TransactionMode.Credit;
                                                    _HCUAccountTransactionChild.TypeId = (int)Item.CustomerTransactionTypeId;
                                                    _HCUAccountTransactionChild.SourceId = (int)Item.CustomerTransactionSourceId;
                                                    _HCUAccountTransactionChild.Amount = (double)Item.CustomerRewardAmount;
                                                    _HCUAccountTransactionChild.Charge = 0;
                                                    _HCUAccountTransactionChild.ComissionAmount = 0;
                                                    _HCUAccountTransactionChild.TotalAmount = (double)Item.CustomerRewardAmount;
                                                    _HCUAccountTransactionChild.Balance = 0;
                                                    _HCUAccountTransactionChild.PurchaseAmount = (double)Item.InvoiceAmount;
                                                    _HCUAccountTransactionChild.ReferenceAmount = (double)Item.InvoiceAmount;
                                                    _HCUAccountTransactionChild.ReferenceInvoiceAmount = (double)Item.InvoiceAmount;
                                                    _HCUAccountTransactionChild.CustomerId = (int)Item.AccountId;
                                                    _HCUAccountTransactionChild.ReferenceNumber = Item.ReferenceNumber;
                                                    _HCUAccountTransactionChild.TransactionDate = HCoreHelper.GetGMTDateTime();
                                                    _HCUAccountTransactionChild.CreateDate = HCoreHelper.GetGMTDateTime();
                                                    _HCUAccountTransactionChild.CreatedById = Item.MerchantId;
                                                    _HCUAccountTransactionChild.GroupKey = Item.TransactionReference;
                                                    _HCUAccountTransactionChild.StatusId = (int)Item.TransactionStatusId;
                                                    _HCUAccountTransactionChild.TCode = Item.FileId.ToString();
                                                    _HCUAccountTransactionChild.ParentTransaction = _HCUAccountTransaction;
                                                    _HCUAccountTransaction.InverseParentTransaction.Add(_HCUAccountTransactionChild);
                                                    //_HCoreContext.HCUAccountTransaction.Add(_HCUAccountTransactionChild);
                                                    #endregion
                                                    #region Comission Transaction
                                                    _HCUAccountTransactionChild = new HCUAccountTransaction();
                                                    _HCUAccountTransactionChild.Guid = HCoreHelper.GenerateGuid();
                                                    _HCUAccountTransactionChild.ParentId = Item.MerchantId;
                                                    _HCUAccountTransactionChild.AccountId = 3;
                                                    _HCUAccountTransactionChild.ModeId = TransactionMode.Credit;
                                                    _HCUAccountTransactionChild.TypeId = TransactionType.CashReward;
                                                    _HCUAccountTransactionChild.SourceId = TransactionSource.Settlement;
                                                    _HCUAccountTransactionChild.Amount = (double)Item.TucComissionAmount;
                                                    _HCUAccountTransactionChild.Charge = 0;
                                                    _HCUAccountTransactionChild.ComissionAmount = 0;
                                                    _HCUAccountTransactionChild.TotalAmount = (double)Item.TucComissionAmount;
                                                    _HCUAccountTransactionChild.Balance = 0;
                                                    _HCUAccountTransactionChild.PurchaseAmount = (double)Item.InvoiceAmount;
                                                    _HCUAccountTransactionChild.ReferenceAmount = (double)Item.InvoiceAmount;
                                                    _HCUAccountTransactionChild.ReferenceInvoiceAmount = (double)Item.InvoiceAmount;
                                                    _HCUAccountTransactionChild.CustomerId = (int)Item.AccountId;
                                                    _HCUAccountTransactionChild.TransactionDate = HCoreHelper.GetGMTDateTime();
                                                    _HCUAccountTransactionChild.ReferenceNumber = Item.ReferenceNumber;
                                                    _HCUAccountTransactionChild.CreateDate = HCoreHelper.GetGMTDateTime();
                                                    _HCUAccountTransactionChild.CreatedById = Item.MerchantId;
                                                    _HCUAccountTransactionChild.GroupKey = Item.TransactionReference;
                                                    _HCUAccountTransactionChild.StatusId = (int)Item.TransactionStatusId;
                                                    _HCUAccountTransactionChild.TCode = Item.FileId.ToString();
                                                    _HCUAccountTransaction.InverseParentTransaction.Add(_HCUAccountTransactionChild);
                                                    _HCoreContext.HCUAccountTransaction.Add(_HCUAccountTransaction);
                                                    //_HCUAccountTransaction.InverseParentTransaction.Add(new HCUAccountTransaction
                                                    //{
                                                    //    Guid = HCoreHelper.GenerateGuid(),
                                                    //    ParentId = Item.MerchantId,
                                                    //    AccountId = 3,
                                                    //    ModeId = TransactionMode.Credit,
                                                    //    TypeId = TransactionType.CashReward,
                                                    //    SourceId = TransactionSource.Settlement,
                                                    //    Amount = (double)Item.TucComissionAmount,
                                                    //    Charge = 0,
                                                    //    ComissionAmount = 0,
                                                    //    TotalAmount = (double)Item.TucComissionAmount,
                                                    //    Balance = 0,
                                                    //    PurchaseAmount = (double)Item.InvoiceAmount,
                                                    //    ReferenceAmount = (double)Item.InvoiceAmount,
                                                    //    ReferenceInvoiceAmount = (double)Item.InvoiceAmount,
                                                    //    CustomerId = (int)Item.AccountId,
                                                    //    TransactionDate = HCoreHelper.GetGMTDateTime(),
                                                    //    CreateDate = HCoreHelper.GetGMTDateTime(),
                                                    //    CreatedById = Item.MerchantId,
                                                    //    GroupKey = Item.TransactionReference,
                                                    //    StatusId = (int)Item.TransactionStatusId,
                                                    //    TCode = Item.FileId.ToString(),
                                                    //});
                                                    #endregion
                                                    #region Online Account
                                                    ProcessStatusId = 3;
                                                    ProcessMessage = "Customer rewarded successfully";
                                                    #endregion
                                                    Item.StatusId = ProcessStatusId;
                                                    Item.StatusMessage = ProcessMessage;
                                                    Item.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                }
                                                _HCoreContext.SaveChanges();
                                                _HCoreContextOperations.SaveChanges();
                                            }
                                        }
                                        offset = offset + 2000;
                                    }
                                    using (_HCoreContextOperations = new HCoreContextOperations())
                                    {
                                        var FileIds = _HCoreContextOperations.HCOUploadFile.Where(x => x.Id == FileId).ToList();
                                        if (FileIds.Count > 0)
                                        {
                                            foreach (var File in FileIds)
                                            {
                                                File.Pending = _HCoreContextOperations.HCOUploadCustomerReward.Count(x => x.FileId == FileId && x.StatusId == 1);
                                                File.Processing = _HCoreContextOperations.HCOUploadCustomerReward.Count(x => x.FileId == FileId && x.StatusId == 2);
                                                File.Success = _HCoreContextOperations.HCOUploadCustomerReward.Count(x => x.FileId == FileId && x.StatusId == 3);
                                                File.Error = _HCoreContextOperations.HCOUploadCustomerReward.Count(x => x.FileId == FileId && x.StatusId == 4);
                                                File.Completed = File.Success + File.Error;
                                                if (File.TotalRecord == File.Completed)
                                                {
                                                    File.StatusId = 3;
                                                }
                                                // 1 => Pending 
                                                // 2 => Processing
                                                // 3 => Success 
                                                //FileDetails.StatusMessage = ProcessMessage;
                                                File.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            }
                                            _HCoreContextOperations.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ProcessRewardTransaction", _Exception, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        public async void ProcessRewardNotification()
        {

            using (_HCoreContextOperations = new HCoreContextOperations())
            {
                var PendingItems = _HCoreContextOperations
                    .HCOUploadCustomerReward
                    .Where(x => x.StatusId == 3 && x.IsSmsSent == 0 && x.FileId > 80)
                    .Skip(0)
                    .Take(500)
                    .ToList();
                if (PendingItems.Count > 0)
                {
                    foreach (var Item in PendingItems)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {

                            var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.Id == Item.AccountId)
                                    .Select(x => new
                                    {
                                        EmailAddress = x.EmailAddress,
                                        DisplayName = x.DisplayName,
                                        AccountNumber = x.AccountCode,
                                        MobileNumber = x.MobileNumber,
                                    }).FirstOrDefault();
                            if (CustomerDetails != null)
                            {

                                _ManageCoreTransaction = new ManageCoreTransaction();
                                if (Item.MerchantId == 583505 || Item.MerchantId == 13)
                                {
                                    if (HostEnvironment == HostEnvironmentType.Live)
                                    {
                                        var CustomerBalance = _ManageCoreTransaction.GetAppUserBalance((long)Item.AccountId, Item.MerchantId, TransactionSource.TUCBlack);
                                        if (!string.IsNullOrEmpty(CustomerDetails.MobileNumber))
                                        {
                                            string Message = "Credit Alert: You have earned " + Item.CustomerRewardAmount + " WakaPoints. Your current balance is: " + CustomerBalance + " points. Book now on https://www.wakanow.com";
                                            HCoreHelper.SendSMS(SmsType.Transaction, Item.AccountCountryIsd, CustomerDetails.MobileNumber, Message, (long)Item.AccountId, null);
                                        }
                                        if (!string.IsNullOrEmpty(CustomerDetails.EmailAddress)) // Wakanow email
                                        {
                                            string Template = "";
                                            string currentDirectory = Directory.GetCurrentDirectory();
                                            string path = "templates";
                                            string fullPath = Path.Combine(currentDirectory, path, "wakanow_alert_reward.html");
                                            using (StreamReader reader = File.OpenText(fullPath))
                                            {
                                                Template = reader.ReadToEnd();
                                            }
                                            var _EmailParameters = new
                                            {
                                                MerchantLogo = "https://points.wakanow.com/assets/img/wakanow-logo.png",
                                                MerchantDisplayName = "WakaPoints",
                                                UserDisplayName = CustomerDetails.DisplayName,
                                                AccountNumber = CustomerDetails.AccountNumber,
                                                Amount = Item.CustomerRewardAmount.ToString(),
                                                Balance = CustomerBalance.ToString(),
                                            };
                                            Template = Template.Replace("{{MerchantLogo}}", _EmailParameters.MerchantLogo);
                                            Template = Template.Replace("{{MerchantDisplayName}}", _EmailParameters.MerchantDisplayName);
                                            Template = Template.Replace("{{UserDisplayName}}", _EmailParameters.UserDisplayName);
                                            Template = Template.Replace("{{AccountNumber}}", _EmailParameters.AccountNumber);
                                            Template = Template.Replace("{{Amount}}", _EmailParameters.Amount);
                                            Template = Template.Replace("{{Balance}}", _EmailParameters.Balance);
                                            var client = new SendGridClient("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");
                                            var msg = new SendGridMessage()
                                            {
                                                From = new EmailAddress("noreply@wakanow.com", "WakaPoints"),
                                                Subject = "WakaPoints Credit Alert - " + _EmailParameters.Amount + " points rewarded",
                                                HtmlContent = Template
                                            };
                                            msg.AddTo(new EmailAddress(CustomerDetails.EmailAddress, CustomerDetails.DisplayName));
                                            var response = client.SendEmailAsync(msg);
                                        }
                                    }
                                }
                                else
                                {
                                    if (HostEnvironment == HostEnvironmentType.Live)
                                    {
                                        var CustomerBalance = _ManageCoreTransaction.GetAppUserBalance((long)Item.AccountId, true);
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == Item.AccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                            if (!string.IsNullOrEmpty(UserNotificationUrl))
                                            {
                                                if (HostEnvironment == HostEnvironmentType.Live)
                                                {
                                                    HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "N " + Item.CustomerRewardAmount + " rewards received.", "Your have received N" + Item.CustomerRewardAmount + " from " + CustomerDetails.DisplayName + " for purchase of N" + Item.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                                }
                                                else
                                                {
                                                    HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST : N " + Item.CustomerRewardAmount + " rewards received.", "Your have received N" + Item.CustomerRewardAmount + " from " + Item.MerchantDisplayName + " for purchase of N" + Item.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                                }
                                            }
                                            else
                                            {
                                                #region Send SMS
                                                string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber((double)Item.CustomerRewardAmount, _AppConfig.SystemExitRoundDouble).ToString() + " points cash reward from " + Item.MerchantDisplayName + "  Bal: N" + HCoreHelper.RoundNumber((CustomerBalance + (double)Item.CustomerRewardAmount), _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
                                                HCoreHelper.SendSMS(SmsType.Transaction, Item.AccountCountryIsd, Item.MobileNumber, Message, (long)Item.AccountId, null, null);
                                                #endregion
                                            }
                                        }
                                        #region Send Email 
                                        if (!string.IsNullOrEmpty(CustomerDetails.EmailAddress))
                                        {
                                            var _EmailParameters = new
                                            {
                                                UserDisplayName = CustomerDetails.DisplayName,
                                                MerchantName = Item.MerchantDisplayName,
                                                InvoiceAmount = Item.InvoiceAmount.ToString(),
                                                Amount = HCoreHelper.RoundNumber((double)Item.CustomerRewardAmount, 2).ToString(),
                                                Balance = CustomerBalance.ToString(),
                                            };
                                            HCoreHelper.BroadCastEmail(Framework.Merchant.App.SendGridEmailTemplateIds.RewardEmail, CustomerDetails.DisplayName, CustomerDetails.EmailAddress, _EmailParameters, null);
                                        }
                                        #endregion
                                    }
                                }
                            }
                            Item.ModifyDate = HCoreHelper.GetGMTDateTime();
                            Item.ModifyById = SystemAccounts.ThankUCashSystemId;
                            Item.IsSmsSent = 2;
                            Item.IsEmailSent = 2;
                            await _HCoreContextOperations.SaveChangesAsync();
                        }
                    }
                    //using (_HCoreContext = new HCoreContext())
                    //{
                    //    _ManageCoreTransaction = new ManageCoreTransaction();
                    //    foreach (var PendingItem in PendingItems)
                    //    {
                    //        var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.Id == PendingItem.AccountId)
                    //            .Select(x => new
                    //            {
                    //                EmailAddress = x.EmailAddress,
                    //                DisplayName = x.DisplayName,
                    //                AccountNumber = x.AccountCode,
                    //            }).FirstOrDefault();
                    //        var CustomerBalance = _ManageCoreTransaction.GetAppUserBalance((long)PendingItem.AccountId, PendingItem.MerchantId, TransactionSource.TUCBlack);
                    //        if (!string.IsNullOrEmpty(PendingItem.MobileNumber))
                    //        {
                    //            string Message = "Credit Alert: You have earned " + PendingItem.CustomerRewardAmount + " WakaPoints. Your current balance is: " + CustomerBalance + " points. Book now on https://www.wakanow.com";
                    //            HCoreHelper.SendSMS(SmsType.Transaction, PendingItem.AccountCountryIsd, PendingItem.MobileNumber, Message, (long)PendingItem.AccountId, null);
                    //        }
                    //        if (!string.IsNullOrEmpty(CustomerDetails.EmailAddress)) // Wakanow email
                    //        {
                    //            string Template = "";
                    //            string currentDirectory = Directory.GetCurrentDirectory();
                    //            string path = "templates";
                    //            string fullPath = Path.Combine(currentDirectory, path, "wakanow_alert_reward.html");
                    //            using (StreamReader reader = File.OpenText(fullPath))
                    //            {
                    //                Template = reader.ReadToEnd();
                    //            }
                    //            var _EmailParameters = new
                    //            {
                    //                MerchantLogo = "https://points.wakanow.com/assets/img/wakanow-logo.png",
                    //                MerchantDisplayName = "WakaPoints",
                    //                UserDisplayName = CustomerDetails.DisplayName,
                    //                AccountNumber = CustomerDetails.AccountNumber,
                    //                Amount = PendingItem.CustomerRewardAmount.ToString(),
                    //                Balance = CustomerBalance.ToString(),
                    //            };
                    //            Template = Template.Replace("{{MerchantLogo}}", _EmailParameters.MerchantLogo);
                    //            Template = Template.Replace("{{MerchantDisplayName}}", _EmailParameters.MerchantDisplayName);
                    //            Template = Template.Replace("{{UserDisplayName}}", _EmailParameters.UserDisplayName);
                    //            Template = Template.Replace("{{AccountNumber}}", _EmailParameters.AccountNumber);
                    //            Template = Template.Replace("{{Amount}}", _EmailParameters.Amount);
                    //            Template = Template.Replace("{{Balance}}", _EmailParameters.Balance);
                    //            var client = new SendGridClient("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");
                    //            var msg = new SendGridMessage()
                    //            {
                    //                From = new EmailAddress("noreply@wakanow.com", "WakaPoints"),
                    //                Subject = "WakaPoints Credit Alert - " + _EmailParameters.Amount + " points rewarded",
                    //                HtmlContent = Template
                    //            };
                    //            msg.AddTo(new EmailAddress(CustomerDetails.EmailAddress, CustomerDetails.DisplayName));
                    //            var response = await client.SendEmailAsync(msg);
                    //        }
                    //        PendingItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                    //        PendingItem.ModifyById = SystemAccounts.ThankUCashSystemId;
                    //        PendingItem.IsSmsSent = 1;
                    //        PendingItem.IsEmailSent = 1;
                    //    }
                    //    await _HCoreContextOperations.SaveChangesAsync();
                    //}
                }
            }
        }
    }
}


