﻿using System;
using HCore.Data.Operations;
using HCore.Data.Operations.Models;
using HCore.Helper;
//using HCore.TUC.Core.Framework.Merchant.Upload;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using System.Linq.Dynamic.Core;
using HCore.TUC.Core.Resource;
using Akka.Actor;
using HCore.TUC.Core.Framework.Merchant.App;
using HCore.TUC.Core.Object.Merchant;
using HCore.Data;
using HCore.Data.Models;
using HCore.Operations;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace HCore.TUC.Core.Framework.Merchant.BulkRewards
{
    public class OBulkRewardUpload
    {
        public class Request
        {
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? FileName { get; set; }
            public List<Item> Customers { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Item
        {
            public string? Name { get; set; }
            public string? FirstName { get; set; }
            public string? MiddleName { get; set; }
            public string? LastName { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? Gender { get; set; }
            public string? ReferenceNumber { get; set; }
            public string? Comment { get; set; }
            public double InvoiceAmount { get; set; }
            public double RewardAmount { get; set; }
            public DateTime? DateOfBirth { get; set; }
        }
    }
    public class ManageBulkRewardUpload
    {
        FrameworkBulkRewardUpload? _FrameworkBulkRewardUpload;
        public OResponse UploadBulkRewardCustomers(OBulkRewardUpload.Request _Request)
        {
            _FrameworkBulkRewardUpload = new FrameworkBulkRewardUpload();
            return _FrameworkBulkRewardUpload.UploadBulkRewardCustomers(_Request);
        }
    }

    public class FrameworkBulkRewardUpload
    {
        internal OResponse UploadBulkRewardCustomers(OBulkRewardUpload.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1500, TUCCoreResource.CA1500M);
                }
                if (_Request.Customers == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1501, TUCCoreResource.CA1501M);
                }
                if (_Request.Customers.Count < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1501, TUCCoreResource.CA1501M);
                }
                var system = ActorSystem.Create("ActorBulkRewardUploadN");
                var greeter = system.ActorOf<ActorBulkRewardUploadN>("ActorBulkRewardUploadN");
                greeter.Tell(_Request);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1502, TUCCoreResource.CA1502M);
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UploadBulkRewardCustomers", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }


    public class ActorBulkRewardUploadN : ReceiveActor
    {
        HCOUploadFile _HCOUploadFile;
        HCOUploadCustomerReward _HCOUploadCustomerReward;
        HCoreContextOperations _HCoreContextOperations;
        HCoreContext _HCoreContext;
        public ActorBulkRewardUploadN()
        {
            Receive<OBulkRewardUpload.Request>(async _Request =>
            {
                long FileId = 0;
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    _HCOUploadFile = new HCOUploadFile();
                    _HCOUploadFile.TypeId = 1;
                    _HCOUploadFile.OwnerId = _Request.AccountId;
                    _HCOUploadFile.Name = _Request.FileName;
                    _HCOUploadFile.TotalRecord = _Request.Customers.Count;
                    _HCOUploadFile.Pending = _Request.Customers.Count;
                    _HCOUploadFile.Processing = 0;
                    _HCOUploadFile.Completed = 0;
                    _HCOUploadFile.Success = 0;
                    _HCOUploadFile.Error = 0;
                    _HCOUploadFile.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCOUploadFile.CreatedById = _Request.UserReference.AccountId;
                    _HCOUploadFile.StatusId = 1;
                    _HCoreContextOperations.HCOUploadFile.Add(_HCOUploadFile);
                    await _HCoreContextOperations.SaveChangesAsync();
                    FileId = _HCOUploadFile.Id;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var Customers = await _HCoreContext.HCUAccount
                    .Where(x => x.AccountTypeId == UserAccountType.Appuser)
                       .Select(x => new
                       {
                           x.Id,
                           x.MobileNumber,
                           x.AccountCode,
                           x.CountryId
                       })
                       .ToListAsync();

                    var Countries = await _HCoreContext.HCCoreCountry
                       .Select(x => new
                       {
                           x.Id,
                           x.MobileNumberLength,
                           x.Isd
                       })
                       .ToListAsync();
                    double SystemCategoryCommissionPercentage = 0;
                    var MerchantDetails = await _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId)
                        .Select(x => new
                        {
                            CategoryId = x.PrimaryCategoryId,
                            DisplayName = x.DisplayName,
                            CountryId = x.CountryId,
                        }).FirstOrDefaultAsync();
                    var CategoryCommission = await _HCoreContext.TUCMerchantCategory.Where(x => x.RootCategoryId == MerchantDetails.CategoryId)
                                                              .Select(x => new
                                                              {
                                                                  Commission = x.Commission,
                                                              }).FirstOrDefaultAsync();
                    //_HCoreContext.Dispose();
                    if (CategoryCommission != null && CategoryCommission.Commission > 0)
                    {
                        SystemCategoryCommissionPercentage = (double)CategoryCommission.Commission;
                    }
                    double TUCRewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", _Request.AccountId)), _AppConfig.SystemRoundPercentage);
                    var TUCGold = HCoreHelper.GetConfigurationDetails("thankucashgold", _Request.AccountId);
                    int ThankUCashPercentageFromRewardAmount = Convert.ToInt32(HCoreHelper.GetConfiguration("rewardcommissionfromrewardamount", _Request.AccountId));
                    var RewardDeductionType = HCoreHelper.GetConfigurationDetails("rewarddeductiontype", _Request.AccountId);
                    double TUCRewardUserAmountPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("userrewardpercentage", _Request.AccountId)), _AppConfig.SystemRoundPercentage);

                    // Gen 2
                    int TAllowCustomReward = Convert.ToInt32(HCoreHelper.GetConfiguration("customreward", _Request.AccountId));
                    double MaximumCommissionAmount = 2500;
                    double SystemDefaultCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardcomissionpercentage")), _AppConfig.SystemRoundPercentage);
                    double SystemMerchantCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetAccountConfiguration("rewardcomissionpercentage", _Request.AccountId)), _AppConfig.SystemRoundPercentage);
                    using (_HCoreContextOperations = new HCoreContextOperations())
                    {
                        foreach (var DataItem in _Request.Customers)
                        {
                            if (!string.IsNullOrEmpty(DataItem.MobileNumber))
                            {
                                double TUCTotalRewardAmount = 0; // 1
                                double TUCTotalRewardAmountPercentage = TUCRewardPercentage; // 1
                                double TUCCustomerRewardAmount = 0;
                                double TUCCustomerRewardAmountPercentage = TUCRewardUserAmountPercentage; // 1
                                double TUCComissionRewardAmount = 0; // 1
                                double TUCComissionRewardAmountPercentage = 0; // 1

                                int MerchantTransactionSource = 0; // 1
                                int MerchantTransactionType = 0; // 1
                                int CustomerTransactionSource = 0; // 1
                                int CustomerTransactionType = 0; // 1
                                int TransactionStatusId = HelperStatus.Transaction.Success; // 1

                                #region Rewards Calculation
                                if (DataItem.InvoiceAmount > 0)
                                {
                                    TUCTotalRewardAmount = HCoreHelper.GetPercentage((double)DataItem.InvoiceAmount, TUCRewardPercentage, _AppConfig.SystemEntryRoundDouble);
                                    #region Reward Calculation
                                    if (ThankUCashPercentageFromRewardAmount == 1)   // 70 - 30 Commission Gen 1 
                                    {
                                        TUCCustomerRewardAmount = HCoreHelper.GetPercentage(TUCTotalRewardAmount, TUCRewardUserAmountPercentage);
                                        TUCComissionRewardAmount = TUCTotalRewardAmount - TUCCustomerRewardAmount;
                                        TUCComissionRewardAmountPercentage = (100 - TUCRewardUserAmountPercentage);

                                        MerchantTransactionSource = TransactionSource.Merchant;
                                        MerchantTransactionType = TransactionType.CashReward;
                                        if (TUCGold != null && !string.IsNullOrEmpty(TUCGold.Value) && TUCGold.Value != "0")
                                        {
                                            CustomerTransactionSource = TransactionSource.TUCBlack;
                                            CustomerTransactionType = TransactionType.CashReward;
                                        }
                                        else
                                        {
                                            CustomerTransactionSource = TransactionSource.TUC;
                                            CustomerTransactionType = TransactionType.CashReward;
                                        }

                                        if (RewardDeductionType.TypeCode == "rewarddeductiontype.postpay")
                                        {
                                            TransactionStatusId = HelperStatus.Transaction.Success;
                                        }
                                        else
                                        {
                                            //_ManageCoreTransaction = new ManageCoreTransaction();
                                            //double MerchantBalance = _ManageCoreTransaction.GetMerchantBalance(Item.MerchantId, TransactionSource.Merchant);
                                            //if (MerchantBalance >= TUCRewardAmount)
                                            //{
                                            //    TransactionStatusId = HelperStatus.Transaction.Success;
                                            //}
                                            //else
                                            //{
                                            TransactionStatusId = HelperStatus.Transaction.Pending;
                                            //}
                                        }
                                    }
                                    else   // Custom Calculation Gen 2  || Merchant Commission Extra to the Reward Amount
                                    {
                                        double TUCRewardCommissionAmountPercentage = 0;
                                        MerchantTransactionSource = TransactionSource.Merchant;
                                        MerchantTransactionType = TransactionType.CashReward;
                                        if (TUCGold != null && !string.IsNullOrEmpty(TUCGold.Value) && TUCGold.Value != "0")
                                        {
                                            CustomerTransactionSource = TransactionSource.TUCBlack;
                                            CustomerTransactionType = TransactionType.CashReward;
                                        }
                                        else
                                        {
                                            CustomerTransactionSource = TransactionSource.TUC;
                                            CustomerTransactionType = TransactionType.CashReward;
                                        }

                                        double TUCRewardAmount = 0;
                                        if (TAllowCustomReward == 1)
                                        {
                                            if (DataItem.RewardAmount > 0)
                                            {
                                                TUCRewardAmount = (double)DataItem.RewardAmount;
                                            }
                                        }

                                        if (SystemMerchantCommissionPercentage > 0)
                                        {
                                            TUCRewardCommissionAmountPercentage = SystemMerchantCommissionPercentage;
                                        }
                                        else if (SystemCategoryCommissionPercentage > 0)
                                        {
                                            TUCRewardCommissionAmountPercentage = SystemCategoryCommissionPercentage;
                                        }
                                        else
                                        {
                                            TUCRewardCommissionAmountPercentage = SystemDefaultCommissionPercentage;
                                        }

                                        TUCComissionRewardAmount = HCoreHelper.GetPercentage((double)DataItem.InvoiceAmount, TUCRewardCommissionAmountPercentage);
                                        if (TUCGold != null && TUCGold.Value == "1")
                                        {
                                            TUCComissionRewardAmount = 0;
                                        }
                                        if (TUCComissionRewardAmountPercentage > MaximumCommissionAmount)
                                        {
                                            TUCComissionRewardAmount = MaximumCommissionAmount;
                                        }
                                        TUCCustomerRewardAmount = TUCRewardAmount;
                                        TUCRewardUserAmountPercentage = TUCRewardPercentage;
                                        TUCCustomerRewardAmountPercentage = TUCRewardUserAmountPercentage;
                                        TUCRewardAmount = TUCCustomerRewardAmount + TUCComissionRewardAmount;
                                        TUCTotalRewardAmount = TUCRewardAmount;
                                        TUCTotalRewardAmountPercentage = TUCRewardUserAmountPercentage + TUCComissionRewardAmountPercentage;
                                        //if (TUCRewardAmount > DataItem.InvoiceAmount)
                                        //{
                                        //    //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14138, TUCCoreResource.CAA14138M);
                                        //}
                                        if (TUCGold != null && TUCGold.Value == "1")
                                        {
                                            TransactionStatusId = HelperStatus.Transaction.Success;
                                        }
                                        else
                                        {
                                            TransactionStatusId = HelperStatus.Transaction.Pending;
                                            //_ManageCoreTransaction = new ManageCoreTransaction();
                                            //double MerchantBalance = _ManageCoreTransaction.GetMerchantBalance(Item.MerchantId, TransactionSource.Merchant);
                                            //if (MerchantBalance >= TUCRewardAmount)
                                            //{
                                            //    TransactionStatusId = HelperStatus.Transaction.Success;
                                            //}
                                            //else
                                            //{
                                            //    if (MerchantBalance > -5000)
                                            //    {
                                            //        if ((MerchantBalance - TUCRewardAmount) > -5000)
                                            //        {
                                            //            TransactionStatusId = HelperStatus.Transaction.Pending;
                                            //        }
                                            //        else
                                            //        {
                                            //            TransactionStatusId = HelperStatus.Transaction.Success;
                                            //        }
                                            //    }
                                            //}
                                        }
                                    }
                                    #endregion
                                }
                                #endregion

                                _HCOUploadCustomerReward = new HCOUploadCustomerReward();
                                _HCOUploadCustomerReward.MerchantId = _Request.AccountId;
                                _HCOUploadCustomerReward.MerchantDisplayName = MerchantDetails.DisplayName;
                                _HCOUploadCustomerReward.MerchantCountryId = MerchantDetails.CountryId;
                                _HCOUploadCustomerReward.MerchantCategoryId = MerchantDetails.CategoryId;
                                _HCOUploadCustomerReward.MerchantCategoryComission = SystemCategoryCommissionPercentage;
                                _HCOUploadCustomerReward.Name = DataItem.Name;
                                _HCOUploadCustomerReward.FirstName = DataItem.FirstName;
                                _HCOUploadCustomerReward.MiddleName = DataItem.MiddleName;
                                _HCOUploadCustomerReward.LastName = DataItem.LastName;
                                if (!string.IsNullOrEmpty(DataItem.MobileNumber))
                                {
                                    DataItem.MobileNumber = HCoreHelper.CleanString(DataItem.MobileNumber);
                                    var ValidateMobileNumber = HCoreHelper.ValidateMobileNumber(DataItem.MobileNumber);
                                    if (ValidateMobileNumber.IsNumberValid == true)
                                    {
                                        var CountryInformation = Countries.Where(x => x.Isd == ValidateMobileNumber.Isd)
                                          .Select(x => new
                                          {
                                              x.Id,
                                              x.MobileNumberLength,
                                              x.Isd
                                          })
                                          .FirstOrDefault();
                                        _HCOUploadCustomerReward.AccountCountryId = CountryInformation.Id;
                                        _HCOUploadCustomerReward.AccountCountryIsd = CountryInformation.Isd;
                                        DataItem.MobileNumber = HCoreHelper.FormatMobileNumber(ValidateMobileNumber.Isd, ValidateMobileNumber.MobileNumber, CountryInformation.MobileNumberLength);
                                        string TUserName = _AppConfig.AppUserPrefix + DataItem.MobileNumber;
                                        string TAccountNumber = DataItem.MobileNumber;
                                        var AccountDetails = Customers.Where(x => x.AccountCode == TAccountNumber || x.MobileNumber == TAccountNumber)
                                         .Select(x => new
                                         {
                                             x.Id,
                                             x.CountryId,
                                             x.AccountCode,
                                         }).FirstOrDefault();
                                        // var AccountDetails = await _HCoreContext.HCUAccount
                                        //.Where(x => (x.AccountTypeId == UserAccountType.Appuser && (x.AccountCode == TAccountNumber || x.MobileNumber == TAccountNumber)))
                                        //.Select(x => new
                                        //{
                                        //    x.Id,
                                        //    x.CountryId,
                                        //    x.AccountCode,
                                        //}).FirstOrDefaultAsync();
                                        if (AccountDetails != null)
                                        {
                                            _HCOUploadCustomerReward.AccountId = AccountDetails.Id;
                                            _HCOUploadCustomerReward.AccountCountryId = CountryInformation.Id;
                                            _HCOUploadCustomerReward.AccountCountryIsd = CountryInformation.Isd;
                                            _HCOUploadCustomerReward.AccountNumber = AccountDetails.AccountCode;
                                        }
                                        _HCOUploadCustomerReward.StatusId = 1;
                                        _HCOUploadCustomerReward.StatusMessage = "Pending";
                                    }
                                    else
                                    {
                                        _HCOUploadCustomerReward.StatusId = 4;
                                        _HCOUploadCustomerReward.StatusMessage = "Invalid mobile number";
                                    }
                                }
                                _HCOUploadCustomerReward.MobileNumber = DataItem.MobileNumber;
                                _HCOUploadCustomerReward.FormattedMobileNumber = DataItem.MobileNumber;
                                _HCOUploadCustomerReward.EmailAddress = DataItem.EmailAddress;
                                _HCOUploadCustomerReward.Gender = DataItem.Gender;
                                _HCOUploadCustomerReward.DateOfBirth = DataItem.DateOfBirth;
                                _HCOUploadCustomerReward.ReferenceNumber = DataItem.ReferenceNumber;
                                _HCOUploadCustomerReward.Comment = DataItem.Comment;
                                _HCOUploadCustomerReward.InvoiceAmount = DataItem.InvoiceAmount;
                                _HCOUploadCustomerReward.RewardAmount = DataItem.RewardAmount;
                                _HCOUploadCustomerReward.IsEmailSent = 0;
                                _HCOUploadCustomerReward.IsSmsSent = 0;
                                _HCOUploadCustomerReward.TucComissionAmount = TUCComissionRewardAmount;
                                _HCOUploadCustomerReward.TucComissionAmountPercentage = TUCComissionRewardAmountPercentage;
                                _HCOUploadCustomerReward.CustomerRewardAmount = TUCCustomerRewardAmount;
                                _HCOUploadCustomerReward.CustomerRewardPercentage = TUCCustomerRewardAmountPercentage;
                                _HCOUploadCustomerReward.TucComissionSource = "gen";
                                _HCOUploadCustomerReward.MerchantTransactionSourceId = MerchantTransactionSource;
                                _HCOUploadCustomerReward.MerchantTransactionTypeId = MerchantTransactionType;
                                _HCOUploadCustomerReward.CustomerTransactionSourceId = CustomerTransactionSource;
                                _HCOUploadCustomerReward.CustomerTransactionTypeId = CustomerTransactionType;
                                _HCOUploadCustomerReward.TransactionStatusId = TransactionStatusId;
                                _HCOUploadCustomerReward.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCOUploadCustomerReward.CreatedById = _Request.UserReference.AccountId;
                                _HCOUploadCustomerReward.TransactionReference = "TRA" + HCoreHelper.GenerateDateString() + HCoreHelper.GetAccountIdString(_Request.AccountId) + HCoreHelper.GetAccountIdString(Convert.ToInt64(HCoreHelper.GenerateRandomNumber(4)));
                                _HCOUploadCustomerReward.FileId = FileId;
                                _HCoreContextOperations.HCOUploadCustomerReward.Add(_HCOUploadCustomerReward);
                            }
                        }
                        await _HCoreContextOperations.SaveChangesAsync();
                    }
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var FileDetails = await _HCoreContextOperations.HCOUploadFile.Where(x => x.Id == FileId).FirstOrDefaultAsync();
                    if (FileDetails != null)
                    {
                        FileDetails.Pending = await _HCoreContextOperations.HCOUploadCustomerReward.CountAsync(x => x.FileId == FileId && x.StatusId == 1);
                        FileDetails.Processing = await _HCoreContextOperations.HCOUploadCustomerReward.CountAsync(x => x.FileId == FileId && x.StatusId == 2);
                        FileDetails.Success = await _HCoreContextOperations.HCOUploadCustomerReward.CountAsync(x => x.FileId == FileId && x.StatusId == 3);
                        FileDetails.Error = await _HCoreContextOperations.HCOUploadCustomerReward.CountAsync(x => x.FileId == FileId && x.StatusId > 3);
                        FileDetails.Completed = FileDetails.Success + FileDetails.Error;
                        if (FileDetails.TotalRecord == FileDetails.Completed)
                        {
                            FileDetails.StatusId = 3;
                        }
                        // 1 => Pending 
                        // 2 => Processing
                        // 3 => Success 
                        //FileDetails.StatusMessage = ProcessMessage;
                        FileDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                    }
                    await _HCoreContextOperations.SaveChangesAsync();
                }
            });
        }
    }

}

