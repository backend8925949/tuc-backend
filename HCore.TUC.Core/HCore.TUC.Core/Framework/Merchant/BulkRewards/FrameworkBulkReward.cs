﻿using System;
using HCore.Data.Operations;
using HCore.Helper;
//using HCore.TUC.Core.Framework.Merchant.Upload;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using System.Linq.Dynamic.Core;
using HCore.TUC.Core.Resource;
using Akka.Actor;
using HCore.TUC.Core.Framework.Merchant.App;
using HCore.TUC.Core.Object.Merchant;

namespace HCore.TUC.Core.Framework.Merchant.BulkRewards
{
    public class OBulkRewards
    {
        public class Request
        {
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? FileName { get; set; }
            public List<Details> Data { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public long OwnerId { get; set; }
            public string? Name { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? MobileNumber { get; set; }
            public string? FormattedMobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? Gender { get; set; }
            public DateTime? DateOfBirth { get; set; }

            public string? ReferenceNumber { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public long? StatusId { get; set; }

            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public string? StatusMessage { get; set; }

            public double? InvoiceAmount { get; set; }
            public double? RewardAmount { get; set; }

        }
        public class File
        {
            public long ReferenceId { get; set; }
            public long OwnerId { get; set; }

            public string? Name { get; set; }

            public long TotalRecord { get; set; }
            public long Processing { get; set; }
            public long Pending { get; set; }
            public long Success { get; set; }
            public long Error { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public string? StatusMessage { get; set; }

        }
    }
    public class ManageBulkRewards
    {
        FrameworkBulkReward? _FrameworkBulkReward;
        public OResponse GetBulkRewardCustomers(OList.Request _Request)
        {
            _FrameworkBulkReward = new FrameworkBulkReward();
            return _FrameworkBulkReward.GetBulkRewardCustomers(_Request);
        }
    }
    internal class FrameworkBulkReward
    {
        HCoreContextOperations? _HCoreContextOperations;
        internal OResponse GetBulkRewardCustomers(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    if (_Request.ReferenceId != 0 && _Request.SubReferenceId != 0)
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContextOperations.HCOUploadCustomerReward
                                                    .Where(x => x.MerchantId == _Request.ReferenceId
                                                    && x.FileId == _Request.SubReferenceId)
                                                    .Select(x => new OBulkRewards.Details
                                                    {
                                                        ReferenceId = x.Id,
                                                        Name = x.Name,
                                                        FirstName = x.FirstName,
                                                        LastName = x.LastName,
                                                        EmailAddress = x.EmailAddress,
                                                        Gender = x.Gender,
                                                        DateOfBirth = x.DateOfBirth,
                                                        FormattedMobileNumber = x.FormattedMobileNumber,
                                                        MobileNumber = x.MobileNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        CreatedById = x.CreatedById,
                                                        CreateDate = x.CreateDate,
                                                        ModifyDate = x.ModifyDate,
                                                        ModifyById = x.ModifyById,
                                                        StatusId = x.StatusId,
                                                        StatusMessage = x.StatusMessage,
                                                        InvoiceAmount = x.InvoiceAmount,
                                                        RewardAmount = x.RewardAmount,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OBulkRewards.Details> Data = _HCoreContextOperations.HCOUploadCustomerReward
                                                    .Where(x => x.MerchantId == _Request.ReferenceId
                                                    && x.FileId == _Request.SubReferenceId)
                                                    .Select(x => new OBulkRewards.Details
                                                    {
                                                        ReferenceId = x.Id,
                                                        Name = x.Name,
                                                        FirstName = x.FirstName,
                                                        LastName = x.LastName,
                                                        EmailAddress = x.EmailAddress,
                                                        Gender = x.Gender,
                                                        DateOfBirth = x.DateOfBirth,
                                                        FormattedMobileNumber = x.FormattedMobileNumber,
                                                        MobileNumber = x.MobileNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        CreatedById = x.CreatedById,
                                                        CreateDate = x.CreateDate,
                                                        ModifyDate = x.ModifyDate,
                                                        ModifyById = x.ModifyById,
                                                        StatusId = x.StatusId,
                                                        StatusMessage = x.StatusMessage,
                                                        InvoiceAmount = x.InvoiceAmount,
                                                        RewardAmount = x.RewardAmount,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        foreach (var DataItem in Data)
                        {
                            if (DataItem.StatusId == 1)
                            {
                                DataItem.StatusCode = "Pending";
                                DataItem.StatusName = "Pending";
                            }
                            else if (DataItem.StatusId == 2)
                            {
                                DataItem.StatusCode = "Processing";
                                DataItem.StatusName = "Processing";
                            }
                            else if (DataItem.StatusId == 3)
                            {
                                DataItem.StatusCode = "Success";
                                DataItem.StatusName = "Success";
                            }
                            else if (DataItem.StatusId == 4)
                            {
                                DataItem.StatusCode = "Error";
                                DataItem.StatusName = "Error";
                            }
                            else if (DataItem.StatusId == 5)
                            {
                                DataItem.StatusCode = "Cancelled";
                                DataItem.StatusName = "Cancelled";
                            }
                        }
                        _HCoreContextOperations.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContextOperations.HCOUploadCustomerReward
                                                    .Where(x => x.MerchantId == _Request.ReferenceId)
                                                    .Select(x => new OBulkRewards.Details
                                                    {
                                                        ReferenceId = x.Id,
                                                        Name = x.Name,
                                                        FirstName = x.FirstName,
                                                        LastName = x.LastName,
                                                        EmailAddress = x.EmailAddress,
                                                        Gender = x.Gender,
                                                        DateOfBirth = x.DateOfBirth,
                                                        FormattedMobileNumber = x.FormattedMobileNumber,
                                                        MobileNumber = x.MobileNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        CreatedById = x.CreatedById,
                                                        CreateDate = x.CreateDate,
                                                        ModifyDate = x.ModifyDate,
                                                        ModifyById = x.ModifyById,
                                                        StatusId = x.StatusId,
                                                        StatusMessage = x.StatusMessage,
                                                        InvoiceAmount = x.InvoiceAmount,
                                                        RewardAmount = x.RewardAmount,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OBulkRewards.Details> Data = _HCoreContextOperations.HCOUploadCustomerReward
                                                    .Where(x => x.MerchantId == _Request.ReferenceId)
                                                    .Select(x => new OBulkRewards.Details
                                                    {
                                                        ReferenceId = x.Id,
                                                        Name = x.Name,
                                                        FirstName = x.FirstName,
                                                        LastName = x.LastName,
                                                        EmailAddress = x.EmailAddress,
                                                        Gender = x.Gender,
                                                        DateOfBirth = x.DateOfBirth,
                                                        FormattedMobileNumber = x.FormattedMobileNumber,
                                                        MobileNumber = x.MobileNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        CreatedById = x.CreatedById,
                                                        CreateDate = x.CreateDate,
                                                        ModifyDate = x.ModifyDate,
                                                        ModifyById = x.ModifyById,
                                                        StatusId = x.StatusId,
                                                        StatusMessage = x.StatusMessage,
                                                        InvoiceAmount = x.InvoiceAmount,
                                                        RewardAmount = x.RewardAmount,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        foreach (var DataItem in Data)
                        {
                            DataItem.CreatedByDisplayName = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.CreatedById).Select(x => x.DisplayName).FirstOrDefault();
                        }
                        _HCoreContextOperations.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetBulkRewardCustomers", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }

}

