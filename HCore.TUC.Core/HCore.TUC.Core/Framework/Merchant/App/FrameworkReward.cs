//==================================================================================
// FileName: FrameworkReward.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to reward
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Akka.Actor;
using ExcelDataReader;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Operations;
using HCore.Helper;
using HCore.Integration.Paystack;
using HCore.Object;
using HCore.Operations;
using HCore.Operations.Object;
//using HCore.TUC.Core.Framework.Merchant.Upload;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Object.Merchant.App;
using HCore.TUC.Core.Resource;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.TUC.Core.Object.Merchant.OUpload.RewardUpload;

namespace HCore.TUC.Core.Framework.Merchant.App
{
    public class FrameworkReward
    {
        HCoreContext _HCoreContext;
        ManageCoreTransaction _ManageCoreTransaction;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        TUCLoyaltyPending _TUCLoyaltyPending;
        OReward.Initialize.Response _RewardTransactionInitialize;
        OReward.Confirm.Response _RewardConfirmResponse;
        /// <summary>
        /// Description: Reward initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse Reward_Initialize(OReward.Initialize.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.AccountNumber) && string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1167, TUCCoreResource.CA1167M);
                }
                else if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1168, TUCCoreResource.CA1168M);
                }
                else if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1169, TUCCoreResource.CA1169M);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        string CountryIsd = "234";
                        int CountryMobileNumberLength = 10;
                        long MerchantId = 0;
                        long MerchantStatusId = HelperStatus.Default.Inactive;
                        if (_Request.UserReference.AccountTypeId == UserAccountType.MerchantCashier)
                        {
                            var MerchantDetails = _HCoreContext.HCUAccount
                             .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                && x.Id == _Request.AccountId
                                && x.Guid == _Request.AccountKey)
                            //.Where(x => x.Owner.Owner.AccountTypeId == UserAccountType.Merchant
                            //    && x.Owner.Id == _Request.AccountId
                            //    && x.Owner.Guid == _Request.AccountKey)
                            .Select(x => new
                            {
                                ReferenceId = x.Id,
                                StatusId = x.StatusId,
                                Isd = x.Country.Isd,
                                MobileNumberLength = x.Country.MobileNumberLength,
                            }).FirstOrDefault();
                            if (MerchantDetails != null)
                            {
                                CountryIsd = MerchantDetails.Isd;
                                CountryMobileNumberLength = MerchantDetails.MobileNumberLength;
                                MerchantId = MerchantDetails.ReferenceId;
                                MerchantStatusId = MerchantDetails.StatusId;
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
                            }
                        }
                        else
                        {
                            var MerchantDetails = _HCoreContext.HCUAccount
                                .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                    && x.Id == _Request.AccountId
                                    && x.Guid == _Request.AccountKey)
                                .Select(x => new
                                {
                                    ReferenceId = x.Id,
                                    StatusId = x.StatusId,
                                    Isd = x.Country.Isd,
                                    MobileNumberLength = x.Country.MobileNumberLength,
                                }).FirstOrDefault();
                            if (MerchantDetails != null)
                            {
                                CountryIsd = MerchantDetails.Isd;
                                CountryMobileNumberLength = MerchantDetails.MobileNumberLength;
                                MerchantId = MerchantDetails.ReferenceId;
                                MerchantStatusId = MerchantDetails.StatusId;
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
                            }
                        }

                        if (MerchantStatusId == HelperStatus.Default.Active)
                        {
                            if (!string.IsNullOrEmpty(_Request.MobileNumber))
                            {
                                string AccountCode = _Request.MobileNumber;

                                if (string.IsNullOrEmpty(_Request.MobileNumber))
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA116900", "Invalid mobile number");
                                }
                                else if (_Request.MobileNumber.Length > 14)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA116900", "Invalid mobile number");
                                }
                                else if (!System.Text.RegularExpressions.Regex.IsMatch(_Request.MobileNumber, "^[0-9]*$"))
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA116900", "Invalid mobile number");
                                }
                                if (_Request.MobileNumber.StartsWith("0"))
                                {
                                    _Request.MobileNumber = HCoreHelper.FormatMobileNumber(CountryIsd, _Request.MobileNumber, CountryMobileNumberLength);
                                }
                                var ValidateMobileNumber = HCoreHelper.ValidateMobileNumber(_Request.MobileNumber);
                                if (ValidateMobileNumber.IsNumberValid == true)
                                {
                                    var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Isd == ValidateMobileNumber.Isd)
                                               .Select(x => new
                                               {
                                                   x.MobileNumberLength,
                                               })
                                               .FirstOrDefault();

                                    if (CountryDetails == null)
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA116900", "Invalid country details");
                                    }
                                    //int CountryNumberLength = _HCoreContext.HCCoreCountry.Where(x => x.Isd == _Request.UserReference.CountryIsd).Select(x => x.MobileNumberLength).FirstOrDefault();
                                    _Request.MobileNumber = HCoreHelper.FormatMobileNumber(ValidateMobileNumber.Isd, _Request.MobileNumber, CountryDetails.MobileNumberLength);
                                    string TMobileNumber = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                                    && (x.User.Username == TMobileNumber || x.AccountCode == AccountCode))
                                   .Select(x => new OReward.Initialize.Response
                                   {
                                       ReferenceId = x.Id,
                                       ReferenceKey = x.Guid,
                                       Name = x.Name,
                                       IconUrl = x.IconStorage.Path,
                                       MobileNumber = x.MobileNumber,
                                       StatusId = x.StatusId,
                                       AccountBalance = 0,
                                       AccountNumber = x.AccountCode,
                                   }).FirstOrDefault();
                                    if (AccountDetails != null)
                                    {
                                        if (AccountDetails.StatusId == HelperStatus.Default.Active)
                                        {
                                            AccountDetails.MobileNumber = _Request.MobileNumber;
                                            AccountDetails.IsNewCustomer = false;
                                            if (!string.IsNullOrEmpty(AccountDetails.IconUrl))
                                            {
                                                AccountDetails.IconUrl = _AppConfig.StorageUrl + AccountDetails.IconUrl;
                                            }
                                            else
                                            {
                                                AccountDetails.IconUrl = _AppConfig.Default_Icon;
                                            }
                                            AccountDetails.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", MerchantId));
                                            int AllowCustomReward = Convert.ToInt32(HCoreHelper.GetConfiguration("customreward", MerchantId));
                                            if (AllowCustomReward == 1)
                                            {
                                                AccountDetails.AllowCustomReward = true;
                                            }
                                            else
                                            {
                                                AccountDetails.AllowCustomReward = false;
                                            }
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, AccountDetails, TUCCoreResource.CA1158, TUCCoreResource.CA1158M);
                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1159, TUCCoreResource.CA1159M);
                                        }
                                    }
                                    else
                                    {
                                        //_ManageCoreTransaction = new ManageCoreTransaction();
                                        _RewardTransactionInitialize = new OReward.Initialize.Response();
                                        _RewardTransactionInitialize.IsNewCustomer = true;
                                        _RewardTransactionInitialize.MobileNumber = _Request.MobileNumber;
                                        _RewardTransactionInitialize.AccountNumber = _Request.AccountNumber;
                                        //_RewardTransactionInitialize.AccountBalance = _ManageCoreTransaction.GetAccountBalance(MerchantId, TransactionSource.Merchant);
                                        _RewardTransactionInitialize.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", MerchantId));
                                        int AllowCustomReward = Convert.ToInt32(HCoreHelper.GetConfiguration("customreward", MerchantId));
                                        if (AllowCustomReward == 1)
                                        {
                                            _RewardTransactionInitialize.AllowCustomReward = true;
                                        }
                                        else
                                        {
                                            _RewardTransactionInitialize.AllowCustomReward = false;
                                        }
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _RewardTransactionInitialize, TUCCoreResource.CA1158, TUCCoreResource.CA1158M);
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA116900", "Invalid mobile number");
                                }
                            }
                            else
                            {
                                var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                                && (x.AccountCode == _Request.AccountNumber
                                || x.Guid == _Request.AccountNumber
                                || x.MobileNumber == _Request.AccountNumber))
                                    .Select(x => new OReward.Initialize.Response
                                    {
                                        ReferenceId = x.Id,
                                        ReferenceKey = x.Guid,
                                        Name = x.Name,
                                        IconUrl = x.IconStorage.Path,
                                        MobileNumber = x.MobileNumber,
                                        StatusId = x.StatusId,
                                        AccountBalance = 0,
                                        AccountNumber = x.AccountCode,
                                    }).FirstOrDefault();
                                if (AccountDetails != null)
                                {
                                    if (AccountDetails.StatusId == HelperStatus.Default.Active)
                                    {
                                        AccountDetails.MobileNumber = _Request.MobileNumber;
                                        if (!string.IsNullOrEmpty(AccountDetails.IconUrl))
                                        {
                                            AccountDetails.IconUrl = _AppConfig.StorageUrl + AccountDetails.IconUrl;
                                        }
                                        else
                                        {
                                            AccountDetails.IconUrl = _AppConfig.Default_Icon;
                                        }
                                        //_ManageCoreTransaction = new ManageCoreTransaction();
                                        int AllowCustomReward = Convert.ToInt32(HCoreHelper.GetConfiguration("customreward", MerchantId));
                                        if (AllowCustomReward == 1)
                                        {
                                            AccountDetails.AllowCustomReward = true;
                                        }
                                        else
                                        {
                                            AccountDetails.AllowCustomReward = false;
                                        }
                                        //AccountDetails.AccountBalance = _ManageCoreTransaction.GetAccountBalance(MerchantId, TransactionSource.Merchant);
                                        AccountDetails.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", MerchantId));
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, AccountDetails, TUCCoreResource.CA1158, TUCCoreResource.CA1158M);
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1159, TUCCoreResource.CA1159M);
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1316, TUCCoreResource.CA1316M);
                                }
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1157, TUCCoreResource.CA1157M);
                        }

                    }
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "Reward_Initialize", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Reward confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse Reward_Confirm(OReward.Confirm.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.AccountNumber) && string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1153, TUCCoreResource.CA1153M);
                }
                else if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1154, TUCCoreResource.CA1154M);
                }
                else if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1155, TUCCoreResource.CA1155M);
                }
                else if (_Request.ReferenceId < 1 && string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1160, TUCCoreResource.CA1160M);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceKey) && string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1161, TUCCoreResource.CA1161M);
                }
                else if (_Request.InvoiceAmount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1162, TUCCoreResource.CA1162M);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        string CountryIsd = "234";
                        int CountryMobileNumberLength = 10;
                        long CashierId = 0;
                        string CustomerDisplayName = null;
                        string CustomerEmailAddress = null;
                        string CustomerAccountNumber = null;
                        long MerchantId = 0;
                        //int MobileNumberLength = 0;
                        int MerchantCategoryId = 0;
                        long MerchantStatusId = HelperStatus.Default.Inactive;
                        string MerchantDisplayName = null;
                        #region Merchant Details
                        if (_Request.UserReference.AccountTypeId == UserAccountType.MerchantCashier)
                        {
                            var MerchantDetails = _HCoreContext.HCUAccount
                                 .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                && x.Id == _Request.AccountId
                                && x.Guid == _Request.AccountKey)
                            //.Where(x => x.Owner.Owner.AccountTypeId == UserAccountType.Merchant
                            //    && x.Owner.Id == _Request.AccountId
                            //    && x.Owner.Guid == _Request.AccountKey)
                            .Select(x => new
                            {
                                ReferenceId = x.Id,
                                StatusId = x.StatusId,
                                MerchantDisplayName = x.DisplayName,
                                MerchantCategoryId = x.PrimaryCategoryId,
                                CountryIsd = x.Country.Isd,
                                MobileNumberLength = x.Country.MobileNumberLength,
                            }).FirstOrDefault();
                            if (MerchantDetails != null)
                            {
                                if (MerchantDetails.MerchantCategoryId != null)
                                {
                                    MerchantCategoryId = (int)MerchantDetails.MerchantCategoryId;
                                }
                                MerchantId = MerchantDetails.ReferenceId;
                                MerchantStatusId = MerchantDetails.StatusId;
                                MerchantDisplayName = MerchantDetails.MerchantDisplayName;
                                CountryIsd = MerchantDetails.CountryIsd;
                                CountryMobileNumberLength = MerchantDetails.MobileNumberLength;
                                //CountryIsd = MerchantDetails.CountryIsd;
                                //MobileNumberLength = MerchantDetails.MobileNumberLength;
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
                            }
                        }
                        else
                        {
                            var MerchantDetails = _HCoreContext.HCUAccount
                                .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                    && x.Id == _Request.AccountId
                                    && x.Guid == _Request.AccountKey)
                                .Select(x => new
                                {
                                    ReferenceId = x.Id,
                                    StatusId = x.StatusId,
                                    MerchantDisplayName = x.DisplayName,
                                    MerchantCategoryId = x.PrimaryCategoryId,
                                    CountryIsd = x.Country.Isd,
                                    //CountryIsd = x.Owner.Owner.Country.Isd,
                                    MobileNumberLength = x.Country.MobileNumberLength,
                                }).FirstOrDefault();
                            if (MerchantDetails != null)
                            {
                                if (MerchantDetails.MerchantCategoryId != null)
                                {
                                    MerchantCategoryId = (int)MerchantDetails.MerchantCategoryId;
                                }
                                CashierId = _HCoreContext.HCUAccount.Where(x => x.DisplayName == _Request.CashierCode && x.Owner.Owner.Id == MerchantDetails.ReferenceId).Select(x => x.Id).FirstOrDefault();
                                MerchantId = MerchantDetails.ReferenceId;
                                MerchantStatusId = MerchantDetails.StatusId;
                                MerchantDisplayName = MerchantDetails.MerchantDisplayName;
                                CountryIsd = MerchantDetails.CountryIsd;
                                CountryMobileNumberLength = MerchantDetails.MobileNumberLength;
                                //CountryIsd = MerchantDetails.CountryIsd;
                                //MobileNumberLength = MerchantDetails.MobileNumberLength;
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
                            }
                        }
                        #endregion

                        if (MerchantStatusId == HelperStatus.Default.Active)
                        {
                            _RewardConfirmResponse = new OReward.Confirm.Response();
                            if (!string.IsNullOrEmpty(_Request.MobileNumber))
                            {
                                //if (_Request.MobileNumber.Length < MobileNumberLength)
                                //{
                                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA116900", "Invalid mobile number");
                                //}
                                //else if (_Request.MobileNumber.Length > 13)
                                //{
                                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA116900", "Invalid mobile number");
                                //}
                                if (!System.Text.RegularExpressions.Regex.IsMatch(_Request.MobileNumber, "^[0-9]*$"))
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA116900", "Invalid mobile number");
                                }
                                if (_Request.MobileNumber.StartsWith("0"))
                                {
                                    _Request.MobileNumber = HCoreHelper.FormatMobileNumber(CountryIsd, _Request.MobileNumber, CountryMobileNumberLength);
                                }
                                //if (_Request.MobileNumber.StartsWith("0"))
                                //{
                                //    _Request.MobileNumber = HCoreHelper.FormatMobileNumber(CountryIsd, _Request.MobileNumber);
                                //}
                                //else
                                //{
                                //    _Request.MobileNumber = HCoreHelper.FormatMobileNumber(CountryIsd, _Request.MobileNumber, MobileNumberLength);
                                //}
                                var ValidateMobileNumber = HCoreHelper.ValidateMobileNumber(_Request.MobileNumber);
                                if (ValidateMobileNumber.IsNumberValid == true)
                                {
                                    var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Isd == ValidateMobileNumber.Isd)
                                               .Select(x => new
                                               {
                                                   x.Id,
                                                   x.MobileNumberLength,
                                               })
                                               .FirstOrDefault();

                                    if (CountryDetails == null)
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA116900", "Invalid country details");
                                    }
                                    CountryIsd = ValidateMobileNumber.Isd;
                                    //int CountryNumberLength = _HCoreContext.HCCoreCountry.Where(x => x.Isd == _Request.UserReference.CountryIsd).Select(x => x.MobileNumberLength).FirstOrDefault();
                                    _Request.MobileNumber = HCoreHelper.FormatMobileNumber(ValidateMobileNumber.Isd, _Request.MobileNumber, CountryDetails.MobileNumberLength);
                                    string TMobileNumber = _AppConfig.AppUserPrefix + _Request.MobileNumber;

                                    //_Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber, _Request.MobileNumber.Length);

                                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                                    && x.User.Username == TMobileNumber)
                                   .Select(x => new OReward.Confirm.Response
                                   {
                                       ReferenceId = x.Id,
                                       ReferenceKey = x.Guid,
                                       DisplayName = x.DisplayName,
                                       Name = x.Name,
                                       IconUrl = x.IconStorage.Path,
                                       MobileNumber = x.MobileNumber,
                                       StatusId = x.StatusId,
                                       AccountNumber = x.AccountCode,
                                       EmailAddress = x.EmailAddress,
                                   }).FirstOrDefault();
                                    if (AccountDetails != null)
                                    {
                                        if (AccountDetails.StatusId == HelperStatus.Default.Active)
                                        {
                                            CustomerAccountNumber = AccountDetails.AccountNumber;
                                            CustomerDisplayName = AccountDetails.DisplayName;
                                            CustomerEmailAddress = AccountDetails.EmailAddress;
                                            _RewardConfirmResponse.ReferenceId = AccountDetails.ReferenceId;
                                            _RewardConfirmResponse.ReferenceKey = AccountDetails.ReferenceKey;
                                            _RewardConfirmResponse.DisplayName = AccountDetails.DisplayName;
                                            _RewardConfirmResponse.Name = AccountDetails.Name;
                                            _RewardConfirmResponse.IconUrl = AccountDetails.IconUrl;
                                            _RewardConfirmResponse.MobileNumber = AccountDetails.MobileNumber;
                                            _RewardConfirmResponse.EmailAddress = AccountDetails.EmailAddress;
                                            _RewardConfirmResponse.StatusId = AccountDetails.StatusId;
                                            _RewardConfirmResponse.AccountNumber = AccountDetails.AccountNumber;
                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1303, TUCCoreResource.CA1303M);
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(_Request.Nmae) && string.IsNullOrEmpty(_Request.Name))
                                        {
                                            _Request.Name = _Request.Nmae;
                                        }
                                        OAppProfile.Request _AppProfileRequest;
                                        ManageCoreUserAccess _ManageCoreUserAccess;
                                        _AppProfileRequest = new OAppProfile.Request();
                                        _AppProfileRequest.OwnerId = _Request.UserReference.AccountId;
                                        _AppProfileRequest.CreatedById = _Request.UserReference.AccountId;
                                        _AppProfileRequest.MobileNumber = _Request.MobileNumber;
                                        _AppProfileRequest.EmailAddress = _Request.EmailAddress;
                                        _AppProfileRequest.DateOfBirth = _Request.DateOfBirth;
                                        _AppProfileRequest.Name = _Request.Name;
                                        if (!string.IsNullOrEmpty(_Request.Name))
                                        {
                                            _AppProfileRequest.DisplayName = _Request.Name;
                                        }
                                        else
                                        {
                                            _AppProfileRequest.DisplayName = _Request.MobileNumber;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.GenderCode))
                                        {
                                            _AppProfileRequest.GenderCode = _Request.GenderCode.ToLower();
                                        }
                                        _AppProfileRequest.UserReference = _Request.UserReference;
                                        _AppProfileRequest.CountryId = CountryDetails.Id;
                                        _ManageCoreUserAccess = new ManageCoreUserAccess();
                                        OAppProfile.Response _AppUserCreateResponse = _ManageCoreUserAccess.CreateAppUserAccount(_AppProfileRequest);
                                        if (_AppUserCreateResponse != null && _AppUserCreateResponse.Status == ResponseStatus.Success)
                                        {
                                            CustomerAccountNumber = _AppUserCreateResponse.AccountCode;
                                            CustomerEmailAddress = _Request.EmailAddress;
                                            CustomerDisplayName = _Request.Name;
                                            _RewardConfirmResponse.ReferenceId = _AppUserCreateResponse.AccountId;
                                            _RewardConfirmResponse.ReferenceKey = _AppUserCreateResponse.AccountKey;
                                            _RewardConfirmResponse.DisplayName = _AppUserCreateResponse.DisplayName;
                                            _RewardConfirmResponse.Name = _AppUserCreateResponse.Name;
                                            _RewardConfirmResponse.MobileNumber = _AppUserCreateResponse.MobileNumber;
                                            _RewardConfirmResponse.StatusId = _AppUserCreateResponse.StatusId;
                                            _RewardConfirmResponse.EmailAddress = _AppUserCreateResponse.EmailAddress;
                                            _RewardConfirmResponse.AccountNumber = _AppUserCreateResponse.AccountCode;
                                        }
                                        else
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _AppUserCreateResponse.StatusResponseCode, _AppUserCreateResponse.StatusMessage);
                                            #endregion
                                        }
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA1162", "Invalid mobile number");
                                }
                            }
                            else
                            {
                                var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                             && (x.AccountCode == _Request.AccountNumber
                             || x.Guid == _Request.AccountNumber
                             || x.MobileNumber == _Request.AccountNumber))
                                 .Select(x => new OReward.Confirm.Response
                                 {
                                     ReferenceId = x.Id,
                                     ReferenceKey = x.Guid,
                                     DisplayName = x.DisplayName,
                                     Name = x.Name,
                                     IconUrl = x.IconStorage.Path,
                                     MobileNumber = x.MobileNumber,
                                     StatusId = x.StatusId,
                                     AccountNumber = x.AccountCode,
                                 }).FirstOrDefault();
                                if (AccountDetails != null)
                                {
                                    if (AccountDetails.StatusId == HelperStatus.Default.Active)
                                    {
                                        _RewardConfirmResponse.ReferenceId = AccountDetails.ReferenceId;
                                        _RewardConfirmResponse.ReferenceKey = AccountDetails.ReferenceKey;
                                        _RewardConfirmResponse.DisplayName = AccountDetails.DisplayName;
                                        _RewardConfirmResponse.Name = AccountDetails.Name;
                                        _RewardConfirmResponse.IconUrl = AccountDetails.IconUrl;
                                        _RewardConfirmResponse.MobileNumber = AccountDetails.MobileNumber;
                                        _RewardConfirmResponse.EmailAddress = AccountDetails.EmailAddress;
                                        _RewardConfirmResponse.StatusId = AccountDetails.StatusId;
                                        _RewardConfirmResponse.AccountNumber = AccountDetails.AccountNumber;
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1159, TUCCoreResource.CA1159M);
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1317, TUCCoreResource.CA1317M);
                                }
                            }
                            var TUCGold = HCoreHelper.GetConfigurationDetails("thankucashgold", MerchantId);
                            double TUCRewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", MerchantId)), _AppConfig.SystemRoundPercentage);
                            if (TUCRewardPercentage > 0)
                            {
                                double TUCRewardAmount = HCoreHelper.GetPercentage(_Request.InvoiceAmount, TUCRewardPercentage, _AppConfig.SystemEntryRoundDouble);
                                if (TUCRewardAmount <= 0)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1172, TUCCoreResource.CA1172M);
                                }
                                if (TUCRewardAmount > _Request.InvoiceAmount)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14138, TUCCoreResource.CAA14138M);
                                }
                                string ReferenceNumber = "TRA" + HCoreHelper.GenerateDateString() + HCoreHelper.GetAccountIdString(MerchantId) + HCoreHelper.GetAccountIdString(_Request.AccountId);
                                int ThankUCashPercentageFromRewardAmount = Convert.ToInt32(HCoreHelper.GetConfiguration("rewardcommissionfromrewardamount", MerchantId));
                                int TransactionStatusId = HelperStatus.Transaction.Success;
                                double TUCRewardUserAmount = 0;
                                double TUCRewardUserAmountPercentage = 0;
                                double TUCRewardCommissionAmount = 0;
                                double TUCRewardCommissionAmountPercentage = 0;
                                if (ThankUCashPercentageFromRewardAmount == 1)   // 70 - 30 Commission Gen 1 
                                {
                                    var RewardDeductionType = HCoreHelper.GetConfigurationDetails("rewarddeductiontype", MerchantId);
                                    TUCRewardUserAmountPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("userrewardpercentage", MerchantId)), _AppConfig.SystemRoundPercentage);
                                    TUCRewardUserAmount = HCoreHelper.GetPercentage(TUCRewardAmount, TUCRewardUserAmountPercentage);
                                    TUCRewardCommissionAmount = TUCRewardAmount - TUCRewardUserAmount;
                                    TUCRewardCommissionAmountPercentage = (100 - TUCRewardUserAmountPercentage);
                                    if (RewardDeductionType.TypeCode == "rewarddeductiontype.prepay")
                                    {
                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                        double MerchantBalance = _ManageCoreTransaction.GetMerchantBalance(MerchantId, TransactionSource.Merchant);
                                        if (MerchantBalance >= TUCRewardAmount)
                                        {
                                            TransactionStatusId = HelperStatus.Transaction.Success;
                                        }
                                        else
                                        {
                                            TransactionStatusId = HelperStatus.Transaction.Pending;
                                        }
                                    }
                                    else if (RewardDeductionType.TypeCode == "rewarddeductiontype.postpay")
                                    {
                                        TransactionStatusId = HelperStatus.Transaction.Success;
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14135, TUCCoreResource.CAA14135M);
                                    }
                                }
                                else   // Custom Calculation Gen 2  || Merchant Commission Extra to the Reward Amount
                                {
                                    int TAllowCustomReward = Convert.ToInt32(HCoreHelper.GetConfiguration("customreward", MerchantId));
                                    if (TAllowCustomReward == 1)
                                    {
                                        if (_Request.RewardAmount > 0)
                                        {
                                            TUCRewardAmount = _Request.RewardAmount;
                                        }
                                    }
                                    double MaximumCommissionAmount = 2500;
                                    double SystemDefaultCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardcomissionpercentage")), _AppConfig.SystemRoundPercentage);
                                    double SystemMerchantCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetAccountConfiguration("rewardcomissionpercentage", MerchantId)), _AppConfig.SystemRoundPercentage);
                                    double SystemCategoryCommissionPercentage = 0;
                                    if (MerchantCategoryId > 0)
                                    {
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var CategoryCommission = _HCoreContext.TUCMerchantCategory.Where(x => x.RootCategoryId == MerchantCategoryId)
                                                .Select(x => new
                                                {
                                                    Commission = x.Commission,
                                                }).FirstOrDefault();
                                            _HCoreContext.Dispose();
                                            if (CategoryCommission != null && CategoryCommission.Commission > 0)
                                            {
                                                SystemCategoryCommissionPercentage = (double)CategoryCommission.Commission;
                                            }
                                        }
                                    }
                                    if (SystemMerchantCommissionPercentage > 0)
                                    {
                                        TUCRewardCommissionAmountPercentage = SystemMerchantCommissionPercentage;
                                    }
                                    else if (SystemCategoryCommissionPercentage > 0)
                                    {
                                        TUCRewardCommissionAmountPercentage = SystemCategoryCommissionPercentage;
                                    }
                                    else
                                    {
                                        TUCRewardCommissionAmountPercentage = SystemDefaultCommissionPercentage;
                                    }

                                    TUCRewardCommissionAmount = HCoreHelper.GetPercentage(_Request.InvoiceAmount, TUCRewardCommissionAmountPercentage);
                                    if (TUCGold != null && TUCGold.Value == "1")
                                    {
                                        TUCRewardCommissionAmount = 0;
                                    }
                                    if (TUCRewardCommissionAmount > MaximumCommissionAmount)
                                    {
                                        TUCRewardCommissionAmount = MaximumCommissionAmount;
                                    }
                                    TUCRewardUserAmount = TUCRewardAmount;
                                    TUCRewardUserAmountPercentage = TUCRewardPercentage;
                                    TUCRewardAmount = TUCRewardUserAmount + TUCRewardCommissionAmount;
                                    if (TUCRewardAmount > _Request.InvoiceAmount)
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14138, TUCCoreResource.CAA14138M);
                                    }
                                    if (TUCGold != null && TUCGold.Value == "1")
                                    {
                                        TransactionStatusId = HelperStatus.Transaction.Success;
                                    }
                                    else
                                    {
                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                        double MerchantBalance = _ManageCoreTransaction.GetMerchantBalance(MerchantId, TransactionSource.Merchant);
                                        if (MerchantBalance >= TUCRewardAmount)
                                        {
                                            TransactionStatusId = HelperStatus.Transaction.Success;
                                        }
                                        else
                                        {
                                            if (MerchantBalance > -5000)
                                            {
                                                if ((MerchantBalance - TUCRewardAmount) > -5000)
                                                {
                                                    TransactionStatusId = HelperStatus.Transaction.Pending;
                                                }
                                                else
                                                {
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14137, TUCCoreResource.CAA14137M);
                                                }
                                            }
                                            else
                                            {
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14136, TUCCoreResource.CAA14136M);
                                            }
                                        }

                                    }
                                }
                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.GroupKey = ReferenceNumber;
                                _CoreTransactionRequest.CustomerId = _Request.AccountId;
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = TransactionStatusId;
                                _CoreTransactionRequest.ParentId = MerchantId;
                                _CoreTransactionRequest.InvoiceAmount = _Request.InvoiceAmount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.InvoiceAmount;
                                _CoreTransactionRequest.ReferenceAmount = TUCRewardAmount;
                                _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                if (_Request.UserReference.AccountTypeId == UserAccountType.MerchantCashier)
                                {
                                    _CoreTransactionRequest.CashierId = _Request.UserReference.AccountId;
                                }
                                else
                                {
                                    if (CashierId > 0)
                                    {
                                        _CoreTransactionRequest.CashierId = CashierId;
                                    }
                                }
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = MerchantId,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = TransactionType.CashReward,
                                    SourceId = TransactionSource.Merchant,
                                    Amount = TUCRewardAmount,
                                    Charge = 0,
                                    Comission = TUCRewardCommissionAmount,
                                    TotalAmount = TUCRewardAmount,
                                });
                                if (TUCGold != null && !string.IsNullOrEmpty(TUCGold.Value) && TUCGold.Value != "0")
                                {
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = _RewardConfirmResponse.ReferenceId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.CashReward,
                                        SourceId = TransactionSource.TUCBlack,
                                        Amount = TUCRewardUserAmount,
                                        TotalAmount = TUCRewardUserAmount,
                                    });
                                }
                                else
                                {
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = _RewardConfirmResponse.ReferenceId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.CashReward,
                                        SourceId = TransactionSource.TUC,
                                        Amount = TUCRewardUserAmount,
                                        TotalAmount = TUCRewardUserAmount,
                                    });
                                }

                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = SystemAccounts.ThankUCashMerchant,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionType.CashReward,
                                    SourceId = TransactionSource.Settlement,
                                    Amount = TUCRewardCommissionAmount,
                                    TotalAmount = TUCRewardCommissionAmount,
                                });
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                    if (!string.IsNullOrEmpty(_RewardConfirmResponse.IconUrl))
                                    {
                                        _RewardConfirmResponse.IconUrl = _AppConfig.StorageUrl + _RewardConfirmResponse.IconUrl;
                                    }
                                    else
                                    {
                                        _RewardConfirmResponse.IconUrl = _AppConfig.Default_Icon;
                                    }
                                    _RewardConfirmResponse.RewardAmount = TUCRewardAmount;
                                    _RewardConfirmResponse.InvoiceAmount = _Request.InvoiceAmount;
                                    _RewardConfirmResponse.Comment = _Request.Comment;
                                    _RewardConfirmResponse.TransactionReferenceId = TransactionResponse.ReferenceId;
                                    _RewardConfirmResponse.TransactionDate = TransactionResponse.TransactionDate;
                                    if (TransactionStatusId == HelperStatus.Transaction.Success)
                                    {
                                        _RewardConfirmResponse.StatusName = "success";
                                    }
                                    else
                                    {
                                        _RewardConfirmResponse.StatusName = "pending";
                                    }
                                    if (TransactionStatusId == HelperStatus.Transaction.Success)
                                    {
                                        if (MerchantId == 583505 || MerchantId == 13)
                                        {
                                            var CustomerBalance = _ManageCoreTransaction.GetAppUserBalance(_RewardConfirmResponse.ReferenceId, MerchantId, TransactionSource.TUCBlack);
                                            if (!string.IsNullOrEmpty(_Request.MobileNumber))
                                            {
                                                string Message = "Credit Alert: You have earned " + TUCRewardAmount + " WakaPoints. Your current balance is: " + CustomerBalance + " points. Book now on https://www.wakanow.com";
                                                HCoreHelper.SendSMS(SmsType.Transaction, CountryIsd, _Request.MobileNumber, Message, _RewardConfirmResponse.ReferenceId, null);
                                            }
                                            if (!string.IsNullOrEmpty(CustomerEmailAddress)) // Wakanow email
                                            {
                                                string Template = "";
                                                string currentDirectory = Directory.GetCurrentDirectory();
                                                string path = "templates";
                                                string fullPath = Path.Combine(currentDirectory, path, "wakanow_alert_reward.html");
                                                using (StreamReader reader = File.OpenText(fullPath))
                                                {
                                                    Template = reader.ReadToEnd();
                                                }
                                                var _EmailParameters = new
                                                {
                                                    MerchantLogo = "https://points.wakanow.com/assets/img/wakanow-logo.png",
                                                    MerchantDisplayName = "WakaPoints",
                                                    UserDisplayName = CustomerDisplayName,
                                                    AccountNumber = CustomerAccountNumber,
                                                    Amount = TUCRewardAmount.ToString(),
                                                    Balance = CustomerBalance.ToString(),
                                                };
                                                Template = Template.Replace("{{MerchantLogo}}", _EmailParameters.MerchantLogo);
                                                Template = Template.Replace("{{MerchantDisplayName}}", _EmailParameters.MerchantDisplayName);
                                                Template = Template.Replace("{{UserDisplayName}}", _EmailParameters.UserDisplayName);
                                                Template = Template.Replace("{{AccountNumber}}", _EmailParameters.AccountNumber);
                                                Template = Template.Replace("{{Amount}}", _EmailParameters.Amount);
                                                Template = Template.Replace("{{Balance}}", _EmailParameters.Balance);
                                                var client = new SendGridClient("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");
                                                var msg = new SendGridMessage()
                                                {
                                                    From = new EmailAddress("noreply@wakanow.com", "WakaPoints"),
                                                    Subject = "WakaPoints Credit Alert - " + _EmailParameters.Amount + " points rewarded",
                                                    HtmlContent = Template
                                                };
                                                msg.AddTo(new EmailAddress(CustomerEmailAddress, CustomerDisplayName));
                                                var response = client.SendEmailAsync(msg);
                                            }

                                        }
                                        else
                                        {
                                            var CustomerBalance = _ManageCoreTransaction.GetAppUserBalance(_RewardConfirmResponse.ReferenceId, true);
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _RewardConfirmResponse.ReferenceId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                                if (!string.IsNullOrEmpty(UserNotificationUrl))
                                                {
                                                    if (HostEnvironment == HostEnvironmentType.Live)
                                                    {
                                                        if (CountryIsd == "254")
                                                        {
                                                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "Ksh " + TUCRewardUserAmount + " rewards received.", "Your have received Ksh" + TUCRewardUserAmount + " from " + MerchantDisplayName + " for purchase of Ksh" + _Request.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                                        }
                                                        else
                                                        {
                                                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", " " + TUCRewardUserAmount + " rewards received.", "Your have received " + TUCRewardUserAmount + " from " + MerchantDisplayName + " for purchase of " + _Request.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST :  " + TUCRewardUserAmount + " rewards received.", "Your have received " + TUCRewardUserAmount + " from " + MerchantDisplayName + " for purchase of " + _Request.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                                    }
                                                }
                                                else
                                                {
                                                    #region Send SMS
                                                    string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString() + " points cash reward from " + MerchantDisplayName + "  Bal: " + HCoreHelper.RoundNumber((CustomerBalance + TUCRewardUserAmount), _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
                                                    HCoreHelper.SendSMS(SmsType.Transaction, CountryIsd, _Request.MobileNumber, Message, _Request.AccountId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
                                                    #endregion
                                                }
                                            }
                                            #region Send Email 
                                            if (!string.IsNullOrEmpty(CustomerEmailAddress))
                                            {
                                                var _EmailParameters = new
                                                {
                                                    UserDisplayName = CustomerDisplayName,
                                                    MerchantName = MerchantDisplayName,
                                                    InvoiceAmount = _Request.InvoiceAmount.ToString(),
                                                    Amount = HCoreHelper.RoundNumber(TUCRewardUserAmount, 2).ToString(),
                                                    Balance = CustomerBalance.ToString(),
                                                };
                                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RewardEmail, CustomerDisplayName, CustomerEmailAddress, _EmailParameters, _Request.UserReference);
                                            }
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        var CustomerBalance = _ManageCoreTransaction.GetAppUserBalance(_RewardConfirmResponse.ReferenceId, true);
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _RewardConfirmResponse.ReferenceId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                            if (!string.IsNullOrEmpty(UserNotificationUrl))
                                            {
                                                if (HostEnvironment == HostEnvironmentType.Live)
                                                {
                                                    if (CountryIsd == "254")
                                                    {
                                                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "Ksh " + TUCRewardUserAmount + " pending rewards received.", "Your have received Ksh" + TUCRewardUserAmount + " from " + MerchantDisplayName + " for purchase of Ksh" + _Request.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                                    }
                                                    else
                                                    {
                                                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", " " + TUCRewardUserAmount + " pending rewards received.", "Your have received " + TUCRewardUserAmount + " from " + MerchantDisplayName + " for purchase of " + _Request.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                                    }
                                                }
                                                else
                                                {
                                                    HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST : " + TUCRewardUserAmount + " pending rewards received.", "Your have received " + TUCRewardUserAmount + " from " + MerchantDisplayName + " for purchase of " + _Request.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                                }
                                            }
                                            else
                                            {
                                                string RewardSms = HCoreHelper.GetConfiguration("pendingrewardsms", MerchantId);
                                                if (!string.IsNullOrEmpty(RewardSms))
                                                {
                                                    #region Send SMS
                                                    string Message = RewardSms
                                                    .Replace("[AMOUNT]", HCoreHelper.RoundNumber(TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString())
                                                    .Replace("[BALANCE]", HCoreHelper.RoundNumber(CustomerBalance, _AppConfig.SystemExitRoundDouble).ToString())
                                                    .Replace("[MERCHANT]", MerchantDisplayName)
                                                    .Replace("[TCODE]", HCoreHelper.GenerateRandomNumber(4));
                                                    HCoreHelper.SendSMS(SmsType.Transaction, CountryIsd, _Request.MobileNumber, Message, _Request.AccountId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Send SMS
                                                    string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString() + " pending points cash reward from " + MerchantDisplayName + "  Bal: " + HCoreHelper.RoundNumber(CustomerBalance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
                                                    HCoreHelper.SendSMS(SmsType.Transaction, CountryIsd, _Request.MobileNumber, Message, _Request.AccountId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
                                                    #endregion
                                                }
                                            }
                                            if (!string.IsNullOrEmpty(_RewardConfirmResponse.EmailAddress))
                                            {
                                                var _EmailParameters = new
                                                {
                                                    UserDisplayName = _RewardConfirmResponse.DisplayName,
                                                    MerchantName = MerchantDisplayName,
                                                    InvoiceAmount = _Request.InvoiceAmount.ToString(),
                                                    Amount = HCoreHelper.RoundNumber(TUCRewardUserAmount, 2).ToString(),
                                                    Balance = CustomerBalance.ToString(),
                                                };
                                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.PendingRewardEmail, _RewardConfirmResponse.DisplayName, _RewardConfirmResponse.EmailAddress, _EmailParameters, _Request.UserReference);
                                            }
                                        }
                                    }

                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _RewardConfirmResponse, TUCCoreResource.CA1158, TUCCoreResource.CA1158M);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1166, TUCCoreResource.CA1166M);
                                }
                            }
                            else
                            {
                                string ReferenceNumber = "TRA" + HCoreHelper.GenerateDateString() + HCoreHelper.GetAccountIdString(MerchantId) + HCoreHelper.GetAccountIdString(_Request.AccountId);
                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.GroupKey = ReferenceNumber;
                                _CoreTransactionRequest.CustomerId = _Request.AccountId;
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
                                _CoreTransactionRequest.ParentId = MerchantId;
                                _CoreTransactionRequest.InvoiceAmount = _Request.InvoiceAmount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.InvoiceAmount;
                                _CoreTransactionRequest.ReferenceAmount = 0;
                                _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                if (_Request.UserReference.AccountTypeId == UserAccountType.MerchantCashier)
                                {
                                    _CoreTransactionRequest.CashierId = _Request.UserReference.AccountId;
                                }
                                else
                                {
                                    if (CashierId > 0)
                                    {
                                        _CoreTransactionRequest.CashierId = CashierId;
                                    }
                                }
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = MerchantId,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = TransactionType.CashReward,
                                    SourceId = TransactionSource.Merchant,
                                    Amount = 0,
                                    Charge = 0,
                                    Comission = 0,
                                    TotalAmount = 0,
                                });
                                if (TUCGold != null && !string.IsNullOrEmpty(TUCGold.Value) && TUCGold.Value != "0")
                                {
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = _RewardConfirmResponse.ReferenceId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.CashReward,
                                        SourceId = TransactionSource.TUCBlack,
                                        Amount = 0,
                                        TotalAmount = 0,
                                    });
                                }
                                else
                                {
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = _RewardConfirmResponse.ReferenceId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.CashReward,
                                        SourceId = TransactionSource.TUC,
                                        Amount = 0,
                                        TotalAmount = 0,
                                    });
                                }
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                    if (!string.IsNullOrEmpty(_RewardConfirmResponse.IconUrl))
                                    {
                                        _RewardConfirmResponse.IconUrl = _AppConfig.StorageUrl + _RewardConfirmResponse.IconUrl;
                                    }
                                    else
                                    {
                                        _RewardConfirmResponse.IconUrl = _AppConfig.Default_Icon;
                                    }
                                    _RewardConfirmResponse.RewardAmount = 0;
                                    _RewardConfirmResponse.InvoiceAmount = _Request.InvoiceAmount;
                                    _RewardConfirmResponse.Comment = _Request.Comment;
                                    _RewardConfirmResponse.TransactionReferenceId = TransactionResponse.ReferenceId;
                                    _RewardConfirmResponse.TransactionDate = TransactionResponse.TransactionDate;
                                    _RewardConfirmResponse.StatusName = "success";
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _RewardConfirmResponse, TUCCoreResource.CA1158, TUCCoreResource.CA1158M);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1166, TUCCoreResource.CA1166M);
                                }
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1157, TUCCoreResource.CA1157M);
                        }

                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "Reward_Confirm", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }


        //internal OResponse Reward(ORewardBulk.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        //if (string.IsNullOrEmpty(_Request.AccountNumber) && string.IsNullOrEmpty(_Request.MobileNumber))
        //        //{
        //        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1153, TUCCoreResource.CA1153M);
        //        //}
        //         if (string.IsNullOrEmpty(_Request.AccountKey))
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1154, TUCCoreResource.CA1154M);
        //        }
        //        else if (_Request.AccountId < 1)
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1155, TUCCoreResource.CA1155M);
        //        }
        //        else if (_Request.Customers.Count == 0)
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14161, TUCCoreResource.CAA14161M);
        //        }
        //        //else if (_Request.ReferenceId < 1 && string.IsNullOrEmpty(_Request.MobileNumber))
        //        //{
        //        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1160, TUCCoreResource.CA1160M);
        //        //}
        //        //else if (string.IsNullOrEmpty(_Request.ReferenceKey) && string.IsNullOrEmpty(_Request.MobileNumber))
        //        //{
        //        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1161, TUCCoreResource.CA1161M);
        //        //}
        //        //else if (_Request.InvoiceAmount < 1)
        //        //{
        //        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1162, TUCCoreResource.CA1162M);
        //        //}
        //        else
        //        {
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                //long CashierId = 0;
        //                string CustomerDisplayName = null;
        //                string CustomerEmailAddress = null;
        //                string CustomerAccountNumber = null;

        //                long MerchantId = 0;
        //                int MerchantCategoryId = 0;
        //                long MerchantStatusId = HelperStatus.Default.Inactive;
        //                string MerchantDisplayName = null;
        //                #region Merchant Details
        //                if (_Request.UserReference.AccountTypeId == UserAccountType.MerchantCashier || _Request.UserReference.AccountTypeId == UserAccountType.MerchantSubAccount)
        //                {
        //                    var MerchantDetails = _HCoreContext.HCUAccount
        //                    .Where(x => x.Owner.Owner.AccountTypeId == UserAccountType.Merchant
        //                        && x.Owner.Id == _Request.AccountId
        //                        && x.Owner.Guid == _Request.AccountKey)
        //                    .Select(x => new
        //                    {
        //                        ReferenceId = x.Owner.Owner.Id,
        //                        StatusId = x.Owner.Owner.StatusId,
        //                        MerchantDisplayName = x.Owner.Owner.DisplayName,
        //                        MerchantCategoryId = x.Owner.Owner.PrimaryCategoryId,
        //                    }).FirstOrDefault();
        //                    if (MerchantDetails != null)
        //                    {
        //                        if (MerchantDetails.MerchantCategoryId != null)
        //                        {
        //                            MerchantCategoryId = (int)MerchantDetails.MerchantCategoryId;
        //                        }
        //                        MerchantId = MerchantDetails.ReferenceId;
        //                        MerchantStatusId = MerchantDetails.StatusId;
        //                        MerchantDisplayName = MerchantDetails.MerchantDisplayName;
        //                    }
        //                    else
        //                    {
        //                        _HCoreContext.Dispose();
        //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
        //                    }
        //                }
        //                else
        //                {
        //                    var MerchantDetails = _HCoreContext.HCUAccount
        //                        .Where(x => x.AccountTypeId == UserAccountType.Merchant
        //                            && x.Id == _Request.AccountId
        //                            && x.Guid == _Request.AccountKey)
        //                        .Select(x => new
        //                        {
        //                            ReferenceId = x.Id,
        //                            StatusId = x.StatusId,
        //                            MerchantDisplayName = x.DisplayName,
        //                            MerchantCategoryId = x.PrimaryCategoryId,
        //                        }).FirstOrDefault();
        //                    if (MerchantDetails != null)
        //                    {
        //                        if (MerchantDetails.MerchantCategoryId != null)
        //                        {
        //                            MerchantCategoryId = (int)MerchantDetails.MerchantCategoryId;
        //                        }
        //                        CashierId = _HCoreContext.HCUAccount.Where(x => x.DisplayName == _Request.CashierCode && x.Owner.Owner.Id == MerchantDetails.ReferenceId).Select(x => x.Id).FirstOrDefault();
        //                        MerchantId = MerchantDetails.ReferenceId;
        //                        MerchantStatusId = MerchantDetails.StatusId;
        //                        MerchantDisplayName = MerchantDetails.MerchantDisplayName;
        //                    }
        //                    else
        //                    {
        //                        _HCoreContext.Dispose();
        //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
        //                    }
        //                }
        //                #endregion

        //                if (MerchantStatusId == HelperStatus.Default.Active)
        //                {
        //                    var TUCGold = HCoreHelper.GetConfigurationDetails("thankucashgold", MerchantId);
        //                    double TUCRewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", MerchantId)), _AppConfig.SystemRoundPercentage);
        //                    var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Isd == _Request.UserReference.CountryIsd).Select(x => new
        //                    {
        //                        Id = x.Id,
        //                        MobileNumberLength = x.MobileNumberLength,
        //                    }).FirstOrDefault();
        //                    foreach (var CustomerItem in _Request.Customers)
        //                    {
        //                        string TCheckAccountNumber = CustomerItem.AccountNumber;
        //                        string TCheckMobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, CustomerItem.AccountNumber, CountryDetails.MobileNumberLength);
        //                        string TCheckUsername = _AppConfig.AppUserPrefix + TCheckMobileNumber;



        //                    }





        //                    _RewardConfirmResponse = new OReward.Confirm.Response();
        //                    if (!string.IsNullOrEmpty(_Request.MobileNumber))
        //                    {
        //                        _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber, _Request.MobileNumber.Length);
        //                        string TMobileNumber = _AppConfig.AppUserPrefix + _Request.MobileNumber;
        //                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
        //                        && x.User.Username == TMobileNumber)
        //                       .Select(x => new OReward.Confirm.Response
        //                       {
        //                           ReferenceId = x.Id,
        //                           ReferenceKey = x.Guid,
        //                           DisplayName = x.DisplayName,
        //                           Name = x.Name,
        //                           IconUrl = x.IconStorage.Path,
        //                           MobileNumber = x.MobileNumber,
        //                           StatusId = x.StatusId,
        //                           AccountNumber = x.AccountCode,
        //                           EmailAddress = x.EmailAddress,
        //                       }).FirstOrDefault();
        //                        if (AccountDetails != null)
        //                        {
        //                            if (AccountDetails.StatusId == HelperStatus.Default.Active)
        //                            {
        //                                CustomerAccountNumber = AccountDetails.AccountNumber;
        //                                CustomerDisplayName = AccountDetails.DisplayName;
        //                                CustomerEmailAddress = AccountDetails.EmailAddress;
        //                                _RewardConfirmResponse.ReferenceId = AccountDetails.ReferenceId;
        //                                _RewardConfirmResponse.ReferenceKey = AccountDetails.ReferenceKey;
        //                                _RewardConfirmResponse.DisplayName = AccountDetails.DisplayName;
        //                                _RewardConfirmResponse.Name = AccountDetails.Name;
        //                                _RewardConfirmResponse.IconUrl = AccountDetails.IconUrl;
        //                                _RewardConfirmResponse.MobileNumber = AccountDetails.MobileNumber;
        //                                _RewardConfirmResponse.EmailAddress = AccountDetails.EmailAddress;
        //                                _RewardConfirmResponse.StatusId = AccountDetails.StatusId;
        //                                _RewardConfirmResponse.AccountNumber = AccountDetails.AccountNumber;
        //                            }
        //                            else
        //                            {
        //                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1303, TUCCoreResource.CA1303M);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            if (!string.IsNullOrEmpty(_Request.Nmae) && string.IsNullOrEmpty(_Request.Name))
        //                            {
        //                                _Request.Name = _Request.Nmae;
        //                            }
        //                            OAppProfile.Request _AppProfileRequest;
        //                            ManageCoreUserAccess _ManageCoreUserAccess;
        //                            _AppProfileRequest = new OAppProfile.Request();
        //                            _AppProfileRequest.OwnerId = _Request.UserReference.AccountId;
        //                            _AppProfileRequest.CreatedById = _Request.UserReference.AccountId;
        //                            _AppProfileRequest.MobileNumber = _Request.MobileNumber;
        //                            _AppProfileRequest.EmailAddress = _Request.EmailAddress;
        //                            _AppProfileRequest.DateOfBirth = _Request.DateOfBirth;
        //                            _AppProfileRequest.Name = _Request.Name;
        //                            if (!string.IsNullOrEmpty(_Request.Name))
        //                            {
        //                                _AppProfileRequest.DisplayName = _Request.Name;
        //                            }
        //                            else
        //                            {
        //                                _AppProfileRequest.DisplayName = _Request.MobileNumber;
        //                            }
        //                            if (!string.IsNullOrEmpty(_Request.GenderCode))
        //                            {
        //                                _AppProfileRequest.GenderCode = _Request.GenderCode.ToLower();
        //                            }
        //                            _AppProfileRequest.UserReference = _Request.UserReference;
        //                            _ManageCoreUserAccess = new ManageCoreUserAccess();
        //                            OAppProfile.Response _AppUserCreateResponse = _ManageCoreUserAccess.CreateAppUserAccount(_AppProfileRequest);
        //                            if (_AppUserCreateResponse != null && _AppUserCreateResponse.Status == ResponseStatus.Success)
        //                            {
        //                                CustomerAccountNumber = _AppUserCreateResponse.AccountCode;
        //                                CustomerEmailAddress = _Request.EmailAddress;
        //                                CustomerDisplayName = _Request.Name;
        //                                _RewardConfirmResponse.ReferenceId = _AppUserCreateResponse.AccountId;
        //                                _RewardConfirmResponse.ReferenceKey = _AppUserCreateResponse.AccountKey;
        //                                _RewardConfirmResponse.DisplayName = _AppUserCreateResponse.DisplayName;
        //                                _RewardConfirmResponse.Name = _AppUserCreateResponse.Name;
        //                                _RewardConfirmResponse.MobileNumber = _AppUserCreateResponse.MobileNumber;
        //                                _RewardConfirmResponse.StatusId = _AppUserCreateResponse.StatusId;
        //                                _RewardConfirmResponse.EmailAddress = _AppUserCreateResponse.EmailAddress;
        //                                _RewardConfirmResponse.AccountNumber = _AppUserCreateResponse.AccountCode;
        //                            }
        //                            else
        //                            {
        //                                #region Send Response
        //                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _AppUserCreateResponse.StatusResponseCode, _AppUserCreateResponse.StatusMessage);
        //                                #endregion
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
        //                     && (x.AccountCode == _Request.AccountNumber
        //                     || x.Guid == _Request.AccountNumber
        //                     || x.MobileNumber == _Request.AccountNumber))
        //                         .Select(x => new OReward.Confirm.Response
        //                         {
        //                             ReferenceId = x.Id,
        //                             ReferenceKey = x.Guid,
        //                             DisplayName = x.DisplayName,
        //                             Name = x.Name,
        //                             IconUrl = x.IconStorage.Path,
        //                             MobileNumber = x.MobileNumber,
        //                             StatusId = x.StatusId,
        //                             AccountNumber = x.AccountCode,
        //                         }).FirstOrDefault();
        //                        if (AccountDetails != null)
        //                        {
        //                            if (AccountDetails.StatusId == HelperStatus.Default.Active)
        //                            {
        //                                _RewardConfirmResponse.ReferenceId = AccountDetails.ReferenceId;
        //                                _RewardConfirmResponse.ReferenceKey = AccountDetails.ReferenceKey;
        //                                _RewardConfirmResponse.DisplayName = AccountDetails.DisplayName;
        //                                _RewardConfirmResponse.Name = AccountDetails.Name;
        //                                _RewardConfirmResponse.IconUrl = AccountDetails.IconUrl;
        //                                _RewardConfirmResponse.MobileNumber = AccountDetails.MobileNumber;
        //                                _RewardConfirmResponse.EmailAddress = AccountDetails.EmailAddress;
        //                                _RewardConfirmResponse.StatusId = AccountDetails.StatusId;
        //                                _RewardConfirmResponse.AccountNumber = AccountDetails.AccountNumber;
        //                            }
        //                            else
        //                            {
        //                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1159, TUCCoreResource.CA1159M);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1317, TUCCoreResource.CA1317M);
        //                        }
        //                    }


        //                    if (TUCRewardPercentage > 0)
        //                    {
        //                        double TUCRewardAmount = HCoreHelper.GetPercentage(_Request.InvoiceAmount, TUCRewardPercentage, _AppConfig.SystemEntryRoundDouble);
        //                        if (TUCRewardAmount <= 0)
        //                        {
        //                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1172, TUCCoreResource.CA1172M);
        //                        }
        //                        if (TUCRewardAmount > _Request.InvoiceAmount)
        //                        {
        //                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14138, TUCCoreResource.CAA14138M);
        //                        }
        //                        string ReferenceNumber = "TRA" + HCoreHelper.GenerateDateString() + HCoreHelper.GetAccountIdString(MerchantId) + HCoreHelper.GetAccountIdString(_Request.AccountId);
        //                        int ThankUCashPercentageFromRewardAmount = Convert.ToInt32(HCoreHelper.GetConfiguration("rewardcommissionfromrewardamount", MerchantId));
        //                        int TransactionStatusId = HelperStatus.Transaction.Success;
        //                        double TUCRewardUserAmount = 0;
        //                        double TUCRewardUserAmountPercentage = 0;
        //                        double TUCRewardCommissionAmount = 0;
        //                        double TUCRewardCommissionAmountPercentage = 0;
        //                        if (ThankUCashPercentageFromRewardAmount == 1)   // 70 - 30 Commission Gen 1 
        //                        {
        //                            var RewardDeductionType = HCoreHelper.GetConfigurationDetails("rewarddeductiontype", MerchantId);
        //                            TUCRewardUserAmountPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("userrewardpercentage", MerchantId)), _AppConfig.SystemRoundPercentage);
        //                            TUCRewardUserAmount = HCoreHelper.GetPercentage(TUCRewardAmount, TUCRewardUserAmountPercentage);
        //                            TUCRewardCommissionAmount = TUCRewardAmount - TUCRewardUserAmount;
        //                            TUCRewardCommissionAmountPercentage = (100 - TUCRewardUserAmountPercentage);
        //                            if (RewardDeductionType.TypeCode == "rewarddeductiontype.prepay")
        //                            {
        //                                _ManageCoreTransaction = new ManageCoreTransaction();
        //                                double MerchantBalance = _ManageCoreTransaction.GetMerchantBalance(MerchantId, TransactionSource.Merchant);
        //                                if (MerchantBalance >= TUCRewardAmount)
        //                                {
        //                                    TransactionStatusId = HelperStatus.Transaction.Success;
        //                                }
        //                                else
        //                                {
        //                                    TransactionStatusId = HelperStatus.Transaction.Pending;
        //                                }
        //                            }
        //                            else if (RewardDeductionType.TypeCode == "rewarddeductiontype.postpay")
        //                            {
        //                                TransactionStatusId = HelperStatus.Transaction.Success;
        //                            }
        //                            else
        //                            {
        //                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14135, TUCCoreResource.CAA14135M);
        //                            }
        //                        }
        //                        else   // Custom Calculation Gen 2  || Merchant Commission Extra to the Reward Amount
        //                        {
        //                            int TAllowCustomReward = Convert.ToInt32(HCoreHelper.GetConfiguration("customreward", MerchantId));
        //                            if (TAllowCustomReward == 1)
        //                            {
        //                                if (_Request.RewardAmount > 0)
        //                                {
        //                                    TUCRewardAmount = _Request.RewardAmount;
        //                                }
        //                            }
        //                            double MaximumCommissionAmount = 2500;
        //                            double SystemDefaultCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardcomissionpercentage")), _AppConfig.SystemRoundPercentage);
        //                            double SystemMerchantCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetAccountConfiguration("rewardcomissionpercentage", MerchantId)), _AppConfig.SystemRoundPercentage);
        //                            double SystemCategoryCommissionPercentage = 0;
        //                            if (MerchantCategoryId > 0)
        //                            {
        //                                using (_HCoreContext = new HCoreContext())
        //                                {
        //                                    var CategoryCommission = _HCoreContext.TUCMerchantCategory.Where(x => x.RootCategoryId == MerchantCategoryId)
        //                                        .Select(x => new
        //                                        {
        //                                            Commission = x.Commission,
        //                                        }).FirstOrDefault();
        //                                    _HCoreContext.Dispose();
        //                                    if (CategoryCommission != null && CategoryCommission.Commission > 0)
        //                                    {
        //                                        SystemCategoryCommissionPercentage = (double)CategoryCommission.Commission;
        //                                    }
        //                                }
        //                            }
        //                            if (SystemMerchantCommissionPercentage > 0)
        //                            {
        //                                TUCRewardCommissionAmountPercentage = SystemMerchantCommissionPercentage;
        //                            }
        //                            else if (SystemCategoryCommissionPercentage > 0)
        //                            {
        //                                TUCRewardCommissionAmountPercentage = SystemCategoryCommissionPercentage;
        //                            }
        //                            else
        //                            {
        //                                TUCRewardCommissionAmountPercentage = SystemDefaultCommissionPercentage;
        //                            }

        //                            TUCRewardCommissionAmount = HCoreHelper.GetPercentage(_Request.InvoiceAmount, TUCRewardCommissionAmountPercentage);
        //                            if (TUCGold != null && TUCGold.Value == "1")
        //                            {
        //                                TUCRewardCommissionAmount = 0;
        //                            }
        //                            if (TUCRewardCommissionAmount > MaximumCommissionAmount)
        //                            {
        //                                TUCRewardCommissionAmount = MaximumCommissionAmount;
        //                            }
        //                            TUCRewardUserAmount = TUCRewardAmount;
        //                            TUCRewardUserAmountPercentage = TUCRewardPercentage;
        //                            TUCRewardAmount = TUCRewardUserAmount + TUCRewardCommissionAmount;
        //                            if (TUCRewardAmount > _Request.InvoiceAmount)
        //                            {
        //                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14138, TUCCoreResource.CAA14138M);
        //                            }
        //                            if (TUCGold != null && TUCGold.Value == "1")
        //                            {
        //                                TransactionStatusId = HelperStatus.Transaction.Success;
        //                            }
        //                            else
        //                            {
        //                                _ManageCoreTransaction = new ManageCoreTransaction();
        //                                double MerchantBalance = _ManageCoreTransaction.GetMerchantBalance(MerchantId, TransactionSource.Merchant);
        //                                if (MerchantBalance >= TUCRewardAmount)
        //                                {
        //                                    TransactionStatusId = HelperStatus.Transaction.Success;
        //                                }
        //                                else
        //                                {
        //                                    if (MerchantBalance > -5000)
        //                                    {
        //                                        if ((MerchantBalance - TUCRewardAmount) > -5000)
        //                                        {
        //                                            TransactionStatusId = HelperStatus.Transaction.Pending;
        //                                        }
        //                                        else
        //                                        {
        //                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14137, TUCCoreResource.CAA14137M);
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14136, TUCCoreResource.CAA14136M);
        //                                    }
        //                                }

        //                            }
        //                        }
        //                        _CoreTransactionRequest = new OCoreTransaction.Request();
        //                        _CoreTransactionRequest.GroupKey = ReferenceNumber;
        //                        _CoreTransactionRequest.CustomerId = _Request.AccountId;
        //                        _CoreTransactionRequest.UserReference = _Request.UserReference;
        //                        _CoreTransactionRequest.StatusId = TransactionStatusId;
        //                        _CoreTransactionRequest.ParentId = MerchantId;
        //                        _CoreTransactionRequest.InvoiceAmount = _Request.InvoiceAmount;
        //                        _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.InvoiceAmount;
        //                        _CoreTransactionRequest.ReferenceAmount = TUCRewardAmount;
        //                        _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
        //                        if (_Request.UserReference.AccountTypeId == UserAccountType.MerchantCashier)
        //                        {
        //                            _CoreTransactionRequest.CashierId = _Request.UserReference.AccountId;
        //                        }
        //                        else
        //                        {
        //                            if (CashierId > 0)
        //                            {
        //                                _CoreTransactionRequest.CashierId = CashierId;
        //                            }
        //                        }
        //                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
        //                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                        {
        //                            UserAccountId = MerchantId,
        //                            ModeId = TransactionMode.Debit,
        //                            TypeId = TransactionType.CashReward,
        //                            SourceId = TransactionSource.Merchant,
        //                            Amount = TUCRewardAmount,
        //                            Charge = 0,
        //                            Comission = TUCRewardCommissionAmount,
        //                            TotalAmount = TUCRewardAmount,
        //                        });
        //                        if (TUCGold != null && !string.IsNullOrEmpty(TUCGold.Value) && TUCGold.Value != "0")
        //                        {
        //                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                            {
        //                                UserAccountId = _RewardConfirmResponse.ReferenceId,
        //                                ModeId = TransactionMode.Credit,
        //                                TypeId = TransactionType.CashReward,
        //                                SourceId = TransactionSource.TUCBlack,
        //                                Amount = TUCRewardUserAmount,
        //                                TotalAmount = TUCRewardUserAmount,
        //                            });
        //                        }
        //                        else
        //                        {
        //                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                            {
        //                                UserAccountId = _RewardConfirmResponse.ReferenceId,
        //                                ModeId = TransactionMode.Credit,
        //                                TypeId = TransactionType.CashReward,
        //                                SourceId = TransactionSource.TUC,
        //                                Amount = TUCRewardUserAmount,
        //                                TotalAmount = TUCRewardUserAmount,
        //                            });
        //                        }

        //                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                        {
        //                            UserAccountId = SystemAccounts.ThankUCashMerchant,
        //                            ModeId = TransactionMode.Credit,
        //                            TypeId = TransactionType.CashReward,
        //                            SourceId = TransactionSource.Settlement,
        //                            Amount = TUCRewardCommissionAmount,
        //                            TotalAmount = TUCRewardCommissionAmount,
        //                        });
        //                        _CoreTransactionRequest.Transactions = _TransactionItems;
        //                        _ManageCoreTransaction = new ManageCoreTransaction();
        //                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
        //                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
        //                        {
        //                            if (!string.IsNullOrEmpty(_RewardConfirmResponse.IconUrl))
        //                            {
        //                                _RewardConfirmResponse.IconUrl = _AppConfig.StorageUrl + _RewardConfirmResponse.IconUrl;
        //                            }
        //                            else
        //                            {
        //                                _RewardConfirmResponse.IconUrl = _AppConfig.Default_Icon;
        //                            }
        //                            _RewardConfirmResponse.RewardAmount = TUCRewardAmount;
        //                            _RewardConfirmResponse.InvoiceAmount = _Request.InvoiceAmount;
        //                            _RewardConfirmResponse.Comment = _Request.Comment;
        //                            _RewardConfirmResponse.TransactionReferenceId = TransactionResponse.ReferenceId;
        //                            _RewardConfirmResponse.TransactionDate = TransactionResponse.TransactionDate;
        //                            if (TransactionStatusId == HelperStatus.Transaction.Success)
        //                            {
        //                                _RewardConfirmResponse.StatusName = "success";
        //                            }
        //                            else
        //                            {
        //                                _RewardConfirmResponse.StatusName = "pending";
        //                            }
        //                            if (TransactionStatusId == HelperStatus.Transaction.Success)
        //                            {
        //                                if (MerchantId == 583505 || MerchantId == 13)
        //                                {
        //                                    var CustomerBalance = _ManageCoreTransaction.GetAppUserBalance(_RewardConfirmResponse.ReferenceId, MerchantId, TransactionSource.TUCBlack);
        //                                    if (!string.IsNullOrEmpty(_Request.MobileNumber))
        //                                    {
        //                                        string Message = "Credit Alert: You have earned " + TUCRewardAmount + " WakaPoints. Your current balance is: " + CustomerBalance + " points. Book now on https://www.wakanow.com";
        //                                        HCoreHelper.SendSMS(SmsType.Transaction, "234", _Request.MobileNumber, Message, _RewardConfirmResponse.ReferenceId, null);
        //                                    }
        //                                    if (!string.IsNullOrEmpty(CustomerEmailAddress)) // Wakanow email
        //                                    {
        //                                        string Template = "";
        //                                        string currentDirectory = Directory.GetCurrentDirectory();
        //                                        string path = "templates";
        //                                        string fullPath = Path.Combine(currentDirectory, path, "wakanow_alert_reward.html");
        //                                        using (StreamReader reader = File.OpenText(fullPath))
        //                                        {
        //                                            Template = reader.ReadToEnd();
        //                                        }
        //                                        var _EmailParameters = new
        //                                        {
        //                                            MerchantLogo = "https://points.wakanow.com/assets/img/wakanow-logo.png",
        //                                            MerchantDisplayName = "WakaPoints",
        //                                            UserDisplayName = CustomerDisplayName,
        //                                            AccountNumber = CustomerAccountNumber,
        //                                            Amount = TUCRewardAmount.ToString(),
        //                                            Balance = CustomerBalance.ToString(),
        //                                        };
        //                                        Template = Template.Replace("{{MerchantLogo}}", _EmailParameters.MerchantLogo);
        //                                        Template = Template.Replace("{{MerchantDisplayName}}", _EmailParameters.MerchantDisplayName);
        //                                        Template = Template.Replace("{{UserDisplayName}}", _EmailParameters.UserDisplayName);
        //                                        Template = Template.Replace("{{AccountNumber}}", _EmailParameters.AccountNumber);
        //                                        Template = Template.Replace("{{Amount}}", _EmailParameters.Amount);
        //                                        Template = Template.Replace("{{Balance}}", _EmailParameters.Balance);
        //                                        var client = new SendGridClient("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");
        //                                        var msg = new SendGridMessage()
        //                                        {
        //                                            From = new EmailAddress("noreply@wakanow.com", "WakaPoints"),
        //                                            Subject = "WakaPoints Credit Alert - " + _EmailParameters.Amount + " points rewarded",
        //                                            HtmlContent = Template
        //                                        };
        //                                        msg.AddTo(new EmailAddress(CustomerEmailAddress, CustomerDisplayName));
        //                                        var response = client.SendEmailAsync(msg);
        //                                    }

        //                                }
        //                                else
        //                                {
        //                                    var CustomerBalance = _ManageCoreTransaction.GetAppUserBalance(_RewardConfirmResponse.ReferenceId, true);
        //                                    using (_HCoreContext = new HCoreContext())
        //                                    {
        //                                        string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _RewardConfirmResponse.ReferenceId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
        //                                        if (!string.IsNullOrEmpty(UserNotificationUrl))
        //                                        {
        //                                            if (HostEnvironment == HostEnvironmentType.Live)
        //                                            {
        //                                                HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "N " + TUCRewardUserAmount + " rewards received.", "Your have received N" + TUCRewardUserAmount + " from " + MerchantDisplayName + " for purchase of N" + _Request.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
        //                                            }
        //                                            else
        //                                            {
        //                                                HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST : N " + TUCRewardUserAmount + " rewards received.", "Your have received N" + TUCRewardUserAmount + " from " + MerchantDisplayName + " for purchase of N" + _Request.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            #region Send SMS
        //                                            string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString() + " points cash reward from " + MerchantDisplayName + "  Bal: N" + HCoreHelper.RoundNumber((CustomerBalance + TUCRewardUserAmount), _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
        //                                            HCoreHelper.SendSMS(SmsType.Transaction, "234", _Request.MobileNumber, Message, _Request.AccountId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
        //                                            #endregion
        //                                        }
        //                                    }
        //                                    #region Send Email 
        //                                    if (!string.IsNullOrEmpty(CustomerEmailAddress))
        //                                    {
        //                                        var _EmailParameters = new
        //                                        {
        //                                            UserDisplayName = CustomerDisplayName,
        //                                            MerchantName = MerchantDisplayName,
        //                                            InvoiceAmount = _Request.InvoiceAmount.ToString(),
        //                                            Amount = HCoreHelper.RoundNumber(TUCRewardUserAmount, 2).ToString(),
        //                                            Balance = CustomerBalance.ToString(),
        //                                        };
        //                                        HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RewardEmail, CustomerDisplayName, CustomerEmailAddress, _EmailParameters, _Request.UserReference);
        //                                    }
        //                                    #endregion
        //                                }
        //                            }
        //                            else
        //                            {
        //                                var CustomerBalance = _ManageCoreTransaction.GetAppUserBalance(_RewardConfirmResponse.ReferenceId, true);
        //                                using (_HCoreContext = new HCoreContext())
        //                                {
        //                                    string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _RewardConfirmResponse.ReferenceId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
        //                                    if (!string.IsNullOrEmpty(UserNotificationUrl))
        //                                    {
        //                                        if (HostEnvironment == HostEnvironmentType.Live)
        //                                        {
        //                                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "N " + TUCRewardUserAmount + " pending rewards received.", "Your have received N" + TUCRewardUserAmount + " from " + MerchantDisplayName + " for purchase of N" + _Request.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
        //                                        }
        //                                        else
        //                                        {
        //                                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST : N " + TUCRewardUserAmount + " pending rewards received.", "Your have received N" + TUCRewardUserAmount + " from " + MerchantDisplayName + " for purchase of N" + _Request.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        string RewardSms = HCoreHelper.GetConfiguration("pendingrewardsms", MerchantId);
        //                                        if (!string.IsNullOrEmpty(RewardSms))
        //                                        {
        //                                            #region Send SMS
        //                                            string Message = RewardSms
        //                                            .Replace("[AMOUNT]", HCoreHelper.RoundNumber(TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString())
        //                                            .Replace("[BALANCE]", HCoreHelper.RoundNumber(CustomerBalance, _AppConfig.SystemExitRoundDouble).ToString())
        //                                            .Replace("[MERCHANT]", MerchantDisplayName)
        //                                            .Replace("[TCODE]", HCoreHelper.GenerateRandomNumber(4));
        //                                            HCoreHelper.SendSMS(SmsType.Transaction, "234", _Request.MobileNumber, Message, _Request.AccountId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
        //                                            #endregion
        //                                        }
        //                                        else
        //                                        {
        //                                            #region Send SMS
        //                                            string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString() + " pending points cash reward from " + MerchantDisplayName + "  Bal: N" + HCoreHelper.RoundNumber(CustomerBalance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
        //                                            HCoreHelper.SendSMS(SmsType.Transaction, "234", _Request.MobileNumber, Message, _Request.AccountId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
        //                                            #endregion
        //                                        }
        //                                    }
        //                                    if (!string.IsNullOrEmpty(_RewardConfirmResponse.EmailAddress))
        //                                    {
        //                                        var _EmailParameters = new
        //                                        {
        //                                            UserDisplayName = _RewardConfirmResponse.DisplayName,
        //                                            MerchantName = MerchantDisplayName,
        //                                            InvoiceAmount = _Request.InvoiceAmount.ToString(),
        //                                            Amount = HCoreHelper.RoundNumber(TUCRewardUserAmount, 2).ToString(),
        //                                            Balance = CustomerBalance.ToString(),
        //                                        };
        //                                        HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.PendingRewardEmail, _RewardConfirmResponse.DisplayName, _RewardConfirmResponse.EmailAddress, _EmailParameters, _Request.UserReference);
        //                                    }
        //                                }
        //                            }

        //                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _RewardConfirmResponse, TUCCoreResource.CA1158, TUCCoreResource.CA1158M);
        //                        }
        //                        else
        //                        {
        //                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1166, TUCCoreResource.CA1166M);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string ReferenceNumber = "TRA" + HCoreHelper.GenerateDateString() + HCoreHelper.GetAccountIdString(MerchantId) + HCoreHelper.GetAccountIdString(_Request.AccountId);
        //                        _CoreTransactionRequest = new OCoreTransaction.Request();
        //                        _CoreTransactionRequest.GroupKey = ReferenceNumber;
        //                        _CoreTransactionRequest.CustomerId = _Request.AccountId;
        //                        _CoreTransactionRequest.UserReference = _Request.UserReference;
        //                        _CoreTransactionRequest.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
        //                        _CoreTransactionRequest.ParentId = MerchantId;
        //                        _CoreTransactionRequest.InvoiceAmount = _Request.InvoiceAmount;
        //                        _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.InvoiceAmount;
        //                        _CoreTransactionRequest.ReferenceAmount = 0;
        //                        _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
        //                        if (_Request.UserReference.AccountTypeId == UserAccountType.MerchantCashier)
        //                        {
        //                            _CoreTransactionRequest.CashierId = _Request.UserReference.AccountId;
        //                        }
        //                        else
        //                        {
        //                            if (CashierId > 0)
        //                            {
        //                                _CoreTransactionRequest.CashierId = CashierId;
        //                            }
        //                        }
        //                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
        //                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                        {
        //                            UserAccountId = MerchantId,
        //                            ModeId = TransactionMode.Debit,
        //                            TypeId = TransactionType.CashReward,
        //                            SourceId = TransactionSource.Merchant,
        //                            Amount = 0,
        //                            Charge = 0,
        //                            Comission = 0,
        //                            TotalAmount = 0,
        //                        });
        //                        if (TUCGold != null && !string.IsNullOrEmpty(TUCGold.Value) && TUCGold.Value != "0")
        //                        {
        //                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                            {
        //                                UserAccountId = _RewardConfirmResponse.ReferenceId,
        //                                ModeId = TransactionMode.Credit,
        //                                TypeId = TransactionType.CashReward,
        //                                SourceId = TransactionSource.TUCBlack,
        //                                Amount = 0,
        //                                TotalAmount = 0,
        //                            });
        //                        }
        //                        else
        //                        {
        //                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                            {
        //                                UserAccountId = _RewardConfirmResponse.ReferenceId,
        //                                ModeId = TransactionMode.Credit,
        //                                TypeId = TransactionType.CashReward,
        //                                SourceId = TransactionSource.TUC,
        //                                Amount = 0,
        //                                TotalAmount = 0,
        //                            });
        //                        }
        //                        _CoreTransactionRequest.Transactions = _TransactionItems;
        //                        _ManageCoreTransaction = new ManageCoreTransaction();
        //                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
        //                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
        //                        {
        //                            if (!string.IsNullOrEmpty(_RewardConfirmResponse.IconUrl))
        //                            {
        //                                _RewardConfirmResponse.IconUrl = _AppConfig.StorageUrl + _RewardConfirmResponse.IconUrl;
        //                            }
        //                            else
        //                            {
        //                                _RewardConfirmResponse.IconUrl = _AppConfig.Default_Icon;
        //                            }
        //                            _RewardConfirmResponse.RewardAmount = 0;
        //                            _RewardConfirmResponse.InvoiceAmount = _Request.InvoiceAmount;
        //                            _RewardConfirmResponse.Comment = _Request.Comment;
        //                            _RewardConfirmResponse.TransactionReferenceId = TransactionResponse.ReferenceId;
        //                            _RewardConfirmResponse.TransactionDate = TransactionResponse.TransactionDate;
        //                            _RewardConfirmResponse.StatusName = "success";
        //                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _RewardConfirmResponse, TUCCoreResource.CA1158, TUCCoreResource.CA1158M);
        //                        }
        //                        else
        //                        {
        //                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1166, TUCCoreResource.CA1166M);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1157, TUCCoreResource.CA1157M);
        //                }

        //            }
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        HCoreHelper.LogException(LogLevel.High, "Reward_Confirm", _Exception, _Request.UserReference);
        //        #endregion
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
        //    }
        //    #endregion
        //}

        public void RewardCustomer(OUpload.RewardUpload.Item DataItem)
        {
            try
            {

                using (_HCoreContext = new HCoreContext())
                {

                    string CustomerDisplayName = null;
                    string CustomerEmailAddress = null;
                    string CustomerAccountNumber = null;
                    OCoreTransaction.Request _CoreTransactionRequest;
                    ManageCoreTransaction _ManageCoreTransaction;
                    OReward.Confirm.Response _RewardConfirmResponse;
                    List<OCoreTransaction.TransactionItem> _TransactionItems;
                    _RewardConfirmResponse = new OReward.Confirm.Response();
                    if (!string.IsNullOrEmpty(DataItem.MobileNumber))
                    {
                        DataItem.MobileNumber = HCoreHelper.FormatMobileNumber(DataItem.CountryIsd, DataItem.MobileNumber, DataItem.MobileNumberLength);
                        var ValidateMobileNumber = HCoreHelper.ValidateMobileNumber(DataItem.MobileNumber);
                        if (ValidateMobileNumber.IsNumberValid == true)
                        {
                            var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Isd == ValidateMobileNumber.Isd)
                                       .Select(x => new
                                       {
                                           x.Id,
                                           x.MobileNumberLength,
                                       })
                                       .FirstOrDefault();
                            DataItem.MobileNumber = HCoreHelper.FormatMobileNumber(ValidateMobileNumber.Isd, DataItem.MobileNumber, CountryDetails.MobileNumberLength);
                            string TMobileNumber = _AppConfig.AppUserPrefix + DataItem.MobileNumber;

                            var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                            && x.User.Username == TMobileNumber)
                           .Select(x => new OReward.Confirm.Response
                           {
                               ReferenceId = x.Id,
                               ReferenceKey = x.Guid,
                               DisplayName = x.DisplayName,
                               Name = x.Name,
                               IconUrl = x.IconStorage.Path,
                               MobileNumber = x.MobileNumber,
                               StatusId = x.StatusId,
                               AccountNumber = x.AccountCode,
                               EmailAddress = x.EmailAddress,
                           }).FirstOrDefault();
                            if (AccountDetails != null)
                            {
                                if (AccountDetails.StatusId == HelperStatus.Default.Active)
                                {
                                    CustomerAccountNumber = AccountDetails.AccountNumber;
                                    CustomerDisplayName = AccountDetails.DisplayName;
                                    CustomerEmailAddress = AccountDetails.EmailAddress;
                                    _RewardConfirmResponse.ReferenceId = AccountDetails.ReferenceId;
                                    _RewardConfirmResponse.ReferenceKey = AccountDetails.ReferenceKey;
                                    _RewardConfirmResponse.DisplayName = AccountDetails.DisplayName;
                                    _RewardConfirmResponse.Name = AccountDetails.Name;
                                    _RewardConfirmResponse.IconUrl = AccountDetails.IconUrl;
                                    _RewardConfirmResponse.MobileNumber = AccountDetails.MobileNumber;
                                    _RewardConfirmResponse.EmailAddress = AccountDetails.EmailAddress;
                                    _RewardConfirmResponse.StatusId = AccountDetails.StatusId;
                                    _RewardConfirmResponse.AccountNumber = AccountDetails.AccountNumber;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(DataItem.Name) && string.IsNullOrEmpty(DataItem.Name))
                                {
                                    DataItem.Name = DataItem.Name;
                                }
                                OAppProfile.Request _AppProfileRequest;
                                ManageCoreUserAccess _ManageCoreUserAccess;
                                _AppProfileRequest = new OAppProfile.Request();
                                _AppProfileRequest.OwnerId = DataItem.MerchantId;
                                _AppProfileRequest.CreatedById = DataItem.MerchantId;
                                _AppProfileRequest.MobileNumber = DataItem.MobileNumber;
                                _AppProfileRequest.EmailAddress = DataItem.EmailAddress;
                                _AppProfileRequest.DateOfBirth = DataItem.DateOfBirth;
                                _AppProfileRequest.Name = DataItem.Name;
                                if (!string.IsNullOrEmpty(DataItem.Name))
                                {
                                    _AppProfileRequest.DisplayName = DataItem.Name;
                                }
                                else
                                {
                                    _AppProfileRequest.DisplayName = DataItem.MobileNumber;
                                }
                                if (!string.IsNullOrEmpty(DataItem.Gender))
                                {
                                    _AppProfileRequest.GenderCode = DataItem.Gender.ToLower();
                                }
                                _AppProfileRequest.UserReference = DataItem.UserReference;
                                _ManageCoreUserAccess = new ManageCoreUserAccess();
                                OAppProfile.Response _AppUserCreateResponse = _ManageCoreUserAccess.CreateAppUserAccount(_AppProfileRequest);
                                if (_AppUserCreateResponse != null && _AppUserCreateResponse.Status == ResponseStatus.Success)
                                {
                                    CustomerAccountNumber = _AppUserCreateResponse.AccountCode;
                                    CustomerEmailAddress = DataItem.EmailAddress;
                                    CustomerDisplayName = DataItem.Name;
                                    _RewardConfirmResponse.ReferenceId = _AppUserCreateResponse.AccountId;
                                    _RewardConfirmResponse.ReferenceKey = _AppUserCreateResponse.AccountKey;
                                    _RewardConfirmResponse.DisplayName = _AppUserCreateResponse.DisplayName;
                                    _RewardConfirmResponse.Name = _AppUserCreateResponse.Name;
                                    _RewardConfirmResponse.MobileNumber = _AppUserCreateResponse.MobileNumber;
                                    _RewardConfirmResponse.StatusId = _AppUserCreateResponse.StatusId;
                                    _RewardConfirmResponse.EmailAddress = _AppUserCreateResponse.EmailAddress;
                                    _RewardConfirmResponse.AccountNumber = _AppUserCreateResponse.AccountCode;
                                }
                            }
                            var TUCGold = HCoreHelper.GetConfigurationDetails("thankucashgold", DataItem.MerchantId);
                            double TUCRewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", DataItem.MerchantId)), _AppConfig.SystemRoundPercentage);
                            if (TUCRewardPercentage > 0)
                            {
                                double TUCRewardAmount = HCoreHelper.GetPercentage(DataItem.InvoiceAmount, TUCRewardPercentage, _AppConfig.SystemEntryRoundDouble);
                                if (TUCRewardAmount <= 0)
                                {
                                    return;
                                    // return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1172, TUCCoreResource.CA1172M);
                                }
                                if (TUCRewardAmount > DataItem.InvoiceAmount)
                                {
                                    return;
                                    // return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14138, TUCCoreResource.CAA14138M);
                                }
                                string ReferenceNumber = "TRA" + HCoreHelper.GenerateDateString() + HCoreHelper.GetAccountIdString(DataItem.MerchantId);
                                int ThankUCashPercentageFromRewardAmount = Convert.ToInt32(HCoreHelper.GetConfiguration("rewardcommissionfromrewardamount", DataItem.MerchantId));
                                int TransactionStatusId = HelperStatus.Transaction.Success;
                                double TUCRewardUserAmount = 0;
                                double TUCRewardUserAmountPercentage = 0;
                                double TUCRewardCommissionAmount = 0;
                                double TUCRewardCommissionAmountPercentage = 0;
                                if (ThankUCashPercentageFromRewardAmount == 1)   // 70 - 30 Commission Gen 1 
                                {
                                    var RewardDeductionType = HCoreHelper.GetConfigurationDetails("rewarddeductiontype", DataItem.MerchantId);
                                    TUCRewardUserAmountPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("userrewardpercentage", DataItem.MerchantId)), _AppConfig.SystemRoundPercentage);
                                    TUCRewardUserAmount = HCoreHelper.GetPercentage(TUCRewardAmount, TUCRewardUserAmountPercentage);
                                    TUCRewardCommissionAmount = TUCRewardAmount - TUCRewardUserAmount;
                                    TUCRewardCommissionAmountPercentage = (100 - TUCRewardUserAmountPercentage);
                                    if (RewardDeductionType.TypeCode == "rewarddeductiontype.prepay")
                                    {
                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                        double MerchantBalance = _ManageCoreTransaction.GetMerchantBalance(DataItem.MerchantId, TransactionSource.Merchant);
                                        if (MerchantBalance >= TUCRewardAmount)
                                        {
                                            TransactionStatusId = HelperStatus.Transaction.Success;
                                        }
                                        else
                                        {
                                            TransactionStatusId = HelperStatus.Transaction.Pending;
                                        }
                                    }
                                    else if (RewardDeductionType.TypeCode == "rewarddeductiontype.postpay")
                                    {
                                        TransactionStatusId = HelperStatus.Transaction.Success;
                                    }
                                    else
                                    {
                                        return;
                                        //  return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14135, TUCCoreResource.CAA14135M);
                                    }
                                }
                                else   // Custom Calculation Gen 2  || Merchant Commission Extra to the Reward Amount
                                {
                                    int TAllowCustomReward = Convert.ToInt32(HCoreHelper.GetConfiguration("customreward", DataItem.MerchantId));
                                    if (TAllowCustomReward == 1)
                                    {
                                        if (DataItem.RewardAmount > 0)
                                        {
                                            TUCRewardAmount = DataItem.RewardAmount;
                                        }
                                    }
                                    double MaximumCommissionAmount = 2500;
                                    double SystemDefaultCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardcomissionpercentage")), _AppConfig.SystemRoundPercentage);
                                    double SystemMerchantCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetAccountConfiguration("rewardcomissionpercentage", DataItem.MerchantId)), _AppConfig.SystemRoundPercentage);
                                    double SystemCategoryCommissionPercentage = 0;
                                    if (DataItem.MerchantCategoryId > 0)
                                    {
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var CategoryCommission = _HCoreContext.TUCMerchantCategory.Where(x => x.RootCategoryId == DataItem.MerchantCategoryId)
                                                .Select(x => new
                                                {
                                                    Commission = x.Commission,
                                                }).FirstOrDefault();
                                            _HCoreContext.Dispose();
                                            if (CategoryCommission != null && CategoryCommission.Commission > 0)
                                            {
                                                SystemCategoryCommissionPercentage = (double)CategoryCommission.Commission;
                                            }
                                        }
                                    }
                                    if (SystemMerchantCommissionPercentage > 0)
                                    {
                                        TUCRewardCommissionAmountPercentage = SystemMerchantCommissionPercentage;
                                    }
                                    else if (SystemCategoryCommissionPercentage > 0)
                                    {
                                        TUCRewardCommissionAmountPercentage = SystemCategoryCommissionPercentage;
                                    }
                                    else
                                    {
                                        TUCRewardCommissionAmountPercentage = SystemDefaultCommissionPercentage;
                                    }

                                    TUCRewardCommissionAmount = HCoreHelper.GetPercentage(DataItem.InvoiceAmount, TUCRewardCommissionAmountPercentage);
                                    if (TUCGold != null && TUCGold.Value == "1")
                                    {
                                        TUCRewardCommissionAmount = 0;
                                    }
                                    if (TUCRewardCommissionAmount > MaximumCommissionAmount)
                                    {
                                        TUCRewardCommissionAmount = MaximumCommissionAmount;
                                    }
                                    TUCRewardUserAmount = TUCRewardAmount;
                                    TUCRewardUserAmountPercentage = TUCRewardPercentage;
                                    TUCRewardAmount = TUCRewardUserAmount + TUCRewardCommissionAmount;
                                    if (TUCRewardAmount > DataItem.InvoiceAmount)
                                    {
                                        return;
                                        //  return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14138, TUCCoreResource.CAA14138M);
                                    }
                                    if (TUCGold != null && TUCGold.Value == "1")
                                    {
                                        TransactionStatusId = HelperStatus.Transaction.Success;
                                    }
                                    else
                                    {
                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                        double MerchantBalance = _ManageCoreTransaction.GetMerchantBalance(DataItem.MerchantId, TransactionSource.Merchant);
                                        if (MerchantBalance >= TUCRewardAmount)
                                        {
                                            TransactionStatusId = HelperStatus.Transaction.Success;
                                        }
                                        else
                                        {
                                            if (MerchantBalance > -5000)
                                            {
                                                if ((MerchantBalance - TUCRewardAmount) > -5000)
                                                {
                                                    TransactionStatusId = HelperStatus.Transaction.Pending;
                                                }
                                                else
                                                {
                                                    return;
                                                    // return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14137, TUCCoreResource.CAA14137M);
                                                }
                                            }
                                            else
                                            {
                                                return;
                                                // return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14136, TUCCoreResource.CAA14136M);
                                            }
                                        }

                                    }
                                }
                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.GroupKey = ReferenceNumber;
                                _CoreTransactionRequest.CustomerId = _RewardConfirmResponse.ReferenceId;
                                _CoreTransactionRequest.UserReference = DataItem.UserReference;
                                _CoreTransactionRequest.StatusId = TransactionStatusId;
                                _CoreTransactionRequest.ParentId = DataItem.MerchantId;
                                _CoreTransactionRequest.InvoiceAmount = DataItem.InvoiceAmount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = DataItem.InvoiceAmount;
                                _CoreTransactionRequest.ReferenceAmount = TUCRewardAmount;
                                _CoreTransactionRequest.ReferenceNumber = DataItem.ReferenceNumber;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = DataItem.MerchantId,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = TransactionType.CashReward,
                                    SourceId = TransactionSource.Merchant,
                                    Amount = TUCRewardAmount,
                                    Charge = 0,
                                    Comission = TUCRewardCommissionAmount,
                                    TotalAmount = TUCRewardAmount,
                                });
                                if (TUCGold != null && !string.IsNullOrEmpty(TUCGold.Value) && TUCGold.Value != "0")
                                {
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = _RewardConfirmResponse.ReferenceId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.CashReward,
                                        SourceId = TransactionSource.TUCBlack,
                                        Amount = TUCRewardUserAmount,
                                        TotalAmount = TUCRewardUserAmount,
                                    });
                                }
                                else
                                {
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = _RewardConfirmResponse.ReferenceId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.CashReward,
                                        SourceId = TransactionSource.TUC,
                                        Amount = TUCRewardUserAmount,
                                        TotalAmount = TUCRewardUserAmount,
                                    });
                                }

                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = SystemAccounts.ThankUCashMerchant,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionType.CashReward,
                                    SourceId = TransactionSource.Settlement,
                                    Amount = TUCRewardCommissionAmount,
                                    TotalAmount = TUCRewardCommissionAmount,
                                });
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                    if (!string.IsNullOrEmpty(_RewardConfirmResponse.IconUrl))
                                    {
                                        _RewardConfirmResponse.IconUrl = _AppConfig.StorageUrl + _RewardConfirmResponse.IconUrl;
                                    }
                                    else
                                    {
                                        _RewardConfirmResponse.IconUrl = _AppConfig.Default_Icon;
                                    }
                                    _RewardConfirmResponse.RewardAmount = TUCRewardAmount;
                                    _RewardConfirmResponse.InvoiceAmount = DataItem.InvoiceAmount;
                                    _RewardConfirmResponse.TransactionReferenceId = TransactionResponse.ReferenceId;
                                    _RewardConfirmResponse.TransactionDate = TransactionResponse.TransactionDate;
                                    if (TransactionStatusId == HelperStatus.Transaction.Success)
                                    {
                                        _RewardConfirmResponse.StatusName = "success";
                                    }
                                    else
                                    {
                                        _RewardConfirmResponse.StatusName = "pending";
                                    }
                                    if (TransactionStatusId == HelperStatus.Transaction.Success)
                                    {
                                        if (DataItem.MerchantId == 583505 || DataItem.MerchantId == 13)
                                        {
                                            var CustomerBalance = _ManageCoreTransaction.GetAppUserBalance(_RewardConfirmResponse.ReferenceId, DataItem.MerchantId, TransactionSource.TUCBlack);
                                            if (!string.IsNullOrEmpty(DataItem.MobileNumber))
                                            {
                                                string Message = "Credit Alert: You have earned " + TUCRewardAmount + " WakaPoints. Your current balance is: " + CustomerBalance + " points. Book now on https://www.wakanow.com";
                                                HCoreHelper.SendSMS(SmsType.Transaction, DataItem.CountryIsd, DataItem.MobileNumber, Message, _RewardConfirmResponse.ReferenceId, null);
                                            }
                                            if (!string.IsNullOrEmpty(CustomerEmailAddress)) // Wakanow email
                                            {
                                                string Template = "";
                                                string currentDirectory = Directory.GetCurrentDirectory();
                                                string path = "templates";
                                                string fullPath = Path.Combine(currentDirectory, path, "wakanow_alert_reward.html");
                                                using (StreamReader reader = File.OpenText(fullPath))
                                                {
                                                    Template = reader.ReadToEnd();
                                                }
                                                var _EmailParameters = new
                                                {
                                                    MerchantLogo = "https://points.wakanow.com/assets/img/wakanow-logo.png",
                                                    MerchantDisplayName = "WakaPoints",
                                                    UserDisplayName = CustomerDisplayName,
                                                    AccountNumber = CustomerAccountNumber,
                                                    Amount = TUCRewardAmount.ToString(),
                                                    Balance = CustomerBalance.ToString(),
                                                };
                                                Template = Template.Replace("{{MerchantLogo}}", _EmailParameters.MerchantLogo);
                                                Template = Template.Replace("{{MerchantDisplayName}}", _EmailParameters.MerchantDisplayName);
                                                Template = Template.Replace("{{UserDisplayName}}", _EmailParameters.UserDisplayName);
                                                Template = Template.Replace("{{AccountNumber}}", _EmailParameters.AccountNumber);
                                                Template = Template.Replace("{{Amount}}", _EmailParameters.Amount);
                                                Template = Template.Replace("{{Balance}}", _EmailParameters.Balance);
                                                var client = new SendGridClient("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");
                                                var msg = new SendGridMessage()
                                                {
                                                    From = new EmailAddress("noreply@wakanow.com", "WakaPoints"),
                                                    Subject = "WakaPoints Credit Alert - " + _EmailParameters.Amount + " points rewarded",
                                                    HtmlContent = Template
                                                };
                                                msg.AddTo(new EmailAddress(CustomerEmailAddress, CustomerDisplayName));
                                                var response = client.SendEmailAsync(msg);
                                            }

                                        }
                                        else
                                        {
                                            var CustomerBalance = _ManageCoreTransaction.GetAppUserBalance(_RewardConfirmResponse.ReferenceId, true);
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _RewardConfirmResponse.ReferenceId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                                if (!string.IsNullOrEmpty(UserNotificationUrl))
                                                {
                                                    if (HostEnvironment == HostEnvironmentType.Live)
                                                    {
                                                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", " " + TUCRewardUserAmount + " rewards received.", "Your have received " + TUCRewardUserAmount + " from " + DataItem.MerchantDisplayName + " for purchase of " + DataItem.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                                    }
                                                    else
                                                    {
                                                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST : " + TUCRewardUserAmount + " rewards received.", "Your have received " + TUCRewardUserAmount + " from " + DataItem.MerchantDisplayName + " for purchase of " + DataItem.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                                    }
                                                }
                                                else
                                                {
                                                    #region Send SMS
                                                    string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString() + " points cash reward from " + DataItem.MerchantDisplayName + "  Bal: " + HCoreHelper.RoundNumber((CustomerBalance + TUCRewardUserAmount), _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
                                                    HCoreHelper.SendSMS(SmsType.Transaction, DataItem.CountryIsd, DataItem.MobileNumber, Message, _RewardConfirmResponse.ReferenceId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
                                                    #endregion
                                                }
                                            }
                                            #region Send Email 
                                            if (!string.IsNullOrEmpty(CustomerEmailAddress))
                                            {
                                                var _EmailParameters = new
                                                {
                                                    UserDisplayName = CustomerDisplayName,
                                                    MerchantName = DataItem.MerchantDisplayName,
                                                    InvoiceAmount = DataItem.InvoiceAmount.ToString(),
                                                    Amount = HCoreHelper.RoundNumber(TUCRewardUserAmount, 2).ToString(),
                                                    Balance = CustomerBalance.ToString(),
                                                };
                                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RewardEmail, CustomerDisplayName, CustomerEmailAddress, _EmailParameters, DataItem.UserReference);
                                            }
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        var CustomerBalance = _ManageCoreTransaction.GetAppUserBalance(_RewardConfirmResponse.ReferenceId, true);
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _RewardConfirmResponse.ReferenceId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                            if (!string.IsNullOrEmpty(UserNotificationUrl))
                                            {
                                                if (HostEnvironment == HostEnvironmentType.Live)
                                                {
                                                    HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", " " + TUCRewardUserAmount + " pending rewards received.", "Your have received N" + TUCRewardUserAmount + " from " + DataItem.MerchantDisplayName + " for purchase of " + DataItem.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                                }
                                                else
                                                {
                                                    HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST : " + TUCRewardUserAmount + " pending rewards received.", "Your have received N" + TUCRewardUserAmount + " from " + DataItem.MerchantDisplayName + " for purchase of " + DataItem.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                                }
                                            }
                                            else
                                            {
                                                string RewardSms = HCoreHelper.GetConfiguration("pendingrewardsms", DataItem.MerchantId);
                                                if (!string.IsNullOrEmpty(RewardSms))
                                                {
                                                    #region Send SMS
                                                    string Message = RewardSms
                                                    .Replace("[AMOUNT]", HCoreHelper.RoundNumber(TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString())
                                                    .Replace("[BALANCE]", HCoreHelper.RoundNumber(CustomerBalance, _AppConfig.SystemExitRoundDouble).ToString())
                                                    .Replace("[MERCHANT]", DataItem.MerchantDisplayName)
                                                    .Replace("[TCODE]", HCoreHelper.GenerateRandomNumber(4));
                                                    HCoreHelper.SendSMS(SmsType.Transaction, DataItem.CountryIsd, DataItem.MobileNumber, Message, _RewardConfirmResponse.ReferenceId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Send SMS
                                                    string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString() + " pending points cash reward from " + DataItem.MerchantDisplayName + "  Bal: " + HCoreHelper.RoundNumber(CustomerBalance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
                                                    HCoreHelper.SendSMS(SmsType.Transaction, DataItem.CountryIsd, DataItem.MobileNumber, Message, _RewardConfirmResponse.ReferenceId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
                                                    #endregion
                                                }
                                            }
                                            if (!string.IsNullOrEmpty(_RewardConfirmResponse.EmailAddress))
                                            {
                                                var _EmailParameters = new
                                                {
                                                    UserDisplayName = _RewardConfirmResponse.DisplayName,
                                                    MerchantName = DataItem.MerchantDisplayName,
                                                    InvoiceAmount = DataItem.InvoiceAmount.ToString(),
                                                    Amount = HCoreHelper.RoundNumber(TUCRewardUserAmount, 2).ToString(),
                                                    Balance = CustomerBalance.ToString(),
                                                };
                                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.PendingRewardEmail, _RewardConfirmResponse.DisplayName, _RewardConfirmResponse.EmailAddress, _EmailParameters, DataItem.UserReference);
                                            }
                                        }
                                    }

                                    //   return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _RewardConfirmResponse, TUCCoreResource.CA1158, TUCCoreResource.CA1158M);
                                }
                                else
                                {
                                    return;
                                    // return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1166, TUCCoreResource.CA1166M);
                                }
                            }
                            else
                            {
                                string ReferenceNumber = "TRA" + HCoreHelper.GenerateDateString() + HCoreHelper.GetAccountIdString(DataItem.MerchantId) + HCoreHelper.GetAccountIdString(_RewardConfirmResponse.ReferenceId);
                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.GroupKey = ReferenceNumber;
                                _CoreTransactionRequest.CustomerId = _RewardConfirmResponse.ReferenceId;
                                _CoreTransactionRequest.UserReference = DataItem.UserReference;
                                _CoreTransactionRequest.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
                                _CoreTransactionRequest.ParentId = DataItem.MerchantId;
                                _CoreTransactionRequest.InvoiceAmount = DataItem.InvoiceAmount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = DataItem.InvoiceAmount;
                                _CoreTransactionRequest.ReferenceAmount = 0;
                                _CoreTransactionRequest.ReferenceNumber = DataItem.ReferenceNumber;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = DataItem.MerchantId,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = TransactionType.CashReward,
                                    SourceId = TransactionSource.Merchant,
                                    Amount = 0,
                                    Charge = 0,
                                    Comission = 0,
                                    TotalAmount = 0,
                                });
                                if (TUCGold != null && !string.IsNullOrEmpty(TUCGold.Value) && TUCGold.Value != "0")
                                {
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = _RewardConfirmResponse.ReferenceId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.CashReward,
                                        SourceId = TransactionSource.TUCBlack,
                                        Amount = 0,
                                        TotalAmount = 0,
                                    });
                                }
                                else
                                {
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = _RewardConfirmResponse.ReferenceId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.CashReward,
                                        SourceId = TransactionSource.TUC,
                                        Amount = 0,
                                        TotalAmount = 0,
                                    });
                                }
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                    if (!string.IsNullOrEmpty(_RewardConfirmResponse.IconUrl))
                                    {
                                        _RewardConfirmResponse.IconUrl = _AppConfig.StorageUrl + _RewardConfirmResponse.IconUrl;
                                    }
                                    else
                                    {
                                        _RewardConfirmResponse.IconUrl = _AppConfig.Default_Icon;
                                    }
                                    _RewardConfirmResponse.RewardAmount = 0;
                                    _RewardConfirmResponse.InvoiceAmount = DataItem.InvoiceAmount;
                                    _RewardConfirmResponse.TransactionReferenceId = TransactionResponse.ReferenceId;
                                    _RewardConfirmResponse.TransactionDate = TransactionResponse.TransactionDate;
                                    _RewardConfirmResponse.StatusName = "success";
                                    //  return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _RewardConfirmResponse, TUCCoreResource.CA1158, TUCCoreResource.CA1158M);
                                }
                                else
                                {
                                    return;
                                    //  return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1166, TUCCoreResource.CA1166M);
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("WakaNowCustomerReward", ex);
            }

        }

        public RewardCustomerResponse NewRewardCustomer(OUpload.RewardUpload.Item DataItem)
        {
            RewardCustomerResponse rewResponse = new RewardCustomerResponse
            {
                ReprocessFlag = RewardCustomerReprocessFlag.DoNotReprocess,
                ResponseMessage = string.Empty,
                Status = RewardCustomerStatus.Failed
            };

            try
            {
                using (_HCoreContext = new HCoreContext())
                {

                    var customerDisplayName = string.Empty;
                    string customerEmailAddress = string.Empty;
                    string customerAccountNumber = string.Empty;
                    OCoreTransaction.Request coreTransactionRequest;
                    ManageCoreTransaction manageCoreTransaction;
                    OReward.Confirm.Response rewardConfirmResponse = new();
                    List<OCoreTransaction.TransactionItem> transactionItems;

                    if (!string.IsNullOrEmpty(DataItem.MobileNumber))
                    {
                        DataItem.MobileNumber = HCoreHelper.FormatMobileNumber(DataItem.CountryIsd, DataItem.MobileNumber, DataItem.MobileNumberLength);
                        var validateMobileNumber = HCoreHelper.ValidateMobileNumber(DataItem.MobileNumber);
                        if (validateMobileNumber.IsNumberValid == true)
                        {
                            string tMobileNumber = _AppConfig.AppUserPrefix + DataItem.MobileNumber;

                            var accountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                            && x.User.Username == tMobileNumber)
                           .Select(x => new OReward.Confirm.Response
                           {
                               ReferenceId = x.Id,
                               ReferenceKey = x.Guid,
                               DisplayName = x.DisplayName,
                               Name = x.Name,
                               IconUrl = x.IconStorage != null ? x.IconStorage.Path : string.Empty,
                               MobileNumber = x.MobileNumber,
                               StatusId = x.StatusId,
                               AccountNumber = x.AccountCode,
                               EmailAddress = x.EmailAddress,
                           }).FirstOrDefault();


                            if (accountDetails != null)
                            {
                                if (accountDetails.StatusId == HelperStatus.Default.Active)
                                {
                                    customerAccountNumber = accountDetails.AccountNumber ?? string.Empty;
                                    customerDisplayName = accountDetails.DisplayName;
                                    customerEmailAddress = accountDetails.EmailAddress ?? string.Empty;

                                    rewardConfirmResponse.ReferenceId = accountDetails.ReferenceId;
                                    rewardConfirmResponse.ReferenceKey = accountDetails.ReferenceKey;
                                    rewardConfirmResponse.DisplayName = accountDetails.DisplayName;
                                    rewardConfirmResponse.Name = accountDetails.Name;
                                    rewardConfirmResponse.IconUrl = accountDetails.IconUrl;
                                    rewardConfirmResponse.MobileNumber = accountDetails.MobileNumber;
                                    rewardConfirmResponse.EmailAddress = accountDetails.EmailAddress;
                                    rewardConfirmResponse.StatusId = accountDetails.StatusId;
                                    rewardConfirmResponse.AccountNumber = accountDetails.AccountNumber;
                                }
                            }
                            else
                            {
                                OAppProfile.Request appProfileRequest = new OAppProfile.Request
                                {
                                    OwnerId = DataItem.MerchantId,
                                    CreatedById = DataItem.MerchantId,
                                    MobileNumber = DataItem.MobileNumber,
                                    EmailAddress = DataItem.EmailAddress,
                                    DateOfBirth = DataItem.DateOfBirth,
                                    Name = DataItem.Name,
                                    DisplayName = string.IsNullOrEmpty(DataItem.Name) ? DataItem.MobileNumber : DataItem.Name,
                                    GenderCode = string.IsNullOrEmpty(DataItem.Gender) ? null : DataItem.Gender.ToLower(),
                                    UserReference = DataItem.UserReference
                                };
                                ManageCoreUserAccess manageCoreUserAccess = new();
                                OAppProfile.Response appUserCreateResponse = manageCoreUserAccess.NewCreateAppUserAccount(appProfileRequest);
                                if (appUserCreateResponse != null)
                                {
                                    if (appUserCreateResponse.Status == ResponseStatus.Success)
                                    {
                                        customerAccountNumber = appUserCreateResponse.AccountCode ?? string.Empty;
                                        customerEmailAddress = DataItem.EmailAddress ?? string.Empty;
                                        customerDisplayName = DataItem.Name;

                                        rewardConfirmResponse.ReferenceId = appUserCreateResponse.AccountId;
                                        rewardConfirmResponse.ReferenceKey = appUserCreateResponse.AccountKey;
                                        rewardConfirmResponse.DisplayName = appUserCreateResponse.DisplayName;
                                        rewardConfirmResponse.Name = appUserCreateResponse.Name;
                                        rewardConfirmResponse.MobileNumber = appUserCreateResponse.MobileNumber;
                                        rewardConfirmResponse.StatusId = appUserCreateResponse.StatusId;
                                        rewardConfirmResponse.EmailAddress = appUserCreateResponse.EmailAddress;
                                        rewardConfirmResponse.AccountNumber = appUserCreateResponse.AccountCode;
                                    }
                                    else
                                    {
                                        var cardRelatedErrorResponseCodes = new List<string> { "AUA101", "AUA102", "AUA103", "AUA104" };
                                        rewResponse.ResponseMessage = appUserCreateResponse.StatusMessage;
                                        rewResponse.Status = RewardCustomerStatus.Failed;
                                        if (!string.IsNullOrEmpty(appUserCreateResponse.StatusResponseCode) &&
                                            cardRelatedErrorResponseCodes.Contains(appUserCreateResponse.StatusResponseCode))
                                        {
                                            //Failed - Dont Retry
                                            rewResponse.ReprocessFlag = RewardCustomerReprocessFlag.DoNotReprocess;
                                        }
                                        else
                                        {
                                            rewResponse.ReprocessFlag = RewardCustomerReprocessFlag.Reprocess;
                                        }
                                        return rewResponse;
                                    }
                                }
                                else
                                {
                                    //Failed - Retry
                                    return new RewardCustomerResponse
                                    {
                                        ResponseMessage = "User creation failed with error(s)",
                                        ReprocessFlag = RewardCustomerReprocessFlag.Reprocess,
                                        Status = RewardCustomerStatus.Failed
                                    };
                                }
                            }
                            var TUCGold = HCoreHelper.GetConfigurationDetails("thankucashgold", DataItem.MerchantId);
                            double TUCRewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", DataItem.MerchantId)), _AppConfig.SystemRoundPercentage);
                            if (TUCRewardPercentage > 0)
                            {
                                double TUCRewardAmount = HCoreHelper.GetPercentage(DataItem.InvoiceAmount, TUCRewardPercentage, _AppConfig.SystemEntryRoundDouble);
                                if (TUCRewardAmount <= 0)
                                {
                                    //PartlyProcessed - Dont retry
                                    return new RewardCustomerResponse
                                    {
                                        ResponseMessage = TUCCoreResource.CA1172M,
                                        ReprocessFlag = RewardCustomerReprocessFlag.DoNotReprocess,
                                        Status = RewardCustomerStatus.PartlyProcessed
                                    };
                                }
                                if (TUCRewardAmount > DataItem.InvoiceAmount)
                                {
                                    //PartlyProcessed - Dont retry
                                    return new RewardCustomerResponse
                                    {
                                        ResponseMessage = TUCCoreResource.CAA14138M,
                                        ReprocessFlag = RewardCustomerReprocessFlag.DoNotReprocess,
                                        Status = RewardCustomerStatus.PartlyProcessed
                                    };
                                }
                                string ReferenceNumber = "TRA" + HCoreHelper.GenerateDateString() + HCoreHelper.GetAccountIdString(DataItem.MerchantId);
                                int ThankUCashPercentageFromRewardAmount = Convert.ToInt32(HCoreHelper.GetConfiguration("rewardcommissionfromrewardamount", DataItem.MerchantId));
                                int TransactionStatusId = HelperStatus.Transaction.Success;
                                double TUCRewardUserAmount = 0;
                                double TUCRewardUserAmountPercentage = 0;
                                double TUCRewardCommissionAmount = 0;
                                double TUCRewardCommissionAmountPercentage = 0;
                                if (ThankUCashPercentageFromRewardAmount == 1)   // 70 - 30 Commission Gen 1 
                                {
                                    var RewardDeductionType = HCoreHelper.GetConfigurationDetails("rewarddeductiontype", DataItem.MerchantId);
                                    TUCRewardUserAmountPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("userrewardpercentage", DataItem.MerchantId)), _AppConfig.SystemRoundPercentage);
                                    TUCRewardUserAmount = HCoreHelper.GetPercentage(TUCRewardAmount, TUCRewardUserAmountPercentage);
                                    TUCRewardCommissionAmount = TUCRewardAmount - TUCRewardUserAmount;
                                    TUCRewardCommissionAmountPercentage = (100 - TUCRewardUserAmountPercentage);
                                    if (RewardDeductionType.TypeCode == "rewarddeductiontype.prepay")
                                    {
                                        manageCoreTransaction = new ManageCoreTransaction();
                                        double MerchantBalance = manageCoreTransaction.GetMerchantBalance(DataItem.MerchantId, TransactionSource.Merchant);
                                        if (MerchantBalance >= TUCRewardAmount)
                                        {
                                            TransactionStatusId = HelperStatus.Transaction.Success;
                                        }
                                        else
                                        {
                                            TransactionStatusId = HelperStatus.Transaction.Pending;
                                        }
                                    }
                                    else if (RewardDeductionType.TypeCode == "rewarddeductiontype.postpay")
                                    {
                                        TransactionStatusId = HelperStatus.Transaction.Success;
                                    }
                                    else
                                    {
                                        //PartlyProcessed - Dont retry
                                        return new RewardCustomerResponse
                                        {
                                            ResponseMessage = TUCCoreResource.CAA14135M,
                                            ReprocessFlag = RewardCustomerReprocessFlag.DoNotReprocess,
                                            Status = RewardCustomerStatus.PartlyProcessed
                                        };
                                    }
                                }
                                else   // Custom Calculation Gen 2  || Merchant Commission Extra to the Reward Amount
                                {
                                    int TAllowCustomReward = Convert.ToInt32(HCoreHelper.GetConfiguration("customreward", DataItem.MerchantId));
                                    if (TAllowCustomReward == 1)
                                    {
                                        if (DataItem.RewardAmount > 0)
                                        {
                                            TUCRewardAmount = DataItem.RewardAmount;
                                        }
                                    }
                                    double MaximumCommissionAmount = 2500;
                                    double SystemDefaultCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardcomissionpercentage")), _AppConfig.SystemRoundPercentage);
                                    double SystemMerchantCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetAccountConfiguration("rewardcomissionpercentage", DataItem.MerchantId)), _AppConfig.SystemRoundPercentage);
                                    double SystemCategoryCommissionPercentage = 0;
                                    if (DataItem.MerchantCategoryId > 0)
                                    {
                                        var CategoryCommission = _HCoreContext.TUCMerchantCategory.Where(x => x.RootCategoryId == DataItem.MerchantCategoryId)
                                                .Select(x => new
                                                {
                                                    Commission = x.Commission,
                                                }).FirstOrDefault();

                                        if (CategoryCommission != null && CategoryCommission.Commission > 0)
                                        {
                                            SystemCategoryCommissionPercentage = (double)CategoryCommission.Commission;
                                        }
                                    }

                                    TUCRewardCommissionAmountPercentage = SystemMerchantCommissionPercentage > 0 ? SystemMerchantCommissionPercentage :
                                         SystemCategoryCommissionPercentage > 0 ? SystemCategoryCommissionPercentage :
                                         SystemDefaultCommissionPercentage;

                                    TUCRewardCommissionAmount = HCoreHelper.GetPercentage(DataItem.InvoiceAmount, TUCRewardCommissionAmountPercentage);

                                    TUCRewardCommissionAmount = TUCGold != null && TUCGold.Value == "1" ? 0 : MaximumCommissionAmount;
                                    TUCRewardUserAmount = TUCRewardAmount;
                                    TUCRewardUserAmountPercentage = TUCRewardPercentage;
                                    TUCRewardAmount = TUCRewardUserAmount + TUCRewardCommissionAmount;
                                    if (TUCRewardAmount > DataItem.InvoiceAmount)
                                    {
                                        //Dont Retry
                                        return new RewardCustomerResponse
                                        {
                                            ResponseMessage = TUCCoreResource.CAA14138M,
                                            ReprocessFlag = RewardCustomerReprocessFlag.DoNotReprocess,
                                            Status = RewardCustomerStatus.PartlyProcessed
                                        };
                                    }

                                    if (TUCGold != null && TUCGold.Value == "1")
                                    {
                                        TransactionStatusId = HelperStatus.Transaction.Success;
                                    }
                                    else
                                    {
                                        manageCoreTransaction = new ManageCoreTransaction();
                                        double MerchantBalance = manageCoreTransaction.GetMerchantBalance(DataItem.MerchantId, TransactionSource.Merchant);
                                        if (MerchantBalance >= TUCRewardAmount)
                                        {
                                            TransactionStatusId = HelperStatus.Transaction.Success;
                                        }
                                        else
                                        {
                                            if (MerchantBalance > -5000)
                                            {
                                                if ((MerchantBalance - TUCRewardAmount) > -5000)
                                                {
                                                    TransactionStatusId = HelperStatus.Transaction.Pending;
                                                }
                                                else
                                                {
                                                    //PartlyProcessed - Dont retry
                                                    return new RewardCustomerResponse
                                                    {
                                                        ResponseMessage = TUCCoreResource.CAA14137M,
                                                        ReprocessFlag = RewardCustomerReprocessFlag.DoNotReprocess,
                                                        Status = RewardCustomerStatus.PartlyProcessed
                                                    };
                                                }
                                            }
                                            else
                                            {
                                                //PartlyProcessed - Dont retry
                                                return new RewardCustomerResponse
                                                {
                                                    ResponseMessage = TUCCoreResource.CAA14136M,
                                                    ReprocessFlag = RewardCustomerReprocessFlag.DoNotReprocess,
                                                    Status = RewardCustomerStatus.PartlyProcessed
                                                };

                                            }
                                        }

                                    }
                                }

                                coreTransactionRequest = new OCoreTransaction.Request();
                                coreTransactionRequest.GroupKey = ReferenceNumber;
                                coreTransactionRequest.CustomerId = rewardConfirmResponse.ReferenceId;
                                coreTransactionRequest.UserReference = DataItem.UserReference;
                                coreTransactionRequest.StatusId = TransactionStatusId;
                                coreTransactionRequest.ParentId = DataItem.MerchantId;
                                coreTransactionRequest.InvoiceAmount = DataItem.InvoiceAmount;
                                coreTransactionRequest.ReferenceInvoiceAmount = DataItem.InvoiceAmount;
                                coreTransactionRequest.ReferenceAmount = TUCRewardAmount;
                                coreTransactionRequest.ReferenceNumber = DataItem.ReferenceNumber;
                                coreTransactionRequest.GroupKey = "TPP" + DataItem.MerchantId + DataItem.MobileNumber + DataItem.InvoiceAmount;
                                transactionItems = new List<OCoreTransaction.TransactionItem>();

                                coreTransactionRequest.Transactions = ComputeTransactionItems(DataItem, rewardConfirmResponse.ReferenceId,
                                    TUCRewardAmount, TUCRewardUserAmount, TUCRewardCommissionAmount, TUCGold);

                                if (coreTransactionRequest.Transactions.Count > 0)
                                {
                                    manageCoreTransaction = new ManageCoreTransaction();
                                    OCoreTransaction.Response TransactionResponse = manageCoreTransaction.ProcessTransaction(coreTransactionRequest);
                                    if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                    {
                                        if (!string.IsNullOrEmpty(rewardConfirmResponse.IconUrl))
                                        {
                                            rewardConfirmResponse.IconUrl = _AppConfig.StorageUrl + rewardConfirmResponse.IconUrl;
                                        }
                                        else
                                        {
                                            rewardConfirmResponse.IconUrl = _AppConfig.Default_Icon;
                                        }
                                        rewardConfirmResponse.RewardAmount = TUCRewardAmount;
                                        rewardConfirmResponse.InvoiceAmount = DataItem.InvoiceAmount;
                                        rewardConfirmResponse.TransactionReferenceId = TransactionResponse.ReferenceId;
                                        rewardConfirmResponse.TransactionDate = TransactionResponse.TransactionDate;
                                        if (TransactionStatusId == HelperStatus.Transaction.Success)
                                        {
                                            rewardConfirmResponse.StatusName = "success";
                                        }
                                        else
                                        {
                                            rewardConfirmResponse.StatusName = "pending";
                                        }
                                        if (TransactionStatusId == HelperStatus.Transaction.Success)
                                        {
                                            if (DataItem.MerchantId == 583505 || DataItem.MerchantId == 13)
                                            {
                                                var CustomerBalance = manageCoreTransaction.GetAppUserBalance(rewardConfirmResponse.ReferenceId, DataItem.MerchantId, TransactionSource.TUCBlack);
                                                if (!string.IsNullOrEmpty(DataItem.MobileNumber))
                                                {
                                                    string Message = "Credit Alert: You have earned " + TUCRewardAmount + " WakaPoints. Your current balance is: " + CustomerBalance + " points. Book now on https://www.wakanow.com";
                                                    HCoreHelper.SendSMS(SmsType.Transaction, DataItem.CountryIsd, DataItem.MobileNumber, Message, rewardConfirmResponse.ReferenceId, null);
                                                }
                                                if (!string.IsNullOrEmpty(customerEmailAddress)) // Wakanow email
                                                {
                                                    string Template = "";
                                                    string currentDirectory = Directory.GetCurrentDirectory();
                                                    string path = "templates";
                                                    string fullPath = Path.Combine(currentDirectory, path, "wakanow_alert_reward.html");
                                                    using (StreamReader reader = File.OpenText(fullPath))
                                                    {
                                                        Template = reader.ReadToEnd();
                                                    }
                                                    var _EmailParameters = new
                                                    {
                                                        MerchantLogo = "https://points.wakanow.com/assets/img/wakanow-logo.png",
                                                        MerchantDisplayName = "WakaPoints",
                                                        UserDisplayName = customerDisplayName,
                                                        AccountNumber = customerAccountNumber,
                                                        Amount = TUCRewardAmount.ToString(),
                                                        Balance = CustomerBalance.ToString(),
                                                    };
                                                    Template = Template.Replace("{{MerchantLogo}}", _EmailParameters.MerchantLogo);
                                                    Template = Template.Replace("{{MerchantDisplayName}}", _EmailParameters.MerchantDisplayName);
                                                    Template = Template.Replace("{{UserDisplayName}}", _EmailParameters.UserDisplayName);
                                                    Template = Template.Replace("{{AccountNumber}}", _EmailParameters.AccountNumber);
                                                    Template = Template.Replace("{{Amount}}", _EmailParameters.Amount);
                                                    Template = Template.Replace("{{Balance}}", _EmailParameters.Balance);
                                                    var client = new SendGridClient("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");
                                                    var msg = new SendGridMessage()
                                                    {
                                                        From = new EmailAddress("noreply@wakanow.com", "WakaPoints"),
                                                        Subject = "WakaPoints Credit Alert - " + _EmailParameters.Amount + " points rewarded",
                                                        HtmlContent = Template
                                                    };
                                                    msg.AddTo(new EmailAddress(customerEmailAddress, customerDisplayName));
                                                    var response = client.SendEmailAsync(msg);
                                                }

                                            }
                                            else
                                            {
                                                var CustomerBalance = manageCoreTransaction.GetAppUserBalance(rewardConfirmResponse.ReferenceId, true);
                                                using (_HCoreContext = new HCoreContext())
                                                {
                                                    string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == rewardConfirmResponse.ReferenceId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                                    if (!string.IsNullOrEmpty(UserNotificationUrl))
                                                    {
                                                        if (HostEnvironment == HostEnvironmentType.Live)
                                                        {
                                                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", " " + TUCRewardUserAmount + " rewards received.", "Your have received N" + TUCRewardUserAmount + " from " + DataItem.MerchantDisplayName + " for purchase of N" + DataItem.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                                        }
                                                        else
                                                        {
                                                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST : " + TUCRewardUserAmount + " rewards received.", "Your have received N" + TUCRewardUserAmount + " from " + DataItem.MerchantDisplayName + " for purchase of N" + DataItem.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        #region Send SMS
                                                        string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString() + " points cash reward from " + DataItem.MerchantDisplayName + "  Bal: " + HCoreHelper.RoundNumber((CustomerBalance + TUCRewardUserAmount), _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
                                                        HCoreHelper.SendSMS(SmsType.Transaction, DataItem.CountryIsd, DataItem.MobileNumber, Message, rewardConfirmResponse.ReferenceId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
                                                        #endregion
                                                    }
                                                }
                                                #region Send Email 
                                                if (!string.IsNullOrEmpty(customerEmailAddress))
                                                {
                                                    var _EmailParameters = new
                                                    {
                                                        UserDisplayName = customerDisplayName,
                                                        MerchantName = DataItem.MerchantDisplayName,
                                                        InvoiceAmount = DataItem.InvoiceAmount.ToString(),
                                                        Amount = HCoreHelper.RoundNumber(TUCRewardUserAmount, 2).ToString(),
                                                        Balance = CustomerBalance.ToString(),
                                                    };
                                                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RewardEmail, customerDisplayName, customerEmailAddress, _EmailParameters, DataItem.UserReference);
                                                }
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            var CustomerBalance = manageCoreTransaction.GetAppUserBalance(rewardConfirmResponse.ReferenceId, true);
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == rewardConfirmResponse.ReferenceId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                                if (!string.IsNullOrEmpty(UserNotificationUrl))
                                                {
                                                    if (HostEnvironment == HostEnvironmentType.Live)
                                                    {
                                                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", " " + TUCRewardUserAmount + " pending rewards received.", "Your have received N" + TUCRewardUserAmount + " from " + DataItem.MerchantDisplayName + " for purchase of N" + DataItem.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                                    }
                                                    else
                                                    {
                                                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST : " + TUCRewardUserAmount + " pending rewards received.", "Your have received N" + TUCRewardUserAmount + " from " + DataItem.MerchantDisplayName + " for purchase of N" + DataItem.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                                    }
                                                }
                                                else
                                                {
                                                    string RewardSms = HCoreHelper.GetConfiguration("pendingrewardsms", DataItem.MerchantId);
                                                    if (!string.IsNullOrEmpty(RewardSms))
                                                    {
                                                        #region Send SMS
                                                        string Message = RewardSms
                                                        .Replace("[AMOUNT]", HCoreHelper.RoundNumber(TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString())
                                                        .Replace("[BALANCE]", HCoreHelper.RoundNumber(CustomerBalance, _AppConfig.SystemExitRoundDouble).ToString())
                                                        .Replace("[MERCHANT]", DataItem.MerchantDisplayName)
                                                        .Replace("[TCODE]", HCoreHelper.GenerateRandomNumber(4));
                                                        HCoreHelper.SendSMS(SmsType.Transaction, DataItem.CountryIsd, DataItem.MobileNumber, Message, rewardConfirmResponse.ReferenceId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
                                                        #endregion
                                                    }
                                                    else
                                                    {
                                                        #region Send SMS
                                                        string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString() + " pending points cash reward from " + DataItem.MerchantDisplayName + "  Bal: " + HCoreHelper.RoundNumber(CustomerBalance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
                                                        HCoreHelper.SendSMS(SmsType.Transaction, DataItem.CountryIsd, DataItem.MobileNumber, Message, rewardConfirmResponse.ReferenceId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
                                                        #endregion
                                                    }
                                                }
                                                if (!string.IsNullOrEmpty(rewardConfirmResponse.EmailAddress))
                                                {
                                                    var _EmailParameters = new
                                                    {
                                                        UserDisplayName = rewardConfirmResponse.DisplayName,
                                                        MerchantName = DataItem.MerchantDisplayName,
                                                        InvoiceAmount = DataItem.InvoiceAmount.ToString(),
                                                        Amount = HCoreHelper.RoundNumber(TUCRewardUserAmount, 2).ToString(),
                                                        Balance = CustomerBalance.ToString(),
                                                    };
                                                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.PendingRewardEmail, rewardConfirmResponse.DisplayName, rewardConfirmResponse.EmailAddress, _EmailParameters, DataItem.UserReference);
                                                }
                                            }
                                        }

                                        //Completed
                                        rewResponse = new RewardCustomerResponse
                                        {
                                            ResponseMessage = TransactionResponse.Message,
                                            ReprocessFlag = RewardCustomerReprocessFlag.DoNotReprocess,
                                            Status = RewardCustomerStatus.Completed
                                        };
                                    }
                                    else
                                    {
                                        //PartlyProcessed - Retry
                                        return new RewardCustomerResponse
                                        {
                                            ResponseMessage = TUCCoreResource.CA1166M,
                                            ReprocessFlag = RewardCustomerReprocessFlag.Reprocess,
                                            Status = RewardCustomerStatus.PartlyProcessed
                                        };
                                    }
                                }
                                else
                                {
                                    //Completed
                                    rewResponse = new RewardCustomerResponse
                                    {
                                        ResponseMessage = "Transactions already processed",
                                        ReprocessFlag = RewardCustomerReprocessFlag.DoNotReprocess,
                                        Status = RewardCustomerStatus.Completed
                                    };
                                }
                            }
                            else
                            {
                                //Completed
                                rewResponse = new RewardCustomerResponse
                                {
                                    ResponseMessage = "No reward processing",
                                    ReprocessFlag = RewardCustomerReprocessFlag.DoNotReprocess,
                                    Status = RewardCustomerStatus.Completed
                                };
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("WakaNowCustomerReward", ex);
                rewResponse.ResponseMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                rewResponse.ReprocessFlag = RewardCustomerReprocessFlag.Reprocess;
                rewResponse.Status = RewardCustomerStatus.Failed;
            }

            return rewResponse;
        }

        private List<OCoreTransaction.TransactionItem> ComputeTransactionItems(OUpload.RewardUpload.Item dataItem, long refId,
            double rewardAmount, double userRewardAmount, double commissionRewardAmount, OConfiguration? tucGold)
        {
            var result = new List<OCoreTransaction.TransactionItem>();

            try
            {
                var today = DateTime.Now.Date.ToString("yyyyMMdd");

                //Merchant Debit Trx
                var genText = dataItem.FileName + dataItem.MerchantId + dataItem.MobileNumber + dataItem.InvoiceAmount;
                var mdtHash = HCoreEncrypt.ComputeSha256Hash("MDT" + genText);

                //Customer Credit Trx
                var cctHash = HCoreEncrypt.ComputeSha256Hash("CCT" + genText);

                //TUC Credit Trx
                var tctHash = HCoreEncrypt.ComputeSha256Hash("TCT" + genText);

                using (var context = new HCoreContext())
                {
                    var mdtTrxExists = context.HCUAccountTransaction.Any(x => x.TransHash == mdtHash &&
                    x.ModeId == TransactionMode.Debit && x.TotalAmount == rewardAmount);

                    if (!mdtTrxExists)
                    {
                        result.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = dataItem.MerchantId,
                            ModeId = TransactionMode.Debit,
                            TypeId = TransactionType.CashReward,
                            SourceId = TransactionSource.Merchant,
                            Amount = rewardAmount,
                            Charge = 0,
                            Comission = commissionRewardAmount,
                            TotalAmount = rewardAmount,
                            TransactionHash = mdtHash
                        });
                    }

                    var cctTrxExists = context.HCUAccountTransaction.Any(x => x.TransHash == cctHash &&
                    x.ModeId == TransactionMode.Credit && x.TotalAmount == userRewardAmount);

                    if (!cctTrxExists)
                    {
                        if (tucGold != null && !string.IsNullOrEmpty(tucGold.Value) && tucGold.Value != "0")
                        {
                            result.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = refId,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionType.CashReward,
                                SourceId = TransactionSource.TUCBlack,
                                Amount = userRewardAmount,
                                TotalAmount = userRewardAmount,
                                TransactionHash = cctHash
                            });
                        }
                        else
                        {
                            result.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = refId,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionType.CashReward,
                                SourceId = TransactionSource.TUC,
                                Amount = userRewardAmount,
                                TotalAmount = userRewardAmount,
                                TransactionHash = cctHash
                            });
                        }
                    }

                    var tctTrxExists = context.HCUAccountTransaction.Any(x => x.TransHash == tctHash &&
                    x.ModeId == TransactionMode.Credit && x.TotalAmount == commissionRewardAmount);

                    if (!tctTrxExists)
                    {
                        result.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = SystemAccounts.ThankUCashMerchant,
                            ModeId = TransactionMode.Credit,
                            TypeId = TransactionType.CashReward,
                            SourceId = TransactionSource.Settlement,
                            Amount = commissionRewardAmount,
                            TotalAmount = commissionRewardAmount,
                            TransactionHash = tctHash
                        });
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }
    }

    internal class ActorUploadCustomerReward : ReceiveActor
    {

        public ActorUploadCustomerReward()
        {
            Receive<OUpload.RewardUpload.Item>(_Request =>
            {
                FrameworkReward _FrameworkReward = new FrameworkReward();
                _FrameworkReward.RewardCustomer(_Request);
            });

        }
    }
    public static class SendGridEmailTemplateIds
    {
        public const string RewardEmail = "d-0e22a726fc3f4fd1aa72cbfee2fee6f1";
        public const string RedeemEmail = "d-1635d8f52c664ed285aae77a553d4046";
        public const string MerchantLoanRedeemEmail = "d-0e8bd8df20cd42e09b9230e3159e8daf";
        public const string CustomerLoanRedeemEmail = "d-87e53d4bfd8c448fa328057af779a00d";
        public const string TUCLoanRedeemEmail = "d-b6ffb35c81df41ad804d99ea5e96f4ac";
        public const string PendingRewardEmail = "d-dd04bdb1ea4e44478a5606e0eb85d5e2";

        public const string MerchantSettlement = "d-cfab42f2c0f541aca671b88b241d2a49";
        public const string TUCMerchantSettlement = "d-a509d17bf9d748ef8d148c22b441e885";
    }


}
