//==================================================================================
// FileName: FrameworkRedeem.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to redeem
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Integration.Paystack;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Core.Object.Merchant.App;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.Framework.Merchant.App
{
    public class FrameworkRedeem
    {
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        ManageCoreTransaction _ManageCoreTransaction;
        HCoreContext _HCoreContext;
        ORedeem.Initialize.Response _RedeemResponse;
        BNPLMerchantSettlement _BNPLMerchantSettlement;
        HCUAccountTransaction _HCUAccountTransaction;

        /// <summary>
        /// Description: Redeem initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse Redeem_Initialize(ORedeem.Initialize.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1352, TUCCoreResource.CA1352M);
                }
                else if (string.IsNullOrEmpty(_Request.Pin))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1353, TUCCoreResource.CA1353M);
                }
                else if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1168, TUCCoreResource.CA1168M);
                }
                else if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1169, TUCCoreResource.CA1169M);
                }
                else
                {
                    _Request.MobileNumber = _Request.MobileNumber.Trim();
                    _Request.Pin = _Request.Pin.Trim();
                    using (_HCoreContext = new HCoreContext())
                    {
                        string CountryIsd = "234";
                        int CountryMobileNumberLength = 10;
                        long MerchantId = 0;
                        long MerchantStatusId = HelperStatus.Default.Inactive;
                        if (_Request.UserReference.AccountTypeId == UserAccountType.MerchantCashier)
                        {
                            var MerchantDetails = _HCoreContext.HCUAccount
                            .Where(x => x.Owner.Owner.AccountTypeId == UserAccountType.Merchant
                                && x.Owner.Id == _Request.AccountId
                                && x.Owner.Guid == _Request.AccountKey)
                            .Select(x => new
                            {
                                ReferenceId = x.Id,
                                StatusId = x.StatusId,
                                Isd = x.Country.Isd,
                                MobileNumberLength = x.Country.MobileNumberLength,
                            }).FirstOrDefault();
                            if (MerchantDetails != null)
                            {
                                MerchantId = MerchantDetails.ReferenceId;
                                MerchantStatusId = MerchantDetails.StatusId;
                                CountryIsd = MerchantDetails.Isd;
                                CountryMobileNumberLength = MerchantDetails.MobileNumberLength;
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
                            }
                        }
                        else
                        {
                            var MerchantDetails = _HCoreContext.HCUAccount
                                .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                    && x.Id == _Request.AccountId
                                    && x.Guid == _Request.AccountKey)
                                .Select(x => new
                                {
                                    ReferenceId = x.Id,
                                    StatusId = x.StatusId,
                                    Isd = x.Country.Isd,
                                    MobileNumberLength = x.Country.MobileNumberLength,
                                }).FirstOrDefault();
                            if (MerchantDetails != null)
                            {
                                MerchantId = MerchantDetails.ReferenceId;
                                MerchantStatusId = MerchantDetails.StatusId;
                                CountryIsd = MerchantDetails.Isd;
                                CountryMobileNumberLength = MerchantDetails.MobileNumberLength;
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
                            }
                        }
                        if (MerchantStatusId == HelperStatus.Default.Active)
                        {
                            if (_Request.MobileNumber.StartsWith("45"))
                            {
                                var AccountDetails = _HCoreContext.BNPLAccountLoan.Where(x => x.LoanCode == _Request.MobileNumber)
                               .Select(x => new
                               {
                                   LoanReferenceId = x.Id,
                                   LoanReferenceKey = x.Guid,
                                   LoanMerchantId = x.MerchantId,
                                   LoanAmount = x.Amount,
                                   LoanPin = x.LoanPin,
                                   LoanStatusId = x.StatusId,
                                   IsRedeemed = x.IsRedeemed,
                                   AccountReferenceId = x.Account.Account.Id,
                                   AccountReferenceKey = x.Account.Account.Guid,
                                   AccountName = x.Account.Account.Name,
                                   AccountIconUrl = x.Account.Account.IconStorage.Path,
                                   AccountMobileNumber = x.Account.Account.MobileNumber,
                                   AccountStatusId = x.Account.Account.StatusId,
                                   AccountNumber = x.Account.Account.AccountCode,
                                   AccountPin = x.Account.Account.AccessPin,
                                   AccountEmailAddress = x.Account.Account.EmailAddress,
                               }).FirstOrDefault();
                                if (AccountDetails != null)
                                {
                                    if (AccountDetails.AccountStatusId == HelperStatus.Default.Active)
                                    {
                                        string TPin = HCoreEncrypt.DecryptHash(AccountDetails.AccountPin);
                                        string TLoanPin = HCoreEncrypt.DecryptHash(AccountDetails.LoanPin);
                                        if (TPin == _Request.Pin || TLoanPin == _Request.Pin)
                                        {
                                            if (AccountDetails.LoanStatusId == 740)
                                            {
                                                if (AccountDetails.IsRedeemed == 0)
                                                {
                                                    _RedeemResponse = new ORedeem.Initialize.Response();
                                                    _RedeemResponse.ReferenceId = AccountDetails.LoanReferenceId;
                                                    _RedeemResponse.ReferenceKey = AccountDetails.LoanReferenceKey;
                                                    _RedeemResponse.Name = AccountDetails.AccountName;
                                                    _RedeemResponse.MobileNumber = AccountDetails.AccountMobileNumber;
                                                    _RedeemResponse.EmailAddress = AccountDetails.AccountEmailAddress;
                                                    _RedeemResponse.StatusId = AccountDetails.AccountStatusId;
                                                    _RedeemResponse.AccountBalance = AccountDetails.LoanAmount;
                                                    _RedeemResponse.AccountNumber = AccountDetails.AccountNumber;

                                                    if (!string.IsNullOrEmpty(_RedeemResponse.IconUrl))
                                                    {
                                                        _RedeemResponse.IconUrl = _AppConfig.StorageUrl + AccountDetails.AccountIconUrl;
                                                    }
                                                    else
                                                    {
                                                        _RedeemResponse.IconUrl = _AppConfig.Default_Icon;
                                                    }

                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _RedeemResponse, TUCCoreResource.CA1158, TUCCoreResource.CA1158M);

                                                }
                                                else
                                                {
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1434, TUCCoreResource.CA1434M);
                                                }
                                            }
                                            else
                                            {
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1433, TUCCoreResource.CA1433M);
                                            }
                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1355, TUCCoreResource.CA1355M);
                                        }

                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1159, TUCCoreResource.CA1159M);
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1354, TUCCoreResource.CA1354M);
                                }
                            }
                            else
                            {
                                var GiftCardDetails = _HCoreContext.CAProductCode.Where(x => x.ItemCode == _Request.MobileNumber).FirstOrDefault();
                                if (GiftCardDetails != null)
                                {
                                    if (GiftCardDetails.StatusId != HelperStatus.ProdutCode.Unused)
                                    {
                                        _HCoreContext.Dispose();
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0404", "This giftcard has been already used. Please use another giftcard redeem code");
                                    }
                                    else
                                    {
                                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == GiftCardDetails.AccountId)
                                       .Select(x => new ORedeem.Initialize.Response
                                       {
                                           ReferenceId = x.Id,
                                           ReferenceKey = x.Guid,
                                           Name = x.Name,
                                           IconUrl = x.IconStorage.Path,
                                           MobileNumber = x.MobileNumber,
                                           StatusId = x.StatusId,
                                           AccountBalance = 0,
                                           AccountNumber = x.AccountCode,
                                           Pin = x.AccessPin,
                                       }).FirstOrDefault();
                                        if (AccountDetails != null)
                                        {
                                            AccountDetails.ItemCode = _Request.MobileNumber;
                                            string RedeemPin = HCoreEncrypt.DecryptHash(GiftCardDetails.ItemPin);
                                            if (RedeemPin == _Request.Pin)
                                            {
                                                if (!string.IsNullOrEmpty(AccountDetails.IconUrl))
                                                {
                                                    AccountDetails.IconUrl = _AppConfig.StorageUrl + AccountDetails.IconUrl;
                                                }
                                                else
                                                {
                                                    AccountDetails.IconUrl = _AppConfig.Default_Icon;
                                                }
                                                _ManageCoreTransaction = new ManageCoreTransaction();
                                                double AccountBalance = _ManageCoreTransaction.GetAppUserBalance(AccountDetails.ReferenceId, true);
                                                double UserGiftPointBalance = _ManageCoreTransaction.GetAppUserGiftPointsBalance(AccountDetails.ReferenceId, MerchantId);
                                                double? ProductBalance = _HCoreContext.CAProductCode
                                                                                      .Where(x => x.AccountId == AccountDetails.ReferenceId
                                                                                              && (x.Product.TypeId == Product.GiftCard || x.Product.TypeId == Product.QuickGiftCard)
                                                                                              && x.AvailableAmount > 0
                                                                                              && x.ItemCode == _Request.MobileNumber
                                                                                              && x.StatusId == HelperStatus.ProdutCode.Unused)
                                                                                      .OrderByDescending(x => x.AvailableAmount)
                                                                                      .Select(x => x.AvailableAmount)
                                                                                      .FirstOrDefault();
                                                if (ProductBalance == null)
                                                {
                                                    ProductBalance = 0;
                                                }
                                                if (ProductBalance > AccountBalance && ProductBalance > UserGiftPointBalance)
                                                {
                                                    AccountDetails.AccountBalance = (double)ProductBalance;
                                                }
                                                else if (UserGiftPointBalance > ProductBalance && UserGiftPointBalance > AccountBalance)
                                                {
                                                    AccountDetails.AccountBalance = UserGiftPointBalance;
                                                }
                                                else
                                                {
                                                    AccountDetails.AccountBalance = AccountBalance;
                                                }
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, AccountDetails, TUCCoreResource.CA1158, TUCCoreResource.CA1158M);
                                            }
                                            else
                                            {
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1355, TUCCoreResource.CA1355M);
                                            }
                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0404", "User details not found");
                                        }
                                    }
                                }
                                else
                                {
                                    string AccountNumber = _Request.MobileNumber;
                                    if (_Request.MobileNumber.StartsWith("0"))
                                    {
                                        _Request.MobileNumber = HCoreHelper.FormatMobileNumber(CountryIsd, _Request.MobileNumber, CountryMobileNumberLength);
                                    }

                                    var ValidateMobileNumber = HCoreHelper.ValidateMobileNumber(_Request.MobileNumber);
                                    if (ValidateMobileNumber.IsNumberValid != true)
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14125, TUCCoreResource.CAA14125M);
                                    }

                                    CountryIsd = ValidateMobileNumber.Isd;
                                    var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Isd == CountryIsd)
                                               .Select(x => new
                                               {
                                                   x.MobileNumberLength,
                                               })
                                               .FirstOrDefault();

                                    if (CountryDetails == null)
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA116900", "Invalid country details");
                                    }
                                    CountryMobileNumberLength = CountryDetails.MobileNumberLength;

                                    #region Gift Card redeem old flow of rdeem gift card with mobile number.
                                    _Request.MobileNumber = HCoreHelper.FormatMobileNumber(CountryIsd, _Request.MobileNumber, CountryMobileNumberLength);
                                    string TMobileNumber = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                                    && (x.User.Username == TMobileNumber || x.AccountCode == AccountNumber))
                                   .Select(x => new ORedeem.Initialize.Response
                                   {
                                       ReferenceId = x.Id,
                                       ReferenceKey = x.Guid,
                                       Name = x.Name,
                                       IconUrl = x.IconStorage.Path,
                                       MobileNumber = x.MobileNumber,
                                       StatusId = x.StatusId,
                                       AccountBalance = 0,
                                       AccountNumber = x.AccountCode,
                                       Pin = x.AccessPin,
                                   }).FirstOrDefault();
                                    if (AccountDetails != null)
                                    {
                                        if (AccountDetails.StatusId == HelperStatus.Default.Active)
                                        {
                                            //string GiftCard = _HCoreContext.CAProductCode.Where(x => x.AccountId == AccountDetails.ReferenceId && (x.Product.TypeId == Product.GiftCard || x.Product.TypeId == Product.QuickGiftCard) && x.AvailableAmount > 0 && x.StatusId == HelperStatus.ProdutCode.Unused).Select(a => a.ItemPin).FirstOrDefault();
                                            string TPin = HCoreEncrypt.DecryptHash(AccountDetails.Pin);
                                            //string RedeemPin = HCoreEncrypt.DecryptHash(GiftCard);
                                            if (TPin == _Request.Pin)
                                            {
                                                if (!string.IsNullOrEmpty(AccountDetails.IconUrl))
                                                {
                                                    AccountDetails.IconUrl = _AppConfig.StorageUrl + AccountDetails.IconUrl;
                                                }
                                                else
                                                {
                                                    AccountDetails.IconUrl = _AppConfig.Default_Icon;
                                                }

                                                long TUCGold = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashgold", MerchantId));
                                                _ManageCoreTransaction = new ManageCoreTransaction();
                                                double AccountBalance = 0;
                                                double UserGiftPointBalance = 0;
                                                if (TUCGold > 0)
                                                {
                                                    AccountBalance = _ManageCoreTransaction.GetAppUserBalance(AccountDetails.ReferenceId, MerchantId, TransactionSource.TUCBlack);
                                                }
                                                else
                                                {
                                                    AccountBalance = _ManageCoreTransaction.GetAppUserBalance(AccountDetails.ReferenceId, true);
                                                    UserGiftPointBalance = _ManageCoreTransaction.GetAppUserGiftPointsBalance(AccountDetails.ReferenceId, MerchantId);
                                                }
                                                //double? ProductBalance = _HCoreContext.CAProductCode
                                                //                                      .Where(x => x.AccountId == AccountDetails.ReferenceId
                                                //                                              && (x.Product.TypeId == Product.GiftCard || x.Product.TypeId == Product.QuickGiftCard)
                                                //                                              && x.AvailableAmount > 0
                                                //                                              && x.StatusId == HelperStatus.ProdutCode.Unused)
                                                //                                      .OrderByDescending(x => x.AvailableAmount)
                                                //                                      .Select(x => x.AvailableAmount)
                                                //                                      .FirstOrDefault();
                                                //if (ProductBalance == null)
                                                //{
                                                //    ProductBalance = 0;
                                                //}
                                                //if (ProductBalance > AccountBalance && ProductBalance > UserGiftPointBalance)
                                                //{
                                                //    AccountDetails.AccountBalance = (double)ProductBalance;
                                                //}
                                                if (UserGiftPointBalance > AccountBalance)
                                                {
                                                    AccountDetails.AccountBalance = UserGiftPointBalance;
                                                }
                                                else
                                                {
                                                    AccountDetails.AccountBalance = AccountBalance;
                                                }
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, AccountDetails, TUCCoreResource.CA1158, TUCCoreResource.CA1158M);
                                            }
                                            else
                                            {
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1355, TUCCoreResource.CA1355M);
                                            }

                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1159, TUCCoreResource.CA1159M);
                                        }
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14125, TUCCoreResource.CAA14125M);
                                    }
                                    #endregion
                                    //  return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14125, TUCCoreResource.CAA14125M);
                                }



                                #region Gift Card redeem old flow of rdeem gift card with mobile number.
                                // _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber, (int)_Request.UserReference.CountryMobileNumberLength);
                                // string TMobileNumber = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                                // var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                                // && x.User.Username == TMobileNumber)
                                //.Select(x => new ORedeem.Initialize.Response
                                //{
                                //    ReferenceId = x.Id,
                                //    ReferenceKey = x.Guid,
                                //    Name = x.Name,
                                //    IconUrl = x.IconStorage.Path,
                                //    MobileNumber = x.MobileNumber,
                                //    StatusId = x.StatusId,
                                //    AccountBalance = 0,
                                //    AccountNumber = x.AccountCode,
                                //    Pin = x.AccessPin,
                                //}).FirstOrDefault();
                                // if (AccountDetails != null)
                                // {
                                //     if (AccountDetails.StatusId == HelperStatus.Default.Active)
                                //     {
                                //         string GiftCard = _HCoreContext.CAProductCode.Where(x => x.AccountId == AccountDetails.ReferenceId && (x.Product.TypeId == Product.GiftCard || x.Product.TypeId == Product.QuickGiftCard) && x.AvailableAmount > 0 && x.StatusId == HelperStatus.ProdutCode.Unused).Select(a => a.ItemPin).FirstOrDefault();
                                //         string TPin = HCoreEncrypt.DecryptHash(AccountDetails.Pin);
                                //         string RedeemPin = HCoreEncrypt.DecryptHash(GiftCard);
                                //         if (TPin == _Request.Pin || RedeemPin == _Request.Pin)
                                //         {
                                //             if (!string.IsNullOrEmpty(AccountDetails.IconUrl))
                                //             {
                                //                 AccountDetails.IconUrl = _AppConfig.StorageUrl + AccountDetails.IconUrl;
                                //             }
                                //             else
                                //             {
                                //                 AccountDetails.IconUrl = _AppConfig.Default_Icon;
                                //             }
                                //             _ManageCoreTransaction = new ManageCoreTransaction();
                                //             double AccountBalance = _ManageCoreTransaction.GetAppUserBalance(AccountDetails.ReferenceId, true);
                                //             double UserGiftPointBalance = _ManageCoreTransaction.GetAppUserGiftPointsBalance(AccountDetails.ReferenceId, MerchantId);
                                //             double? ProductBalance = _HCoreContext.CAProductCode
                                //                                                   .Where(x => x.AccountId == AccountDetails.ReferenceId
                                //                                                           && (x.Product.TypeId == Product.GiftCard || x.Product.TypeId == Product.QuickGiftCard)
                                //                                                           && x.AvailableAmount > 0
                                //                                                           && x.StatusId == HelperStatus.ProdutCode.Unused)
                                //                                                   .OrderByDescending(x => x.AvailableAmount)
                                //                                                   .Select(x => x.AvailableAmount)
                                //                                                   .FirstOrDefault();
                                //             if (ProductBalance == null)
                                //             {
                                //                 ProductBalance = 0;
                                //             }
                                //             if (ProductBalance > AccountBalance && ProductBalance > UserGiftPointBalance)
                                //             {
                                //                 AccountDetails.AccountBalance = (double)ProductBalance;
                                //             }
                                //             else if (UserGiftPointBalance > ProductBalance && UserGiftPointBalance > AccountBalance)
                                //             {
                                //                 AccountDetails.AccountBalance = UserGiftPointBalance;
                                //             }
                                //             else
                                //             {
                                //                 AccountDetails.AccountBalance = AccountBalance;
                                //             }
                                //             return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, AccountDetails, TUCCoreResource.CA1158, TUCCoreResource.CA1158M);
                                //         }
                                //         else
                                //         {
                                //             return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1355, TUCCoreResource.CA1355M);
                                //         }

                                //     }
                                //     else
                                //     {
                                //         return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1159, TUCCoreResource.CA1159M);
                                //     }
                                // }
                                // else
                                // {
                                //     return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14125, TUCCoreResource.CAA14125M);
                                // }
                                #endregion


                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1157, TUCCoreResource.CA1157M);
                        }

                    }
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "Redeem_Initialize", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Redeem confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse Redeem_Confirm(ORedeem.Confirm.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                else if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                else if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1168, TUCCoreResource.CA1168M);
                }
                //else if (string.IsNullOrEmpty(_Request.ItemCode))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1521, TUCCoreResource.CA1521M);
                //}
                else if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1169, TUCCoreResource.CA1169M);
                }
                else if ((_Request.RedeemAmount) <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1356, TUCCoreResource.CA1356M);
                }
                else if ((_Request.InvoiceAmount) <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1357, TUCCoreResource.CA1357M);
                }
                else if (_Request.RedeemAmount > _Request.InvoiceAmount)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1358, TUCCoreResource.CA1358M);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        string MerchantDisplayName = null;
                        long MerchantId = 0;
                        long MerchantStatusId = HelperStatus.Default.Inactive;
                        if (_Request.UserReference.AccountTypeId == UserAccountType.MerchantCashier)
                        {
                            var MerchantDetails = _HCoreContext.HCUAccount
                            .Where(x => x.Owner.Owner.AccountTypeId == UserAccountType.Merchant
                                && x.Owner.Id == _Request.AccountId
                                && x.Owner.Guid == _Request.AccountKey)
                            .Select(x => new
                            {
                                ReferenceId = x.Id,
                                StatusId = x.StatusId,
                                DisplayName = x.DisplayName,
                            }).FirstOrDefault();
                            if (MerchantDetails != null)
                            {
                                MerchantId = MerchantDetails.ReferenceId;
                                MerchantStatusId = MerchantDetails.StatusId;
                                MerchantDisplayName = MerchantDetails.DisplayName;
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
                            }
                        }
                        else
                        {
                            var MerchantDetails = _HCoreContext.HCUAccount
                                .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                    && x.Id == _Request.AccountId
                                    && x.Guid == _Request.AccountKey)
                                .Select(x => new
                                {
                                    ReferenceId = x.Id,
                                    StatusId = x.StatusId,
                                    DisplayName = x.DisplayName,
                                }).FirstOrDefault();
                            if (MerchantDetails != null)
                            {
                                MerchantId = MerchantDetails.ReferenceId;
                                MerchantStatusId = MerchantDetails.StatusId;
                                MerchantDisplayName = MerchantDetails.DisplayName;
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
                            }
                        }
                        if (MerchantStatusId == HelperStatus.Default.Active)
                        {
                            var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                            && x.Id == _Request.ReferenceId
                            && x.Guid == _Request.ReferenceKey)
                           .Select(x => new ORedeem.Initialize.Response
                           {
                               ReferenceId = x.Id,
                               ReferenceKey = x.Guid,
                               Name = x.Name,
                               IconUrl = x.IconStorage.Path,
                               MobileNumber = x.MobileNumber,
                               EmailAddress = x.EmailAddress,
                               StatusId = x.StatusId,
                               AccountBalance = 0,
                               AccountNumber = x.AccountCode,
                               CustomerCountryIsd = x.Country.Isd,
                           }).FirstOrDefault();
                            if (AccountDetails != null)
                            {
                                if (AccountDetails.StatusId == HelperStatus.Default.Active)
                                {
                                    if (!string.IsNullOrEmpty(AccountDetails.IconUrl))
                                    {
                                        AccountDetails.IconUrl = _AppConfig.StorageUrl + AccountDetails.IconUrl;
                                    }
                                    else
                                    {
                                        AccountDetails.IconUrl = _AppConfig.Default_Icon;
                                    }
                                    //var TUCBlack = HCoreHelper.GetConfigurationDetails("rewarddeductiontype", MerchantId);
                                    long TUCGold = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashgold", MerchantId));
                                    //var TUCBlack = HCoreHelper.GetConfigurationDetails("thankucashgold", MerchantId);
                                    long RedeemSourceId = 0;
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    double? ProductBalance = _HCoreContext.CAProductCode
                                                                        .Where(x => x.AccountId == AccountDetails.ReferenceId
                                                                                && (x.Product.TypeId == Product.GiftCard || x.Product.TypeId == Product.QuickGiftCard)
                                                                                && x.AvailableAmount > 0
                                                                                && x.ItemCode == _Request.ItemCode
                                                                                && x.StatusId == HelperStatus.ProdutCode.Unused)
                                                                        .OrderByDescending(x => x.AvailableAmount)
                                                                        .Select(x => x.AvailableAmount)
                                                                        .FirstOrDefault();
                                    if (ProductBalance == null)
                                    {
                                        ProductBalance = 0;
                                    }
                                    double AccountBalance = 0;
                                    if (TUCGold > 0)
                                    {
                                        AccountBalance = _ManageCoreTransaction.GetAppUserBalance(AccountDetails.ReferenceId, MerchantId, TransactionSource.TUCBlack);
                                    }
                                    else
                                    {
                                        AccountBalance = _ManageCoreTransaction.GetAppUserBalance(AccountDetails.ReferenceId, true);
                                    }
                                    double UserGiftPointBalance = _ManageCoreTransaction.GetAppUserGiftPointsBalance(AccountDetails.ReferenceId, MerchantId);

                                    if (ProductBalance > AccountBalance && ProductBalance > UserGiftPointBalance)
                                    {
                                        //_Request.RedeemAmount = (double)ProductBalance;(Comented as Suraj Sir told that amount that is entered for GiftCard that should be redeemed)
                                        AccountDetails.AccountBalance = (double)ProductBalance;
                                        RedeemSourceId = TransactionSource.GiftCards;
                                    }
                                    else if (UserGiftPointBalance > ProductBalance && UserGiftPointBalance > AccountBalance)
                                    {
                                        AccountDetails.AccountBalance = UserGiftPointBalance;
                                        RedeemSourceId = TransactionSource.GiftPoints;
                                    }
                                    else
                                    {
                                        if (TUCGold > 0)
                                        {
                                            AccountDetails.AccountBalance = AccountBalance;
                                            RedeemSourceId = TransactionSource.TUCBlack;
                                        }
                                        else
                                        {
                                            AccountDetails.AccountBalance = AccountBalance;
                                            RedeemSourceId = TransactionSource.TUC;
                                        }
                                    }

                                    if (RedeemSourceId == TransactionSource.TUC)
                                    {
                                        double RewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", MerchantId)), _AppConfig.SystemRoundPercentage);
                                        if (RewardPercentage <= 0)
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14126, TUCCoreResource.CAA14126M);
                                        }
                                    }
                                    if (_Request.RedeemAmount > AccountDetails.AccountBalance)
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1359, TUCCoreResource.CA1359M);
                                    }
                                    else
                                    {
                                        _CoreTransactionRequest = new OCoreTransaction.Request();
                                        _CoreTransactionRequest.CustomerId = AccountDetails.ReferenceId;
                                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                        _CoreTransactionRequest.ParentId = MerchantId;
                                        //_CoreTransactionRequest.SubParentId = _OGatewayInfo.StoreId;
                                        _CoreTransactionRequest.InvoiceAmount = _Request.InvoiceAmount;
                                        _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.InvoiceAmount;
                                        _CoreTransactionRequest.ReferenceAmount = _Request.RedeemAmount;
                                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                        //_CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
                                        //_CoreTransactionRequest.CashierId = _OGatewayInfo.CashierId;
                                        _CoreTransactionRequest.ReferenceNumber = "GCR" + HCoreHelper.GenerateRandomNumber(6);
                                        //if (_OGatewayInfo.AcquirerId != null)
                                        //{
                                        //    _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
                                        //}
                                        //_CoreTransactionRequest.TerminalId = _OGatewayInfo.TerminalId;
                                        //if (_OGatewayInfo.TerminalId != 0)
                                        //{
                                        //    _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                        //}
                                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                        if (RedeemSourceId == TransactionSource.GiftCards)
                                        {
                                            if (ProductBalance == 0 || ProductBalance == null)
                                            {
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1359, TUCCoreResource.CA1359M);
                                            }
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = AccountDetails.ReferenceId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.Loyalty.TUCRedeem.GiftCardRedeem,
                                                SourceId = TransactionSource.GiftCards,
                                                Amount = _Request.RedeemAmount,
                                                TotalAmount = _Request.RedeemAmount,
                                            });
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = AccountDetails.ReferenceId,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionType.Loyalty.TUCRedeem.GiftCardRedeem,
                                                SourceId = TransactionSource.GiftCards,
                                                Amount = _Request.RedeemAmount,
                                                TotalAmount = _Request.RedeemAmount,
                                            });
                                        }
                                        if (RedeemSourceId == TransactionSource.TUC)
                                        {
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = AccountDetails.ReferenceId,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionType.Loyalty.TUCRedeem.AppRedeem,
                                                SourceId = TransactionSource.TUC,
                                                Amount = _Request.RedeemAmount,
                                                TotalAmount = _Request.RedeemAmount,
                                            });
                                            //if (TUCGold > 0)
                                            //{
                                            //    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            //    {
                                            //        UserAccountId = AccountDetails.ReferenceId,
                                            //        ModeId = TransactionMode.Debit,
                                            //        TypeId = TransactionType.Loyalty.TUCRedeem.AppRedeem,
                                            //        SourceId = TransactionSource.TUCBlack,
                                            //        Amount = _Request.RedeemAmount,
                                            //        TotalAmount = _Request.RedeemAmount,
                                            //    });
                                            //}
                                            //else
                                            //{
                                            //    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            //    {
                                            //        UserAccountId = AccountDetails.ReferenceId,
                                            //        ModeId = TransactionMode.Debit,
                                            //        TypeId = TransactionType.Loyalty.TUCRedeem.AppRedeem,
                                            //        SourceId = TransactionSource.TUC,
                                            //        Amount = _Request.RedeemAmount,
                                            //        TotalAmount = _Request.RedeemAmount,
                                            //    });
                                            //}
                                        }
                                        if (RedeemSourceId == TransactionSource.TUCBlack)
                                        {
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = AccountDetails.ReferenceId,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionType.Loyalty.TUCRedeem.AppRedeem,
                                                SourceId = TransactionSource.TUCBlack,
                                                Amount = _Request.RedeemAmount,
                                                TotalAmount = _Request.RedeemAmount,
                                            });
                                        }
                                        if (RedeemSourceId == TransactionSource.GiftPoints)
                                        {
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = AccountDetails.ReferenceId,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionType.Loyalty.TUCRedeem.GiftPointRedeem,
                                                SourceId = TransactionSource.GiftPoints,
                                                Amount = _Request.RedeemAmount,
                                                TotalAmount = _Request.RedeemAmount,
                                            });
                                        }
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = MerchantId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionType.Loyalty.TUCRedeem.AppRedeem,
                                            SourceId = TransactionSource.Settlement,
                                            Amount = _Request.RedeemAmount,
                                            TotalAmount = _Request.RedeemAmount,
                                        });

                                        // Debit From User
                                        //_TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        //{
                                        //    UserAccountId = _UserInfo.UserAccountId,
                                        //    ModeId = TransactionMode.Debit,
                                        //    TypeId = TransactionType.OnlineRedeem,
                                        //    SourceId = RedeemSourceId,
                                        //    Amount = RedeemAmount,
                                        //    TotalAmount = RedeemAmount,
                                        //});
                                        //if (RedeemSourceId == TransactionSource.GiftCards)
                                        //{
                                        //    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        //    {
                                        //        UserAccountId = _UserInfo.UserAccountId,
                                        //        ModeId = TransactionMode.Debit,
                                        //        TypeId = TransactionType.GiftCardRedeem,
                                        //        SourceId = TransactionSource.GiftCards,
                                        //        Amount = RedeemAmount,
                                        //        TotalAmount = RedeemAmount,
                                        //        Comment = ProductCodeId.ToString(),
                                        //    });
                                        //}
                                        //if (RedeemSourceId == TransactionSource.TUC)
                                        //{
                                        //if (TUCBlack != null && !string.IsNullOrEmpty(TUCBlack.Value) && TUCBlack.Value != "0")
                                        //{
                                        //    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        //    {
                                        //        UserAccountId = AccountDetails.ReferenceId,
                                        //        ModeId = TransactionMode.Debit,
                                        //        TypeId = TransactionType.Loyalty.TUCRedeem.AppRedeem,
                                        //        SourceId = TransactionSource.TUCBlack,
                                        //        Amount = _Request.RedeemAmount,
                                        //        TotalAmount = _Request.RedeemAmount,
                                        //    });
                                        //}
                                        //else
                                        //{
                                        //    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        //    {
                                        //        UserAccountId = AccountDetails.ReferenceId,
                                        //        ModeId = TransactionMode.Debit,
                                        //        TypeId = TransactionType.Loyalty.TUCRedeem.AppRedeem,
                                        //        SourceId = TransactionSource.TUC,
                                        //        Amount = _Request.RedeemAmount,
                                        //        TotalAmount = _Request.RedeemAmount,
                                        //    });
                                        //}
                                        //}
                                        //if (RedeemSourceId == TransactionSource.GiftPoints)
                                        //{
                                        //    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        //    {
                                        //        UserAccountId = _UserInfo.UserAccountId,
                                        //        ModeId = TransactionMode.Debit,
                                        //        TypeId = TransactionType.GiftPointRedeem,
                                        //        SourceId = TransactionSource.GiftPoints,
                                        //        Amount = RedeemAmount,
                                        //        TotalAmount = RedeemAmount,
                                        //    });
                                        //}
                                        _CoreTransactionRequest.Transactions = _TransactionItems;
                                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                        {
                                            AccountDetails.TransactionReferenceId = TransactionResponse.ReferenceId;
                                            AccountDetails.TransactionDate = TransactionResponse.TransactionDate;
                                            AccountDetails.StatusName = "success";
                                            double UpdatedAccountBalance = Math.Round(AccountDetails.AccountBalance - _Request.RedeemAmount, 2);
                                            //_GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
                                            //_GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
                                            //_GatewayResponse.TransactionReference = TransactionResponse.GroupKey;
                                            //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG121");
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                var GiftcardDetails = _HCoreContext.CAProductCode.Where(x => x.AccountId == AccountDetails.ReferenceId && (x.Product.TypeId == Product.GiftCard || x.Product.TypeId == Product.QuickGiftCard) && x.AvailableAmount > 0 && x.StatusId == HelperStatus.ProdutCode.Unused).FirstOrDefault();
                                                if (GiftcardDetails != null)
                                                {
                                                    GiftcardDetails.TransactionId = TransactionResponse.ReferenceId;
                                                    GiftcardDetails.LastUseDate = HCoreHelper.GetGMTDateTime();
                                                    GiftcardDetails.UseCount = GiftcardDetails.UseCount + 1;
                                                    GiftcardDetails.UseAttempts = GiftcardDetails.UseAttempts + 1;
                                                    GiftcardDetails.AvailableAmount = GiftcardDetails.AvailableAmount - _Request.RedeemAmount;
                                                    GiftcardDetails.StatusId = HelperStatus.ProdutCode.Used;
                                                    GiftcardDetails.ModifyById = AccountDetails.ReferenceId;
                                                    GiftcardDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                    _HCoreContext.SaveChanges();
                                                }

                                            }
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                string MerchantNotificationUrl = _HCoreContext.HCUAccountSession
                                                    .Where(x => x.AccountId == _Request.UserReference.AccountId && x.StatusId == 2 && x.NotificationUrl != null)
                                                    .OrderByDescending(x => x.LoginDate)
                                                    .Select(x => x.NotificationUrl)
                                                    .FirstOrDefault();
                                                if (!string.IsNullOrEmpty(MerchantNotificationUrl))
                                                {
                                                    string Message = "Payment of N" + HCoreHelper.RoundNumber(_Request.RedeemAmount, _AppConfig.SystemExitRoundDouble).ToString() + " received from " + AccountDetails.Name;
                                                    if (HostEnvironment == HostEnvironmentType.Live)
                                                    {
                                                        HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "dashboard", "Payment Alert: " + _Request.RedeemAmount + " received.", Message, "dashboard", 0, null, "View details", null);
                                                    }
                                                    else
                                                    {
                                                        HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "dashboard", "TEST : Payment Alert: " + _Request.RedeemAmount + " received.", Message, "dashboard", 0, null, "View details", null);
                                                    }
                                                }
                                                string UserNotificationUrl = _HCoreContext.HCUAccountSession
                                                    .Where(x => x.AccountId == _Request.ReferenceId && x.StatusId == 2 && x.NotificationUrl != null)
                                                    .OrderByDescending(x => x.LoginDate)
                                                    .Select(x => x.NotificationUrl)
                                                    .FirstOrDefault();
                                                if (!string.IsNullOrEmpty(UserNotificationUrl))
                                                {
                                                    string Message = "You just redeemed " + HCoreHelper.RoundNumber(_Request.RedeemAmount, _AppConfig.SystemExitRoundDouble).ToString() + " Points at " + MerchantDisplayName + ". Your TUC Bal: " + HCoreHelper.RoundNumber(UpdatedAccountBalance, _AppConfig.SystemExitRoundDouble).ToString() + " points";
                                                    if (HostEnvironment == HostEnvironmentType.Live)
                                                    {
                                                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "Redeem Alert: " + _Request.RedeemAmount + " redeemed.", Message, "dashboard", 0, null, "View details", false, null, null);
                                                    }
                                                    else
                                                    {
                                                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST : Redeem Alert: " + _Request.RedeemAmount + " redeemed.", Message, "dashboard", 0, null, "View details", false, null, null);
                                                    }
                                                }
                                                else
                                                {
                                                    #region Send SMS
                                                    string Message = "Redeem Alert: You just redeemed " + HCoreHelper.RoundNumber(_Request.RedeemAmount, _AppConfig.SystemExitRoundDouble).ToString() + " points at " + MerchantDisplayName + ". Your TUC Bal: " + HCoreHelper.RoundNumber(UpdatedAccountBalance, _AppConfig.SystemExitRoundDouble).ToString() + " points.  Download TUC App: https://bit.ly/tuc-app. Thank U Very Much";
                                                    HCoreHelper.SendSMS(SmsType.Transaction, AccountDetails.CustomerCountryIsd, AccountDetails.MobileNumber, Message, (long)AccountDetails.ReferenceId, TransactionResponse.ReferenceKey, TransactionResponse.ReferenceId);
                                                    #endregion
                                                }
                                            }
                                            #region Send Email 
                                            if (!string.IsNullOrEmpty(AccountDetails.EmailAddress))
                                            {
                                                var _EmailParameters = new
                                                {
                                                    UserDisplayName = AccountDetails.Name,
                                                    MerchantName = MerchantDisplayName,
                                                    InvoiceAmount = _Request.InvoiceAmount.ToString(),
                                                    Amount = _Request.InvoiceAmount.ToString(),
                                                    Balance = UpdatedAccountBalance.ToString(),
                                                };
                                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RedeemEmail, AccountDetails.Name, AccountDetails.EmailAddress, _EmailParameters, _Request.UserReference);
                                            }
                                            #endregion
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, AccountDetails, TUCCoreResource.CA1360, TUCCoreResource.CA1360M);
                                        }
                                        else
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                            #endregion
                                        }
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1159, TUCCoreResource.CA1159M);
                                }
                            }
                            else
                            {
                                var LoanDetails = _HCoreContext.BNPLAccountLoan
                                    .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                    .FirstOrDefault();
                                if (LoanDetails != null)
                                {
                                    if (LoanDetails.StatusId == 740)
                                    {
                                        if (LoanDetails.IsRedeemed == 0)
                                        {
                                            var LoanInformation = _HCoreContext.BNPLAccountLoan.Where(x => x.Id == LoanDetails.Id)
                                                                 .Select(x => new
                                                                 {
                                                                     LoanReferenceId = x.Id,
                                                                     LoanReferenceKey = x.Guid,
                                                                     LoanMerchantId = x.MerchantId,
                                                                     LoanAmount = x.Amount,
                                                                     LoanPin = x.LoanPin,
                                                                     LoanStatusId = x.StatusId,
                                                                     IsRedeemed = x.IsRedeemed,
                                                                     AccountReferenceId = x.Account.Account.Id,
                                                                     AccountReferenceKey = x.Account.Account.Guid,
                                                                     AccountName = x.Account.Account.Name,
                                                                     AccountIconUrl = x.Account.Account.IconStorage.Path,
                                                                     AccountMobileNumber = x.Account.Account.MobileNumber,
                                                                     AccountStatusId = x.Account.Account.StatusId,
                                                                     AccountNumber = x.Account.Account.AccountCode,
                                                                     AccountPin = x.Account.Account.AccessPin,
                                                                     AccountEmailAddress = x.Account.Account.EmailAddress,
                                                                     MerchantSettelementCriterea = x.Merchant.SettelmentDays,
                                                                     MerchantTransactionSource = x.Merchant.TransactionSourceId,
                                                                     LoanProviderId = x.ProviderId
                                                                 }).FirstOrDefault();

                                            AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                            && x.Id == LoanInformation.AccountReferenceId)
                                                           .Select(x => new ORedeem.Initialize.Response
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,
                                                               Name = x.Name,
                                                               IconUrl = x.IconStorage.Path,
                                                               MobileNumber = x.MobileNumber,
                                                               EmailAddress = x.EmailAddress,
                                                               StatusId = x.StatusId,
                                                               AccountBalance = 0,
                                                               AccountNumber = x.AccountCode,
                                                           }).FirstOrDefault();

                                            if (AccountDetails.StatusId != HelperStatus.Default.Active)
                                            {
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, "Account not active. Please contact support.");
                                            }

                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.CustomerId = LoanInformation.AccountReferenceId;
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.ParentId = MerchantId;
                                            _CoreTransactionRequest.InvoiceAmount = _Request.InvoiceAmount;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.InvoiceAmount;
                                            _CoreTransactionRequest.ReferenceAmount = LoanInformation.LoanAmount;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = LoanInformation.AccountReferenceId,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionType.TUCBnpl.LoanRedeem,
                                                SourceId = TransactionSource.TUCBnpl,
                                                Amount = LoanInformation.LoanAmount,
                                                TotalAmount = LoanInformation.LoanAmount,
                                            });

                                            //_TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            //{
                                            //    UserAccountId = MerchantId,
                                            //    ModeId = TransactionMode.Credit,
                                            //    TypeId = TransactionType.TUCBnpl.LoanCredit,
                                            //    SourceId = TransactionSource.TUCBnpl,
                                            //    Amount = LoanInformation.LoanAmount,
                                            //    TotalAmount = LoanInformation.LoanAmount,
                                            //});

                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                            {
                                                AccountDetails.TransactionReferenceId = TransactionResponse.ReferenceId;
                                                AccountDetails.TransactionDate = TransactionResponse.TransactionDate;
                                                AccountDetails.StatusName = "success";
                                                double UpdatedAccountBalance = Math.Round(AccountDetails.AccountBalance - _Request.RedeemAmount, 2);
                                                using (_HCoreContext = new HCoreContext())
                                                {
                                                    var _LoanDetails = _HCoreContext.BNPLAccountLoan.Where(x => x.Id == LoanDetails.Id).FirstOrDefault();
                                                    _LoanDetails.IsRedeemed = 1;
                                                    _LoanDetails.RedeemTransactionId = TransactionResponse.ReferenceId;
                                                    _LoanDetails.RedeemDate = HCoreHelper.GetGMTDateTime();
                                                    _HCoreContext.SaveChanges();
                                                }
                                                if (LoanInformation.MerchantSettelementCriterea == 0)
                                                {
                                                    if (LoanInformation.MerchantTransactionSource == TransactionSource.TUCBnpl)
                                                    {
                                                        _CoreTransactionRequest = new OCoreTransaction.Request();
                                                        _CoreTransactionRequest.CustomerId = LoanInformation.AccountReferenceId;
                                                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                                                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                                        _CoreTransactionRequest.ParentId = MerchantId;
                                                        _CoreTransactionRequest.InvoiceAmount = _Request.InvoiceAmount;
                                                        _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.InvoiceAmount;
                                                        _CoreTransactionRequest.ReferenceAmount = LoanInformation.LoanAmount;
                                                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                        {
                                                            UserAccountId = MerchantId,
                                                            ModeId = TransactionMode.Credit,
                                                            TypeId = TransactionType.TUCBnpl.LoanCredit,
                                                            SourceId = TransactionSource.TUCBnpl,
                                                            Amount = LoanInformation.LoanAmount,
                                                            TotalAmount = LoanInformation.LoanAmount,
                                                        });

                                                        _CoreTransactionRequest.Transactions = _TransactionItems;
                                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                                        OCoreTransaction.Response MTransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                                        if (MTransactionResponse.Status == HelperStatus.Transaction.Success)
                                                        {
                                                            using (_HCoreContext = new HCoreContext())
                                                            {
                                                                _BNPLMerchantSettlement = new BNPLMerchantSettlement();
                                                                _BNPLMerchantSettlement.Guid = HCoreHelper.GenerateGuid();
                                                                _BNPLMerchantSettlement.LoanId = LoanInformation.LoanReferenceId;
                                                                _BNPLMerchantSettlement.MerchantId = LoanInformation.LoanMerchantId;
                                                                _BNPLMerchantSettlement.ProviderId = (long)LoanInformation.LoanProviderId;
                                                                _BNPLMerchantSettlement.InvoiceNumber = "#BNPL_0" + HCoreHelper.GenerateRandomNumber(2) + "MS" + LoanInformation.LoanReferenceId + LoanInformation.LoanMerchantId;
                                                                _BNPLMerchantSettlement.LoanRedeenDate = HCoreHelper.GetGMTDateTime();
                                                                _BNPLMerchantSettlement.SettlementDate = HCoreHelper.GetGMTDateTime();
                                                                _BNPLMerchantSettlement.LoanAmount = LoanInformation.LoanAmount;
                                                                _BNPLMerchantSettlement.TucFees = (LoanInformation.LoanAmount / 100) * 2;
                                                                _BNPLMerchantSettlement.SettlementAmount = (LoanInformation.LoanAmount - _BNPLMerchantSettlement.TucFees);
                                                                // _BNPLMerchantSettlement.StatusId = Integration.BNPLHelpers.BNPLHelper.StatusHelper.MerchantSetelment.Successful;
                                                                _BNPLMerchantSettlement.CreateDate = HCoreHelper.GetGMTDateTime();
                                                                _BNPLMerchantSettlement.CreatedById = _Request.UserReference.AccountId;
                                                                _HCoreContext.BNPLMerchantSettlement.Add(_BNPLMerchantSettlement);
                                                                _HCoreContext.SaveChanges();
                                                            }
                                                            using (_HCoreContext = new HCoreContext())
                                                            {
                                                                string MerchantNotificationUrl = _HCoreContext.HCUAccountSession
                                                                    .Where(x => x.AccountId == _Request.UserReference.AccountId && x.StatusId == 2 && x.NotificationUrl != null)
                                                                    .OrderByDescending(x => x.LoginDate)
                                                                    .Select(x => x.NotificationUrl)
                                                                    .FirstOrDefault();
                                                                if (!string.IsNullOrEmpty(MerchantNotificationUrl))
                                                                {
                                                                    string Message = "Payment of " + HCoreHelper.RoundNumber(_Request.RedeemAmount, _AppConfig.SystemExitRoundDouble).ToString() + " received from " + AccountDetails.Name;
                                                                    if (HostEnvironment == HostEnvironmentType.Live)
                                                                    {
                                                                        HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "dashboard", "Payment Alert: " + _Request.RedeemAmount + " points received.", Message, "dashboard", 0, null, "View details", null);
                                                                    }
                                                                    else
                                                                    {
                                                                        HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "dashboard", "TEST : Payment Alert: " + _Request.RedeemAmount + "points received.", Message, "dashboard", 0, null, "View details", null);
                                                                    }
                                                                }

                                                                string UserNotificationUrl = _HCoreContext.HCUAccountSession
                                                                    .Where(x => x.AccountId == LoanInformation.AccountReferenceId && x.StatusId == 2 && x.NotificationUrl != null)
                                                                    .OrderByDescending(x => x.LoginDate)
                                                                    .Select(x => x.NotificationUrl)
                                                                    .FirstOrDefault();
                                                                if (!string.IsNullOrEmpty(UserNotificationUrl))
                                                                {
                                                                    string Message = "You just redeemed " + HCoreHelper.RoundNumber(_Request.RedeemAmount, _AppConfig.SystemExitRoundDouble).ToString() + " Points at " + MerchantDisplayName + ". Your TUC Bal: " + HCoreHelper.RoundNumber(UpdatedAccountBalance, _AppConfig.SystemExitRoundDouble).ToString() + "";
                                                                    if (HostEnvironment == HostEnvironmentType.Live)
                                                                    {
                                                                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "Redeem Alert: " + _Request.RedeemAmount + " redeemed.", Message, "dashboard", 0, null, "View details", false, null, null);
                                                                    }
                                                                    else
                                                                    {
                                                                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST : Redeem Alert: " + _Request.RedeemAmount + " redeemed.", Message, "dashboard", 0, null, "View details", false, null, null);
                                                                    }
                                                                }

                                                                var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.Id == LoanInformation.AccountReferenceId && x.StatusId == HelperStatus.Default.Active).FirstOrDefault();
                                                                if (!string.IsNullOrEmpty(CustomerDetails.EmailAddress))
                                                                {
                                                                    var _EmailParameters = new
                                                                    {
                                                                        UserDisplayName = CustomerDetails.DisplayName,
                                                                        MerchantName = MerchantDisplayName,
                                                                        LoanAmount = LoanInformation.LoanAmount
                                                                    };
                                                                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.CustomerLoanRedeemEmail, CustomerDetails.Name, CustomerDetails.EmailAddress, _EmailParameters, _Request.UserReference);
                                                                }
                                                            }
                                                            #region Send Email
                                                            if (!string.IsNullOrEmpty(AccountDetails.EmailAddress))
                                                            {
                                                                var _EmailParameters = new
                                                                {
                                                                    UserDisplayName = AccountDetails.Name,
                                                                    MerchantName = MerchantDisplayName,
                                                                    InvoiceAmount = _Request.InvoiceAmount.ToString(),
                                                                    Amount = _Request.InvoiceAmount.ToString(),
                                                                    Balance = UpdatedAccountBalance.ToString(),
                                                                    RedeemDate = HCoreHelper.GetGMTDateTime(),
                                                                    TransactionRef = TransactionResponse.ReferenceId,
                                                                    TransactionDate = TransactionResponse.TransactionDate.ToString("dd-MM-yyyy HH:mm"),
                                                                };

                                                                var _Email = new
                                                                {
                                                                    RedeemDate = HCoreHelper.GetGMTDateTime(),
                                                                    UserDisplayName = AccountDetails.Name,
                                                                    MerchantName = MerchantDisplayName,
                                                                    Amount = _Request.InvoiceAmount.ToString(),
                                                                    TransactionRef = TransactionResponse.ReferenceId,
                                                                    TransactionDate = TransactionResponse.TransactionDate.ToString("dd-MM-yyyy HH:mm"),
                                                                };
                                                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.MerchantLoanRedeemEmail, AccountDetails.Name, AccountDetails.EmailAddress, _EmailParameters, _Request.UserReference);
                                                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.TUCLoanRedeemEmail, "ThankUCash", "bnpl@thankucash.com", _Email, _Request.UserReference);

                                                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.MerchantLoanRedeemEmail, AccountDetails.Name, AccountDetails.EmailAddress, _EmailParameters, _Request.UserReference);
                                                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.TUCMerchantSettlement, "ThankUCash", "bnpl@thankucash.com", _Email, _Request.UserReference);
                                                            }
                                                            #endregion
                                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, AccountDetails, TUCCoreResource.CA1360, TUCCoreResource.CA1360M);
                                                        }
                                                        else
                                                        {
                                                            #region Send Response
                                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                                            #endregion
                                                        }
                                                    }
                                                    else
                                                    {
                                                        using (_HCoreContext = new HCoreContext())
                                                        {
                                                            string GroupKey = "bnpl" + _Request.AccountId + "settlement" + HCoreHelper.GenerateDateString();
                                                            var BankDetails = _HCoreContext.HCUAccountBank.Where(x => x.AccountId == MerchantId).FirstOrDefault();
                                                            double WalletBalance = _ManageCoreTransaction.GetAccountBalance(MerchantId, TransactionSource.TUCBnpl);
                                                            var _TransferResponse = PaystackTransfer.CreateTransfer(_AppConfig.PaystackPrivateKey, GroupKey, BankDetails.ReferenceCode, Math.Round(LoanInformation.LoanAmount), "TUC BNPL");
                                                            if (_TransferResponse != null)
                                                            {
                                                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                                                _CoreTransactionRequest.CustomerId = LoanInformation.AccountReferenceId;
                                                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                                                _CoreTransactionRequest.ParentId = MerchantId;
                                                                _CoreTransactionRequest.InvoiceAmount = _Request.InvoiceAmount;
                                                                _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.InvoiceAmount;
                                                                _CoreTransactionRequest.ReferenceAmount = LoanInformation.LoanAmount;
                                                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                                {
                                                                    UserAccountId = MerchantId,
                                                                    ModeId = TransactionMode.Debit,
                                                                    TypeId = TransactionType.TUCBnpl.LoanRedeem,
                                                                    SourceId = TransactionSource.TUCBnpl,
                                                                    Amount = LoanInformation.LoanAmount,
                                                                    TotalAmount = LoanInformation.LoanAmount,
                                                                });

                                                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                                                _ManageCoreTransaction = new ManageCoreTransaction();
                                                                OCoreTransaction.Response TTransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);

                                                                double UpdatedBalance = Math.Round(WalletBalance - (LoanInformation.LoanAmount - ((LoanInformation.LoanAmount / 100) * 2)), 2);

                                                                using (_HCoreContext = new HCoreContext())
                                                                {
                                                                    ///add the ytransaction record into the database
                                                                    _HCUAccountTransaction = new HCUAccountTransaction();
                                                                    _HCUAccountTransaction.Guid = HCoreHelper.GenerateGuid();
                                                                    _HCUAccountTransaction.ParentId = MerchantId;
                                                                    _HCUAccountTransaction.CustomerId = LoanInformation.AccountReferenceId;
                                                                    _HCUAccountTransaction.AccountId = LoanInformation.AccountReferenceId;
                                                                    _HCUAccountTransaction.ModeId = TransactionMode.Credit;
                                                                    _HCUAccountTransaction.TypeId = TransactionType.TUCBnpl.LoanRedeem;
                                                                    _HCUAccountTransaction.SourceId = TransactionSource.TUCBnpl;
                                                                    _HCUAccountTransaction.PurchaseAmount = LoanInformation.LoanAmount;
                                                                    _HCUAccountTransaction.ReferenceAmount = LoanInformation.LoanAmount;
                                                                    _HCUAccountTransaction.ReferenceInvoiceAmount = LoanInformation.LoanAmount;
                                                                    _HCUAccountTransaction.Amount = LoanInformation.LoanAmount;
                                                                    _HCUAccountTransaction.Charge = (LoanInformation.LoanAmount / 100) * 2;
                                                                    _HCUAccountTransaction.ComissionAmount = (LoanInformation.LoanAmount / 100) * 2;
                                                                    _HCUAccountTransaction.TotalAmount = (_HCUAccountTransaction.Amount + _HCUAccountTransaction.Charge + _HCUAccountTransaction.ComissionAmount);
                                                                    _HCUAccountTransaction.TotalAmountPercentage = HCoreHelper.GetAmountPercentage(_HCUAccountTransaction.Charge, LoanInformation.LoanAmount);
                                                                    _HCUAccountTransaction.TransactionDate = HCoreHelper.GetGMTDateTime();
                                                                    _HCUAccountTransaction.ReferenceNumber = "SETTLEMENT" + HCoreHelper.GenerateRandomNumber(10);
                                                                    _HCUAccountTransaction.TCode = HCoreHelper.GenerateRandomNumber(4);
                                                                    _HCUAccountTransaction.RequestKey = HCoreHelper.GenerateGuid();
                                                                    _HCUAccountTransaction.GroupKey = HCoreHelper.GenerateGuid();
                                                                    _HCUAccountTransaction.PaymentMethodId = PaymentMethod.Bank;
                                                                    _HCUAccountTransaction.StatusId = HelperStatus.Transaction.Success;
                                                                    _HCUAccountTransaction.CreateDate = HCoreHelper.GetGMTDateTime();
                                                                    _HCUAccountTransaction.CreatedById = _Request.UserReference.AccountId;
                                                                    _HCoreContext.HCUAccountTransaction.Add(_HCUAccountTransaction);

                                                                    ///create the successful settlement
                                                                    _BNPLMerchantSettlement = new BNPLMerchantSettlement();
                                                                    _BNPLMerchantSettlement.Guid = HCoreHelper.GenerateGuid();
                                                                    _BNPLMerchantSettlement.LoanId = LoanInformation.LoanReferenceId;
                                                                    _BNPLMerchantSettlement.MerchantId = LoanInformation.LoanMerchantId;
                                                                    _BNPLMerchantSettlement.ProviderId = (long)LoanInformation.LoanProviderId;
                                                                    _BNPLMerchantSettlement.TransactionReferenceNavigation = _HCUAccountTransaction;
                                                                    _BNPLMerchantSettlement.InvoiceNumber = "#BNPL_0" + HCoreHelper.GenerateRandomNumber(2) + "MS" + LoanInformation.LoanReferenceId + LoanInformation.LoanMerchantId;
                                                                    _BNPLMerchantSettlement.LoanRedeenDate = HCoreHelper.GetGMTDateTime();
                                                                    _BNPLMerchantSettlement.SettlementDate = HCoreHelper.GetGMTDateTime();
                                                                    _BNPLMerchantSettlement.LoanAmount = LoanInformation.LoanAmount;
                                                                    _BNPLMerchantSettlement.TucFees = (LoanInformation.LoanAmount / 100) * 2;
                                                                    _BNPLMerchantSettlement.SettlementAmount = (LoanInformation.LoanAmount - _BNPLMerchantSettlement.TucFees);
                                                                    // _BNPLMerchantSettlement.StatusId = Integration.BNPLHelpers.BNPLHelper.StatusHelper.MerchantSetelment.Successful;
                                                                    _BNPLMerchantSettlement.CreateDate = HCoreHelper.GetGMTDateTime();
                                                                    _BNPLMerchantSettlement.CreatedById = _Request.UserReference.AccountId;
                                                                    _HCoreContext.BNPLMerchantSettlement.Add(_BNPLMerchantSettlement);

                                                                    _HCoreContext.SaveChanges();
                                                                }
                                                                using (_HCoreContext = new HCoreContext())
                                                                {
                                                                    string MerchantNotificationUrl = _HCoreContext.HCUAccountSession
                                                                        .Where(x => x.AccountId == _Request.UserReference.AccountId && x.StatusId == 2 && x.NotificationUrl != null)
                                                                        .OrderByDescending(x => x.LoginDate)
                                                                        .Select(x => x.NotificationUrl)
                                                                        .FirstOrDefault();
                                                                    if (!string.IsNullOrEmpty(MerchantNotificationUrl))
                                                                    {
                                                                        string Message = "Payment of " + HCoreHelper.RoundNumber(_Request.RedeemAmount, _AppConfig.SystemExitRoundDouble).ToString() + " received from " + AccountDetails.Name;
                                                                        if (HostEnvironment == HostEnvironmentType.Live)
                                                                        {
                                                                            HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "dashboard", "Payment Alert: " + _Request.RedeemAmount + " points received.", Message, "dashboard", 0, null, "View details", null);
                                                                        }
                                                                        else
                                                                        {
                                                                            HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "dashboard", "TEST : Payment Alert: " + _Request.RedeemAmount + " points received.", Message, "dashboard", 0, null, "View details", null);
                                                                        }
                                                                    }

                                                                    string UserNotificationUrl = _HCoreContext.HCUAccountSession
                                                                        .Where(x => x.AccountId == LoanInformation.AccountReferenceId && x.StatusId == 2 && x.NotificationUrl != null)
                                                                        .OrderByDescending(x => x.LoginDate)
                                                                        .Select(x => x.NotificationUrl)
                                                                        .FirstOrDefault();
                                                                    if (!string.IsNullOrEmpty(UserNotificationUrl))
                                                                    {
                                                                        string Message = "You just redeemed N" + HCoreHelper.RoundNumber(_Request.RedeemAmount, _AppConfig.SystemExitRoundDouble).ToString() + " Cash at " + MerchantDisplayName + ". Your TUC Bal: " + HCoreHelper.RoundNumber(UpdatedAccountBalance, _AppConfig.SystemExitRoundDouble).ToString() + "";
                                                                        if (HostEnvironment == HostEnvironmentType.Live)
                                                                        {
                                                                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "Redeem Alert: " + _Request.RedeemAmount + " points redeemed.", Message, "dashboard", 0, null, "View details", false, null, null);
                                                                        }
                                                                        else
                                                                        {
                                                                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST : Redeem Alert: " + _Request.RedeemAmount + " points redeemed.", Message, "dashboard", 0, null, "View details", false, null, null);
                                                                        }
                                                                    }

                                                                    var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.Id == LoanInformation.AccountReferenceId && x.StatusId == HelperStatus.Default.Active).FirstOrDefault();
                                                                    if (!string.IsNullOrEmpty(CustomerDetails.EmailAddress))
                                                                    {
                                                                        var _EmailParameters = new
                                                                        {
                                                                            UserDisplayName = CustomerDetails.DisplayName,
                                                                            MerchantName = MerchantDisplayName,
                                                                            LoanAmount = LoanInformation.LoanAmount
                                                                        };
                                                                        HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.CustomerLoanRedeemEmail, CustomerDetails.Name, CustomerDetails.EmailAddress, _EmailParameters, _Request.UserReference);
                                                                    }
                                                                }
                                                                #region Send Email
                                                                if (!string.IsNullOrEmpty(AccountDetails.EmailAddress))
                                                                {
                                                                    var _EmailParameters = new
                                                                    {
                                                                        UserDisplayName = AccountDetails.Name,
                                                                        MerchantName = MerchantDisplayName,
                                                                        InvoiceAmount = _Request.InvoiceAmount.ToString(),
                                                                        Amount = _Request.InvoiceAmount.ToString(),
                                                                        Balance = UpdatedBalance.ToString(),
                                                                        RedeemDate = HCoreHelper.GetGMTDateTime(),
                                                                        TransactionRef = TransactionResponse.ReferenceId,
                                                                        AccountName = BankDetails.Name,
                                                                        AccountNumber = BankDetails.AccountNumber,
                                                                        TransactionDate = TransactionResponse.TransactionDate.ToString("dd-MM-yyyy HH:mm"),
                                                                        BankName = BankDetails.BankName
                                                                    };

                                                                    var _Email = new
                                                                    {
                                                                        RedeemDate = HCoreHelper.GetGMTDateTime(),
                                                                        UserDisplayName = AccountDetails.Name,
                                                                        MerchantName = MerchantDisplayName,
                                                                        Amount = _Request.InvoiceAmount.ToString(),
                                                                        TransactionRef = TransactionResponse.ReferenceId,
                                                                        AccountName = BankDetails.Name,
                                                                        AccountNumber = BankDetails.AccountNumber,
                                                                        TransactionDate = TransactionResponse.TransactionDate.ToString("dd-MM-yyyy HH:mm"),
                                                                        BankName = BankDetails.BankName
                                                                    };
                                                                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.MerchantLoanRedeemEmail, AccountDetails.Name, AccountDetails.EmailAddress, _EmailParameters, _Request.UserReference);
                                                                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.TUCLoanRedeemEmail, "ThankUCash", "bnpl@thankucash.com", _Email, _Request.UserReference);

                                                                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.MerchantLoanRedeemEmail, AccountDetails.Name, AccountDetails.EmailAddress, _EmailParameters, _Request.UserReference);
                                                                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.TUCMerchantSettlement, "ThankUCash", "bnpl@thankucash.com", _Email, _Request.UserReference);
                                                                }
                                                                #endregion
                                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, AccountDetails, TUCCoreResource.CA1360, TUCCoreResource.CA1360M);
                                                            }
                                                            else
                                                            {
                                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        _BNPLMerchantSettlement = new BNPLMerchantSettlement();
                                                        _BNPLMerchantSettlement.Guid = HCoreHelper.GenerateGuid();
                                                        _BNPLMerchantSettlement.LoanId = LoanInformation.LoanReferenceId;
                                                        _BNPLMerchantSettlement.MerchantId = LoanInformation.LoanMerchantId;
                                                        _BNPLMerchantSettlement.ProviderId = (long)LoanInformation.LoanProviderId;
                                                        _BNPLMerchantSettlement.InvoiceNumber = "#BNPL_0" + HCoreHelper.GenerateRandomNumber(2) + "MS" + LoanInformation.LoanReferenceId + LoanInformation.LoanMerchantId;
                                                        _BNPLMerchantSettlement.LoanRedeenDate = HCoreHelper.GetGMTDateTime();
                                                        _BNPLMerchantSettlement.SettlementDate = HCoreHelper.GetGMTDateTime().AddDays((double)LoanInformation.MerchantSettelementCriterea);
                                                        _BNPLMerchantSettlement.LoanAmount = LoanInformation.LoanAmount;
                                                        _BNPLMerchantSettlement.TucFees = (LoanInformation.LoanAmount / 100) * 2;
                                                        _BNPLMerchantSettlement.SettlementAmount = (LoanInformation.LoanAmount - _BNPLMerchantSettlement.TucFees);
                                                        // _BNPLMerchantSettlement.StatusId = Integration.BNPLHelpers.BNPLHelper.StatusHelper.MerchantSetelment.Initialized;
                                                        _BNPLMerchantSettlement.CreateDate = HCoreHelper.GetGMTDateTime();
                                                        _BNPLMerchantSettlement.CreatedById = _Request.UserReference.AccountId;
                                                        _HCoreContext.BNPLMerchantSettlement.Add(_BNPLMerchantSettlement);
                                                        _HCoreContext.SaveChanges();

                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, AccountDetails, TUCCoreResource.CA1360, TUCCoreResource.CA1360M);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1434, TUCCoreResource.CA1434M);
                                        }
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1433, TUCCoreResource.CA1433M);
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1354, TUCCoreResource.CA1354M);
                                }
                            }

                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1157, TUCCoreResource.CA1157M);
                        }

                    }
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "Redeem_Confirm", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }
}
