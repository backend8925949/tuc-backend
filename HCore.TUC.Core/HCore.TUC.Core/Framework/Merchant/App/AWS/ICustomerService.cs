﻿using HCore.Operations.Object;
using HCore.TUC.Core.Object.Merchant.App;

namespace HCore.TUC.Core.Framework.Merchant.App.AWS
{
	public interface ICustomerService
	{
        Task<OAppProfile.Response> CreateAppUserAccountAsync(OAppProfile.Request request);
        Task<OReward.Confirm.Response> GetAccountDetailsAsync(long accountTypeId, string username);
    }
}

