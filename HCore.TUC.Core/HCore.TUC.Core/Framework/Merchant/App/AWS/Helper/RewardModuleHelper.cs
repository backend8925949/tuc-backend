﻿using System;
using HCore.Data.Helper;
using HCore.Helper;
using MySql.Data.MySqlClient;
using Dapper;

namespace HCore.TUC.Core.Framework.Merchant.App.AWS.Helper
{
	public static class RewardModuleHelper
	{
		public static async Task LogErrorAsync(Exception exception, string title)
		{
			try
			{
				var conString = HostHelper.Host;
				
				if (!string.IsNullOrEmpty(conString))
				{
					using var con = new MySqlConnection(conString);
					var query = "INSERT INTO HCLCoreLog(Title, Message, Data, HostName) " +
						"VALUES (@title, @message, @data, '')";

					var msg = exception.InnerException != null ?
						$"{exception.Message} (Inner: {exception.InnerException.Message})" :
						exception.Message;

					var logEntry = new
					{
						title = title,
						message = msg,
						data = exception.StackTrace
					};

					await con.ExecuteAsync(query, logEntry);
                }
            }
			catch(Exception)
			{

			}
		}
	}
}

