﻿using Akka.Actor;
using Dapper;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Helper;
using HCore.Helper;
using HCore.Operations.Actor;
using HCore.Operations.Object;
using HCore.TUC.Core.Framework.Merchant.App.AWS.Helper;
using HCore.TUC.Core.Object.Merchant;
using Microsoft.Extensions.Caching.Memory;
using MySql.Data.MySqlClient;
using static HCore.CoreConstant.CoreHelperStatus;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.Framework.Merchant.App.AWS
{
    public class TransactionService : ITransactionService
    {
        private readonly IMemoryCache _cache;
        private readonly string _dbName;

        public TransactionService(IMemoryCache cache)
        {
            _cache = cache;
            _dbName = GetDatabase();
        }

        public async Task<List<OCoreTransaction.TransactionItem>> ComputeTransactionItemsAsync(OUpload.RewardUpload.Item dataItem, long refId, double rewardAmount,
            double userRewardAmount, double commissionRewardAmount, OConfiguration? config)
        {
            var result = new List<OCoreTransaction.TransactionItem>();
            try
            {
                var dbName = GetDatabase();
                using var con = new MySqlConnection(HostHelper.Host);

                var genText = dataItem.FileName + dataItem.MerchantId + dataItem.MobileNumber + dataItem.InvoiceAmount;
                var groupTrxHash = HCoreEncrypt.ComputeSha256Hash(genText);

                var existingTrxId = await con.QueryFirstOrDefaultAsync<long>($"select Id from {dbName}.HCUAccountTransaction" +
                    $" where TransHash = @hash", new { hash = groupTrxHash });

                if (existingTrxId == 0)
                {
                    //Merchant Debit Trx
                    result.Add(new OCoreTransaction.TransactionItem
                    {
                        UserAccountId = dataItem.MerchantId,
                        ModeId = TransactionMode.Debit,
                        TypeId = TransactionType.CashReward,
                        SourceId = TransactionSource.Merchant,
                        Amount = rewardAmount,
                        Charge = 0,
                        Comission = commissionRewardAmount,
                        TotalAmount = rewardAmount,
                        TransactionHash = groupTrxHash
                    });

                    //Customer Credit Trx
                    if (config != null && !string.IsNullOrEmpty(config.Value) && config.Value != "0")
                    {
                        result.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = refId,
                            ModeId = TransactionMode.Credit,
                            TypeId = TransactionType.CashReward,
                            SourceId = TransactionSource.TUCBlack,
                            Amount = userRewardAmount,
                            TotalAmount = userRewardAmount,
                            TransactionHash = groupTrxHash
                        });
                    }
                    else
                    {
                        result.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = refId,
                            ModeId = TransactionMode.Credit,
                            TypeId = TransactionType.CashReward,
                            SourceId = TransactionSource.TUC,
                            Amount = userRewardAmount,
                            TotalAmount = userRewardAmount,
                            TransactionHash = groupTrxHash
                        });
                    }

                    //TUC Credit Trx
                    result.Add(new OCoreTransaction.TransactionItem
                    {
                        UserAccountId = SystemAccounts.ThankUCashMerchant,
                        ModeId = TransactionMode.Credit,
                        TypeId = TransactionType.CashReward,
                        SourceId = TransactionSource.Settlement,
                        Amount = commissionRewardAmount,
                        TotalAmount = commissionRewardAmount,
                        TransactionHash = groupTrxHash
                    });
                }

            }
            catch (Exception ex)
            {
                await RewardModuleHelper.LogErrorAsync(ex, "ComputeTransactionItemsAsync");
            }
            return result;
        }

        public async Task<double> GetAppUserBalanceAsync(long userAccountId, long merchantId, long sourceId)
        {
            double balance = 0;
            try
            {
                using var con = new MySqlConnection(HostHelper.Host);
                double creditSum = await con.QueryFirstOrDefaultAsync<double>(
                    $"select Sum(TotalAmount) from {_dbName}.HCUAccountTransaction " +
                    "where AccountId = @accId and ParentId = @parId and SourceId = @srcId and ProgramId is null and ModeId = @modeId " +
                    "and (StatusId = @success or StatusId = @pending);",
                    new
                    {
                        accId = userAccountId,
                        parId = merchantId,
                        srcId = sourceId,
                        modeId = TransactionMode.Credit,
                        success = Transaction.Success,
                        pending = Transaction.Pending
                    });

                double debitSum = await con.QueryFirstOrDefaultAsync<double>(
                    $"select Sum(TotalAmount) from {_dbName}.HCUAccountTransaction " +
                    "where AccountId = @accId and ParentId = @parId and SourceId = @srcId and ModeId = @modeId " +
                    "and (StatusId = @success or StatusId = @pending);",
                    new
                    {
                        accId = userAccountId,
                        parId = merchantId,
                        srcId = sourceId,
                        modeId = TransactionMode.Debit,
                        success = Transaction.Success,
                        pending = Transaction.Pending
                    });

                balance = HCoreHelper.RoundNumber((creditSum - debitSum), _AppConfig.SystemEntryRoundDouble);
            }
            catch (Exception ex)
            {
                await RewardModuleHelper.LogErrorAsync(ex, "GetMerchantBalanceAsync");
            }

            return balance;
        }

        public async Task<double> GetAppUserBalanceAsync(long userAccountId)
        {
            double balance = 0;
            try
            {
                using var con = new MySqlConnection(HostHelper.Host);

                double creditSum = 0;
                try
                {
                    creditSum = await con.QueryFirstOrDefaultAsync<double>(
                                         $"select Sum(TotalAmount) from {_dbName}.HCUAccountTransaction " +
                                         "where AccountId = @accId and SourceId = @srcId and ProgramId is null and ModeId = @modeId " +
                                         "and StatusId = @statusId;",
                                         new
                                         {
                                             accId = userAccountId,
                                             srcId = TransactionSource.TUC,
                                             modeId = TransactionMode.Credit,
                                             statusId = Transaction.Success
                                         });
                }
                catch
                {

                }

                double debitSum = 0;
                try
                {
                    debitSum = await con.QueryFirstOrDefaultAsync<double>(
                    $"select Sum(TotalAmount) from {_dbName}.HCUAccountTransaction " +
                    "where AccountId = @accId and SourceId = @srcId and ModeId = @modeId " +
                    "and StatusId = @statusId;",
                    new
                    {
                        accId = userAccountId,
                        srcId = TransactionSource.TUC,
                        modeId = TransactionMode.Debit,
                        statusId = Transaction.Success,
                    });
                }
                catch
                {

                }


                balance = HCoreHelper.RoundNumber((creditSum - debitSum), _AppConfig.SystemEntryRoundDouble);
            }
            catch (Exception ex)
            {
                await RewardModuleHelper.LogErrorAsync(ex, "GetMerchantBalanceAsync");
            }

            return balance;
        }

        public async Task<OConfiguration> GetConfigurationDetailsAsync(string configCode, long accountId)
        {
            OConfiguration? result = null;
            try
            {
                var cacheKey = "config" + configCode + accountId;
                if (!_cache.TryGetValue(cacheKey, out result))
                {
                    using var con = new MySqlConnection(HostHelper.Host);
                    result = await con.QueryFirstOrDefaultAsync<OConfiguration>(
                        "select hcc.Name, hcc.Id as ReferenceId, hcc.Value, hc.SystemName as TypeCode, hc.Name as TypeName" +
                        $" from {_dbName}.HCCoreCommon hcc" +
                        $" left join {_dbName}.HCCore hc on hcc.HelperId = hc.Id" +
                        $" where hcc.SystemName = @code and hcc.StatusId = @statusId and hcc.TypeId = @typeId;",
                        new
                        {
                            code = configCode,
                            statusId = HelperStatus.Default.Active,
                            typeId = HelperType.Configuration
                        });

                    if (result != null)
                    {
                        var conValue = await con.QueryFirstOrDefaultAsync<string>(
                        "select Value" +
                        $" from {_dbName}.HCCoreCommon" +
                        $" where ParentId = @id and SubParentId is null and StatusId = @statusId and TypeId = @typeId;",
                        new
                        {
                            id = result.ReferenceId,
                            statusId = HelperStatus.Default.Active,
                            typeId = HelperType.ConfigurationValue
                        });

                        if (!string.IsNullOrEmpty(conValue))
                        {
                            result.Value = conValue;
                        }

                        if (accountId > 0)
                        {
                            var countryConfigValue = await con.QueryFirstOrDefaultAsync<dynamic>(
                                "select ap.Value, ap.SystemName as HelperCode, ap.Name as HelperName" +
                                $" from {_dbName}.HCUAccountParameter ap" +
                                $" inner join {_dbName}.HCCore hc on ap.HelperId = hc.Id" +
                                $" where ap.CommonId = @refId and ap.AccountId = @accId and ap.StatusId = @statusId and ap.TypeId = @typeId;",
                                new
                                {
                                    refId = result.ReferenceId,
                                    accId = accountId,
                                    statusId = HelperStatus.Default.Active,
                                    typeId = HelperType.Configuration
                                });

                            if (countryConfigValue != null)
                            {
                                result.Value = countryConfigValue.Value;
                                result.TypeCode = countryConfigValue.HelperCode;
                                result.TypeName = countryConfigValue.HelperName;
                            }
                        }

                        _cache.Set(cacheKey, result, TimeSpan.FromDays(1));
                    }
                }
            }
            catch (Exception ex)
            {
                await RewardModuleHelper.LogErrorAsync(ex, "GetConfigurationAsync");
            }
            return result;
        }

        public async Task<string> GetConfigurationAsync(string configCode, long accountId)
        {
            string result = "0";
            try
            {
                var cacheKey = "config" + configCode + accountId;
                if (!_cache.TryGetValue(cacheKey, out result))
                {
                    using var con = new MySqlConnection(HostHelper.Host);
                    var config = await con.QueryFirstOrDefaultAsync<OConfiguration>(
                        "select hcc.Name, hcc.Id as ReferenceId, hcc.Value, hc.SystemName as TypeCode, hc.Name as TypeName" +
                        $" from {_dbName}.HCCoreCommon hcc" +
                        $" left join {_dbName}.HCCore hc on hcc.HelperId = hc.Id" +
                        $" where hcc.SystemName = @code and hcc.StatusId = @statusId and hcc.TypeId = @typeId;",
                        new
                        {
                            code = configCode,
                            statusId = HelperStatus.Default.Active,
                            typeId = HelperType.Configuration
                        });

                    if (config != null)
                    {
                        if (accountId > 0)
                        {
                            string userConfigValue = await con.QueryFirstOrDefaultAsync<string>(
                                $"select Value from {_dbName}.HCUAccountParameter where CommonId = @refId and AccountId = @accId and " +
                                "StatusId = @statusId and TypeId = @typeId;", new
                                {
                                    refId = config.ReferenceId,
                                    accId = accountId,
                                    statusId = HelperStatus.Default.Active,
                                    typeId = HelperType.ConfigurationValue
                                });

                            if (!string.IsNullOrEmpty(userConfigValue))
                            {
                                result = userConfigValue;
                            }
                        }
                        else
                        {
                            var configValue = await con.QueryFirstOrDefaultAsync<string>(
                               "select Value from {_dbName}.HCCoreCommon " +
                               " where ParentId = @parId and SubParentId is null and " +
                               " StatusId = @statusId and hcc.TypeId = @typeId;",
                               new
                               {
                                   parId = config.ReferenceId,
                                   statusId = HelperStatus.Default.Active,
                                   typeId = HelperType.ConfigurationValue
                               });

                            if (!string.IsNullOrEmpty(configValue))
                            {
                                result = configValue;
                            }
                        }

                        _cache.Set(cacheKey, result, TimeSpan.FromDays(1));
                    }
                }
            }
            catch (Exception ex)
            {
                await RewardModuleHelper.LogErrorAsync(ex, "GetConfigurationAsync");
            }
            return result;
        }

        public async Task<double> GetMerchantBalanceAsync(long merchantId, long sourceId)
        {
            double balance = 0;
            try
            {
                var dbName = GetDatabase();
                using var con = new MySqlConnection(HostHelper.Host);
                var creditAmount = await con.QueryFirstOrDefaultAsync<double>(
                    $"select Sum(TotalAmount) from {dbName}.HCUAccountTransaction " +
                    "where AccountId = @accId and SourceId = @srcId and ProgramId is null and ModeId = @modeId " +
                    "and (StatusId = @success or StatusId = @pending);",
                    new
                    {
                        accId = merchantId,
                        srcId = sourceId,
                        modeId = TransactionMode.Credit,
                        success = Transaction.Success,
                        pending = Transaction.Pending
                    });

                var debitAmount = await con.QueryFirstOrDefaultAsync<double>(
                    $"select Sum(TotalAmount) from {dbName}.HCUAccountTransaction " +
                    "where AccountId = @accId and SourceId = @srcId and ModeId = @modeId " +
                    "and (StatusId = @success or StatusId = @pending);",
                    new
                    {
                        accId = merchantId,
                        srcId = sourceId,
                        modeId = TransactionMode.Debit,
                        success = Transaction.Success,
                        pending = Transaction.Pending
                    });

                return HCoreHelper.RoundNumber((creditAmount - debitAmount), _AppConfig.SystemEntryRoundDouble);
            }
            catch (Exception ex)
            {
                await RewardModuleHelper.LogErrorAsync(ex, "GetMerchantBalanceAsync");
            }
            return balance;
        }

        public async Task<OCoreTransaction.Response> ProcessTransactionAsync(OCoreTransaction.Request request)
        {
            var result = new OCoreTransaction.Response();
            try
            {
                DateTime TransctionDate = HCoreHelper.GetGMTDateTime();
                if (request.TTransactionDate != null && HostEnvironment == HostEnvironmentType.Test && HostEnvironment == HostEnvironmentType.Local)
                {
                    TransctionDate = (DateTime)request.TTransactionDate;
                }

                if (request.UserReference != null && request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                {
                    request.ProviderId = request.UserReference.AccountId;
                }

                if (request.CreatedById != 0)
                {
                    request.CreatedById = request.CreatedById;
                }
                else
                {
                    if (request.UserReference != null && request.UserReference.AccountId != 0)
                    {
                        request.CreatedById = request.UserReference.AccountId;
                        request.CreatedByAccountTypeId = request.UserReference.AccountTypeId;
                    }
                }
                using (var context = new HCoreContext())
                {
                    long parentTransactionId = 0;
                    string groupKey = string.Empty;
                    string tCode = HCoreHelper.GenerateRandomNumber(4);

                    #region Save Child Transactions
                    foreach (OCoreTransaction.TransactionItem trx in request.Transactions)
                    {
                        HCUAccountTransaction transaction = new()
                        {
                            InoviceNumber = request.InvoiceNumber,
                            ModeId = trx.ModeId,
                            TypeId = trx.TypeId,
                            SourceId = trx.SourceId,
                            Amount = trx.Amount,
                            Charge = trx.Charge,
                            ComissionAmount = trx.Comission,
                            TotalAmount = trx.TotalAmount,
                            Balance = 0,
                            PurchaseAmount = request.InvoiceAmount,
                            ReferenceNumber = request.ReferenceNumber,
                            Comment = trx.Comment,
                            CreateDate = TransctionDate
                        };

                        if (string.IsNullOrEmpty(groupKey) && !string.IsNullOrEmpty(request.GroupKey))
                        {
                            groupKey = request.GroupKey;

                            if (!string.IsNullOrEmpty(request.ParentTransactionKey) && request.GroupKey == request.ParentTransactionKey)
                            {
                                transaction.Guid = HCoreHelper.GenerateGuid();
                            }
                            else
                            {
                                transaction.Guid = groupKey;
                            }
                        }
                        else
                        {
                            transaction.Guid = HCoreHelper.GenerateGuid();
                        }

                        if (!string.IsNullOrEmpty(request.ParentTransactionKey))
                        {
                            groupKey = request.ParentTransactionKey;
                        }

                        if (request.ParentId > 0)
                        {
                            transaction.ParentId = request.ParentId;
                        }

                        if (trx.UserAccountId != 0)
                        {
                            transaction.AccountId = trx.UserAccountId;
                        }
                        #region Save Transaction

                        if (request.PaymentMethodId > 0)
                        {
                            transaction.PaymentMethodId = request.PaymentMethodId;
                        }
                        if (request.ReferenceInvoiceAmount != 0)
                        {
                            transaction.ReferenceInvoiceAmount = request.ReferenceInvoiceAmount;
                        }
                        else
                        {
                            transaction.ReferenceInvoiceAmount = request.InvoiceAmount;
                        }
                        transaction.ReferenceAmount = (trx.ReferenceAmount == null || trx.ReferenceAmount == 0) ? request.ReferenceAmount : trx.ReferenceAmount;
                        if (request.BankId != 0)
                        {
                            transaction.BankId = request.BankId;
                        }
                        if (request.CustomerId != 0)
                        {
                            transaction.CustomerId = request.CustomerId;
                        }
                        else
                        {
                            if (trx.SourceId == TransactionSource.TUC)
                            {
                                transaction.CustomerId = trx.UserAccountId;
                            }
                        }
                        if (request.SubParentId != 0)
                        {
                            transaction.SubParentId = request.SubParentId;
                        }
                        if (request.CashierId != 0)
                        {
                            transaction.CashierId = request.CashierId;
                        }
                        if (request.TerminalId != 0)
                        {
                            transaction.TerminalId = request.TerminalId;
                        }
                        if (request.BinNumberId != 0)
                        {
                            transaction.BinNumberId = request.BinNumberId;
                        }
                        if (trx.TransactionDate != null)
                        {
                            transaction.TransactionDate = (DateTime)trx.TransactionDate;
                        }
                        else
                        {
                            transaction.TransactionDate = TransctionDate;
                        }
                        if (!string.IsNullOrEmpty(request.AccountNumber))
                        {
                            request.AccountNumber = request.AccountNumber.Trim().Replace("*", "X").Replace("x", "X").ToUpper();
                            transaction.AccountNumber = request.AccountNumber;
                        }

                        if (parentTransactionId != 0)
                        {
                            transaction.ParentTransactionId = parentTransactionId;
                        }
                        if (request.CreatedById != 0)
                        {
                            transaction.CreatedById = request.CreatedById;
                        }
                        else
                        {
                            if (request.UserReference != null && request.UserReference.AccountId != 0)
                            {
                                transaction.CreatedById = request.UserReference.AccountId;
                            }
                        }
                        if (request.UserReference != null && request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                        {
                            transaction.ProviderId = request.UserReference.AccountId;
                        }
                        if (!string.IsNullOrEmpty(groupKey))
                        {
                            transaction.GroupKey = groupKey;
                        }
                        if (request.UserReference != null && !string.IsNullOrEmpty(request.UserReference.RequestKey))
                        {
                            transaction.RequestKey = request.UserReference.RequestKey;
                        }
                        if (request.CampaignId != 0)
                        {
                            transaction.CampaignId = request.CampaignId;
                        }
                        transaction.TCode = tCode;
                        if (trx.StatusId != 0)
                        {
                            transaction.StatusId = trx.StatusId;
                        }
                        else
                        {
                            transaction.StatusId = request.StatusId;
                        }
                        if (request.ProgramId > 0)
                        {
                            transaction.ProgramId = request.ProgramId;
                        }
                        if (request.CashierReward > 0)
                        {
                            transaction.CashierReward = request.CashierReward;
                        }

                        if (!string.IsNullOrEmpty(trx.TransactionHash))
                        {
                            transaction.TransHash = trx.TransactionHash;
                        }

                        trx.TransactionKey = transaction.Guid;
                        await context.HCUAccountTransaction.AddAsync(transaction);
                        #endregion
                    }
                    #endregion

                    await context.SaveChangesAsync();

                    if (string.IsNullOrEmpty(request.ParentTransactionKey))
                    {
                        parentTransactionId = context.HCUAccountTransaction.Where(x => x.Guid == request.GroupKey).Select(x => x.Id).FirstOrDefault();
                        if (parentTransactionId != 0)
                        {
                            List<HCUAccountTransaction> Transactions = context.HCUAccountTransaction.Where(x => x.GroupKey == groupKey && x.Id != parentTransactionId).ToList();
                            if (Transactions.Count > 0)
                            {
                                foreach (HCUAccountTransaction Transaction in Transactions)
                                {
                                    Transaction.ParentTransactionId = parentTransactionId;
                                }
                                context.SaveChanges();
                            }
                        }
                    }

                    #region Transaction Balance Updater
                    try
                    {
                        List<OCoreTransaction.TransactionItem> appUserTransactions = request.Transactions.Where(x => x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.GiftCards).ToList();
                        if (appUserTransactions != null && appUserTransactions.Count > 0)
                        {
                            if (request.ParentId > 0)
                            {
                                var fromAccountInfo = context.HCUAccount.Where(x => x.Id == request.ParentId).FirstOrDefault();
                                if (fromAccountInfo != null)
                                {
                                    fromAccountInfo.LastActivityDate = HCoreHelper.GetGMTDateTime();
                                    fromAccountInfo.LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                }
                            }

                            if (request.CustomerId > 0)
                            {
                                var toAccountInfo = context.HCUAccount.Where(x => x.Id == request.CustomerId).FirstOrDefault();
                                if (toAccountInfo != null)
                                {
                                    toAccountInfo.LastActivityDate = HCoreHelper.GetGMTDateTime();
                                    toAccountInfo.LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                }
                            }

                            foreach (var trxItem in appUserTransactions)
                            {
                                // Parent Transaction Balance
                                if (trxItem.SourceId == TransactionSource.ThankUCashPlus || trxItem.SourceId == TransactionSource.TUCBlack)
                                {
                                    double sourceTotalCredit = await GetBalanceAsync(trxItem.UserAccountId, trxItem.SourceId, Transaction.Success,
                                        TransactionMode.Credit, request.ParentId);

                                    double sourceTotalDebit = await GetBalanceAsync(trxItem.UserAccountId, trxItem.SourceId, Transaction.Success,
                                        TransactionMode.Debit, request.ParentId);

                                    long sourceTransactions = await GetTransactionsCountAsync(trxItem.UserAccountId,
                                        trxItem.SourceId, Transaction.Success, request.ParentId);

                                    double sourceBalance = HCoreHelper.RoundNumber((sourceTotalCredit - sourceTotalDebit), _AppConfig.SystemEntryRoundDouble);

                                    var _ = await UpdateTransactionBalanceAsync(trxItem.UserAccountId, sourceBalance, request.GroupKey);

                                    var updated = await UpdateAccountBalanceAsync(trxItem.UserAccountId, request.ParentId,
                                        trxItem.SourceId, sourceBalance, sourceTotalCredit, sourceTotalDebit,
                                        sourceTransactions);

                                    if (updated == 0)
                                    {
                                        var accBalance = new HCUAccountBalance();
                                        accBalance.Guid = HCoreHelper.GenerateGuid();
                                        accBalance.AccountId = trxItem.UserAccountId;
                                        accBalance.ParentId = request.ParentId;
                                        accBalance.SourceId = trxItem.SourceId;
                                        accBalance.Credit = sourceTotalCredit;
                                        accBalance.Debit = sourceTotalDebit;
                                        accBalance.Balance = sourceBalance;
                                        accBalance.Transactions = sourceTransactions;
                                        accBalance.StatusId = HelperStatus.Default.Active;
                                        await context.HCUAccountBalance.AddAsync(accBalance);
                                    }
                                }

                                double totalCredit = await GetBalanceAsync(trxItem.UserAccountId, trxItem.SourceId, Transaction.Success,
                                        TransactionMode.Credit);

                                double totalDebit = await GetBalanceAsync(trxItem.UserAccountId, trxItem.SourceId, Transaction.Success,
                                        TransactionMode.Debit);

                                long transactions = await GetTransactionsCountAsync(trxItem.UserAccountId,
                                        trxItem.SourceId, Transaction.Success);

                                double balance = HCoreHelper.RoundNumber((totalCredit - totalDebit), _AppConfig.SystemEntryRoundDouble);

                                if (trxItem.SourceId != TransactionSource.ThankUCashPlus && trxItem.SourceId != TransactionSource.TUCBlack)
                                {
                                    var _ = await UpdateTransactionBalanceAsync(trxItem.UserAccountId, balance, request.GroupKey);
                                }
                                if (trxItem.SourceId == TransactionSource.TUC)
                                {
                                    var _ = await UpdateAccountBalanceAsync(trxItem.UserAccountId, balance);
                                }

                                var updated2 = await UpdateAccountBalanceAsync(trxItem.UserAccountId, 1,
                                        trxItem.SourceId, balance, totalCredit, totalDebit,
                                        transactions);

                                if (updated2 == 0)
                                {
                                    var accBalance = new HCUAccountBalance();
                                    accBalance.Guid = HCoreHelper.GenerateGuid();
                                    accBalance.AccountId = trxItem.UserAccountId;
                                    accBalance.ParentId = 1;
                                    accBalance.SourceId = trxItem.SourceId;
                                    accBalance.Credit = totalCredit;
                                    accBalance.Debit = totalDebit;
                                    accBalance.Balance = balance;
                                    accBalance.Transactions = transactions;
                                    accBalance.StatusId = HelperStatus.Default.Active;
                                    accBalance.LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                    await context.HCUAccountBalance.AddAsync(accBalance);
                                }
                            }
                            await context.SaveChangesAsync();
                        }
                    }
                    catch (Exception ex)
                    {
                        await RewardModuleHelper.LogErrorAsync(ex, "ProcessTransactionAsync.Inner");
                    }
                    #endregion

                    #region TransactionPostProcess
                    request.TransactionDate = TransctionDate;
                    request.GroupKey = groupKey;
                    request.ParentTransactionId = parentTransactionId;

                    var transactionPostProcessActor = ActorSystem.Create("TransactionPostProcessActor");
                    var transactionPostProcessActorNotify = transactionPostProcessActor.ActorOf<TransactionPostProcessActor>("TransactionPostProcessActor");
                    transactionPostProcessActorNotify.Tell(request);

                    #endregion
                    #region Send Response
                    result.Status = Transaction.Success;
                    result.TransactionDate = TransctionDate;
                    result.ReferenceId = parentTransactionId;
                    result.ReferenceKey = groupKey;
                    result.TCode = tCode;
                    if (!string.IsNullOrEmpty(request.GroupKey))
                    {
                        result.GroupKey = request.GroupKey;
                    }
                    result.Message = "Transaction complete";
                    return result;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region Log Bug
                await RewardModuleHelper.LogErrorAsync(ex, "ProcessTransactionAsync");
                #endregion
                #region Send Response
                result.Status = Transaction.Error;
                result.Message = "Error occured while processing request";
                return result;
                #endregion
            }
        }

        #region Private Members
        /// <summary>
        /// Gets current DB Name from Config
        /// </summary>
        /// <returns></returns>
        private static string GetDatabase()
        {
            var result = string.Empty;
            var dbObjects = HostHelper.Host.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var ob in dbObjects)
            {
                if (ob.Contains("Database"))
                {
                    var dbExp = ob.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                    result = dbExp[1];
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// Gets count of HCUAccountTransactions
        /// </summary>
        /// <param name="accountId">Account ID</param>
        /// <param name="sourceId">Transaction Source ID</param>
        /// <param name="statusId">Transaction Status ID</param>
        /// <param name="parentId">Optional Parent Transaction ID</param>
        /// <returns></returns>
        private async Task<long> GetTransactionsCountAsync(long accountId, long sourceId, long statusId, long parentId = 0)
        {
            long result = 0;
            try
            {
                using var con = new MySqlConnection(HostHelper.Host);
                if (parentId > 0)
                {
                    result = await con.QueryFirstOrDefaultAsync<long>($"select Count(Id) from {_dbName}.HCUAccountTransaction " +
                   "where AccountId = @accId and SourceId = @srcId and StatusId = @statId and (ModeId = @debit or " +
                   "ModeId = @credit) and ParentId = @parId;",
                   new
                   {
                       accId = accountId,
                       srcId = sourceId,
                       statId = statusId,
                       debit = TransactionMode.Debit,
                       credit = TransactionMode.Credit,
                       parId = parentId
                   });
                }
                else
                {
                    result = await con.QueryFirstOrDefaultAsync<long>($"select Count(Id) from {_dbName}.HCUAccountTransaction " +
                   "where AccountId = @accId and SourceId = @srcId and StatusId = @statId and (ModeId = @debit or " +
                   "ModeId = @credit);",
                   new
                   {
                       accId = accountId,
                       srcId = sourceId,
                       statId = statusId,
                       debit = TransactionMode.Debit,
                       credit = TransactionMode.Credit
                   });
                }

            }
            catch (Exception ex)
            {
                await RewardModuleHelper.LogErrorAsync(ex, "GetTransactionsCountAsync");
            }
            return result;
        }

        /// <summary>
        /// Gets SUM of HCUAccountTransactions for an account based on query parameters
        /// </summary>
        /// <param name="accountId">Account ID</param>
        /// <param name="sourceId">Transaction Source ID</param>
        /// <param name="statusId">Transaction Status</param>
        /// <param name="modeId">Transaction Mode ID (CREDIT or DEBIT)</param>
        /// <param name="parentId">Optional Parent Transaction ID</param>
        /// <returns></returns>
        private async Task<double> GetBalanceAsync(long accountId, long sourceId, long statusId, long modeId,
            long parentId = 0)
        {
            double result = 0;
            try
            {
                using var con = new MySqlConnection(HostHelper.Host);

                if (parentId > 0)
                {
                    var query = $"select Sum(TotalAmount) from {_dbName}.HCUAccountTransaction " +
                    "where AccountId = @accId and SourceId = @srcId and StatusId = @statId and ModeId = @modeId and " +
                    "ParentId = @parId;";

                    var queryParams = new
                    {
                        accId = accountId,
                        srcId = sourceId,
                        statId = statusId,
                        modId = modeId,
                        parId = parentId
                    };

                    result = await con.QueryFirstOrDefaultAsync<double>(query, queryParams);
                }
                else
                {
                    var query = $"select Sum(TotalAmount) from {_dbName}.HCUAccountTransaction " +
                    "where AccountId = @accId and SourceId = @srcId and StatusId = @statId and ModeId = @modId;";

                    var queryParams = new
                    {
                        accId = accountId,
                        srcId = sourceId,
                        statId = statusId,
                        modId = modeId
                    };

                    result = await con.QueryFirstOrDefaultAsync<double>(query, queryParams);
                }

                result = HCoreHelper.RoundNumber(result, _AppConfig.SystemEntryRoundDouble);
            }
            catch (Exception ex)
            {
                await RewardModuleHelper.LogErrorAsync(ex, "GetTransactionsCountAsync");
            }
            return result;
        }

        /// <summary>
        /// Updates HCUAccountTransaction record if it exists
        /// </summary>
        /// <param name="accountId">Account ID of the transaction</param>
        /// <param name="balance">New Balance</param>
        /// <param name="groupKey">Group Key</param>
        /// <returns>Returns 1 if updated, 0 if the record does not exist</returns>
        private async Task<int> UpdateTransactionBalanceAsync(long accountId, double balance, string? groupKey)
        {
            var rowsChanged = 0;
            try
            {
                using var con = new MySqlConnection(HostHelper.Host);
                rowsChanged = await con.ExecuteAsync($"Update {_dbName}.HCUAccountTransaction " +
                    "Set Balance = @bal where AccountId = @accId and GroupKey = @gKey", new
                    {
                        bal = balance,
                        accId = accountId,
                        gKey = groupKey
                    });
            }
            catch (Exception ex)
            {
                await RewardModuleHelper.LogErrorAsync(ex, "UpdateTransactionBalanceAsync");
            }
            return rowsChanged;
        }

        /// <summary>
        /// Updates HCUAccount record if it exists
        /// </summary>
        /// <param name="accountId">ID of the account</param>
        /// <param name="balance">New Account Balance</param>
        /// <returns>Returns 1 if updated, 0 if the record does not exist</returns>
        private async Task<int> UpdateAccountBalanceAsync(long accountId, double balance)
        {
            var rowsChanged = 0;
            try
            {
                using var con = new MySqlConnection(HostHelper.Host);
                rowsChanged = await con.ExecuteAsync($"Update {_dbName}.HCUAccount " +
                    "Set MainBalance = @bal where Id = @accId", new
                    {
                        bal = balance,
                        accId = accountId
                    });
            }
            catch (Exception ex)
            {
                await RewardModuleHelper.LogErrorAsync(ex, "UpdateAccountBalanceAsync");
            }
            return rowsChanged;
        }

        /// <summary>
        /// Updates HCUAccountBalance record if it exists
        /// </summary>
        /// <param name="accountId">User Account Id</param>
        /// <param name="parentId">Merchant Id</param>
        /// <param name="sourceId">Transaction Source ID</param>
        /// <param name="balance">New Balance</param>
        /// <param name="totalCredit">New Total Credit Sum</param>
        /// <param name="totalDebit">New Total Debit </param>
        /// <param name="transactions">New Transaction Total</param>
        /// <returns>Returns 1 if updated, 0 if the record does not exist and -1 if update operation fails.</returns>
        private async Task<int> UpdateAccountBalanceAsync(long accountId, long parentId, long sourceId, double balance,
            double totalCredit, double totalDebit, long transactions)
        {
            var rowsChanged = 0;
            try
            {
                using var con = new MySqlConnection(HostHelper.Host);
                rowsChanged = await con.ExecuteAsync($"Update {_dbName}.HCUAccountBalance " +
                    "Set Balance = @bal, Credit = @cBal, Debit = @dBal, Transactions = @trx" +
                    " where AccountId = @accId and ParentId = @parId and SourceId = @srcId and StatusId = @statId", new
                    {
                        bal = balance,
                        cBal = totalCredit,
                        dBal = totalDebit,
                        trx = transactions,
                        accId = accountId,
                        parId = parentId,
                        srcId = sourceId,
                        statId = Transaction.Success
                    });
            }
            catch (Exception ex)
            {
                await RewardModuleHelper.LogErrorAsync(ex, "UpdateAccountBalanceAsync");
                rowsChanged = -1;
            }
            return rowsChanged;
        }

        #endregion
    }
}

