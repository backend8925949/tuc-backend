﻿using HCore.Data.Helper;
using HCore.Helper;
using HCore.Operations.Object;
using HCore.TUC.Core.Framework.Merchant.App.AWS.Helper;
using MySql.Data.MySqlClient;
using Dapper;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant;
using HCore.Data;
using HCore.Data.Models;
using HCore.TUC.Core.Object.Merchant.App;
using Microsoft.Extensions.Caching.Memory;

namespace HCore.TUC.Core.Framework.Merchant.App.AWS
{
    public class CustomerService : ICustomerService
    {
        private readonly IMemoryCache _cache;
        private readonly string _dbName;

        public CustomerService(IMemoryCache cache)
        {
            _cache = cache;
            _dbName = GetDatabase();
        }

        public async Task<OAppProfile.Response> CreateAppUserAccountAsync(OAppProfile.Request request)
        {
            OAppProfile.Response? result = null;
            try
            {
                if (string.IsNullOrEmpty(request.MobileNumber))
                {
                    return new OAppProfile.Response
                    {
                        Status = ResponseStatus.Error,
                        StatusMessage = "Mobile number required",
                        StatusResponseCode = "AUA100"
                    };
                }

                DateTime dateCreated = DateTime.UtcNow;

                using var con = new MySqlConnection(HostHelper.Host);
                var merchantName = await con.QueryFirstOrDefaultAsync<string>(
                    $"select DisplayName from {_dbName}.HCUAccount where Id = @oId and (AccountTypeId = @merchant or AccountTypeId = @acquirer);",
                    new
                    {
                        oId = request.OwnerId,
                        merchant = UserAccountType.Merchant,
                        acquirer = UserAccountType.Acquirer
                    });

                var genderId = await GetGenderId(request.GenderCode);

                string appAccount = _AppConfig.AppUserPrefix + request.MobileNumber;
                long existingUserId = await con.QueryFirstOrDefaultAsync<long>(
                    $"select Id from {_dbName}.HCUAccountAuth where Username = @username", new { username = appAccount });

                if (request.UserReference != null && request.UserReference.CountryId > 0)
                {
                    request.MobileNumber = HCoreHelper.FormatMobileNumber(request.UserReference.CountryId.Value.ToString(),
                        request.MobileNumber);
                }

                if (existingUserId != 0)
                {
                    result = await con.QueryFirstOrDefaultAsync<OAppProfile.Response>(
                        "select Id as AccountId, Guid as AccountKey, createdDate, OwnerId, Name, EmailAddress, MobileNumber," +
                        $"StatusId, AccountCode, CountryId FROM {_dbName}.HCUAccount where UserId = @uId and AccountTypeId = " +
                        "@typeId", new { uId = existingUserId, typeId = UserAccountType.Appuser });

                    if (result != null)
                    {
                        result.Status = ResponseStatus.Success;
                        result.StatusResponseCode = "Account created";
                        result.StatusResponseCode = "AUA105";

                        return result;
                    }
                    else
                    {
                        result = await CreateAccountAsync(request, genderId, merchantName);
                    }
                }
                else
                {
                    result = await CreateAccountAsync(request, genderId, merchantName);
                }
            }
            catch (Exception ex)
            {
                await RewardModuleHelper.LogErrorAsync(ex, "CreateAppUserAccountAsync");
            }
            return result;
        }

        public async Task<OReward.Confirm.Response> GetAccountDetailsAsync(long accountTypeId, string username)
        {
            OReward.Confirm.Response response = null;
            try
            {
                var cacheKey = accountTypeId + username;
                if (!_cache.TryGetValue(cacheKey, out response))
                {
                    using var con = new MySqlConnection(HostHelper.Host);

                    var userId = await con.QueryFirstOrDefaultAsync<long>(
                        $"select Id from {_dbName}.HCUAccountAuth where Username = @usrname", new { usrname = username });

                    if (userId > 0)
                    {
                        var query = "select Id as ReferenceId, Guid as ReferenceKey, DisplayName, Name, MobileNumber, StatusId, " +
                        $" AccountCode as AccountNumber, EmailAddress " +
                        $" from {_dbName}.HCUAccount " +
                        " where AccountTypeId = @typeId and UserId = @usrId";

                        response = await con.QueryFirstOrDefaultAsync<OReward.Confirm.Response>(query, new
                        {
                            typeId = accountTypeId,
                            usrId = userId
                        });

                        if (response != null)
                        {
                            _cache.Set(cacheKey, response, TimeSpan.FromDays(1));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                await RewardModuleHelper.LogErrorAsync(ex, "GetAccountDetailsAsync");
            }
            return response;
        }

        #region Private Members
        private static async Task<OAppProfile.Response> CreateAccountAsync(OAppProfile.Request request, int genderId,
            string merchantName)
        {
            OAppProfile.Response? result = null;
            try
            {
                var createdDate = DateTime.UtcNow;
                string userKey = HCoreHelper.GenerateGuid();
                string userAccountKey = HCoreHelper.GenerateGuid();
                var random = new Random();
                string accessPin = random.Next(1111, 9999).ToString();
                if (HostEnvironment == HostEnvironmentType.Test)
                {
                    accessPin = "1234";
                }
                #region Save User
                var password = HCoreEncrypt.EncryptHash(request.MobileNumber ?? "");
                var accountAuth = new HCUAccountAuth
                {
                    Guid = userKey,
                    Username = _AppConfig.AppUserPrefix + request.MobileNumber,
                    Password = password,
                    SecondaryPassword = password,
                    SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid()),
                    CreateDate = createdDate,
                    StatusId = HelperStatus.Default.Active,
                };

                if (request.CreatedById != 0)
                {
                    accountAuth.CreatedById = request.CreatedById;
                }
                else
                {
                    if (request.UserReference != null && request.UserReference.AccountId != 0)
                    {
                        accountAuth.CreatedById = request.UserReference.AccountId;
                    }
                }
                #endregion

                #region Online Account
                string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                var account = new HCUAccount
                {
                    Guid = userAccountKey,
                    AccountTypeId = UserAccountType.Appuser,
                    AccountOperationTypeId = AccountOperationType.Online,
                    EmailVerificationStatus = 0,
                    EmailVerificationStatusDate = createdDate,
                    NumberVerificationStatus = 0,
                    NumberVerificationStatusDate = createdDate,

                    AccountCode = AccountCode,
                    ReferralCode = request.MobileNumber,
                    RegistrationSourceId = RegistrationSource.System,
                    CreateDate = createdDate,
                    StatusId = HelperStatus.Default.Active,
                    User = accountAuth,
                    AccessPin = HCoreEncrypt.EncryptHash(accessPin),
                    MobileNumber = request.MobileNumber,

                    OwnerId = request.OwnerId != 0 ? request.OwnerId : null,
                    SubOwnerId = request.SubOwnerId != 0 ? request.SubOwnerId : null,
                    DisplayName = !string.IsNullOrEmpty(request.DisplayName) ? request.DisplayName :
                    !string.IsNullOrEmpty(request.MobileNumber) ? request.MobileNumber : "",
                    FirstName = request.FirstName,
                    MiddleName = request.MiddleName,
                    LastName = request.LastName,
                    Name = request.Name,
                    GenderId = genderId,
                    DateOfBirth = request.DateOfBirth,
                    CountryId = request.CountryId > 0 ? request.CountryId :
                    request.UserReference != null && request.UserReference.CountryId > 0 ? request.UserReference.CountryId :
                    null,
                    CreatedById = request.CreatedById > 0 ? request.CreatedById :
                    request.UserReference != null && request.UserReference.AccountId > 0 ? request.UserReference.AccountId :
                    null,
                    RequestKey = request.UserReference != null ? request.UserReference.RequestKey : null
                };

                if (!string.IsNullOrEmpty(request.MobileNumber))
                {
                    account.MobileNumber = request.MobileNumber;
                    account.ContactNumber = request.MobileNumber;
                }

                if (!string.IsNullOrEmpty(request.EmailAddress))
                {
                    account.EmailAddress = request.EmailAddress;
                    account.SecondaryEmailAddress = request.EmailAddress;
                }

                using (var context = new HCoreContext())
                {
                    await context.HCUAccount.AddAsync(account);
                    var rowsChanged = await context.SaveChangesAsync();
                    #endregion

                    if (rowsChanged > 0)
                    {
                        if (HostEnvironment != HostEnvironmentType.Local)
                        {
                            if (!string.IsNullOrEmpty(request.MobileNumber))
                            {
                                if (!string.IsNullOrEmpty(merchantName))
                                {
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", request.MobileNumber, "Welcome to " + merchantName + " Thank U Cash. To redeem your cash, use pin: " + accessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app", account.Id, null);
                                }
                                else
                                {
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", request.MobileNumber, "Welcome to Thank U Cash. To redeem your cash, use pin: " + accessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app", account.Id, null);
                                }
                            }
                        }

                        result = new OAppProfile.Response
                        {
                            AccountId = account.Id,
                            AccountKey = userAccountKey,
                            OwnerId = request.OwnerId,
                            SubOwnerId = account.SubOwnerId,
                            CreateDate = createdDate,
                            CountryId = account.CountryId,
                            MobileNumber = request.MobileNumber,
                            AccountCode = AccountCode,
                            StatusMessage = "Account created",
                            StatusResponseCode = "AUA107",
                            Status = ResponseStatus.Success,
                            StatusId = HelperStatus.Default.Active
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                await RewardModuleHelper.LogErrorAsync(ex, "CreateAccountAsync");
            }
            return result;
        }

        private static string GetDatabase()
        {
            var result = string.Empty;
            var dbObjects = HostHelper.Host.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var ob in dbObjects)
            {
                if (ob.Contains("Database"))
                {
                    var dbExp = ob.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                    result = dbExp[1];
                    break;
                }
            }
            return result;
        }

        private async Task<int> GetGenderId(string? genderCode)
        {
            var result = 0;
            try
            {
                if (!_cache.TryGetValue(genderCode, out result))
                {
                    var dbName = GetDatabase();
                    using var con = new MySqlConnection(HostHelper.Host);
                    result = !string.IsNullOrEmpty(genderCode) ?
                        await con.QueryFirstOrDefaultAsync<int>(
                        $"select Id from {dbName}.HCCore where LOWER(Name) = @gCode and ParentId = @pId and StatusId = @sId",
                        new
                        {
                            gCode = genderCode.ToLower(),
                            pId = HelperType.Gender,
                            sId = HelperStatus.Default.Active
                        })
                        : 0;

                    if (result > 0)
                    {
                        _cache.Set(genderCode, result, TimeSpan.FromDays(1));
                    }
                }
            }
            catch (Exception ex)
            {
                await RewardModuleHelper.LogErrorAsync(ex, "GetGenderId");
            }
            return result;
        }

        #endregion
    }
}