﻿using HCore.TUC.Core.Object.Merchant;
using static HCore.TUC.Core.Object.Merchant.OUpload.RewardUpload;

namespace HCore.TUC.Core.Framework.Merchant.App.AWS
{
	public interface IRewardService
	{
		Task<RewardCustomerResponse> RewardCustomerAsync(OUpload.RewardUpload.Item dataItem);
	}
}

