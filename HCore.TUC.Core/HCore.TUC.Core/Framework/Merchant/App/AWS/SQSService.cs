﻿using Amazon.SQS.Model;
using HCore.Helper;
using HCore.TUC.Core.Object.Merchant;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Core.Framework.Merchant.App.AWS
{
    public class SQSService : ISQSService
    {
        public SQSService()
        {
        }

        public async Task<(bool Success, string Message)> QueueMessageAsync(OUpload.RewardUpload.MQUploadRequest request)
        {
            (bool Success, string Message) result;
            try
            {
                var awsCredentials = new Amazon.Runtime.BasicAWSCredentials(_AppConfig.SQSAccessKey, _AppConfig.SQSSecretKey);
                using var client = new Amazon.SQS.AmazonSQSClient(awsCreden‌​tials, Amazon.RegionEndpoint.EUWest2);

                if(request != null && request.Customers != null && request.Customers.Count > 0)
                {
                    var customerCount = request.Customers.Count;
                    if(customerCount > 1000)
                    {
                        var batches = request.Customers.Chunk(1000).ToList();
                        List<Task> taskList = new List<Task>();
                        foreach (var customerBatch in batches)
                        {
                            var newRequest = new OUpload.RewardUpload.MQUploadRequest
                            {
                                AccountId = request.AccountId,
                                AccountKey = request.AccountKey,
                                CountryIsd = request.CountryIsd,
                                Customers = customerBatch.ToList(),
                                Environment = request.Environment,
                                FileName = request.FileName,
                                MerchantId = request.MerchantId,
                                MobileNumberLength = request.MobileNumberLength,
                                UserReference = request.UserReference
                            };

                            var task = SendMessageWithoutCatchingError(newRequest);
                            taskList.Add(task);
                        }

                        await Task.WhenAll(taskList);
                        result = (true, "OK");
                    }
                    else
                    {
                        var r = await SendMessageWithoutCatchingError(request);
                        result = (true, "OK");
                    }
                }
                else
                {
                    result = (false, "The request is null");
                }
            }
            catch(Exception ex)
            {
                HCoreHelper.LogException(LogLevel.High, "QueueMessageAsync", ex, null);
                result = (false, ex.Message);
            }
            return result;
        }

        public async Task<SendMessageResponse> SendMessageToSqsQueueAsync(object request)
        {
            SendMessageResponse sendMessageResponse = new SendMessageResponse();
            try
            {
                sendMessageResponse = await SendMessageWithoutCatchingError(request);
            }
            catch(Exception ex)
            {
                HCoreHelper.LogException(LogLevel.High, "SendMessageToSqsQueueAsync", ex, null);
            }
            return sendMessageResponse;
        }

        private async Task<SendMessageResponse> SendMessageWithoutCatchingError(object request)
        {
            var awsCredentials = new Amazon.Runtime.BasicAWSCredentials(_AppConfig.SQSAccessKey, _AppConfig.SQSSecretKey);
            using var client = new Amazon.SQS.AmazonSQSClient(awsCreden‌​tials, Amazon.RegionEndpoint.EUWest2);
            var sendMessageRequest = new SendMessageRequest
            {
                QueueUrl = _AppConfig.SQSQueueUrl,
                MessageBody = JsonConvert.SerializeObject(request),
                MessageGroupId = _AppConfig.SQSMessageGroupId
            };

            return await client.SendMessageAsync(sendMessageRequest);
        }
    }
}

