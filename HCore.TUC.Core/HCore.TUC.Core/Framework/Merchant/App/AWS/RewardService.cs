﻿using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations.Object;
using HCore.TUC.Core.Framework.Merchant.App.AWS.Helper;
using HCore.TUC.Core.Object.Merchant.App;
using HCore.TUC.Core.Resource;
using SendGrid;
using SendGrid.Helpers.Mail;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.TUC.Core.Object.Merchant.OUpload.RewardUpload;

namespace HCore.TUC.Core.Framework.Merchant.App.AWS
{
    public class RewardService : IRewardService
    {
        private readonly ITransactionService _trxService;
        private readonly ICustomerService _customerService;

        public RewardService(ITransactionService trxService,
            ICustomerService customerService)
        {
            _trxService = trxService;
            _customerService = customerService;
        }

        public async Task<RewardCustomerResponse> RewardCustomerAsync(Item dataItem)
        {
            var rawResponse = GetResponse(string.Empty, RewardCustomerReprocessFlag.DoNotReprocess, RewardCustomerStatus.Failed);
            try
            {
                using (var context = new HCoreContext())
                {
                    var customerDisplayName = string.Empty;
                    string customerEmailAddress = string.Empty;
                    string customerAccountNumber = string.Empty;
                    OCoreTransaction.Request coreTransactionRequest;
                    OReward.Confirm.Response rewardConfirmResponse = new();
                    List<OCoreTransaction.TransactionItem> transactionItems;

                    if (!string.IsNullOrEmpty(dataItem.MobileNumber))
                    {
                        dataItem.MobileNumber = HCoreHelper.FormatMobileNumber(dataItem.CountryIsd, dataItem.MobileNumber, dataItem.MobileNumberLength);
                        var validateMobileNumber = HCoreHelper.ValidateMobileNumber(dataItem.MobileNumber);
                        if (validateMobileNumber.IsNumberValid == true)
                        {
                            string tMobileNumber = _AppConfig.AppUserPrefix + dataItem.MobileNumber;
                            var accountDetails = await _customerService.GetAccountDetailsAsync(UserAccountType.Appuser,
                                tMobileNumber);

                            if (accountDetails != null)
                            {
                                if (accountDetails.StatusId == HelperStatus.Default.Active)
                                {
                                    customerAccountNumber = accountDetails.AccountNumber ?? string.Empty;
                                    customerDisplayName = accountDetails.DisplayName;
                                    customerEmailAddress = accountDetails.EmailAddress ?? string.Empty;

                                    rewardConfirmResponse.ReferenceId = accountDetails.ReferenceId;
                                    rewardConfirmResponse.ReferenceKey = accountDetails.ReferenceKey;
                                    rewardConfirmResponse.DisplayName = accountDetails.DisplayName;
                                    rewardConfirmResponse.Name = accountDetails.Name;
                                    rewardConfirmResponse.IconUrl = accountDetails.IconUrl;
                                    rewardConfirmResponse.MobileNumber = accountDetails.MobileNumber;
                                    rewardConfirmResponse.EmailAddress = accountDetails.EmailAddress;
                                    rewardConfirmResponse.StatusId = accountDetails.StatusId;
                                    rewardConfirmResponse.AccountNumber = accountDetails.AccountNumber;
                                }
                            }
                            else
                            {
                                var appProfileRequest = new OAppProfile.Request
                                {
                                    OwnerId = dataItem.MerchantId,
                                    CreatedById = dataItem.MerchantId,
                                    MobileNumber = dataItem.MobileNumber,
                                    EmailAddress = dataItem.EmailAddress,
                                    DateOfBirth = dataItem.DateOfBirth,
                                    Name = dataItem.Name,
                                    DisplayName = string.IsNullOrEmpty(dataItem.Name) ? dataItem.MobileNumber : dataItem.Name,
                                    GenderCode = string.IsNullOrEmpty(dataItem.Gender) ? null : dataItem.Gender.ToLower(),
                                    UserReference = dataItem.UserReference
                                };

                                var appUserCreateResponse = await _customerService.CreateAppUserAccountAsync(appProfileRequest);
                                if (appUserCreateResponse != null)
                                {
                                    if (appUserCreateResponse.Status == ResponseStatus.Success)
                                    {
                                        customerAccountNumber = appUserCreateResponse.AccountCode ?? string.Empty;
                                        customerEmailAddress = dataItem.EmailAddress ?? string.Empty;
                                        customerDisplayName = dataItem.Name;

                                        rewardConfirmResponse.ReferenceId = appUserCreateResponse.AccountId;
                                        rewardConfirmResponse.ReferenceKey = appUserCreateResponse.AccountKey;
                                        rewardConfirmResponse.DisplayName = appUserCreateResponse.DisplayName;
                                        rewardConfirmResponse.Name = appUserCreateResponse.Name;
                                        rewardConfirmResponse.MobileNumber = appUserCreateResponse.MobileNumber;
                                        rewardConfirmResponse.StatusId = appUserCreateResponse.StatusId;
                                        rewardConfirmResponse.EmailAddress = appUserCreateResponse.EmailAddress;
                                        rewardConfirmResponse.AccountNumber = appUserCreateResponse.AccountCode;
                                    }
                                    else
                                    {
                                        var cardRelatedErrorResponseCodes = new List<string> { "AUA101", "AUA102", "AUA103", "AUA104" };
                                        rawResponse.ResponseMessage = appUserCreateResponse.StatusMessage;
                                        rawResponse.Status = RewardCustomerStatus.Failed;
                                        if (!string.IsNullOrEmpty(appUserCreateResponse.StatusResponseCode) &&
                                            cardRelatedErrorResponseCodes.Contains(appUserCreateResponse.StatusResponseCode))
                                        {
                                            rawResponse.ReprocessFlag = RewardCustomerReprocessFlag.DoNotReprocess;
                                        }
                                        else
                                        {
                                            rawResponse.ReprocessFlag = RewardCustomerReprocessFlag.Reprocess;
                                        }
                                        return rawResponse;
                                    }
                                }
                                else
                                {
                                    return GetResponse("User creation failed with error(s)", RewardCustomerReprocessFlag.Reprocess,
                                        RewardCustomerStatus.Failed);
                                }
                            }

                            var tucGold = await _trxService.GetConfigurationDetailsAsync("thankucashgold", dataItem.MerchantId);
                            var rewardPerConfig = await _trxService.GetConfigurationAsync("rewardpercentage", dataItem.MerchantId);
                            double tucRewardPercentage = !string.IsNullOrEmpty(rewardPerConfig) ? HCoreHelper.RoundNumber(Convert.ToDouble(rewardPerConfig),
                                   _AppConfig.SystemRoundPercentage) : 0;

                            if (tucRewardPercentage > 0)
                            {
                                double tucRewardAmount = HCoreHelper.GetPercentage(dataItem.InvoiceAmount, tucRewardPercentage, _AppConfig.SystemEntryRoundDouble);
                                if (tucRewardAmount <= 0)
                                {
                                    return GetResponse(TUCCoreResource.CA1172M, RewardCustomerReprocessFlag.DoNotReprocess, RewardCustomerStatus.PartlyProcessed);
                                }
                                if (tucRewardAmount > dataItem.InvoiceAmount)
                                {
                                    return GetResponse(TUCCoreResource.CAA14138M, RewardCustomerReprocessFlag.DoNotReprocess, RewardCustomerStatus.PartlyProcessed);
                                }

                                string referenceNumber = "TRA" + HCoreHelper.GenerateDateString() + HCoreHelper.GetAccountIdString(dataItem.MerchantId);
                                var commissionConfig = await _trxService.GetConfigurationAsync("rewardcommissionfromrewardamount", dataItem.MerchantId);

                                int thankUCashPercentageFromRewardAmount = !string.IsNullOrEmpty(commissionConfig) ? Convert.ToInt32(commissionConfig) : 0;

                                int transactionStatusId = HelperStatus.Transaction.Success;
                                double tucRewardUserAmount = 0;
                                double tucRewardUserAmountPercentage = 0;
                                double tucRewardCommissionAmount = 0;
                                double tucRewardCommissionAmountPercentage = 0;
                                if (thankUCashPercentageFromRewardAmount == 1)
                                {
                                    var rewardDeductionType = await _trxService.GetConfigurationDetailsAsync("rewarddeductiontype",
                                        dataItem.MerchantId);
                                    var userRewardPercenConfig = await _trxService.GetConfigurationAsync("userrewardpercentage", dataItem.MerchantId);
                                    tucRewardUserAmountPercentage = !string.IsNullOrEmpty(userRewardPercenConfig) ?
                                        HCoreHelper.RoundNumber(Convert.ToDouble(userRewardPercenConfig), _AppConfig.SystemRoundPercentage) : 0;
                                    tucRewardUserAmount = HCoreHelper.GetPercentage(tucRewardAmount, tucRewardUserAmountPercentage);
                                    tucRewardCommissionAmount = tucRewardAmount - tucRewardUserAmount;
                                    tucRewardCommissionAmountPercentage = (100 - tucRewardUserAmountPercentage);
                                    if (rewardDeductionType.TypeCode == "rewarddeductiontype.prepay")
                                    {
                                        double merchantBalance = await _trxService.GetMerchantBalanceAsync(dataItem.MerchantId, TransactionSource.Merchant);
                                        if (merchantBalance >= tucRewardAmount)
                                        {
                                            transactionStatusId = HelperStatus.Transaction.Success;
                                        }
                                        else
                                        {
                                            transactionStatusId = HelperStatus.Transaction.Pending;
                                        }
                                    }
                                    else if (rewardDeductionType.TypeCode == "rewarddeductiontype.postpay")
                                    {
                                        transactionStatusId = HelperStatus.Transaction.Success;
                                    }
                                    else
                                    {
                                        return GetResponse(TUCCoreResource.CAA14135M, RewardCustomerReprocessFlag.DoNotReprocess, RewardCustomerStatus.PartlyProcessed);
                                    }
                                }
                                else   // Custom Calculation Gen 2  || Merchant Commission Extra to the Reward Amount
                                {
                                    var customRewardConfig = await _trxService.GetConfigurationAsync("customreward", dataItem.MerchantId);
                                    int tAllowCustomReward = !string.IsNullOrEmpty(customRewardConfig) ?
                                        Convert.ToInt32(customRewardConfig) : 0;
                                    if (tAllowCustomReward == 1)
                                    {
                                        if (dataItem.RewardAmount > 0)
                                        {
                                            tucRewardAmount = dataItem.RewardAmount;
                                        }
                                    }

                                    double maximumCommissionAmount = 2500;
                                    var defaultCommConfig = await _trxService.GetConfigurationAsync("rewardcomissionpercentage", 0);
                                    var systemDefaultCommissionPercentage = !string.IsNullOrEmpty(defaultCommConfig) ?
                                         HCoreHelper.RoundNumber(Convert.ToDouble(defaultCommConfig), _AppConfig.SystemRoundPercentage) : 0;

                                    var rewardCommiConfig = await _trxService.GetConfigurationAsync("rewardcomissionpercentage", dataItem.MerchantId);
                                    double systemMerchantCommissionPercentage = !string.IsNullOrEmpty(rewardCommiConfig) ?
                                        HCoreHelper.RoundNumber(Convert.ToDouble(rewardCommiConfig), _AppConfig.SystemRoundPercentage) : 0;

                                    double systemCategoryCommissionPercentage = 0;
                                    if (dataItem.MerchantCategoryId > 0)
                                    {
                                        var CategoryCommission = context.TUCMerchantCategory.Where(x => x.RootCategoryId == dataItem.MerchantCategoryId)
                                                .Select(x => new
                                                {
                                                    x.Commission,
                                                }).FirstOrDefault();

                                        if (CategoryCommission != null && CategoryCommission.Commission > 0)
                                        {
                                            systemCategoryCommissionPercentage = (double)CategoryCommission.Commission;
                                        }
                                    }

                                    tucRewardCommissionAmountPercentage = systemMerchantCommissionPercentage > 0 ? systemMerchantCommissionPercentage :
                                         systemCategoryCommissionPercentage > 0 ? systemCategoryCommissionPercentage :
                                         0;

                                    tucRewardCommissionAmount = HCoreHelper.GetPercentage(dataItem.InvoiceAmount, tucRewardCommissionAmountPercentage);

                                    tucRewardCommissionAmount = tucGold != null && tucGold.Value == "1" ? 0 : maximumCommissionAmount;
                                    tucRewardUserAmount = tucRewardAmount;
                                    tucRewardUserAmountPercentage = tucRewardPercentage;
                                    tucRewardAmount = tucRewardUserAmount + tucRewardCommissionAmount;
                                    if (tucRewardAmount > dataItem.InvoiceAmount)
                                    {
                                        return GetResponse(TUCCoreResource.CAA14138M, RewardCustomerReprocessFlag.DoNotReprocess, RewardCustomerStatus.PartlyProcessed);
                                    }

                                    if (tucGold != null && tucGold.Value == "1")
                                    {
                                        transactionStatusId = HelperStatus.Transaction.Success;
                                    }
                                    else
                                    {
                                        double merchantBalance = await _trxService.GetMerchantBalanceAsync(dataItem.MerchantId, TransactionSource.Merchant);
                                        if (merchantBalance >= tucRewardAmount)
                                        {
                                            transactionStatusId = HelperStatus.Transaction.Success;
                                        }
                                        else
                                        {
                                            if (merchantBalance > -5000)
                                            {
                                                if ((merchantBalance - tucRewardAmount) > -5000)
                                                {
                                                    transactionStatusId = HelperStatus.Transaction.Pending;
                                                }
                                                else
                                                {
                                                    return GetResponse(TUCCoreResource.CAA14137M, RewardCustomerReprocessFlag.DoNotReprocess,
                                                    RewardCustomerStatus.PartlyProcessed);
                                                }
                                            }
                                            else
                                            {
                                                return GetResponse(TUCCoreResource.CAA14136M, RewardCustomerReprocessFlag.DoNotReprocess,
                                                    RewardCustomerStatus.PartlyProcessed);
                                            }
                                        }
                                    }
                                }

                                coreTransactionRequest = new OCoreTransaction.Request();
                                coreTransactionRequest.GroupKey = referenceNumber;
                                coreTransactionRequest.CustomerId = rewardConfirmResponse.ReferenceId;
                                coreTransactionRequest.UserReference = dataItem.UserReference;
                                coreTransactionRequest.StatusId = transactionStatusId;
                                coreTransactionRequest.ParentId = dataItem.MerchantId;
                                coreTransactionRequest.InvoiceAmount = dataItem.InvoiceAmount;
                                coreTransactionRequest.ReferenceInvoiceAmount = dataItem.InvoiceAmount;
                                coreTransactionRequest.ReferenceAmount = tucRewardAmount;
                                coreTransactionRequest.ReferenceNumber = dataItem.ReferenceNumber;
                                transactionItems = new List<OCoreTransaction.TransactionItem>();

                                coreTransactionRequest.Transactions = await _trxService.ComputeTransactionItemsAsync(
                                    dataItem, rewardConfirmResponse.ReferenceId, tucRewardAmount, tucRewardUserAmount,
                                    tucRewardCommissionAmount, tucGold);

                                if (coreTransactionRequest.Transactions.Count > 0)
                                {
                                    OCoreTransaction.Response transactionResponse = await _trxService
                                        .ProcessTransactionAsync(coreTransactionRequest);

                                    if (transactionResponse.Status == HelperStatus.Transaction.Success)
                                    {
                                        if (!string.IsNullOrEmpty(rewardConfirmResponse.IconUrl))
                                        {
                                            rewardConfirmResponse.IconUrl = _AppConfig.StorageUrl + rewardConfirmResponse.IconUrl;
                                        }
                                        else
                                        {
                                            rewardConfirmResponse.IconUrl = _AppConfig.Default_Icon;
                                        }
                                        rewardConfirmResponse.RewardAmount = tucRewardAmount;
                                        rewardConfirmResponse.InvoiceAmount = dataItem.InvoiceAmount;
                                        rewardConfirmResponse.TransactionReferenceId = transactionResponse.ReferenceId;
                                        rewardConfirmResponse.TransactionDate = transactionResponse.TransactionDate;
                                        if (transactionStatusId == HelperStatus.Transaction.Success)
                                        {
                                            rewardConfirmResponse.StatusName = "success";
                                        }
                                        else
                                        {
                                            rewardConfirmResponse.StatusName = "pending";
                                        }
                                        if (transactionStatusId == HelperStatus.Transaction.Success)
                                        {
                                            if (dataItem.MerchantId == 583505 || dataItem.MerchantId == 13)
                                            {
                                                var CustomerBalance = await _trxService.GetAppUserBalanceAsync(rewardConfirmResponse.ReferenceId, dataItem.MerchantId, TransactionSource.TUCBlack);
                                                if (!string.IsNullOrEmpty(dataItem.MobileNumber))
                                                {
                                                    string Message = "Credit Alert: You have earned " + tucRewardAmount + " WakaPoints. Your current balance is: " + CustomerBalance + " points. Book now on https://www.wakanow.com";
                                                    HCoreHelper.SendSMS(SmsType.Transaction, dataItem.CountryIsd, dataItem.MobileNumber, Message, rewardConfirmResponse.ReferenceId, null);
                                                }
                                                if (!string.IsNullOrEmpty(customerEmailAddress)) // Wakanow email
                                                {
                                                    string Template = "";
                                                    string currentDirectory = Directory.GetCurrentDirectory();
                                                    string path = "templates";
                                                    string fullPath = Path.Combine(currentDirectory, path, "wakanow_alert_reward.html");
                                                    using (StreamReader reader = File.OpenText(fullPath))
                                                    {
                                                        Template = reader.ReadToEnd();
                                                    }
                                                    var _EmailParameters = new
                                                    {
                                                        MerchantLogo = "https://points.wakanow.com/assets/img/wakanow-logo.png",
                                                        MerchantDisplayName = "WakaPoints",
                                                        UserDisplayName = customerDisplayName,
                                                        AccountNumber = customerAccountNumber,
                                                        Amount = tucRewardAmount.ToString(),
                                                        Balance = CustomerBalance.ToString(),
                                                    };
                                                    Template = Template.Replace("{{MerchantLogo}}", _EmailParameters.MerchantLogo);
                                                    Template = Template.Replace("{{MerchantDisplayName}}", _EmailParameters.MerchantDisplayName);
                                                    Template = Template.Replace("{{UserDisplayName}}", _EmailParameters.UserDisplayName);
                                                    Template = Template.Replace("{{AccountNumber}}", _EmailParameters.AccountNumber);
                                                    Template = Template.Replace("{{Amount}}", _EmailParameters.Amount);
                                                    Template = Template.Replace("{{Balance}}", _EmailParameters.Balance);
                                                    var client = new SendGridClient("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");
                                                    var msg = new SendGridMessage()
                                                    {
                                                        From = new EmailAddress("noreply@wakanow.com", "WakaPoints"),
                                                        Subject = "WakaPoints Credit Alert - " + _EmailParameters.Amount + " points rewarded",
                                                        HtmlContent = Template
                                                    };
                                                    msg.AddTo(new EmailAddress(customerEmailAddress, customerDisplayName));
                                                    var response = client.SendEmailAsync(msg);
                                                }
                                            }
                                            else
                                            {
                                                var customerBalance = await _trxService.GetAppUserBalanceAsync(rewardConfirmResponse.ReferenceId);
                                                string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(tucRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString() + " points cash reward from " + dataItem.MerchantDisplayName + "  Bal: N" + HCoreHelper.RoundNumber((customerBalance + tucRewardUserAmount), _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
                                                HCoreHelper.SendSMS(SmsType.Transaction, dataItem.CountryIsd, dataItem.MobileNumber, Message, rewardConfirmResponse.ReferenceId, transactionResponse.GroupKey, transactionResponse.ReferenceId);

                                                if (!string.IsNullOrEmpty(customerEmailAddress))
                                                {
                                                    var _EmailParameters = new
                                                    {
                                                        UserDisplayName = customerDisplayName,
                                                        MerchantName = dataItem.MerchantDisplayName,
                                                        InvoiceAmount = dataItem.InvoiceAmount.ToString(),
                                                        Amount = HCoreHelper.RoundNumber(tucRewardUserAmount, 2).ToString(),
                                                        Balance = customerBalance.ToString(),
                                                    };
                                                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RewardEmail, customerDisplayName, customerEmailAddress, _EmailParameters, dataItem.UserReference);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var customerBalance = await _trxService.GetAppUserBalanceAsync(rewardConfirmResponse.ReferenceId);
                                            string rewardSms = await _trxService.GetConfigurationAsync("pendingrewardsms", dataItem.MerchantId);
                                            if (!string.IsNullOrEmpty(rewardSms))
                                            {
                                                string Message = rewardSms
                                                .Replace("[AMOUNT]", HCoreHelper.RoundNumber(tucRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString())
                                                .Replace("[BALANCE]", HCoreHelper.RoundNumber(customerBalance, _AppConfig.SystemExitRoundDouble).ToString())
                                                .Replace("[MERCHANT]", dataItem.MerchantDisplayName)
                                                .Replace("[TCODE]", HCoreHelper.GenerateRandomNumber(4));
                                                HCoreHelper.SendSMS(SmsType.Transaction, dataItem.CountryIsd, dataItem.MobileNumber, Message, rewardConfirmResponse.ReferenceId, transactionResponse.GroupKey, transactionResponse.ReferenceId);
                                            }
                                            else
                                            {
                                                string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(tucRewardUserAmount, _AppConfig.SystemExitRoundDouble).ToString() + " pending points cash reward from " + dataItem.MerchantDisplayName + "  Bal: N" + HCoreHelper.RoundNumber(customerBalance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
                                                HCoreHelper.SendSMS(SmsType.Transaction, dataItem.CountryIsd, dataItem.MobileNumber, Message, rewardConfirmResponse.ReferenceId, transactionResponse.GroupKey, transactionResponse.ReferenceId);
                                            }

                                            if (!string.IsNullOrEmpty(rewardConfirmResponse.EmailAddress))
                                            {
                                                var _EmailParameters = new
                                                {
                                                    UserDisplayName = rewardConfirmResponse.DisplayName,
                                                    MerchantName = dataItem.MerchantDisplayName,
                                                    InvoiceAmount = dataItem.InvoiceAmount.ToString(),
                                                    Amount = HCoreHelper.RoundNumber(tucRewardUserAmount, 2).ToString(),
                                                    Balance = customerBalance.ToString(),
                                                };
                                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.PendingRewardEmail, rewardConfirmResponse.DisplayName, rewardConfirmResponse.EmailAddress, _EmailParameters, dataItem.UserReference);
                                            }
                                        }

                                        rawResponse = GetResponse(transactionResponse.Message ?? "", RewardCustomerReprocessFlag.DoNotReprocess,
                                     RewardCustomerStatus.Completed);
                                    }
                                    else
                                    {
                                        return GetResponse(TUCCoreResource.CA1166M, RewardCustomerReprocessFlag.Reprocess,
                                            RewardCustomerStatus.PartlyProcessed);
                                    }
                                }
                                else
                                {
                                    rawResponse = GetResponse("Transactions already processed", RewardCustomerReprocessFlag.DoNotReprocess,
                                     RewardCustomerStatus.Completed);
                                }

                            }
                            else
                            {
                                rawResponse = GetResponse("No reward processing", RewardCustomerReprocessFlag.DoNotReprocess, RewardCustomerStatus.Completed);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                await RewardModuleHelper.LogErrorAsync(ex, "RewardCustomerAsync");
            }
            return rawResponse;
        }

        private static RewardCustomerResponse GetResponse(string msg, RewardCustomerReprocessFlag flag,
            RewardCustomerStatus status)
        {
            return new RewardCustomerResponse
            {
                ResponseMessage = msg,
                ReprocessFlag = flag,
                Status = status
            };
        }
    }
}