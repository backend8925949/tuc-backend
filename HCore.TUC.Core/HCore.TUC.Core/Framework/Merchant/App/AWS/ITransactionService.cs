﻿using HCore.Helper;
using HCore.Operations.Object;
using HCore.TUC.Core.Object.Merchant;

namespace HCore.TUC.Core.Framework.Merchant.App.AWS
{
	public interface ITransactionService
	{
        Task<OConfiguration> GetConfigurationDetailsAsync(string configCode, long accountId);

        Task<string> GetConfigurationAsync(string configCode, long accountId);

        Task<double> GetMerchantBalanceAsync(long merchantId, long sourceId);

        Task<List<OCoreTransaction.TransactionItem>> ComputeTransactionItemsAsync(OUpload.RewardUpload.Item dataItem, long refId,
            double rewardAmount, double userRewardAmount, double commissionRewardAmount, OConfiguration? config);

        Task<OCoreTransaction.Response> ProcessTransactionAsync(OCoreTransaction.Request request);

        Task<double> GetAppUserBalanceAsync(long userAccountId, long merchantId, long sourceId);
        Task<double> GetAppUserBalanceAsync(long userAccountId);
    }
}

