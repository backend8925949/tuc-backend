﻿using Amazon.SQS.Model;
using HCore.TUC.Core.Object.Merchant;

namespace HCore.TUC.Core.Framework.Merchant.App.AWS
{
    public interface ISQSService
    {
        Task<SendMessageResponse> SendMessageToSqsQueueAsync(object request);
        Task<(bool Success, string Message)> QueueMessageAsync(OUpload.RewardUpload.MQUploadRequest request);
    }
}

