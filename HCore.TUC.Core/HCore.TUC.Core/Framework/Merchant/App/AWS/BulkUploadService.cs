﻿using HCore.Helper;
using ExcelDataReader;
using HCore.Data;
using HCore.Data.Models;
using System.Data;
using Newtonsoft.Json;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.TUC.Core.Object.Merchant.OUpload.RewardUpload;

namespace HCore.TUC.Core.Framework.Merchant.App.AWS
{
    public class BulkUploadService : IBulkUploadService
    {
        private readonly ISQSService _sqsService;
        public BulkUploadService(ISQSService sqsService)
        {
            _sqsService = sqsService;
        }

        public async Task<OResponse> UploadBulkCustomerRewardsFileAsync(UploadRequest request)
        {
            OResponse response;
            #region Manage Exception
            try
            {
                if (request.AccountId < 0)
                {
                    response = HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1500, TUCCoreResource.CA1500M);
                }

                if (request.File != null)
                {
                    if (request.File.FileName.EndsWith(".xls") || request.File.FileName.EndsWith(".xlsx"))
                    {
                        using (HCoreContext _HCoreContext = new HCoreContext())
                        {
                            var merchantExists = _HCoreContext.HCUAccount.Any(x => x.Id == request.AccountId);

                            if (merchantExists)
                            {
                                if (request.UserReference == null)
                                {
                                    request.UserReference = new OUserReference
                                    {
                                        AccountId = request.AccountId,
                                        AccountKey = request.AccountKey,
                                        CountryIsd = request.CountryIsd
                                    };
                                }

                                DataSet dsexcelRecords = new();
                                IExcelDataReader? reader = null;
                                using var ms = new MemoryStream();
                                request.File.CopyTo(ms);
                                reader = request.File.FileName.EndsWith(".xls") ? ExcelReaderFactory.CreateBinaryReader(ms) : ExcelReaderFactory.CreateOpenXmlReader(ms);
                                dsexcelRecords = reader.AsDataSet(new ExcelDataSetConfiguration
                                {
                                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration
                                    {
                                        UseHeaderRow = true
                                    }
                                });
                                reader.Close();

                                if (dsexcelRecords != null && dsexcelRecords.Tables.Count > 0)
                                {
                                    var dTable = dsexcelRecords.Tables[0];

                                    var json = JsonConvert.SerializeObject(dTable);
                                    var customers = JsonConvert.DeserializeObject<List<CustomerData>>(json);

                                    var mqRequest = new MQUploadRequest
                                    {
                                        AccountId = request.AccountId,
                                        AccountKey = request.AccountKey,
                                        CountryIsd = request.CountryIsd,
                                        Customers = customers,
                                        FileName = request.File.FileName,
                                        MerchantId = request.MerchantId,
                                        MobileNumberLength = request.MobileNumberLength,
                                        UserReference = request.UserReference,
                                        Environment = HCoreConstant.HostEnvironment.ToString()
                                    };

                                    var result = await _sqsService.QueueMessageAsync(mqRequest);
                                    if (result.Success)
                                    {
                                        response = HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1502, TUCCoreResource.CA1502M);
                                    }
                                    else
                                    {
                                        response = HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1501, result.Message);
                                    }
                                }
                                else
                                {
                                    response = HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1501, "Excel document contains no data");
                                }
                            }
                            else
                            {
                                response = HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1500, "Invalid Account ID.");
                            }
                        }
                    }
                    else
                    {
                        response = HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1501, "File uploaded is not excel.");
                    }
                }
                else
                {
                    response = HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1501, "No file is uploaded.");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardCustomers", _Exception, request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
            return response;
        }
    }
}

