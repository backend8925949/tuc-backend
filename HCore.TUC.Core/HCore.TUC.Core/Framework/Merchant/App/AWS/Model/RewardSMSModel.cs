﻿using System;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Core.Framework.Merchant.App.AWS.Model
{
	public class RewardSMSModel
	{
        public SmsType SmsType { get; set; }
        public string? CountryISD { get; set; }
        public string? MobileNumber { get; set; }
        public string? Message { get; set; }
        public long AccountId { get; set; }
        public string? SystemReference { get; set; }
        public long TransactionId { get; set; }
        public string? SenderId { get; set; }
        public string? APIKey { get; set; }
        public string? BaseURL { get; set; }
    }
}

