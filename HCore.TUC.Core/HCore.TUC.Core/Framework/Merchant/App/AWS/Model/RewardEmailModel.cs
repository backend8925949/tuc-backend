﻿using System;
namespace HCore.TUC.Core.Framework.Merchant.App.AWS.Model
{
	public class RewardEmailModel
	{
		public string? CustomerName { get; set; }
		public string? CustomerAccountNumber { get; set; }
		public double RewardAmount { get; set; }
		public double CustomerBalance { get; set; }
		public string? CustomerEmailAddress { get; set; }
		public string? SenderEmailAddres { get; set; }
		public string? SenderName { get; set; }
		public string? SenderLogoLink { get; set; }
	}
}

