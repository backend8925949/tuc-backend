﻿using HCore.TUC.Core.Object.Merchant;
using HCore.Helper;

namespace HCore.TUC.Core.Framework.Merchant.App.AWS
{
	public interface IBulkUploadService
	{
        Task<OResponse> UploadBulkCustomerRewardsFileAsync(OUpload.RewardUpload.UploadRequest request);
    }
}

