//==================================================================================
// FileName: FrameworkAccounts.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to accounts
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Resource;
using Akka.Actor;
using System.Security.Cryptography.X509Certificates;
using ClosedXML.Excel;
using System.Reflection;
using System.IO;
using System.Net;

namespace HCore.TUC.Core.Framework.Merchant
{
    public class FrameworkAccounts
    {
        OAccounts.ContactPerson _ContactPerson;
        OAccounts.RelationshipManager _RelationshipManager;
        HCoreContext _HCoreContext;
        List<OAccounts.Cashier.List> _Cashiers;
        List<OAccounts.SubAccount.List> _SubAccounts;
        List<OAccounts.Store.List> _Stores;
        List<OAccounts.Terminal.List> _Terminals;
        List<OAccounts.Customer.List> _Customers;
        OAccounts.Merchant.Response _MerchantDetails;
        OAccounts.Store.Details _StoreDetails;
        OAccounts.Cashier.Details _CashierDetails;
        OAccounts.SubAccount.Details _SubAccountDetails;
        OAccounts.Terminal.Details _TerminalDetails;
        OAddress _OAddress;
        OAccounts.Customer.Overview _CustomerDetails;
        OAccounts.Terminal.Overview _TerminalOverview;
        OAccounts.Cashier.Overview _CashiersOverview;
        HCUAccountParameter _HCUAccountParameter;
        List<OAccounts.SubAccount.ListDownload> _ListDownload;
        /// <summary>
        /// Description: Gets the merchant details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchant(OAccounts.Merchant.Request _Request)
        {

            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey && x.Id == _Request.AccountId)
                        .Select(x => new
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            DisplayName = x.DisplayName,
                            ContactNumber = x.ContactNumber,
                            EmailAddress = x.EmailAddress,
                            IconUrl = x.IconStorage.Path,
                            AccountCode = x.AccountCode,
                            Name = x.Name,
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            SecondaryEmailAddress = x.SecondaryEmailAddress,
                            MobileNumber = x.MobileNumber,
                            Description = x.Description,


                            Address = x.Address,
                            Latitude = x.Latitude,
                            Longitude = x.Longitude,

                            CityAreaId = x.CityAreaId,
                            CityAreaCode = x.CityArea.Guid,
                            CityAreaName = x.CityArea.Name,

                            CityId = x.CityId,
                            CityCode = x.City.Guid,
                            CityName = x.City.Name,

                            StateId = x.StateId,
                            StateCode = x.State.Guid,
                            StateName = x.State.Name,

                            CountryId = x.CountryId,
                            CountryCode = x.Country.Guid,
                            CountryName = x.Country.Name,


                            OwnerName = x.FirstName,
                            ReferralCode = x.ReferralCode,
                            UserName = x.User.Username,
                            RewardPercentage = x.AccountPercentage,

                            CreateDate = x.CreateDate,
                            CreatedByReferenceId = x.CreatedById,
                            CreatedByReferenceKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyByReferenceId = x.ModifyById,
                            ModifyByReferenceKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                            WebsiteUrl = x.WebsiteUrl,

                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                            CpFirstName = x.CpFirstName,
                            CpLastName = x.CpLastName,
                            CpEmailAddress = x.CpEmailAddress,
                            CpContactNumber = x.CpContactNumber,
                            CpMobileNumber = x.CpMobileNumber
                        }).FirstOrDefault();
                    if (Details != null)
                    {
                        _MerchantDetails = new OAccounts.Merchant.Response();
                        _MerchantDetails.Categories = _HCoreContext.TUCCategoryAccount.Where(x => x.AccountId == Details.ReferenceId && x.StatusId == HelperStatus.Default.Active)
                            //.OrderBy(x => x.Category.Name)
                            .Select(x => new OAccounts.Category
                            {
                                ReferenceId = x.Category.Id,
                                ReferenceKey = x.Category.Guid,
                                Name = x.Category.Name,
                                IconUrl = _AppConfig.StorageUrl + x.Category.IconStorage.Path,
                            }).ToList();
                        _MerchantDetails.Stores = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.MerchantStore && x.OwnerId == Details.ReferenceId);
                        _MerchantDetails.Cashiers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.MerchantCashier && x.Owner.OwnerId == Details.ReferenceId);
                        _MerchantDetails.Terminals = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == Details.ReferenceId);

                        _MerchantDetails.ReferenceId = Details.ReferenceId;
                        _MerchantDetails.ReferenceKey = Details.ReferenceKey;
                        //  _MerchantDetails.RewardPercentage = Details.RewardPercentage;
                        _MerchantDetails.RewardPercentage = Convert.ToDouble(_HCoreContext.HCUAccountParameter.Where(y => y.AccountId == Details.ReferenceId
                                                              && y.Account.Guid == Details.ReferenceKey
                                                              && y.Common.SystemName == "rewardpercentage").OrderByDescending(x => x.CreateDate)
                                                         .Select(y => y.Value).FirstOrDefault());

                        //_MerchantDetails.OwnerName = string.IsNullOrEmpty(Details.OwnerName) ? Details.Name.Split(" ")[0] : Details.OwnerName;
                        _MerchantDetails.OwnerName = Details.OwnerName;

                        _MerchantDetails.AccountCode = Details.AccountCode;
                        _MerchantDetails.DisplayName = Details.DisplayName;
                        _MerchantDetails.Name = Details.Name;
                        _MerchantDetails.EmailAddress = Details.EmailAddress;
                        _MerchantDetails.ContactNumber = Details.ContactNumber;

                        _MerchantDetails.WebsiteUrl = Details.WebsiteUrl;
                        _MerchantDetails.ReferralCode = Details.ReferralCode;
                        _MerchantDetails.UserName = Details.UserName;
                        _MerchantDetails.Description = Details.Description;
                        string[] Names = Details.DisplayName.Split(" ");
                        if (Names.Length == 0)
                        {
                            _MerchantDetails.NickName = Details.DisplayName.Length >= 12 ? Details.DisplayName.Substring(0, 12) : Details.DisplayName;
                        }
                        else
                        {
                            _MerchantDetails.NickName = Names[0].Length >= 12 ? Names[0].Substring(0, 12) : Names[0];
                        }
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            _MerchantDetails.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            _MerchantDetails.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            _MerchantDetails.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            _MerchantDetails.IconUrl = _AppConfig.Default_Icon;
                        }
                        _ContactPerson = new OAccounts.ContactPerson
                        {
                            FirstName = Details.CpFirstName,
                            LastName = Details.CpLastName,
                            EmailAddress = Details.CpEmailAddress,
                            MobileNumber = Details.CpMobileNumber
                        };

                        _RelationshipManager = new OAccounts.RelationshipManager
                        {
                            Name = Details.FirstName + " " + Details.LastName,
                            EmailAddress = Details.SecondaryEmailAddress,
                            MobileNumber = Details.MobileNumber,
                        };

                        _MerchantDetails.ContactPerson = _ContactPerson;
                        _MerchantDetails.Rm = _RelationshipManager;
                        _MerchantDetails.CreateDate = Details.CreateDate;
                        _MerchantDetails.CreatedByReferenceId = Details.CreatedByReferenceId;
                        _MerchantDetails.CreatedByReferenceKey = Details.CreatedByReferenceKey;
                        _MerchantDetails.CreatedByDisplayName = Details.CreatedByDisplayName;
                        _MerchantDetails.ModifyDate = Details.ModifyDate;
                        _MerchantDetails.ModifyByReferenceId = Details.ModifyByReferenceId;
                        _MerchantDetails.ModifyByReferenceKey = Details.ModifyByReferenceKey;
                        _MerchantDetails.ModifyByDisplayName = Details.ModifyByDisplayName;
                        _MerchantDetails.StatusCode = Details.StatusCode;
                        _MerchantDetails.StatusName = Details.StatusName;

                        _OAddress = new OAddress();
                        _OAddress.Address = Details.Address;

                        _OAddress.CityAreaId = Details.CityAreaId;
                        _OAddress.CityAreaCode = Details.CityAreaCode;
                        _OAddress.CityAreaName = Details.CityAreaName;

                        _OAddress.CityId = Details.CityId;
                        _OAddress.CityCode = Details.CityCode;
                        _OAddress.CityName = Details.CityName;

                        _OAddress.StateId = Details.StateId;
                        _OAddress.StateCode = Details.StateCode;
                        _OAddress.StateName = Details.StateName;

                        _OAddress.CountryId = Details.CountryId;
                        _OAddress.CountryCode = Details.CountryCode;
                        _OAddress.CountryName = Details.CountryName;

                        _OAddress.Latitude = Details.Latitude;
                        _OAddress.Longitude = Details.Longitude;

                        _MerchantDetails.Address = _OAddress.Address;
                        _MerchantDetails.AddressComponent = _OAddress;

                        var ProgramDetails = _HCoreContext.TUCLProgram
                                                            .Where(x => x.AccountId == _MerchantDetails.ReferenceId
                                                                    && x.ProgramType.SystemName == "programtype.grouployalty"
                                                                    && x.StatusId == HelperStatus.Default.Active)
                                                            .Select(x => new
                                                            {
                                                                ProgramId = x.Id,
                                                                ProgramReferenceKey = x.Guid,
                                                                SystemName = x.ProgramType.SystemName,
                                                                TypeName = x.ProgramType.Name
                                                            }).FirstOrDefault();
                        var CloseOpenDetails = _HCoreContext.TUCLProgram
                                                               .Where(x => x.AccountId == _MerchantDetails.ReferenceId
                                                                       && (x.ProgramType.SystemName == "programtype.openloyalty"
                                                                       || x.ProgramType.SystemName == "programtype.closeloyalty")
                                                                       && x.StatusId == HelperStatus.Default.Active)
                                                               .Select(x => new
                                                               {
                                                                   ProgramId = x.Id,
                                                                   ProgramReferenceKey = x.Guid,
                                                                   SystemName = x.ProgramType.SystemName,
                                                                   TypeName = x.ProgramType.Name
                                                               }).FirstOrDefault();
                        var details = _HCoreContext.HCUAccountParameter
                                                    .Where(x => (x.AccountId == _MerchantDetails.ReferenceId)
                                                        && x.TypeId == HelperType.ThankUCashLoyalty
                                                        && (x.SubTypeId == HelperType.ThankUCashLoyalties.OpenLoyalty || x.SubTypeId == HelperType.ThankUCashLoyalties.ClosedLoyalty)
                                                        ).
                                                        Select(x => new
                                                        {
                                                            SubTypeId = x.SubTypeId
                                                        }).FirstOrDefault();
                        if (ProgramDetails != null && CloseOpenDetails != null)
                        {
                            _MerchantDetails.MerchantType = ProgramDetails.TypeName + " , " + CloseOpenDetails.TypeName;
                        }
                        else if (ProgramDetails == null && CloseOpenDetails != null)
                        {
                            _MerchantDetails.MerchantType = CloseOpenDetails.TypeName;
                        }
                        else if (ProgramDetails != null)
                        {
                            _MerchantDetails.MerchantType = ProgramDetails.TypeName;
                        }
                        else if (details != null)
                        {
                            bool IsOpenCloseLoyalty = (details.SubTypeId == HelperType.ThankUCashLoyalties.ClosedLoyalty) ? true : false;
                            if (IsOpenCloseLoyalty)
                            {
                                _MerchantDetails.MerchantType = "Close Loyalty Program";
                            }
                            else
                            {
                                _MerchantDetails.MerchantType = "Open Loyalty Program";
                            }
                        }
                        else
                        {
                            _MerchantDetails.MerchantType = "Open Loyalty Program";
                        }

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _MerchantDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetMerchant", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the cashier list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCashiers(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Cashiers = new List<OAccounts.Cashier.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier && (x.Owner.OwnerId == _Request.AccountId || x.OwnerId == _Request.AccountId))
                                                .Select(x => new OAccounts.Cashier.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CashierId = x.DisplayName,
                                                    EmployeeId = x.ReferralCode,
                                                    Name = x.Name,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    GenderCode = x.Gender.SystemName,
                                                    GenderName = x.Gender.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    StoreReferenceId = x.Owner.Id,
                                                    StoreReferenceKey = x.Owner.Guid,
                                                    StoreDisplayName = x.Owner.DisplayName,
                                                    StoreAddress = x.Address,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    LastActivityDate = x.LastActivityDate,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    //Transactions = x.TUCSaleCashier.Count,
                                                    Transactions = x.HCUAccountTransactionCashier.Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                                                                 && x.ParentId == _Request.AccountId
                                                                                                 && (x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit)
                                                                                                 && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                                                                 && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)).Count(),
                                                    //Transactions = x.TUCSaleCashier.Count(a=>a.StatusId == HelperStatus.Transaction.Success),
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Cashiers = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier && (x.Owner.OwnerId == _Request.AccountId || x.OwnerId == _Request.AccountId))
                                                .Select(x => new OAccounts.Cashier.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CashierId = x.DisplayName,
                                                    EmployeeId = x.ReferralCode,
                                                    Name = x.Name,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    GenderCode = x.Gender.SystemName,
                                                    GenderName = x.Gender.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    StoreReferenceId = x.Owner.Id,
                                                    StoreReferenceKey = x.Owner.Guid,
                                                    StoreDisplayName = x.Owner.DisplayName,
                                                    StoreAddress = x.Address,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    LastActivityDate = x.LastActivityDate,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    //Transactions = x.TUCSaleCashier.Count,
                                                    Transactions = x.HCUAccountTransactionCashier.Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                                                                 && x.ParentId == _Request.AccountId
                                                                                                 && (x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit)
                                                                                                 && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                                                                 && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)).Count(),
                                                    //Transactions = x.TUCSaleCashier.Count(a=>a.StatusId == HelperStatus.Transaction.Success),
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Cashiers)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Cashiers, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCashiers", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the cashiers overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCashiersOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _CashiersOverview = new OAccounts.Cashier.Overview();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _CashiersOverview.Total = _HCoreContext.HCUAccount
                                             .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier && (x.Owner.OwnerId == _Request.AccountId || x.OwnerId == _Request.AccountId))
                                                .Select(x => new OAccounts.Cashier.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CashierId = x.DisplayName,
                                                    EmployeeId = x.ReferralCode,
                                                    Name = x.Name,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    GenderCode = x.Gender.SystemName,
                                                    GenderName = x.Gender.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    StoreReferenceId = x.Owner.Id,
                                                    StoreReferenceKey = x.Owner.Guid,
                                                    StoreDisplayName = x.Owner.DisplayName,
                                                    StoreAddress = x.Address,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    LastActivityDate = x.LastActivityDate,
                                                })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    _CashiersOverview.Active = _HCoreContext.HCUAccount
                                            .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier && x.StatusId == HelperStatus.Default.Active && (x.Owner.OwnerId == _Request.AccountId || x.OwnerId == _Request.AccountId))
                                                .Select(x => new OAccounts.Cashier.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CashierId = x.DisplayName,
                                                    EmployeeId = x.ReferralCode,
                                                    Name = x.Name,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    GenderCode = x.Gender.SystemName,
                                                    GenderName = x.Gender.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    StoreReferenceId = x.Owner.Id,
                                                    StoreReferenceKey = x.Owner.Guid,
                                                    StoreDisplayName = x.Owner.DisplayName,
                                                    StoreAddress = x.Address,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    LastActivityDate = x.LastActivityDate,
                                                })
                                          .Where(_Request.SearchCondition)
                                  .Count();
                    _CashiersOverview.Blocked = _HCoreContext.HCUAccount
                                         .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier && x.StatusId != HelperStatus.Default.Active && x.Owner.OwnerId == _Request.AccountId)
                                         .Select(x => new OAccounts.Cashier.List
                                         {
                                             ReferenceId = x.Id,
                                             ReferenceKey = x.Guid,
                                             CashierId = x.DisplayName,
                                             EmployeeId = x.ReferralCode,
                                             Name = x.Name,
                                             EmailAddress = x.EmailAddress,
                                             ContactNumber = x.ContactNumber,
                                             GenderCode = x.Gender.SystemName,
                                             GenderName = x.Gender.Name,
                                             IconUrl = x.IconStorage.Path,
                                             StoreReferenceId = x.Owner.Id,
                                             StoreReferenceKey = x.Owner.Guid,
                                             StoreDisplayName = x.Owner.DisplayName,
                                             StoreAddress = x.Address,
                                             CreateDate = x.CreateDate,
                                             StatusCode = x.Status.SystemName,
                                             StatusName = x.Status.Name,
                                             LastActivityDate = x.LastActivityDate,
                                         })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CashiersOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetCashiersOverview", _Exception, _Request.UserReference, _CashiersOverview, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the cashier details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCashier(OAccounts.Cashier.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey && x.Id == _Request.AccountId && x.AccountTypeId == UserAccountType.MerchantCashier)
                        .Select(x => new
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            DisplayName = x.DisplayName,
                            Name = x.DisplayName,
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            MobileNumber = x.MobileNumber,
                            ContactNumber = x.ContactNumber,
                            EmailAddress = x.EmailAddress,
                            IconUrl = x.IconStorage.Path,
                            Address = x.Address,
                            Latitude = x.Latitude,
                            Longitude = x.Longitude,

                            CityAreaId = x.CityAreaId,
                            CityAreaCode = x.Guid,
                            CityAreaName = x.CityArea.Name,

                            CityId = x.CityId,
                            CityCode = x.Guid,
                            CityName = x.City.Name,

                            StateId = x.StateId,
                            StateCode = x.Guid,
                            StateName = x.State.Name,

                            CountryId = x.CountryId,
                            CountryCode = x.Guid,
                            CountryName = x.Country.Name,


                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                            EmployeeId = x.ReferralCode,
                            GenderCode = x.Gender.SystemName,
                            GenderName = x.Gender.Name,
                            StoreName = x.Owner.DisplayName,
                            StoreAddress = x.Owner.Address,
                            StoreId = x.Owner.Id,
                            StoreKey = x.Owner.Guid,

                            UserName = x.User.Username,
                            Password = x.User.Password,

                            CreateDate = x.CreateDate,
                            CreatedByReferenceId = x.CreatedById,
                            CreatedByReferenceKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyByReferenceId = x.ModifyById,
                            ModifyByReferenceKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,
                        }).FirstOrDefault();
                    if (Details != null)
                    {
                        _CashierDetails = new OAccounts.Cashier.Details();
                        _CashierDetails.ReferenceId = Details.ReferenceId;
                        _CashierDetails.ReferenceKey = Details.ReferenceKey;
                        _CashierDetails.CashierId = Details.DisplayName;
                        _CashierDetails.Name = Details.Name;
                        _CashierDetails.FirstName = Details.FirstName;
                        _CashierDetails.LastName = Details.LastName;
                        _CashierDetails.EmailAddress = Details.EmailAddress;
                        _CashierDetails.ContactNumber = Details.ContactNumber;
                        _CashierDetails.UserName = Details.UserName;
                        _CashierDetails.Password = HCoreEncrypt.DecryptHash(Details.Password);
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            _CashierDetails.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            _CashierDetails.IconUrl = _AppConfig.Default_Icon;
                        }
                        _CashierDetails.GenderCode = Details.GenderCode;
                        _CashierDetails.GenderName = Details.GenderName;
                        _CashierDetails.EmployeeId = Details.EmployeeId;

                        _CashierDetails.StoreReferenceId = Details.StoreId;
                        _CashierDetails.StoreReferenceKey = Details.StoreKey;
                        _CashierDetails.StoreDisplayName = Details.StoreName;
                        _CashierDetails.StoreAddress = Details.StoreAddress;

                        _CashierDetails.StatusCode = Details.StatusCode;
                        _CashierDetails.StatusName = Details.StatusName;

                        _CashierDetails.CreateDate = Details.CreateDate;
                        _CashierDetails.CreatedByReferenceId = Details.CreatedByReferenceId;
                        _CashierDetails.CreatedByReferenceKey = Details.CreatedByReferenceKey;
                        _CashierDetails.CreatedByDisplayName = Details.CreatedByDisplayName;

                        _CashierDetails.ModifyDate = Details.ModifyDate;
                        _CashierDetails.ModifyByReferenceId = Details.ModifyByReferenceId;
                        _CashierDetails.ModifyByReferenceKey = Details.ModifyByReferenceKey;
                        _CashierDetails.ModifyByDisplayName = Details.ModifyByDisplayName;

                        _OAddress = new OAddress();
                        _OAddress.Address = Details.Address;

                        _OAddress.CityAreaId = Details.CityAreaId;
                        _OAddress.CityAreaCode = Details.CityAreaCode;
                        _OAddress.CityAreaName = Details.CityAreaName;

                        _OAddress.CityId = Details.CityId;
                        _OAddress.CityCode = Details.CityCode;
                        _OAddress.CityName = Details.CityName;

                        _OAddress.StateId = Details.StateId;
                        _OAddress.StateCode = Details.StateCode;
                        _OAddress.StateName = Details.StateName;

                        _OAddress.CountryId = Details.CountryId;
                        _OAddress.CountryCode = Details.CountryCode;
                        _OAddress.CountryName = Details.CountryName;

                        _OAddress.Latitude = Details.Latitude;
                        _OAddress.Longitude = Details.Longitude;

                        _CashierDetails.Address = _OAddress.Address;
                        _CashierDetails.AddressComponent = _OAddress;
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CashierDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCashier", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the store list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStores(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Stores = new List<OAccounts.Store.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore && (x.OwnerId == _Request.AccountId || x.Owner.OwnerId == _Request.AccountId))
                                                .Select(x => new OAccounts.Store.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    ManagerName = x.SubOwner.FirstName + " " + x.SubOwner.LastName,
                                                    ManagerMobileNumber = x.SubOwner.ContactNumber,
                                                    Address = x.Address,
                                                    Latitude = x.Latitude,
                                                    Longitude = x.Longitude,

                                                    CityAreaId = x.CityAreaId,
                                                    CityAreaCode = x.Guid,
                                                    CityAreaName = x.CityArea.Name,

                                                    CityId = x.CityId,
                                                    CityCode = x.Guid,
                                                    CityName = x.City.Name,

                                                    StateId = x.StateId,
                                                    StateCode = x.Guid,
                                                    StateName = x.State.Name,

                                                    CountryId = x.CountryId,
                                                    CountryCode = x.Guid,
                                                    CountryName = x.Country.Name,
                                                    Cashiers = _HCoreContext.HCUAccount.Count(a => a.AccountTypeId == UserAccountType.MerchantCashier && a.OwnerId == x.Id),
                                                    Terminals = _HCoreContext.TUCTerminal.Count(a => a.StoreId == x.Id),
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Stores = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore && (x.OwnerId == _Request.AccountId || x.Owner.OwnerId == _Request.AccountId))
                                                .Select(x => new OAccounts.Store.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    ManagerName = x.SubOwner.FirstName + " " + x.SubOwner.LastName,
                                                    ManagerMobileNumber = x.SubOwner.ContactNumber,
                                                    Address = x.Address,
                                                    Latitude = x.Latitude,
                                                    Longitude = x.Longitude,

                                                    CityAreaId = x.CityAreaId,
                                                    CityAreaCode = x.Guid,
                                                    CityAreaName = x.CityArea.Name,

                                                    CityId = x.CityId,
                                                    CityCode = x.Guid,
                                                    CityName = x.City.Name,

                                                    StateId = x.StateId,
                                                    StateCode = x.Guid,
                                                    StateName = x.State.Name,

                                                    CountryId = x.CountryId,
                                                    CountryCode = x.Guid,
                                                    CountryName = x.Country.Name,
                                                    Cashiers = _HCoreContext.HCUAccount.Count(a => a.AccountTypeId == UserAccountType.MerchantCashier && a.OwnerId == x.Id),
                                                    Terminals = _HCoreContext.TUCTerminal.Count(a => a.StoreId == x.Id),
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    CpFirstName = x.CpFirstName,
                                                    CpLastName = x.CpLastName,
                                                    CpMobileNumber = x.CpMobileNumber,
                                                    CpEmailAddress = x.CpEmailAddress,
                                                    MobileNumber = x.MobileNumber
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    //foreach (var DataItem in _Stores)
                    //{
                    //    // As per Suraj Sir's Change that Store Manager should be selected from store manager drop down this is not need
                    //    // This condition was for when we create merchant through new onboarding process default store is created.
                    //    if ((DataItem.DisplayName.Trim() == DataItem.ManagerName.Trim()) || (DataItem.ManagerName.Trim().Length == 0))
                    //    {
                    //        DataItem.ManagerName = DataItem.CpFirstName + " " + DataItem.CpLastName;
                    //        DataItem.ManagerMobileNumber = DataItem.CpMobileNumber;
                    //        DataItem.ContactNumber = DataItem.MobileNumber;
                    //    }
                    //}
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Stores, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCashiers", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the store details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStore(OAccounts.Store.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey && x.Id == _Request.AccountId && x.AccountTypeId == UserAccountType.MerchantStore)
                        .Select(x => new
                        {
                            MerchantId = x.OwnerId,
                            MerchantKey = x.Owner.Guid,
                            MerchantName = x.Owner.DisplayName,
                            MerchantStatusId = x.Owner.StatusId,
                            MerchantStatusCode = x.Owner.Status.SystemName,
                            RewardPercentage = x.Owner.AccountPercentage,
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            DisplayName = x.DisplayName,
                            MobileNumber = x.MobileNumber,
                            ContactNumber = x.ContactNumber,
                            EmailAddress = x.EmailAddress,
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            SecondaryEmailAddress = x.SecondaryEmailAddress,
                            IconUrl = x.Owner.IconStorage.Path,
                            AccountCode = x.AccountCode,
                            Address = x.Address,
                            Latitude = x.Latitude,
                            Longitude = x.Longitude,

                            CityAreaId = x.CityAreaId,
                            CityAreaCode = x.CityArea.Guid,
                            CityAreaName = x.CityArea.Name,

                            CityId = x.CityId,
                            CityCode = x.City.Guid,
                            CityName = x.City.Name,

                            StateId = x.StateId,
                            StateCode = x.State.Guid,
                            StateName = x.State.Name,

                            CountryId = x.CountryId,
                            CountryCode = x.Country.Guid,
                            CountryName = x.Country.Name,

                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,

                            CreateDate = x.CreateDate,
                            CreatedByReferenceId = x.CreatedById,
                            CreatedByReferenceKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyByReferenceId = x.ModifyById,
                            ModifyByReferenceKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,
                            UserName = x.User.Username,
                            Password = x.User.Password,
                            Name = x.Name,
                            //As per suraj sir change now Store Manager's drop down is added so details will come accordingly
                            CpFirstName = x.SubOwner.FirstName,
                            CpLastName = x.SubOwner.LastName,
                            CpEmailAddress = x.SubOwner.EmailAddress,
                            CpMobileNumber = x.SubOwner.ContactNumber,
                            SubOwnerId = x.SubOwnerId
                        }).FirstOrDefault();
                    if (Details != null)
                    {
                        _StoreDetails = new OAccounts.Store.Details();
                        _StoreDetails.Cashiers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.MerchantCashier && x.OwnerId == Details.ReferenceId);
                        _StoreDetails.Terminals = _HCoreContext.TUCTerminal.Count(x => x.StoreId == Details.ReferenceId);
                        _StoreDetails.ReferenceId = Details.ReferenceId;
                        _StoreDetails.ReferenceKey = Details.ReferenceKey;
                        _StoreDetails.OwnerId = Details.MerchantId;
                        _StoreDetails.OwnerKey = Details.MerchantKey;
                        _StoreDetails.OwnerName = Details.MerchantName;
                        _StoreDetails.OwnerStatusId = Details.MerchantStatusId;
                        _StoreDetails.OwnerStatusCode = Details.MerchantStatusCode;
                        // _StoreDetails.RewardPercentage = Details.RewardPercentage;
                        _StoreDetails.RewardPercentage = Convert.ToDouble(_HCoreContext.HCUAccountParameter.Where(y => y.AccountId == Details.MerchantId
                                                              && y.Account.Guid == Details.MerchantKey
                                                              && y.Common.SystemName == "rewardpercentage").OrderByDescending(x => x.CreateDate)
                                                         .Select(y => y.Value).FirstOrDefault());
                        _StoreDetails.Name = Details.Name;
                        _StoreDetails.DisplayName = Details.DisplayName;
                        _StoreDetails.EmailAddress = Details.EmailAddress;
                        _StoreDetails.ContactNumber = string.IsNullOrEmpty(Details.ContactNumber) ? Details.MobileNumber : Details.ContactNumber;
                        _StoreDetails.Username = Details.UserName;
                        _StoreDetails.Password = Details.Password;
                        _StoreDetails.StoreManagerId = Details.SubOwnerId;
                        _ContactPerson = new OAccounts.ContactPerson
                        {
                            FirstName = Details.CpFirstName,
                            LastName = Details.CpLastName,
                            EmailAddress = Details.CpEmailAddress,
                            MobileNumber = Details.CpMobileNumber,
                        };
                        _StoreDetails.ContactPerson = _ContactPerson;
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            _StoreDetails.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            _StoreDetails.IconUrl = _AppConfig.Default_Icon;
                        }
                        _StoreDetails.CreateDate = Details.CreateDate;
                        _StoreDetails.CreatedByReferenceId = Details.CreatedByReferenceId;
                        _StoreDetails.CreatedByReferenceKey = Details.CreatedByReferenceKey;
                        _StoreDetails.CreatedByDisplayName = Details.CreatedByDisplayName;
                        _StoreDetails.ModifyDate = Details.ModifyDate;
                        _StoreDetails.ModifyByReferenceId = Details.ModifyByReferenceId;
                        _StoreDetails.ModifyByReferenceKey = Details.ModifyByReferenceKey;
                        _StoreDetails.ModifyByDisplayName = Details.ModifyByDisplayName;
                        _StoreDetails.StatusCode = Details.StatusCode;
                        _StoreDetails.StatusName = Details.StatusName;
                        _OAddress = new OAddress();

                        _OAddress.Address = Details.Address;

                        _OAddress.CityAreaId = Details.CityAreaId;
                        _OAddress.CityAreaCode = Details.CityAreaCode;
                        _OAddress.CityAreaName = Details.CityAreaName;

                        _OAddress.CityId = Details.CityId;
                        _OAddress.CityCode = Details.CityCode;
                        _OAddress.CityName = Details.CityName;

                        _OAddress.StateId = Details.StateId;
                        _OAddress.StateCode = Details.StateCode;
                        _OAddress.StateName = Details.StateName;

                        _OAddress.CountryId = Details.CountryId;
                        _OAddress.CountryCode = Details.CountryCode;
                        _OAddress.CountryName = Details.CountryName;

                        _OAddress.Latitude = Details.Latitude;
                        _OAddress.Longitude = Details.Longitude;

                        _StoreDetails.Address = _OAddress.Address;
                        _StoreDetails.AddressComponent = _OAddress;
                        _StoreDetails.Categories = _HCoreContext.TUCCategoryAccount.Where(x => x.AccountId == Details.ReferenceId && x.StatusId == HelperStatus.Default.Active)
                          .OrderBy(x => x.Category.Name)
                          .Select(x => new OAccounts.Category
                          {
                              ReferenceId = x.Category.Id,
                              ReferenceKey = x.Category.Guid,
                              Name = x.Category.Name,
                              IconUrl = _AppConfig.StorageUrl + x.Category.IconStorage.Path,
                          }).ToList();
                        if (_StoreDetails.Categories.Count == 0)
                        {
                            _StoreDetails.Categories = _HCoreContext.TUCCategoryAccount.Where(x => x.AccountId == Details.MerchantId && x.StatusId == HelperStatus.Default.Active)
                             .OrderBy(x => x.Category.Name)
                             .Select(x => new OAccounts.Category
                             {
                                 ReferenceId = x.Category.Id,
                                 ReferenceKey = x.Category.Guid,
                                 Name = x.Category.Name,
                                 IconUrl = _AppConfig.StorageUrl + x.Category.IconStorage.Path,
                             }).ToList();
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _StoreDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetStore", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the terminal list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTerminals(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
                DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
                DateTime T7DayStart = TDayStart.AddDays(-7);
                DateTime T7DayEnd = TDayStart.AddSeconds(1);
                DateTime TDeadDayEnd = T7DayStart;
                _Terminals = new List<OAccounts.Terminal.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.Type == "active")
                    {
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                    .Where(x => x.MerchantId == _Request.AccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,

                                                        ProviderReferenceId = x.ProviderId,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,
                                                        ProviderIconUrl = x.Provider.IconStorage.Path,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,
                                                        AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,

                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,


                                                        CreateDate = x.CreateDate,
                                                        LastActivityDate = x.LastActivityDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                        }
                        #region Get Data
                        _Terminals = _HCoreContext.TUCTerminal
                                                    .Where(x => x.MerchantId == _Request.AccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,

                                                        ProviderReferenceId = x.ProviderId,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,
                                                        ProviderIconUrl = x.Provider.IconStorage.Path,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,
                                                        AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,
                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,
                                                        CreateDate = x.CreateDate,
                                                        LastActivityDate = x.LastActivityDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                    }
                    else if (_Request.Type == "idle")
                    {
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                    .Where(x => x.MerchantId == _Request.AccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,

                                                        ProviderReferenceId = x.Provider.Id,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,
                                                        ProviderIconUrl = x.Provider.IconStorage.Path,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,
                                                        AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,
                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,
                                                        CreateDate = x.CreateDate,
                                                        LastActivityDate = x.LastActivityDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                        }
                        #region Get Data
                        _Terminals = _HCoreContext.TUCTerminal
                                                    .Where(x => x.MerchantId == _Request.AccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,

                                                        ProviderReferenceId = x.Provider.Id,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,
                                                        ProviderIconUrl = x.Provider.IconStorage.Path,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,
                                                        AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,
                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,
                                                        CreateDate = x.CreateDate,
                                                        LastActivityDate = x.LastActivityDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                    }
                    else if (_Request.Type == "dead")
                    {
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                    .Where(x => x.MerchantId == _Request.AccountId && x.LastTransactionDate < TDeadDayEnd)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,

                                                        ProviderReferenceId = x.Provider.Id,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,
                                                        ProviderIconUrl = x.Provider.IconStorage.Path,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,
                                                        AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,
                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,
                                                        CreateDate = x.CreateDate,
                                                        LastActivityDate = x.LastActivityDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                        }
                        #region Get Data
                        _Terminals = _HCoreContext.TUCTerminal
                                                    .Where(x => x.MerchantId == _Request.AccountId && x.LastTransactionDate < TDeadDayEnd)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,

                                                        ProviderReferenceId = x.Provider.Id,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,
                                                        ProviderIconUrl = x.Provider.IconStorage.Path,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,
                                                        AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,
                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,
                                                        CreateDate = x.CreateDate,
                                                        LastActivityDate = x.LastActivityDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                    }
                    else if (_Request.Type == "unused")
                    {
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                    .Where(x => x.MerchantId == _Request.AccountId && x.LastTransactionDate == null)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,

                                                        ProviderReferenceId = x.Provider.Id,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,
                                                        ProviderIconUrl = x.Provider.IconStorage.Path,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,
                                                        AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,
                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,
                                                        CreateDate = x.CreateDate,
                                                        LastActivityDate = x.LastActivityDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                        }
                        #region Get Data
                        _Terminals = _HCoreContext.TUCTerminal
                                                    .Where(x => x.MerchantId == _Request.AccountId && x.LastTransactionDate == null)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,

                                                        ProviderReferenceId = x.Provider.Id,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,
                                                        ProviderIconUrl = x.Provider.IconStorage.Path,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,
                                                        AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,
                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,
                                                        CreateDate = x.CreateDate,
                                                        LastActivityDate = x.LastActivityDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                    .Where(x => x.MerchantId == _Request.AccountId)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,

                                                        ProviderReferenceId = x.Provider.Id,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,
                                                        ProviderIconUrl = x.Provider.IconStorage.Path,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,
                                                        AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,
                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,
                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,
                                                        CreateDate = x.CreateDate,
                                                        LastActivityDate = x.LastActivityDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        _Terminals = _HCoreContext.TUCTerminal
                                                    .Where(x => x.MerchantId == _Request.AccountId)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,

                                                        ProviderReferenceId = x.Provider.Id,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,
                                                        ProviderIconUrl = x.Provider.IconStorage.Path,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,
                                                        AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,
                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,
                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,
                                                        CreateDate = x.CreateDate,
                                                        LastActivityDate = x.LastActivityDate,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                    }

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Terminals)
                    {
                        DataItem.LastActivityDate = DataItem.LastTransactionDate;
                        if (DataItem.LastTransactionDate > TDayStart && DataItem.LastTransactionDate < TDayEnd)
                        {
                            DataItem.ActivityStatusCode = "active";
                            DataItem.ActivityStatusName = "active";
                        }
                        else if (DataItem.LastTransactionDate > T7DayStart && DataItem.LastTransactionDate < T7DayEnd)
                        {
                            DataItem.ActivityStatusCode = "idle";
                            DataItem.ActivityStatusName = "idle";
                        }
                        else if (DataItem.LastTransactionDate < TDeadDayEnd)
                        {
                            DataItem.ActivityStatusCode = "dead";
                            DataItem.ActivityStatusName = "dead";
                        }
                        else if (DataItem.LastTransactionDate == null)
                        {
                            DataItem.ActivityStatusCode = "unused";
                            DataItem.ActivityStatusName = "unused";
                        }



                        if (!string.IsNullOrEmpty(DataItem.ProviderIconUrl))
                        {
                            DataItem.ProviderIconUrl = _AppConfig.StorageUrl + DataItem.ProviderIconUrl;
                        }
                        else
                        {
                            DataItem.ProviderIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.AcquirerIconUrl))
                        {
                            DataItem.AcquirerIconUrl = _AppConfig.StorageUrl + DataItem.AcquirerIconUrl;
                        }
                        else
                        {
                            DataItem.AcquirerIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Terminals, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetTerminals", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the terminals overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTerminalsOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
                DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
                DateTime T7DayStart = TDayStart.AddDays(-7);
                DateTime T7DayEnd = TDayStart.AddSeconds(1);
                DateTime TDeadDayEnd = T7DayStart;
                _TerminalOverview = new OAccounts.Terminal.Overview();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Terminals Overview
                    //_TerminalOverview.Total = _HCoreContext.TUCTerminal.Count(x =>x.SubOwner.OwnerId == _Request.UserAccountId);
                    //_TerminalOverview.Unused = _HCoreContext.TUCTerminal.Count(x =>x.SubOwner.OwnerId == _Request.UserAccountId && x.LastTransactionDate == null);
                    //_TerminalOverview.Active = _HCoreContext.TUCTerminal.Count(x =>x.SubOwner.OwnerId == _Request.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
                    //_TerminalOverview.Idle = _HCoreContext.TUCTerminal.Count(x =>x.SubOwner.OwnerId == _Request.UserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
                    //_TerminalOverview.Dead = _HCoreContext.TUCTerminal.Count(x =>x.SubOwner.OwnerId == _Request.UserAccountId && x.LastTransactionDate < TDeadDayEnd);
                    #endregion
                    #region Total Records
                    _TerminalOverview.Total = _HCoreContext.TUCTerminal
                                            .Where(x => x.MerchantId == _Request.AccountId
                                            && x.Merchant.Guid == _Request.AccountKey)
                                            //.Where(x => x.Account.AccountTypeId == UserAccountType.Terminal
                                            //&& (x.Account.Owner.OwnerId == _Request.AccountId || x.Account.OwnerId == _Request.AccountId))
                                            .Select(x => new OAccounts.Terminal.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                //TypeCode = "POS",
                                                //TypeName = "POS",
                                                TerminalId = x.DisplayName,
                                                IdentificationNumber = x.IdentificationNumber,
                                                SerialNumber = x.SerialNumber,

                                                ProviderReferenceId = x.Provider.Id,
                                                ProviderReferenceKey = x.Provider.Guid,
                                                ProviderDisplayName = x.Provider.DisplayName,
                                                ProviderIconUrl = x.Provider.IconStorage.Path,

                                                AcquirerReferenceId = x.Acquirer.Id,
                                                AcquirerReferenceKey = x.Acquirer.Guid,
                                                AcquirerDisplayName = x.Acquirer.DisplayName,
                                                AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                StoreReferenceId = x.Store.Id,
                                                StoreReferenceKey = x.Store.Guid,
                                                StoreDisplayName = x.Store.DisplayName,
                                                StoreAddress = x.Store.Address,
                                                MerchantReferenceId = x.MerchantId,
                                                MerchantReferenceKey = x.Merchant.Guid,
                                                MerchantDisplayName = x.Merchant.DisplayName,
                                                CreateDate = x.CreateDate,
                                                LastActivityDate = x.LastActivityDate,
                                                LastTransactionDate = x.LastTransactionDate,

                                                ActivityStatusCode = x.ActivityStatus.SystemName,
                                                ActivityStatusName = x.ActivityStatus.Name,

                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,

                                                TypeId = x.TypeId,
                                                TypeCode = x.Type.SystemName,
                                                TypeName = x.Type.Name,
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    #region Unused Records
                    _TerminalOverview.Unused = _HCoreContext.TUCTerminal
                                            .Where(x => x.MerchantId == _Request.AccountId
                                            && x.LastTransactionDate == null)
                                            //.Where(x => x.Account.AccountTypeId == UserAccountType.Terminal
                                            //&& (x.Account.Owner.OwnerId == _Request.AccountId || x.Account.OwnerId == _Request.AccountId))
                                            .Select(x => new OAccounts.Terminal.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                //TypeCode = "POS",
                                                //TypeName = "POS",
                                                TerminalId = x.DisplayName,
                                                IdentificationNumber = x.IdentificationNumber,
                                                SerialNumber = x.SerialNumber,

                                                ProviderReferenceId = x.Provider.Id,
                                                ProviderReferenceKey = x.Provider.Guid,
                                                ProviderDisplayName = x.Provider.DisplayName,
                                                ProviderIconUrl = x.Provider.IconStorage.Path,

                                                AcquirerReferenceId = x.Acquirer.Id,
                                                AcquirerReferenceKey = x.Acquirer.Guid,
                                                AcquirerDisplayName = x.Acquirer.DisplayName,
                                                AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                StoreReferenceId = x.Store.Id,
                                                StoreReferenceKey = x.Store.Guid,
                                                StoreDisplayName = x.Store.DisplayName,
                                                StoreAddress = x.Store.Address,
                                                MerchantReferenceId = x.MerchantId,
                                                MerchantReferenceKey = x.Merchant.Guid,
                                                MerchantDisplayName = x.Merchant.DisplayName,
                                                CreateDate = x.CreateDate,
                                                LastActivityDate = x.LastActivityDate,
                                                LastTransactionDate = x.LastTransactionDate,

                                                ActivityStatusCode = x.ActivityStatus.SystemName,
                                                ActivityStatusName = x.ActivityStatus.Name,

                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,

                                                TypeId = x.TypeId,
                                                TypeCode = x.Type.SystemName,
                                                TypeName = x.Type.Name,
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    #region Active Records
                    _TerminalOverview.Active = _HCoreContext.TUCTerminal
                                            .Where(x => x.MerchantId == _Request.AccountId
                                            && x.LastTransactionDate > TDayStart
                                            && x.LastTransactionDate < TDayEnd)
                                            //.Where(x => x.Account.AccountTypeId == UserAccountType.Terminal
                                            //&& (x.Account.Owner.OwnerId == _Request.AccountId || x.Account.OwnerId == _Request.AccountId))
                                            .Select(x => new OAccounts.Terminal.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                //TypeCode = "POS",
                                                //TypeName = "POS",
                                                TerminalId = x.DisplayName,
                                                IdentificationNumber = x.IdentificationNumber,
                                                SerialNumber = x.SerialNumber,

                                                ProviderReferenceId = x.Provider.Id,
                                                ProviderReferenceKey = x.Provider.Guid,
                                                ProviderDisplayName = x.Provider.DisplayName,
                                                ProviderIconUrl = x.Provider.IconStorage.Path,

                                                AcquirerReferenceId = x.Acquirer.Id,
                                                AcquirerReferenceKey = x.Acquirer.Guid,
                                                AcquirerDisplayName = x.Acquirer.DisplayName,
                                                AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                StoreReferenceId = x.Store.Id,
                                                StoreReferenceKey = x.Store.Guid,
                                                StoreDisplayName = x.Store.DisplayName,
                                                StoreAddress = x.Store.Address,
                                                MerchantReferenceId = x.MerchantId,
                                                MerchantReferenceKey = x.Merchant.Guid,
                                                MerchantDisplayName = x.Merchant.DisplayName,
                                                CreateDate = x.CreateDate,
                                                LastActivityDate = x.LastActivityDate,
                                                LastTransactionDate = x.LastTransactionDate,

                                                ActivityStatusCode = x.ActivityStatus.SystemName,
                                                ActivityStatusName = x.ActivityStatus.Name,

                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,

                                                TypeId = x.TypeId,
                                                TypeCode = x.Type.SystemName,
                                                TypeName = x.Type.Name,
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    #region Idle Records
                    _TerminalOverview.Idle = _HCoreContext.TUCTerminal
                                            .Where(x => x.MerchantId == _Request.AccountId
                                            && x.LastTransactionDate > T7DayStart
                                            && x.LastTransactionDate < T7DayEnd)
                                            //.Where(x => x.Account.AccountTypeId == UserAccountType.Terminal
                                            //&& (x.Account.Owner.OwnerId == _Request.AccountId || x.Account.OwnerId == _Request.AccountId))
                                            .Select(x => new OAccounts.Terminal.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                //TypeCode = "POS",
                                                //TypeName = "POS",
                                                TerminalId = x.DisplayName,
                                                IdentificationNumber = x.IdentificationNumber,
                                                SerialNumber = x.SerialNumber,

                                                ProviderReferenceId = x.Provider.Id,
                                                ProviderReferenceKey = x.Provider.Guid,
                                                ProviderDisplayName = x.Provider.DisplayName,
                                                ProviderIconUrl = x.Provider.IconStorage.Path,

                                                AcquirerReferenceId = x.Acquirer.Id,
                                                AcquirerReferenceKey = x.Acquirer.Guid,
                                                AcquirerDisplayName = x.Acquirer.DisplayName,
                                                AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                StoreReferenceId = x.Store.Id,
                                                StoreReferenceKey = x.Store.Guid,
                                                StoreDisplayName = x.Store.DisplayName,
                                                StoreAddress = x.Store.Address,
                                                MerchantReferenceId = x.MerchantId,
                                                MerchantReferenceKey = x.Merchant.Guid,
                                                MerchantDisplayName = x.Merchant.DisplayName,
                                                CreateDate = x.CreateDate,
                                                LastActivityDate = x.LastActivityDate,
                                                LastTransactionDate = x.LastTransactionDate,

                                                ActivityStatusCode = x.ActivityStatus.SystemName,
                                                ActivityStatusName = x.ActivityStatus.Name,

                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,

                                                TypeId = x.TypeId,
                                                TypeCode = x.Type.SystemName,
                                                TypeName = x.Type.Name,
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    #region Dead Records
                    _TerminalOverview.Dead = _HCoreContext.TUCTerminal
                                            .Where(x => x.MerchantId == _Request.AccountId
                                            && x.LastTransactionDate < TDeadDayEnd)
                                            //.Where(x => x.Account.AccountTypeId == UserAccountType.Terminal
                                            //&& (x.Account.Owner.OwnerId == _Request.AccountId || x.Account.OwnerId == _Request.AccountId))
                                            .Select(x => new OAccounts.Terminal.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                //TypeCode = "POS",
                                                //TypeName = "POS",
                                                TerminalId = x.DisplayName,
                                                IdentificationNumber = x.IdentificationNumber,
                                                SerialNumber = x.SerialNumber,

                                                ProviderReferenceId = x.Provider.Id,
                                                ProviderReferenceKey = x.Provider.Guid,
                                                ProviderDisplayName = x.Provider.DisplayName,
                                                ProviderIconUrl = x.Provider.IconStorage.Path,

                                                AcquirerReferenceId = x.Acquirer.Id,
                                                AcquirerReferenceKey = x.Acquirer.Guid,
                                                AcquirerDisplayName = x.Acquirer.DisplayName,
                                                AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                StoreReferenceId = x.Store.Id,
                                                StoreReferenceKey = x.Store.Guid,
                                                StoreDisplayName = x.Store.DisplayName,
                                                StoreAddress = x.Store.Address,
                                                MerchantReferenceId = x.MerchantId,
                                                MerchantReferenceKey = x.Merchant.Guid,
                                                MerchantDisplayName = x.Merchant.DisplayName,
                                                CreateDate = x.CreateDate,
                                                LastActivityDate = x.LastActivityDate,
                                                LastTransactionDate = x.LastTransactionDate,

                                                ActivityStatusCode = x.ActivityStatus.SystemName,
                                                ActivityStatusName = x.ActivityStatus.Name,

                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,

                                                TypeId = x.TypeId,
                                                TypeCode = x.Type.SystemName,
                                                TypeName = x.Type.Name,
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _TerminalOverview, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetTerminalsOverview", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        //internal OResponse GetTerminals(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
        //        DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
        //        DateTime T7DayStart = TDayStart.AddDays(-7);
        //        DateTime T7DayEnd = TDayStart.AddSeconds(1);
        //        DateTime TDeadDayEnd = T7DayStart;
        //        _Terminals = new List<OAccounts.Terminal.List>();
        //        if (string.IsNullOrEmpty(_Request.SearchCondition))
        //        {
        //            HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
        //        }
        //        if (string.IsNullOrEmpty(_Request.SortExpression))
        //        {
        //            HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
        //        }
        //        if (_Request.Limit < 1)
        //        {
        //            _Request.Limit = _AppConfig.DefaultRecordsLimit;
        //        }
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            if (_Request.Type == "active")
        //            {
        //                if (_Request.RefreshCount)
        //                {
        //                    _Request.TotalRecords = _HCoreContext.TUCTerminal
        //                                            .Where(x =>  x.MerchantId == _Request.AccountId && _HCoreContext.HCUAccountTransaction.Any(a => a.TerminalId == x.Id && a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd))
        //                                            .Select(x => new OAccounts.Terminal.List
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TypeCode = "POS",
        //                                                TypeName = "POS",
        //                                                TerminalId = x.DisplayName,
        //                                                SerialNumber = x.SerialNumber,

        //                                                ProviderReferenceId = x.ProviderId,
        //                                                ProviderReferenceKey = x.Provider.Guid,
        //                                                ProviderDisplayName = x.Provider.DisplayName,
        //                                                ProviderIconUrl = x.Provider.IconStorage.Path,

        //                                                AcquierReferenceId = x.Acquirer.Id,
        //                                                AcquirerReferenceKey = x.Acquirer.Guid,
        //                                                AcquirerDisplayName = x.Acquirer.DisplayName,
        //                                                AcquirerIconUrl = x.Acquirer.IconStorage.Path,

        //                                                StoreReferenceId = x.Store.Id,
        //                                                StoreReferenceKey = x.Store.Guid,
        //                                                StoreDisplayName = x.Store.DisplayName,
        //                                                StoreAddress = x.Store.Address,

        //                                                MerchantReferenceId = x.MerchantId,
        //                                                MerchantReferenceKey = x.Merchant.Guid,
        //                                                MerchantDisplayName = x.Merchant.DisplayName,


        //                                                CreateDate = x.CreateDate,
        //                                                LastActivityDate = x.LastActivityDate,
        //                                                LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(a=>a.TerminalId == x.Id).OrderByDescending(a=>a.TransactionDate).Select(a=>a.TransactionDate).FirstOrDefault(),

        //                                                ActivityStatusCode = x.ActivityStatus.SystemName,
        //                                                ActivityStatusName = x.ActivityStatus.Name,

        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                           .Where(_Request.SearchCondition)
        //                                   .Count();
        //                }
        //                #region Get Data
        //                _Terminals = _HCoreContext.TUCTerminal
        //                                            .Where(x => x.MerchantId == _Request.AccountId && _HCoreContext.HCUAccountTransaction.Any(a => a.TerminalId == x.Id && a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd))
        //                                            .Select(x => new OAccounts.Terminal.List
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TypeCode = "POS",
        //                                                TypeName = "POS",
        //                                                TerminalId = x.DisplayName,
        //                                                SerialNumber = x.SerialNumber,

        //                                                ProviderReferenceId = x.ProviderId,
        //                                                ProviderReferenceKey = x.Provider.Guid,
        //                                                ProviderDisplayName = x.Provider.DisplayName,
        //                                                ProviderIconUrl = x.Provider.IconStorage.Path,

        //                                                AcquierReferenceId = x.Acquirer.Id,
        //                                                AcquirerReferenceKey = x.Acquirer.Guid,
        //                                                AcquirerDisplayName = x.Acquirer.DisplayName,
        //                                                AcquirerIconUrl = x.Acquirer.IconStorage.Path,

        //                                                StoreReferenceId = x.Store.Id,
        //                                                StoreReferenceKey = x.Store.Guid,
        //                                                StoreDisplayName = x.Store.DisplayName,
        //                                                StoreAddress = x.Store.Address,
        //                                                MerchantReferenceId = x.MerchantId,
        //                                                MerchantReferenceKey = x.Merchant.Guid,
        //                                                MerchantDisplayName = x.Merchant.DisplayName,
        //                                                CreateDate = x.CreateDate,
        //                                                LastActivityDate = x.LastActivityDate,
        //                                                LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(a=>a.TerminalId == x.Id).OrderByDescending(a=>a.TransactionDate).Select(a=>a.TransactionDate).FirstOrDefault(),
        //                                                //LastTransactionDate = x.LastTransactionDate,

        //                                                ActivityStatusCode = x.ActivityStatus.SystemName,
        //                                                ActivityStatusName = x.ActivityStatus.Name,

        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                         .Where(_Request.SearchCondition)
        //                                         .OrderBy(_Request.SortExpression)
        //                                         .Skip(_Request.Offset)
        //                                         .Take(_Request.Limit)
        //                                         .ToList();
        //                #endregion
        //            }
        //            else if (_Request.Type == "idle")
        //            {
        //                if (_Request.RefreshCount)
        //                {
        //                    _Request.TotalRecords = _HCoreContext.TUCTerminal
        //                                            .Where(x =>x.MerchantId == _Request.AccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd))
        //                                            .Select(x => new OAccounts.Terminal.List
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TypeCode = "POS",
        //                                                TypeName = "POS",
        //                                                TerminalId = x.DisplayName,
        //                                                SerialNumber = x.SerialNumber,

        //                                                ProviderReferenceId = x.Provider.Id,
        //                                                ProviderReferenceKey = x.Provider.Guid,
        //                                                ProviderDisplayName = x.Provider.DisplayName,
        //                                                ProviderIconUrl = x.Provider.IconStorage.Path,

        //                                                AcquierReferenceId = x.Acquirer.Id,
        //                                                AcquirerReferenceKey = x.Acquirer.Guid,
        //                                                AcquirerDisplayName = x.Acquirer.DisplayName,
        //                                                AcquirerIconUrl = x.Acquirer.IconStorage.Path,

        //                                                StoreReferenceId = x.Store.Id,
        //                                                StoreReferenceKey = x.Store.Guid,
        //                                                StoreDisplayName = x.Store.DisplayName,
        //                                                StoreAddress = x.Store.Address,
        //                                                MerchantReferenceId = x.MerchantId,
        //                                                MerchantReferenceKey = x.Merchant.Guid,
        //                                                MerchantDisplayName = x.Merchant.DisplayName,
        //                                                CreateDate = x.CreateDate,
        //                                                LastActivityDate = x.LastActivityDate,
        //                                                LastTransactionDate = x.LastTransactionDate,

        //                                                ActivityStatusCode = x.ActivityStatus.SystemName,
        //                                                ActivityStatusName = x.ActivityStatus.Name,

        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                           .Where(_Request.SearchCondition)
        //                                   .Count();
        //                }
        //                #region Get Data
        //                _Terminals = _HCoreContext.TUCTerminal
        //                                            .Where(x => x.MerchantId == _Request.AccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd))
        //                                            .Select(x => new OAccounts.Terminal.List
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TypeCode = "POS",
        //                                                TypeName = "POS",
        //                                                TerminalId = x.DisplayName,
        //                                                SerialNumber = x.SerialNumber,

        //                                                ProviderReferenceId = x.Provider.Id,
        //                                                ProviderReferenceKey = x.Provider.Guid,
        //                                                ProviderDisplayName = x.Provider.DisplayName,
        //                                                ProviderIconUrl = x.Provider.IconStorage.Path,

        //                                                AcquierReferenceId = x.Acquirer.Id,
        //                                                AcquirerReferenceKey = x.Acquirer.Guid,
        //                                                AcquirerDisplayName = x.Acquirer.DisplayName,
        //                                                AcquirerIconUrl = x.Acquirer.IconStorage.Path,

        //                                                StoreReferenceId = x.Store.Id,
        //                                                StoreReferenceKey = x.Store.Guid,
        //                                                StoreDisplayName = x.Store.DisplayName,
        //                                                StoreAddress = x.Store.Address,
        //                                                MerchantReferenceId = x.MerchantId,
        //                                                MerchantReferenceKey = x.Merchant.Guid,
        //                                                MerchantDisplayName = x.Merchant.DisplayName,
        //                                                CreateDate = x.CreateDate,
        //                                                LastActivityDate = x.LastActivityDate,
        //                                                LastTransactionDate = x.LastTransactionDate,

        //                                                ActivityStatusCode = x.ActivityStatus.SystemName,
        //                                                ActivityStatusName = x.ActivityStatus.Name,

        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                         .Where(_Request.SearchCondition)
        //                                         .OrderBy(_Request.SortExpression)
        //                                         .Skip(_Request.Offset)
        //                                         .Take(_Request.Limit)
        //                                         .ToList();
        //                #endregion
        //            }
        //            else if (_Request.Type == "dead")
        //            {
        //                if (_Request.RefreshCount)
        //                {
        //                    _Request.TotalRecords = _HCoreContext.TUCTerminal
        //                                            .Where(x =>  x.MerchantId== _Request.AccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > TDeadDayEnd) == false)
        //                                            .Select(x => new OAccounts.Terminal.List
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TypeCode = "POS",
        //                                                TypeName = "POS",
        //                                                TerminalId = x.DisplayName,
        //                                                SerialNumber = x.SerialNumber,

        //                                                ProviderReferenceId = x.Provider.Id,
        //                                                ProviderReferenceKey = x.Provider.Guid,
        //                                                ProviderDisplayName = x.Provider.DisplayName,
        //                                                ProviderIconUrl = x.Provider.IconStorage.Path,

        //                                                AcquierReferenceId = x.Acquirer.Id,
        //                                                AcquirerReferenceKey = x.Acquirer.Guid,
        //                                                AcquirerDisplayName = x.Acquirer.DisplayName,
        //                                                AcquirerIconUrl = x.Acquirer.IconStorage.Path,

        //                                                StoreReferenceId = x.Store.Id,
        //                                                StoreReferenceKey = x.Store.Guid,
        //                                                StoreDisplayName = x.Store.DisplayName,
        //                                                StoreAddress = x.Store.Address,
        //                                                MerchantReferenceId = x.MerchantId,
        //                                                MerchantReferenceKey = x.Merchant.Guid,
        //                                                MerchantDisplayName = x.Merchant.DisplayName,
        //                                                CreateDate = x.CreateDate,
        //                                                LastActivityDate = x.LastActivityDate,
        //                                                LastTransactionDate = x.LastTransactionDate,

        //                                                ActivityStatusCode = x.ActivityStatus.SystemName,
        //                                                ActivityStatusName = x.ActivityStatus.Name,

        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                           .Where(_Request.SearchCondition)
        //                                   .Count();
        //                }
        //                #region Get Data
        //                _Terminals = _HCoreContext.TUCTerminal
        //                                            .Where(x => x.MerchantId == _Request.AccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > TDeadDayEnd) == false)
        //                                            .Select(x => new OAccounts.Terminal.List
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TypeCode = "POS",
        //                                                TypeName = "POS",
        //                                                TerminalId = x.DisplayName,
        //                                                SerialNumber = x.SerialNumber,

        //                                                ProviderReferenceId = x.Provider.Id,
        //                                                ProviderReferenceKey = x.Provider.Guid,
        //                                                ProviderDisplayName = x.Provider.DisplayName,
        //                                                ProviderIconUrl = x.Provider.IconStorage.Path,

        //                                                AcquierReferenceId = x.Acquirer.Id,
        //                                                AcquirerReferenceKey = x.Acquirer.Guid,
        //                                                AcquirerDisplayName = x.Acquirer.DisplayName,
        //                                                AcquirerIconUrl = x.Acquirer.IconStorage.Path,

        //                                                StoreReferenceId = x.Store.Id,
        //                                                StoreReferenceKey = x.Store.Guid,
        //                                                StoreDisplayName = x.Store.DisplayName,
        //                                                StoreAddress = x.Store.Address,
        //                                                MerchantReferenceId = x.MerchantId,
        //                                                MerchantReferenceKey = x.Merchant.Guid,
        //                                                MerchantDisplayName = x.Merchant.DisplayName,
        //                                                CreateDate = x.CreateDate,
        //                                                LastActivityDate = x.LastActivityDate,
        //                                                LastTransactionDate = x.LastTransactionDate,

        //                                                ActivityStatusCode = x.ActivityStatus.SystemName,
        //                                                ActivityStatusName = x.ActivityStatus.Name,

        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                         .Where(_Request.SearchCondition)
        //                                         .OrderBy(_Request.SortExpression)
        //                                         .Skip(_Request.Offset)
        //                                         .Take(_Request.Limit)
        //                                         .ToList();
        //                #endregion
        //            }
        //            else if (_Request.Type == "unused")
        //            {
        //                if (_Request.RefreshCount)
        //                {
        //                    _Request.TotalRecords = _HCoreContext.TUCTerminal
        //                                            .Where(x => x.MerchantId == _Request.AccountId && x.HCUAccountTransaction.Any() == false)
        //                                            .Select(x => new OAccounts.Terminal.List
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TypeCode = "POS",
        //                                                TypeName = "POS",
        //                                                TerminalId = x.DisplayName,
        //                                                SerialNumber = x.SerialNumber,

        //                                                ProviderReferenceId = x.Provider.Id,
        //                                                ProviderReferenceKey = x.Provider.Guid,
        //                                                ProviderDisplayName = x.Provider.DisplayName,
        //                                                ProviderIconUrl = x.Provider.IconStorage.Path,

        //                                                AcquierReferenceId = x.Acquirer.Id,
        //                                                AcquirerReferenceKey = x.Acquirer.Guid,
        //                                                AcquirerDisplayName = x.Acquirer.DisplayName,
        //                                                AcquirerIconUrl = x.Acquirer.IconStorage.Path,

        //                                                StoreReferenceId = x.Store.Id,
        //                                                StoreReferenceKey = x.Store.Guid,
        //                                                StoreDisplayName = x.Store.DisplayName,
        //                                                StoreAddress = x.Store.Address,
        //                                                MerchantReferenceId = x.MerchantId,
        //                                                MerchantReferenceKey = x.Merchant.Guid,
        //                                                MerchantDisplayName = x.Merchant.DisplayName,
        //                                                CreateDate = x.CreateDate,
        //                                                LastActivityDate = x.LastActivityDate,
        //                                                LastTransactionDate = x.LastTransactionDate,

        //                                                ActivityStatusCode = x.ActivityStatus.SystemName,
        //                                                ActivityStatusName = x.ActivityStatus.Name,

        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                           .Where(_Request.SearchCondition)
        //                                   .Count();
        //                }
        //                #region Get Data
        //                _Terminals = _HCoreContext.TUCTerminal
        //                                            .Where(x =>  x.MerchantId == _Request.AccountId && x.HCUAccountTransaction.Any() == false)
        //                                            .Select(x => new OAccounts.Terminal.List
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TypeCode = "POS",
        //                                                TypeName = "POS",
        //                                                TerminalId = x.DisplayName,
        //                                                SerialNumber = x.SerialNumber,

        //                                                ProviderReferenceId = x.Provider.Id,
        //                                                ProviderReferenceKey = x.Provider.Guid,
        //                                                ProviderDisplayName = x.Provider.DisplayName,
        //                                                ProviderIconUrl = x.Provider.IconStorage.Path,

        //                                                AcquierReferenceId = x.Acquirer.Id,
        //                                                AcquirerReferenceKey = x.Acquirer.Guid,
        //                                                AcquirerDisplayName = x.Acquirer.DisplayName,
        //                                                AcquirerIconUrl = x.Acquirer.IconStorage.Path,

        //                                                StoreReferenceId = x.Store.Id,
        //                                                StoreReferenceKey = x.Store.Guid,
        //                                                StoreDisplayName = x.Store.DisplayName,
        //                                                StoreAddress = x.Store.Address,
        //                                                MerchantReferenceId = x.MerchantId,
        //                                                MerchantReferenceKey = x.Merchant.Guid,
        //                                                MerchantDisplayName = x.Merchant.DisplayName,
        //                                                CreateDate = x.CreateDate,
        //                                                LastActivityDate = x.LastActivityDate,
        //                                                LastTransactionDate = x.LastTransactionDate,

        //                                                ActivityStatusCode = x.ActivityStatus.SystemName,
        //                                                ActivityStatusName = x.ActivityStatus.Name,

        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                         .Where(_Request.SearchCondition)
        //                                         .OrderBy(_Request.SortExpression)
        //                                         .Skip(_Request.Offset)
        //                                         .Take(_Request.Limit)
        //                                         .ToList();
        //                #endregion
        //            }
        //            else
        //            {
        //                if (_Request.RefreshCount)
        //                {
        //                    #region Total Records
        //                    _Request.TotalRecords = _HCoreContext.TUCTerminal
        //                                            .Where(x => x.MerchantId == _Request.AccountId)
        //                                            .Select(x => new OAccounts.Terminal.List
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TypeCode = "POS",
        //                                                TypeName = "POS",
        //                                                TerminalId = x.DisplayName,
        //                                                SerialNumber = x.SerialNumber,

        //                                                ProviderReferenceId = x.Provider.Id,
        //                                                ProviderReferenceKey = x.Provider.Guid,
        //                                                ProviderDisplayName = x.Provider.DisplayName,
        //                                                ProviderIconUrl = x.Provider.IconStorage.Path,

        //                                                AcquierReferenceId = x.Acquirer.Id,
        //                                                AcquirerReferenceKey = x.Acquirer.Guid,
        //                                                AcquirerDisplayName = x.Acquirer.DisplayName,
        //                                                AcquirerIconUrl = x.Acquirer.IconStorage.Path,

        //                                                StoreReferenceId = x.Store.Id,
        //                                                StoreReferenceKey = x.Store.Guid,
        //                                                StoreDisplayName = x.Store.DisplayName,
        //                                                StoreAddress = x.Store.Address,
        //                                                MerchantReferenceId = x.MerchantId,
        //                                                MerchantReferenceKey = x.Merchant.Guid,
        //                                                MerchantDisplayName = x.Merchant.DisplayName,
        //                                                CreateDate = x.CreateDate,
        //                                                LastActivityDate = x.LastActivityDate,
        //                                                //LastTransactionDate = x.LastTransactionDate,
        //                                                LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(a => a.TerminalId == x.Id).OrderByDescending(a => a.TransactionDate).Select(a => a.TransactionDate).FirstOrDefault(),

        //                                                ActivityStatusCode = x.ActivityStatus.SystemName,
        //                                                ActivityStatusName = x.ActivityStatus.Name,

        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                           .Where(_Request.SearchCondition)
        //                                   .Count();
        //                    #endregion
        //                }
        //                #region Get Data
        //                _Terminals = _HCoreContext.TUCTerminal
        //                                            .Where(x =>  x.MerchantId == _Request.AccountId)
        //                                            .Select(x => new OAccounts.Terminal.List
        //                                            {
        //                                                ReferenceId = x.Id,
        //                                                ReferenceKey = x.Guid,

        //                                                TypeCode = "POS",
        //                                                TypeName = "POS",
        //                                                TerminalId = x.DisplayName,
        //                                                SerialNumber = x.SerialNumber,

        //                                                ProviderReferenceId = x.Provider.Id,
        //                                                ProviderReferenceKey = x.Provider.Guid,
        //                                                ProviderDisplayName = x.Provider.DisplayName,
        //                                                ProviderIconUrl = x.Provider.IconStorage.Path,

        //                                                AcquierReferenceId = x.Acquirer.Id,
        //                                                AcquirerReferenceKey = x.Acquirer.Guid,
        //                                                AcquirerDisplayName = x.Acquirer.DisplayName,
        //                                                AcquirerIconUrl = x.Acquirer.IconStorage.Path,

        //                                                StoreReferenceId = x.Store.Id,
        //                                                StoreReferenceKey = x.Store.Guid,
        //                                                StoreDisplayName = x.Store.DisplayName,
        //                                                StoreAddress = x.Store.Address,
        //                                                MerchantReferenceId = x.MerchantId,
        //                                                MerchantReferenceKey = x.Merchant.Guid,
        //                                                MerchantDisplayName = x.Merchant.DisplayName,
        //                                                CreateDate = x.CreateDate,
        //                                                LastActivityDate = x.LastActivityDate,
        //                                                //LastTransactionDate = x.LastTransactionDate,
        //                                                LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(a => a.TerminalId == x.Id).OrderByDescending(a => a.TransactionDate).Select(a => a.TransactionDate).FirstOrDefault(),
        //                                                ActivityStatusCode = x.ActivityStatus.SystemName,
        //                                                ActivityStatusName = x.ActivityStatus.Name,

        //                                                StatusCode = x.Status.SystemName,
        //                                                StatusName = x.Status.Name,
        //                                            })
        //                                         .Where(_Request.SearchCondition)
        //                                         .OrderBy(_Request.SortExpression)
        //                                         .Skip(_Request.Offset)
        //                                         .Take(_Request.Limit)
        //                                         .ToList();
        //                #endregion
        //            }

        //            #region Create  Response Object
        //            _HCoreContext.Dispose();
        //            foreach (var DataItem in _Terminals)
        //            {
        //                DataItem.LastActivityDate = DataItem.LastTransactionDate;
        //                if (DataItem.LastTransactionDate > TDayStart && DataItem.LastTransactionDate < TDayEnd)
        //                {
        //                    DataItem.ActivityStatusCode = "active";
        //                    DataItem.ActivityStatusName = "active";
        //                }
        //                else if (DataItem.LastTransactionDate > T7DayStart && DataItem.LastTransactionDate < T7DayEnd)
        //                {
        //                    DataItem.ActivityStatusCode = "idle";
        //                    DataItem.ActivityStatusName = "idle";
        //                }
        //                else if (DataItem.LastTransactionDate < TDeadDayEnd)
        //                {
        //                    DataItem.ActivityStatusCode = "dead";
        //                    DataItem.ActivityStatusName = "dead";
        //                }
        //                else if (DataItem.LastTransactionDate == null)
        //                {
        //                    DataItem.ActivityStatusCode = "unused";
        //                    DataItem.ActivityStatusName = "unused";
        //                }



        //                if (!string.IsNullOrEmpty(DataItem.ProviderIconUrl))
        //                {
        //                    DataItem.ProviderIconUrl = _AppConfig.StorageUrl + DataItem.ProviderIconUrl;
        //                }
        //                else
        //                {
        //                    DataItem.ProviderIconUrl = _AppConfig.Default_Icon;
        //                }
        //                if (!string.IsNullOrEmpty(DataItem.AcquirerIconUrl))
        //                {
        //                    DataItem.AcquirerIconUrl = _AppConfig.StorageUrl + DataItem.AcquirerIconUrl;
        //                }
        //                else
        //                {
        //                    DataItem.AcquirerIconUrl = _AppConfig.Default_Icon;
        //                }
        //            }
        //            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Terminals, _Request.Offset, _Request.Limit);
        //            #endregion
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //            #endregion
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        return HCoreHelper.LogException("GetTerminals", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
        //    }
        //    #endregion
        //}
        //internal OResponse GetTerminalsOverview(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {

        //        DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
        //        DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
        //        DateTime T7DayStart = TDayStart.AddDays(-7);
        //        DateTime T7DayEnd = TDayStart.AddSeconds(1);
        //        DateTime TDeadDayEnd = T7DayStart;
        //        _TerminalOverview = new OAccounts.Terminal.Overview();
        //        if (string.IsNullOrEmpty(_Request.SearchCondition))
        //        {
        //            HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
        //        }
        //        if (string.IsNullOrEmpty(_Request.SortExpression))
        //        {
        //            HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
        //        }
        //        if (_Request.Limit < 1)
        //        {
        //            _Request.Limit = _AppConfig.DefaultRecordsLimit;
        //        }
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            #region Terminals Overview
        //            //_TerminalOverview.Total = _HCoreContext.TUCTerminal.Count(x =>x.SubOwner.OwnerId == _Request.UserAccountId);
        //            //_TerminalOverview.Unused = _HCoreContext.TUCTerminal.Count(x =>x.SubOwner.OwnerId == _Request.UserAccountId && x.LastTransactionDate == null);
        //            //_TerminalOverview.Active = _HCoreContext.TUCTerminal.Count(x =>x.SubOwner.OwnerId == _Request.UserAccountId && x.LastTransactionDate > TDayStart && x.LastTransactionDate < TDayEnd);
        //            //_TerminalOverview.Idle = _HCoreContext.TUCTerminal.Count(x =>x.SubOwner.OwnerId == _Request.UserAccountId && x.LastTransactionDate > T7DayStart && x.LastTransactionDate < T7DayEnd);
        //            //_TerminalOverview.Dead = _HCoreContext.TUCTerminal.Count(x =>x.SubOwner.OwnerId == _Request.UserAccountId && x.LastTransactionDate < TDeadDayEnd);
        //            #endregion
        //            #region Total Records
        //            _TerminalOverview.Total = _HCoreContext.TUCTerminal
        //                                    .Where(x => x.MerchantId == _Request.AccountId)
        //                                    .Select(x => new OAccounts.Terminal.List
        //                                    {
        //                                        ReferenceId = x.Id,
        //                                        ReferenceKey = x.Guid,

        //                                        TypeCode = "POS",
        //                                        TypeName = "POS",
        //                                        TerminalId = x.DisplayName,
        //                                        SerialNumber = x.SerialNumber,

        //                                        ProviderReferenceId = x.Provider.Id,
        //                                        ProviderReferenceKey = x.Provider.Guid,
        //                                        ProviderDisplayName = x.Provider.DisplayName,
        //                                        ProviderIconUrl = x.Provider.IconStorage.Path,

        //                                        AcquierReferenceId = x.Acquirer.Id,
        //                                        AcquirerReferenceKey = x.Acquirer.Guid,
        //                                        AcquirerDisplayName = x.Acquirer.DisplayName,
        //                                        AcquirerIconUrl = x.Acquirer.IconStorage.Path,

        //                                        StoreReferenceId = x.Store.Id,
        //                                        StoreReferenceKey = x.Store.Guid,
        //                                        StoreDisplayName = x.Store.DisplayName,
        //                                        StoreAddress = x.Store.Address,
        //                                        MerchantReferenceId = x.MerchantId,
        //                                        MerchantReferenceKey = x.Merchant.Guid,
        //                                        MerchantDisplayName = x.Merchant.DisplayName,
        //                                        CreateDate = x.CreateDate,
        //                                        LastActivityDate = x.LastActivityDate,
        //                                        LastTransactionDate = x.LastTransactionDate,

        //                                        ActivityStatusCode = x.ActivityStatus.SystemName,
        //                                        ActivityStatusName = x.ActivityStatus.Name,

        //                                        StatusCode = x.Status.SystemName,
        //                                        StatusName = x.Status.Name,
        //                                    })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //            #endregion
        //            #region Unused Records
        //            _TerminalOverview.Unused = _HCoreContext.TUCTerminal
        //                                    .Where(x =>  x.MerchantId == _Request.AccountId && _HCoreContext.HCUAccountTransaction.Where(a=>a.TerminalId == x.Id).Any() == false)
        //                                    .Select(x => new OAccounts.Terminal.List
        //                                    {
        //                                        ReferenceId = x.Id,
        //                                        ReferenceKey = x.Guid,

        //                                        TypeCode = "POS",
        //                                        TypeName = "POS",
        //                                        TerminalId = x.DisplayName,
        //                                        SerialNumber = x.SerialNumber,

        //                                        ProviderReferenceId = x.Provider.Id,
        //                                        ProviderReferenceKey = x.Provider.Guid,
        //                                        ProviderDisplayName = x.Provider.DisplayName,
        //                                        ProviderIconUrl = x.Provider.IconStorage.Path,

        //                                        AcquierReferenceId = x.Acquirer.Id,
        //                                        AcquirerReferenceKey = x.Acquirer.Guid,
        //                                        AcquirerDisplayName = x.Acquirer.DisplayName,
        //                                        AcquirerIconUrl = x.Acquirer.IconStorage.Path,

        //                                        StoreReferenceId = x.Store.Id,
        //                                        StoreReferenceKey = x.Store.Guid,
        //                                        StoreDisplayName = x.Store.DisplayName,
        //                                        StoreAddress = x.Store.Address,
        //                                        MerchantReferenceId = x.MerchantId,
        //                                        MerchantReferenceKey = x.Merchant.Guid,
        //                                        MerchantDisplayName = x.Merchant.DisplayName,
        //                                        CreateDate = x.CreateDate,
        //                                        LastActivityDate = x.LastActivityDate,
        //                                        LastTransactionDate = x.LastTransactionDate,

        //                                        ActivityStatusCode = x.ActivityStatus.SystemName,
        //                                        ActivityStatusName = x.ActivityStatus.Name,

        //                                        StatusCode = x.Status.SystemName,
        //                                        StatusName = x.Status.Name,
        //                                    })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //            #endregion
        //            #region Active Records
        //            _TerminalOverview.Active = _HCoreContext.TUCTerminal
        //                                    .Where(x =>  x.MerchantId == _Request.AccountId && _HCoreContext.HCUAccountTransaction.Any(a => a.TerminalId == x.Id && a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd))
        //                                    .Select(x => new OAccounts.Terminal.List
        //                                    {
        //                                        ReferenceId = x.Id,
        //                                        ReferenceKey = x.Guid,

        //                                        TypeCode = "POS",
        //                                        TypeName = "POS",
        //                                        TerminalId = x.DisplayName,
        //                                        SerialNumber = x.SerialNumber,

        //                                        ProviderReferenceId = x.Provider.Id,
        //                                        ProviderReferenceKey = x.Provider.Guid,
        //                                        ProviderDisplayName = x.Provider.DisplayName,
        //                                        ProviderIconUrl = x.Provider.IconStorage.Path,

        //                                        AcquierReferenceId = x.Acquirer.Id,
        //                                        AcquirerReferenceKey = x.Acquirer.Guid,
        //                                        AcquirerDisplayName = x.Acquirer.DisplayName,
        //                                        AcquirerIconUrl = x.Acquirer.IconStorage.Path,

        //                                        StoreReferenceId = x.Store.Id,
        //                                        StoreReferenceKey = x.Store.Guid,
        //                                        StoreDisplayName = x.Store.DisplayName,
        //                                        StoreAddress = x.Store.Address,
        //                                        MerchantReferenceId = x.MerchantId,
        //                                        MerchantReferenceKey = x.Merchant.Guid,
        //                                        MerchantDisplayName = x.Merchant.DisplayName,
        //                                        CreateDate = x.CreateDate,
        //                                        LastActivityDate = x.LastActivityDate,
        //                                        LastTransactionDate = x.LastTransactionDate,

        //                                        ActivityStatusCode = x.ActivityStatus.SystemName,
        //                                        ActivityStatusName = x.ActivityStatus.Name,

        //                                        StatusCode = x.Status.SystemName,
        //                                        StatusName = x.Status.Name,
        //                                    })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //            #endregion
        //            #region Idle Records
        //            _TerminalOverview.Idle = _HCoreContext.TUCTerminal
        //                                    .Where(x => x.MerchantId == _Request.AccountId && _HCoreContext.HCUAccountTransaction.Any(a =>a.TerminalId == x.Id && a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd))
        //                                    .Select(x => new OAccounts.Terminal.List
        //                                    {
        //                                        ReferenceId = x.Id,
        //                                        ReferenceKey = x.Guid,

        //                                        TypeCode = "POS",
        //                                        TypeName = "POS",
        //                                        TerminalId = x.DisplayName,
        //                                        SerialNumber = x.SerialNumber,

        //                                        ProviderReferenceId = x.Provider.Id,
        //                                        ProviderReferenceKey = x.Provider.Guid,
        //                                        ProviderDisplayName = x.Provider.DisplayName,
        //                                        ProviderIconUrl = x.Provider.IconStorage.Path,

        //                                        AcquierReferenceId = x.Acquirer.Id,
        //                                        AcquirerReferenceKey = x.Acquirer.Guid,
        //                                        AcquirerDisplayName = x.Acquirer.DisplayName,
        //                                        AcquirerIconUrl = x.Acquirer.IconStorage.Path,

        //                                        StoreReferenceId = x.Store.Id,
        //                                        StoreReferenceKey = x.Store.Guid,
        //                                        StoreDisplayName = x.Store.DisplayName,
        //                                        StoreAddress = x.Store.Address,
        //                                        MerchantReferenceId = x.MerchantId,
        //                                        MerchantReferenceKey = x.Merchant.Guid,
        //                                        MerchantDisplayName = x.Merchant.DisplayName,
        //                                        CreateDate = x.CreateDate,
        //                                        LastActivityDate = x.LastActivityDate,
        //                                        LastTransactionDate = x.LastTransactionDate,

        //                                        ActivityStatusCode = x.ActivityStatus.SystemName,
        //                                        ActivityStatusName = x.ActivityStatus.Name,

        //                                        StatusCode = x.Status.SystemName,
        //                                        StatusName = x.Status.Name,
        //                                    })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //            #endregion
        //            #region Dead Records
        //            _TerminalOverview.Dead = _HCoreContext.TUCTerminal
        //                                    .Where(x =>  x.MerchantId == _Request.AccountId && _HCoreContext.HCUAccountTransaction.Any(a =>a.TerminalId ==  x.Id && a.TransactionDate > TDeadDayEnd) == false)
        //                                    .Select(x => new OAccounts.Terminal.List
        //                                    {
        //                                        ReferenceId = x.Id,
        //                                        ReferenceKey = x.Guid,

        //                                        TypeCode = "POS",
        //                                        TypeName = "POS",
        //                                        TerminalId = x.DisplayName,
        //                                        SerialNumber = x.SerialNumber,

        //                                        ProviderReferenceId = x.Provider.Id,
        //                                        ProviderReferenceKey = x.Provider.Guid,
        //                                        ProviderDisplayName = x.Provider.DisplayName,
        //                                        ProviderIconUrl = x.Provider.IconStorage.Path,

        //                                        AcquierReferenceId = x.Acquirer.Id,
        //                                        AcquirerReferenceKey = x.Acquirer.Guid,
        //                                        AcquirerDisplayName = x.Acquirer.DisplayName,
        //                                        AcquirerIconUrl = x.Acquirer.IconStorage.Path,

        //                                        StoreReferenceId = x.Store.Id,
        //                                        StoreReferenceKey = x.Store.Guid,
        //                                        StoreDisplayName = x.Store.DisplayName,
        //                                        StoreAddress = x.Store.Address,
        //                                        MerchantReferenceId = x.MerchantId,
        //                                        MerchantReferenceKey = x.Merchant.Guid,
        //                                        MerchantDisplayName = x.Merchant.DisplayName,
        //                                        CreateDate = x.CreateDate,
        //                                        LastActivityDate = x.LastActivityDate,
        //                                        LastTransactionDate = x.LastTransactionDate,

        //                                        ActivityStatusCode = x.ActivityStatus.SystemName,
        //                                        ActivityStatusName = x.ActivityStatus.Name,

        //                                        StatusCode = x.Status.SystemName,
        //                                        StatusName = x.Status.Name,
        //                                    })
        //                                   .Where(_Request.SearchCondition)
        //                           .Count();
        //            #endregion

        //            #region Create  Response Object
        //            _HCoreContext.Dispose();
        //            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _TerminalOverview, _Request.Offset, _Request.Limit);
        //            #endregion
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //            #endregion
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        return HCoreHelper.LogException("GetTerminals", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
        //    }
        //    #endregion
        //}




        /// <summary>
        /// Description: Gets the terminal details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTerminal(OAccounts.Terminal.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.TUCTerminal
                        .Where(x => x.Guid == _Request.AccountKey && x.Id == _Request.AccountId)
                        .Select(x => new
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            //TypeCode = "POS",
                            //TypeName = "POS",
                            TerminalId = x.DisplayName,
                            TerminalIdentificationNumber = x.IdentificationNumber,
                            SerialNumber = x.SerialNumber,
                            ReceiptStorageUrl = x.ReceiptStorage.Path,

                            ProviderReferenceId = x.ProviderId,
                            ProviderReferenceKey = x.Provider.Guid,
                            ProviderDisplayName = x.Provider.DisplayName,
                            ProviderIconUrl = x.Provider.IconStorage.Path,


                            SupportContactName = x.Provider.FirstName + " " + x.Provider.LastName,
                            SupportContactEmailAddress = x.Provider.SecondaryEmailAddress,
                            SupportContactContactNumber = x.Provider.ContactNumber,

                            AcquierReferenceId = x.Acquirer.Id,
                            AcquirerReferenceKey = x.Acquirer.Guid,
                            AcquirerDisplayName = x.Acquirer.DisplayName,
                            AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                            StoreReferenceId = x.Store.Id,
                            StoreReferenceKey = x.Store.Guid,
                            StoreDisplayName = x.Store.DisplayName,

                            LastActivityDate = x.LastActivityDate,

                            ActivityStatusCode = x.ActivityStatus.SystemName,
                            ActivityStatusName = x.ActivityStatus.Name,
                            MerchantReferenceId = x.MerchantId,
                            MerchantReferenceKey = x.Merchant.Guid,
                            MerchantDisplayName = x.Merchant.DisplayName,



                            Address = x.Store.Address,
                            Latitude = x.Store.Latitude,
                            Longitude = x.Store.Longitude,

                            CityAreaId = x.Store.CityAreaId,
                            CityAreaCode = x.Store.Guid,
                            CityAreaName = x.Store.CityArea.Name,

                            CityId = x.Store.CityId,
                            CityCode = x.Store.Guid,
                            CityName = x.Store.City.Name,

                            StateId = x.Store.StateId,
                            StateCode = x.Store.Guid,
                            StateName = x.Store.State.Name,

                            CountryId = x.Store.CountryId,
                            CountryCode = x.Store.Guid,
                            CountryName = x.Store.Country.Name,


                            CreateDate = x.CreateDate,
                            CreatedByReferenceId = x.CreatedById,
                            CreatedByReferenceKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyByReferenceId = x.ModifyById,
                            ModifyByReferenceKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,

                            TypeId = x.TypeId,
                            TypeCode = x.Type.SystemName,
                            TypeName = x.Type.Name,
                        }).FirstOrDefault();
                    if (Details != null)
                    {
                        _TerminalDetails = new OAccounts.Terminal.Details();
                        _TerminalDetails.ReferenceId = Details.ReferenceId;
                        _TerminalDetails.ReferenceKey = Details.ReferenceKey;
                        _TerminalDetails.TypeId = Details.TypeId;
                        _TerminalDetails.TypeCode = Details.TypeCode;
                        _TerminalDetails.TypeName = Details.TypeName;
                        _TerminalDetails.TerminalId = Details.TerminalId;
                        _TerminalDetails.TerminalIdentificationNumber = Details.TerminalIdentificationNumber;
                        _TerminalDetails.SerialNumber = Details.SerialNumber;
                        _TerminalDetails.ProviderReferenceId = Details.ProviderReferenceId;
                        _TerminalDetails.ProviderReferenceKey = Details.ProviderReferenceKey;
                        _TerminalDetails.ProviderDisplayName = Details.ProviderDisplayName;
                        _TerminalDetails.ProviderIconUrl = Details.ProviderIconUrl;
                        _TerminalDetails.AcquierReferenceId = Details.AcquierReferenceId;
                        _TerminalDetails.AcquirerReferenceKey = Details.AcquirerReferenceKey;
                        _TerminalDetails.AcquirerDisplayName = Details.AcquirerDisplayName;
                        _TerminalDetails.AcquirerIconUrl = Details.AcquirerIconUrl;
                        _TerminalDetails.StoreReferenceId = Details.StoreReferenceId;
                        _TerminalDetails.StoreReferenceKey = Details.StoreReferenceKey;
                        _TerminalDetails.StoreDisplayName = Details.StoreDisplayName;
                        _TerminalDetails.LastActivityDate = Details.LastActivityDate;
                        _TerminalDetails.ActivityStatusCode = Details.ActivityStatusCode;
                        _TerminalDetails.ActivityStatusName = Details.ActivityStatusName;
                        _TerminalDetails.MerchantReferenceId = Details.MerchantReferenceId;
                        _TerminalDetails.MerchantReferenceKey = Details.MerchantReferenceKey;
                        _TerminalDetails.MerchantDisplayName = Details.MerchantDisplayName;

                        _TerminalDetails.SupportContactName = Details.SupportContactName;
                        _TerminalDetails.SupportContactEmailAddress = Details.SupportContactEmailAddress;
                        _TerminalDetails.SupportContactContactNumber = Details.SupportContactContactNumber;


                        //_StoreDetails.DisplayName = Details.DisplayName;
                        //_StoreDetails.EmailAddress = Details.EmailAddress;
                        //_StoreDetails.ContactNumber = Details.ContactNumber;
                        //_ContactPerson = new OAccounts.ContactPerson
                        //{
                        //    FirstName = Details.FirstName,
                        //    LastName = Details.LastName,
                        //    EmailAddress = Details.EmailAddress,
                        //    MobileNumber = Details.MobileNumber,
                        //};
                        //_StoreDetails.ContactPerson = _ContactPerson;

                        if (!string.IsNullOrEmpty(_TerminalDetails.ProviderIconUrl))
                        {
                            _TerminalDetails.ProviderIconUrl = _AppConfig.StorageUrl + _TerminalDetails.ProviderIconUrl;
                        }
                        else
                        {
                            _TerminalDetails.ProviderIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_TerminalDetails.AcquirerIconUrl))
                        {
                            _TerminalDetails.AcquirerIconUrl = _AppConfig.StorageUrl + _TerminalDetails.AcquirerIconUrl;
                        }
                        else
                        {
                            _TerminalDetails.AcquirerIconUrl = _AppConfig.Default_Icon;
                        }

                        _TerminalDetails.CreateDate = Details.CreateDate;
                        _TerminalDetails.CreatedByReferenceId = Details.CreatedByReferenceId;
                        _TerminalDetails.CreatedByReferenceKey = Details.CreatedByReferenceKey;
                        _TerminalDetails.CreatedByDisplayName = Details.CreatedByDisplayName;

                        _TerminalDetails.ModifyDate = Details.ModifyDate;
                        _TerminalDetails.ModifyByReferenceId = Details.ModifyByReferenceId;
                        _TerminalDetails.ModifyByReferenceKey = Details.ModifyByReferenceKey;
                        _TerminalDetails.ModifyByDisplayName = Details.ModifyByDisplayName;

                        _TerminalDetails.StatusCode = Details.StatusCode;
                        _TerminalDetails.StatusName = Details.StatusName;

                        _OAddress = new OAddress();

                        _OAddress.Address = Details.Address;

                        _OAddress.CityAreaId = Details.CityAreaId;
                        _OAddress.CityAreaCode = Details.CityAreaCode;
                        _OAddress.CityAreaName = Details.CityAreaName;

                        _OAddress.CityId = Details.CityId;
                        _OAddress.CityCode = Details.CityCode;
                        _OAddress.CityName = Details.CityName;

                        _OAddress.StateId = Details.StateId;
                        _OAddress.StateCode = Details.StateCode;
                        _OAddress.StateName = Details.StateName;

                        _OAddress.CountryId = Details.CountryId;
                        _OAddress.CountryCode = Details.CountryCode;
                        _OAddress.CountryName = Details.CountryName;

                        _OAddress.Latitude = Details.Latitude;
                        _OAddress.Longitude = Details.Longitude;
                        _TerminalDetails.StoreAddress = _OAddress.Address;
                        _TerminalDetails.StoreAddressComponent = _OAddress;
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _TerminalDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetTerminal", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the sub account details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSubAccount(OAccounts.SubAccount.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey && x.Id == _Request.AccountId && x.AccountTypeId == UserAccountType.MerchantSubAccount)
                        .Select(x => new
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            DisplayName = x.DisplayName,
                            Name = x.DisplayName,
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            MobileNumber = x.MobileNumber,
                            ContactNumber = x.ContactNumber,
                            EmailAddress = x.EmailAddress,
                            IconUrl = x.IconStorage.Path,
                            Address = x.Address,
                            Latitude = x.Latitude,
                            Longitude = x.Longitude,

                            CityAreaId = x.CityAreaId,
                            CityAreaCode = x.Guid,
                            CityAreaName = x.CityArea.Name,

                            CityId = x.CityId,
                            CityCode = x.Guid,
                            CityName = x.City.Name,

                            StateId = x.StateId,
                            StateCode = x.Guid,
                            StateName = x.State.Name,

                            CountryId = x.CountryId,
                            CountryCode = x.Guid,
                            CountryName = x.Country.Name,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                            EmployeeId = x.ReferralCode,
                            GenderCode = x.Gender.SystemName,
                            GenderName = x.Gender.Name,

                            RoleId = x.RoleId,
                            RoleKey = x.Role.SystemName,
                            RoleName = x.Role.Name,

                            StoreReferenceId = x.SubOwnerId,
                            StoreReferenceKey = x.SubOwner.Guid,
                            StoreDislayName = x.SubOwner.DisplayName,
                            StoreAddress = x.SubOwner.Address,

                            CreateDate = x.CreateDate,
                            CreatedByReferenceId = x.CreatedById,
                            CreatedByReferenceKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyByReferenceId = x.ModifyById,
                            ModifyByReferenceKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,
                        }).FirstOrDefault();
                    if (Details != null)
                    {
                        _SubAccountDetails = new OAccounts.SubAccount.Details();
                        _SubAccountDetails.ReferenceId = Details.ReferenceId;
                        _SubAccountDetails.ReferenceKey = Details.ReferenceKey;
                        _SubAccountDetails.Name = Details.Name;
                        _SubAccountDetails.FirstName = Details.FirstName;
                        _SubAccountDetails.LastName = Details.LastName;
                        _SubAccountDetails.EmailAddress = Details.EmailAddress;
                        _SubAccountDetails.ContactNumber = Details.ContactNumber;
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            _SubAccountDetails.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            _SubAccountDetails.IconUrl = _AppConfig.Default_Icon;
                        }
                        _SubAccountDetails.GenderCode = Details.GenderCode;
                        _SubAccountDetails.GenderName = Details.GenderName;

                        _SubAccountDetails.RoleId = Details.RoleId;
                        _SubAccountDetails.RoleKey = Details.RoleKey;
                        _SubAccountDetails.RoleName = Details.RoleName;

                        _SubAccountDetails.StoreReferenceId = Details.StoreReferenceId;
                        _SubAccountDetails.StoreReferenceKey = Details.StoreReferenceKey;
                        _SubAccountDetails.StoreDislayName = Details.StoreDislayName;
                        _SubAccountDetails.StoreAddress = Details.StoreAddress;

                        _SubAccountDetails.StatusCode = Details.StatusCode;
                        _SubAccountDetails.StatusName = Details.StatusName;

                        _SubAccountDetails.CreateDate = Details.CreateDate;
                        _SubAccountDetails.CreatedByReferenceId = Details.CreatedByReferenceId;
                        _SubAccountDetails.CreatedByReferenceKey = Details.CreatedByReferenceKey;
                        _SubAccountDetails.CreatedByDisplayName = Details.CreatedByDisplayName;

                        _SubAccountDetails.ModifyDate = Details.ModifyDate;
                        _SubAccountDetails.ModifyByReferenceId = Details.ModifyByReferenceId;
                        _SubAccountDetails.ModifyByReferenceKey = Details.ModifyByReferenceKey;
                        _SubAccountDetails.ModifyByDisplayName = Details.ModifyByDisplayName;

                        _OAddress = new OAddress();
                        _OAddress.Address = Details.Address;

                        _OAddress.CityAreaId = Details.CityAreaId;
                        _OAddress.CityAreaCode = Details.CityAreaCode;
                        _OAddress.CityAreaName = Details.CityAreaName;

                        _OAddress.CityId = Details.CityId;
                        _OAddress.CityCode = Details.CityCode;
                        _OAddress.CityName = Details.CityName;

                        _OAddress.StateId = Details.StateId;
                        _OAddress.StateCode = Details.StateCode;
                        _OAddress.StateName = Details.StateName;

                        _OAddress.CountryId = Details.CountryId;
                        _OAddress.CountryCode = Details.CountryCode;
                        _OAddress.CountryName = Details.CountryName;

                        _OAddress.Latitude = Details.Latitude;
                        _OAddress.Longitude = Details.Longitude;
                        _SubAccountDetails.Address = _OAddress.Address;
                        _SubAccountDetails.AddressComponent = _OAddress;
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SubAccountDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCashier", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the sub account list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSubAccount(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _SubAccounts = new List<OAccounts.SubAccount.List>();
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetSubAccountsDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetSubAccountsDownload>("ActorGetSubAccountsDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantSubAccount && x.OwnerId == _Request.AccountId)
                                                .Select(x => new OAccounts.SubAccount.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CashierId = x.DisplayName,
                                                    Name = x.Name,
                                                    UserName = x.User.Username,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    GenderCode = x.Gender.SystemName,
                                                    GenderName = x.Gender.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    RoleId = x.RoleId,
                                                    RoleKey = x.Role.SystemName,
                                                    RoleName = x.Role.Name,

                                                    StoreReferenceId = x.SubOwner.Id,
                                                    StoreReferenceKey = x.SubOwner.Guid,
                                                    StoreDislayName = x.SubOwner.DisplayName,
                                                    StoreAddress = x.SubOwner.Address,
                                                    LastActivityDate = x.LastActivityDate
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _SubAccounts = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantSubAccount && x.OwnerId == _Request.AccountId)
                                                .Select(x => new OAccounts.SubAccount.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CashierId = x.DisplayName,
                                                    Name = x.Name,
                                                    UserName = x.User.Username,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    GenderCode = x.Gender.SystemName,
                                                    GenderName = x.Gender.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    RoleId = x.RoleId,
                                                    RoleKey = x.Role.SystemName,
                                                    RoleName = x.Role.Name,

                                                    StoreReferenceId = x.SubOwner.Id,
                                                    StoreReferenceKey = x.SubOwner.Guid,
                                                    StoreDislayName = x.SubOwner.DisplayName,
                                                    StoreAddress = x.SubOwner.Address,
                                                    LastActivityDate = x.LastActivityDate
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _SubAccounts)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _SubAccounts, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetSubAccounts", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the sub accounts download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetSubAccountsDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Limit = 800;
                _ListDownload = new List<OAccounts.SubAccount.ListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                            .Where(x => x.AccountTypeId == UserAccountType.MerchantSubAccount && x.OwnerId == _Request.AccountId)
                                            .Select(x => new OAccounts.SubAccount.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                CashierId = x.DisplayName,
                                                Name = x.Name,
                                                UserName = x.User.Username,
                                                EmailAddress = x.EmailAddress,
                                                ContactNumber = x.ContactNumber,
                                                GenderCode = x.Gender.SystemName,
                                                GenderName = x.Gender.Name,
                                                IconUrl = x.IconStorage.Path,
                                                CreateDate = x.CreateDate,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,

                                                RoleId = x.RoleId,
                                                RoleKey = x.Role.SystemName,
                                                RoleName = x.Role.Name,

                                                StoreReferenceId = x.SubOwner.Id,
                                                StoreReferenceKey = x.SubOwner.Guid,
                                                StoreDislayName = x.SubOwner.DisplayName,
                                                StoreAddress = x.SubOwner.Address,
                                                LastActivityDate = x.LastActivityDate
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantSubAccount && x.OwnerId == _Request.AccountId)
                                                .Select(x => new OAccounts.SubAccount.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CashierId = x.DisplayName,
                                                    Name = x.Name,
                                                    UserName = x.User.Username,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    GenderCode = x.Gender.SystemName,
                                                    GenderName = x.Gender.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    RoleId = x.RoleId,
                                                    RoleKey = x.Role.SystemName,
                                                    RoleName = x.Role.Name,

                                                    StoreReferenceId = x.SubOwner.Id,
                                                    StoreReferenceKey = x.SubOwner.Guid,
                                                    StoreDislayName = x.SubOwner.DisplayName,
                                                    StoreAddress = x.SubOwner.Address,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();

                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            if (!string.IsNullOrEmpty(_DataItem.IconUrl))
                            {
                                _DataItem.IconUrl = _AppConfig.StorageUrl + _DataItem.IconUrl;
                            }
                            else
                            {
                                _DataItem.IconUrl = _AppConfig.Default_Icon;
                            }

                            _ListDownload.Add(new OAccounts.SubAccount.ListDownload
                            {

                                Name = _DataItem.Name,
                                UserName = _DataItem.UserName,
                                EmailAddress = _DataItem.EmailAddress,
                                ContactNumber = _DataItem.ContactNumber,
                                GenderName = _DataItem.GenderName,
                                IconUrl = _DataItem.IconUrl,
                                AddedOn = _DataItem.CreateDate,
                                StatusName = _DataItem.StatusName,
                                RoleName = _DataItem.RoleName,
                                StoreDislayName = _DataItem.StoreDislayName,
                                StoreAddress = _DataItem.StoreAddress,
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("SubAccounts_List", _ListDownload, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetSubAccountsDownload", _Exception);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the customer list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomer(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetCustomerDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetCustomerDownload>("ActorGetCustomerDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                if (_Request.StartDate == null)
                {
                    _Request.StartDate = new DateTime(2015, 01, 01, 00, 00, 00);
                }
                if (_Request.EndDate == null)
                {
                    _Request.EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                }
                _Customers = new List<OAccounts.Customer.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    long IsTucPlusEnable = Convert.ToInt64(HCoreHelper.GetConfigurationValueByUserAccount("thankucashplus", _Request.AccountId, _Request.UserReference));
                    if (IsTucPlusEnable == 1)
                    {
                        if (_Request.Type == "new")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            CreateDate = x.CreateDate,
                                                            GenderCode = x.Gender.SystemName,
                                                            GenderName = x.Gender.Name,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            //TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                            //                                .Count(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success),
                                                            //TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success)
                                                            //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            //TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.ReferenceId
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success
                                                            //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                            //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                            //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                            //                                .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                            //TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                            //                                .Count(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.ReferenceId
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success
                                                            //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                            //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                            //                                 && m.SourceId == TransactionSource.ThankUCashPlus),
                                                            TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && ((m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                                                            TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                                                            TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                            TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                            TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),
                                                            TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                            RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                            RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                            RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                            TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                            LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                        })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           CreateDate = x.CreateDate,
                                                           GenderCode = x.Gender.SystemName,
                                                           GenderName = x.Gender.Name,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,
                                                           //TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Count(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                           //                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success),
                                                           //TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Where(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                           //                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success)
                                                           //                                 .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                           //TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Where(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.ReferenceId
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success
                                                           //                                 && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                           //                                 && m.ModeId == Helpers.TransactionMode.Credit
                                                           //                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                           //                                 .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                           //TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Count(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.ReferenceId
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success
                                                           //                                 && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                           //                                 && m.ModeId == Helpers.TransactionMode.Credit
                                                           //                                  && m.SourceId == TransactionSource.ThankUCashPlus),

                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                           TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                                                           TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                           TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                           TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),
                                                           TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                           TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                           RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                           TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                           LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Customers)
                            {
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                        else if (_Request.Type == "loyal")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            CreateDate = x.CreateDate,
                                                            GenderCode = x.Gender.SystemName,
                                                            GenderName = x.Gender.Name,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            //TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                            //                                .Count(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success),
                                                            //TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success)
                                                            //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            //TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.ReferenceId
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success
                                                            //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                            //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                            //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                            //                                .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                            //TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                            //                                .Count(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.ReferenceId
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success
                                                            //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                            //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                            //                                 && m.SourceId == TransactionSource.ThankUCashPlus),
                                                            TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                                                            TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                                                            TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                            TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                            TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),
                                                            TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                            RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                            RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                            RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                            TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                            LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                        })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           CreateDate = x.CreateDate,
                                                           GenderCode = x.Gender.SystemName,
                                                           GenderName = x.Gender.Name,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,
                                                           //TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Count(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                           //                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success),
                                                           //TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Where(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                           //                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success)
                                                           //                                 .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                           //TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Where(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.ReferenceId
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success
                                                           //                                 && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                           //                                 && m.ModeId == Helpers.TransactionMode.Credit
                                                           //                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                           //                                 .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                           //TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Count(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.ReferenceId
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success
                                                           //                                 && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                           //                                 && m.ModeId == Helpers.TransactionMode.Credit
                                                           //                                  && m.SourceId == TransactionSource.ThankUCashPlus),
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                           TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                                                           TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                           TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                           TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),
                                                           TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                           TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                           RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                           TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                           LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Customers)
                            {
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                        else if (_Request.Type == "lost")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                         && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate < _Request.StartDate)
                                                         && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            CreateDate = x.CreateDate,
                                                            GenderCode = x.Gender.SystemName,
                                                            GenderName = x.Gender.Name,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            //TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                            //                                .Count(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success),
                                                            //TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success)
                                                            //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            //TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.ReferenceId
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success
                                                            //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                            //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                            //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                            //                                .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                            //TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                            //                                .Count(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.ReferenceId
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success
                                                            //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                            //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                            //                                 && m.SourceId == TransactionSource.ThankUCashPlus),
                                                            TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                                                            TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                                                            TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                            TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                            TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),
                                                            TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                            RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                            RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                            RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                            TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                            LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                        })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                         && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate < _Request.StartDate)
                                                         && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            CreateDate = x.CreateDate,
                                                            GenderCode = x.Gender.SystemName,
                                                            GenderName = x.Gender.Name,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            //TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                            //                                .Count(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success),
                                                            //TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success)
                                                            //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            //TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.ReferenceId
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success
                                                            //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                            //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                            //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                            //                                .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                            //TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                            //                                .Count(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.ReferenceId
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success
                                                            //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                            //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                            //                                 && m.SourceId == TransactionSource.ThankUCashPlus),
                                                            TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                                                            TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                                                            TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                            TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                            TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),
                                                            TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                            RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                            RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                            RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                            TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                            LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                        })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Customers)
                            {
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                        else
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.OwnerId == _Request.AccountId || x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate)))
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            //LastTransactionDate = x.LastTransactionDate,
                                                            // LastTransactionDate = x.LastTransactionDate,
                                                            //TLastTransactionDate = x.HCUAccountTransactionAccount.Where(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).Select(d => d.TransactionDate).FirstOrDefault(),
                                                            CreateDate = x.CreateDate,
                                                            GenderCode = x.Gender.SystemName,
                                                            GenderName = x.Gender.Name,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            //TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                            //                                .Count(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success),
                                                            //TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success)
                                                            //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            //TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.ReferenceId
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success
                                                            //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                            //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                            //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                            //                                .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                            //TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                            //                                .Count(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.ReferenceId
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success
                                                            //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                            //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                            //                                 && m.SourceId == TransactionSource.ThankUCashPlus),

                                                            TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                                                            TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                                                            TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                            TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                            TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),
                                                            TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                            RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                            RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                            RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                            TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),

                                                            LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                        })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.OwnerId == _Request.AccountId || x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate)))
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           //LastTransactionDate = x.LastTransactionDate,
                                                           //TLastTransactionDate = x.HCUAccountTransactionAccount.Where(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).Select(d => d.TransactionDate).FirstOrDefault(),
                                                           CreateDate = x.CreateDate,
                                                           GenderCode = x.Gender.SystemName,
                                                           GenderName = x.Gender.Name,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,
                                                           //TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Count(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)))
                                                           //                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success),
                                                           //TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Where(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)))
                                                           //                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success)
                                                           //                                 .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                           //TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Where(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.ReferenceId
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success
                                                           //                                 && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                           //                                 && m.ModeId == Helpers.TransactionMode.Credit
                                                           //                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                           //                                 .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                           //TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Count(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.ReferenceId
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success
                                                           //                                 && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                           //                                 && m.ModeId == Helpers.TransactionMode.Credit
                                                           //                                  && m.SourceId == TransactionSource.ThankUCashPlus),

                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                           TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                                                           TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                           TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                           TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),
                                                           TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                           TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                           RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                           TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                         && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                           LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Customers)
                            {
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                    }
                    else
                    {
                        if (_Request.Type == "new")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            CreateDate = x.CreateDate,
                                                            GenderCode = x.Gender.SystemName,
                                                            GenderName = x.Gender.Name,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                            TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                            TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),

                                                            TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                            RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                            RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                            RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                            LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),

                                                        })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           CreateDate = x.CreateDate,
                                                           GenderCode = x.Gender.SystemName,
                                                           GenderName = x.Gender.Name,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,
                                                           //TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Count(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                           //                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success),

                                                           //TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Where(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                           //                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success)
                                                           //                                 .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                           //TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Where(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success
                                                           //                                 && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                           //                                 && m.ModeId == Helpers.TransactionMode.Credit
                                                           //                                 && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                           //                                 &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                           //                                 .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                           //RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Where(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success
                                                           //                                 && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                           //                                 && m.ModeId == Helpers.TransactionMode.Debit
                                                           //                                 &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                           //                                 .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                           TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),

                                                           TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                           TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                           RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                           RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                           LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            //foreach (var DataItem in _Customers)
                            //{
                            //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            //    {
                            //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            //    }
                            //    else
                            //    {
                            //        DataItem.IconUrl = _AppConfig.Default_Icon;
                            //    }
                            //}
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                        else if (_Request.Type == "loyal")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            CreateDate = x.CreateDate,
                                                            GenderCode = x.Gender.SystemName,
                                                            GenderName = x.Gender.Name,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            //TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                            //                                .Count(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success),

                                                            //TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success)
                                                            //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            //TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success
                                                            //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                            //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                            //                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                            //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                            //RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success
                                                            //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                            //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                            //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                            TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                            TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                            TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),

                                                            TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                            RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                            RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                            RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                            LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                        })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           CreateDate = x.CreateDate,
                                                           GenderCode = x.Gender.SystemName,
                                                           GenderName = x.Gender.Name,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,
                                                           //TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Count(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                           //                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success),

                                                           //TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Where(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                           //                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success)
                                                           //                                 .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                           //TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Where(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success
                                                           //                                 && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                           //                                 && m.ModeId == Helpers.TransactionMode.Credit
                                                           //                                 && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                           //                                 &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                           //                                 .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                           //RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Where(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success
                                                           //                                 && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                           //                                 && m.ModeId == Helpers.TransactionMode.Debit
                                                           //                                 &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                           //                                 .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                           TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),

                                                           TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                           TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                           RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                           RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                           LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            //foreach (var DataItem in _Customers)
                            //{
                            //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            //    {
                            //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            //    }
                            //    else
                            //    {
                            //        DataItem.IconUrl = _AppConfig.Default_Icon;
                            //    }
                            //}
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                        else if (_Request.Type == "lost")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate < _Request.StartDate)
                                                         && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)

                                                         .Select(x => new OAccounts.Customer.List
                                                         {
                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,
                                                             DisplayName = x.DisplayName,
                                                             Name = x.Name,
                                                             FirstName = x.FirstName,
                                                             LastName = x.LastName,
                                                             EmailAddress = x.EmailAddress,
                                                             MobileNumber = x.MobileNumber,
                                                             CreateDate = x.CreateDate,
                                                             GenderCode = x.Gender.SystemName,
                                                             GenderName = x.Gender.Name,
                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name,
                                                             //TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                             //                               .Count(m => m.AccountId == x.Id
                                                             //                               && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                             //                               && m.ParentId == _Request.AccountId
                                                             //                               && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                             //                               || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                             //                               && m.StatusId == HelperStatus.Transaction.Success),

                                                             //TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                             //                               .Where(m => m.AccountId == x.Id
                                                             //                               && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                             //                               && m.ParentId == _Request.AccountId
                                                             //                               && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                             //                               || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                             //                               && m.StatusId == HelperStatus.Transaction.Success)
                                                             //                               .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                             //TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                             //                               .Where(m => m.AccountId == x.Id
                                                             //                               && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                             //                               && m.ParentId == _Request.AccountId
                                                             //                               && m.StatusId == HelperStatus.Transaction.Success
                                                             //                               && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                             //                               && m.ModeId == Helpers.TransactionMode.Credit
                                                             //                               && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                             //                               &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                             //                               .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                             //RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                             //                               .Where(m => m.AccountId == x.Id
                                                             //                               && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                             //                               && m.ParentId == _Request.AccountId
                                                             //                               && m.StatusId == HelperStatus.Transaction.Success
                                                             //                               && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                             //                               && m.ModeId == Helpers.TransactionMode.Debit
                                                             //                               &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                             //                               .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                             TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                             TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                             TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),

                                                             TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                             TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                             RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                             RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                             RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                             LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                         })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                    && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate < _Request.StartDate)
                                                    && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            CreateDate = x.CreateDate,
                                                            GenderCode = x.Gender.SystemName,
                                                            GenderName = x.Gender.Name,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            //TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                            //                                .Count(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success),

                                                            //TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success)
                                                            //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                            //TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success
                                                            //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                            //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                            //                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                            //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                            //RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success
                                                            //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                            //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                            //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                            TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                            TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                            TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),

                                                            TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                            RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                            RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                            RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                            LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                        })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Customers)
                            {
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                        else
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.OwnerId == _Request.AccountId || x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate)))
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            //LastTransactionDate = x.LastTransactionDate,
                                                            //TLastTransactionDate = x.HCUAccountTransactionAccount.Where(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).OrderByDescending(d => d.TransactionDate).Select(d => d.TransactionDate).FirstOrDefault(),
                                                            CreateDate = x.CreateDate,
                                                            GenderCode = x.Gender.SystemName,
                                                            GenderName = x.Gender.Name,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            //TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                            //                                .Count(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success),

                                                            //TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success)
                                                            //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            //TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success
                                                            //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                            //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                            //                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                            //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                            //RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                            //                                .Where(m => m.AccountId == x.Id
                                                            //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                            //                                && m.ParentId == _Request.AccountId
                                                            //                                && m.StatusId == HelperStatus.Transaction.Success
                                                            //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                            //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                            //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                            //                                .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                            TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                            TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                            TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),

                                                            TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                            RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                            RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                            RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                            LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                        })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && (x.OwnerId == _Request.AccountId || x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate)))
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           //TLastTransactionDate = x.HCUAccountTransactionAccount.Where(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).Select(d => d.TransactionDate).FirstOrDefault(),
                                                           CreateDate = x.CreateDate,
                                                           GenderCode = x.Gender.SystemName,
                                                           GenderName = x.Gender.Name,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,
                                                           //TLastTransactionDate = x.HCUAccountTransactionAccount.Where(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).OrderByDescending(d => d.TransactionDate).Select(d => d.TransactionDate).FirstOrDefault(),
                                                           //TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Count(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                           //                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success),

                                                           //TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Where(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                           //                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success)
                                                           //                                 .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                           //TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Where(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success
                                                           //                                 && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                           //                                 && m.ModeId == Helpers.TransactionMode.Credit
                                                           //                                 && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                           //                                 &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                           //                                 .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                           //RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                           //                                 .Where(m => m.AccountId == x.Id
                                                           //                                 && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                           //                                 && m.ParentId == _Request.AccountId
                                                           //                                 && m.StatusId == HelperStatus.Transaction.Success
                                                           //                                 && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                           //                                 && m.ModeId == Helpers.TransactionMode.Debit
                                                           //                                 &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                           //                                 .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),

                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                           TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),

                                                           TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                           TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                           RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                           RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                           LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.ParentId == _Request.AccountId
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            //foreach (var DataItem in _Customers)
                            //{
                            //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            //    {
                            //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            //    }
                            //    else
                            //    {
                            //        DataItem.IconUrl = _AppConfig.Default_Icon;
                            //    }
                            //}
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCustomers", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the customer list download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetCustomerDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.StartDate == null)
                {
                    _Request.StartDate = new DateTime(2015, 01, 01, 00, 00, 00);
                }
                if (_Request.EndDate == null)
                {
                    _Request.EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                }
                _Request.Limit = 800;
                _Customers = new List<OAccounts.Customer.List>();
                long StorageId = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                    _HCUAccountParameter.Name = "Customers_Sheet";
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Inactive;
                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    _HCoreContext.SaveChanges();
                    StorageId = _HCUAccountParameter.Id;
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    long IsTucPlusEnable = Convert.ToInt64(HCoreHelper.GetConfigurationValueByUserAccount("thankucashplus", _Request.ReferenceId, _Request.UserReference));
                    if (IsTucPlusEnable == 1)
                    {
                        if (_Request.Type == "new")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success),
                                                            TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.ReferenceId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus),
                                                        })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           LastTransactionDate = x.LastTransactionDate,
                                                           CreateDate = x.CreateDate,
                                                           StatusId = x.StatusId,
                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Customers)
                            {
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion

                        }
                        else if (_Request.Type == "loyal")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success),
                                                            TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.ReferenceId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus),
                                                        })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           LastTransactionDate = x.LastTransactionDate,
                                                           CreateDate = x.CreateDate,
                                                           StatusId = x.StatusId,
                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Customers)
                            {
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion

                        }
                        else if (_Request.Type == "lost")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                         && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate < _Request.StartDate)
                                                         && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success),
                                                            TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.ReferenceId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus),
                                                        })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                         && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate < _Request.StartDate)
                                                         && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                        })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Customers)
                            {
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion

                        }
                        else
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            // LastTransactionDate = x.LastTransactionDate,
                                                            //TLastTransactionDate = x.HCUAccountTransactionAccount.Where(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).Select(d => d.TransactionDate).FirstOrDefault(),
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success),
                                                            TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.ReferenceId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus),
                                                        })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                     && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           LastTransactionDate = x.LastTransactionDate,
                                                           //TLastTransactionDate = x.HCUAccountTransactionAccount.Where(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).Select(d => d.TransactionDate).FirstOrDefault(),
                                                           CreateDate = x.CreateDate,
                                                           StatusId = x.StatusId,
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success),
                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                           TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.ReferenceId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus),

                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Customers)
                            {
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion

                        }

                    }
                    else
                    {
                        if (_Request.Type == "new")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success),

                                                            TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                            RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                        })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           LastTransactionDate = x.LastTransactionDate,
                                                           CreateDate = x.CreateDate,
                                                           StatusId = x.StatusId,
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success),

                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                           TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            //foreach (var DataItem in _Customers)
                            //{
                            //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            //    {
                            //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            //    }
                            //    else
                            //    {
                            //        DataItem.IconUrl = _AppConfig.Default_Icon;
                            //    }
                            //}
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion

                        }
                        else if (_Request.Type == "loyal")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success),

                                                            TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                            RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                        })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           LastTransactionDate = x.LastTransactionDate,
                                                           CreateDate = x.CreateDate,
                                                           StatusId = x.StatusId,
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success),

                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                           TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            //foreach (var DataItem in _Customers)
                            //{
                            //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            //    {
                            //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            //    }
                            //    else
                            //    {
                            //        DataItem.IconUrl = _AppConfig.Default_Icon;
                            //    }
                            //}
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion

                        }
                        else if (_Request.Type == "lost")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate < _Request.StartDate)
                                                         && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)

                                                         .Select(x => new OAccounts.Customer.List
                                                         {
                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,
                                                             DisplayName = x.DisplayName,
                                                             Name = x.Name,
                                                             FirstName = x.FirstName,
                                                             LastName = x.LastName,
                                                             EmailAddress = x.EmailAddress,
                                                             MobileNumber = x.MobileNumber,
                                                             LastTransactionDate = x.LastTransactionDate,
                                                             CreateDate = x.CreateDate,
                                                             StatusId = x.StatusId,
                                                             TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success),

                                                             TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                             TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                             RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                         })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                    && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate < _Request.StartDate)
                                                    && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success),

                                                            TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                            TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                            RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                        })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Customers)
                            {
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion

                        }
                        else
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                        && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                        .Select(x => new OAccounts.Customer.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            EmailAddress = x.EmailAddress,
                                                            MobileNumber = x.MobileNumber,
                                                            //LastTransactionDate = x.LastTransactionDate,
                                                            //TLastTransactionDate = x.HCUAccountTransactionAccount.Where(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).OrderByDescending(d => d.TransactionDate).Select(d => d.TransactionDate).FirstOrDefault(),
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success),

                                                            TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                            TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                            RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                        })
                                                        .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Data
                            _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                     && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                       .Select(x => new OAccounts.Customer.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           FirstName = x.FirstName,
                                                           LastName = x.LastName,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           //TLastTransactionDate = x.HCUAccountTransactionAccount.Where(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).Select(d => d.TransactionDate).FirstOrDefault(),
                                                           CreateDate = x.CreateDate,
                                                           StatusId = x.StatusId,
                                                           //TLastTransactionDate = x.HCUAccountTransactionAccount.Where(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).OrderByDescending(d => d.TransactionDate).Select(d => d.TransactionDate).FirstOrDefault(),
                                                           TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                            .Count(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success),

                                                           TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                           TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                            && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,
                                                           RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                            && m.ParentId == _Request.AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                       })
                                                    .Where(_Request.SearchCondition)
                                                    .OrderBy(_Request.SortExpression)
                                                    .Skip(_Request.Offset)
                                                    .Take(_Request.Limit)
                                                    .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            //foreach (var DataItem in _Customers)
                            //{
                            //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            //    {
                            //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            //    }
                            //    else
                            //    {
                            //        DataItem.IconUrl = _AppConfig.Default_Icon;
                            //    }
                            //}
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                            #endregion

                        }

                    }
                    if (_Customers.Count > 0)
                    {
                        using (var _XLWorkbook = new ClosedXML.Excel.XLWorkbook())
                        {
                            var _WorkSheet = _XLWorkbook.Worksheets.Add("Rewards_Sheet");
                            PropertyInfo[] properties = _Customers.First().GetType().GetProperties();
                            List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                            for (int i = 0; i < headerNames.Count; i++)
                            {
                                _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                            }
                            _WorkSheet.Cell(2, 1).InsertData(_Customers);
                            MemoryStream _MemoryStream = new MemoryStream();
                            _XLWorkbook.SaveAs(_MemoryStream);
                            long? _StorageId = HCoreHelper.SaveStorage("Customers_Sheet", "xlsx", _MemoryStream, _Request.UserReference);
                            if (_StorageId != null && _StorageId != 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                                    if (TItem != null)
                                    {
                                        TItem.IconStorageId = _StorageId;
                                        TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        TItem.ModifyById = _Request.UserReference.AccountId;
                                        TItem.StatusId = HelperStatus.Default.Active;
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCustomerDownload", _Exception, _Request.UserReference);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the customer details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomer(OAccounts.Customer.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                        .Where(x => x.Guid == _Request.AccountKey && x.Id == _Request.AccountId && x.AccountTypeId == UserAccountType.Appuser)
                        .Select(x => new OAccounts.Customer.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            Name = x.Name,
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            DisplayName = x.DisplayName,
                            EmailAddress = x.EmailAddress,
                            MobileNumber = x.MobileNumber,
                            DateOfBirth = x.DateOfBirth,
                            GenderName = x.Gender.Name,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                        }).FirstOrDefault();
                    if (Details != null)
                    {
                        Details.FirstTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == _Request.AccountId && x.ParentId == _Request.MerchantId).Select(x => x.TransactionDate).FirstOrDefault();
                        Details.LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == _Request.AccountId && x.ParentId == _Request.MerchantId).OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomer", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the customer overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomerOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _CustomerDetails = new OAccounts.Customer.Overview();
                if (_Request.StartDate == null)
                {
                    _Request.StartDate = new DateTime(2015, 01, 01, 00, 00, 00);
                }
                if (_Request.EndDate == null)
                {
                    _Request.EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    long IsTucPlusEnable = Convert.ToInt64(HCoreHelper.GetConfigurationValueByUserAccount("thankucashplus", _Request.ReferenceId, _Request.UserReference));
                    if (IsTucPlusEnable == 1)
                    {

                        #region Total Records
                        _CustomerDetails.Total = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                .Select(x => new OAccounts.Customer.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    FirstName = x.FirstName,
                                                    LastName = x.LastName,
                                                    EmailAddress = x.EmailAddress,
                                                    MobileNumber = x.MobileNumber,
                                                    //GenderId = x.GenderId,
                                                    //DateOfBirth = x.DateOfBirth,
                                                    //IconUrl = x.IconStorage.Path,
                                                    //OwnerId = x.OwnerId,
                                                    //OwnerKey = x.Owner.Guid,
                                                    //OwnerDisplayName = x.Owner.DisplayName,
                                                    //OwnerIconUrl = x.Owner.IconStorage.Path,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    //LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault(),
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    //ApplicationStatusId = x.ApplicationStatusId,
                                                    //AccountLevelId = x.AccountLevel,
                                                    TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                    .Count(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                    && m.StatusId == HelperStatus.Transaction.Success),
                                                    TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                    .Count(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.ReferenceId
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),
                                                    //TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    //TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                    //TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                    //TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                    //                                .Count(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus),
                                                    //TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    //TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                    //RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                    //                                .Count(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                    //RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    //RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                    //TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                 && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                })
                                                .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                        #region New Records
                        _CustomerDetails.New = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                .Select(x => new OAccounts.Customer.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    FirstName = x.FirstName,
                                                    LastName = x.LastName,
                                                    EmailAddress = x.EmailAddress,
                                                    MobileNumber = x.MobileNumber,
                                                    //GenderId = x.GenderId,
                                                    //DateOfBirth = x.DateOfBirth,
                                                    //IconUrl = x.IconStorage.Path,
                                                    //OwnerId = x.OwnerId,
                                                    //OwnerKey = x.Owner.Guid,
                                                    //OwnerDisplayName = x.Owner.DisplayName,
                                                    //OwnerIconUrl = x.Owner.IconStorage.Path,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    //LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault(),
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    //ApplicationStatusId = x.ApplicationStatusId,
                                                    //AccountLevelId = x.AccountLevel,
                                                    TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                    .Count(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                    && m.StatusId == HelperStatus.Transaction.Success),
                                                    TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                    .Count(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.ReferenceId
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),
                                                    //TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    //TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                    //TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                    //TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                    //                                .Count(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus),
                                                    //TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    //TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                    //RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                    //                                .Count(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                    //RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    //RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                    //TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                 && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                })
                                                .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                        #region Repeating Records
                        _CustomerDetails.New = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                .Select(x => new OAccounts.Customer.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    FirstName = x.FirstName,
                                                    LastName = x.LastName,
                                                    EmailAddress = x.EmailAddress,
                                                    MobileNumber = x.MobileNumber,
                                                    //GenderId = x.GenderId,
                                                    //DateOfBirth = x.DateOfBirth,
                                                    //IconUrl = x.IconStorage.Path,
                                                    //OwnerId = x.OwnerId,
                                                    //OwnerKey = x.Owner.Guid,
                                                    //OwnerDisplayName = x.Owner.DisplayName,
                                                    //OwnerIconUrl = x.Owner.IconStorage.Path,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    //LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault(),
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    //ApplicationStatusId = x.ApplicationStatusId,
                                                    //AccountLevelId = x.AccountLevel,
                                                    TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                    .Count(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                    && m.StatusId == HelperStatus.Transaction.Success),
                                                    TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                    .Count(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.ReferenceId
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),
                                                    //TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    //TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                    //TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                    //TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                    //                                .Count(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus),
                                                    //TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    //TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                    //RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                    //                                .Count(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                    //RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    //RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                    //TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                 && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                })
                                                .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                        #region Lost Records
                        _CustomerDetails.New = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                  && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate < _Request.StartDate)
                                                  && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                .Select(x => new OAccounts.Customer.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    FirstName = x.FirstName,
                                                    LastName = x.LastName,
                                                    EmailAddress = x.EmailAddress,
                                                    MobileNumber = x.MobileNumber,
                                                    //GenderId = x.GenderId,
                                                    //DateOfBirth = x.DateOfBirth,
                                                    //IconUrl = x.IconStorage.Path,
                                                    //OwnerId = x.OwnerId,
                                                    //OwnerKey = x.Owner.Guid,
                                                    //OwnerDisplayName = x.Owner.DisplayName,
                                                    //OwnerIconUrl = x.Owner.IconStorage.Path,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    //LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault(),
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    //ApplicationStatusId = x.ApplicationStatusId,
                                                    //AccountLevelId = x.AccountLevel,
                                                    TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                    .Count(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                    && m.StatusId == HelperStatus.Transaction.Success),
                                                    TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                    .Count(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.ReferenceId
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                     && m.SourceId == TransactionSource.ThankUCashPlus),
                                                    //TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    //TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                    //TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                 && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                    //TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                    //                                .Count(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus),
                                                    //TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    //TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                    //RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                    //                                .Count(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                    //RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    //RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                    //TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                 && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                && m.SourceId == TransactionSource.ThankUCashPlus)
                                                    //                                .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                })
                                                .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerDetails, "HC0001");
                        #endregion
                    }
                    else
                    {
                        #region Total Records
                        _CustomerDetails.Total = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate))
                                                .Select(x => new OAccounts.Customer.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    FirstName = x.FirstName,
                                                    LastName = x.LastName,
                                                    EmailAddress = x.EmailAddress,
                                                    MobileNumber = x.MobileNumber,
                                                    //GenderId = x.GenderId,
                                                    //DateOfBirth = x.DateOfBirth,
                                                    //IconUrl = x.IconStorage.Path,
                                                    //OwnerId = x.OwnerId,
                                                    //OwnerKey = x.Owner.Guid,
                                                    //OwnerDisplayName = x.Owner.DisplayName,
                                                    //OwnerIconUrl = x.Owner.IconStorage.Path,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    //LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault(),
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    //ApplicationStatusId = x.ApplicationStatusId,
                                                    //AccountLevelId = x.AccountLevel,
                                                    TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                    .Count(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                    TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    //TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                    //                                .Count(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),

                                                    //TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                    //RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                    //                                .Count(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                    //RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                })
                                                .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                        #region New Records
                        _CustomerDetails.New = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 1)
                                                .Select(x => new OAccounts.Customer.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    FirstName = x.FirstName,
                                                    LastName = x.LastName,
                                                    EmailAddress = x.EmailAddress,
                                                    MobileNumber = x.MobileNumber,
                                                    //GenderId = x.GenderId,
                                                    //DateOfBirth = x.DateOfBirth,
                                                    //IconUrl = x.IconStorage.Path,
                                                    //OwnerId = x.OwnerId,
                                                    //OwnerKey = x.Owner.Guid,
                                                    //OwnerDisplayName = x.Owner.DisplayName,
                                                    //OwnerIconUrl = x.Owner.IconStorage.Path,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    //LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault(),
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    //ApplicationStatusId = x.ApplicationStatusId,
                                                    //AccountLevelId = x.AccountLevel,
                                                    TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                    .Count(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                    TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    //TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                    //                                .Count(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),

                                                    //TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                    //RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                    //                                .Count(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                    //RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                })
                                                .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                        #region Repeating Records
                        _CustomerDetails.Repeating = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) > 1)
                                                .Select(x => new OAccounts.Customer.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    FirstName = x.FirstName,
                                                    LastName = x.LastName,
                                                    EmailAddress = x.EmailAddress,
                                                    MobileNumber = x.MobileNumber,
                                                    //GenderId = x.GenderId,
                                                    //DateOfBirth = x.DateOfBirth,
                                                    //IconUrl = x.IconStorage.Path,
                                                    //OwnerId = x.OwnerId,
                                                    //OwnerKey = x.Owner.Guid,
                                                    //OwnerDisplayName = x.Owner.DisplayName,
                                                    //OwnerIconUrl = x.Owner.IconStorage.Path,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    //LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault(),
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    //ApplicationStatusId = x.ApplicationStatusId,
                                                    //AccountLevelId = x.AccountLevel,
                                                    TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                    .Count(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                    TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    //TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                    //                                .Count(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),

                                                    //TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                    //RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                    //                                .Count(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                    //RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                })
                                                .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                        #region Lost Records
                        _CustomerDetails.Lost = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                && x.HCUAccountTransactionAccount.Any(m => m.ParentId == _Request.AccountId && m.TransactionDate < _Request.StartDate)
                                                && x.HCUAccountTransactionAccount.Count(m => m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate) == 0)
                                                .Select(x => new OAccounts.Customer.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    FirstName = x.FirstName,
                                                    LastName = x.LastName,
                                                    EmailAddress = x.EmailAddress,
                                                    MobileNumber = x.MobileNumber,
                                                    //GenderId = x.GenderId,
                                                    //DateOfBirth = x.DateOfBirth,
                                                    //IconUrl = x.IconStorage.Path,
                                                    //OwnerId = x.OwnerId,
                                                    //OwnerKey = x.Owner.Guid,
                                                    //OwnerDisplayName = x.Owner.DisplayName,
                                                    //OwnerIconUrl = x.Owner.IconStorage.Path,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    //LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate).OrderByDescending(m => m.TransactionDate).Select(m => m.TransactionDate).FirstOrDefault(),
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    //ApplicationStatusId = x.ApplicationStatusId,
                                                    //AccountLevelId = x.AccountLevel,
                                                    TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                    .Count(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                    && m.StatusId == HelperStatus.Transaction.Success),

                                                    TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                    && m.StatusId == HelperStatus.Transaction.Success)
                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    //TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                    //                                .Count(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),

                                                    //TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                    //                                && m.ModeId == Helpers.TransactionMode.Credit
                                                    //                                && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                    && ((m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) || m.SourceId == TransactionSource.TUCBlack))
                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                    //RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                    //                                .Count(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)),


                                                    //RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                    //                                .Where(m => m.AccountId == x.Id
                                                    //                                && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                    //                                && m.ParentId == _Request.ReferenceId
                                                    //                                && m.StatusId == HelperStatus.Transaction.Success
                                                    //                                && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                    //                                && m.ModeId == Helpers.TransactionMode.Debit
                                                    //                                &&  (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                    //                                .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                    .Where(m => m.AccountId == x.Id
                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                    && m.ParentId == _Request.AccountId
                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                                                                                    && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0,

                                                })
                                                .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        //foreach (var DataItem in _Customers)
                        //{
                        //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        //    {
                        //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        //    }
                        //    else
                        //    {
                        //        DataItem.IconUrl = _AppConfig.Default_Icon;
                        //    }
                        //}
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerDetails, "HC0001");
                        #endregion
                    }
                    //if (_Request.RefreshCount)
                    //{
                    //    #region Total Records
                    //    _Request.TotalRecords = _HCoreContext.HCUAccount
                    //                            .Where(x => x.AccountTypeId == UserAccountType.MerchantSubAccount && x.OwnerId == _Request.AccountId)
                    //                            .Select(x => new OAccounts.SubAccount.List
                    //                            {
                    //                                ReferenceId = x.Id,
                    //                                ReferenceKey = x.Guid,
                    //                                CashierId = x.DisplayName,
                    //                                Name = x.Name,
                    //                                EmailAddress = x.EmailAddress,
                    //                                ContactNumber = x.ContactNumber,
                    //                                GenderCode = x.Gender.SystemName,
                    //                                GenderName = x.Gender.Name,
                    //                                IconUrl = x.IconStorage.Path,
                    //                                StatusCode = x.Status.SystemName,
                    //                                StatusName = x.Status.Name,
                    //                                CreateDate = x.CreateDate,
                    //                            })
                    //                           .Where(_Request.SearchCondition)
                    //                   .Count();
                    //    #endregion
                    //}
                    //#region Get Data
                    //_SubAccounts = _HCoreContext.HCUAccount
                    //                            .Where(x => x.AccountTypeId == UserAccountType.MerchantSubAccount && x.OwnerId == _Request.AccountId)
                    //                            .Select(x => new OAccounts.SubAccount.List
                    //                            {
                    //                                ReferenceId = x.Id,
                    //                                ReferenceKey = x.Guid,
                    //                                CashierId = x.DisplayName,
                    //                                Name = x.Name,
                    //                                EmailAddress = x.EmailAddress,
                    //                                ContactNumber = x.ContactNumber,
                    //                                GenderCode = x.Gender.SystemName,
                    //                                GenderName = x.Gender.Name,
                    //                                IconUrl = x.IconStorage.Path,
                    //                                CreateDate = x.CreateDate,
                    //                                StatusCode = x.Status.SystemName,
                    //                                StatusName = x.Status.Name,
                    //                            })
                    //                         .Where(_Request.SearchCondition)
                    //                         .OrderBy(_Request.SortExpression)
                    //                         .Skip(_Request.Offset)
                    //                         .Take(_Request.Limit)
                    //                         .ToList();
                    //#endregion
                    //#region Create  Response Object
                    //_HCoreContext.Dispose();
                    //foreach (var DataItem in _SubAccounts)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //    {
                    //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.IconUrl = _AppConfig.Default_Icon;
                    //    }
                    //}
                    //OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _SubAccounts, _Request.Offset, _Request.Limit);
                    //#endregion
                    //#region Send Response
                    //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    //#endregion
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetSubAccounts", _Exception, _Request.UserReference, _CustomerDetails, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the store managers list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStoreManagers(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Stores = new List<OAccounts.Store.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantSubAccount
                                                    && x.OwnerId == _Request.AccountId
                                                    && x.StatusId == HelperStatus.Default.Active
                                                    && x.Role.Name == "Store Manager")
                                                .Select(x => new OAccounts.Store.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    CreateDate = x.CreateDate
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Stores = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantSubAccount
                                                    && x.OwnerId == _Request.AccountId
                                                    && x.StatusId == HelperStatus.Default.Active
                                                    && x.Role.Name == "Store Manager")
                                                .Select(x => new OAccounts.Store.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    CreateDate = x.CreateDate,
                                                    RoleName = x.Role != null ? x.Role.Name : ""
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(0)
                                             .Take(100)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Stores, 0, 100);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetStoreManagers", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        internal ResponseDto<CustomerAccountDetails> SaveMerchantBankAccount(CustomerAccountDetails request)
        {
            try
            {
                if (string.IsNullOrEmpty(request.AccountNumber))
                {
                    return ResponseDto<CustomerAccountDetails>.Fail("Account number must not be empty", (int)HttpStatusCode.BadRequest);
                }
                else if (string.IsNullOrEmpty(request.AccountName))
                {
                    return ResponseDto<CustomerAccountDetails>.Fail("Account name must not be empty", (int)HttpStatusCode.BadRequest);
                }
                else if (string.IsNullOrEmpty(request.BankName))
                {
                    return ResponseDto<CustomerAccountDetails>.Fail("Bank name must not be empty", (int)HttpStatusCode.BadRequest);
                }
                else
                {
                    using (HCoreContext context = new HCoreContext())
                    {
                        var AccountDetails = context.HCUAccount.Where(x => x.Id == request.UserId).FirstOrDefault();
                        if (AccountDetails == null)
                        {
                            return ResponseDto<CustomerAccountDetails>.Fail("Account not found", (int)HttpStatusCode.NotFound);
                        }
                        else
                        {
                            //var accountDetails = new HCUBankDetails()
                            //{
                            //    BankName = request.BankName,
                            //    AccountName = request.AccountName,
                            //    AccountNumber = request.AccountNumber,
                            //    UserId = request.UserId
                            //};
                            HCUAccountBank hcubank = new HCUAccountBank()
                            {
                                Guid = HCoreHelper.GenerateGuid(),
                                AccountId = request.UserId,
                                Name = request.AccountName,
                                BankName = request.BankName,
                                AccountNumber = request.AccountNumber,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                CreatedById = request.UserId,
                                StatusId = HelperStatus.Default.Active,
                                BankCode = request.BankCode,
                            };
                            context.HCUAccountBank.Add(hcubank);
                            context.SaveChanges();
                            return ResponseDto<CustomerAccountDetails>.Success("Saved", request, (int)HttpStatusCode.Created);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return ResponseDto<CustomerAccountDetails>.Fail("Internal server error", (int)HttpStatusCode.InternalServerError);
            }
        }
    }

    internal class ActorGetCustomerDownload : ReceiveActor
    {
        public ActorGetCustomerDownload()
        {
            FrameworkAccounts _FrameworkAccounts;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkAccounts = new FrameworkAccounts();
                _FrameworkAccounts.GetCustomerDownload(_Request);
            });
        }
    }
    internal class ActorGetSubAccountsDownload : ReceiveActor
    {
        public ActorGetSubAccountsDownload()
        {
            FrameworkAccounts _FrameworkAccounts;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkAccounts = new FrameworkAccounts();
                _FrameworkAccounts.GetSubAccountsDownload(_Request);
            });
        }
    }
}
