//==================================================================================
// FileName: FrameworkOnboardingV2.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Akka.Actor;
using CoralPay.HttpServiceHandler.Models;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Operations;
using HCore.Data.Operations.Models;
using HCore.Helper;
using HCore.Integration.Mailerlite;
using HCore.Integration.Mailerlite.Requests;
using HCore.Integration.Mailerlite.Responses;
using HCore.Integration.Paystack;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Core.Framework.Merchant.Actor;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Resource;
using Microsoft.AspNetCore.Http;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.TUC.Core.Helper.HelperEmailTemplate;
using static HCore.TUC.Core.Object.Merchant.OOnboardingV2;

namespace HCore.TUC.Core.Framework.Merchant
{
    public class FrameworkOnboardingV2
    {
        List<TUCCategoryAccount> _TUCCategoryAccounts;
        Random _Random;
        List<HCUAccountParameter> _HCUAccountParameters;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        HCUAccountParameter _HCUAccountParameter;
        HCoreContext _HCoreContext;
        HCoreContextOperations _HCoreContextOperations;
        HCTMerchantOnboarding _HCTMerchantOnboarding;
        OCoreVerificationManager.Request _VerificationRequest;
        ManageCoreVerification _ManageCoreVerification;
        OCoreVerificationManager.RequestVerify _VerifyRequest;
        HCUAccountSubscription _HCUAccountSubscription;
        HCUAccountBank _HCUAccountBank;
        OOnboardingV2.MerchantDetails _MerchantDetails;
        OAddress _OAddress;
        TUCCategoryAccount _TUCCategoryAccount;
        HCore.TUC.Core.Framework.Operations.FrameworkSubscription _FrameworkSubscription;
        OOnboardingV2.BankDetails BankDetails;

        internal OResponse OnboardMerchant_Request(OOnboardingV2.Profile.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.businessName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg000", "Business name required");
                }
                if (string.IsNullOrEmpty(_Request.businessEmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg001", "Business email required");
                }
                if (string.IsNullOrEmpty(_Request.name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg002", "Name required");
                }
                if (string.IsNullOrEmpty(_Request.source))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg002", "Registration source required");
                }
                if (string.IsNullOrEmpty(_Request.countryIsd))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg002", "Country required");
                }
                if (string.IsNullOrEmpty(_Request.mobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg002", "Mobile number required");
                }
                if (HCoreHelper.ValidateEmailAddress(_Request.businessEmailAddress) == false)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg003", "Email address required");
                }
                if (string.IsNullOrEmpty(_Request.password))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg004", "Password required");
                }
                if (_Request.password.Length < 6)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg005", "Password must be atleast 6 character long");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var UserNameCheck = _HCoreContext.HCUAccountAuth.Any(x => x.Username == _Request.businessEmailAddress);
                    if (UserNameCheck)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg005", "Email address already used");
                    }
                    var countryDetails = _HCoreContext.HCCoreCountry
                        .Where(x => x.Isd == _Request.countryIsd)
                        .Select(x => new
                        {
                            x.Id,
                            x.MobileNumberLength,
                            x.Isd,
                            x.StatusId
                        })
                        .FirstOrDefault();
                    if (countryDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg005", "Invalid country code");
                    }

                    var MobileCheck = HCoreHelper.ValidateMobileNumber(_Request.countryIsd + _Request.mobileNumber);
                    if (MobileCheck.IsNumberValid == false)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg005", "Invalid mobile number");
                    }
                    _HCoreContext.Dispose();

                    using (_HCoreContextOperations = new HCoreContextOperations())
                    {
                        var CheckOnboarding = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.EmailAddress == _Request.businessEmailAddress).FirstOrDefault();
                        if (CheckOnboarding != null)
                        {
                            _HCoreContextOperations.HCTMerchantOnboarding.Remove(CheckOnboarding);
                            _HCoreContextOperations.SaveChanges();
                        }
                        _HCoreContextOperations.Dispose();
                    }

                    using (_HCoreContextOperations = new HCoreContextOperations())
                    {
                        var CheckOnboarding = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.EmailAddress == _Request.businessEmailAddress).FirstOrDefault();
                        if (CheckOnboarding != null)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg005", "Operation failed. Please contact support");
                        }
                        var BusinessNameCheck = _HCoreContextOperations.HCTMerchantOnboarding.Any(x => x.DisplayName == _Request.businessName);
                        if (BusinessNameCheck)
                        {
                            _HCoreContextOperations.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg005", "Business Name address already used");
                        }
                        _HCTMerchantOnboarding = new HCTMerchantOnboarding();
                        _HCTMerchantOnboarding.Guid = HCoreHelper.GenerateGuid();
                        _HCTMerchantOnboarding.DisplayName = _Request.businessName;
                        _HCTMerchantOnboarding.EmailAddress = _Request.businessEmailAddress;
                        _HCTMerchantOnboarding.MobileNumber = _Request.mobileNumber;
                        _HCTMerchantOnboarding.Name = _Request.name;
                        _HCTMerchantOnboarding.Source = _Request.source;
                        _HCTMerchantOnboarding.Host = _Request.host;
                        _HCTMerchantOnboarding.CountryIsd = _Request.countryIsd;
                        _HCTMerchantOnboarding.CountryId = countryDetails.Id;
                        _HCTMerchantOnboarding.Password = HCoreEncrypt.EncryptHash(_Request.password);
                        _HCTMerchantOnboarding.EmailOtp = HCoreHelper.GenerateRandomNumber(4);
                        _HCTMerchantOnboarding.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCTMerchantOnboarding.SubscriptionKey = _Request.subscriptionKey;
                        _HCTMerchantOnboarding.StatusId = 1;
                        _HCoreContextOperations.HCTMerchantOnboarding.Add(_HCTMerchantOnboarding);
                        _HCoreContextOperations.SaveChanges();
                        var EmailObject = new
                        {
                            Code = _HCTMerchantOnboarding.EmailOtp,
                            UserDisplayName = _Request.businessName
                        };
                        var _Response = new OOnboardingV2.Profile.Response
                        {
                            reference = _HCTMerchantOnboarding.Guid,
                            businessName = _Request.businessName,
                            businessEmailAddress = _Request.businessEmailAddress,
                            name = _Request.name,
                            password = _Request.password,
                            source = _Request.source,
                            countryIsd = _Request.countryIsd,
                            mobileNumber = _Request.mobileNumber,
                            subscriptionKey = _Request.subscriptionKey,
                            host = _Request.host,
                        };
                        HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantOnboardingEmailVerificationCode, "TUC Merchant", _Request.businessEmailAddress, EmailObject, _Request.UserReference);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1063, TUCCoreResource.CA1063M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchant_St1", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        internal OResponse OnboardMerchant_EmailUpdate(OOnboardingV2.EmailChange.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.businessEmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg000", "Email address required");
                }
                if (string.IsNullOrEmpty(_Request.reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg001", "Reference required");
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var CheckOnboarding = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Guid == _Request.reference).FirstOrDefault();
                    if (CheckOnboarding != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var UserNameCheck = _HCoreContext.HCUAccountAuth.Any(x => x.Username == _Request.businessEmailAddress);
                            if (UserNameCheck)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg005", "Email address already used");
                            }
                            _HCoreContext.Dispose();
                        }
                        CheckOnboarding.EmailOtp = HCoreHelper.GenerateRandomNumber(4);
                        CheckOnboarding.EmailAddress = _Request.businessEmailAddress;
                        _HCoreContextOperations.SaveChanges();
                        _HCoreContextOperations.Dispose();

                        var EmailObject = new
                        {
                            Code = CheckOnboarding.EmailOtp,
                            UserDisplayName = CheckOnboarding.DisplayName
                        };
                        var _Response = new OOnboardingV2.EmailChange.Response
                        {
                            reference = CheckOnboarding.Guid,
                            businessEmailAddress = CheckOnboarding.EmailAddress,
                        };
                        HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantOnboardingEmailVerificationCode, "TUC Merchant", CheckOnboarding.EmailAddress, EmailObject, _Request.UserReference);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "reg001", "Email address updated successfully");
                    }
                    else
                    {
                        _HCoreContextOperations.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg001", "Invalid reference");
                    }
                }


                //using (_HCoreContext = new HCoreContext())
                //{
                //    var UserNameCheck = _HCoreContext.HCUAccountAuth.Any(x => x.Username == _Request.businessEmailAddress);
                //    if (UserNameCheck)
                //    {
                //        _HCoreContext.Dispose();
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg005", "Email address already used");
                //    }
                //    var countryDetails = _HCoreContext.HCCoreCountry
                //        .Where(x => x.Isd == _Request.countryIsd)
                //        .Select(x => new
                //        {
                //            x.Id,
                //            x.MobileNumberLength,
                //            x.Isd,
                //            x.StatusId
                //        })
                //        .FirstOrDefault();
                //    if (countryDetails == null)
                //    {
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg005", "Invalid country code");
                //    }

                //    _HCoreContext.Dispose();

                //    using (_HCoreContextOperations = new HCoreContextOperations())
                //    {
                //        var CheckOnboarding = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.EmailAddress == _Request.businessEmailAddress).FirstOrDefault();
                //        if (CheckOnboarding != null)
                //        {
                //            _HCoreContextOperations.HCTMerchantOnboarding.Remove(CheckOnboarding);
                //            _HCoreContextOperations.SaveChanges();
                //        }
                //        _HCoreContextOperations.Dispose();
                //    }

                //    using (_HCoreContextOperations = new HCoreContextOperations())
                //    {
                //        var CheckOnboarding = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.EmailAddress == _Request.businessEmailAddress).FirstOrDefault();
                //        if (CheckOnboarding != null)
                //        {
                //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg005", "Operation failed. Please contact support");
                //        }
                //        _HCTMerchantOnboarding = new HCTMerchantOnboarding();
                //        _HCTMerchantOnboarding.Guid = HCoreHelper.GenerateGuid();
                //        _HCTMerchantOnboarding.DisplayName = _Request.businessName;
                //        _HCTMerchantOnboarding.EmailAddress = _Request.businessEmailAddress;
                //        _HCTMerchantOnboarding.Name = _Request.name;
                //        _HCTMerchantOnboarding.Source = _Request.source;
                //        _HCTMerchantOnboarding.Host = _Request.host;
                //        _HCTMerchantOnboarding.Password = HCoreEncrypt.EncryptHash(_Request.password);
                //        _HCTMerchantOnboarding.EmailOtp = HCoreHelper.GenerateRandomNumber(4);
                //        _HCTMerchantOnboarding.CreateDate = HCoreHelper.GetGMTDateTime();
                //        _HCTMerchantOnboarding.SubscriptionKey = _Request.subscriptionKey;
                //        _HCTMerchantOnboarding.StatusId = 1;
                //        _HCoreContextOperations.HCTMerchantOnboarding.Add(_HCTMerchantOnboarding);
                //        _HCoreContextOperations.SaveChanges();
                //        var EmailObject = new
                //        {
                //            Code = _HCTMerchantOnboarding.EmailOtp
                //        };
                //        var _Response = new
                //        {
                //            Reference = _HCTMerchantOnboarding.Guid,
                //        };
                //        HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantOnboardingEmailVerificationCode, "TUC Merchant", _Request.businessEmailAddress, EmailObject, _Request.UserReference);
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1063, TUCCoreResource.CA1063M);
                //    }
                //}


            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchant_St1", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        internal OResponse OnboardMerchant_ResendEmailCode(OOnboardingV2.EmailChange.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg001", "Reference required");
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var CheckOnboarding = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Guid == _Request.reference).FirstOrDefault();
                    if (CheckOnboarding != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var UserNameCheck = _HCoreContext.HCUAccountAuth.Any(x => x.Username == _Request.businessEmailAddress);
                            if (UserNameCheck)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg005", "Email address already used");
                            }
                            _HCoreContext.Dispose();
                        }
                        CheckOnboarding.EmailOtp = HCoreHelper.GenerateRandomNumber(4);
                        CheckOnboarding.EmailAddress = _Request.businessEmailAddress;
                        _HCoreContextOperations.SaveChanges();
                        _HCoreContextOperations.Dispose();

                        var EmailObject = new
                        {
                            Code = CheckOnboarding.EmailOtp,
                            UserDisplayName = CheckOnboarding.DisplayName
                        };
                        var _Response = new
                        {
                            Reference = CheckOnboarding.Guid,
                            EmailAddress = CheckOnboarding.EmailAddress,
                        };
                        HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantOnboardingEmailVerificationCode, "TUC Merchant", CheckOnboarding.EmailAddress, EmailObject, _Request.UserReference);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "reg001", "Verification code sent to your email address successfully");
                    }
                    else
                    {
                        _HCoreContextOperations.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg001", "Invalid reference");
                    }
                }


                //using (_HCoreContext = new HCoreContext())
                //{
                //    var UserNameCheck = _HCoreContext.HCUAccountAuth.Any(x => x.Username == _Request.businessEmailAddress);
                //    if (UserNameCheck)
                //    {
                //        _HCoreContext.Dispose();
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg005", "Email address already used");
                //    }
                //    var countryDetails = _HCoreContext.HCCoreCountry
                //        .Where(x => x.Isd == _Request.countryIsd)
                //        .Select(x => new
                //        {
                //            x.Id,
                //            x.MobileNumberLength,
                //            x.Isd,
                //            x.StatusId
                //        })
                //        .FirstOrDefault();
                //    if (countryDetails == null)
                //    {
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg005", "Invalid country code");
                //    }

                //    _HCoreContext.Dispose();

                //    using (_HCoreContextOperations = new HCoreContextOperations())
                //    {
                //        var CheckOnboarding = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.EmailAddress == _Request.businessEmailAddress).FirstOrDefault();
                //        if (CheckOnboarding != null)
                //        {
                //            _HCoreContextOperations.HCTMerchantOnboarding.Remove(CheckOnboarding);
                //            _HCoreContextOperations.SaveChanges();
                //        }
                //        _HCoreContextOperations.Dispose();
                //    }

                //    using (_HCoreContextOperations = new HCoreContextOperations())
                //    {
                //        var CheckOnboarding = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.EmailAddress == _Request.businessEmailAddress).FirstOrDefault();
                //        if (CheckOnboarding != null)
                //        {
                //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg005", "Operation failed. Please contact support");
                //        }
                //        _HCTMerchantOnboarding = new HCTMerchantOnboarding();
                //        _HCTMerchantOnboarding.Guid = HCoreHelper.GenerateGuid();
                //        _HCTMerchantOnboarding.DisplayName = _Request.businessName;
                //        _HCTMerchantOnboarding.EmailAddress = _Request.businessEmailAddress;
                //        _HCTMerchantOnboarding.Name = _Request.name;
                //        _HCTMerchantOnboarding.Source = _Request.source;
                //        _HCTMerchantOnboarding.Host = _Request.host;
                //        _HCTMerchantOnboarding.Password = HCoreEncrypt.EncryptHash(_Request.password);
                //        _HCTMerchantOnboarding.EmailOtp = HCoreHelper.GenerateRandomNumber(4);
                //        _HCTMerchantOnboarding.CreateDate = HCoreHelper.GetGMTDateTime();
                //        _HCTMerchantOnboarding.SubscriptionKey = _Request.subscriptionKey;
                //        _HCTMerchantOnboarding.StatusId = 1;
                //        _HCoreContextOperations.HCTMerchantOnboarding.Add(_HCTMerchantOnboarding);
                //        _HCoreContextOperations.SaveChanges();
                //        var EmailObject = new
                //        {
                //            Code = _HCTMerchantOnboarding.EmailOtp
                //        };
                //        var _Response = new
                //        {
                //            Reference = _HCTMerchantOnboarding.Guid,
                //        };
                //        HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantOnboardingEmailVerificationCode, "TUC Merchant", _Request.businessEmailAddress, EmailObject, _Request.UserReference);
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1063, TUCCoreResource.CA1063M);
                //    }
                //}


            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchant_St1", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        internal OResponse OnboardMerchant_VerifyEmail(OOnboardingV2.EmailVerify.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1413, TUCCoreResource.CA1413M);
                }
                if (string.IsNullOrEmpty(_Request.code))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14163, TUCCoreResource.CAA14163M);
                }
                if (string.IsNullOrEmpty(_Request.businessEmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14164, TUCCoreResource.CAA14164M);
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var Details = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Guid == _Request.reference).FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.IsEmailVerified != 1)
                        {
                            if (Details.EmailOtp == _Request.code)
                            {
                                Details.EmailVerificationDate = HCoreHelper.GetGMTDateTime();
                                Details.IsEmailVerified = 1;
                                Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                                _HCoreContextOperations.SaveChanges();

                                #region Request Verification
                                _VerificationRequest = new OCoreVerificationManager.Request();
                                _VerificationRequest.CountryIsd = Details.CountryIsd;
                                _VerificationRequest.Type = 1;
                                _VerificationRequest.MobileNumber = Details.MobileNumber;
                                _VerificationRequest.UserReference = _Request.UserReference;
                                _ManageCoreVerification = new ManageCoreVerification();
                                var VerificationResponse = _ManageCoreVerification.RequestOtp(_VerificationRequest);
                                #endregion
                                if (VerificationResponse.Status == StatusSuccess)
                                {
                                    OCoreVerificationManager.Response VerificationResponseItem = (OCoreVerificationManager.Response)VerificationResponse.Result;
                                    using (_HCoreContextOperations = new HCoreContextOperations())
                                    {
                                        var UDetails = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Id == Details.Id).FirstOrDefault();
                                        if (UDetails != null)
                                        {
                                            if (_Request.CellAuth == true || _Request.CellAuth == null)
                                            {
                                                UDetails.MobileVerificationToken = VerificationResponseItem.RequestToken;
                                                _HCoreContextOperations.SaveChanges();

                                                var _Response = new OOnboardingV2.MobileVerify.Response
                                                {
                                                    reference = Details.Guid,
                                                    token = VerificationResponseItem.RequestToken,
                                                    mobileNumber = Details.MobileNumber,
                                                };

                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "REG", "Email verification successful");
                                            }
                                            else
                                            {
                                                UDetails.IsMobileVerified = 0;
                                                UDetails.MobileVerificationDate = HCoreHelper.GetGMTDateTime();
                                                UDetails.StatusId = HelperStatus.Default.Active;
                                                _HCoreContextOperations.SaveChanges();
                                                return OnboardMerchant_Request_CreateAccount(Details.Id, _Request.IsNewsLetter, _Request.IsPolicy);
                                            }
                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1414, "EmaiAddress not found please start again.");
                                        }
                                    }

                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1414, "Unable to start mobile verification please try after some time.");
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14165, TUCCoreResource.CAA14165M);
                            }
                        }
                        else
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var HCUAccountDetails = _HCoreContext.HCUAccount.Where(x => x.EmailAddress == Details.EmailAddress).FirstOrDefault();
                                string SystemPassword = _HCoreContext.HCUAccountAuth.Where(x => x.Username == Details.EmailAddress).Select(x => x.SystemPassword).FirstOrDefault();
                                SystemPassword = HCoreEncrypt.DecryptHash(SystemPassword);
                                var ReqObject = new
                                {
                                    ReferenceId = HCUAccountDetails.Id,
                                    ReferenceKey = HCUAccountDetails.Guid,
                                    UserName = HCUAccountDetails.EmailAddress,
                                    AuthToken = SystemPassword,
                                };
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ReqObject, TUCCoreResource.CA1415, TUCCoreResource.CA1415M);
                            }
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1414, TUCCoreResource.CA1414M);
                    }

                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchant_St1", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        internal OResponse OnboardMerchant_MobileUpdate(OOnboardingV2.MobileChange.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.businessEmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg000", "Email address required");
                }
                if (string.IsNullOrEmpty(_Request.reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg001", "Reference required");
                }
                if (string.IsNullOrEmpty(_Request.mobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg001", "Mobile number required");
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var CheckOnboarding = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Guid == _Request.reference).FirstOrDefault();
                    if (CheckOnboarding != null)
                    {
                        _HCoreContext = new HCoreContext();
                        var countryDetails = _HCoreContext.HCCoreCountry
                        .Where(x => x.Id == CheckOnboarding.CountryId)
                        .Select(x => new
                        {
                            x.Id,
                            x.MobileNumberLength,
                            x.Isd,
                            x.StatusId
                        })
                        .FirstOrDefault();
                        _HCoreContext.Dispose();
                        if (countryDetails == null)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg005", "Invalid country code");
                        }

                        var MobileCheck = HCoreHelper.ValidateMobileNumber(countryDetails.Isd + _Request.mobileNumber);
                        if (MobileCheck.IsNumberValid == false)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg005", "Invalid mobile number");
                        }
                        CheckOnboarding.MobileNumber = _Request.mobileNumber;
                        _HCoreContextOperations.SaveChanges();
                        _HCoreContextOperations.Dispose();
                        #region Request Verification
                        _VerificationRequest = new OCoreVerificationManager.Request();
                        _VerificationRequest.CountryIsd = countryDetails.Isd;
                        _VerificationRequest.Type = 1;
                        _VerificationRequest.MobileNumber = _Request.mobileNumber;
                        _VerificationRequest.UserReference = _Request.UserReference;
                        _ManageCoreVerification = new ManageCoreVerification();
                        var VerificationResponse = _ManageCoreVerification.RequestOtp(_VerificationRequest);
                        #endregion
                        if (VerificationResponse.Status == StatusSuccess)
                        {
                            OCoreVerificationManager.Response VerificationResponseItem = (OCoreVerificationManager.Response)VerificationResponse.Result;
                            using (_HCoreContextOperations = new HCoreContextOperations())
                            {
                                var UDetails = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Id == CheckOnboarding.Id).FirstOrDefault();
                                UDetails.MobileVerificationToken = VerificationResponseItem.RequestToken;
                                _HCoreContextOperations.SaveChanges();
                            }
                            var _Response = new OOnboardingV2.MobileVerify.Response
                            {
                                reference = CheckOnboarding.Guid,
                                token = VerificationResponseItem.RequestToken,
                                mobileNumber = CheckOnboarding.MobileNumber,
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "REG", "Mobile number updated successfully. Verification code sent to your udpated mobile number");
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1414, "Unable to start mobile verification please try after some time.");
                        }
                    }
                    else
                    {
                        _HCoreContextOperations.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg001", "Invalid reference");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchant_MobileUpdate", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        internal OResponse OnboardMerchant_Request_MobileVerification(OOnboardingV2.MobileVerify.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1427, TUCCoreResource.CA1427M);
                }
                if (string.IsNullOrEmpty(_Request.businessEmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1417, TUCCoreResource.CA1417M);
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var Details = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Guid == _Request.reference && x.EmailAddress == _Request.businessEmailAddress).FirstOrDefault();
                    if (Details != null)
                    {
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContextOperations.SaveChanges();
                        #region Request Verification
                        _VerificationRequest = new OCoreVerificationManager.Request();
                        _VerificationRequest.CountryIsd = Details.CountryIsd;
                        _VerificationRequest.Type = 1;
                        _VerificationRequest.MobileNumber = Details.MobileNumber;
                        _VerificationRequest.UserReference = _Request.UserReference;
                        _ManageCoreVerification = new ManageCoreVerification();
                        var VerificationResponse = _ManageCoreVerification.RequestOtp(_VerificationRequest);
                        #endregion
                        if (VerificationResponse.Status == StatusSuccess)
                        {
                            OCoreVerificationManager.Response VerificationResponseItem = (OCoreVerificationManager.Response)VerificationResponse.Result;
                            using (_HCoreContextOperations = new HCoreContextOperations())
                            {
                                var UDetails = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Id == Details.Id).FirstOrDefault();
                                UDetails.MobileVerificationToken = VerificationResponseItem.RequestToken;
                                _HCoreContextOperations.SaveChanges();
                            }
                            var _Response = new OOnboardingV2.MobileVerify.Response
                            {
                                reference = Details.Guid,
                                token = VerificationResponseItem.RequestToken,
                                mobileNumber = Details.MobileNumber,
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "REG", "Verification code sent to your registered mobile number");
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1414, "Unable to start mobile verification please try after some time.");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1414, TUCCoreResource.CA1414M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchant_Request_MobileVerification", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        internal OResponse OnboardMerchant_Request_MobileVerificationConfirm(OOnboardingV2.MobileVerifyConfirm.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1405, TUCCoreResource.CA1405M);
                }
                if (string.IsNullOrEmpty(_Request.code))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1421, TUCCoreResource.CA1421M);
                }
                if (string.IsNullOrEmpty(_Request.token))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1416, TUCCoreResource.CA1416M);
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var Details = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Guid == _Request.reference && x.MobileVerificationToken == _Request.token).FirstOrDefault();
                    if (Details.IsMobileVerified != 1)
                    {
                        if (Details != null)
                        {
                            #region Request Verification
                            _VerifyRequest = new OCoreVerificationManager.RequestVerify();
                            _VerifyRequest.RequestToken = Details.MobileVerificationToken;
                            _VerifyRequest.AccessCode = _Request.code;
                            _VerifyRequest.UserReference = _Request.UserReference;
                            _ManageCoreVerification = new ManageCoreVerification();
                            var VerificationResponse = _ManageCoreVerification.VerifyOtp(_VerifyRequest);
                            #endregion
                            if (VerificationResponse.Status == StatusSuccess)
                            {
                                OCoreVerificationManager.Response VerificationResponseItem = (OCoreVerificationManager.Response)VerificationResponse.Result;
                                using (_HCoreContextOperations = new HCoreContextOperations())
                                {
                                    var AccountDetails = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Guid == _Request.reference).FirstOrDefault();
                                    AccountDetails.IsMobileVerified = 1;
                                    AccountDetails.MobileVerificationDate = HCoreHelper.GetGMTDateTime();
                                    AccountDetails.StatusId = HelperStatus.Default.Active;
                                    _HCoreContextOperations.SaveChanges();
                                }
                                var _Response = new OOnboardingV2.MobileVerifyConfirm.Request
                                {
                                    reference = Details.Guid,
                                };
                                return OnboardMerchant_Request_CreateAccount(Details.Id, _Request.IsNewsLetter, _Request.IsPolicy);
                            }
                            else
                            {
                                return VerificationResponse;
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1414, TUCCoreResource.CA1414M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1415, TUCCoreResource.CA1415M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchant_St4_Request", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        private OResponse OnboardMerchant_Request_CreateAccount(long Id, string? IsNewsLetter, string? IsPolicy)
        {
            #region Manage Exception
            try
            {
                //if (string.IsNullOrEmpty(_Request.Reference))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1405, TUCCoreResource.CA1405M);
                //}
                //if (string.IsNullOrEmpty(_Request.Source))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1416, TUCCoreResource.CA1416M);
                //}
                //if (string.IsNullOrEmpty(_Request.DisplayName))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1423, TUCCoreResource.CA1423M);
                //}
                //if (string.IsNullOrEmpty(_Request.SubscriptionKey))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1424, TUCCoreResource.CA1424M);
                //}
                //if (_Request.Categories == null || _Request.Categories.Count == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1425, TUCCoreResource.CA1425M);
                //}
                //if (_Request.Address == null)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1426, TUCCoreResource.CA1426M);
                //}
                //OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.Address, _Request.UserReference);
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var Details = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Id == Id).FirstOrDefault();
                    if (Details != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            string SysPassword = HCoreHelper.GenerateGuid();
                            _Random = new Random();
                            string AccountCode = _Random.Next(100, 999).ToString() + _Random.Next(000000000, 999999999).ToString();
                            string ReferenceKey = HCoreHelper.GenerateGuid();
                            long RewardPercentage = Convert.ToInt64(_HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage" && x.TypeId == HelperType.Configuration).Select(x => x.Value).FirstOrDefault());
                            string? CountryIsd = _HCoreContext.HCCoreCountry.Where(x => x.Id == Details.CountryId).Select(x => x.Isd).FirstOrDefault();
                            _HCUAccountParameters = new List<HCUAccountParameter>();
                            _HCUAccountParameter = new HCUAccountParameter();
                            _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                            _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewarddeductiontype" && x.TypeId == HelperType.Configuration).Select(x => x.Id).FirstOrDefault();
                            _HCUAccountParameter.Value = "Prepay";
                            _HCUAccountParameter.HelperId = 253;
                            _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                            _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                            _HCUAccountParameters.Add(_HCUAccountParameter);

                            //_HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                            //_HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                            //_HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage" && x.TypeId == HelperType.Configuration).Select(x => x.Id).FirstOrDefault();
                            //_HCUAccountParameter.Value = RewardPercentage.ToString();
                            //_HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                            //_HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                            //_HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                            //_HCUAccountParameters.Add(_HCUAccountParameter);
                            //if (_Request.UserReference.AccountId != 0)
                            //{
                            //_HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                            //}
                            //_HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;

                            _HCUAccountAuth = new HCUAccountAuth();
                            _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountAuth.Username = Details.EmailAddress;
                            _HCUAccountAuth.Password = Details.Password;
                            _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                            _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(SysPassword);
                            _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                            #region Save UserAccount
                            _HCUAccount = new HCUAccount();
                            _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                            //if (_Request.Categories != null && _Request.Categories.Count > 0)
                            //{
                            //    _HCUAccountParameters = new List<HCUAccountParameter>();
                            //    foreach (var Category in _Request.Categories)
                            //    {
                            //        HCUAccountParameter _HCUAccountParameterItem = new HCUAccountParameter
                            //        {
                            //            Guid = HCoreHelper.GenerateGuid(),
                            //            TypeId = HelperType.MerchantCategory,
                            //            CommonId = Category,
                            //            CreateDate = HCoreHelper.GetGMTDateTime(),
                            //            StatusId = HelperStatus.Default.Active,
                            //        };
                            //        _HCUAccount.HCUAccountParameterAccount.Add(_HCUAccountParameterItem);
                            //    }
                            //}
                            //#region Set Categories
                            //if (_Request.Categories != null && _Request.Categories.Count > 0)
                            //{
                            //    _TUCCategoryAccount = new List<TUCCategoryAccount>();
                            //    foreach (var Category in _Request.Categories)
                            //    {
                            //        _TUCCategoryAccount.Add(new TUCCategoryAccount
                            //        {
                            //            Guid = HCoreHelper.GenerateGuid(),
                            //            TypeId = 1,
                            //            CategoryId = Category,
                            //            CreateDate = HCoreHelper.GetGMTDateTime(),
                            //            CreatedById = 1,
                            //            StatusId = HelperStatus.Default.Active,
                            //        });
                            //    }
                            //    _HCUAccount.TUCCategoryAccountAccount = _TUCCategoryAccount;
                            //}
                            //#endregion
                            string[] FullName = Details.Name.Split(" ");
                            string FirstName = string.Empty, LastName = string.Empty;
                            if (FullName.Length == 0 || FullName.Length == 1)
                            {
                                FirstName = Details.Name;
                            }
                            else if (FullName.Length == 2)
                            {
                                FirstName = FullName[0];
                                LastName = FullName[1];
                            }
                            else if (FullName.Length == 3)
                            {
                                FirstName = FullName[0];
                                LastName = FullName[2];
                            }
                            _HCUAccount.Guid = ReferenceKey;
                            _HCUAccount.AccountTypeId = UserAccountType.Merchant;
                            _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                            _HCUAccount.OwnerId = 3;
                            _HCUAccount.DisplayName = Details.DisplayName;
                            _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                            _HCUAccount.Name = Details.DisplayName;
                            _HCUAccount.FirstName = FirstName;
                            _HCUAccount.LastName = LastName;
                            _HCUAccount.CpFirstName = FirstName;
                            _HCUAccount.CpLastName = LastName;
                            _HCUAccount.CpEmailAddress = Details.EmailAddress;
                            _HCUAccount.CpMobileNumber = Details.MobileNumber;
                            //_HCUAccount.CpMobileNumber = HCoreHelper.FormatMobileNumber(CountryIsd, Details.MobileNumber, Details.MobileNumber.Length);
                            _HCUAccount.EmailAddress = Details.EmailAddress;
                            _HCUAccount.ContactNumber = Details.MobileNumber;
                            //_HCUAccount.ContactNumber = HCoreHelper.FormatMobileNumber(CountryIsd, Details.MobileNumber, Details.MobileNumber.Length);
                            if (!string.IsNullOrEmpty(Details.ReferralCode))
                            {
                                long ReferrerId = _HCoreContext.HCUAccount
                                    .Where(x => x.ReferralCode == Details.ReferralCode
                                    && (x.AccountTypeId == UserAccountType.Merchant || x.AccountTypeId == UserAccountType.Acquirer || x.AccountTypeId == UserAccountType.PgAccount || x.AccountTypeId == UserAccountType.PosAccount))
                                    .Select(x => x.Id).FirstOrDefault();
                                if (ReferrerId > 0)
                                {
                                    _HCUAccount.OwnerId = ReferrerId;
                                }
                            }
                            _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                            _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
                            //_HCUAccount.MobileNumber = Details.MobileNumber;
                            _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(CountryIsd, Details.MobileNumber, Details.MobileNumber.Length);
                            //_HCUAccount.Address = _AddressResponse.Address;
                            //_HCUAccount.Latitude = _AddressResponse.Latitude;
                            //_HCUAccount.Longitude = _AddressResponse.Longitude;
                            _HCUAccount.CountryId = (int)Details.CountryId;
                            //if (_AddressResponse.StateId != 0)
                            //{
                            //    _HCUAccount.StateId = _AddressResponse.StateId;
                            //}
                            //if (_AddressResponse.CityId != 0)
                            //{
                            //    _HCUAccount.CityId = _AddressResponse.CityId;
                            //}
                            //if (_Request.UserReference.AppVersionId != 0)
                            //{
                            //    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                            //}
                            //_HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                            _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.Website;
                            _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccount.StatusId = HelperStatus.Default.Active;
                            //if (_AddressResponse.CountryId > 0)
                            //{
                            //    _HCUAccount.CountryId = _AddressResponse.CountryId;
                            //}
                            //else
                            //{
                            //    _HCUAccount.CountryId = _Request.UserReference.SystemCountry;
                            //}
                            _HCUAccount.EmailVerificationStatus = 2;
                            _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccount.NumberVerificationStatus = 2;
                            _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccount.DocumentVerificationStatus = 0;
                            _HCUAccount.AccountPercentage = RewardPercentage;
                            _HCUAccount.User = _HCUAccountAuth;
                            #endregion
                            _HCoreContext.HCUAccount.Add(_HCUAccount);
                            _HCoreContext.SaveChanges();
                            long MerchantId = _HCUAccount.Id;
                            string MerchantKey = _HCUAccount.Guid;

                            #region By Default Close Loyalty Program is Assigned
                            using (HCoreContext hc = new HCoreContext())
                            {
                                long SubscriptionId = hc.HCSubscription.Where(x => x.Guid == "merchantstoresubscription").Select(x => x.Id).FirstOrDefault();
                                int ProgramTypeId = hc.HCCore.Where(x => x.SystemName == "programtype.closeloyalty").Select(x => x.Id).FirstOrDefault();
                                int ComissionTypeId = hc.HCCore.Where(x => x.SystemName == "commissiontype.zero").Select(x => x.Id).FirstOrDefault();
                                TUCLProgram _TUCProgram = new TUCLProgram();
                                _TUCProgram.Guid = HCoreHelper.GenerateGuid();
                                _TUCProgram.Name = Details.DisplayName;
                                _TUCProgram.AccountId = MerchantId;
                                _TUCProgram.ProgramTypeId = ProgramTypeId;
                                _TUCProgram.SubscriptionId = SubscriptionId;
                                _TUCProgram.ComissionTypeId = ComissionTypeId;
                                _TUCProgram.Comission = 0;
                                _TUCProgram.CreateDate = HCoreHelper.GetGMTDateTime();
                                _TUCProgram.CreatedById = MerchantId;
                                _TUCProgram.StatusId = HelperStatus.Default.Active;
                                hc.TUCLProgram.Add(_TUCProgram);
                                hc.SaveChanges();
                                hc.Dispose();
                            }
                            #endregion

                            if (!string.IsNullOrEmpty(IsNewsLetter))
                            {
                                _HCUAccountParameter = new HCUAccountParameter();
                                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountParameter.AccountId = MerchantId;
                                _HCUAccountParameter.TypeId = 813; //TypeId is Terms and Conditions
                                _HCUAccountParameter.SubTypeId = 814; // Sub Type Id is News Letter
                                _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "terms").Select(x => x.Id).FirstOrDefault();
                                _HCUAccountParameter.Value = IsNewsLetter;
                                _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                            }
                            if (!string.IsNullOrEmpty(IsPolicy))
                            {
                                _HCUAccountParameter = new HCUAccountParameter();
                                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountParameter.AccountId = MerchantId;
                                _HCUAccountParameter.TypeId = 813; //TypeId is Terms and Conditions
                                _HCUAccountParameter.SubTypeId = 815; // Sub Type Id is Policy
                                _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "terms").Select(x => x.Id).FirstOrDefault();
                                _HCUAccountParameter.Value = IsPolicy;
                                _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                            }
                            var EmailObject = new
                            {
                                EmailAddress = _HCUAccount.EmailAddress,
                                UserName = _HCUAccountAuth.Username,
                                Password = HCoreEncrypt.DecryptHash(Details.Password),
                                DisplayName = _HCUAccount.DisplayName,
                                UserDisplayName = _HCUAccount.DisplayName
                            };
                            using (_HCoreContext = new HCoreContext())
                            {
                                _HCUAccountAuth = new HCUAccountAuth();
                                _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(10);
                                _HCUAccountAuth.Password = HCoreHelper.GenerateRandomNumber(6);
                                _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountAuth.CreatedById = MerchantId;
                                _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                #region Save UserAccount
                                _HCUAccount = new HCUAccount();
                                _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                                _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                                _HCUAccount.OwnerId = MerchantId;
                                _HCUAccount.DisplayName = Details.DisplayName;
                                _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                                _HCUAccount.Name = Details.DisplayName;
                                _HCUAccount.EmailAddress = Details.EmailAddress;
                                _HCUAccount.ContactNumber = Details.MobileNumber;
                                _HCUAccount.MobileNumber = Details.MobileNumber;
                                //_HCUAccount.ContactNumber = HCoreHelper.FormatMobileNumber(CountryIsd, Details.MobileNumber, Details.MobileNumber.Length);
                                //_HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(CountryIsd, Details.MobileNumber, Details.MobileNumber.Length);
                                _HCUAccount.CountryId = (int)Details.CountryId;
                                _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                                _HCUAccount.AccountCode = _Random.Next(100000000, 999999999).ToString();
                                _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                                _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccount.StatusId = HelperStatus.Default.Active;
                                _HCUAccount.EmailVerificationStatus = 0;
                                _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccount.NumberVerificationStatus = 0;
                                _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccount.DocumentVerificationStatus = 0;
                                _HCUAccount.User = _HCUAccountAuth;
                                #endregion
                                _HCoreContext.HCUAccount.Add(_HCUAccount);
                                _HCoreContext.SaveChanges();
                            }
                            using (_HCoreContextOperations = new HCoreContextOperations())
                            {
                                var ExistingAcc = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Id == Id).FirstOrDefault();
                                if (ExistingAcc != null)
                                {
                                    ExistingAcc.AccountId = MerchantId;
                                    ExistingAcc.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    _HCoreContextOperations.SaveChanges();
                                }
                            }
                            HCoreHelper.BroadCastEmail(MerchantOnboarding.WelcomeEmail, EmailObject.DisplayName, EmailObject.EmailAddress, EmailObject, null);
                            var ReqObject = new
                            {
                                ReferenceId = MerchantId,
                                ReferenceKey = MerchantKey,
                                UserName = Details.EmailAddress,
                                AuthToken = SysPassword,
                            };
                            return HCoreHelper.SendResponse(null, ResponseStatus.Success, ReqObject, TUCCoreResource.CA1104, TUCCoreResource.CA1104M);
                            //using (_HCoreContext = new HCoreContext())
                            //{
                            //    var SubscriptionDetails = _HCoreContext.HCSubscription.Where(x => x.Guid == _Request.SubscriptionKey).FirstOrDefault();
                            //    if (SubscriptionDetails != null)
                            //    {
                            //        if (SubscriptionDetails.SellingPrice > 0 && string.IsNullOrEmpty(_Request.PaymentReference))
                            //        {
                            //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1085, TUCCoreResource.CA1085M);
                            //        }
                            //        _HCUAccountSubscription = new HCUAccountSubscription();
                            //        _HCUAccountSubscription.Guid = HCoreHelper.GenerateGuid();
                            //        _HCUAccountSubscription.TypeId = SubscriptionDetails.TypeId;
                            //        _HCUAccountSubscription.AccountId = MerchantId;
                            //        _HCUAccountSubscription.SubscriptionId = SubscriptionDetails.Id;
                            //        _HCUAccountSubscription.StartDate = HCoreHelper.GetGMTDate();
                            //        _HCUAccountSubscription.EndDate = _HCUAccountSubscription.StartDate.AddDays(SubscriptionDetails.TotalDays);
                            //        _HCUAccountSubscription.RenewDate = _HCUAccountSubscription.EndDate.AddDays(1);
                            //        _HCUAccountSubscription.ActualPrice = SubscriptionDetails.ActualPrice;
                            //        _HCUAccountSubscription.SellingPrice = SubscriptionDetails.SellingPrice;
                            //        _HCUAccountSubscription.CreateDate = HCoreHelper.GetGMTDateTime();
                            //        _HCUAccountSubscription.StatusId = HelperStatus.Default.Active;
                            //        _HCoreContext.HCUAccountSubscription.Add(_HCUAccountSubscription);
                            //        _HCoreContext.SaveChanges();
                            //        if (SubscriptionDetails.Id == 3
                            //            || SubscriptionDetails.Id == 5
                            //            || SubscriptionDetails.Id == 7
                            //            || SubscriptionDetails.Id == 9
                            //            || SubscriptionDetails.Id == 11
                            //            || SubscriptionDetails.Id == 13
                            //            || SubscriptionDetails.Id == 15
                            //            || SubscriptionDetails.Id == 17
                            //            || SubscriptionDetails.Id == 19
                            //            || SubscriptionDetails.Id == 21
                            //            )
                            //        {
                            //            using (_HCoreContext = new HCoreContext())
                            //            {
                            //                var ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x =>
                            //                  x.SystemName == "thankucashgold"
                            //                  && x.StatusId == HCoreConstant.HelperStatus.Default.Active
                            //                  && x.TypeId == HCoreConstant.HelperType.Configuration
                            //                  ).Select(x => new
                            //                  {
                            //                      SystemName = x.SystemName,
                            //                      HelperId = x.HelperId,
                            //                      Value = x.Value,
                            //                      ConfigurationId = x.Id,
                            //                  }).FirstOrDefault();
                            //                if (ConfigurationDetails != null)
                            //                {
                            //                    _HCUAccountParameter = new HCUAccountParameter();
                            //                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                            //                    _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                            //                    _HCUAccountParameter.AccountId = MerchantId;
                            //                    _HCUAccountParameter.CommonId = ConfigurationDetails.ConfigurationId;
                            //                    _HCUAccountParameter.Value = "1";
                            //                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                            //                    _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                            //                    _HCUAccountParameter.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //                    _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                            //                    _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                            //                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                            //                    _HCoreContext.SaveChanges();
                            //                }
                            //            }
                            //        }

                            //    }
                            //    else
                            //    {
                            //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1103, TUCCoreResource.CA1103M);
                            //    }
                            //}

                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, TUCCoreResource.CA1414, TUCCoreResource.CA1414M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchant_Request_CreateAccount", _Exception, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        internal OResponse OnboardMerchant_Request_UpdateBusinessDetails(OOnboardingV2.BusinessInformation _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAREF", "ReferenceId Required");
                }
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAREFKEY", "ReferenceKey Required");
                }
                //if (string.IsNullOrEmpty(_Request.Bankdetails.AccountName))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0001", "Bank Account Name Required");
                //}
                //if (_Request.Bankdetails.BankId < 1)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0002", "Bank Id Required");
                //}
                //if (string.IsNullOrEmpty(_Request.Bankdetails.AccountNumber))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0003", "Bank Account Number Required");
                //}
                //if (_Request.Category.ReferenceId < 1)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0002", "Category Id Required");
                //}

                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.Address, _Request.UserReference);

                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        //if (!string.IsNullOrEmpty(_Request.BusinessName) && Details.DisplayName != _Request.BusinessName)
                        //{
                        //    Details.DisplayName = _Request.BusinessName;
                        //}
                        //if (!string.IsNullOrEmpty(_Request.BusinessName) && Details.Name != _Request.BusinessName)
                        //{
                        //    Details.Name = _Request.BusinessName;                          
                        //}
                        //if (!string.IsNullOrEmpty(_Request.BusinessEmail) && Details.EmailAddress != _Request.BusinessEmail)
                        //{
                        //    Details.EmailAddress = _Request.BusinessEmail;
                        //}
                        if (_Request.Address != null && _AddressResponse != null)
                        {
                            if (_AddressResponse.CountryId > 0 && Details.CountryId != _AddressResponse.CountryId)
                            {
                                Details.CountryId = _AddressResponse.CountryId;
                            }
                            if (_AddressResponse.StateId > 0 && Details.StateId != _AddressResponse.StateId)
                            {
                                Details.StateId = _AddressResponse.StateId;
                            }
                            if (_AddressResponse.CityId > 0 && Details.CityId != _AddressResponse.CityId)
                            {
                                Details.CityId = _AddressResponse.CityId;
                            }
                            if (_AddressResponse.CityAreaId > 0 && Details.CityAreaId != _AddressResponse.CityAreaId)
                            {
                                Details.CityAreaId = _AddressResponse.CityAreaId;
                            }
                            if (_AddressResponse.Latitude != 0 && Details.Latitude != _AddressResponse.Latitude)
                            {
                                Details.Latitude = _AddressResponse.Latitude;
                            }
                            if (_AddressResponse.Longitude != 0 && Details.Longitude != _AddressResponse.Longitude)
                            {
                                Details.Longitude = _AddressResponse.Longitude;
                            }
                            if (!string.IsNullOrEmpty(_AddressResponse.Address) && Details.Address != _AddressResponse.Address)
                            {
                                Details.Address = _AddressResponse.Address;
                            }

                            //if (_Request.ContactPerson != null)
                            //{
                            //    //Details.FirstName = _Request.ContactPerson.FirstName;
                            //    //Details.LastName = _Request.ContactPerson.LastName;
                            //    //Details.SecondaryEmailAddress = _Request.ContactPerson.EmailAddress;
                            //    //Details.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.ContactPerson.countryIsd, _Request.ContactPerson.MobileNumber, _Request.ContactPerson.MobileNumber.Length);

                            //    Details.CpFirstName = _Request.ContactPerson.FirstName;
                            //    Details.CpLastName = _Request.ContactPerson.LastName;
                            //    Details.CpEmailAddress = _Request.ContactPerson.EmailAddress;
                            //    Details.CpMobileNumber = HCoreHelper.FormatMobileNumber(_Request.ContactPerson.countryIsd, _Request.ContactPerson.MobileNumber, _Request.ContactPerson.MobileNumber.Length);
                            //    Details.CpContactNumber = Details.CpMobileNumber;
                            //}
                        }

                        //if (_Request.Category != null)
                        //{
                        //    _HCUAccountParameter = new HCUAccountParameter();
                        //    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                        //    _HCUAccountParameter.AccountId = Details.Id;
                        //    _HCUAccountParameter.TypeId = HelperType.MerchantCategory;
                        //    _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "businesscategory").Select(a => a.Id).FirstOrDefault();
                        //    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        //    _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                        //    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                        //}

                        //#region Set Category
                        //if (_Request.Category != null)
                        //{
                        //    _TUCCategoryAccount = new TUCCategoryAccount();
                        //    _TUCCategoryAccount.Guid = HCoreHelper.GenerateGuid();
                        //    _TUCCategoryAccount.AccountId = Details.Id;
                        //    _TUCCategoryAccount.TypeId = HelperType.MerchantCategory;
                        //    _TUCCategoryAccount.CategoryId = _Request.Category.ReferenceId;
                        //    _TUCCategoryAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                        //    _TUCCategoryAccount.CreatedById = _Request.UserReference.AccountId;
                        //    _TUCCategoryAccount.StatusId = HelperStatus.Default.Active;
                        //    _HCoreContext.TUCCategoryAccount.Add(_TUCCategoryAccount);

                        //    Details.PrimaryCategoryId = _Request.Category.ReferenceId;
                        //}
                        //#endregion

                        //#region Set Bank Details
                        //if (_Request.Bankdetails != null)
                        //{
                        //    var BankDetails = PaystackTransfer.VerifyAccountNumber(_AppConfig.PaystackPrivateKey, _Request.Bankdetails.AccountNumber, _Request.Bankdetails.BankCode);
                        //    if (BankDetails != null)
                        //    {
                        //        var RecepientBankDetails = PaystackTransfer.CreateRecepient(_AppConfig.PaystackPrivateKey, BankDetails.account_name, BankDetails.account_number, _Request.Bankdetails.BankCode);
                        //        if (RecepientBankDetails != null)
                        //        {
                        //            _HCUAccountBank = new HCUAccountBank();
                        //            _HCUAccountBank.Guid = HCoreHelper.GenerateGuid();
                        //            _HCUAccountBank.AccountId = Details.Id;
                        //            _HCUAccountBank.Name = BankDetails.account_name;
                        //            _HCUAccountBank.AccountNumber = BankDetails.account_number;
                        //            _HCUAccountBank.BankName = _Request.Bankdetails.BankName;
                        //            _HCUAccountBank.BankCode = _Request.Bankdetails.BankCode;
                        //            _HCUAccountBank.BankId = Convert.ToInt64(BankDetails.bank_id);
                        //            _HCUAccountBank.ReferenceCode = RecepientBankDetails.recipient_code;
                        //            _HCUAccountBank.ReferenceKey = RecepientBankDetails.id;
                        //            //_HCUAccountBank.BvnNumber = _Request.Bankdetails.BvnNumber;
                        //            //_HCUAccountBank.Comment = _Request.Bankdetails.Comment;
                        //            _HCUAccountBank.CreateDate = HCoreHelper.GetGMTDateTime();
                        //            _HCUAccountBank.CreatedById = _Request.UserReference.AccountId;
                        //            _HCUAccountBank.StatusId = HelperStatus.Default.Active;
                        //            _HCoreContext.HCUAccountBank.Add(_HCUAccountBank);
                        //            _HCoreContext.SaveChanges();
                        //        }
                        //    }
                        //}
                        //#endregion

                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.ModifyById = _Request.UserReference.AccountId;

                        #region Update Store Address
                        var StoreDetails = _HCoreContext.HCUAccount.Where(x => x.OwnerId == Details.Id && x.Owner.Guid == Details.Guid || (x.Owner.OwnerId == Details.Id && x.Owner.Owner.Guid == Details.Guid)).FirstOrDefault();
                        if (StoreDetails != null)
                        {
                            if (_AddressResponse.CountryId > 0 && StoreDetails.CountryId != _AddressResponse.CountryId)
                            {
                                StoreDetails.CountryId = _AddressResponse.CountryId;
                            }
                            if (_AddressResponse.StateId > 0 && StoreDetails.StateId != _AddressResponse.StateId)
                            {
                                StoreDetails.StateId = _AddressResponse.StateId;
                            }
                            if (_AddressResponse.CityId > 0 && StoreDetails.CityId != _AddressResponse.CityId)
                            {
                                StoreDetails.CityId = _AddressResponse.CityId;
                            }
                            if (_AddressResponse.CityAreaId > 0 && StoreDetails.CityAreaId != _AddressResponse.CityAreaId)
                            {
                                StoreDetails.CityAreaId = _AddressResponse.CityAreaId;
                            }
                            if (_AddressResponse.Latitude != 0 && StoreDetails.Latitude != _AddressResponse.Latitude)
                            {
                                StoreDetails.Latitude = _AddressResponse.Latitude;
                            }
                            if (_AddressResponse.Longitude != 0 && StoreDetails.Longitude != _AddressResponse.Longitude)
                            {
                                StoreDetails.Longitude = _AddressResponse.Longitude;
                            }
                            if (!string.IsNullOrEmpty(_AddressResponse.Address) && StoreDetails.Address != _AddressResponse.Address)
                            {
                                StoreDetails.Address = _AddressResponse.Address;
                            }
                            //if (_Request.ContactPerson != null)
                            //{
                            //    //Details.FirstName = _Request.ContactPerson.FirstName;
                            //    //Details.LastName = _Request.ContactPerson.LastName;
                            //    //Details.SecondaryEmailAddress = _Request.ContactPerson.EmailAddress;
                            //    //Details.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.ContactPerson.countryIsd, _Request.ContactPerson.MobileNumber, _Request.ContactPerson.MobileNumber.Length);

                            //    StoreDetails.CpFirstName = _Request.ContactPerson.FirstName;
                            //    StoreDetails.CpLastName = _Request.ContactPerson.LastName;
                            //    StoreDetails.CpEmailAddress = _Request.ContactPerson.EmailAddress;
                            //    StoreDetails.CpMobileNumber = HCoreHelper.FormatMobileNumber(_Request.ContactPerson.countryIsd, _Request.ContactPerson.MobileNumber, _Request.ContactPerson.MobileNumber.Length);
                            //    StoreDetails.CpContactNumber = StoreDetails.CpMobileNumber;
                            //}
                        }
                        #endregion

                        _HCoreContext.SaveChanges();

                        //long? ImageStorageId = null;
                        //if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                        //{
                        //    ImageStorageId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, Details.IconStorageId, _Request.UserReference);
                        //}
                        //if (ImageStorageId != null)
                        //{
                        //    using (_HCoreContext = new HCoreContext())
                        //    {
                        //        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == Details.Guid).FirstOrDefault();
                        //        if (AccountDetails != null)
                        //        {
                        //            if (ImageStorageId != null)
                        //            {
                        //                AccountDetails.PosterStorageId = ImageStorageId;
                        //            }
                        //            _HCoreContext.SaveChanges();
                        //        }
                        //        else
                        //        {
                        //            _HCoreContext.SaveChanges();
                        //        }
                        //    }
                        //}

                        var Response = new
                        {
                            ReferenceId = Details.Id,
                            ReferenceKey = Details.Guid
                        };

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, "CASAVE", "Business Details updated Successfully.");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0404", "Details not found.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnBoardMerchant_Request_UpdateBusinessDetails", _Exception, _Request.UserReference, "CA0500", "Internal server error occured. Please try after some time.");
            }
        }
        internal OResponse OnboardMerchant_Request_AccountConfiguration(OOnboardingV2.AccountType _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAREF", "ReferenceId Required");
                }
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAREFKEY", "ReferenceKey Required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();

                    if (Details != null)
                    {
                        if (_Request.AccountTypes != null)
                        {
                            foreach (var AccountType in _Request.AccountTypes)
                            {
                                if (AccountType.AccountTypeCodes == "thankucashdeals")
                                {
                                    if (AccountType.Value == "0")
                                    {
                                        _HCUAccountParameter = new HCUAccountParameter();
                                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                        _HCUAccountParameter.AccountId = Details.Id;
                                        _HCUAccountParameter.TypeId = HelperType.ThankUCashDeals;
                                        _HCUAccountParameter.SubTypeId = HelperType.ThankUCashDeal.ProductDeals;
                                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "dealcategory").Select(x => x.Id).FirstOrDefault();
                                        _HCUAccountParameter.Value = AccountType.Value;
                                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                    }
                                    else if (AccountType.Value == "1")
                                    {
                                        _HCUAccountParameter = new HCUAccountParameter();
                                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                        _HCUAccountParameter.AccountId = Details.Id;
                                        _HCUAccountParameter.TypeId = HelperType.ThankUCashDeals;
                                        _HCUAccountParameter.SubTypeId = HelperType.ThankUCashDeal.ServiceDeals;
                                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "dealcategory").Select(x => x.Id).FirstOrDefault();
                                        _HCUAccountParameter.Value = AccountType.Value;
                                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                    }
                                    else
                                    {
                                        _HCUAccountParameter = new HCUAccountParameter();
                                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                        _HCUAccountParameter.AccountId = Details.Id;
                                        _HCUAccountParameter.TypeId = HelperType.ThankUCashDeals;
                                        _HCUAccountParameter.SubTypeId = HelperType.ThankUCashDeal.Both;
                                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "dealcategory").Select(x => x.Id).FirstOrDefault();
                                        _HCUAccountParameter.Value = AccountType.Value;
                                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                    }
                                }

                                if (AccountType.AccountTypeCodes == "thankucashloyalty")
                                {
                                    if (AccountType.ConfigurationValue == "openloyaltymodel")
                                    {
                                        long RewardPercentage = Convert.ToInt64(_HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage" && x.TypeId == HelperType.Configuration).Select(x => x.Value).FirstOrDefault());
                                        _HCUAccountParameter = new HCUAccountParameter();
                                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                        _HCUAccountParameter.AccountId = Details.Id;
                                        _HCUAccountParameter.TypeId = HelperType.ThankUCashLoyalty;
                                        _HCUAccountParameter.SubTypeId = HelperType.ThankUCashLoyalties.OpenLoyalty;
                                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage").Select(x => x.Id).FirstOrDefault();
                                        if (AccountType.Value == null)
                                        {
                                            _HCUAccountParameter.Value = RewardPercentage.ToString();
                                            Details.AccountPercentage = RewardPercentage;
                                        }
                                        else
                                        {
                                            _HCUAccountParameter.Value = AccountType.Value;
                                            Details.AccountPercentage = Convert.ToDouble(AccountType.Value);
                                        }
                                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                    }
                                    else
                                    {

                                        long RewardPercentage = Convert.ToInt64(_HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage" && x.TypeId == HelperType.Configuration).Select(x => x.Value).FirstOrDefault());
                                        _HCUAccountParameter = new HCUAccountParameter();
                                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                        _HCUAccountParameter.AccountId = Details.Id;
                                        _HCUAccountParameter.TypeId = HelperType.ThankUCashLoyalty;
                                        _HCUAccountParameter.SubTypeId = HelperType.ThankUCashLoyalties.ClosedLoyalty;
                                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage").Select(x => x.Id).FirstOrDefault();
                                        if (AccountType.Value == null)
                                        {
                                            _HCUAccountParameter.Value = RewardPercentage.ToString();
                                            Details.AccountPercentage = RewardPercentage;
                                        }
                                        else
                                        {
                                            _HCUAccountParameter.Value = AccountType.Value;
                                            Details.AccountPercentage = Convert.ToDouble(AccountType.Value);
                                        }
                                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);


                                        #region  Close Loyalty
                                        #region Set Closed Loyalty
                                        _HCUAccountParameter = new HCUAccountParameter();
                                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                        _HCUAccountParameter.AccountId = Details.Id;
                                        _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                        _HCUAccountParameter.HelperId = Helpers.DataType.Number;
                                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "thankucashgold").Select(x => x.Id).FirstOrDefault();
                                        _HCUAccountParameter.Value = "1";
                                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                        #endregion

                                        #region Set User Reward % to 100
                                        _HCUAccountParameter = new HCUAccountParameter();
                                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                        _HCUAccountParameter.AccountId = Details.Id;
                                        _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                        _HCUAccountParameter.HelperId = Helpers.DataType.Number;
                                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "userrewardpercentage").Select(x => x.Id).FirstOrDefault();
                                        _HCUAccountParameter.Value = "100";
                                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                        #endregion

                                        #region Set TUC Reward % to 0
                                        _HCUAccountParameter = new HCUAccountParameter();
                                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                        _HCUAccountParameter.AccountId = Details.Id;
                                        _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                        _HCUAccountParameter.HelperId = Helpers.DataType.Number;
                                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardcomissionpercentage").Select(x => x.Id).FirstOrDefault();
                                        _HCUAccountParameter.Value = "0";
                                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                        #endregion
                                        #endregion



                                    }
                                }
                            }
                            _HCoreContext.SaveChanges();
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CASAVE", "Account Configuration Details Saved Successfully.");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0404", "Details not found.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchant_Request_AccountConfiguration", _Exception, _Request.UserReference, "CA0500", "Internal server error occured. Please try after some time.");
            }
        }

        internal OResponse GetMerchantDetails(OReference _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAREF", "ReferenceId Required");
                }
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAREFKEY", "ReferenceKey Required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                          .Select(x => new
                                          {
                                              ReferenceId = x.Id,
                                              ReferenceKey = x.Guid,

                                              DisplayName = x.DisplayName,
                                              Name = x.DisplayName,
                                              MobileNumber = x.MobileNumber,
                                              ContactNumber = x.ContactNumber,
                                              EmailAddress = x.EmailAddress,
                                              IconUrl = x.IconStorage.Path,
                                              RewardPercentage = x.AccountPercentage,

                                              Address = x.Address,
                                              Latitude = x.Latitude,
                                              Longitude = x.Longitude,

                                              CityAreaId = x.CityAreaId,
                                              CityAreaCode = x.Guid,
                                              CityAreaName = x.CityArea.Name,

                                              CityId = x.CityId,
                                              CityCode = x.Guid,
                                              CityName = x.City.Name,

                                              StateId = x.StateId,
                                              StateCode = x.State.Guid,
                                              StateName = x.State.Name,

                                              CountryId = x.CountryId,
                                              CountryCode = x.Guid,
                                              CountryName = x.Country.Name,

                                              StatusId = x.StatusId,
                                              StatusCode = x.Status.SystemName,
                                              StatusName = x.Status.Name,

                                              UserName = x.User.Username,

                                              CreateDate = x.CreateDate,
                                              CreatedByReferenceId = x.CreatedById,
                                              CreatedByReferenceKey = x.CreatedBy.Guid,
                                              CreatedByDisplayName = x.CreatedBy.DisplayName,

                                              ModifyDate = x.ModifyDate,
                                              ModifyByReferenceId = x.ModifyById,
                                              ModifyByReferenceKey = x.ModifyBy.Guid,
                                              ModifyByDisplayName = x.ModifyBy.DisplayName,
                                              FirstName = x.FirstName,
                                              LastName = x.LastName
                                          }).FirstOrDefault();

                    if (MerchantDetails != null)
                    {
                        _MerchantDetails = new OOnboardingV2.MerchantDetails();
                        _MerchantDetails.Category = _HCoreContext.TUCCategoryAccount.Where(x => x.AccountId == MerchantDetails.ReferenceId && x.StatusId == HelperStatus.Default.Active)
                                                    .OrderByDescending(x => x.Id)
                                                    .Select(x => new OOnboardingV2.Category
                                                    {
                                                        ReferenceId = x.Category.Id,
                                                        ReferenceKey = x.Category.Guid,
                                                        Name = x.Category.Name,
                                                        IconUrl = _AppConfig.StorageUrl + x.Category.IconStorage.Path,
                                                    }).FirstOrDefault();
                        _MerchantDetails.Configuration = _HCoreContext.HCUAccountParameter.Where(x => x.AccountId == MerchantDetails.ReferenceId)
                                                    .Select(x => new OOnboardingV2.Configuration
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        TypeId = x.TypeId,
                                                        SubTypeId = x.SubTypeId,
                                                        CommonId = x.CommonId,
                                                        Value = x.Value,
                                                    }).ToList();
                        _MerchantDetails.ContactPerson = _HCoreContext.HCUAccount.Where(x => x.Id == MerchantDetails.ReferenceId && x.Guid == MerchantDetails.ReferenceKey)
                                                         .Select(x => new OOnboardingV2.ContactPerson
                                                         {
                                                             FirstName = x.CpFirstName,
                                                             LastName = x.CpLastName,
                                                             MobileNumber = x.CpMobileNumber,
                                                             EmailAddress = x.CpEmailAddress
                                                         }).FirstOrDefault();

                        _MerchantDetails.ReferenceId = MerchantDetails.ReferenceId;
                        _MerchantDetails.ReferenceKey = MerchantDetails.ReferenceKey;

                        _MerchantDetails.RewardPercentage = MerchantDetails.RewardPercentage;

                        _MerchantDetails.DisplayName = MerchantDetails.DisplayName;
                        _MerchantDetails.Name = MerchantDetails.Name;
                        _MerchantDetails.EmailAddress = MerchantDetails.EmailAddress;
                        _MerchantDetails.MobileNumber = MerchantDetails.MobileNumber;
                        _MerchantDetails.ContactNumber = MerchantDetails.ContactNumber;

                        _MerchantDetails.UserName = MerchantDetails.UserName;
                        _MerchantDetails.FirstName = MerchantDetails.FirstName;
                        _MerchantDetails.LastName = MerchantDetails.LastName;

                        if (!string.IsNullOrEmpty(MerchantDetails.IconUrl))
                        {
                            _MerchantDetails.IconUrl = _AppConfig.StorageUrl + MerchantDetails.IconUrl;
                        }
                        else
                        {
                            _MerchantDetails.IconUrl = _AppConfig.Default_Icon;
                        }

                        _MerchantDetails.CreateDate = MerchantDetails.CreateDate;
                        _MerchantDetails.CreatedById = MerchantDetails.CreatedByReferenceId;
                        _MerchantDetails.CreatedByKey = MerchantDetails.CreatedByReferenceKey;
                        _MerchantDetails.CreatedByDisplayName = MerchantDetails.CreatedByDisplayName;

                        _MerchantDetails.ModifyDate = MerchantDetails.ModifyDate;
                        _MerchantDetails.ModifyById = MerchantDetails.ModifyByReferenceId;
                        _MerchantDetails.ModifyByKey = MerchantDetails.ModifyByReferenceKey;
                        _MerchantDetails.ModifyByDisplayName = MerchantDetails.ModifyByDisplayName;

                        _MerchantDetails.StatusId = _MerchantDetails.StatusId;
                        _MerchantDetails.StatusCode = MerchantDetails.StatusCode;
                        _MerchantDetails.StatusName = MerchantDetails.StatusName;

                        _OAddress = new OAddress();
                        _OAddress.Address = MerchantDetails.Address;

                        _OAddress.CityAreaId = MerchantDetails.CityAreaId;
                        _OAddress.CityAreaCode = MerchantDetails.CityAreaCode;
                        _OAddress.CityAreaName = MerchantDetails.CityAreaName;

                        _OAddress.CityId = MerchantDetails.CityId;
                        _OAddress.CityCode = MerchantDetails.CityCode;
                        _OAddress.CityName = MerchantDetails.CityName;

                        _OAddress.StateId = MerchantDetails.StateId;
                        _OAddress.StateCode = MerchantDetails.StateCode;
                        _OAddress.StateName = MerchantDetails.StateName;

                        _OAddress.CountryId = MerchantDetails.CountryId;
                        _OAddress.CountryCode = MerchantDetails.CountryCode;
                        _OAddress.CountryName = MerchantDetails.CountryName;

                        _OAddress.Latitude = MerchantDetails.Latitude;
                        _OAddress.Longitude = MerchantDetails.Longitude;

                        _MerchantDetails.AddressComponent = _OAddress;
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _MerchantDetails, "CA0200", "Details Loaded Successfully.");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0404", "Details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("", _Exception, _Request.UserReference, "CA0500", "Internal server error occired. Please try after some time.");
            }
        }
        internal OResponse SaveMerchant(OOnboardingV2.BusinessInformation _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Bankdetails.AccountName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0001", "Bank Account Name Required");
                }
                if (_Request.Bankdetails.BankId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0002", "Bank Id Required");
                }
                if (string.IsNullOrEmpty(_Request.Bankdetails.AccountNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0003", "Bank Account Number Required");
                }
                if (_Request.Category.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0002", "Category Id Required");
                }

                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.Address, _Request.UserReference);
                using (_HCoreContext = new HCoreContext())
                {
                    bool ValidateName = _HCoreContext.HCUAccount.Any(x => x.User.Username == _Request.BusinessEmail);
                    if (ValidateName)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1432, TUCCoreResource.CA1432M);
                    }
                    var BusinessNameCheck = _HCoreContext.HCUAccount.Any(x => x.DisplayName == _Request.BusinessName);
                    if (BusinessNameCheck)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "reg005", "Business Name address already used");
                    }

                    _Random = new Random();
                    string AccountCode = _Random.Next(100, 999).ToString() + _Random.Next(000000000, 999999999).ToString();
                    _Request.ReferenceKey = HCoreHelper.GenerateGuid();
                    //_HCUAccountParameters = new List<HCUAccountParameter>();
                    //_HCUAccountParameter = new HCUAccountParameter();
                    //_HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    //_HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                    //_HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage").Select(x => x.Id).FirstOrDefault();
                    ////_HCUAccountParameter.Value = _Request.RewardPercentage.ToString();
                    //_HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                    //_HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    //if (_Request.UserReference.AccountId != 0)
                    //{
                    //    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    //}
                    //_HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                    //_HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                    //_HCUAccountParameters.Add(_HCUAccountParameter);


                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = _Request.BusinessEmail;
                    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash("Rewards@4321");
                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    #region Save UserAccount
                    string[] FullName = _Request.BusinessName.Split(" ");
                    string FirstName = string.Empty, LastName = string.Empty;
                    if (FullName.Length == 0 || FullName.Length == 1)
                    {
                        FirstName = _Request.BusinessName;
                    }
                    else if (FullName.Length == 2)
                    {
                        FirstName = FullName[0];
                        LastName = FullName[1];
                    }
                    else if (FullName.Length == 3)
                    {
                        FirstName = FullName[0];
                        LastName = FullName[2];
                    }
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.Guid = _Request.ReferenceKey;
                    _HCUAccount.AccountTypeId = UserAccountType.Merchant;
                    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                    _HCUAccount.OwnerId = _Request.UserReference.AccountId;
                    //_HCUAccount.BankId = _Request.Bankdetails.BankId;
                    _HCUAccount.DisplayName = _Request.BusinessName;

                    _HCUAccount.Name = _Request.BusinessName;
                    //Updated acc to bug LLT:-57
                    _HCUAccount.FirstName = _Request.ContactPerson.FirstName;
                    _HCUAccount.LastName = _Request.ContactPerson.LastName;
                    _HCUAccount.EmailAddress = _Request.BusinessEmail;
                    _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.countryIsd, _Request.MobileNumber, _Request.MobileNumber.Length);
                    _HCUAccount.ContactNumber = _Request.MobileNumber;
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
                    _HCUAccount.Address = _AddressResponse.Address;
                    _HCUAccount.Latitude = _AddressResponse.Latitude;
                    _HCUAccount.Longitude = _AddressResponse.Longitude;
                    _HCUAccount.CountryId = _AddressResponse.CountryId;
                    if (_Request.ContactPerson != null)
                    {
                        //_HCUAccount.FirstName = _Request.ContactPerson.FirstName;
                        //_HCUAccount.LastName = _Request.ContactPerson.LastName;
                        //_HCUAccount.SecondaryEmailAddress = _Request.ContactPerson.EmailAddress;
                        //_HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.ContactPerson.countryIsd, _Request.ContactPerson.MobileNumber, _Request.ContactPerson.MobileNumber.Length);


                        _HCUAccount.CpFirstName = _Request.ContactPerson.FirstName;
                        _HCUAccount.CpLastName = _Request.ContactPerson.LastName;
                        _HCUAccount.CpEmailAddress = _Request.ContactPerson.EmailAddress;
                        _HCUAccount.CpMobileNumber = _Request.ContactPerson.MobileNumber;
                        _HCUAccount.CpContactNumber = _HCUAccount.CpMobileNumber;
                    }
                    if (_AddressResponse.StateId != 0)
                    {
                        _HCUAccount.StateId = _AddressResponse.StateId;
                    }
                    if (_AddressResponse.CityId != 0)
                    {
                        _HCUAccount.CityId = _AddressResponse.CityId;
                    }
                    if (_AddressResponse.CityAreaId != 0)
                    {
                        _HCUAccount.CityAreaId = _AddressResponse.CityAreaId;
                    }
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccount.StatusId = 2;
                    _HCUAccount.CountryId = _Request.UserReference.CountryId;
                    _HCUAccount.EmailVerificationStatus = 2;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.NumberVerificationStatus = 2;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.DocumentVerificationStatus = 2;
                    //_HCUAccount.PrimaryCategoryId = _Request.Category.ReferenceId;
                    //_HCUAccount.AccountPercentage = _Request.RewardPercentage;
                    _HCUAccount.User = _HCUAccountAuth;
                    _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                    #endregion
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    _HCoreContext.SaveChanges();


                    if (_Request.Category != null)
                    {
                        _HCUAccountParameter = new HCUAccountParameter();
                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountParameter.AccountId = _HCUAccount.Id;
                        _HCUAccountParameter.TypeId = HelperType.MerchantCategory;
                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "businesscategory").Select(a => a.Id).FirstOrDefault();
                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    }

                    #region Set Category
                    //if (_Request.Category != null)
                    //{
                    //    _TUCCategoryAccount = new TUCCategoryAccount();
                    //    _TUCCategoryAccount.Guid = HCoreHelper.GenerateGuid();
                    //    //_TUCCategoryAccount.AccountId = _HCUAccount.Id;
                    //    //_TUCCategoryAccount.TypeId = HelperType.MerchantCategory;
                    //    //_TUCCategoryAccount.CategoryId = _Request.Category.ReferenceId;
                    //    _TUCCategoryAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    //    _TUCCategoryAccount.CreatedById = _Request.UserReference.AccountId;
                    //    //_TUCCategoryAccount.StatusId = HelperStatus.Default.Active;
                    //    _HCoreContext.TUCCategoryAccount.Add(_TUCCategoryAccount);


                    //}
                    #endregion

                    #region Set Bank Details
                    if (_Request.Bankdetails != null)
                    {
                        _HCUAccountBank = new HCUAccountBank();
                        _HCUAccountBank.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountBank.AccountId = _HCUAccount.Id;
                        _HCUAccountBank.Name = _Request.Bankdetails.AccountName;
                        _HCUAccountBank.AccountNumber = _Request.Bankdetails.AccountNumber;
                        _HCUAccountBank.BankName = _Request.Bankdetails.BankName;
                        _HCUAccountBank.BankCode = _Request.Bankdetails.BankCode;
                        _HCUAccountBank.BankId = _Request.Bankdetails.BankId;
                        _HCUAccountBank.ReferenceCode = "RCP_" + HCoreHelper.GenerateGuid();
                        _HCUAccountBank.ReferenceKey = HCoreHelper.GenerateGuid();
                        _HCUAccountBank.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountBank.CreatedById = _Request.UserReference.AccountId;
                        _HCUAccountBank.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.HCUAccountBank.Add(_HCUAccountBank);
                        _HCoreContext.SaveChanges();
                    }
                    #endregion


                    long? ImageStorageId = null;
                    if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                    {
                        ImageStorageId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, null, _Request.UserReference);
                    }
                    if (ImageStorageId != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _HCUAccount.Guid).FirstOrDefault();
                            if (AccountDetails != null)
                            {
                                if (ImageStorageId != null)
                                {
                                    AccountDetails.PosterStorageId = ImageStorageId;
                                }
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }

                    long MerchantId = _HCUAccount.Id;
                    OAddressResponse _StoreAddress = HCoreHelper.GetAddressComponent(_Request.Address, _Request.UserReference);
                    using (_HCoreContext = new HCoreContext())
                    {
                        _HCUAccountAuth = new HCUAccountAuth();
                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(10);
                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                        }
                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                        #region Save UserAccount
                        _HCUAccount = new HCUAccount();
                        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                        _HCUAccount.AccountOperationTypeId = AccountOperationType.Offline;
                        _HCUAccount.OwnerId = MerchantId;
                        //_HCUAccount.BankId = _Request.Bankdetails.BankId;
                        //_HCUAccount.SubOwnerId = _Request.StoreId;
                        _HCUAccount.DisplayName = _Request.BusinessName;
                        _HCUAccount.Name = _Request.BusinessName;
                        _HCUAccount.EmailAddress = _Request.BusinessEmail;
                        _HCUAccount.ContactNumber = _Request.MobileNumber;
                        _HCUAccount.FirstName = _Request.BusinessName;
                        //_HCUAccount.LastName = _Store.ContactPerson.LastName;
                        _HCUAccount.SecondaryEmailAddress = _Request.BusinessEmail;
                        _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.countryIsd, _Request.MobileNumber, _Request.MobileNumber.Length);

                        _HCUAccount.Address = _StoreAddress.Address;
                        _HCUAccount.Latitude = _StoreAddress.Latitude;
                        _HCUAccount.Longitude = _StoreAddress.Longitude;
                        _HCUAccount.CountryId = _StoreAddress.CountryId;
                        if (_StoreAddress.StateId != 0)
                        {
                            _HCUAccount.StateId = _StoreAddress.StateId;
                        }
                        if (_StoreAddress.CityId != 0)
                        {
                            _HCUAccount.CityId = _StoreAddress.CityId;
                        }
                        if (_StoreAddress.CityAreaId != 0)
                        {
                            _HCUAccount.CityAreaId = _StoreAddress.CityAreaId;
                        }
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                        _HCUAccount.AccountCode = _Random.Next(100000000, 999999999).ToString();
                        if (_Request.UserReference.AppVersionId != 0)
                        {
                            _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                        }
                        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                        _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                        }
                        if (_Request.ContactPerson != null)
                        {
                            _HCUAccount.CpFirstName = _Request.ContactPerson.FirstName;
                            _HCUAccount.CpLastName = _Request.ContactPerson.LastName;
                            _HCUAccount.CpEmailAddress = _Request.ContactPerson.EmailAddress;
                            _HCUAccount.CpMobileNumber = _Request.ContactPerson.MobileNumber;
                            _HCUAccount.CpContactNumber = _HCUAccount.CpMobileNumber;
                        }
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                        _HCUAccount.EmailVerificationStatus = 0;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.NumberVerificationStatus = 0;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.User = _HCUAccountAuth;
                        #endregion

                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                        _HCoreContext.SaveChanges();
                    }

                    #region Add new Merchant Email Address to Mailerlite
                    dynamic _Response = null;
                    var newEmail = new CreateListRequest
                    {
                        name = "New Onboarded Merchant",
                        emails = new string[] { _Request.BusinessEmail }
                    };
                    MailerliteImple mailerlite = new MailerliteImple();

                    var jsonResponse = mailerlite.AddEmailToMailerList(newEmail);
                    if (jsonResponse != null)
                        if (jsonResponse.StatusCode.Equals(StatusCodes.Status201Created))
                        {


                            _Response = new
                            {
                                ReferenceId = MerchantId,
                                ReferenceKey = _Request.ReferenceKey,
                            };
                            _FrameworkSubscription = new HCore.TUC.Core.Framework.Operations.FrameworkSubscription();
                            _FrameworkSubscription.SetPartnerReferralMerchantSubscription(MerchantId);


                        }


                    _Response = new
                    {
                        ReferenceId = MerchantId,
                        ReferenceKey = _Request.ReferenceKey,
                    };
                    _FrameworkSubscription = new HCore.TUC.Core.Framework.Operations.FrameworkSubscription();
                    _FrameworkSubscription.SetPartnerReferralMerchantSubscription(MerchantId);

                    #endregion
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA14160, TUCCoreResource.CAA14160M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveMerchant", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        internal OResponse GetOnboardMerchants(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant && x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new OOnboardingV2.OnboardMerchants
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.DisplayName,
                                                    EmailAddress = x.EmailAddress,
                                                    MobileNumber = x.MobileNumber,
                                                    ContactNumber = x.ContactNumber,
                                                    IconUrl = x.IconStorage.Path,
                                                    PosterUrl = x.PosterStorage.Path,
                                                    Address = x.Address,
                                                    DocumentVerificationStatus = x.DocumentVerificationStatus,
                                                    DocumentVerificationStatusDate = x.DocumentVerificationStatusDate,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();
                    }
                    List<OOnboardingV2.OnboardMerchants> _Merchants = _HCoreContext.HCUAccount
                                                             .Where(x => x.AccountTypeId == UserAccountType.Merchant && x.StatusId == HelperStatus.Default.Active)
                                                                       .Select(x => new OOnboardingV2.OnboardMerchants
                                                                       {
                                                                           ReferenceId = x.Id,
                                                                           ReferenceKey = x.Guid,
                                                                           Name = x.DisplayName,
                                                                           EmailAddress = x.EmailAddress,
                                                                           MobileNumber = x.MobileNumber,
                                                                           ContactNumber = x.ContactNumber,
                                                                           IconUrl = x.IconStorage.Path,
                                                                           PosterUrl = x.PosterStorage.Path,
                                                                           Address = x.Address,
                                                                           DocumentVerificationStatus = x.DocumentVerificationStatus,
                                                                           DocumentVerificationStatusDate = x.DocumentVerificationStatusDate,

                                                                           StatusId = x.StatusId,
                                                                           StatusCode = x.Status.SystemName,
                                                                           StatusName = x.Status.Name,

                                                                           CreateDate = x.CreateDate,
                                                                           CreatedById = x.CreatedById,
                                                                           CreatedByKey = x.CreatedBy.Guid,
                                                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                           ModifyDate = x.ModifyDate,
                                                                           ModifyById = x.ModifyById,
                                                                           ModifyByKey = x.ModifyBy.Guid,
                                                                           ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                                       })
                                                                       .Where(_Request.SearchCondition)
                                                                       .OrderBy(_Request.SortExpression)
                                                                       .Skip(_Request.Offset)
                                                                       .Take(_Request.Limit)
                                                                       .ToList();
                    foreach (var Merchant in _Merchants)
                    {
                        if (!string.IsNullOrEmpty(Merchant.IconUrl))
                        {
                            Merchant.IconUrl = _AppConfig.StorageUrl + Merchant.IconUrl;
                        }
                        else
                        {
                            Merchant.IconUrl = _AppConfig.StorageUrl + _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Merchant.PosterUrl))
                        {
                            Merchant.PosterUrl = _AppConfig.StorageUrl + Merchant.PosterUrl;
                        }
                        else
                        {
                            Merchant.PosterUrl = _AppConfig.StorageUrl + _AppConfig.Default_Icon;
                        }

                        Merchant.Bankdetails = _HCoreContext.HCUAccountBank.Where(x => x.AccountId == Merchant.ReferenceId)
                                               .Select(x => new OOnboardingV2.BankDetails
                                               {
                                                   AccountName = x.Account.DisplayName,
                                                   AccountNumber = x.AccountNumber,
                                                   BankId = x.Id,
                                                   BankKey = x.Guid,
                                                   BankName = x.BankName,
                                                   BankCode = x.BankCode
                                               }).FirstOrDefault();
                        //Merchant.Documents = _HCoreContext.HCCoreStorage.Where(x => x.AccountId == Merchant.ReferenceId)
                        //                     .Select(x => new OOnboardingV2.Documents
                        //                     {
                        //                         DocumentUrl = _AppConfig.StorageUrl + x.Path
                        //                     }).ToList();
                        //Merchant.Documents = _HCoreContext.HCCoreStorage.Where(x => x.AccountId == Merchant.ReferenceId)
                        //                     .Select(x => new OOnboardingV2.Documents
                        //                     {
                        //                         DocumentUrl = _AppConfig.StorageUrl + x.Path
                        //                     }).FirstOrDefault();
                    }

                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Merchants, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetOnboardMerchants", _Exception, _Request.UserReference, "CA0500", "Internal server error occured. Please try after some time.");
            }
        }

        internal OResponse Merchant_Onboarding_VerifyDocument(VerifyDocument _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAREF", "ReferenceId Required");
                }
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAREFKEY", "ReferenceKey Required");
                }

                int? StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);

                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (MerchantDetails != null)
                    {
                        if (_Request.VerifyDocuments == 2)
                        {
                            if (MerchantDetails.DocumentVerificationStatus != _Request.VerifyDocuments && _Request.VerifyDocuments != null)
                            {
                                MerchantDetails.DocumentVerificationStatus = _Request.VerifyDocuments;
                            }
                            if (MerchantDetails.StatusId != StatusId && StatusId > 0)
                            {
                                MerchantDetails.StatusId = (int)StatusId;
                            }

                            MerchantDetails.DocumentVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                            MerchantDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            MerchantDetails.ModifyById = _Request.UserReference.AccountId;
                            _HCoreContext.SaveChanges();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CAUPDATE", "Merchant document verification successfully.");
                        }
                        else
                        {
                            if (MerchantDetails.DocumentVerificationStatus != _Request.VerifyDocuments && _Request.VerifyDocuments != null)
                            {
                                MerchantDetails.DocumentVerificationStatus = _Request.VerifyDocuments;
                            }
                            if (MerchantDetails.StatusId != StatusId && StatusId > 0)
                            {
                                MerchantDetails.StatusId = (int)StatusId;
                            }

                            MerchantDetails.DocumentVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                            MerchantDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            MerchantDetails.ModifyById = _Request.UserReference.AccountId;
                            _HCoreContext.SaveChanges();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAUPDATE", "Merchant document verification denied.");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0404", "Merchant details not found.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("Merchant_Onboarding_VerifyDocument", _Exception, _Request.UserReference, "CA0500", "Internal server error occured. Please try after some time.");
            }
        }
    }
}
