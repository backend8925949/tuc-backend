//==================================================================================
// FileName: FrameworkOnboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to onboarding
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Operations;
using HCore.Data.Operations.Models;
using HCore.Helper;
using HCore.Integration.Mailerlite;
using HCore.Integration.Mailerlite.Requests;
using HCore.Integration.Mailerlite.Responses;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Core.Framework.Merchant.Actor;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Resource;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.TUC.Core.Helper.HelperEmailTemplate;

namespace HCore.TUC.Core.Framework.Merchant
{
    public class FrameworkOnboarding
    {
        List<TUCCategoryAccount> _TUCCategoryAccount;
        Random _Random;
        List<HCUAccountParameter> _HCUAccountParameters;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        HCUAccountParameter _HCUAccountParameter;
        HCoreContext _HCoreContext;
        HCoreContextOperations _HCoreContextOperations;
        HCTMerchantOnboarding _HCTMerchantOnboarding;
        OCoreVerificationManager.Request _VerificationRequest;
        ManageCoreVerification _ManageCoreVerification;
        OCoreVerificationManager.RequestVerify _VerifyRequest;
        HCUAccountSubscription _HCUAccountSubscription;

        // Requiest Email Verification
        /// <summary>
        /// Description: Onboard merchant ST1.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse OnboardMerchant_St1(OOnboarding.St1.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1405, TUCCoreResource.CA1405M);
                }
                if (string.IsNullOrEmpty(_Request.Source))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1416, TUCCoreResource.CA1416M);
                }
                if (HCoreHelper.ValidateEmailAddress(_Request.EmailAddress) == false)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1408, TUCCoreResource.CA1408M);
                }
                if (string.IsNullOrEmpty(_Request.Password))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1406, TUCCoreResource.CA1406M);
                }
                if (_Request.Password.Length < 6)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1407, TUCCoreResource.CA1407M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var UserNameCheck = _HCoreContext.HCUAccountAuth.Any(x => x.Username == _Request.EmailAddress);
                    if (UserNameCheck)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1409, TUCCoreResource.CA1409M);
                    }
                    _HCoreContext.Dispose();
                    using (_HCoreContextOperations = new HCoreContextOperations())
                    {
                        var CheckOnboarding = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.EmailAddress == _Request.EmailAddress).FirstOrDefault();
                        if (CheckOnboarding != null)
                        {
                            if (CheckOnboarding.StatusId == 2)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1431, TUCCoreResource.CA1431M);
                            }
                            else
                            {
                                string Link1 = "";
                                if (_Request.Source == "web")
                                {
                                    if (HostEnvironment == HostEnvironmentType.Test)
                                    {
                                        Link1 = "https://test.thankucash.com/merchant-email-verified.html?token=" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(CheckOnboarding.Id + "|" + CheckOnboarding.Guid));
                                    }
                                    else
                                    {
                                        Link1 = "https://thankucash.com/merchant-email-verified.html?token=" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(CheckOnboarding.Id + "|" + CheckOnboarding.Guid));
                                    }
                                }
                                else
                                {
                                    if (HostEnvironment == HostEnvironmentType.Test)
                                    {
                                        Link1 = "https://" + _Request.Host + "/account/verifyemail/" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(CheckOnboarding.Id + "|" + CheckOnboarding.Guid));
                                    }
                                    else
                                    {
                                        Link1 = "https://" + _Request.Host + "/account/verifyemail/" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(CheckOnboarding.Id + "|" + CheckOnboarding.Guid));
                                    }
                                }
                                var EmailObject1 = new
                                {
                                    Link = Link1
                                };
                                var _Response1 = new
                                {
                                    Reference = CheckOnboarding.Guid,
                                };
                                HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantOnboardingEmailVerification, "TUC Merchant", _Request.EmailAddress, EmailObject1, _Request.UserReference);
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response1, TUCCoreResource.CA1063, TUCCoreResource.CA1063M);
                            }
                        }
                        _HCTMerchantOnboarding = new HCTMerchantOnboarding();
                        _HCTMerchantOnboarding.Guid = HCoreHelper.GenerateGuid();
                        _HCTMerchantOnboarding.EmailAddress = _Request.EmailAddress;
                        _HCTMerchantOnboarding.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                        _HCTMerchantOnboarding.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCTMerchantOnboarding.SubscriptionKey = _Request.SubscriptionKey;
                        _HCTMerchantOnboarding.StatusId = 1;
                        _HCoreContextOperations.HCTMerchantOnboarding.Add(_HCTMerchantOnboarding);
                        _HCoreContextOperations.SaveChanges();
                        string Link = "";
                        if (_Request.Source == "web")
                        {
                            if (HostEnvironment == HostEnvironmentType.Test)
                            {
                                Link = "https://test.thankucash.com/merchant-email-verified.html?token=" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(_HCTMerchantOnboarding.Id + "|" + _HCTMerchantOnboarding.Guid));
                            }
                            else
                            {
                                Link = "https://thankucash.com/merchant-email-verified.html?token=" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(_HCTMerchantOnboarding.Id + "|" + _HCTMerchantOnboarding.Guid));
                            }
                        }
                        else
                        {
                            if (HostEnvironment == HostEnvironmentType.Test)
                            {
                                Link = "https://" + _Request.Host + "/account/verifyemail/" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(_HCTMerchantOnboarding.Id + "|" + _HCTMerchantOnboarding.Guid));
                            }
                            else
                            {
                                Link = "https://" + _Request.Host + "/account/verifyemail/" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(_HCTMerchantOnboarding.Id + "|" + _HCTMerchantOnboarding.Guid));
                            }
                        }
                        var EmailObject = new
                        {
                            Link = Link
                        };
                        var _Response = new
                        {
                            Reference = _HCTMerchantOnboarding.Guid,
                        };
                        HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantOnboardingEmailVerification, "TUC Merchant", _Request.EmailAddress, EmailObject, _Request.UserReference);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1063, TUCCoreResource.CA1063M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchant_St1", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        // Resend Email Verification
        /// <summary>
        /// Description: Onboard merchant ST2 resend email.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse OnboardMerchant_St2_ResendEmail(OOnboarding.St2.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1405, TUCCoreResource.CA1405M);
                }
                if (string.IsNullOrEmpty(_Request.Source))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1416, TUCCoreResource.CA1416M);
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var Details = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Guid == _Request.Reference).FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.IsEmailVerified != 1)
                        {
                            string Link = "https://" + _Request.Host + "/account/verifyemail/" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(Details.Id + "|" + Details.Guid));
                            //if (HostEnvironment == HostEnvironmentType.Test)
                            //{
                            //    Link = "https://" + _Request.Host + "/account/verifyemail/" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(Details.Id + "|" + Details.Guid));
                            //}
                            if (_Request.Source == "web")
                            {
                                if (HostEnvironment == HostEnvironmentType.Test)
                                {
                                    Link = "https://test.thankucash.com/merchant-email-verified.html?token=" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(Details.Id + "|" + Details.Guid));
                                }
                                else
                                {
                                    Link = "https://thankucash.com/merchant-email-verified.html?token=" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(Details.Id + "|" + Details.Guid));
                                }
                            }
                            else
                            {
                                if (HostEnvironment == HostEnvironmentType.Test)
                                {
                                    Link = "https://" + _Request.Host + "/account/verifyemail/" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(Details.Id + "|" + Details.Guid));
                                }
                                else
                                {
                                    Link = "https://" + _Request.Host + "/account/verifyemail/" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(Details.Id + "|" + Details.Guid));
                                }
                            }

                            var EmailObject = new
                            {
                                Link = Link
                            };
                            var _Response = new
                            {
                                Reference = Details.Guid,
                            };
                            HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantOnboardingEmailVerification, "TUC Merchant", Details.EmailAddress, EmailObject, _Request.UserReference);
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1412, TUCCoreResource.CA1412M);

                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1411, TUCCoreResource.CA1411M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1410, TUCCoreResource.CA1410M);
                    }

                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchant_St1", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        // Verify Email Address
        /// <summary>
        /// Description: Onboard merchant ST3 verify email.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse OnboardMerchant_St3_VerifyEmail(OOnboarding.St3.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Token))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1413, TUCCoreResource.CA1413M);
                }
                if (string.IsNullOrEmpty(_Request.Source))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1416, TUCCoreResource.CA1416M);
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    if (!_Request.Token.EndsWith("=="))
                    {
                        _Request.Token = _Request.Token + "==";
                    }
                    string TokenParameter = HCoreEncrypt.DecryptHash(HCoreEncrypt.DecodeText(_Request.Token));
                    string[] TokenParameterItems = TokenParameter.Split('|');
                    long ReferenceId = Convert.ToInt64(TokenParameterItems[0]);
                    string ReferenceKey = TokenParameterItems[1].ToString();
                    var Details = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Id == ReferenceId && x.Guid == ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        Details.EmailVerificationDate = HCoreHelper.GetGMTDateTime();
                        Details.IsEmailVerified = 1;
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContextOperations.SaveChanges();
                        var _Response = new
                        {
                            Reference = Details.Guid,
                            EmailAddress = Details.EmailAddress,
                            SubscriptionKey = Details.SubscriptionKey
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1415, TUCCoreResource.CA1415M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1414, TUCCoreResource.CA1414M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchant_St1", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        // Request Mobile  Verification
        /// <summary>
        /// Description: Onboard merchant ST4 request mobile verification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse OnboardMerchant_St4_RequestMobileVerification(OOnboarding.St4.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1427, TUCCoreResource.CA1427M);
                }
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1417, TUCCoreResource.CA1417M);
                }
                if (string.IsNullOrEmpty(_Request.Source))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1416, TUCCoreResource.CA1416M);
                }
                if (string.IsNullOrEmpty(_Request.Isd))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1418, TUCCoreResource.CA1418M);
                }
                if (_Request.Isd != "234" && _Request.Isd != "233")
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1419, TUCCoreResource.CA1419M);
                }
                long CountryId = 1;
                using (_HCoreContext = new HCoreContext())
                {
                    CountryId = _HCoreContext.HCCoreCountry.Where(x => x.Isd == _Request.Isd).Select(x => x.Id).FirstOrDefault();
                    if (CountryId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1419, TUCCoreResource.CA1419M);
                    }
                    string MobileNumber = HCoreHelper.FormatMobileNumber(_Request.Isd, _Request.MobileNumber);
                    bool MobileNumberCheck = _HCoreContext.HCUAccount.Any(x => x.MobileNumber == MobileNumber && x.AccountTypeId == UserAccountType.Merchant);
                    if (MobileNumberCheck)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1420, TUCCoreResource.CA1420M);
                    }
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    string MobileNumber = HCoreHelper.FormatMobileNumber(_Request.Isd, _Request.MobileNumber);
                    var Details = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Guid == _Request.Reference).FirstOrDefault();
                    if (Details != null)
                    {
                        Details.MobileNumber = _Request.MobileNumber;
                        Details.CountryId = CountryId;
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContextOperations.SaveChanges();

                        #region Request Verification
                        _VerificationRequest = new OCoreVerificationManager.Request();
                        _VerificationRequest.CountryIsd = _Request.UserReference.CountryIsd;
                        _VerificationRequest.Type = 1;
                        _VerificationRequest.MobileNumber = _Request.MobileNumber;
                        _VerificationRequest.UserReference = _Request.UserReference;
                        _ManageCoreVerification = new ManageCoreVerification();
                        var VerificationResponse = _ManageCoreVerification.RequestOtp(_VerificationRequest);
                        #endregion
                        if (VerificationResponse.Status == StatusSuccess)
                        {
                            OCoreVerificationManager.Response VerificationResponseItem = (OCoreVerificationManager.Response)VerificationResponse.Result;
                            using (_HCoreContextOperations = new HCoreContextOperations())
                            {
                                var UDetails = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Id == Details.Id).FirstOrDefault();
                                UDetails.MobileVerificationToken = VerificationResponseItem.RequestToken;
                                _HCoreContextOperations.SaveChanges();
                            }
                        }

                        var _Response = new
                        {
                            Reference = Details.Guid,
                            EmailAddress = Details.EmailAddress,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1415, TUCCoreResource.CA1415M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1414, TUCCoreResource.CA1414M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchant_St4_Request", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        // Verify Mobile  Verification
        /// <summary>
        /// Description: Onboard merchant ST5 verify mobile number.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse OnboardMerchant_St5_VerifyMobileNumber(OOnboarding.St5.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1405, TUCCoreResource.CA1405M);
                }
                if (string.IsNullOrEmpty(_Request.Otp))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1421, TUCCoreResource.CA1421M);
                }
                if (string.IsNullOrEmpty(_Request.Source))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1416, TUCCoreResource.CA1416M);
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var Details = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Guid == _Request.Reference).FirstOrDefault();
                    if (Details != null)
                    {
                        #region Request Verification
                        _VerifyRequest = new OCoreVerificationManager.RequestVerify();
                        _VerifyRequest.RequestToken = Details.MobileVerificationToken;
                        _VerifyRequest.AccessCode = _Request.Otp;
                        _VerifyRequest.UserReference = _Request.UserReference;
                        _ManageCoreVerification = new ManageCoreVerification();
                        var VerificationResponse = _ManageCoreVerification.VerifyOtp(_VerifyRequest);
                        #endregion
                        if (VerificationResponse.Status == StatusSuccess)
                        {
                            OCoreVerificationManager.Response VerificationResponseItem = (OCoreVerificationManager.Response)VerificationResponse.Result;
                            using (_HCoreContextOperations = new HCoreContextOperations())
                            {
                                var AccountDetails = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Guid == _Request.Reference).FirstOrDefault();
                                AccountDetails.IsMobileVerified = 1;
                                AccountDetails.MobileVerificationDate = HCoreHelper.GetGMTDateTime();
                                AccountDetails.StatusId = HelperStatus.Default.Active;
                                _HCoreContextOperations.SaveChanges();
                            }
                            var _Response = new
                            {
                                Reference = Details.Guid,
                                EmailAddress = Details.EmailAddress,
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1422, TUCCoreResource.CA1422M);
                        }
                        else
                        {
                            return VerificationResponse;
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1414, TUCCoreResource.CA1414M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchant_St4_Request", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        // End Onboarding
        /// <summary>
        /// Description: Onboard merchant ST6 complete setup.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse OnboardMerchant_St6_CompleteSetup(OOnboarding.St6.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1405, TUCCoreResource.CA1405M);
                }
                if (string.IsNullOrEmpty(_Request.Source))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1416, TUCCoreResource.CA1416M);
                }
                if (string.IsNullOrEmpty(_Request.DisplayName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1423, TUCCoreResource.CA1423M);
                }
                if (string.IsNullOrEmpty(_Request.SubscriptionKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1424, TUCCoreResource.CA1424M);
                }
                if (_Request.Categories == null || _Request.Categories.Count == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1425, TUCCoreResource.CA1425M);
                }
                if (_Request.Address == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1426, TUCCoreResource.CA1426M);
                }
                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.Address, _Request.UserReference);
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    long MerchantId = 0;
                    var Details = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Guid == _Request.Reference).FirstOrDefault();
                    if (Details != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            string SysPassword = HCoreHelper.GenerateGuid();
                            _Random = new Random();
                            string AccountCode = _Random.Next(100, 999).ToString() + _Random.Next(000000000, 999999999).ToString();
                            string ReferenceKey = HCoreHelper.GenerateGuid();
                            long RewardPercentage = Convert.ToInt64(_HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage" && x.TypeId == HelperType.Configuration).Select(x => x.Value).FirstOrDefault());
                            _HCUAccountParameters = new List<HCUAccountParameter>();
                            _HCUAccountParameter = new HCUAccountParameter();
                            _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                            _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewarddeductiontype" && x.TypeId == HelperType.Configuration).Select(x => x.Id).FirstOrDefault();
                            _HCUAccountParameter.Value = "Prepay";
                            _HCUAccountParameter.HelperId = 253;
                            _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                            _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                            _HCUAccountParameters.Add(_HCUAccountParameter);
                            _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                            _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage" && x.TypeId == HelperType.Configuration).Select(x => x.Id).FirstOrDefault();
                            _HCUAccountParameter.Value = RewardPercentage.ToString();
                            _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                            _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                            }
                            _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                            _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                            _HCUAccountParameters.Add(_HCUAccountParameter);

                            _HCUAccountAuth = new HCUAccountAuth();
                            _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountAuth.Username = Details.EmailAddress;
                            _HCUAccountAuth.Password = Details.Password;
                            _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                            _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(SysPassword);
                            _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                            #region Save UserAccount
                            _HCUAccount = new HCUAccount();
                            _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                            //if (_Request.Categories != null && _Request.Categories.Count > 0)
                            //{
                            //    _HCUAccountParameters = new List<HCUAccountParameter>();
                            //    foreach (var Category in _Request.Categories)
                            //    {
                            //        HCUAccountParameter _HCUAccountParameterItem = new HCUAccountParameter
                            //        {
                            //            Guid = HCoreHelper.GenerateGuid(),
                            //            TypeId = HelperType.MerchantCategory,
                            //            CommonId = Category,
                            //            CreateDate = HCoreHelper.GetGMTDateTime(),
                            //            StatusId = HelperStatus.Default.Active,
                            //        };
                            //        _HCUAccount.HCUAccountParameterAccount.Add(_HCUAccountParameterItem);
                            //    }
                            //}

                            #region Set Categories
                            if (_Request.Categories != null && _Request.Categories.Count > 0)
                            {
                                _TUCCategoryAccount = new List<TUCCategoryAccount>();
                                foreach (var Category in _Request.Categories)
                                {
                                    _TUCCategoryAccount.Add(new TUCCategoryAccount
                                    {
                                        Guid = HCoreHelper.GenerateGuid(),
                                        TypeId = 1,
                                        CategoryId = Category,
                                        CreateDate = HCoreHelper.GetGMTDateTime(),
                                        CreatedById = 1,
                                        StatusId = HelperStatus.Default.Active,
                                    });
                                }
                                _HCUAccount.TUCCategoryAccountAccount = _TUCCategoryAccount;
                            }
                            #endregion
                            _HCUAccount.Guid = ReferenceKey;
                            _HCUAccount.AccountTypeId = UserAccountType.Merchant;
                            _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                            _HCUAccount.OwnerId = 3;
                            _HCUAccount.DisplayName = _Request.DisplayName;
                            _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                            _HCUAccount.Name = _Request.DisplayName;
                            _HCUAccount.EmailAddress = Details.EmailAddress;
                            _HCUAccount.ContactNumber = Details.MobileNumber;
                            if (!string.IsNullOrEmpty(_Request.ReferralCode))
                            {
                                long ReferrerId = _HCoreContext.HCUAccount
                                    .Where(x => x.ReferralCode == _Request.ReferralCode
                                    && (x.AccountTypeId == UserAccountType.Merchant || x.AccountTypeId == UserAccountType.Acquirer || x.AccountTypeId == UserAccountType.PgAccount || x.AccountTypeId == UserAccountType.PosAccount))
                                    .Select(x => x.Id).FirstOrDefault();
                                if (ReferrerId > 0)
                                {
                                    _HCUAccount.OwnerId = ReferrerId;
                                }
                            }
                            _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                            _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
                            _HCUAccount.MobileNumber = Details.MobileNumber;
                            _HCUAccount.Address = _AddressResponse.Address;
                            _HCUAccount.Latitude = _AddressResponse.Latitude;
                            _HCUAccount.Longitude = _AddressResponse.Longitude;
                            _HCUAccount.CountryId = _AddressResponse.CountryId;
                            if (_AddressResponse.StateId != 0)
                            {
                                _HCUAccount.StateId = _AddressResponse.StateId;
                            }
                            if (_AddressResponse.CityId != 0)
                            {
                                _HCUAccount.CityId = _AddressResponse.CityId;
                            }
                            if (_Request.UserReference.AppVersionId != 0)
                            {
                                _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                            }
                            _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                            _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.Website;
                            _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccount.StatusId = HelperStatus.Default.Active;
                            if (_AddressResponse.CountryId > 0)
                            {
                                _HCUAccount.CountryId = _AddressResponse.CountryId;
                            }
                            else
                            {
                                _HCUAccount.CountryId = _Request.UserReference.SystemCountry;
                            }
                            _HCUAccount.EmailVerificationStatus = 1;
                            _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccount.NumberVerificationStatus = 1;
                            _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccount.AccountPercentage = RewardPercentage;
                            _HCUAccount.User = _HCUAccountAuth;
                            #endregion
                            _HCoreContext.HCUAccount.Add(_HCUAccount);
                            _HCoreContext.SaveChanges();
                            MerchantId = _HCUAccount.Id;
                            var EmailObject = new
                            {
                                EmailAddress = _HCUAccount.EmailAddress,
                                UserName = _HCUAccountAuth.Username,
                                Password = HCoreEncrypt.DecryptHash(Details.Password),
                                DisplayName = _HCUAccount.DisplayName,
                            };
                            using (_HCoreContext = new HCoreContext())
                            {
                                #region  Close Loyalty
                                #region Set Closed Loyalty
                                _HCUAccountParameter = new HCUAccountParameter();
                                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountParameter.AccountId = MerchantId;
                                _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                _HCUAccountParameter.HelperId = Helpers.DataType.Number;
                                _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "thankucashgold").Select(x => x.Id).FirstOrDefault();
                                _HCUAccountParameter.Value = "1";
                                _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                #endregion

                                #region Set User Reward % to 100
                                _HCUAccountParameter = new HCUAccountParameter();
                                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountParameter.AccountId = MerchantId;
                                _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                _HCUAccountParameter.HelperId = Helpers.DataType.Number;
                                _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "userrewardpercentage").Select(x => x.Id).FirstOrDefault();
                                _HCUAccountParameter.Value = "100";
                                _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                #endregion

                                #region Set TUC Reward % to 0
                                _HCUAccountParameter = new HCUAccountParameter();
                                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountParameter.AccountId = MerchantId;
                                _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                _HCUAccountParameter.HelperId = Helpers.DataType.Number;
                                _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardcomissionpercentage").Select(x => x.Id).FirstOrDefault();
                                _HCUAccountParameter.Value = "0";
                                _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                #endregion
                                #endregion
                                _HCoreContext.SaveChanges();
                            }
                            using (_HCoreContext = new HCoreContext())
                            {
                                _HCUAccountAuth = new HCUAccountAuth();
                                _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(10);
                                _HCUAccountAuth.Password = HCoreHelper.GenerateRandomNumber(6);
                                _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountAuth.CreatedById = MerchantId;
                                _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                #region Save UserAccount
                                _HCUAccount = new HCUAccount();
                                _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                                _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                                _HCUAccount.OwnerId = MerchantId;
                                _HCUAccount.DisplayName = _Request.DisplayName;
                                _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                                _HCUAccount.Name = _Request.DisplayName;
                                _HCUAccount.EmailAddress = Details.EmailAddress;
                                _HCUAccount.ContactNumber = Details.MobileNumber;
                                _HCUAccount.MobileNumber = Details.MobileNumber;
                                _HCUAccount.Address = _AddressResponse.Address;
                                _HCUAccount.Latitude = _AddressResponse.Latitude;
                                _HCUAccount.Longitude = _AddressResponse.Longitude;
                                if (_AddressResponse.CountryId > 0)
                                {
                                    _HCUAccount.CountryId = _AddressResponse.CountryId;
                                }
                                else
                                {
                                    _HCUAccount.CountryId = _Request.UserReference.SystemCountry;
                                }
                                if (_AddressResponse.StateId != 0)
                                {
                                    _HCUAccount.StateId = _AddressResponse.StateId;
                                }
                                if (_AddressResponse.CityId != 0)
                                {
                                    _HCUAccount.CityId = _AddressResponse.CityId;
                                }
                                _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                                _HCUAccount.AccountCode = _Random.Next(100000000, 999999999).ToString();
                                if (_Request.UserReference.AppVersionId != 0)
                                {
                                    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                                }
                                _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                                _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                                _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                if (_Request.UserReference.AccountId != 0)
                                {
                                    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                }
                                _HCUAccount.StatusId = HelperStatus.Default.Active;
                                _HCUAccount.EmailVerificationStatus = 0;
                                _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccount.NumberVerificationStatus = 0;
                                _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccount.User = _HCUAccountAuth;
                                #endregion
                                _HCoreContext.HCUAccount.Add(_HCUAccount);
                                _HCoreContext.SaveChanges();
                            }
                            using (_HCoreContext = new HCoreContext())
                            {
                                var SubscriptionDetails = _HCoreContext.HCSubscription.Where(x => x.Guid == _Request.SubscriptionKey).FirstOrDefault();
                                if (SubscriptionDetails != null)
                                {
                                    if (SubscriptionDetails.SellingPrice > 0 && string.IsNullOrEmpty(_Request.PaymentReference))
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1085, TUCCoreResource.CA1085M);
                                    }
                                    _HCUAccountSubscription = new HCUAccountSubscription();
                                    _HCUAccountSubscription.Guid = HCoreHelper.GenerateGuid();
                                    _HCUAccountSubscription.TypeId = SubscriptionDetails.TypeId;
                                    _HCUAccountSubscription.AccountId = MerchantId;
                                    _HCUAccountSubscription.SubscriptionId = SubscriptionDetails.Id;
                                    _HCUAccountSubscription.StartDate = HCoreHelper.GetGMTDate();
                                    _HCUAccountSubscription.EndDate = _HCUAccountSubscription.StartDate.AddDays(SubscriptionDetails.TotalDays);
                                    _HCUAccountSubscription.RenewDate = _HCUAccountSubscription.EndDate.AddDays(1);
                                    _HCUAccountSubscription.ActualPrice = SubscriptionDetails.ActualPrice;
                                    _HCUAccountSubscription.SellingPrice = SubscriptionDetails.SellingPrice;
                                    _HCUAccountSubscription.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccountSubscription.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.HCUAccountSubscription.Add(_HCUAccountSubscription);
                                    _HCoreContext.SaveChanges();
                                    if (SubscriptionDetails.Id == 3
                                        || SubscriptionDetails.Id == 5
                                        || SubscriptionDetails.Id == 7
                                        || SubscriptionDetails.Id == 9
                                        || SubscriptionDetails.Id == 11
                                        || SubscriptionDetails.Id == 13
                                        || SubscriptionDetails.Id == 15
                                        || SubscriptionDetails.Id == 17
                                        || SubscriptionDetails.Id == 19
                                        || SubscriptionDetails.Id == 21
                                        )
                                    {
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x =>
                                              x.SystemName == "thankucashgold"
                                              && x.StatusId == HCoreConstant.HelperStatus.Default.Active
                                              && x.TypeId == HCoreConstant.HelperType.Configuration
                                              ).Select(x => new
                                              {
                                                  SystemName = x.SystemName,
                                                  HelperId = x.HelperId,
                                                  Value = x.Value,
                                                  ConfigurationId = x.Id,
                                              }).FirstOrDefault();
                                            if (ConfigurationDetails != null)
                                            {
                                                _HCUAccountParameter = new HCUAccountParameter();
                                                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                                _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                                _HCUAccountParameter.AccountId = MerchantId;
                                                _HCUAccountParameter.CommonId = ConfigurationDetails.ConfigurationId;
                                                _HCUAccountParameter.Value = "1";
                                                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                                _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                                                _HCUAccountParameter.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                                _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                                _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                                _HCoreContext.SaveChanges();
                                            }
                                        }
                                    }

                                    using (_HCoreContextOperations = new HCoreContextOperations())
                                    {
                                        var ExistingAcc = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Guid == _Request.Reference).FirstOrDefault();
                                        if (ExistingAcc != null)
                                        {
                                            ExistingAcc.StatusId = 2;
                                            ExistingAcc.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            _HCoreContextOperations.SaveChanges();
                                        }
                                    }
                                    HCoreHelper.BroadCastEmail(MerchantOnboarding.WelcomeEmail, EmailObject.DisplayName, EmailObject.EmailAddress, EmailObject, _Request.UserReference);
                                    var ReqObject = new
                                    {
                                        UserName = Details.EmailAddress,
                                        AuthToken = SysPassword,
                                    };
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ReqObject, TUCCoreResource.CA1104, TUCCoreResource.CA1104M);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1103, TUCCoreResource.CA1103M);
                                }
                            }

                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1414, TUCCoreResource.CA1414M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchant_St4_Request", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Onboard customers.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse OnboardCustomers(OOnboarding.Customer.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1070, TUCCoreResource.CA1070M);
                }
                if (_Request.Data == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1068, TUCCoreResource.CA1068M);
                }
                if (_Request.Data.Count() < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1069, TUCCoreResource.CA1069M);
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var system = ActorSystem.Create("ActorOnboardCustomer");
                    var greeter = system.ActorOf<ActorOnboardCustomer>("ActorOnboardCustomer");
                    greeter.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1071, TUCCoreResource.CA1071M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardCustomers", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Processes the customer onboarding.
        /// </summary>
        internal void ProcessCustomerOnboarding()
        {
            #region Manage Exception
            try
            {
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var PendingItems = _HCoreContextOperations.HCOUploadCustomer.Where(x => x.StatusId == 1).Skip(0).Take(500).ToList();
                    if (PendingItems.Count > 0)
                    {
                        //string AllowSms = HCoreHelper.GetConfiguration("custimportonboardallowsms", PendingItems[0].OwnerId);
                        //string SmsMessage = HCoreHelper.GetConfiguration("custimportonboardmessage", PendingItems[0].OwnerId);
                        //string InfoBipUrl = HCoreHelper.GetConfiguration("infobip_url", PendingItems[0].OwnerId);
                        //string InfoBipKey = HCoreHelper.GetConfiguration("infobip_key", PendingItems[0].OwnerId);
                        //string InfoBipSenderId = HCoreHelper.GetConfiguration("infobip_senderid", PendingItems[0].OwnerId);

                        foreach (var PendingItem in PendingItems)
                        {
                            PendingItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                            PendingItem.ModifyById = SystemAccounts.ThankUCashSystemId;
                            PendingItem.StatusId = 2;
                            PendingItem.StatusMessage = "Processing";
                        }
                        _HCoreContextOperations.SaveChanges();
                        foreach (var _Request in PendingItems)
                        {

                            bool AllowProcess = true;
                            long ProcessStatusId = 1;
                            string ProcessMessage = "";
                            if (string.IsNullOrEmpty(_Request.MobileNumber))
                            {
                                ProcessStatusId = 4;
                                ProcessMessage = "Mobile number missing";
                                AllowProcess = false;
                            }
                            //if (string.IsNullOrEmpty(_Request.Name))
                            //{
                            //    ProcessStatusId = 4;
                            //    ProcessMessage = "Name missing";
                            //    AllowProcess = false;
                            //}
                            if (_Request.MobileNumber.Length < 10)
                            {
                                ProcessStatusId = 4;
                                ProcessMessage = "Invalid mobile number";
                                AllowProcess = false;
                            }
                            if (_Request.MobileNumber.Length > 13)
                            {
                                ProcessStatusId = 4;
                                ProcessMessage = "Invalid mobile number";
                                AllowProcess = false;
                            }
                            if (!System.Text.RegularExpressions.Regex.IsMatch(_Request.MobileNumber, "^[0-9]*$"))
                            {
                                ProcessStatusId = 4;
                                ProcessMessage = "Invalid mobile number";
                                AllowProcess = false;
                            }
                            long AccountId = 0;
                            if (AllowProcess)
                            {
                                #region Manage Operations
                                using (_HCoreContext = new HCoreContext())
                                {
                                    string MobileNumber = HCoreHelper.FormatMobileNumber("234", _Request.MobileNumber);
                                    #region  Process Registration
                                    string CustomerAppId = _AppConfig.AppUserPrefix + MobileNumber;
                                    bool AccountCheck = _HCoreContext.HCUAccountAuth.Any(x => x.Username == CustomerAppId);
                                    if (!AccountCheck)
                                    {
                                        DateTime CreateDate = HCoreHelper.GetGMTDateTime();
                                        int GenderId = 0;
                                        //string MerchantDisplayName = "";
                                        //if (_Request.OwnerId != 0)
                                        //{
                                        //    MerchantDisplayName = DataStore.DataStore.Merchants.Where(x => x.ReferenceId == _Request.OwnerId).Select(x => x.DisplayName).FirstOrDefault();
                                        //    if (string.IsNullOrEmpty(MerchantDisplayName))
                                        //    {
                                        //        MerchantDisplayName = DataStore.DataStore.Partners.Where(x => x.ReferenceId == _Request.OwnerId && (x.AccountTypeId == UserAccountType.Acquirer || x.AccountTypeId == UserAccountType.Partner)).Select(x => x.DisplayName).FirstOrDefault();
                                        //    }
                                        //}
                                        if (!string.IsNullOrEmpty(_Request.Gender))
                                        {
                                            if (_Request.Gender == "gender.male" || _Request.Gender == "male")
                                            {
                                                GenderId = Gender.Male;
                                            }
                                            if (_Request.Gender == "gender.female" || _Request.Gender == "female")
                                            {
                                                GenderId = Gender.Female;
                                            }
                                            if (_Request.Gender == "gender.other" || _Request.Gender == "other")
                                            {
                                                GenderId = Gender.Other;
                                            }
                                        }
                                        string UserKey = HCoreHelper.GenerateGuid();
                                        string UserAccountKey = HCoreHelper.GenerateGuid();
                                        _Random = new Random();
                                        string AccessPin = _Random.Next(1111, 9999).ToString();
                                        #region Save User
                                        _HCUAccountAuth = new HCUAccountAuth();
                                        _HCUAccountAuth.Guid = UserKey;
                                        _HCUAccountAuth.Username = CustomerAppId;
                                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(CustomerAppId);
                                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                        _HCUAccountAuth.CreateDate = CreateDate;
                                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                        _HCUAccountAuth.CreatedById = _Request.OwnerId;
                                        #endregion
                                        #region Online Account
                                        _HCUAccount = new HCUAccount();
                                        _HCUAccount.Guid = UserAccountKey;
                                        _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                                        _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                                        string MerchantDisplayName = "";
                                        if (_Request.OwnerId != 0)
                                        {
                                            _HCUAccount.OwnerId = _Request.OwnerId;
                                            MerchantDisplayName = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.OwnerId).Select(x => x.DisplayName).FirstOrDefault();
                                        }
                                        _HCUAccount.MobileNumber = _Request.MobileNumber;
                                        if (!string.IsNullOrEmpty(_Request.FirstName))
                                        {
                                            _HCUAccount.DisplayName = _Request.FirstName;
                                        }
                                        else
                                        {
                                            _HCUAccount.DisplayName = _Request.Name;
                                        }
                                        _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(AccessPin);
                                        if (!string.IsNullOrEmpty(_Request.Name))
                                        {
                                            _HCUAccount.Name = _Request.Name;
                                        }

                                        if (!string.IsNullOrEmpty(_Request.MobileNumber))
                                        {
                                            _HCUAccount.MobileNumber = _Request.MobileNumber;
                                            _HCUAccount.ContactNumber = _Request.MobileNumber;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.EmailAddress))
                                        {
                                            _HCUAccount.EmailAddress = _Request.EmailAddress;
                                            _HCUAccount.SecondaryEmailAddress = _Request.EmailAddress;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.FirstName))
                                        {
                                            _HCUAccount.FirstName = _Request.FirstName;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.LastName))
                                        {
                                            _HCUAccount.LastName = _Request.LastName;
                                        }
                                        if (string.IsNullOrEmpty(_HCUAccount.Name))
                                        {
                                            if (!string.IsNullOrEmpty(_Request.FirstName) && !string.IsNullOrEmpty(_Request.LastName))
                                            {
                                                _HCUAccount.Name = _Request.FirstName + " " + _Request.LastName;
                                            }
                                            else
                                            {
                                                _HCUAccount.Name = _HCUAccount.MobileNumber;
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(_Request.ReferenceNumber))
                                        {
                                            _HCUAccount.ReferenceNumber = _Request.ReferenceNumber;
                                        }
                                        if (GenderId != 0)
                                        {
                                            _HCUAccount.GenderId = GenderId;
                                        }
                                        if (_Request.DateOfBirth != null)
                                        {
                                            _HCUAccount.DateOfBirth = _Request.DateOfBirth;
                                        }
                                        _HCUAccount.CountryId = 1;
                                        _HCUAccount.EmailVerificationStatus = 0;
                                        _HCUAccount.EmailVerificationStatusDate = CreateDate;
                                        _HCUAccount.NumberVerificationStatus = 0;
                                        _HCUAccount.NumberVerificationStatusDate = CreateDate;
                                        _Random = new Random();
                                        string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                                        _HCUAccount.AccountCode = AccountCode;
                                        _HCUAccount.ReferralCode = _Request.MobileNumber;
                                        _HCUAccount.RegistrationSourceId = RegistrationSource.MerchantImport;
                                        if (_Request.CreatedById != 0)
                                        {
                                            _HCUAccount.CreatedById = _Request.CreatedById;
                                        }
                                        _HCUAccount.CreateDate = CreateDate;
                                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                                        _HCUAccount.User = _HCUAccountAuth;
                                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                                        _HCoreContext.SaveChanges();

                                        //Add Onboarded Customer's Email Address to Mailerlite
                                        #region
                                        MailerliteImple mailerlite = new MailerliteImple();
                                        var createEmailObj = new CreateListRequest
                                        {
                                            name = "New Customer Email",
                                            emails = new string[] { _Request.EmailAddress }
                                        };
                                        var jsonResponse = mailerlite.AddEmailToMailerList(createEmailObj);
                                        if (jsonResponse != null)
                                        {
                                            if (jsonResponse.StatusCode.Equals(StatusCodes.Status201Created))
                                            {
                                                AccountId = _HCUAccount.Id;
                                                ProcessStatusId = 3;
                                                ProcessMessage = "Customer onboarded";
                                            }
                                        }
                                        else
                                        {
                                            AccountId = _HCUAccount.Id;
                                            ProcessStatusId = 3;
                                            ProcessMessage = "Customer Onboarded";
                                        }
                                        #endregion
                                        if ((HostEnvironment == HostEnvironmentType.Live && _Request.OwnerId == 139618) || (HostEnvironment == HostEnvironmentType.Test && _Request.OwnerId == 96689))
                                        {
                                            string Message = "Welcome to Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app";
                                            if (!string.IsNullOrEmpty(MerchantDisplayName))
                                            {
                                                Message = "Welcome to " + MerchantDisplayName + " Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app";
                                            }
                                            else
                                            {
                                                Message = "Welcome to Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app";
                                            }
                                            HCoreHelper.SendSMS(SmsType.Transaction, "234", MobileNumber, Message, AccountId, null);
                                            //if (AllowSms != "0")
                                            //{
                                            //    string Message = SmsMessage;
                                            //    if (!string.IsNullOrEmpty(Message))
                                            //    {
                                            //        Message = Message.Replace("{{AccessPin}}", AccessPin);
                                            //        Message = Message.Replace("{{Balance}}", "0");

                                            //        if (!string.IsNullOrEmpty(Message)
                                            //   && Message != "0"
                                            //   && !string.IsNullOrEmpty(InfoBipUrl)
                                            //   && Message != "0"
                                            //   && !string.IsNullOrEmpty(InfoBipKey)
                                            //   && InfoBipKey != "0"
                                            //   && !string.IsNullOrEmpty(InfoBipSenderId)
                                            //   && InfoBipSenderId != "0")
                                            //        {
                                            //            HCoreHelper.SendSMS(SmsType.Transaction, "234", MobileNumber, Message, AccountId, _HCUAccount.Guid, null, InfoBipSenderId, InfoBipKey, InfoBipUrl);
                                            //        }
                                            //    }
                                            //    else
                                            //    {
                                            //        HCoreHelper.SendSMS(SmsType.Transaction, "234", MobileNumber, Message, AccountId, _HCUAccount.Guid);
                                            //    }
                                            //}
                                        }
                                    }
                                    else
                                    {
                                        var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                                        && x.User.Username == CustomerAppId)
                                            .Select(x => new
                                            {
                                                AccountKey = x.Guid,
                                                AccountId = x.Id,
                                                AccessPin = x.AccessPin,
                                                StatusId = x.StatusId,
                                            })
                                          .FirstOrDefault();
                                        if (CustomerDetails != null)
                                        {
                                            ProcessStatusId = 4;
                                            ProcessMessage = "Customer already present";
                                            AllowProcess = false;
                                            //string AccessPin = HCoreEncrypt.DecryptHash(_HCoreContext.HCUAccount.Where(a => a.AccountTypeId == UserAccountType.Appuser && a.User.Username == CustomerAppId).Select(a => a.AccessPin).FirstOrDefault());
                                            if (CustomerDetails.StatusId == HelperStatus.Default.Active)
                                            {
                                                string AccessPin = null;
                                                if (!string.IsNullOrEmpty(CustomerDetails.AccessPin))
                                                {
                                                    AccessPin = HCoreEncrypt.DecryptHash(CustomerDetails.AccessPin);
                                                }
                                                _HCoreContext.Dispose();

                                                if (HostEnvironment == HostEnvironmentType.Live)
                                                {

                                                    //string Message = SmsMessage;
                                                    //if (AllowSms != "0")
                                                    //{
                                                    //    if (!string.IsNullOrEmpty(Message)
                                                    //    && Message != "0"
                                                    //    && !string.IsNullOrEmpty(InfoBipUrl)
                                                    //    && Message != "0"
                                                    //    && !string.IsNullOrEmpty(InfoBipKey)
                                                    //    && InfoBipKey != "0"
                                                    //    && !string.IsNullOrEmpty(InfoBipSenderId)
                                                    //    && InfoBipSenderId != "0")
                                                    //    {

                                                    //        double Balance = 0;
                                                    //        Message = Message.Replace("{{AccessPin}}", AccessPin);

                                                    //        if (Message.Contains("{{Balance}}"))
                                                    //        {
                                                    //            ManageCoreTransaction _ManageCoreTransaction = new ManageCoreTransaction();
                                                    //            Balance = _ManageCoreTransaction.GetAppUserBalance(CustomerDetails.AccountId, true);
                                                    //        }
                                                    //        Message = Message.Replace("{{Balance}}", Balance.ToString());
                                                    //        HCoreHelper.SendSMS(SmsType.Transaction, "234", MobileNumber, Message, CustomerDetails.AccountId, CustomerDetails.AccountKey, null, InfoBipSenderId, InfoBipKey, InfoBipUrl);
                                                    //    }
                                                    //}
                                                }
                                            }

                                        }

                                    }
                                    #endregion
                                }
                                #endregion
                            }
                            using (_HCoreContextOperations = new HCoreContextOperations())
                            {
                                long FileId = 0;
                                var UpItem = _HCoreContextOperations.HCOUploadCustomer.Where(x => x.Id == _Request.Id).FirstOrDefault();
                                if (UpItem != null)
                                {
                                    if (AccountId > 0)
                                    {
                                        UpItem.AccountId = AccountId;
                                    }
                                    UpItem.StatusId = ProcessStatusId;
                                    UpItem.StatusMessage = ProcessMessage;
                                    UpItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    _HCoreContextOperations.SaveChanges();
                                    if (UpItem.FileId != null)
                                    {
                                        FileId = (long)UpItem.FileId;
                                    }
                                }
                                else
                                {
                                    _HCoreContextOperations.SaveChanges();
                                }
                                if (FileId > 0)
                                {
                                    using (_HCoreContextOperations = new HCoreContextOperations())
                                    {

                                        var FileDetails = _HCoreContextOperations.HCOUploadFile.Where(x => x.Id == FileId).FirstOrDefault();
                                        if (FileDetails != null)
                                        {
                                            FileDetails.Pending = _HCoreContextOperations.HCOUploadCustomer.Count(x => x.FileId == FileId && x.StatusId == 1);
                                            FileDetails.Processing = _HCoreContextOperations.HCOUploadCustomer.Count(x => x.FileId == FileId && x.StatusId == 2);
                                            FileDetails.Success = _HCoreContextOperations.HCOUploadCustomer.Count(x => x.FileId == FileId && x.StatusId == 3);
                                            FileDetails.Error = _HCoreContextOperations.HCOUploadCustomer.Count(x => x.FileId == FileId && x.StatusId == 4);
                                            FileDetails.Completed = FileDetails.Success + FileDetails.Error;
                                            if (FileDetails.TotalRecord == FileDetails.Completed)
                                            {
                                                FileDetails.StatusId = 3;
                                            }
                                            // 1 => Pending 
                                            // 2 => Processing
                                            // 3 => Success 
                                            //FileDetails.StatusMessage = ProcessMessage;
                                            FileDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            _HCoreContextOperations.SaveChanges();
                                        }
                                        else
                                        {
                                            _HCoreContextOperations.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("ProcessCustomerOnboarding", _Exception, null);
                #endregion
            }
            #endregion
        }
    }
}
#endregion