//==================================================================================
// FileName: ActorOnboardCustomer.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using Akka.Actor;
using HCore.Data.Operations;
using HCore.Data.Operations.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Merchant;

namespace HCore.TUC.Core.Framework.Merchant.Actor
{
    internal class ActorOnboardCustomer : ReceiveActor
    {
        HCOUploadCustomer _HCOUploadCustomer;
        HCOUploadFile _HCOUploadFile;
        HCoreContextOperations _HCoreContextOperations;
        public ActorOnboardCustomer()
        {
            Receive<OOnboarding.Customer.Request>(_Request =>
            {
                long FileId = 0;
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    _HCOUploadFile = new HCOUploadFile();
                    _HCOUploadFile.TypeId = 1;
                    _HCOUploadFile.OwnerId = _Request.AccountId;
                    _HCOUploadFile.Name = _Request.FileName;
                    _HCOUploadFile.TotalRecord = _Request.Data.Count;
                    _HCOUploadFile.Pending = _Request.Data.Count;
                    _HCOUploadFile.Processing = 0;
                    _HCOUploadFile.Completed = 0;
                    _HCOUploadFile.Success = 0;
                    _HCOUploadFile.Error = 0;
                    _HCOUploadFile.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCOUploadFile.CreatedById = _Request.UserReference.AccountId;
                    _HCOUploadFile.StatusId = 1;
                    _HCoreContextOperations.HCOUploadFile.Add(_HCOUploadFile);
                    _HCoreContextOperations.SaveChanges();
                    FileId = _HCOUploadFile.Id;
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    foreach (var DataItem in _Request.Data)
                    {
                        //if (!string.IsNullOrEmpty(DataItem.MobileNumber))
                        //{
                        _HCOUploadCustomer = new HCOUploadCustomer();
                        _HCOUploadCustomer.OwnerId = _Request.AccountId;
                        _HCOUploadCustomer.Name = DataItem.Name;
                        _HCOUploadCustomer.FirstName = DataItem.FirstName;
                        _HCOUploadCustomer.LastName = DataItem.LastName;
                        _HCOUploadCustomer.MobileNumber = DataItem.MobileNumber;
                        _HCOUploadCustomer.FormattedMobileNumber = HCoreHelper.FormatMobileNumber("234", DataItem.MobileNumber);
                        _HCOUploadCustomer.EmailAddress = DataItem.EmailAddress;
                        _HCOUploadCustomer.Gender = DataItem.Gender;
                        _HCOUploadCustomer.DateOfBirth = DataItem.DateOfBirth;
                        _HCOUploadCustomer.ReferenceNumber = DataItem.ReferenceNumber;
                        _HCOUploadCustomer.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCOUploadCustomer.CreatedById = _Request.UserReference.AccountId;
                        _HCOUploadCustomer.StatusId = 1;
                        _HCOUploadCustomer.StatusMessage = "Pending";
                        _HCOUploadCustomer.FileId = FileId;
                        _HCoreContextOperations.HCOUploadCustomer.Add(_HCOUploadCustomer);
                        //}
                    }
                    _HCoreContextOperations.SaveChanges();
                }

                var system = ActorSystem.Create("ActorProcessOnboardCustomer");
                var greeter = system.ActorOf<ActorProcessOnboardCustomer>("ActorProcessOnboardCustomer");
                greeter.Tell(_Request.AccountId);
            });
        }
    }
    internal class ActorProcessOnboardCustomer : ReceiveActor
    {
        FrameworkOnboarding _FrameworkOnboarding;
        public ActorProcessOnboardCustomer()
        {
            Receive<long>(_Action =>
            {
                _FrameworkOnboarding = new FrameworkOnboarding();
                _FrameworkOnboarding.ProcessCustomerOnboarding();
            });
        }
    }

}
