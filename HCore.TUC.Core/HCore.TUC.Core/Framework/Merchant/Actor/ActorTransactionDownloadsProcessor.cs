//==================================================================================
// FileName: ActorTransactionDownloadsProcessor.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using Akka.Actor;
using HCore.Helper;
using HCore.TUC.Core.Core;
using HCore.TUC.Core.Framework.Merchant;

namespace HCore.ThankUCash.Actors
{
    
    internal class ActorRewardTransactionDownloader : ReceiveActor
    {
        public ActorRewardTransactionDownloader()
        {
            Receive<OList.Request>(_Request =>
            {
                FrameworkTransactions _FrameworkTransactions = new FrameworkTransactions();
                _FrameworkTransactions.GetRewardTransactionDownload(_Request);
            });
        }
    }
    internal class ActorPendingRewardTransactionDownloader : ReceiveActor
    {
        public ActorPendingRewardTransactionDownloader()
        {
            Receive<OList.Request>(_Request =>
            {
                FrameworkTransactions _FrameworkTransactions = new FrameworkTransactions();
                _FrameworkTransactions.GetPendingRewardTransactionDownload(_Request);
            });
        }
    }
    internal class ActorSaleTransactionDownloader : ReceiveActor
    {
        public ActorSaleTransactionDownloader()
        {
            Receive<OList.Request>(_Request =>
            {
                FrameworkTransactions _FrameworkTransactions = new FrameworkTransactions();
                _FrameworkTransactions.GetSaleTransactionDownload(_Request);
            });
        }
    }
    internal class ActorRewardClaimTransactionDownloader : ReceiveActor
    {
        public ActorRewardClaimTransactionDownloader()
        {
            Receive<OList.Request>(_Request =>
            {
                FrameworkTransactions _FrameworkTransactions = new FrameworkTransactions();
                _FrameworkTransactions.GetRewardClaimTransactionDownload(_Request);
            });
        }
    }
    internal class ActorRedeemTransactionDownloader : ReceiveActor
    {
        public ActorRedeemTransactionDownloader()
        {
            Receive<OList.Request>(_Request =>
            {
                FrameworkTransactions _FrameworkTransactions = new FrameworkTransactions();
                _FrameworkTransactions.GetRedeemTransactionDownload(_Request);
            });
        }
    }
    internal class ActorAppUserDownloader : ReceiveActor
    {
        public ActorAppUserDownloader()
        {
            //Receive<OList.Request>(_Request =>
            //{
            //    FrameworkAccountsAppUsers _FrameworkAccountsAppUsers = new FrameworkAccountsAppUsers();
            //    _FrameworkAccountsAppUsers.GetAppUserDownload(_Request);
            //});
        }
    }
}
