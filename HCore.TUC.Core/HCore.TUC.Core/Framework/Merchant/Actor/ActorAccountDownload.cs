//==================================================================================
// FileName: ActorAccountDownload.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using Akka.Actor;
using HCore.Helper;
using HCore.TUC.Core.Core;
using HCore.TUC.Core.Framework.Merchant;


namespace HCore.TUC.Core.Framework.Merchant.Actor
{
  public  class ActorAccountDownload : ReceiveActor
    {
        public ActorAccountDownload()
        {
            Receive<OList.Request>(_Request =>
            {
                FrameworkCustomer _FrameworkCustomer = new FrameworkCustomer();
                _FrameworkCustomer.GetCustomerDownload(_Request);
            });
        }
    }
}
