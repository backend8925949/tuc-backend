//==================================================================================
// FileName: FrameworkPromoCode.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to promocode
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Data;
using HCore.Data.Models;
using HCore.TUC.Core.Object.Operations;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Collections.Generic;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant;
using HCore.TUC.Core.Resource;

namespace HCore.TUC.Core.Framework.Operations
{
    public class FrameworkPromoCode
    {
        HCoreContext _HCoreContext;
        TUCPromoCode _TUCPromoCode;
        TUCPromoCodeAccount _TUCPromoCodeAccount;
        TUCPromoCodeCondition _TUCPromoCodeCondition;
        OPromoCode.PromoCodeOperations.Overview _PromoCodeOverview;
        OPromoCode.PromoCodeOperations.Usage _PromoCodeUsage;

        /// <summary>
        /// Description: Method defined to get promocode conditions
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetPromoCodeConditions(OList.Request _Request)
       {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.TUCPromoCodeCondition
                                               //.Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                               .Select(x => new OPromoCode.PromoCodeConditions
                                               {
                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,

                                                   CountryId = x.CountryId,
                                                   CountryKey = x.Country.Guid,
                                                   CountryName = x.Country.Name,

                                                   AccountTypeId = x.AccountTypeId,
                                                   AccountTypeCode = x.AccountType.SystemName,
                                                   AccountTypeName = x.AccountType.Name,

                                                   Name = x.Name,
                                                   Description = x.Description,
                                                   Value = x.Value,
                                               }).Where(_Request.SearchCondition)
                                                 .Count();
                    }
                    List<OPromoCode.PromoCodeConditions> _Data = _HCoreContext.TUCPromoCodeCondition
                                               //.Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                               .Select(x => new OPromoCode.PromoCodeConditions
                                               {
                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,

                                                   CountryId = x.CountryId,
                                                   CountryKey = x.Country.Guid,
                                                   CountryName = x.Country.Name,

                                                   AccountTypeId = x.AccountTypeId,
                                                   AccountTypeCode = x.AccountType.SystemName,
                                                   AccountTypeName = x.AccountType.Name,

                                                   Name = x.Name,
                                                   Description = x.Description,
                                                   Value = x.Value,
                                               })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();

                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, _Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetPromoCodeConditions", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }

        /// <summary>
        /// Description: Method to get list of promocode accounts
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetPromoCodeAccounts(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "StartDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.TUCPromoCodeAccount
                                               .Select(x => new OPromoCode.PromoCodeAccounts
                                               {
                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,
                                                   PromoCodeId = x.PromoCode.Id,
                                                   PromoCodeKey = x.PromoCode.Guid,
                                                   PromoCode = x.PromoCode.Code,
                                                   PromoCodeValue = x.PromoCode.Value,
                                                   StartDate = x.PromoCode.StartDate,
                                                   EndDate = x.PromoCode.EndDate,
                                                   AccountId = x.AccountId,
                                                   AccountKey = x.Account.Guid,
                                                   AccountName = x.Account.Name,
                                                   AccountMobileNumber = x.Account.MobileNumber,
                                                   AccoountEmailAddress = x.Account.EmailAddress,
                                                   AccountRegistrationSourceId = x.Account.RegistrationSourceId,
                                                   AccountRegistrationSourceCode = x.Account.RegistrationSource.SystemName,
                                                   AccountRegistratonSourceName = x.Account.RegistrationSource.Name,
                                                   Value = x.Value,
                                                   UseDate = x.UseDate,
                                               }).Where(_Request.SearchCondition)
                                                 .Count();
                    }
                    List<OPromoCode.PromoCodeAccounts> _Data = _HCoreContext.TUCPromoCodeAccount
                                               .Select(x => new OPromoCode.PromoCodeAccounts
                                               {
                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,
                                                   PromoCodeId = x.PromoCode.Id,
                                                   PromoCodeKey = x.PromoCode.Guid,
                                                   PromoCode = x.PromoCode.Code,
                                                   PromoCodeValue = x.PromoCode.Value,
                                                   StartDate = x.PromoCode.StartDate,
                                                   EndDate = x.PromoCode.EndDate,
                                                   AccountId = x.AccountId,
                                                   AccountKey = x.Account.Guid,
                                                   AccountName = x.Account.Name,
                                                   AccountMobileNumber = x.Account.MobileNumber,
                                                   AccoountEmailAddress = x.Account.EmailAddress,
                                                   AccountRegistrationSourceId = x.Account.RegistrationSourceId,
                                                   AccountRegistrationSourceCode = x.Account.RegistrationSource.SystemName,
                                                   AccountRegistratonSourceName = x.Account.RegistrationSource.Name,
                                                   Value = x.Value,
                                                   UseDate = x.UseDate,
                                               })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();

                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, _Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetPromoCodeAccounts", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }

        /// <summary>
        /// Description: Method to get details of promocode account
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetPromoCodeAccount(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _PromoCodeDetails = _HCoreContext.TUCPromoCodeAccount.Where(x => (x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey))
                        .Select(x => new OPromoCode.PromoCodeAccount
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            PromoCodeId = x.PromoCode.Id,
                            PromoCodeKey = x.PromoCode.Guid,
                            PromoCode = x.PromoCode.Code,
                            PromoCodeValue = x.PromoCode.Value,
                            StartDate = x.PromoCode.StartDate,
                            EndDate = x.PromoCode.EndDate,
                            AccountId = x.AccountId,
                            AccountKey = x.Account.Guid,
                            AccountName = x.Account.Name,
                            AccountMobileNumber = x.Account.MobileNumber,
                            AccoountEmailAddress = x.Account.EmailAddress,
                            AccountIconUrl = x.Account.IconStorage.Path,
                            Value = x.Value,
                            UseDate = x.UseDate,
                        })
                       .FirstOrDefault();

                    _HCoreContext.Dispose();

                    if (_PromoCodeDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_PromoCodeDetails.AccountIconUrl))
                        {
                            _PromoCodeDetails.AccountIconUrl = _AppConfig.StorageUrl + _PromoCodeDetails.AccountIconUrl;
                        }
                        else
                        {
                            _PromoCodeDetails.AccountIconUrl = _AppConfig.Default_Icon;
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _PromoCodeDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetPromoCodeAccount", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Method defined to save promocode
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SavePromoCode(OPromoCode.PromoCodeOperations.Save _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.CountryKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14143, TUCCoreResource.CAA14143M);
                }
                if (string.IsNullOrEmpty(_Request.AccountTypeCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14144, TUCCoreResource.CAA14144M);
                }
                if (string.IsNullOrEmpty(_Request.ConditionKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14145, TUCCoreResource.CAA14145M);
                }
                if (string.IsNullOrEmpty(_Request.TypeCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14149, TUCCoreResource.CAA14149M);
                }
                int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _PromoCodeDetails = _HCoreContext.TUCPromoCode.Where(x => x.CountryId == _Request.CountryId && x.AccountTypeId == _Request.AccountTypeId).FirstOrDefault();
                    if(_PromoCodeDetails.Code == _Request.PromoCode && _PromoCodeDetails.Title == _Request.Title)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14148, TUCCoreResource.CAA14148M);
                    }
                    _TUCPromoCode = new TUCPromoCode();
                    _TUCPromoCode.Guid = HCoreHelper.GenerateGuid();
                    _TUCPromoCode.CountryId = _Request.CountryId;
                    _TUCPromoCode.AccountTypeId = _Request.AccountTypeId;
                    _TUCPromoCode.ConditionId = _Request.ConditionId;
                    _TUCPromoCode.TypeId = _Request.TypeId;
                    _TUCPromoCode.Title = _Request.Title;
                    _TUCPromoCode.Description = _Request.Description;
                    _TUCPromoCode.Budget = _Request.Budget;
                    _TUCPromoCode.StartDate = _Request.StartDate;
                    _TUCPromoCode.EndDate = _Request.EndDate;
                    _TUCPromoCode.MaximumLimit = _Request.MaximumLimit;
                    _TUCPromoCode.MaximumLimitPerUser = _Request.MaximumLimitPerUser;
                    _TUCPromoCode.Code = _Request.PromoCode;
                    _TUCPromoCode.Value = _Request.PromoCodeValue;
                    _TUCPromoCode.CreateDate = HCoreHelper.GetGMTDateTime();
                    _TUCPromoCode.CreatedById = _Request.UserReference.AccountId;
                    _TUCPromoCode.StatusId = HelperStatus.Default.Active;
                    _HCoreContext.TUCPromoCode.Add(_TUCPromoCode);
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();

                    var _Response = new
                    {
                        ReferenceId = _TUCPromoCode.Id,
                        ReferenceKey = _TUCPromoCode.Guid,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA14146, TUCCoreResource.CAA14146M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SavePromoCode", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Method defined to update promocode
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdatePromoCode(OPromoCode.PromoCodeOperations.Update _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _PromoCodeDetails = _HCoreContext.TUCPromoCode.Where(x => x.Guid == _Request.ReferenceKey && x.Id == _Request.ReferenceId).FirstOrDefault();
                    if (_PromoCodeDetails != null)
                    {
                        if (_Request.CountryId > 0)
                        {
                            _PromoCodeDetails.CountryId = _Request.CountryId;
                        }
                        if (_Request.AccountTypeId > 0)
                        {
                            _PromoCodeDetails.AccountTypeId = _Request.AccountTypeId;
                        }
                        if (_Request.TypeId > 0)
                        {
                            _PromoCodeDetails.TypeId = _Request.TypeId;
                        }
                        if (!string.IsNullOrEmpty(_Request.PromoCode) && _PromoCodeDetails.Code != _Request.PromoCode)
                        {
                            _PromoCodeDetails.Code = _Request.PromoCode;
                        }
                        if (_Request.PromoCodeValue > 0)
                        {
                            _PromoCodeDetails.Value = _Request.PromoCodeValue;
                        }
                        if (!string.IsNullOrEmpty(_Request.Title) && _PromoCodeDetails.Title != _Request.Title)
                        {
                            _PromoCodeDetails.Title = _Request.Title;
                        }
                        if (!string.IsNullOrEmpty(_Request.Description) && _PromoCodeDetails.Description != _Request.Description)
                        {
                            _PromoCodeDetails.Description = _Request.Description;
                        }
                        if (_Request.Budget > 0)
                        {
                            _PromoCodeDetails.Budget = _Request.Budget;
                        }
                        if (_Request.ConditionId > 0)
                        {
                            _PromoCodeDetails.ConditionId = _Request.ConditionId;
                        }
                        if (_Request.StartDate != null && _PromoCodeDetails.StartDate != _Request.StartDate)
                        {
                            _PromoCodeDetails.StartDate = (DateTime)_Request.StartDate;
                        }
                        if (_Request.EndDate != null && _PromoCodeDetails.EndDate != _Request.EndDate)
                        {
                            _PromoCodeDetails.EndDate = (DateTime)_Request.EndDate;
                        }
                        if (_Request.MaximumLimit > 0 && _Request.MaximumLimit != _PromoCodeDetails.MaximumLimit)
                        {
                            _PromoCodeDetails.MaximumLimit = _Request.MaximumLimit;
                        }
                        if (_Request.MaximumLimitPerUser > 0 && _Request.MaximumLimit != _PromoCodeDetails.MaximumLimitPerUser)
                        {
                            _PromoCodeDetails.MaximumLimitPerUser = _Request.MaximumLimitPerUser;
                        }
                        if (StatusId > 0)
                        {
                            _PromoCodeDetails.StatusId = StatusId;
                        }
                        _PromoCodeDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _PromoCodeDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();

                        var _Response = new
                        {
                            ReferenceId = _PromoCodeDetails.Id,
                            ReferenceKey = _PromoCodeDetails.Guid,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA14142, TUCCoreResource.CAA14142M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdatePromoCode", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Method defined to delete promocode
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeletePromoCode(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _PromoCodeDetails = _HCoreContext.TUCPromoCode
                        .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .FirstOrDefault();
                    if (_PromoCodeDetails != null)
                    {
                        _HCoreContext.TUCPromoCode.Remove(_PromoCodeDetails);
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CAA14147, TUCCoreResource.CAA14147M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DeletePromoCode", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }

        /// <summary>
        /// Description: Method defined to get promocode details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetPromoCode(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _PromoCodeDetails = _HCoreContext.TUCPromoCode.Where(x => (x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey))
                        .Select(x => new OPromoCode.PromoCodeOperations.PromoCode
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            CountryId = x.CountryId,
                            CountryKey = x.Country.Guid,
                            CountryName = x.Country.Name,

                            AccountTypeId = x.AccountTypeId                            ,
                            AccountTypeCode = x.AccountType.SystemName,
                            AccountTypeName = x.AccountType.Name,

                            ConditionId = x.ConditionId,
                            ConditionKey = x.Condition.Guid,
                            ConditionName = x.Condition.Name,
                            ConditionValue = x.Condition.Value,
                            ConditionDescription = x.Condition.Description,

                            TypeId = x.TypeId,
                            TypeCode = x.Type.SystemName,
                            TypeName = x.Type.Name,

                            Title = x.Title,
                            Description = x.Description,
                            Code = x.Code,
                            Budget = x.Budget,
                            PromoCodeValue = x.Value,
                            StartDate = x.StartDate,
                            EndDate = x.EndDate,
                            MaximumLimit = x.MaximumLimit,
                            MaximumLimitPerUser = x.MaximumLimitPerUser,
                            PromocodeUse = (x.MaximumLimitPerUser > 1),

                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyById,
                            ModifyByKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name
                        })
                       .FirstOrDefault();

                    _HCoreContext.Dispose();

                    if (_PromoCodeDetails != null)
                    {
                        //if (!string.IsNullOrEmpty(_PromoCodeDetails.ImageUrl))
                        //{
                        //    _PromoCodeDetails.ImageUrl = _AppConfig.StorageUrl + _PromoCodeDetails.ImageUrl;
                        //}
                        //else
                        //{
                        //    _PromoCodeDetails.ImageUrl = _AppConfig.Default_Icon;
                        //}
                        if (_PromoCodeDetails.EndDate.Date < HCoreHelper.GetGMTDate()) {
                            _PromoCodeDetails.StatusId = 1;
                            _PromoCodeDetails.StatusCode = "default.inactive";
                            _PromoCodeDetails.StatusName = "Inactive";
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _PromoCodeDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetPromoCode", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Method defined to get list of promocodes
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetPromoCodes(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.TUCPromoCode
                            .Select(x => new OPromoCode.PromoCodeOperations.Promocodes
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                CountryId = x.CountryId,
                                CountryKey = x.Country.Guid,
                                CountryName = x.Country.Name,

                                AccountTypeId = x.AccountTypeId,
                                AccountTypeCode = x.AccountType.SystemName,
                                AccountTypeName = x.AccountType.Name,

                                ConditionId = x.ConditionId,
                                ConditionKey = x.Condition.Guid,
                                ConditionName = x.Condition.Name,

                                TypeId = x.TypeId,
                                TypeCode = x.Type.SystemName,
                                TypeName = x.Type.Name,

                                Title = x.Title,
                                Description = x.Description,
                                PromoCode = x.Code,
                                PromoCodeValue = x.Value,
                                Budget = x.Budget,
                                StartDate = x.StartDate,
                                EndDate = x.EndDate,
                                MaximumLimit = x.MaximumLimit,
                                MaximumLimitPerUser = x.MaximumLimitPerUser,
                                TotalUsed = x.TUCPromoCodeAccount.Count(),
                                TotalAvailable = (x.MaximumLimit - x.TUCPromoCodeAccount.Count()),

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByKey = x.CreatedBy.Guid,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyById = x.ModifyById,
                                ModifyByKey = x.ModifyBy.Guid,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name
                            })
                            .Where(_Request.SearchCondition)
                            .Count();

                    }

                    List<OPromoCode.PromoCodeOperations.Promocodes> Data = _HCoreContext.TUCPromoCode
                                                .Select(x => new OPromoCode.PromoCodeOperations.Promocodes
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    CountryId = x.CountryId,
                                                    CountryKey = x.Country.Guid,
                                                    CountryName = x.Country.Name,

                                                    AccountTypeId = x.AccountTypeId,
                                                    AccountTypeCode = x.AccountType.SystemName,
                                                    AccountTypeName = x.AccountType.Name,

                                                    ConditionId = x.ConditionId,
                                                    ConditionKey = x.Condition.Guid,
                                                    ConditionName = x.Condition.Name,

                                                    TypeId = x.TypeId,
                                                    TypeCode = x.Type.SystemName,
                                                    TypeName = x.Type.Name,

                                                    Title = x.Title,
                                                    Description = x.Description,
                                                    PromoCode = x.Code,
                                                    PromoCodeValue = x.Value,
                                                    Budget = x.Budget,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    MaximumLimit = x.MaximumLimit,
                                                    MaximumLimitPerUser = x.MaximumLimitPerUser,
                                                    TotalUsed = x.TUCPromoCodeAccount.Count(),
                                                    TotalAvailable = (x.MaximumLimit - x.TUCPromoCodeAccount.Count()),

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.EndDate.Date < HCoreHelper.GetGMTDate() ? 1 : x.StatusId,
                                                    StatusCode = x.EndDate.Date < HCoreHelper.GetGMTDate() ? "default.inactive" : x.Status.SystemName,
                                                    StatusName = x.EndDate.Date < HCoreHelper.GetGMTDate() ? "Inactive" : x.Status.Name
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    foreach (var DataItem in Data)
                    {
                        if (DataItem.EndDate.Date < HCoreHelper.GetGMTDate())
                        {
                            DataItem.StatusId = 1;
                            DataItem.StatusCode = "default.inactive";
                            DataItem.StatusName = "Inactive";
                        }
                        //if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                        //{
                        //    DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                        //}
                    }

                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetPromoCodes", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }

        /// <summary>
        /// Description: Method defined to update promocode limit
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdatePromoCodeLimit(OPromoCode.PromoCodeOperations.Update _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _PromoCodeDetails = _HCoreContext.TUCPromoCode.Where(x => x.Guid == _Request.ReferenceKey && x.Id == _Request.ReferenceId).FirstOrDefault();
                    if (_PromoCodeDetails != null)
                    {
                        if (_Request.MaximumLimit > 0)
                        {
                            _PromoCodeDetails.MaximumLimit = _PromoCodeDetails.MaximumLimit + _Request.MaximumLimit;
                        }
                        //if (_Request.MaximumLimitPerUser > 0)
                        //{
                        //    _PromoCodeDetails.MaximumLimitPerUser = _PromoCodeDetails.MaximumLimitPerUser + _Request.MaximumLimitPerUser;
                        //}
                        _PromoCodeDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _PromoCodeDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();

                        var _Response = new
                        {
                            ReferenceId = _PromoCodeDetails.Id,
                            ReferenceKey = _PromoCodeDetails.Guid,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA14142, TUCCoreResource.CAA14142M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdatePromoCodeLimit", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Method defined to update promocode schedule
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdatePromoCodeShedule(OPromoCode.PromoCodeOperations.Update _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _PromoCodeDetails = _HCoreContext.TUCPromoCode.Where(x => x.Guid == _Request.ReferenceKey && x.Id == _Request.ReferenceId).FirstOrDefault();
                    if (_PromoCodeDetails != null)
                    {
                        if (_Request.StartDate != null && _PromoCodeDetails.StartDate != _Request.StartDate)
                        {
                            _PromoCodeDetails.StartDate = (DateTime)_Request.StartDate;
                        }
                        if (_Request.EndDate != null && _PromoCodeDetails.EndDate != _Request.EndDate)
                        {
                            _PromoCodeDetails.EndDate = (DateTime)_Request.EndDate;
                        }
                        _PromoCodeDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _PromoCodeDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();

                        var _Response = new
                        {
                            ReferenceId = _PromoCodeDetails.Id,
                            ReferenceKey = _PromoCodeDetails.Guid,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA14142, TUCCoreResource.CAA14142M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdatePromoCodeShedule", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Method defined to get  promocode overview
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetPromocodeOverview(OList.Request _Request)
        {
            try
            {
                _PromoCodeOverview = new OPromoCode.PromoCodeOperations.Overview();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                DateTime ToDayDateTime = HCoreHelper.GetGMTDateTime();
                using (_HCoreContext = new HCoreContext())
                {
                    _PromoCodeOverview.Total = _HCoreContext.TUCPromoCode
                                            .Select(x => new OPromoCode.PromoCodeOperations.Promocodes
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                CountryId = x.CountryId,
                                                CountryKey = x.Country.Guid,
                                                CountryName = x.Country.Name,

                                                AccountTypeId = x.AccountTypeId,
                                                AccountTypeCode = x.AccountType.SystemName,
                                                AccountTypeName = x.AccountType.Name,

                                                ConditionId = x.ConditionId,
                                                ConditionKey = x.Condition.Guid,
                                                ConditionName = x.Condition.Name,

                                                TypeId = x.TypeId,
                                                TypeCode = x.Type.SystemName,
                                                TypeName = x.Type.Name,

                                                Title = x.Title,
                                                Description = x.Description,
                                                PromoCode = x.Code,
                                                PromoCodeValue = x.Value,
                                                Budget = x.Budget,
                                                StartDate = x.StartDate,
                                                EndDate = x.EndDate,
                                                MaximumLimit = x.MaximumLimit,
                                                MaximumLimitPerUser = x.MaximumLimitPerUser,

                                                CreateDate = x.CreateDate,
                                                CreatedById = x.CreatedById,
                                                CreatedByKey = x.CreatedBy.Guid,
                                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                ModifyDate = x.ModifyDate,
                                                ModifyById = x.ModifyById,
                                                ModifyByKey = x.ModifyBy.Guid,
                                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                StatusId = x.StatusId,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name
                                            }).Where(_Request.SearchCondition)
                                              .Count();

                    _PromoCodeOverview.Active = _HCoreContext.TUCPromoCode
                        .Where(x => x.EndDate >= ToDayDateTime)
                         .Select(x => new OPromoCode.PromoCodeOperations.Promocodes
                         {
                             ReferenceId = x.Id,
                             ReferenceKey = x.Guid,

                             CountryId = x.CountryId,
                             CountryKey = x.Country.Guid,
                             CountryName = x.Country.Name,

                             AccountTypeId = x.AccountTypeId,
                             AccountTypeCode = x.AccountType.SystemName,
                             AccountTypeName = x.AccountType.Name,

                             ConditionId = x.ConditionId,
                             ConditionKey = x.Condition.Guid,
                             ConditionName = x.Condition.Name,

                             TypeId = x.TypeId,
                             TypeCode = x.Type.SystemName,
                             TypeName = x.Type.Name,

                             Title = x.Title,
                             Description = x.Description,
                             PromoCode = x.Code,
                             PromoCodeValue = x.Value,
                             Budget = x.Budget,
                             StartDate = x.StartDate,
                             EndDate = x.EndDate,
                             MaximumLimit = x.MaximumLimit,
                             MaximumLimitPerUser = x.MaximumLimitPerUser,

                             CreateDate = x.CreateDate,
                             CreatedById = x.CreatedById,
                             CreatedByKey = x.CreatedBy.Guid,
                             CreatedByDisplayName = x.CreatedBy.DisplayName,

                             ModifyDate = x.ModifyDate,
                             ModifyById = x.ModifyById,
                             ModifyByKey = x.ModifyBy.Guid,
                             ModifyByDisplayName = x.ModifyBy.DisplayName,

                             StatusId = x.StatusId,
                             StatusCode = x.Status.SystemName,
                             StatusName = x.Status.Name
                         }).Where(_Request.SearchCondition)
                           .Count();

                    _PromoCodeOverview.Expired = _HCoreContext.TUCPromoCode
                        .Where(x => x.EndDate < ToDayDateTime)
                         .Select(x => new OPromoCode.PromoCodeOperations.Promocodes
                         {
                             ReferenceId = x.Id,
                             ReferenceKey = x.Guid,

                             CountryId = x.CountryId,
                             CountryKey = x.Country.Guid,
                             CountryName = x.Country.Name,

                             AccountTypeId = x.AccountTypeId,
                             AccountTypeCode = x.AccountType.SystemName,
                             AccountTypeName = x.AccountType.Name,

                             ConditionId = x.ConditionId,
                             ConditionKey = x.Condition.Guid,
                             ConditionName = x.Condition.Name,

                             TypeId = x.TypeId,
                             TypeCode = x.Type.SystemName,
                             TypeName = x.Type.Name,

                             Title = x.Title,
                             Description = x.Description,
                             PromoCode = x.Code,
                             PromoCodeValue = x.Value,
                             Budget = x.Budget,
                             StartDate = x.StartDate,
                             EndDate = x.EndDate,
                             MaximumLimit = x.MaximumLimit,
                             MaximumLimitPerUser = x.MaximumLimitPerUser,

                             CreateDate = x.CreateDate,
                             CreatedById = x.CreatedById,
                             CreatedByKey = x.CreatedBy.Guid,
                             CreatedByDisplayName = x.CreatedBy.DisplayName,

                             ModifyDate = x.ModifyDate,
                             ModifyById = x.ModifyById,
                             ModifyByKey = x.ModifyBy.Guid,
                             ModifyByDisplayName = x.ModifyBy.DisplayName,

                             StatusId = x.StatusId,
                             StatusCode = x.Status.SystemName,
                             StatusName = x.Status.Name
                         }).Where(_Request.SearchCondition)
                           .Count();


                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _PromoCodeOverview, "HC0001");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetPromocodeOverview", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }

        /// <summary>
        /// Description: Method defined to get promocode usage overview
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetPromocodeUsageOverview(OReference _Request)
        {
            try
            {
                _PromoCodeUsage = new OPromoCode.PromoCodeOperations.Usage();
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    _PromoCodeUsage.Used = _HCoreContext.TUCPromoCodeAccount.Count(x => x.PromoCodeId == _Request.ReferenceId);
                    _PromoCodeUsage.UsedAmount = _HCoreContext.TUCPromoCodeAccount.Where(x => x.PromoCodeId == _Request.ReferenceId).Sum(x=>x.Value);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _PromoCodeUsage, "HC0001");
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetPromocodeUsageOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }

        /// <summary>
        /// Description: Method defined to get list of  customers by referral
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCustomersByReferrals(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using(_HCoreContext = new HCoreContext())
                {
                    if(_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                               .Where(x => x.OwnerId == _Request.ReferenceId
                                               && x.Owner.Guid == _Request.ReferenceKey
                                               && x.Owner.AccountTypeId == UserAccountType.Appuser)
                                               .Select(x => new OPromoCode.CustomerReferrals
                                               {
                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,
                                                   Name = x.Name,
                                                   DisplayName = x.DisplayName,
                                                   MobileNumber = x.MobileNumber,
                                                   EmailAddress = x.EmailAddress,
                                                   IconUrl = x.IconStorage.Path,
                                                   LastActivityDate = x.LastLoginDate,
                                                   CreateDate = x.CreateDate,
                                                   CreatedById = x.CreatedById,
                                                   CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                   ModifyDate = x.ModifyDate,
                                                   ModifyById = x.ModifyById,
                                                   ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                   StatusId = x.StatusId,
                                                   StatusCode = x.Status.SystemName,
                                                   StatusName = x.Status.Name
                                               }).Where(_Request.SearchCondition)
                                               .Count();
                    }

                    List<OPromoCode.CustomerReferrals> _CustomersByReferral = _HCoreContext.HCUAccount
                                               .Where(x => x.OwnerId == _Request.ReferenceId
                                               && x.Owner.Guid == _Request.ReferenceKey
                                               && x.Owner.AccountTypeId == UserAccountType.Appuser)
                                               .Select(x => new OPromoCode.CustomerReferrals
                                               {
                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,
                                                   Name = x.Name,
                                                   DisplayName = x.DisplayName,
                                                   MobileNumber = x.MobileNumber,
                                                   EmailAddress = x.EmailAddress,
                                                   IconUrl = x.IconStorage.Path,
                                                   LastActivityDate = x.LastLoginDate,
                                                   CreateDate = x.CreateDate,
                                                   CreatedById = x.CreatedById,
                                                   CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                   ModifyDate = x.ModifyDate,
                                                   ModifyById = x.ModifyById,
                                                   ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                   StatusId = x.StatusId,
                                                   StatusCode = x.Status.SystemName,
                                                   StatusName = x.Status.Name
                                               }).Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();

                    _HCoreContext.Dispose();
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _CustomersByReferral)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, _CustomersByReferral, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCustomersByReferrals", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
    }
}
