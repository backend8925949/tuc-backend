﻿//==================================================================================
// FileName: FrameworkProgram.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to programs
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-10-2022      | Priya Chavadiya   : Developed Logics for Programs
//
//==================================================================================
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Integration.Paystack;
using HCore.Operations.Object;
using HCore.Operations;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant.HelperStatus;

namespace HCore.TUC.Core.Framework.Operations
{
    public class FrameworkProgram
    {
        HCoreContext _HCoreContext;
        TUCLProgram _TUCProgram;
        OCoreTransaction.Request _CoreTransactionRequest;
        ManageCoreTransaction _ManageCoreTransaction;
        Object.Merchant.OSubscription.Balance.Response _BalanceResponse;
        List<OCoreTransaction.TransactionItem> _TransactionItems;

        /// <summary>
        /// Description: Method defined to save program
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveProgram(OProgram.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1523, TUCCoreResource.CA1523M);
                }
                if (_Request.AccountId <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    bool CheckItem = _HCoreContext.TUCLProgram.Any(x => x.Name == _Request.Name && x.AccountId != _Request.AccountId);
                    if (!CheckItem)
                    {
                        long SubscriptionId = _HCoreContext.HCSubscription.Where(x => x.Guid == "merchantstoresubscription").Select(x => x.Id).FirstOrDefault();
                        int ProgramTypeId = HCoreHelper.GetStatusId(_Request.ProgramTypeCode, _Request.UserReference);
                        int ComissionTypeId = HCoreHelper.GetStatusId(_Request.ComissionTypeCode, _Request.UserReference);
                        _TUCProgram = new TUCLProgram();
                        _TUCProgram.Guid = HCoreHelper.GenerateGuid();
                        _TUCProgram.Name = _Request.Name;
                        _TUCProgram.AccountId = _Request.AccountId;
                        _TUCProgram.ProgramTypeId = ProgramTypeId;
                        _TUCProgram.SubscriptionId = SubscriptionId;
                        _TUCProgram.ComissionTypeId = ComissionTypeId;
                        _TUCProgram.Comission = _Request.Comission;
                        _TUCProgram.RewardPercentage = _Request.RewardCap;
                        _TUCProgram.CreateDate = HCoreHelper.GetGMTDateTime();
                        _TUCProgram.CreatedById = _Request.UserReference.AccountId;
                        _TUCProgram.StatusId = HelperStatus.Default.Active;
                        _TUCProgram.MaxInvoiceAmount = _Request.MaxInvoiceAmount;
                        _TUCProgram.MinInvoiceAmount = _Request.MinInvoiceAmount;
                        _TUCProgram.MaxRewards = _Request.MaxRewards;
                        _TUCProgram.MaxComission = _Request.MaxComission;
                        _TUCProgram.CashierMaxReward = _Request.CashierMaxReward;
                        _TUCProgram.CashierCommission = _Request.CashierCommission;

                        _HCoreContext.TUCLProgram.Add(_TUCProgram);
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            ReferenceId = _TUCProgram.Id,
                            ReferenceKey = _TUCProgram.Guid,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1524, TUCCoreResource.CA1524M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1525, TUCCoreResource.CA1525M);
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveProgram", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to update program
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateProgram(OProgram.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                using (_HCoreContext = new HCoreContext())
                {

                    var ProgramDetails = _HCoreContext.TUCLProgram
                                                                 .Where(x => x.Id == _Request.ReferenceId
                                                                 && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (ProgramDetails != null)
                    {
                        int ProgramTypeId = HCoreHelper.GetStatusId(_Request.ProgramTypeCode, _Request.UserReference);
                        int ComissionTypeId = HCoreHelper.GetStatusId(_Request.ComissionTypeCode, _Request.UserReference);
                        if (!string.IsNullOrEmpty(_Request.Name) && ProgramDetails.Name != _Request.Name)
                        {
                            bool NameCheck = _HCoreContext.TUCLProgram
                                                        .Any(x =>
                                                        x.Id != _Request.ReferenceId
                                                        && x.Name == _Request.Name);
                            if (NameCheck)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1525, TUCCoreResource.CA1525M);
                            }
                            ProgramDetails.Name = _Request.Name;
                        }

                        ProgramDetails.ProgramTypeId = ProgramTypeId;
                        ProgramDetails.ComissionTypeId = ComissionTypeId;
                        ProgramDetails.Comission = _Request.Comission;
                        ProgramDetails.RewardPercentage = _Request.RewardCap;
                        ProgramDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        ProgramDetails.ModifyById = _Request.UserReference.AccountId;
                        ProgramDetails.MaxInvoiceAmount = _Request.MaxInvoiceAmount;
                        ProgramDetails.MinInvoiceAmount = _Request.MinInvoiceAmount;
                        ProgramDetails.MaxRewards = _Request.MaxRewards;
                        ProgramDetails.MaxComission = _Request.MaxComission;
                        ProgramDetails.CashierCommission = _Request.CashierCommission;
                        ProgramDetails.CashierMaxReward = _Request.CashierMaxReward;

                        _HCoreContext.SaveChanges();
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1526, TUCCoreResource.CA1526M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateProgram", _Exception, _Request.UserReference);
                #endregion
            }

        }

        /// <summary>
        /// Description: Method defined to delete program
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteProgram(OProgram.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.TUCLProgram.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        bool ProgramCheck = _HCoreContext.TUCLProgramGroup.Any(x => x.ProgramId == Details.Id);
                        if (ProgramCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1527, TUCCoreResource.CA1527M);
                        }

                        Details.StatusId = HelperStatus.Default.Inactive;
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.ModifyById = _Request.UserReference.AccountId;

                        _HCoreContext.SaveChanges();
                        #region Send Response
                        var _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1528, TUCCoreResource.CA1528M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteProgram", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get program details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetProgram(OProgram.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OProgram.List _Details = _HCoreContext.TUCLProgram
                                                  .Where(x => x.Guid == _Request.ReferenceKey
                                                  && x.Id == _Request.ReferenceId)
                                                  .Select(x => new OProgram.List
                                                  {

                                                      ReferenceId = x.Id,
                                                      ReferenceKey = x.Guid,
                                                      Name = x.Name,
                                                      CreateDate = x.CreateDate,
                                                      CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                      ModifyDate = x.ModifyDate,
                                                      ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                      AccountId = x.AccountId,
                                                      AccountDisplayName = x.Account.DisplayName,
                                                      Comission = x.Comission,
                                                      ComissionTypeId = x.ComissionTypeId,
                                                      ComissionTypeName = x.ComissionType.Name,
                                                      ProgramTypeId = x.ProgramTypeId,
                                                      ProgramTypeName = x.ProgramType.Name,
                                                      SubscriptionId = x.SubscriptionId,
                                                      SubscriptionName = x.Subscription.Name,
                                                      StatusId = x.StatusId,
                                                      StatusCode = x.Status.SystemName,
                                                      StatusName = x.Status.Name,
                                                      RewardCap = x.RewardPercentage,
                                                      ProgramTypeCode = x.ProgramType.SystemName,
                                                      ComissionTypeCode = x.ComissionType.SystemName,
                                                      StartDate = x.StartDate,
                                                      EndDate = x.EndDate,
                                                      MaxInvoiceAmount = x.MaxInvoiceAmount,
                                                      MinInvoiceAmount = x.MinInvoiceAmount,
                                                      MaxRewards = x.MaxRewards,
                                                      MaxComission = x.MaxComission,
                                                      CashierCommission = x.CashierCommission,
                                                      CashierMaxReward = x.CashierMaxReward
                                                      
                                                  })
                                                .FirstOrDefault();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    if (_Details != null)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetProgram", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get list of  programs
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetPrograms(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.TUCLProgram
                                                 .Where(x => x.StatusId == HelperStatus.Default.Active)
                                                 .Select(x => new OProgram.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     Name = x.Name,
                                                     CreateDate = x.CreateDate,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                     AccountId = x.AccountId,
                                                     AccountDisplayName = x.Account.DisplayName,
                                                     Comission = x.Comission,
                                                     ComissionTypeId = x.ComissionTypeId,
                                                     ComissionTypeName = x.ComissionType.Name,
                                                     ProgramTypeId = x.ProgramTypeId,
                                                     ProgramTypeName = x.ProgramType.Name,
                                                     SubscriptionId = x.SubscriptionId,
                                                     SubscriptionName = x.Subscription.Name,
                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                     RewardCap = x.RewardPercentage,
                                                     ProgramTypeCode = x.ProgramType.SystemName,
                                                     ComissionTypeCode = x.ComissionType.SystemName,
                                                     StartDate = x.StartDate,
                                                     EndDate = x.EndDate,
                                                     MaxInvoiceAmount = x.MaxInvoiceAmount,
                                                     MinInvoiceAmount = x.MinInvoiceAmount,
                                                     MaxRewards = x.MaxRewards,
                                                     MaxComission = x.MaxComission,
                                                     CashierMaxReward = x.CashierMaxReward,
                                                     CashierCommission = x.CashierCommission
                                                     
                                                 })
                                                 .Where(_Request.SearchCondition)
                                                 .Count();
                    }
                    #endregion
                    #region Get Data
                    List<OProgram.List> Data = _HCoreContext.TUCLProgram
                                                 .Where(x => x.StatusId == HelperStatus.Default.Active)
                                                 .Select(x => new OProgram.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     Name = x.Name,
                                                     CreateDate = x.CreateDate,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                     AccountId = x.AccountId,
                                                     AccountDisplayName = x.Account.DisplayName,
                                                     Comission = x.Comission,
                                                     ComissionTypeId = x.ComissionTypeId,
                                                     ComissionTypeName = x.ComissionType.Name,
                                                     ProgramTypeId = x.ProgramTypeId,
                                                     ProgramTypeName = x.ProgramType.Name,
                                                     SubscriptionId = x.SubscriptionId,
                                                     SubscriptionName = x.Subscription.Name,
                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                     RewardCap = x.RewardPercentage,
                                                     ProgramTypeCode = x.ProgramType.SystemName,
                                                     ComissionTypeCode = x.ComissionType.SystemName,
                                                     StartDate = x.StartDate,
                                                     EndDate = x.EndDate,
                                                     MaxInvoiceAmount = x.MaxInvoiceAmount,
                                                     MinInvoiceAmount = x.MinInvoiceAmount,
                                                     MaxRewards = x.MaxRewards,
                                                     MaxComission = x.MaxComission,
                                                     CashierCommission = x.CashierCommission,
                                                     CashierMaxReward = x.CashierMaxReward
                                                 })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                    #endregion
                    #region Create  Response Object                    
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetPrograms", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to save program group
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveProgramGroup(OProgram.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.MerchantList.Count() <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1531, TUCCoreResource.CA1531M);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    List<TUCLProgramGroup> _TUCLProgramGroups = new List<TUCLProgramGroup>();
                    foreach (var items in _Request.MerchantList)
                    {
                        TUCLProgramGroup _TUCLProgramGroup = new TUCLProgramGroup();
                        _TUCLProgramGroup.Guid = HCoreHelper.GenerateGuid();
                        _TUCLProgramGroup.AccountId = items.ReferenceId;
                        _TUCLProgramGroup.ProgramId = _Request.ReferenceId;
                        _TUCLProgramGroup.StartDate = _Request.StartDate;
                        _TUCLProgramGroup.EndDate = _Request.EndDate;
                        _TUCLProgramGroup.RewardCap = _Request.RewardCap;
                        _TUCLProgramGroup.CreateDate = HCoreHelper.GetGMTDateTime();
                        _TUCLProgramGroup.CreatedById = _Request.UserReference.AccountId;
                        _TUCLProgramGroup.StatusId = HelperStatus.Default.Active;
                        _TUCLProgramGroups.Add(_TUCLProgramGroup);

                    }
                    _HCoreContext.TUCLProgramGroup.AddRange(_TUCLProgramGroups);
                    _HCoreContext.SaveChanges();
                    var _Response = new
                    {
                        ReferenceId = _Request.ReferenceId,
                        ReferenceKey = _Request.ReferenceKey,
                    };
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1529, TUCCoreResource.CA1529M);
                    #endregion

                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveProgramGroup", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to update program
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateProgramGroup(OProgram.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                using (_HCoreContext = new HCoreContext())
                {

                    var ProgramDetails = _HCoreContext.TUCLProgramGroup
                                                                 .Where(x => x.Program.Id == _Request.ReferenceId
                                                                 && x.Program.Guid == _Request.ReferenceKey).ToList();
                    if (ProgramDetails.Count() > 0)
                    {
                        _HCoreContext.TUCLProgramGroup.RemoveRange(ProgramDetails);
                        List<TUCLProgramGroup> _TUCLProgramGroups = new List<TUCLProgramGroup>();
                        foreach (var items in _Request.MerchantList)
                        {
                            TUCLProgramGroup _TUCLProgramGroup = new TUCLProgramGroup();
                            _TUCLProgramGroup.Guid = HCoreHelper.GenerateGuid();
                            _TUCLProgramGroup.AccountId = items.ReferenceId;
                            _TUCLProgramGroup.ProgramId = _Request.ReferenceId;
                            _TUCLProgramGroup.StartDate = _Request.StartDate;
                            _TUCLProgramGroup.EndDate = _Request.EndDate;
                            _TUCLProgramGroup.RewardCap = _Request.RewardCap;
                            _TUCLProgramGroup.CreateDate = HCoreHelper.GetGMTDateTime();
                            _TUCLProgramGroup.CreatedById = _Request.UserReference.AccountId;
                            _TUCLProgramGroup.StatusId = HelperStatus.Default.Active;
                            _TUCLProgramGroups.Add(_TUCLProgramGroup);

                        }
                        _HCoreContext.TUCLProgramGroup.AddRange(_TUCLProgramGroups);
                        _HCoreContext.SaveChanges();
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1530, TUCCoreResource.CA1530M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateProgramGroup", _Exception, _Request.UserReference);
                #endregion
            }

        }

        /// <summary>
        /// Description: Method defined to get program group detail
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetProgramGroup(OProgram.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OProgram.List _Details = _HCoreContext.TUCLProgram
                                                  .Where(x => x.Guid == _Request.ReferenceKey
                                                  && x.Id == _Request.ReferenceId)
                                                  .Select(x => new OProgram.List
                                                  {

                                                      ReferenceId = x.Id,
                                                      ReferenceKey = x.Guid,
                                                      Name = x.Name,
                                                      CreateDate = x.CreateDate,
                                                      CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                      ModifyDate = x.ModifyDate,
                                                      ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                      RewardCap = x.RewardPercentage,
                                                      StartDate = x.StartDate,
                                                      EndDate = x.EndDate,
                                                      StatusId = x.StatusId,
                                                      StatusCode = x.Status.SystemName,
                                                      StatusName = x.Status.Name,
                                                      MerchantList = x.TUCLProgramGroup.Where(y => y.ProgramId == x.Id)
                                                                     .Select(y => new OProgram.MerchantList
                                                                     {
                                                                         ReferenceId = y.AccountId,
                                                                         ReferenceKey = y.Account.Guid,
                                                                         DisplayName = y.Account.DisplayName
                                                                     }).ToList()
                                                  })
                                                .FirstOrDefault();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    if (_Details != null)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetProgramGroup", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get list of  program groups
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetProgramGroups(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.TUCLProgram
                                                 .Where(x => x.StatusId == HelperStatus.Default.Active
                                                  && x.ProgramType.SystemName == "programtype.grouployalty")
                                                 .Select(x => new OProgram.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     Name = x.Name,
                                                     CreateDate = x.CreateDate,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                     RewardCap = x.RewardPercentage,
                                                     StartDate = x.StartDate,
                                                     EndDate = x.EndDate,
                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                     MerchantList = x.TUCLProgramGroup.Where(y => y.ProgramId == x.Id)
                                                                     .Select(y => new OProgram.MerchantList
                                                                     {
                                                                         ReferenceId = y.AccountId,
                                                                         ReferenceKey = y.Account.Guid,
                                                                         DisplayName = y.Account.DisplayName
                                                                     }).ToList()
                                                 })
                                                 .Where(_Request.SearchCondition)
                                                 .Count();
                    }
                    #endregion
                    #region Get Data
                    List<OProgram.List> Data = _HCoreContext.TUCLProgram
                                                 .Where(x => x.StatusId == HelperStatus.Default.Active
                                                  && x.ProgramType.SystemName == "programtype.grouployalty")
                                                 .Select(x => new OProgram.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     Name = x.Name,
                                                     CreateDate = x.CreateDate,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                     RewardCap = x.RewardPercentage,
                                                     StartDate = x.StartDate,
                                                     EndDate = x.EndDate,
                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                     MerchantList = x.TUCLProgramGroup.Where(y => y.ProgramId == x.Id)
                                                                     .Select(y => new OProgram.MerchantList
                                                                     {
                                                                         ReferenceId = y.AccountId,
                                                                         ReferenceKey = y.Account.Guid,
                                                                         DisplayName = y.Account.DisplayName
                                                                     }).ToList()
                                                 })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                    #endregion
                    #region Create  Response Object                    
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetProgramGroups", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }

        internal void UpdateProgramStatus()
        {
            #region Manage Exception
            try
            {

                using (_HCoreContext = new HCoreContext())
                {
                    var StatusList = _HCoreContext.TUCLProgram.Where(x => x.StartDate.Value.Date == HCoreHelper.GetGMTDate() && x.StatusId == HelperStatus.Default.Inactive).ToList();
                    if (StatusList.Count() > 0)
                    {
                        foreach (var list in StatusList)
                        {
                            list.StatusId = HelperStatus.Default.Active;
                            list.ModifyDate = HCoreHelper.GetGMTDate();
                        }
                        _HCoreContext.TUCLProgram.UpdateRange(StatusList);
                        _HCoreContext.SaveChanges();
                    }
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "UpdateProgramStatus", _Exception, null);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to delete program group
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteProgramGroup(OProgram.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.TUCLProgramGroup.Where(x => x.ProgramId == _Request.ReferenceId
                                                && x.Program.Guid == _Request.ReferenceKey
                                                && x.AccountId == _Request.AccountId).FirstOrDefault();
                    if (Details != null)
                    {
                        bool ProgramCheck = _HCoreContext.TUCLProgramGroup.Any(x => x.ProgramId == Details.Id);
                        if (_Request.IsRemove)
                        {
                            _HCoreContext.TUCLProgramGroup.Remove(Details);
                            var _Response = new
                            {
                                ReferenceId = _Request.ReferenceId,
                                ReferenceKey = _Request.ReferenceKey,
                            };
                            _HCoreContext.SaveChanges();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1535, TUCCoreResource.CA1535M);
                        }
                        else
                        {

                            Details.StatusId = _Request.IsEnableDisable ? HelperStatus.Default.Active : HelperStatus.Default.Inactive;
                            Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            Details.ModifyById = _Request.UserReference.AccountId;
                            _HCoreContext.SaveChanges();
                            var _Response = new
                            {
                                ReferenceId = _Request.ReferenceId,
                                ReferenceKey = _Request.ReferenceKey,
                            };
                            string message = _Request.IsEnableDisable ? "Merchant enabled successfully." : "Merchant disabled successfully.";
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1536, message);
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteProgramGroup", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Topups the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse TopupAccount(Object.Merchant.OSubscription.Topup.Request _Request)
        {
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                if (_Request.Amount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1084, TUCCoreResource.CA1084M);
                }
                if (_Request.TransactionId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1086, TUCCoreResource.CA1086M);
                }
                if (string.IsNullOrEmpty(_Request.PaymentReference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1084, TUCCoreResource.CA1084M);
                }
                _CoreTransactionRequest = new OCoreTransaction.Request();
                _CoreTransactionRequest.UserReference = _Request.UserReference;
                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                _CoreTransactionRequest.ParentId = _Request.AccountId;
                _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                _CoreTransactionRequest.PaymentMethodId = Helpers.PaymentMethod.Bank;
                _CoreTransactionRequest.ProgramId = _Request.ProgramId;
                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                {
                    UserAccountId = _Request.AccountId,
                    ModeId = TransactionMode.Credit,
                    TypeId = TransactionType.AcquirerCredit,
                    SourceId = TransactionSource.TUCBlack,
                    Amount = _Request.Amount,
                    Comission = 0,
                    TotalAmount = _Request.Amount,
                });
                _CoreTransactionRequest.Transactions = _TransactionItems;
                _ManageCoreTransaction = new ManageCoreTransaction();
                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1087, TUCCoreResource.CA1087M);
                    #endregion
                }
                else
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1088, TUCCoreResource.CA1088M);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("TopupAccount", _Exception);
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                #endregion
            }
        }


        /// <summary>
        /// Description: Gets the account balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountBalance(Object.Merchant.OSubscription.Balance.Request _Request)
        {

            _BalanceResponse = new Object.Merchant.OSubscription.Balance.Response();
            #region Manage Exception
            try
            {
                DateTime? LastTransactionDate = HCoreHelper.GetGMTDate();
                using (_HCoreContext = new HCoreContext())
                {
                    _BalanceResponse.Credit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                             .Where(x => ((x.AccountId == _Request.AccountId &&
                                                     x.SourceId == TransactionSource.TUCBlack) && (x.ProgramId == _Request.ProgramId)) &&
                                                    x.ModeId == TransactionMode.Credit &&
                                                    (x.StatusId == Transaction.Success || x.StatusId == Transaction.Pending))
                                             .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                    _BalanceResponse.Debit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                     .Where(x => ((x.AccountId == _Request.AccountId &&
                                                     x.SourceId == TransactionSource.TUCBlack) && (x.ProgramId == _Request.ProgramId)) &&
                                                             x.ModeId == TransactionMode.Debit &&
                                                             (x.StatusId == Transaction.Success || x.StatusId == Transaction.Pending))
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                    var LastTransaction = _HCoreContext.HCUAccountTransaction
                                             .Where(x => ((x.AccountId == _Request.AccountId &&
                                                     x.SourceId == TransactionSource.TUCBlack) && (x.ProgramId == _Request.ProgramId)) &&
                                                    x.ModeId == TransactionMode.Credit &&
                                                   (x.StatusId == Transaction.Success || x.StatusId == Transaction.Pending))
                                             .OrderByDescending(x => x.TransactionDate)
                                             .Select(x => new
                                             {
                                                 x.TotalAmount,
                                                 x.TransactionDate
                                             }).FirstOrDefault();
                    if (LastTransaction != null)
                    {
                        _BalanceResponse.LastTransactionAmount = HCoreHelper.RoundNumber(LastTransaction.TotalAmount, _AppConfig.SystemEntryRoundDouble);
                        _BalanceResponse.LastTransactionDate = LastTransaction.TransactionDate;
                    }
                    if (_BalanceResponse.LastTransactionDate != null)
                    {
                        double TCredit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                        .Where(x => (x.AccountId == _Request.AccountId && x.ProgramId == _Request.ProgramId) &&
                                        x.TransactionDate < _BalanceResponse.LastTransactionDate &&
                                               x.SourceId == TransactionSource.TUCBlack && x.ModeId == TransactionMode.Credit &&
                                                (x.StatusId == Transaction.Success || x.StatusId == Transaction.Pending))
                                        .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                        double TDebit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                         .Where(x => (x.AccountId == _Request.AccountId && x.ProgramId == _Request.ProgramId) &&
                                        x.TransactionDate < _BalanceResponse.LastTransactionDate &&
                                                                x.SourceId == TransactionSource.TUCBlack &&
                                                                 x.ModeId == TransactionMode.Debit &&
                                                                 (x.StatusId == Transaction.Success || x.StatusId == Transaction.Pending))
                                                          .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                        _BalanceResponse.LastTopupBalance = HCoreHelper.RoundNumber(((TCredit - TDebit) + _BalanceResponse.LastTransactionAmount), _AppConfig.SystemEntryRoundDouble);
                    }
                    _BalanceResponse.Balance = HCoreHelper.RoundNumber((_BalanceResponse.Credit - _BalanceResponse.Debit), _AppConfig.SystemEntryRoundDouble);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, TUCCoreResource.CA1090, TUCCoreResource.CA1090M);
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAccountBalance", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
