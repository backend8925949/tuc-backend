﻿//using System;
//using HCore.Data;
//using HCore.Data.Models;
//using HCore.Operations;
//using HCore.Operations.Object;
//using HCore.TUC.Core.Object.Merchant;
//using static HCore.Helper.HCoreConstant.Helpers;
//using System.Linq;
//using static HCore.TUC.Core.Object.Merchant.OAnalytics;
//using static HCore.Helper.HCoreConstant.Helpers.TransactionType;
//using Akka.Actor;
//using HCore.Helper;
//using HCore.TUC.Core.Framework.Console;
//using static ClosedXML.Excel.XLPredefinedFormat;
//using static HCore.Helper.HCoreConstant;
//using DateTime = System.DateTime;
//using HCore.TUC.Core.Framework.Merchant;
//using Delivery.Object.Response.Shipments;
//using SendGrid.Helpers.Mail;
//using SendGrid;
//using DocumentFormat.OpenXml.Wordprocessing;
//using HCore.Data.Logging;
//using HCore.Data.Logging.Models;

//namespace HCore.TUC.Core.Framework.Operations
//{
//    public class FrameworkSendMails
//    {
//        HCoreContext _HCoreContext;
//        //ManageCoreTransaction _ManageCoreTransaction;
//        //List<OAccounts.Customer.List> _Customers;
//        //OAccounts.Terminal.Overview _TerminalOverview;


//        /// <summary>
//        /// Author: Azeez Dauda
//        /// Description:Method to Send Weekly Activity report to Merchant.
//        /// </summary>
//        internal void SendWeeklyReportToMerchant()
//        {

//            #region Manage Exception
//            try
//            {
//                using (_HCoreContext = new HCoreContext())
//                {
//                    DateTime endDate = HCoreHelper.GetGMTDateTime();
//                    DateTime startDate = endDate.AddDays(-7).Date;
//                    if ((int)endDate.DayOfWeek == 0) // This will run on sunday only
//                    {
//                        var merchantList = _HCoreContext.HCUAccount
//                                                           .Where(x => x.AccountTypeId == UserAccountType.Merchant
//                                                               && x.StatusId == HelperStatus.Default.Active)
//                                                           .Select(x => new MerchantList
//                                                           {
//                                                               ReferenceId = x.Id,
//                                                               ReferenceKey = x.Guid,
//                                                               DisplayName = x.DisplayName,
//                                                               EmailAddress = x.EmailAddress
//                                                           }).ToList();
//                        if (merchantList.Count > 0)
//                        {
//                            using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
//                            {

//                                var system = ActorSystem.Create("ActorMerchantWeeklyReport");
//                                var greeter = system.ActorOf<ActorMerchantWeeklyReport>("ActorMerchantWeeklyReport");
//                                foreach (var merchant in merchantList)
//                                {
//                                    bool isSendEmail = _HCoreContextLogging.HCLCoreNotification
//                                               .Any(x => x.AccountId == merchant.ReferenceId && x.Attempts == 1
//                                                && x.SendDate.Day == endDate.Day);
//                                    if (!isSendEmail)
//                                    {
//                                        merchant.startDate = startDate;
//                                        merchant.endDate = endDate;
//                                        greeter.Tell(merchant);
//                                    }
//                                    else
//                                    {
//                                        break;
//                                    }
//                                }

//                            }
//                        }
//                    }

//                }
//            }
//            catch (Exception ex)
//            {
//                HCoreHelper.LogException("SendEmail", ex);
//            }
//            #endregion

//        }

//        public void SendEmail(MerchantList merchant)
//        {
//            try
//            {
//                using (_HCoreContext = new HCoreContext())
//                {

//                    var customerTotal = _HCoreContext.HCUAccount
//                                               .Count(x => x.AccountTypeId == UserAccountType.Appuser
//                                              && (x.OwnerId == merchant.ReferenceId || x.cmt_loyalty_customerowner.Any()
//                                              || x.HCUAccountTransactionAccount.Any(m => m.TransactionDate >= merchant.startDate && m.TransactionDate < merchant.endDate
//                                                                                               && m.ParentId == merchant.ReferenceId
//                                                                                               && m.StatusId == HelperStatus.Transaction.Success
//                                                                                               && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack)
//                                                                                               && m.TypeId != TransactionType.ThankUCashPlusCredit
//                                                                                                )));
//                    var newCustomer = _HCoreContext.HCUAccount
//                                               .Count(x => x.AccountTypeId == UserAccountType.Appuser
//                                              && (x.OwnerId == merchant.ReferenceId || x.cmt_loyalty_customerowner.Any()
//                                              || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
//                                                                                            && m.TransactionDate >= merchant.startDate && m.TransactionDate < merchant.endDate
//                                                                                            && m.ParentId == merchant.ReferenceId
//                                                                                            && m.StatusId == HelperStatus.Transaction.Success
//                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus
//                                                                                            && m.ModeId == TransactionMode.Credit
//                                                                                                ))
//                                               && x.HCUAccountTransactionAccount.Count(m => m.ParentId == merchant.ReferenceId && m.TransactionDate >= merchant.startDate && m.TransactionDate < merchant.endDate) == 1);



//                    var returningCustomer = _HCoreContext.HCUAccount
//                                               .Count(x => x.AccountTypeId == UserAccountType.Appuser
//                                              && (x.OwnerId == merchant.ReferenceId || x.cmt_loyalty_customerowner.Any()
//                                              || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
//                                                                                            && m.TransactionDate >= merchant.startDate && m.TransactionDate < merchant.endDate
//                                                                                            && m.ParentId == merchant.ReferenceId
//                                                                                            && m.StatusId == HelperStatus.Transaction.Success
//                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus
//                                                                                            && m.ModeId == TransactionMode.Credit
//                                                                                                ))
//                                               && x.HCUAccountTransactionAccount.Count(m => m.ParentId == merchant.ReferenceId && m.TransactionDate >= merchant.startDate && m.TransactionDate < merchant.endDate) > 1);
//                    var lostCustomer = _HCoreContext.HCUAccount
//                                               .Count(x => x.AccountTypeId == UserAccountType.Appuser
//                                              && (x.OwnerId == merchant.ReferenceId || x.cmt_loyalty_customerowner.Any()
//                                              || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id
//                                                                                           && m.TransactionDate >= merchant.startDate && m.TransactionDate < merchant.endDate
//                                                                                           && m.ParentId == merchant.ReferenceId
//                                                                                           && m.StatusId == HelperStatus.Transaction.Success
//                                                                                           && m.SourceId == TransactionSource.ThankUCashPlus
//                                                                                           && m.ModeId == TransactionMode.Credit
//                                                                                                ))
//                                               && x.HCUAccountTransactionAccount.Count(m => m.ParentId == merchant.ReferenceId && m.TransactionDate >= merchant.startDate && m.TransactionDate < merchant.endDate) == 0);


//                    var noOfTransactionPerWeek = _HCoreContext.HCUAccountTransaction
//                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
//                                                && x.TransactionDate >= merchant.startDate && x.TransactionDate < merchant.endDate
//                                                && x.ParentId == merchant.ReferenceId
//                                                && (x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit)
//                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
//                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward))
//                                                .Count();

//                    var salesAmountPerWeek = _HCoreContext.HCUAccountTransaction
//                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser && x.TransactionDate >= merchant.startDate && x.TransactionDate < merchant.endDate
//                                                && x.ParentId == merchant.ReferenceId
//                                                && (x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit)
//                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
//                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward))
//                                                .Select(x => new HCore.TUC.Core.Object.Merchant.OTransaction.Sale
//                                                {
//                                                    ReferenceId = x.Id,
//                                                    ReferenceKey = x.Guid,
//                                                    TotalAmount = x.TotalAmount,
//                                                    InvoiceAmount = x.PurchaseAmount,
//                                                    TransactionDate = x.TransactionDate,

//                                                })
//                                                .Sum(x => x.InvoiceAmount);

//                    List<OAccounts.Cashier.List> _lst = new List<OAccounts.Cashier.List>();
//                    _lst = _HCoreContext.HCUAccount
//                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier
//                                                && x.LastActivityDate >= merchant.startDate && x.LastActivityDate < merchant.endDate
//                                                && (x.Owner.OwnerId == merchant.ReferenceId || x.OwnerId == merchant.ReferenceId))
//                                                .Select(x => new OAccounts.Cashier.List
//                                                {
//                                                    ReferenceId = x.Id,
//                                                    ReferenceKey = x.Guid,
//                                                    Name = x.DisplayName,
//                                                    IconUrl = x.IconStorage.Path,
//                                                    Transactions = x.HCUAccountTransactionCashier.Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
//                                                                                                 && x.ParentId == merchant.ReferenceId
//                                                                                                 && x.CashierId == x.Id
//                                                                                                 && (x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit)
//                                                                                                 && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
//                                                                                                 && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)).Count(),
//                                                    TotalTransAmount = x.HCUAccountTransactionCashier.Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
//                                                                                                 && x.ParentId == merchant.ReferenceId
//                                                                                                 && x.CashierId == x.Id
//                                                                                                 && (x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit)
//                                                                                                 && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
//                                                                                                 && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)).Sum(x => x.TotalAmount),


//                                                })
//                                       .OrderByDescending(x => x.Transactions)
//                                       .Take(3)
//                                       .ToList();
//                    string htmlstring = string.Empty;
//                    int count = 0;
//                    foreach (var DataItem in _lst)
//                    {
//                        htmlstring = string.Empty;
//                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
//                        {
//                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
//                        }
//                        else
//                        {
//                            DataItem.IconUrl = _AppConfig.Default_Icon;
//                        }
//                        htmlstring = htmlstring + "<td valign='top' width='30%' style='padding-top:20px;'>";
//                        htmlstring = htmlstring + "<table role='presentation' cellspacing='0' cellpadding='0' border='0' width='100%'>";
//                        htmlstring = htmlstring + "    <tr>";
//                        htmlstring = htmlstring + "        <td class='box'>";
//                        if (count == 0)
//                        {
//                            htmlstring = htmlstring + "            <div class='ribbon-1 right'>FIRST</div>";
//                        }
//                        else if (count == 1)
//                        {
//                            htmlstring = htmlstring + "            <div class='ribbon-1 right'>SECOND</div>";
//                        }
//                        else
//                        {
//                            htmlstring = htmlstring + "            <div class='ribbon-1 right'>THIRD</div>";
//                        }
//                        htmlstring = htmlstring + "            <img src='" + DataItem.IconUrl + "' alt='' style='width:100%;max-width:600px;height:auto;margin:auto;display:block;'>";
//                        htmlstring = htmlstring + "        </td>";
//                        htmlstring = htmlstring + "    </tr>";
//                        htmlstring = htmlstring + "    <tr>";
//                        htmlstring = htmlstring + "        <td class='text-services' style='text-align:left;background-color:#FFFFFF;padding-left:13px;'>";
//                        htmlstring = htmlstring + "            <p class='meta'><span>" + DataItem.Name + "</span></p>";
//                        htmlstring = htmlstring + "            <p><b style='color:#004FBF'>" + DataItem.Transaction + " </b>transactions</p>";
//                        htmlstring = htmlstring + "            <p><a style='color:#004FBF'>&#8358; " + DataItem.TotalTransAmount + "</a></p>";
//                        htmlstring = htmlstring + "        </td>";
//                        htmlstring = htmlstring + "    </tr>";
//                        htmlstring = htmlstring + "</table>";
//                        htmlstring = htmlstring + "</td>";
//                        count++;

//                    }


//                    var totalPosTerminal = _HCoreContext.TUCTerminal
//                                           .Where(x => x.MerchantId == merchant.ReferenceId)
//                                           .Count();
//                    var activePosTerminal = _HCoreContext.TUCTerminal
//                                            .Where(x => x.MerchantId == merchant.ReferenceId
//                                            && x.StatusId == HelperStatus.Default.Active
//                                            && x.LastTransactionDate >= merchant.startDate
//                                            && x.LastTransactionDate < merchant.endDate)
//                                            .Count();
//                    var inactivePosTerminal = _HCoreContext.TUCTerminal
//                                            .Where(x => x.MerchantId == merchant.ReferenceId
//                                            && x.StatusId == HelperStatus.Default.Inactive
//                                            && x.LastTransactionDate >= merchant.startDate
//                                            && x.LastTransactionDate < merchant.endDate)
//                                            .Count();


//                    var rewardedCustomer = _HCoreContext.HCUAccountTransaction
//                                                  .Where(x => x.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
//                                                  && x.Account.AccountTypeId == UserAccountType.Appuser
//                                                  && x.TransactionDate >= merchant.startDate && x.TransactionDate < merchant.endDate
//                                                  && x.ParentId == merchant.ReferenceId
//                                                  && x.ModeId == TransactionMode.Credit
//                                                  && x.ParentTransaction.TotalAmount > 0
//                                                  && x.CampaignId == null
//                                                  && ((x.TypeId != TransactionType.ThankUCashPlusCredit
//                                                  && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
//                                                  || x.SourceId == TransactionSource.ThankUCashPlus))
//                                                  .Select(x => new HCore.TUC.Core.Object.Merchant.OTransaction.Sale
//                                                  {
//                                                      ReferenceId = x.Id,
//                                                      ReferenceKey = x.Guid,
//                                                  })
//                                                  .Count();

//                    var amountRewarded = _HCoreContext.HCUAccountTransaction
//                                                   .Where(x => x.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
//                                                   && x.Account.AccountTypeId == UserAccountType.Appuser
//                                                    && x.TransactionDate >= merchant.startDate && x.TransactionDate < merchant.endDate
//                                               && x.ParentId == merchant.ReferenceId
//                                               && x.CampaignId == null
//                                               && x.ModeId == TransactionMode.Credit
//                                               && x.ParentTransaction.TotalAmount > 0
//                                               && ((x.TypeId != TransactionType.ThankUCashPlusCredit
//                                                   && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
//                                               || x.SourceId == TransactionSource.ThankUCashPlus))
//                                               .Select(x => new HCore.TUC.Core.Object.Merchant.OTransaction.Sale
//                                               {
//                                                   ReferenceId = x.Id,
//                                                   ReferenceKey = x.Guid,
//                                               })
//                                               .Count();

//                    var redeemedCustomer = _HCoreContext.HCUAccountTransaction
//                                           .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
//                                            && x.TransactionDate >= merchant.startDate && x.TransactionDate < merchant.endDate
//                                           && x.ParentId == merchant.ReferenceId
//                                           && x.ModeId == TransactionMode.Debit
//                                           && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
//                                           && x.Type.SubParentId == TransactionTypeCategory.Redeem)
//                                           .Select(x => new HCore.TUC.Core.Object.Merchant.OTransaction.Sale
//                                           {
//                                               ReferenceId = x.Id,
//                                               ReferenceKey = x.Guid,
//                                           })
//                                           .Count();

//                    var redeemAmount = _HCoreContext.HCUAccountTransaction
//                                             .Where(x => x.Account != null && x.Account.AccountTypeId == UserAccountType.Appuser
//                                              && x.TransactionDate >= merchant.startDate && x.TransactionDate < merchant.endDate
//                                             && x.ParentId == merchant.ReferenceId
//                                             && x.ModeId == TransactionMode.Debit
//                                             && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
//                                             && x.Type.SubParentId == TransactionTypeCategory.Redeem)
//                                           .Select(x => new HCore.TUC.Core.Object.Merchant.OTransaction.Sale
//                                           {
//                                               ReferenceId = x.Id,
//                                               ReferenceKey = x.Guid,
//                                               RedeemAmount = x.ReferenceAmount
//                                           })
//                                           .Sum(x => x.RedeemAmount);

//                    string PanelUrl = "";
//                    if (HostEnvironment == HostEnvironmentType.Dev)
//                    {
//                        PanelUrl = "https://merchant.thankucash.dev";
//                    }
//                    else if (HostEnvironment == HostEnvironmentType.Tech)
//                    {
//                        PanelUrl = "https://merchant.thankucash.tech";
//                    }
//                    else if (HostEnvironment == HostEnvironmentType.Test)
//                    {
//                        PanelUrl = "https://merchant.thankucash.co";
//                    }
//                    else
//                    {
//                        PanelUrl = "https://merchant.thankucash.com";
//                    }

//                    using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
//                    {
//                        HCLCoreNotification _HCLCoreNotification = new HCLCoreNotification
//                        {
//                            Guid = HCoreHelper.GenerateGuid(),
//                            TypeId = Helpers.NotificationType.Email,
//                            ToName = merchant.DisplayName,
//                            Message = "Weekly MerchantReport Log",
//                            SendDate = HCoreHelper.GetGMTDate(),
//                            Attempts = 1,
//                            CreateDate = HCoreHelper.GetGMTDate(),
//                            StatusId = 2,
//                            AccountId = merchant.ReferenceId
//                        };
//                        _HCoreContextLogging.HCLCoreNotification.Add(_HCLCoreNotification);
//                        _HCoreContextLogging.SaveChanges();
//                    }

//                    var _EmailParameters = new
//                    {
//                        MerchantDisplayName = merchant.DisplayName,
//                        EmailAddress = merchant.EmailAddress,
//                        TotalCustomer = customerTotal,
//                        NewCustomer = newCustomer,
//                        ReturningCustomer = returningCustomer,
//                        LostCustomer = lostCustomer,
//                        WeeklyTransaction = noOfTransactionPerWeek,
//                        WeeklySales = salesAmountPerWeek,
//                        TopPerformingCashier = htmlstring,
//                        AllPOSTerminal = totalPosTerminal,
//                        ActivePOS = activePosTerminal,
//                        InAactivePos = inactivePosTerminal,
//                        TotalPointRewarded = amountRewarded,
//                        CustomerRewarded = rewardedCustomer,
//                        TotalAmountRedeem = redeemAmount,
//                        RedeemedCustomer = redeemedCustomer,
//                        PanelUrl = PanelUrl
//                    };
//                    HCoreHelper.BroadCastEmail("d-80000b25d4704d139b1bd0f90d1e66df", merchant.DisplayName, merchant.EmailAddress, _EmailParameters, null);
//                }
//            }
//            catch (Exception ex)
//            {
//                HCoreHelper.LogException("SendEmail_Innerloop", ex);
//            }
//        }

//    }
//    internal class ActorMerchantWeeklyReport : ReceiveActor
//    {

//        public ActorMerchantWeeklyReport()
//        {
//            Receive<MerchantList>(_Request =>
//            {
//                FrameworkSendMails _FrameworkSendMails = new FrameworkSendMails();
//                _FrameworkSendMails.SendEmail(_Request);
//            });

//        }
//    }

//    public class MerchantList
//    {

//        public long ReferenceId { get; set; }
//        public string ReferenceKey { get; set; }
//        public string DisplayName { get; set; }
//        public string EmailAddress { get; set; }
//        public DateTime endDate { get; set; }
//        public DateTime startDate { get; set; }
//    }

//}

