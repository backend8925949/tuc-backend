//==================================================================================
// FileName: FrameworkCustomerOnboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to onboard customers
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
//using HCore.TUC.Core.Object.Acquirer;
using HCore.TUC.Core.Resource;
using System.Collections.Generic;
using HCore.Data.Operations;
using Akka.Actor;
//using HCore.TUC.Core.Framework.Acquirer.Actor;
using HCore.Operations;
using HCore.TUC.Core.Object.Operations;

namespace HCore.TUC.Core.Framework.Operations
{
    internal class FrameworkCustomerOnboarding
    {
        Random _Random;
        HCoreContext _HCoreContext;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        HCoreContextOperations _HCoreContextOperations;
        /// <summary>
        /// Description: Method defined to onboard customer
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse OnboardCustomer(OCustomerOboarding.Request _Request)
        {
            #region Manage Exception
            try
            {
                string AllowSms = HCoreHelper.GetConfiguration("custimportonboardallowsms", _Request.AccountId);
                string Message = HCoreHelper.GetConfiguration("custimportonboardmessage", _Request.AccountId);
                string InfoBipUrl = HCoreHelper.GetConfiguration("infobip_url", _Request.AccountId);
                string InfoBipKey = HCoreHelper.GetConfiguration("infobip_key", _Request.AccountId);
                string InfoBipSenderId = HCoreHelper.GetConfiguration("infobip_senderid", _Request.AccountId);
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1306, TUCCoreResource.CA1306M);
                }
                else if (_Request.MobileNumber.Length < 8)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1307, TUCCoreResource.CA1307M);
                }
                else if (_Request.MobileNumber.Length > 14)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1308, TUCCoreResource.CA1308M);
                }
                else if (!System.Text.RegularExpressions.Regex.IsMatch(_Request.MobileNumber, "^[0-9]*$"))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1309, TUCCoreResource.CA1309M);
                }
                else
                {
                    long AccountId = 0;
                    #region Manage Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        var OwnerDetails = _HCoreContext.HCUAccount
                            .Where(x => x.Id == _Request.AccountId
                            && (x.AccountTypeId == UserAccountType.Merchant
                            || x.AccountTypeId == UserAccountType.Acquirer
                           || x.AccountTypeId == UserAccountType.Partner))
                            .Select(x => new
                            {
                                AccountTypeId = x.AccountTypeId,
                            }).FirstOrDefault();
                        if (OwnerDetails == null)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1312, TUCCoreResource.CA1312M);
                        }
                        var countrydetails = _HCoreContext.HCCoreCountry
                                              .Where(x => x.Id == _Request.CountryId && x.Guid == _Request.CountryKey)
                                              .Select(x => new
                                              {
                                                  CountryIsd = x.Isd,
                                                  MobileLength = x.MobileNumberLength
                                              }).FirstOrDefault();
                        string MobileNumber = string.Empty;
                        if (countrydetails != null)
                        {
                            MobileNumber = HCoreHelper.FormatMobileNumber(countrydetails.CountryIsd, _Request.MobileNumber,countrydetails.MobileLength);
                        }
                        else
                        {
                            MobileNumber = HCoreHelper.FormatMobileNumber("234", _Request.MobileNumber);
                        }
                        #region  Process Registration
                        string CustomerAppId = _AppConfig.AppUserPrefix + MobileNumber;
                        bool AccountCheck = _HCoreContext.HCUAccountAuth.Any(x => x.Username == CustomerAppId);
                        if (!AccountCheck)
                        {
                            if (!string.IsNullOrEmpty(_Request.EmailAddress))
                            {
                                var EmailCheck = _HCoreContext.HCUAccount.Any(x => x.AccountTypeId == UserAccountType.Appuser && x.EmailAddress == _Request.EmailAddress);
                                if (EmailCheck)
                                {
                                    _HCoreContext.Dispose();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1393, TUCCoreResource.CA1393M);
                                }
                            }
                            DateTime CreateDate = HCoreHelper.GetGMTDateTime();
                            int GenderId = 0;
                            //string MerchantDisplayName = "";
                            //if (_Request.OwnerId != 0)
                            //{
                            //    MerchantDisplayName = DataStore.DataStore.Merchants.Where(x => x.ReferenceId == _Request.OwnerId).Select(x => x.DisplayName).FirstOrDefault();
                            //    if (string.IsNullOrEmpty(MerchantDisplayName))
                            //    {
                            //        MerchantDisplayName = DataStore.DataStore.Partners.Where(x => x.ReferenceId == _Request.OwnerId && (x.AccountTypeId == UserAccountType.Acquirer || x.AccountTypeId == UserAccountType.Partner)).Select(x => x.DisplayName).FirstOrDefault();
                            //    }
                            //}
                            if (!string.IsNullOrEmpty(_Request.Gender))
                            {
                                if (_Request.Gender == "gender.male" || _Request.Gender == "male")
                                {
                                    GenderId = Gender.Male;
                                }
                                if (_Request.Gender == "gender.female" || _Request.Gender == "female")
                                {
                                    GenderId = Gender.Female;
                                }
                                if (_Request.Gender == "gender.other" || _Request.Gender == "other")
                                {
                                    GenderId = Gender.Other;
                                }
                            }
                            if (!string.IsNullOrEmpty(_Request.GenderCode))
                            {
                                if (_Request.GenderCode == "gender.male" || _Request.GenderCode == "male")
                                {
                                    GenderId = Gender.Male;
                                }
                                if (_Request.GenderCode == "gender.female" || _Request.GenderCode == "female")
                                {
                                    GenderId = Gender.Female;
                                }
                                if (_Request.GenderCode == "gender.other" || _Request.GenderCode == "other")
                                {
                                    GenderId = Gender.Other;
                                }
                            }
                            string UserKey = HCoreHelper.GenerateGuid();
                            string UserAccountKey = HCoreHelper.GenerateGuid();
                            _Random = new Random();
                            string ReferenceNumber = _Random.Next(1111, 9999).ToString();
                            string AccessPin = _Random.Next(1111, 9999).ToString();
                            #region Save User
                            _HCUAccountAuth = new HCUAccountAuth();
                            _HCUAccountAuth.Guid = UserKey;
                            _HCUAccountAuth.Username = CustomerAppId;
                            _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(MobileNumber);
                            _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                            _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                            _HCUAccountAuth.CreateDate = CreateDate;
                            _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                            _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                            #endregion
                            #region Online Account
                            _HCUAccount = new HCUAccount();
                            _HCUAccount.Guid = UserAccountKey;
                            _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                            _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                            if (_Request.AccountId != 0)
                            {
                                _HCUAccount.OwnerId = _Request.AccountId;
                            }
                            _HCUAccount.MobileNumber = MobileNumber;
                            if (!string.IsNullOrEmpty(_Request.FirstName))
                            {
                                _HCUAccount.DisplayName = _Request.FirstName;
                            }
                            else
                            {
                                _HCUAccount.DisplayName = _Request.MobileNumber;
                            }

                            var referralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                            _HCUAccount.ReferralCode = referralCode;
                            _HCUAccount.ReferralUrl = _AppConfig.WebsiteUrl + $"?refby={referralCode}";
                            if (!string.IsNullOrEmpty(_Request.ReferralCode))
                            {
                                long? OwnerId = _HCoreContext.HCUAccount.Where(x => x.ReferralCode == _Request.ReferralCode).Select(x => x.Id).FirstOrDefault();
                                if (OwnerId > 0)
                                {
                                    _HCUAccount.OwnerId = OwnerId;
                                }
                                else
                                {
                                    _HCUAccount.OwnerId = 3;
                                }
                            }
                            _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(AccessPin);
                            _HCUAccount.ReferenceNumber = HCoreEncrypt.EncryptHash(ReferenceNumber);
                            _HCUAccount.Name = _Request.FirstName + " " + _Request.LastName;
                            if (!string.IsNullOrEmpty(_Request.MobileNumber))
                            {
                                _HCUAccount.MobileNumber = _Request.MobileNumber;
                                _HCUAccount.ContactNumber = _Request.MobileNumber;
                            }
                            if (!string.IsNullOrEmpty(_Request.EmailAddress))
                            {
                                _HCUAccount.EmailAddress = _Request.EmailAddress;
                                _HCUAccount.SecondaryEmailAddress = _Request.EmailAddress;
                            }
                            if (!string.IsNullOrEmpty(_Request.FirstName))
                            {
                                _HCUAccount.FirstName = _Request.FirstName;
                            }
                            if (!string.IsNullOrEmpty(_Request.LastName))
                            {
                                _HCUAccount.LastName = _Request.LastName;
                            }
                            //if (_Request.AddressLatitude != 0)
                            //{
                            //    _HCUAccount.Latitude = _Request.AddressLatitude;
                            //}
                            //if (_Request.AddressLongitude != 0)
                            //{
                            //    _HCUAccount.Longitude = _Request.AddressLongitude;
                            //}
                            if (GenderId != 0)
                            {
                                _HCUAccount.GenderId = GenderId;
                            }
                            if (_Request.DateOfBirth != null)
                            {
                                _HCUAccount.DateOfBirth = _Request.DateOfBirth;
                            }
                            _HCUAccount.CountryId = _Request.CountryId;
                            _HCUAccount.EmailVerificationStatus = 0;
                            _HCUAccount.EmailVerificationStatusDate = CreateDate;
                            _HCUAccount.NumberVerificationStatus = 0;
                            _HCUAccount.NumberVerificationStatusDate = CreateDate;
                            _Random = new Random();
                            string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                            _HCUAccount.AccountCode = AccountCode;
                            _HCUAccount.ReferralCode = _Request.MobileNumber;
                            if (OwnerDetails.AccountTypeId == UserAccountType.Merchant)
                            {
                                _HCUAccount.RegistrationSourceId = RegistrationSource.MerchantImport;
                            }
                            else if (OwnerDetails.AccountTypeId == UserAccountType.Partner)
                            {
                                _HCUAccount.RegistrationSourceId = RegistrationSource.PartnerImport;
                            }
                            else if (OwnerDetails.AccountTypeId == UserAccountType.Acquirer)
                            {
                                _HCUAccount.RegistrationSourceId = RegistrationSource.AcquirerImport;
                            }
                            else
                            {
                                _HCUAccount.RegistrationSourceId = RegistrationSource.System;
                            }
                            //_HCUAccount.RegistrationSourceId = 12;
                            //if (_Request.UserReference.AppVersionId != 0)
                            //{
                            //    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                            //}
                            _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                            //else
                            //{
                            //    if (_Request.UserReference.AccountId != 0)
                            //    {
                            //        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                            //    }
                            //}
                            //_HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                            _HCUAccount.CreateDate = CreateDate;
                            _HCUAccount.StatusId = HelperStatus.Default.Active;
                            _HCUAccount.User = _HCUAccountAuth;
                            _HCoreContext.HCUAccount.Add(_HCUAccount);
                            _HCoreContext.SaveChanges();
                            AccountId = _HCUAccount.Id;
                            long? IconStorageId = null;
                            if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                            {
                                IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                            }
                            if (IconStorageId != null)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var UserAccDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _HCUAccount.Id).FirstOrDefault();
                                    if (UserAccDetails != null)
                                    {
                                        if (IconStorageId != null)
                                        {
                                            UserAccDetails.IconStorageId = IconStorageId;
                                        }
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                            #endregion
                            if ((HostEnvironment == HostEnvironmentType.Live && _Request.AccountId == 139618) || (HostEnvironment == HostEnvironmentType.Test && _Request.AccountId == 96689))
                            {
                                if (AllowSms != "0")
                                {
                                    if (!string.IsNullOrEmpty(Message))
                                    {
                                        Message = Message.Replace("{{AccessPin}}", AccessPin);
                                        Message = Message.Replace("{{Balance}}", "0");

                                        if (!string.IsNullOrEmpty(Message)
                                   && Message != "0"
                                   && !string.IsNullOrEmpty(InfoBipUrl)
                                   && Message != "0"
                                   && !string.IsNullOrEmpty(InfoBipKey)
                                   && InfoBipKey != "0"
                                   && !string.IsNullOrEmpty(InfoBipSenderId)
                                   && InfoBipSenderId != "0")
                                        {
                                            HCoreHelper.SendSMS(SmsType.Transaction, "234", MobileNumber, Message, AccountId, _HCUAccount.Guid, null, InfoBipSenderId, InfoBipKey, InfoBipUrl);
                                        }
                                    }
                                    else
                                    {
                                        HCoreHelper.SendSMS(SmsType.Transaction, "234", MobileNumber, Message, AccountId, _HCUAccount.Guid);
                                    }
                                }
                            }
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1310, TUCCoreResource.CA1310M);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1311, TUCCoreResource.CA1311M);
                            //var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                            //&& x.User.Username == CustomerAppId
                            //&& x.StatusId == HelperStatus.Default.Active)
                            //    .Select(x => new
                            //    {
                            //        AccountId = x.Id,
                            //        AccessPin = x.AccessPin,
                            //    })
                            //  .FirstOrDefault();
                            //if (CustomerDetails != null)
                            //{
                            //    //string AccessPin = HCoreEncrypt.DecryptHash(_HCoreContext.HCUAccount.Where(a => a.AccountTypeId == UserAccountType.Appuser && a.User.Username == CustomerAppId).Select(a => a.AccessPin).FirstOrDefault());
                            //    string AccessPin = null;
                            //    if (!string.IsNullOrEmpty(CustomerDetails.AccessPin))
                            //    {
                            //        AccessPin = HCoreEncrypt.DecryptHash(CustomerDetails.AccessPin);
                            //    }
                            //    _HCoreContext.Dispose();
                            //    ProcessStatusId = 4;
                            //    ProcessMessage = "Customer already present";
                            //    AllowProcess = false;
                            //    if ((HostEnvironment == HostEnvironmentType.Live && _Request.OwnerId == 139618) || (HostEnvironment == HostEnvironmentType.Test && _Request.OwnerId == 96689))
                            //    {
                            //        if (AllowSms != "0")
                            //        {
                            //            if (!string.IsNullOrEmpty(Message)
                            //            && Message != "0"
                            //            && !string.IsNullOrEmpty(InfoBipUrl)
                            //            && Message != "0"
                            //            && !string.IsNullOrEmpty(InfoBipKey)
                            //            && InfoBipKey != "0"
                            //            && !string.IsNullOrEmpty(InfoBipSenderId)
                            //            && InfoBipSenderId != "0")
                            //            {
                            //                double Balance = 0;
                            //                Message = Message.Replace("{{AccessPin}}", AccessPin);
                            //                if (Message.Contains("{{Balance}}"))
                            //                {
                            //                    ManageCoreTransaction _ManageCoreTransaction = new ManageCoreTransaction();
                            //                    Balance = _ManageCoreTransaction.GetAppUserBalance(CustomerDetails.AccountId, true);
                            //                }
                            //                Message = Message.Replace("{{Balance}}", Balance.ToString());
                            //                HCoreHelper.SendSMS(SmsType.Transaction, "234", MobileNumber, Message, InfoBipSenderId, InfoBipKey, InfoBipUrl);
                            //            }
                            //        }
                            //    }
                            //}
                        }
                        #endregion
                    }
                    #endregion
                    //using (_HCoreContextOperations = new HCoreContextOperations())
                    //{
                    //    long FileId = 0;
                    //    var UpItem = _HCoreContextOperations.HCOUploadCustomer.Where(x => x.Id == _Request.Id).FirstOrDefault();
                    //    if (UpItem != null)
                    //    {
                    //        if (AccountId > 0)
                    //        {
                    //            UpItem.AccountId = AccountId;
                    //        }
                    //        UpItem.StatusId = ProcessStatusId;
                    //        UpItem.StatusMessage = ProcessMessage;
                    //        UpItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                    //        _HCoreContextOperations.SaveChanges();
                    //        if (UpItem.FileId != null)
                    //        {
                    //            FileId = (long)UpItem.FileId;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        _HCoreContextOperations.SaveChanges();
                    //    }
                    //    if (FileId > 0)
                    //    {
                    //        using (_HCoreContextOperations = new HCoreContextOperations())
                    //        {

                    //            var FileDetails = _HCoreContextOperations.HCOUploadFile.Where(x => x.Id == FileId).FirstOrDefault();
                    //            if (FileDetails != null)
                    //            {
                    //                FileDetails.Pending = _HCoreContextOperations.HCOUploadCustomer.Count(x => x.FileId == FileId && x.StatusId == 1);
                    //                FileDetails.Processing = _HCoreContextOperations.HCOUploadCustomer.Count(x => x.FileId == FileId && x.StatusId == 2);
                    //                FileDetails.Success = _HCoreContextOperations.HCOUploadCustomer.Count(x => x.FileId == FileId && x.StatusId == 3);
                    //                FileDetails.Error = _HCoreContextOperations.HCOUploadCustomer.Count(x => x.FileId == FileId && x.StatusId == 4);
                    //                FileDetails.Completed = FileDetails.Success + FileDetails.Error;
                    //                if (FileDetails.TotalRecord == FileDetails.Completed)
                    //                {
                    //                    FileDetails.StatusId = 3;
                    //                }
                    //                // 1 => Pending 
                    //                // 2 => Processing
                    //                // 3 => Success 
                    //                //FileDetails.StatusMessage = ProcessMessage;
                    //                FileDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                    //                _HCoreContextOperations.SaveChanges();
                    //            }
                    //            else
                    //            {
                    //                _HCoreContextOperations.SaveChanges();
                    //            }
                    //        }
                    //    }
                    //}


                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardCustomer", _Exception, _Request.UserReference);
            }
            #endregion
        }

    }
}
