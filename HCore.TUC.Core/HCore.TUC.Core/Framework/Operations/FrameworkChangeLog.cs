//==================================================================================
// FileName: FrameworkChangeLog.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to change log
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Data;
using HCore.Data.Models;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using System.Collections.Generic;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant;
using HCore.Data.Logging;
using HCore.Data.Logging.Models;

namespace HCore.TUC.Core.Framework.Operations
{
    public class FrameworkChangeLog
    {
        HCoreContext _HCoreContext;
        HCoreContextLogging _HCoreContextLogging;
        HCLChangeLog _HCLChangeLog;

        /// <summary>
        /// Description: Method defined to save change log
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveChangeLog(OCoreChangeLog.Save.Request _Request)
        {
            try
            {
                if (_Request.Title == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14151, TUCCoreResource.CAA14151M);
                }

                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    _HCLChangeLog = new HCLChangeLog();
                    _HCLChangeLog.Guid = HCoreHelper.GenerateGuid();
                    _HCLChangeLog.Title = _Request.Title;
                    _HCLChangeLog.LocationName = _Request.LocationName;
                    _HCLChangeLog.Log = _Request.Log;
                    _HCLChangeLog.Comment = _Request.Comment;
                    _HCLChangeLog.ReleaseDate = _Request.ReleaseDate;
                    _HCLChangeLog.VersionName = _Request.VersionName;
                    _HCLChangeLog.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCLChangeLog.CreatedById = _Request.UserReference.AccountId;
                    _HCLChangeLog.StatusId = _Request.StatusId;
                    _HCoreContextLogging.HCLChangeLog.Add(_HCLChangeLog);
                    _HCoreContextLogging.SaveChanges();

                    var _Response = new
                    {
                        ReferenceId = _HCLChangeLog.Id,
                        ReferenceKey = _HCLChangeLog.Guid,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA14152, TUCCoreResource.CAA14152M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveChangeLog", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Method defined to update change log
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateChangeLog(OCoreChangeLog.Update _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    var _HCLChangeLogDetails = _HCoreContextLogging.HCLChangeLog.Where(x => x.Guid == _Request.ReferenceKey && x.Id == _Request.ReferenceId).FirstOrDefault();
                    if (_HCLChangeLogDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Title) && _HCLChangeLogDetails.Title != _Request.Title)
                        {
                            _HCLChangeLogDetails.Title = _Request.Title;
                        }
                        if (!string.IsNullOrEmpty(_Request.LocationName) && _HCLChangeLogDetails.LocationName != _Request.LocationName)
                        {
                            _HCLChangeLogDetails.LocationName = _Request.LocationName;
                        }
                        if (!string.IsNullOrEmpty(_Request.Log) && _HCLChangeLogDetails.Log != _Request.Log)
                        {
                            _HCLChangeLogDetails.Log = _Request.Log;
                        }
                        if (!string.IsNullOrEmpty(_Request.Comment) && _HCLChangeLogDetails.Comment != _Request.Comment)
                        {
                            _HCLChangeLogDetails.Comment = _Request.Comment;
                        }
                        if (_Request.ReleaseDate != null && _HCLChangeLogDetails.ReleaseDate != _Request.ReleaseDate)
                        {
                            _HCLChangeLogDetails.ReleaseDate = _Request.ReleaseDate;
                        }
                        if (!string.IsNullOrEmpty(_Request.VersionName) && _HCLChangeLogDetails.VersionName != _Request.VersionName)
                        {
                            _HCLChangeLogDetails.VersionName = _Request.VersionName;
                        }
                        if (_Request.StatusId > 0)
                        {
                            _HCLChangeLogDetails.StatusId = _Request.StatusId;
                        }
                        _HCLChangeLogDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCLChangeLogDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContextLogging.SaveChanges();

                        var _Response = new
                        {
                            ReferenceId = _HCLChangeLogDetails.Id,
                            ReferenceKey = _HCLChangeLogDetails.Guid,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA14153, TUCCoreResource.CAA14153M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateChangeLog", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Method defined to delete change log
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteChangeLog(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    var _HCLChangeLogDetails = _HCoreContextLogging.HCLChangeLog
                        .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .FirstOrDefault();
                    if (_HCLChangeLogDetails != null)
                    {
                        _HCoreContextLogging.HCLChangeLog.Remove(_HCLChangeLogDetails);
                        _HCoreContextLogging.SaveChanges();
                        _HCoreContextLogging.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CAA14154, TUCCoreResource.CAA14154M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DeleteChangeLog", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }

        /// <summary>
        /// Description: Method defined to get change log details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetChangeLog(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    var _HCLChangeLogDetails = _HCoreContextLogging.HCLChangeLog.Where(x => (x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey))
                        .Select(x => new OCoreChangeLog.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            Title = x.Title,
                            LocationName = x.LocationName,
                            Log = x.Log,
                            Comment = x.Comment,
                            ReleaseDate = x.ReleaseDate,
                            VersionName = x.VersionName,

                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,

                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyById,

                            StatusId = x.StatusId,
                        })
                       .FirstOrDefault();

                    _HCoreContextLogging.Dispose();

                    if (_HCLChangeLogDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _HCLChangeLogDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetChangeLog", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Method defined to get list of change logs
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetChangeLogs(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContextLogging.HCLChangeLog
                            .Select(x => new OCoreChangeLog.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                Title = x.Title,
                                LocationName = x.LocationName,
                                Log = x.Log,
                                Comment = x.Comment,
                                ReleaseDate = x.ReleaseDate,
                                VersionName = x.VersionName,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,

                                ModifyDate = x.ModifyDate,
                                ModifyById = x.ModifyById,

                                StatusId = x.StatusId,
                            })
                            .Where(_Request.SearchCondition)
                            .Count();

                    }

                    List<OCoreChangeLog.List> Data = _HCoreContextLogging.HCLChangeLog
                                                .Select(x => new OCoreChangeLog.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Title = x.Title,
                                                    LocationName = x.LocationName,
                                                    Log = x.Log,
                                                    Comment = x.Comment,
                                                    ReleaseDate = x.ReleaseDate,
                                                    VersionName = x.VersionName,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,

                                                    StatusId = x.StatusId,
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContextLogging.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetChangeLogs", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
    }
}
