//==================================================================================
// FileName: FrameworkConfiguration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to account configuration
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operation.Object;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;

namespace HCore.Operation.Framework
{
    public class FrameworkCoreConfiguration
    {
        HCoreContext _HCoreContext;
        HCCoreConfiguration _HCCoreConfiguration;
        HCCoreConfigurationHistory _HCCoreConfigurationHistory;
        HCUAccountConfiguration _HCUAccountConfiguration;
        HCUAccountConfigurationHistory _HCUAccountConfigurationHistory;
        /// <summary>
        /// Description: Method defined to save configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveConfiguration(OCoreConfiguration.Save.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1459, TUCCoreResource.CA1459M);
                }
                if (string.IsNullOrEmpty(_Request.SystemName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1459, TUCCoreResource.CA1459M);
                }
                if (string.IsNullOrEmpty(_Request.Value))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1460, TUCCoreResource.CA1460M);
                }
                if (string.IsNullOrEmpty(_Request.DataTypeCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1463, TUCCoreResource.CA1463M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                
                int StatusId = StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }
                int? DataTypeId = 0;
                if (!string.IsNullOrEmpty(_Request.DataTypeCode))
                {
                    DataTypeId = HCoreHelper.GetSystemHelperId(_Request.DataTypeCode, _Request.UserReference);
                    if (DataTypeId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1464, TUCCoreResource.CA1464M);
                    }
                }

                int? AccountTypeId = 0;
                if (!string.IsNullOrEmpty(_Request.AccountTypeCode))
                {
                    AccountTypeId = HCoreHelper.GetSystemHelperId(_Request.AccountTypeCode, _Request.UserReference);
                    if (AccountTypeId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1466, TUCCoreResource.CA1466M);
                    }
                }

                int? ValueHelperTypeId = 0;
                if (!string.IsNullOrEmpty(_Request.ValueHelperCode))
                {
                    ValueHelperTypeId = HCoreHelper.GetSystemHelperId(_Request.ValueHelperCode, _Request.UserReference);
                    if (ValueHelperTypeId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1462, TUCCoreResource.CA1462M);
                    }
                }
                using (_HCoreContext = new HCoreContext())
                {
                   
                    bool _IsDetailsExist = _HCoreContext.HCCoreConfiguration.Any(x => x.SystemName == _Request.Name || x.Name == _Request.Name);
                    if (_IsDetailsExist)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1472, TUCCoreResource.CA1472M);
                    }

                    _HCCoreConfiguration = new HCCoreConfiguration();
                    _HCCoreConfiguration.Guid = HCoreHelper.GenerateGuid();
                    _HCCoreConfiguration.Name = _Request.Name;
                    _HCCoreConfiguration.SystemName = _Request.SystemName;
                    _HCCoreConfiguration.Description = _Request.Description;
                    _HCCoreConfiguration.Value = _Request.Value;
                    if (ValueHelperTypeId > 0)
                    {
                        _HCCoreConfiguration.ValueHelperId = ValueHelperTypeId;
                    }
                    if (DataTypeId > 0)
                    {
                        _HCCoreConfiguration.DataTypeId = (int)DataTypeId;
                    }
                    if (AccountTypeId > 0)
                    {
                        _HCCoreConfiguration.AccountTypeId = AccountTypeId;
                    }
                    _HCCoreConfiguration.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCCoreConfiguration.CreatedById = _Request.UserReference.AccountId;
                    _HCCoreConfiguration.StatusId = StatusId;
                    _HCCoreConfigurationHistory = new HCCoreConfigurationHistory();
                    _HCCoreConfigurationHistory.Guid = HCoreHelper.GenerateGuid();
                    _HCCoreConfigurationHistory.Configuration = _HCCoreConfiguration;
                    _HCCoreConfigurationHistory.Value = _Request.Value;
                    if (ValueHelperTypeId > 0)
                    {
                        _HCCoreConfigurationHistory.ValueHelperId = ValueHelperTypeId;
                    }
                    _HCCoreConfigurationHistory.StartDate = HCoreHelper.GetGMTDateTime();
                    _HCCoreConfigurationHistory.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCCoreConfigurationHistory.CreatedById = _Request.UserReference.AccountId;
                    _HCCoreConfigurationHistory.StatusId = HelperStatus.Default.Active;
                    _HCoreContext.HCCoreConfiguration.Add(_HCCoreConfiguration);
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                    var _Response = new
                    {
                        ReferenceId = _HCCoreConfiguration.Id,
                        ReferenceKey = _HCCoreConfiguration.Guid
                    };

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1467, TUCCoreResource.CA1467M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Save-CoreConfiguration", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Method defined to update configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateConfiguration(OCoreConfiguration.Update _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1470, TUCCoreResource.CA1470M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1471, TUCCoreResource.CA1471M);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                }
                int? DataTypeId = 0;
                if (!string.IsNullOrEmpty(_Request.DataTypeCode))
                {
                    DataTypeId = HCoreHelper.GetSystemHelperId(_Request.DataTypeCode, _Request.UserReference);
                    if (DataTypeId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1464, TUCCoreResource.CA1464M);
                    }
                }

                int AccountTypeId = 0;
                if (!string.IsNullOrEmpty(_Request.AccountTypeCode))
                {
                    AccountTypeId = HCoreHelper.GetStatusId(_Request.AccountTypeCode, _Request.UserReference);
                    if (AccountTypeId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1466, TUCCoreResource.CA1466M);
                    }
                }

                int ValueHelperTypeId = 0;
                if (!string.IsNullOrEmpty(_Request.ValueHelperCode))
                {
                    ValueHelperTypeId = HCoreHelper.GetStatusId(_Request.ValueHelperCode, _Request.UserReference);
                    if (ValueHelperTypeId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1462, TUCCoreResource.CA1462M);
                    }
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCCoreConfiguration.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && _Details.Name != _Request.Name)
                        {
                            bool _IsExist = _HCoreContext.HCCoreConfiguration.Any(x => x.Name == _Request.Name && x.Id != _Details.Id);
                            if (_IsExist)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1472, TUCCoreResource.CA1472M);
                            }

                            _Details.Name = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.SystemName) && _Details.SystemName != _Request.SystemName)
                        {
                            bool _IsExist = _HCoreContext.HCCoreConfiguration.Any(x => x.SystemName == _Request.SystemName && x.Id != _Details.Id);
                            if (_IsExist)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1472, TUCCoreResource.CA1472M);
                            }

                            _Details.SystemName = _Request.SystemName;
                        }

                        if (!string.IsNullOrEmpty(_Request.Value) && _Details.Value != _Request.Value)
                        {
                            _Details.Value = _Request.Value;
                        }
                        if (!string.IsNullOrEmpty(_Request.Description) && _Details.Description != _Request.Description)
                        {
                            _Details.Description = _Request.Description;
                        }
                        if (ValueHelperTypeId > 0)
                        {
                            _Details.ValueHelperId = ValueHelperTypeId;
                        }

                        if (DataTypeId > 0)
                        {
                            _Details.DataTypeId = (int)DataTypeId;
                        }
                        if (AccountTypeId > 0)
                        {
                            _Details.AccountTypeId = AccountTypeId;
                        }
                        if (StatusId > 0)
                        {
                            _Details.StatusId = StatusId;
                        }
                        _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _Details.ModifyById = _Request.UserReference.AccountId;
                        if ((!string.IsNullOrEmpty(_Request.Value) && _Details.Value != _Request.Value) || (ValueHelperTypeId > 0 && ValueHelperTypeId != _Details.ValueHelperId))
                        {
                            var ActiveConfigurationHistory = _HCoreContext.HCCoreConfigurationHistory.Where(x => x.ConfigurationId == _Details.Id && x.StatusId == HelperStatus.Default.Active).ToList();
                            if (ActiveConfigurationHistory.Count > 0)
                            {
                                foreach (var ActiveConfigurationHistoryItem in ActiveConfigurationHistory)
                                {
                                    ActiveConfigurationHistoryItem.EndDate = HCoreHelper.GetGMTDateTime();
                                    ActiveConfigurationHistoryItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    ActiveConfigurationHistoryItem.ModifyById = _Request.UserReference.AccountId;
                                    ActiveConfigurationHistoryItem.StatusId = HelperStatus.Default.Blocked;
                                }
                            }
                            _HCCoreConfigurationHistory = new HCCoreConfigurationHistory();
                            _HCCoreConfigurationHistory.Guid = HCoreHelper.GenerateGuid();
                            _HCCoreConfigurationHistory.ConfigurationId = _Details.Id;
                            _HCCoreConfigurationHistory.Value = _Request.Value;
                            if (ValueHelperTypeId > 0)
                            {
                                _HCCoreConfigurationHistory.ValueHelperId = ValueHelperTypeId;
                            }
                            _HCCoreConfigurationHistory.StartDate = HCoreHelper.GetGMTDateTime();
                            _HCCoreConfigurationHistory.Comment = _Request.Comment;
                            _HCCoreConfigurationHistory.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCCoreConfigurationHistory.CreatedById = _Request.UserReference.AccountId;
                            _HCCoreConfigurationHistory.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.HCCoreConfiguration.Add(_HCCoreConfiguration);
                        }
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1468, TUCCoreResource.CA1468M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }

            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Update-CoreConfiguration", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Method defined to delete configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteConfiguration(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1470, TUCCoreResource.CA1470M);
                }

                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1471, TUCCoreResource.CA1471M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCCoreConfiguration
                        .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .FirstOrDefault();
                    if (_Details != null)
                    {
                        _HCoreContext.HCCoreConfiguration.Remove(_Details);
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA1469, TUCCoreResource.CA1469M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Delete-CoreConfiguration", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Method defined to get details of configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetConfiguration(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1470, TUCCoreResource.CA1470M);
                }

                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1471, TUCCoreResource.CA1471M);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCCoreConfiguration.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .Select(x => new OCoreConfiguration.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            Name = x.Name,
                            SystemName = x.SystemName,
                            Description = x.Description,

                            Value = x.Value,
                            ValueHelperId = (int)x.ValueHelper.Id,
                            ValueHelperCode = x.ValueHelper.Guid,
                            ValueHelperName = x.ValueHelper.Name,

                            DataTypeId = x.DataType.Id,
                            DataTypeCode = x.DataType.Guid,

                            AccountTypeId = x.AccountType.Id,
                            AccountTypeCode = x.AccountType.Guid,
                            AccountTypeName = x.AccountType.Name,

                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedBy.Id,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyById,
                            ModifyByKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                        }).FirstOrDefault();
                    if (_Details != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Get-CoreConfiguration", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Method defined to get list of configurations
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetConfiguration(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCCoreConfiguration
                            .Select(x => new OCoreConfiguration.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                Name = x.Name,
                                SystemName = x.SystemName,
                                Description = x.Description,
                                DataTypeId = x.DataTypeId,
                                DataTypeCode = x.DataType.SystemName,

                                Value = x.Value,
                                ValueHelperId = x.ValueHelper.Id,
                                ValueHelperCode = x.ValueHelper.SystemName,
                                ValueHelperName = x.ValueHelper.Name,

                                AccountTypeId = x.AccountType.Id,
                                AccountTypeCode = x.AccountType.SystemName,
                                AccountTypeName = x.AccountType.Name,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedBy.Id,
                                CreatedByKey = x.CreatedBy.Guid,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                            })
                            .Where(_Request.SearchCondition)
                            .Count();

                    }
                    List<OCoreConfiguration.List> Data = _HCoreContext.HCCoreConfiguration
                                                .Select(x => new OCoreConfiguration.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Name = x.Name,
                                                    SystemName = x.SystemName,
                                                    Description = x.Description,
                                                    DataTypeId = x.DataTypeId,
                                                    DataTypeCode = x.DataType.SystemName,

                                                    Value = x.Value,
                                                    ValueHelperId = (int)x.ValueHelper.Id,
                                                    ValueHelperCode = x.ValueHelper.SystemName,
                                                    ValueHelperName = x.ValueHelper.Name,

                                                    AccountTypeId = x.AccountType.Id,
                                                    AccountTypeCode = x.AccountType.SystemName,
                                                    AccountTypeName = x.AccountType.Name,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedBy.Id,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);    
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Get-CoreConfigurations", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Method defined to get list of history configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetConfigurationHistory(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCCoreConfigurationHistory
                            .Select(x => new OCoreConfigurationHistory.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                Value = x.Value,
                                Comment = x.Comment,


                                StartDate = x.StartDate,
                                EndDate = x.EndDate,

                                CreateDate = x.CreateDate,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                            })
                            .Where(_Request.SearchCondition)
                            .Count();

                    }
                    List<OCoreConfigurationHistory.List> Data = _HCoreContext.HCCoreConfigurationHistory
                                                .Select(x => new OCoreConfigurationHistory.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Value = x.Value,
                                                    Comment = x.Comment,


                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,

                                                    CreateDate = x.CreateDate,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Get-Configurationistory", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }

        /// <summary>
        /// Description: Method defined to save account configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveAccountConfiguration(OAccountConfiguration.Save.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.Value))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1460, TUCCoreResource.CA1460M);
                }

                if (string.IsNullOrEmpty(_Request.ConfigurationKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1477, TUCCoreResource.CA1477M);
                }

                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1478, TUCCoreResource.CA1478M);
                }

                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                int? ValueHelperId = 0;
                if (!string.IsNullOrEmpty(_Request.ValueHelperCode))
                {
                    ValueHelperId = HCoreHelper.GetSystemHelperId(_Request.ValueHelperCode, _Request.UserReference);
                    if (ValueHelperId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1466, TUCCoreResource.CA1466M);
                    }
                }
                int StatusId = HelperStatus.Default.Active;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var ConfigurationDetails = _HCoreContext.HCCoreConfiguration.Any(x => x.Id == _Request.ConfigurationId && x.Guid == _Request.ConfigurationKey);
                    if (!ConfigurationDetails)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1479, TUCCoreResource.CA1479M);
                    }

                    var AccountDetails = _HCoreContext.HCUAccount.Any(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey);
                    if (!AccountDetails)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1480, TUCCoreResource.CA1480M);
                    }



                    bool IsDetailsExist = _HCoreContext.HCUAccountConfiguration.Any(x => x.AccountId == _Request.AccountId && x.ConfigurationId == _Request.ConfigurationId);
                    if (IsDetailsExist)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA14910, TUCCoreResource.CA14910M);
                    }

                    _HCUAccountConfiguration = new HCUAccountConfiguration();
                    _HCUAccountConfiguration.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountConfiguration.Value = _Request.Value;
                    _HCUAccountConfiguration.ValueHelperId = ValueHelperId;
                    _HCUAccountConfiguration.ConfigurationId = _Request.ConfigurationId;
                    _HCUAccountConfiguration.AccountId = _Request.AccountId;
                    _HCUAccountConfiguration.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountConfiguration.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountConfiguration.StatusId = StatusId;


                    _HCUAccountConfigurationHistory = new HCUAccountConfigurationHistory();
                    _HCUAccountConfigurationHistory.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountConfigurationHistory.Value = _Request.Value;
                    if (ValueHelperId > 0)
                    {
                        _HCUAccountConfigurationHistory.ValueHelperId = ValueHelperId;
                    }
                    _HCUAccountConfigurationHistory.StartDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountConfigurationHistory.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountConfigurationHistory.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountConfigurationHistory.StatusId = HelperStatus.Default.Active;
                    _HCUAccountConfigurationHistory.AccountConfiguration = _HCUAccountConfiguration;
                    _HCoreContext.HCUAccountConfigurationHistory.Add(_HCUAccountConfigurationHistory);
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();

                    var _Response = new
                    {
                        ReferenceId = _HCUAccountConfiguration.Id,
                        ReferenceKey = _HCUAccountConfiguration.Guid
                    };

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1474, TUCCoreResource.CA1474M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Save-AccountConfiguration", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Decription: Method defined to update account configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateAccountConfiguration(OAccountConfiguration.Update _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1470, TUCCoreResource.CA1470M);
                }

                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1471, TUCCoreResource.CA1471M);
                }

                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                }
                int? ValueHelperId = 0;
                if (!string.IsNullOrEmpty(_Request.ValueHelperCode))
                {
                    ValueHelperId = HCoreHelper.GetSystemHelperId(_Request.ValueHelperCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCUAccountConfiguration.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Value) && _Details.Value != _Request.Value)
                        {
                            _Details.Value = _Request.Value;
                        }
                        if (ValueHelperId > 0)
                        {
                            _Details.ValueHelperId = ValueHelperId;
                        }
                        if (StatusId > 0)
                        {
                            _Details.StatusId = StatusId;
                        }
                        _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _Details.ModifyById = _Request.UserReference.AccountId;
                        if ((!string.IsNullOrEmpty(_Request.Value) && _Details.Value != _Request.Value) || (ValueHelperId > 0 && _Details.ValueHelperId != ValueHelperId))
                        {
                            var ActiveConfigurationHistory = _HCoreContext.HCUAccountConfigurationHistory.Where(x => x.AccountConfigurationId == _Details.Id && x.StatusId == HelperStatus.Default.Active).ToList();
                            if (ActiveConfigurationHistory.Count > 0)
                            {
                                foreach (var ActiveConfigurationHistoryItem in ActiveConfigurationHistory)
                                {
                                    ActiveConfigurationHistoryItem.EndDate = HCoreHelper.GetGMTDateTime();
                                    ActiveConfigurationHistoryItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    ActiveConfigurationHistoryItem.ModifyById = _Request.UserReference.AccountId;
                                    ActiveConfigurationHistoryItem.StatusId = HelperStatus.Default.Blocked;
                                }
                            }
                            _HCUAccountConfigurationHistory = new HCUAccountConfigurationHistory();
                            _HCUAccountConfigurationHistory.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountConfigurationHistory.AccountConfigurationId = _Details.Id;
                            _HCUAccountConfigurationHistory.Value = _Request.Value;
                            if (ValueHelperId > 0)
                            {
                                _HCUAccountConfigurationHistory.ValueHelperId = ValueHelperId;
                            }
                            _HCUAccountConfigurationHistory.StartDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountConfigurationHistory.Comment = _Request.Comment;
                            _HCUAccountConfigurationHistory.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountConfigurationHistory.CreatedById = _Request.UserReference.AccountId;
                            _HCUAccountConfigurationHistory.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.HCUAccountConfigurationHistory.Add(_HCUAccountConfigurationHistory);
                        }
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1475, TUCCoreResource.CA1475M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }

            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Update-AccountConfiguration", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Method defined to delete account configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteAccountConfiguration(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1470, TUCCoreResource.CA1470M);
                }

                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1471, TUCCoreResource.CA1471M);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCUAccountConfiguration
                        .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .FirstOrDefault();
                    if (_Details != null)
                    {
                        var ConfigurationHistory = _HCoreContext.HCUAccountConfigurationHistory.Where(x => x.AccountConfigurationId == _Details.Id).ToList();
                        if (ConfigurationHistory.Count > 0)
                        {
                            _HCoreContext.HCUAccountConfigurationHistory.RemoveRange(ConfigurationHistory);
                            _HCoreContext.SaveChanges();
                            var Config = _HCoreContext.HCUAccountConfiguration
                                    .Where(x => x.Id == _Details.Id)
                                    .FirstOrDefault();
                            _HCoreContext.HCUAccountConfiguration.Remove(Config);
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA1476, TUCCoreResource.CA1476M);
                        }
                        else
                        {
                            _HCoreContext.HCUAccountConfiguration.Remove(_Details);
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA1476, TUCCoreResource.CA1476M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Delete-AccountConfiguration", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Method defined to get details of account configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetAccountConfiguration(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1470, TUCCoreResource.CA1470M);
                }

                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1471, TUCCoreResource.CA1471M);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCUAccountConfiguration.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .Select(x => new OAccountConfiguration.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            AccountId = x.AccountId,
                            AccountKey = x.Account.Guid,
                            AccountDisplayName = x.Account.DisplayName,

                            AccountTypeId = x.Account.AccountTypeId,
                            AccountTypeCode = x.Account.AccountType.SystemName,
                            AccountTypeName = x.Account.AccountType.Name,


                            ConfigurationId = x.ConfigurationId,
                            ConfigurationKey = x.Configuration.Guid,
                            ConfigurationName = x.Configuration.Name,
                            ConfigurationSystemName = x.Configuration.SystemName,

                            DefaultValue = x.Configuration.Value,
                            DefaultValueHelperId = x.Configuration.ValueHelperId,
                            DefaultValueHelperCode = x.Configuration.ValueHelper.SystemName,
                            DefaultValueHelperName = x.Configuration.ValueHelper.Name,

                            Value = x.Value,
                            ValueHelperId = x.ValueHelperId,
                            ValueHelperCode = x.ValueHelper.SystemName,
                            ValueHelperName = x.ValueHelper.Name,

                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyById,
                            ModifyByKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                        }).FirstOrDefault();


                    if (_Details != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Get-AccountConfiguration", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Method defined to get list of account configurations
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetAccountConfiguration(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCUAccountConfiguration
                            .Select(x => new OAccountConfiguration.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                AccountId = x.AccountId,
                                AccountKey = x.Account.Guid,
                                AccountDisplayName = x.Account.DisplayName,

                                AccountTypeId = x.Account.AccountTypeId,
                                AccountTypeCode = x.Account.AccountType.SystemName,
                                AccountTypeName = x.Account.AccountType.Name,


                                ConfigurationId = x.ConfigurationId,
                                ConfigurationKey = x.Configuration.Guid,
                                ConfigurationName = x.Configuration.Name,
                                ConfigurationSystemName = x.Configuration.SystemName,

                                DefaultValue = x.Configuration.Value,
                                DefaultValueHelperId = x.Configuration.ValueHelperId,
                                DefaultValueHelperCode = x.Configuration.ValueHelper.SystemName,
                                DefaultValueHelperName = x.Configuration.ValueHelper.Name,

                                Value = x.Value,
                                ValueHelperId = x.ValueHelperId,
                                ValueHelperCode = x.ValueHelper.SystemName,
                                ValueHelperName = x.ValueHelper.Name,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByKey = x.CreatedBy.Guid,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                            })
                            .Where(_Request.SearchCondition)
                            .Count();

                    }
                    List<OAccountConfiguration.List> Data = _HCoreContext.HCUAccountConfiguration
                                                .Select(x => new OAccountConfiguration.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,

                                                    AccountTypeId = x.Account.AccountTypeId,
                                                    AccountTypeCode = x.Account.AccountType.SystemName,
                                                    AccountTypeName = x.Account.AccountType.Name,


                                                    ConfigurationId = x.ConfigurationId,
                                                    ConfigurationKey = x.Configuration.Guid,
                                                    ConfigurationName = x.Configuration.Name,
                                                    ConfigurationSystemName = x.Configuration.SystemName,

                                                    DefaultValue = x.Configuration.Value,
                                                    DefaultValueHelperId = x.Configuration.ValueHelperId,
                                                    DefaultValueHelperCode = x.Configuration.ValueHelper.SystemName,
                                                    DefaultValueHelperName = x.Configuration.ValueHelper.Name,

                                                    Value = x.Value,
                                                    ValueHelperId = x.ValueHelperId,
                                                    ValueHelperCode = x.ValueHelper.SystemName,
                                                    ValueHelperName = x.ValueHelper.Name,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Get-AccountConfigurations", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Method defined to get list of history account configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetAccountConfigurationHistory(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCUAccountConfigurationHistory
                            .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                            .Select(x => new OAccountConfigurationHistory.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                Value = x.Value,
                                ValueHelperId = x.ValueHelperId,
                                ValueHelperCode = x.ValueHelper.SystemName,
                                ValueHelperName = x.ValueHelper.Name,

                                Comment = x.Comment,

                                StartDate = x.StartDate,
                                EndDate = x.EndDate,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByKey = x.CreatedBy.Guid,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyById = x.ModifyById,
                                ModifyByKey = x.ModifyBy.Guid,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name
                            })
                            .Where(_Request.SearchCondition)
                            .Count();

                    }
                    List<OAccountConfigurationHistory.List> Data = _HCoreContext.HCUAccountConfigurationHistory
                                                .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OAccountConfigurationHistory.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Value = x.Value,
                                                    ValueHelperId = x.ValueHelperId,
                                                    ValueHelperCode = x.ValueHelper.SystemName,
                                                    ValueHelperName = x.ValueHelper.Name,

                                                    Comment = x.Comment,

                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Get-AccountConfigurationHistory", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }

    }

    internal class FrameworkAccountConfiguration
    {
        

      
    }

}