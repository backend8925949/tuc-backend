//==================================================================================
// FileName: FrameworkDealCategory.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to deal category
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Core.Framework.Operations
{
    internal class FrameworkDealCategory
    {
        HCoreContext _HCoreContext;
        MDCategory _MDCategory;
        TUCCategory _TUCCategory;

        /// <summary>
        /// Description: Method defined to save deal category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveDealCategory(ODealCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.RootCategoryId <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.MCAREF, TUCCoreResource.MCAREFM);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                int? StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var SubCategoories = _HCoreContext.TUCCategory.Where(x => x.ParentCategoryId == _Request.RootCategoryId && x.ParentCategory.Guid == _Request.RootCategoryKey && x.ParentCategoryId != null).ToList();
                    bool RootCategoryExists = _HCoreContext.TUCCategory.Any(x => x.Id == _Request.RootCategoryId && x.Guid == _Request.RootCategoryKey);
                    if (RootCategoryExists)
                    {
                        bool Exists = _HCoreContext.MDCategory.Any(x => x.RootCategoryId == _Request.RootCategoryId && x.RootCategory.Guid == _Request.RootCategoryKey);
                        if (Exists)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAEXISTS, TUCCoreResource.CAEXISTSM);
                        }
                        _MDCategory = new MDCategory();
                        _MDCategory.Guid = HCoreHelper.GenerateGuid();
                        _MDCategory.RootCategoryId = _Request.RootCategoryId;
                        _MDCategory.Commission = _Request.Commission;
                        _MDCategory.CreateDate = HCoreHelper.GetGMTDateTime();
                        _MDCategory.CreatedById = _Request.UserReference.AccountId;
                        _MDCategory.StatusId = (int)StatusId;
                        _HCoreContext.MDCategory.Add(_MDCategory);

                        if(SubCategoories.Count > 0 || SubCategoories != null)
                        {
                            foreach (var Category in SubCategoories)
                            {
                                _MDCategory = new MDCategory();
                                _MDCategory.Guid = HCoreHelper.GenerateGuid();
                                _MDCategory.RootCategoryId = Category.Id;
                                _MDCategory.Commission = _Request.Commission;
                                _MDCategory.CreateDate = HCoreHelper.GetGMTDateTime();
                                _MDCategory.CreatedById = _Request.UserReference.AccountId;
                                _MDCategory.StatusId = (int)StatusId;
                                _HCoreContext.MDCategory.Add(_MDCategory);
                            }
                        }

                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();

                        var Response = new
                        {
                            ReferenceId = _MDCategory.Id,
                            ReferenceKey = _MDCategory.Guid
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, TUCCoreResource.CASAVE, TUCCoreResource.CASAVEM);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveDealCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to update deal category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateDealCategory(ODealCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var DealCategoryDetails = _HCoreContext.MDCategory.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (DealCategoryDetails != null)
                    {
                        if (_Request.Commission >= 0 && _Request.Commission != DealCategoryDetails.Commission)
                        {
                            DealCategoryDetails.Commission = _Request.Commission;
                        }
                        if (StatusId != 0 && DealCategoryDetails.StatusId != StatusId)
                        {
                            DealCategoryDetails.StatusId = StatusId;
                        }
                        DealCategoryDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        DealCategoryDetails.ModifyById = _Request.UserReference.AccountId;

                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            var rootcategory = _HCoreContext.MDCategory.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).Select(s => s.RootCategory).FirstOrDefault();
                            rootcategory.IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, rootcategory.IconStorageId, _Request.UserReference);
                        }

                        _HCoreContext.SaveChanges();

                        var _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.DCA14167, TUCCoreResource.DCA14167M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateDealCategory", _Exception, _Request.UserReference);
                #endregion
            }

        }

        /// <summary>
        /// Description: Method defined to delete deal category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteDealCategory(ODealCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    MDCategory Details = _HCoreContext.MDCategory.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        bool SubCategoryCheck = _HCoreContext.MDCategory.Any(x => x.RootCategory.ParentCategoryId == Details.RootCategoryId);
                        if (SubCategoryCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAT004, TUCCoreResource.CAT004M);
                        }
                        _HCoreContext.MDCategory.Remove(Details);
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.DCA14168, TUCCoreResource.DCA14168M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteDealCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get details of deal category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetDealCategory(ODealCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    ODealCategory.Details _Details = (from x in _HCoreContext.MDCategory
                                                      where x.Guid == _Request.ReferenceKey
                                                      && x.Id == _Request.ReferenceId
                                                      select new ODealCategory.Details
                                                      {

                                                          ReferenceId = x.Id,
                                                          ReferenceKey = x.Guid,

                                                          RootCategoryId = x.RootCategoryId,
                                                          RootCategoryKey = x.RootCategory.Guid,
                                                          RootCategoryName = x.RootCategory.Name,
                                                          RootCategoryDescription = x.RootCategory.Description,
                                                          RootCategoryIconUrl = x.RootCategory.IconStorage.Path,
                                                          RootCategoryPosterUrl = x.RootCategory.PosterStorage.Path,

                                                          ParentCateforyId = x.RootCategory.ParentCategoryId,
                                                          ParentCategoryKey = x.RootCategory.ParentCategory.Guid,
                                                          ParentCategoryName = x.RootCategory.ParentCategory.Name,

                                                          Name = x.RootCategory.Name,
                                                          Description = x.RootCategory.Description,
                                                          Commission = x.Commission,
                                                          IconUrl = x.RootCategory.IconStorage.Path,
                                                          PosterUrl = x.RootCategory.PosterStorage.Path,

                                                          CreateDate = x.CreateDate,
                                                          CreatedById = x.CreatedById,
                                                          CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                          ModifyDate = x.ModifyDate,
                                                          ModifyById = x.ModifyById,
                                                          ModiFyByDisplayName = x.ModifyBy.DisplayName,

                                                          StatusId = x.StatusId,
                                                          StatusCode = x.Status.SystemName,
                                                          StatusName = x.Status.Name,
                                                      })
                                              .FirstOrDefault();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    if (_Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Details.IconUrl))
                        {
                            _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                        }
                        else
                        {
                            _Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.PosterUrl))
                        {
                            _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                        }
                        else
                        {
                            _Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                        if (!string.IsNullOrEmpty(_Details.RootCategoryIconUrl))
                        {
                            _Details.RootCategoryIconUrl = _AppConfig.StorageUrl + _Details.RootCategoryIconUrl;
                        }
                        else
                        {
                            _Details.RootCategoryIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.RootCategoryPosterUrl))
                        {
                            _Details.RootCategoryPosterUrl = _AppConfig.StorageUrl + _Details.RootCategoryPosterUrl;
                        }
                        else
                        {
                            _Details.RootCategoryPosterUrl = _AppConfig.Default_Icon;
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetDealCategory", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method to get list of deal categories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetDealCategoryList(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.MDCategory
                                                 where x.RootCategory.ParentCategoryId == _Request.ReferenceId
                                                 select new ODealCategory.Details
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     RootCategoryId = x.RootCategoryId,
                                                     RootCategoryKey = x.RootCategory.Guid,
                                                     RootCategoryName = x.RootCategory.Name,
                                                     RootCategoryDescription = x.RootCategory.Description,
                                                     RootCategoryIconUrl = x.RootCategory.IconStorage.Path,
                                                     RootCategoryPosterUrl = x.RootCategory.PosterStorage.Path,

                                                     ParentCateforyId = x.RootCategory.ParentCategoryId,
                                                     ParentCategoryKey = x.RootCategory.ParentCategory.Guid,
                                                     ParentCategoryName = x.RootCategory.ParentCategory.Name,

                                                     Name = x.RootCategory.Name,
                                                     Description = x.RootCategory.Description,
                                                     Commission = x.Commission,
                                                     IconUrl = x.RootCategory.IconStorage.Path,
                                                     PosterUrl = x.RootCategory.PosterStorage.Path,

                                                     CreateDate = x.CreateDate,

                                                     ModifyDate = x.ModifyDate,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                 })
                                                                .Where(_Request.SearchCondition)
                                                        .Count();
                    }
                    #endregion
                    #region Get Data
                    List<ODealCategory.Details> Data = (from x in _HCoreContext.MDCategory
                                                        where x.RootCategory.ParentCategoryId == _Request.ReferenceId
                                                        select new ODealCategory.Details
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,

                                                            RootCategoryId = x.RootCategoryId,
                                                            RootCategoryKey = x.RootCategory.Guid,
                                                            RootCategoryName = x.RootCategory.Name,
                                                            RootCategoryDescription = x.RootCategory.Description,
                                                            RootCategoryIconUrl = x.RootCategory.IconStorage.Path,
                                                            RootCategoryPosterUrl = x.RootCategory.PosterStorage.Path,

                                                            ParentCateforyId = x.RootCategory.ParentCategoryId,
                                                            ParentCategoryKey = x.RootCategory.ParentCategory.Guid,
                                                            ParentCategoryName = x.RootCategory.ParentCategory.Name,

                                                            Name = x.RootCategory.Name,
                                                            Description = x.RootCategory.Description,
                                                            Commission = x.Commission,
                                                            IconUrl = x.RootCategory.IconStorage.Path,
                                                            PosterUrl = x.RootCategory.PosterStorage.Path,

                                                            CreateDate = x.CreateDate,

                                                            ModifyDate = x.ModifyDate,

                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                        })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var _Details in Data)
                    {
                        if (!string.IsNullOrEmpty(_Details.IconUrl))
                        {
                            _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                        }
                        else
                        {
                            _Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.PosterUrl))
                        {
                            _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                        }
                        else
                        {
                            _Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                        if (!string.IsNullOrEmpty(_Details.RootCategoryIconUrl))
                        {
                            _Details.RootCategoryIconUrl = _AppConfig.StorageUrl + _Details.RootCategoryIconUrl;
                        }
                        else
                        {
                            _Details.RootCategoryIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.RootCategoryPosterUrl))
                        {
                            _Details.RootCategoryPosterUrl = _AppConfig.StorageUrl + _Details.RootCategoryPosterUrl;
                        }
                        else
                        {
                            _Details.RootCategoryPosterUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetDealCategoryList", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get list of root categories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetRootCategories(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.MDCategory
                                                 where x.RootCategory.ParentCategoryId == null
                                                 select new ODealCategory.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     RootCategoryId = x.RootCategoryId,
                                                     RootCategoryKey = x.RootCategory.Guid,
                                                     Name = x.RootCategory.Name,
                                                     Commission = x.Commission,
                                                     IconUrl = x.RootCategory.IconStorage.Path,
                                                     PosterUrl = x.RootCategory.PosterStorage.Path,

                                                     CreateDate = x.CreateDate,
                                                     CreatedById = x.CreatedById,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyById = x.ModifyById,
                                                     ModiFyByDisplayName = x.ModifyBy.DisplayName,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                 })
                                                                .Where(_Request.SearchCondition)
                                                        .Count();
                    }
                    #endregion
                    #region Get Data
                    List<ODealCategory.List> Data = (from x in _HCoreContext.MDCategory
                                                     where x.RootCategory.ParentCategoryId == null
                                                     select new ODealCategory.List
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,

                                                         RootCategoryId = x.RootCategoryId,
                                                         RootCategoryKey = x.RootCategory.Guid,
                                                         Name = x.RootCategory.Name,
                                                         Commission = x.Commission,
                                                         IconUrl = x.RootCategory.IconStorage.Path,
                                                         PosterUrl = x.RootCategory.PosterStorage.Path,

                                                         CreateDate = x.CreateDate,
                                                         CreatedById = x.CreatedById,
                                                         CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                         ModifyDate = x.ModifyDate,
                                                         ModifyById = x.ModifyById,
                                                         ModiFyByDisplayName = x.ModifyBy.DisplayName,

                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name,
                                                     })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var _Details in Data)
                    {
                        if (!string.IsNullOrEmpty(_Details.IconUrl))
                        {
                            _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                        }
                        else
                        {
                            _Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.PosterUrl))
                        {
                            _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                        }
                        else
                        {
                            _Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetRootCategories", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
