//==================================================================================
// FileName: FrameworkOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to storage,configuration and categories
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;


namespace HCore.TUC.Core.Framework.Operations
{
    public class FrameworkOperations
    {
        HCoreContext _HCoreContext;
        internal OResponse GetCategories()
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    var Data = _HCoreContext.TUCMerchantCategory
                                                .Where(x => x.StatusId == HelperStatus.Default.Active && x.RootCategory.ParentCategoryId == null)
                                                .OrderBy(x => x.RootCategory.Name)
                                                .Select(x => new OOperations.Category
                                                {
                                                    ReferenceId = x.RootCategory.Id,
                                                    ReferenceKey = x.RootCategory.Guid,
                                                    Name = x.RootCategory.Name,
                                                    IconUrl = x.RootCategory.IconStorage.Path,
                                                })
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(Data.Count, Data, 0, Data.Count);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(null, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetCategories", _Exception, null, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get list of categories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCategories(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    var Data = _HCoreContext.TUCMerchantCategory
                                                .Where(x =>x.StatusId == HelperStatus.Default.Active && x.RootCategory.ParentCategoryId == null)
                                                .OrderBy(x=>x.RootCategory.Name)
                                                .Select(x => new OOperations.Category
                                                {
                                                    ReferenceId = x.RootCategory.Id,
                                                    ReferenceKey = x.RootCategory.Guid,
                                                    Name = x.RootCategory.Name,
                                                    IconUrl = x.RootCategory.IconStorage.Path,
                                                })
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCategories", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to get list of city areas
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCityArea(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    var Data = _HCoreContext.HCCoreCountryStateCityArea
                                                .Where(x =>  x.StatusId == HelperStatus.Default.Active && x.SystemName == _Request.ReferenceKey)
                                                .OrderBy(x => x.Name)
                                                .Select(x => new OOperations.Category
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                })
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCityArea", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to get list of downloaded files
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.HCUAccountParameter
                                                 where x.TypeId == HelperType.Downloads
                                                 && x.AccountId == _Request.AccountId
                                                 && x.Account.Guid == _Request.AccountKey
                                                 && x.Account.CountryId == _Request.UserReference.CountryId
                                                 select new OOperations.Download
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     Title = x.Name,
                                                     Url = x.IconStorage.Path,
                                                     CreateDate = x.CreateDate,
                                                     CreatedById = x.CreatedById,
                                                     CreatedByKey = x.CreatedBy.DisplayName,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     ModifyDate = x.ModifyDate,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                     
                                                 })
                                         .Where(_Request.SearchCondition)
                                 .Count();
                    }
                    #endregion
                    #region Get Data
                    List<OOperations.Download> _Data = (from x in _HCoreContext.HCUAccountParameter
                                                        where x.TypeId == HelperType.Downloads
                                                                       && x.AccountId == _Request.AccountId
                                                                       && x.Account.Guid == _Request.AccountKey
                                                                       && x.Account.CountryId == _Request.UserReference.CountryId
                                                                       select new OOperations.Download
                                                                       {
                                                                           ReferenceId = x.Id,
                                                                           ReferenceKey = x.Guid,
                                                                           Title = x.Name,
                                                                           Url = x.IconStorage.Path,
                                                                           CreateDate = x.CreateDate,
                                                                           CreatedById = x.CreatedById,
                                                                           CreatedByKey = x.CreatedBy.DisplayName,
                                                                           CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                                           ModifyDate = x.ModifyDate,
                                                                           StatusCode = x.Status.SystemName,
                                                                           StatusName = x.Status.Name,

                                                                       })
                                         .Where(_Request.SearchCondition)
                                         .OrderBy(_Request.SortExpression)
                                         .Skip(_Request.Offset)
                                         .Take(_Request.Limit)
                                               .ToList();
                    #endregion
                    foreach (var DataItem in _Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.Url))
                        {
                            DataItem.Url = _AppConfig.StorageUrl + DataItem.Url;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Data, _Request.Offset, _Request.Limit);
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse,TUCCoreResource.CA0200 , TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetDownload", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
    }
}
