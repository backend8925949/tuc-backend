//==================================================================================
// FileName: FrameworkCoreAppManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to core app and its versions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Core.Framework.Operations
{
    public class FrameworkCoreAppManager
    {
        HCoreContext _HCoreContext;
        HCCoreApp _HCCoreApp;
        HCCoreAppVersion _HCCoreAppVersion;

        /// <summary>
        /// Description: Method defined to save core app
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveCoreApp(OCoreAppManager.Save.Request _Request)
        {
            try
            {
                if(string.IsNullOrEmpty(_Request.Accountkey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA1476, TUCCoreResource.CAA1476M);
                }

                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA1483, TUCCoreResource.CAA1483M);
                }

                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA1478, TUCCoreResource.CAA1478M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    bool _IsExists = _HCoreContext.HCUAccount.Any(x => x.Id == _Request.AccountId && x.Guid == _Request.Accountkey);
                    if (_IsExists)
                    {
                        bool _IsDetailsExist = _HCoreContext.HCCoreApp.Any(x => x.SystemName == SystemName && x.AccountId == _Request.AccountId);
                        if (_IsDetailsExist)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA1479, TUCCoreResource.CAA1479M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA1491, TUCCoreResource.CAA1491M);
                    }

                    _HCCoreApp = new HCCoreApp();
                    _HCCoreApp.Guid = HCoreHelper.GenerateGuid();
                    _HCCoreApp.AccountId = _Request.AccountId;
                    _HCCoreApp.Name = _Request.Name;
                    _HCCoreApp.SystemName = SystemName;
                    _HCCoreApp.Description = _Request.Description;
                    _HCCoreApp.AppKey = HCoreHelper.GenerateGuid();
                    _HCCoreApp.IpAddress = _Request.IpAddress;
                    _HCCoreApp.AllowLogging = _Request.AllowLogging;
                    _HCCoreApp.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCCoreApp.CreatedById = _Request.UserReference.AccountId;
                    _HCCoreApp.StatusId = StatusId;
                    _HCoreContext.HCCoreApp.Add(_HCCoreApp);
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                    var _Response = new
                    {
                        ReferenceId = _HCCoreApp.Id,
                        ReferenceKey = _HCCoreApp.Guid
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA1480, TUCCoreResource.CAA1480M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Save-CoreApp", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Method defined to update core app
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateCoreApp(OCoreAppManager.Update _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _AppDetails = _HCoreContext.HCCoreApp.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_AppDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && _AppDetails.Name != _Request.Name)
                        {
                            string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                            bool _IsExist = _HCoreContext.HCCoreApp.Any(x => x.Name == _Request.Name && x.Id != _AppDetails.Id && _AppDetails.AccountId == _Request.AccountId);
                            if (_IsExist)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA1479, TUCCoreResource.CAA1479M);
                            }

                            _AppDetails.Name = _Request.Name;
                            _AppDetails.SystemName = SystemName;
                        }

                        if (!string.IsNullOrEmpty(_Request.Description) && _AppDetails.Description != _Request.Description)
                        {
                            _AppDetails.Description = _Request.Description;
                        }
                        if (!string.IsNullOrEmpty(_Request.IpAddress) && _Request.IpAddress != _AppDetails.IpAddress)
                        {
                            _AppDetails.IpAddress = _Request.IpAddress;
                        }
                        if (_Request.AllowLogging > 0)
                        {
                            _AppDetails.AllowLogging = _Request.AllowLogging;
                        }
                        if (StatusId > 0)
                        {
                            _AppDetails.StatusId = StatusId;
                        }
                        _AppDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _AppDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AppDetails, TUCCoreResource.CAA1481, TUCCoreResource.CAA1481M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }

            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Update-CoreApp", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Method defined to delete core app
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteCoreApp(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCCoreApp
                        .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .FirstOrDefault();
                    if (_Details != null)
                    {
                        var _VersionDetails = _HCoreContext.HCCoreAppVersion
                            .Any(x => x.AppId == _Request.ReferenceId && x.App.Guid == _Request.ReferenceKey);
                        if(!_VersionDetails)
                        {
                            _HCoreContext.HCCoreApp.Remove(_Details);
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CAA1482, TUCCoreResource.CAA1482M);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA1484, TUCCoreResource.CAA1484M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Delete-CoreApp", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Method to get details of core app
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCoreApp(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCCoreApp.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .Select(x => new OCoreAppManager.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            AccountId = x.AccountId,
                            Accountkey = x.Account.Guid,
                            AccountDisplayName = x.Account.DisplayName,

                            AccountTypeId = x.Account.AccountTypeId,
                            AccountTypeCode = x.Account.AccountType.Guid,
                            AccountTypeName = x.Account.AccountType.Name,

                            Name = x.Name,
                            SystemName = x.SystemName,
                            Description = x.Description,
                            AppKey = x.AppKey,
                            IpAddress = x.IpAddress,
                            AllowLogging = x.AllowLogging,

                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyById,
                            ModifyByKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                        }).FirstOrDefault();
                    if (_Details != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Get-CoreApp", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Method defined to get list of core app
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCoreApps(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCCoreApp
                            .Select(x => new OCoreAppManager.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                AccountId = x.AccountId,
                                Accountkey = x.Account.Guid,
                                AccountDisplayName = x.Account.DisplayName,

                                AccountTypeId = x.Account.AccountTypeId,
                                AccountTypeName = x.Account.AccountType.Name,

                                Name = x.Name,
                                SystemName = x.SystemName,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedBy.Id,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyById = x.ModifyBy.Id,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                            })
                            .Where(_Request.SearchCondition)
                            .Count();

                    }
                    List<OCoreAppManager.List> Data = _HCoreContext.HCCoreApp
                                                .Select(x => new OCoreAppManager.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    AccountId = x.AccountId,
                                                    Accountkey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,

                                                    AccountTypeId = x.Account.AccountType.Id,
                                                    AccountTypeName = x.Account.AccountType.Name,

                                                    Name = x.Name,
                                                    SystemName = x.SystemName,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedBy.Id,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyBy.Id,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Get-CoreApps", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }


        /// <summary>
        /// Description: Method defined to save core app version
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveCoreAppVersion(OCoreAppVersionManager.Save.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.AppKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA1485, TUCCoreResource.CAA1485M);
                }
                if (_Request.AppId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA1486, TUCCoreResource.CAA1486M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    bool _IsDetailsExist = _HCoreContext.HCCoreApp.Any(x => x.Id == _Request.AppId && x.Guid == _Request.AppKey);
                    if (_IsDetailsExist)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA1487, TUCCoreResource.CAA1487M);
                    }

                    _HCCoreAppVersion = new HCCoreAppVersion();
                    _HCCoreAppVersion.Guid = HCoreHelper.GenerateGuid();
                    _HCCoreAppVersion.AppId = _Request.AppId;
                    _HCCoreAppVersion.VersionId = (_HCoreContext.HCCoreAppVersion.Where(x => x.VersionId == _HCCoreAppVersion.VersionId).Count()+1).ToString();
                   // _HCCoreAppVersion.VersionId = _HCoreContext.HCCoreAppVersion.Where(x => x.AppId == _HCCoreAppVersion.AppId).Count().ToString();
                    _HCCoreAppVersion.ApiKey = HCoreHelper.GenerateGuid();
                    _HCCoreAppVersion.PublicKey = HCoreHelper.GenerateGuid();
                    _HCCoreAppVersion.PrivateKey = HCoreHelper.GenerateGuid();
                    _HCCoreAppVersion.SystemPublicKey = HCoreHelper.GenerateGuid();
                    _HCCoreAppVersion.SystemPrivateKey = HCoreHelper.GenerateGuid();
                    _HCCoreAppVersion.Comment = HCoreHelper.GenerateGuid();
                    _HCCoreAppVersion.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCCoreAppVersion.CreatedById = _Request.UserReference.AccountId;
                    _HCCoreAppVersion.StatusId = StatusId;
                    _HCoreContext.HCCoreAppVersion.Add(_HCCoreAppVersion);
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                    var _Response = new
                    {
                        ReferenceId = _HCCoreAppVersion.Id,
                        ReferenceKey = _HCCoreAppVersion.Guid
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA1488, TUCCoreResource.CAA1488M);
                }
            }
            catch (Exception _Exception)
            {

                HCoreHelper.LogException("Save-CoreAppVersion", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Method defined to update core app version status
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateCoreAppVersionStatus(OCoreAppVersionManager.UpdateStatus _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                    var _AppVersionDetails = _HCoreContext.HCCoreAppVersion.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_AppVersionDetails != null)
                    {
                        if (StatusId > 0)
                        {
                            _AppVersionDetails.StatusId = StatusId;
                        }
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AppVersionDetails, TUCCoreResource.CAA1489, TUCCoreResource.CAA1489M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }

            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Update-CoreAppVersionStatus", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Method defined to delete core app version
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteCoreAppVersion(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCCoreAppVersion
                        .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .FirstOrDefault();
                    if (_Details != null)
                    {
                        _HCoreContext.HCCoreAppVersion.Remove(_Details);
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CAA1490, TUCCoreResource.CAA1490M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Delete-CoreAppVersion", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Method defined to get details of core app version
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCoreAppVersion(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCCoreAppVersion.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .Select(x => new OCoreAppVersionManager.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            AppId = x.AppId,
                            AppKey = x.App.Guid,
                            AppName = x.App.Name,
                            AppSystemName = x.App.SystemName,

                            VersionId = x.VersionId,
                            PublicKey = x.PublicKey,
                            PrivateKey = x.PrivateKey,
                            SystemPublicKey = x.SystemPublicKey,
                            SystemPrivateKey = x.SystemPrivateKey,
                            Comment = x.Comment,

                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyById,
                            ModifyByKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                        }).FirstOrDefault();
                    if (_Details != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Get-CoreAppVersion", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Method defined to get list of core app version
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCoreAppVersions(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCCoreAppVersion
                            .Select(x => new OCoreAppVersionManager.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                AppId = x.AppId,
                                AppKey = x.App.Guid,
                                AppName = x.App.Name,

                                VersionId = x.VersionId,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyById = x.ModifyById,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                            })
                            .Where(_Request.SearchCondition)
                            .Count();

                    }
                    List<OCoreAppVersionManager.List> Data = _HCoreContext.HCCoreAppVersion
                                                .Select(x => new OCoreAppVersionManager.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    AppId = x.AppId,
                                                    AppKey = x.App.Guid,
                                                    AppName = x.App.Name,

                                                    VersionId = x.VersionId,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Get-CoreAppVersions", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
    }
}
