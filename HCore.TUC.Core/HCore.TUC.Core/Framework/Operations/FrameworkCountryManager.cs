//==================================================================================
// FileName: FrameworkCountryManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to country
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Core.Framework.Operations
{
    public class FrameworkCountryManager
    {
        HCoreContext _HCoreContext;
        HCCoreCountry _HCCoreCountry;

        /// <summary>
        /// Description:Method to get list of countries
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCountry(OList.Request _Request)
        {
            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCCoreCountry
                            .Select(x => new OCountryManager.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                Name = x.Name,
                                SystemName = x.SystemName,
                                ISD = x.Isd,
                                ISO = x.Iso,
                                CurrencyName = x.CurrencyName,
                                CurrencyNotation = x.CurrencyNotation,
                                CurrencySymbol = x.CurrencySymbol,
                                CurrencyHex = x.CurrencyHex,
                                CapitalName = x.CapitalName,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyById = x.ModifyById,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                            })
                            .Where(_Request.SearchCondition)
                            .Count();

                    }
                    List<OCountryManager.List> Data = _HCoreContext.HCCoreCountry
                                                .Select(x => new OCountryManager.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Name = x.Name,
                                                    SystemName = x.SystemName,
                                                    ISD = x.Isd,
                                                    ISO = x.Iso,
                                                    CurrencyName = x.CurrencyName,
                                                    CurrencyNotation = x.CurrencyNotation,
                                                    CurrencySymbol = x.CurrencySymbol,
                                                    CurrencyHex = x.CurrencyHex,
                                                    CapitalName = x.CapitalName,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCountries", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }



        /// <summary>
        /// Description:Method to get list of countries
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCountries(OList.Request _Request)
        {


            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCCoreCountry
                            .Select(x => new OCountryManager.ListLite
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                Name = x.Name,
                                SystemName = x.SystemName,
                            })
                                                 .Where(_Request.SearchCondition)
                                                 .Count();

                    }
                    List<OCountryManager.ListLite> Data = _HCoreContext.HCCoreCountry
                           .Select(x => new OCountryManager.ListLite
                           {
                               ReferenceId = x.Id,
                               ReferenceKey = x.Guid,
                               Name = x.Name,
                               SystemName = x.SystemName,
                           })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCountries", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }


            #endregion
        }
    }
}
