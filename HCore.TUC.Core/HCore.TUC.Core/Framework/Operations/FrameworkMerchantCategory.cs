//==================================================================================
// FileName: FrameworkMerchantCategory.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to merchant category
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Core.Framework.Operations
{
    public class FrameworkMerchantCategory
    {

        HCoreContext _HCoreContext;
        TUCMerchantCategory _TUCMerchantCategory;
        TUCCategory _TUCCategory;

        /// <summary>
        /// Description: Method defined to save merchant category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveMerchantCategory(OMerchantCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.RootCategoryId <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.MCAREF, TUCCoreResource.MCAREFM);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                int? StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    bool RootCategoryExists = _HCoreContext.TUCCategory.Any(x => x.Id == _Request.RootCategoryId && x.Guid == _Request.RootCategoryKey);
                    if (RootCategoryExists)
                    {
                        var SubCategoories = _HCoreContext.TUCCategory.Where(x => x.ParentCategoryId == _Request.RootCategoryId && x.ParentCategory.Guid == _Request.RootCategoryKey && x.ParentCategoryId != null).ToList();
                        bool Exists = _HCoreContext.TUCMerchantCategory.Any(x => x.RootCategoryId == _Request.RootCategoryId && x.RootCategory.Guid == _Request.RootCategoryKey);
                        if (Exists)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAEXISTS, TUCCoreResource.CAEXISTSM);
                        }
                        _TUCMerchantCategory = new TUCMerchantCategory();
                        _TUCMerchantCategory.Guid = HCoreHelper.GenerateGuid();
                        _TUCMerchantCategory.RootCategoryId = _Request.RootCategoryId;
                        _TUCMerchantCategory.Commission = _Request.Commission;
                        _TUCMerchantCategory.CreateDate = HCoreHelper.GetGMTDateTime();
                        _TUCMerchantCategory.CreatedById = _Request.UserReference.AccountId;
                        _TUCMerchantCategory.StatusId = (int)StatusId;
                        _HCoreContext.TUCMerchantCategory.Add(_TUCMerchantCategory);

                        foreach(var Category in SubCategoories)
                        {
                            _TUCMerchantCategory = new TUCMerchantCategory();
                            _TUCMerchantCategory.Guid = HCoreHelper.GenerateGuid();
                            _TUCMerchantCategory.RootCategoryId = Category.Id;
                            _TUCMerchantCategory.Commission = _Request.Commission;
                            _TUCMerchantCategory.CreateDate = HCoreHelper.GetGMTDateTime();
                            _TUCMerchantCategory.CreatedById = _Request.UserReference.AccountId;
                            _TUCMerchantCategory.StatusId = (int)StatusId;
                            _HCoreContext.TUCMerchantCategory.Add(_TUCMerchantCategory);
                        }

                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();

                        var Response = new
                        {
                            ReferenceId = _TUCMerchantCategory.Id,
                            ReferenceKey = _TUCMerchantCategory.Guid
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, TUCCoreResource.CASAVE, TUCCoreResource.CASAVEM);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveMerchantCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to update merchant category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateMerchantCategory(OMerchantCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantCategoryDetails = _HCoreContext.TUCMerchantCategory.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (MerchantCategoryDetails != null)
                    {
                        if (_Request.Commission >= 0 && _Request.Commission != MerchantCategoryDetails.Commission)
                        {
                            MerchantCategoryDetails.Commission = _Request.Commission;
                        }
                        if (StatusId != 0 && MerchantCategoryDetails.StatusId != StatusId)
                        {
                            MerchantCategoryDetails.StatusId = StatusId;
                        }
                        MerchantCategoryDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        MerchantCategoryDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();

                        var _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.MCA14164, TUCCoreResource.MCA14164M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateMerchantCategory", _Exception, _Request.UserReference);
                #endregion
            }
        }

        /// <summary>
        /// Description: Method defined to delete merchant category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteMerchantCategory(OMerchantCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    TUCMerchantCategory Details = _HCoreContext.TUCMerchantCategory.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        bool SubCategoryCheck = _HCoreContext.TUCMerchantCategory.Any(x => x.RootCategory.ParentCategoryId == Details.RootCategoryId);
                        if (SubCategoryCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAT004, TUCCoreResource.CAT004M);
                        }
                        _HCoreContext.TUCMerchantCategory.Remove(Details);
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.MCA14165, TUCCoreResource.MCA14165M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteMerchantCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get details of merchant category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetMerchantCategory(OMerchantCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OMerchantCategory.Details _Details = (from x in _HCoreContext.TUCMerchantCategory
                                                          where x.Guid == _Request.ReferenceKey
                                                          && x.Id == _Request.ReferenceId
                                                          select new OMerchantCategory.Details
                                                          {

                                                              ReferenceId = x.Id,
                                                              ReferenceKey = x.Guid,

                                                              RootCategoryId = x.RootCategoryId,
                                                              RootCategoryKey = x.RootCategory.Guid,
                                                              RootCategoryName = x.RootCategory.Name,
                                                              RootCategoryDescription = x.RootCategory.Description,
                                                              RootCategoryIconUrl = x.RootCategory.IconStorage.Path,
                                                              RootCategoryPosterUrl = x.RootCategory.PosterStorage.Path,

                                                              ParentCateforyId = x.RootCategory.ParentCategoryId,
                                                              ParentCategoryKey = x.RootCategory.ParentCategory.Guid,
                                                              ParentCategoryName = x.RootCategory.ParentCategory.Name,

                                                              Name = x.RootCategory.Name,
                                                              Description = x.RootCategory.Description,
                                                              Commission = x.Commission,
                                                              IconUrl = x.RootCategory.IconStorage.Path,
                                                              PosterUrl = x.RootCategory.PosterStorage.Path,

                                                              CreateDate = x.CreateDate,
                                                              CreatedById = x.CreatedById,
                                                              CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                              ModifyDate = x.ModifyDate,
                                                              ModifyById = x.ModifyById,
                                                              ModiFyByDisplayName = x.ModifyBy.DisplayName,

                                                              StatusId = x.StatusId,
                                                              StatusCode = x.Status.SystemName,
                                                              StatusName = x.Status.Name,
                                                          })
                                              .FirstOrDefault();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    if (_Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Details.IconUrl))
                        {
                            _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                        }
                        else
                        {
                            _Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.PosterUrl))
                        {
                            _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                        }
                        else
                        {
                            _Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                        if (!string.IsNullOrEmpty(_Details.RootCategoryIconUrl))
                        {
                            _Details.RootCategoryIconUrl = _AppConfig.StorageUrl + _Details.RootCategoryIconUrl;
                        }
                        else
                        {
                            _Details.RootCategoryIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.RootCategoryPosterUrl))
                        {
                            _Details.RootCategoryPosterUrl = _AppConfig.StorageUrl + _Details.RootCategoryPosterUrl;
                        }
                        else
                        {
                            _Details.RootCategoryPosterUrl = _AppConfig.Default_Icon;
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetMerchantCategory", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get list of merchant categories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetMerchantCategoryList(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.TUCMerchantCategory
                                                 where x.RootCategory.ParentCategoryId == _Request.ReferenceId
                                                 select new OMerchantCategory.Details
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     RootCategoryId = x.RootCategoryId,
                                                     RootCategoryKey = x.RootCategory.Guid,
                                                     RootCategoryName = x.RootCategory.Name,
                                                     RootCategoryDescription = x.RootCategory.Description,
                                                     RootCategoryIconUrl = x.RootCategory.IconStorage.Path,
                                                     RootCategoryPosterUrl = x.RootCategory.PosterStorage.Path,

                                                     ParentCateforyId = x.RootCategory.ParentCategoryId,
                                                     ParentCategoryKey = x.RootCategory.ParentCategory.Guid,
                                                     ParentCategoryName = x.RootCategory.ParentCategory.Name,

                                                     Name = x.RootCategory.Name,
                                                     Description = x.RootCategory.Description,
                                                     Commission = x.Commission,
                                                     IconUrl = x.RootCategory.IconStorage.Path,
                                                     PosterUrl = x.RootCategory.PosterStorage.Path,

                                                     CreateDate = x.CreateDate,

                                                     ModifyDate = x.ModifyDate,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                 })
                                                                .Where(_Request.SearchCondition)
                                                        .Count();
                    }
                    #endregion
                    #region Get Data
                    List<OMerchantCategory.Details> Data = (from x in _HCoreContext.TUCMerchantCategory
                                                            where x.RootCategory.ParentCategoryId == _Request.ReferenceId
                                                            select new OMerchantCategory.Details
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,

                                                                RootCategoryId = x.RootCategoryId,
                                                                RootCategoryKey= x.RootCategory.Guid,
                                                                RootCategoryName = x.RootCategory.Name,
                                                                RootCategoryDescription = x.RootCategory.Description,
                                                                RootCategoryIconUrl = x.RootCategory.IconStorage.Path,
                                                                RootCategoryPosterUrl = x.RootCategory.PosterStorage.Path,

                                                                ParentCateforyId = x.RootCategory.ParentCategoryId,
                                                                ParentCategoryKey = x.RootCategory.ParentCategory.Guid,
                                                                ParentCategoryName = x.RootCategory.ParentCategory.Name,

                                                                Name = x.RootCategory.Name,
                                                                Description = x.RootCategory.Description,
                                                                Commission = x.Commission,
                                                                IconUrl = x.RootCategory.IconStorage.Path,
                                                                PosterUrl = x.RootCategory.PosterStorage.Path,

                                                                CreateDate = x.CreateDate,

                                                                ModifyDate = x.ModifyDate,

                                                                StatusId = x.StatusId,
                                                                StatusCode = x.Status.SystemName,
                                                                StatusName = x.Status.Name,
                                                            })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var _Details in Data)
                    {
                        if (!string.IsNullOrEmpty(_Details.IconUrl))
                        {
                            _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                        }
                        else
                        {
                            _Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.PosterUrl))
                        {
                            _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                        }
                        else
                        {
                            _Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                        if (!string.IsNullOrEmpty(_Details.RootCategoryIconUrl))
                        {
                            _Details.RootCategoryIconUrl = _AppConfig.StorageUrl + _Details.RootCategoryIconUrl;
                        }
                        else
                        {
                            _Details.RootCategoryIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.RootCategoryPosterUrl))
                        {
                            _Details.RootCategoryPosterUrl = _AppConfig.StorageUrl + _Details.RootCategoryPosterUrl;
                        }
                        else
                        {
                            _Details.RootCategoryPosterUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetMerchantCategoryList", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method to get list of root categories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetRootCategories(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.TUCMerchantCategory
                                                 where x.RootCategory.ParentCategoryId == null
                                                 select new OMerchantCategory.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     RootCategoryId = x.RootCategoryId,
                                                     RootCategoryKey = x.RootCategory.Guid,
                                                     Name = x.RootCategory.Name,
                                                     Commission = x.Commission,
                                                     IconUrl = x.RootCategory.IconStorage.Path,
                                                     PosterUrl = x.RootCategory.PosterStorage.Path,

                                                     CreateDate = x.CreateDate,
                                                     CreatedById = x.CreatedById,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyById = x.ModifyById,
                                                     ModiFyByDisplayName = x.ModifyBy.DisplayName,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                 })
                                                                .Where(_Request.SearchCondition)
                                                        .Count();
                    }
                    #endregion
                    #region Get Data
                    List<OMerchantCategory.List> Data = (from x in _HCoreContext.TUCMerchantCategory
                                                         where x.RootCategory.ParentCategoryId == null
                                                         select new OMerchantCategory.List
                                                         {
                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,

                                                             RootCategoryId = x.RootCategoryId,
                                                             RootCategoryKey = x.RootCategory.Guid,
                                                             Name = x.RootCategory.Name,
                                                             Commission = x.Commission,
                                                             IconUrl = x.RootCategory.IconStorage.Path,
                                                             PosterUrl = x.RootCategory.PosterStorage.Path,

                                                             CreateDate = x.CreateDate,
                                                             CreatedById = x.CreatedById,
                                                             CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                             ModifyDate = x.ModifyDate,
                                                             ModifyById = x.ModifyById,
                                                             ModiFyByDisplayName = x.ModifyBy.DisplayName,

                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name,
                                                         })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var _Details in Data)
                    {
                        if (!string.IsNullOrEmpty(_Details.IconUrl))
                        {
                            _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                        }
                        else
                        {
                            _Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.PosterUrl))
                        {
                            _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                        }
                        else
                        {
                            _Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetRootCategories", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
