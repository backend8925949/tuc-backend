//==================================================================================
// FileName: FrameworkStorage.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to storage
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant.HelperStatus;

namespace HCore.TUC.Core.Framework.Operations
{
    internal class FrameworkStorage
    {
        HCoreContext _HCoreContext;
        HCCoreStorage _HCCoreStorage;
        /// <summary>
        /// Description: Method defined to save store images,pdf any attachments
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveStorage(OStorageManager.Request _Request)
        {
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    string StoragePath  = HCoreHelper.SaveStorage(_Request.StorageContent.Name, _Request.StorageContent.Extension, _Request.StorageContent.Content, _Request.UserReference);
                    if (string.IsNullOrEmpty(StoragePath) )
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1295, TUCCoreResource.CA1295M);
                    }
                    HCCoreStorage _HCCoreStorage = new HCCoreStorage();
                    _HCCoreStorage.Guid = HCoreHelper.GenerateGuid();
                    _HCCoreStorage.AccountId = _Request.AccountId;
                    _HCCoreStorage.StorageTypeId = 1;
                    _HCCoreStorage.SourceId = _AppConfig.StorageSourceId;
                    if (string.IsNullOrEmpty(_Request.StorageContent.Name))
                    {
                        _HCCoreStorage.Name = HCoreHelper.GenerateGuid();
                    }
                    else
                    {
                        _HCCoreStorage.Name = _Request.StorageContent.Name;
                    }
                    _HCCoreStorage.Path = StoragePath;
                    _HCCoreStorage.Extension = _Request.StorageContent.Extension;
                    //_HCCoreStorage.HelperId = HelperId;
                    _HCCoreStorage.Size = 0;
                    //_HCCoreStorage.Reference = Reference;
                    _HCCoreStorage.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCCoreStorage.StatusId = 2;
                    _HCoreContext.HCCoreStorage.Add(_HCCoreStorage);
                    //HCoreHelper.LogActivity(_HCoreContext, UserReference);
                    _HCoreContext.SaveChanges();
                    var _Response = new
                    {
                        ReferenceId = _HCCoreStorage.Id,
                        ReferenceKey = _HCCoreStorage.Guid,
                        Url = _AppConfig.StorageUrl + StoragePath
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1296, TUCCoreResource.CA1296M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DeleteStorage", _Exception);
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }

        }

        /// <summary>
        /// Description: Method defined to get list of storage
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetStorage(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.HCCoreStorage
                                                 where x.AccountId == _Request.UserReference.AccountId
                                                 || x.HelperId == 1
                                                 select new OStorageManager.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     IconUrl = _AppConfig.StorageUrl + x.Path,
                                                     IsPublic = x.HelperId,
                                                     CreateDate = x.CreateDate,
                                                 })
                                         .Where(_Request.SearchCondition)
                                 .Count();
                    }
                    #endregion
                    #region Get Data
                    List<OStorageManager.List> _Subscriptions = (from x in _HCoreContext.HCCoreStorage
                                                                 where x.AccountId == _Request.UserReference.AccountId
                                                                 || x.HelperId == 1
                                                                 select new OStorageManager.List
                                                                 {
                                                                     ReferenceId = x.Id,
                                                                     ReferenceKey = x.Guid,
                                                                     IconUrl = _AppConfig.StorageUrl + x.Path,
                                                                     IsPublic = x.HelperId,
                                                                     CreateDate = x.CreateDate,
                                                                 }).Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                    #endregion
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Subscriptions, _Request.Offset, _Request.Limit);
                    _HCoreContext.Dispose();
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetStorage", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to delete stored images,pdf any attachments
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteStorage(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var StorageDetails = _HCoreContext.HCCoreStorage.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (StorageDetails != null)
                    {
                        string Path = StorageDetails.Path;
                        _HCoreContext.HCCoreStorage.Remove(StorageDetails);
                        _HCoreContext.SaveChanges();
                        HCoreHelper.DeleteStorage(Path, _Request.UserReference);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1294, TUCCoreResource.CA1294M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DeleteStorage", _Exception);
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }

        }

    }
}
