//==================================================================================
// FileName: FramweorkCityAreaManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to city area
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Core.Framework.Operations
{
    public class FrameworkCityAreaManager
    {
        HCoreContext _HCoreContext;
        HCCoreCountryStateCityArea _HCCoreCountryStateCityArea;

        /// <summary>
        /// Description: Method defined to save city area
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveCityArea(OCityAreaManager.Save.Request _Request)
        {
            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.CityKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14100, TUCCoreResource.CAA14100M);
                }
                if (_Request.CityId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14101, TUCCoreResource.CAA14101M);
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14102, TUCCoreResource.CAA14102M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    bool _IsExists = _HCoreContext.HCCoreCountryStateCity.Any(x => x.Id == _Request.CityId && x.Guid == _Request.CityKey);
                    if (_IsExists)
                    {
                        bool _Exists = _HCoreContext.HCCoreCountryStateCityArea.Any(x => x.SystemName == SystemName && x.CityId == _Request.CityId);
                        if (_Exists)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14104, TUCCoreResource.CAA14104M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14103, TUCCoreResource.CAA14103M);
                    }

                    _HCCoreCountryStateCityArea = new HCCoreCountryStateCityArea();
                    _HCCoreCountryStateCityArea.Guid = HCoreHelper.GenerateGuid();
                    _HCCoreCountryStateCityArea.CityId = _Request.CityId;
                    _HCCoreCountryStateCityArea.Name = _Request.Name;
                    _HCCoreCountryStateCityArea.SystemName = SystemName;
                    _HCCoreCountryStateCityArea.Latitude = _Request.Latitude;
                    _HCCoreCountryStateCityArea.Longitude = _Request.Longitude;
                    _HCCoreCountryStateCityArea.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCCoreCountryStateCityArea.CreatedById = _Request.UserReference.AccountId;
                    _HCCoreCountryStateCityArea.StatusId = StatusId;
                    _HCoreContext.HCCoreCountryStateCityArea.Add(_HCCoreCountryStateCityArea);
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();

                    var _Response = new
                    {
                        ReferenceId = _HCCoreCountryStateCityArea.Id,
                        ReferenceKey = _HCCoreCountryStateCityArea.Guid
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA14105, TUCCoreResource.CAA14105M);
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SaveCityArea", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to update city area
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateCityArea(OCityAreaManager.Update _Request)
        {
            #region
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _CityAreaDetails = _HCoreContext.HCCoreCountryStateCityArea.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_CityAreaDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && _CityAreaDetails.Name != _Request.Name)
                        {
                            string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                            bool _IsExist = _HCoreContext.HCCoreCountryStateCityArea.Any(x => x.Name == _Request.Name && x.Id != _CityAreaDetails.Id && _CityAreaDetails.CityId == _Request.CityId);
                            if (_IsExist)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14104, TUCCoreResource.CAA14104M);
                            }

                            _CityAreaDetails.Name = _Request.Name;
                            _CityAreaDetails.SystemName = SystemName;
                        }
                        if (_Request.Longitude != 0)
                        {
                            _CityAreaDetails.Longitude = _Request.Longitude;
                        }
                        if (_Request.Latitude != 0)
                        {
                            _CityAreaDetails.Latitude = _Request.Latitude;
                        }
                        if (StatusId > 0)
                        {
                            _CityAreaDetails.StatusId = StatusId;
                        }

                        _CityAreaDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _CityAreaDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();

                        var _Response = new
                        {
                            ReferenceId = _CityAreaDetails.Id,
                            ReferenceKey = _CityAreaDetails.Guid
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA14106, TUCCoreResource.CAA14106M);

                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateCityArea", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to delete city area
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteCityArea(OReference _Request)
        {
            #region
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _CityAreaDetails = _HCoreContext.HCCoreCountryStateCityArea
                        .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .FirstOrDefault();
                    if (_CityAreaDetails != null)
                    {
                        _HCoreContext.HCCoreCountryStateCityArea.Remove(_CityAreaDetails);
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CAA14107, TUCCoreResource.CAA14107M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DeleteCityArea", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to get city area details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCityArea(OReference _Request)
        {
            #region
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCCoreCountryStateCityArea.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .Select(x => new OCityAreaManager.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            CityId = x.CityId,
                            CityKey = x.City.Guid,
                            CityName = x.City.Name,
                            Name = x.Name,
                            SystemName = x.SystemName,
                            Latitude = x.Latitude,
                            Longitude = x.Longitude,

                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyById,
                            ModifyByKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                        }).FirstOrDefault();

                    if (_Details != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCityArea", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to get list of city areas
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCityArea(OList.Request _Request)
        {
            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCCoreCountryStateCityArea
                            .Where(x => x.CityId == _Request.ReferenceId
                             && x.City.Guid == _Request.ReferenceKey)
                            .Select(x => new OCityAreaManager.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                CityId = x.CityId,
                                CityKey = x.City.Guid,
                                CityName = x.City.Name,
                                Name = x.Name,
                                SystemName = x.SystemName,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyById = x.ModifyById,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                            })
                            .Where(_Request.SearchCondition)
                            .Count();

                    }
                    List<OCityAreaManager.List> Data = _HCoreContext.HCCoreCountryStateCityArea
                            .Where(x => x.CityId == _Request.ReferenceId
                             && x.City.Guid == _Request.ReferenceKey)
                                                .Select(x => new OCityAreaManager.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    CityId = x.CityId,
                                                    CityKey = x.City.Guid,
                                                    CityName = x.City.Name,
                                                    Name = x.Name,
                                                    SystemName = x.SystemName,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCityArea", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }


        /// <summary>
        /// Description: Method defined to get list of city areas
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCityAreas(OList.Request _Request)
        {


            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCCoreCountryStateCityArea
                            .Select(x => new OCityAreaManager.ListLite
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                Name = x.Name,
                                SystemName = x.SystemName,
                            })
                                                 .Where(_Request.SearchCondition)
                                                 .Count();

                    }
                    List<OCityAreaManager.ListLite> Data = _HCoreContext.HCCoreCountryStateCityArea
                           .Select(x => new OCityAreaManager.ListLite
                           {
                               ReferenceId = x.Id,
                               ReferenceKey = x.Guid,
                               Name = x.Name,
                               SystemName = x.SystemName,
                           })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAreas", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }


            #endregion
        }
    }
}
