//==================================================================================
// FileName: FrameworkAddress.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to states
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.Framework.Operations
{
    public class FrameworkAddress
    {
        HCoreContext? _HCoreContext;
        HCCoreAddress? _HCCoreAddress;
        MDDealAddress? _MDDealAddress;

        /// <summary>
        /// Description: Method to save address
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveAddress(OAddressManager.Save.Request _Request)
        {
            try
            {
                //if (_Request.CountryId < 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM440", ResponseCodeAddress.HCLM440);
                //}
                if (_Request.StateId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM441", ResponseCodeAddress.HCLM441);
                }
                if (_Request.CityId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM442", ResponseCodeAddress.HCLM442);
                }
                if (string.IsNullOrEmpty(_Request.AddressLine1))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", ResponseCodeAddress.HCLM443);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey)
                             .Select(x => new
                             {
                                 AccountId = x.Id,
                                 StatusId = x.StatusId,
                                 Name = x.Name,
                                 EmailAddress = x.EmailAddress,
                                 MobileNumber = x.MobileNumber,
                                 CountryId = x.CountryId
                             }).FirstOrDefault();

                    _HCCoreAddress = new HCCoreAddress();
                    _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                    _HCCoreAddress.AccountId = AccountDetails.AccountId;
                    if (_Request.IsPrimary != null)
                    {
                        if (_Request.IsPrimary ==  true)
                        {
                            _HCCoreAddress.IsPrimaryAddress = 1;
                            var CustAddressList = _HCoreContext.HCCoreAddress.Where(x => x.AccountId == _Request.AccountId).ToList();
                            foreach (var CustAdd in CustAddressList)
                            {
                                CustAdd.IsPrimaryAddress = 0;
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(_Request.DisplayName))
                    {
                        _HCCoreAddress.DisplayName = _Request.DisplayName;
                    }
                    else
                    {
                        _HCCoreAddress.DisplayName = "Other";
                    }
                    if (_Request.LocationTypeId > 0)
                    {
                        _HCCoreAddress.LocationTypeId = _Request.LocationTypeId;
                    }
                    else
                    {
                        _HCCoreAddress.LocationTypeId = 800;
                    }

                    if (!string.IsNullOrEmpty(_Request.Name))
                    {
                        _HCCoreAddress.Name = _Request.Name;
                    }
                    else
                    {
                        _HCCoreAddress.Name = AccountDetails.Name;
                    }

                    if (!string.IsNullOrEmpty(_Request.ContactNumber))
                    {
                        _HCCoreAddress.ContactNumber = _Request.ContactNumber;
                    }
                    else
                    {
                        _HCCoreAddress.ContactNumber = AccountDetails.MobileNumber;
                    }


                    if (!string.IsNullOrEmpty(_Request.EmailAddress))
                    {
                        _HCCoreAddress.EmailAddress = _Request.EmailAddress;
                    }
                    else
                    {
                        _HCCoreAddress.EmailAddress = AccountDetails.EmailAddress;
                    }
                    _HCCoreAddress.AdditionalContactNumber = _Request.AdditionalMobileNumber;
                    _HCCoreAddress.AddressLine1 = _Request.AddressLine1;
                    _HCCoreAddress.AddressLine2 = _Request.AddressLine2;
                    _HCCoreAddress.ZipCode = _Request.ZipCode;
                    _HCCoreAddress.Landmark = _Request.Landmark;
                    if (_Request.CityAreaId > 0)
                    {
                        _HCCoreAddress.CityAreaId = _Request.CityAreaId;
                    }
                    if (_Request.CityId > 0)
                    {
                        _HCCoreAddress.CityId = _Request.CityId;
                    }
                    if (_Request.StateId > 0)
                    {
                        _HCCoreAddress.StateId = _Request.StateId;
                    }
                    _HCCoreAddress.MapAddress = _Request.MapAddress;
                    _HCCoreAddress.CountryId = (int)AccountDetails.CountryId;
                    _HCCoreAddress.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCCoreAddress.CreatedById = _Request.UserReference.AccountId;
                    _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                    _HCoreContext.HCCoreAddress.Add(_HCCoreAddress);
                    _HCoreContext.SaveChanges();

                    _MDDealAddress = new MDDealAddress();
                    _MDDealAddress.AccountId = AccountDetails.AccountId;
                    _MDDealAddress.Address = _HCCoreAddress;
                    _HCoreContext.MDDealAddress.Add(_MDDealAddress);
                    _HCoreContext.SaveChanges();

                    var Response = new OAddressManager.Save.Response
                    {
                        ReferenceId = _HCCoreAddress.Id,
                        ReferenceKey = _HCCoreAddress.Guid,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, "HCLM444", ResponseCodeAddress.HCLM444);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveAddress", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Method to update address
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateAddress(OAddressManager.Save.Request _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM445", ResponseCodeAddress.HCLM445);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM445", ResponseCodeAddress.HCLM445);
                }
                //if (_Request.CountryId < 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM440", ResponseCodeAddress.HCLM440);
                //}
                if (_Request.StateId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM441", ResponseCodeAddress.HCLM441);
                }
                if (_Request.CityId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM442", ResponseCodeAddress.HCLM442);
                }
                if (string.IsNullOrEmpty(_Request.AddressLine1))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM443", ResponseCodeAddress.HCLM443);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var AddressDetails = _HCoreContext.HCCoreAddress.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey).FirstOrDefault();
                    if (AddressDetails == null)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM446", ResponseCodeAddress.HCLM446);
                    }

                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey)
                             .Select(x => new
                             {
                                 AccountId = x.Id,
                                 StatusId = x.StatusId,
                                 Name = x.Name,
                                 EmailAddress = x.EmailAddress,
                                 MobileNumber = x.MobileNumber,
                                 CountryId = x.CountryId
                             }).FirstOrDefault();

                    if(_Request.IsPrimary == true)
                    {
                        AddressDetails.IsPrimaryAddress = 1;
                        var Addresses = _HCoreContext.HCCoreAddress.Where(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey).ToList();
                        foreach(var Address in Addresses)
                        {
                            Address.IsPrimaryAddress = 0;
                        }
                        _HCoreContext.SaveChanges();
                    }

                    if (!string.IsNullOrEmpty(_Request.DisplayName))
                    {
                        AddressDetails.DisplayName = _Request.DisplayName;
                    }
                    else
                    {
                        AddressDetails.DisplayName = "Other";
                    }
                    AddressDetails.LocationTypeId = _Request.LocationTypeId;

                    if (!string.IsNullOrEmpty(_Request.Name))
                    {
                        AddressDetails.Name = _Request.Name;
                    }
                    else
                    {
                        AddressDetails.Name = AccountDetails.Name;
                    }

                    if (!string.IsNullOrEmpty(_Request.ContactNumber))
                    {
                        AddressDetails.ContactNumber = _Request.ContactNumber;
                    }
                    else
                    {
                        AddressDetails.ContactNumber = AccountDetails.MobileNumber;
                    }


                    if (!string.IsNullOrEmpty(_Request.EmailAddress))
                    {
                        AddressDetails.EmailAddress = _Request.EmailAddress;
                    }
                    else
                    {
                        AddressDetails.EmailAddress = AccountDetails.EmailAddress;
                    }

                    AddressDetails.AdditionalContactNumber = _Request.AdditionalMobileNumber;
                    AddressDetails.AddressLine1 = _Request.AddressLine1;
                    AddressDetails.AddressLine2 = _Request.AddressLine2;
                    AddressDetails.ZipCode = _Request.ZipCode;
                    AddressDetails.Landmark = _Request.Landmark;
                    if (_Request.CityAreaId > 0)
                    {
                        AddressDetails.CityAreaId = _Request.CityAreaId;
                    }
                    if (_Request.CityId > 0)
                    {
                        AddressDetails.CityId = _Request.CityId;
                    }
                    if (_Request.StateId > 0)
                    {
                        AddressDetails.StateId = _Request.StateId;
                    }
                    AddressDetails.CountryId = (int)AccountDetails.CountryId;
                    AddressDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                    AddressDetails.ModifyById = _Request.UserReference.AccountId;
                    _HCoreContext.SaveChanges();

                    var Response = new OAddressManager.Save.Response
                    {
                        ReferenceId = AddressDetails.Id,
                        ReferenceKey = AddressDetails.Guid,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, "HCLM447", ResponseCodeAddress.HCLM447);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateAddress", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Method to delete address
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteAddress(OAddressManager.Save.Request _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM448", ResponseCodeAddress.HCLM448);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM448", ResponseCodeAddress.HCLM448);
                }
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM445", ResponseCodeAddress.HCLM445);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM445", ResponseCodeAddress.HCLM445);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var UserAccountTypeId = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey).Select(x => x.AccountTypeId).FirstOrDefault();
                    if(UserAccountTypeId == UserAccountType.Appuser)
                    {
                        var AddressDetails = _HCoreContext.HCCoreAddress.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey).FirstOrDefault();

                        var PrimaryAddress = _HCoreContext.HCCoreAddress.Where(x => x.AccountId == _Request.AccountId && x.IsPrimaryAddress == 1).Select(x => x.Id).FirstOrDefault();

                        if (AddressDetails == null)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM446", ResponseCodeAddress.HCLM446);
                        }

                        if (AddressDetails.IsPrimaryAddress == 1)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM448", "Can not delete this address. Please add another address as your default address and then try to delete this address.");
                        }

                        var Orders = _HCoreContext.LSShipments.Where(x => x.ToAddressId == AddressDetails.Id).ToList();
                        if (Orders != null)
                        {
                            if (PrimaryAddress != null)
                            {
                                foreach (var Order in Orders)
                                {
                                    Order.ToAddressId = PrimaryAddress;
                                }
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0500", "You cannot delete this address. Please add a new default address and try to delete this address.");
                            }
                        }

                        var DealCodes = _HCoreContext.MDDealCode.Where(x => x.ToAddressId == AddressDetails.Id && x.AccountId == _Request.AccountId).ToList();
                        if (DealCodes != null)
                        {
                            if (PrimaryAddress != null)
                            {
                                foreach (var DealCode in DealCodes)
                                {
                                    DealCode.ToAddressId = PrimaryAddress;
                                }
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0500", "You cannot delete this address. Please add a new default address and try to delete this address.");
                            }
                        }

                        var Details = _HCoreContext.MDDealAddress.Where(x => x.AddressId == AddressDetails.Id).FirstOrDefault();
                        if (Details != null)
                        {
                            _HCoreContext.MDDealAddress.Remove(Details);
                            _HCoreContext.SaveChanges();
                        }
                        _HCoreContext.HCCoreAddress.Remove(AddressDetails);
                        _HCoreContext.SaveChanges();

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCLM449", ResponseCodeAddress.HCLM449);
                    }
                    else
                    {
                        var AddressDetails = _HCoreContext.HCCoreAddress.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey).FirstOrDefault();

                        var PrimaryAddress = _HCoreContext.HCCoreAddress.Where(x => x.AccountId == _Request.AccountId && x.IsPrimaryAddress == 1).Select(x => x.Id).FirstOrDefault();

                        if (AddressDetails == null)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM446", ResponseCodeAddress.HCLM446);
                        }

                        if (AddressDetails.IsPrimaryAddress == 1)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM448", "Can not delete this address. Please add another address as your default address and then try to delete this address.");
                        }

                        var Orders = _HCoreContext.LSShipments.Where(x => x.FromAddressId == AddressDetails.Id || x.ReturnAddressId == AddressDetails.Id).ToList();
                        if (Orders != null)
                        {
                            if(PrimaryAddress != null)
                            {
                                foreach (var Order in Orders)
                                {
                                    Order.FromAddressId = PrimaryAddress;
                                    Order.ReturnAddressId = PrimaryAddress;
                                }
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0500", "You cannot delete this address. Please add a new default address and try to delete this address.");
                            }
                        }

                        var DealCodes = _HCoreContext.MDDealCode.Where(x => x.FromAddressId == AddressDetails.Id && x.Deal.AccountId == _Request.AccountId).ToList();
                        if (DealCodes != null)
                        {
                            if (PrimaryAddress != null)
                            {
                                foreach (var DealCode in DealCodes)
                                {
                                    DealCode.FromAddressId = PrimaryAddress;
                                }
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0500", "You cannot delete this address. Please add a new default address and try to delete this address.");
                            }
                        }

                        var Details = _HCoreContext.MDDealAddress.Where(x => x.AddressId == AddressDetails.Id).FirstOrDefault();
                        if (Details != null)
                        {
                            _HCoreContext.MDDealAddress.Remove(Details);
                            _HCoreContext.SaveChanges();
                        }
                        _HCoreContext.HCCoreAddress.Remove(AddressDetails);
                        _HCoreContext.SaveChanges();

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCLM449", ResponseCodeAddress.HCLM449);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteAddress", _Exception, _Request.UserReference, "HC0500", "Unable to process your request. Please try after sometime.");
            }
        }

        /// <summary>
        /// Description: Method to get address details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetAddress(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM448", ResponseCodeAddress.HCLM448);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM448", ResponseCodeAddress.HCLM448);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var AddressDetails = _HCoreContext.HCCoreAddress.Where(x =>
                    x.Id == _Request.ReferenceId
                    && x.Guid == _Request.ReferenceKey
                    && x.StatusId == HelperStatus.Default.Active)
                                         .Select(x => new OAddressManager.Details
                                         {
                                             ReferenceId = x.Id,
                                             ReferenceKey = x.Guid,

                                             DisplayName = x.DisplayName,
                                             Instructions = x.Instructions,
                                             Landmark = x.Landmark,
                                             Latitude = x.Latitude,
                                             Longitude = x.Longitude,
                                             MapAddress = x.MapAddress,


                                             CountryId = x.CountryId,
                                             CountryName = x.Country.Name,

                                             StateId = x.StateId,
                                             StateName = x.State.Name,

                                             CityId = x.CityId,
                                             CityName = x.City.Name,

                                             CityAreaId = x.CityAreaId,
                                             CityAreaName = x.CityArea.Name,


                                             Name = x.Name,
                                             EmailAddress = x.EmailAddress,
                                             ContactNumber = x.ContactNumber,
                                             AdditionalMobileNumber = x.AdditionalContactNumber,
                                             AddressLine1 = x.AddressLine1,
                                             AddressLine2 = x.AddressLine2,
                                             LocationTypeId = x.LocationTypeId,
                                             LocationTypeName = x.LocationType.Name,
                                             ZipCode = x.ZipCode,

                                             CreateDate = x.CreateDate,
                                             CreatedById = x.CreatedById,
                                             CreatedByDisplayName = x.CreatedBy.DisplayName,

                                             ModifyDate = x.ModifyDate,
                                             ModifyById = x.ModifyById,
                                             ModifyByDisplayName = x.ModifyBy.DisplayName,

                                         })
                                         .FirstOrDefault();
                    if (AddressDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, AddressDetails, "HCLM200", ResponseCodeAddress.HCLM200);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCLM404", ResponseCodeAddress.HCLM404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAddress", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Method to get list of address
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetAddress(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCCoreAddress
                            .Where(x=>x.AccountId == _Request.AccountId
                            && x.Account.Guid == _Request.AccountKey
                            && x.StatusId == HelperStatus.Default.Active)
                            .Select(x => new OAddressManager.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            DisplayName = x.DisplayName,
                            Instructions = x.Instructions,
                            Landmark = x.Landmark,
                            Latitude = x.Latitude,
                            Longitude = x.Longitude,
                            MapAddress = x.MapAddress,
                            IsPrimaryAddress = x.IsPrimaryAddress,

                            CountryId = x.CountryId,
                            CountryKey = x.Country.Guid,
                            CountryName = x.Country.Name,

                            StateId = x.StateId,
                            StateKey = x.State.Guid,
                            StateName = x.State.Name,

                            CityId = x.CityId,
                            CityKey = x.City.Guid,
                            CityName = x.City.Name,

                            CityAreaId = x.CityAreaId,
                            CityAreaKey = x.CityArea.Guid,
                            CityAreaName = x.CityArea.Name,

                            AdditionalMobileNumber = x.AdditionalContactNumber,
                            Name = x.Name,
                            EmailAddress = x.EmailAddress,
                            ContactNumber = x.ContactNumber,
                            AddressLine1 = x.AddressLine1,
                            AddressLine2 = x.AddressLine2,
                            LocationTypeId = x.LocationTypeId,
                            LocationTypeName = x.LocationType.Name,
                            ZipCode = x.ZipCode,

                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyById,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                        }).Where(_Request.SearchCondition)
                                             .Count();
                    }

                    List<OAddressManager.Details> Data = _HCoreContext.HCCoreAddress
                            .Where(x => x.AccountId == _Request.AccountId
                            && x.Account.Guid == _Request.AccountKey
                            && x.StatusId == HelperStatus.Default.Active)
                            .Select(x => new OAddressManager.Details
                    {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                DisplayName = x.DisplayName,
                                Instructions = x.Instructions,
                                Landmark = x.Landmark,
                                Latitude = x.Latitude,
                                Longitude = x.Longitude,
                                MapAddress = x.MapAddress,
                                IsPrimaryAddress = x.IsPrimaryAddress,

                                CountryId = x.CountryId,
                                CountryKey = x.Country.Guid,
                                CountryName = x.Country.Name,

                                StateId = x.StateId,
                                StateKey = x.State.Guid,
                                StateName = x.State.Name,

                                CityId = x.CityId,
                                CityKey = x.City.Guid,
                                CityName = x.City.Name,

                                CityAreaId = x.CityAreaId,
                                CityAreaKey = x.CityArea.Guid,
                                CityAreaName = x.CityArea.Name,

                                AdditionalMobileNumber = x.AdditionalContactNumber,
                                Name = x.Name,
                                EmailAddress = x.EmailAddress,
                                ContactNumber = x.ContactNumber,
                                AddressLine1 = x.AddressLine1,
                                AddressLine2 = x.AddressLine2,
                                LocationTypeId = x.LocationTypeId,
                                LocationTypeName = x.LocationType.Name,
                                ZipCode = x.ZipCode,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyById = x.ModifyById,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                            }).Where(_Request.SearchCondition)
                                                .OrderBy(_Request.SortExpression)
                                                .Skip(_Request.Offset)
                                                .Take(_Request.Limit)
                                                 .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCLM200", ResponseCodeAddress.HCLM200);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAddresses", _Exception, _Request.UserReference);
            }
        }
    }
}
