//==================================================================================
// FileName: FrameworkStateManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to states
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Core.Framework.Operations
{
    class FrameworkStateManager
    {
        HCoreContext _HCoreContext;
        HCCoreCountryState _HCCoreCountryState;

        /// <summary>
        /// Description: Method defined to save state
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveState(OStateManager.Save.Request _Request)
        {
            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.CountryKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14108, TUCCoreResource.CAA14108M);
                }
                if (_Request.CountryId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14109, TUCCoreResource.CAA14109M);
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14110, TUCCoreResource.CAA14110M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }

                using (_HCoreContext = new HCoreContext())
                {
                    string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    bool _IsExists = _HCoreContext.HCCoreCountry.Any(x => x.Id == _Request.CountryId && x.Guid == _Request.CountryKey);
                    if (_IsExists)
                    {
                        bool _Exists = _HCoreContext.HCCoreCountryState.Any(x => x.SystemName == SystemName && x.CountryId == _Request.CountryId);
                        if (_Exists)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14112, TUCCoreResource.CAA14112M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14111, TUCCoreResource.CAA14111M);
                    }

                    _HCCoreCountryState = new HCCoreCountryState();
                    _HCCoreCountryState.Guid = HCoreHelper.GenerateGuid();
                    if (_Request.CountryId > 0)
                    {
                        _HCCoreCountryState.CountryId = _Request.CountryId;
                    }
                    else
                    {
                        _HCCoreCountryState.CountryId = (int)_Request.UserReference.CountryId;
                    }
                    _HCCoreCountryState.Name = _Request.Name;
                    _HCCoreCountryState.SystemName = SystemName;
                    _HCCoreCountryState.Latitude = _Request.Latitude;
                    _HCCoreCountryState.Longitude = _Request.Longitude;
                    _HCCoreCountryState.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCCoreCountryState.CreatedById = _Request.UserReference.AccountId;
                    _HCCoreCountryState.StatusId = StatusId;
                    _HCoreContext.HCCoreCountryState.Add(_HCCoreCountryState);
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();

                    var _Response = new
                    {
                        ReferenceId = _HCCoreCountryState.Id,
                        ReferenceKey = _HCCoreCountryState.Guid
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA14114, TUCCoreResource.CAA14114M);
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SaveState", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to update state
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateState(OStateManager.Update _Request)
        {
            #region
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _StateDetails = _HCoreContext.HCCoreCountryState.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_StateDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && _StateDetails.Name != _Request.Name)
                        {
                            string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                            bool _IsExist = _HCoreContext.HCCoreCountryState.Any(x => x.Name == _Request.Name && x.Id != _StateDetails.Id && _StateDetails.CountryId == _Request.CountryId);
                            if (_IsExist)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14113, TUCCoreResource.CAA14113M);
                            }

                            _StateDetails.Name = _Request.Name;
                            _StateDetails.SystemName = SystemName;
                        }
                        if (_Request.Longitude != 0)
                        {
                            _StateDetails.Longitude = _Request.Longitude;
                        }
                        if (_Request.Latitude != 0)
                        {
                            _StateDetails.Latitude = _Request.Latitude;
                        }

                        if (StatusId > 0)
                        {
                            _StateDetails.StatusId = StatusId;
                        }
                        _StateDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _StateDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();

                        var _Response = new
                        {
                            ReferenceId = _StateDetails.Id,
                            ReferenceKey = _StateDetails.Guid
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA14114, TUCCoreResource.CAA14114M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateState", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to delete state
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteState(OReference _Request)
        {
            #region
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _StateDetails = _HCoreContext.HCCoreCountryState
                        .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .FirstOrDefault();
                    if (_StateDetails != null)
                    {
                        var _CityDetails = _HCoreContext.HCCoreCountryStateCity
                            .Any(x => x.StateId == _Request.ReferenceId && x.State.Guid == _Request.ReferenceKey);
                        if (!_CityDetails)
                        {
                            _HCoreContext.HCCoreCountryState.Remove(_StateDetails);
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CAA14115, TUCCoreResource.CAA14115M);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14123, TUCCoreResource.CAA14123M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DeleteState", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to get state details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetState(OReference _Request)
        {
            #region
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCCoreCountryState.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .Select(x => new OStateManager.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            CountryId = x.CountryId,
                            CountryKey = x.Country.Guid,
                            CountryName = x.Country.Name,
                            Name = x.Name,
                            SystemName = x.SystemName,
                            Latitude = x.Latitude,
                            Longitude = x.Longitude,

                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyById,
                            ModifyByKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                        }).FirstOrDefault();

                    if (_Details != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetState", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get list of states
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetState(OList.Request _Request)
        {
            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCCoreCountryState
                            .Where(x => x.CountryId == _Request.ReferenceId
                             && x.Country.Guid == _Request.ReferenceKey)
                            .Select(x => new OStateManager.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                CountryId = x.CountryId,
                                CountryKey = x.Country.Guid,
                                CountryName = x.Country.Name,
                                Name = x.Name,
                                SystemName = x.SystemName,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyById = x.ModifyById,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                            })
                                                 .Where(_Request.SearchCondition)
                                                 .Count();

                    }
                    List<OStateManager.List> Data = _HCoreContext.HCCoreCountryState
                            .Where(x => x.CountryId == _Request.ReferenceId
                             && x.Country.Guid == _Request.ReferenceKey)
                            .Select(x => new OStateManager.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                CountryId = x.CountryId,
                                CountryKey = x.Country.Guid,
                                CountryName = x.Country.Name,
                                Name = x.Name,
                                SystemName = x.SystemName,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyById = x.ModifyById,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                            })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetStates", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get list of states
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetStates(OList.Request _Request)
        {




            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCCoreCountryState
                            .Where(x => x.CountryId == _Request.ReferenceId
                             && x.Country.Guid == _Request.ReferenceKey)
                            .Select(x => new OStateManager.ListLite
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                Name = x.Name,
                                SystemName = x.SystemName,
                            })
                                                 .Where(_Request.SearchCondition)
                                                 .Count();

                    }
                    List<OStateManager.ListLite> Data = _HCoreContext.HCCoreCountryState
                            .Where(x => x.CountryId == _Request.ReferenceId
                             && x.Country.Guid == _Request.ReferenceKey)
                           .Select(x => new OStateManager.ListLite
                           {
                               ReferenceId = x.Id,
                               ReferenceKey = x.Guid,
                               Name = x.Name,
                               SystemName = x.SystemName,
                           })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetStates", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }




            #endregion
        }


    }
}
