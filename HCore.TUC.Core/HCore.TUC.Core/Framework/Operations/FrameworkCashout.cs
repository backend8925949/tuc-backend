//==================================================================================
// FileName: FrameworkCashout.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to cashouts
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Text;
using Akka.Actor;
using Delivery.Object.Response.Shipments;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Integration.Paystack;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant.HelperStatus;
namespace HCore.TUC.Core.Framework.Operations
{
    internal class FrameworkCashout
    {

        private const double MinimumTransferBalance = 100;
        private const double MinimumTransferAmount = 100;
        private const double MaximumTransferAmount = 500000;
        private const double TransferCharges = 50;

        OCashOut.Configuration.Response? _CashOutConfigurationResponse;
        HCoreContext? _HCoreContext;
        ManageCoreTransaction? _ManageCoreTransaction;
        TUCPayOut? _TUCPayout;
        OCoreTransaction.Request? _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem>? _TransactionItems;
        OCashOutOperation.Overview? _OCashOutOperationOverview;
        List<OCashOut.RewardList>? _RewardLists;
        OCashOut.Configuration.SummarizeResponse? _CashOutSummConfigurationResponse;
        List<OCashOut.ListDownload>? _ListDownload;

        /// <summary>
        /// Description:Method to get cashout configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal async Task<OResponse> GetTucCashOutConfiguration(OCashOut.Configuration.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId == 0)
                    {
                        _Request.AccountId = _Request.UserReference.AccountId;
                        _Request.AccountKey = _Request.UserReference.AccountKey;
                    }
                    var CustomerDetails = await _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.AccountId
                                    && x.Guid == _Request.AccountKey)
                       .FirstOrDefaultAsync();
                    if (CustomerDetails != null)
                    {
                        if (CustomerDetails.StatusId == HelperStatus.Default.Active)
                        {
                            _CashOutConfigurationResponse = new OCashOut.Configuration.Response();
                            _CashOutConfigurationResponse.Banks = await _HCoreContext.HCUAccountBank
                          .Where(x => x.AccountId == _Request.AccountId
                           && x.Account.Guid == _Request.AccountKey
                           && x.StatusId == HelperStatus.Default.Active)
                          .OrderByDescending(x => x.CreateDate)
                          .OrderByDescending(x => x.ModifyDate)
                                               .Select(x => new OBank.List
                                               {
                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,
                                                   Name = x.Name,
                                                   BankName = x.BankName,
                                                   AccountNumber = x.AccountNumber,
                                               })
                                            .ToListAsync();
                            await _HCoreContext.DisposeAsync();
                            _ManageCoreTransaction = new ManageCoreTransaction();
                            if (CustomerDetails.AccountTypeId == UserAccountType.Merchant)
                            {
                                _CashOutConfigurationResponse.TransferBalance = _ManageCoreTransaction.GetAccountBalance(_Request.AccountId, TransactionSource.Merchant);
                                _CashOutConfigurationResponse.MinimumTransferBalance = 1;
                                _CashOutConfigurationResponse.MinimumTransferAmount = 1;
                                _CashOutConfigurationResponse.MaximumTransferAmount = _CashOutConfigurationResponse.TransferBalance;
                                _CashOutConfigurationResponse.Charge = TransferCharges;
                                _CashOutConfigurationResponse.Title = "";
                                _CashOutConfigurationResponse.Description = "";
                            }
                            else
                            {
                                _CashOutConfigurationResponse.TransferBalance = _ManageCoreTransaction.GetAccountBalance(_Request.AccountId, TransactionSource.TUC);
                                _CashOutConfigurationResponse.MinimumTransferBalance = MinimumTransferBalance;
                                _CashOutConfigurationResponse.MinimumTransferAmount = MinimumTransferAmount;
                                _CashOutConfigurationResponse.MaximumTransferAmount = MaximumTransferAmount;
                                _CashOutConfigurationResponse.Charge = TransferCharges;
                                _CashOutConfigurationResponse.Title = "";
                                _CashOutConfigurationResponse.Description = "";

                            }

                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CashOutConfigurationResponse, TUCCoreResource.CA1272, TUCCoreResource.CA1272);
                        }
                        else
                        {
                            await _HCoreContext.DisposeAsync();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1176, TUCCoreResource.CA1176M);
                        }
                    }
                    else
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException(LogLevel.High, "GetTucCashOutConfiguration", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Author:Priya Chavadiya
        /// Description:Method to get configuration of all cashouts i.e BNPL,Deals,Loyalty
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal async Task<OResponse> GetTucSummarizeCashOutConfiguration(OCashOut.Configuration.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId == 0)
                    {
                        _Request.AccountId = _Request.UserReference.AccountId;
                        _Request.AccountKey = _Request.UserReference.AccountKey;
                    }
                    var CustomerDetails = await _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.AccountId
                                    && x.Guid == _Request.AccountKey)
                       .FirstOrDefaultAsync();
                    if (CustomerDetails != null)
                    {
                        if (CustomerDetails.StatusId == HelperStatus.Default.Active)
                        {
                            _CashOutSummConfigurationResponse = new OCashOut.Configuration.SummarizeResponse();
                            _CashOutSummConfigurationResponse.Banks = await _HCoreContext.HCUAccountBank
                           .Where(x => x.AccountId == _Request.AccountId
                            && x.Account.Guid == _Request.AccountKey
                            && x.StatusId == HelperStatus.Default.Active)
                           .OrderByDescending(x => x.CreateDate)
                           .OrderByDescending(x => x.ModifyDate)
                                                .Select(x => new OBank.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    BankName = x.BankName,
                                                    AccountNumber = x.AccountNumber,
                                                })
                                             .ToListAsync();
                            await _HCoreContext.DisposeAsync();
                            _CashOutSummConfigurationResponse.Loyalty = new OCashOut.Configuration.Response();
                            _CashOutSummConfigurationResponse.Deals = new OCashOut.Configuration.Response();
                            _CashOutSummConfigurationResponse.BNPL = new OCashOut.Configuration.Response();
                            _ManageCoreTransaction = new ManageCoreTransaction();
                            _CashOutSummConfigurationResponse.Loyalty.TransferBalance = GetAccountBalance(_Request.AccountId, TransactionSource.Merchant, RedeemFrom.Loyalty, CustomerDetails.AccountTypeId);
                            _CashOutSummConfigurationResponse.Loyalty.MinimumTransferBalance = 1;
                            _CashOutSummConfigurationResponse.Loyalty.MinimumTransferAmount = 1;
                            _CashOutSummConfigurationResponse.Loyalty.MaximumTransferAmount = _CashOutSummConfigurationResponse.Loyalty.TransferBalance;
                            _CashOutSummConfigurationResponse.Loyalty.Charge = TransferCharges;
                            _CashOutSummConfigurationResponse.Loyalty.Title = "";
                            _CashOutSummConfigurationResponse.Loyalty.Description = "";

                            _CashOutSummConfigurationResponse.Deals.TransferBalance = _ManageCoreTransaction.GetAccountBalance(_Request.AccountId, TransactionSource.Merchant);
                            _CashOutSummConfigurationResponse.Deals.MinimumTransferBalance = 1;
                            _CashOutSummConfigurationResponse.Deals.MinimumTransferAmount = 1;
                            _CashOutSummConfigurationResponse.Deals.MaximumTransferAmount = _CashOutSummConfigurationResponse.Deals.TransferBalance;
                            _CashOutSummConfigurationResponse.Deals.Charge = TransferCharges;
                            _CashOutSummConfigurationResponse.Deals.Title = "";
                            _CashOutSummConfigurationResponse.Deals.Description = "";

                            _CashOutSummConfigurationResponse.BNPL.TransferBalance = GetAccountBalance(_Request.AccountId, TransactionSource.Merchant, RedeemFrom.BNPL, CustomerDetails.AccountTypeId);
                            _CashOutSummConfigurationResponse.BNPL.MinimumTransferBalance = 1;
                            _CashOutSummConfigurationResponse.BNPL.MinimumTransferAmount = 1;
                            _CashOutSummConfigurationResponse.BNPL.MaximumTransferAmount = _CashOutSummConfigurationResponse.BNPL.TransferBalance;
                            _CashOutSummConfigurationResponse.BNPL.Charge = TransferCharges;
                            _CashOutSummConfigurationResponse.BNPL.Title = "";
                            _CashOutSummConfigurationResponse.BNPL.Description = "";

                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CashOutSummConfigurationResponse, TUCCoreResource.CA1272, TUCCoreResource.CA1272);
                        }
                        else
                        {
                            await _HCoreContext.DisposeAsync();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1176, TUCCoreResource.CA1176M);
                        }
                    }
                    else
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException(LogLevel.High, "GetTucSummarizeCashOutConfiguration", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description:Method to intialize cashout.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal async Task<OResponse> TucCashOutInitialize(OCashOut.Initialize.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.Amount <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1235, TUCCoreResource.CA1235M);
                }
                else if (string.IsNullOrEmpty(_Request.BankKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1273, TUCCoreResource.CA1273M);
                }
                else if (_Request.BankId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1274, TUCCoreResource.CA1274M);
                }
                else
                {
                    if (_Request.AccountId == 0)
                    {
                        _Request.AccountId = _Request.UserReference.AccountId;
                        _Request.AccountKey = _Request.UserReference.AccountKey;
                    }
                    double AccountBalance = 0;
                    using (_HCoreContext = new HCoreContext())
                    {
                        var CustomerDetails = await _HCoreContext.HCUAccount
                           .Where(x => x.Id == _Request.AccountId
                                       && x.Guid == _Request.AccountKey)
                          .FirstOrDefaultAsync();
                        if (CustomerDetails.AccountTypeId == UserAccountType.Merchant || CustomerDetails.AccountTypeId == UserAccountType.MerchantStore || CustomerDetails.AccountTypeId == UserAccountType.Appuser)
                        {
                            await _HCoreContext.DisposeAsync();
                            _ManageCoreTransaction = new ManageCoreTransaction();
                            if (CustomerDetails.AccountTypeId == UserAccountType.Appuser)
                            {
                                AccountBalance = _ManageCoreTransaction.GetAccountBalance(_Request.AccountId, TransactionSource.TUC);
                            }
                            else if (CustomerDetails.AccountTypeId == UserAccountType.Merchant)
                            {
                                if (_Request.RedeemFrom == RedeemFrom.Deals)
                                {
                                    AccountBalance = _ManageCoreTransaction.GetAccountBalance(_Request.AccountId, TransactionSource.Merchant);
                                }
                                else
                                {
                                    AccountBalance = GetAccountBalance(_Request.AccountId, TransactionSource.Merchant, _Request.RedeemFrom, CustomerDetails.AccountTypeId);
                                }
                            }
                            else if (CustomerDetails.AccountTypeId == UserAccountType.MerchantStore)
                            {
                                AccountBalance = GetAccountBalance(_Request.AccountId, TransactionSource.Merchant, _Request.RedeemFrom, CustomerDetails.AccountTypeId);
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1313, TUCCoreResource.CA1313M);
                            }
                        }

                        if (AccountBalance < MinimumTransferBalance)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA12751, TUCCoreResource.CA12751M);
                        }
                        if (CustomerDetails.AccountTypeId == UserAccountType.Merchant && _Request.Amount < 1)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1276, TUCCoreResource.CA1276M + " " + 1);
                        }
                        if (CustomerDetails.AccountTypeId == UserAccountType.Appuser && _Request.Amount < MinimumTransferAmount)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1276, TUCCoreResource.CA1276M + MinimumTransferAmount);
                        }
                        if (CustomerDetails.AccountTypeId == UserAccountType.Merchant && _Request.Amount > AccountBalance)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1277, TUCCoreResource.CA1277M + AccountBalance);
                        }
                        if (CustomerDetails.AccountTypeId == UserAccountType.Appuser && _Request.Amount > MaximumTransferAmount)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1277, TUCCoreResource.CA1277M + MaximumTransferAmount);
                        }

                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        var CustomerDetails = await _HCoreContext.HCUAccount
                            .Where(x => x.Id == _Request.AccountId
                                        && x.Guid == _Request.AccountKey)
                           .FirstOrDefaultAsync();
                        if (CustomerDetails != null)
                        {
                            if (CustomerDetails.StatusId == HelperStatus.Default.Active)
                            {

                                var BankDetails = await _HCoreContext.HCUAccountBank.Where(x => x.Id == _Request.BankId && x.Guid == _Request.BankKey && x.AccountId == _Request.AccountId).FirstOrDefaultAsync();
                                string GroupKey = "tsp" + _Request.AccountId + "O" + HCoreHelper.GenerateDateString();
                                string ReferenceNumber = GroupKey;
                                string ReferenceCode = null;
                                string ExtInvNo = null;
                                double PayStackCharges = 50;
                                double Commission = 0;
                                double TotalAmount = _Request.Amount + PayStackCharges + Commission;
                                if (TotalAmount > AccountBalance)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1319, TUCCoreResource.CA1319M);
                                }

                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.CustomerId = _Request.AccountId;
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = Transaction.Success;
                                _CoreTransactionRequest.GroupKey = GroupKey;
                                _CoreTransactionRequest.ParentId = SystemAccounts.TUCVas;
                                _CoreTransactionRequest.InvoiceAmount = TotalAmount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                                _CoreTransactionRequest.ReferenceNumber = ReferenceNumber;
                                _CoreTransactionRequest.InvoiceNumber = ExtInvNo;
                                _CoreTransactionRequest.CreatedById = _Request.AccountId;
                                _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                if (CustomerDetails.AccountTypeId == UserAccountType.Merchant)
                                {
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = _Request.AccountId,
                                        ModeId = TransactionMode.Debit,
                                        TypeId = TransactionType.TUCCashOut.CashOut,
                                        SourceId = TransactionSource.Merchant,
                                        Amount = _Request.Amount,
                                        Charge = PayStackCharges,
                                        Comission = Commission,
                                        TotalAmount = TotalAmount,
                                        Comment = ReferenceCode,
                                        RedeemFrom = _Request.RedeemFrom
                                    });
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = SystemAccounts.TUCVas,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.TUCCashOut.CashOut,
                                        SourceId = TransactionSource.CashOut,
                                        Amount = _Request.Amount,
                                        Charge = PayStackCharges,
                                        Comission = Commission,
                                        TotalAmount = TotalAmount,
                                        Comment = ReferenceCode,
                                        RedeemFrom = _Request.RedeemFrom
                                    });
                                }
                                else
                                {
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = _Request.AccountId,
                                        ModeId = TransactionMode.Debit,
                                        TypeId = TransactionType.TUCCashOut.CashOut,
                                        SourceId = TransactionSource.TUC,
                                        Amount = _Request.Amount,
                                        Charge = PayStackCharges,
                                        Comission = Commission,
                                        TotalAmount = TotalAmount,
                                        Comment = ReferenceCode,
                                    });
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = SystemAccounts.TUCVas,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.TUCCashOut.CashOut,
                                        SourceId = TransactionSource.CashOut,
                                        Amount = _Request.Amount,
                                        Charge = PayStackCharges,
                                        Comission = Commission,
                                        TotalAmount = TotalAmount,
                                        Comment = ReferenceCode,
                                    });
                                }
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        _TUCPayout = new TUCPayOut();
                                        _TUCPayout.Guid = HCoreHelper.GenerateGuid();
                                        _TUCPayout.AccountId = _Request.AccountId;
                                        _TUCPayout.BankId = _Request.BankId;
                                        _TUCPayout.Amount = _Request.Amount;
                                        _TUCPayout.Charge = PayStackCharges;
                                        _TUCPayout.TotalAmount = _Request.Amount + PayStackCharges;
                                        _TUCPayout.CommissionAmount = Commission;
                                        _TUCPayout.TotalAmount = TotalAmount;
                                        _TUCPayout.ReferenceNumber = GroupKey;
                                        _TUCPayout.StartDate = HCoreHelper.GetGMTDateTime();
                                        _TUCPayout.SourceId = TransactionSource.CashOut;
                                        _TUCPayout.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _TUCPayout.CreatedById = _Request.UserReference.AccountId;
                                        _TUCPayout.StatusId = CashoutStatus.Initialized;
                                        _TUCPayout.RedeemFrom = _Request.RedeemFrom;
                                        await _HCoreContext.TUCPayOut.AddAsync(_TUCPayout);
                                        await _HCoreContext.SaveChangesAsync();
                                    }
                                    var _ResponseLog = new
                                    {
                                        Amount = _Request.Amount,
                                        AccountId = CustomerDetails.Id,
                                        AccountKey = CustomerDetails.Guid,
                                        AccountDisplayName = CustomerDetails.DisplayName,
                                        AccountMobileNumber = CustomerDetails.MobileNumber,
                                        CreateDate = HCoreHelper.GetGMTDateTime().AddHours(1),
                                        Url = "https://betaconsole.thankucash.com/console/cashouts/" + _TUCPayout.Id + "/" + _TUCPayout.Guid + "/" + CustomerDetails.Id + "/" + CustomerDetails.Guid,
                                    };
                                    HCoreHelper.BroadCastAdminEmail("d-5c402a72b3d64e588c2d83aa14c3a97f", "TUC ACCOUNTS", "accounts@thankucash.com", _ResponseLog, _Request.UserReference);
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1286, TUCCoreResource.CA1286M);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1237, TUCCoreResource.CA1237M);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1176, TUCCoreResource.CA1176M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "TucCashOutInitialize", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description:Method to get list of cashouts
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal async Task<OResponse> GetTucCashout(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                if (_Request.AccountId == 0)
                {
                    _Request.AccountId = _Request.UserReference.AccountId;
                    _Request.AccountKey = _Request.UserReference.AccountKey;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = await _HCoreContext.TUCPayOut
                            .Where(x => x.AccountId == _Request.AccountId
                            && x.Account.Guid == _Request.AccountKey)
                                                .Select(x => new OCashOut.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    BankAccountName = x.Bank.Name,
                                                    BankName = x.Bank.BankName,
                                                    BankAccountNumber = x.Bank.AccountNumber,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    Amount = x.Amount,
                                                    Charge = x.Charge,
                                                    TotalAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    SystemComment = x.SystemComment,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .CountAsync();
                        #endregion
                    }
                    #region Get Data
                    List<OCashOut.List> _List = await _HCoreContext.TUCPayOut
                            .Where(x => x.AccountId == _Request.AccountId
                            && x.Account.Guid == _Request.AccountKey)
                                                .Select(x => new OCashOut.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    BankAccountName = x.Bank.Name,
                                                    BankName = x.Bank.BankName,
                                                    BankAccountNumber = x.Bank.AccountNumber,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    Amount = x.Amount,
                                                    Charge = x.Charge,
                                                    TotalAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    SystemComment = x.SystemComment
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToListAsync();
                    #endregion
                    //foreach (var DataItem in _List)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //    {
                    //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.IconUrl = _AppConfig.Default_Icon;
                    //    }
                    //}
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetTucCashout", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Author:Priya Chavadiya
        /// Description:Method to get list of all cashouts i.e BNPL,Deals,Loyalty
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetTucSummarizeCashout(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetRedeemWalletsDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetRedeemWalletsDownload>("ActorGetRedeemWalletsDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                if (_Request.AccountId == 0)
                {
                    _Request.AccountId = _Request.UserReference.AccountId;
                    _Request.AccountKey = _Request.UserReference.AccountKey;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.TUCPayOut
                            .Where(x => x.AccountId == _Request.AccountId
                            && x.Account.Guid == _Request.AccountKey
                            && x.RedeemFrom == _Request.RedeemFrom)
                                                .Select(x => new OCashOut.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    BankAccountName = x.Bank.Name,
                                                    BankName = x.Bank.BankName,
                                                    BankAccountNumber = x.Bank.AccountNumber,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    Amount = x.Amount,
                                                    Charge = x.Charge,
                                                    TotalAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    SystemComment = x.SystemComment,
                                                    TransactionTypeId = TransactionType.TUCCashOut.CashOut,
                                                    TransactionType = "CashOut",
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    WalletType = "Redeem Wallet",
                                                    SentFrom = "Redeem Wallet",
                                                    Description = "Cash out from Redeem Wallet",
                                                    MobileNumber = x.Account.MobileNumber,
                                                    StatusId = x.StatusId
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OCashOut.List> _List = _HCoreContext.TUCPayOut
                            .Where(x => x.AccountId == _Request.AccountId
                            && x.Account.Guid == _Request.AccountKey
                             && x.RedeemFrom == _Request.RedeemFrom)
                                                .Select(x => new OCashOut.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    BankAccountName = x.Bank.Name,
                                                    BankName = x.Bank.BankName,
                                                    BankAccountNumber = x.Bank.AccountNumber,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    Amount = x.Amount,
                                                    Charge = x.Charge,
                                                    TotalAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    SystemComment = x.SystemComment,
                                                    TransactionTypeId = TransactionType.TUCCashOut.CashOut,
                                                    TransactionType = "Cash Out",
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    WalletType = "Redeem Wallet",
                                                    SentFrom = "Redeem Wallet",
                                                    Description = "Cash out from Redeem Wallet",
                                                    MobileNumber = x.Account.MobileNumber,
                                                    StatusId = x.StatusId,
                                                    StoreName = _HCoreContext.HCUAccount.Where(y => y.AccountTypeId == UserAccountType.MerchantStore && y.OwnerId == x.AccountId).Select(s => s.DisplayName).FirstOrDefault(),
                                                    TransactionDate = x.CreateDate

                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    foreach (var DataItem in _List)
                    {
                        if (DataItem.StatusId == CashoutStatus.Initialized)
                        {
                            DataItem.StatusName = "Pending";
                        }
                        if (DataItem.StatusId == CashoutStatus.Success)
                        {
                            DataItem.StatusName = "Approved";
                        }
                        if (_Request.RedeemFrom == RedeemFrom.Loyalty)
                        {
                            DataItem.Account = RedeemFrom.LoyaltyM;
                        }
                        else if (_Request.RedeemFrom == RedeemFrom.Deals)
                        {
                            DataItem.Account = RedeemFrom.DealsM;
                        }
                        else if (_Request.RedeemFrom == RedeemFrom.BNPL)
                        {
                            DataItem.Account = RedeemFrom.BNPLM;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetTucCashout", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }


        //internal OResponse GetTucSummarizeCashoutFromParties(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        if (_Request.IsDownload)
        //        {
        //            var _Actor = ActorSystem.Create("ActorGetRedeemWalletsDownload");
        //            var _ActorNotify = _Actor.ActorOf<ActorGetRedeemWalletsDownload>("ActorGetRedeemWalletsDownload");
        //            _ActorNotify.Tell(_Request);
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
        //        }
        //        if (string.IsNullOrEmpty(_Request.SearchCondition))
        //        {
        //            HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
        //        }
        //        if (string.IsNullOrEmpty(_Request.SortExpression))
        //        {
        //            HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
        //        }
        //        if (_Request.Limit < 1)
        //        {
        //            _Request.Limit = _AppConfig.DefaultRecordsLimit;
        //        }
        //        if (_Request.AccountId == 0)
        //        {
        //            _Request.AccountId = _Request.UserReference.AccountId;
        //            _Request.AccountKey = _Request.UserReference.AccountKey;
        //        }
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            List<OCashOut.List> _List = _HCoreContext.TUCPayOut.Where(x => x.ParentId == _Request.AccountId
        //                                        && x.Parent.CountryId == _Request.UserReference.SystemCountry
        //                                        && x.TypeId == TransactionType.TUCCashOut.CashOut
        //                                        && x.ModeId == TransactionMode.Debit
        //                                        && x.SourceId == TransactionSource.Merchant).Select(x => new OCashOut.List
        //                                        {
        //                                            ReferenceId = x.Id,
        //                                            ReferenceKey = x.Guid,
        //                                            BankAccountName = x.Bank.Name,
        //                                            BankName = x.Bank.BankName,
        //                                            BankAccountNumber = x.Bank.AccountNumber,
        //                                            StartDate = x.StartDate,
        //                                            EndDate = x.EndDate,
        //                                            Amount = x.Amount,
        //                                            Charge = x.Charge,
        //                                            TotalAmount = x.TotalAmount,
        //                                            ReferenceNumber = x.ReferenceNumber,
        //                                            StatusCode = x.Status.SystemName,
        //                                            StatusName = x.Status.Name,
        //                                            SystemComment = x.SystemComment,
        //                                            TransactionTypeId = TransactionType.TUCCashOut.CashOut,
        //                                            TransactionType = "Cash Out",
        //                                            AccountDisplayName = x.Account.DisplayName,
        //                                            WalletType = "Redeem Wallet",
        //                                            SentFrom = "Redeem Wallet",
        //                                            Description = "Cash out from Redeem Wallet",
        //                                            MobileNumber = x.Account.MobileNumber,
        //                                            StatusId = x.StatusId,
        //                                            StoreName = _HCoreContext.HCUAccount.Where(y => y.AccountTypeId == UserAccountType.MerchantStore && y.OwnerId == x.AccountId).Select(s => s.DisplayName).FirstOrDefault(),
        //                                            TransactionDate = x.CreateDate

        //                                        })
        //                                     .Where(_Request.SearchCondition)
        //                                     .OrderBy(_Request.SortExpression)
        //                                     .Skip(_Request.Offset)
        //                                     .Take(_Request.Limit)
        //                                     .ToList();
        //            #endregion
        //            foreach (var DataItem in _List)
        //            {
        //                if (DataItem.StatusId == CashoutStatus.Initialized)
        //                {
        //                    DataItem.StatusName = "Pending";
        //                }
        //                if (DataItem.StatusId == CashoutStatus.Success)
        //                {
        //                    DataItem.StatusName = "Approved";
        //                }
        //                if (_Request.RedeemFrom == RedeemFrom.Loyalty)
        //                {
        //                    DataItem.Account = RedeemFrom.LoyaltyM;
        //                }
        //                else if (_Request.RedeemFrom == RedeemFrom.Loyalty)
        //                {
        //                    DataItem.Account = RedeemFrom.DealsM;
        //                }
        //                else if (_Request.RedeemFrom == RedeemFrom.BNPL)
        //                {
        //                    DataItem.Account = RedeemFrom.BNPLM;
        //                }
        //            }
        //            _HCoreContext.Dispose();
        //            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        return HCoreHelper.LogException("GetTucCashout", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
        //    }

        //}


        internal void UpdateCashOutStatus(string _Request)
        {
            #region Manage Exception
            try
            {
                // HCoreHelper.LogData(HCoreConstant.LogType.Log, "WEBHOOKRECEIVEDCASHOUT", HCoreHelper.GenerateDateString(), _Request, HCoreHelper.GenerateDateString());
                OPaystackPayment.Request _Response = JsonConvert.DeserializeObject<OPaystackPayment.Request>(_Request);
                if (_Response.data != null)
                {
                    if (_Response.data.reference.StartsWith("tsp"))
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var Payout = _HCoreContext.TUCPayOut.Where(x => x.ReferenceNumber == _Response.data.reference).FirstOrDefault();
                            if (Payout != null)
                            {
                                if (Payout.StatusId == CashoutStatus.Initialized || Payout.StatusId == CashoutStatus.Processing)
                                {
                                    Payout.StatusId = CashoutStatus.Success;
                                    Payout.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "UpdateCashOutStatus", _Exception, null);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Author:Priya Chavadiya
        /// Description:Method to get overview of redeem wallets
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCashoutOverview(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC109", "Reference id missing");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC110", "Reference key missing");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId).FirstOrDefault();
                    _OCashOutOperationOverview = new OCashOutOperation.Overview();

                    _OCashOutOperationOverview.TotalAmount = GetAccountBalance(_Request.ReferenceId, TransactionSource.Merchant, _Request.RedeemFrom, CustomerDetails.AccountTypeId);

                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OCashOutOperationOverview, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                //OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCashoutOverview", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Author:Priya Chavadiya
        /// Description:Method to top up from Redeem wallet
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse RedeemFromWallet(OCashOut.Initialize.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.Amount <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1235, TUCCoreResource.CA1235M);
                }
                else
                {
                    if (_Request.AccountId == 0)
                    {
                        _Request.AccountId = _Request.UserReference.AccountId;
                        _Request.AccountKey = _Request.UserReference.AccountKey;
                    }
                    double AccountBalance = 0;
                    using (_HCoreContext = new HCoreContext())
                    {
                        var CustomerDetails = _HCoreContext.HCUAccount
                           .Where(x => x.Id == _Request.AccountId
                                       && x.Guid == _Request.AccountKey)
                          .FirstOrDefault();
                        if (CustomerDetails.AccountTypeId == UserAccountType.Merchant || CustomerDetails.AccountTypeId == UserAccountType.Appuser)
                        {
                            _HCoreContext.Dispose();

                            if (CustomerDetails.AccountTypeId == UserAccountType.Merchant)
                            {
                                AccountBalance = GetAccountBalance(_Request.AccountId, TransactionSource.Merchant, _Request.RedeemFrom, CustomerDetails.AccountTypeId);
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1313, TUCCoreResource.CA1313M);
                            }
                        }

                        if (AccountBalance < MinimumTransferBalance)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA12751, TUCCoreResource.CA12751M);
                        }
                        if (CustomerDetails.AccountTypeId == UserAccountType.Merchant && _Request.Amount < 1)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1276, TUCCoreResource.CA1276M + " " + 1);
                        }

                        if (CustomerDetails.AccountTypeId == UserAccountType.Merchant && _Request.Amount > AccountBalance)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1277, TUCCoreResource.CA1277M + AccountBalance);
                        }


                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        var CustomerDetails = _HCoreContext.HCUAccount
                            .Where(x => x.Id == _Request.AccountId
                                        && x.Guid == _Request.AccountKey)
                           .FirstOrDefault();
                        if (CustomerDetails != null)
                        {
                            if (CustomerDetails.StatusId == HelperStatus.Default.Active)
                            {

                                //var BankDetails = _HCoreContext.HCUAccountBank.Where(x => x.Id == _Request.BankId && x.Guid == _Request.BankKey && x.AccountId == _Request.AccountId).FirstOrDefault();
                                string GroupKey = "tsp" + _Request.AccountId + "O" + HCoreHelper.GenerateDateString();
                                string ReferenceNumber = GroupKey;
                                string ReferenceCode = null;
                                string ExtInvNo = null;
                                double PayStackCharges = 0;
                                double Commission = 0;

                                double TotalAmount = _Request.Amount + PayStackCharges + Commission;
                                if (TotalAmount > AccountBalance)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1319, TUCCoreResource.CA1319M);
                                }

                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.CustomerId = _Request.AccountId;
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = Transaction.Success;
                                _CoreTransactionRequest.GroupKey = GroupKey;
                                _CoreTransactionRequest.ParentId = _Request.AccountId;
                                _CoreTransactionRequest.InvoiceAmount = TotalAmount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                                _CoreTransactionRequest.ReferenceNumber = ReferenceNumber;
                                _CoreTransactionRequest.InvoiceNumber = ExtInvNo;
                                _CoreTransactionRequest.CreatedById = _Request.AccountId;
                                _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                if (CustomerDetails.AccountTypeId == UserAccountType.Merchant)
                                {
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = _Request.AccountId,
                                        ModeId = TransactionMode.Debit,
                                        TypeId = TransactionType.TUCWalletTopup,
                                        SourceId = TransactionSource.Merchant,
                                        Amount = _Request.Amount,
                                        Charge = PayStackCharges,
                                        Comission = Commission,
                                        TotalAmount = TotalAmount,
                                        Comment = ReferenceCode,
                                        RedeemFrom = _Request.RedeemFrom
                                    });
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = _Request.AccountId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.Loyalty.TUCRedeem.TUCPay,
                                        SourceId = TransactionSource.Merchant,
                                        Amount = _Request.Amount,
                                        Charge = PayStackCharges,
                                        Comission = Commission,
                                        TotalAmount = TotalAmount,
                                        Comment = ReferenceCode,
                                        RedeemFrom = _Request.RedeemFrom
                                    });
                                }

                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA12776, TUCCoreResource.CA12776M + _Request.Amount);
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1088, TUCCoreResource.CA1088M);
                                    #endregion
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1176, TUCCoreResource.CA1176M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "TucCashOutInitialize", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Author:Priya Chavadiya
        /// Description:Method to get list of reward wallets
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetRewardWallets(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetRewardWalletsDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetRewardWalletsDownload>("ActorGetRewardWalletsDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _RewardLists = new List<OCashOut.RewardList>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.HCUAccountTransaction
                                                 where x.AccountId == _Request.AccountId
                                                 && x.ModeId == TransactionMode.Credit
                                                 && (x.TypeId == TransactionType.MerchantCredit || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
                                                 && x.SourceId == TransactionSource.Merchant
                                                 // && (x.RedeemFrom == RedeemFrom.Deals || x.RedeemFrom == RedeemFrom.BNPL || x.RedeemFrom == RedeemFrom.Loyalty)
                                                 select new OCashOut.RewardList
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     Amount = x.Amount,
                                                     TransactionDate = x.CreateDate,
                                                     PaymentReference = x.ReferenceNumber,
                                                     PaymentMethodCode = x.PaymentMethod.SystemName,
                                                     PaymentMethodName = x.PaymentMethod.Name,
                                                     CreatedByReferenceId = x.CreatedById,
                                                     CreatedByReferenceKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.Name,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                     TransactionTypeId = x.TypeId,
                                                     TransactionType = "Top-Up",
                                                     AccountDisplayName = x.Account.DisplayName,
                                                     WalletType = "Reward Wallet",
                                                     SentFrom = (x.TypeId == TransactionType.MerchantCredit ? "Card" : "Redeem Wallet"),
                                                     Description = "Top-Up of Reward Wallet",
                                                     MobileNumber = x.Account.MobileNumber,
                                                     RedeemFrom = x.RedeemFrom ?? 0
                                                 })
                                         .Where(_Request.SearchCondition)
                                 .Count();
                    }
                    #endregion
                    #region Get Data
                    _RewardLists = (from x in _HCoreContext.HCUAccountTransaction
                                    where x.AccountId == _Request.AccountId
                                   && x.ModeId == TransactionMode.Credit
                                   && (x.TypeId == TransactionType.MerchantCredit || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
                                   && x.SourceId == TransactionSource.Merchant
                                    //&& (x.RedeemFrom == RedeemFrom.Deals || x.RedeemFrom == RedeemFrom.BNPL || x.RedeemFrom == RedeemFrom.Loyalty)
                                    select new OCashOut.RewardList
                                    {
                                        ReferenceId = x.Id,
                                        ReferenceKey = x.Guid,
                                        Amount = x.Amount,
                                        TransactionDate = x.CreateDate,
                                        PaymentReference = x.ReferenceNumber,
                                        PaymentMethodCode = x.PaymentMethod.SystemName,
                                        PaymentMethodName = x.PaymentMethod.Name,
                                        CreatedByReferenceId = x.CreatedById,
                                        CreatedByReferenceKey = x.CreatedBy.Guid,
                                        CreatedByDisplayName = x.CreatedBy.Name,
                                        StatusCode = x.Status.SystemName,
                                        StatusName = x.Status.Name,
                                        TransactionTypeId = x.TypeId,
                                        TransactionType = "Top-Up",
                                        AccountDisplayName = x.Account.DisplayName,
                                        WalletType = "Reward Wallet",
                                        SentFrom = (x.TypeId == TransactionType.MerchantCredit ? "Card" : "Redeem Wallet"),
                                        Description = "Top-Up of Reward Wallet",
                                        MobileNumber = x.Account.MobileNumber,
                                        RedeemFrom = x.RedeemFrom ?? 0,
                                        StoreName = _HCoreContext.HCUAccount.Where(y => y.AccountTypeId == UserAccountType.MerchantStore && y.OwnerId == x.AccountId).Select(s => s.DisplayName).FirstOrDefault()

                                    })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object

                    foreach (var DataItem in _RewardLists)
                    {
                        if (_Request.RedeemFrom == RedeemFrom.Loyalty)
                        {
                            DataItem.Account = RedeemFrom.LoyaltyM;
                        }
                        else if (_Request.RedeemFrom == RedeemFrom.Loyalty)
                        {
                            DataItem.Account = RedeemFrom.DealsM;
                        }
                        else if (_Request.RedeemFrom == RedeemFrom.BNPL)
                        {
                            DataItem.Account = RedeemFrom.BNPLM;
                        }
                    }
                    _HCoreContext.Dispose();

                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _RewardLists, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardWallets", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

        private double GetAccountBalance(long AccountId, long SourceId, long RedeemId, long AccountTypeId)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    double Balance = 0, ReedemFromWallet = 0, Cashouts = 0;
                    if (RedeemId == RedeemFrom.Loyalty)
                    {
                        if (AccountTypeId == UserAccountType.MerchantStore)
                        {
                            Balance = _HCoreContext.HCUAccountTransaction
                                                   .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                   && x.SubParentId == AccountId
                                                   && x.ModeId == TransactionMode.Debit
                                                   && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                   && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                                  .Sum(x => x.TotalAmount);
                            ReedemFromWallet = _HCoreContext.HCUAccountTransaction
                                                       .Where(x =>
                                                        x.SubParentId == AccountId
                                                        && x.ModeId == TransactionMode.Debit
                                                        && x.RedeemFrom == RedeemFrom.Loyalty
                                                        && (x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
                                                        && x.SourceId == TransactionSource.Merchant)
                                                      .Sum(x => x.Amount);

                            Cashouts = _HCoreContext.TUCPayOut
                                   .Where(x => x.AccountId == AccountId
                                   && x.RedeemFrom == RedeemFrom.Loyalty
                                   && (x.StatusId == CashoutStatus.Success || x.StatusId == CashoutStatus.Initialized)).Distinct().Sum(x => x.Amount);
                        }
                        else
                        {
                            Balance = _HCoreContext.HCUAccountTransaction
                                                   .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                   && x.ParentId == AccountId
                                                   && x.ModeId == TransactionMode.Debit
                                                   && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
                                                   && x.Type.SubParentId == TransactionTypeCategory.Redeem)
                                                  .Sum(x => x.TotalAmount);
                            ReedemFromWallet = _HCoreContext.HCUAccountTransaction
                                                       .Where(x =>
                                                        x.AccountId == AccountId
                                                        && x.ModeId == TransactionMode.Debit
                                                        && x.RedeemFrom == RedeemFrom.Loyalty
                                                        && (x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
                                                        && x.SourceId == TransactionSource.Merchant)
                                                      .Sum(x => x.Amount);

                            Cashouts = _HCoreContext.TUCPayOut
                                   .Where(x => x.AccountId == AccountId
                                   && x.RedeemFrom == RedeemFrom.Loyalty
                                   && (x.StatusId == CashoutStatus.Success || x.StatusId == CashoutStatus.Initialized)).Distinct().Sum(x => x.Amount);
                        }

                    }
                    else if (RedeemId == RedeemFrom.Deals)
                    {
                        Balance = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == AccountId
                                  && x.ModeId == TransactionMode.Debit
                                  && x.SourceId == TransactionSource.Merchant
                                  && x.StatusId == HelperStatus.Transaction.Success
                                  && x.ProgramId == null)
                                  .Sum(x => x.TotalAmount);

                        ReedemFromWallet = _HCoreContext.HCUAccountTransaction
                                                   .Where(x =>
                                                    x.AccountId == AccountId
                                                    && x.ModeId == TransactionMode.Debit
                                                    && x.RedeemFrom == RedeemFrom.Deals
                                                    && (x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
                                                    && x.SourceId == TransactionSource.Merchant)
                                                  .Sum(x => x.TotalAmount);

                        Cashouts = _HCoreContext.TUCPayOut
                               .Where(x => x.AccountId == AccountId
                               && x.RedeemFrom == RedeemFrom.Deals
                               && (x.StatusId == CashoutStatus.Success || x.StatusId == CashoutStatus.Initialized)).Distinct().Sum(x => x.Amount);
                    }
                    else if (RedeemId == RedeemFrom.BNPL)
                    {
                        Balance = _HCoreContext.HCUAccountTransaction
                                                                   .Where(x => x.ParentId == AccountId
                                                                   && x.ModeId == TransactionMode.Debit
                                                                   && x.TypeId == TransactionType.TUCBnpl.LoanRedeem
                                                                   && x.SourceId == TransactionSource.TUCBnpl
                                                                   ).Sum(a => a.Amount);

                        ReedemFromWallet = _HCoreContext.HCUAccountTransaction
                                                  .Where(x =>
                                                   x.AccountId == AccountId
                                                   && x.ModeId == TransactionMode.Debit
                                                   && x.RedeemFrom == RedeemFrom.BNPL
                                                   && (x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
                                                   && x.SourceId == TransactionSource.Merchant)
                                                 .Sum(x => x.Amount);

                        Cashouts = _HCoreContext.TUCPayOut
                               .Where(x => x.AccountId == AccountId
                               && x.RedeemFrom == RedeemFrom.BNPL
                               && (x.StatusId == CashoutStatus.Success || x.StatusId == CashoutStatus.Initialized)).Distinct().Sum(x => x.Amount);

                    }
                    double FinalBalance = Balance - ReedemFromWallet - Cashouts;
                    return HCoreHelper.RoundNumber(FinalBalance, _AppConfig.SystemEntryRoundDouble); // Math.Round((TotalCredit - TotalDebit), HCoreConstant._AppConfig.AmountRoundDigit);
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAppUserBalance", _Exception);
                #endregion
                #region Send Response
                return 0;
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Author:Priya Chavadiya
        /// Description:Method to dowload list of reward wallets
        /// </summary>
        /// <param name="_Request"></param>
        internal void GetRewardWalletsDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Limit = 800;
                _ListDownload = new List<OCashOut.ListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _Request.TotalRecords = (from x in _HCoreContext.HCUAccountTransaction
                                             where x.AccountId == _Request.AccountId
                                             && x.ModeId == TransactionMode.Credit
                                             && (x.TypeId == TransactionType.MerchantCredit || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
                                             && x.SourceId == TransactionSource.Merchant
                                             // && (x.RedeemFrom == RedeemFrom.Deals || x.RedeemFrom == RedeemFrom.BNPL || x.RedeemFrom == RedeemFrom.Loyalty)
                                             select new OCashOut.RewardList
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,
                                                 Amount = x.Amount,
                                                 TransactionDate = x.CreateDate,
                                                 PaymentReference = x.ReferenceNumber,
                                                 PaymentMethodCode = x.PaymentMethod.SystemName,
                                                 PaymentMethodName = x.PaymentMethod.Name,
                                                 CreatedByReferenceId = x.CreatedById,
                                                 CreatedByReferenceKey = x.CreatedBy.Guid,
                                                 CreatedByDisplayName = x.CreatedBy.Name,
                                                 StatusCode = x.Status.SystemName,
                                                 StatusName = x.Status.Name,
                                                 TransactionTypeId = x.TypeId,
                                                 TransactionType = "Top-Up",
                                                 AccountDisplayName = x.Account.DisplayName,
                                                 WalletType = "Reward Wallet",
                                                 SentFrom = (x.TypeId == TransactionType.MerchantCredit ? "Card" : "Redeem Wallet"),
                                                 Description = "Top-Up of Reward Wallet",
                                                 MobileNumber = x.Account.MobileNumber,
                                                 RedeemFrom = x.RedeemFrom ?? 0
                                             })
                                         .Where(_Request.SearchCondition)
                                 .Count();
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data                      
                        var _Data = (from x in _HCoreContext.HCUAccountTransaction
                                     where x.AccountId == _Request.AccountId
                                    && x.ModeId == TransactionMode.Credit
                                    && (x.TypeId == TransactionType.MerchantCredit || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
                                    && x.SourceId == TransactionSource.Merchant
                                     //&& (x.RedeemFrom == RedeemFrom.Deals || x.RedeemFrom == RedeemFrom.BNPL || x.RedeemFrom == RedeemFrom.Loyalty)
                                     select new OCashOut.RewardList
                                     {
                                         ReferenceId = x.Id,
                                         ReferenceKey = x.Guid,
                                         Amount = x.Amount,
                                         TransactionDate = x.CreateDate,
                                         PaymentReference = x.ReferenceNumber,
                                         PaymentMethodCode = x.PaymentMethod.SystemName,
                                         PaymentMethodName = x.PaymentMethod.Name,
                                         CreatedByReferenceId = x.CreatedById,
                                         CreatedByReferenceKey = x.CreatedBy.Guid,
                                         CreatedByDisplayName = x.CreatedBy.Name,
                                         StatusCode = x.Status.SystemName,
                                         StatusName = x.Status.Name,
                                         TransactionTypeId = x.TypeId,
                                         TransactionType = "Top-Up",
                                         AccountDisplayName = x.Account.DisplayName,
                                         WalletType = "Reward Wallet",
                                         SentFrom = (x.TypeId == TransactionType.MerchantCredit ? "Card" : "Redeem Wallet"),
                                         Description = "Top-Up of Reward Wallet",
                                         MobileNumber = x.Account.MobileNumber,
                                         RedeemFrom = x.RedeemFrom ?? 0
                                     })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();

                        #endregion
                        foreach (var _DataItem in _Data)
                        {

                            if (_Request.RedeemFrom == RedeemFrom.Loyalty)
                            {
                                _DataItem.Account = RedeemFrom.LoyaltyM;
                            }
                            else if (_Request.RedeemFrom == RedeemFrom.Deals)
                            {
                                _DataItem.Account = RedeemFrom.DealsM;
                            }
                            else if (_Request.RedeemFrom == RedeemFrom.BNPL)
                            {
                                _DataItem.Account = RedeemFrom.BNPLM;
                            }
                            _ListDownload.Add(new OCashOut.ListDownload
                            {
                                Date = _DataItem.TransactionDate.ToString("dd/MM/yyyy"),
                                Time = _DataItem.TransactionDate.ToString("hh:mm tt"),
                                StoreName = "",
                                TransactionType = _DataItem.TransactionType,
                                TransactionID = _DataItem.PaymentReference
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("RewardWallets", _ListDownload, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardWalletsDownload", _Exception);
            }
            #endregion
        }

        /// <summary>
        /// Author:Priya Chavadiya
        /// Description:Method to dowload list of redeem wallets
        /// </summary>
        /// <param name="_Request"></param>
        internal void GetRedeemWalletsDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Limit = 800;
                _ListDownload = new List<OCashOut.ListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                if (_Request.AccountId == 0)
                {
                    _Request.AccountId = _Request.UserReference.AccountId;
                    _Request.AccountKey = _Request.UserReference.AccountKey;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _Request.TotalRecords = _HCoreContext.TUCPayOut
                        .Where(x => x.AccountId == _Request.AccountId
                        && x.Account.Guid == _Request.AccountKey
                        && x.RedeemFrom == _Request.RedeemFrom)
                                            .Select(x => new OCashOut.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                BankAccountName = x.Bank.Name,
                                                BankName = x.Bank.BankName,
                                                BankAccountNumber = x.Bank.AccountNumber,
                                                StartDate = x.StartDate,
                                                EndDate = x.EndDate,
                                                Amount = x.Amount,
                                                Charge = x.Charge,
                                                TotalAmount = x.TotalAmount,
                                                ReferenceNumber = x.ReferenceNumber,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,
                                                SystemComment = x.SystemComment,
                                                TransactionTypeId = TransactionType.TUCCashOut.CashOut,
                                                TransactionType = "CashOut",
                                                AccountDisplayName = x.Account.DisplayName,
                                                WalletType = "Redeem Wallet",
                                                SentFrom = "Redeem Wallet",
                                                Description = "Cash out from Redeem Wallet",
                                                MobileNumber = x.Account.MobileNumber,
                                                StatusId = x.StatusId,
                                                TransactionDate = x.CreateDate
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = _HCoreContext.TUCPayOut
                                .Where(x => x.AccountId == _Request.AccountId
                                && x.Account.Guid == _Request.AccountKey
                                 && x.RedeemFrom == _Request.RedeemFrom)
                                                    .Select(x => new OCashOut.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        BankAccountName = x.Bank.Name,
                                                        BankName = x.Bank.BankName,
                                                        BankAccountNumber = x.Bank.AccountNumber,
                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,
                                                        Amount = x.Amount,
                                                        Charge = x.Charge,
                                                        TotalAmount = x.TotalAmount,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        SystemComment = x.SystemComment,
                                                        TransactionTypeId = TransactionType.TUCCashOut.CashOut,
                                                        TransactionType = "Cash Out",
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        WalletType = "Redeem Wallet",
                                                        SentFrom = "Redeem Wallet",
                                                        Description = "Cash out from Redeem Wallet",
                                                        MobileNumber = x.Account.MobileNumber,
                                                        StatusId = x.StatusId,
                                                        TransactionDate = x.CreateDate

                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion

                        foreach (var _DataItem in _Data)
                        {
                            if (_DataItem.StatusId == CashoutStatus.Initialized)
                            {
                                _DataItem.StatusName = "Pending";
                            }
                            if (_Request.RedeemFrom == RedeemFrom.Loyalty)
                            {
                                _DataItem.Account = RedeemFrom.LoyaltyM;
                            }
                            else if (_Request.RedeemFrom == RedeemFrom.Deals)
                            {
                                _DataItem.Account = RedeemFrom.DealsM;
                            }
                            else if (_Request.RedeemFrom == RedeemFrom.BNPL)
                            {
                                _DataItem.Account = RedeemFrom.BNPLM;
                            }
                            _ListDownload.Add(new OCashOut.ListDownload
                            {
                                Date = _DataItem.StartDate.ToString("dd/MM/yyyy"),
                                Time = _DataItem.StartDate.ToString("hh:mm tt"),
                                StoreName = "",
                                TransactionType = _DataItem.TransactionType,
                                TransactionID = _DataItem.ReferenceNumber
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("RedeemWallets", _ListDownload, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRedeemWalletsDownload", _Exception);
            }
            #endregion
        }

    }

    internal class FrameworkCashoutOperation
    {
        OCashOutOperation.Overview? _OCashOutOperationOverview;
        HCoreContext? _HCoreContext;
        OCoreTransaction.Request? _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem>? _TransactionItems;
        ManageCoreTransaction? _ManageCoreTransaction;

        /// <summary>
        /// Description:Method to get list for cashouts in Console Panel.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal async Task<OResponse> GetCashout(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "StartDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = await _HCoreContext.TUCPayOut
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                .Select(x => new OCashOutOperation.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountMobileNumber = x.Account.MobileNumber,
                                                    AccountIconUrl = x.Account.IconStorage.Path,
                                                    AccountTypeCode = x.Account.AccountType.SystemName,
                                                    EmailAddress = x.Account.EmailAddress,
                                                    BankAccountName = x.Bank.Name,
                                                    BankName = x.Bank.BankName,
                                                    BankAccountNumber = x.Bank.AccountNumber,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    Amount = x.Amount,
                                                    Charge = x.Charge,
                                                    TotalAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    RedeemFromId = x.RedeemFrom,
                                                    StatusId = x.StatusId,
                                                    CreateDate = x.CreateDate,
                                                    SystemComment = x.SystemComment,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .CountAsync();
                        #endregion
                    }
                    #region Get Data
                    List<OCashOutOperation.List> _List = await _HCoreContext.TUCPayOut
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                .Select(x => new OCashOutOperation.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountMobileNumber = x.Account.MobileNumber,
                                                    AccountIconUrl = x.Account.IconStorage.Path,
                                                    AccountTypeCode = x.Account.AccountType.SystemName,
                                                    EmailAddress = x.Account.EmailAddress,

                                                    BankAccountName = x.Bank.Name,
                                                    BankName = x.Bank.BankName,
                                                    BankAccountNumber = x.Bank.AccountNumber,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    Amount = x.Amount,
                                                    Charge = x.Charge,
                                                    TotalAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    RedeemFromId = x.RedeemFrom,
                                                    StatusId = x.StatusId,
                                                    CreateDate = x.CreateDate,
                                                    SystemComment = x.SystemComment,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToListAsync();
                    #endregion
                    foreach (var DataItem in _List)
                    {
                        if (DataItem.StatusId == CashoutStatus.Initialized)
                        {
                            DataItem.StatusName = "Pending";
                        }
                        if (!string.IsNullOrEmpty(DataItem.AccountIconUrl))
                        {
                            DataItem.AccountIconUrl = _AppConfig.StorageUrl + DataItem.AccountIconUrl;
                        }
                        else
                        {
                            DataItem.AccountIconUrl = _AppConfig.Default_Icon;
                        }

                        if (DataItem.StatusId == CashoutStatus.Success)
                        {
                            DataItem.StatusName = "Approved";
                        }

                        if (DataItem.RedeemFromId == RedeemFrom.Loyalty)
                        {
                            DataItem.RedeemFromName = RedeemFrom.LoyaltyM;
                        }
                        else if (DataItem.RedeemFromId == RedeemFrom.Deals)
                        {
                            DataItem.RedeemFromName = RedeemFrom.DealsM;
                        }
                        else if (DataItem.RedeemFromId == RedeemFrom.BNPL)
                        {
                            DataItem.RedeemFromName = RedeemFrom.BNPLM;
                        }

                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCashout", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description:Method to get overview for cashouts in Console Panel.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCashoutOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "StartDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _OCashOutOperationOverview = new OCashOutOperation.Overview();
                    _OCashOutOperationOverview.Transactions = _HCoreContext.TUCPayOut
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                            .Select(x => new OCashOutOperation.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                AccountId = x.AccountId,
                                                AccountKey = x.Account.Guid,
                                                AccountDisplayName = x.Account.DisplayName,
                                                AccountMobileNumber = x.Account.MobileNumber,
                                                AccountIconUrl = x.Account.IconStorage.Path,
                                                AccountTypeCode = x.Account.AccountType.SystemName,
                                                EmailAddress = x.Account.EmailAddress,
                                                BankAccountName = x.Bank.Name,
                                                BankName = x.Bank.BankName,
                                                BankAccountNumber = x.Bank.AccountNumber,
                                                StartDate = x.StartDate,
                                                EndDate = x.EndDate,
                                                Amount = x.Amount,
                                                Charge = x.Charge,
                                                CommissionAmount = x.CommissionAmount,
                                                TotalAmount = x.TotalAmount,
                                                ReferenceNumber = x.ReferenceNumber,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,
                                                RedeemFromId = x.RedeemFrom
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    _OCashOutOperationOverview.Customers = _HCoreContext.TUCPayOut
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                           .Select(x => new OCashOutOperation.List
                                           {
                                               ReferenceId = x.Id,
                                               ReferenceKey = x.Guid,
                                               AccountId = x.AccountId,
                                               AccountKey = x.Account.Guid,
                                               AccountDisplayName = x.Account.DisplayName,
                                               AccountMobileNumber = x.Account.MobileNumber,
                                               AccountIconUrl = x.Account.IconStorage.Path,
                                               AccountTypeCode = x.Account.AccountType.SystemName,
                                               EmailAddress = x.Account.EmailAddress,
                                               BankAccountName = x.Bank.Name,
                                               BankName = x.Bank.BankName,
                                               BankAccountNumber = x.Bank.AccountNumber,
                                               StartDate = x.StartDate,
                                               EndDate = x.EndDate,
                                               Amount = x.Amount,
                                               Charge = x.Charge,
                                               CommissionAmount = x.CommissionAmount,
                                               TotalAmount = x.TotalAmount,
                                               ReferenceNumber = x.ReferenceNumber,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                               RedeemFromId = x.RedeemFrom
                                           })
                                          .Where(_Request.SearchCondition)
                                  .Select(x => x.AccountId).Distinct().Count();

                    _OCashOutOperationOverview.Amount = _HCoreContext.TUCPayOut
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                          .Select(x => new OCashOutOperation.List
                                          {
                                              ReferenceId = x.Id,
                                              ReferenceKey = x.Guid,
                                              AccountId = x.AccountId,
                                              AccountKey = x.Account.Guid,
                                              AccountDisplayName = x.Account.DisplayName,
                                              AccountMobileNumber = x.Account.MobileNumber,
                                              AccountTypeCode = x.Account.AccountType.SystemName,
                                              EmailAddress = x.Account.EmailAddress,
                                              AccountIconUrl = x.Account.IconStorage.Path,
                                              BankAccountName = x.Bank.Name,
                                              BankName = x.Bank.BankName,
                                              BankAccountNumber = x.Bank.AccountNumber,
                                              StartDate = x.StartDate,
                                              EndDate = x.EndDate,
                                              Amount = x.Amount,
                                              Charge = x.Charge,
                                              CommissionAmount = x.CommissionAmount,
                                              TotalAmount = x.TotalAmount,
                                              ReferenceNumber = x.ReferenceNumber,
                                              StatusCode = x.Status.SystemName,
                                              StatusName = x.Status.Name,
                                              RedeemFromId = x.RedeemFrom
                                          })
                                         .Where(_Request.SearchCondition)
                                 .Sum(x => x.Amount);

                    _OCashOutOperationOverview.Charge = _HCoreContext.TUCPayOut
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                          .Select(x => new OCashOutOperation.List
                                          {
                                              ReferenceId = x.Id,
                                              ReferenceKey = x.Guid,
                                              AccountId = x.AccountId,
                                              AccountKey = x.Account.Guid,
                                              AccountDisplayName = x.Account.DisplayName,
                                              AccountMobileNumber = x.Account.MobileNumber,
                                              EmailAddress = x.Account.EmailAddress,
                                              AccountIconUrl = x.Account.IconStorage.Path,
                                              AccountTypeCode = x.Account.AccountType.SystemName,
                                              BankAccountName = x.Bank.Name,
                                              BankName = x.Bank.BankName,
                                              BankAccountNumber = x.Bank.AccountNumber,
                                              StartDate = x.StartDate,
                                              EndDate = x.EndDate,
                                              Amount = x.Amount,
                                              Charge = x.Charge,
                                              CommissionAmount = x.CommissionAmount,
                                              TotalAmount = x.TotalAmount,
                                              ReferenceNumber = x.ReferenceNumber,
                                              StatusCode = x.Status.SystemName,
                                              StatusName = x.Status.Name,
                                              RedeemFromId = x.RedeemFrom
                                          })
                                         .Where(_Request.SearchCondition)
                                 .Sum(x => x.Charge);
                    _OCashOutOperationOverview.CommissionAmount = _HCoreContext.TUCPayOut
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                         .Select(x => new OCashOutOperation.List
                                         {
                                             ReferenceId = x.Id,
                                             ReferenceKey = x.Guid,
                                             AccountId = x.AccountId,
                                             AccountKey = x.Account.Guid,
                                             AccountDisplayName = x.Account.DisplayName,
                                             AccountMobileNumber = x.Account.MobileNumber,
                                             AccountTypeCode = x.Account.AccountType.SystemName,
                                             EmailAddress = x.Account.EmailAddress,
                                             AccountIconUrl = x.Account.IconStorage.Path,
                                             BankAccountName = x.Bank.Name,
                                             BankName = x.Bank.BankName,
                                             BankAccountNumber = x.Bank.AccountNumber,
                                             StartDate = x.StartDate,
                                             EndDate = x.EndDate,
                                             Amount = x.Amount,
                                             Charge = x.Charge,
                                             CommissionAmount = x.CommissionAmount,
                                             TotalAmount = x.TotalAmount,
                                             ReferenceNumber = x.ReferenceNumber,
                                             StatusCode = x.Status.SystemName,
                                             StatusName = x.Status.Name,
                                             RedeemFromId = x.RedeemFrom
                                         })
                                        .Where(_Request.SearchCondition)
                                .Sum(x => x.CommissionAmount);

                    _OCashOutOperationOverview.TotalAmount = _HCoreContext.TUCPayOut
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                        .Select(x => new OCashOutOperation.List
                                        {
                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,
                                            AccountId = x.AccountId,
                                            AccountKey = x.Account.Guid,
                                            AccountDisplayName = x.Account.DisplayName,
                                            AccountMobileNumber = x.Account.MobileNumber,
                                            AccountTypeCode = x.Account.AccountType.SystemName,
                                            EmailAddress = x.Account.EmailAddress,
                                            AccountIconUrl = x.Account.IconStorage.Path,
                                            BankAccountName = x.Bank.Name,
                                            BankName = x.Bank.BankName,
                                            BankAccountNumber = x.Bank.AccountNumber,
                                            StartDate = x.StartDate,
                                            EndDate = x.EndDate,
                                            Amount = x.Amount,
                                            Charge = x.Charge,
                                            CommissionAmount = x.CommissionAmount,
                                            TotalAmount = x.TotalAmount,
                                            ReferenceNumber = x.ReferenceNumber,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name,
                                            RedeemFromId = x.RedeemFrom
                                        })
                                       .Where(_Request.SearchCondition)
                               .Sum(x => x.TotalAmount);
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OCashOutOperationOverview, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCashoutOverview", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description:Method to get detail for cashouts in Console Panel.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal async Task<OResponse> GetCashout(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                else if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                else if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var _Details = await _HCoreContext.TUCPayOut
                            .Where(x => x.AccountId == _Request.AccountId
                            && x.Account.Guid == _Request.AccountKey
                            && x.Id == _Request.ReferenceId
                            && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OCashOutOperation.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountMobileNumber = x.Account.MobileNumber,
                                                    AccountIconUrl = x.Account.IconStorage.Path,
                                                    AccountTypeCode = x.Account.AccountType.SystemName,
                                                    EmailAddress = x.Account.EmailAddress,

                                                    SourceId = x.SourceId,
                                                    SourceCode = x.Source.Guid,
                                                    SourceName = x.Source.Name,
                                                    SourceSystemName = x.Source.SystemName,

                                                    BankAccountName = x.Bank.Name,
                                                    BankName = x.Bank.BankName,
                                                    BankAccountNumber = x.Bank.AccountNumber,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    Amount = x.Amount,
                                                    Charge = x.Charge,
                                                    TotalAmount = x.TotalAmount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    SystemComment = x.SystemComment,
                                                    UserComment = x.UserComment,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,

                                                    StatusId = x.StatusId,
                                                    RedeemFromId = x.RedeemFrom
                                                }).FirstOrDefaultAsync();
                        if (_Details != null)
                        {
                            if (!string.IsNullOrEmpty(_Details.AccountIconUrl))
                            {
                                _Details.AccountIconUrl = _AppConfig.StorageUrl + _Details.AccountIconUrl;
                            }
                            else
                            {
                                _Details.AccountIconUrl = _AppConfig.Default_Icon;
                            }
                            if (_Details.StatusId == CashoutStatus.Success)
                            {
                                _Details.StatusName = "Approved";
                            }

                            if (_Details.StatusId == CashoutStatus.Initialized)
                            {
                                _Details.StatusName = "Pending";
                            }

                            if (_Details.RedeemFromId == RedeemFrom.Loyalty)
                            {
                                _Details.RedeemFromName = RedeemFrom.LoyaltyM;
                            }
                            else if (_Details.RedeemFromId == RedeemFrom.Deals)
                            {
                                _Details.RedeemFromName = RedeemFrom.DealsM;
                            }
                            else if (_Details.RedeemFromId == RedeemFrom.BNPL)
                            {
                                _Details.RedeemFromName = RedeemFrom.BNPLM;
                            }
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetCashout", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description:Method to update status for cashout requests from Console Panel.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal async Task<OResponse> UpdateCashoutStatus(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                else if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                else if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                else if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                else
                {
                    long StatusId = 0;
                    if (!string.IsNullOrEmpty(_Request.StatusCode))
                    {
                        StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                        if (StatusId < 1)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                        }
                    }
                    if (StatusId == CashoutStatus.Processing || StatusId == CashoutStatus.Rejected)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var _Details = await _HCoreContext.TUCPayOut
                                .Where(x => x.AccountId == _Request.AccountId
                                && x.Account.Guid == _Request.AccountKey
                                && x.Id == _Request.ReferenceId
                                && x.Guid == _Request.ReferenceKey)
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                .FirstOrDefaultAsync();
                            if (_Details != null)
                            {
                                if (_Details.StatusId == CashoutStatus.Initialized)
                                {
                                    if (_Details.StatusId == CashoutStatus.Rejected && string.IsNullOrEmpty(_Request.Comment))
                                    {
                                        await _HCoreContext.DisposeAsync();
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1289, TUCCoreResource.CA1289M);
                                    }
                                    else
                                    {
                                        string? EmailId = await _HCoreContext.HCUAccount.Where(x => x.Id == _Details.AccountId).Select(x => x.EmailAddress).FirstOrDefaultAsync();
                                        string? UserDisplayName = await _HCoreContext.HCUAccount.Where(x => x.Id == _Details.AccountId).Select(x => x.DisplayName).FirstOrDefaultAsync();
                                        string? senderName = await _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.DisplayName).FirstOrDefaultAsync();
                                        if (StatusId == CashoutStatus.Processing)
                                        {
                                            string? BankCode = await _HCoreContext.HCUAccountBank.Where(x => x.Id == _Details.BankId).Select(x => x.ReferenceCode).FirstOrDefaultAsync();
                                            var _TransferResponse = PaystackTransfer.CreateTransfer(_AppConfig.PaystackPrivateKey, _Details.ReferenceNumber, BankCode, Math.Round(_Details.Amount), "TUC Cashout");
                                            if (_TransferResponse != null)
                                            {
                                                using (_HCoreContext = new HCoreContext())
                                                {
                                                    var _DetailsUpdate = await _HCoreContext.TUCPayOut.Where(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey && x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                                                    if (_DetailsUpdate != null)
                                                    {
                                                        _DetailsUpdate.ExtReferenceNumber = _TransferResponse.transfer_code;
                                                        _DetailsUpdate.Request = _TransferResponse.request;
                                                        _DetailsUpdate.Response = _TransferResponse.response;
                                                        _DetailsUpdate.ExtReferenceId = _TransferResponse.id;
                                                        _DetailsUpdate.StatusId = CashoutStatus.Success;
                                                        _DetailsUpdate.SystemComment = _Request.Comment;
                                                        _DetailsUpdate.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                        _DetailsUpdate.ModifyById = _Request.UserReference.AccountId;
                                                        await _HCoreContext.SaveChangesAsync();
                                                    }
                                                }
                                                var _ResponseLog = new
                                                {
                                                    Amount = "₦ " + _Details.Amount.ToString(),
                                                    Status = "Approved",
                                                    UserDisplayName = UserDisplayName
                                                };
                                                HCoreHelper.BroadCastEmail("d-6a54b5bbcdaf4ac6a84f371a09fbc1d2", UserDisplayName, EmailId, _ResponseLog, null);
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1286, TUCCoreResource.CA1286M);
                                            }
                                            else
                                            {
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1287, TUCCoreResource.CA1287M);
                                            }
                                        }
                                        else if (StatusId == CashoutStatus.Rejected)
                                        {
                                            var CustomerDetails = await _HCoreContext.HCUAccount.Where(x => x.Id == _Details.AccountId).FirstOrDefaultAsync();
                                            _Details.StatusId = CashoutStatus.Rejected;
                                            _Details.SystemComment = _Request.Comment;
                                            _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            _Details.ModifyById = _Request.UserReference.AccountId;
                                            await _HCoreContext.SaveChangesAsync();


                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.CustomerId = _Details.AccountId;
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateDateString();
                                            _CoreTransactionRequest.ParentId = SystemAccounts.TUCVas;
                                            _CoreTransactionRequest.InvoiceAmount = _Details.TotalAmount;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = _Details.TotalAmount;
                                            _CoreTransactionRequest.ReferenceNumber = _CoreTransactionRequest.GroupKey;
                                            //_CoreTransactionRequest.InvoiceNumber = ExtInvNo;
                                            _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                            _CoreTransactionRequest.ReferenceAmount = _Details.TotalAmount;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            if (CustomerDetails.AccountTypeId == UserAccountType.Merchant)
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = SystemAccounts.TUCVas,
                                                    ModeId = TransactionMode.Debit,
                                                    TypeId = TransactionType.TUCCashOut.CashOutRefund,
                                                    SourceId = TransactionSource.CashOut,
                                                    Amount = _Details.TotalAmount,
                                                    Charge = 0,
                                                    Comission = 0,
                                                    TotalAmount = _Details.TotalAmount,
                                                    Comment = _Request.Comment,
                                                });
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _Details.AccountId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionType.TUCCashOut.CashOutRefund,
                                                    SourceId = TransactionSource.Merchant,
                                                    Amount = _Details.TotalAmount,
                                                    Charge = 0,
                                                    Comission = 0,
                                                    TotalAmount = _Details.TotalAmount,
                                                    Comment = _Request.Comment,
                                                });
                                            }
                                            else
                                            {

                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = SystemAccounts.TUCVas,
                                                    ModeId = TransactionMode.Debit,
                                                    TypeId = TransactionType.TUCCashOut.CashOutRefund,
                                                    SourceId = TransactionSource.CashOut,
                                                    Amount = _Details.TotalAmount,
                                                    Charge = 0,
                                                    Comission = 0,
                                                    TotalAmount = _Details.TotalAmount,
                                                    Comment = _Request.Comment,
                                                });
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _Request.AccountId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionType.TUCCashOut.CashOutRefund,
                                                    SourceId = TransactionSource.TUC,
                                                    Amount = _Details.TotalAmount,
                                                    Charge = 0,
                                                    Comission = 0,
                                                    TotalAmount = _Details.TotalAmount,
                                                    Comment = _Request.Comment,
                                                });
                                            }

                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            var _ResponseLog = new
                                            {
                                                Amount = "₦ " + _Details.Amount.ToString(),
                                                Status = "Rejected",
                                                UserDisplayName = UserDisplayName
                                            };
                                            HCoreHelper.BroadCastEmail("d-6a54b5bbcdaf4ac6a84f371a09fbc1d2", UserDisplayName, EmailId, _ResponseLog, null);
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1291, TUCCoreResource.CA1291M);
                                        }
                                        else
                                        {
                                            await _HCoreContext.DisposeAsync();
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1290, TUCCoreResource.CA1290M);
                                        }
                                    }
                                }
                                else
                                {
                                    await _HCoreContext.DisposeAsync();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1288, TUCCoreResource.CA1288M);
                                }
                            }
                            else
                            {
                                await _HCoreContext.DisposeAsync();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                            }
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1293, TUCCoreResource.CA1293M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "UpdateCashoutStatus", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Author: Priya Chavadiya
        /// Description:Method to get pending cashout counts.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetPendingCashoutCounts(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                using (_HCoreContext = new HCoreContext())
                {
                    _OCashOutOperationOverview = new OCashOutOperation.Overview();
                    _OCashOutOperationOverview.Total = _HCoreContext.TUCPayOut
                                                .Count(x => x.Account.CountryId == _Request.UserReference.CountryId
                                                && x.StatusId == CashoutStatus.Initialized);
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OCashOutOperationOverview, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCashoutOverview", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        internal async Task<OResponse> GetCashoutsOverview(OReference _Request)
        {
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "GC0001", "Account id required");
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "GC0002", "Account key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    OCashOutOperation.CashOutOverview? _CashoutOverview = new OCashOutOperation.CashOutOverview();

                    _CashoutOverview.TotalApprovedCashouts = await _HCoreContext.TUCPayOut
                                                             .Where(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey
                                                             && x.StatusId == CashoutStatus.Success)
                                                             .CountAsync();

                    _CashoutOverview.ApprovedAmount = await _HCoreContext.TUCPayOut
                                                      .Where(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey
                                                      && x.StatusId == CashoutStatus.Success)
                                                      .SumAsync(x => x.Amount);

                    _CashoutOverview.TotalRejectedCashouts = await _HCoreContext.TUCPayOut
                                                             .Where(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey
                                                             && x.StatusId == CashoutStatus.Rejected)
                                                             .CountAsync();

                    _CashoutOverview.RejectedAmount = await _HCoreContext.TUCPayOut
                                                      .Where(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey
                                                      && x.StatusId == CashoutStatus.Rejected)
                                                      .SumAsync(x => x.Amount);

                    _CashoutOverview.TotalPendingCashouts = await _HCoreContext.TUCPayOut
                                                            .Where(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey
                                                            && (x.StatusId == CashoutStatus.Initialized || x.StatusId == CashoutStatus.Processing))
                                                            .CountAsync();

                    _CashoutOverview.PendingAmount = await _HCoreContext.TUCPayOut
                                                     .Where(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey
                                                     && (x.StatusId == CashoutStatus.Initialized || x.StatusId == CashoutStatus.Processing))
                                                     .SumAsync(x => x.Amount);

                    await _HCoreContext.DisposeAsync();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CashoutOverview, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetCashoutsOverview", _Exception, _Request.UserReference, "HC0500", "Unable to load overview details. Please try after some time.");
            }
        }
    }

    internal class ActorGetRewardWalletsDownload : ReceiveActor
    {
        public ActorGetRewardWalletsDownload()
        {
            FrameworkCashout FrameworkCashout;
            Receive<OList.Request>(_Request =>
            {
                FrameworkCashout = new FrameworkCashout();
                FrameworkCashout.GetRewardWalletsDownload(_Request);
            });
        }
    }

    internal class ActorGetRedeemWalletsDownload : ReceiveActor
    {
        public ActorGetRedeemWalletsDownload()
        {
            FrameworkCashout FrameworkCashout;
            Receive<OList.Request>(_Request =>
            {
                FrameworkCashout = new FrameworkCashout();
                FrameworkCashout.GetRedeemWalletsDownload(_Request);
            });
        }

    }
}
