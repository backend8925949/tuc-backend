//==================================================================================
// FileName: FrameworkCityManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to city and city area
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Core.Framework.Operations
{
    public class FrameworkCityManager
    {
        HCoreContext _HCoreContext;
        HCCoreCountryStateCity _HCCoreCountryStateCity;

        /// <summary>
        /// Description: Method defined to save city
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveCity(OCityManager.Save.Request _Request)
        {
            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.StateKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA1492, TUCCoreResource.CAA1492M);
                }
                if (_Request.StateId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA1493, TUCCoreResource.CAA1493M);
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA1494, TUCCoreResource.CAA1494M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    bool _IsExists = _HCoreContext.HCCoreCountryState.Any(x => x.Id == _Request.StateId && x.Guid == _Request.StateKey);
                    if (_IsExists)
                    {
                        bool _Exists = _HCoreContext.HCCoreCountryStateCity.Any(x => x.SystemName == SystemName && x.StateId == _Request.StateId);
                        if (_Exists)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA1496, TUCCoreResource.CAA1496M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA1495, TUCCoreResource.CAA1495M);
                    }

                    _HCCoreCountryStateCity = new HCCoreCountryStateCity();
                    _HCCoreCountryStateCity.Guid = HCoreHelper.GenerateGuid();
                    _HCCoreCountryStateCity.StateId = _Request.StateId;
                    _HCCoreCountryStateCity.Name = _Request.Name;
                    _HCCoreCountryStateCity.SystemName = SystemName;
                    _HCCoreCountryStateCity.Latitude = _Request.Latitude;
                    _HCCoreCountryStateCity.Longitude = _Request.Longitude;
                    _HCCoreCountryStateCity.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCCoreCountryStateCity.CreatedById = _Request.UserReference.AccountId;
                    _HCCoreCountryStateCity.StatusId = StatusId;
                    _HCoreContext.HCCoreCountryStateCity.Add(_HCCoreCountryStateCity);
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();

                    var _Response = new
                    {
                        ReferenceId = _HCCoreCountryStateCity.Id,
                        ReferenceKey = _HCCoreCountryStateCity.Guid
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA1497, TUCCoreResource.CAA1497M);
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SaveCity", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to update city
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateCity(OCityManager.Update _Request)
        {
            #region
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }


                using (_HCoreContext = new HCoreContext())
                {
                    var _CityDetails = _HCoreContext.HCCoreCountryStateCity.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_CityDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && _CityDetails.Name != _Request.Name)
                        {
                            string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                            bool _IsExist = _HCoreContext.HCCoreCountryStateCity.Any(x => x.Name == _Request.Name && x.Id != _CityDetails.Id && _CityDetails.StateId == _Request.StateId);
                            if (_IsExist)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA1496, TUCCoreResource.CAA1496M);
                            }

                            _CityDetails.Name = _Request.Name;
                            _CityDetails.SystemName = SystemName;
                        }
                        if (_Request.Longitude != 0)
                        {
                            _CityDetails.Longitude = _Request.Longitude;
                        }
                        if (_Request.Latitude != 0)
                        {
                            _CityDetails.Latitude = _Request.Latitude;
                        }
                        if (StatusId > 0)
                        {
                            _CityDetails.StatusId = StatusId;
                        }

                        _CityDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _CityDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();

                        var _Response = new
                        {
                            ReferenceId = _CityDetails.Id,
                            ReferenceKey = _CityDetails.Guid
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA1498, TUCCoreResource.CAA1498M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateCity", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to delete city
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteCity(OReference _Request)
        {
            #region
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _CityDetails = _HCoreContext.HCCoreCountryStateCity
                        .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .FirstOrDefault();
                    if (_CityDetails != null)
                    {
                        var _CityAreaDetails = _HCoreContext.HCCoreCountryStateCityArea
                            .Any(x => x.CityId == _Request.ReferenceId && x.City.Guid == _Request.ReferenceKey);
                        if (!_CityAreaDetails)
                        {
                            _HCoreContext.HCCoreCountryStateCity.Remove(_CityDetails);
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CAA1499, TUCCoreResource.CAA1499M);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14124, TUCCoreResource.CAA14124M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DeleteCity", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to get city details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCity(OReference _Request)
        {
            #region
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCCoreCountryStateCity.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .Select(x => new OCityManager.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            StateId = x.StateId,
                            StateKey = x.State.Guid,
                            StateName = x.State.Name,
                            Name = x.Name,
                            SystemName = x.SystemName,
                            Latitude = x.Latitude,
                            Longitude = x.Longitude,

                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyById,
                            ModifyByKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                        }).FirstOrDefault();

                    if (_Details != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCity", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to get city list
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCity(OList.Request _Request)
        {
            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCCoreCountryStateCity
                            .Where(x => x.StateId == _Request.ReferenceId
                             && x.State.Guid == _Request.ReferenceKey)
                            .Select(x => new OCityManager.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                StateId = x.StateId,
                                StateKey = x.State.Guid,
                                StateName = x.State.Name,
                                Name = x.Name,
                                SystemName = x.SystemName,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyById = x.ModifyById,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                            })
                            .Where(_Request.SearchCondition)
                            .Count();

                    }
                    List<OCityManager.List> Data = _HCoreContext.HCCoreCountryStateCity
                            .Where(x => x.StateId == _Request.ReferenceId
                             && x.State.Guid == _Request.ReferenceKey)
                                                .Select(x => new OCityManager.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    StateId = x.StateId,
                                                    StateKey = x.State.Guid,
                                                    StateName = x.State.Name,
                                                    Name = x.Name,
                                                    SystemName = x.SystemName,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCities", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get city list
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCities(OList.Request _Request)
        {


            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCCoreCountryStateCity
                                                        .Where(x => x.StateId == _Request.ReferenceId
                                                        && x.State.Guid == _Request.ReferenceKey)
                            .Select(x => new OCityManager.ListLite
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                Name = x.Name,
                                SystemName = x.SystemName,
                            })
                                                 .Where(_Request.SearchCondition)
                                                 .Count();

                    }
                    List<OCityManager.ListLite> Data = _HCoreContext.HCCoreCountryStateCity
                                                        .Where(x => x.StateId == _Request.ReferenceId
                                                        && x.State.Guid == _Request.ReferenceKey)
                           .Select(x => new OCityManager.ListLite
                           {
                               ReferenceId = x.Id,
                               ReferenceKey = x.Guid,
                               Name = x.Name,
                               SystemName = x.SystemName,
                           })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCities", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }


            #endregion
        }
    }
}
