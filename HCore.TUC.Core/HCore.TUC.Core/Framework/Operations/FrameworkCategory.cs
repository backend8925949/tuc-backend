//==================================================================================
// FileName: FrameworkCategory.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to category and subcategory
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;
using System.Collections.Generic;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;

namespace HCore.TUC.Core.Framework.Operations
{
    public class FrameworkCategory
    {
        HCoreContext _HCoreContext;
        TUCCategory _TUCCategory;

        /// <summary>
        /// Description: Method defined to save category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveCategory(OCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1439, TUCCoreResource.CA1439M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                int? StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    bool CheckItem = _HCoreContext.TUCCategory.Any(x => x.ParentCategoryId == null && x.Name == _Request.Name);
                    if (!CheckItem)
                    {
                        _TUCCategory = new TUCCategory();
                        _TUCCategory.Guid = HCoreHelper.GenerateGuid();
                        _TUCCategory.Name = _Request.Name;
                        _TUCCategory.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                        _TUCCategory.Description = _Request.Description;
                        _TUCCategory.CreateDate = HCoreHelper.GetGMTDateTime();
                        _TUCCategory.CreatedById = _Request.UserReference.AccountId;
                        _TUCCategory.StatusId = (int)StatusId;
                        _HCoreContext.TUCCategory.Add(_TUCCategory);
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            ReferenceId = _TUCCategory.Id,
                            ReferenceKey = _TUCCategory.Guid,
                        };
                        long? IconStorageId = null;
                        long? PosterStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                        }
                        if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                        {
                            PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, null, _Request.UserReference);
                        }
                        if (IconStorageId != null || PosterStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var PrDetails = _HCoreContext.TUCCategory.Where(x => x.Guid == _TUCCategory.Guid).FirstOrDefault();
                                if (PrDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        PrDetails.IconStorageId = IconStorageId;
                                    }
                                    if (PosterStorageId != null)
                                    {
                                        PrDetails.PosterStorageId = PosterStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        if (_Request.Subcategories != null && _Request.Subcategories.Count > 0)
                        {
                            SaveSubCategory(_Request, _Response.ReferenceId);
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1440, TUCCoreResource.CA1440M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1441, TUCCoreResource.CA1441M);
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_CategoryRequest"></param>
        /// <param name="CategoryId"></param>
        private void SaveSubCategory(OCategory.Request _CategoryRequest, int CategoryId)
        {
            #region Manage Exception
            try
            {
                foreach (var _Request in _CategoryRequest.Subcategories)
                {
                    if (string.IsNullOrEmpty(_Request.Name))
                    {
                        HCoreHelper.SendResponse(_CategoryRequest.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1439, TUCCoreResource.CA1439M);
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        _TUCCategory = new TUCCategory();
                        _TUCCategory.Guid = HCoreHelper.GenerateGuid();
                        _TUCCategory.ParentCategoryId = CategoryId;
                        _TUCCategory.Name = _Request.Name;
                        _TUCCategory.Description = _Request.Description;
                        _TUCCategory.CreateDate = HCoreHelper.GetGMTDateTime();
                        _TUCCategory.CreatedById = _CategoryRequest.UserReference.AccountId;
                        _TUCCategory.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.TUCCategory.Add(_TUCCategory);
                        _HCoreContext.SaveChanges();
                        long? IconStorageId = null;
                        long? PosterStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _CategoryRequest.UserReference);
                        }
                        if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                        {
                            PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, null, _CategoryRequest.UserReference);
                        }
                        if (IconStorageId != null || PosterStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var PrDetails = _HCoreContext.TUCCategory.Where(x => x.Id == _TUCCategory.Id).FirstOrDefault();
                                if (PrDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        PrDetails.IconStorageId = IconStorageId;
                                    }
                                    if (PosterStorageId != null)
                                    {
                                        PrDetails.PosterStorageId = PosterStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SaveSubCategoryBulk", _Exception, _CategoryRequest.UserReference);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to update category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateCategory(OCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    int StatusId = 0;
                    if (!string.IsNullOrEmpty(_Request.StatusCode))
                    {
                        StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                        if (StatusId == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                        }
                    }
                    var ProductCategoryDetails = _HCoreContext.TUCCategory.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (ProductCategoryDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && ProductCategoryDetails.Name != _Request.Name)
                        {
                            bool NameCheck = _HCoreContext.TUCCategory.Any(x => x.ParentCategoryId == null && x.Name == _Request.Name);
                            if (NameCheck)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1456, TUCCoreResource.CA1456M);
                            }
                            ProductCategoryDetails.Name = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.Description) && ProductCategoryDetails.Description != _Request.Description)
                        {
                            ProductCategoryDetails.Description = _Request.Description;
                        }
                        ProductCategoryDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        ProductCategoryDetails.ModifyById = _Request.UserReference.AccountId;
                        if (StatusId != 0 && ProductCategoryDetails.StatusId != StatusId)
                        {
                            ProductCategoryDetails.StatusId = StatusId;
                        }
                        _HCoreContext.SaveChanges();
                        long? IconStorageId = null;
                        long? PosterStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, ProductCategoryDetails.IconStorageId, _Request.UserReference);
                        }
                        if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                        {
                            PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, ProductCategoryDetails.PosterStorageId, _Request.UserReference);
                        }
                        if (IconStorageId != null || PosterStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var PrDetails = _HCoreContext.TUCCategory.Where(x => x.Guid == ProductCategoryDetails.Guid).FirstOrDefault();
                                if (PrDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        PrDetails.IconStorageId = IconStorageId;
                                    }
                                    if (PosterStorageId != null)
                                    {
                                        PrDetails.PosterStorageId = PosterStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1445, TUCCoreResource.CA1445M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateCategory", _Exception, _Request.UserReference);
                #endregion
            }

        }

        /// <summary>
        /// Description: Method defined to delete category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteCategory(OCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.TUCCategory.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        bool AccountCategoryCheck = _HCoreContext.HCUAccount.Any(x => x.PrimaryCategoryId == Details.Id);
                        if (AccountCategoryCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAT001, TUCCoreResource.CAT001M);
                        }
                        bool MerchantCategoryCheck = _HCoreContext.TUCMerchantCategory.Any(x => x.RootCategoryId == Details.Id);
                        if (MerchantCategoryCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAT002, TUCCoreResource.CAT002M);
                        }
                        bool DealCategoryCheck = _HCoreContext.MDCategory.Any(x => x.RootCategoryId == Details.Id);
                        if (DealCategoryCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAT003, TUCCoreResource.CAT003M);
                        }
                        bool SubCategoryCheck = _HCoreContext.TUCCategory.Any(x => x.ParentCategoryId == Details.Id);
                        if (SubCategoryCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAT004, TUCCoreResource.CAT004M);
                        }
                        _HCoreContext.TUCCategory.Remove(Details);
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        var _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1446, TUCCoreResource.CA1446M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get category details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCategory(OCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OCategory.Details _Details = (from x in _HCoreContext.TUCCategory
                                                  where x.Guid == _Request.ReferenceKey
                                                  && x.Id == _Request.ReferenceId
                                                  && x.ParentCategoryId == null
                                                  select new OCategory.Details
                                                  {

                                                      ReferenceId = x.Id,
                                                      ReferenceKey = x.Guid,

                                                      Name = x.Name,
                                                      Description = x.Description,

                                                      IconUrl = x.IconStorage.Path,
                                                      PosterUrl = x.PosterStorage.Path,

                                                      CreateDate = x.CreateDate,
                                                      CreatedByKey = x.CreatedBy.Guid,
                                                      CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                      ModifyDate = x.ModifyDate,
                                                      ModifyByKey = x.ModifyBy.Guid,
                                                      ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                      StatusId = x.StatusId,
                                                      StatusCode = x.Status.SystemName,
                                                      StatusName = x.Status.Name
                                                  })
                                              .FirstOrDefault();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    if (_Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Details.IconUrl))
                        {
                            _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                        }
                        else
                        {
                            _Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.PosterUrl))
                        {
                            _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                        }
                        else
                        {
                            _Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCategory", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get list of  categories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCategory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.TUCCategory
                                                 where x.ParentCategoryId == null
                                                 select new OCategory.Details
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     Name = x.Name,
                                                     Description = x.Description,

                                                     IconUrl = x.IconStorage.Path,
                                                     PosterUrl = x.PosterStorage.Path,

                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByKey = x.ModifyBy.Guid,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                                                .Where(_Request.SearchCondition)
                                                        .Count();
                    }
                    #endregion
                    #region Get Data
                    List<OCategory.Details> Data = (from x in _HCoreContext.TUCCategory
                                                    where x.ParentCategoryId == null
                                                    select new OCategory.Details
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        Name = x.Name,
                                                        Description = x.Description,

                                                        IconUrl = x.IconStorage.Path,
                                                        PosterUrl = x.PosterStorage.Path,

                                                        CreateDate = x.CreateDate,
                                                        CreatedByKey = x.CreatedBy.Guid,
                                                        CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                        ModifyDate = x.ModifyDate,
                                                        ModifyByKey = x.ModifyBy.Guid,
                                                        ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name
                                                    })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var _Details in Data)
                    {
                        if (!string.IsNullOrEmpty(_Details.IconUrl))
                        {
                            _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                        }
                        else
                        {
                            _Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.PosterUrl))
                        {
                            _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                        }
                        else
                        {
                            _Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCategory", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to save subcategory
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveSubCategory(OCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.CategoryKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1447, TUCCoreResource.CA1447M);
                }
                if (_Request.CategoryId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1448, TUCCoreResource.CA1448M);
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1449, TUCCoreResource.CA1449M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }

                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    bool CategoryCheck = _HCoreContext.TUCCategory.Any(x => x.Id == _Request.CategoryId && x.Guid == _Request.CategoryKey);
                    if (!CategoryCheck)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1451, TUCCoreResource.CA1451M);
                    }

                    bool ProductCategoryDetails = _HCoreContext.TUCCategory
                                                        .Any(x =>
                                                        x.ParentCategoryId == _Request.CategoryId
                                                        && x.Name == _Request.Name);
                    if (!ProductCategoryDetails)
                    {
                        _TUCCategory = new TUCCategory();
                        _TUCCategory.Guid = HCoreHelper.GenerateGuid();
                        _TUCCategory.ParentCategoryId = _Request.CategoryId;
                        _TUCCategory.Name = _Request.Name;
                        _TUCCategory.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                        _TUCCategory.Description = _Request.Description;
                        _TUCCategory.CreateDate = HCoreHelper.GetGMTDateTime();
                        _TUCCategory.CreatedById = _Request.UserReference.AccountId;
                        _TUCCategory.StatusId = (int)StatusId;
                        _HCoreContext.TUCCategory.Add(_TUCCategory);
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            ReferenceId = _TUCCategory.Id,
                            ReferenceKey = _TUCCategory.Guid,
                        };
                        long? IconStorageId = null;
                        long? PosterStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                        }
                        if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                        {
                            PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, null, _Request.UserReference);
                        }
                        if (IconStorageId != null || PosterStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var PrDetails = _HCoreContext.TUCCategory.Where(x => x.Guid == _TUCCategory.Guid).FirstOrDefault();
                                if (PrDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        PrDetails.IconStorageId = IconStorageId;
                                    }
                                    if (PosterStorageId != null)
                                    {
                                        PrDetails.PosterStorageId = PosterStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1486, TUCCoreResource.CA1486M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1450, TUCCoreResource.CA1450M);
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveSubCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to save multiple subcategories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveSubCategories(OCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.CategoryKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1447, TUCCoreResource.CA1447M);
                }
                if (_Request.CategoryId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1448, TUCCoreResource.CA1448M);
                }
                if (_Request.Subcategories == null || _Request.Subcategories.Count == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA14411, TUCCoreResource.CA14411M);
                }

                //int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                //if (StatusId == null)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDINSTATUS", ResponseCode.HCDINSTATUS);
                //}
                using (_HCoreContext = new HCoreContext())
                {
                    bool CategoryCheck = _HCoreContext.TUCCategory.Any(x => x.Id == _Request.CategoryId && x.Guid == _Request.CategoryKey);
                    if (!CategoryCheck)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1451, TUCCoreResource.CA1451M);
                    }
                    _HCoreContext.Dispose();
                    foreach (var Subcategory in _Request.Subcategories)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            bool ProductCategoryDetails = _HCoreContext.TUCCategory
                                                      .Any(x =>
                                                      x.ParentCategoryId == _Request.CategoryId
                                                      && x.Name == Subcategory.Name);
                            if (!ProductCategoryDetails)
                            {
                                _TUCCategory = new TUCCategory();
                                _TUCCategory.Guid = HCoreHelper.GenerateGuid();
                                _TUCCategory.ParentCategoryId = _Request.CategoryId;
                                _TUCCategory.Name = Subcategory.Name;
                                _TUCCategory.SystemName = HCoreHelper.GenerateSystemName(Subcategory.Name);
                                _TUCCategory.Description = Subcategory.Description;
                                _TUCCategory.CreateDate = HCoreHelper.GetGMTDateTime();
                                _TUCCategory.CreatedById = _Request.UserReference.AccountId;
                                _TUCCategory.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.TUCCategory.Add(_TUCCategory);
                                _HCoreContext.SaveChanges();
                                var _Response = new
                                {
                                    ReferenceId = _TUCCategory.Id,
                                    ReferenceKey = _TUCCategory.Guid,
                                };
                                long? IconStorageId = null;
                                long? PosterStorageId = null;
                                if (Subcategory.IconContent != null && !string.IsNullOrEmpty(Subcategory.IconContent.Content))
                                {
                                    IconStorageId = HCoreHelper.SaveStorage(Subcategory.IconContent.Name, Subcategory.IconContent.Extension, Subcategory.IconContent.Content, null, _Request.UserReference);
                                }
                                if (Subcategory.PosterContent != null && !string.IsNullOrEmpty(Subcategory.PosterContent.Content))
                                {
                                    PosterStorageId = HCoreHelper.SaveStorage(Subcategory.PosterContent.Name, Subcategory.PosterContent.Extension, Subcategory.PosterContent.Content, null, _Request.UserReference);
                                }
                                if (IconStorageId != null || PosterStorageId != null)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        var PrDetails = _HCoreContext.TUCCategory.Where(x => x.Guid == _TUCCategory.Guid).FirstOrDefault();
                                        if (PrDetails != null)
                                        {
                                            if (IconStorageId != null)
                                            {
                                                PrDetails.IconStorageId = IconStorageId;
                                            }
                                            if (PosterStorageId != null)
                                            {
                                                PrDetails.PosterStorageId = PosterStorageId;
                                            }
                                            _HCoreContext.SaveChanges();
                                        }
                                    }
                                }
                                //#region Send Response
                                //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0172", ResponseCode.HCP0172);
                                //#endregion
                            }
                            //else
                            //{
                            //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1450, TUCCoreResource.CA1450M);
                            //}
                        }
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA14422, TUCCoreResource.CA14422M);
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveSubCategories", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to update subcategory
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateSubCategory(OCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    int StatusId = 0;
                    if (!string.IsNullOrEmpty(_Request.StatusCode))
                    {
                        StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                        if (StatusId == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                        }
                    }
                    var ProductCategoryDetails = _HCoreContext.TUCCategory
                                                                 .Where(x => x.Id == _Request.ReferenceId
                                                                 && x.Guid == _Request.ReferenceKey
                                                                 && x.ParentCategoryId != null).FirstOrDefault();
                    if (ProductCategoryDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && ProductCategoryDetails.Name != _Request.Name)
                        {
                            string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                            bool NameCheck = _HCoreContext.TUCCategory
                                                        .Any(x =>
                                                        x.ParentCategoryId == ProductCategoryDetails.ParentCategoryId
                                                        && x.Name == _Request.Name);
                            if (NameCheck)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1453, TUCCoreResource.CA1453M);
                            }
                            ProductCategoryDetails.Name = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.Description) && ProductCategoryDetails.Description != _Request.Description)
                        {
                            ProductCategoryDetails.Description = _Request.Description;
                        }

                        ProductCategoryDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        ProductCategoryDetails.ModifyById = _Request.UserReference.AccountId;
                        if (StatusId != 0 && ProductCategoryDetails.StatusId != StatusId)
                        {
                            ProductCategoryDetails.StatusId = StatusId;
                        }
                        _HCoreContext.SaveChanges();
                        long? IconStorageId = null;
                        long? PosterStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, ProductCategoryDetails.IconStorageId, _Request.UserReference);
                        }
                        if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                        {
                            PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, ProductCategoryDetails.PosterStorageId, _Request.UserReference);
                        }
                        if (IconStorageId != null || PosterStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var PrDetails = _HCoreContext.TUCCategory.Where(x => x.Guid == ProductCategoryDetails.Guid).FirstOrDefault();
                                if (PrDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        PrDetails.IconStorageId = IconStorageId;
                                    }
                                    if (PosterStorageId != null)
                                    {
                                        PrDetails.PosterStorageId = PosterStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1454, TUCCoreResource.CA1454M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateSubCategory", _Exception, _Request.UserReference);
                #endregion
            }

        }

        /// <summary>
        /// Description: Method defined to delete category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteSubCategory(OCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    TUCCategory Details = _HCoreContext.TUCCategory.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.ParentCategoryId != null).FirstOrDefault();
                    if (Details != null)
                    {
                        //bool ProductCheck = _HCoreContext.SCProduct.Any(x => x.CategoryId == Details.Id);
                        //if (ProductCheck)
                        //{
                        //    _HCoreContext.Dispose();
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.HCC149, TUCCoreResource.HCC149M);
                        //}
                        _HCoreContext.TUCCategory.Remove(Details);
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1455, TUCCoreResource.CA1455M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteSubCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get details of subcategory
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetSubCategory(OCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFKEY);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OCategory.Details _Details = (from x in _HCoreContext.TUCCategory
                                                  where x.Guid == _Request.ReferenceKey
                                                  && x.Id == _Request.ReferenceId
                                                  && x.ParentCategory != null
                                                  select new OCategory.Details
                                                  {

                                                      ReferenceId = x.Id,
                                                      ReferenceKey = x.Guid,

                                                      ParentCategoryId = x.ParentCategoryId,
                                                      ParentCategoryKey = x.ParentCategory.Guid,
                                                      ParentCategoryName = x.ParentCategory.Name,

                                                      Name = x.Name,
                                                      Description = x.Description,

                                                      IconUrl = x.IconStorage.Path,
                                                      PosterUrl = x.PosterStorage.Path,

                                                      CreateDate = x.CreateDate,
                                                      CreatedByKey = x.CreatedBy.Guid,
                                                      CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                      ModifyDate = x.ModifyDate,
                                                      ModifyByKey = x.ModifyBy.Guid,
                                                      ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                      StatusId = x.StatusId,
                                                      StatusCode = x.Status.SystemName,
                                                      StatusName = x.Status.Name
                                                  })
                                              .FirstOrDefault();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    if (_Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Details.IconUrl))
                        {
                            _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                        }
                        else
                        {
                            _Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.PosterUrl))
                        {
                            _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                        }
                        else
                        {
                            _Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetSubCategory", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get list of  subcategories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetSubCategory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    if (!string.IsNullOrEmpty(_Request.ReferenceKey) && _Request.ReferenceId != 0)
                    {
                        #region Total Records
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = (from x in _HCoreContext.TUCCategory
                                                     where x.ParentCategory.Guid == _Request.ReferenceKey
                                                     & x.ParentCategoryId == _Request.ReferenceId
                                                     select new OCategory.Details
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,

                                                         Name = x.Name,
                                                         Description = x.Description,

                                                         IconUrl = x.IconStorage.Path,
                                                         PosterUrl = x.PosterStorage.Path,

                                                         ParentCategoryId = x.ParentCategoryId,
                                                         ParentCategoryKey = x.ParentCategory.Guid,
                                                         ParentCategoryName = x.ParentCategory.Name,

                                                         CreateDate = x.CreateDate,
                                                         CreatedByKey = x.CreatedBy.Guid,
                                                         CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                         ModifyDate = x.ModifyDate,
                                                         ModifyByKey = x.ModifyBy.Guid,
                                                         ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name
                                                     })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        }
                        #endregion
                        #region Get Data
                        List<OCategory.Details> Data = (from x in _HCoreContext.TUCCategory
                                                        where x.ParentCategory.Guid == _Request.ReferenceKey
                                                        & x.ParentCategoryId == _Request.ReferenceId
                                                        select new OCategory.Details
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,

                                                            Name = x.Name,
                                                            Description = x.Description,

                                                            IconUrl = x.IconStorage.Path,
                                                            PosterUrl = x.PosterStorage.Path,

                                                            ParentCategoryId = x.ParentCategoryId,
                                                            ParentCategoryKey = x.ParentCategory.Guid,
                                                            ParentCategoryName = x.ParentCategory.Name,

                                                            CreateDate = x.CreateDate,
                                                            CreatedByKey = x.CreatedBy.Guid,
                                                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                            ModifyDate = x.ModifyDate,
                                                            ModifyByKey = x.ModifyBy.Guid,
                                                            ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name
                                                        })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var _Details in Data)
                        {
                            if (!string.IsNullOrEmpty(_Details.IconUrl))
                            {
                                _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                            }
                            else
                            {
                                _Details.IconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(_Details.PosterUrl))
                            {
                                _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                            }
                            else
                            {
                                _Details.PosterUrl = _AppConfig.Default_Poster;
                            }
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        #region Total Records
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = (from x in _HCoreContext.TUCCategory
                                                     select new OCategory.Details
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,

                                                         CategoryId = x.ParentCategory.Id,
                                                         CategoryKey = x.ParentCategory.Guid,
                                                         CategoryName = x.ParentCategory.Name,

                                                         Name = x.Name,
                                                         Description = x.Description,

                                                         IconUrl = x.IconStorage.Path,
                                                         PosterUrl = x.PosterStorage.Path,

                                                         CreateDate = x.CreateDate,
                                                         CreatedByKey = x.CreatedBy.Guid,
                                                         CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                         ModifyDate = x.ModifyDate,
                                                         ModifyByKey = x.ModifyBy.Guid,
                                                         ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name
                                                     })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        }
                        #endregion
                        #region Get Data
                        List<OCategory.Details> Data = (from x in _HCoreContext.TUCCategory
                                                        select new OCategory.Details
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,

                                                            CategoryId = x.ParentCategory.Id,
                                                            CategoryKey = x.ParentCategory.Guid,
                                                            CategoryName = x.ParentCategory.Name,

                                                            Name = x.Name,
                                                            Description = x.Description,

                                                            IconUrl = x.IconStorage.Path,
                                                            PosterUrl = x.PosterStorage.Path,

                                                            CreateDate = x.CreateDate,
                                                            CreatedByKey = x.CreatedBy.Guid,
                                                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                            ModifyDate = x.ModifyDate,
                                                            ModifyByKey = x.ModifyBy.Guid,
                                                            ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name
                                                        })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var _Details in Data)
                        {
                            if (!string.IsNullOrEmpty(_Details.IconUrl))
                            {
                                _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                            }
                            else
                            {
                                _Details.IconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(_Details.PosterUrl))
                            {
                                _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                            }
                            else
                            {
                                _Details.PosterUrl = _AppConfig.Default_Poster;
                            }
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetSubCategory", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }

        List<OCategory.AllCats> _AllCats;
        /// <summary>
        /// Description: Method defined to get list of all categories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetAllCategory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var TCategories = _HCoreContext.TUCCategory
                           .Select(x => new OCategory.Details
                           {
                               ReferenceId = x.Id,
                               ReferenceKey = x.Guid,

                               ParentCategoryId = x.ParentCategoryId,
                               ParentCategoryName = x.ParentCategory.Name,

                               Name = x.Name,
                               Description = x.Description,

                               IconUrl = x.IconStorage.Path,
                               PosterUrl = x.PosterStorage.Path,

                               StatusId = x.StatusId,
                               StatusCode = x.Status.SystemName,
                               StatusName = x.Status.Name
                           }).OrderBy(x => x.Name).ToList();
                    foreach (var TCategoryItem in TCategories)
                    {
                        if (!string.IsNullOrEmpty(TCategoryItem.IconUrl))
                        {
                            TCategoryItem.IconUrl = _AppConfig.StorageUrl + TCategoryItem.IconUrl;
                        }
                        else
                        {
                            TCategoryItem.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(TCategoryItem.PosterUrl))
                        {
                            TCategoryItem.PosterUrl = _AppConfig.StorageUrl + TCategoryItem.PosterUrl;
                        }
                        else
                        {
                            TCategoryItem.PosterUrl = _AppConfig.Default_Poster;
                        }
                    }
                    _AllCats = new List<OCategory.AllCats>();
                    var RootCategories = TCategories.Where(x => x.ParentCategoryId == null).ToList();
                    var SubCategories = TCategories.Where(x => x.ParentCategoryId != null).ToList();
                    foreach (var RootCategory in RootCategories)
                    {
                        var SubCats = SubCategories.Where(x => x.ParentCategoryId == RootCategory.ReferenceId)
                            .Select(x => new OCategory.Item
                            {
                                ReferenceId = x.ReferenceId,
                                ReferenceKey = x.ReferenceKey,
                                Name = x.Name,
                                IconUrl = x.IconUrl,
                            }).ToList();
                        _AllCats.Add(new OCategory.AllCats
                        {
                            ReferenceId = RootCategory.ReferenceId,
                            ReferenceKey = RootCategory.ReferenceKey,
                            Name = RootCategory.Name,
                            IconUrl = RootCategory.IconUrl,
                            Items = SubCats,
                        });
                    }
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AllCats, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAllCategory", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
