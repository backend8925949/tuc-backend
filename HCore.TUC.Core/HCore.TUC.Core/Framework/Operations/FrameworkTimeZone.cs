//==================================================================================
// FileName: FrameworkTimeZone.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to timezone
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.Framework.Operations
{
	public class FrameworkTimeZone
	{
        HCoreContext _HCoreContext;
        HCCoreCommon _HCCoreCommon;
        List<OTimeZone> _Timezones;

        /// <summary>
        /// Description: Method defined to save timezone
        /// </summary>
        /// <returns></returns>
        internal OResponse SaveTimeZones()
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
                    {
                        _HCCoreCommon = new HCCoreCommon();
                        _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                        _HCCoreCommon.TypeId = UserAccountType.TimeZone;
                        _HCCoreCommon.Name = z.Id;
                        _HCCoreCommon.SystemName = z.DisplayName;
                        _HCCoreCommon.Value = z.DaylightName;
                        _HCCoreCommon.Description = z.StandardName;
                        _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCCoreCommon.CreatedById = 2;
                        _HCCoreCommon.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                        _HCoreContext.SaveChanges();

                        //Console.WriteLine(z.Id);
                        //Console.WriteLine(z.DisplayName);
                        //Console.WriteLine(z.DaylightName);
                        //Console.WriteLine(z.StandardName);
                    }
                    return HCoreHelper.SendResponse(null, HCoreConstant.ResponseStatus.Success, null, "TIMEZONE", "Timezone saved successfully");
                }
            }
            catch (Exception ex)
            {
                return HCoreHelper.LogException(null, ex, null);
            }
        }

        /// <summary>
        /// Description: Method defined to get list of timezones
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetTimeZones(OList.Request _Request)
        {
            try
            {
                _Timezones = new List<OTimeZone>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCCoreCommon
                                                .Where(x => x.TypeId == UserAccountType.TimeZone)
                                                .Select(x => new OTimeZone
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    CountryId = x.CountryId,
                                                    CountryKey = x.Country.Guid,
                                                    CountryName = x.Country.Name,
                                                    CountrySystemName = x.Country.SystemName,

                                                    Name = x.Name,
                                                    SystemName = x.SystemName,
                                                    Value = x.Value,
                                                    Description = x.Description,
                                                    TypeId = x.TypeId,
                                                    TypeCode = x.Type.SystemName,
                                                    TypeName = x.Type.Name,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Timezones = _HCoreContext.HCCoreCommon
                                                .Where(x => x.TypeId == UserAccountType.TimeZone)
                                                .Select(x => new OTimeZone
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    CountryId = x.CountryId,
                                                    CountryKey = x.Country.Guid,
                                                    CountryName = x.Country.Name,
                                                    CountrySystemName = x.Country.SystemName,

                                                    Name = x.Name,
                                                    SystemName = x.SystemName,
                                                    Value = x.Value,
                                                    Description = x.Description,
                                                    TypeId = x.TypeId,
                                                    TypeCode = x.Type.SystemName,
                                                    TypeName = x.Type.Name,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Timezones)
                    {

                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Timezones, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetMerchant", _Exception, _Request.UserReference, _OResponse, "TMZ500", "Internal servern error. Please try after some time");
            }
        }
	}
}

