//==================================================================================
// FileName: FrameworkSubscription.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to subscription
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Integration.Paystack;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant.HelperStatus;
using static HCore.TUC.Core.Helper.HelperEmailTemplate;

namespace HCore.TUC.Core.Framework.Operations
{
    internal class FrameworkSubscription
    {
        OSubscription.Update _SubUpdate;
        HCoreContext _HCoreContext;
        HCUAccountSubscription _HCUAccountSubscription;
        /// <summary>
        /// Description: Method defined to get list of subscriptions
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetSubscription(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.HCSubscription
                                                 where x.AccountType.SystemName == _Request.AccountTypeCode
                                                 && x.CountryId == _Request.UserReference.SystemCountry
                                                 && x.StatusId == HelperStatus.Default.Active
                                                 select new OSubscription.Subscription
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     TypeName = x.Type.Name,
                                                     Name = x.Name,
                                                     Description = x.Description,
                                                     ActualPrice = x.ActualPrice,
                                                     SellingPrice = x.SellingPrice,
                                                     Policy = x.Policy,
                                                 })
                                         .Where(_Request.SearchCondition)
                                 .Count();
                    }
                    #endregion
                    #region Get Data
                    List<OSubscription.Subscription> _Subscriptions = (from x in _HCoreContext.HCSubscription
                                                                       where x.AccountTypeId == UserAccountType.Merchant
                                                 && x.CountryId == _Request.UserReference.SystemCountry
                                                                       && x.StatusId == HelperStatus.Default.Active
                                                                       select new OSubscription.Subscription
                                                                       {
                                                                           ReferenceId = x.Id,
                                                                           ReferenceKey = x.Guid,
                                                                           TypeName = x.Type.Name,
                                                                           Name = x.Name,
                                                                           Description = x.Description,
                                                                           ActualPrice = x.ActualPrice,
                                                                           SellingPrice = x.SellingPrice,
                                                                           Policy = x.Policy,
                                                                       })
                                                                      .OrderBy(x => x.SellingPrice)
                                               .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var DataItem in _Subscriptions)
                    {
                        DataItem.Features = _HCoreContext.HCSubscriptionFeature
                            .Where(x => x.SubscriptionId == DataItem.ReferenceId && x.StatusId == HelperStatus.Default.Active)
                            .Select(x => new OSubscription.SubscriptionFeature
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                Name = x.Name,
                                ShortDescription = x.ShortDescription,
                                Description = x.Description,
                                Policy = x.Policy,
                                MinimumLimit = x.MinimumLimit,
                                MaximumLimit = x.MaximumLimit,
                                MinimumAmount = x.MinimumAmount,
                                MaximumAmount = x.MaximumAmount,
                            }).ToList();
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Subscriptions.Count, _Subscriptions, _Request.Offset, _Request.Limit);
                    _HCoreContext.Dispose();
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetSubscription", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to get list of account subscriptions
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetAccountSubscription(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ActualPrice", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreatedDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId > 0 && !string.IsNullOrEmpty(_Request.AccountKey))
                    {
                        #region Total Records
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = (from x in _HCoreContext.HCUAccountSubscription
                                                     where x.AccountId == _Request.AccountId
                                                     && x.Account.Guid == _Request.AccountKey
                                                     && x.StatusId == Default.Active
                                                     && x.Account.CountryId == _Request.UserReference.CountryId
                                                     orderby x.EndDate descending
                                                     select new OSubscription.AccountSubscription
                                                     {
                                                         //ReferenceId = x.Id,
                                                         //ReferenceKey = x.Guid,
                                                         TypeName = x.Type.Name,
                                                         SubscriptionId = x.Subscription.Id,
                                                         SubscriptionKey = x.Subscription.Guid,
                                                         Name = x.Subscription.Name,
                                                         Description = x.Subscription.Description,
                                                         ActualPrice = x.ActualPrice,
                                                         SellingPrice = x.SellingPrice,
                                                         Policy = x.Subscription.Policy,
                                                         StartDate = x.StartDate,
                                                         EndDate = x.EndDate,
                                                         RenewDate = x.RenewDate,
                                                         CreatedDate = x.CreateDate.AddSeconds(-x.CreateDate.Second),
                                                         //NoOfStores = x.Value
                                                     })
                                                     .Where(_Request.SearchCondition)
                                                     .Distinct()
                                     .Count();
                        }
                        #endregion
                        #region Get Data
                        List<OSubscription.AccountSubscription> _Subscriptions = (from x in _HCoreContext.HCUAccountSubscription
                                                                                  where x.AccountId == _Request.AccountId
                                                                                  && x.Account.Guid == _Request.AccountKey
                                                                                  && x.StatusId == Default.Active
                                                                                  && x.Account.CountryId == _Request.UserReference.CountryId
                                                                                  orderby x.EndDate descending
                                                                                  select new OSubscription.AccountSubscription
                                                                                  {
                                                                                      //ReferenceId = x.Id,
                                                                                      //ReferenceKey = x.Guid,
                                                                                      TypeName = x.Type.Name,
                                                                                      SubscriptionId = x.Subscription.Id,
                                                                                      SubscriptionKey = x.Subscription.Guid,
                                                                                      Name = x.Subscription.Name,
                                                                                      Description = x.Subscription.Description,
                                                                                      ActualPrice = x.ActualPrice,
                                                                                      SellingPrice = x.SellingPrice,
                                                                                      Policy = x.Subscription.Policy,
                                                                                      StartDate = x.StartDate,
                                                                                      EndDate = x.EndDate,
                                                                                      RenewDate = x.RenewDate,
                                                                                      CreatedDate = x.CreateDate.AddSeconds(-x.CreateDate.Second),
                                                                                      //NoOfStores = x.Value
                                                                                  })
                                                                                  .Where(_Request.SearchCondition)
                                                                                  .Distinct()
                                                                                  .OrderBy(_Request.SortExpression)
                                                                                  .Skip(_Request.Offset)
                                                                                  .Take(_Request.Limit)
                                                                                  .ToList();
                        #endregion
                        #region Create  Response Object
                        foreach (var DataItem in _Subscriptions)
                        {
                            DataItem.Features = _HCoreContext.HCUAccountSubscriptionFeature
                                .Where(x => x.AccountSubscriptionId == DataItem.ReferenceId && x.StatusId == HelperStatus.Default.Active)
                                .Select(x => new OSubscription.AccountSubscriptionFeature
                                {
                                    ReferenceId = x.Id,
                                    ReferenceKey = x.Guid,
                                    Name = x.SubscriptionFeature.Name,
                                    ShortDescription = x.SubscriptionFeature.ShortDescription,
                                    Description = x.SubscriptionFeature.Description,
                                    Policy = x.SubscriptionFeature.Policy,
                                    MinimumLimit = x.MinimumLimit,
                                    MaximumLimit = x.MaximumLimit,
                                    MinimumAmount = x.MinimumAmount,
                                    MaximumAmount = x.MaximumAmount,
                                }).ToList();
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Subscriptions, _Request.Offset, _Request.Limit);
                        _HCoreContext.Dispose();
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        #region Total Records
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = (from x in _HCoreContext.HCUAccountSubscription
                                                         //where x.AccountId == _Request.AccountId
                                                         //&& x.Account.Guid == _Request.AccountKey
                                                     where x.StatusId == Default.Active
                                                     && x.Account.CountryId == _Request.UserReference.CountryId
                                                     select new OSubscription.AccountSubscription
                                                     {
                                                         //ReferenceId = x.Id,
                                                         //ReferenceKey = x.Guid,
                                                         TypeName = x.Type.Name,
                                                         SubscriptionId = x.Subscription.Id,
                                                         SubscriptionKey = x.Subscription.Guid,
                                                         Name = x.Subscription.Name,
                                                         Description = x.Subscription.Description,
                                                         ActualPrice = x.ActualPrice,
                                                         SellingPrice = x.SellingPrice,
                                                         Policy = x.Subscription.Policy,
                                                         StartDate = x.StartDate,
                                                         EndDate = x.EndDate,
                                                         RenewDate = x.RenewDate,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name,
                                                         AccountId = x.CreatedById,
                                                         AccountKey = x.CreatedBy.Guid,
                                                         AccountDisplayName = x.CreatedBy.DisplayName,
                                                         AccountIconUrl = x.CreatedBy.IconStorage.Path,
                                                         CreatedDate = x.CreateDate.AddSeconds(-x.CreateDate.Second),
                                                         TransactionId = x.TransactionId
                                                         //NoOfStores = x.Value
                                                     })
                                                     .Where(_Request.SearchCondition)
                                                     .Distinct()
                                     .Count();
                        }
                        #endregion
                        #region Get Data
                        List<OSubscription.AccountSubscription> _Subscriptions = (from x in _HCoreContext.HCUAccountSubscription
                                                                                      //where x.AccountId == _Request.AccountId
                                                                                      //&& x.Account.Guid == _Request.AccountKey
                                                                                  where x.StatusId == Default.Active
                                                                                  && x.Account.CountryId == _Request.UserReference.CountryId
                                                                                  select new OSubscription.AccountSubscription
                                                                                  {
                                                                                      //ReferenceId = x.Id,
                                                                                      //ReferenceKey = x.Guid,
                                                                                      TypeName = x.Type.Name,
                                                                                      SubscriptionId = x.Subscription.Id,
                                                                                      SubscriptionKey = x.Subscription.Guid,
                                                                                      Name = x.Subscription.Name,
                                                                                      Description = x.Subscription.Description,
                                                                                      ActualPrice = x.ActualPrice,
                                                                                      SellingPrice = x.SellingPrice,
                                                                                      Policy = x.Subscription.Policy,
                                                                                      StartDate = x.StartDate,
                                                                                      EndDate = x.EndDate,
                                                                                      RenewDate = x.RenewDate,
                                                                                      StatusCode = x.Status.SystemName,
                                                                                      StatusName = x.Status.Name,
                                                                                      AccountId = x.CreatedById,
                                                                                      AccountKey = x.CreatedBy.Guid,
                                                                                      AccountDisplayName = x.CreatedBy.DisplayName,
                                                                                      AccountIconUrl = x.CreatedBy.IconStorage.Path,
                                                                                      CreatedDate = x.CreateDate.AddSeconds(-x.CreateDate.Second),
                                                                                      TransactionId = x.TransactionId
                                                                                      //NoOfStores = x.Value
                                                                                  }).Where(_Request.SearchCondition)
                                                                                  .Distinct()
                                                                                  .OrderBy(_Request.SortExpression)
                                                                                  .Skip(_Request.Offset)
                                                                                  .Take(_Request.Limit)
                                                                                  .ToList();
                        #endregion
                        #region Create  Response Object
                        foreach (var DataItem in _Subscriptions)
                        {
                            DataItem.NoOfStores = _HCoreContext.HCUAccountSubscription.Where(
                                        x => x.CreatedById == DataItem.AccountId && x.TransactionId == DataItem.TransactionId).Count();

                            if (!string.IsNullOrEmpty(DataItem.AccountIconUrl))
                            {
                                DataItem.AccountIconUrl = _AppConfig.StorageUrl + DataItem.AccountIconUrl;
                            }
                            else
                            {
                                DataItem.AccountIconUrl = _AppConfig.Default_Icon;
                            }
                            DataItem.Features = _HCoreContext.HCUAccountSubscriptionFeature
                                .Where(x => x.AccountSubscriptionId == DataItem.ReferenceId && x.StatusId == HelperStatus.Default.Active)
                                .Select(x => new OSubscription.AccountSubscriptionFeature
                                {
                                    ReferenceId = x.Id,
                                    ReferenceKey = x.Guid,
                                    Name = x.SubscriptionFeature.Name,
                                    ShortDescription = x.SubscriptionFeature.ShortDescription,
                                    Description = x.SubscriptionFeature.Description,
                                    Policy = x.SubscriptionFeature.Policy,
                                    MinimumLimit = x.MinimumLimit,
                                    MaximumLimit = x.MaximumLimit,
                                    MinimumAmount = x.MinimumAmount,
                                    MaximumAmount = x.MaximumAmount,
                                }).ToList();
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Subscriptions, _Request.Offset, _Request.Limit);
                        _HCoreContext.Dispose();
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAccountSubscription", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to get account subscription overview
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetAccountSubscriptionOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    List<OSubscription.Subscription> _SubscriptionsList = _HCoreContext.HCSubscription.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                                           && x.StatusId == HelperStatus.Default.Active)
                        .Select(x => new OSubscription.Subscription
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            Name = x.Name,
                        }).ToList();


                    //(from x in _HCoreContext.HCSubscription
                    //                                                   where x.AccountTypeId == UserAccountType.Merchant
                    //                                                   && x.StatusId == HelperStatus.Default.Active
                    //                                                   orderby x.SellingPrice
                    //                                                   select new OSubscription.Subscription
                    //                                                   {
                    //                                                       ReferenceId = x.Id,
                    //                                                       ReferenceKey = x.Guid,
                    //                                                       Name = x.Name,
                    //                                                   })
                    //                     .ToList();
                    #region Total Records
                    foreach (var item in _SubscriptionsList)
                    {
                        item.Customers = _HCoreContext.HCUAccountSubscription.Count(x => x.StatusId == Default.Active && x.SubscriptionId == item.ReferenceId && x.Account.CountryId == _Request.UserReference.CountryId);
                        //item.Customers = (from x in _HCoreContext.HCUAccountSubscription
                        //                      //where x.AccountId == _Request.AccountId
                        //                      //&& x.Account.Guid == _Request.AccountKey
                        //                  where x.StatusId == Default.Active
                        //                  && x.SubscriptionId == item.ReferenceId
                        //                  select new OSubscription.AccountSubscription
                        //                  {
                        //                      ReferenceId = x.Id,
                        //                      ReferenceKey = x.Guid,
                        //                      TypeName = x.Type.Name,
                        //                      Name = x.Subscription.Name,
                        //                      Description = x.Subscription.Description,
                        //                      ActualPrice = x.ActualPrice,
                        //                      SellingPrice = x.SellingPrice,
                        //                      Policy = x.Subscription.Policy,
                        //                      StartDate = x.StartDate,
                        //                      EndDate = x.EndDate,
                        //                      RenewDate = x.RenewDate,
                        //                      StatusCode = x.Status.SystemName,
                        //                      StatusName = x.Status.Name,
                        //                      AccountId = x.AccountId,
                        //                      AccountKey = x.Account.Guid,
                        //                      AccountDisplayName = x.Account.DisplayName,
                        //                      AccountIconUrl = x.Account.IconStorage.Path,
                        //                  })
                        //        .Count();
                    }
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SubscriptionsList, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAccountSubscriptionOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

        HCUAccountParameter _HCUAccountParameter;
        /// <summary>
        /// Description: Method defined to update subscription
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateSubscription(OSubscription.SubscriptionUpdate.Request _Request)
        {
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                if (_Request.SubscriptionId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1101, TUCCoreResource.CA1101M);
                }
                if (string.IsNullOrEmpty(_Request.SubscriptionKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1102, TUCCoreResource.CA1102M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var UserActiveSubscriptions = _HCoreContext.HCUAccountSubscription.Where(x => x.AccountId == _Request.AccountId && x.StatusId == Default.Active).ToList();
                    foreach (var UserActiveSubscription in UserActiveSubscriptions)
                    {
                        if (_Request.UserReference != null && _Request.UserReference.AccountId > 0)
                        {
                            UserActiveSubscription.ModifyById = _Request.UserReference.AccountId;
                        }
                        UserActiveSubscription.ModifyDate = HCoreHelper.GetGMTDateTime();
                        UserActiveSubscription.StatusId = HelperStatus.Default.Blocked;
                    }
                    _HCoreContext.SaveChanges();
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey).FirstOrDefault();
                    if (AccountDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                    var SubscriptionDetails = _HCoreContext.HCSubscription.Where(x => x.Id == _Request.SubscriptionId && x.Guid == _Request.SubscriptionKey).FirstOrDefault();
                    if (SubscriptionDetails != null)
                    {
                        if (SubscriptionDetails.SellingPrice > 0 && string.IsNullOrEmpty(_Request.PaymentReference))
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1085, TUCCoreResource.CA1085M);
                        }
                        var AccountSubscriptionEndDate = _HCoreContext.HCUAccountSubscription.Where(x => x.AccountId == _Request.AccountId).OrderByDescending(x => x.RenewDate).FirstOrDefault();
                        _HCUAccountSubscription = new HCUAccountSubscription();
                        _HCUAccountSubscription.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountSubscription.TypeId = SubscriptionDetails.TypeId;
                        _HCUAccountSubscription.AccountId = _Request.AccountId;
                        _HCUAccountSubscription.SubscriptionId = SubscriptionDetails.Id;
                        if (AccountSubscriptionEndDate != null && AccountSubscriptionEndDate.SellingPrice > 0)
                        {
                            _HCUAccountSubscription.StartDate = AccountSubscriptionEndDate.RenewDate;
                        }
                        else
                        {
                            _HCUAccountSubscription.StartDate = HCoreHelper.GetGMTDate();
                        }
                        _HCUAccountSubscription.EndDate = _HCUAccountSubscription.StartDate.AddDays(SubscriptionDetails.TotalDays);
                        _HCUAccountSubscription.RenewDate = _HCUAccountSubscription.EndDate.AddDays(1);
                        _HCUAccountSubscription.ActualPrice = SubscriptionDetails.ActualPrice;
                        _HCUAccountSubscription.SellingPrice = SubscriptionDetails.SellingPrice;
                        _HCUAccountSubscription.CreateDate = HCoreHelper.GetGMTDateTime();
                        if (_Request.UserReference != null && _Request.UserReference.AccountId > 0)
                        {
                            _HCUAccountSubscription.CreatedById = _Request.UserReference.AccountId;
                        }
                        _HCUAccountSubscription.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.HCUAccountSubscription.Add(_HCUAccountSubscription);
                        _HCoreContext.SaveChanges();

                        using (_HCoreContext = new HCoreContext())
                        {
                            var AccSub = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId).FirstOrDefault();
                            if (AccSub != null)
                            {
                                AccSub.SubscriptionId = _HCUAccountSubscription.Id;
                                _HCoreContext.SaveChanges();
                            }
                        }

                        if (SubscriptionDetails.Id == 3
                            || SubscriptionDetails.Id == 5
                            || SubscriptionDetails.Id == 7
                            || SubscriptionDetails.Id == 9
                            || SubscriptionDetails.Id == 11
                            || SubscriptionDetails.Id == 13
                            || SubscriptionDetails.Id == 15
                            || SubscriptionDetails.Id == 17
                            || SubscriptionDetails.Id == 19
                            || SubscriptionDetails.Id == 21

                            )
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x =>
                                  x.SystemName == "thankucashgold"
                                  && x.StatusId == HCoreConstant.HelperStatus.Default.Active
                                  && x.TypeId == HCoreConstant.HelperType.Configuration
                                  ).Select(x => new
                                  {
                                      SystemName = x.SystemName,
                                      HelperId = x.HelperId,
                                      Value = x.Value,
                                      ConfigurationId = x.Id,
                                  }).FirstOrDefault();
                                if (ConfigurationDetails != null)
                                {
                                    _HCUAccountParameter = new HCUAccountParameter();
                                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                    _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                    _HCUAccountParameter.AccountId = _Request.AccountId;
                                    _HCUAccountParameter.CommonId = ConfigurationDetails.ConfigurationId;
                                    _HCUAccountParameter.Value = "1";
                                    //if (_Request.UserReference != null && _Request.UserReference.AccountId > 0)
                                    //{
                                    _HCUAccountParameter.CreatedById = _Request.AccountId;
                                    //}
                                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                                    _HCUAccountParameter.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                    _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                    _HCoreContext.SaveChanges();
                                }
                            }

                        }
                        else
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x =>
                                  x.SystemName == "thankucashgold"
                                  && x.StatusId == HCoreConstant.HelperStatus.Default.Active
                                  && x.TypeId == HCoreConstant.HelperType.Configuration
                                  ).Select(x => new
                                  {
                                      SystemName = x.SystemName,
                                      HelperId = x.HelperId,
                                      Value = x.Value,
                                      ConfigurationId = x.Id,
                                  }).FirstOrDefault();
                                if (ConfigurationDetails != null)
                                {
                                    var HCUserPara = _HCoreContext.HCUAccountParameter.Where(x => x.Id == _Request.AccountId && x.CommonId == ConfigurationDetails.ConfigurationId).FirstOrDefault();
                                    if (HCUserPara != null)
                                    {
                                        HCUserPara.EndTime = HCoreHelper.GetGMTDateTime();
                                        HCUserPara.StatusId = HelperStatus.Default.Blocked;
                                        HCUserPara.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                        }

                        var EmailObject = new
                        {
                            EmailAddress = AccountDetails.EmailAddress,
                            DisplayName = AccountDetails.DisplayName,
                        };

                        HCoreHelper.BroadCastEmail(MerchantOnboarding.SubscriptionConfirmed, EmailObject.DisplayName, EmailObject.EmailAddress, EmailObject, _Request.UserReference);

                        //SubscriptionConfirmed
                        using (_HCoreContext = new HCoreContext())
                        {
                            _SubUpdate = new OSubscription.Update();
                            _SubUpdate.Features = _HCoreContext.HCSubscriptionFeature
                      .Where(x => x.SubscriptionId == SubscriptionDetails.Id
                      && x.StatusId == HelperStatus.Default.Active)
                      .Select(x => new OSubscription.OFeatures
                      {
                          Name = x.Name,
                          SystemName = x.Guid
                          //PermissionId = x.Id,
                          //Name = x.SystemFeature.Name,
                          //SystemName = x.SystemFeature.SystemName,
                          //IsAccessPinRequired = false,
                          //IsDefault = false,
                          //IsPasswordRequired = false,
                          //MinimumLimit = x.MinimumLimit,
                          //MaximumLimit = x.MaximumLimit,
                          //MinimumValue = x.MinimumAmount,
                          //MaximumValue = x.MaximumAmount,

                      }).ToList();
                            _SubUpdate.Permissions = _HCoreContext.HCSubscriptionFeatureItem
                                .Where(x => x.SubscriptionFeature.Subscription.Id == SubscriptionDetails.Id
                                && x.StatusId == HelperStatus.Default.Active)
                                .Select(x => new OSubscription.OUserAccountRolePermission
                                {
                                    PermissionId = x.Id,
                                    Name = x.SystemFeature.Name,
                                    SystemName = x.SystemFeature.SystemName,
                                    IsAccessPinRequired = false,
                                    IsDefault = false,
                                    IsPasswordRequired = false,
                                    MinimumLimit = x.MinimumLimit,
                                    MaximumLimit = x.MaximumLimit,
                                    MinimumValue = x.MinimumAmount,
                                    MaximumValue = x.MaximumAmount,

                                }).ToList();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SubUpdate, TUCCoreResource.CA1104, TUCCoreResource.CA1104M);
                        }


                        //if (SubscriptionDetails.SellingPrice > 0)
                        //{
                        //    PayStackResponseData _PayStackResponseData = PaystackTransaction.GetTransaction(_Request.PaymentReference, _AppConfig.PaystackPrivateKey);
                        //    if (_PayStackResponseData != null && _PayStackResponseData.status == "success")
                        //    {
                        //        //_CoreTransactionRequest = new OCoreTransaction.Request();
                        //        //_CoreTransactionRequest.UserReference = _Request.UserReference;
                        //        //_CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                        //        //_CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                        //        //_CoreTransactionRequest.ParentId = _Request.AccountId;
                        //        //_CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                        //        //_CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                        //        //_CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                        //        //_CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                        //        //_TransactionItems = new List<OCoreTransaction.TransactionItem>();
                        //        //_TransactionItems.Add(new OCoreTransaction.TransactionItem
                        //        //{
                        //        //    UserAccountId = _Request.AccountId,
                        //        //    ModeId = TransactionMode.Credit,
                        //        //    TypeId = TransactionType.MerchantCredit,
                        //        //    SourceId = TransactionSource.Merchant,
                        //        //    Amount = _Request.Amount,
                        //        //    Comission = 0,
                        //        //    TotalAmount = _Request.Amount,
                        //        //});
                        //        //_CoreTransactionRequest.Transactions = _TransactionItems;
                        //        //_ManageCoreTransaction = new ManageCoreTransaction();
                        //        //OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                        //        //if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                        //        //{
                        //        //    #region Send Response
                        //        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1087, TUCCoreResource.CA1087M);
                        //        //    #endregion
                        //        //}
                        //        //else
                        //        //{
                        //        //    #region Send Response
                        //        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1088, TUCCoreResource.CA1088M);
                        //        //    #endregion
                        //        //}
                        //    }
                        //}
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1103, TUCCoreResource.CA1103M);
                    }
                }


                //PayStackResponseData _PayStackResponseData = PaystackTransaction.GetTransaction(_Request.PaymentReference, _AppConfig.PaystackPrivateKey);
                //if (_PayStackResponseData != null && _PayStackResponseData.status == "success")
                //{
                //    //_CoreTransactionRequest = new OCoreTransaction.Request();
                //    //_CoreTransactionRequest.UserReference = _Request.UserReference;
                //    //_CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                //    //_CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                //    //_CoreTransactionRequest.ParentId = _Request.AccountId;
                //    //_CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                //    //_CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                //    //_CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                //    //_CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                //    //_TransactionItems = new List<OCoreTransaction.TransactionItem>();
                //    //_TransactionItems.Add(new OCoreTransaction.TransactionItem
                //    //{
                //    //    UserAccountId = _Request.AccountId,
                //    //    ModeId = TransactionMode.Credit,
                //    //    TypeId = TransactionType.MerchantCredit,
                //    //    SourceId = TransactionSource.Merchant,
                //    //    Amount = _Request.Amount,
                //    //    Comission = 0,
                //    //    TotalAmount = _Request.Amount,
                //    //});
                //    //_CoreTransactionRequest.Transactions = _TransactionItems;
                //    //_ManageCoreTransaction = new ManageCoreTransaction();
                //    //OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                //    //if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                //    //{
                //    //    #region Send Response
                //    //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1087, TUCCoreResource.CA1087M);
                //    //    #endregion
                //    //}
                //    //else
                //    //{
                //    //    #region Send Response
                //    //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1088, TUCCoreResource.CA1088M);
                //    //    #endregion
                //    //}
                //}
                //else
                //{
                //    #region Send Response
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1089, TUCCoreResource.CA1089M);
                //    #endregion
                //}


            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateSubscription", _Exception);
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                #endregion
            }

        }
        internal void SetPartnerReferralMerchantSubscription(long AccountId)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var UserActiveSubscriptions = _HCoreContext.HCUAccountSubscription.Where(x => x.AccountId == AccountId && x.StatusId == Default.Active).ToList();
                    if (UserActiveSubscriptions.Count > 0)
                    {
                        foreach (var UserActiveSubscription in UserActiveSubscriptions)
                        {
                            UserActiveSubscription.ModifyDate = HCoreHelper.GetGMTDateTime();
                            UserActiveSubscription.StatusId = HelperStatus.Default.Blocked;
                        }
                        _HCoreContext.SaveChanges();
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                    }

                }
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == AccountId).FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        var SubscriptionDetails = _HCoreContext.HCSubscription.Where(x => x.Id == 2).FirstOrDefault();
                        if (SubscriptionDetails != null)
                        {
                            _HCUAccountSubscription = new HCUAccountSubscription();
                            _HCUAccountSubscription.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountSubscription.TypeId = SubscriptionDetails.TypeId;
                            _HCUAccountSubscription.AccountId = AccountId;
                            _HCUAccountSubscription.SubscriptionId = SubscriptionDetails.Id;
                            _HCUAccountSubscription.StartDate = HCoreHelper.GetGMTDate();
                            _HCUAccountSubscription.EndDate = _HCUAccountSubscription.StartDate.AddDays(90);
                            _HCUAccountSubscription.RenewDate = _HCUAccountSubscription.EndDate.AddDays(1);
                            _HCUAccountSubscription.ActualPrice = SubscriptionDetails.ActualPrice;
                            _HCUAccountSubscription.SellingPrice = SubscriptionDetails.SellingPrice;
                            _HCUAccountSubscription.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountSubscription.CreatedById = AccountId;
                            _HCUAccountSubscription.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.HCUAccountSubscription.Add(_HCUAccountSubscription);
                            //AccountDetails.SubscriptionId = SubscriptionDetails.Id;
                            _HCoreContext.SaveChanges();
                            using (_HCoreContext = new HCoreContext())
                            {
                                var AccSub = _HCoreContext.HCUAccount.Where(x => x.Id == AccountDetails.Id).FirstOrDefault();
                                if (AccSub != null)
                                {
                                    AccSub.SubscriptionId = _HCUAccountSubscription.Id;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                            //var EmailObject = new
                            //{
                            //    EmailAddress = AccountDetails.EmailAddress,
                            //    DisplayName = AccountDetails.DisplayName,
                            //};
                            //HCoreHelper.BroadCastEmail(MerchantOnboarding.SubscriptionConfirmed, EmailObject.DisplayName, EmailObject.EmailAddress, EmailObject, null);
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateSubscription", _Exception);
            }

        }
        internal void SetSubscriptionId(long AccountId, long SubscriptionId)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var UserActiveSubscriptions = _HCoreContext.HCUAccountSubscription.Where(x => x.AccountId == AccountId && x.StatusId == Default.Active).ToList();
                    foreach (var UserActiveSubscription in UserActiveSubscriptions)
                    {

                        UserActiveSubscription.ModifyById = AccountId;
                        UserActiveSubscription.ModifyDate = HCoreHelper.GetGMTDateTime();
                        UserActiveSubscription.StatusId = Default.Blocked;
                    }
                    _HCoreContext.SaveChanges();
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == AccountId).FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        var SubscriptionDetails = _HCoreContext.HCSubscription.Where(x => x.Id == SubscriptionId).FirstOrDefault();
                        if (SubscriptionDetails != null)
                        {
                            var AccountSubscriptionEndDate = _HCoreContext.HCUAccountSubscription.Where(x => x.AccountId == AccountId).OrderByDescending(x => x.EndDate).FirstOrDefault();
                            _HCUAccountSubscription = new HCUAccountSubscription();
                            _HCUAccountSubscription.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountSubscription.TypeId = SubscriptionDetails.TypeId;
                            _HCUAccountSubscription.AccountId = AccountId;
                            _HCUAccountSubscription.SubscriptionId = SubscriptionDetails.Id;
                            if (AccountSubscriptionEndDate != null)
                            {
                                _HCUAccountSubscription.StartDate = AccountSubscriptionEndDate.RenewDate;
                            }
                            else
                            {
                                _HCUAccountSubscription.StartDate = HCoreHelper.GetGMTDate();
                            }
                            _HCUAccountSubscription.EndDate = _HCUAccountSubscription.StartDate.AddDays(SubscriptionDetails.TotalDays);
                            _HCUAccountSubscription.RenewDate = _HCUAccountSubscription.EndDate.AddDays(1);
                            _HCUAccountSubscription.ActualPrice = SubscriptionDetails.ActualPrice;
                            _HCUAccountSubscription.SellingPrice = SubscriptionDetails.SellingPrice;
                            _HCUAccountSubscription.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountSubscription.CreatedById = AccountId;
                            _HCUAccountSubscription.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.HCUAccountSubscription.Add(_HCUAccountSubscription);
                            _HCoreContext.SaveChanges();
                            using (_HCoreContext = new HCoreContext())
                            {
                                var AccSub = _HCoreContext.HCUAccount.Where(x => x.Id == AccountId).FirstOrDefault();
                                if (AccSub != null)
                                {
                                    AccSub.SubscriptionId = _HCUAccountSubscription.Id;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                            if (SubscriptionDetails.Id == 3
                                || SubscriptionDetails.Id == 5
                                || SubscriptionDetails.Id == 7
                                || SubscriptionDetails.Id == 9
                                || SubscriptionDetails.Id == 11
                                || SubscriptionDetails.Id == 13
                                || SubscriptionDetails.Id == 15
                                || SubscriptionDetails.Id == 17
                                || SubscriptionDetails.Id == 19
                                || SubscriptionDetails.Id == 21

                                )
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x =>
                                      x.SystemName == "thankucashgold"
                                      && x.StatusId == HCoreConstant.HelperStatus.Default.Active
                                      && x.TypeId == HCoreConstant.HelperType.Configuration
                                      ).Select(x => new
                                      {
                                          SystemName = x.SystemName,
                                          HelperId = x.HelperId,
                                          Value = x.Value,
                                          ConfigurationId = x.Id,
                                      }).FirstOrDefault();
                                    if (ConfigurationDetails != null)
                                    {
                                        _HCUAccountParameter = new HCUAccountParameter();
                                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                        _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                        _HCUAccountParameter.AccountId = AccountId;
                                        _HCUAccountParameter.CommonId = ConfigurationDetails.ConfigurationId;
                                        _HCUAccountParameter.Value = "1";
                                        //if (_Request.UserReference != null && _Request.UserReference.AccountId > 0)
                                        //{
                                        _HCUAccountParameter.CreatedById = AccountId;
                                        //}
                                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                        _HCoreContext.SaveChanges();
                                    }
                                }

                            }
                            else
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x =>
                                      x.SystemName == "thankucashgold"
                                      && x.StatusId == HCoreConstant.HelperStatus.Default.Active
                                      && x.TypeId == HCoreConstant.HelperType.Configuration
                                      ).Select(x => new
                                      {
                                          SystemName = x.SystemName,
                                          HelperId = x.HelperId,
                                          Value = x.Value,
                                          ConfigurationId = x.Id,
                                      }).FirstOrDefault();
                                    if (ConfigurationDetails != null)
                                    {
                                        var HCUserPara = _HCoreContext.HCUAccountParameter.Where(x => x.Id == AccountId && x.CommonId == ConfigurationDetails.ConfigurationId).FirstOrDefault();
                                        if (HCUserPara != null)
                                        {
                                            HCUserPara.EndTime = HCoreHelper.GetGMTDateTime();
                                            HCUserPara.StatusId = HelperStatus.Default.Blocked;
                                            HCUserPara.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            _HCoreContext.SaveChanges();
                                        }
                                    }
                                }
                            }
                            var EmailObject = new
                            {
                                EmailAddress = AccountDetails.EmailAddress,
                                DisplayName = AccountDetails.DisplayName,
                            };

                            HCoreHelper.BroadCastEmail(MerchantOnboarding.SubscriptionConfirmed, EmailObject.DisplayName, EmailObject.EmailAddress, EmailObject, null);
                            using (_HCoreContext = new HCoreContext())
                            {
                                _SubUpdate = new OSubscription.Update();
                                _SubUpdate.Features = _HCoreContext.HCSubscriptionFeature
                          .Where(x => x.SubscriptionId == SubscriptionDetails.Id
                          && x.StatusId == HelperStatus.Default.Active)
                          .Select(x => new OSubscription.OFeatures
                          {
                              Name = x.Name,
                              SystemName = x.Guid
                          }).ToList();
                                _SubUpdate.Permissions = _HCoreContext.HCSubscriptionFeatureItem
                                    .Where(x => x.SubscriptionFeature.Subscription.Id == SubscriptionDetails.Id
                                    && x.StatusId == HelperStatus.Default.Active)
                                    .Select(x => new OSubscription.OUserAccountRolePermission
                                    {
                                        PermissionId = x.Id,
                                        Name = x.SystemFeature.Name,
                                        SystemName = x.SystemFeature.SystemName,
                                        IsAccessPinRequired = false,
                                        IsDefault = false,
                                        IsPasswordRequired = false,
                                        MinimumLimit = x.MinimumLimit,
                                        MaximumLimit = x.MaximumLimit,
                                        MinimumValue = x.MinimumAmount,
                                        MaximumValue = x.MaximumAmount,

                                    }).ToList();
                            }
                        }
                    }
                }





            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateSubscription", _Exception);
            }

        }

        //internal bool AddAccountFreeSubscription(long AccountId, long UserAccountTypeId)
        //{
        //    try
        //    {
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == AccountId).FirstOrDefault();
        //            if (AccountDetails != null)
        //            {
        //                var SubscriptionDetails = _HCoreContext.HCSubscription.Where(x => x.AccountTypeId == UserAccountTypeId).OrderBy(x => x.ActualPrice).FirstOrDefault();
        //                if (SubscriptionDetails != null)
        //                {
        //                    AccountDetails.SubscriptionId = SubscriptionDetails.Id;
        //                    _HCUAccountSubscription = new HCUAccountSubscription();
        //                    _HCUAccountSubscription.Guid = HCoreHelper.GenerateGuid();
        //                    _HCUAccountSubscription.TypeId = SubscriptionDetails.TypeId;
        //                    _HCUAccountSubscription.AccountId = AccountId;
        //                    _HCUAccountSubscription.SubscriptionId = SubscriptionDetails.Id;
        //                    _HCUAccountSubscription.StartDate = HCoreHelper.GetGMTDate();
        //                    _HCUAccountSubscription.EndDate = _HCUAccountSubscription.StartDate.AddDays(7);
        //                    _HCUAccountSubscription.RenewDate = _HCUAccountSubscription.StartDate.AddDays(7);
        //                    _HCUAccountSubscription.ActualPrice = SubscriptionDetails.ActualPrice;
        //                    _HCUAccountSubscription.SellingPrice = SubscriptionDetails.SellingPrice;
        //                    _HCUAccountSubscription.CreateDate = HCoreHelper.GetGMTDateTime();
        //                    _HCUAccountSubscription.CreatedById = AccountId;
        //                    _HCUAccountSubscription.StatusId = HelperStatus.Default.Active;
        //                    _HCoreContext.HCUAccountSubscription.Add(_HCUAccountSubscription);
        //                    _HCoreContext.SaveChanges();
        //                    return true;
        //                }
        //                else
        //                {
        //                    return false;
        //                }
        //            }
        //            else
        //            {
        //                return false;
        //            }

        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException("UpdateSubscription", _Exception);
        //        return false;
        //    }

        //}


        /// <summary>
        /// Sends Subscription Remainder
        /// </summary>
        internal void SubscriptionRemainder()
        {
            #region Manage Exception
            try
            {

                using (_HCoreContext = new HCoreContext())
                {
                    DateTime now = HCoreHelper.GetGMTDate();
                    DateTime StartDate = new DateTime(now.Year, now.Month, 1);
                    DateTime EndDate = StartDate.AddMonths(1).AddDays(-1);
                    //DateTime RenewDate = new DateTime(StartDate.AddMonths(1).Year, StartDate.AddMonths(1).Month, 1);
                    //DateTime StartDate = HCoreHelper.GetGMTDate();
                    //DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(3);
                    if ((EndDate - now).Days <= 3) //If last 3 days are left then only remainder should be send.
                    {
                        var _Subscriptions = (from x in _HCoreContext.HCUAccountSubscription
                                              where x.StatusId == HelperStatus.Default.Active
                                              && x.Subscription.Guid == "merchantstoresubscription"
                                              && x.EndDate.Date > now.Date && x.EndDate.Date < EndDate.Date
                                              orderby x.Id descending
                                              select new
                                              {
                                                  EmailAddress = x.Account.EmailAddress,
                                                  EndDate = x.EndDate,
                                                  DisplayName = x.Account.DisplayName,
                                              })
                                              .Distinct()
                                              .ToList();
                        _HCoreContext.Dispose();
                        if (_Subscriptions.Count > 0)
                        {
                            foreach (var _Subscription in _Subscriptions)
                            {
                                int DaysLeft = (_Subscription.EndDate - now).Days;
                                if (DaysLeft == 0)
                                {
                                    var EmailObject = new
                                    {
                                        EmailAddress = _Subscription.EmailAddress,
                                        DisplayName = _Subscription.DisplayName,
                                        Days = DaysLeft,
                                    };
                                    HCoreHelper.BroadCastEmail(MerchantOnboarding.SubscriptionRemindeToday, EmailObject.DisplayName, EmailObject.EmailAddress, EmailObject, null);
                                }
                                else
                                {
                                    var EmailObject = new
                                    {
                                        EmailAddress = _Subscription.EmailAddress,
                                        DisplayName = _Subscription.DisplayName,
                                        Days = DaysLeft,
                                    };
                                    HCoreHelper.BroadCastEmail(MerchantOnboarding.SubscriptionReminderDays, EmailObject.DisplayName, EmailObject.EmailAddress, EmailObject, null);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SubscriptionRemainder", _Exception, null);

            }
            #endregion
        }


        /// <summary>
        /// Description: Method defined to monthly pay subscription
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        ///
        OPayStackResponseData _PayStackResponseData;
        internal OResponse UpdateMonthlySubscription(OSubscription.SubscriptionUpdate.MonthlySubscriptionRequest _Request)
        {
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                if (string.IsNullOrEmpty(_Request.PaymentReference) || _Request.TotalAmount <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1085, TUCCoreResource.CA1085M);
                }
                string Key = string.Empty;
                bool AllowPayment = false;
                string AuthCode = null;
                _PayStackResponseData = new OPayStackResponseData();
                if (_Request.UserReference.CountryId == 1) // NG
                {
                    Key = _AppConfig.PaystackPrivateKey;
                    _PayStackResponseData = PaystackTransaction.GetTransaction(_Request.PaymentReference, Key);
                    if (_PayStackResponseData != null && _PayStackResponseData.status == "success")
                    {
                        // AuthCode = _PayStackResponseData.authorization.authorization_code;
                        AllowPayment = true;
                    }
                    else
                    {
                        AllowPayment = true;
                    }
                }
                if (_Request.UserReference.CountryId == 87) // NG
                {
                    Key = _AppConfig.PaystackPrivateKey;
                    _PayStackResponseData = PaystackTransaction.GetTransaction(_Request.PaymentReference, Key);
                    if (_PayStackResponseData != null && _PayStackResponseData.status == "success")
                    {
                        // AuthCode = _PayStackResponseData.authorization.authorization_code;
                        AllowPayment = true;
                    }
                    else
                    {
                        AllowPayment = true;
                    }
                }
                else // GH
                {
                    //Key = _AppConfig.PaystackGhanaPrivateKey;
                    AllowPayment = true;
                }
                //_PayStackResponseData = PaystackTransaction.GetTransaction(_Request.PaymentReference, Key);
                //if (_PayStackResponseData != null && _PayStackResponseData.status == "success")
                //{
                if (AllowPayment)
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var AccountDetails = _HCoreContext.HCUAccount
                                         .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                         .Select(x => new
                                         {
                                             Id = x.Id,
                                             DisplayName = x.DisplayName,
                                             EmailAddress = x.EmailAddress,
                                             Guid = x.Guid
                                         })
                                         .FirstOrDefault();
                        var SubscriptionDetails = _HCoreContext.HCSubscription.Where(x => x.Guid == "merchantstoresubscription").FirstOrDefault();
                        if (SubscriptionDetails != null)
                        {

                            DateTime now = HCoreHelper.GetGMTDate();
                            DateTime StartDate = new DateTime(now.Year, now.Month, 1);
                            DateTime EndDate = StartDate.AddMonths(1).AddDays(-1);
                            DateTime RenewalDate = new DateTime(now.AddMonths(1).Year, now.AddMonths(1).Month, 1);

                            for (int i = 0; i < _Request.NoOfStores; i++)
                            {
                                _HCUAccountSubscription = new HCUAccountSubscription();
                                _HCUAccountSubscription.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountSubscription.TypeId = SubscriptionDetails.TypeId;
                                _HCUAccountSubscription.AccountId = _Request.ReferenceId;
                                _HCUAccountSubscription.SubscriptionId = SubscriptionDetails.Id;
                                _HCUAccountSubscription.StartDate = now;
                                _HCUAccountSubscription.EndDate = EndDate;
                                _HCUAccountSubscription.RenewDate = RenewalDate;
                                _HCUAccountSubscription.ActualPrice = _Request.AmountPerStore;
                                _HCUAccountSubscription.SellingPrice = _Request.AmountPerStore;
                                _HCUAccountSubscription.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountSubscription.CreatedById = _Request.ReferenceId;
                                _HCUAccountSubscription.StatusId = HelperStatus.Default.Active;
                                _HCUAccountSubscription.PaymentReference = _Request.PaymentReference;
                                _HCUAccountSubscription.TransactionId = _Request.TransactionId;
                                _HCUAccountSubscription.Value = Helpers.ActivityStatusType.Unused;
                                _HCUAccountSubscription.AuthorizationCode = AuthCode;
                                _HCoreContext.HCUAccountSubscription.Add(_HCUAccountSubscription);
                                _HCoreContext.SaveChanges();
                            }

                            OCoreTransaction.Request _CoreTransactionRequest = new OCoreTransaction.Request();
                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                            _CoreTransactionRequest.ParentId = _Request.ReferenceId;
                            _CoreTransactionRequest.InvoiceAmount = _Request.TotalAmount;
                            _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.TotalAmount;
                            _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                            _CoreTransactionRequest.ReferenceAmount = _Request.TotalAmount;
                            _CoreTransactionRequest.PaymentMethodId = PaymentMethod.Bank;
                            List<OCoreTransaction.TransactionItem> _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = _Request.ReferenceId,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionType.MerchantCredit,
                                SourceId = TransactionSource.Merchant,
                                Amount = _Request.TotalAmount,
                                Comission = 0,
                                Comment = "Subscription Amount Credit to Wallet",
                                TotalAmount = _Request.TotalAmount,
                            });
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = _Request.ReferenceId,
                                ModeId = TransactionMode.Debit,
                                TypeId = TransactionType.MerchantCredit,
                                SourceId = TransactionSource.Merchant,
                                Amount = _Request.TotalAmount,
                                Comission = 0,
                                Comment = "Subscription Amount Debit from Wallet",
                                TotalAmount = _Request.TotalAmount,
                            });
                            _CoreTransactionRequest.Transactions = _TransactionItems;
                            ManageCoreTransaction _ManageCoreTransaction = new ManageCoreTransaction();
                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                            {
                                var EmailObject = new
                                {
                                    EmailAddress = AccountDetails.EmailAddress,
                                    DisplayName = AccountDetails.DisplayName,
                                };

                                HCoreHelper.BroadCastEmail(MerchantOnboarding.SubscriptionConfirmed, EmailObject.DisplayName, EmailObject.EmailAddress, EmailObject, _Request.UserReference);

                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1104, TUCCoreResource.CA1104M);
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1103, TUCCoreResource.CA1103M);
                                #endregion
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1103, TUCCoreResource.CA1103M);
                        }
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "PR2123", "Transaction details not found");
                }


            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateMonthlySubscription", _Exception);
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                #endregion
            }

        }

        /// <summary>
        /// Description: Method defined to get list of account monthly subscriptions
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetAccountSubscriptionStorewise(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ActualPrice", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreatedDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCUAccountSubscription.
                                                     Where(x => x.Account.Id == _Request.AccountId
                                                     && x.Account.Guid == _Request.AccountKey
                                                     && x.StatusId == Default.Active
                                                     && x.Subscription.Guid == "merchantstoresubscription")
                                                     .Select(x => new OSubscription.AccountSubscription
                                                     {
                                                         ActualPrice = x.ActualPrice,
                                                         SubscriptionId = x.SubscriptionId,
                                                         StartDate = x.StartDate,
                                                         EndDate = x.EndDate,
                                                         RenewDate = x.RenewDate,
                                                         AccountId = x.CreatedById,
                                                         AccountDisplayName = x.CreatedBy.DisplayName,
                                                         AccountKey = x.CreatedBy.Guid,
                                                         TransactionId = x.TransactionId,
                                                         PaymentReference = x.PaymentReference,
                                                         CreatedDate = x.CreateDate.AddSeconds(-x.CreateDate.Second),
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name
                                                         //NoOfStores = x.Value                                                         
                                                     })
                                                     .Distinct()
                                                     .Where(_Request.SearchCondition)
                                                     .Count();
                    }
                    #endregion
                    #region Get Data
                    List<OSubscription.AccountSubscription> _Subscriptions = new List<OSubscription.AccountSubscription>();
                    _Subscriptions = _HCoreContext.HCUAccountSubscription.
                                                 Where(x => x.Account.Id == _Request.AccountId
                                                 && x.Account.Guid == _Request.AccountKey
                                                 && x.StatusId == Default.Active
                                                 && x.Subscription.Guid == "merchantstoresubscription")
                                                 .Select(x => new OSubscription.AccountSubscription
                                                 {
                                                     ActualPrice = x.ActualPrice,
                                                     SubscriptionId = x.SubscriptionId,
                                                     StartDate = x.StartDate,
                                                     EndDate = x.EndDate,
                                                     RenewDate = x.RenewDate,
                                                     AccountId = x.CreatedById,
                                                     AccountDisplayName = x.CreatedBy.DisplayName,
                                                     AccountKey = x.CreatedBy.Guid,
                                                     TransactionId = x.TransactionId,
                                                     PaymentReference = x.PaymentReference,
                                                     CreatedDate = x.CreateDate.AddSeconds(-x.CreateDate.Second),
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                     //NoOfStores = x.Value                                                         
                                                 })
                                                 .Distinct()
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();

                    #endregion
                    #region Create  Response Object     
                    foreach (var _DataItem in _Subscriptions)
                    {
                        _DataItem.NoOfStores = _HCoreContext.HCUAccountSubscription.Where(x => x.PaymentReference == _DataItem.PaymentReference)
                                                .Count();
                        _DataItem.ActualPrice = _DataItem.NoOfStores * _DataItem.ActualPrice;
                        var idList = _HCoreContext.HCUAccountSubscription.Where(x => x.PaymentReference == _DataItem.PaymentReference)
                                                  .Select(x => new { Id = x.Id }).ToList();
                        if (idList.Count() > 0)
                        {
                            List<OSubscription.Stores> _lst = new List<OSubscription.Stores>();
                            foreach (var id in idList)
                            {
                                OSubscription.Stores stores = _HCoreContext.HCUAccount
                                            .Where(x => x.SubscriptionId == id.Id)
                                            .Select(x => new OSubscription.Stores
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                DisplayName = x.DisplayName
                                            }).FirstOrDefault();
                                if (stores != null)
                                {
                                    _lst.Add(stores);
                                }
                            }
                            _DataItem.Stores = _lst;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Subscriptions, _Request.Offset, _Request.Limit);
                    _OResponse.Total = _HCoreContext.HCUAccountSubscription.Where(x => x.AccountId == _Request.AccountId
                                                                        && x.Subscription.Guid == "merchantstoresubscription"
                                                                        && x.StatusId == HelperStatus.Default.Active
                                                                        && x.EndDate.Date >= HCoreHelper.GetGMTDate().Date).Count();
                    _OResponse.Inactive = _HCoreContext.HCUAccountSubscription
                                            .Where(x => x.AccountId == _Request.AccountId
                                            && x.Subscription.Guid == "merchantstoresubscription"
                                            && x.Value == Helpers.ActivityStatusType.Unused
                                            && x.EndDate.Date >= HCoreHelper.GetGMTDate().Date
                                            && x.StatusId == HelperStatus.Default.Active).Count();
                    _OResponse.Active = _HCoreContext.HCUAccountSubscription.Where(x => x.AccountId == _Request.AccountId
                                         && x.Value == Helpers.ActivityStatusType.Active
                                         && x.EndDate.Date >= HCoreHelper.GetGMTDate().Date
                                         && x.StatusId == HelperStatus.Default.Active).Count();
                    _HCoreContext.Dispose();
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    #endregion

                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAccountSubscriptionStorewise", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method to get amount paid to paystack for subscription monthly
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetAmountMonthly(OReference _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {

                    var SubscriptionDetails = _HCoreContext.HCSubscription.Where(x => x.Guid == "merchantstoresubscription").FirstOrDefault();
                    DateTime now = HCoreHelper.GetGMTDate();
                    DateTime StartDate = new DateTime(now.Year, now.Month, 1);
                    DateTime EndDate = StartDate.AddMonths(1).AddDays(-1);
                    DateTime RenewalDate = new DateTime(now.AddMonths(1).Year, now.AddMonths(1).Month, 1);
                    double NoOfDays = Convert.ToDouble((EndDate - StartDate).TotalDays) + 1;
                    double ActualChargableDays = Convert.ToDouble((EndDate - now).TotalDays) + 1;
                    double PerDayCharge = (Convert.ToDouble(SubscriptionDetails.SellingPrice) / NoOfDays);
                    double ActualAmount = Math.Round(PerDayCharge * ActualChargableDays);
                    var details = new
                    {
                        Amount = ActualAmount,
                        Description = "Amount per store",
                        NoOfDays = NoOfDays,
                        ActualChargableDays = ActualChargableDays
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAmountMonthly", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to update subscription store
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateSubscriptionStoreWise(OSubscription.SubscriptionUpdate.MonthlySubscriptionRequest _Request)
        {
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                int count = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    var Unused = _HCoreContext.HCUAccountSubscription
                                .Where(x => x.AccountId == _Request.AccountId
                                 && x.Value == Helpers.ActivityStatusType.Unused)
                                 .ToList();

                    foreach (var stores in _Request.Stores)
                    {
                        using (HCoreContext hc = new HCoreContext())
                        {
                            var AccSub = _HCoreContext.HCUAccount.Where(x => x.Id == stores.ReferenceId).FirstOrDefault();
                            if (AccSub != null)
                            {
                                AccSub.SubscriptionId = Unused[count].Id;
                                AccSub.ModifyById = _Request.AccountId;
                                AccSub.ModifyDate = HCoreHelper.GetGMTDate();
                                Unused[count].Value = Helpers.ActivityStatusType.Active;
                                Unused[count].ModifyById = _Request.AccountId;
                                Unused[count].ModifyDate = HCoreHelper.GetGMTDate();
                                hc.SaveChanges();
                            }
                            hc.Dispose();
                        }
                        count++;
                    }
                    if (Unused.Count() > 0)
                    {
                        _HCoreContext.HCUAccountSubscription.UpdateRange(Unused);
                        _HCoreContext.SaveChanges();
                    }
                }

                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1534, TUCCoreResource.CA1534M);
                #endregion

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateSubscriptionStoreWise", _Exception);
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }

        }

        /// <summary>
        /// Description: Method defined to get list of stores who are not subscribed.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetStoreNotSubscribed(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    List<OSubscription.Stores> _Stores = new List<OSubscription.Stores>();
                    _Stores = _HCoreContext.HCUAccount.
                                                 Where(x => x.OwnerId == _Request.AccountId
                                                 && x.Owner.Guid == _Request.AccountKey
                                                 && x.AccountTypeId == UserAccountType.MerchantStore
                                                 && x.StatusId == Default.Active
                                                 && x.SubscriptionId == null)
                                                 .Select(x => new OSubscription.Stores
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     DisplayName = x.DisplayName
                                                 })
                                                 .Where(_Request.SearchCondition)
                                                 .ToList();
                    _Request.TotalRecords = _Stores.Count();

                    #endregion
                    #region Create  Response Object                        
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Stores, _Request.Offset, _Request.Limit);
                    _HCoreContext.Dispose();
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    #endregion

                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAccountSubscriptionStorewise", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Auto renewal of Subscription
        /// </summary>
        internal void SubscriptionAutoRenewal()
        {
            #region Manage Exception
            try
            {
                DateTime now = HCoreHelper.GetGMTDate();
                DateTime StartDate = new DateTime(now.Year, now.Month, 1);
                if (now.Date == StartDate.Date) //1st Date of the Month then only renewal process should be done.
                {
                    DateTime EndDate = StartDate.AddMonths(1).AddDays(-1);
                    DateTime RenewDate = new DateTime(StartDate.AddMonths(1).Year, StartDate.AddMonths(1).Month, 1);
                    using (_HCoreContext = new HCoreContext())
                    {
                        var _Subscriptions = (from x in _HCoreContext.HCUAccountSubscription
                                              where x.StatusId == HelperStatus.Default.Active
                                              && x.Subscription.Guid == "merchantstoresubscription"
                                              && x.RenewDate.Date == StartDate.Date
                                              && x.AuthorizationCode != null
                                              orderby x.Id descending
                                              select new
                                              {
                                                  EmailAddress = x.Account.EmailAddress,
                                                  EndDate = x.EndDate,
                                                  DisplayName = x.Account.DisplayName,
                                                  Amount = x.ActualPrice,
                                                  PaymentReference = x.PaymentReference,
                                                  TransactionId = x.TransactionId,
                                                  AuthorizationCode = x.AuthorizationCode
                                              })
                                              .Distinct()
                                              .ToList();

                        if (_Subscriptions.Count > 0)
                        {
                            foreach (var _Subscription in _Subscriptions)
                            {
                                OChargeData _PayStackResponseData = PaystackTransaction.GetPayStackRecuringCharge(_AppConfig.PaystackPrivateKey, _Subscription.EmailAddress, (double)_Subscription.Amount, _Subscription.AuthorizationCode);
                                if (_PayStackResponseData.status == true)
                                {
                                    if (_PayStackResponseData.data.status == "success")
                                    {
                                        var details = _HCoreContext.HCUAccountSubscription
                                                      .Where(x => x.TransactionId == _Subscription.TransactionId)
                                                      .ToList();
                                        details.ToList().ForEach(u =>
                                        {
                                            u.RenewDate = RenewDate;
                                            u.ModifyDate = now;
                                            u.StartDate = StartDate;
                                            u.EndDate = EndDate;
                                        });
                                        _HCoreContext.SaveChanges();

                                        var EmailObject = new
                                        {
                                            EmailAddress = _Subscription.EmailAddress,
                                            DisplayName = _Subscription.DisplayName,
                                        };
                                        HCoreHelper.BroadCastEmail(MerchantOnboarding.SubscriptionConfirmed, EmailObject.DisplayName, EmailObject.EmailAddress, EmailObject, null);
                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SubscriptionRemainder", _Exception, null);
            }
            #endregion
        }
    }
}
