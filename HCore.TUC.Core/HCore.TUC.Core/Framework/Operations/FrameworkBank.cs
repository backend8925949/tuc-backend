//==================================================================================
// FileName: FrameworkBank.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to bank
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Akka.Actor;
using Amazon.S3.Model;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Integration.Paystack;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using Microsoft.EntityFrameworkCore;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
namespace HCore.TUC.Core.Framework.Operations
{
    public class FrameworkBank
    {
        HCoreContext? _HCoreContext;
        HCUAccountBank? _HCUAccountBank;
        List<OBank.BankCode>? _BankCodes;
        List<OBank.ListDownload>? _ListDownload;

        /// <summary>
        /// Description: Method defined to get list of bank codes
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal async Task<OResponse> GetBankCode(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    List<OBank.BankCode> _List = await _HCoreContext.HCCoreCommon
                                            .Where(x => x.CountryId == _Request.CountryId
                                            && x.Type.SystemName == "hcore.banklist"
                                            && x.StatusId == HelperStatus.Default.Active)
                                            .Select(x => new OBank.BankCode
                                            {
                                                Name = x.Name,
                                                TName = x.Name.ToLower(),
                                                SystemName = x.SystemName,
                                                Code = x.Value,
                                                ReferenceId = Convert.ToInt64(x.SubValue)
                                            })
                                         //.Where(_Request.SearchCondition)
                                         //.OrderBy(_Request.SortExpression)
                                         .Skip(_Request.Offset)
                                         .Take(_Request.Limit)
                                         .ToListAsync();
                    #endregion
                    #region Create  Response Object
                    await _HCoreContext.DisposeAsync();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                //string Key = string.Empty;
                //if (HostEnvironment == HostEnvironmentType.Live)
                //{
                //    if (_Request.UserReference.CountryId == 1) // NG
                //    {
                //        Key = _AppConfig.PaystackPrivateKey;
                //    }
                //    else // GH
                //    {
                //        Key = _AppConfig.PaystackGhanaPrivateKey;
                //    }
                //}
                //else
                //{
                //    if (_Request.UserReference.CountryId == 1) // NG
                //    {
                //        Key = _AppConfig.PaystackPrivateKey;
                //    }
                //    else // GH
                //    {
                //        Key = _AppConfig.PaystackGhanaPrivateKey;
                //    }
                //}
                //var Response = PaystackTransfer.GetBanksList(Key);
                //if (Response != null && Response.Count > 0)
                //{
                //    _BankCodes = new List<OBank.BankCode>();
                //    foreach (var ResponseItem in Response)
                //    {
                //        _BankCodes.Add(new OBank.BankCode
                //        {
                //            Name = ResponseItem.name,
                //            TName = ResponseItem.name.ToLower(),
                //            SystemName = ResponseItem.slug,
                //            Code = ResponseItem.code,
                //            LongCode = ResponseItem.longcode,
                //            Gateway = ResponseItem.gateway,
                //            //PayWithBank = ResponseItem.pay_with_bank,
                //            //Active = ResponseItem.active,
                //            //IsDeleted = ResponseItem.is_deleted,
                //            Country = ResponseItem.country,
                //            Currency = ResponseItem.currency,
                //            Type = ResponseItem.type,
                //            ReferenceId = ResponseItem.id
                //        });
                //    }
                //    if (_Request.TypeId == 1)
                //    {
                //        if (!string.IsNullOrEmpty(_Request.SearchCondition))
                //        {
                //            _Request.SearchCondition = _Request.SearchCondition.Replace("Name.Contains", "TName.Contains");
                //            _Request.SearchCondition = _Request.SearchCondition.ToLower().Replace("tname.contains", "TName.Contains");
                //            var Codes = _BankCodes.AsQueryable().Where(_Request.SearchCondition).ToList();
                //            OList.Response _OResponse = HCoreHelper.GetListResponse(Codes.Count, Codes.OrderBy(x => x.Name), 0, Codes.Count);
                //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                //        }
                //        else
                //        {
                //            OList.Response _OResponse = HCoreHelper.GetListResponse(_BankCodes.Count, _BankCodes.OrderBy(x => x.Name), 0, _BankCodes.Count);
                //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                //        }
                //    }
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BankCodes.OrderBy(x => x.Name), TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                //}
                //else
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1285, TUCCoreResource.CA1285M);
                //}

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetBankCode", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to save bank account
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal async Task<OResponse> SaveBankAccount(OBank.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.AccountNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1278, TUCCoreResource.CA1278M);
                }
                if (string.IsNullOrEmpty(_Request.BankCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1279, TUCCoreResource.CA1279M);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.AccountId == 0)
                        {
                            _Request.AccountId = _Request.UserReference.AccountId;
                            _Request.AccountKey = _Request.UserReference.AccountKey;
                        }
                        var AccountDetails = await _HCoreContext.HCUAccount
                            .Where(x => x.Id == _Request.AccountId
                            && x.Guid == _Request.AccountKey)
                            .FirstOrDefaultAsync();
                        if (AccountDetails != null)
                        {
                            if (AccountDetails.StatusId == HelperStatus.Default.Active)
                            {
                                var ValidateBankDetails = await _HCoreContext.HCUAccountBank.AnyAsync(x => x.AccountId == _Request.AccountId
                                && x.AccountNumber == _Request.AccountNumber);
                                if (ValidateBankDetails)
                                {
                                    await _HCoreContext.DisposeAsync();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1282, TUCCoreResource.CA1282M);
                                }
                                else
                                {
                                    await _HCoreContext.DisposeAsync();
                                }
                                var BankDetails = PaystackTransfer.VerifyAccountNumber(_AppConfig.PaystackPrivateKey, _Request.AccountNumber, _Request.BankCode);
                                if (BankDetails != null)
                                {
                                    var RecepientBankDetails = PaystackTransfer.CreateRecepient(_AppConfig.PaystackPrivateKey, BankDetails.account_name, BankDetails.account_number, _Request.BankCode);
                                    if (RecepientBankDetails != null)
                                    {
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            _HCUAccountBank = new HCUAccountBank();
                                            _HCUAccountBank.Guid = HCoreHelper.GenerateGuid();
                                            _HCUAccountBank.AccountId = _Request.AccountId;
                                            _HCUAccountBank.Name = BankDetails.account_name;
                                            _HCUAccountBank.AccountNumber = BankDetails.account_number;
                                            _HCUAccountBank.BankName = _Request.BankName;
                                            _HCUAccountBank.BankCode = _Request.BankCode;
                                            _HCUAccountBank.BankId = Convert.ToInt64(BankDetails.bank_id);
                                            _HCUAccountBank.ReferenceCode = RecepientBankDetails.recipient_code;
                                            _HCUAccountBank.ReferenceKey = RecepientBankDetails.id;
                                            _HCUAccountBank.BvnNumber = _Request.BvnNumber;
                                            _HCUAccountBank.Comment = _Request.Comment;
                                            _HCUAccountBank.CreateDate = HCoreHelper.GetGMTDateTime();
                                            _HCUAccountBank.CreatedById = _Request.UserReference.AccountId;
                                            _HCUAccountBank.StatusId = HelperStatus.Default.Active;
                                            await _HCoreContext.HCUAccountBank.AddAsync(_HCUAccountBank);
                                            await _HCoreContext.SaveChangesAsync();
                                            await _HCoreContext.DisposeAsync();
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1283, TUCCoreResource.CA1283M);
                                        }
                                    }
                                    else
                                    {
                                        await _HCoreContext.DisposeAsync();
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1281, TUCCoreResource.CA1281M);
                                    }
                                }
                                else
                                {
                                    await _HCoreContext.DisposeAsync();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1281, TUCCoreResource.CA1281M);
                                }
                            }
                            else
                            {
                                await _HCoreContext.DisposeAsync();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1257, TUCCoreResource.CA1257M);
                            }
                        }
                        else
                        {
                            await _HCoreContext.DisposeAsync();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
                        }
                    }
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "SaveBankAccount", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to update user's bank account details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal async Task<OResponse> UpdateBankAccount(OBank.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId == 0)
                    {
                        _Request.AccountId = _Request.UserReference.AccountId;
                        _Request.AccountKey = _Request.UserReference.AccountKey;
                    }
                    var AccountDetails = await _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.AccountId
                        && x.Guid == _Request.AccountKey)
                        .FirstOrDefaultAsync();
                    if (AccountDetails != null)
                    {
                        if (AccountDetails.StatusId == HelperStatus.Default.Active)
                        {
                            var BankDetails = PaystackTransfer.VerifyAccountNumber(_AppConfig.PaystackPrivateKey, _Request.AccountNumber, _Request.BankCode);
                            if (BankDetails != null)
                            {
                                var RecepientBankDetails = PaystackTransfer.CreateRecepient(_AppConfig.PaystackPrivateKey, BankDetails.account_name, BankDetails.account_number, _Request.BankCode);
                                if (RecepientBankDetails != null)
                                {
                                    var UserBankDetails = await _HCoreContext.HCUAccountBank.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                                    if (UserBankDetails != null)
                                    {
                                        if(!string.IsNullOrEmpty(BankDetails.account_name) && UserBankDetails.Name != BankDetails.account_name)
                                        {
                                            UserBankDetails.Name = BankDetails.account_name;
                                        }
                                        if (!string.IsNullOrEmpty(BankDetails.account_number) && UserBankDetails.AccountNumber != BankDetails.account_number)
                                        {
                                            UserBankDetails.AccountNumber = BankDetails.account_number;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.BankName) && UserBankDetails.BankName != _Request.BankName)
                                        {
                                            UserBankDetails.BankName = _Request.BankName;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.BankCode) && UserBankDetails.BankCode != _Request.BankCode)
                                        {
                                            UserBankDetails.BankCode = _Request.BankCode;
                                        }
                                        if (!string.IsNullOrEmpty(BankDetails.bank_id) && UserBankDetails.BankId != Convert.ToInt64(BankDetails.bank_id))
                                        {
                                            UserBankDetails.BankId = Convert.ToInt64(BankDetails.bank_id);
                                        }
                                        if (!string.IsNullOrEmpty(RecepientBankDetails.recipient_code) && UserBankDetails.ReferenceCode != RecepientBankDetails.recipient_code)
                                        {
                                            UserBankDetails.ReferenceCode = RecepientBankDetails.recipient_code;
                                        }
                                        if (!string.IsNullOrEmpty(RecepientBankDetails.id) && UserBankDetails.ReferenceKey != RecepientBankDetails.id)
                                        {
                                            UserBankDetails.ReferenceKey = RecepientBankDetails.id;
                                        }

                                        if(_Request.IsPrimaryAccount == true)
                                        {
                                            var UserBanks = await _HCoreContext.HCUAccountBank.Where(x => x.AccountId == _Request.AccountId).ToListAsync();
                                            if(UserBanks.Count > 0)
                                            {
                                                foreach(var Bank in UserBanks)
                                                {
                                                    Bank.IsPrimary = 0;
                                                }
                                            }

                                            UserBankDetails.IsPrimary = 1;
                                        }
                                        else
                                        {
                                            UserBankDetails.IsPrimary = 0;
                                        }

                                        UserBankDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        UserBankDetails.ModifyById = _Request.UserReference.AccountId;
                                        await _HCoreContext.SaveChangesAsync();
                                        await _HCoreContext.DisposeAsync();

                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "BA003", "bank account details updated successfully");
                                    }
                                    else
                                    {
                                        await _HCoreContext.DisposeAsync();
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1281, TUCCoreResource.CA1281M);
                                    }
                                }
                                else
                                {
                                    await _HCoreContext.DisposeAsync();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1281, TUCCoreResource.CA1281M);
                                }
                            }
                            else
                            {
                                await _HCoreContext.DisposeAsync();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1281, TUCCoreResource.CA1281M);
                            }
                        }
                        else
                        {
                            await _HCoreContext.DisposeAsync();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1257, TUCCoreResource.CA1257M);
                        }
                    }
                    else
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "UpdateBankAccount", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to delete bank account
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal async Task<OResponse> DeleteBankAccount(OBank.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                else
                {
                    if (_Request.AccountId == 0)
                    {
                        _Request.AccountId = _Request.UserReference.AccountId;
                        _Request.AccountKey = _Request.UserReference.AccountKey;
                    }

                    using (_HCoreContext = new HCoreContext())
                    {
                        var AccountDetails = await _HCoreContext.HCUAccount
                            .Where(x => x.Id == _Request.AccountId
                            && x.Guid == _Request.AccountKey)
                            .FirstOrDefaultAsync();
                        if (AccountDetails != null)
                        {
                            if (AccountDetails.StatusId == HelperStatus.Default.Active)
                            {
                                var BankDetails = await _HCoreContext.HCUAccountBank.Where(x => x.AccountId == _Request.AccountId
                                && x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                                if (BankDetails == null)
                                {
                                    await _HCoreContext.DisposeAsync();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                                }
                                _HCoreContext.HCUAccountBank.Remove(BankDetails);
                                await _HCoreContext.SaveChangesAsync();
                                await _HCoreContext.DisposeAsync();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1284, TUCCoreResource.CA1284M);
                            }
                            else
                            {
                                await _HCoreContext.DisposeAsync();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1257, TUCCoreResource.CA1257M);
                            }
                        }
                        else
                        {
                            await _HCoreContext.DisposeAsync();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1156M);
                        }
                    }
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "DeleteBankAccount", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get list of bank accounts
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal async Task<OResponse> GetBankAccount(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetBankAccountDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetBankAccountDownload>("ActorGetBankAccountDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId == 0)
                    {
                        _Request.AccountId = _Request.UserReference.AccountId;
                        _Request.AccountKey = _Request.UserReference.AccountKey;
                    }
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = await _HCoreContext.HCUAccountBank
                            .Where(x => x.AccountId == _Request.AccountId
                            && x.Account.Guid == _Request.AccountKey
                            && x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new OBank.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    BankName = x.BankName,
                                                    BankCode = x.BankCode,
                                                    BankId = x.BankId,
                                                    BankReferenceCode = x.ReferenceCode,
                                                    BankReferenceKey = x.ReferenceKey,
                                                    BvnNumber = x.BvnNumber,
                                                    Comment = x.Comment,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    CreateDate = x.CreateDate,
                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    IsPrimary = x.IsPrimary
                                                })
                                               .Where(_Request.SearchCondition)
                                       .CountAsync();
                        #endregion
                    }
                    #region Get Data
                    List<OBank.List> _List = await _HCoreContext.HCUAccountBank
                           .Where(x => x.AccountId == _Request.AccountId
                            && x.Account.Guid == _Request.AccountKey
                            && x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new OBank.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    BankName = x.BankName,
                                                    BankCode = x.BankCode,
                                                    BankId = x.BankId,
                                                    BankReferenceCode = x.ReferenceCode,
                                                    BankReferenceKey = x.ReferenceKey,
                                                    BvnNumber = x.BvnNumber,
                                                    Comment = x.Comment,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    CreateDate = x.CreateDate,
                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    IsPrimary = x.IsPrimary
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToListAsync();
                    //List<OBank.List> _List2 = _HCoreContext.HCUAccountBank.Where(x => x.AccountId == _Request.AccountId)
                    //                           .Select(x => new OBank.List
                    //                           {
                    //                               ReferenceId = x.Id,
                    //                               ReferenceKey = x.Guid,
                    //                               Name = x.Name,
                    //                               AccountNumber = x.AccountNumber,
                    //                               BankName = x.BankName,
                    //                           }).ToList();
                    //var TotalAccount = _List.Union(_List2);
                    //_Request.TotalRecords = _Request.TotalRecords + _List2.Count;
                    foreach(var BankAccount in _List)
                    {
                        if(BankAccount.IsPrimary == 1)
                        {
                            BankAccount.IsPrimaryAccount = true;
                        }
                        else
                        {
                            BankAccount.IsPrimaryAccount = false;
                        }
                    }
                    #endregion

                    await _HCoreContext.DisposeAsync();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetBankAccount", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get details of bank account
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal async Task<OResponse> GetBankAccount(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var UserBankDetails = await _HCoreContext.HCUAccountBank.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                          .Select(x => new OBank.Details
                                          {
                                              ReferenceId = x.Id,
                                              ReferenceKey = x.Guid,
                                              Name = x.Name,
                                              AccountNumber = x.AccountNumber,
                                              BankName = x.BankName,
                                              BankCode = x.BankCode,
                                              BankId = x.BankId,
                                              BankReferenceCode = x.ReferenceCode,
                                              BankReferenceKey = x.ReferenceKey,
                                              BvnNumber = x.BvnNumber,
                                              Comment = x.Comment,
                                              StatusCode = x.Status.SystemName,
                                              StatusName = x.Status.Name,
                                              CreateDate = x.CreateDate,
                                              AccountId = x.AccountId,
                                              AccountKey = x.Account.Guid,
                                              AccountDisplayName = x.Account.DisplayName,
                                              IsPrimary = x.IsPrimary
                                          }).FirstOrDefaultAsync();

                    if(UserBankDetails != null)
                    {
                        if (UserBankDetails.IsPrimary == 1)
                        {
                            UserBankDetails.IsPrimaryAccount = true;
                        }
                        else
                        {
                            UserBankDetails.IsPrimaryAccount = false;
                        }
                    }

                    await _HCoreContext.DisposeAsync();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, UserBankDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetBankAccount", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }

        /// <summary>
        /// Description: Method defined to download list of  bank account
        /// </summary>
        /// <param name="_Request"></param>
        internal async Task GetBankAccountDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Limit = 800;
                _ListDownload = new List<OBank.ListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId == 0)
                    {
                        _Request.AccountId = _Request.UserReference.AccountId;
                        _Request.AccountKey = _Request.UserReference.AccountKey;
                    }
                    #region Total Records
                    _Request.TotalRecords = await _HCoreContext.HCUAccountBank
                        .Where(x => x.AccountId == _Request.AccountId
                        && x.Account.Guid == _Request.AccountKey
                        && x.StatusId == HelperStatus.Default.Active)
                                            .Select(x => new OBank.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                Name = x.Name,
                                                AccountNumber = x.AccountNumber,
                                                BankName = x.BankName,
                                                BankCode = x.BankCode,
                                                BankId = x.BankId,
                                                BankReferenceCode = x.ReferenceCode,
                                                BankReferenceKey = x.ReferenceKey,
                                                BvnNumber = x.BvnNumber,
                                                Comment = x.Comment,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,
                                                CreateDate = x.CreateDate,
                                                IsPrimary = x.IsPrimary
                                            })
                                           .Where(_Request.SearchCondition)
                                   .CountAsync();
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = await _HCoreContext.HCUAccountBank
                           .Where(x => x.AccountId == _Request.AccountId
                            && x.Account.Guid == _Request.AccountKey
                            && x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new OBank.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    AccountNumber = x.AccountNumber,
                                                    BankName = x.BankName,
                                                    BankCode = x.BankCode,
                                                    BankId = x.BankId,
                                                    BankReferenceCode = x.ReferenceCode,
                                                    BankReferenceKey = x.ReferenceKey,
                                                    BvnNumber = x.BvnNumber,
                                                    Comment = x.Comment,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    CreateDate = x.CreateDate,
                                                    IsPrimary = x.IsPrimary
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToListAsync();

                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            _ListDownload.Add(new OBank.ListDownload
                            {
                                Name = _DataItem.Name,
                                AccountNumber = _DataItem.AccountNumber,
                                BankName = _DataItem.BankName,
                                BvnNumber = _DataItem.BvnNumber,
                                BankCode = _DataItem.BankCode,
                                CreatedOn = _DataItem.CreateDate,
                                StatusName = _DataItem.StatusName
                            });

                            if (_DataItem.IsPrimary == 1)
                            {
                                _DataItem.IsPrimaryAccount = true;
                            }
                            else
                            {
                                _DataItem.IsPrimaryAccount = false;
                            }
                        }
                        _Request.Offset += 800;
                    }

                    await _HCoreContext.DisposeAsync();
                    HCoreHelper.CreateDownload("Bank_List", _ListDownload, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetBankAccountDownload", _Exception);
            }
            #endregion
        }

        /// <summary>
        /// Description: This method saves customer bank account
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal async Task<OResponse> QuickSaveBankAccount(OBank.QuickSave _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.AccountNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1278, TUCCoreResource.CA1278M);
                }
                if (string.IsNullOrEmpty(_Request.BankName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1279, TUCCoreResource.CB007N);
                }
                if (string.IsNullOrEmpty(_Request.AccountName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1279, TUCCoreResource.CB007A);
                }
                else
                {
                    using (HCoreContext context = new HCoreContext())
                    {
                        var AccountDetails = await context.HCUAccount.Where(x => x.Id == _Request.UserId && x.Guid == _Request.AccountKey).FirstOrDefaultAsync();
                        if (AccountDetails != null)
                        {
                            if (AccountDetails.StatusId == HelperStatus.Default.Active)
                            {
                                HCUAccountBank hcubank = new HCUAccountBank()
                                {
                                    Guid = HCoreHelper.GenerateGuid(),
                                    AccountId = _Request.UserId,
                                    Name = _Request.AccountName,
                                    BankName = _Request.BankName,
                                    AccountNumber = _Request.AccountNumber,
                                    CreateDate = HCoreHelper.GetGMTDateTime(),
                                    CreatedById = _Request.UserReference.AccountId,
                                    StatusId = HelperStatus.Default.Active,
                                    BankCode = _Request.BankCode,
                                };
                                await context.HCUAccountBank.AddAsync(hcubank);
                                await context.SaveChangesAsync();
                                await context.DisposeAsync();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1283, TUCCoreResource.CA1283M);
                            }
                            else
                            {
                                await context.DisposeAsync();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1257, TUCCoreResource.CA1257M);
                            }
                        }
                        else
                        {
                            await context.DisposeAsync();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1156, TUCCoreResource.CA1072M);
                        }
                    }
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "QuickSaveBankAccount", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }

    internal class ActorGetBankAccountDownload : ReceiveActor
    {
        public ActorGetBankAccountDownload()
        {
            FrameworkBank? _FrameworkBank;
            ReceiveAsync<OList.Request>(async _Request =>
            {
                _FrameworkBank = new FrameworkBank();
                await _FrameworkBank.GetBankAccountDownload(_Request);
            });
        }
    }
}
