//==================================================================================
// FileName: FrameworkAppPromotion.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to app promotions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Core.Framework.Operations
{
    public class FrameworkAppPromotion
    {
        List<OAppPromotion.Slider> _Sliders;
        HCoreContext _HCoreContext;
        TUCAppPromotion _TUCAppPromotion;

        /// <summary>
        /// Description: Method defined to save app promotion
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveAppPromotion(OAppPromotion.Save.Request _Request)
        {
            try
            {
                if (_Request.ImageContent == null )
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14128, TUCCoreResource.CAA14128M);
                }
                if (string.IsNullOrEmpty(_Request.NavigationType))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14129, TUCCoreResource.CAA14129M);
                }
                if (_Request.StartDate == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14130, TUCCoreResource.CAA14130M);
                }
                if (_Request.EndDate == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14131, TUCCoreResource.CAA14131M);
                }
                int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    long? referenceId = _HCoreContext.HCCoreStorage.Where(x => x.Guid == _Request.ImageReference).Select(x => x.Id).FirstOrDefault();
                    _TUCAppPromotion = new TUCAppPromotion();
                    _TUCAppPromotion.Guid = HCoreHelper.GenerateGuid();
                    _TUCAppPromotion.AppId = 1;
                    _TUCAppPromotion.CountryId = _Request.UserReference.CountryId;
                    if (_Request.NavigationType == "app")
                    {
                        _TUCAppPromotion.NavigationType = "app";
                    }
                    else 
                    {
                        _TUCAppPromotion.NavigationType = "url";
                    }
                    _TUCAppPromotion.NavigateUrl = _Request.NavigateUrl;
                    _TUCAppPromotion.NavigationData =  _Request.NavigationData;
                    _TUCAppPromotion.StartDate = HCoreHelper.ConvertToUTC(_Request.StartDate, _Request.UserReference.CountryTimeZone);
                    _TUCAppPromotion.EndDate = HCoreHelper.ConvertToUTC(_Request.EndDate, _Request.UserReference.CountryTimeZone);
                    _TUCAppPromotion.CreateDate = HCoreHelper.GetGMTDateTime();
                    _TUCAppPromotion.CreatedById = _Request.UserReference.AccountId;
                    _TUCAppPromotion.StatusId = HelperStatus.Default.Active;
                    _HCoreContext.TUCAppPromotion.Add(_TUCAppPromotion);
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                    if (referenceId > 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var SaveDetails = _HCoreContext.TUCAppPromotion.Where(x => x.Id == _TUCAppPromotion.Id && x.Guid == _TUCAppPromotion.Guid).FirstOrDefault();
                            if (SaveDetails != null)
                            {
                                SaveDetails.ImageStorageId = referenceId;
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    else if (_Request.ImageContent != null)
                    {
                        long? ImageStorageId = null;
                        if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                        {
                            ImageStorageId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, null, _Request.UserReference);
                        }
                        if (ImageStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var SaveDetails = _HCoreContext.TUCAppPromotion.Where(x => x.Id == _TUCAppPromotion.Id && x.Guid == _TUCAppPromotion.Guid).FirstOrDefault();
                                if (SaveDetails != null)
                                {
                                    if (ImageStorageId != null)
                                    {
                                        SaveDetails.ImageStorageId = (long)ImageStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }

                    var _Response = new
                    {
                        ReferenceId = _TUCAppPromotion.Id,
                        ReferenceKey = _TUCAppPromotion.Guid,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA14132, TUCCoreResource.CAA14132M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveAppPromotion", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Method defined to update app promotion
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateAppPromotion(OAppPromotion.Update _Request)
        {
            try
            {   
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    long? referenceId = _HCoreContext.HCCoreStorage.Where(x => x.Guid == _Request.ImageReference).Select(x => x.Id).FirstOrDefault();
                    var _AppPromotionDetails = _HCoreContext.TUCAppPromotion.Where(x => x.Guid == _Request.ReferenceKey && x.Id == _Request.ReferenceId).FirstOrDefault();
                    if(_AppPromotionDetails != null)
                    {
                        if (_Request.NavigationType != null && _AppPromotionDetails.NavigationType != _Request.NavigationType)
                        {
                            _AppPromotionDetails.NavigationType = _Request.NavigationType;
                        }
                        if (_Request.NavigateUrl != null && _AppPromotionDetails.NavigateUrl != _Request.NavigateUrl)
                        {
                            _AppPromotionDetails.NavigateUrl = _Request.NavigateUrl;
                        }
                        if (_Request.NavigationData != null && _AppPromotionDetails.NavigationData != _Request.NavigationData)
                        {
                            _AppPromotionDetails.NavigationData = _Request.NavigationData;
                        }
                        if (_Request.StartDate != null && _AppPromotionDetails.StartDate != _Request.StartDate)
                        {
                            _AppPromotionDetails.StartDate = HCoreHelper.ConvertToUTC(_Request.StartDate, _Request.UserReference.CountryTimeZone);  
                        }
                        if (_Request.EndDate != null && _AppPromotionDetails.EndDate != _Request.EndDate)
                        {
                            _AppPromotionDetails.EndDate = HCoreHelper.ConvertToUTC(_Request.EndDate, _Request.UserReference.CountryTimeZone);
                        }
                        if (StatusId > 0)
                        {
                            _AppPromotionDetails.StatusId = StatusId;
                        }
                        _AppPromotionDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _AppPromotionDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        if (referenceId > 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var AppDetails = _HCoreContext.TUCAppPromotion.Where(x => x.Guid == _AppPromotionDetails.Guid).FirstOrDefault();
                                if (AppDetails != null)
                                {
                                    if (referenceId > 0)
                                    {
                                        AppDetails.ImageStorageId = referenceId;
                                    }
                                    else if (referenceId == null)
                                    {
                                        AppDetails.ImageStorageId = null;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        else if (_Request.ImageContent != null)
                        {
                            long? ImageStorageId = null;
                            if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                            {
                                ImageStorageId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, _AppPromotionDetails.ImageStorageId, _Request.UserReference);
                            }
                            if (ImageStorageId != null)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var AppDetails = _HCoreContext.TUCAppPromotion.Where(x => x.Guid == _AppPromotionDetails.Guid).FirstOrDefault();
                                    if (AppDetails != null)
                                    {
                                        if (ImageStorageId != null)
                                        {
                                            AppDetails.ImageStorageId = ImageStorageId;
                                        }
                                        _HCoreContext.SaveChanges();
                                    }
                                    else
                                    {
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                        }

                        var _Response = new
                        {
                            ReferenceId = _AppPromotionDetails.Id,
                            ReferenceKey = _AppPromotionDetails.Guid,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CAA14133, TUCCoreResource.CAA14133M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateAppPromotion", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Method defined to delete app promotion
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse DeleteAppPromotion(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _AppPromotionDetails = _HCoreContext.TUCAppPromotion
                        .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .FirstOrDefault();
                    if (_AppPromotionDetails != null)
                    {
                        _HCoreContext.TUCAppPromotion.Remove(_AppPromotionDetails);
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CAA14134, TUCCoreResource.CAA14134M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DeleteAppPromotion", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }

        /// <summary>
        /// Description: Method defined to get details of app promotion
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetAppPromotion(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _AppPromotionDetails = _HCoreContext.TUCAppPromotion.Where(x => (x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey))
                        .Select(x => new OAppPromotion.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            AppId = x.AppId,

                            CountryId = x.CountryId,
                            CountryKey = x.Country.Guid,
                            CountryCode = x.Country.SystemName,
                            CountryName = x.Country.Name,

                            NavigateUrl = x.NavigateUrl,
                            NavigationType = x.NavigationType,
                            NavigationData = x.NavigationData,

                            ImageUrl = x.ImageStorage.Path,

                            StartDate = x.StartDate,
                            EndDate = x.EndDate,

                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyById,
                            ModifyByKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name
                        })
                       .FirstOrDefault();

                    _HCoreContext.Dispose();

                    if (_AppPromotionDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_AppPromotionDetails.ImageUrl))
                        {
                            _AppPromotionDetails.ImageUrl = _AppConfig.StorageUrl + _AppPromotionDetails.ImageUrl;
                        }
                        else
                        {
                            _AppPromotionDetails.ImageUrl = _AppConfig.Default_Icon;
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AppPromotionDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAppPromotion", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Method defined to get list of app promotion
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetAppPromotions(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.TUCAppPromotion
                            .Where(x => x.CountryId == _Request.UserReference.CountryId)
                            .Select(x => new OAppPromotion.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                AppId = x.AppId,

                                CountryId = x.CountryId,
                                CountryCode = x.Country.SystemName,
                                CountryName = x.Country.Name,

                                NavigateUrl = x.NavigateUrl,
                                NavigationType = x.NavigationType,
                                NavigationData = x.NavigationData,

                                ImageUrl = x.ImageStorage.Path,

                                StartDate = x.StartDate,
                                EndDate = x.EndDate,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByKey = x.CreatedBy.Guid,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyById = x.ModifyById,
                                ModifyByKey = x.ModifyBy.Guid,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name
                            })
                            .Where(_Request.SearchCondition)
                            .Count();

                    }

                    List<OAppPromotion.List> Data = _HCoreContext.TUCAppPromotion
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Select(x => new OAppPromotion.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    AppId = x.AppId,

                                                    CountryId = x.CountryId,
                                                    CountryCode = x.Country.SystemName,
                                                    CountryName = x.Country.Name,

                                                    NavigateUrl = x.NavigateUrl,
                                                    NavigationType = x.NavigationType,
                                                    NavigationData = x.NavigationData,

                                                    ImageUrl = x.ImageStorage.Path,

                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                        {
                            DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                        }
                    }

                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAppPromotions", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }

        /// <summary>
        /// Description: Method defined to get list of app sliders
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetAppSlider(OReference _Request)
        {
            #region Manage Exception
            try
            {

                _Sliders = new List<OAppPromotion.Slider>();
                _Sliders.Add(new OAppPromotion.Slider
                {
                    Name = "Dashboard",
                    SystemName = "dashboard",
                });

                _Sliders.Add(new OAppPromotion.Slider
                {
                    Name = "About",
                    SystemName = "about",
                });

                _Sliders.Add(new OAppPromotion.Slider
                {
                    Name = "Live Support",
                    SystemName = "livesupport",
                });

                _Sliders.Add(new OAppPromotion.Slider
                {
                    Name = "Profile",
                    SystemName = "profile",
                });

                _Sliders.Add(new OAppPromotion.Slider
                {
                    Name = "Update Pin",
                    SystemName = "updatepin",
                });

                _Sliders.Add(new OAppPromotion.Slider
                {
                    Name = "Notifications",
                    SystemName = "notifications",
                });

                _Sliders.Add(new OAppPromotion.Slider
                {
                    Name = "FAQ",
                    SystemName = "faq",
                });

                _Sliders.Add(new OAppPromotion.Slider
                {
                    Name = "Sales History",
                    SystemName = "saleshistory",
                });

                _Sliders.Add(new OAppPromotion.Slider
                {
                    Name = "Points History",
                    SystemName = "pointshistory",
                });

                _Sliders.Add(new OAppPromotion.Slider
                {
                    Name = "LCC TopUp",
                    SystemName = "lcctopup",
                });

                _Sliders.Add(new OAppPromotion.Slider
                {
                    Name = "Select Biller",
                    SystemName = "selectbiller",
                });

                _Sliders.Add(new OAppPromotion.Slider
                {
                    Name = "Stores",
                    SystemName = "stores",
                });

                _Sliders.Add(new OAppPromotion.Slider
                {
                    Name = "Deals Dashboard",
                    SystemName = "dealsdashboard",
                });

                _Sliders.Add(new OAppPromotion.Slider
                {
                    Name = "Flash Deals",
                    SystemName = "flashdeals",
                });

                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Sliders, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetAppSlider", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }



        /// <summary>
        /// Description: Method defined to remove app promotion image
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse RemoveAppPromotionImage(OAppPromotion.Image _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var Promotion = _HCoreContext.TUCAppPromotion.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Promotion != null)
                    {
                        if (Promotion != null && Promotion.ImageStorageId > 0)
                        {
                            long? IconStorageId = Promotion.ImageStorageId;
                            Promotion.ImageStorageId = null;
                            Promotion.ModifyById = _Request.UserReference.AccountId;
                            Promotion.ModifyDate = HCoreHelper.GetGMTDateTime();
                            _HCoreContext.SaveChanges();

                            bool DeleteStatus = HCoreHelper.DeleteStorage(IconStorageId, _Request.UserReference);
                            if (DeleteStatus == false)
                            {
                                TUCAppPromotion _DetailUpdates = _HCoreContext.TUCAppPromotion.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                _DetailUpdates.ImageStorageId = IconStorageId;
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAA14139, TUCCoreResource.CAA14139M);
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CAA14140, TUCCoreResource.CAA14140M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAA14141, TUCCoreResource.CAA14141M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("RemoveAppPromotionImage", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
        }
    }
}
