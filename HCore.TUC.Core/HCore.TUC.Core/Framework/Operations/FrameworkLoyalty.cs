//==================================================================================
// FileName: FramewworkLoyalty.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to monthly E-Statement for Wakanow and Customer's App.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.CoreConstant;
using HCore.TUC.Core.Resource;
using HCore.Operations.Object;
using static HCore.CoreConstant.CoreHelperStatus;
using HCore.Data.Store;
using System.Linq.Dynamic.Core;
using System.IO;
using SendGrid.Helpers.Mail;
using SendGrid;
using HCore.TUC.Core.Object.Operations;
using Akka.Actor;

namespace HCore.TUC.Core.Framework.Operations
{
    //public class FrameworkLoyalty
    //{

    //    HCoreContext _HCoreContext;
    //    ManageCoreTransaction _ManageCoreTransaction;
    //    OAccountBalance _OAccountBalance;
    //    /// <summary>
    //    /// Author: Priya Chavadiya
    //    /// Description:Method to Send Monthly E-Statement to the customers of Customer's App.
    //    /// </summary>
    //    internal void SendMonthlyCustomerEstatement()
    //    {
    //        #region Manage Exception
    //        try
    //        {
    //            using (_HCoreContext = new HCoreContext())
    //            {
    //                var CustomerDetails = _HCoreContext.HCUAccount
    //                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser)
    //                                        .Select(x => new
    //                                        {
    //                                            Id = x.Id,
    //                                            MobileNumber = x.MobileNumber,
    //                                            Name = x.Name,
    //                                            StatusId = x.StatusId,
    //                                            EmailAddress = x.EmailAddress,
    //                                            DisplayName = x.DisplayName
    //                                        })
    //                                        .ToList();
    //                if (CustomerDetails != null)
    //                {

    //                    var _Actor = ActorSystem.Create("ActorAppCustomerMonthlyEStatementSms");
    //                    var _ActorNotify = _Actor.ActorOf<ActorAppCustomerMonthlyEStatementSms>("ActorAppCustomerMonthlyEStatementSms");

    //                    var _ActorEmail = ActorSystem.Create("ActorAppCustomerMonthlyEStatement");
    //                    var _ActorNotifyEmail = _ActorEmail.ActorOf<ActorAppCustomerMonthlyEStatement>("ActorAppCustomerMonthlyEStatement");
    //                    foreach (var customer in CustomerDetails)
    //                    {
    //                        DateTime now = DateTime.Now;
    //                        DateTime StartDate = Convert.ToDateTime(new DateTime(now.Year, now.Month, 1)).Date;
    //                        DateTime EndDate = Convert.ToDateTime(StartDate.AddMonths(1).AddDays(-1)).Date;
    //                        var BalanceDetails = GetCustomerBalance(customer.Id);
    //                        long balanceamount = Convert.ToInt64(BalanceDetails.Balance);
    //                        OLoyalty.AppCustomerSendEmailRequest appCustomerSendEmailRequest = new OLoyalty.AppCustomerSendEmailRequest();
    //                        appCustomerSendEmailRequest.UserDisplayName = customer.DisplayName;
    //                        appCustomerSendEmailRequest.EmailAddress = customer.EmailAddress;
    //                        appCustomerSendEmailRequest.MobileNumber = customer.MobileNumber;
    //                        appCustomerSendEmailRequest.custBalance = balanceamount;
    //                        appCustomerSendEmailRequest.Balance = BalanceDetails.Balance;
    //                        appCustomerSendEmailRequest.RewardPoints = BalanceDetails.Credit;
    //                        appCustomerSendEmailRequest.RewardPoints = BalanceDetails.Debit;
    //                        appCustomerSendEmailRequest.MerchantStore = BalanceDetails.StoreName;
    //                        appCustomerSendEmailRequest.Month = HCoreHelper.GetGMTDateTime().ToString("MMMM");
    //                        appCustomerSendEmailRequest.EndDate = EndDate;
    //                        appCustomerSendEmailRequest.StatusId = customer.StatusId;
    //                        if (!string.IsNullOrEmpty(customer.MobileNumber))
    //                        {

    //                            _ActorNotify.Tell(appCustomerSendEmailRequest);
    //                        }

    //                        if (!string.IsNullOrEmpty(customer.EmailAddress))
    //                        {
    //                            using (HCoreContext _HCoreContextnew = new HCoreContext())
    //                            {
    //                                var CustomerTransactions = _HCoreContextnew.HCUAccountTransaction
    //                                              .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
    //                                              && x.Parent.CountryId == 1
    //                                              && x.AccountId == customer.Id
    //                                              && (x.TransactionDate >= StartDate && x.TransactionDate <= EndDate)
    //                                              && x.Status.SystemName == "transaction.success"
    //                                              && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash
    //                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
    //                                              || x.SourceId == TransactionSource.ThankUCashPlus))
    //                                              .Select(x => new HCUAccountTransaction
    //                                              {
    //                                                  Id = x.Id,
    //                                                  Guid = x.Guid,
    //                                                  ModeId = x.ModeId,
    //                                                  ReferenceAmount = x.ReferenceAmount,
    //                                                  TotalAmount = x.TotalAmount,
    //                                                  ParentId = x.ParentId,
    //                                                  SubParentId = x.SubParentId,
    //                                                  Balance = x.Balance,
    //                                                  TransactionDate = x.TransactionDate
    //                                              })
    //                                              .ToList();
    //                                appCustomerSendEmailRequest.CustomerTransactions = CustomerTransactions;

    //                                _ActorNotifyEmail.Tell(appCustomerSendEmailRequest);

    //                                _HCoreContextnew.Dispose();
    //                            }
    //                        }
    //                    }
    //                }
    //            }

    //        }
    //        catch (Exception _Exception)
    //        {
    //            #region  Log Exception
    //            HCoreHelper.LogException("SendMonthlyCustomerEstatement", _Exception, null, "HCD0500", "Unable to process. Please try after some time");
    //            #endregion

    //        }
    //        #endregion
    //    }
    //    internal OAccountBalance GetCustomerBalance(long UserAccountId)
    //    {
    //        #region Manage Exception
    //        try
    //        {
    //            _OAccountBalance = new OAccountBalance();
    //            _OAccountBalance.Credit = 0;
    //            _OAccountBalance.Debit = 0;
    //            _OAccountBalance.Balance = 0;
    //            DateTime? LastTransactionDate = HCoreHelper.GetGMTDate();
    //            using (_HCoreContext = new HCoreContext())
    //            {
    //                DateTime now = DateTime.Now;
    //                var startDate = new DateTime(now.Year, now.Month, 1);
    //                string sdate = new DateTime(now.Year, now.Month, 1).ToString("yyyy-MM-dd") + " 00:00:00";
    //                var endDate = startDate.AddMonths(1).AddDays(-1);
    //                string edate = endDate.ToString("yyyy-MM-dd") + " 23:59:59";
    //                string SearchCondition = "(( StatusCode = \"transaction.success\" ) AND ( TransactionDate > \"" + sdate + "\" AND TransactionDate < \"" + edate + "\")) AND (UserAccountId = \"" + UserAccountId + "\" )";
    //                _OAccountBalance.Credit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
    //                                          .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
    //                                          && x.Parent.CountryId == 1
    //                                          && x.ModeId == TransactionMode.Credit
    //                                          && x.Type.SubParentId == TransactionTypeCategory.Reward
    //                                          && x.CampaignId == null
    //                                          && x.ParentTransaction.TotalAmount > 0
    //                                          && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.TypeId != TransactionType.TucSuperCash
    //                                              && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
    //                                          || x.SourceId == TransactionSource.ThankUCashPlus))
    //                                          .Select(x => new
    //                                          {
    //                                              UserAccountId = x.AccountId,
    //                                              RewardAmount = x.ReferenceAmount,
    //                                              TransactionDate = x.TransactionDate,
    //                                              StatusId = x.StatusId,
    //                                              StatusCode = x.Status.SystemName,
    //                                          })
    //                                          .Where(SearchCondition)
    //                                          //.Take(40)
    //                                          .Sum(x => x.RewardAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

    //                _OAccountBalance.Debit = _HCoreContext.HCUAccountTransaction
    //                                           .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
    //                                            && x.Parent.CountryId == 1
    //                                            && x.ModeId == TransactionMode.Debit
    //                                            && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints || x.SourceId == TransactionSource.TUCBlack)
    //                                            && x.Type.SubParentId == TransactionTypeCategory.Redeem)
    //                                          .Select(x => new
    //                                          {
    //                                              UserAccountId = x.AccountId,
    //                                              RedeemAmount = x.TotalAmount,
    //                                              TransactionDate = x.TransactionDate,
    //                                              StatusId = x.StatusId,
    //                                              StatusCode = x.Status.SystemName,
    //                                          })
    //                                          .Where(SearchCondition)
    //                                          //.Take(40)
    //                                          .Sum(x => x.RedeemAmount);

    //                _OAccountBalance.Balance = HCoreHelper.RoundNumber((_OAccountBalance.Credit - _OAccountBalance.Debit), _AppConfig.SystemEntryRoundDouble);
    //                long LastTransactionStoreId = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == UserAccountId
    //                                                              && x.SourceId == TransactionSource.TUC && x.TransactionDate.Month == 4 && x.TransactionDate != null)
    //                                                             .OrderByDescending(x => x.TransactionDate).Select(x => x.SubParentId).FirstOrDefault() ?? 0;
    //                // long LastTransactionStoreId = 0;
    //                if (LastTransactionStoreId > 0)
    //                {
    //                    var SubParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == LastTransactionStoreId);
    //                    _OAccountBalance.StoreName = SubParentDetails.OwnerDisplayName;
    //                }
    //                else
    //                {
    //                    _OAccountBalance.StoreName = "-";
    //                }
    //                _HCoreContext.Dispose();
    //                return _OAccountBalance;
    //            }
    //        }
    //        catch (Exception _Exception)
    //        {
    //            #region Log Bug
    //            HCoreHelper.LogException("GetAppUserBalance", _Exception);
    //            #endregion
    //            #region Send Response
    //            return _OAccountBalance;
    //            #endregion
    //        }
    //        #endregion
    //    }

    //    /// <summary>
    //    /// Author: Priya Chavadiya
    //    /// Description:Method to Send Monthly E-Statement to the customers of WakaNow.
    //    /// </summary>
    //    public void WakaNowMonthlyEStatement()
    //    {
    //        try
    //        {
    //            using (_HCoreContext = new HCoreContext())
    //            {
    //                DateTime now = DateTime.Now;
    //                DateTime Startdate = Convert.ToDateTime(new DateTime(now.Year, now.Month, 1)).Date;
    //                DateTime EndDate = Convert.ToDateTime(Startdate.AddMonths(1).AddDays(-1)).Date;
    //                var Customers = _HCoreContext.HCUAccount
    //                    .Where(x => x.OwnerId == _AppConfig.WakanowId)
    //                    .Select(x => new
    //                    {
    //                        Id = x.Id,
    //                        Guid = x.Guid,
    //                        EmailAddress = x.EmailAddress,
    //                        DisplayName = x.DisplayName,
    //                        MobileNumber = x.MobileNumber,
    //                        AccountCode = x.AccountCode
    //                    }).ToList();
    //                var _Actor = ActorSystem.Create("ActorWakanowMonthlyEStatement");
    //                var _ActorNotify = _Actor.ActorOf<ActorWakanowMonthlyEStatement>("ActorWakanowMonthlyEStatement");
    //                foreach (var Customer in Customers)
    //                {
    //                    if (!string.IsNullOrEmpty(Customer.EmailAddress))
    //                    {
    //                        var CustomerTransactions = _HCoreContext.HCUAccountTransaction
    //                                                    .Where(x => (x.TransactionDate >= Startdate && x.TransactionDate <= EndDate)
    //                                                    && x.AccountId == Customer.Id)
    //                                                    .Select(x => new HCUAccountTransaction
    //                                                    {
    //                                                        Id = x.Id,
    //                                                        Guid = x.Guid,
    //                                                        ModeId = x.ModeId,
    //                                                        TransactionDate = x.TransactionDate,
    //                                                        ReferenceAmount = x.ReferenceAmount,
    //                                                        TotalAmount = x.TotalAmount,
    //                                                        Balance = x.Balance
    //                                                    })
    //                                                    .ToList();
    //                        OLoyalty.WakanowSendEmailRequest wakanowSendEmailRequest = new OLoyalty.WakanowSendEmailRequest();
    //                        wakanowSendEmailRequest.TemplateId = SendGridCustomerEmailTemplateIds.WakaNow_E_Statement;
    //                        wakanowSendEmailRequest.TemplateName = "Wakanow_Monthly_E_Statement.html";
    //                        wakanowSendEmailRequest.CustomerTransactions = CustomerTransactions;
    //                        wakanowSendEmailRequest.EmailAddress = Customer.EmailAddress;
    //                        wakanowSendEmailRequest.DisplayName = Customer.DisplayName;
    //                        wakanowSendEmailRequest.Month = HCoreHelper.GetGMTDateTime().ToString("MMMM");
    //                        wakanowSendEmailRequest.MobileNumber = Customer.MobileNumber;
    //                        wakanowSendEmailRequest.AccountCode = Customer.AccountCode;

    //                        _ActorNotify.Tell(wakanowSendEmailRequest);
    //                        // SendEmailWithPdf(SendGridCustomerEmailTemplateIds.WakaNow_E_Statement, "Wakanow_Monthly_E_Statement.html", CustomerTransactions,
    //                        //Customer.EmailAddress, _EmailParameters, Customer.DisplayName, Customer.MobileNumber, Customer.AccountCode);
    //                    }
    //                }
    //            }
    //        }
    //        catch (Exception _Exception)
    //        {
    //            HCoreHelper.LogException("WakaNowMonthlyEStatement", _Exception);
    //        }
    //    }
    //    public static void WakaNowSendEmailWithPdf(OLoyalty.WakanowSendEmailRequest wakanowSendEmailRequest)
    //    {
    //        string htmlstring = string.Empty;
    //        double TotalAmount = 0.0, ReferenceAmount = 0.0, Balance = 0.0;
    //        foreach (var Transaction in wakanowSendEmailRequest.CustomerTransactions)
    //        {
    //            //Transaction tr = new Transaction
    //            //{
    //            //    TransactionDate = Customer.HCUAccountTransactionAccount.Where(a => a.TransactionDate >= EndDate && a.TransactionDate <= Startdate).Select(m => m.TransactionDate).FirstOrDefault(),
    //            //    RewardAmount = Customer.HCUAccountTransactionAccount.Where(a => a.TransactionDate >= EndDate && a.TransactionDate <= Startdate).Select(m => m.TotalAmount).FirstOrDefault(),
    //            //    RedeemAmount = Customer.HCUAccountTransactionAccount.Where(a => a.TransactionDate >= EndDate && a.TransactionDate <= Startdate).Select(m => m.ReferenceAmount).FirstOrDefault(),
    //            //    WalletBalance = Customer.HCUAccountTransactionAccount.Where(a => a.TransactionDate >= EndDate && a.TransactionDate <= Startdate).Select(m => m.Balance).FirstOrDefault(),
    //            //};

    //            double RewardAmount = Convert.ToDouble(wakanowSendEmailRequest.CustomerTransactions.Where(x => x.ModeId == TransactionMode.Credit && x.Id == Transaction.Id).Select(x => x.ReferenceAmount).FirstOrDefault());
    //            double RedeemAmount = Convert.ToDouble(wakanowSendEmailRequest.CustomerTransactions.Where(x => x.ModeId == TransactionMode.Debit && x.Id == Transaction.Id).Select(x => x.TotalAmount).FirstOrDefault());
    //            double tranBalance = (RewardAmount - RedeemAmount);


    //            htmlstring = htmlstring + "<tr>";
    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: left;'>" + Transaction.TransactionDate.ToString("yyyy-MM-dd hh:MM:ss") + "</div>";
    //            htmlstring = htmlstring + "</td>";
    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: left;'>" + String.Format("{0:0.00}", RewardAmount) + "</div>";
    //            htmlstring = htmlstring + "</td>";

    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: left;'>" + String.Format("{0:0.00}", RedeemAmount) + "</div>";
    //            htmlstring = htmlstring + "</td>";

    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: right;'>" + String.Format("{0:0.00}", Transaction.Balance) + "</div>";
    //            htmlstring = htmlstring + "</td>";
    //            htmlstring = htmlstring + "</tr>";
    //            TotalAmount = TotalAmount + RedeemAmount;
    //            ReferenceAmount = ReferenceAmount + RewardAmount;
    //            Balance = Balance + Transaction.Balance;

    //        }

    //        if (string.IsNullOrEmpty(htmlstring))
    //        {
    //            htmlstring = htmlstring + "<tr><td colspan='4' style='border:1px solid black;border-collapse:collapse;'>No records found. </td></tr>";
    //        }
    //        else
    //        {
    //            htmlstring = htmlstring + "<tr>";
    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: left;'>Total</div>";
    //            htmlstring = htmlstring + "</td>";
    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: left;'>" + Math.Round(ReferenceAmount) + "</div>";
    //            htmlstring = htmlstring + "</td>";
    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: right;'>" + Math.Round(TotalAmount) + "</div>";
    //            htmlstring = htmlstring + "</td>";
    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: right;'>" + Math.Round(ReferenceAmount - TotalAmount) + "</div>";
    //            htmlstring = htmlstring + "</td>";
    //            htmlstring = htmlstring + "</tr>";
    //        }
    //        StringBuilder Html = new StringBuilder(System.IO.File.ReadAllText("./templates/" + wakanowSendEmailRequest.TemplateName));
    //        Html.Replace("{{customerName}}", wakanowSendEmailRequest.DisplayName);
    //        Html.Replace("{{customerPhone}}", wakanowSendEmailRequest.MobileNumber);
    //        Html.Replace("{{customerEmail}}", wakanowSendEmailRequest.EmailAddress);
    //        Html.Replace("{{wakaPonitsId}}", wakanowSendEmailRequest.AccountCode);
    //        Html.Replace("{{RewardAmount}}", Math.Round(ReferenceAmount).ToString());
    //        Html.Replace("{{RedeemAmount}}", Math.Round(TotalAmount).ToString());
    //        Html.Replace("{{Balance}}", Math.Round(ReferenceAmount - TotalAmount).ToString());
    //        Html.Replace("{{date}}", HCoreHelper.GetGMTDateTime().ToString("yyyy-MM-dd hh:mm:ss"));
    //        Html.Replace("{{Transactions}}", htmlstring);

    //        string AttachmentName = "EStatement_" + wakanowSendEmailRequest.MobileNumber + ".pdf";
    //        string pdfBase64String = HCoreHelper.HtmlToPdfBase64(Html.ToString());

    //        var _SendGridClient = new SendGridClient("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");
    //        List<EmailAddress> _Email = new List<EmailAddress>();
    //        _Email.Add(new EmailAddress
    //        {
    //            Email = wakanowSendEmailRequest.EmailAddress,
    //            Name = wakanowSendEmailRequest.DisplayName,
    //        });
    //        var _EmailParameters = new
    //        {
    //            DisplayName = wakanowSendEmailRequest.DisplayName,
    //            Month = wakanowSendEmailRequest.Month
    //        };
    //        var _FromAddress = new EmailAddress("noreply@wakanow.com", "WakaPoints");
    //        var _BroacastMessage = MailHelper.CreateSingleTemplateEmailToMultipleRecipients(_FromAddress, _Email, wakanowSendEmailRequest.TemplateId, _EmailParameters);
    //        if (!string.IsNullOrWhiteSpace(pdfBase64String))
    //        {
    //            _BroacastMessage.AddAttachment(AttachmentName, pdfBase64String);
    //        }
    //        var _BroadCastResponse = _SendGridClient.SendEmailAsync(_BroacastMessage);
    //    }
    //    public static void AppCustomerSendEmailWithPdf(OLoyalty.AppCustomerSendEmailRequest appcustSendEmailRequest)
    //    {
    //        double TotalAmount = 0.0, ReferenceAmount = 0.0, Balance = 0.0;
    //        string htmlstring = string.Empty, StoreName = string.Empty, MerchantName = string.Empty;
    //        foreach (var Transaction in appcustSendEmailRequest.CustomerTransactions)
    //        {
    //            //Transaction tr = new Transaction
    //            //{
    //            //    TransactionDate = Customer.HCUAccountTransactionAccount.Where(a => a.TransactionDate >= EndDate && a.TransactionDate <= Startdate).Select(m => m.TransactionDate).FirstOrDefault(),
    //            //    RewardAmount = Customer.HCUAccountTransactionAccount.Where(a => a.TransactionDate >= EndDate && a.TransactionDate <= Startdate).Select(m => m.TotalAmount).FirstOrDefault(),
    //            //    RedeemAmount = Customer.HCUAccountTransactionAccount.Where(a => a.TransactionDate >= EndDate && a.TransactionDate <= Startdate).Select(m => m.ReferenceAmount).FirstOrDefault(),
    //            //    WalletBalance = Customer.HCUAccountTransactionAccount.Where(a => a.TransactionDate >= EndDate && a.TransactionDate <= Startdate).Select(m => m.Balance).FirstOrDefault(),
    //            //};

    //            double RewardAmount = Convert.ToDouble(appcustSendEmailRequest.CustomerTransactions.Where(x => x.ModeId == TransactionMode.Credit && x.Id == Transaction.Id).Select(x => x.ReferenceAmount).FirstOrDefault());
    //            double RedeemAmount = Convert.ToDouble(appcustSendEmailRequest.CustomerTransactions.Where(x => x.ModeId == TransactionMode.Debit && x.Id == Transaction.Id).Select(x => x.TotalAmount).FirstOrDefault());
    //            double tranBalance = (RewardAmount - RedeemAmount);
    //            long StoreId = Convert.ToInt64(appcustSendEmailRequest.CustomerTransactions.Where(x => x.ModeId == TransactionMode.Debit && x.Id == Transaction.Id).Select(x => x.SubParentId).FirstOrDefault());
    //            long MerchantId = Convert.ToInt64(appcustSendEmailRequest.CustomerTransactions.Where(x => x.ModeId == TransactionMode.Credit && x.Id == Transaction.Id).Select(x => x.SubParentId).FirstOrDefault());
    //            if (StoreId != 0)
    //            {
    //                StoreName = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == StoreId).DisplayName;
    //            }
    //            if (MerchantId != 0)
    //            {
    //                MerchantName = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == MerchantId).OwnerDisplayName;
    //            }
    //            htmlstring = htmlstring + "<tr>";
    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: center;'>" + Transaction.TransactionDate.ToString("yyyy-MM-dd hh:MM:ss") + "</div>";
    //            htmlstring = htmlstring + "</td>";

    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: right;'>" + String.Format("{0:0.00}", RewardAmount) + "</div>";
    //            htmlstring = htmlstring + "</td>";

    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: center;'>" + MerchantName + "</div>";
    //            htmlstring = htmlstring + "</td>";

    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: right;'>" + String.Format("{0:0.00}", RedeemAmount) + "</div>";
    //            htmlstring = htmlstring + "</td>";

    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: center;'>" + StoreName + "</div>";
    //            htmlstring = htmlstring + "</td>";

    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: right;'>" + String.Format("{0:0.00}", Transaction.Balance) + "</div>";
    //            htmlstring = htmlstring + "</td>";
    //            htmlstring = htmlstring + "</tr>";
    //            TotalAmount = TotalAmount + RedeemAmount;
    //            ReferenceAmount = ReferenceAmount + RewardAmount;
    //            Balance = Balance + Transaction.Balance;

    //        }
    //        if (string.IsNullOrEmpty(htmlstring))
    //        {
    //            htmlstring = htmlstring + "<tr><td colspan='6' style='border:1px solid black;border-collapse:collapse;'>No records found. </td></tr>";
    //        }
    //        else
    //        {
    //            htmlstring = htmlstring + "<tr>";
    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: left;'>Total</div>";
    //            htmlstring = htmlstring + "</td>";
    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: right;'>" + Math.Round(ReferenceAmount) + "</div>";
    //            htmlstring = htmlstring + "</td>";
    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: left;'></div>";
    //            htmlstring = htmlstring + "</td>";
    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: right;'>" + Math.Round(TotalAmount) + "</div>";
    //            htmlstring = htmlstring + "</td>";
    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: left;'></div>";
    //            htmlstring = htmlstring + "</td>";
    //            htmlstring = htmlstring + "<td style='width:200px;max-width:25%;border:1px solid black;border-collapse:collapse;'>";
    //            htmlstring = htmlstring + "    <div style='text-align: right;'>" + Math.Round(Balance) + "</div>";
    //            htmlstring = htmlstring + "</td>";
    //            htmlstring = htmlstring + "</tr>";
    //        }
    //        StringBuilder Html = new StringBuilder(System.IO.File.ReadAllText("./templates/" + "Thankucash_Monthly_E_Statement.html"));
    //        Html.Replace("{{Transactions}}", htmlstring);
    //        Html.Replace("{{WalletBalancePoints}}", Math.Round(ReferenceAmount - TotalAmount).ToString());
    //        Html.Replace("{{LastDateofMonth}}", appcustSendEmailRequest.EndDate.ToString("dd/MM/yyyy"));

    //        string AttachmentName = "EStatement_" + appcustSendEmailRequest.MobileNumber + ".pdf";
    //        string pdfBase64String = HCoreHelper.HtmlToPdfBase64(Html.ToString());

    //        var _EmailParameters = new
    //        {

    //            UserDisplayName = appcustSendEmailRequest.UserDisplayName,
    //            Balance = Convert.ToInt64(appcustSendEmailRequest.Balance),
    //            RewardPoints = Convert.ToInt64(appcustSendEmailRequest.RewardPoints),
    //            RedeemPoints = Convert.ToInt64(appcustSendEmailRequest.RedeemPoints),
    //            Month = appcustSendEmailRequest.Month,
    //            MerchantStore = appcustSendEmailRequest.MerchantStore
    //        };
    //        if (appcustSendEmailRequest.custBalance <= 0 || appcustSendEmailRequest.StatusId == HelperStatus.Default.Inactive)
    //        {
    //            HCoreHelper.BroadCastEmail(SendGridCustomerEmailTemplateIds.CustomerWithZeroBalance, appcustSendEmailRequest.UserDisplayName, appcustSendEmailRequest.EmailAddress, _EmailParameters, null, AttachmentBase64String: pdfBase64String, AttachmentName: AttachmentName);
    //        }
    //        else if (appcustSendEmailRequest.StatusId == HelperStatus.Default.Active && appcustSendEmailRequest.custBalance > 0)
    //        {
    //            HCoreHelper.BroadCastEmail(SendGridCustomerEmailTemplateIds.CustomerWithBalance, appcustSendEmailRequest.UserDisplayName, appcustSendEmailRequest.EmailAddress, _EmailParameters, null, AttachmentBase64String: pdfBase64String, AttachmentName: AttachmentName);
    //        }
    //    }
    //    public static void AppCustomerSendSms(OLoyalty.AppCustomerSendEmailRequest appcustSendEmailRequest)
    //    {
    //        if (appcustSendEmailRequest.custBalance <= 0 || appcustSendEmailRequest.StatusId == HelperStatus.Default.Inactive)
    //        {
    //            string s1 = "";
    //            s1 = s1 + "Dear " + appcustSendEmailRequest.UserDisplayName;
    //            s1 = s1 + System.Environment.NewLine;
    //            s1 = s1 + "It's been a while since we saw you. We have missed your activities on the ThankUCash app. While you were away we kept the data of your wallet balance in " + DateTime.Now.ToString("MMMM") + ". Take a look.";
    //            s1 = s1 + System.Environment.NewLine;
    //            s1 = s1 + "Total wallet balance:" + Convert.ToInt64(appcustSendEmailRequest.Balance);
    //            s1 = s1 + System.Environment.NewLine;
    //            s1 = s1 + "Total reward points earned:" + Convert.ToInt64(appcustSendEmailRequest.RewardPoints);
    //            s1 = s1 + System.Environment.NewLine;
    //            s1 = s1 + "Total redeem amount:" + Convert.ToInt64(appcustSendEmailRequest.RedeemPoints);
    //            s1 = s1 + System.Environment.NewLine;
    //            s1 = s1 + "Merchant store:" + appcustSendEmailRequest.MerchantStore;
    //            s1 = s1 + System.Environment.NewLine;
    //            s1 = s1 + "You can use your reward points to purchase products, buy Airtime, pay utility bills and other services on the ThankUcash App. The more you buy, the more reward points you get. Click here: https://thankucash.com/download.html to download the thankucash app. We can't wait to see you back. ThankU.";
    //            var ValidateMobileNumber1 = HCoreHelper.ValidateMobileNumber(appcustSendEmailRequest.MobileNumber);

    //            HCoreHelper.SendSMS(SmsType.Transaction, ValidateMobileNumber1.Isd, ValidateMobileNumber1.MobileNumber, s1.ToString(), 1, null);
    //        }
    //        else if (appcustSendEmailRequest.StatusId == HelperStatus.Default.Active && appcustSendEmailRequest.custBalance > 0)
    //        {
    //            string s1 = "";
    //            s1 = s1 + "Dear " + appcustSendEmailRequest.UserDisplayName;
    //            s1 = s1 + System.Environment.NewLine;
    //            s1 = s1 + "Hope you are well, Here are your activities on the ThankUCash app. As a loyal customer we kept the data of your wallet balance in " + DateTime.Now.ToString("MMMM") + ". Check it out. ";
    //            s1 = s1 + System.Environment.NewLine;
    //            s1 = s1 + "Total wallet balance:" + Convert.ToInt64(appcustSendEmailRequest.Balance);
    //            s1 = s1 + System.Environment.NewLine;
    //            s1 = s1 + "Total reward points earned:" + Convert.ToInt64(appcustSendEmailRequest.RewardPoints);
    //            s1 = s1 + System.Environment.NewLine;
    //            s1 = s1 + "Total redeem amount:" + Convert.ToInt64(appcustSendEmailRequest.RedeemPoints);
    //            s1 = s1 + System.Environment.NewLine;
    //            s1 = s1 + "Merchant store:" + appcustSendEmailRequest.MerchantStore;
    //            s1 = s1 + System.Environment.NewLine;
    //            s1 = s1 + "You can use your reward points to purchase products, buy Airtime, pay utility bills and other services on the ThankUcash App. Remember the more you buy, the more reward points you get. ThankU.";
    //            var ValidateMobileNumber1 = HCoreHelper.ValidateMobileNumber(appcustSendEmailRequest.MobileNumber);

    //            HCoreHelper.SendSMS(SmsType.Transaction, ValidateMobileNumber1.Isd, ValidateMobileNumber1.MobileNumber, s1.ToString(), 1, null);
    //        }
    //    }

    //}
    //internal class ActorWakanowMonthlyEStatement : ReceiveActor
    //{
    //    public ActorWakanowMonthlyEStatement()
    //    {
    //        Receive<OLoyalty.WakanowSendEmailRequest>(_Request =>
    //        {
    //            FrameworkLoyalty _FrameworkLoyalty = new FrameworkLoyalty();
    //            FrameworkLoyalty.WakaNowSendEmailWithPdf(_Request);
    //        });
    //    }
    //}
    //internal class ActorAppCustomerMonthlyEStatement : ReceiveActor
    //{
    //    public ActorAppCustomerMonthlyEStatement()
    //    {
    //        Receive<OLoyalty.AppCustomerSendEmailRequest>(_Request =>
    //        {
    //            FrameworkLoyalty _FrameworkLoyalty = new FrameworkLoyalty();
    //            FrameworkLoyalty.AppCustomerSendEmailWithPdf(_Request);
    //        });
    //    }
    //}
    //internal class ActorAppCustomerMonthlyEStatementSms : ReceiveActor
    //{
    //    public ActorAppCustomerMonthlyEStatementSms()
    //    {
    //        Receive<OLoyalty.AppCustomerSendEmailRequest>(_Request =>
    //        {
    //            FrameworkLoyalty _FrameworkLoyalty = new FrameworkLoyalty();
    //            FrameworkLoyalty.AppCustomerSendSms(_Request);
    //        });
    //    }
    //}


}




public static class SendGridCustomerEmailTemplateIds
{
    public const string CustomerWithBalance = "d-d855cf48b0fd4470ac7da5fd16588e1f";
    public const string CustomerWithZeroBalance = "d-086e1ecc4d4a4e40a283656bcd4eb820";
    public const string WakaNow_E_Statement = "d-2a581f6700ef4bc8b0a5d67378b3ef6a";
}


