//==================================================================================
// FileName: FrameworkRmManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Acquirer;
using HCore.TUC.Core.Resource;

namespace HCore.TUC.Core.Framework.Acquirer
{
    internal class FrameworkRmManager
    {
        TUCBranchRmTarget _TUCBranchRmTarget;
        List<TUCBranchRmTarget> _TUCBranchRmTargets;
        HCoreContext _HCoreContext;

        internal OResponse SaveRmTarget(ORmManager.BulkTarget.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request == null || _Request.Targets.Count == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1011, TUCCoreResource.CA1011M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _TUCBranchRmTargets = new List<TUCBranchRmTarget>();
                    foreach (var Target in _Request.Targets)
                    {
                        var TargetCheck = _HCoreContext.TUCBranchRmTarget.Where(x => x.RmId == Target.RmId && x.ManagerId == Target.RmId && x.StartDate == Target.StartDate && x.EndDate == Target.EndDate).FirstOrDefault();
                        if (TargetCheck != null)
                        {
                            TargetCheck.TotalTarget = Target.TotalTarget;
                            TargetCheck.ModifyDate = HCoreHelper.GetGMTDateTime();
                            TargetCheck.ModifyById = _Request.UserReference.AccountId;
                        }
                        else
                        {
                            _TUCBranchRmTarget = new TUCBranchRmTarget();
                            if (Target.BranchId != 0)
                            {
                                _TUCBranchRmTarget.Guid = HCoreHelper.GenerateGuid();
                                if (Target.BranchId > 0)
                                {
                                    _TUCBranchRmTarget.BranchId = Target.BranchId;
                                }
                                _TUCBranchRmTarget.ManagerId = Target.ManagerId;
                                _TUCBranchRmTarget.RmId = Target.RmId;
                                _TUCBranchRmTarget.StartDate = Target.StartDate;
                                _TUCBranchRmTarget.EndDate = Target.EndDate;
                                _TUCBranchRmTarget.TotalTarget = Target.TotalTarget;
                                _TUCBranchRmTarget.AchievedTarget = 0;
                                _TUCBranchRmTarget.RemainingTarget = Target.TotalTarget;
                                _TUCBranchRmTarget.CreateDate = HCoreHelper.GetGMTDateTime();
                                _TUCBranchRmTarget.CreatedById = _Request.UserReference.AccountId;
                                _TUCBranchRmTarget.StatusId = HelperStatus.Default.Active;
                                _TUCBranchRmTargets.Add(_TUCBranchRmTarget);
                            }
                        }
                    }
                    if (_TUCBranchRmTargets.Count > 0)
                    {
                        _HCoreContext.TUCBranchRmTarget.AddRange(_TUCBranchRmTargets);
                    }
                    _HCoreContext.SaveChanges();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1098, TUCCoreResource.CA1098M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveRmTarget", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        internal OResponse EditRmTarget(ORmManager.BulkTarget.UpdateRequest _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var TargetCheck = _HCoreContext.TUCBranchRmTarget.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey ).FirstOrDefault();
                    if (TargetCheck != null)
                    {
                        if (_Request.BranchId != 0)
                        {
                            TargetCheck.BranchId = _Request.BranchId;
                        }
                        if (_Request.TotalTarget != 0)
                        {
                            TargetCheck.TotalTarget = _Request.TotalTarget;
                        }
                        if (_Request.RmId != 0)
                        {
                            TargetCheck.RmId = _Request.RmId;
                        }
                        if (_Request.StartDate != null)
                        {
                            TargetCheck.StartDate = _Request.StartDate;
                        }
                        if (_Request.EndDate != null)
                        {
                            TargetCheck.EndDate = _Request.EndDate;
                        }
                        TargetCheck.ModifyDate = HCoreHelper.GetGMTDateTime();
                        TargetCheck.ModifyById = _Request.UserReference.AccountId;
                        TargetCheck.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1109, TUCCoreResource.CA1109M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                    }

                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveRmTarget", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }


        internal OResponse GetRmTarget(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "RmId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.TUCBranchRmTarget
                                                .Select(x => new ORmManager.List
                                                {
                                                    ReferenceId  = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    BranchId = x.BranchId,
                                                    BranchKey = x.Branch.Guid,

                                                    RmId = x.RmId,
                                                    RmKey = x.Rm.Guid,

                                                    RmDisplayName = x.Rm.DisplayName,
                                                    RmMobileNumber = x.Rm.MobileNumber,

                                                    ManagerId = x.Manager.Id,
                                                    ManagerKey = x.Manager.Guid,
                                                    //ManagerName = x.Manager.DisplayName,

                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    TotalTarget = x.TotalTarget,
                                                    AchievedTarget = x.AchievedTarget,
                                                    RemainingTarget = x.RemainingTarget,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<ORmManager.List> Data = _HCoreContext.TUCBranchRmTarget
                                                .Select(x => new ORmManager.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    BranchId = x.BranchId,
                                                    BranchKey = x.Branch.Guid,

                                                    RmId = x.RmId,
                                                    RmKey = x.Rm.Guid,

                                                    RmDisplayName = x.Rm.DisplayName,
                                                    RmMobileNumber = x.Rm.MobileNumber,

                                                    ManagerId = x.Manager.Id,
                                                    ManagerKey = x.Manager.Guid,
                                                    //ManagerName = x.Manager.DisplayName,

                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    TotalTarget = x.TotalTarget,
                                                    AchievedTarget = x.AchievedTarget,
                                                    RemainingTarget = x.RemainingTarget,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in Data)
                    {

                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetRmTarget", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }
}
