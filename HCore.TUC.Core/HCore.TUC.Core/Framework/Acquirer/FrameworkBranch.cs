//==================================================================================
// FileName: FrameworkBranch.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to branch
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Acquirer;
using HCore.TUC.Core.Resource;
namespace HCore.TUC.Core.Framework.Acquirer
{
    public class FrameworkBranch
    {
        #region Declare
        HCoreContext _HCoreContext;
        TUCBranch _TUCBranch;
        HCUAccountParameter _HCUAccountParameter;
        HCCoreParameter _HCCoreParameter;
        HCCoreCountry _HCCoreCountry;
        HCCoreCountryState _HCCoreCountryState;
        HCCoreCountryStateCity _HCCoreCountryStateCity;
        HCCoreCountryStateCityArea _HCCoreCountryStateCityArea;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        TUCBranchAccount _TUCBranchAccount;
        OAddress _OAddress;
        #endregion

        /// <summary>
        /// It will creae the new branch and saves its manager details.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveBranch(OBranch.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1011, TUCCoreResource.CA1011M);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1012, TUCCoreResource.CA1012M);
                }
                if (_Request.Manager == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1013, TUCCoreResource.CA1013M);
                }
                if (_Request.Manager.ReferenceId == 0 && string.IsNullOrEmpty(_Request.Manager.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1014, TUCCoreResource.CA1014M);
                }
                if (_Request.Manager.ReferenceId == 0 && string.IsNullOrEmpty(_Request.Manager.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1014, TUCCoreResource.CA1014M);
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1015, TUCCoreResource.CA1015M);
                }
                if (string.IsNullOrEmpty(_Request.DisplayName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1016, TUCCoreResource.CA1016M);
                }
                if (string.IsNullOrEmpty(_Request.BranchCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1017, TUCCoreResource.CA1017M);
                }
                if (string.IsNullOrEmpty(_Request.PhoneNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1018, TUCCoreResource.CA1018M);
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1019, TUCCoreResource.CA1019M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }
                long AccountId = HCoreHelper.GetUserAccountId(_Request.AccountId, _Request.AccountKey, UserAccountType.Acquirer, _Request.UserReference);
                if (AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }

                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.Address, _Request.UserReference);

                using (_HCoreContext = new HCoreContext())
                {
                    bool BranchExists = _HCoreContext.TUCBranch.Any(a => a.EmailAddress == _Request.EmailAddress);
                    bool BranchCodeExists = _HCoreContext.TUCBranch.Any(x => x.BranchCode == _Request.BranchCode);
                    if (BranchExists)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.BR001, TUCCoreResource.BR001M);
                    }
                    if (BranchCodeExists)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14127, TUCCoreResource.CAA14127M);
                    }
                    _TUCBranch = new TUCBranch();
                    _TUCBranch.Guid = HCoreHelper.GenerateGuid();
                    _TUCBranch.OwnerId = AccountId;
                    _TUCBranch.Name = _Request.Name;
                    _TUCBranch.BranchCode = _Request.BranchCode;
                    _TUCBranch.DisplayName = _Request.DisplayName;
                    _TUCBranch.PhoneNumber = _Request.PhoneNumber;
                    _TUCBranch.EmailAddress = _Request.EmailAddress;
                    if (_AddressResponse != null)
                    {
                        if (!string.IsNullOrEmpty(_AddressResponse.Address))
                        {
                            _TUCBranch.Address = _AddressResponse.Address;
                        }
                        if (_AddressResponse.CityAreaId != 0)
                        {
                            _TUCBranch.CityAreaId = _AddressResponse.CityAreaId;
                        }
                        if (_AddressResponse.CityId != 0)
                        {
                            _TUCBranch.CityId = _AddressResponse.CityId;
                        }
                        if (_AddressResponse.StateId != 0)
                        {
                            _TUCBranch.StateId = _AddressResponse.StateId;
                        }
                        if (_AddressResponse.CountryId != 0)
                        {
                            _TUCBranch.CountryId = _AddressResponse.CountryId;
                        }
                        if (_AddressResponse.Latitude != 0)
                        {
                            _TUCBranch.Latitude = _AddressResponse.Latitude;
                        }
                        if (_AddressResponse.Longitude != 0)
                        {
                            _TUCBranch.Longitude = _AddressResponse.Longitude;
                        }
                    }
                    _TUCBranch.CreateDate = HCoreHelper.GetGMTDateTime();
                    _TUCBranch.CreatedById = _Request.UserReference.AccountId;
                    _TUCBranch.StatusId = StatusId;

                    #region If Selects Existing Manager
                    if (_Request.Manager.ReferenceId > 0)
                    {
                        var Manager = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.Manager.ReferenceId && x.AccountTypeId == UserAccountType.Manager).FirstOrDefault();
                        if (Manager != null)
                        {
                            _TUCBranch.ManagerId = _Request.Manager.ReferenceId;

                            #region
                            _TUCBranchAccount = new TUCBranchAccount();
                            _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                            _TUCBranchAccount.OwnerId = Manager.OwnerId;
                            _TUCBranchAccount.Branch = _TUCBranch;
                            _TUCBranchAccount.Account = Manager;
                            _TUCBranchAccount.ManagerId = _Request.Manager.ReferenceId;
                            _TUCBranchAccount.AccountLevelId = (long)Manager.RoleId;
                            _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                            _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                            _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                            _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.TUCBranchAccount.Add(_TUCBranchAccount);
                            #endregion
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                    }
                    #endregion

                    #region If Selects add new manager
                    else
                    {
                        #region Check manager already exists or not
                        bool ManagerExists = _HCoreContext.HCUAccount.Any(x => (x.EmailAddress == _Request.Manager.EmailAddress && x.MobileNumber == _Request.Manager.MobileNumber) && x.AccountTypeId == UserAccountType.Manager);
                        if (ManagerExists)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.BR002, TUCCoreResource.BR002M);
                        }
                        #endregion

                        #region Create UserName and Passowrd for new manager
                        _HCUAccountAuth = new HCUAccountAuth();
                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(8);
                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.HCUAccountAuth.Add(_HCUAccountAuth);
                        #endregion

                        #region Save UserAccount
                        _HCUAccount = new HCUAccount();
                        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccount.AccountTypeId = UserAccountType.Manager;
                        _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                        _HCUAccount.OwnerId = _Request.OwnerId;
                        _HCUAccount.BankId = _Request.AccountId;
                        _HCUAccount.DisplayName = _Request.Manager.Name;
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                        _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(14);
                        _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.Manager.MobileNumber, _Request.Manager.MobileNumber.Length);
                        _HCUAccount.RoleId = 6;
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                        _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                        _HCUAccount.Name = _Request.Manager.Name;
                        _HCUAccount.ContactNumber = _Request.Manager.MobileNumber;
                        _HCUAccount.EmailAddress = _Request.Manager.EmailAddress;
                        _HCUAccount.CountryId = _Request.UserReference.CountryId;
                        _HCUAccount.EmailVerificationStatus = 0;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.NumberVerificationStatus = 0;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.User = _HCUAccountAuth;
                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                        _HCoreContext.SaveChanges();
                        #endregion


                        #region
                        _TUCBranchAccount = new TUCBranchAccount();
                        _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                        _TUCBranchAccount.OwnerId = AccountId;
                        _TUCBranchAccount.Branch = _TUCBranch;
                        _TUCBranchAccount.Account = _HCUAccount;
                        _TUCBranchAccount.ManagerId = _HCUAccount.Id;
                        _TUCBranchAccount.AccountLevelId = (long)_HCUAccount.RoleId;
                        _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                        _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                        _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                        _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.TUCBranchAccount.Add(_TUCBranchAccount);
                        #endregion

                        _TUCBranch.Manager = _HCUAccount;
                    }
                    #endregion

                    _HCoreContext.TUCBranch.Add(_TUCBranch);
                    _HCoreContext.SaveChanges();

                    #region Old Code
                    // Manager Information
                    //if (_Request.Manager.ReferenceId != 0)
                    //{
                    //    _TUCBranch.ManagerId = _Request.Manager.ReferenceId;
                    //}
                    //else
                    //{
                    //    _HCUAccountAuth = new HCUAccountAuth();
                    //    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    //    _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(8);
                    //    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                    //    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    //    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    //    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    //    if (_Request.UserReference.AccountId != 0)
                    //    {
                    //        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    //    }
                    //    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    //    #region Save UserAccount
                    //    _HCUAccount = new HCUAccount();
                    //    _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                    //    _HCUAccount.AccountTypeId = UserAccountType.Manager;
                    //    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                    //    _HCUAccount.OwnerId = _Request.AccountId;
                    //    _HCUAccount.BankId = _Request.AccountId;
                    //    _HCUAccount.DisplayName = _Request.Manager.Name;
                    //    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    //    _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(14);
                    //    _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.Manager.MobileNumber, _Request.Manager.MobileNumber.Length);
                    //    _HCUAccount.RoleId = 1;
                    //    if (_Request.UserReference.AppVersionId != 0)
                    //    {
                    //        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    //    }
                    //    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    //    _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                    //    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    //    if (_Request.UserReference.AccountId != 0)
                    //    {
                    //        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    //    }
                    //    _HCUAccount.StatusId = HelperStatus.Default.Active;
                    //    _HCUAccount.Name = _Request.Manager.Name;
                    //    _HCUAccount.ContactNumber = _Request.Manager.MobileNumber;
                    //    _HCUAccount.EmailAddress = _Request.Manager.EmailAddress;
                    //    _HCUAccount.CountryId = _Request.UserReference.CountryId;
                    //    _HCUAccount.EmailVerificationStatus = 0;
                    //    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    //    _HCUAccount.NumberVerificationStatus = 0;
                    //    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    //    _HCUAccount.User = _HCUAccountAuth;
                    //    #endregion
                    //    _TUCBranchAccount = new TUCBranchAccount();
                    //    _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                    //    if (_Request.OwnerId != 0)
                    //    {
                    //        _TUCBranchAccount.OwnerId = _Request.OwnerId;
                    //    }
                    //    _TUCBranchAccount.Account = _HCUAccount;
                    //    _TUCBranchAccount.AccountLevelId = 6;
                    //    _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                    //    _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    //    _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                    //    _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                    //}

                    //_HCoreContext.TUCBranch.Add(_TUCBranch);
                    //_HCoreContext.SaveChanges();
                    //long BranchId = _TUCBranch.Id;
                    //if (_Request.Manager.ReferenceId == 0 && !string.IsNullOrEmpty(_Request.Manager.EmailAddress))
                    //{
                    //    using (_HCoreContext = new HCoreContext())
                    //    {
                    //        _HCUAccountAuth = new HCUAccountAuth();
                    //        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    //        _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(8);
                    //        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                    //        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    //        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    //        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    //        if (_Request.UserReference.AccountId != 0)
                    //        {
                    //            _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    //        }
                    //        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    //        #region Save UserAccount
                    //        _HCUAccount = new HCUAccount();
                    //        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                    //        _HCUAccount.AccountTypeId = UserAccountType.Manager;
                    //        _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                    //        _HCUAccount.OwnerId = _Request.AccountId;
                    //        _HCUAccount.BankId = _Request.AccountId;
                    //        _HCUAccount.DisplayName = _Request.Manager.Name;
                    //        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    //        _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(14);
                    //        _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.Manager.MobileNumber, _Request.Manager.MobileNumber.Length);
                    //        _HCUAccount.RoleId = 1;
                    //        if (_Request.UserReference.AppVersionId != 0)
                    //        {
                    //            _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    //        }
                    //        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    //        _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                    //        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    //        if (_Request.UserReference.AccountId != 0)
                    //        {
                    //            _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    //        }
                    //        _HCUAccount.StatusId = HelperStatus.Default.Active;
                    //        _HCUAccount.Name = _Request.Manager.Name;
                    //        _HCUAccount.ContactNumber = _Request.Manager.MobileNumber;
                    //        _HCUAccount.EmailAddress = _Request.Manager.EmailAddress;
                    //        _HCUAccount.CountryId = _Request.UserReference.CountryId;
                    //        _HCUAccount.EmailVerificationStatus = 0;
                    //        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    //        _HCUAccount.NumberVerificationStatus = 0;
                    //        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    //        _HCUAccount.User = _HCUAccountAuth;
                    //        #endregion
                    //        _TUCBranchAccount = new TUCBranchAccount();
                    //        _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                    //        _TUCBranchAccount.BranchId = BranchId;
                    //        _TUCBranchAccount.Account = _HCUAccount;
                    //        if (_Request.Manager.OwnerId != 0)
                    //        {
                    //            _TUCBranchAccount.OwnerId = _Request.Manager.OwnerId;
                    //        }
                    //        if (_Request.Manager.RoleId != 0)
                    //        {
                    //            _TUCBranchAccount.AccountLevelId = _Request.Manager.RoleId;
                    //        }
                    //        else
                    //        {
                    //            _TUCBranchAccount.AccountLevelId = 6;
                    //        }
                    //        _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                    //        _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    //        _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                    //        _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                    //        _HCoreContext.TUCBranchAccount.Add(_TUCBranchAccount);
                    //        _HCoreContext.SaveChanges();
                    //        long ManagerId = _TUCBranchAccount.Id;
                    //        using (_HCoreContext = new HCoreContext())
                    //        {
                    //            TUCBranch ManagerUpdator = _HCoreContext.TUCBranch.Where(x => x.Id == BranchId).FirstOrDefault();
                    //            if (ManagerUpdator != null)
                    //            {
                    //                ManagerUpdator.ManagerId = ManagerId;
                    //                _HCoreContext.SaveChanges();
                    //            }
                    //            _HCoreContext.Dispose();
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    using (_HCoreContext = new HCoreContext())
                    //    {
                    //        var BrancAcc = _HCoreContext.TUCBranchAccount.Where(x => x.Id == _Request.Manager.ReferenceId).FirstOrDefault();
                    //        BrancAcc.EndDate = HCoreHelper.GetGMTDateTime();
                    //        BrancAcc.StatusId = HelperStatus.Default.Suspended;
                    //        BrancAcc.ModifyDate = HCoreHelper.GetGMTDateTime();
                    //        BrancAcc.ModifyById = _Request.UserReference.AccountId;
                    //        _TUCBranchAccount = new TUCBranchAccount();
                    //        _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                    //        _TUCBranchAccount.BranchId = BranchId;
                    //        _TUCBranchAccount.AccountId = BrancAcc.AccountId;
                    //        if (_Request.Manager.OwnerId != 0)
                    //        {
                    //            _TUCBranchAccount.OwnerId = _Request.Manager.OwnerId;
                    //        }
                    //        else
                    //        {
                    //            _TUCBranchAccount.OwnerId = AccountId;
                    //        }
                    //        if (_Request.Manager.RoleId != 0)
                    //        {
                    //            _TUCBranchAccount.AccountLevelId = _Request.Manager.RoleId;
                    //        }
                    //        else
                    //        {
                    //            _TUCBranchAccount.AccountLevelId = 6;
                    //        }
                    //        _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                    //        _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    //        _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                    //        _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                    //        _HCoreContext.TUCBranchAccount.Add(_TUCBranchAccount);
                    //        _HCoreContext.SaveChanges();
                    //    }

                    //}
                    #endregion
                    var _Response = new
                    {
                        ReferenceId = _TUCBranch.Id,
                        ReferenceKey = _TUCBranch.Guid,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1024, TUCCoreResource.CA1024M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveBranch", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to update branch
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateBranch(OBranch.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                //long AccountId = 0;
                //long? BranchManagerId = 0;

                //OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.Address, _Request.UserReference);
                #region TUCBranchAccount
                //using (_HCoreContext = new HCoreContext())
                //{
                //    var Acc = _HCoreContext.TUCBranch.Where(x => x.Id == _Request.ReferenceId
                //     && x.Guid == _Request.ReferenceKey).Select(x => new { x.OwnerId, x.ManagerId }).FirstOrDefault();
                //    if (Acc == null)
                //    {
                //        _HCoreContext.Dispose();
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                //    }
                //    else
                //    {
                //        BranchManagerId = Acc.ManagerId;
                //        AccountId = Acc.OwnerId;
                //        using (_HCoreContext = new HCoreContext())
                //        {
                //            var BrancAcc = _HCoreContext.TUCBranchAccount.Where(x => x.Id == _Request.Manager.ReferenceId).FirstOrDefault();
                //            BrancAcc.EndDate = HCoreHelper.GetGMTDateTime();
                //            BrancAcc.StatusId = HelperStatus.Default.Suspended;
                //            BrancAcc.ModifyDate = HCoreHelper.GetGMTDateTime();
                //            BrancAcc.ModifyById = _Request.UserReference.AccountId;
                //            _TUCBranchAccount = new TUCBranchAccount();
                //            _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                //            _TUCBranchAccount.BranchId = _Request.ReferenceId;
                //            _TUCBranchAccount.AccountId = BrancAcc.AccountId;
                //            if (_Request.Manager.OwnerId != 0 && _Request.Manager.OwnerId != _TUCBranchAccount.OwnerId)
                //            {
                //                _TUCBranchAccount.OwnerId = _Request.Manager.OwnerId;
                //            }
                //            if (_Request.Manager.RoleId != 0 && _Request.Manager.RoleId != _TUCBranchAccount.AccountLevelId)
                //            {
                //                _TUCBranchAccount.AccountLevelId = _Request.Manager.RoleId;
                //            }
                //            else
                //            {
                //                _TUCBranchAccount.AccountLevelId = 6;
                //            }
                //            _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                //            _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                //            _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                //            _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                //            _HCoreContext.TUCBranchAccount.Add(_TUCBranchAccount);
                //            _HCoreContext.SaveChanges();
                //        }
                //    }

                //    _HCoreContext.Dispose();
                //}
                #endregion
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                }

                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.Address, _Request.UserReference);

                using (_HCoreContext = new HCoreContext())
                {
                    TUCBranch Details = _HCoreContext.TUCBranch.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && Details.Name != _Request.Name)
                        {
                            Details.Name = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.DisplayName) && Details.DisplayName != _Request.DisplayName)
                        {
                            Details.DisplayName = _Request.DisplayName;
                        }
                        if (!string.IsNullOrEmpty(_Request.BranchCode) && Details.BranchCode != _Request.BranchCode)
                        {
                            Details.BranchCode = _Request.BranchCode;
                        }
                        if (!string.IsNullOrEmpty(_Request.PhoneNumber) && Details.PhoneNumber != _Request.PhoneNumber)
                        {
                            Details.PhoneNumber = _Request.PhoneNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.EmailAddress) && Details.EmailAddress != _Request.EmailAddress)
                        {
                            Details.EmailAddress = _Request.EmailAddress;
                        }
                        if (_AddressResponse != null)
                        {
                            if (!string.IsNullOrEmpty(_AddressResponse.Address) && Details.Address != _AddressResponse.Address)
                            {
                                Details.Address = _AddressResponse.Address;
                            }
                            if (_AddressResponse.CityAreaId != 0)
                            {
                                Details.CityAreaId = _AddressResponse.CityAreaId;
                            }
                            if (_AddressResponse.CityId != 0)
                            {
                                Details.CityId = _AddressResponse.CityId;
                            }
                            if (_AddressResponse.StateId != 0)
                            {
                                Details.StateId = _AddressResponse.StateId;
                            }
                            if (_AddressResponse.CountryId != 0)
                            {
                                Details.CountryId = _AddressResponse.CountryId;
                            }
                            if (_AddressResponse.Latitude != 0)
                            {
                                Details.Latitude = _AddressResponse.Latitude;
                            }
                            if (_AddressResponse.Longitude != 0)
                            {
                                Details.Longitude = _AddressResponse.Longitude;
                            }
                        }
                        if (_Request.Manager.ReferenceId > 0 && Details.ManagerId != _Request.Manager.ReferenceId)
                        {
                            var Manager = _HCoreContext.TUCBranchAccount.Where(x => x.BranchId == _Request.ReferenceId).FirstOrDefault();
                            if (Manager != null)
                            {
                                if (_Request.Manager.ReferenceId != 0)
                                {
                                    Details.ManagerId = _Request.Manager.ReferenceId;

                                    Manager.ManagerId = _Request.Manager.ReferenceId;
                                }
                                Manager.ModifyDate = HCoreHelper.GetGMTDateTime();
                                Manager.ModifyById = _Request.UserReference.AccountId;
                            }
                            else
                            {
                                _TUCBranchAccount = new TUCBranchAccount();
                                _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                                _TUCBranchAccount.OwnerId = Details.OwnerId;
                                _TUCBranchAccount.Branch = Details;
                                _TUCBranchAccount.AccountId = _Request.Manager.ReferenceId;
                                _TUCBranchAccount.ManagerId = _Request.Manager.ReferenceId;
                                _TUCBranchAccount.AccountLevelId = 6;
                                _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                                _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                                _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.TUCBranchAccount.Add(_TUCBranchAccount);
                            }
                        }
                        if (StatusId != 0)
                        {
                            Details.StatusId = StatusId;
                        }
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();

                        var _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0201, TUCCoreResource.CA0201M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateBranch", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion


        }

        /// <summary>
        /// Description: Method defined to get branch details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetBranch(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    OBranch.Details Details = _HCoreContext.TUCBranch
                                                .Where(x => x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OBranch.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    BranchCode = x.BranchCode,
                                                    DisplayName = x.DisplayName,
                                                    PhoneNumber = x.PhoneNumber,
                                                    EmailAddress = x.EmailAddress,

                                                    Address = x.Address,
                                                    Latitude = x.Latitude,
                                                    Longitude = x.Longitude,
                                                    CityName = x.City.Name,
                                                    FullAddress = x.Address,
                                                    CountryId = x.CountryId,
                                                    CountryKey = x.Country.Guid,
                                                    CountryName = x.Country.Name,
                                                    StateId = x.StateId,
                                                    StateKey = x.State.Guid,
                                                    StateName = x.State.Name,
                                                    CityId = x.CityId,
                                                    CityKey = x.City.Guid,
                                                    CityAreaId = x.CityAreaId,
                                                    CityAreaKey = x.CityArea.Guid,
                                                    CityAreaName = x.CityArea.Name,

                                                    Stores = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.MerchantStore && x.TUCBranchAccountAccount.Any(a => a.BranchId == _Request.ReferenceId)),
                                                    Terminals = _HCoreContext.TUCTerminal.Count(x => x.Store.TUCBranchAccountAccount.Any(a => a.BranchId == _Request.ReferenceId)),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.Store.TUCBranchAccountAccount.Any(a => a.BranchId == _Request.ReferenceId) && m.LastTransactionDate > ActivityDate),

                                                    ManagerId = x.ManagerId,
                                                    ManagerKey = x.Manager.Guid,
                                                    ManagerName = x.Manager.DisplayName,
                                                    ManagerMobileNumber = x.Manager.MobileNumber,
                                                    ManagerEmailAddress = x.Manager.EmailAddress,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).FirstOrDefault();
                    if (Details != null)
                    {
                        _OAddress = new OAddress();

                        _OAddress.Address = Details.FullAddress;
                        _OAddress.CityAreaId = Details.CityAreaId;
                        _OAddress.CityAreaCode = Details.CityAreaKey;
                        _OAddress.CityAreaName = Details.CityAreaName;

                        _OAddress.CityId = Details.CityId;
                        _OAddress.CityCode = Details.CityKey;
                        _OAddress.CityName = Details.CityName;

                        _OAddress.StateId = Details.StateId;
                        _OAddress.StateCode = Details.StateKey;
                        _OAddress.StateName = Details.StateName;
                        _OAddress.CountryId = (int)Details.CountryId;
                        _OAddress.CountryCode = Details.CountryKey;
                        _OAddress.CountryName = Details.CountryName;
                        _OAddress.Latitude = Details.Latitude;
                        _OAddress.Longitude = Details.Longitude;
                        Details.Address = _OAddress.Address;
                        Details.AddressComponent = _OAddress;
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetBranch", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get list of branchs
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetBranch(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.TUCBranch
                                                .Where(x => x.OwnerId == _Request.ReferenceId && x.Owner.Guid == _Request.ReferenceKey)
                                                .Select(x => new OBranch.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    BranchCode = x.BranchCode,
                                                    DisplayName = x.DisplayName,
                                                    PhoneNumber = x.PhoneNumber,
                                                    EmailAddress = x.EmailAddress,

                                                    Address = x.Address,
                                                    Latitude = x.Latitude,
                                                    Longitude = x.Longitude,
                                                    CityName = x.City.Name,

                                                    Stores = _HCoreContext.HCUAccount.Count(a => a.AccountTypeId == UserAccountType.MerchantStore && a.TUCBranchAccountAccount.Any(m => m.Branch.OwnerId == _Request.ReferenceId && m.Branch.Owner.Guid == _Request.ReferenceKey && m.BranchId == x.Id)),
                                                    Terminals = _HCoreContext.TUCTerminal.Count(a => a.Store.TUCBranchAccountAccount.Any(m => m.Branch.OwnerId == _Request.ReferenceId && m.Branch.Owner.Guid == _Request.ReferenceKey && m.BranchId == x.Id)),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(a => a.Store.TUCBranchAccountAccount.Any(m => m.Branch.OwnerId == _Request.ReferenceId && m.Branch.Owner.Guid == _Request.ReferenceKey && m.BranchId == x.Id) && a.LastTransactionDate > ActivityDate),

                                                    ManagerId = x.Manager.Id,
                                                    ManagerKey = x.Manager.Guid,
                                                    ManagerName = x.Manager.DisplayName,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OBranch.List> Data = _HCoreContext.TUCBranch
                                                .Where(x => x.OwnerId == _Request.ReferenceId && x.Owner.Guid == _Request.ReferenceKey)
                                                .Select(x => new OBranch.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    BranchCode = x.BranchCode,
                                                    DisplayName = x.DisplayName,
                                                    PhoneNumber = x.PhoneNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    Address = x.Address,
                                                    Latitude = x.Latitude,
                                                    Longitude = x.Longitude,
                                                    CityName = x.City.Name,

                                                    Stores = _HCoreContext.HCUAccount.Count(a => a.AccountTypeId == UserAccountType.MerchantStore && a.TUCBranchAccountAccount.Any(m => m.Branch.OwnerId == _Request.ReferenceId && m.Branch.Owner.Guid == _Request.ReferenceKey && m.BranchId == x.Id)),
                                                    Terminals = _HCoreContext.TUCTerminal.Count(a => a.Store.TUCBranchAccountAccount.Any(m => m.Branch.OwnerId == _Request.ReferenceId && m.Branch.Owner.Guid == _Request.ReferenceKey && m.BranchId == x.Id)),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(a => a.Store.TUCBranchAccountAccount.Any(m => m.Branch.OwnerId == _Request.ReferenceId && m.Branch.Owner.Guid == _Request.ReferenceKey && m.BranchId == x.Id) && a.LastTransactionDate > ActivityDate),

                                                    ManagerId = x.ManagerId,
                                                    ManagerKey = x.Manager.Guid,
                                                    ManagerName = x.Manager.DisplayName,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in Data)
                    {

                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetBranch", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to save manager
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveManager(OManager.Request _Request)
        {
            #region Manage Exception
            try
            {

                if (_Request.BranchId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1025, TUCCoreResource.CA1025M);
                }
                if (string.IsNullOrEmpty(_Request.BranchKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1026, TUCCoreResource.CA1026M);
                }
                if (_Request.RoleId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1027, TUCCoreResource.CA1027M);
                }
                if (string.IsNullOrEmpty(_Request.RoleKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1027, TUCCoreResource.CA1027M);
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1028, TUCCoreResource.CA1028M);
                }
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1029, TUCCoreResource.CA1029M);
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1030, TUCCoreResource.CA1030M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    bool _Exists = _HCoreContext.HCUAccount.Any(x => (x.EmailAddress == _Request.EmailAddress && (x.ContactNumber == _Request.MobileNumber || x.MobileNumber == _Request.MobileNumber)) && x.AccountTypeId == UserAccountType.Manager);
                    if (_Exists)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA14157, TUCCoreResource.CAA14157M);
                    }

                    var BranchDetails = _HCoreContext.TUCBranch.Where(x => x.Id == _Request.BranchId)
                        .Select(x => new
                        {
                            OwnerId = x.OwnerId,
                        }).FirstOrDefault();

                    #region Create UserName and Password
                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(8);
                    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    _HCoreContext.HCUAccountAuth.Add(_HCUAccountAuth);
                    #endregion

                    #region Save UserAccount
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccount.AccountTypeId = UserAccountType.Manager;
                    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                    _HCUAccount.OwnerId = BranchDetails.OwnerId;
                    _HCUAccount.BankId = BranchDetails.OwnerId;
                    _HCUAccount.DisplayName = _Request.Name;
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(14);
                    _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber, _Request.MobileNumber.Length);
                    _HCUAccount.RoleId = _Request.RoleId;
                    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccount.StatusId = HelperStatus.Default.Active;
                    _HCUAccount.Name = _Request.Name;
                    _HCUAccount.ContactNumber = _Request.MobileNumber;
                    _HCUAccount.EmailAddress = _Request.EmailAddress;
                    _HCUAccount.CountryId = _Request.UserReference.CountryId;
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.NumberVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.User = _HCUAccountAuth;
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    #endregion

                    #region
                    _TUCBranchAccount = new TUCBranchAccount();
                    _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                    _TUCBranchAccount.BranchId = _Request.BranchId;
                    _TUCBranchAccount.ManagerId = _Request.OwnerId;
                    _TUCBranchAccount.Account = _HCUAccount;
                    _TUCBranchAccount.OwnerId = BranchDetails.OwnerId;
                    _TUCBranchAccount.AccountLevelId = _Request.RoleId;
                    _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                    _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                    _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                    _HCoreContext.TUCBranchAccount.Add(_TUCBranchAccount);
                    #endregion

                    _HCoreContext.SaveChanges();
                    var _Response = new
                    {
                        ReferenceId = _HCUAccount.Id,
                        ReferenceKey = _HCUAccount.Guid,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1063, TUCCoreResource.CA1063M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveManager", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to update manager
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UpdateManager(OManager.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Old Code
                    //var BranchAccount = _HCoreContext.TUCBranchAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    //if (BranchAccount != null)
                    //{
                    //    if (_Request.BranchId != 0 && BranchAccount.BranchId != _Request.BranchId)
                    //    {
                    //        BranchAccount.BranchId = _Request.BranchId;
                    //    }
                    //    if (_Request.RoleId != 0)
                    //    {
                    //        BranchAccount.AccountLevelId = _Request.RoleId;
                    //    }
                    //    if (_Request.OwnerId != 0)
                    //    {
                    //        if (BranchAccount.AccountLevelId == 8)
                    //        {
                    //            long OwnerId = _HCoreContext.TUCBranchAccount.Where(x => x.Id == _Request.OwnerId).Select(x => x.AccountId).FirstOrDefault();
                    //            BranchAccount.OwnerId = OwnerId!=0? OwnerId : BranchAccount.OwnerId;
                    //        }
                    //        else
                    //        {
                    //            BranchAccount.OwnerId = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.OwnerId).Select(x => x.Id).FirstOrDefault();
                    //        }
                    //        //BranchAccount.OwnerId = _HCoreContext.TUCBranchAccount.Where(x => x.Id == _Request.BranchId).Select(x => x.AccountId).FirstOrDefault();
                    //    }
                    //    BranchAccount.ModifyDate = HCoreHelper.GetGMTDateTime();
                    //    BranchAccount.ModifyById = _Request.UserReference.AccountId;
                    //    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == BranchAccount.AccountId).FirstOrDefault();
                    //    if (AccountDetails != null)
                    //    {
                    //        if (!string.IsNullOrEmpty(_Request.Name) && AccountDetails.EmailAddress != _Request.Name)
                    //        {
                    //            AccountDetails.Name = _Request.Name;
                    //            AccountDetails.DisplayName = _Request.Name;
                    //        }
                    //        if (!string.IsNullOrEmpty(_Request.EmailAddress) && AccountDetails.EmailAddress != _Request.EmailAddress)
                    //        {
                    //            AccountDetails.EmailAddress = _Request.EmailAddress;
                    //        }
                    //        if (!string.IsNullOrEmpty(_Request.MobileNumber))
                    //        {
                    //            AccountDetails.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber, _Request.MobileNumber.Length);
                    //        }
                    //        if (_Request.RoleId != 0)
                    //        {
                    //            AccountDetails.RoleId = _Request.RoleId;
                    //        }
                    //        AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                    //        AccountDetails.ModifyById = _Request.UserReference.AccountId;
                    //    }
                    //    _HCoreContext.SaveChanges();
                    //    var _Response = new
                    //    {
                    //        ReferenceId = BranchAccount.Id,
                    //        ReferenceKey = BranchAccount.Guid,
                    //    };
                    //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0201, TUCCoreResource.CA0201M);
                    //}
                    #endregion

                    var Details = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && Details.Name != _Request.Name)
                        {
                            Details.Name = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.EmailAddress) && Details.EmailAddress != _Request.EmailAddress)
                        {
                            Details.EmailAddress = _Request.EmailAddress;
                        }
                        if (!string.IsNullOrEmpty(_Request.MobileNumber) && Details.MobileNumber != _Request.MobileNumber)
                        {
                            Details.MobileNumber = _Request.MobileNumber;
                        }
                        if (_Request.RoleId > 0 && Details.RoleId != _Request.RoleId)
                        {
                            Details.RoleId = _Request.RoleId;
                        }
                        if (_Request.BranchId > 0 && _Request.OwnerId > 0)
                        {
                            var Manager = _HCoreContext.TUCBranchAccount.Where(a => a.AccountId == Details.Id).FirstOrDefault();
                            if (Manager != null)
                            {
                                Manager.BranchId = _Request.BranchId;
                                Manager.ManagerId = _Request.OwnerId;
                                Manager.AccountLevelId = _Request.RoleId;
                                Manager.ModifyDate = HCoreHelper.GetGMTDateTime();
                                Manager.ModifyById = _Request.UserReference.AccountId;
                            }
                            else
                            {
                                _TUCBranchAccount = new TUCBranchAccount();
                                _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                                _TUCBranchAccount.BranchId = _Request.BranchId;
                                _TUCBranchAccount.ManagerId = _Request.OwnerId;
                                _TUCBranchAccount.Account = _HCUAccount;
                                _TUCBranchAccount.OwnerId = Details.OwnerId;
                                _TUCBranchAccount.AccountLevelId = _Request.RoleId;
                                _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                                _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                                _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.TUCBranchAccount.Add(_TUCBranchAccount);
                            }
                        }

                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();

                        var Response = new
                        {
                            ReferenceId = Details.Id,
                            ReferenceKey = Details.Guid,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, TUCCoreResource.CA0201, TUCCoreResource.CA0201M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }

                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateManager", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get manager details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetManager(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    OManager.Details Details = _HCoreContext.HCUAccount
                                                .Where(x => x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey
                                                && x.AccountTypeId == UserAccountType.Manager)
                                                .Select(x => new OManager.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    BranchId = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.BranchId).FirstOrDefault(),
                                                    BranchKey = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.Guid).FirstOrDefault(),
                                                    BranchName = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.Name).FirstOrDefault(),
                                                    BranchAddress = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.Address).FirstOrDefault(),

                                                    OwnerId = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.ManagerId).FirstOrDefault(),
                                                    OwnerKey = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Manager.Guid).FirstOrDefault(),
                                                    OwnerDisplayName = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Manager.DisplayName).FirstOrDefault(),

                                                    Name = x.Name,
                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,

                                                    CityName = x.City.Name,
                                                    IconUrl = x.IconStorage.Path,

                                                    Stores = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.MerchantStore && x.TUCBranchAccountAccount.Any(a => a.ManagerId == _Request.ReferenceId)),
                                                    Terminals = _HCoreContext.TUCTerminal.Count(x => x.Store.TUCBranchAccountAccount.Any(a => a.ManagerId == _Request.ReferenceId)),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.Store.TUCBranchAccountAccount.Any(a => a.ManagerId == _Request.ReferenceId) && m.LastTransactionDate > ActivityDate),

                                                    RoleId = x.RoleId,
                                                    RoleKey = x.Role.Guid,
                                                    RoleName = x.Role.Name,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).FirstOrDefault();
                    if (Details != null)
                    {
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetManager", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get list of managers
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetManager(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.BankId == _Request.ReferenceId && x.Bank.Guid == _Request.ReferenceKey
                                                && x.StatusId != HelperStatus.Default.Suspended && x.AccountTypeId == UserAccountType.Manager)
                                                .Select(x => new OManager.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    BranchId = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.BranchId).FirstOrDefault(),
                                                    BranchKey = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.Guid).FirstOrDefault(),
                                                    BranchName = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.Name).FirstOrDefault(),
                                                    BranchAddress = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.Address).FirstOrDefault(),

                                                    OwnerId = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.ManagerId).FirstOrDefault(),
                                                    OwnerKey = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Manager.Guid).FirstOrDefault(),
                                                    OwnerDisplayName = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Manager.DisplayName).FirstOrDefault(),

                                                    Name = x.Name,
                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,

                                                    //CityName = x.City.Name,
                                                    CityName = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.City.Name).FirstOrDefault(),
                                                    IconUrl = x.IconStorage.Path,

                                                    Stores = _HCoreContext.HCUAccount.Count(a => a.AccountTypeId == UserAccountType.MerchantStore && a.TUCBranchAccountAccount.Any(m => m.Manager.OwnerId == _Request.ReferenceId && m.Manager.Owner.Guid == _Request.ReferenceKey && m.ManagerId == x.Id)),
                                                    Terminals = _HCoreContext.TUCTerminal.Count(a => a.Store.TUCBranchAccountAccount.Any(m => m.Manager.OwnerId == _Request.ReferenceId && m.Manager.Owner.Guid == _Request.ReferenceKey && m.ManagerId == x.Id)),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(a => a.Store.TUCBranchAccountAccount.Any(m => m.Manager.OwnerId == _Request.ReferenceId && m.Manager.Owner.Guid == _Request.ReferenceKey && m.ManagerId == x.Id) && a.LastTransactionDate > ActivityDate),

                                                    RoleId = x.RoleId,
                                                    RoleKey = x.Role.Guid,
                                                    RoleName = x.Role.Name,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OManager.List> Data = _HCoreContext.HCUAccount
                                                .Where(x => x.BankId == _Request.ReferenceId && x.Bank.Guid == _Request.ReferenceKey
                                                && x.StatusId != HelperStatus.Default.Suspended && x.AccountTypeId == UserAccountType.Manager)
                                                .Select(x => new OManager.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    BranchId = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.BranchId).FirstOrDefault(),
                                                    BranchKey = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.Guid).FirstOrDefault(),
                                                    BranchName = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.Name).FirstOrDefault(),
                                                    BranchAddress = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.Address).FirstOrDefault(),

                                                    OwnerId = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.ManagerId).FirstOrDefault(),
                                                    OwnerKey = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Manager.Guid).FirstOrDefault(),
                                                    OwnerDisplayName = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Manager.DisplayName).FirstOrDefault(),

                                                    Name = x.Name,
                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,

                                                    //CityName = x.City.Name,
                                                    CityName = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.City.Name).FirstOrDefault(),
                                                    IconUrl = x.IconStorage.Path,

                                                    Stores = _HCoreContext.HCUAccount.Count(a => a.AccountTypeId == UserAccountType.MerchantStore && a.TUCBranchAccountAccount.Any(m => m.Manager.OwnerId == _Request.ReferenceId && m.Manager.Owner.Guid == _Request.ReferenceKey && m.ManagerId == x.Id)),
                                                    Terminals = _HCoreContext.TUCTerminal.Count(a => a.Store.TUCBranchAccountAccount.Any(m => m.Manager.OwnerId == _Request.ReferenceId && m.Manager.Owner.Guid == _Request.ReferenceKey && m.ManagerId == x.Id)),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(a => a.Store.TUCBranchAccountAccount.Any(m => m.Manager.OwnerId == _Request.ReferenceId && m.Manager.Owner.Guid == _Request.ReferenceKey && m.ManagerId == x.Id) && a.LastTransactionDate > ActivityDate),

                                                    RoleId = x.RoleId,
                                                    RoleKey = x.Role.Guid,
                                                    RoleName = x.Role.Name,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion

                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetManager", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to get list of branch managers
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetBranchManager(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.BankId == _Request.ReferenceId && x.Bank.Guid == _Request.ReferenceKey
                                                && x.StatusId != HelperStatus.Default.Suspended && x.AccountTypeId == UserAccountType.Manager)
                                                .Select(x => new OManager.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    BranchId = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.BranchId).FirstOrDefault(),
                                                    BranchKey = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.Guid).FirstOrDefault(),
                                                    BranchName = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.Name).FirstOrDefault(),
                                                    BranchAddress = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.Address).FirstOrDefault(),

                                                    OwnerId = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.ManagerId).FirstOrDefault(),
                                                    OwnerKey = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Manager.Guid).FirstOrDefault(),
                                                    OwnerDisplayName = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Manager.DisplayName).FirstOrDefault(),

                                                    Name = x.Name,
                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,

                                                    //CityName = x.City.Name,
                                                    CityName = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.City.Name).FirstOrDefault(),
                                                    IconUrl = x.IconStorage.Path,

                                                    Stores = _HCoreContext.HCUAccount.Count(a => a.AccountTypeId == UserAccountType.MerchantStore && a.TUCBranchAccountAccount.Any(m => m.Manager.OwnerId == _Request.ReferenceId && m.Manager.Owner.Guid == _Request.ReferenceKey && m.ManagerId == x.Id)),
                                                    Terminals = _HCoreContext.TUCTerminal.Count(a => a.Store.TUCBranchAccountAccount.Any(m => m.Manager.OwnerId == _Request.ReferenceId && m.Manager.Owner.Guid == _Request.ReferenceKey && m.ManagerId == x.Id)),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(a => a.Store.TUCBranchAccountAccount.Any(m => m.Manager.OwnerId == _Request.ReferenceId && m.Manager.Owner.Guid == _Request.ReferenceKey && m.ManagerId == x.Id) && a.LastTransactionDate > ActivityDate),

                                                    RoleId = x.RoleId,
                                                    RoleKey = x.Role.Guid,
                                                    RoleName = x.Role.Name,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                               .Where(y => y.BranchId == _Request.SubReferenceId && y.BranchKey == _Request.SubReferenceKey)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OManager.List> Data = _HCoreContext.HCUAccount
                                                .Where(x => x.BankId == _Request.ReferenceId && x.Bank.Guid == _Request.ReferenceKey
                                                && x.StatusId != HelperStatus.Default.Suspended && x.AccountTypeId == UserAccountType.Manager)
                                                .Select(x => new OManager.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    BranchId = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.BranchId).FirstOrDefault(),
                                                    BranchKey = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.Guid).FirstOrDefault(),
                                                    BranchName = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.Name).FirstOrDefault(),
                                                    BranchAddress = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.Address).FirstOrDefault(),

                                                    OwnerId = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.ManagerId).FirstOrDefault(),
                                                    OwnerKey = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Manager.Guid).FirstOrDefault(),
                                                    OwnerDisplayName = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Manager.DisplayName).FirstOrDefault(),

                                                    Name = x.Name,
                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,

                                                    //CityName = x.City.Name,
                                                    CityName = x.TUCBranchAccountAccount.Where(a => a.AccountId == x.Id).Select(m => m.Branch.City.Name).FirstOrDefault(),
                                                    IconUrl = x.IconStorage.Path,

                                                    Stores = _HCoreContext.HCUAccount.Count(a => a.AccountTypeId == UserAccountType.MerchantStore && a.TUCBranchAccountAccount.Any(m => m.Manager.OwnerId == _Request.ReferenceId && m.Manager.Owner.Guid == _Request.ReferenceKey && m.ManagerId == x.Id)),
                                                    Terminals = _HCoreContext.TUCTerminal.Count(a => a.Store.TUCBranchAccountAccount.Any(m => m.Manager.OwnerId == _Request.ReferenceId && m.Manager.Owner.Guid == _Request.ReferenceKey && m.ManagerId == x.Id)),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(a => a.Store.TUCBranchAccountAccount.Any(m => m.Manager.OwnerId == _Request.ReferenceId && m.Manager.Owner.Guid == _Request.ReferenceKey && m.ManagerId == x.Id) && a.LastTransactionDate > ActivityDate),

                                                    RoleId = x.RoleId,
                                                    RoleKey = x.Role.Guid,
                                                    RoleName = x.Role.Name,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(y=>y.BranchId==_Request.SubReferenceId && y.BranchKey==_Request.SubReferenceKey)
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion

                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetBranchManager", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }
}
