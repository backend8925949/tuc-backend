//==================================================================================
// FileName: FrameworkOnboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to onboarding merchant and customer functionality
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Acquirer;
using HCore.TUC.Core.Resource;
using System.Collections.Generic;
using HCore.Data.Operations;
using Akka.Actor;
using HCore.TUC.Core.Framework.Acquirer.Actor;
using HCore.Operations;

namespace HCore.TUC.Core.Framework.Acquirer
{
    internal class FrameworkOnboarding
    {
        //HCOMerchantUpload _HCOMerchantUpload;
        HCoreContext _HCoreContext;
        Random _Random;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        HCoreContextOperations _HCoreContextOperations;
        /// <summary>
        /// Description: Method defined to onboard merchants
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse OnboardMerchants(OOnboarding.Merchant.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1070, TUCCoreResource.CA1070M);
                }
                if (_Request.Data == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1068, TUCCoreResource.CA1068M);
                }
                if (_Request.Data.Count() < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1069, TUCCoreResource.CA1069M);
                }
                var system = ActorSystem.Create("ActorOnboardMerchant");
                var greeter = system.ActorOf<ActorOnboardMerchant>("ActorOnboardMerchant");
                greeter.Tell(_Request);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1071, TUCCoreResource.CA1071M);
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveMerchantInformation", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to get list of onboarded merchants
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetOnboardedMerchants(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContextOperations.HCOMerchantUpload
                                                .Where(x => x.OwnerId == _Request.ReferenceId)
                                                .Select(x => new OOnboarding.Merchant.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    Address = x.Address,
                                                    CityAreaName = x.CityAreaName,
                                                    CityName = x.CityName,
                                                    StateName = x.StateName,
                                                    CountryName = x.CountryName,
                                                    AccountId = x.AccountId,
                                                    CategoryName = x.CategoryName,
                                                    ContactPersonName = x.ContactPersonName,
                                                    ContactPersonMobileNumber = x.ContactPersonMobileNumber,
                                                    ContactPersonEmailAddress = x.ContactPersonEmailAddress,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    RewardPercentage = x.RewardPercentage,
                                                    CreateDate = x.CreateDate,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    StatusId = x.StatusId,
                                                    StatusMessage = x.StatusMessage,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OOnboarding.Merchant.Details> Data = _HCoreContextOperations.HCOMerchantUpload
                                                .Where(x => x.OwnerId == _Request.ReferenceId)
                                                .Select(x => new OOnboarding.Merchant.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    Address = x.Address,
                                                    CityAreaName = x.CityAreaName,
                                                    CityName = x.CityName,
                                                    StateName = x.StateName,
                                                    CountryName = x.CountryName,
                                                    AccountId = x.AccountId,
                                                    CategoryName = x.CategoryName,
                                                    ContactPersonName = x.ContactPersonName,
                                                    ContactPersonMobileNumber = x.ContactPersonMobileNumber,
                                                    ContactPersonEmailAddress = x.ContactPersonEmailAddress,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    RewardPercentage = x.RewardPercentage,
                                                    CreateDate = x.CreateDate,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    StatusId = x.StatusId,
                                                    StatusMessage = x.StatusMessage,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    //foreach (var DataItem in Data)
                    //{
                    //}
                    _HCoreContextOperations.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetOnboardedMerchants", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to upload terminal
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse UploadTerminal(OAccounts.TerminalUpload.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1039, TUCCoreResource.CA1039M);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1040, TUCCoreResource.CA1040M);
                }
                if (_Request.Terminals == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1041, TUCCoreResource.CA1041M);
                }
                if (_Request.Terminals.Count() == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1099, TUCCoreResource.CA1099M);
                }
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1100, TUCCoreResource.CA1100M);
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveTerminal", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to get list of uploaded terminals
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetUploadedTerminal(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    //if (_Request.RefreshCount)
                    //{
                    //    #region Total Records
                    //    _Request.TotalRecords = _HCoreContextOperations.HCOUploadTerminal
                    //                            .Where(x => x.AccountId == _Request.ReferenceId)
                    //                            .Select(x => new OOnboarding.Terminal.Details
                    //                            {
                    //                                ReferenceId = x.Id,
                    //                                ReferenceKey = x.Guid,
                    //                                AccountId = x.AccountId,
                    //                                MerchantId = x.MerchantId,
                    //                                MerchantDisplayName = x.MerchantName,
                    //                                StoreId = x.StoreId,
                    //                                StoreDisplayName = x.StoreName,
                    //                                AcquirerId = x.AcquirerId,
                    //                                AcquirerDisplayName = x.AcquirerName,
                    //                                ProviderId = x.ProviderId,
                    //                                ProviderDisplayName = x.ProviderName,
                    //                                CashierId = x.CashierId,
                    //                                CashierDisplayName = x.CashierName,
                    //                                CreateDate = x.CreateDate,
                    //                                CreatedById = x.CreatedById,
                    //                                ModifyDate = x.ModifyDate,
                    //                                ModifyById = x.ModifyById,
                    //                                ImportStatusId = x.ImportStatusId,
                    //                                StatusId = x.StatusId,
                    //                                Comment = x.Comment,
                    //                            })
                    //                           .Where(_Request.SearchCondition)
                    //                   .Count();
                    //    #endregion
                    //}
                    //#region Get Data
                    //List<OOnboarding.Terminal.Details> Data = _HCoreContextOperations.HCOUploadTerminal
                    //                            .Where(x => x.AccountId == _Request.ReferenceId)
                    //                            .Select(x => new OOnboarding.Terminal.Details
                    //                            {
                    //                                ReferenceId = x.Id,
                    //                                ReferenceKey = x.Guid,
                    //                                AccountId = x.AccountId,
                    //                                MerchantId = x.MerchantId,
                    //                                MerchantDisplayName = x.MerchantName,
                    //                                StoreId = x.StoreId,
                    //                                StoreDisplayName = x.StoreName,
                    //                                AcquirerId = x.AcquirerId,
                    //                                AcquirerDisplayName = x.AcquirerName,
                    //                                ProviderId = x.ProviderId,
                    //                                ProviderDisplayName = x.ProviderName,
                    //                                CashierId = x.CashierId,
                    //                                CashierDisplayName = x.CashierName,
                    //                                CreateDate = x.CreateDate,
                    //                                CreatedById = x.CreatedById,
                    //                                ModifyDate = x.ModifyDate,
                    //                                ModifyById = x.ModifyById,
                    //                                ImportStatusId = x.ImportStatusId,
                    //                                StatusId = x.StatusId,
                    //                                Comment = x.Comment,
                    //                            })
                    //                         .Where(_Request.SearchCondition)
                    //                         .OrderBy(_Request.SortExpression)
                    //                         .Skip(_Request.Offset)
                    //                         .Take(_Request.Limit)
                    //                         .ToList();
                    //#endregion
                    //#region Create  Response Object
                    //foreach (var DataItem in Data)
                    //{

                    //}
                    //_HCoreContextOperations.Dispose();
                    //OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    //#endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetUploadedTerminal", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to onboard customers
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse OnboardCustomers(OOnboarding.Customer.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1070, TUCCoreResource.CA1070M);
                }
                if (_Request.Data == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1068, TUCCoreResource.CA1068M);
                }
                if (_Request.Data.Count() < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1069, TUCCoreResource.CA1069M);
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var system = ActorSystem.Create("ActorOnboarCustomer");
                    var greeter = system.ActorOf<ActorOnboarCustomer>("ActorOnboarCustomer");
                    greeter.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1071, TUCCoreResource.CA1071M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardCustomers", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to get list of onboarded customers
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetOnboardedCustomers(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    if (_Request.ReferenceId != 0 && _Request.SubReferenceId != 0)
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContextOperations.HCOUploadCustomer
                                                    .Where(x => x.OwnerId == _Request.ReferenceId
                                                    && x.FileId == _Request.SubReferenceId)
                                                    .Select(x => new OOnboarding.Customer.Details
                                                    {
                                                        ReferenceId = x.Id,
                                                        Name = x.Name,
                                                        FirstName = x.FirstName,
                                                        LastName = x.LastName,
                                                        EmailAddress = x.EmailAddress,
                                                        Gender = x.Gender,
                                                        DateOfBirth = x.DateOfBirth,
                                                        FormattedMobileNumber = x.FormattedMobileNumber,
                                                        MobileNumber = x.MobileNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        CreatedById = x.CreatedById,
                                                        //CreatedByDisplayName = _HCoreContext.HCUAccount.Where(a=>a.Id == x.CreatedById).Select(a=>a.DisplayName).FirstOrDefault(),
                                                        CreateDate = x.CreateDate,
                                                        ModifyDate = x.ModifyDate,
                                                        ModifyById = x.ModifyById,
                                                        StatusId = x.StatusId,
                                                        StatusMessage = x.StatusMessage,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OOnboarding.Customer.Details> Data = _HCoreContextOperations.HCOUploadCustomer
                                                    .Where(x => x.OwnerId == _Request.ReferenceId
                                                    && x.FileId == _Request.SubReferenceId)
                                                    .Select(x => new OOnboarding.Customer.Details
                                                    {
                                                        ReferenceId = x.Id,
                                                        Name = x.Name,
                                                        FirstName = x.FirstName,
                                                        LastName = x.LastName,
                                                        EmailAddress = x.EmailAddress,
                                                        Gender = x.Gender,
                                                        DateOfBirth = x.DateOfBirth,
                                                        FormattedMobileNumber = x.FormattedMobileNumber,
                                                        MobileNumber = x.MobileNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        CreatedById = x.CreatedById,
                                                        //CreatedByDisplayName = _HCoreContext.HCUAccount.Where(a=>a.Id == x.CreatedById).Select(a=>a.DisplayName).FirstOrDefault(),
                                                        CreateDate = x.CreateDate,
                                                        ModifyDate = x.ModifyDate,
                                                        ModifyById = x.ModifyById,
                                                        StatusId = x.StatusId,
                                                        StatusMessage = x.StatusMessage
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        foreach (var DataItem in Data)
                        {
                            if (DataItem.StatusId == 1)
                            {
                                DataItem.StatusCode = "Pending";
                                DataItem.StatusName = "Pending";
                            }
                            else if (DataItem.StatusId == 2)
                            {
                                DataItem.StatusCode = "Processing";
                                DataItem.StatusName = "Processing";
                            }
                            else if (DataItem.StatusId == 3)
                            {
                                DataItem.StatusCode = "Success";
                                DataItem.StatusName = "Success";
                            }
                            else if (DataItem.StatusId == 4)
                            {
                                DataItem.StatusCode = "Error";
                                DataItem.StatusName = "Error";
                            }
                            DataItem.CreatedByDisplayName = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.CreatedById).Select(x => x.DisplayName).FirstOrDefault();
                        }
                        _HCoreContextOperations.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContextOperations.HCOUploadCustomer
                                                    .Where(x => x.OwnerId == _Request.ReferenceId)
                                                    .Select(x => new OOnboarding.Customer.Details
                                                    {
                                                        ReferenceId = x.Id,
                                                        Name = x.Name,
                                                        FirstName = x.FirstName,
                                                        LastName = x.LastName,
                                                        EmailAddress = x.EmailAddress,
                                                        Gender = x.Gender,
                                                        DateOfBirth = x.DateOfBirth,
                                                        FormattedMobileNumber = x.FormattedMobileNumber,
                                                        MobileNumber = x.MobileNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        CreatedById = x.CreatedById,
                                                        //CreatedByDisplayName = _HCoreContext.HCUAccount.Where(a=>a.Id == x.CreatedById).Select(a=>a.DisplayName).FirstOrDefault(),
                                                        CreateDate = x.CreateDate,
                                                        ModifyDate = x.ModifyDate,
                                                        ModifyById = x.ModifyById,
                                                        StatusId = x.StatusId,
                                                        StatusMessage = x.StatusMessage,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OOnboarding.Customer.Details> Data = _HCoreContextOperations.HCOUploadCustomer
                                                    .Where(x => x.OwnerId == _Request.ReferenceId)
                                                    .Select(x => new OOnboarding.Customer.Details
                                                    {
                                                        ReferenceId = x.Id,
                                                        Name = x.Name,
                                                        FirstName = x.FirstName,
                                                        LastName = x.LastName,
                                                        EmailAddress = x.EmailAddress,
                                                        Gender = x.Gender,
                                                        DateOfBirth = x.DateOfBirth,
                                                        FormattedMobileNumber = x.FormattedMobileNumber,
                                                        MobileNumber = x.MobileNumber,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        CreatedById = x.CreatedById,
                                                        //CreatedByDisplayName = _HCoreContext.HCUAccount.Where(a=>a.Id == x.CreatedById).Select(a=>a.DisplayName).FirstOrDefault(),
                                                        CreateDate = x.CreateDate,
                                                        ModifyDate = x.ModifyDate,
                                                        ModifyById = x.ModifyById,
                                                        StatusId = x.StatusId,
                                                        StatusMessage = x.StatusMessage
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        foreach (var DataItem in Data)
                        {
                            DataItem.CreatedByDisplayName = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.CreatedById).Select(x => x.DisplayName).FirstOrDefault();
                        }
                        _HCoreContextOperations.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetOnboardedCustomers", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Method defined to get list of onboarded customer files
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetOnboardedCustomerFiles(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContextOperations.HCOUploadFile
                                                .Where(x => x.OwnerId == _Request.ReferenceId)
                                                .Select(x => new OOnboarding.Customer.File
                                                {
                                                    ReferenceId = x.Id,
                                                    Name = x.Name,
                                                    TotalRecord = x.TotalRecord,
                                                    Processing = x.Processing,
                                                    Pending = x.Pending,
                                                    Success = x.Success,
                                                    Error = x.Error,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    StatusId = x.StatusId,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OOnboarding.Customer.File> Data = _HCoreContextOperations.HCOUploadFile
                                                .Where(x => x.OwnerId == _Request.ReferenceId)
                                                .Select(x => new OOnboarding.Customer.File
                                                {
                                                    ReferenceId = x.Id,
                                                    Name = x.Name,
                                                    TotalRecord = x.TotalRecord,
                                                    Processing = x.Processing,
                                                    Pending = x.Pending,
                                                    Success = x.Success,
                                                    Error = x.Error,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    StatusId = x.StatusId,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var DataItem in Data)
                    {
                        if (DataItem.StatusId == 1)
                        {
                            DataItem.StatusCode = "Pending";
                            DataItem.StatusName = "Pending";
                            DataItem.StatusMessage = "Pending";
                        }
                        else if (DataItem.StatusId == 2)
                        {
                            DataItem.StatusCode = "Processing";
                            DataItem.StatusName = "Processing";
                            DataItem.StatusMessage = "Processing";
                        }
                        else if (DataItem.StatusId == 3)
                        {
                            DataItem.StatusCode = "Complete";
                            DataItem.StatusName = "Complete";
                            DataItem.StatusMessage = "Complete";
                        }
                        else if (DataItem.StatusId == 4)
                        {
                            DataItem.StatusCode = "Error";
                            DataItem.StatusName = "Error";
                            DataItem.StatusMessage = "Error";
                        }
                        DataItem.CreatedByDisplayName = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.CreatedById).Select(x => x.DisplayName).FirstOrDefault();
                    }
                    _HCoreContextOperations.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetOnboardedCustomers", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        internal void ProcessCustomerOnboarding()
        {
            #region Manage Exception
            try
            {
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var PendingItems = _HCoreContextOperations.HCOUploadCustomer.Where(x => x.StatusId == 1).Skip(0).Take(500).ToList();
                    if (PendingItems.Count > 0)
                    {
                        string AllowSms = HCoreHelper.GetConfiguration("custimportonboardallowsms", PendingItems[0].OwnerId);
                        string SmsMessage = HCoreHelper.GetConfiguration("custimportonboardmessage", PendingItems[0].OwnerId);
                        string InfoBipUrl = HCoreHelper.GetConfiguration("infobip_url", PendingItems[0].OwnerId);
                        string InfoBipKey = HCoreHelper.GetConfiguration("infobip_key", PendingItems[0].OwnerId);
                        string InfoBipSenderId = HCoreHelper.GetConfiguration("infobip_senderid", PendingItems[0].OwnerId);

                        foreach (var PendingItem in PendingItems)
                        {
                            PendingItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                            PendingItem.ModifyById = SystemAccounts.ThankUCashSystemId;
                            PendingItem.StatusId = 2;
                            PendingItem.StatusMessage = "Processing";
                        }
                        _HCoreContextOperations.SaveChanges();
                        foreach (var _Request in PendingItems)
                        {

                            bool AllowProcess = true;
                            long ProcessStatusId = 1;
                            string ProcessMessage = "";
                            if (string.IsNullOrEmpty(_Request.MobileNumber))
                            {
                                ProcessStatusId = 4;
                                ProcessMessage = "Mobile number missing";
                                AllowProcess = false;
                            }
                            //if (string.IsNullOrEmpty(_Request.Name))
                            //{
                            //    ProcessStatusId = 4;
                            //    ProcessMessage = "Name missing";
                            //    AllowProcess = false;
                            //}
                            if (_Request.MobileNumber.Length < 10)
                            {
                                ProcessStatusId = 4;
                                ProcessMessage = "Invalid mobile number";
                                AllowProcess = false;
                            }
                            if (_Request.MobileNumber.Length > 13)
                            {
                                ProcessStatusId = 4;
                                ProcessMessage = "Invalid mobile number";
                                AllowProcess = false;
                            }
                            if (!System.Text.RegularExpressions.Regex.IsMatch(_Request.MobileNumber, "^[0-9]*$"))
                            {
                                ProcessStatusId = 4;
                                ProcessMessage = "Invalid mobile number";
                                AllowProcess = false;
                            }
                            long AccountId = 0;
                            if (AllowProcess)
                            {
                                #region Manage Operations
                                using (_HCoreContext = new HCoreContext())
                                {
                                    string MobileNumber = HCoreHelper.FormatMobileNumber("234", _Request.MobileNumber);
                                    #region  Process Registration
                                    string CustomerAppId = _AppConfig.AppUserPrefix + MobileNumber;
                                    bool AccountCheck = _HCoreContext.HCUAccountAuth.Any(x => x.Username == CustomerAppId);
                                    if (!AccountCheck)
                                    {
                                        DateTime CreateDate = HCoreHelper.GetGMTDateTime();
                                        int GenderId = 0;
                                        //string MerchantDisplayName = "";
                                        //if (_Request.OwnerId != 0)
                                        //{
                                        //    MerchantDisplayName = DataStore.DataStore.Merchants.Where(x => x.ReferenceId == _Request.OwnerId).Select(x => x.DisplayName).FirstOrDefault();
                                        //    if (string.IsNullOrEmpty(MerchantDisplayName))
                                        //    {
                                        //        MerchantDisplayName = DataStore.DataStore.Partners.Where(x => x.ReferenceId == _Request.OwnerId && (x.AccountTypeId == UserAccountType.Acquirer || x.AccountTypeId == UserAccountType.Partner)).Select(x => x.DisplayName).FirstOrDefault();
                                        //    }
                                        //}
                                        if (!string.IsNullOrEmpty(_Request.Gender))
                                        {
                                            if (_Request.Gender == "gender.male" || _Request.Gender == "male")
                                            {
                                                GenderId = Gender.Male;
                                            }
                                            if (_Request.Gender == "gender.female" || _Request.Gender == "female")
                                            {
                                                GenderId = Gender.Female;
                                            }
                                            if (_Request.Gender == "gender.other" || _Request.Gender == "other")
                                            {
                                                GenderId = Gender.Other;
                                            }
                                        }
                                        string UserKey = HCoreHelper.GenerateGuid();
                                        string UserAccountKey = HCoreHelper.GenerateGuid();
                                        _Random = new Random();
                                        string AccessPin = _Random.Next(1111, 9999).ToString();
                                        #region Save User
                                        _HCUAccountAuth = new HCUAccountAuth();
                                        _HCUAccountAuth.Guid = UserKey;
                                        _HCUAccountAuth.Username = CustomerAppId;
                                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(CustomerAppId);
                                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                        _HCUAccountAuth.CreateDate = CreateDate;
                                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                        _HCUAccountAuth.CreatedById = _Request.OwnerId;
                                        #endregion
                                        #region Online Account
                                        _HCUAccount = new HCUAccount();
                                        _HCUAccount.Guid = UserAccountKey;
                                        _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                                        _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                                        if (_Request.OwnerId != 0)
                                        {
                                            _HCUAccount.OwnerId = _Request.OwnerId;
                                        }
                                        _HCUAccount.MobileNumber = _Request.MobileNumber;
                                        if (!string.IsNullOrEmpty(_Request.FirstName))
                                        {
                                            _HCUAccount.DisplayName = _Request.FirstName;
                                        }
                                        else
                                        {
                                            _HCUAccount.DisplayName = _Request.Name;
                                        }
                                        _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(AccessPin);
                                        if (!string.IsNullOrEmpty(_Request.Name))
                                        {
                                            _HCUAccount.Name = _Request.Name;
                                        }

                                        if (!string.IsNullOrEmpty(_Request.MobileNumber))
                                        {
                                            _HCUAccount.MobileNumber = _Request.MobileNumber;
                                            _HCUAccount.ContactNumber = _Request.MobileNumber;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.EmailAddress))
                                        {
                                            _HCUAccount.EmailAddress = _Request.EmailAddress;
                                            _HCUAccount.SecondaryEmailAddress = _Request.EmailAddress;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.FirstName))
                                        {
                                            _HCUAccount.FirstName = _Request.FirstName;
                                        }
                                        if (!string.IsNullOrEmpty(_Request.LastName))
                                        {
                                            _HCUAccount.LastName = _Request.LastName;
                                        }
                                        if (string.IsNullOrEmpty(_HCUAccount.Name))
                                        {
                                            if (!string.IsNullOrEmpty(_Request.FirstName) && !string.IsNullOrEmpty(_Request.LastName))
                                            {
                                                _HCUAccount.Name = _Request.FirstName + " " + _Request.LastName;
                                            }
                                            else
                                            {
                                                _HCUAccount.Name = _HCUAccount.MobileNumber;
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(_Request.ReferenceNumber))
                                        {
                                            _HCUAccount.ReferenceNumber = _Request.ReferenceNumber;
                                        }
                                        if (GenderId != 0)
                                        {
                                            _HCUAccount.GenderId = GenderId;
                                        }
                                        if (_Request.DateOfBirth != null)
                                        {
                                            _HCUAccount.DateOfBirth = _Request.DateOfBirth;
                                        }
                                        _HCUAccount.CountryId = 1;
                                        _HCUAccount.EmailVerificationStatus = 0;
                                        _HCUAccount.EmailVerificationStatusDate = CreateDate;
                                        _HCUAccount.NumberVerificationStatus = 0;
                                        _HCUAccount.NumberVerificationStatusDate = CreateDate;
                                        _Random = new Random();
                                        string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                                        _HCUAccount.AccountCode = AccountCode;
                                        _HCUAccount.ReferralCode = _Request.MobileNumber;
                                        _HCUAccount.RegistrationSourceId = RegistrationSource.AcquirerImport;
                                        if (_Request.CreatedById != 0)
                                        {
                                            _HCUAccount.CreatedById = _Request.CreatedById;
                                        }
                                        _HCUAccount.CreateDate = CreateDate;
                                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                                        _HCUAccount.User = _HCUAccountAuth;
                                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                                        _HCoreContext.SaveChanges();
                                        AccountId = _HCUAccount.Id;
                                        ProcessStatusId = 3;
                                        ProcessMessage = "Customer onboarded";
                                        #endregion
                                        if ((HostEnvironment == HostEnvironmentType.Live && _Request.OwnerId == 139618) || (HostEnvironment == HostEnvironmentType.Test && _Request.OwnerId == 96689))
                                        {
                                            if (AllowSms != "0")
                                            {
                                                string Message = SmsMessage;
                                                if (!string.IsNullOrEmpty(Message))
                                                {
                                                    Message = Message.Replace("{{AccessPin}}", AccessPin);
                                                    Message = Message.Replace("{{Balance}}", "0");

                                                    if (!string.IsNullOrEmpty(Message)
                                               && Message != "0"
                                               && !string.IsNullOrEmpty(InfoBipUrl)
                                               && Message != "0"
                                               && !string.IsNullOrEmpty(InfoBipKey)
                                               && InfoBipKey != "0"
                                               && !string.IsNullOrEmpty(InfoBipSenderId)
                                               && InfoBipSenderId != "0")
                                                    {
                                                        HCoreHelper.SendSMS(SmsType.Transaction, "234", MobileNumber, Message, AccountId, _HCUAccount.Guid, null, InfoBipSenderId, InfoBipKey, InfoBipUrl);
                                                    }
                                                }
                                                else
                                                {
                                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", MobileNumber, Message, AccountId, _HCUAccount.Guid);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                                        && x.User.Username == CustomerAppId)
                                            .Select(x => new
                                            {
                                                AccountKey = x.Guid,
                                                AccountId = x.Id,
                                                AccessPin = x.AccessPin,
                                                StatusId = x.StatusId,
                                            })
                                          .FirstOrDefault();
                                        if (CustomerDetails != null)
                                        {
                                            ProcessStatusId = 4;
                                            ProcessMessage = "Customer already present";
                                            AllowProcess = false;
                                            //string AccessPin = HCoreEncrypt.DecryptHash(_HCoreContext.HCUAccount.Where(a => a.AccountTypeId == UserAccountType.Appuser && a.User.Username == CustomerAppId).Select(a => a.AccessPin).FirstOrDefault());
                                            if (CustomerDetails.StatusId == HelperStatus.Default.Active)
                                            {
                                                string AccessPin = null;
                                                if (!string.IsNullOrEmpty(CustomerDetails.AccessPin))
                                                {
                                                    AccessPin = HCoreEncrypt.DecryptHash(CustomerDetails.AccessPin);
                                                }
                                                _HCoreContext.Dispose();

                                                if ((HostEnvironment == HostEnvironmentType.Live && _Request.OwnerId == 139618) || (HostEnvironment == HostEnvironmentType.Test && _Request.OwnerId == 96689))
                                                {
                                                    string Message = SmsMessage;
                                                    if (AllowSms != "0")
                                                    {
                                                        if (!string.IsNullOrEmpty(Message)
                                                        && Message != "0"
                                                        && !string.IsNullOrEmpty(InfoBipUrl)
                                                        && Message != "0"
                                                        && !string.IsNullOrEmpty(InfoBipKey)
                                                        && InfoBipKey != "0"
                                                        && !string.IsNullOrEmpty(InfoBipSenderId)
                                                        && InfoBipSenderId != "0")
                                                        {

                                                            double Balance = 0;
                                                            Message = Message.Replace("{{AccessPin}}", AccessPin);

                                                            if (Message.Contains("{{Balance}}"))
                                                            {
                                                                ManageCoreTransaction _ManageCoreTransaction = new ManageCoreTransaction();
                                                                Balance = _ManageCoreTransaction.GetAppUserBalance(CustomerDetails.AccountId, true);
                                                            }
                                                            Message = Message.Replace("{{Balance}}", Balance.ToString());
                                                            HCoreHelper.SendSMS(SmsType.Transaction, "234", MobileNumber, Message, CustomerDetails.AccountId, CustomerDetails.AccountKey, null, InfoBipSenderId, InfoBipKey, InfoBipUrl);
                                                        }
                                                    }
                                                }
                                            }

                                        }

                                    }
                                    #endregion
                                }
                                #endregion
                            }
                            using (_HCoreContextOperations = new HCoreContextOperations())
                            {
                                long FileId = 0;
                                var UpItem = _HCoreContextOperations.HCOUploadCustomer.Where(x => x.Id == _Request.Id).FirstOrDefault();
                                if (UpItem != null)
                                {
                                    if (AccountId > 0)
                                    {
                                        UpItem.AccountId = AccountId;
                                    }
                                    UpItem.StatusId = ProcessStatusId;
                                    UpItem.StatusMessage = ProcessMessage;
                                    UpItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    _HCoreContextOperations.SaveChanges();
                                    if (UpItem.FileId != null)
                                    {
                                        FileId = (long)UpItem.FileId;
                                    }
                                }
                                else
                                {
                                    _HCoreContextOperations.SaveChanges();
                                }
                                if (FileId > 0)
                                {
                                    using (_HCoreContextOperations = new HCoreContextOperations())
                                    {

                                        var FileDetails = _HCoreContextOperations.HCOUploadFile.Where(x => x.Id == FileId).FirstOrDefault();
                                        if (FileDetails != null)
                                        {
                                            FileDetails.Pending = _HCoreContextOperations.HCOUploadCustomer.Count(x => x.FileId == FileId && x.StatusId == 1);
                                            FileDetails.Processing = _HCoreContextOperations.HCOUploadCustomer.Count(x => x.FileId == FileId && x.StatusId == 2);
                                            FileDetails.Success = _HCoreContextOperations.HCOUploadCustomer.Count(x => x.FileId == FileId && x.StatusId == 3);
                                            FileDetails.Error = _HCoreContextOperations.HCOUploadCustomer.Count(x => x.FileId == FileId && x.StatusId == 4);
                                            FileDetails.Completed = FileDetails.Success + FileDetails.Error;
                                            if (FileDetails.TotalRecord == FileDetails.Completed)
                                            {
                                                FileDetails.StatusId = 3;
                                            }
                                            // 1 => Pending 
                                            // 2 => Processing
                                            // 3 => Success 
                                            //FileDetails.StatusMessage = ProcessMessage;
                                            FileDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            _HCoreContextOperations.SaveChanges();
                                        }
                                        else
                                        {
                                            _HCoreContextOperations.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("ProcessCustomerOnboarding", _Exception, null);
                #endregion
            }
            #endregion
        }
    }
}
