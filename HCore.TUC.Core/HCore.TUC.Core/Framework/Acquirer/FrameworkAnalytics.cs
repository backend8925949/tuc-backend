//==================================================================================
// FileName: FrameworkAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to account overview and terminal activity
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Acquirer;
namespace HCore.TUC.Core.Framework.Acquirer
{
    public class FrameworkAnalytics
    {
        HCoreContext _HCoreContext;
        OAnalytics.Counts _Counts;
        OAnalytics.TerminalStatus _TerminalStatus;
        OAnalytics.DateRangeResponse _DateRangeResponse;
        List<OAnalytics.DateRange> _DateRanges;
        /// <summary>
        /// Description: Method defined to get terminal activity history
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetTerminalActivityHistory(OAnalytics.Request _Request)
        {
            _DateRangeResponse = new OAnalytics.DateRangeResponse();
            _DateRanges = new List<OAnalytics.DateRange>();
            try
            {
                int Days = (_Request.EndTime - _Request.StartTime).Days;
                DateTime StartTime = _Request.StartTime;
                DateTime EndTime = StartTime.AddDays(1);


                DateTime TDayStart = StartTime;
                DateTime TDayEnd = EndTime.AddHours(24).AddSeconds(-1);
                DateTime T7DayStart = TDayStart.Date.AddDays(-7);
                DateTime T7DayEnd = TDayStart.Date.AddSeconds(-1);
                DateTime TDeadDayEnd = TDayStart.AddDays(-7);

                using (_HCoreContext = new HCoreContext())
                {
                    long SubUserAccountId = 0;
                    long SubUserAccountTypeId = 0;
                    if (!string.IsNullOrEmpty(_Request.SubAccountKey) && _Request.SubAccountId > 0)
                    {
                        var SubUserAccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.SubAccountKey && x.Id == _Request.SubAccountId)
                         .Select(x => new
                         {
                             UserAccountId = x.Id,
                             AccountTypeId = x.AccountTypeId,
                         }).FirstOrDefault();
                        var SubAccountDetails = _HCoreContext.TUCTerminal.Where(x => x.Guid == _Request.SubAccountKey && x.Id == _Request.SubAccountId)
                         .Select(x => new
                         {
                             UserAccountId = x.Id,
                         }).FirstOrDefault();
                        if (SubUserAccountDetails != null)
                        {
                            SubUserAccountId = SubUserAccountDetails.UserAccountId;
                            SubUserAccountTypeId = SubUserAccountDetails.AccountTypeId;
                        }
                        else if (SubAccountDetails != null)
                        {
                            SubUserAccountId = SubAccountDetails.UserAccountId;
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1086");
                            #endregion
                        }
                    }
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey && x.AccountTypeId == UserAccountType.Acquirer)
                       .Select(x => new
                       {
                           UserAccountId = x.Id,
                           AccountTypeId = x.AccountTypeId,
                           OwnerId = x.OwnerId,
                       }).FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        if (SubUserAccountTypeId == UserAccountType.Merchant)
                        {
                            for (int i = 0; i < Days; i++)
                            {
                                _TerminalStatus = new OAnalytics.TerminalStatus();
                                _TerminalStatus.Active = _HCoreContext.TUCTerminalStatus.Count(x => x.BankId == _Request.AccountId && x.MerchantId == _Request.SubAccountId && x.ActivityStatusId == Helpers.ActivityStatusType.Active && (x.Date > StartTime && x.Date < EndTime));
                                _TerminalStatus.Idle = _HCoreContext.TUCTerminalStatus.Count(x => x.BankId == _Request.AccountId && x.MerchantId == _Request.SubAccountId && x.ActivityStatusId == Helpers.ActivityStatusType.Idle && (x.Date > StartTime && x.Date < EndTime));
                                _TerminalStatus.Dead = _HCoreContext.TUCTerminalStatus.Count(x => x.BankId == _Request.AccountId && x.MerchantId == _Request.SubAccountId && x.ActivityStatusId == Helpers.ActivityStatusType.Dead && (x.Date > StartTime && x.Date < EndTime));
                                _TerminalStatus.Inactive = _HCoreContext.TUCTerminalStatus.Count(x => x.BankId == _Request.AccountId && x.MerchantId == _Request.SubAccountId && x.ActivityStatusId == Helpers.ActivityStatusType.Unused && (x.Date > StartTime && x.Date < EndTime));
                                _DateRanges.Add(new OAnalytics.DateRange
                                {
                                    Data = _TerminalStatus,
                                    Date = StartTime,
                                    StartDate = StartTime,
                                    EndDate = EndTime,
                                });
                                StartTime = StartTime.AddDays(1);
                                EndTime = EndTime.AddDays(1);
                            }
                        }
                        else if (SubUserAccountTypeId == UserAccountType.MerchantStore)
                        {
                            for (int i = 0; i < Days; i++)
                            {
                                _TerminalStatus = new OAnalytics.TerminalStatus();
                                _TerminalStatus.Active = _HCoreContext.TUCTerminalStatus.Count(x => x.BankId == _Request.AccountId && x.StoreId == _Request.SubAccountId && x.ActivityStatusId == Helpers.ActivityStatusType.Active && (x.Date > StartTime && x.Date < EndTime));
                                _TerminalStatus.Idle = _HCoreContext.TUCTerminalStatus.Count(x => x.BankId == _Request.AccountId && x.StoreId == _Request.SubAccountId && x.ActivityStatusId == Helpers.ActivityStatusType.Idle && (x.Date > StartTime && x.Date < EndTime));
                                _TerminalStatus.Dead = _HCoreContext.TUCTerminalStatus.Count(x => x.BankId == _Request.AccountId && x.StoreId == _Request.SubAccountId && x.ActivityStatusId == Helpers.ActivityStatusType.Dead && (x.Date > StartTime && x.Date < EndTime));
                                _TerminalStatus.Inactive = _HCoreContext.TUCTerminalStatus.Count(x => x.BankId == _Request.AccountId && x.StoreId == _Request.SubAccountId && x.ActivityStatusId == Helpers.ActivityStatusType.Unused && (x.Date > StartTime && x.Date < EndTime));
                                _DateRanges.Add(new OAnalytics.DateRange
                                {
                                    Data = _TerminalStatus,
                                    Date = StartTime,
                                    StartDate = StartTime,
                                    EndDate = EndTime,
                                });
                                StartTime = StartTime.AddDays(1);
                                EndTime = EndTime.AddDays(1);
                            }
                        }
                        else if (SubUserAccountTypeId == UserAccountType.Terminal || SubUserAccountId > 0)
                        {
                            for (int i = 0; i < Days; i++)
                            {
                                _TerminalStatus = new OAnalytics.TerminalStatus();
                                _TerminalStatus.TransactionAmount = _HCoreContext.TUCTerminal.Where(x => x.Id == _Request.SubAccountId && x.AcquirerId == _Request.AccountId).Select(a => a.HCUAccountTransaction.Where(z => z.TerminalId == _Request.SubAccountId && z.BankId == _Request.AccountId && (z.TransactionDate > StartTime && z.TransactionDate < EndTime)).Select(m => m.PurchaseAmount).FirstOrDefault()).FirstOrDefault();
                                _DateRanges.Add(new OAnalytics.DateRange
                                {
                                    Data = _TerminalStatus,
                                    Date = StartTime,
                                    StartDate = StartTime,
                                    EndDate = EndTime,
                                });
                                StartTime = StartTime.AddDays(1);
                                EndTime = EndTime.AddDays(1);
                            }
                        }
                        else
                        {
                            for (int i = 0; i < Days; i++)
                            {
                                _TerminalStatus = new OAnalytics.TerminalStatus();
                                _TerminalStatus.Active = _HCoreContext.TUCTerminalStatus.Count(x => x.BankId == _Request.AccountId && x.ActivityStatusId == Helpers.ActivityStatusType.Active && (x.Date > StartTime && x.Date < EndTime));
                                _TerminalStatus.Idle = _HCoreContext.TUCTerminalStatus.Count(x => x.BankId == _Request.AccountId && x.ActivityStatusId == Helpers.ActivityStatusType.Idle && (x.Date > StartTime && x.Date < EndTime));
                                _TerminalStatus.Dead = _HCoreContext.TUCTerminalStatus.Count(x => x.BankId == _Request.AccountId && x.ActivityStatusId == Helpers.ActivityStatusType.Dead && (x.Date > StartTime && x.Date < EndTime));
                                _TerminalStatus.Inactive = _HCoreContext.TUCTerminalStatus.Count(x => x.BankId == _Request.AccountId && x.ActivityStatusId == Helpers.ActivityStatusType.Unused && (x.Date > StartTime && x.Date < EndTime));
                                _DateRanges.Add(new OAnalytics.DateRange
                                {
                                    Data = _TerminalStatus,
                                    Date = StartTime,
                                    StartDate = StartTime,
                                    EndDate = EndTime,
                                });
                                StartTime = StartTime.AddDays(1);
                                EndTime = EndTime.AddDays(1);
                            }
                        }
                    }
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DateRanges, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetTerminalActivityHistory", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

        /// <summary>
        /// Description: Method defined to get account overview
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetAccountOverview(OAnalytics.Request _Request)
        {
            _Counts = new OAnalytics.Counts();
            _Counts.TotalMerchants = 0;
            _Counts.ActiveMerchants = 0;
            _Counts.TotalStores = 0;
            _Counts.ActiveStores = 0;
            _Counts.TotalTerminals = 0;
            _Counts.ActiveTerminals = 0;
            _Counts.Ptsp = 0;
            _Counts.TotalTransactions = 0;
            _Counts.TotalSale = 0;
            _Counts.AverageTransactions = 0;
            _Counts.AverageTransactionAmount = 0;
            _Counts.CardTransactions = 0;
            _Counts.CardTransactionsAmount = 0;
            _Counts.CashTransactions = 0;
            _Counts.CashTransactionAmount = 0;
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long SubUserAccountId = 0;
                    long SubUserAccountTypeId = 0;
                    if (!string.IsNullOrEmpty(_Request.SubAccountKey) && _Request.SubAccountId > 0)
                    {
                        var SubUserAccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.SubAccountKey && x.Id == _Request.SubAccountId)
                         .Select(x => new
                         {
                             UserAccountId = x.Id,
                             AccountTypeId = x.AccountTypeId,
                         }).FirstOrDefault();
                        var SubAccountDetails = _HCoreContext.TUCTerminal.Where(x => x.Guid == _Request.SubAccountKey && x.Id == _Request.SubAccountId)
                         .Select(x => new
                         {
                             UserAccountId = x.Id,
                         }).FirstOrDefault();
                        if (SubUserAccountDetails != null)
                        {
                            SubUserAccountId = SubUserAccountDetails.UserAccountId;
                            SubUserAccountTypeId = SubUserAccountDetails.AccountTypeId;
                        }
                        else if (SubAccountDetails != null)
                        {
                            SubUserAccountId = SubAccountDetails.UserAccountId;
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1086");
                            #endregion
                        }
                    }
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey && x.AccountTypeId == UserAccountType.Acquirer)
                       .Select(x => new
                       {
                           UserAccountId = x.Id,
                           AccountTypeId = x.AccountTypeId,
                           OwnerId = x.OwnerId,
                       }).FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        DateTime TDayStart = HCoreHelper.GetGMTDate();
                        DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                        DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                        DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddDays(-1);
                        DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);
                        if (SubUserAccountTypeId == UserAccountType.Merchant)
                        {
                            #region  Overview
                            _Counts.TotalStores = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                    && x.OwnerId == _Request.SubAccountId
                                                    && (x.Owner.OwnerId == _Request.AccountId
                                                    || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == x.Id && m.AcquirerId == _Request.AccountId))).Select(x => x.Id).Distinct().Count();

                            _Counts.ActiveStores = _HCoreContext.HCUAccount
                                                  .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                    && x.OwnerId == _Request.SubAccountId
                                                   && x.HCUAccountTransactionSubParent.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd && a.BankId == _Request.AccountId)
                                                  && (x.Owner.OwnerId == _Request.AccountId
                                                  || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == x.Id && m.AcquirerId == _Request.AccountId))).Select(x => x.Id).Distinct().Count();

                            _Counts.Ptsp = _HCoreContext.TUCTerminal.Where(x => x.MerchantId == _Request.SubAccountId && x.AcquirerId == _Request.AccountId).Select(x => x.AcquirerId).Distinct().Count();
                            _Counts.TotalTerminals = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.SubAccountId && x.AcquirerId == AccountDetails.UserAccountId);
                            _Counts.ActiveTerminals = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.SubAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd));
                            _TerminalStatus = new OAnalytics.TerminalStatus();
                            _TerminalStatus.Total = _Counts.TotalTerminals;
                            _TerminalStatus.Active = _Counts.ActiveTerminals;
                            _TerminalStatus.Idle = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.SubAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd));
                            _TerminalStatus.Dead = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.SubAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate < TDeadDayEnd));
                            _TerminalStatus.Inactive = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.SubAccountId && x.AcquirerId == AccountDetails.UserAccountId && !x.HCUAccountTransaction.Any());
                            _Counts.TerminalStatus = _TerminalStatus;

                            _Counts.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ParentId == _Request.SubAccountId
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                           && m.ModeId == TransactionMode.Credit
                                                           && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                           || m.SourceId == TransactionSource.ThankUCashPlus)
                                                       ).Count();

                            _Counts.TotalSale = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.ParentId == _Request.SubAccountId
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                          && m.ModeId == TransactionMode.Credit
                                                          && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || m.SourceId == TransactionSource.ThankUCashPlus)
                                                      ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            if (_Counts.ActiveTerminals > 0)
                            {
                                _Counts.AverageTransactions = _Counts.TotalTransactions / _Counts.ActiveTerminals;
                            }
                            else
                            {
                                _Counts.AverageTransactions = 0;
                            }
                            if (_Counts.ActiveTerminals > 0)
                            {
                                _Counts.AverageTransactionAmount = _Counts.TotalSale / _Counts.ActiveTerminals;
                            }
                            else
                            {
                                _Counts.AverageTransactionAmount = 0;
                            }
                            _Counts.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.BankId == _Request.AccountId
                                                                    && m.ParentId == _Request.SubAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && m.TypeId == TransactionType.CardReward
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                            _Counts.CardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.BankId == _Request.AccountId
                                                                    && m.ParentId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            _Counts.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ParentId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Count();
                            _Counts.CashTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ParentId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;






                            _Counts.OwnerCardTransactions = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.BankId == _Request.AccountId
                                                                  && m.ParentId == _Request.SubAccountId
                                                                    && m.CardBank.AccountId == _Request.AccountId
                                                                 && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                 && m.TypeId == TransactionType.CardReward
                                                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                 && m.StatusId == HelperStatus.Transaction.Success
                                                               ).Count();
                            _Counts.OwnerCardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.BankId == _Request.AccountId
                                                                    && m.ParentId == _Request.SubAccountId
                                                                    && m.CardBank.AccountId == _Request.AccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;


                            #endregion
                            _Counts.CardTypeSale = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                       .Select(x => new OAnalytics.CardTypeSale
                                       {
                                           ReferenceId = x.Id,
                                           Name = x.Name,
                                           Transactions = 0,
                                           Amount = 0
                                       }).ToList();

                            foreach (var CardBrand in _Counts.CardTypeSale)
                            {

                                CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.BankId == _Request.AccountId
                                                          && m.ParentId == _Request.SubAccountId
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && m.CardBrandId == CardBrand.ReferenceId
                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                        ).Count();

                                CardBrand.Amount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.BankId == _Request.AccountId
                                                                 && m.ParentId == _Request.SubAccountId
                                                                 && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                 && m.CardBrandId == CardBrand.ReferenceId
                                                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                 && m.StatusId == HelperStatus.Transaction.Success
                                                               ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }
                            _Counts.OwnerCardTypeSale = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                  .Select(x => new OAnalytics.CardTypeSale
                                  {
                                      ReferenceId = x.Id,
                                      Name = x.Name,
                                      Transactions = 0,
                                      Amount = 0
                                  }).ToList();
                            foreach (var CardBrand in _Counts.OwnerCardTypeSale)
                            {

                                CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.BankId == _Request.AccountId
                                                          && m.ParentId == _Request.SubAccountId
                                                                    && m.CardBank.AccountId == _Request.AccountId
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && m.CardBrandId == CardBrand.ReferenceId
                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                        ).Count();

                                CardBrand.Amount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.BankId == _Request.AccountId
                                                                 && m.ParentId == _Request.SubAccountId
                                                                    && m.CardBank.AccountId == _Request.AccountId
                                                                 && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                 && m.CardBrandId == CardBrand.ReferenceId
                                                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                 && m.StatusId == HelperStatus.Transaction.Success
                                                               ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }

                        }
                        else if (SubUserAccountTypeId == UserAccountType.MerchantStore)
                        {

                            #region  Overview
                            _Counts.TotalTerminals = _HCoreContext.TUCTerminal.Count(x => x.StoreId == _Request.SubAccountId && x.AcquirerId == AccountDetails.UserAccountId);
                            _Counts.ActiveTerminals = _HCoreContext.TUCTerminal.Count(x => x.StoreId == _Request.SubAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd));
                            _TerminalStatus = new OAnalytics.TerminalStatus();
                            _TerminalStatus.Total = _Counts.TotalTerminals;
                            _TerminalStatus.Active = _Counts.ActiveTerminals;
                            _TerminalStatus.Idle = _HCoreContext.TUCTerminal.Count(x => x.StoreId == _Request.SubAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd));
                            _TerminalStatus.Dead = _HCoreContext.TUCTerminal.Count(x => x.StoreId == _Request.SubAccountId && x.AcquirerId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate < TDeadDayEnd));
                            _TerminalStatus.Inactive = _HCoreContext.TUCTerminal.Count(x => x.StoreId == _Request.SubAccountId && x.AcquirerId == AccountDetails.UserAccountId && !x.HCUAccountTransaction.Any());
                            _Counts.TerminalStatus = _TerminalStatus;
                            _Counts.Ptsp = _HCoreContext.TUCTerminal.Where(x => x.MerchantId == _Request.SubAccountId && x.AcquirerId == _Request.AccountId).Select(x => x.ProviderId).Distinct().Count();


                            _Counts.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.BankId == _Request.AccountId
                                                       && m.SubParentId == _Request.SubAccountId
                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                       && m.TransactionDate > _Request.StartTime
                                                       && m.TransactionDate < _Request.EndTime
                                                       && m.ModeId == TransactionMode.Credit
                                                       && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack || m.SourceId == TransactionSource.ThankUCashPlus)
                                                       && (m.TypeId == TransactionType.CardReward || m.TypeId == TransactionType.CashReward || m.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
                                                       && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                       || m.SourceId == TransactionSource.ThankUCashPlus)).Count();

                            _Counts.TotalSale = _HCoreContext.HCUAccountTransaction
                                                .Where(m => m.BankId == _Request.AccountId
                                                && m.SubParentId == _Request.SubAccountId
                                                && m.StatusId == HelperStatus.Transaction.Success
                                                && m.TransactionDate > _Request.StartTime
                                                && m.TransactionDate < _Request.EndTime
                                                && m.ModeId == TransactionMode.Credit
                                                && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack || m.SourceId == TransactionSource.ThankUCashPlus)
                                                && (m.TypeId == TransactionType.CardReward || m.TypeId == TransactionType.CashReward || m.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
                                                && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                || m.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            if (_Counts.ActiveTerminals > 0)
                            {
                                _Counts.AverageTransactions = _Counts.TotalTransactions / _Counts.ActiveTerminals;
                            }
                            else
                            {
                                _Counts.AverageTransactions = 0;
                            }
                            if (_Counts.ActiveTerminals > 0)
                            {
                                _Counts.AverageTransactionAmount = _Counts.TotalSale / _Counts.ActiveTerminals;
                            }
                            else
                            {
                                _Counts.AverageTransactionAmount = 0;
                            }
                            _Counts.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.BankId == _Request.AccountId
                                                                    && m.SubParentId == _Request.SubAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && m.TypeId == TransactionType.CardReward
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                            _Counts.CardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.BankId == _Request.AccountId
                                                                    && m.SubParentId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            _Counts.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.BankId == _Request.AccountId
                                                                    && m.SubParentId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Count();
                            _Counts.CashTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.BankId == _Request.AccountId
                                                                    && m.SubParentId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;




                            _Counts.OwnerCardTransactions = _HCoreContext.HCUAccountTransaction
                                                        .Where(m => m.BankId == _Request.AccountId
                                                          && m.SubParentId == _Request.SubAccountId
                                                                    && m.CardBank.AccountId == _Request.AccountId
                                                          && m.TransactionDate > _Request.StartTime
                                                         && m.TransactionDate < _Request.EndTime
                                                         && m.TypeId == TransactionType.CardReward
                                                         && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                         || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                         && m.StatusId == HelperStatus.Transaction.Success
                                                       ).Count();
                            _Counts.OwnerCardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.BankId == _Request.AccountId
                                                                    && m.SubParentId == _Request.SubAccountId
                                                                    && m.CardBank.AccountId == _Request.AccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            #endregion
                            _Counts.CardTypeSale = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                       .Select(x => new OAnalytics.CardTypeSale
                                       {
                                           ReferenceId = x.Id,
                                           Name = x.Name,
                                           Transactions = 0,
                                           Amount = 0
                                       }).ToList();

                            foreach (var CardBrand in _Counts.CardTypeSale)
                            {

                                CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.BankId == _Request.AccountId
                                                          && m.SubParentId == _Request.SubAccountId
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && m.CardBrandId == CardBrand.ReferenceId
                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                        ).Count();

                                CardBrand.Amount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.BankId == _Request.AccountId
                                                                 && m.SubParentId == _Request.SubAccountId
                                                                 && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                 && m.CardBrandId == CardBrand.ReferenceId
                                                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                 && m.StatusId == HelperStatus.Transaction.Success
                                                               ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }
                            _Counts.OwnerCardTypeSale = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                .Select(x => new OAnalytics.CardTypeSale
                                {
                                    ReferenceId = x.Id,
                                    Name = x.Name,
                                    Transactions = 0,
                                    Amount = 0
                                }).ToList();

                            foreach (var CardBrand in _Counts.OwnerCardTypeSale)
                            {

                                CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.BankId == _Request.AccountId
                                                          && m.SubParentId == _Request.SubAccountId
                                                          && m.CardBank.AccountId == _Request.AccountId

                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && m.CardBrandId == CardBrand.ReferenceId
                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                        ).Count();

                                CardBrand.Amount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.BankId == _Request.AccountId
                                                                 && m.SubParentId == _Request.SubAccountId
                                                                  && m.CardBank.AccountId == _Request.AccountId

                                                                 && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                 && m.CardBrandId == CardBrand.ReferenceId
                                                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                 && m.StatusId == HelperStatus.Transaction.Success
                                                               ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }

                        }
                        else if (SubUserAccountTypeId == UserAccountType.Terminal || SubUserAccountId > 0)
                        {

                            #region  Overview
                            _Counts.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.BankId == _Request.AccountId
                                                            && m.TerminalId == _Request.SubAccountId
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                           && m.ModeId == TransactionMode.Credit
                                                           && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                           || m.SourceId == TransactionSource.ThankUCashPlus)
                                                       ).Count();

                            _Counts.TotalSale = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.BankId == _Request.AccountId
                                                            && m.TerminalId == _Request.SubAccountId
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && m.ModeId == TransactionMode.Credit
                                                          && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || m.SourceId == TransactionSource.ThankUCashPlus)
                                                      ).Sum(m => (double?)m.PurchaseAmount) ?? 0;


                            _Counts.AverageTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(a => a.BankId == _Request.AccountId
                                                              && a.TerminalId == _Request.SubAccountId
                                                          && a.StatusId == HelperStatus.Transaction.Success
                                                          && a.ModeId == TransactionMode.Credit
                                                          && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                                          && a.SourceId == TransactionSource.TUC)
                                                          || a.SourceId == TransactionSource.ThankUCashPlus))
                                                              .Average(m => (double?)m.PurchaseAmount) ?? 0;

                            //if (_Counts.ActiveTerminals > 0)
                            //{
                            //    _Counts.AverageTransactions = _Counts.TotalTransactions / _Counts.ActiveTerminals;
                            //}
                            //else
                            //{
                            //    _Counts.AverageTransactions = 0;
                            //}
                            //if (_Counts.ActiveTerminals > 0)
                            //{
                            //    _Counts.AverageTransactionAmount = _Counts.TotalSale / _Counts.ActiveTerminals;
                            //}
                            //else
                            //{
                            //    _Counts.AverageTransactionAmount = 0;
                            //}
                            _Counts.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.BankId == _Request.AccountId
                                                                    && m.TerminalId == _Request.SubAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && m.TypeId == TransactionType.CardReward
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                            _Counts.CardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.BankId == _Request.AccountId
                                                                    && m.TerminalId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            _Counts.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.BankId == _Request.AccountId
                                                                    && m.TerminalId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Count();
                            _Counts.CashTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.BankId == _Request.AccountId
                                                                    && m.TerminalId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            #endregion
                            _Counts.CardTypeSale = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                       .Select(x => new OAnalytics.CardTypeSale
                                       {
                                           ReferenceId = x.Id,
                                           Name = x.Name,
                                           Transactions = 0,
                                           Amount = 0
                                       }).ToList();

                            foreach (var CardBrand in _Counts.CardTypeSale)
                            {

                                CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.BankId == _Request.AccountId
                                                          && m.TerminalId == _Request.SubAccountId
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && m.CardBrandId == CardBrand.ReferenceId
                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                        ).Count();

                                CardBrand.Amount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.BankId == _Request.AccountId
                                                                 && m.TerminalId == _Request.SubAccountId
                                                                 && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                 && m.CardBrandId == CardBrand.ReferenceId
                                                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                 && m.StatusId == HelperStatus.Transaction.Success
                                                               ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }
                        }
                        else
                        {

                            _Counts.Ptsp = _HCoreContext.TUCTerminal.Where(x => x.ProviderId == _Request.SubAccountId && x.AcquirerId == _Request.AccountId).Select(x => x.AcquirerId).Distinct().Count();

                            #region  Overview
                            _Counts.TotalMerchants = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && (x.OwnerId == _Request.AccountId
                                                    || _HCoreContext.TUCTerminal.Any(m => m.MerchantId == x.Id && m.AcquirerId == _Request.AccountId))).Select(x => x.Id).Distinct().Count();

                            _Counts.ActiveMerchants = _HCoreContext.HCUAccount
                                                   .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                   && x.HCUAccountTransactionParent.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd && a.BankId == _Request.AccountId)
                                                   && (x.OwnerId == _Request.AccountId
                                                   || _HCoreContext.TUCTerminal.Any(m => m.MerchantId == x.Id && m.AcquirerId == _Request.AccountId))).Select(x => x.Id).Distinct().Count();

                            _Counts.TotalStores = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                        && (x.Owner.OwnerId == _Request.AccountId || _HCoreContext.TUCTerminal.Any(m => m.AcquirerId == _Request.AccountId && m.StoreId == x.Id))).Count();

                            _Counts.ActiveStores = _HCoreContext.HCUAccount
                                                  .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                   && x.HCUAccountTransactionSubParent.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd && a.BankId == _Request.AccountId)
                                                   && (x.OwnerId == _Request.AccountId
                                                   || _HCoreContext.TUCTerminal.Any(m => m.StoreId == x.Id && m.AcquirerId == _Request.AccountId))).Select(x => x.Id).Distinct().Count();

                            _Counts.TotalTerminals = _HCoreContext.TUCTerminal.Count(x => x.AcquirerId == AccountDetails.UserAccountId);
                            _Counts.ActiveTerminals = _HCoreContext.TUCTerminal.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd));
                            _TerminalStatus = new OAnalytics.TerminalStatus();
                            _TerminalStatus.Total = _Counts.TotalTerminals;
                            _TerminalStatus.Active = _Counts.ActiveTerminals;
                            _TerminalStatus.Idle = _HCoreContext.TUCTerminal.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd));
                            _TerminalStatus.Dead = _HCoreContext.TUCTerminal.Count(x => x.AcquirerId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate < TDeadDayEnd));
                            _TerminalStatus.Inactive = _HCoreContext.TUCTerminal.Count(x => x.AcquirerId == AccountDetails.UserAccountId && !x.HCUAccountTransaction.Any());
                            _Counts.TerminalStatus = _TerminalStatus;


                            //x => x.Account.AccountTypeId == UserAccountType.Appuser
                            //                    && x.Parent.CountryId == _Request.UserReference.SystemCountry
                            //                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack || x.SourceId == TransactionSource.ThankUCashPlus)
                            //                    && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)


                            _Counts.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.BankId == _Request.AccountId
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                           && m.TransactionDate > _Request.StartTime && m.TransactionDate < _Request.EndTime
                                                           && m.ModeId == TransactionMode.Credit
                                                           && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack || m.SourceId == TransactionSource.ThankUCashPlus)
                                                           && (m.TypeId == TransactionType.CardReward || m.TypeId == TransactionType.CashReward || m.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
                                                           && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                           || m.SourceId == TransactionSource.ThankUCashPlus)).Count();

                            _Counts.TotalSale = _HCoreContext.HCUAccountTransaction
                                                .Where(m => m.BankId == _Request.AccountId
                                                && m.StatusId == HelperStatus.Transaction.Success
                                                && m.TransactionDate > _Request.StartTime
                                                && m.TransactionDate < _Request.EndTime
                                                && m.ModeId == TransactionMode.Credit
                                                && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack || m.SourceId == TransactionSource.ThankUCashPlus)
                                                && (m.TypeId == TransactionType.CardReward || m.TypeId == TransactionType.CashReward || m.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
                                                && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                || m.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            if (_Counts.ActiveTerminals > 0)
                            {
                                _Counts.AverageTransactions = _Counts.TotalTransactions / _Counts.ActiveTerminals;
                            }
                            else
                            {
                                _Counts.AverageTransactions = 0;
                            }
                            if (_Counts.ActiveTerminals > 0)
                            {
                                _Counts.AverageTransactionAmount = _Counts.TotalSale / _Counts.ActiveTerminals;
                            }
                            else
                            {
                                _Counts.AverageTransactionAmount = 0;
                            }
                            _Counts.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.BankId == _Request.AccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && m.TypeId == TransactionType.CardReward
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                            _Counts.CardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.BankId == _Request.AccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            _Counts.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.BankId == _Request.AccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Count();
                            _Counts.CashTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.BankId == _Request.AccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            #endregion
                            _Counts.CardTypeSale = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                       .Select(x => new OAnalytics.CardTypeSale
                                       {
                                           ReferenceId = x.Id,
                                           Name = x.Name,
                                           Transactions = 0,
                                           Amount = 0
                                       }).ToList();

                            foreach (var CardBrand in _Counts.CardTypeSale)
                            {

                                CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.BankId == _Request.AccountId
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && m.CardBrandId == CardBrand.ReferenceId
                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                        ).Count();

                                CardBrand.Amount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.BankId == _Request.AccountId
                                                                 && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                 && m.CardBrandId == CardBrand.ReferenceId
                                                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                 && m.StatusId == HelperStatus.Transaction.Success
                                                               ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Counts, "HC0001", "Details loaded");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001", "Details not found");
                        #endregion
                    }

                }


            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

    }
}
