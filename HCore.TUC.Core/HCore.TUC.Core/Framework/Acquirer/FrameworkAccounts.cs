//==================================================================================
// FileName: FrameworkAccounts.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to customers,merchants,stores,terminals
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Acquirer;
using HCore.TUC.Core.Resource;
namespace HCore.TUC.Core.Framework.Acquirer
{
    internal class FrameworkAccounts
    {
        #region Declare
        HCoreContext _HCoreContext;
        List<OAccounts.Customer.List> _Customers;
        List<OAccounts.Merchant.List> _Merchants;
        List<OAccounts.Store.List> _Stores;
        List<OAccounts.Terminal.List> _Terminals;
        OAccounts.Merchant.Details _MerchantDetails;
        OAccounts.Customer.Details _CustomerDetails;
        OAccounts.Store.Details _StoreDetails;
        OAccounts.Terminal.Details _TerminalDetails;
        OAddress _OAddress;

        #endregion
        /// <summary>
        /// Description: Method defined to get customer details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCustomer(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _CustomerDetails = new OAccounts.Customer.Details();
                    _CustomerDetails = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                && x.OwnerId == _Request.AccountId
                                                && x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OAccounts.Customer.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    FirstName = x.FirstName,
                                                    LastName = x.LastName,
                                                    DateOfBirth = x.DateOfBirth,
                                                    GenderCode = x.Gender.SystemName,
                                                    GenderName = x.Gender.Name,
                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    IconUrl = x.IconStorage.Path,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).FirstOrDefault();
                    if (_CustomerDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_CustomerDetails.ReferenceNumber))
                        {
                            _CustomerDetails.ReferenceNumber = HCoreEncrypt.DecryptHash(_CustomerDetails.ReferenceNumber);
                        }
                        if (!string.IsNullOrEmpty(_CustomerDetails.IconUrl))
                        {
                            _CustomerDetails.IconUrl = _AppConfig.StorageUrl + _CustomerDetails.IconUrl;
                        }
                        else
                        {
                            _CustomerDetails.IconUrl = _AppConfig.Default_Icon;
                        }
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetCustomer", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get list of customers
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetCustomer(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Customers = new List<OAccounts.Customer.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                && x.OwnerId == _Request.ReferenceId)
                                                .Select(x => new OAccounts.Customer.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    EmailAddress = x.EmailAddress,
                                                    MobileNumber = x.MobileNumber,
                                                    GenderCode = x.Gender.SystemName,
                                                    GenderName = x.Gender.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Customers = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                && x.OwnerId == _Request.ReferenceId)
                                                .Select(x => new OAccounts.Customer.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    EmailAddress = x.EmailAddress,
                                                    MobileNumber = x.MobileNumber,
                                                    GenderCode = x.Gender.SystemName,
                                                    GenderName = x.Gender.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Customers)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCustomer", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get merchant details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetMerchant(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    _MerchantDetails = new OAccounts.Merchant.Details();
                    _MerchantDetails = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                && x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OAccounts.Merchant.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    AccountCode = x.AccountCode,
                                                    OwnerName = x.Owner.DisplayName,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    FullAddress = x.Address,
                                                    Latitude = x.Latitude,
                                                    Longitude = x.Longitude,
                                                    CountryId = x.CountryId,
                                                    CountryKey = x.Country.Guid,
                                                    CountryName = x.Country.Name,
                                                    StateId = x.StateId,
                                                    StateKey = x.State.Guid,
                                                    StateName = x.State.Name,
                                                    CityId = x.CityId,
                                                    CityKey = x.City.Guid,
                                                    CityName = x.City.Name,
                                                    CityAreaId = x.CityAreaId,
                                                    CityAreaKey = x.CityArea.Guid,
                                                    CityAreaName = x.CityArea.Name,
                                                    ReferralCode = x.ReferralCode,
                                                    RewardPercentage = x.AccountPercentage,
                                                    LastActivityDate = x.LastActivityDate,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).FirstOrDefault();
                    if (_MerchantDetails != null)
                    {
                        _MerchantDetails.Stores = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.MerchantStore && x.OwnerId == _MerchantDetails.ReferenceId);
                        _MerchantDetails.Terminals = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _MerchantDetails.ReferenceId && (x.AcquirerId == _MerchantDetails.OwnerId || x.AcquirerId == _Request.AccountId));
                        _MerchantDetails.ActiveTerminals = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _MerchantDetails.ReferenceId && x.AcquirerId == _MerchantDetails.OwnerId && x.LastTransactionDate > ActivityDate);
                        _MerchantDetails.ContactPerson = _HCoreContext.HCUAccount.Where(x => x.Id == _MerchantDetails.ReferenceId)
                            .Select(x => new OAccounts.Merchant.ContactDetails
                            {
                                Name = x.FirstName + " " + x.LastName,
                                MobileNumber = x.MobileNumber,
                                EmailAddress = x.EmailAddress,
                            }).FirstOrDefault();

                        _MerchantDetails.Rm = _HCoreContext.TUCBranchAccount.Where(x => x.MerchantId == _MerchantDetails.ReferenceId)
                        .Select(x => new OAccounts.Merchant.ManagerDetails
                        {
                            RmId = x.AccountId,
                            RmKey = x.Account.Guid,
                            Name = x.Account.Name,
                            MobileNumber = x.Account.MobileNumber,
                            EmailAddress = x.Account.EmailAddress,
                        }).FirstOrDefault();
                        if (!string.IsNullOrEmpty(_MerchantDetails.IconUrl))
                        {
                            _MerchantDetails.IconUrl = _AppConfig.StorageUrl + _MerchantDetails.IconUrl;
                        }
                        else
                        {
                            _MerchantDetails.IconUrl = _AppConfig.Default_Icon;
                        }

                        _OAddress = new OAddress();

                        _OAddress.Address = _MerchantDetails.FullAddress;
                        _OAddress.CityAreaId = _MerchantDetails.CityAreaId;
                        _OAddress.CityAreaCode = _MerchantDetails.CityAreaKey;
                        _OAddress.CityAreaName = _MerchantDetails.CityAreaName;

                        _OAddress.CityId = _MerchantDetails.CityId;
                        _OAddress.CityCode = _MerchantDetails.CityKey;
                        _OAddress.CityName = _MerchantDetails.CityName;

                        _OAddress.StateId = _MerchantDetails.StateId;
                        _OAddress.StateCode = _MerchantDetails.StateKey;
                        _OAddress.StateName = _MerchantDetails.StateName;
                        _OAddress.CountryId = (int)_MerchantDetails.CountryId;
                        _OAddress.CountryCode = _MerchantDetails.CountryKey;
                        _OAddress.CountryName = _MerchantDetails.CountryName;
                        _OAddress.Latitude = _MerchantDetails.Latitude;
                        _OAddress.Longitude = _MerchantDetails.Longitude;
                        _MerchantDetails.Address = _OAddress.Address;
                        _MerchantDetails.AddressComponent = _OAddress;

                        _MerchantDetails.Categories = _HCoreContext.TUCCategoryAccount.Where(x => x.AccountId == _MerchantDetails.ReferenceId
                           && x.StatusId == HelperStatus.Default.Active)
                               .Select(x => new OAccounts.Merchant.Category
                               {
                                   ReferenceId = x.Id,
                                   ReferenceKey = x.Guid,
                                   Name = x.Category.Name,
                               }).ToList();
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _MerchantDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetMerchant", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get list of merchants
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetMerchant(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
                DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
                DateTime T7DayStart = TDayStart.AddDays(-7);
                DateTime T7DayEnd = TDayStart.AddSeconds(1);
                DateTime TDeadDayEnd = T7DayStart;
                _Merchants = new List<OAccounts.Merchant.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                && x.OwnerId == _Request.ReferenceId)
                                                .Select(x => new OAccounts.Merchant.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    Address = x.Address,
                                                    CityName = x.City.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    Stores = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantStore),
                                                    Terminals = _HCoreContext.TUCTerminal.Count(m => m.AcquirerId == _Request.ReferenceId && m.MerchantId == x.Id),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.AcquirerId == _Request.ReferenceId && m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                                    //RmDisplayName = "n/a",

                                                    RmId = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                    RmName = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                    RmMobileNumber = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                    RmEmailAddress = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                    RmDisplayName = x.TUCBranchAccountStore.Where(a => a.MerchantId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    RewardPercentage = x.AccountPercentage,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    ActivityStatusCode = x.ActivityStatus.SystemName,
                                                    ActivityStatusName = x.ActivityStatus.Name,

                                                    ProgramId = x.TUCLProgramGroupAccount.Where(a => a.AccountId == x.Id && a.Program.StatusId == HelperStatus.Default.Active).Select(a => a.ProgramId).FirstOrDefault(),
                                                    ProgramReferenceKey = x.TUCLProgramGroupAccount.Where(a => a.AccountId == x.Id && a.Program.StatusId == HelperStatus.Default.Active).Select(a => a.Program.Guid).FirstOrDefault(),
                                                    ProgramStatus = x.TUCLProgramGroupAccount.Where(a => a.AccountId == x.Id).Select(a => a.Status.Name).FirstOrDefault(),
                                                    ProgramStatusCode = x.TUCLProgramGroupAccount.Where(a => a.AccountId == x.Id).Select(a => a.Status.SystemName).FirstOrDefault()
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Merchants = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                && x.OwnerId == _Request.ReferenceId)
                                                            .Select(x => new OAccounts.Merchant.List
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                DisplayName = x.DisplayName,
                                                                EmailAddress = x.EmailAddress,
                                                                ContactNumber = x.ContactNumber,
                                                                Address = x.Address,
                                                                CityName = x.City.Name,
                                                                IconUrl = x.IconStorage.Path,
                                                                Stores = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantStore),
                                                                Terminals = _HCoreContext.TUCTerminal.Count(m => m.AcquirerId == _Request.ReferenceId && m.MerchantId == x.Id),
                                                                ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.AcquirerId == _Request.ReferenceId && m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                                                //RmDisplayName = "n/a",

                                                                RmId = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                                RmName = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                                RmMobileNumber = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                                RmEmailAddress = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                                RmDisplayName = x.TUCBranchAccountStore.Where(a => a.MerchantId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                                CreateDate = x.CreateDate,
                                                                StatusId = x.StatusId,
                                                                StatusCode = x.Status.SystemName,
                                                                StatusName = x.Status.Name,
                                                                RewardPercentage = x.AccountPercentage,
                                                                LastTransactionDate = x.LastTransactionDate,
                                                                ActivityStatusCode = x.ActivityStatus.SystemName,
                                                                ActivityStatusName = x.ActivityStatus.Name,

                                                                ProgramId = x.TUCLProgramGroupAccount.Where(a => a.AccountId == x.Id && a.Program.StatusId == HelperStatus.Default.Active).Select(a => a.ProgramId).FirstOrDefault(),
                                                                ProgramReferenceKey = x.TUCLProgramGroupAccount.Where(a => a.AccountId == x.Id && a.Program.StatusId == HelperStatus.Default.Active).Select(a => a.Program.Guid).FirstOrDefault(),
                                                                ProgramStatus = x.TUCLProgramGroupAccount.Where(a => a.AccountId == x.Id).Select(a => a.Status.Name).FirstOrDefault(),
                                                                ProgramStatusCode = x.TUCLProgramGroupAccount.Where(a => a.AccountId == x.Id).Select(a => a.Status.SystemName).FirstOrDefault()
                                                            })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Merchants)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Merchants, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetMerchant", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get store details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetStore(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    _StoreDetails = new OAccounts.Store.Details();
                    _StoreDetails = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                && x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OAccounts.Store.Details
                                                {
                                                    MerchantId = x.OwnerId,
                                                    MerchantKey = x.Owner.Guid,
                                                    MerchantName = x.Owner.DisplayName,
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    AccountCode = x.AccountCode,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.Owner.IconStorage.Path,
                                                    Address = x.Address,
                                                    Terminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == _Request.ReferenceId),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == _Request.ReferenceId && m.LastTransactionDate > ActivityDate),
                                                    //Terminals = _HCoreContext.TUCTerminal.Count(m =>  m.AcquirerId == _Request.ReferenceId && m.ProviderId == x.Id),
                                                    //ActiveTerminals = _HCoreContext.TUCTerminal.Count(m =>  m.AcquirerId == _Request.ReferenceId && m.ProviderId == x.Id && m.LastTransactionDate > ActivityDate),
                                                    RewardPercentage = x.Owner.AccountPercentage,
                                                    LastActivityDate = x.LastActivityDate,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    //IconUrl = x.Owner.IconStorage.Path
                                                }).FirstOrDefault();
                    if (_StoreDetails != null)
                    {

                        _StoreDetails.AddressComponent = _HCoreContext.HCUAccount.Where(x => x.Id == _StoreDetails.ReferenceId)
                            .Select(x => new OAddress
                            {
                                Address = x.Address,
                                MapAddress = x.Address,
                                Latitude = x.Latitude,
                                Longitude = x.Longitude,
                                CountryId = x.CountryId,
                                CountryCode = x.Country.Guid,
                                CountryName = x.Country.Name,
                                StateId = x.StateId,
                                StateCode = x.State.Guid,
                                StateName = x.State.Name,
                                CityId = x.CityId,
                                CityCode = x.City.Guid,
                                CityName = x.City.Name,
                                CityAreaId = x.CityAreaId,
                                CityAreaCode = x.CityArea.Guid,
                                CityAreaName = x.CityArea.Name,
                            })
                            .FirstOrDefault();


                        _StoreDetails.ContactPerson = _HCoreContext.HCUAccount.Where(x => x.Id == _StoreDetails.ReferenceId)
                            .Select(x => new OAccounts.Store.ContactDetails
                            {
                                Name = x.FirstName + " " + x.LastName,
                                FirstName = x.FirstName,
                                LastName = x.LastName,
                                MobileNumber = x.MobileNumber,
                                EmailAddress = x.SecondaryEmailAddress,
                            }).FirstOrDefault();

                        _StoreDetails.Rm = _HCoreContext.TUCBranchAccount.Where(x => x.StoreId == _StoreDetails.ReferenceId)
                           .Select(x => new OAccounts.Store.ContactDetails
                           {
                               ReferenceId = x.AccountId,
                               Name = x.Account.Name,
                               MobileNumber = x.Account.MobileNumber,
                               EmailAddress = x.Account.EmailAddress,
                           }).FirstOrDefault();

                        _StoreDetails.Branch = _HCoreContext.TUCBranchAccount.Where(x => x.StoreId == _StoreDetails.ReferenceId)
                            .Select(x => new OAccounts.Store.BranchDetails
                            {
                                BranchId = x.BranchId,
                                BranchKey = x.Branch.Guid,
                                BranchCode = x.Branch.BranchCode,
                                Name = x.Branch.Name,
                                DisplayName = x.Branch.DisplayName,
                            }).FirstOrDefault();

                        //_StoreDetails.Categories = _HCoreContext.HCUAccountParameter.Where(x => x.TypeId == HelperType.MerchantCategory
                        //&& x.AccountId == _StoreDetails.MerchantId
                        //&& x.StatusId == HelperStatus.Default.Active)
                        //    .Select(x => new OAccounts.Store.Category
                        //    {
                        //        ReferenceId = x.Id,
                        //        ReferenceKey = x.Guid,
                        //        Name = x.Common.Name,
                        //    }).ToList();

                        _StoreDetails.Categories = _HCoreContext.TUCCategoryAccount.Where(x => x.AccountId == _StoreDetails.MerchantId
                    && x.StatusId == HelperStatus.Default.Active)
                        .Select(x => new OAccounts.Store.Category
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            Name = x.Category.Name,
                        }).ToList();

                        if (!string.IsNullOrEmpty(_StoreDetails.IconUrl))
                        {
                            _StoreDetails.IconUrl = _AppConfig.StorageUrl + _StoreDetails.IconUrl;
                        }
                        else
                        {
                            _StoreDetails.IconUrl = _AppConfig.Default_Icon;
                        }
                        _HCoreContext.Dispose();
                        //_StoreDetails.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("rewardpercentage", _StoreDetails.ReferenceId, _Request.UserReference));
                        if (_StoreDetails.RewardPercentage < 1)
                        {
                            _StoreDetails.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("rewardpercentage", (long)_StoreDetails.MerchantId, _Request.UserReference));
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _StoreDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetStore", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get list of stores
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetStore(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
                DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
                DateTime T7DayStart = TDayStart.AddDays(-7);
                DateTime T7DayEnd = TDayStart.AddSeconds(1);
                DateTime TDeadDayEnd = T7DayStart;
                _Stores = new List<OAccounts.Store.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.SubReferenceId != 0 && !string.IsNullOrEmpty(_Request.SubReferenceKey))
                    {
                        if (_Request.Type == "branch")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                        && x.TUCBranchAccountStore.Any(a => a.BranchId == _Request.SubReferenceId))
                                                        .Select(x => new OAccounts.Store.List
                                                        {
                                                            MerchantId = x.OwnerId,
                                                            MerchantKey = x.Owner.Guid,
                                                            RewardPercentage = x.Owner.AccountPercentage,
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            EmailAddress = x.EmailAddress,
                                                            ContactNumber = x.ContactNumber,
                                                            CountryId = x.CountryId,
                                                            CountryKey = x.Country.Guid,
                                                            CountryName = x.Country.Name,
                                                            StateId = x.StateId,
                                                            StateKey = x.State.Guid,
                                                            StateName = x.State.Name,
                                                            CityId = x.CityId,
                                                            CityKey = x.City.Guid,
                                                            CityName = x.City.Name,
                                                            CityAreaId = x.CityAreaId,
                                                            CityAreaKey = x.CityArea.Guid,
                                                            CityAreaName = x.CityArea.Name,
                                                            Address = x.Address,
                                                            Terminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id),
                                                            ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id && m.LastTransactionDate > ActivityDate),
                                                            RmId = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            RmDisplayName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,
                                                        })
                                                       .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Get Data
                            _Stores = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                        && x.TUCBranchAccountStore.Any(a => a.BranchId == _Request.SubReferenceId))
                                                        .Select(x => new OAccounts.Store.List
                                                        {
                                                            MerchantId = x.OwnerId,
                                                            MerchantKey = x.Owner.Guid,
                                                            RewardPercentage = x.Owner.AccountPercentage,
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            EmailAddress = x.EmailAddress,
                                                            ContactNumber = x.ContactNumber,
                                                            CountryId = x.CountryId,
                                                            CountryKey = x.Country.Guid,
                                                            CountryName = x.Country.Name,
                                                            StateId = x.StateId,
                                                            StateKey = x.State.Guid,
                                                            StateName = x.State.Name,
                                                            CityId = x.CityId,
                                                            CityKey = x.City.Guid,
                                                            CityName = x.City.Name,
                                                            CityAreaId = x.CityAreaId,
                                                            CityAreaKey = x.CityArea.Guid,
                                                            CityAreaName = x.CityArea.Name,
                                                            Address = x.Address,
                                                            Terminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id),
                                                            ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id && m.LastTransactionDate > ActivityDate),
                                                            RmId = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            RmDisplayName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,
                                                        })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Stores)
                            {
                                //DataItem.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("rewardpercentage", DataItem.ReferenceId, _Request.UserReference));
                                if (DataItem.RewardPercentage < 1)
                                {
                                    DataItem.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("rewardpercentage", (long)DataItem.MerchantId, _Request.UserReference));
                                }
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Stores, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                        else if (_Request.Type == "rm")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                        && x.TUCBranchAccountStore.Any(a => a.ManagerId == _Request.SubReferenceId))
                                                        .Select(x => new OAccounts.Store.List
                                                        {
                                                            MerchantId = x.OwnerId,
                                                            MerchantKey = x.Owner.Guid,
                                                            RewardPercentage = x.Owner.AccountPercentage,
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            EmailAddress = x.EmailAddress,
                                                            ContactNumber = x.ContactNumber,
                                                            CountryId = x.CountryId,
                                                            CountryKey = x.Country.Guid,
                                                            CountryName = x.Country.Name,
                                                            StateId = x.StateId,
                                                            StateKey = x.State.Guid,
                                                            StateName = x.State.Name,
                                                            CityId = x.CityId,
                                                            CityKey = x.City.Guid,
                                                            CityName = x.City.Name,
                                                            CityAreaId = x.CityAreaId,
                                                            CityAreaKey = x.CityArea.Guid,
                                                            CityAreaName = x.CityArea.Name,
                                                            Address = x.Address,
                                                            Terminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id),
                                                            ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id && m.LastTransactionDate > ActivityDate),
                                                            RmId = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            RmDisplayName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,
                                                        })
                                                       .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Get Data
                            _Stores = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                        && x.TUCBranchAccountStore.Any(a => a.ManagerId == _Request.SubReferenceId))
                                                        .Select(x => new OAccounts.Store.List
                                                        {
                                                            MerchantId = x.OwnerId,
                                                            MerchantKey = x.Owner.Guid,
                                                            RewardPercentage = x.Owner.AccountPercentage,
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            EmailAddress = x.EmailAddress,
                                                            ContactNumber = x.ContactNumber,
                                                            CountryId = x.CountryId,
                                                            CountryKey = x.Country.Guid,
                                                            CountryName = x.Country.Name,
                                                            StateId = x.StateId,
                                                            StateKey = x.State.Guid,
                                                            StateName = x.State.Name,
                                                            CityId = x.CityId,
                                                            CityKey = x.City.Guid,
                                                            CityName = x.City.Name,
                                                            CityAreaId = x.CityAreaId,
                                                            CityAreaKey = x.CityArea.Guid,
                                                            CityAreaName = x.CityArea.Name,
                                                            Address = x.Address,
                                                            Terminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id),
                                                            ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id && m.LastTransactionDate > ActivityDate),
                                                            RmId = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            RmDisplayName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,
                                                        })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Stores)
                            {
                                //DataItem.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("rewardpercentage", DataItem.ReferenceId, _Request.UserReference));
                                if (DataItem.RewardPercentage < 1)
                                {
                                    DataItem.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("rewardpercentage", (long)DataItem.MerchantId, _Request.UserReference));
                                }
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Stores, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                        else if (_Request.Type == "manager")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                        && x.TUCBranchAccountStore.Any(a => a.ManagerId == _Request.SubReferenceId))
                                                        .Select(x => new OAccounts.Store.List
                                                        {
                                                            MerchantId = x.OwnerId,
                                                            MerchantKey = x.Owner.Guid,
                                                            RewardPercentage = x.Owner.AccountPercentage,
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            EmailAddress = x.EmailAddress,
                                                            ContactNumber = x.ContactNumber,
                                                            CountryId = x.CountryId,
                                                            CountryKey = x.Country.Guid,
                                                            CountryName = x.Country.Name,
                                                            StateId = x.StateId,
                                                            StateKey = x.State.Guid,
                                                            StateName = x.State.Name,
                                                            CityId = x.CityId,
                                                            CityKey = x.City.Guid,
                                                            CityName = x.City.Name,
                                                            CityAreaId = x.CityAreaId,
                                                            CityAreaKey = x.CityArea.Guid,
                                                            CityAreaName = x.CityArea.Name,
                                                            Address = x.Address,
                                                            Terminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id),
                                                            ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id && m.LastTransactionDate > ActivityDate),
                                                            RmId = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            RmDisplayName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,
                                                        })
                                                       .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Get Data
                            _Stores = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                        && x.TUCBranchAccountStore.Any(a => a.ManagerId == _Request.SubReferenceId))
                                                        .Select(x => new OAccounts.Store.List
                                                        {
                                                            MerchantId = x.OwnerId,
                                                            MerchantKey = x.Owner.Guid,
                                                            RewardPercentage = x.Owner.AccountPercentage,
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            EmailAddress = x.EmailAddress,
                                                            ContactNumber = x.ContactNumber,
                                                            CountryId = x.CountryId,
                                                            CountryKey = x.Country.Guid,
                                                            CountryName = x.Country.Name,
                                                            StateId = x.StateId,
                                                            StateKey = x.State.Guid,
                                                            StateName = x.State.Name,
                                                            CityId = x.CityId,
                                                            CityKey = x.City.Guid,
                                                            CityName = x.City.Name,
                                                            CityAreaId = x.CityAreaId,
                                                            CityAreaKey = x.CityArea.Guid,
                                                            CityAreaName = x.CityArea.Name,
                                                            Address = x.Address,
                                                            Terminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id),
                                                            ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id && m.LastTransactionDate > ActivityDate),
                                                            RmId = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            RmDisplayName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,
                                                        })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Stores)
                            {
                                //DataItem.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("rewardpercentage", DataItem.ReferenceId, _Request.UserReference));
                                if (DataItem.RewardPercentage < 1)
                                {
                                    DataItem.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("rewardpercentage", (long)DataItem.MerchantId, _Request.UserReference));
                                }
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Stores, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                        else
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                        && x.OwnerId == _Request.ReferenceId && x.Owner.Guid == _Request.ReferenceKey)
                                                        .Select(x => new OAccounts.Store.List
                                                        {
                                                            MerchantId = x.OwnerId,
                                                            MerchantKey = x.Owner.Guid,
                                                            RewardPercentage = x.Owner.AccountPercentage,
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            EmailAddress = x.EmailAddress,
                                                            ContactNumber = x.ContactNumber,
                                                            CountryId = x.CountryId,
                                                            CountryKey = x.Country.Guid,
                                                            CountryName = x.Country.Name,
                                                            StateId = x.StateId,
                                                            StateKey = x.State.Guid,
                                                            StateName = x.State.Name,
                                                            CityId = x.CityId,
                                                            CityKey = x.City.Guid,
                                                            CityName = x.City.Name,
                                                            CityAreaId = x.CityAreaId,
                                                            CityAreaKey = x.CityArea.Guid,
                                                            CityAreaName = x.CityArea.Name,
                                                            Address = x.Address,
                                                            Terminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id),
                                                            ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id && m.LastTransactionDate > ActivityDate),
                                                            RmId = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            RmDisplayName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,
                                                        })
                                                       .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Get Data
                            _Stores = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                        && x.OwnerId == _Request.ReferenceId && x.Owner.Guid == _Request.ReferenceKey)
                                                        .Select(x => new OAccounts.Store.List
                                                        {
                                                            MerchantId = x.OwnerId,
                                                            MerchantKey = x.Owner.Guid,
                                                            RewardPercentage = x.Owner.AccountPercentage,
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            DisplayName = x.DisplayName,
                                                            EmailAddress = x.EmailAddress,
                                                            ContactNumber = x.ContactNumber,
                                                            CountryId = x.CountryId,
                                                            CountryKey = x.Country.Guid,
                                                            CountryName = x.Country.Name,
                                                            StateId = x.StateId,
                                                            StateKey = x.State.Guid,
                                                            StateName = x.State.Name,
                                                            CityId = x.CityId,
                                                            CityKey = x.City.Guid,
                                                            CityName = x.City.Name,
                                                            CityAreaId = x.CityAreaId,
                                                            CityAreaKey = x.CityArea.Guid,
                                                            CityAreaName = x.CityArea.Name,
                                                            Address = x.Address,
                                                            Terminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id),
                                                            ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id && m.LastTransactionDate > ActivityDate),
                                                            RmId = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            RmDisplayName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),
                                                            CreateDate = x.CreateDate,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,
                                                        })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Stores)
                            {
                                //DataItem.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("rewardpercentage", DataItem.ReferenceId, _Request.UserReference));
                                if (DataItem.RewardPercentage < 1)
                                {
                                    DataItem.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("rewardpercentage", (long)DataItem.MerchantId, _Request.UserReference));
                                }
                                if (!string.IsNullOrEmpty(DataItem.IconUrl))
                                {
                                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                                }
                                else
                                {
                                    DataItem.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Stores, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                    && (x.Owner.OwnerId == _Request.ReferenceId || _HCoreContext.TUCTerminal.Any(m => m.AcquirerId == _Request.ReferenceId && m.StoreId == x.Id)))
                                                    .Select(x => new OAccounts.Store.List
                                                    {
                                                        MerchantId = x.OwnerId,
                                                        MerchantKey = x.Owner.Guid,
                                                        RewardPercentage = x.Owner.AccountPercentage,
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        MerchantDisplayName = x.Owner.DisplayName,
                                                        MerchantEmailAddress = x.Owner.EmailAddress,
                                                        DisplayName = x.DisplayName,
                                                        Address = x.Address,
                                                        CountryId = x.CountryId,
                                                        CountryKey = x.Country.Guid,
                                                        CountryName = x.Country.Name,
                                                        StateId = x.StateId,
                                                        StateKey = x.State.Guid,
                                                        StateName = x.State.Name,
                                                        CityId = x.CityId,
                                                        CityKey = x.City.Guid,
                                                        CityName = x.City.Name,
                                                        CityAreaId = x.CityAreaId,
                                                        CityAreaKey = x.CityArea.Guid,
                                                        CityAreaName = x.CityArea.Name,
                                                        IconUrl = x.Owner.IconStorage.Path,
                                                        Terminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id),
                                                        ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id && m.LastTransactionDate > ActivityDate),
                                                        RmId = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                        RmName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                        RmMobileNumber = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                        RmEmailAddress = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                        RmDisplayName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),
                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        TodaysTransactionAmount = 0,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        _Stores = _HCoreContext.HCUAccount

                                                    .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                    && (x.Owner.OwnerId == _Request.ReferenceId || _HCoreContext.TUCTerminal.Any(m => m.AcquirerId == _Request.ReferenceId && m.StoreId == x.Id)))
                                                                      .Select(x => new OAccounts.Store.List
                                                                      {
                                                                          MerchantId = x.OwnerId,
                                                                          MerchantKey = x.Owner.Guid,
                                                                          RewardPercentage = x.Owner.AccountPercentage,
                                                                          ReferenceId = x.Id,
                                                                          ReferenceKey = x.Guid,
                                                                          MerchantDisplayName = x.Owner.DisplayName,
                                                                          MerchantEmailAddress = x.Owner.EmailAddress,
                                                                          DisplayName = x.DisplayName,
                                                                          Address = x.Address,
                                                                          CountryId = x.CountryId,
                                                                          CountryKey = x.Country.Guid,
                                                                          CountryName = x.Country.Name,
                                                                          StateId = x.StateId,
                                                                          StateKey = x.State.Guid,
                                                                          StateName = x.State.Name,
                                                                          CityId = x.CityId,
                                                                          CityKey = x.City.Guid,
                                                                          CityName = x.City.Name,
                                                                          CityAreaId = x.CityAreaId,
                                                                          CityAreaKey = x.CityArea.Guid,
                                                                          CityAreaName = x.CityArea.Name,
                                                                          IconUrl = x.Owner.IconStorage.Path,
                                                                          Terminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id),
                                                                          ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == x.Id && m.LastTransactionDate > ActivityDate),
                                                                          RmId = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                                          RmName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                                          RmMobileNumber = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                                          RmEmailAddress = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                                          RmDisplayName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),
                                                                          CreateDate = x.CreateDate,
                                                                          StatusId = x.StatusId,
                                                                          StatusCode = x.Status.SystemName,
                                                                          StatusName = x.Status.Name,
                                                                          TodaysTransactionAmount = 0,
                                                                          LastTransactionDate = x.LastTransactionDate,
                                                                          ActivityStatusCode = x.ActivityStatus.SystemName,
                                                                          ActivityStatusName = x.ActivityStatus.Name,
                                                                      })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var DataItem in _Stores)
                        {
                            //DataItem.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("rewardpercentage", DataItem.ReferenceId, _Request.UserReference));
                            if (DataItem.RewardPercentage < 1)
                            {
                                DataItem.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("rewardpercentage", (long)DataItem.MerchantId, _Request.UserReference));
                            }
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            else
                            {
                                DataItem.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Stores, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetStore", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get terminal details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetTerminal(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _TerminalDetails = new OAccounts.Terminal.Details();
                    _TerminalDetails = _HCoreContext.TUCTerminal
                                                .Where(x => x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OAccounts.Terminal.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    TerminalId = x.DisplayName,

                                                    ProviderId = x.ProviderId,
                                                    ProviderKey = x.Provider.Guid,
                                                    ProviderName = x.Provider.DisplayName,
                                                    ProviderIconUrl = x.Provider.IconStorage.Path,

                                                    MerchantId = x.Merchant.Id,
                                                    MerchantKey = x.Merchant.Guid,
                                                    MerchantName = x.Merchant.DisplayName,
                                                    MerchantIconUrl = x.Merchant.IconStorage.Path,

                                                    StoreId = x.Store.Id,
                                                    StoreKey = x.Store.Guid,
                                                    StoreName = x.Store.DisplayName,
                                                    StoreAddress = x.Store.Address,
                                                    StoreLatitude = x.Store.Latitude,
                                                    StoreLongitude = x.Store.Longitude,

                                                    LastActivityDate = x.LastActivityDate,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    TypeId = x.TypeId,
                                                    TypeCode = x.Type.SystemName,
                                                    TypeName = x.Type.Name,
                                                }).FirstOrDefault();
                    if (_TerminalDetails != null)
                    {
                        _TerminalDetails.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                      .Where(a => a.TerminalId == _Request.ReferenceId
                                      && a.StatusId == HelperStatus.Transaction.Success
                                      && a.ModeId == TransactionMode.Credit
                                      && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                      && a.SourceId == TransactionSource.TUC)
                                      || a.SourceId == TransactionSource.ThankUCashPlus))
                                     .Sum(m => m.PurchaseAmount);

                        _TerminalDetails.ContactPerson = _HCoreContext.HCUAccount.Where(x => x.Id == _TerminalDetails.MerchantId)
                            .Select(x => new OAccounts.Terminal.ContactDetails
                            {
                                Name = x.FirstName + " " + x.LastName,
                                MobileNumber = x.MobileNumber,
                                EmailAddress = x.EmailAddress,
                            }).FirstOrDefault();

                        //  _TerminalDetails.Rm = _HCoreContext.TUCBranchAccount.Where(x => x.AccountId == _TerminalDetails.StoreId
                        //&& x.AccountLevelId == 8
                        //&& (x.Branch.OwnerId == _Request.UserReference.AccountId || x.Branch.OwnerId == _Request.UserReference.AccountOwnerId))
                        //   .Select(x => new OAccounts.Terminal.ContactDetails
                        //   {
                        //       Name = x.Owner.Name,
                        //       FirstName = x.Owner.FirstName,
                        //       LastName = x.Owner.LastName,
                        //       MobileNumber = x.Owner.MobileNumber,
                        //       EmailAddress = x.Owner.EmailAddress,
                        //   }).FirstOrDefault();

                        _TerminalDetails.Rm = _HCoreContext.TUCBranchAccount.Where(x => x.StoreId == _TerminalDetails.StoreId)
                           .Select(x => new OAccounts.Terminal.ContactDetails
                           {
                               ReferenceId = x.AccountId,
                               Name = x.Account.Name,
                               MobileNumber = x.Account.MobileNumber,
                               EmailAddress = x.Account.EmailAddress,
                           }).FirstOrDefault();


                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _TerminalDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetTerminal", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get list of terminals
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetTerminal(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
                DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
                DateTime T7DayStart = TDayStart.AddDays(-7);
                DateTime T7DayEnd = TDayStart.AddSeconds(1);
                DateTime TDeadDayEnd = T7DayStart;
                _Terminals = new List<OAccounts.Terminal.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.SubReferenceId != 0 && !string.IsNullOrEmpty(_Request.SubReferenceKey))
                    {
                        if (_Request.Type == "branch")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                        .Where(x => x.AcquirerId == _Request.ReferenceId && x.Store.TUCBranchAccountStore.Any(a => a.BranchId == _Request.SubReferenceId))
                                                        .Select(x => new OAccounts.Terminal.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            TerminalId = x.DisplayName,
                                                            IdentificationNumber = x.IdentificationNumber,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,
                                                            StoreMobileNumber = x.Store.MobileNumber,
                                                            ProviderName = x.Provider.DisplayName,
                                                            ProviderIconUrl = x.Provider.IconStorage.Path,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,
                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            InvoiceAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id
                                                                                                   && a.BankId == _Request.ReferenceId
                                                                                                   && a.Bank.Guid == _Request.ReferenceKey
                                                                                                   && a.StatusId == HelperStatus.Transaction.Success
                                                                                                   && a.ModeId == TransactionMode.Credit
                                                                                                   && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                   && a.SourceId == TransactionSource.TUC)
                                                                                                   || a.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => m.PurchaseAmount),
                                                            RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),

                                                            TypeId = x.TypeId,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                        })
                                                       .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Get Data
                            _Terminals = _HCoreContext.TUCTerminal
                                                        .Where(x => x.AcquirerId == _Request.ReferenceId && x.Store.TUCBranchAccountStore.Any(a => a.BranchId == _Request.SubReferenceId))
                                                        .Select(x => new OAccounts.Terminal.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            TerminalId = x.DisplayName,
                                                            IdentificationNumber = x.IdentificationNumber,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,
                                                            StoreMobileNumber = x.Store.MobileNumber,
                                                            ProviderName = x.Provider.DisplayName,
                                                            ProviderIconUrl = x.Provider.IconStorage.Path,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,
                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            InvoiceAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id
                                                                                                   && a.BankId == _Request.ReferenceId
                                                                                                   && a.Bank.Guid == _Request.ReferenceKey
                                                                                                   && a.StatusId == HelperStatus.Transaction.Success
                                                                                                   && a.ModeId == TransactionMode.Credit
                                                                                                   && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                   && a.SourceId == TransactionSource.TUC)
                                                                                                   || a.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => m.PurchaseAmount),
                                                            RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),

                                                            TypeId = x.TypeId,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                        })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Terminals)
                            {
                                if (DataItem.LastTransactionDate > TDayStart && DataItem.LastTransactionDate < TDayEnd)
                                {
                                    DataItem.ActivityStatusCode = "active";
                                    DataItem.ActivityStatusName = "active";
                                }
                                else if (DataItem.LastTransactionDate > T7DayStart && DataItem.LastTransactionDate < T7DayEnd)
                                {
                                    DataItem.ActivityStatusCode = "idle";
                                    DataItem.ActivityStatusName = "idle";
                                }
                                else if (DataItem.LastTransactionDate < TDeadDayEnd)
                                {
                                    DataItem.ActivityStatusCode = "dead";
                                    DataItem.ActivityStatusName = "dead";
                                }
                                else if (DataItem.LastTransactionDate == null)
                                {
                                    DataItem.ActivityStatusCode = "unused";
                                    DataItem.ActivityStatusName = "unused";
                                }
                                if (!string.IsNullOrEmpty(DataItem.ProviderIconUrl))
                                {
                                    DataItem.ProviderIconUrl = _AppConfig.StorageUrl + DataItem.ProviderIconUrl;
                                }
                                else
                                {
                                    DataItem.ProviderIconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Terminals, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                        if (_Request.Type == "manager")
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                        .Where(x => x.Store.TUCBranchAccountStore.Any(a => a.ManagerId == _Request.SubReferenceId && a.Manager.OwnerId == _Request.ReferenceId))
                                                        .Select(x => new OAccounts.Terminal.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            TerminalId = x.DisplayName,
                                                            IdentificationNumber = x.IdentificationNumber,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,
                                                            ProviderName = x.Store.DisplayName,
                                                            ProviderIconUrl = x.Store.IconStorage.Path,
                                                            StoreMobileNumber = x.Store.MobileNumber,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            InvoiceAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id
                                                                                                   && a.BankId == _Request.ReferenceId
                                                                                                   && a.Bank.Guid == _Request.ReferenceKey
                                                                                                   && a.StatusId == HelperStatus.Transaction.Success
                                                                                                   && a.ModeId == TransactionMode.Credit
                                                                                                   && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                   && a.SourceId == TransactionSource.TUC)
                                                                                                   || a.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => m.PurchaseAmount),
                                                            RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),

                                                            TypeId = x.TypeId,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                            ProviderId = x.ProviderId
                                                        })
                                                       .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Get Data
                            _Terminals = _HCoreContext.TUCTerminal
                                                        .Where(x => x.Store.TUCBranchAccountStore.Any(a => a.ManagerId == _Request.SubReferenceId && a.Manager.OwnerId == _Request.ReferenceId))
                                                        .Select(x => new OAccounts.Terminal.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            TerminalId = x.DisplayName,
                                                            IdentificationNumber = x.IdentificationNumber,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,
                                                            ProviderName = x.Store.DisplayName,
                                                            ProviderIconUrl = x.Store.IconStorage.Path,
                                                            StoreMobileNumber = x.Store.MobileNumber,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            InvoiceAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id
                                                                                                   && a.BankId == _Request.ReferenceId
                                                                                                   && a.Bank.Guid == _Request.ReferenceKey
                                                                                                   && a.StatusId == HelperStatus.Transaction.Success
                                                                                                   && a.ModeId == TransactionMode.Credit
                                                                                                   && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                   && a.SourceId == TransactionSource.TUC)
                                                                                                   || a.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => m.PurchaseAmount),
                                                            RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),

                                                            TypeId = x.TypeId,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                            ProviderId = x.ProviderId
                                                        })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Terminals)
                            {
                                if (DataItem.LastTransactionDate > TDayStart && DataItem.LastTransactionDate < TDayEnd)
                                {
                                    DataItem.ActivityStatusCode = "active";
                                    DataItem.ActivityStatusName = "active";
                                }
                                else if (DataItem.LastTransactionDate > T7DayStart && DataItem.LastTransactionDate < T7DayEnd)
                                {
                                    DataItem.ActivityStatusCode = "idle";
                                    DataItem.ActivityStatusName = "idle";
                                }
                                else if (DataItem.LastTransactionDate < TDeadDayEnd)
                                {
                                    DataItem.ActivityStatusCode = "dead";
                                    DataItem.ActivityStatusName = "dead";
                                }
                                else if (DataItem.LastTransactionDate == null)
                                {
                                    DataItem.ActivityStatusCode = "unused";
                                    DataItem.ActivityStatusName = "unused";
                                }
                                if (!string.IsNullOrEmpty(DataItem.ProviderIconUrl))
                                {
                                    DataItem.ProviderIconUrl = _AppConfig.StorageUrl + DataItem.ProviderIconUrl;
                                }
                                else
                                {
                                    DataItem.ProviderIconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Terminals, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                        else
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                       .Where(x => x.AcquirerId == _Request.ReferenceId && (x.StoreId == _Request.SubReferenceId || x.MerchantId == _Request.SubReferenceId))
                                                        .Select(x => new OAccounts.Terminal.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            TerminalId = x.DisplayName,
                                                            IdentificationNumber = x.IdentificationNumber,
                                                            StoreId = x.Store.Id,
                                                            StoreKey = x.Store.Guid,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,
                                                            StoreMobileNumber = x.Store.MobileNumber,
                                                            ProviderId = x.Provider.Id,
                                                            ProviderKey = x.Provider.Guid,
                                                            ProviderName = x.Provider.DisplayName,
                                                            ProviderIconUrl = x.Provider.IconStorage.Path,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            InvoiceAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id
                                                                                                   && a.BankId == _Request.ReferenceId
                                                                                                   && a.Bank.Guid == _Request.ReferenceKey
                                                                                                   && a.StatusId == HelperStatus.Transaction.Success
                                                                                                   && a.ModeId == TransactionMode.Credit
                                                                                                   && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                   && a.SourceId == TransactionSource.TUC)
                                                                                                   || a.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => m.PurchaseAmount),
                                                            RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            TypeId = x.TypeId,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                        })
                                                       .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Get Data
                            _Terminals = _HCoreContext.TUCTerminal
                                                       .Where(x => x.AcquirerId == _Request.ReferenceId && (x.StoreId == _Request.SubReferenceId || x.MerchantId == _Request.SubReferenceId))
                                                        .Select(x => new OAccounts.Terminal.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            TerminalId = x.DisplayName,
                                                            IdentificationNumber = x.IdentificationNumber,
                                                            StoreId = x.Store.Id,
                                                            StoreKey = x.Store.Guid,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,
                                                            StoreMobileNumber = x.Store.MobileNumber,
                                                            ProviderId = x.Provider.Id,
                                                            ProviderKey = x.Provider.Guid,
                                                            ProviderName = x.Provider.DisplayName,
                                                            ProviderIconUrl = x.Provider.IconStorage.Path,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            InvoiceAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id
                                                                                                   && a.BankId == _Request.ReferenceId
                                                                                                   && a.Bank.Guid == _Request.ReferenceKey
                                                                                                   && a.StatusId == HelperStatus.Transaction.Success
                                                                                                   && a.ModeId == TransactionMode.Credit
                                                                                                   && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                   && a.SourceId == TransactionSource.TUC)
                                                                                                   || a.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => m.PurchaseAmount),
                                                            RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            TypeId = x.TypeId,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                        })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                            #endregion
                            #region Create  Response Object
                            _HCoreContext.Dispose();
                            foreach (var DataItem in _Terminals)
                            {
                                if (DataItem.LastTransactionDate > TDayStart && DataItem.LastTransactionDate < TDayEnd)
                                {
                                    DataItem.ActivityStatusCode = "active";
                                    DataItem.ActivityStatusName = "active";
                                }
                                else if (DataItem.LastTransactionDate > T7DayStart && DataItem.LastTransactionDate < T7DayEnd)
                                {
                                    DataItem.ActivityStatusCode = "idle";
                                    DataItem.ActivityStatusName = "idle";
                                }
                                else if (DataItem.LastTransactionDate < TDeadDayEnd)
                                {
                                    DataItem.ActivityStatusCode = "dead";
                                    DataItem.ActivityStatusName = "dead";
                                }
                                else if (DataItem.LastTransactionDate == null)
                                {
                                    DataItem.ActivityStatusCode = "unused";
                                    DataItem.ActivityStatusName = "unused";
                                }
                                if (!string.IsNullOrEmpty(DataItem.ProviderIconUrl))
                                {
                                    DataItem.ProviderIconUrl = _AppConfig.StorageUrl + DataItem.ProviderIconUrl;
                                }
                                else
                                {
                                    DataItem.ProviderIconUrl = _AppConfig.Default_Icon;
                                }
                            }
                            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Terminals, _Request.Offset, _Request.Limit);
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                            #endregion
                        }
                    }
                    else
                    {
                        if (_Request.Type == "active")
                        {
                            if (_Request.RefreshCount)
                            {
                                _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                    .Where(x => x.AcquirerId == _Request.ReferenceId)
                                                        .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd))
                                                        .Select(x => new OAccounts.Terminal.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            TerminalId = x.DisplayName,
                                                            IdentificationNumber = x.IdentificationNumber,
                                                            MerchantId = x.Merchant.Id,
                                                            MerchantKey = x.Merchant.Guid,
                                                            MerchantDisplayName = x.Merchant.DisplayName,
                                                            MerchantIconUrl = x.Merchant.IconStorage.Path,
                                                            RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            StoreId = x.Store.Id,
                                                            StoreKey = x.Store.Guid,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,
                                                            StoreMobileNumber = x.Store.MobileNumber,
                                                            ProviderId = x.Provider.Id,
                                                            ProviderKey = x.Provider.Guid,
                                                            ProviderName = x.Provider.DisplayName,
                                                            ProviderIconUrl = x.Provider.IconStorage.Path,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            InvoiceAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id
                                                                                                   && a.BankId == _Request.ReferenceId
                                                                                                   && a.Bank.Guid == _Request.ReferenceKey
                                                                                                   && a.StatusId == HelperStatus.Transaction.Success
                                                                                                   && a.ModeId == TransactionMode.Credit
                                                                                                   && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                   && a.SourceId == TransactionSource.TUC)
                                                                                                   || a.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => m.PurchaseAmount),

                                                            TypeId = x.TypeId,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                        })
                                                       .Where(_Request.SearchCondition)
                                               .Count();
                            }
                            #region Get Data
                            _Terminals = _HCoreContext.TUCTerminal
                                                    .Where(x => x.AcquirerId == _Request.ReferenceId)
                                                        .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd))
                                                        .Select(x => new OAccounts.Terminal.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            TerminalId = x.DisplayName,
                                                            IdentificationNumber = x.IdentificationNumber,
                                                            MerchantId = x.Merchant.Id,
                                                            MerchantKey = x.Merchant.Guid,
                                                            MerchantDisplayName = x.Merchant.DisplayName,
                                                            MerchantIconUrl = x.Merchant.IconStorage.Path,
                                                            RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            StoreId = x.Store.Id,
                                                            StoreKey = x.Store.Guid,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,
                                                            StoreMobileNumber = x.Store.MobileNumber,
                                                            ProviderId = x.Provider.Id,
                                                            ProviderKey = x.Provider.Guid,
                                                            ProviderName = x.Provider.DisplayName,
                                                            ProviderIconUrl = x.Provider.IconStorage.Path,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            InvoiceAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id
                                                                                                   && a.BankId == _Request.ReferenceId
                                                                                                   && a.Bank.Guid == _Request.ReferenceKey
                                                                                                   && a.StatusId == HelperStatus.Transaction.Success
                                                                                                   && a.ModeId == TransactionMode.Credit
                                                                                                   && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                   && a.SourceId == TransactionSource.TUC)
                                                                                                   || a.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => m.PurchaseAmount),

                                                            TypeId = x.TypeId,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                        })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                            #endregion
                        }
                        else if (_Request.Type == "idle")
                        {
                            if (_Request.RefreshCount)
                            {
                                _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                    .Where(x => x.AcquirerId == _Request.ReferenceId)
                                                        .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd))
                                                        .Select(x => new OAccounts.Terminal.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            TerminalId = x.DisplayName,
                                                            IdentificationNumber = x.IdentificationNumber,
                                                            MerchantId = x.Merchant.Id,
                                                            MerchantKey = x.Merchant.Guid,
                                                            MerchantDisplayName = x.Merchant.DisplayName,
                                                            MerchantIconUrl = x.Merchant.IconStorage.Path,
                                                            RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            StoreId = x.Store.Id,
                                                            StoreKey = x.Store.Guid,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,
                                                            StoreMobileNumber = x.Store.MobileNumber,
                                                            ProviderId = x.Provider.Id,
                                                            ProviderKey = x.Provider.Guid,
                                                            ProviderName = x.Provider.DisplayName,
                                                            ProviderIconUrl = x.Provider.IconStorage.Path,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            InvoiceAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id
                                                                                                   && a.BankId == _Request.ReferenceId
                                                                                                   && a.Bank.Guid == _Request.ReferenceKey
                                                                                                   && a.StatusId == HelperStatus.Transaction.Success
                                                                                                   && a.ModeId == TransactionMode.Credit
                                                                                                   && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                   && a.SourceId == TransactionSource.TUC)
                                                                                                   || a.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => m.PurchaseAmount),

                                                            TypeId = x.TypeId,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                        })
                                                       .Where(_Request.SearchCondition)
                                               .Count();
                            }
                            #region Get Data
                            _Terminals = _HCoreContext.TUCTerminal
                                                    .Where(x => x.AcquirerId == _Request.ReferenceId)
                                                        .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd))
                                                        .Select(x => new OAccounts.Terminal.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            TerminalId = x.DisplayName,
                                                            IdentificationNumber = x.IdentificationNumber,
                                                            MerchantId = x.Merchant.Id,
                                                            MerchantKey = x.Merchant.Guid,
                                                            MerchantDisplayName = x.Merchant.DisplayName,
                                                            MerchantIconUrl = x.Merchant.IconStorage.Path,
                                                            RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            StoreId = x.Store.Id,
                                                            StoreKey = x.Store.Guid,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,
                                                            StoreMobileNumber = x.Store.MobileNumber,
                                                            ProviderId = x.Provider.Id,
                                                            ProviderKey = x.Provider.Guid,
                                                            ProviderName = x.Provider.DisplayName,
                                                            ProviderIconUrl = x.Provider.IconStorage.Path,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            InvoiceAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id
                                                                                                   && a.BankId == _Request.ReferenceId
                                                                                                   && a.Bank.Guid == _Request.ReferenceKey
                                                                                                   && a.StatusId == HelperStatus.Transaction.Success
                                                                                                   && a.ModeId == TransactionMode.Credit
                                                                                                   && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                   && a.SourceId == TransactionSource.TUC)
                                                                                                   || a.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => m.PurchaseAmount),

                                                            TypeId = x.TypeId,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                        })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                            #endregion
                        }
                        else if (_Request.Type == "dead")
                        {
                            if (_Request.RefreshCount)
                            {
                                _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                    .Where(x => x.AcquirerId == _Request.ReferenceId)
                                                        .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate < TDeadDayEnd))
                                                        .Select(x => new OAccounts.Terminal.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            TerminalId = x.DisplayName,
                                                            IdentificationNumber = x.IdentificationNumber,
                                                            MerchantId = x.Merchant.Id,
                                                            MerchantKey = x.Merchant.Guid,
                                                            MerchantDisplayName = x.Merchant.DisplayName,
                                                            MerchantIconUrl = x.Merchant.IconStorage.Path,
                                                            RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            StoreId = x.Store.Id,
                                                            StoreKey = x.Store.Guid,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,
                                                            StoreMobileNumber = x.Store.MobileNumber,
                                                            ProviderId = x.Provider.Id,
                                                            ProviderKey = x.Provider.Guid,
                                                            ProviderName = x.Provider.DisplayName,
                                                            ProviderIconUrl = x.Provider.IconStorage.Path,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            InvoiceAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id
                                                                                                   && a.BankId == _Request.ReferenceId
                                                                                                   && a.Bank.Guid == _Request.ReferenceKey
                                                                                                   && a.StatusId == HelperStatus.Transaction.Success
                                                                                                   && a.ModeId == TransactionMode.Credit
                                                                                                   && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                   && a.SourceId == TransactionSource.TUC)
                                                                                                   || a.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => m.PurchaseAmount),

                                                            TypeId = x.TypeId,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                        })
                                                       .Where(_Request.SearchCondition)
                                               .Count();
                            }
                            #region Get Data
                            _Terminals = _HCoreContext.TUCTerminal
                                                    .Where(x => x.AcquirerId == _Request.ReferenceId)
                                                        .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate < TDeadDayEnd))
                                                        .Select(x => new OAccounts.Terminal.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            TerminalId = x.DisplayName,
                                                            IdentificationNumber = x.IdentificationNumber,
                                                            MerchantId = x.Merchant.Id,
                                                            MerchantKey = x.Merchant.Guid,
                                                            MerchantDisplayName = x.Merchant.DisplayName,
                                                            MerchantIconUrl = x.Merchant.IconStorage.Path,
                                                            RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            StoreId = x.Store.Id,
                                                            StoreKey = x.Store.Guid,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,
                                                            StoreMobileNumber = x.Store.MobileNumber,
                                                            ProviderId = x.Provider.Id,
                                                            ProviderKey = x.Provider.Guid,
                                                            ProviderName = x.Provider.DisplayName,
                                                            ProviderIconUrl = x.Provider.IconStorage.Path,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            InvoiceAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id
                                                                                                   && a.BankId == _Request.ReferenceId
                                                                                                   && a.Bank.Guid == _Request.ReferenceKey
                                                                                                   && a.StatusId == HelperStatus.Transaction.Success
                                                                                                   && a.ModeId == TransactionMode.Credit
                                                                                                   && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                   && a.SourceId == TransactionSource.TUC)
                                                                                                   || a.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => m.PurchaseAmount),

                                                            TypeId = x.TypeId,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                        })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                            #endregion
                        }
                        else if (_Request.Type == "unused")
                        {
                            if (_Request.RefreshCount)
                            {
                                _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                    .Where(x => x.AcquirerId == _Request.ReferenceId)
                                                        .Where(x => x.HCUAccountTransaction.Any() == false)
                                                        .Select(x => new OAccounts.Terminal.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            TerminalId = x.DisplayName,
                                                            IdentificationNumber = x.IdentificationNumber,
                                                            MerchantId = x.Merchant.Id,
                                                            MerchantKey = x.Merchant.Guid,
                                                            MerchantDisplayName = x.Merchant.DisplayName,
                                                            MerchantIconUrl = x.Merchant.IconStorage.Path,
                                                            RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            StoreId = x.Store.Id,
                                                            StoreKey = x.Store.Guid,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,
                                                            StoreMobileNumber = x.Store.MobileNumber,
                                                            ProviderId = x.Provider.Id,
                                                            ProviderKey = x.Provider.Guid,
                                                            ProviderName = x.Provider.DisplayName,
                                                            ProviderIconUrl = x.Provider.IconStorage.Path,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            InvoiceAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id
                                                                                                   && a.BankId == _Request.ReferenceId
                                                                                                   && a.Bank.Guid == _Request.ReferenceKey
                                                                                                   && a.StatusId == HelperStatus.Transaction.Success
                                                                                                   && a.ModeId == TransactionMode.Credit
                                                                                                   && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                   && a.SourceId == TransactionSource.TUC)
                                                                                                   || a.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => m.PurchaseAmount),

                                                            TypeId = x.TypeId,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                        })
                                                       .Where(_Request.SearchCondition)
                                               .Count();
                            }
                            #region Get Data
                            _Terminals = _HCoreContext.TUCTerminal
                                                    .Where(x => x.AcquirerId == _Request.ReferenceId)
                                                        .Where(x => x.HCUAccountTransaction.Any() == false)
                                                        .Select(x => new OAccounts.Terminal.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            TerminalId = x.DisplayName,
                                                            IdentificationNumber = x.IdentificationNumber,
                                                            MerchantId = x.Merchant.Id,
                                                            MerchantKey = x.Merchant.Guid,
                                                            MerchantDisplayName = x.Merchant.DisplayName,
                                                            MerchantIconUrl = x.Merchant.IconStorage.Path,
                                                            RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            StoreId = x.Store.Id,
                                                            StoreKey = x.Store.Guid,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,
                                                            StoreMobileNumber = x.Store.MobileNumber,
                                                            ProviderId = x.Provider.Id,
                                                            ProviderKey = x.Provider.Guid,
                                                            ProviderName = x.Provider.DisplayName,
                                                            ProviderIconUrl = x.Provider.IconStorage.Path,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            InvoiceAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id
                                                                                                   && a.BankId == _Request.ReferenceId
                                                                                                   && a.Bank.Guid == _Request.ReferenceKey
                                                                                                   && a.StatusId == HelperStatus.Transaction.Success
                                                                                                   && a.ModeId == TransactionMode.Credit
                                                                                                   && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                   && a.SourceId == TransactionSource.TUC)
                                                                                                   || a.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => m.PurchaseAmount),

                                                            TypeId = x.TypeId,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                        })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                            #endregion
                        }
                        else
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                    .Where(x => x.AcquirerId == _Request.ReferenceId)
                                                        .Select(x => new OAccounts.Terminal.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            TerminalId = x.DisplayName,
                                                            IdentificationNumber = x.IdentificationNumber,
                                                            MerchantId = x.Merchant.Id,
                                                            MerchantKey = x.Merchant.Guid,
                                                            MerchantDisplayName = x.Merchant.DisplayName,
                                                            MerchantIconUrl = x.Merchant.IconStorage.Path,
                                                            RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            StoreId = x.Store.Id,
                                                            StoreKey = x.Store.Guid,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,
                                                            StoreMobileNumber = x.Store.MobileNumber,
                                                            ProviderId = x.Provider.Id,
                                                            ProviderKey = x.Provider.Guid,
                                                            ProviderName = x.Provider.DisplayName,
                                                            ProviderIconUrl = x.Provider.IconStorage.Path,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            InvoiceAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id
                                                                                                   && a.BankId == _Request.ReferenceId
                                                                                                   && a.Bank.Guid == _Request.ReferenceKey
                                                                                                   && a.StatusId == HelperStatus.Transaction.Success
                                                                                                   && a.ModeId == TransactionMode.Credit
                                                                                                   && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                   && a.SourceId == TransactionSource.TUC)
                                                                                                   || a.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => m.PurchaseAmount),

                                                            TypeId = x.TypeId,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                        })
                                                       .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Get Data
                            _Terminals = _HCoreContext.TUCTerminal
                                                    .Where(x => x.AcquirerId == _Request.ReferenceId)
                                                        .Select(x => new OAccounts.Terminal.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            TerminalId = x.DisplayName,
                                                            IdentificationNumber = x.IdentificationNumber,
                                                            MerchantId = x.Merchant.Id,
                                                            MerchantKey = x.Merchant.Guid,
                                                            MerchantDisplayName = x.Merchant.DisplayName,
                                                            MerchantIconUrl = x.Merchant.IconStorage.Path,
                                                            RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                            RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                            RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                            RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                            StoreId = x.Store.Id,
                                                            StoreKey = x.Store.Guid,
                                                            StoreName = x.Store.DisplayName,
                                                            StoreAddress = x.Store.Address,
                                                            StoreLatitude = x.Store.Latitude,
                                                            StoreLongitude = x.Store.Longitude,
                                                            StoreMobileNumber = x.Store.MobileNumber,
                                                            ProviderId = x.Provider.Id,
                                                            ProviderKey = x.Provider.Guid,
                                                            ProviderName = x.Provider.DisplayName,
                                                            ProviderIconUrl = x.Provider.IconStorage.Path,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            ActivityStatusCode = x.ActivityStatus.SystemName,
                                                            ActivityStatusName = x.ActivityStatus.Name,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,
                                                            TodaysTransactionAmount = 0,
                                                            InvoiceAmount = x.HCUAccountTransaction.Where(a => a.TerminalId == x.Id
                                                                                                   && a.BankId == _Request.ReferenceId
                                                                                                   && a.Bank.Guid == _Request.ReferenceKey
                                                                                                   && a.StatusId == HelperStatus.Transaction.Success
                                                                                                   && a.ModeId == TransactionMode.Credit
                                                                                                   && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                                   && a.SourceId == TransactionSource.TUC)
                                                                                                   || a.SourceId == TransactionSource.ThankUCashPlus)).Sum(m => m.PurchaseAmount),

                                                            TypeId = x.TypeId,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                        })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                            #endregion
                        }

                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var DataItem in _Terminals)
                        {
                            if (DataItem.LastTransactionDate > TDayStart && DataItem.LastTransactionDate < TDayEnd)
                            {
                                DataItem.ActivityStatusCode = "active";
                                DataItem.ActivityStatusName = "active";
                            }
                            else if (DataItem.LastTransactionDate > T7DayStart && DataItem.LastTransactionDate < T7DayEnd)
                            {
                                DataItem.ActivityStatusCode = "idle";
                                DataItem.ActivityStatusName = "idle";
                            }
                            else if (DataItem.LastTransactionDate < TDeadDayEnd)
                            {
                                DataItem.ActivityStatusCode = "dead";
                                DataItem.ActivityStatusName = "dead";
                            }
                            else if (DataItem.LastTransactionDate == null)
                            {
                                DataItem.ActivityStatusCode = "unused";
                                DataItem.ActivityStatusName = "unused";
                            }
                            if (!string.IsNullOrEmpty(DataItem.MerchantIconUrl))
                            {
                                DataItem.MerchantIconUrl = _AppConfig.StorageUrl + DataItem.MerchantIconUrl;
                            }
                            else
                            {
                                DataItem.MerchantIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(DataItem.ProviderIconUrl))
                            {
                                DataItem.ProviderIconUrl = _AppConfig.StorageUrl + DataItem.ProviderIconUrl;
                            }
                            else
                            {
                                DataItem.ProviderIconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Terminals, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetTerminal", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Method defined to get list of acquirer program merchants
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetAcquirerProgramMerchant(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
                DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
                DateTime T7DayStart = TDayStart.AddDays(-7);
                DateTime T7DayEnd = TDayStart.AddSeconds(1);
                DateTime TDeadDayEnd = T7DayStart;
                _Merchants = new List<OAccounts.Merchant.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                 && (x.StatusId == HelperStatus.Default.Active &&
                                                    x.TUCLProgramGroupAccount.Any(y => y.AccountId == x.Id && y.ProgramId == _Request.ProgramId && y.Program.StatusId == HelperStatus.Default.Active)))
                                                .Select(x => new OAccounts.Merchant.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    Address = x.Address,
                                                    CityName = x.City.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    Stores = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantStore),
                                                    Terminals = _HCoreContext.TUCTerminal.Count(m => m.AcquirerId == _Request.ReferenceId && m.MerchantId == x.Id),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.AcquirerId == _Request.ReferenceId && m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                                    //RmDisplayName = "n/a",

                                                    RmId = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                    RmName = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                    RmMobileNumber = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                    RmEmailAddress = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                    RmDisplayName = x.TUCBranchAccountStore.Where(a => a.MerchantId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    RewardPercentage = x.AccountPercentage,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    ActivityStatusCode = x.ActivityStatus.SystemName,
                                                    ActivityStatusName = x.ActivityStatus.Name,

                                                    ProgramId = x.TUCLProgramGroupAccount.Where(a => a.AccountId == x.Id && a.Program.StatusId == HelperStatus.Default.Active).Select(a => a.ProgramId).FirstOrDefault(),
                                                    ProgramReferenceKey = x.TUCLProgramGroupAccount.Where(a => a.AccountId == x.Id && a.Program.StatusId == HelperStatus.Default.Active).Select(a => a.Program.Guid).FirstOrDefault(),
                                                    ProgramStatus = x.TUCLProgramGroupAccount.Where(a => a.AccountId == x.Id).Select(a => a.Status.Name).FirstOrDefault(),
                                                    ProgramStatusCode = x.TUCLProgramGroupAccount.Where(a => a.AccountId == x.Id).Select(a => a.Status.SystemName).FirstOrDefault()
                                                })
                                              .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Merchants = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                && (x.StatusId == HelperStatus.Default.Active && x.TUCLProgramGroupAccount.Any(y => y.AccountId == x.Id && y.ProgramId == _Request.ProgramId && y.Program.StatusId == HelperStatus.Default.Active)))
                                                            .Select(x => new OAccounts.Merchant.List
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                DisplayName = x.DisplayName,
                                                                EmailAddress = x.EmailAddress,
                                                                ContactNumber = x.ContactNumber,
                                                                Address = x.Address,
                                                                CityName = x.City.Name,
                                                                IconUrl = x.IconStorage.Path,
                                                                Stores = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantStore),
                                                                Terminals = _HCoreContext.TUCTerminal.Count(m => m.AcquirerId == _Request.ReferenceId && m.MerchantId == x.Id),
                                                                ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.AcquirerId == _Request.ReferenceId && m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                                                //RmDisplayName = "n/a",

                                                                RmId = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                                RmName = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                                RmMobileNumber = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                                RmEmailAddress = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                                RmDisplayName = x.TUCBranchAccountStore.Where(a => a.MerchantId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                                CreateDate = x.CreateDate,
                                                                StatusId = x.StatusId,
                                                                StatusCode = x.Status.SystemName,
                                                                StatusName = x.Status.Name,
                                                                RewardPercentage = x.AccountPercentage,
                                                                LastTransactionDate = x.LastTransactionDate,
                                                                ActivityStatusCode = x.ActivityStatus.SystemName,
                                                                ActivityStatusName = x.ActivityStatus.Name,

                                                                ProgramId = x.TUCLProgramGroupAccount.Where(a => a.AccountId == x.Id && a.Program.StatusId == HelperStatus.Default.Active).Select(a => a.ProgramId).FirstOrDefault(),
                                                                ProgramReferenceKey = x.TUCLProgramGroupAccount.Where(a => a.AccountId == x.Id && a.Program.StatusId == HelperStatus.Default.Active).Select(a => a.Program.Guid).FirstOrDefault(),
                                                                ProgramStatus = x.TUCLProgramGroupAccount.Where(a => a.AccountId == x.Id).Select(a => a.Status.Name).FirstOrDefault(),
                                                                ProgramStatusCode = x.TUCLProgramGroupAccount.Where(a => a.AccountId == x.Id).Select(a => a.Status.SystemName).FirstOrDefault()
                                                            })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Merchants)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Merchants, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetAcquirerProgramMerchant", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }
}
