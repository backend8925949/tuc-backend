//==================================================================================
// FileName: FrameworkMerchant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to merchant
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.OpenApi;
using HCore.TUC.Core.Resource;

namespace HCore.TUC.Core.Framework.OpenApi
{  
    internal class FrameworkMerchant
    {
        HCoreContext _HCoreContext;
        List<OMerchant.List> _Merchants;
        /// <summary>
        /// Description: Gets the list of merchant locations.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchant(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                //if (_Request.UserReference.RequestIpAddress == "160.153.131.217")
                //{
                    _Merchants = new List<OMerchant.List>();
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    }
                    if (string.IsNullOrEmpty(_Request.SortExpression))
                    {
                        HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    }
                    if (_Request.Limit < 1)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && x.IconStorage != null
                                                    && x.StatusId == HelperStatus.Default.Active
                                                    //&& x.HCUAccountOwnerOwner.Any(a=>a.AccountTypeId == UserAccountType.MerchantStore)
                                                    && x.AccountPercentage > 0)
                                                    .OrderBy(x => x.DisplayName)
                                                    .Select(x => new OMerchant.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        DisplayName = x.DisplayName,
                                                        Address = x.Address,
                                                        IconUrl = x.IconStorage.Path,
                                                    }).Count();
                            #endregion
                        }
                        #region Get Data
                        _Merchants = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && x.IconStorage != null
                                                    && x.StatusId == HelperStatus.Default.Active
                                                    //&& x.HCUAccountOwnerOwner.Any(a=>a.AccountTypeId == UserAccountType.MerchantStore)
                                                    && x.AccountPercentage > 0)
                                                    .OrderBy(x => x.DisplayName)
                                                    .Select(x => new OMerchant.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        DisplayName = x.DisplayName,
                                                        Address = x.Address,
                                                        IconUrl = x.IconStorage.Path,
                                                    })
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        foreach (var DataItem in _Merchants)
                        {
                            DataItem.Stores = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                    && x.OwnerId == DataItem.ReferenceId
                                    && x.StatusId == HelperStatus.Default.Active)
                                .OrderBy(x => x.DisplayName)
                                .Select(x => new OMerchant.Store
                                {
                                    DisplayName = x.DisplayName,
                                    Address = x.Address,
                                    Latitude = x.Latitude,
                                    Longitude = x.Longitude,
                                }).ToList();
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            else
                            {
                                DataItem.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Merchants, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                //}
                //else
                //{
                //    OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                //}
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetMerchant", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }
}
