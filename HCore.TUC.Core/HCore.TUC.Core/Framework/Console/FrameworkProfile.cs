//==================================================================================
// FileName: FrameworkProfile.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to account
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.CoreAccount;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;


namespace HCore.TUC.Core.Framework.Console
{
    internal class FrameworkProfile
    {
        HCoreContext _HCoreContext;
        CoreAdmin _CoreAdmin;
        OCoreAdmin.Request _OCoreAdminRequest;
        /// <summary>
        /// Description: Updates the profile.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateProfile(OProfile.Request _Request)
        {
            _OCoreAdminRequest = new OCoreAdmin.Request();
            _OCoreAdminRequest.FirstName = _Request.FirstName;
            _OCoreAdminRequest.LastName = _Request.LastName;
            _OCoreAdminRequest.MobileNumber = _Request.MobileNumber;
            _OCoreAdminRequest.EmailAddress = _Request.EmailAddress;
            _OCoreAdminRequest.StatusCode = _Request.StatusCode;
            _OCoreAdminRequest.GenderCode = _Request.GenderCode;
            _OCoreAdminRequest.RoleKey = _Request.RoleKey;
            _OCoreAdminRequest.DateOfBirth = _Request.DateOfBirth;
            _OCoreAdminRequest.IconContent = _Request.IconContent;
            _OCoreAdminRequest.Address = _Request.Address;
            _OCoreAdminRequest.UserReference = _Request.UserReference;
            _CoreAdmin = new CoreAdmin();
            OCoreAdmin.Response _Response = _CoreAdmin.UpdateAdmin(_OCoreAdminRequest);
            if (_Response.Status == HCoreConstant.ResponseStatus.Success)
            {
                var Details = new
                {
                    ReferenceId = _Response.ReferenceId,
                    ReferenceKey = _Response.ReferenceKey
                };
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, Details, _Response.StatusCode);
            }
            else
            {
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, null, _Response.StatusCode);
            }

        }
        /// <summary>
        /// Description: Updates the password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdatePassword(OProfile.Password.Request _Request)
        {
            if (string.IsNullOrEmpty(_Request.OldPassword))
            {
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1192, TUCCoreResource.CA1192M);
            }
            else if (string.IsNullOrEmpty(_Request.NewPassword))
            {
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1193, TUCCoreResource.CA1193M);
            }
            else
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Admin && x.Id == _Request.UserReference.AccountId && x.Guid == _Request.UserReference.AccountKey).FirstOrDefault();
                    if (_Details != null)
                    {
                        if (_Details.StatusId == HelperStatus.Default.Active)
                        {
                            var _UserAuth = _HCoreContext.HCUAccountAuth.Where(x => x.Id == _Details.UserId).FirstOrDefault();
                            if (_UserAuth != null)
                            {
                                string OPassword = HCoreEncrypt.DecryptHash(_UserAuth.Password);
                                if (_Request.OldPassword == OPassword)
                                {
                                    _UserAuth.Password = HCoreEncrypt.EncryptHash(_Request.NewPassword);
                                    _UserAuth.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    _UserAuth.ModifyById = _Request.UserReference.AccountId;
                                    _Details.ModifyById = _Request.UserReference.AccountId;
                                    _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    _HCoreContext.SaveChanges();
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1195, TUCCoreResource.CA1195M);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1196, TUCCoreResource.CA1196M);
                                }
                            }
                            else
                            {
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1194, TUCCoreResource.CA1194M);
                            }
                        }
                        else
                        {
                            _HCoreContext.SaveChanges();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }

                    }
                    else
                    {
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
        }
        /// <summary>
        /// Description: Gets the profile.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetProfile(OProfile.Details.Request _Request)
        {
            if (_Request.ReferenceId == 0)
            {
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
            }
            else if (string.IsNullOrEmpty(_Request.ReferenceKey))
            {
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
            }
            else
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCUAccount
                        .Where(x => x.AccountTypeId == UserAccountType.Admin
                        && x.Id == _Request.UserReference.AccountId
                        && x.Guid == _Request.UserReference.AccountKey)
                        .Select(x => new OProfile.Details.Response
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            DisplayName = x.DisplayName,
                            Name = x.Name,
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            EmailAddress = x.EmailAddress,
                            MobileNumber = x.ContactNumber,
                            ContactNumber = x.ContactNumber,
                            GenderCode = x.Gender.SystemName,
                            GenderName = x.Gender.Name,
                            DateOfBirth = x.DateOfBirth,
                            RoleKey = x.Role.SystemName,
                            RoleName = x.Role.Name,
                            IconUrl = x.IconStorage.Path,
                            IconStorageId = x.IconStorageId,
                            OwnerId = x.OwnerId,
                            OwnerKey = x.Owner.Guid,
                            OwnerDisplayName = x.Owner.DisplayName,
                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyById,
                            ModifyByKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,
                            Address = x.Address,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                        }).FirstOrDefault();
                    if (_Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Details.IconUrl))
                        {
                            _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                        }
                        else
                        {
                            _Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
        }



        /// <summary>
        /// Description: Removes the profile image.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse RemoveProfileImage(OProfile.Profile _Request)
        {
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var Profile = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Profile != null)
                    {
                        if (Profile.IconStorageId != null && Profile.IconStorageId > 0)
                        {
                            long? IconStorageId = Profile.IconStorageId;
                            //_HCoreContext.HCUAccount.DeleteByKey(IconStorageId);
                            Profile.IconStorageId = null;
                            Profile.ModifyById = _Request.UserReference.AccountId;
                            Profile.ModifyDate = HCoreHelper.GetGMTDateTime();
                            _HCoreContext.SaveChanges();

                            bool DeleteStatus = HCoreHelper.DeleteStorage(IconStorageId, _Request.UserReference);
                            if (DeleteStatus == false)
                            {
                                HCUAccount _DetailUpdates = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                _DetailUpdates.IconStorageId = IconStorageId;
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1494, TUCCoreResource.CA1494M);
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1493, TUCCoreResource.CA1493M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1494, TUCCoreResource.CA1494M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("RemoveProfileImage", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
        }
    }
}
