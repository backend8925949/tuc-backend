//==================================================================================
// FileName: FrameworkMerchantOnBoarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to merchant onboarding
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Operations;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using HCore.TUC.Core.Object.Console;
using System.Collections.Generic;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.Operations.Object;
using HCore.Operations;
using HCore.Data.Operations.Models;

namespace HCore.TUC.Core.Framework.Console
{
    public class FrameworkMerchantOnBoarding
    {
        HCoreContext _HCoreContext;
        HCoreContextOperations _HCoreContextOperations;
        OCoreVerificationManager.Request _VerificationRequest;
        ManageCoreVerification _ManageCoreVerification;
        HCTMerchantOnboarding _HCTMerchantOnboarding;

        /// <summary>
        /// Description: Gets the merchant list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchantList(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContextOperations.HCTMerchantOnboarding
                            .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                            .Select(x => new OMerchantOnBoarding.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                EmailAddress = x.EmailAddress,
                                IsEmailVerified = x.IsEmailVerified,
                                EmailVerificationDate = x.EmailVerificationDate,
                                EmailCode = x.EmailCode,
                                DisplayName = x.DisplayName,
                                Categories = x.Categories,
                                CountryId = x.CountryId,
                                MobileNumber = x.MobileNumber,
                                IsMobileVrified = x.IsMobileVerified,
                                MobileVerificationDate = x.MobileVerificationDate,
                                MobileVerificationToken = x.MobileVerificationToken,
                                Address = x.Address,
                                CreateDate = x.CreateDate,
                                StatusId = x.StatusId,

                            }).Where(_Request.SearchCondition)
                              .Count();
                    }
                    List<OMerchantOnBoarding.List> _Data = _HCoreContextOperations.HCTMerchantOnboarding
                            .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                            .Select(x => new OMerchantOnBoarding.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                EmailAddress = x.EmailAddress,
                                IsEmailVerified = x.IsEmailVerified,
                                EmailVerificationDate = x.EmailVerificationDate,
                                EmailCode = x.EmailCode,
                                DisplayName = x.DisplayName,
                                Categories = x.Categories,
                                CountryId = x.CountryId,
                                MobileNumber = x.MobileNumber,
                                IsMobileVrified = x.IsMobileVerified,
                                MobileVerificationDate = x.MobileVerificationDate,
                                MobileVerificationToken = x.MobileVerificationToken,
                                Address = x.Address,
                                CreateDate = x.CreateDate,
                                StatusId = x.StatusId,

                            }).Where(_Request.SearchCondition)
                              .OrderBy(_Request.SortExpression)
                              .Skip(_Request.Offset)
                              .Take(_Request.Limit)
                              .ToList();

                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetMerchantList", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }


        /// <summary>
        /// Description: Resends the merchant verification email.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ResendMerchantVerificationEmail(OMerchantOnBoarding.EmailVerificationRequest _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1405, TUCCoreResource.CA1405M);
                }
                if (string.IsNullOrEmpty(_Request.Source))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1416, TUCCoreResource.CA1416M);
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var Details = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Guid == _Request.Reference).FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.IsEmailVerified != 1)
                        {
                            string Link = "https://" + _Request.Host + "/account/verifyemail/" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(Details.Id + "|" + Details.Guid));
                            //if (HostEnvironment == HostEnvironmentType.Test)
                            //{
                            //    Link = "https://" + _Request.Host + "/account/verifyemail/" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(Details.Id + "|" + Details.Guid));
                            //}
                            if (_Request.Source == "web")
                            {
                                if (HostEnvironment == HostEnvironmentType.Test)
                                {
                                    Link = "https://test.thankucash.com/merchant-email-verified.html?token=" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(Details.Id + "|" + Details.Guid));
                                }
                                else
                                {
                                    Link = "https://thankucash.com/merchant-email-verified.html?token=" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(Details.Id + "|" + Details.Guid));
                                }
                            }
                            else
                            {
                                if (HostEnvironment == HostEnvironmentType.Test)
                                {
                                    Link = "https://" + _Request.Host + "/account/verifyemail/" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(Details.Id + "|" + Details.Guid));
                                }
                                else
                                {
                                    Link = "https://" + _Request.Host + "/account/verifyemail/" + HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(Details.Id + "|" + Details.Guid));
                                }
                            }

                            var EmailObject = new
                            {
                                Link = Link
                            };
                            var _Response = new
                            {
                                Reference = Details.Guid,
                            };
                            HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantOnboardingEmailVerification, "TUC Merchant", Details.EmailAddress, EmailObject, _Request.UserReference);
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1412, TUCCoreResource.CA1412M);

                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1411, TUCCoreResource.CA1411M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1410, TUCCoreResource.CA1410M);
                    }

                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ResendMerchantVerificationEmail", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }


        /// <summary>
        /// Description: Resends the mobile verification otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ResendMobileVerificationOtp(OMerchantOnBoarding.MobileVerificationRequest _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1427, TUCCoreResource.CA1427M);
                }
                if (string.IsNullOrEmpty(_Request.Source))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1416, TUCCoreResource.CA1416M);
                }
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var Details = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Guid == _Request.Reference).FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.IsMobileVerified != 1)
                        {
                            Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            _HCoreContextOperations.SaveChanges();

                            #region Request Verification
                            _VerificationRequest = new OCoreVerificationManager.Request();
                            _VerificationRequest.CountryIsd = _Request.UserReference.CountryIsd;
                            _VerificationRequest.Type = 1;
                            _ManageCoreVerification = new ManageCoreVerification();
                            var VerificationResponse = _ManageCoreVerification.RequestOtp(_VerificationRequest);
                            #endregion
                            if (VerificationResponse.Status == StatusSuccess)
                            {
                                OCoreVerificationManager.Response VerificationResponseItem = (OCoreVerificationManager.Response)VerificationResponse.Result;
                                using (_HCoreContextOperations = new HCoreContextOperations())
                                {
                                    var UDetails = _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Id == Details.Id).FirstOrDefault();
                                    UDetails.MobileVerificationToken = VerificationResponseItem.RequestToken;
                                    _HCoreContextOperations.SaveChanges();
                                }
                            }

                            var _Response = new
                            {
                                Reference = Details.Guid,
                                EmailAddress = Details.EmailAddress,
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1415, TUCCoreResource.CA1415M);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1420, TUCCoreResource.CA1420M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1410, TUCCoreResource.CA1410M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ResendMobileVerificationOtp", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }
}
