//==================================================================================
// FileName: FrameworkMerchant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to topup history
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Text;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Integration.Paystack;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant.HelperStatus;

namespace HCore.TUC.Core.Framework.Console
{
    internal class FrameworkMerchant
    {
        HCoreContext _HCoreContext;
        /// <summary>
        /// Description: Gets the merchant topup history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchantTopupHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId > 0 && !string.IsNullOrEmpty(_Request.AccountKey))
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x=>x.Account.CountryId == _Request.UserReference.SystemCountry)
                                .Where(x => x.AccountId == _Request.AccountId
                                && x.Account.Guid == _Request.AccountKey
                                && x.ModeId == TransactionMode.Credit
                                && x.Account.AccountTypeId == UserAccountType.Merchant
                                 && x.SourceId == TransactionSource.Merchant)
                                                    .Select(x => new OMerchant.TopupHistory
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        Amount = x.Amount,
                                                        Charge = x.Charge,
                                                        TotalAmount = x.TotalAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        PaymentMethodCode = x.PaymentMethod.SystemName,
                                                        PaymentMethodName = x.PaymentMethod.Name,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        CreatedById = x.CreatedBy.Id,
                                                        CreatedByKey = x.CreatedBy.Guid,
                                                        CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                        TypeId = x.TypeId,
                                                        TypeKey = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                        TypeCategory = x.Type.SubParent.SystemName,

                                                        ModeId = x.ModeId,
                                                        ModeKey = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OMerchant.TopupHistory> _List = _HCoreContext.HCUAccountTransaction
                                                .Where(x=>x.Account.CountryId == _Request.UserReference.SystemCountry)
                                .Where(x => x.AccountId == _Request.AccountId
                                && x.Account.Guid == _Request.AccountKey
                                && x.ModeId == TransactionMode.Credit
                                && x.Account.AccountTypeId == UserAccountType.Merchant
                                 && x.SourceId == TransactionSource.Merchant)
                                                    .Select(x => new OMerchant.TopupHistory
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        Amount = x.Amount,
                                                        Charge = x.Charge,
                                                        TotalAmount = x.TotalAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        PaymentMethodCode = x.PaymentMethod.SystemName,
                                                        PaymentMethodName = x.PaymentMethod.Name,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        CreatedById = x.CreatedBy.Id,
                                                        CreatedByKey = x.CreatedBy.Guid,
                                                        CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                        TypeId = x.TypeId,
                                                        TypeKey = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                        TypeCategory = x.Type.SubParent.SystemName,

                                                        ModeId = x.ModeId,
                                                        ModeKey = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        foreach (var DataItem in _List)
                        {
                            if (!string.IsNullOrEmpty(DataItem.AccountIconUrl))
                            {
                                DataItem.AccountIconUrl = _AppConfig.StorageUrl + DataItem.AccountIconUrl;
                            }
                            else
                            {
                                DataItem.AccountIconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x=>x.Account.CountryId == _Request.UserReference.SystemCountry)
                                .Where(x => x.ModeId == TransactionMode.Credit
                                && x.Account.AccountTypeId == UserAccountType.Merchant
                                 && x.SourceId == TransactionSource.Merchant)
                                                    .Select(x => new OMerchant.TopupHistory
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        Amount = x.Amount,
                                                        Charge = x.Charge,
                                                        TotalAmount = x.TotalAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        PaymentMethodCode = x.PaymentMethod.SystemName,
                                                        PaymentMethodName = x.PaymentMethod.Name,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        TypeName = x.Type.Name,
                                                        TypeCategory = x.Type.SubParent.SystemName,

                                                        ModeId = x.ModeId,
                                                        ModeKey = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountMobileNumber = x.Account.MobileNumber,
                                                        AccountIconUrl = x.Account.IconStorage.Path,

                                                        CreatedById = x.CreatedBy.Id,
                                                        CreatedByKey = x.CreatedBy.Guid,
                                                        CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OMerchant.TopupHistory> _List = _HCoreContext.HCUAccountTransaction
                                                .Where(x=>x.Account.CountryId == _Request.UserReference.SystemCountry)
                                .Where(x => x.ModeId == TransactionMode.Credit
                                && x.Account.AccountTypeId == UserAccountType.Merchant
                                 && x.SourceId == TransactionSource.Merchant)
                                                    .Select(x => new OMerchant.TopupHistory
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        Amount = x.Amount,
                                                        Charge = x.Charge,
                                                        TotalAmount = x.TotalAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        PaymentMethodCode = x.PaymentMethod.SystemName,
                                                        PaymentMethodName = x.PaymentMethod.Name,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        TypeName = x.Type.Name,
                                                        TypeCategory = x.Type.SubParent.SystemName,

                                                        ModeId = x.ModeId,
                                                        ModeKey = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountMobileNumber = x.Account.MobileNumber,
                                                        AccountIconUrl = x.Account.IconStorage.Path,

                                                        CreatedById = x.CreatedBy.Id,
                                                        CreatedByKey = x.CreatedBy.Guid,
                                                        CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        foreach (var DataItem in _List)
                        {
                            if (!string.IsNullOrEmpty(DataItem.AccountIconUrl))
                            {
                                DataItem.AccountIconUrl = _AppConfig.StorageUrl + DataItem.AccountIconUrl;
                            }
                            else
                            {
                                DataItem.AccountIconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }

                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetMerchantTopupHistory", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

    }
}
