﻿using System;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Console.Analytics;
using System.Collections.Generic;
using System.Linq;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using System.Linq.Dynamic.Core;

namespace HCore.TUC.Core.Framework.Console.Analytics
{
    public class FrameworkCustomersOverview
    {
        HCoreContext _HCoreContext;
        List<OCustomerOverview.Categories> _FrequentPurchaseCategories;
        List<OCustomerOverview.Locations> _FrequentlyVisitedPlaces;
        OCustomerOverview.Transactions _Transactions;
        OCustomerOverview.TransactionsOverview _TransactionsOverview;

        internal OResponse GetFrequentlyPurchasedCategories(OCustomerOverview.Request _Request)
        {
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC001", "Account id missing");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    _FrequentPurchaseCategories = new List<OCustomerOverview.Categories>();
                    _FrequentPurchaseCategories = _HCoreContext.TUCCategory
                        .Where(x => x.HCUAccountPrimaryCategory.Where(a => a.HCUAccountTransactionAccount.Any(z => z.ParentId == a.Id && z.Account.AccountTypeId == UserAccountType.Appuser && z.AccountId == _Request.AccountId && (z.TransactionDate > _Request.StartDate && z.TransactionDate < _Request.EndDate))).Count() > 0)
                        .OrderByDescending(x => x.HCUAccountPrimaryCategory.Count)
                        .Select(x => new OCustomerOverview.Categories
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            Name = x.Name,
                            IconUrl = _AppConfig.StorageUrl + x.IconStorage.Path,
                            Amount = x.HCUAccountPrimaryCategory.Sum(a => a.HCUAccountTransactionAccount.Where(m => m.ParentId == a.Id && m.Account.AccountTypeId == UserAccountType.Appuser && m.AccountId == _Request.AccountId && (m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate)).Sum(z => z.TotalAmount)),
                        })
                        .Skip(0)
                        .Take(5)
                        .ToList();

                    OList.Response _OResponse = HCoreHelper.GetListResponse(_FrequentPurchaseCategories.Count, _FrequentPurchaseCategories, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001", "Details loaded");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetFrequentlyPurchasedCategories", _Exception, _Request.UserReference, "HCC0500", "Unable to load frequently purchase categories.");
            }
        }

        internal OResponse GetFrequentlyVisitedPlaces(OCustomerOverview.Request _Request)
        {
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC001", "Account id missing");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    _FrequentlyVisitedPlaces = new List<OCustomerOverview.Locations>();
                    _FrequentlyVisitedPlaces = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.Parent.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Transaction.Success && x.Parent.AccountTypeId == UserAccountType.Merchant
                        && x.AccountId == _Request.AccountId && x.Account.AccountTypeId == UserAccountType.Appuser)
                        .GroupBy(x => x.ParentId)
                        .Select(x => new OCustomerOverview.Locations
                        {
                            ReferenceId = x.Key,
                            Count = x.Count(),
                        })
                        .OrderByDescending(x => x.Count)
                        .Skip(0)
                        .Take(5)
                        .ToList();
                    foreach (var DataItem in _FrequentlyVisitedPlaces)
                    {
                        DataItem.ReferenceKey = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.ReferenceKey).FirstOrDefault();
                        DataItem.DisplayName = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.DisplayName).FirstOrDefault();
                        DataItem.Address = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.Address).FirstOrDefault();
                        DataItem.Latitude = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.Latitude).FirstOrDefault();
                        DataItem.Longitude = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.Longitude).FirstOrDefault();
                    }

                    OList.Response _OResponse = HCoreHelper.GetListResponse(_FrequentlyVisitedPlaces.Count, _FrequentlyVisitedPlaces, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001", "Details loaded");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetFrequentlyVisitedPlaces", _Exception, _Request.UserReference, "HCC0500", "Unable to load frequently visited places.");
            }
        }

        internal OResponse GetCustomersTransactions(OCustomerOverview.Request _Request)
        {
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC001", "Account id missing");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    _Transactions = new OCustomerOverview.Transactions();
                    _Transactions.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                     .Count(x => x.AccountId == _Request.AccountId
                                                     && x.TypeId == TransactionType.CardReward
                                                     && x.Customer.CountryId == _Request.UserReference.CountryId
                                                     && x.Type.SubParentId == TransactionTypeCategory.Reward
                                                     && x.StatusId == HelperStatus.Transaction.Success);

                    _Transactions.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                     .Count(x => x.AccountId == _Request.AccountId
                                                     && x.TypeId == TransactionType.CashReward
                                                     && x.Customer.CountryId == _Request.UserReference.CountryId
                                                     && x.Type.SubParentId == TransactionTypeCategory.Reward
                                                     && x.StatusId == HelperStatus.Transaction.Success);

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Transactions, "HC0001", "Details loaded");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetCustomersTransactions", _Exception, _Request.UserReference, "HCC0500", "Unable to load transactions data.");
            }
        }

        internal OResponse GetTransactionsOverview(OCustomerOverview.Request _Request)
        {
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC001", "Account id missing");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    _TransactionsOverview = new OCustomerOverview.TransactionsOverview();

                    _TransactionsOverview.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(x => x.AccountId == _Request.AccountId
                                                               && x.Account.AccountTypeId == UserAccountType.Appuser
                                                               && x.ParentId != null
                                                               && x.ModeId == TransactionMode.Credit
                                                               && x.ParentTransaction.TotalAmount > 0
                                                               && x.CampaignId == null
                                                               && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                               && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.TUCBlack))
                                                               || x.SourceId == TransactionSource.ThankUCashPlus)).Count();

                    _TransactionsOverview.RewardsAmount = _HCoreContext.HCUAccountTransaction
                                                          .Where(x => x.AccountId == _Request.AccountId
                                                          && x.Account.AccountTypeId == UserAccountType.Appuser
                                                          && x.ParentId != null
                                                          && x.ModeId == TransactionMode.Credit
                                                          && x.ParentTransaction.TotalAmount > 0
                                                          && x.CampaignId == null
                                                          && ((x.TypeId != TransactionType.ThankUCashPlusCredit
                                                          && (x.SourceId == TransactionSource.TUC
                                                          || x.SourceId == TransactionSource.TUCBlack))
                                                          || x.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount);

                    _TransactionsOverview.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Where(x => x.AccountId == _Request.AccountId
                                                               && x.ModeId == TransactionMode.Debit
                                                               && (x.SourceId == TransactionSource.TUC
                                                               || x.SourceId == TransactionSource.GiftPoints
                                                               || x.SourceId == TransactionSource.TUCBlack)
                                                               && x.Type.SubParentId == TransactionTypeCategory.Redeem).Count();

                    _TransactionsOverview.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(x => x.AccountId == _Request.AccountId
                                                               && x.ModeId == TransactionMode.Debit
                                                               && (x.SourceId == TransactionSource.TUC
                                                               || x.SourceId == TransactionSource.GiftPoints
                                                               || x.SourceId == TransactionSource.TUCBlack)
                                                               && x.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.TotalAmount);

                    _TransactionsOverview.RewardClaimTransactions = _HCoreContext.HCUAccountTransaction
                                                                    .Where(x => x.AccountId == _Request.AccountId
                                                                    && x.ParentId != null
                                                                    && x.ModeId == TransactionMode.Debit
                                                                    && x.SourceId == TransactionSource.ThankUCashPlus).Count();

                    _TransactionsOverview.RewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                              .Where(x => x.AccountId == _Request.AccountId
                                                              && x.ParentId != null
                                                              && x.ModeId == TransactionMode.Debit
                                                              && x.SourceId == TransactionSource.ThankUCashPlus).Sum(a => a.TotalAmount);

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _TransactionsOverview, "HC0200", "Details loaded.");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetTransactionsOverview", _Exception, _Request.UserReference, "HC0500", "Unable to load transactions data.");
            }
        }
    }
}