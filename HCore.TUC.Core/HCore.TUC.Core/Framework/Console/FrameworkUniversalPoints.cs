//==================================================================================
// FileName: FrameworkUniversalPoints.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Resource;
using HCore.TUC.Core.Object.Console;
using HCore.Operations.Object;
using HCore.Operations;
using static HCore.Helper.HCoreConstant.HelperStatus;
namespace HCore.TUC.Core.Framework.Console
{
    public class FrameworkUniversalPoints
    {

        ManageCoreTransaction _ManageCoreTransaction;
        OCoreTransaction.Request _CoreTransactionRequest;
        HCoreContext _HCoreContext;
        internal OResponse CreditUniversalPoints(OUniversalPoint.Request _Request)
        {
            if (_Request.AccountId < 1)
            {
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
            }
            else if (string.IsNullOrEmpty(_Request.AccountKey))
            {
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
            }
            else if (string.IsNullOrEmpty(_Request.Pin))
            {
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1297, TUCCoreResource.CA1297M);
            }
            //else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
            //{
            //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1298, TUCCoreResource.CA1298M);
            //}
            else if (_Request.Amount < 1)
            {
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1300, TUCCoreResource.CA1300M);
            }
            else if (string.IsNullOrEmpty(_Request.Comment))
            {
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1299, TUCCoreResource.CA1299M);
            }
            else
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var AdminDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Admin).FirstOrDefault();
                    if (AdminDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                    if (AdminDetails.StatusId != HelperStatus.Default.Active)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1303, TUCCoreResource.CA1303M);
                    }
                    var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.Id == _Request.AccountId && x.Guid == _Request.AccountKey).FirstOrDefault();
                    if (CustomerDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                    if (CustomerDetails.StatusId != HelperStatus.Default.Active)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1303, TUCCoreResource.CA1303M);
                    }

                    _CoreTransactionRequest = new OCoreTransaction.Request();
                    _CoreTransactionRequest.CustomerId = _Request.AccountId;
                    _CoreTransactionRequest.UserReference = _Request.UserReference;
                    _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                    _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                    _CoreTransactionRequest.ParentId = SystemAccounts.ThankUCashSystemId;
                    _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                    _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                    _CoreTransactionRequest.ReferenceNumber = HCoreHelper.GenerateDateString();
                    _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                    _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                    List<OCoreTransaction.TransactionItem> _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                    {
                        UserAccountId = SystemAccounts.ThankUCashSystemId,
                        ModeId = TransactionMode.Debit,
                        TypeId = TransactionType.TucSuperCash,
                        SourceId = TransactionSource.TUCSuperCash,
                        Amount = _Request.Amount,
                        Charge = 0,
                        TotalAmount = _Request.Amount,
                        Comment = _Request.Comment,
                    });
                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                    {
                        UserAccountId = _Request.AccountId,
                        ModeId = TransactionMode.Credit,
                        TypeId = TransactionType.TucSuperCash,
                        SourceId = TransactionSource.TUC,
                        Amount = _Request.Amount,
                        Charge = 0,
                        TotalAmount = _Request.Amount,
                        Comment = _Request.Comment,
                    });
                    _CoreTransactionRequest.Transactions = _TransactionItems;
                    _ManageCoreTransaction = new ManageCoreTransaction();
                    OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                    if (TransactionResponse.Status == Transaction.Success)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var UserNotUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Default.Active).OrderByDescending(x => x.Id).Select(x => x.NotificationUrl).FirstOrDefault();
                            _HCoreContext.Dispose();
                            if (!string.IsNullOrEmpty(UserNotUrl))
                            {
                                HCoreHelper.SendPushToDevice(UserNotUrl, "dashboard", "N" + _Request.Amount + " TUC SuperCash  Credited", "", "N" + _Request.Amount + " points credited to your ThankUCash account", 0, null, "View details", true, null);
                            }
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1481, TUCCoreResource.CA1481M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1482, TUCCoreResource.CA1482M);
                    }
                }
            }

        }
        internal OResponse GetUniversalPoints(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.Account.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == SystemAccounts.ThankUCashSystemId
                                                && x.ParentTransaction.SourceId == TransactionSource.TUCSuperCash
                                                && x.TypeId == TransactionType.TucSuperCash
                                                && x.StatusId == HelperStatus.Transaction.Success)
                                                .Select(x => new OUniversalPoint.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountEmailAddress = x.Account.EmailAddress,
                                                    EmailAddress = x.Account.EmailAddress,
                                                    AccountMobileNumber = x.Account.MobileNumber,
                                                    Amount = x.TotalAmount,
                                                    Comment = x.Comment,
                                                    //ReferenceNumber = x.ReferenceNumber,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    List<OUniversalPoint.List> Data = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.Account.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                && x.ParentId == SystemAccounts.ThankUCashSystemId
                                                && x.ParentTransaction.SourceId == TransactionSource.TUCSuperCash
                                                && x.TypeId == TransactionType.TucSuperCash
                                                && x.StatusId == HelperStatus.Transaction.Success)
                                                .Select(x => new OUniversalPoint.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    EmailAddress = x.Account.EmailAddress,
                                                    AccountEmailAddress = x.Account.EmailAddress,
                                                    AccountMobileNumber = x.Account.MobileNumber,
                                                    AccountIconUrl = x.Account.IconStorage.Path,
                                                    Amount = x.TotalAmount,
                                                    Comment = x.Comment,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        if (!string.IsNullOrEmpty(Details.AccountIconUrl))
                        {
                            Details.AccountIconUrl = _AppConfig.StorageUrl + Details.AccountIconUrl;
                        }
                        else
                        {
                            Details.AccountIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetUniversalPoints", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

    }
}
