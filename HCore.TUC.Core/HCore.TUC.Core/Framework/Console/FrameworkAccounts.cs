//==================================================================================
// FileName: FrameworkAccounts.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Resource;
using HCore.TUC.Core.CoreAccount;
using Akka.Actor;
using HCore.Operations.Object;
using HCore.Operations;
using static HCore.TUC.Core.Object.CustomerWeb.OAccount;
using Newtonsoft.Json;
using DocumentFormat.OpenXml.Drawing;
// || m.SourceId == TransactionSource.ThankUCashPlus
namespace HCore.TUC.Core.Framework.Console
{
    public class FrameworkAccounts
    {
        HCoreContext _HCoreContext;
        List<OAccounts.List> _Accounts;
        List<OAccounts.Partners.List> _Partners;
        List<OAccounts.Partners.ListDownload> _PartnersDownload;
        List<OAccounts.Acquirers.List> _Acquirers;
        List<OAccounts.Acquirers.ListDownload> _AcquirersDownload;
        List<OAccounts.Ptsp.List> _Ptsps;
        List<OAccounts.Ptsp.ListDownload> _PtspsDownloads;
        List<OAccounts.Pssp.List> _Pssps;
        List<OAccounts.Pssp.ListDownload> _PsspsDownloads;
        List<OAccounts.Merchant.List> _Merchants;
        List<OAccounts.Merchant.ListDownload> _MerchantsDownload;
        OAccounts.Merchant.Overview _MerchantOverview;
        OAccounts.Store.Overview _StoreOverview;
        List<OAccounts.Store.List> _Stores;
        List<OAccounts.Store.ListDownload> _StoresDownload;
        List<OAccounts.Store.Location> _StoreLocations;
        List<OAccounts.Terminal.List> _Terminals;
        OAccounts.Terminal.Overview _TerminalOverview;
        List<OAccounts.Customer.List> _Customers;
        List<OAccounts.Customer.ListDownload> _CustomersDownload;
        OAccounts.Partners.Details _PartnerDetails;
        OAccounts.Acquirers.Details _AcquirerDetails;
        OAccounts.Acquirers.Overview _AcquirerDetailsOverview;
        OAccounts.Ptsp.Details _PtspDetails;
        OAccounts.Ptsp.Overview _PtspDetailsOverview;
        OAccounts.Pssp.Details _PsspDetails;
        OAccounts.Pssp.Overview _PsspDetailsOverview;
        OAccounts.Merchant.Details _MerchantDetails;
        OAccounts.Merchant.DetailsOverview _MerchantDetailsOverview;
        OAccounts.Cashier.Overview _CashiersOverview;
        OAccounts.Cashier.Details _CashierDetails;
        List<OAccounts.Cashier.List> _Cashiers;
        List<OAccounts.Cashier.ListDownload> _CashiersDownload;
        OAddress _OAddress;
        ManageCoreTransaction _ManageCoreTransaction;
        OList.FResponse _CreditResponse;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        /// <summary>
        /// Description: Gets the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccount(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Accounts = new List<OAccounts.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Select(x => new OAccounts.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    AccountTypeCode = x.AccountType.SystemName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    CreateDate = x.CreateDate,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Accounts = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Select(x => new OAccounts.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    AccountTypeCode = x.AccountType.SystemName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    CreateDate = x.CreateDate,
                                                })
                                               .Where(_Request.SearchCondition)
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Accounts, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetAccount", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }


        /// <summary>
        /// Cancels deal count
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse CancelDealCount(OList.CancelDealRequest _Request)
        {
            try
            {
                #region Validation
                if (_Request.DealId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA1375M", TUCCoreResource.CA1375);
                }
                #endregion

                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    int Delivered = HCoreHelper.GetStatusId("orderstatus.delivered", _Request.UserReference);
                    int Shipped = HCoreHelper.GetStatusId("orderstatus.pickuped", _Request.UserReference);
                    int Shipped1 = HCoreHelper.GetStatusId("orderstatus.procvessedatfacility", _Request.UserReference);
                    int Shipped2 = HCoreHelper.GetStatusId("orderstatus.departedtofacility", _Request.UserReference);
                    int Shipped3 = HCoreHelper.GetStatusId("orderstatus.arrivedatfacility", _Request.UserReference);
                    int Shipped4 = HCoreHelper.GetStatusId("orderstatus.departedfromfacility", _Request.UserReference);
                    int Shipped5 = HCoreHelper.GetStatusId("orderstatus.outfordelivery", _Request.UserReference);

                    var OrderStatus = _HCoreContext.LSShipments.Where(x => x.DealId == _Request.DealId && x.Guid == _Request.DealKey).FirstOrDefault();
                    if (OrderStatus.StatusId == Delivered || OrderStatus.StatusId == Shipped || OrderStatus.StatusId == Shipped1 || OrderStatus.StatusId == Shipped2 || OrderStatus.StatusId == Shipped3 || OrderStatus.StatusId == Shipped4 || OrderStatus.StatusId == Shipped5)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CreditResponse, TUCCoreResource.CA1549, TUCCoreResource.CA1549M);
                    }
                    MDDealCode Details = _HCoreContext.MDDealCode.Where(x => x.DealId == _Request.DealId).FirstOrDefault();
                    if (Details != null)
                    {
                        Details.AvailableAmount = Details.AvailableAmount + _Request.CancelCount;

                        int? PaymentMethodId = HCoreHelper.GetSystemHelperId(PaymentMethod.TucWalletS, _Request.UserReference);

                        double SystemAmount = 0;
                        int TransactionSourceId = 0;
                        int TransactionTypeId = 0;


                        TransactionTypeId = TransactionType.Deal.DealPurchase;
                        TransactionSourceId = TransactionSource.Payments;


                        _CoreTransactionRequest = new OCoreTransaction.Request();
                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                        _CoreTransactionRequest.ParentId = _Request.UserReference.AccountId;
                        _CoreTransactionRequest.InvoiceAmount = _Request.DealPrice * _Request.CancelCount;
                        _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.DealPrice * _Request.CancelCount;
                        _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                        _CoreTransactionRequest.InvoiceNumber = _Request.TransactionReference;
                        _CoreTransactionRequest.ReferenceAmount = _Request.DealPrice * _Request.CancelCount;
                        _CoreTransactionRequest.PaymentMethodId = (int)PaymentMethodId;
                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = _Request.UserReference.AccountId,
                            ModeId = TransactionMode.Credit,
                            TypeId = TransactionTypeId,
                            SourceId = TransactionSourceId,
                            Comment = "Cancellation Reversal",
                            Amount = _Request.DealPrice * _Request.CancelCount,
                            Comission = SystemAmount,
                            TotalAmount = _Request.DealPrice * _Request.CancelCount,
                        });
                        _CoreTransactionRequest.Transactions = _TransactionItems;
                        _ManageCoreTransaction = new ManageCoreTransaction();
                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {

                                _CreditResponse.Credit = _HCoreContext.HCUAccountTransaction
                               .Where(x => x.AccountId == _Request.UserReference.AccountId
                               && x.SourceId == TransactionSource.Payments
                                  && x.StatusId == HelperStatus.Transaction.Success
                               && x.ModeId == TransactionMode.Credit)
                               .Sum(x => (double?)x.TotalAmount) ?? 0;
                                _CreditResponse.Debit = _HCoreContext.HCUAccountTransaction
                                  .Where(x => x.AccountId == _Request.UserReference.AccountId
                                  && x.SourceId == TransactionSource.Payments
                                  && x.StatusId == HelperStatus.Transaction.Success
                                  && x.ModeId == TransactionMode.Debit)
                                  .Sum(x => (double?)x.TotalAmount) ?? 0;
                                _CreditResponse.Balance = _CreditResponse.Credit - _CreditResponse.Debit;
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CreditResponse, TUCCoreResource.CA1116, TUCCoreResource.CA1116M);


                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1115, TUCCoreResource.CA1115M);
                        }



                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, "CA0404", TUCCoreResource.CA0404M);
                    }
                }
            }
            #region Exception
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("CancelDealCount", _Exception, _Request.UserReference, null, "CA0500M", TUCCoreResource.CA0500M);
            }
            #endregion
        }



        /// <summary>
        /// This api will return customers list as per type.
        /// If Type = thankucash it will return the customers list who has updated there profile.
        /// If Type = merchant it will return the customers list who has registered through the mecrhant and has not yet updated there profile.
        /// If Type = all It will eturn all customers list.
        /// If Type = appdownloads then it will return customers list who has downloaded the app in there system.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomer(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetCustomerDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetCustomerDownload>("ActorGetCustomerDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Customers = new List<OAccounts.Customer.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.Type == "thankucash")
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber)
                                                    .Select(x => new OAccounts.Customer.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        OwnerId = x.OwnerId,
                                                        OwnerKey = x.Owner.Guid,
                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                        OwnerAccountTypeId = x.Owner.AccountTypeId,
                                                        OwnerAccounttypeCode = x.Owner.AccountType.SystemName,
                                                        DisplayName = x.DisplayName,
                                                        Name = x.Name,
                                                        MobileNumber = x.MobileNumber,
                                                        EmailAddress = x.EmailAddress,
                                                        IconUrl = x.IconStorage.Path,
                                                        MainBalance = 0,
                                                        //LastTransactionDate = x.LastTransactionDate,
                                                        SubscriptionKey = "Purple",
                                                        SubscriptionName = "Purple",
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RegistrationSourceCode = x.RegistrationSource.SystemName,
                                                        RegistrationSourceName = x.RegistrationSource.Name,
                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            //&& (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                            && (((m.ModeId != TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash && m.TypeId != TransactionType.TUCRefund.Refund) && m.SourceId == TransactionSource.TUC)
                                                                                                            //|| (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            || (m.ModeId != TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success),
                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            //&& (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                            && (((m.ModeId != TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash && m.TypeId != TransactionType.TUCRefund.Refund) && m.SourceId == TransactionSource.TUC)
                                                                                                            //|| (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            || (m.ModeId != TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus),
                                                        TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            //&& m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.ModeId != Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                        TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                        TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus),
                                                        TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            // && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                        RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC),


                                                        RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                        TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0),



                                                        TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash)
                                                                                                        && m.SourceId == TransactionSource.TUC),

                                                        TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash)
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash)
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,




                                                        LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                            //&&m.SourceId == TransactionSource.TUC)
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        _Customers = _HCoreContext.HCUAccount
                                                    .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber).Select(x => new OAccounts.Customer.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        OwnerId = x.OwnerId,
                                                        OwnerKey = x.Owner.Guid,
                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                        OwnerAccountTypeId = x.Owner.AccountTypeId,
                                                        OwnerAccounttypeCode = x.Owner.AccountType.SystemName,
                                                        DisplayName = x.DisplayName,
                                                        Name = x.Name,
                                                        MobileNumber = x.MobileNumber,
                                                        EmailAddress = x.EmailAddress,
                                                        IconUrl = x.IconStorage.Path,
                                                        MainBalance = 0,
                                                        //LastTransactionDate = x.LastTransactionDate,
                                                        SubscriptionKey = "Purple",
                                                        SubscriptionName = "Purple",
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RegistrationSourceCode = x.RegistrationSource.SystemName,
                                                        RegistrationSourceName = x.RegistrationSource.Name,
                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            //&& (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                            && (((m.ModeId != TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash && m.TypeId != TransactionType.TUCRefund.Refund) && m.SourceId == TransactionSource.TUC)
                                                                                                            //|| (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            || (m.ModeId != TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success),
                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            //&& (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                            && (((m.ModeId != TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash && m.TypeId != TransactionType.TUCRefund.Refund) && m.SourceId == TransactionSource.TUC)
                                                                                                            //|| (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            || (m.ModeId != TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            // && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus),
                                                        TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            //&& m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.ModeId != Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                        TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                        TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus),
                                                        TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                        RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC),


                                                        RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                        TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                        TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash)
                                                                                                        && m.SourceId == TransactionSource.TUC),

                                                        TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash)
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash)
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,


                                                        LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                            //&&m.SourceId == TransactionSource.TUC)
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var DataItem in _Customers)
                        {
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            else
                            {
                                DataItem.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else if (_Request.Type == "merchant")
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                     .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber)
                                                    .Select(x => new OAccounts.Customer.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        OwnerId = x.OwnerId,
                                                        OwnerKey = x.Owner.Guid,
                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                        OwnerAccountTypeId = x.Owner.AccountTypeId,
                                                        OwnerAccounttypeCode = x.Owner.AccountType.SystemName,
                                                        DisplayName = x.DisplayName,
                                                        Name = x.Name,
                                                        MobileNumber = x.MobileNumber,
                                                        EmailAddress = x.EmailAddress,
                                                        IconUrl = x.IconStorage.Path,
                                                        MainBalance = 0,
                                                        //LastTransactionDate = x.LastTransactionDate,
                                                        SubscriptionKey = "Purple",
                                                        SubscriptionName = "Purple",
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RegistrationSourceCode = x.RegistrationSource.SystemName,
                                                        RegistrationSourceName = x.RegistrationSource.Name,
                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            //&& (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                            && (((m.ModeId != TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash && m.TypeId != TransactionType.TUCRefund.Refund) && m.SourceId == TransactionSource.TUC)
                                                                                                            //|| (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            || (m.ModeId != TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success),
                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            //&& (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                            && (((m.ModeId != TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash && m.TypeId != TransactionType.TUCRefund.Refund) && m.SourceId == TransactionSource.TUC)
                                                                                                            //|| (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            || (m.ModeId != TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != TransactionType.TucSuperCash
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus),
                                                        TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            //&& m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.ModeId != Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != TransactionType.TucSuperCash
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != TransactionType.TucSuperCash
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                        TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                        TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus),
                                                        TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                        RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC),


                                                        RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                        TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                        TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash)
                                                                                                        && m.SourceId == TransactionSource.TUC),

                                                        TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash)
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash)
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                        LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                        //                                                    && m.SourceId == TransactionSource.Merchant)
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        _Customers = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                     .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber)
                                                    .Select(x => new OAccounts.Customer.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        OwnerId = x.OwnerId,
                                                        OwnerKey = x.Owner.Guid,
                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                        OwnerAccountTypeId = x.Owner.AccountTypeId,
                                                        OwnerAccounttypeCode = x.Owner.AccountType.SystemName,
                                                        DisplayName = x.DisplayName,
                                                        Name = x.Name,
                                                        MobileNumber = x.MobileNumber,
                                                        EmailAddress = x.EmailAddress,
                                                        IconUrl = x.IconStorage.Path,
                                                        //LastTransactionDate = x.LastTransactionDate,
                                                        MainBalance = 0,
                                                        SubscriptionKey = "Purple",
                                                        SubscriptionName = "Purple",
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RegistrationSourceCode = x.RegistrationSource.SystemName,
                                                        RegistrationSourceName = x.RegistrationSource.Name,
                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            //&& (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                            && (((m.ModeId != TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash && m.TypeId != TransactionType.TUCRefund.Refund) && m.SourceId == TransactionSource.TUC)
                                                                                                            //|| (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            || (m.ModeId != TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success),
                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            //&& (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                            && (((m.ModeId != TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash && m.TypeId != TransactionType.TUCRefund.Refund) && m.SourceId == TransactionSource.TUC)
                                                                                                            //|| (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            || (m.ModeId != TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != Helpers.TransactionType.TucSuperCash
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus),
                                                        TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            //&& m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.ModeId != Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != Helpers.TransactionType.TucSuperCash
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.TypeId != Helpers.TransactionType.TucSuperCash
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                        TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                        TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus),
                                                        TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                        RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC),


                                                        RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                        TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                        TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash)
                                                                                                        && m.SourceId == TransactionSource.TUC),

                                                        TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash)
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash)
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                        LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                            //&& m.SourceId == TransactionSource.Merchant)
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var DataItem in _Customers)
                        {
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            else
                            {
                                DataItem.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else if (_Request.Type == "appdownloads")
                    {
                        if (_Request.RefreshCount)
                        {
                            //_Request.TotalRecords = _HCoreContext.HCUAccountSession
                            //                       .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser)
                            //                       .OrderByDescending(x=>x.LoginDate)
                            //                       .GroupBy(x => x.UserAccountId)
                            //                       .Select(x => new OAccounts.Customer.List
                            //                       {
                            //                           ReferenceId = x.FirstOrDefault().Account.Id,
                            //                           ReferenceKey = x.FirstOrDefault().Account.Guid,
                            //                           //OwnerId = x.FirstOrDefault().Account.OwnerId,
                            //                           //OwnerKey = x.FirstOrDefault().Account.Owner.Guid,
                            //                           //OwnerDisplayName = x.FirstOrDefault().Account.Owner.DisplayName,
                            //                           DisplayName = x.FirstOrDefault().Account.DisplayName,
                            //                           Name = x.FirstOrDefault().Account.Name,
                            //                           MobileNumber = x.FirstOrDefault().Account.MobileNumber,
                            //                           EmailAddress = x.FirstOrDefault().Account.EmailAddress,
                            //                           //IconUrl = x.FirstOrDefault().Account.IconStorage.Path,
                            //                           LastActivityDate = x.FirstOrDefault().LastActivityDate,
                            //                           AppRegistrationDate =x.FirstOrDefault().LoginDate,
                            //                            CreateDate = x.FirstOrDefault().Account.CreateDate,
                            //                           StatusCode = x.FirstOrDefault().Account.Status.SystemName,
                            //                           StatusName = x.FirstOrDefault().Account.Status.Name,
                            //                       })
                            //                       .Distinct()
                            //                      .Where(_Request.SearchCondition)
                            //              .Count();
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                    && x.HCUAccountSessionAccount.Any())
                                                    .Select(x => new OAccounts.Customer.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        OwnerId = x.OwnerId,
                                                        OwnerKey = x.Owner.Guid,
                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                        OwnerAccountTypeId = x.Owner.AccountTypeId,
                                                        OwnerAccounttypeCode = x.Owner.AccountType.SystemName,
                                                        DisplayName = x.DisplayName,
                                                        Name = x.Name,
                                                        MainBalance = 0,
                                                        MobileNumber = x.MobileNumber,
                                                        EmailAddress = x.EmailAddress,
                                                        IconUrl = x.IconStorage.Path,
                                                        AppRegistrationDate = x.HCUAccountSessionAccount.Select(a => a.LoginDate).FirstOrDefault().ToString(),
                                                        TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                        //LastActivityDate = x.HCUAccountSessionAccount.OrderByDescending(a=>a.LoginDate).Select(a=>a.LastActivityDate).FirstOrDefault().ToString(),
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RegistrationSourceCode = x.RegistrationSource.SystemName,
                                                        RegistrationSourceName = x.RegistrationSource.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        //#region Get Data
                        //_Customers = _HCoreContext.HCUAccountSession
                        //                           .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser)
                        //                           .OrderByDescending(x => x.LoginDate)
                        //                           .GroupBy(x => x.UserAccountId)
                        //                           .Select(x => new OAccounts.Customer.List
                        //                           {
                        //                               ReferenceId = x.FirstOrDefault().Account.Id,
                        //                               ReferenceKey = x.FirstOrDefault().Account.Guid,
                        //                               //OwnerId = x.FirstOrDefault().Account.OwnerId,
                        //                               //OwnerKey = x.FirstOrDefault().Account.Owner.Guid,
                        //                               //OwnerDisplayName = x.FirstOrDefault().Account.Owner.DisplayName,
                        //                               DisplayName = x.FirstOrDefault().Account.DisplayName,
                        //                               Name = x.FirstOrDefault().Account.Name,
                        //                               MobileNumber = x.FirstOrDefault().Account.MobileNumber,
                        //                               EmailAddress = x.FirstOrDefault().Account.EmailAddress,
                        //                               //IconUrl = x.FirstOrDefault().Account.IconStorage.Path,
                        //                               LastActivityDate = x.FirstOrDefault().LastActivityDate,
                        //                               AppRegistrationDate = x.FirstOrDefault().LoginDate,
                        //                               CreateDate = x.FirstOrDefault().Account.CreateDate,
                        //                               StatusCode = x.FirstOrDefault().Account.Status.SystemName,
                        //                               StatusName = x.FirstOrDefault().Account.Status.Name,
                        //                           })
                        //                         .Where(_Request.SearchCondition)
                        //                         .OrderBy(_Request.SortExpression)
                        //                         .Skip(_Request.Offset)
                        //                         .Take(_Request.Limit)
                        //                         .ToList();
                        //#endregion
                        #region Get Data
                        _Customers = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                    && x.HCUAccountSessionAccount.Any())
                                                    .Select(x => new OAccounts.Customer.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        OwnerId = x.OwnerId,
                                                        OwnerKey = x.Owner.Guid,
                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                        OwnerAccountTypeId = x.Owner.AccountTypeId,
                                                        OwnerAccounttypeCode = x.Owner.AccountType.SystemName,
                                                        DisplayName = x.DisplayName,
                                                        Name = x.Name,
                                                        MainBalance = 0,
                                                        MobileNumber = x.MobileNumber,
                                                        EmailAddress = x.EmailAddress,
                                                        IconUrl = x.IconStorage.Path,
                                                        AppRegistrationDate = x.HCUAccountSessionAccount.Select(a => a.LoginDate).FirstOrDefault().ToString(),
                                                        TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                        //LastActivityDate = x.HCUAccountSessionAccount.OrderByDescending(a => a.LoginDate).Select(a => a.LastActivityDate).ToString(),
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RegistrationSourceCode = x.RegistrationSource.SystemName,
                                                        RegistrationSourceName = x.RegistrationSource.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var DataItem in _Customers)
                        {
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            else
                            {
                                DataItem.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser || x.AccountTypeId == UserApplicationStatus.AppInstalled)
                                                    .Select(x => new OAccounts.Customer.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        OwnerId = x.OwnerId,
                                                        OwnerKey = x.Owner.Guid,
                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                        OwnerAccountTypeId = x.Owner.AccountTypeId,
                                                        OwnerAccounttypeCode = x.Owner.AccountType.SystemName,
                                                        DisplayName = x.DisplayName,
                                                        Name = x.Name,
                                                        MobileNumber = x.MobileNumber,
                                                        EmailAddress = x.EmailAddress,
                                                        IconUrl = x.IconStorage.Path,
                                                        MainBalance = 0,

                                                        //LastTransactionDate = x.LastTransactionDate,
                                                        SubscriptionKey = "Purple",
                                                        SubscriptionName = "Purple",
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RegistrationSourceCode = x.RegistrationSource.SystemName,
                                                        RegistrationSourceName = x.RegistrationSource.Name,
                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && (((m.ModeId != TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash && m.TypeId != TransactionType.TUCRefund.Refund) && m.SourceId == TransactionSource.TUC)
                                                                                                            || (m.ModeId != TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success),
                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && (((m.ModeId != TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash && m.TypeId != TransactionType.TUCRefund.Refund) && m.SourceId == TransactionSource.TUC)
                                                                                                            || (m.ModeId != TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                        TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                        && m.SourceId == TransactionSource.TUC),

                                                        TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId != Helpers.TransactionMode.Credit
                                                                                                        && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash)
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash)
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,


                                                        TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus),
                                                        TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                        TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                        TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus),
                                                        TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                        RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC),


                                                        RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                        TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                        LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.ModeId == Helpers.TransactionMode.Debit || m.ModeId == Helpers.TransactionMode.Credit))
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        _Customers = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.AccountTypeId == UserAccountType.Appuser || x.AccountTypeId == UserApplicationStatus.AppInstalled)
                                                    .Select(x => new OAccounts.Customer.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        OwnerId = x.OwnerId,
                                                        OwnerKey = x.Owner.Guid,
                                                        OwnerDisplayName = x.Owner.DisplayName,
                                                        OwnerAccountTypeId = x.Owner.AccountTypeId,
                                                        OwnerAccounttypeCode = x.Owner.AccountType.SystemName,
                                                        DisplayName = x.DisplayName,
                                                        Name = x.Name,
                                                        MobileNumber = x.MobileNumber,
                                                        EmailAddress = x.EmailAddress,
                                                        IconUrl = x.IconStorage.Path,
                                                        MainBalance = 0,
                                                        //LastTransactionDate = x.LastTransactionDate,
                                                        SubscriptionKey = "Purple",
                                                        SubscriptionName = "Purple",
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RegistrationSourceCode = x.RegistrationSource.SystemName,
                                                        RegistrationSourceName = x.RegistrationSource.Name,

                                                        TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && (((m.ModeId != TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash && m.TypeId != TransactionType.TUCRefund.Refund) && m.SourceId == TransactionSource.TUC)
                                                                                                            //|| (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            || (m.ModeId != TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success),

                                                        TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash && m.TypeId != TransactionType.TUCRefund.Refund) && m.SourceId == TransactionSource.TUC)
                                                                                                            //|| (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            || (m.ModeId != TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                        TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash)
                                                                                                        && m.SourceId == TransactionSource.TUC),

                                                        TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        //&& m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.ModeId != Helpers.TransactionMode.Credit
                                                                                                        && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash)
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        // && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && (m.TypeId != TransactionType.ThankUCashPlusCredit && m.TypeId != TransactionType.TucSuperCash)
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,






                                                        TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            // && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus),
                                                        TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                        TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                             && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                        TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus),
                                                        TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                        TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                        RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                            .Count(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC),


                                                        RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC)
                                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                        RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            //&& m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.TUC)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                        TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                            && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                            && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0),

                                                        LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                            .Where(m => m.AccountId == x.Id
                                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                                            && (m.ModeId == Helpers.TransactionMode.Debit || m.ModeId == Helpers.TransactionMode.Credit))
                                                                                                            //&& m.SourceId == TransactionSource.TUC)
                                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var DataItem in _Customers)
                        {
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            else
                            {
                                DataItem.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Customers, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCustomer", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Updates referral Id.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateReferralId(OList.ORequest _Request)
        {
            try
            {
                #region Manage Operations


                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.Id == _Request.AuthAccountId).FirstOrDefault();
                    if (AccountDetails != null)
                    {

                        if (!string.IsNullOrEmpty(_Request.ReferralCode))
                        {
                            var existingReferalCodeCheck = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.Id == _Request.AuthAccountId && x.ReferralCode == _Request.ReferralCode).Count();

                            if (existingReferalCodeCheck > 0)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1177, TUCCoreResource.CA1177M);
                            }
                            else
                            {
                                var oldRefCode = AccountDetails.ReferralCode;
                                AccountDetails.ReferralCode = _Request.ReferralCode;

                                var Referrers = _HCoreContext.HCUAccount.Where(x => x.ReferrerId == oldRefCode).ToList();

                                foreach (var ids in Referrers)
                                {
                                    ids.ReferrerId = _Request.ReferralCode;
                                }
                            }

                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1174, TUCCoreResource.CA1174M);
                        }

                        AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1178, TUCCoreResource.CA1178M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("UpdateAccount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }

        /// <summary>
        /// Description: Fetchess referral Amount Details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetReferralAmount(HCore.TUC.Core.Object.CustomerWeb.OAccount.ReferralAmountRequest _Request)
        {
            try
            {
                #region Manage Operations
                if (_Request.AuthAccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1371, TUCCoreResource.CA1371M);
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1369, TUCCoreResource.CA1369M);
                }
                if (_Request.Amount <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1382, TUCCoreResource.CA1382M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var TotalCount = _HCoreContext.ReferralSettings.Count();
                    var Data = _HCoreContext.ReferralSettings.OrderByDescending(x => x.Status).ToList();

                    if (Data != null)
                    {
                        OList.Response _OListResponse = new OList.Response();
                        _OListResponse.TotalRecords = TotalCount;
                        _OListResponse.Data = Data;
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OListResponse, TUCCoreResource.CA1540, TUCCoreResource.CA1540M);

                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1539, TUCCoreResource.CA1539M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GetReferralAmount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }


        /// <summary>
        /// Description: Sets Referral Amount.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SetReferralAmount(HCore.TUC.Core.Object.CustomerWeb.OAccount.ReferralAmountRequest _Request)
        {
            try
            {
                #region Manage Operations
                if (_Request.AuthAccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1371, TUCCoreResource.CA1371M);
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1369, TUCCoreResource.CA1369M);
                }
                if (_Request.Amount <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1382, TUCCoreResource.CA1382M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var ReferralDetails = _HCoreContext.ReferralSettings.Where(x => x.Status == HelperStatus.Default.Active).ToList();
                    foreach (var records in ReferralDetails)
                    {
                        records.Status = 0;
                    }

                    var newReferralSettings = new ReferralSettings
                    {
                        Amount = _Request.Amount,
                        DateSet = DateTime.Now,
                        SetBy = _Request.EmailAddress,
                        Status = 2
                    };

                    _HCoreContext.ReferralSettings.Add(newReferralSettings);
                    _HCoreContext.SaveChanges();

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, newReferralSettings, TUCCoreResource.CA1541, TUCCoreResource.CA1541M);

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SetReferralAmount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }


        /// <summary>
        /// Description: Gets Referral Bonus History.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetBonusHistory(HCore.TUC.Core.Object.CustomerWeb.OAccount.MerchantReferralRequest _Request)
        {
            try
            {
                #region Manage Operations

                using (_HCoreContext = new HCoreContext())
                {

                    var ReferralAmount = _HCoreContext.ReferralSettings.OrderByDescending(x => x.Status.ToString() == "2").FirstOrDefault();
                    var MerchantReferrals = _HCoreContext.HCUAccount.Where(x => x.ReferrerId == _Request.Referrer).Select(x => new MerchantReferralsResponse
                    {
                        Name = x.Name,
                        PhoneNumber = x.MobileNumber,
                        Amount = ReferralAmount.Amount

                    })
                    .Skip(_Request.Offset)
                    .Take(_Request.Limit)
                    .ToList();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, MerchantReferrals, _Request.Offset, _Request.Limit);
                    if (MerchantReferrals != null)
                    {

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, MerchantReferrals, TUCCoreResource.CA1543, TUCCoreResource.CA1543M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1542, TUCCoreResource.CA1542M);
                    }

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GetBonusHistory", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }



        /// <summary>
        /// Description: Fund user wallet.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse FundWallet(OList.FRequest _Request)
        {
            _ManageCoreTransaction = new ManageCoreTransaction();
            _CreditResponse = new OList.FResponse();
            _CreditResponse.Credit = 0;
            _CreditResponse.Debit = 0;
            _CreditResponse.Balance = 0;
            #region Manage Exception 
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1031, TUCCoreResource.CA1031M);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAA1476, TUCCoreResource.CAA1476M);
                }
                if (_Request.Amount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1085, TUCCoreResource.CA1085M);
                }

                if (string.IsNullOrEmpty(_Request.PaymentReference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1084, TUCCoreResource.CA1084M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId)
                        .Select(x => new
                        {
                            Id = x.Id,
                            AccountTypeId = x.AccountTypeId,
                            StatusId = x.StatusId,
                        })
                        .FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        if (AccountDetails.StatusId == HelperStatus.Default.Active)
                        {

                            double SystemAmount = 0;
                            int TransactionSourceId = 0;
                            int TransactionTypeId = 0;

                            if (_Request.PaymentType == "WalletFunding")
                            {
                                TransactionTypeId = TransactionType.Deal.DealPurchase;
                                TransactionSourceId = TransactionSource.Payments;
                            }

                            _CoreTransactionRequest = new OCoreTransaction.Request();
                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                            _CoreTransactionRequest.ParentId = _Request.AccountId;
                            _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                            _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                            _CoreTransactionRequest.ReferenceNumber = "Ref" + _Request.PaymentReference;
                            _CoreTransactionRequest.InvoiceNumber = _Request.TransactionReference;
                            _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = _Request.AccountId,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionTypeId,
                                SourceId = TransactionSourceId,
                                Comment = _Request.Comment,
                                Amount = _Request.Amount,
                                Comission = SystemAmount,
                                TotalAmount = _Request.Amount,
                            });
                            _CoreTransactionRequest.Transactions = _TransactionItems;
                            _ManageCoreTransaction = new ManageCoreTransaction();
                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    if (_Request.PaymentType == "WalletFunding")
                                    {
                                        _CreditResponse.Credit = _HCoreContext.HCUAccountTransaction
                                       .Where(x => x.AccountId == _Request.AccountId
                                       && x.SourceId == TransactionSource.Payments
                                          && x.StatusId == HelperStatus.Transaction.Success
                                       && x.ModeId == TransactionMode.Credit)
                                       .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _CreditResponse.Debit = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.AccountId == _Request.AccountId
                                          && x.SourceId == TransactionSource.Payments
                                          && x.StatusId == HelperStatus.Transaction.Success
                                          && x.ModeId == TransactionMode.Debit)
                                          .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _CreditResponse.Balance = _CreditResponse.Credit - _CreditResponse.Debit;
                                        _CreditResponse.Credit = _Request.Amount;
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CreditResponse, TUCCoreResource.CA1116, TUCCoreResource.CA1116M);
                                    }
                                    else
                                    {
                                        _CreditResponse.Credit = _HCoreContext.HCUAccountTransaction
                                       .Where(x => x.AccountId == _Request.AccountId
                                       && x.Source.SystemName == _Request.SourceCode
                                          && x.StatusId == HelperStatus.Transaction.Success
                                       && x.ModeId == TransactionMode.Credit)
                                       .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _CreditResponse.Debit = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.AccountId == _Request.AccountId
                                          && x.Source.SystemName == _Request.SourceCode
                                          && x.StatusId == HelperStatus.Transaction.Success
                                          && x.ModeId == TransactionMode.Debit)
                                          .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _CreditResponse.Balance = _CreditResponse.Credit - _CreditResponse.Debit;
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CreditResponse, TUCCoreResource.CA1116, TUCCoreResource.CA1116M);
                                    }

                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1115, TUCCoreResource.CA1115M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1170, TUCCoreResource.CA1170M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1176, TUCCoreResource.CA1176M);
                    }


                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreditWallet", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }




        /// <summary>
        /// Description: Gets the customer download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetCustomerDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Limit = 800;
                _CustomersDownload = new List<OAccounts.Customer.ListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.Type == "thankucash")
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber)
                                                .Select(x => new OAccounts.Customer.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    MainBalance = 0,
                                                    //LastTransactionDate = x.LastTransactionDate,
                                                    SubscriptionKey = "Purple",
                                                    SubscriptionName = "Purple",
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                                                    TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                                                    TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                    TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                    TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),
                                                    TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                    RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                    RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                    TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),



                                                    TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC),

                                                    TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,




                                                    LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                        double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                        _Request.Offset = 0;
                        for (int i = 0; i < Iterations; i++)
                        {
                            #region Get Data
                            var _Data = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                          .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber)
                                                          .Select(x => new OAccounts.Customer.List
                                                          {
                                                              ReferenceId = x.Id,
                                                              ReferenceKey = x.Guid,
                                                              OwnerId = x.OwnerId,
                                                              OwnerKey = x.Owner.Guid,
                                                              OwnerDisplayName = x.Owner.DisplayName,
                                                              DisplayName = x.DisplayName,
                                                              Name = x.Name,
                                                              MobileNumber = x.MobileNumber,
                                                              EmailAddress = x.EmailAddress,
                                                              IconUrl = x.IconStorage.Path,
                                                              MainBalance = 0,
                                                              SubscriptionKey = "Purple",
                                                              SubscriptionName = "Purple",
                                                              CreateDate = x.CreateDate,
                                                              StatusCode = x.Status.SystemName,
                                                              StatusName = x.Status.Name,
                                                              GenderName = x.Gender.Name,
                                                              DateOfBirth = x.DateOfBirth,
                                                              TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                                  .Count(m => m.AccountId == x.Id
                                                                                                                  && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                                  && m.StatusId == HelperStatus.Transaction.Success),
                                                              TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                                  .Where(m => m.AccountId == x.Id
                                                                                                                  && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                                  && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                                  || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                                  && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                                  .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                              TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                                  .Count(m => m.AccountId == x.Id
                                                                                                                  && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                                                                  && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                                  && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                                   && m.SourceId == TransactionSource.ThankUCashPlus),
                                                              TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                                  .Where(m => m.AccountId == x.Id
                                                                                                                  && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                                                                  && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                                  && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                                   && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                                  .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                              TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                                  .Where(m => m.AccountId == x.Id
                                                                                                                  && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                                                                  && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                                  && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                                   && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                                  .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                              TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                                  .Where(m => m.AccountId == x.Id
                                                                                                                  && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                                                                  && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                                  && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                                   && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                                  .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                              TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                                  .Count(m => m.AccountId == x.Id
                                                                                                                  && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                                                                  && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                                  && m.SourceId == TransactionSource.ThankUCashPlus),
                                                              TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                                  .Where(m => m.AccountId == x.Id
                                                                                                                  && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                                                                  && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                                  && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                                  .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                              TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                                  .Where(m => m.AccountId == x.Id
                                                                                                                  && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                                                                  && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                                  && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                                  .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                              RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                                  .Count(m => m.AccountId == x.Id
                                                                                                                  && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                                                                  && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                                  && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                                  && m.SourceId == TransactionSource.TUC),


                                                              RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                                  .Where(m => m.AccountId == x.Id
                                                                                                                  && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                                                                  && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                                  && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                                  && m.SourceId == TransactionSource.TUC)
                                                                                                                  .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                              RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                                  .Where(m => m.AccountId == x.Id
                                                                                                                  && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                                                                  && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                                  && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                                  && m.SourceId == TransactionSource.TUC)
                                                                                                                  .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                              TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                                  .Where(m => m.AccountId == x.Id
                                                                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                                                                  && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                                  && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                                  && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                                  .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                                  .Where(m => m.AccountId == x.Id
                                                                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                                                                  && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                                  && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                                  .Sum(m => (double?)m.TotalAmount) ?? 0),


                                                              TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                              .Count(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                              && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                              && m.SourceId == TransactionSource.TUC),

                                                              TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                              && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                              && m.SourceId == TransactionSource.TUC)
                                                                                                              .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                              TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                              && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                              && m.SourceId == TransactionSource.TUC)
                                                                                                              .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,


                                                              LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                                  .Where(m => m.AccountId == x.Id
                                                                                                                  && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                                  && m.StatusId == HelperStatus.Transaction.Success
                                                                                                                  && m.SourceId == TransactionSource.TUC)
                                                                                                                  .OrderByDescending(m => m.TransactionDate)
                                                                                                                  .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),

                                                          })
                                                       .Where(_Request.SearchCondition)
                                                       .OrderBy(_Request.SortExpression)
                                                       .Skip(_Request.Offset)
                                                       .Take(_Request.Limit)
                                                       .ToList();
                            #endregion
                            foreach (var _DataItem in _Data)
                            {
                                string Dob = null;
                                if (_DataItem.DateOfBirth != null)
                                {
                                    Dob = _DataItem.DateOfBirth.Value.ToString("dd-MM-yyyy");
                                }
                                string LTD = null;
                                if (_DataItem.LastTransactionDate != null)
                                {
                                    LTD = _DataItem.LastTransactionDate.Value.ToString("dd-MM-yyyy HH:mm");
                                }
                                _CustomersDownload.Add(new OAccounts.Customer.ListDownload
                                {
                                    ReferenceId = _DataItem.ReferenceId,
                                    DisplayName = _DataItem.DisplayName,
                                    Name = _DataItem.Name,
                                    EmailAddress = _DataItem.EmailAddress,
                                    MobileNumber = _DataItem.MobileNumber,
                                    Gender = _DataItem.GenderName,
                                    DateOfBirth = Dob,
                                    //Transactions = _DataItem.Transactions,
                                    MainBalance = _DataItem.MainBalance,
                                    LastTransactionDate = LTD,
                                    RegisteredOn = _DataItem.CreateDate,
                                    Status = _DataItem.StatusName,
                                });
                            }
                            _Request.Offset += 800;
                        }
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        HCoreHelper.CreateDownload("ThankUCash_Customers_List", _CustomersDownload, _Request.UserReference);
                        #endregion
                    }
                    else if (_Request.Type == "merchant")
                    {

                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                 .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber)
                                                .Select(x => new OAccounts.Customer.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    MainBalance = 0,
                                                    //LastTransactionDate = x.LastTransactionDate,
                                                    SubscriptionKey = "Purple",
                                                    SubscriptionName = "Purple",
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                                                    TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                                                    TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                    TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                    TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),
                                                    TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                    RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                    RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                    TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                    LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                        double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                        _Request.Offset = 0;
                        for (int i = 0; i < Iterations; i++)
                        {
                            #region Get Data
                            var _Data = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                       .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber)
                                                      .Select(x => new OAccounts.Customer.List
                                                      {
                                                          ReferenceId = x.Id,
                                                          ReferenceKey = x.Guid,
                                                          OwnerId = x.OwnerId,
                                                          OwnerKey = x.Owner.Guid,
                                                          OwnerDisplayName = x.Owner.DisplayName,
                                                          DisplayName = x.DisplayName,
                                                          Name = x.Name,
                                                          MobileNumber = x.MobileNumber,
                                                          EmailAddress = x.EmailAddress,
                                                          IconUrl = x.IconStorage.Path,
                                                          MainBalance = 0,
                                                          SubscriptionKey = "Purple",
                                                          SubscriptionName = "Purple",
                                                          CreateDate = x.CreateDate,
                                                          StatusCode = x.Status.SystemName,
                                                          StatusName = x.Status.Name,

                                                          TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                              .Count(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success),
                                                          TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                              .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                          TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                              .Count(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                               && m.SourceId == TransactionSource.ThankUCashPlus),
                                                          TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                               && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                              .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                          TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                               && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                              .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                          TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                               && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                              .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                          TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                              .Count(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                              && m.SourceId == TransactionSource.ThankUCashPlus),
                                                          TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                              && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                              .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                          TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                              && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                              .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                          RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                              .Count(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                              && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                              && m.SourceId == TransactionSource.TUC),


                                                          RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                              && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                              && m.SourceId == TransactionSource.TUC)
                                                                                                              .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                          RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                              && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                              && m.SourceId == TransactionSource.TUC)
                                                                                                              .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                          TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                              && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                              .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                              && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                              .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                          LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.SourceId == TransactionSource.TUC)
                                                                                                              .OrderByDescending(m => m.TransactionDate)
                                                                                                              .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                      })
                                                   .Where(_Request.SearchCondition)
                                                   .OrderBy(_Request.SortExpression)
                                                   .Skip(_Request.Offset)
                                                   .Take(_Request.Limit)
                                                   .ToList();
                            #endregion
                            foreach (var _DataItem in _Data)
                            {
                                _DataItem.MainBalance = _DataItem.TucRewardAmount - _DataItem.RedeemAmount;
                                string Dob = null;
                                if (_DataItem.DateOfBirth != null)
                                {
                                    Dob = _DataItem.DateOfBirth.Value.ToString("dd-MM-yyyy");
                                }
                                string LTD = null;
                                if (_DataItem.LastTransactionDate != null)
                                {
                                    LTD = _DataItem.LastTransactionDate.Value.ToString("dd-MM-yyyy HH:mm");
                                }
                                _CustomersDownload.Add(new OAccounts.Customer.ListDownload
                                {
                                    ReferenceId = _DataItem.ReferenceId,
                                    DisplayName = _DataItem.DisplayName,
                                    Name = _DataItem.Name,
                                    EmailAddress = _DataItem.EmailAddress,
                                    MobileNumber = _DataItem.MobileNumber,
                                    Gender = _DataItem.GenderName,
                                    DateOfBirth = Dob,
                                    //Transactions = _DataItem.Transactions,
                                    MainBalance = _DataItem.MainBalance,
                                    LastTransactionDate = LTD,
                                    RegisteredOn = _DataItem.CreateDate,
                                    Status = _DataItem.StatusName,
                                });
                            }
                            _Request.Offset += 800;
                        }
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        HCoreHelper.CreateDownload("Merchant_Customers_List", _CustomersDownload, _Request.UserReference);
                        #endregion

                    }
                    else if (_Request.Type == "appdownloads")
                    {


                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                && x.HCUAccountSessionAccount.Any())
                                                .Select(x => new OAccounts.Customer.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    AppRegistrationDate = x.HCUAccountSessionAccount.Select(a => a.LoginDate).FirstOrDefault().ToString(),
                                                    //LastActivityDate = x.HCUAccountSessionAccount.OrderByDescending(a=>a.LoginDate).Select(a=>a.LastActivityDate).FirstOrDefault().ToString(),
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                        double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                        _Request.Offset = 0;
                        for (int i = 0; i < Iterations; i++)
                        {
                            #region Get Data
                            var _Data = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                      .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                      && x.HCUAccountSessionAccount.Any())
                                                      .Select(x => new OAccounts.Customer.List
                                                      {
                                                          ReferenceId = x.Id,
                                                          ReferenceKey = x.Guid,
                                                          OwnerId = x.OwnerId,
                                                          OwnerKey = x.Owner.Guid,
                                                          OwnerDisplayName = x.Owner.DisplayName,
                                                          DisplayName = x.DisplayName,
                                                          Name = x.Name,
                                                          MobileNumber = x.MobileNumber,
                                                          EmailAddress = x.EmailAddress,
                                                          GenderName = x.Gender.Name,
                                                          DateOfBirth = x.DateOfBirth,
                                                          IconUrl = x.IconStorage.Path,
                                                          AppRegistrationDate = x.HCUAccountSessionAccount.Select(a => a.LoginDate).FirstOrDefault().ToString(),
                                                          //LastActivityDate = x.HCUAccountSessionAccount.OrderByDescending(a => a.LoginDate).Select(a => a.LastActivityDate).ToString(),
                                                          CreateDate = x.CreateDate,
                                                          StatusCode = x.Status.SystemName,
                                                          StatusName = x.Status.Name,
                                                      })
                                                   .Where(_Request.SearchCondition)
                                                   .OrderBy(_Request.SortExpression)
                                                   .Skip(_Request.Offset)
                                                   .Take(_Request.Limit)
                                                   .ToList();
                            #endregion
                            foreach (var _DataItem in _Data)
                            {
                                //_DataItem.MainBalance = _DataItem.TucRewardAmount - _DataItem.RedeemAmount;
                                string Dob = null;
                                if (_DataItem.DateOfBirth != null)
                                {
                                    Dob = _DataItem.DateOfBirth.Value.ToString("dd-MM-yyyy");
                                }

                                _CustomersDownload.Add(new OAccounts.Customer.ListDownload
                                {
                                    ReferenceId = _DataItem.ReferenceId,
                                    DisplayName = _DataItem.DisplayName,
                                    Name = _DataItem.Name,
                                    EmailAddress = _DataItem.EmailAddress,
                                    MobileNumber = _DataItem.MobileNumber,
                                    Gender = _DataItem.GenderName,
                                    DateOfBirth = Dob,
                                    //Transactions = _DataItem.Transactions,
                                    //MainBalance = _DataItem.MainBalance,
                                    //LastTransactionDate = _DataItem.LastTransactionDate,
                                    AppRegistrationDate = _DataItem.AppRegistrationDate,
                                    RegisteredOn = _DataItem.CreateDate,
                                    Status = _DataItem.StatusName,
                                });
                            }
                            #region Create  Response Object
                            _Request.Offset += 800;
                            #endregion
                        }
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        HCoreHelper.CreateDownload("App_Customers_List", _CustomersDownload, _Request.UserReference);
                        #endregion

                    }
                    else
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.Appuser)
                                                .Select(x => new OAccounts.Customer.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    MainBalance = 0,
                                                    //LastTransactionDate = x.LastTransactionDate,
                                                    SubscriptionKey = "Purple",
                                                    SubscriptionName = "Purple",
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    GenderName = x.Gender.Name,
                                                    DateOfBirth = x.DateOfBirth,
                                                    TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success),
                                                    TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,


                                                    TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                    .Count(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC),

                                                    TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                    .Where(m => m.AccountId == x.Id
                                                                                                    && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                                                    && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                    && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                    && m.SourceId == TransactionSource.TUC)
                                                                                                    .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,


                                                    TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus),
                                                    TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                    TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                         && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                    TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus),
                                                    TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                    TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                    RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                        .Count(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC),


                                                    RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                    RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                    TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                        && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                        && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                        .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                    LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                        .Where(m => m.AccountId == x.Id
                                                                                                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                                                        && m.SourceId == TransactionSource.TUC)
                                                                                                        .OrderByDescending(m => m.TransactionDate)
                                                                                                        .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                        double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                        _Request.Offset = 0;
                        for (int i = 0; i < Iterations; i++)
                        {
                            #region Get Data
                            var _Data = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                      .Where(x => x.AccountTypeId == UserAccountType.Appuser)
                                                      .Select(x => new OAccounts.Customer.List
                                                      {
                                                          ReferenceId = x.Id,
                                                          ReferenceKey = x.Guid,
                                                          OwnerId = x.OwnerId,
                                                          OwnerKey = x.Owner.Guid,
                                                          OwnerDisplayName = x.Owner.DisplayName,
                                                          DisplayName = x.DisplayName,
                                                          Name = x.Name,
                                                          MobileNumber = x.MobileNumber,
                                                          EmailAddress = x.EmailAddress,
                                                          IconUrl = x.IconStorage.Path,
                                                          MainBalance = 0,
                                                          SubscriptionKey = "Purple",
                                                          SubscriptionName = "Purple",
                                                          GenderName = x.Gender.Name,
                                                          DateOfBirth = x.DateOfBirth,
                                                          CreateDate = x.CreateDate,
                                                          StatusCode = x.Status.SystemName,
                                                          StatusName = x.Status.Name,

                                                          TotalTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                              .Count(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success),
                                                          TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success)
                                                                                                              .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                          TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                          .Count(m => m.AccountId == x.Id
                                                                                                          && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                                                                          && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                          && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                          && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                          && m.SourceId == TransactionSource.TUC),

                                                          TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                          .Where(m => m.AccountId == x.Id
                                                                                                          && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                                                                          && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                          && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                          && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                          && m.SourceId == TransactionSource.TUC)
                                                                                                          .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                          TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                          .Where(m => m.AccountId == x.Id
                                                                                                          && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                                                                          && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                          && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                          && m.TypeId != Helpers.TransactionType.ThankUCashPlusCredit
                                                                                                          && m.SourceId == TransactionSource.TUC)
                                                                                                          .Sum(m => (double?)m.ParentTransaction.TotalAmount) ?? 0,

                                                          TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                              .Count(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                               && m.SourceId == TransactionSource.ThankUCashPlus),
                                                          TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                               && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                              .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                          TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                               && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                              .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                          TucPlusConvinenceCharge = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                               && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                              .Sum(m => (double?)m.ParentTransaction.ComissionAmount) ?? 0,
                                                          TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                              .Count(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                              && m.SourceId == TransactionSource.ThankUCashPlus),
                                                          TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                              && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                              .Sum(m => (double?)m.PurchaseAmount) ?? 0,
                                                          TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                              && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                              .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                          RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                                                                              .Count(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                              && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                              && m.SourceId == TransactionSource.TUC),


                                                          RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                              && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                              && m.SourceId == TransactionSource.TUC)
                                                                                                              .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                          RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                                              && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                              && m.SourceId == TransactionSource.TUC)
                                                                                                              .Sum(m => (double?)m.TotalAmount) ?? 0,


                                                          TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                                              && m.ModeId == Helpers.TransactionMode.Credit
                                                                                                              && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                              .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.ModeId == Helpers.TransactionMode.Debit
                                                                                                              && m.SourceId == TransactionSource.ThankUCashPlus)
                                                                                                              .Sum(m => (double?)m.TotalAmount) ?? 0),
                                                          LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                                              .Where(m => m.AccountId == x.Id
                                                                                                              && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                                                                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                                                                              && m.SourceId == TransactionSource.TUC)
                                                                                                              .OrderByDescending(m => m.TransactionDate)
                                                                                                              .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                                      })
                                                   .Where(_Request.SearchCondition)
                                                   .OrderBy(_Request.SortExpression)
                                                   .Skip(_Request.Offset)
                                                   .Take(_Request.Limit)
                                                   .ToList();
                            #endregion
                            foreach (var _DataItem in _Data)
                            {
                                _DataItem.MainBalance = _DataItem.TucRewardAmount - _DataItem.RedeemAmount;
                                string Dob = null;
                                if (_DataItem.DateOfBirth != null)
                                {
                                    Dob = _DataItem.DateOfBirth.Value.ToString("dd-MM-yyyy");
                                }
                                string LTD = null;
                                if (_DataItem.LastTransactionDate != null)
                                {
                                    LTD = _DataItem.DateOfBirth.Value.ToString("dd-MM-yyyy HH:mm");
                                }
                                _CustomersDownload.Add(new OAccounts.Customer.ListDownload
                                {
                                    ReferenceId = _DataItem.ReferenceId,
                                    DisplayName = _DataItem.DisplayName,
                                    Name = _DataItem.Name,
                                    EmailAddress = _DataItem.EmailAddress,
                                    MobileNumber = _DataItem.MobileNumber,
                                    Gender = _DataItem.GenderName,
                                    DateOfBirth = Dob,
                                    //Transactions = _DataItem.Transactions,
                                    MainBalance = _DataItem.MainBalance,
                                    LastTransactionDate = LTD,
                                    RegisteredOn = _DataItem.CreateDate,
                                    Status = _DataItem.StatusName,
                                });
                            }
                            #region Create  Response Object
                            _Request.Offset += 800;
                            #endregion
                        }
                        _HCoreContext.Dispose();
                        HCoreHelper.CreateDownload("All_Customers_List", _CustomersDownload, _Request.UserReference);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCustomerDownload", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the partner.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPartner(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetPartnerDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetPartnerDownload>("ActorGetPartnerDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Partners = new List<OAccounts.Partners.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.Partner)
                                                .Select(x => new OAccounts.Partners.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    RmDisplayName = "ThankUCash",
                                                    IconUrl = x.IconStorage.Path,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Partners = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.Partner)
                                                .Select(x => new OAccounts.Partners.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Partners)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Partners, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetPartner", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the partner download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetPartnerDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Limit = 800;
                _PartnersDownload = new List<OAccounts.Partners.ListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.Partner)
                                                .Select(x => new OAccounts.Partners.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    RmDisplayName = "ThankUCash",
                                                    IconUrl = x.IconStorage.Path,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.Partner)
                                                .Select(x => new OAccounts.Partners.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            _PartnersDownload.Add(new OAccounts.Partners.ListDownload
                            {
                                ReferenceId = _DataItem.ReferenceId,
                                DisplayName = _DataItem.DisplayName,
                                Name = _DataItem.Name,
                                ContactNumber = _DataItem.ContactNumber,
                                EmailAddress = _DataItem.EmailAddress,
                                RelationshipManager = _DataItem.RmDisplayName,
                                LastTransactionDate = _DataItem.LastTransactionDate,
                                AddedOn = _DataItem.CreateDate,
                                Status = _DataItem.StatusName,
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("Partners_List", _PartnersDownload, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetPartnerDownload", _Exception);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the partner.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPartner(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _PartnerDetails = new OAccounts.Partners.Details();
                    _PartnerDetails = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.Partner
                                                && x.Id == _Request.AccountId
                                                && x.Guid == _Request.AccountKey)
                                                .Select(x => new OAccounts.Partners.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    AccountCode = x.AccountCode,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    LastActivityDate = x.LastActivityDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    StartDate = x.DateOfBirth,
                                                    Description = x.Description,
                                                    UserName = x.User.Username,
                                                    WebsiteUrl = x.WebsiteUrl,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    ReferralCode = x.ReferralCode,
                                                    Address = x.Address,
                                                }).FirstOrDefault();
                    if (_PartnerDetails != null)
                    {
                        _PartnerDetails.ContactPerson = _HCoreContext.HCUAccount.Where(x => x.Id == _PartnerDetails.ReferenceId)
                            .Select(x => new ContactPerson
                            {
                                FirstName = x.FirstName,
                                LastName = x.LastName,
                                MobileNumber = x.MobileNumber,
                                EmailAddress = x.EmailAddress,
                            }).FirstOrDefault();
                        _PartnerDetails.AddressComponent = _HCoreContext.HCUAccount.Where(x => x.Id == _PartnerDetails.ReferenceId)
                           .Select(x => new OAddress
                           {
                               Address = x.Address,
                               Latitude = x.Latitude,
                               Longitude = x.Longitude,
                               CityAreaId = x.CityAreaId,
                               CityAreaCode = x.CityArea.Guid,
                               CityAreaName = x.CityArea.Name,
                               CityId = x.CityId,
                               CityCode = x.City.Guid,
                               CityName = x.City.Name,
                               StateId = x.StateId,
                               StateCode = x.State.Guid,
                               StateName = x.State.Name,
                               CountryId = x.CountryId,
                               CountryCode = x.Country.Guid,
                               CountryName = x.Country.Name,
                           }).FirstOrDefault();

                        if (!string.IsNullOrEmpty(_PartnerDetails.IconUrl))
                        {
                            _PartnerDetails.IconUrl = _AppConfig.StorageUrl + _PartnerDetails.IconUrl;
                        }
                        else
                        {
                            _PartnerDetails.IconUrl = _AppConfig.Default_Icon;
                        }
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _PartnerDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetPartner", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the acquirer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAcquirer(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetAcquirerDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetAcquirerDownload>("ActorGetAcquirerDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Acquirers = new List<OAccounts.Acquirers.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.Acquirer)
                                                .Select(x => new OAccounts.Acquirers.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TotalTerminals = x.TUCTerminalAcquirer.Count(),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.AcquirerId == x.Id && m.StatusId == HelperStatus.Default.Active),
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Acquirers = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.Acquirer)
                                                .Select(x => new OAccounts.Acquirers.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TotalTerminals = x.TUCTerminalAcquirer.Count(),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.AcquirerId == x.Id && m.StatusId == HelperStatus.Default.Active),
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Acquirers)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Acquirers, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetAcquirer", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the acquirers download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetAcquirersDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Limit = 800;
                _AcquirersDownload = new List<OAccounts.Acquirers.ListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.Acquirer)
                                                .Select(x => new OAccounts.Acquirers.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TotalTerminals = x.TUCTerminalAcquirer.Count(),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.AcquirerId == x.Id && m.StatusId == HelperStatus.Default.Active),
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.Acquirer)
                                                .Select(x => new OAccounts.Acquirers.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TotalTerminals = x.TUCTerminalAcquirer.Count(),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.AcquirerId == x.Id && m.StatusId == HelperStatus.Default.Active),
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            _AcquirersDownload.Add(new OAccounts.Acquirers.ListDownload
                            {
                                ReferenceId = _DataItem.ReferenceId,
                                DisplayName = _DataItem.DisplayName,
                                Name = _DataItem.Name,
                                ContactNumber = _DataItem.ContactNumber,
                                EmailAddress = _DataItem.EmailAddress,
                                RelationshipManager = _DataItem.RmDisplayName,
                                LastTransactionDate = _DataItem.LastTransactionDate,
                                AddedOn = _DataItem.CreateDate,
                                Status = _DataItem.StatusName,
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("Acquirers_List", _AcquirersDownload, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAcquirersDownload", _Exception);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the acquirer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAcquirer(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _AcquirerDetails = new OAccounts.Acquirers.Details();
                    _AcquirerDetails = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.Acquirer
                                                && x.Id == _Request.AccountId
                                                && x.Guid == _Request.AccountKey)
                                                .Select(x => new OAccounts.Acquirers.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    AccountCode = x.AccountCode,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    LastActivityDate = x.LastActivityDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    StartDate = x.DateOfBirth,
                                                    Description = x.Description,
                                                    UserName = x.User.Username,
                                                    WebsiteUrl = x.WebsiteUrl,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    ReferralCode = x.ReferralCode,
                                                    Address = x.Address
                                                }).FirstOrDefault();
                    if (_AcquirerDetails != null)
                    {

                        _AcquirerDetails.ContactPerson = _HCoreContext.HCUAccount.Where(x => x.Id == _AcquirerDetails.ReferenceId)
                            .Select(x => new ContactPerson
                            {
                                FirstName = x.FirstName,
                                LastName = x.LastName,
                                MobileNumber = x.MobileNumber,
                                EmailAddress = x.SecondaryEmailAddress,
                            }).FirstOrDefault();
                        _AcquirerDetails.AddressComponent = _HCoreContext.HCUAccount.Where(x => x.Id == _AcquirerDetails.ReferenceId)
                            .Select(x => new OAddress
                            {
                                Address = x.Address,
                                Latitude = x.Latitude,
                                Longitude = x.Longitude,
                                CityAreaId = x.CityAreaId,
                                CityAreaCode = x.CityArea.Guid,
                                CityAreaName = x.CityArea.Name,
                                CityId = x.CityId,
                                CityCode = x.City.Guid,
                                CityName = x.City.Name,
                                StateId = x.StateId,
                                StateCode = x.State.Guid,
                                StateName = x.State.Name,
                                CountryId = x.CountryId,
                                CountryCode = x.Country.Guid,
                                CountryName = x.Country.Name,
                            }).FirstOrDefault();
                        if (!string.IsNullOrEmpty(_AcquirerDetails.IconUrl))
                        {
                            _AcquirerDetails.IconUrl = _AppConfig.StorageUrl + _AcquirerDetails.IconUrl;
                        }
                        else
                        {
                            _AcquirerDetails.IconUrl = _AppConfig.Default_Icon;
                        }
                        _AcquirerDetailsOverview = new OAccounts.Acquirers.Overview();

                        _AcquirerDetailsOverview.ReferredMerchants = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && (x.OwnerId == _Request.AccountId
                                                    || _HCoreContext.TUCTerminal.Any(m => m.MerchantId == x.Id && m.AcquirerId == _Request.AccountId))).Select(x => x.Id).Distinct().Count();

                        _AcquirerDetailsOverview.TotalMerchants = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && (x.OwnerId == _Request.AccountId
                                                    //|| _HCoreContext.TUCTerminal.Any(m => m.MerchantId == x.Id && m.AcquirerId == _Request.AccountId)
                                                    ))
                                                    //.Select(x => x.Id).Distinct()
                                                    .Count();

                        _AcquirerDetailsOverview.Terminals = _HCoreContext.TUCTerminal.Count(x => x.AcquirerId == _AcquirerDetails.ReferenceId);


                        _AcquirerDetailsOverview.Stores = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                    && (x.Owner.OwnerId == _Request.AccountId
                                                    || _HCoreContext.TUCTerminal.Any(m => m.AcquirerId == _Request.AccountId && m.StoreId == x.Id)
                                                    )).Count();


                        _AcquirerDetails.Overview = _AcquirerDetailsOverview;

                        var ProgramDetails = _HCoreContext.TUCLProgram
                                          .Where(x => x.AccountId == _AcquirerDetails.ReferenceId
                                          && x.StatusId == HelperStatus.Default.Active)
                                          .Select(x => new OAccounts.Acquirers.ProgramDetails
                                          {
                                              ProgramId = x.Id,
                                              ProgramReferenceKey = x.Guid
                                          }).ToList();
                        if (ProgramDetails.Count() > 0)
                        {
                            _AcquirerDetails.ProgramDetails = ProgramDetails;
                        }
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AcquirerDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAcquirer", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the PTSP.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPtsp(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetPtspDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetPtspDownload>("ActorGetPtspDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Ptsps = new List<OAccounts.Ptsp.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.PosAccount)
                                                .Select(x => new OAccounts.Ptsp.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TotalTerminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == x.Id),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == x.Id && m.StatusId == HelperStatus.Default.Active),
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Ptsps = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.PosAccount)
                                                .Select(x => new OAccounts.Ptsp.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TotalTerminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == x.Id),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == x.Id && m.StatusId == HelperStatus.Default.Active),
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Ptsps)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Ptsps, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetPtsp", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the PTSP.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPtsp(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _PtspDetails = new OAccounts.Ptsp.Details();
                    _PtspDetails = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.PosAccount
                                                && x.Id == _Request.AccountId
                                                && x.Guid == _Request.AccountKey)
                                                .Select(x => new OAccounts.Ptsp.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    AccountCode = x.AccountCode,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    LastActivityDate = x.LastActivityDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    StartDate = x.DateOfBirth,
                                                    Description = x.Description,
                                                    UserName = x.User.Username,
                                                    WebsiteUrl = x.WebsiteUrl,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    ReferralCode = x.ReferralCode,
                                                    Address = x.Address,
                                                }).FirstOrDefault();
                    if (_PtspDetails != null)
                    {

                        _PtspDetails.ContactPerson = _HCoreContext.HCUAccount.Where(x => x.Id == _PtspDetails.ReferenceId)
                            .Select(x => new ContactPerson
                            {
                                FirstName = x.FirstName,
                                LastName = x.LastName,
                                MobileNumber = x.MobileNumber,
                                EmailAddress = x.SecondaryEmailAddress,
                            }).FirstOrDefault();


                        _PtspDetails.AddressComponent = _HCoreContext.HCUAccount.Where(x => x.Id == _PtspDetails.ReferenceId)
                                                   .Select(x => new OAddress
                                                   {
                                                       Address = x.Address,
                                                       Latitude = x.Latitude,
                                                       Longitude = x.Longitude,
                                                       CityAreaId = x.CityAreaId,
                                                       CityAreaCode = x.CityArea.Guid,
                                                       CityAreaName = x.CityArea.Name,
                                                       CityId = x.CityId,
                                                       CityCode = x.City.Guid,
                                                       CityName = x.City.Name,
                                                       StateId = x.StateId,
                                                       StateCode = x.State.Guid,
                                                       StateName = x.State.Name,
                                                       CountryId = x.CountryId,
                                                       CountryCode = x.Country.Guid,
                                                       CountryName = x.Country.Name,
                                                   }).FirstOrDefault();

                        if (!string.IsNullOrEmpty(_PtspDetails.IconUrl))
                        {
                            _PtspDetails.IconUrl = _AppConfig.StorageUrl + _PtspDetails.IconUrl;
                        }
                        else
                        {
                            _PtspDetails.IconUrl = _AppConfig.Default_Icon;
                        }
                        _PtspDetailsOverview = new OAccounts.Ptsp.Overview();
                        _PtspDetailsOverview.ReferredMerchants = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && (x.OwnerId == _Request.AccountId
                                                    || _HCoreContext.TUCTerminal.Any(m => m.MerchantId == x.Id && m.ProviderId == _Request.AccountId))).Select(x => x.Id).Distinct().Count();
                        _PtspDetailsOverview.TotalMerchants = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && (x.OwnerId == _Request.AccountId
                                                    || _HCoreContext.TUCTerminal.Any(m => m.MerchantId == x.Id && m.ProviderId == _Request.AccountId))).Select(x => x.Id).Distinct().Count();
                        _PtspDetailsOverview.Terminals = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == _PtspDetails.ReferenceId);

                        _PtspDetailsOverview.Stores = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                    && x.OwnerId == _Request.AccountId && x.Owner.Guid == _Request.AccountKey
                                                    || _HCoreContext.TUCTerminal.Any(m => m.StoreId == x.Id && m.AcquirerId == _Request.AccountId)).Select(x => x.Id).Distinct().Count();

                        _PtspDetailsOverview.Stores = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                    && (x.Owner.OwnerId == _Request.AccountId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == _Request.AccountId && m.StoreId == x.Id))).Count();


                        _PtspDetails.Overview = _PtspDetailsOverview;
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _PtspDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetPtsp", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the PTSP download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetPtspDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Limit = 800;
                _PtspsDownloads = new List<OAccounts.Ptsp.ListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.PosAccount)
                                                .Select(x => new OAccounts.Ptsp.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TotalTerminals = x.InverseBank.Count(),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == x.Id && m.LastTransactionDate > ActivityDate),
                                                })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.PosAccount)
                                                .Select(x => new OAccounts.Ptsp.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    TotalTerminals = x.InverseBank.Count(),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == x.Id && m.LastTransactionDate > ActivityDate),
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            _PtspsDownloads.Add(new OAccounts.Ptsp.ListDownload
                            {
                                ReferenceId = _DataItem.ReferenceId,
                                DisplayName = _DataItem.DisplayName,
                                Name = _DataItem.Name,
                                ContactNumber = _DataItem.ContactNumber,
                                EmailAddress = _DataItem.EmailAddress,
                                RelationshipManager = _DataItem.RmDisplayName,
                                LastTransactionDate = _DataItem.LastTransactionDate,
                                AddedOn = _DataItem.CreateDate,
                                Status = _DataItem.StatusName,
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("Ptsp_List", _PtspsDownloads, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetPtspDownload", _Exception);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the PSSP.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPssp(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetPsspDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetPsspDownload>("ActorGetPsspDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Pssps = new List<OAccounts.Pssp.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.PgAccount)
                                                .Select(x => new OAccounts.Pssp.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Pssps = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.PgAccount)
                                                .Select(x => new OAccounts.Pssp.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Pssps)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Pssps, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetPssp", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the PSSP.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPssp(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _PsspDetails = new OAccounts.Pssp.Details();
                    _PsspDetails = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.PgAccount
                                                && x.Id == _Request.AccountId
                                                && x.Guid == _Request.AccountKey)
                                                .Select(x => new OAccounts.Pssp.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    AccountCode = x.AccountCode,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    LastActivityDate = x.LastActivityDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    StartDate = x.DateOfBirth,
                                                    Description = x.Description,
                                                    UserName = x.User.Username,
                                                    WebsiteUrl = x.WebsiteUrl,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    ReferralCode = x.ReferralCode,
                                                    Address = x.Address,
                                                }).FirstOrDefault();
                    if (_PsspDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_PsspDetails.IconUrl))
                        {
                            _PsspDetails.IconUrl = _AppConfig.StorageUrl + _PsspDetails.IconUrl;
                        }
                        else
                        {
                            _PsspDetails.IconUrl = _AppConfig.Default_Icon;
                        }
                        _PsspDetails.ContactPerson = _HCoreContext.HCUAccount.Where(x => x.Id == _PsspDetails.ReferenceId)
                            .Select(x => new ContactPerson
                            {
                                FirstName = x.FirstName,
                                LastName = x.LastName,
                                MobileNumber = x.MobileNumber,
                                EmailAddress = x.SecondaryEmailAddress,
                            }).FirstOrDefault();


                        _PsspDetails.AddressComponent = _HCoreContext.HCUAccount.Where(x => x.Id == _PsspDetails.ReferenceId)
                                                   .Select(x => new OAddress
                                                   {
                                                       Address = x.Address,
                                                       Latitude = x.Latitude,
                                                       Longitude = x.Longitude,
                                                       CityAreaId = x.CityAreaId,
                                                       CityAreaCode = x.CityArea.Guid,
                                                       CityAreaName = x.CityArea.Name,
                                                       CityId = x.CityId,
                                                       CityCode = x.City.Guid,
                                                       CityName = x.City.Name,
                                                       StateId = x.StateId,
                                                       StateCode = x.State.Guid,
                                                       StateName = x.State.Name,
                                                       CountryId = x.CountryId,
                                                       CountryCode = x.Country.Guid,
                                                       CountryName = x.Country.Name,
                                                   }).FirstOrDefault();

                        //_PsspDetails.Address = _HCoreContext.HCUAccount.Where(x => x.Id == _PsspDetails.ReferenceId)
                        //    .Select(x => new OAddressDetails
                        //    {
                        //        Address = x.Address,
                        //        Latitude = x.Latitude,
                        //        Longitude = x.Longitude,
                        //        CityAreaId = x.CityAreaId,
                        //        CityAreaName = x.CityArea.Name,
                        //        CityId = x.CityId,
                        //        CityName = x.City.Name,
                        //        StateId = x.StateId,
                        //        StateName = x.State.Name,
                        //        CountryId = x.CountryId,
                        //        CountryName = x.Country.Name,
                        //    }).FirstOrDefault();
                        _PsspDetailsOverview = new OAccounts.Pssp.Overview();
                        _PsspDetailsOverview.TotalMerchants = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant && x.OwnerId == _PsspDetails.ReferenceId);
                        _PsspDetails.Overview = _PsspDetailsOverview;
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _PsspDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetPssp", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the PSSP download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetPsspDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Limit = 800;
                _PsspsDownloads = new List<OAccounts.Pssp.ListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.PgAccount)
                                                .Select(x => new OAccounts.Pssp.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.PgAccount)
                                                .Select(x => new OAccounts.Pssp.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            _PsspsDownloads.Add(new OAccounts.Pssp.ListDownload
                            {
                                ReferenceId = _DataItem.ReferenceId,
                                DisplayName = _DataItem.DisplayName,
                                Name = _DataItem.Name,
                                ContactNumber = _DataItem.ContactNumber,
                                EmailAddress = _DataItem.EmailAddress,
                                RelationshipManager = _DataItem.RmDisplayName,
                                LastTransactionDate = _DataItem.LastTransactionDate,
                                AddedOn = _DataItem.CreateDate,
                                Status = _DataItem.StatusName,
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("Pssp_List", _PsspsDownloads, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetPsspDownload", _Exception);
            }
            #endregion
        }


        /// <summary>
        /// Description: Gets the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchant(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
                DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
                DateTime T7DayStart = TDayStart.AddDays(-7);
                DateTime T7DayEnd = TDayStart.AddSeconds(1);
                DateTime TDeadDayEnd = T7DayStart;
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetMerchantDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetMerchantDownload>("ActorGetMerchantDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Merchants = new List<OAccounts.Merchant.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant)
                                                .Select(x => new OAccounts.Merchant.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    ActivityStatusCode = x.ActivityStatus.SystemName,
                                                    ActivityStatusName = x.ActivityStatus.Name,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    MainBalance = 0,
                                                    RewardPercentage = x.AccountPercentage,
                                                    SubscriptionName = x.Subscription.Subscription.Name,
                                                    Address = x.Address,
                                                    PrimaryCategoryKey = x.PrimaryCategory.SystemName,
                                                    PrimaryCategoryName = x.PrimaryCategory.Name,
                                                    Stores = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantStore),
                                                    TotalTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id && m.StatusId == HelperStatus.Default.Active)
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Merchants = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant)
                                                .Select(x => new OAccounts.Merchant.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    ActivityStatusCode = x.ActivityStatus.SystemName,
                                                    ActivityStatusName = x.ActivityStatus.Name,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    MainBalance = 0,
                                                    RewardPercentage = x.AccountPercentage,
                                                    SubscriptionName = x.Subscription.Subscription.Name,
                                                    Address = x.Address,
                                                    PrimaryCategoryKey = x.PrimaryCategory.SystemName,
                                                    PrimaryCategoryName = x.PrimaryCategory.Name,
                                                    Stores = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantStore),
                                                    TotalTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id && m.StatusId == HelperStatus.Default.Active),
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object

                    foreach (var DataItem in _Merchants)
                    {
                        if (DataItem.LastTransactionDate > TDayStart && DataItem.LastTransactionDate < TDayEnd)
                        {
                            DataItem.ActivityStatusCode = "active";
                            DataItem.ActivityStatusName = "active";
                        }
                        else if ((DataItem.LastTransactionDate > T7DayStart && DataItem.LastTransactionDate < T7DayEnd) || DataItem.LastTransactionDate == null)
                        {
                            DataItem.ActivityStatusCode = "inactive";
                            DataItem.ActivityStatusName = "inactive";
                        }
                        else if (DataItem.StatusId == HelperStatus.Default.Blocked)
                        {
                            DataItem.ActivityStatusCode = "blocked";
                            DataItem.ActivityStatusName = "blocked";
                        }
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                        DataItem.RewardPercentage = Convert.ToDouble(_HCoreContext.HCUAccountParameter.Where(y => y.AccountId == DataItem.ReferenceId
                                                              && y.Account.Guid == DataItem.ReferenceKey
                                                              && y.Common.SystemName == "rewardpercentage").OrderByDescending(x => x.CreateDate)
                                                        .Select(y => y.Value).FirstOrDefault());

                        var ProgramDetails = _HCoreContext.TUCLProgram
                                                           .Where(x => x.AccountId == DataItem.ReferenceId
                                                                   && x.ProgramType.SystemName == "programtype.grouployalty"
                                                                   && x.StatusId == HelperStatus.Default.Active)
                                                           .Select(x => new
                                                           {
                                                               ProgramId = x.Id,
                                                               ProgramReferenceKey = x.Guid,
                                                               SystemName = x.ProgramType.SystemName,
                                                               TypeName = x.ProgramType.Name
                                                           }).FirstOrDefault();
                        var CloseOpenDetails = _HCoreContext.TUCLProgram
                                                               .Where(x => x.AccountId == DataItem.ReferenceId
                                                                       && (x.ProgramType.SystemName == "programtype.openloyalty"
                                                                       || x.ProgramType.SystemName == "programtype.closeloyalty")
                                                                       && x.StatusId == HelperStatus.Default.Active)
                                                               .Select(x => new
                                                               {
                                                                   ProgramId = x.Id,
                                                                   ProgramReferenceKey = x.Guid,
                                                                   SystemName = x.ProgramType.SystemName,
                                                                   TypeName = x.ProgramType.Name
                                                               }).FirstOrDefault();
                        var details = _HCoreContext.HCUAccountParameter
                                                    .Where(x => (x.AccountId == DataItem.ReferenceId)
                                                        && x.TypeId == HelperType.ThankUCashLoyalty
                                                        && (x.SubTypeId == HelperType.ThankUCashLoyalties.OpenLoyalty || x.SubTypeId == HelperType.ThankUCashLoyalties.ClosedLoyalty)
                                                        ).
                                                        Select(x => new
                                                        {
                                                            SubTypeId = x.SubTypeId
                                                        }).FirstOrDefault();

                        if (ProgramDetails != null && CloseOpenDetails != null)
                        {
                            DataItem.MerchantType = ProgramDetails.TypeName + " , " + CloseOpenDetails.TypeName;
                        }
                        else if (ProgramDetails == null && CloseOpenDetails != null)
                        {
                            DataItem.MerchantType = CloseOpenDetails.TypeName;
                        }
                        else if (ProgramDetails != null)
                        {
                            if (CloseOpenDetails != null)
                            {
                                DataItem.MerchantType = CloseOpenDetails.TypeName;
                            }
                            else
                            {
                                DataItem.MerchantType = ProgramDetails.TypeName;
                            }
                        }
                        else if (details != null)
                        {
                            bool IsOpenCloseLoyalty = (details.SubTypeId == HelperType.ThankUCashLoyalties.ClosedLoyalty) ? true : false;
                            if (IsOpenCloseLoyalty)
                            {
                                DataItem.MerchantType = "Close Loyalty Program";
                            }
                            else
                            {
                                DataItem.MerchantType = "Open Loyalty Program";
                            }
                        }
                        else
                        {
                            DataItem.MerchantType = "Open Loyalty Program";
                        }

                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Merchants, _Request.Offset, _Request.Limit);
                    _HCoreContext.Dispose();
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetAcquirer", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the merchant download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetMerchantDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Limit = 800;
                _MerchantsDownload = new List<OAccounts.Merchant.ListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant)
                                                .Select(x => new OAccounts.Merchant.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    MainBalance = 0,
                                                    RewardPercentage = x.AccountPercentage,
                                                    SubscriptionName = x.Subscription.Subscription.Name,
                                                    Address = x.Address,
                                                    PrimaryCategoryKey = x.PrimaryCategory.SystemName,
                                                    PrimaryCategoryName = x.PrimaryCategory.Name,
                                                    Stores = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantStore),
                                                    TotalTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                                })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant)
                                                .Select(x => new OAccounts.Merchant.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    MainBalance = 0,
                                                    RewardPercentage = x.AccountPercentage,
                                                    SubscriptionName = x.Subscription.Subscription.Name,
                                                    Address = x.Address,
                                                    PrimaryCategoryKey = x.PrimaryCategory.SystemName,
                                                    PrimaryCategoryName = x.PrimaryCategory.Name,
                                                    Stores = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantStore),
                                                    TotalTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            _MerchantsDownload.Add(new OAccounts.Merchant.ListDownload
                            {
                                ReferenceId = _DataItem.ReferenceId,
                                DisplayName = _DataItem.DisplayName,
                                Name = _DataItem.Name,
                                ContactNumber = _DataItem.ContactNumber,
                                EmailAddress = _DataItem.EmailAddress,
                                Address = _DataItem.Address,
                                Referrer = _DataItem.OwnerDisplayName,
                                ActiveTerminals = _DataItem.ActiveTerminals,
                                TotalTerminals = _DataItem.TotalTerminals,
                                Stores = _DataItem.Stores,
                                RelationshipManager = _DataItem.RmDisplayName,
                                ActiveSubscription = _DataItem.SubscriptionName,
                                WalletBalance = _DataItem.MainBalance,
                                RewardPercentage = _DataItem.RewardPercentage,
                                LastTransactionDate = _DataItem.LastTransactionDate,
                                CreateDate = _DataItem.CreateDate,
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("Merchants_List", _MerchantsDownload, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetMerchantDownload", _Exception);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the merchant overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchantOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _MerchantOverview = new OAccounts.Merchant.Overview();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total 
                    _MerchantOverview.Total = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                            .Where(x => x.AccountTypeId == UserAccountType.Merchant)
                                            .Select(x => new OAccounts.Merchant.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                OwnerId = x.OwnerId,
                                                OwnerKey = x.Owner.Guid,
                                                OwnerDisplayName = x.Owner.DisplayName,
                                                DisplayName = x.DisplayName,
                                                Name = x.Name,
                                                ContactNumber = x.ContactNumber,
                                                EmailAddress = x.EmailAddress,
                                                IconUrl = x.IconStorage.Path,
                                                RmDisplayName = "ThankUCash",
                                                LastTransactionDate = x.LastTransactionDate,
                                                LastLoginDate = x.LastLoginDate,
                                                CreateDate = x.CreateDate,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,
                                                MainBalance = 0,
                                                RewardPercentage = x.AccountPercentage,
                                                SubscriptionName = x.Subscription.Subscription.Name,
                                                Stores = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantStore),
                                                TotalTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                                ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    #region Active 
                    _MerchantOverview.Active = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                            .Where(x => x.AccountTypeId == UserAccountType.Merchant && x.StatusId == HelperStatus.Default.Active)
                                            .Select(x => new OAccounts.Merchant.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                OwnerId = x.OwnerId,
                                                OwnerKey = x.Owner.Guid,
                                                OwnerDisplayName = x.Owner.DisplayName,
                                                DisplayName = x.DisplayName,
                                                Name = x.Name,
                                                ContactNumber = x.ContactNumber,
                                                EmailAddress = x.EmailAddress,
                                                IconUrl = x.IconStorage.Path,
                                                RmDisplayName = "ThankUCash",
                                                LastTransactionDate = x.LastTransactionDate,
                                                LastLoginDate = x.LastLoginDate,
                                                CreateDate = x.CreateDate,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,
                                                MainBalance = 0,
                                                RewardPercentage = x.AccountPercentage,
                                                SubscriptionName = x.Subscription.Subscription.Name,
                                                Stores = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantStore),
                                                TotalTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                                ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    #region Inactive 
                    _MerchantOverview.Inactive = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                            .Where(x => x.AccountTypeId == UserAccountType.Merchant && x.StatusId == HelperStatus.Default.Inactive)
                                            .Select(x => new OAccounts.Merchant.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                OwnerId = x.OwnerId,
                                                OwnerKey = x.Owner.Guid,
                                                OwnerDisplayName = x.Owner.DisplayName,
                                                DisplayName = x.DisplayName,
                                                Name = x.Name,
                                                ContactNumber = x.ContactNumber,
                                                EmailAddress = x.EmailAddress,
                                                IconUrl = x.IconStorage.Path,
                                                RmDisplayName = "ThankUCash",
                                                LastTransactionDate = x.LastTransactionDate,
                                                LastLoginDate = x.LastLoginDate,
                                                CreateDate = x.CreateDate,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,
                                                MainBalance = 0,
                                                RewardPercentage = x.AccountPercentage,
                                                SubscriptionName = x.Subscription.Subscription.Name,
                                                Stores = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantStore),
                                                TotalTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                                ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    #region Inactive 
                    _MerchantOverview.Suspended = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                            .Where(x => x.AccountTypeId == UserAccountType.Merchant && x.StatusId == HelperStatus.Default.Suspended)
                                            .Select(x => new OAccounts.Merchant.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                OwnerId = x.OwnerId,
                                                OwnerKey = x.Owner.Guid,
                                                OwnerDisplayName = x.Owner.DisplayName,
                                                DisplayName = x.DisplayName,
                                                Name = x.Name,
                                                ContactNumber = x.ContactNumber,
                                                EmailAddress = x.EmailAddress,
                                                IconUrl = x.IconStorage.Path,
                                                RmDisplayName = "ThankUCash",
                                                LastTransactionDate = x.LastTransactionDate,
                                                LastLoginDate = x.LastLoginDate,
                                                CreateDate = x.CreateDate,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,
                                                MainBalance = 0,
                                                RewardPercentage = x.AccountPercentage,
                                                SubscriptionName = x.Subscription.Subscription.Name,
                                                Stores = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantStore),
                                                TotalTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                                ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    #region Inactive 
                    _MerchantOverview.Blocked = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                            .Where(x => x.AccountTypeId == UserAccountType.Merchant && x.StatusId == HelperStatus.Default.Blocked)
                                            .Select(x => new OAccounts.Merchant.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                OwnerId = x.OwnerId,
                                                OwnerKey = x.Owner.Guid,
                                                OwnerDisplayName = x.Owner.DisplayName,
                                                DisplayName = x.DisplayName,
                                                Name = x.Name,
                                                ContactNumber = x.ContactNumber,
                                                EmailAddress = x.EmailAddress,
                                                IconUrl = x.IconStorage.Path,
                                                RmDisplayName = "ThankUCash",
                                                LastTransactionDate = x.LastTransactionDate,
                                                LastLoginDate = x.LastLoginDate,
                                                CreateDate = x.CreateDate,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,
                                                MainBalance = 0,
                                                RewardPercentage = x.AccountPercentage,
                                                SubscriptionName = x.Subscription.Subscription.Name,
                                                Stores = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantStore),
                                                TotalTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                                ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _MerchantOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetAcquirer", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchant(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    _MerchantDetails = new OAccounts.Merchant.Details();
                    _MerchantDetails = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                && x.Id == _Request.AccountId
                                                && x.Guid == _Request.AccountKey)
                                                .Select(x => new OAccounts.Merchant.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    AccountCode = x.AccountCode,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    LastActivityDate = x.LastActivityDate,
                                                    LastLoginDate = x.LastLoginDate,
                                                    StartDate = x.DateOfBirth,
                                                    Description = x.Description,
                                                    UserName = x.User.Username,
                                                    WebsiteUrl = x.WebsiteUrl,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    ReferralCode = x.ReferralCode,
                                                    Address = x.Address,
                                                }).FirstOrDefault();
                    if (_MerchantDetails != null)
                    {
                        _MerchantDetails.ContactPerson = _HCoreContext.HCUAccount.Where(x => x.Id == _MerchantDetails.ReferenceId)
                            .Select(x => new ContactPerson
                            {
                                FirstName = x.FirstName,
                                LastName = x.LastName,
                                MobileNumber = x.MobileNumber,
                                EmailAddress = x.EmailAddress,
                            }).FirstOrDefault();



                        _MerchantDetails.AddressComponent = _HCoreContext.HCUAccount.Where(x => x.Id == _MerchantDetails.ReferenceId)
                                                   .Select(x => new OAddress
                                                   {
                                                       Address = x.Address,
                                                       Latitude = x.Latitude,
                                                       Longitude = x.Longitude,
                                                       CityAreaId = x.CityAreaId,
                                                       CityAreaCode = x.CityArea.Guid,
                                                       CityAreaName = x.CityArea.Name,
                                                       CityId = x.CityId,
                                                       CityCode = x.City.Guid,
                                                       CityName = x.City.Name,
                                                       StateId = x.StateId,
                                                       StateCode = x.State.Guid,
                                                       StateName = x.State.Name,
                                                       CountryId = x.CountryId,
                                                       CountryCode = x.Country.Guid,
                                                       CountryName = x.Country.Name,
                                                   }).FirstOrDefault();
                        //_MerchantDetails.Address = _HCoreContext.HCUAccount.Where(x => x.Id == _MerchantDetails.ReferenceId)
                        //    .Select(x => new OAddressDetails
                        //    {
                        //        Address = x.Address,
                        //        Latitude = x.Latitude,
                        //        Longitude = x.Longitude,
                        //        CityAreaId = x.CityAreaId,
                        //        CityAreaName = x.CityArea.Name,
                        //        CityId = x.CityId,
                        //        CityName = x.City.Name,
                        //        StateId = x.StateId,
                        //        StateName = x.State.Name,
                        //        CountryId = x.CountryId,
                        //        CountryName = x.Country.Name,
                        //    }).FirstOrDefault();
                        _MerchantDetails.Subscription = _HCoreContext.HCUAccountSubscription.Where(x => x.StatusId == HelperStatus.Default.Active
                        && x.AccountId == _MerchantDetails.ReferenceId)
                            .Select(x => new OAccounts.Merchant.Subscription
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                Name = x.Subscription.Name,
                                StartDate = x.StartDate,
                                EndDate = x.EndDate,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                                Amount = x.SellingPrice,
                            }).FirstOrDefault();
                        //_MerchantDetails.Categories = _HCoreContext.HCUAccountParameter.Where(x => x.AccountId == _MerchantDetails.ReferenceId && x.TypeId == HelperType.MerchantCategory && x.StatusId == HelperStatus.Default.Active)
                        //  .OrderBy(x => x.Common.Name)
                        //  .Select(x => new OAccounts.Category
                        //  {
                        //      ReferenceId = x.Common.Id,
                        //      ReferenceKey = x.Common.Guid,
                        //      Name = x.Common.Name,
                        //      IconUrl = _AppConfig.StorageUrl + x.Common.IconStorage.Path,
                        //  }).ToList();
                        if (!string.IsNullOrEmpty(_MerchantDetails.IconUrl))
                        {
                            _MerchantDetails.IconUrl = _AppConfig.StorageUrl + _MerchantDetails.IconUrl;
                        }
                        else
                        {
                            _MerchantDetails.IconUrl = _AppConfig.Default_Icon;
                        }
                        _MerchantDetailsOverview = new OAccounts.Merchant.DetailsOverview();
                        _MerchantDetailsOverview.Stores = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.MerchantStore && x.OwnerId == _MerchantDetails.ReferenceId);
                        _MerchantDetailsOverview.Cashiers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.MerchantCashier && x.SubOwnerId == _MerchantDetails.ReferenceId);
                        _MerchantDetailsOverview.Terminals = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _MerchantDetails.ReferenceId);
                        _MerchantDetails.Overview = _MerchantDetailsOverview;
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _MerchantDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }



                    //var Details = _HCoreContext.HCUAccount
                    //    .Where(x => x.Guid == _Request.AccountKey && x.Id == _Request.AccountId && x.AccountTypeId == UserAccountType.Merchant)
                    //    .Select(x => new
                    //    {
                    //        ReferenceId = x.Id,
                    //        ReferenceKey = x.Guid,
                    //        DisplayName = x.DisplayName,
                    //        ContactNumber = x.ContactNumber,
                    //        EmailAddress = x.EmailAddress,
                    //        IconUrl = x.IconStorage.Path,
                    //        AccountCode = x.AccountCode,
                    //        FirstName = x.FirstName,
                    //        LastName = x.LastName,
                    //        SecondaryEmailAddress = x.SecondaryEmailAddress,
                    //        MobileNumber = x.MobileNumber,
                    //        Description = x.Description,
                    //        Address = x.Address,
                    //        Latitude = x.Latitude,
                    //        Longitude = x.Longitude,
                    //        CityAreaCode = x.CityArea.SystemName,
                    //        CityAreaName = x.CityArea.Name,
                    //        CityCode = x.City.SystemName,
                    //        CityName = x.City.Name,
                    //        StateCode = x.Region.SystemName,
                    //        StateName = x.Region.Name,
                    //        CountryCode = x.Country.SystemName,
                    //        CountryName = x.Country.Name,
                    //        OwnerName = x.CpName,
                    //        CreateDate = x.CreateDate,
                    //        CreatedByReferenceId = x.CreatedById,
                    //        CreatedByReferenceKey = x.CreatedBy.Guid,
                    //        CreatedByDisplayName = x.CreatedBy.DisplayName,
                    //        ModifyDate = x.ModifyDate,
                    //        ModifyByReferenceId = x.ModifyById,
                    //        ModifyByReferenceKey = x.ModifyBy.Guid,
                    //        ModifyByDisplayName = x.ModifyBy.DisplayName,
                    //        WebsiteUrl = x.WebsiteUrl,

                    //        StatusCode = x.Status.SystemName,
                    //        StatusName = x.Status.Name,
                    //    }).FirstOrDefault();
                    //if (Details != null)
                    //{

                    //    _MerchantDetails = new OAccounts.Merchant.Response();
                    //    _MerchantDetails.OwnerName = Details.OwnerName;
                    //    _MerchantDetails.Categories = _HCoreContext.HCUAccountParameter.Where(x => x.AccountId == Details.ReferenceId && x.TypeId == HelperType.MerchantCategory && x.StatusId == HelperStatus.Default.Active)
                    //        .OrderBy(x => x.Common.Name)
                    //        .Select(x => new OAccounts.Category
                    //        {
                    //            ReferenceId = x.Common.Id,
                    //            ReferenceKey = x.Common.Guid,
                    //            Name = x.Common.Name,
                    //            IconUrl = _AppConfig.StorageUrl + x.Common.IconStorage.Path,
                    //        }).ToList();
                    //    _MerchantDetails.Stores = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.MerchantStore && x.OwnerId == Details.ReferenceId);
                    //    _MerchantDetails.Cashiers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.MerchantCashier && x.SubOwnerId == Details.ReferenceId);
                    //    _MerchantDetails.Terminals = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.TerminalAccount && x.SubOwner.OwnerId == Details.ReferenceId);
                    //    _MerchantDetails.ReferenceId = Details.ReferenceId;
                    //    _MerchantDetails.ReferenceKey = Details.ReferenceKey;
                    //    _MerchantDetails.AccountCode = Details.AccountCode;
                    //    _MerchantDetails.DisplayName = Details.DisplayName;
                    //    _MerchantDetails.EmailAddress = Details.EmailAddress;
                    //    _MerchantDetails.WebsiteUrl = Details.WebsiteUrl;
                    //    _MerchantDetails.Description = Details.Description;
                    //    if (!string.IsNullOrEmpty(Details.IconUrl))
                    //    {
                    //        _MerchantDetails.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                    //    }
                    //    else
                    //    {
                    //        _MerchantDetails.IconUrl = _AppConfig.Default_Icon;
                    //    }
                    //    if (!string.IsNullOrEmpty(Details.IconUrl))
                    //    {
                    //        _MerchantDetails.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                    //    }
                    //    else
                    //    {
                    //        _MerchantDetails.IconUrl = _AppConfig.Default_Icon;
                    //    }
                    //    _ContactPerson = new OAccounts.ContactPerson
                    //    {
                    //        FirstName = Details.FirstName,
                    //        LastName = Details.LastName,
                    //        EmailAddress = Details.EmailAddress,
                    //        MobileNumber = Details.MobileNumber,
                    //    };
                    //    _MerchantDetails.ContactPerson = _ContactPerson;
                    //    _MerchantDetails.CreateDate = Details.CreateDate;
                    //    _MerchantDetails.CreatedByReferenceId = Details.CreatedByReferenceId;
                    //    _MerchantDetails.CreatedByReferenceKey = Details.CreatedByReferenceKey;
                    //    _MerchantDetails.CreatedByDisplayName = Details.CreatedByDisplayName;
                    //    _MerchantDetails.ModifyDate = Details.ModifyDate;
                    //    _MerchantDetails.ModifyByReferenceId = Details.ModifyByReferenceId;
                    //    _MerchantDetails.ModifyByReferenceKey = Details.ModifyByReferenceKey;
                    //    _MerchantDetails.ModifyByDisplayName = Details.ModifyByDisplayName;
                    //    _MerchantDetails.StatusCode = Details.StatusCode;
                    //    _MerchantDetails.WebsiteUrl = Details.StatusName;
                    //    _MerchantDetails.StatusName = Details.StatusName;
                    //    _OAddress = new OAddress();
                    //    _OAddress.Address = Details.Address;
                    //    _OAddress.CityAreaCode = Details.CityAreaCode;
                    //    _OAddress.CityAreaName = Details.CityAreaName;
                    //    _OAddress.CityCode = Details.CityCode;
                    //    _OAddress.CityName = Details.CityName;
                    //    _OAddress.StateCode = Details.StateCode;
                    //    _OAddress.StateName = Details.StateName;
                    //    _OAddress.CountryCode = Details.CountryCode;
                    //    _OAddress.CountryName = Details.CountryName;
                    //    _OAddress.Latitude = Details.Latitude;
                    //    _OAddress.Longitude = Details.Longitude;
                    //    _MerchantDetails.Address = _OAddress;
                    //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _MerchantDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    //}
                    //else
                    //{
                    //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    //}

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetMerchant", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStore(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
                DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
                DateTime T7DayStart = TDayStart.AddDays(-7);
                DateTime T7DayEnd = TDayStart.AddSeconds(1);
                DateTime TDeadDayEnd = T7DayStart;

                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetStoreDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetStoreDownload>("ActorGetStoreDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }

                _Stores = new List<OAccounts.Store.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore)
                                                .Select(x => new OAccounts.Store.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    MerchantReferenceId = x.OwnerId,
                                                    MerchantReferenceKey = x.Owner.Guid,
                                                    MerchantDisplayName = x.Owner.DisplayName,
                                                    MerchantStatusId = x.Owner.StatusId,
                                                    MerchantStatusCode = x.Owner.Status.SystemName,
                                                    MerchantStatusName = x.Owner.Status.Name,
                                                    MerchantRewardPecentage = x.Owner.AccountPercentage,
                                                    DisplayName = x.DisplayName,
                                                    Address = x.Address,
                                                    CountryId = x.CountryId,
                                                    CountryKey = x.Country.Guid,
                                                    CountryName = x.Country.Name,
                                                    StateId = x.StateId,
                                                    StateKey = x.State.Guid,
                                                    StateName = x.State.Name,
                                                    CityId = x.CityId,
                                                    CityKey = x.City.Guid,
                                                    CityName = x.City.Name,
                                                    CityAreaId = x.CityAreaId,
                                                    CityAreaKey = x.CityArea.Guid,
                                                    CityAreaName = x.CityArea.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.Owner.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    //LastTransactionDate = x.HCUAccountTransactionSubParent.OrderByDescending(a=>a.TransactionDate).Select(a=>(DateTime?)a.TransactionDate).FirstOrDefault() ?? null,
                                                    //LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(a=>a.SubParentId == x.Id ).OrderByDescending(a=>a.TransactionDate).Select(a => (DateTime?)a.TransactionDate).FirstOrDefault() ?? null,
                                                    LastActivityDate = x.LastActivityDate,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    ActivityStatusCode = x.ActivityStatus.SystemName,
                                                    ActivityStatusName = x.ActivityStatus.Name,
                                                    //LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    Cashiers = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantCashier),
                                                    TotalTerminals = x.TUCTerminalStore.Count(),
                                                    ActiveTerminals = x.TUCTerminalStore.Count(m => m.StatusId == HelperStatus.Default.Active),
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Stores = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore)
                                                .Select(x => new OAccounts.Store.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    MerchantReferenceId = x.OwnerId,
                                                    MerchantReferenceKey = x.Owner.Guid,
                                                    MerchantDisplayName = x.Owner.DisplayName,
                                                    MerchantStatusId = x.Owner.StatusId,
                                                    MerchantStatusCode = x.Owner.Status.SystemName,
                                                    MerchantStatusName = x.Owner.Status.Name,
                                                    MerchantRewardPecentage = x.Owner.AccountPercentage,
                                                    Address = x.Address,
                                                    DisplayName = x.DisplayName,
                                                    CountryId = x.CountryId,
                                                    CountryKey = x.Country.Guid,
                                                    CountryName = x.Country.Name,
                                                    StateId = x.StateId,
                                                    StateKey = x.State.Guid,
                                                    StateName = x.State.Name,
                                                    CityId = x.CityId,
                                                    CityKey = x.City.Guid,
                                                    CityName = x.City.Name,
                                                    CityAreaId = x.CityAreaId,
                                                    CityAreaKey = x.CityArea.Guid,
                                                    CityAreaName = x.CityArea.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.Owner.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    //LastTransactionDate = x.HCUAccountTransactionSubParent.OrderByDescending(a=>a.TransactionDate).Select(a => (DateTime?)a.TransactionDate).FirstOrDefault() ?? null,
                                                    //LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(a=>a.SubParentId == x.Id ).OrderByDescending(a=>a.TransactionDate).Select(a => (DateTime?)a.TransactionDate).FirstOrDefault() ?? null,
                                                    LastActivityDate = x.LastActivityDate,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    ActivityStatusCode = x.ActivityStatus.SystemName,
                                                    ActivityStatusName = x.ActivityStatus.Name,
                                                    LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    Cashiers = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantCashier),
                                                    TotalTerminals = x.TUCTerminalStore.Count(),
                                                    ActiveTerminals = x.TUCTerminalStore.Count(m => m.StatusId == HelperStatus.Default.Active),
                                                    MobileNumber = x.MobileNumber
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object

                    foreach (var DataItem in _Stores)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                        DataItem.MerchantRewardPecentage = Convert.ToDouble(_HCoreContext.HCUAccountParameter.Where(y => y.AccountId == DataItem.MerchantReferenceId
                                                             && y.Common.SystemName == "rewardpercentage").OrderByDescending(x => x.CreateDate)
                                                        .Select(y => y.Value).FirstOrDefault());
                        DataItem.ContactNumber = string.IsNullOrEmpty(DataItem.ContactNumber) ? DataItem.MobileNumber : DataItem.ContactNumber;
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Stores, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }

            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetStore", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the store overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStoreOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _StoreOverview = new OAccounts.Store.Overview();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total 
                    _StoreOverview.Total = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore)
                                                .Select(x => new OAccounts.Store.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    MerchantReferenceId = x.OwnerId,
                                                    MerchantReferenceKey = x.Owner.Guid,
                                                    MerchantDisplayName = x.Owner.DisplayName,
                                                    MerchantRewardPecentage = x.Owner.AccountPercentage,
                                                    DisplayName = x.DisplayName,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.Owner.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    //LastTransactionDate = x.HCUAccountTransactionSubParent.OrderByDescending(a=>a.TransactionDate).Select(a=>(DateTime?)a.TransactionDate).FirstOrDefault() ?? null,
                                                    //LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(a=>a.SubParentId == x.Id ).OrderByDescending(a=>a.TransactionDate).Select(a => (DateTime?)a.TransactionDate).FirstOrDefault() ?? null,
                                                    LastActivityDate = x.LastActivityDate,
                                                    //LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    Cashiers = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantCashier),
                                                    TotalTerminals = x.TUCTerminalStore.Count(),
                                                    ActiveTerminals = x.TUCTerminalStore.Count(m => m.StatusId == HelperStatus.Default.Active),
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                    #endregion
                    #region Active 
                    _StoreOverview.Active = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore && x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new OAccounts.Store.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    MerchantReferenceId = x.OwnerId,
                                                    MerchantReferenceKey = x.Owner.Guid,
                                                    MerchantDisplayName = x.Owner.DisplayName,
                                                    MerchantRewardPecentage = x.Owner.AccountPercentage,
                                                    DisplayName = x.DisplayName,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.Owner.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    //LastTransactionDate = x.HCUAccountTransactionSubParent.OrderByDescending(a=>a.TransactionDate).Select(a=>(DateTime?)a.TransactionDate).FirstOrDefault() ?? null,
                                                    //LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(a=>a.SubParentId == x.Id ).OrderByDescending(a=>a.TransactionDate).Select(a => (DateTime?)a.TransactionDate).FirstOrDefault() ?? null,
                                                    LastActivityDate = x.LastActivityDate,
                                                    //LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    Cashiers = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantCashier),
                                                    TotalTerminals = x.TUCTerminalStore.Count(),
                                                    ActiveTerminals = x.TUCTerminalStore.Count(m => m.StatusId == HelperStatus.Default.Active),
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                    #endregion
                    #region Inactive 
                    _StoreOverview.Inactive = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore && x.StatusId == HelperStatus.Default.Inactive)
                                                .Select(x => new OAccounts.Store.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    MerchantReferenceId = x.OwnerId,
                                                    MerchantReferenceKey = x.Owner.Guid,
                                                    MerchantDisplayName = x.Owner.DisplayName,
                                                    MerchantRewardPecentage = x.Owner.AccountPercentage,
                                                    DisplayName = x.DisplayName,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.Owner.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    //LastTransactionDate = x.HCUAccountTransactionSubParent.OrderByDescending(a=>a.TransactionDate).Select(a=>(DateTime?)a.TransactionDate).FirstOrDefault() ?? null,
                                                    //LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(a=>a.SubParentId == x.Id ).OrderByDescending(a=>a.TransactionDate).Select(a => (DateTime?)a.TransactionDate).FirstOrDefault() ?? null,
                                                    LastActivityDate = x.LastActivityDate,
                                                    //LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    Cashiers = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantCashier),
                                                    TotalTerminals = x.TUCTerminalStore.Count(),
                                                    ActiveTerminals = x.TUCTerminalStore.Count(m => m.StatusId == HelperStatus.Default.Active),
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                    #endregion
                    #region Inactive 
                    _StoreOverview.Suspended = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore && x.StatusId == HelperStatus.Default.Suspended)
                                                .Select(x => new OAccounts.Store.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    MerchantReferenceId = x.OwnerId,
                                                    MerchantReferenceKey = x.Owner.Guid,
                                                    MerchantDisplayName = x.Owner.DisplayName,
                                                    MerchantRewardPecentage = x.Owner.AccountPercentage,
                                                    DisplayName = x.DisplayName,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.Owner.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    //LastTransactionDate = x.HCUAccountTransactionSubParent.OrderByDescending(a=>a.TransactionDate).Select(a=>(DateTime?)a.TransactionDate).FirstOrDefault() ?? null,
                                                    //LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(a=>a.SubParentId == x.Id ).OrderByDescending(a=>a.TransactionDate).Select(a => (DateTime?)a.TransactionDate).FirstOrDefault() ?? null,
                                                    LastActivityDate = x.LastActivityDate,
                                                    //LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    Cashiers = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantCashier),
                                                    TotalTerminals = x.TUCTerminalStore.Count(),
                                                    ActiveTerminals = x.TUCTerminalStore.Count(m => m.StatusId == HelperStatus.Default.Active),
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                    #endregion
                    #region Inactive 
                    _StoreOverview.Blocked = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore && x.StatusId == HelperStatus.Default.Blocked)
                                                .Select(x => new OAccounts.Store.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    MerchantReferenceId = x.OwnerId,
                                                    MerchantReferenceKey = x.Owner.Guid,
                                                    MerchantDisplayName = x.Owner.DisplayName,
                                                    MerchantRewardPecentage = x.Owner.AccountPercentage,
                                                    DisplayName = x.DisplayName,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.Owner.IconStorage.Path,
                                                    RmDisplayName = "ThankUCash",
                                                    //LastTransactionDate = x.HCUAccountTransactionSubParent.OrderByDescending(a=>a.TransactionDate).Select(a=>(DateTime?)a.TransactionDate).FirstOrDefault() ?? null,
                                                    //LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(a=>a.SubParentId == x.Id ).OrderByDescending(a=>a.TransactionDate).Select(a => (DateTime?)a.TransactionDate).FirstOrDefault() ?? null,
                                                    LastActivityDate = x.LastActivityDate,
                                                    //LastLoginDate = x.LastLoginDate,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    Cashiers = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantCashier),
                                                    TotalTerminals = x.TUCTerminalStore.Count(),
                                                    ActiveTerminals = x.TUCTerminalStore.Count(m => m.StatusId == HelperStatus.Default.Active),
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                    #endregion

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _StoreOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetStoreOverview", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the stores download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetStoresDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Limit = 800;
                _StoresDownload = new List<OAccounts.Store.ListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                            .Where(x => x.AccountTypeId == UserAccountType.MerchantStore)
                                            .Select(x => new OAccounts.Store.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                MerchantReferenceId = x.OwnerId,
                                                MerchantReferenceKey = x.Owner.Guid,
                                                MerchantDisplayName = x.Owner.DisplayName,
                                                DisplayName = x.DisplayName,
                                                ContactNumber = x.ContactNumber,
                                                EmailAddress = x.EmailAddress,
                                                IconUrl = x.Owner.IconStorage.Path,
                                                RmDisplayName = "ThankUCash",
                                                PrimaryCategoryKey = x.Owner.PrimaryCategory.Guid,
                                                PrimaryCategoryName = x.Owner.PrimaryCategory.Name,
                                                Address = x.Address,
                                                Latitude = x.Latitude,
                                                Longitude = x.Longitude,
                                                LastTransactionDate = x.LastTransactionDate,
                                                LastLoginDate = x.LastLoginDate,
                                                CreateDate = x.CreateDate,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,

                                                Cashiers = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantCashier),
                                                TotalTerminals = x.TUCTerminalStore.Count(),
                                                ActiveTerminals = x.TUCTerminalStore.Count(m => m.LastTransactionDate > ActivityDate),
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                         .Where(x => x.AccountTypeId == UserAccountType.MerchantStore)
                                                        .Select(x => new OAccounts.Store.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            MerchantReferenceId = x.OwnerId,
                                                            MerchantReferenceKey = x.Owner.Guid,
                                                            MerchantDisplayName = x.Owner.DisplayName,
                                                            DisplayName = x.DisplayName,
                                                            ContactNumber = x.ContactNumber,
                                                            EmailAddress = x.EmailAddress,
                                                            IconUrl = x.Owner.IconStorage.Path,
                                                            RmDisplayName = "ThankUCash",
                                                            PrimaryCategoryKey = x.Owner.PrimaryCategory.Guid,
                                                            PrimaryCategoryName = x.Owner.PrimaryCategory.Name,
                                                            Address = x.Address,
                                                            Latitude = x.Latitude,
                                                            Longitude = x.Longitude,
                                                            LastTransactionDate = x.LastTransactionDate,
                                                            LastLoginDate = x.LastLoginDate,
                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,

                                                            Cashiers = x.InverseOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantCashier),
                                                            TotalTerminals = x.TUCTerminalStore.Count(),
                                                            ActiveTerminals = x.TUCTerminalStore.Count(m => m.LastTransactionDate > ActivityDate),
                                                        })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            _StoresDownload.Add(new OAccounts.Store.ListDownload
                            {
                                ReferenceId = _DataItem.ReferenceId,
                                Merchant = _DataItem.MerchantDisplayName,
                                StoreName = _DataItem.DisplayName,
                                ContactNumber = _DataItem.ContactNumber,
                                EmailAddress = _DataItem.EmailAddress,
                                Address = _DataItem.Address,
                                Latitude = _DataItem.Latitude,
                                Longitude = _DataItem.Longitude,
                                ActiveTerminals = _DataItem.ActiveTerminals,
                                TotalTerminals = _DataItem.TotalTerminals,
                                Cashiers = _DataItem.Cashiers,
                                RelationshipManager = _DataItem.RmDisplayName,
                                LastTransactionDate = _DataItem.LastTransactionDate,
                                PrimaryCategory = _DataItem.PrimaryCategoryName,
                                AddedOn = _DataItem.CreateDate,
                                Status = _DataItem.StatusName,
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("Stores_List", _StoresDownload, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetStoresDownload", _Exception);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the store location.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStoreLocation(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _StoreLocations = new List<OAccounts.Store.Location>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore)
                                                .Select(x => new OAccounts.Store.Location
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    MerchantReferenceId = x.OwnerId,
                                                    MerchantDisplayName = x.Owner.DisplayName,
                                                    IconUrl = x.Owner.IconStorage.Path,
                                                    DisplayName = x.DisplayName,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    Latitude = x.Latitude,
                                                    Longitude = x.Longitude,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _StoreLocations = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore)
                                                .Select(x => new OAccounts.Store.Location
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    MerchantReferenceId = x.OwnerId,
                                                    MerchantDisplayName = x.Owner.DisplayName,
                                                    IconUrl = x.Owner.IconStorage.Path,
                                                    DisplayName = x.DisplayName,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    Latitude = x.Latitude,
                                                    Longitude = x.Longitude,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _StoreLocations)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _StoreLocations, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetStore", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the cashiers.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCashiers(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetCashierDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetCashierDownload>("ActorGetCashierDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Cashiers = new List<OAccounts.Cashier.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.Owner.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier)
                                                .Select(x => new OAccounts.Cashier.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CashierId = x.DisplayName,
                                                    EmployeeId = x.ReferralCode,
                                                    Name = x.Name,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    GenderCode = x.Gender.SystemName,
                                                    GenderName = x.Gender.Name,
                                                    IconUrl = x.Owner.Owner.IconStorage.Path,
                                                    MerchantReferenceId = x.Owner.Owner.Id,
                                                    MerchantDisplayName = x.Owner.Owner.DisplayName,
                                                    MerchantReferenceKey = x.Owner.Owner.Guid,
                                                    StoreReferenceId = x.Owner.Id,
                                                    StoreReferenceKey = x.Owner.Guid,
                                                    StoreDisplayName = x.Owner.DisplayName,
                                                    StoreAddress = x.Address,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    LastActivityDate = x.LastActivityDate,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    //Transactions = x.TUCSaleCashier.Count,
                                                    SaleAmount = x.HCUAccountTransactionCashier.Where(x => x.StatusId == HelperStatus.Transaction.Success
                                                                                                && (x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit)
                                                                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)).Sum(m => m.PurchaseAmount),
                                                    Transactions = x.HCUAccountTransactionCashier.Where(x => x.StatusId == HelperStatus.Transaction.Success
                                                                                                 && (x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit)
                                                                                                 && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                                                                 && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)).Count(),
                                                    MainBalance = 0,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Cashiers = _HCoreContext.HCUAccount
                                                .Where(x => x.Owner.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier)
                                                .Select(x => new OAccounts.Cashier.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CashierId = x.DisplayName,
                                                    EmployeeId = x.ReferralCode,
                                                    Name = x.Name,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    GenderCode = x.Gender.SystemName,
                                                    GenderName = x.Gender.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    MerchantReferenceId = x.Owner.Owner.Id,
                                                    MerchantDisplayName = x.Owner.Owner.DisplayName,
                                                    MerchantReferenceKey = x.Owner.Owner.Guid,
                                                    StoreReferenceId = x.Owner.Id,
                                                    StoreReferenceKey = x.Owner.Guid,
                                                    StoreDisplayName = x.Owner.DisplayName,
                                                    StoreAddress = x.Address,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    LastActivityDate = x.LastActivityDate,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    //Transactions = x.TUCSaleCashier.Count,
                                                    SaleAmount = x.HCUAccountTransactionCashier.Where(x => x.StatusId == HelperStatus.Transaction.Success
                                                                                                && (x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit)
                                                                                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                                                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)).Sum(m => m.PurchaseAmount),
                                                    Transactions = x.HCUAccountTransactionCashier.Where(x => x.StatusId == HelperStatus.Transaction.Success
                                                                                                 && (x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit)
                                                                                                 && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus || x.SourceId == TransactionSource.TUCBlack)
                                                                                                 && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)).Count(),
                                                    MainBalance = 0,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Cashiers)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Cashiers, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCashiers", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the cashiers overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCashiersOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _CashiersOverview = new OAccounts.Cashier.Overview();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _CashiersOverview.Total = _HCoreContext.HCUAccount
                                                .Where(x => x.Owner.CountryId == _Request.UserReference.SystemCountry)
                                             .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier)
                                                .Select(x => new OAccounts.Cashier.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CashierId = x.DisplayName,
                                                    EmployeeId = x.ReferralCode,
                                                    Name = x.Name,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    GenderCode = x.Gender.SystemName,
                                                    GenderName = x.Gender.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    StoreReferenceId = x.Owner.Id,
                                                    StoreReferenceKey = x.Owner.Guid,
                                                    StoreDisplayName = x.Owner.DisplayName,
                                                    StoreAddress = x.Address,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    LastActivityDate = x.LastActivityDate,
                                                })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    _CashiersOverview.Active = _HCoreContext.HCUAccount
                                                .Where(x => x.Owner.CountryId == _Request.UserReference.SystemCountry)
                                            .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier && x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new OAccounts.Cashier.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CashierId = x.DisplayName,
                                                    EmployeeId = x.ReferralCode,
                                                    Name = x.Name,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    GenderCode = x.Gender.SystemName,
                                                    GenderName = x.Gender.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    StoreReferenceId = x.Owner.Id,
                                                    StoreReferenceKey = x.Owner.Guid,
                                                    StoreDisplayName = x.Owner.DisplayName,
                                                    StoreAddress = x.Address,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    LastActivityDate = x.LastActivityDate,
                                                })
                                          .Where(_Request.SearchCondition)
                                  .Count();
                    _CashiersOverview.Blocked = _HCoreContext.HCUAccount
                                                .Where(x => x.Owner.CountryId == _Request.UserReference.SystemCountry)
                                         .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier && x.StatusId == HelperStatus.Default.Blocked)
                                         .Select(x => new OAccounts.Cashier.List
                                         {
                                             ReferenceId = x.Id,
                                             ReferenceKey = x.Guid,
                                             CashierId = x.DisplayName,
                                             EmployeeId = x.ReferralCode,
                                             Name = x.Name,
                                             EmailAddress = x.EmailAddress,
                                             ContactNumber = x.ContactNumber,
                                             GenderCode = x.Gender.SystemName,
                                             GenderName = x.Gender.Name,
                                             IconUrl = x.IconStorage.Path,
                                             StoreReferenceId = x.Owner.Id,
                                             StoreReferenceKey = x.Owner.Guid,
                                             StoreDisplayName = x.Owner.DisplayName,
                                             StoreAddress = x.Address,
                                             CreateDate = x.CreateDate,
                                             StatusCode = x.Status.SystemName,
                                             StatusName = x.Status.Name,
                                             LastActivityDate = x.LastActivityDate,
                                         })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CashiersOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetCashiers", _Exception, _Request.UserReference, _CashiersOverview, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the cashiers download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetCashiersDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Limit = 800;
                _CashiersDownload = new List<OAccounts.Cashier.ListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.Owner.CountryId == _Request.UserReference.SystemCountry)
                            .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier)
                                                .Select(x => new OAccounts.Cashier.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CashierId = x.DisplayName,
                                                    EmployeeId = x.ReferralCode,
                                                    Name = x.Name,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    GenderCode = x.Gender.SystemName,
                                                    GenderName = x.Gender.Name,
                                                    IconUrl = x.Owner.Owner.IconStorage.Path,
                                                    MerchantReferenceId = x.Owner.Owner.Id,
                                                    MerchantDisplayName = x.Owner.Owner.DisplayName,
                                                    MerchantReferenceKey = x.Owner.Owner.Guid,
                                                    StoreReferenceId = x.Owner.Id,
                                                    StoreReferenceKey = x.Owner.Guid,
                                                    StoreDisplayName = x.Owner.DisplayName,
                                                    StoreAddress = x.Address,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    LastActivityDate = x.LastActivityDate,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    Transactions = x.cmt_salecashier.Count,
                                                })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = _HCoreContext.HCUAccount
                                                .Where(x => x.Owner.CountryId == _Request.UserReference.SystemCountry)
                            .Where(x => x.AccountTypeId == UserAccountType.MerchantCashier)
                                                .Select(x => new OAccounts.Cashier.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CashierId = x.DisplayName,
                                                    EmployeeId = x.ReferralCode,
                                                    Name = x.Name,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    GenderCode = x.Gender.SystemName,
                                                    GenderName = x.Gender.Name,
                                                    IconUrl = x.Owner.Owner.IconStorage.Path,
                                                    MerchantReferenceId = x.Owner.Owner.Id,
                                                    MerchantDisplayName = x.Owner.Owner.DisplayName,
                                                    MerchantReferenceKey = x.Owner.Owner.Guid,
                                                    StoreReferenceId = x.Owner.Id,
                                                    StoreReferenceKey = x.Owner.Guid,
                                                    StoreDisplayName = x.Owner.DisplayName,
                                                    StoreAddress = x.Address,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    LastActivityDate = x.LastActivityDate,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    Transactions = x.cmt_salecashier.Count,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            _CashiersDownload.Add(new OAccounts.Cashier.ListDownload
                            {
                                ReferenceId = _DataItem.ReferenceId,
                                CashierId = _DataItem.CashierId,
                                Name = _DataItem.Name,
                                GenderName = _DataItem.GenderName,
                                EmployeeId = _DataItem.EmployeeId,
                                ContactNumber = _DataItem.ContactNumber,
                                EmailAddress = _DataItem.EmailAddress,
                                Store = _DataItem.StoreDisplayName,
                                StoreAddress = _DataItem.StoreAddress,
                                Merchant = _DataItem.MerchantDisplayName,
                                Transactions = _DataItem.Transactions,
                                LastTransactionDate = _DataItem.LastTransactionDate,
                                AddedOn = _DataItem.CreateDate,
                                Status = _DataItem.StatusName,
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("Cashiers_List", _CashiersDownload, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCashiersDownload", _Exception);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the cashier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCashier(OAccounts.Cashier.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.HCUAccount
                                                .Where(x => x.Owner.CountryId == _Request.UserReference.SystemCountry)
                        .Where(x => x.Guid == _Request.AccountKey && x.Id == _Request.AccountId && x.AccountTypeId == UserAccountType.MerchantCashier)
                        .Select(x => new
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            DisplayName = x.DisplayName,
                            Name = x.DisplayName,
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            MobileNumber = x.MobileNumber,
                            ContactNumber = x.ContactNumber,
                            EmailAddress = x.EmailAddress,
                            IconUrl = x.Owner.Owner.IconStorage.Path,
                            Address = x.Address,
                            Latitude = x.Latitude,
                            Longitude = x.Longitude,
                            CityAreaCode = x.CityArea.SystemName,
                            CityAreaName = x.CityArea.Name,
                            CityCode = x.City.SystemName,
                            CityName = x.City.Name,
                            StateCode = x.State.SystemName,
                            StateName = x.State.Name,
                            CountryCode = x.Country.SystemName,
                            CountryName = x.Country.Name,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                            EmployeeId = x.ReferralCode,
                            GenderCode = x.Gender.SystemName,
                            GenderName = x.Gender.Name,
                            StoreName = x.Owner.DisplayName,
                            StoreAddress = x.Owner.Address,
                            StoreId = x.Owner.Id,
                            StoreKey = x.Owner.Guid,

                            MerchantId = x.Owner.Owner.Id,
                            MerchantKey = x.Owner.Owner.Guid,
                            MerchantDisplayName = x.Owner.Owner.DisplayName,

                            CreateDate = x.CreateDate,
                            CreatedByReferenceId = x.CreatedById,
                            CreatedByReferenceKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyByReferenceId = x.ModifyById,
                            ModifyByReferenceKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,
                        }).FirstOrDefault();
                    if (Details != null)
                    {
                        _CashierDetails = new OAccounts.Cashier.Details();
                        _CashierDetails.ReferenceId = Details.ReferenceId;
                        _CashierDetails.ReferenceKey = Details.ReferenceKey;
                        _CashierDetails.CashierId = Details.DisplayName;
                        _CashierDetails.Name = Details.Name;
                        _CashierDetails.FirstName = Details.FirstName;
                        _CashierDetails.LastName = Details.LastName;
                        _CashierDetails.EmailAddress = Details.EmailAddress;
                        _CashierDetails.ContactNumber = Details.ContactNumber;
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            _CashierDetails.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            _CashierDetails.IconUrl = _AppConfig.Default_Icon;
                        }
                        _CashierDetails.GenderCode = Details.GenderCode;
                        _CashierDetails.GenderName = Details.GenderName;
                        _CashierDetails.EmployeeId = Details.EmployeeId;

                        _CashierDetails.StoreReferenceId = Details.StoreId;
                        _CashierDetails.StoreReferenceKey = Details.StoreKey;
                        _CashierDetails.StoreDisplayName = Details.StoreName;
                        _CashierDetails.StoreAddress = Details.StoreAddress;

                        _CashierDetails.MerchantReferenceId = Details.MerchantId;
                        _CashierDetails.MerchantReferenceKey = Details.MerchantKey;
                        _CashierDetails.MerchantDisplayName = Details.MerchantDisplayName;


                        _CashierDetails.StatusCode = Details.StatusCode;
                        _CashierDetails.StatusName = Details.StatusName;

                        _CashierDetails.CreateDate = Details.CreateDate;
                        _CashierDetails.CreatedByReferenceId = Details.CreatedByReferenceId;
                        _CashierDetails.CreatedByReferenceKey = Details.CreatedByReferenceKey;
                        _CashierDetails.CreatedByDisplayName = Details.CreatedByDisplayName;

                        _CashierDetails.ModifyDate = Details.ModifyDate;
                        _CashierDetails.ModifyByReferenceId = Details.ModifyByReferenceId;
                        _CashierDetails.ModifyByReferenceKey = Details.ModifyByReferenceKey;
                        _CashierDetails.ModifyByDisplayName = Details.ModifyByDisplayName;

                        _OAddress = new OAddress();
                        _OAddress.Address = Details.Address;
                        _OAddress.CityAreaCode = Details.CityAreaCode;
                        _OAddress.CityAreaName = Details.CityAreaName;
                        _OAddress.CityCode = Details.CityCode;
                        _OAddress.CityName = Details.CityName;
                        _OAddress.StateCode = Details.StateCode;
                        _OAddress.StateName = Details.StateName;
                        _OAddress.CountryCode = Details.CountryCode;
                        _OAddress.CountryName = Details.CountryName;
                        _OAddress.Latitude = Details.Latitude;
                        _OAddress.Longitude = Details.Longitude;
                        _CashierDetails.Address = _OAddress;
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CashierDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCashier", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the terminal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTerminal(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
                DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
                DateTime T7DayStart = TDayStart.AddDays(-7);
                DateTime T7DayEnd = TDayStart.AddSeconds(1);
                DateTime TDeadDayEnd = T7DayStart;
                _Terminals = new List<OAccounts.Terminal.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.Type == "active")
                    {
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                .Where(x => x.Provider.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd))
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,
                                                        IconUrl = x.Provider.IconStorage.Path,

                                                        ProviderReferenceId = x.ProviderId,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,

                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,



                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        CreateDate = x.CreateDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RmDisplayName = "ThankUCash"
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                        }
                        #region Get Data
                        _Terminals = _HCoreContext.TUCTerminal
                                                .Where(x => x.Provider.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd))
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,
                                                        IconUrl = x.Provider.IconStorage.Path,

                                                        ProviderReferenceId = x.ProviderId,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,

                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,


                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        CreateDate = x.CreateDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RmDisplayName = "ThankUCash"
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                    }
                    else if (_Request.Type == "idle")
                    {
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                .Where(x => x.Provider.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd))
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,
                                                        IconUrl = x.Provider.IconStorage.Path,

                                                        ProviderReferenceId = x.ProviderId,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,

                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,

                                                        CreateDate = x.CreateDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RmDisplayName = "ThankUCash"
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                        }
                        #region Get Data
                        _Terminals = _HCoreContext.TUCTerminal
                                                .Where(x => x.Provider.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd))
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,
                                                        IconUrl = x.Provider.IconStorage.Path,

                                                        ProviderReferenceId = x.ProviderId,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,


                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,

                                                        CreateDate = x.CreateDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RmDisplayName = "ThankUCash"
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                    }
                    else if (_Request.Type == "dead")
                    {
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                .Where(x => x.Provider.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate < TDeadDayEnd))
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,
                                                        IconUrl = x.Provider.IconStorage.Path,

                                                        ProviderReferenceId = x.ProviderId,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,


                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,

                                                        CreateDate = x.CreateDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RmDisplayName = "ThankUCash"
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                        }
                        #region Get Data
                        _Terminals = _HCoreContext.TUCTerminal
                                                .Where(x => x.Provider.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate < TDeadDayEnd))
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,
                                                        IconUrl = x.Provider.IconStorage.Path,

                                                        ProviderReferenceId = x.ProviderId,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,


                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,

                                                        CreateDate = x.CreateDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RmDisplayName = "ThankUCash"
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                    }
                    else if (_Request.Type == "unused")
                    {
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                .Where(x => x.Provider.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.HCUAccountTransaction.Any() == false)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,
                                                        IconUrl = x.Provider.IconStorage.Path,

                                                        ProviderReferenceId = x.ProviderId,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,


                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,

                                                        CreateDate = x.CreateDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RmDisplayName = "ThankUCash"
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                        }
                        #region Get Data
                        _Terminals = _HCoreContext.TUCTerminal
                                                .Where(x => x.Provider.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.HCUAccountTransaction.Any() == false)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,
                                                        IconUrl = x.Provider.IconStorage.Path,

                                                        ProviderReferenceId = x.ProviderId,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,


                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,

                                                        CreateDate = x.CreateDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RmDisplayName = "ThankUCash"
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                .Where(x => x.Provider.CountryId == _Request.UserReference.CountryId)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,
                                                        IconUrl = x.Provider.IconStorage.Path,

                                                        ProviderReferenceId = x.ProviderId,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,


                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,

                                                        CreateDate = x.CreateDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RmDisplayName = "ThankUCash"
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        _Terminals = _HCoreContext.TUCTerminal
                                                .Where(x => x.Provider.CountryId == _Request.UserReference.CountryId)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        //TypeCode = "POS",
                                                        //TypeName = "POS",
                                                        TerminalId = x.IdentificationNumber,
                                                        SerialNumber = x.SerialNumber,
                                                        IconUrl = x.Provider.IconStorage.Path,

                                                        ProviderReferenceId = x.ProviderId,
                                                        ProviderReferenceKey = x.Provider.Guid,
                                                        ProviderDisplayName = x.Provider.DisplayName,

                                                        AcquirerReferenceId = x.Acquirer.Id,
                                                        AcquirerReferenceKey = x.Acquirer.Guid,
                                                        AcquirerDisplayName = x.Acquirer.DisplayName,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        MerchantReferenceId = x.MerchantId,
                                                        MerchantReferenceKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,


                                                        StoreReferenceId = x.Store.Id,
                                                        StoreReferenceKey = x.Store.Guid,
                                                        StoreDisplayName = x.Store.DisplayName,
                                                        StoreAddress = x.Store.Address,

                                                        CreateDate = x.CreateDate,
                                                        LastTransactionDate = x.LastTransactionDate,

                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        RmDisplayName = "ThankUCash"
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                    }

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Terminals)
                    {
                        if (DataItem.LastTransactionDate > TDayStart && DataItem.LastTransactionDate < TDayEnd)
                        {
                            DataItem.ActivityStatusCode = "active";
                            DataItem.ActivityStatusName = "active";
                        }
                        else if (DataItem.LastTransactionDate > T7DayStart && DataItem.LastTransactionDate < T7DayEnd)
                        {
                            DataItem.ActivityStatusCode = "idle";
                            DataItem.ActivityStatusName = "idle";
                        }
                        else if (DataItem.LastTransactionDate < TDeadDayEnd)
                        {
                            DataItem.ActivityStatusCode = "dead";
                            DataItem.ActivityStatusName = "dead";
                        }
                        else if (DataItem.LastTransactionDate == null)
                        {
                            DataItem.ActivityStatusCode = "unused";
                            DataItem.ActivityStatusName = "unused";
                        }

                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Terminals, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetTerminals", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the terminals overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTerminalsOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
                DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
                DateTime T7DayStart = TDayStart.AddDays(-7);
                DateTime T7DayEnd = TDayStart.AddSeconds(1);
                DateTime TDeadDayEnd = T7DayStart;
                _TerminalOverview = new OAccounts.Terminal.Overview();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _TerminalOverview.Total = _HCoreContext.TUCTerminal
                                                .Where(x => x.Provider.CountryId == _Request.UserReference.SystemCountry)
                                            .Select(x => new OAccounts.Terminal.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                //TypeCode = "POS",
                                                //TypeName = "POS",
                                                TerminalId = x.IdentificationNumber,
                                                SerialNumber = x.SerialNumber,

                                                ProviderReferenceId = x.ProviderId,
                                                ProviderReferenceKey = x.Provider.Guid,
                                                ProviderDisplayName = x.Provider.DisplayName,

                                                AcquirerReferenceId = x.Acquirer.Id,
                                                AcquirerReferenceKey = x.Acquirer.Guid,
                                                AcquirerDisplayName = x.Acquirer.DisplayName,
                                                IconUrl = x.Acquirer.IconStorage.Path,

                                                MerchantReferenceId = x.Merchant.Id,
                                                MerchantReferenceKey = x.Merchant.Guid,
                                                MerchantDisplayName = x.Merchant.DisplayName,


                                                TypeId = x.TypeId,
                                                TypeCode = x.Type.SystemName,
                                                TypeName = x.Type.Name,

                                                StoreReferenceId = x.Store.Id,
                                                StoreReferenceKey = x.Store.Guid,
                                                StoreDisplayName = x.Store.DisplayName,
                                                StoreAddress = x.Store.Address,

                                                CreateDate = x.CreateDate,
                                                LastTransactionDate = x.LastTransactionDate,

                                                ActivityStatusCode = x.ActivityStatus.SystemName,
                                                ActivityStatusName = x.ActivityStatus.Name,

                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,
                                                RmDisplayName = "ThankUCash"
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    #region Unused Records
                    _TerminalOverview.Unused = _HCoreContext.TUCTerminal
                                                .Where(x => x.Provider.CountryId == _Request.UserReference.SystemCountry)
                                            .Where(x => x.HCUAccountTransaction.Any() == false)
                                            //.Where(x => x.StatusId == HelperStatus.Default.Inactive)
                                            .Select(x => new OAccounts.Terminal.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                //TypeCode = "POS",
                                                //TypeName = "POS",
                                                TerminalId = x.IdentificationNumber,
                                                SerialNumber = x.SerialNumber,

                                                ProviderReferenceId = x.ProviderId,
                                                ProviderReferenceKey = x.Provider.Guid,
                                                ProviderDisplayName = x.Provider.DisplayName,

                                                AcquirerReferenceId = x.Acquirer.Id,
                                                AcquirerReferenceKey = x.Acquirer.Guid,
                                                AcquirerDisplayName = x.Acquirer.DisplayName,
                                                IconUrl = x.Acquirer.IconStorage.Path,

                                                MerchantReferenceId = x.MerchantId,
                                                MerchantReferenceKey = x.Merchant.Guid,
                                                MerchantDisplayName = x.Merchant.DisplayName,

                                                TypeId = x.TypeId,
                                                TypeCode = x.Type.SystemName,
                                                TypeName = x.Type.Name,


                                                StoreReferenceId = x.Store.Id,
                                                StoreReferenceKey = x.Store.Guid,
                                                StoreDisplayName = x.Store.DisplayName,
                                                StoreAddress = x.Store.Address,

                                                CreateDate = x.CreateDate,
                                                LastTransactionDate = x.LastTransactionDate,

                                                ActivityStatusCode = x.ActivityStatus.SystemName,
                                                ActivityStatusName = x.ActivityStatus.Name,

                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,
                                                RmDisplayName = "ThankUCash"
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    #region Active Records
                    _TerminalOverview.Active = _HCoreContext.TUCTerminal
                                                .Where(x => x.Provider.CountryId == _Request.UserReference.SystemCountry)
                                            //.Where(x => x.StatusId == HelperStatus.Default.Active)
                                            .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd))
                                            .Select(x => new OAccounts.Terminal.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                //TypeCode = "POS",
                                                //TypeName = "POS",
                                                TerminalId = x.IdentificationNumber,
                                                SerialNumber = x.SerialNumber,

                                                ProviderReferenceId = x.ProviderId,
                                                ProviderReferenceKey = x.Provider.Guid,
                                                ProviderDisplayName = x.Provider.DisplayName,

                                                AcquirerReferenceId = x.Acquirer.Id,
                                                AcquirerReferenceKey = x.Acquirer.Guid,
                                                AcquirerDisplayName = x.Acquirer.DisplayName,
                                                IconUrl = x.Acquirer.IconStorage.Path,

                                                MerchantReferenceId = x.MerchantId,
                                                MerchantReferenceKey = x.Merchant.Guid,
                                                MerchantDisplayName = x.Merchant.DisplayName,

                                                TypeId = x.TypeId,
                                                TypeCode = x.Type.SystemName,
                                                TypeName = x.Type.Name,


                                                StoreReferenceId = x.Store.Id,
                                                StoreReferenceKey = x.Store.Guid,
                                                StoreDisplayName = x.Store.DisplayName,
                                                StoreAddress = x.Store.Address,

                                                CreateDate = x.CreateDate,
                                                LastTransactionDate = x.LastTransactionDate,

                                                ActivityStatusCode = x.ActivityStatus.SystemName,
                                                ActivityStatusName = x.ActivityStatus.Name,

                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,
                                                RmDisplayName = "ThankUCash"
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    #region Idle Records
                    _TerminalOverview.Idle = _HCoreContext.TUCTerminal
                                                .Where(x => x.Provider.CountryId == _Request.UserReference.SystemCountry)
                                            //.Where(x => x.StatusId == HelperStatus.Default.Suspended)
                                            .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd))
                                            .Select(x => new OAccounts.Terminal.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                //TypeCode = "POS",
                                                //TypeName = "POS",
                                                TerminalId = x.IdentificationNumber,
                                                SerialNumber = x.SerialNumber,

                                                ProviderReferenceId = x.ProviderId,
                                                ProviderReferenceKey = x.Provider.Guid,
                                                ProviderDisplayName = x.Provider.DisplayName,

                                                AcquirerReferenceId = x.Acquirer.Id,
                                                AcquirerReferenceKey = x.Acquirer.Guid,
                                                AcquirerDisplayName = x.Acquirer.DisplayName,
                                                IconUrl = x.Acquirer.IconStorage.Path,

                                                MerchantReferenceId = x.MerchantId,
                                                MerchantReferenceKey = x.Merchant.Guid,
                                                MerchantDisplayName = x.Merchant.DisplayName,

                                                TypeId = x.TypeId,
                                                TypeCode = x.Type.SystemName,
                                                TypeName = x.Type.Name,

                                                StoreReferenceId = x.Store.Id,
                                                StoreReferenceKey = x.Store.Guid,
                                                StoreDisplayName = x.Store.DisplayName,
                                                StoreAddress = x.Store.Address,

                                                CreateDate = x.CreateDate,
                                                LastTransactionDate = x.LastTransactionDate,

                                                ActivityStatusCode = x.ActivityStatus.SystemName,
                                                ActivityStatusName = x.ActivityStatus.Name,

                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,
                                                RmDisplayName = "ThankUCash"
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    #region Dead Records
                    _TerminalOverview.Dead = _HCoreContext.TUCTerminal
                                                .Where(x => x.Provider.CountryId == _Request.UserReference.SystemCountry)
                                           //.Where(x => x.StatusId == HelperStatus.Default.Blocked)
                                           .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate < TDeadDayEnd))
                                            .Select(x => new OAccounts.Terminal.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                //TypeCode = "POS",
                                                //TypeName = "POS",
                                                TerminalId = x.IdentificationNumber,
                                                SerialNumber = x.SerialNumber,

                                                ProviderReferenceId = x.ProviderId,
                                                ProviderReferenceKey = x.Provider.Guid,
                                                ProviderDisplayName = x.Provider.DisplayName,

                                                AcquirerReferenceId = x.Acquirer.Id,
                                                AcquirerReferenceKey = x.Acquirer.Guid,
                                                AcquirerDisplayName = x.Acquirer.DisplayName,
                                                IconUrl = x.Acquirer.IconStorage.Path,

                                                MerchantReferenceId = x.MerchantId,
                                                MerchantReferenceKey = x.Merchant.Guid,
                                                MerchantDisplayName = x.Merchant.DisplayName,

                                                TypeId = x.TypeId,
                                                TypeCode = x.Type.SystemName,
                                                TypeName = x.Type.Name,


                                                StoreReferenceId = x.Store.Id,
                                                StoreReferenceKey = x.Store.Guid,
                                                StoreDisplayName = x.Store.DisplayName,
                                                StoreAddress = x.Store.Address,

                                                CreateDate = x.CreateDate,
                                                LastTransactionDate = x.LastTransactionDate,

                                                ActivityStatusCode = x.ActivityStatus.SystemName,
                                                ActivityStatusName = x.ActivityStatus.Name,

                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,
                                                RmDisplayName = "ThankUCash"
                                            })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _TerminalOverview, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetTerminalsOverview", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the group merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetGroupMerchantList(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
                DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
                DateTime T7DayStart = TDayStart.AddDays(-7);
                DateTime T7DayEnd = TDayStart.AddSeconds(1);
                DateTime TDeadDayEnd = T7DayStart;
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetMerchantDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetMerchantDownload>("ActorGetMerchantDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Merchants = new List<OAccounts.Merchant.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant)
                                                .Where(x => !x.TUCLProgramGroupAccount.Any(y => y.ProgramId == _Request.ProgramId
                                                                        && y.Program.StatusId == HelperStatus.Default.Active
                                                                        && y.StatusId == HelperStatus.Default.Active))
                                                .Select(x => new OAccounts.Merchant.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Merchants = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant)
                                                .Where(x => !x.TUCLProgramGroupAccount.Any(y => y.ProgramId == _Request.ProgramId
                                                                        && y.Program.StatusId == HelperStatus.Default.Active
                                                                        && y.StatusId == HelperStatus.Default.Active))
                                                .Select(x => new OAccounts.Merchant.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.DisplayName,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    CreateDate = x.CreateDate
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object

                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Merchants, _Request.Offset, _Request.Limit);
                    _HCoreContext.Dispose();
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetGroupMerchantList", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        //internal OResponse GetTerminal(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        _Terminals = new List<OAccounts.Terminal.List>();
        //        if (string.IsNullOrEmpty(_Request.SearchCondition))
        //        {
        //            HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
        //        }
        //        if (string.IsNullOrEmpty(_Request.SortExpression))
        //        {
        //            HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
        //        }
        //        if (_Request.Limit < 1)
        //        {
        //            _Request.Limit = _AppConfig.DefaultRecordsLimit;
        //        }
        //        DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            if (_Request.RefreshCount)
        //            {
        //                #region Total Records
        //                _Request.TotalRecords = _HCoreContext.HCUAccount
        //                                        .Where(x => x.AccountTypeId == UserAccountType.TerminalAccount)
        //                                        .Select(x => new OAccounts.Terminal.List
        //                                        {
        //                                            ReferenceId = x.Id,
        //                                            ReferenceKey = x.Guid,

        //                                            TerminalId = x.DisplayName,
        //                                            SerialNumber = x.ReferralCode,

        //                                            ProviderReferenceId = x.OwnerId,
        //                                            ProviderReferenceKey = x.Owner.Guid,
        //                                            ProviderDisplayName = x.Owner.DisplayName,

        //                                            StoreReferenceId = x.SubOwnerId,
        //                                            StoreReferenceKey = x.SubOwner.Guid,
        //                                            StoreDisplayName = x.SubOwner.DisplayName,

        //                                            MerchantReferenceId = x.SubOwner.OwnerId,
        //                                            MerchantReferenceKey = x.SubOwner.Owner.Guid,
        //                                            MerchantDisplayName = x.SubOwner.Owner.DisplayName,

        //                                            AcquirerReferenceId = x.BankId,
        //                                            AcquirerReferenceKey = x.Bank.Guid,
        //                                            AcquirerDisplayName = x.Bank.DisplayName,

        //                                            IconUrl = x.Bank.IconStorage.Path,
        //                                            RmDisplayName = "ThankUCash",
        //                                            LastTransactionDate = x.LastTransactionDate,
        //                                            CreateDate = x.CreateDate,
        //                                            StatusCode = x.Status.SystemName,
        //                                            StatusName = x.Status.Name,
        //                                        })
        //                                       .Where(_Request.SearchCondition)
        //                               .Count();
        //                #endregion
        //            }
        //            #region Get Data
        //            _Terminals = _HCoreContext.HCUAccount
        //                                        .Where(x => x.AccountTypeId == UserAccountType.TerminalAccount)
        //                                        .Select(x => new OAccounts.Terminal.List
        //                                        {

        //                                            ReferenceId = x.Id,
        //                                            ReferenceKey = x.Guid,

        //                                            TerminalId = x.DisplayName,
        //                                            SerialNumber = x.ReferralCode,

        //                                            ProviderReferenceId = x.OwnerId,
        //                                            ProviderReferenceKey = x.Owner.Guid,
        //                                            ProviderDisplayName = x.Owner.DisplayName,

        //                                            StoreReferenceId = x.SubOwnerId,
        //                                            StoreReferenceKey = x.SubOwner.Guid,
        //                                            StoreDisplayName = x.SubOwner.DisplayName,

        //                                            MerchantReferenceId = x.SubOwner.OwnerId,
        //                                            MerchantReferenceKey = x.SubOwner.Owner.Guid,
        //                                            MerchantDisplayName = x.SubOwner.Owner.DisplayName,

        //                                            AcquirerReferenceId = x.BankId,
        //                                            AcquirerReferenceKey = x.Bank.Guid,
        //                                            AcquirerDisplayName = x.Bank.DisplayName,

        //                                            IconUrl = x.Bank.IconStorage.Path,
        //                                            RmDisplayName = "ThankUCash",
        //                                            LastTransactionDate = x.LastTransactionDate,
        //                                            CreateDate = x.CreateDate,
        //                                            StatusCode = x.Status.SystemName,
        //                                            StatusName = x.Status.Name,
        //                                        })
        //                                     .Where(_Request.SearchCondition)
        //                                     .OrderBy(_Request.SortExpression)
        //                                     .Skip(_Request.Offset)
        //                                     .Take(_Request.Limit)
        //                                     .ToList();
        //            #endregion
        //            #region Create  Response Object
        //            _HCoreContext.Dispose();
        //            foreach (var DataItem in _Terminals)
        //            {
        //                if (!string.IsNullOrEmpty(DataItem.IconUrl))
        //                {
        //                    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
        //                }
        //                else
        //                {
        //                    DataItem.IconUrl = _AppConfig.Default_Icon;
        //                }
        //            }
        //            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Terminals, _Request.Offset, _Request.Limit);
        //            #endregion
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //            #endregion
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        return HCoreHelper.LogException("GetTerminals", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
        //    }
        //    #endregion
        //}
    }
    internal class ActorGetStoreDownload : ReceiveActor
    {
        public ActorGetStoreDownload()
        {
            FrameworkAccounts _FrameworkAccounts;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkAccounts = new FrameworkAccounts();
                _FrameworkAccounts.GetStoresDownload(_Request);
            });
        }
    }
    internal class ActorGetCustomerDownload : ReceiveActor
    {
        public ActorGetCustomerDownload()
        {
            FrameworkAccounts _FrameworkAccounts;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkAccounts = new FrameworkAccounts();
                _FrameworkAccounts.GetCustomerDownload(_Request);
            });
        }
    }
    internal class ActorGetCashierDownload : ReceiveActor
    {
        public ActorGetCashierDownload()
        {
            FrameworkAccounts _FrameworkAccounts;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkAccounts = new FrameworkAccounts();
                _FrameworkAccounts.GetCashiersDownload(_Request);
            });
        }
    }
    internal class ActorGetMerchantDownload : ReceiveActor
    {
        public ActorGetMerchantDownload()
        {
            FrameworkAccounts _FrameworkAccounts;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkAccounts = new FrameworkAccounts();
                _FrameworkAccounts.GetMerchantDownload(_Request);
            });
        }
    }
    internal class ActorGetPsspDownload : ReceiveActor
    {
        public ActorGetPsspDownload()
        {
            FrameworkAccounts _FrameworkAccounts;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkAccounts = new FrameworkAccounts();
                _FrameworkAccounts.GetPsspDownload(_Request);
            });
        }
    }

    internal class ActorGetPtspDownload : ReceiveActor
    {
        public ActorGetPtspDownload()
        {
            FrameworkAccounts _FrameworkAccounts;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkAccounts = new FrameworkAccounts();
                _FrameworkAccounts.GetPtspDownload(_Request);
            });
        }
    }
    internal class ActorGetPartnerDownload : ReceiveActor
    {
        public ActorGetPartnerDownload()
        {
            FrameworkAccounts _FrameworkAccounts;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkAccounts = new FrameworkAccounts();
                _FrameworkAccounts.GetPartnerDownload(_Request);
            });
        }
    }

    internal class ActorGetAcquirerDownload : ReceiveActor
    {
        public ActorGetAcquirerDownload()
        {
            FrameworkAccounts _FrameworkAccounts;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkAccounts = new FrameworkAccounts();
                _FrameworkAccounts.GetAcquirersDownload(_Request);
            });
        }
    }
}

#endregion