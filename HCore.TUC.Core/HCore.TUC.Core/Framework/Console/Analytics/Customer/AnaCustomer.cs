//==================================================================================
// FileName: AnaCustomer.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to tracking customer realated activities
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Console.Analytics;
using HCore.TUC.Core.Object.Console.Analytics.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
namespace HCore.TUC.Core.Framework.Console.Analytics.Customer
{
    internal class AnaCustomer
    {
        private class OTrList
        {
            public double RewardAmount { get; set; }
            public double RedeemAmount { get; set; }
            public DateTime TransactionDate { get; set; }
            public double InvoiceAmount { get; set; }
            public long? UserAccountId { get; set; }
            public double? CommissionAmount { get; set; }
        }
        List<OTrList> _OTrList;
        OAnalytics.SalesOverview _SalesOverview;
        OAnalytics.Loyalty _LoyaltyOverview;
        OCustomerAnalytics.Response _CustomerAnalytics;
        OCustomerAnalytics.Count _CustomerAnalyticsCount;
        List<OCustomerAnalytics.User> _CustomerAnalyticsUsers;
        List<OCustomerAnalytics.Range> _CustomerAnalyticsRange;
        HCoreContext _HCoreContext;
        OMerchantAnalytics.Response _MerchantAnalyticsResponse;
        OMerchantAnalytics.Count _MerchantAnalyticsCount;
        List<OAnalytics.Sale> _SalesHistory;
        OCustomerAnalytics.StatusCount _CustomerStatusCount;
        OCustomerAnalytics.ReferrerCount _CustomerReferrerCount;
        OCustomerAnalytics.ActivityStatusCount _CustomerActivityStatusCount;
        OCustomerAnalytics.AppDownloadStatus _CustomerAppDownloadStatus;
        OCustomerAnalytics.PaymentMode _CustomerPaymentMode;
        List<OCustomerAnalytics.SaleRange> _SaleRange;

        /// <summary>
        /// Description: Method defined to get status count of customers
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomerStatusCount(OAnalytics.Request _Request)
        {
            _CustomerStatusCount = new OCustomerAnalytics.StatusCount();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {

                    _CustomerStatusCount.Total = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId);
                    _CustomerStatusCount.Active = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Default.Active && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate);
                    _CustomerStatusCount.Suspended = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Default.Suspended && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate);
                    _CustomerStatusCount.Blocked = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Default.Blocked && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate);
                    _CustomerStatusCount.AppUsers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Default.Active && x.HCUAccountSessionAccount.Any() && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate);
                    _CustomerStatusCount.NonAppUsers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Default.Active && !x.HCUAccountSessionAccount.Any() && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate);


                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerStatusCount, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomerStatusCount", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Method defined to get the customers payment mode.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomersPaymentMode(OAnalytics.Request _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _CustomerPaymentMode = new OCustomerAnalytics.PaymentMode();
                    _CustomerPaymentMode.CardUsers = _HCoreContext.HCUAccount.Count(a => a.AccountTypeId == UserAccountType.Appuser && a.StatusId == HCoreConstant.HelperStatus.Default.Active && a.HCUAccountTransactionAccount.Any(x => x.TransactionDate >= _Request.StartDate && x.TransactionDate <= _Request.EndDate && x.TypeId == TransactionType.CardReward && x.Customer.CountryId == _Request.UserReference.CountryId && x.Type.SubParentId == TransactionTypeCategory.Reward && x.StatusId == HelperStatus.Transaction.Success));
                    _CustomerPaymentMode.CashUsers = _HCoreContext.HCUAccount.Count(a => a.AccountTypeId == UserAccountType.Appuser && a.StatusId == HCoreConstant.HelperStatus.Default.Active && a.HCUAccountTransactionAccount.Any(x => x.TransactionDate >= _Request.StartDate && x.TransactionDate <= _Request.EndDate && x.TypeId == TransactionType.CashReward && x.Customer.CountryId == _Request.UserReference.CountryId && x.Type.SubParentId == TransactionTypeCategory.Reward && x.StatusId == HelperStatus.Transaction.Success));
                    _CustomerPaymentMode.CardTransactions = _HCoreContext.HCUAccountTransaction.Count(x => x.TransactionDate >= _Request.StartDate && x.TransactionDate <= _Request.EndDate && x.TypeId == TransactionType.CardReward && x.Customer.CountryId == _Request.UserReference.CountryId && x.Type.SubParentId == TransactionTypeCategory.Reward && x.StatusId == HelperStatus.Transaction.Success);
                    _CustomerPaymentMode.CashTransactions = _HCoreContext.HCUAccountTransaction.Count(x => x.TransactionDate >= _Request.StartDate && x.TransactionDate <= _Request.EndDate && x.TypeId == TransactionType.CashReward && x.Customer.CountryId == _Request.UserReference.CountryId && x.Type.SubParentId == TransactionTypeCategory.Reward && x.StatusId == HelperStatus.Transaction.Success);
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerPaymentMode, "HC0001", "Details loaded");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersPaymentMode", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Method defined to get the customer referrer count.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomerReferrerCount(OAnalytics.Request _Request)
        {
            _CustomerReferrerCount = new OCustomerAnalytics.ReferrerCount();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _CustomerReferrerCount.Total = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId);
                    _CustomerReferrerCount.Tuc = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate && x.DisplayName != x.MobileNumber);
                    _CustomerReferrerCount.NonTuc = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate && x.DisplayName == x.MobileNumber);
                    _CustomerReferrerCount.Customers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.Owner.AccountTypeId == UserAccountType.Appuser && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate);
                    _CustomerReferrerCount.Merchants = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.Owner.AccountTypeId == UserAccountType.Merchant && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate);
                    _CustomerReferrerCount.Partners = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && (x.Owner.AccountTypeId == UserAccountType.Acquirer || x.Owner.AccountTypeId == UserAccountType.Partner || x.Owner.AccountTypeId == UserAccountType.PgAccount || x.Owner.AccountTypeId == UserAccountType.PgAccount) && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate);

                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerReferrerCount, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Method defined to get the customer activity status count.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomerActivityStatusCount(OAnalytics.Request _Request)
        {
            _CustomerActivityStatusCount = new OCustomerAnalytics.ActivityStatusCount();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime BaseStartDate = _Request.EndDate.Value.Date.AddHours(-1);
                    DateTime BaseEndDate = _Request.EndDate.Value.Date.AddHours(24).AddHours(-1);

                    var baseDate = _Request.EndDate.Value.Date;
                    var thisWeekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek).AddHours(-1);
                    var thisWeekEnd = thisWeekStart.AddDays(7).AddSeconds(-1).AddHours(-1);
                    var lastWeekStart = thisWeekStart.AddDays(-7).AddHours(-1);
                    var lastWeekEnd = thisWeekStart.AddSeconds(-1).AddHours(-1);
                    var thisMonthStart = baseDate.AddDays(1 - baseDate.Day).AddHours(-1);
                    var thisMonthEnd = thisMonthStart.AddMonths(1).AddSeconds(-1).AddHours(-1);
                    var lastMonthStart = thisMonthStart.AddMonths(-1).AddHours(-1);
                    var lastMonthEnd = thisMonthStart.AddSeconds(-1).AddHours(-1);
                    _CustomerActivityStatusCount.Total = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId);
                    _CustomerActivityStatusCount.Active = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.HCUAccountTransactionAccount.Any(a => a.TransactionDate > BaseStartDate && a.TransactionDate < BaseEndDate));
                    _CustomerActivityStatusCount.Idle = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.HCUAccountTransactionAccount.Any(a => a.TransactionDate > thisWeekStart && a.TransactionDate < thisWeekEnd));
                    _CustomerActivityStatusCount.Dead = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.HCUAccountTransactionAccount.Any(a => a.TransactionDate < lastMonthStart));

                    _CustomerAppDownloadStatus = new OCustomerAnalytics.AppDownloadStatus();
                    _CustomerAppDownloadStatus.Total = _HCoreContext.HCUAccount
                        .Count(x => x.AccountTypeId == UserAccountType.Appuser
                        && x.CountryId == _Request.UserReference.CountryId
                        && x.StatusId == HelperStatus.Default.Active
                        && x.HCUAccountSessionAccount.Where(a => a.PlatformId == Helpers.Platform.Mobile && a.LoginDate > _Request.StartDate && a.LoginDate < _Request.EndDate).Any()); ;

                    _CustomerAppDownloadStatus.Active = _HCoreContext.HCUAccount
                       .Count(x => x.AccountTypeId == UserAccountType.Appuser
                       && x.CountryId == _Request.UserReference.CountryId
                       && x.StatusId == HelperStatus.Default.Active
                       && x.HCUAccountSessionAccount.Where(a => a.PlatformId == Helpers.Platform.Mobile && a.LastActivityDate > _Request.StartDate && a.LastActivityDate < _Request.EndDate).Any());

                    DateTime IdleStart = _Request.StartDate.Value.AddDays(-15);
                    DateTime IdleEnd = _Request.EndDate.Value.AddDays(-15);

                    _CustomerAppDownloadStatus.Idle = _HCoreContext.HCUAccount
                     .Count(x => x.AccountTypeId == UserAccountType.Appuser
                     && x.CountryId == _Request.UserReference.CountryId
                     && x.StatusId == HelperStatus.Default.Active
                     && x.HCUAccountSessionAccount.Where(a => a.PlatformId == Helpers.Platform.Mobile && a.LastActivityDate > IdleStart && a.LastActivityDate < IdleEnd).Any());

                    _CustomerAppDownloadStatus.Dead = _HCoreContext.HCUAccount
                    .Count(x => x.AccountTypeId == UserAccountType.Appuser
                    && x.CountryId == _Request.UserReference.CountryId
                    && x.StatusId == HelperStatus.Default.Active
                    && x.HCUAccountSessionAccount.Where(a => a.PlatformId == Helpers.Platform.Mobile && a.LastActivityDate < IdleStart).Any());

                    _CustomerActivityStatusCount.AppDownloads = _CustomerAppDownloadStatus;

                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerActivityStatusCount, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Method defined to get the customer application activity status count.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomerAppActivityStatusCount(OAnalytics.Request _Request)
        {
            _CustomerActivityStatusCount = new OCustomerAnalytics.ActivityStatusCount();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime BaseStartDate = _Request.EndDate.Value.Date.AddHours(-1);
                    DateTime BaseEndDate = _Request.EndDate.Value.Date.AddHours(24).AddHours(-1);
                    var baseDate = _Request.EndDate.Value.Date;
                    var thisWeekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek).AddHours(-1);
                    var thisWeekEnd = thisWeekStart.AddDays(7).AddSeconds(-1).AddHours(-1);
                    var lastWeekStart = thisWeekStart.AddDays(-7).AddHours(-1);
                    var lastWeekEnd = thisWeekStart.AddSeconds(-1).AddHours(-1);
                    var thisMonthStart = baseDate.AddDays(1 - baseDate.Day).AddHours(-1);
                    var thisMonthEnd = thisMonthStart.AddMonths(1).AddSeconds(-1).AddHours(-1);
                    var lastMonthStart = thisMonthStart.AddMonths(-1).AddHours(-1);
                    var lastMonthEnd = thisMonthStart.AddSeconds(-1).AddHours(-1);
                    _CustomerActivityStatusCount.Total = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.HCUAccountSessionAccount.Any());
                    _CustomerActivityStatusCount.Active = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.HCUAccountSessionAccount.Any() && x.HCUAccountSessionAccount.OrderByDescending(a => a.LoginDate).Where(a => a.LastActivityDate > BaseStartDate && a.LastActivityDate < BaseEndDate).Any());
                    _CustomerActivityStatusCount.Idle = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.HCUAccountSessionAccount.Any() && x.HCUAccountSessionAccount.OrderByDescending(a => a.LoginDate).Where(a => a.LastActivityDate > thisWeekStart && a.LastActivityDate < thisWeekEnd).Any());
                    _CustomerActivityStatusCount.Dead = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.HCUAccountSessionAccount.Any() && x.HCUAccountSessionAccount.OrderByDescending(a => a.LoginDate).Where(a => a.LastActivityDate > lastMonthStart).Any());
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerActivityStatusCount, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Method defined to get the customers application activity status count.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomersAppActivityStatusCount(OAnalytics.Request _Request)
        {
            try
            {
                DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
                DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
                DateTime T7DayStart = TDayStart.AddDays(-7);
                DateTime T7DayEnd = TDayStart.AddSeconds(1);
                DateTime TDeadDayEnd = T7DayStart;
                using (_HCoreContext = new HCoreContext())
                {
                    _CustomerAppDownloadStatus = new OCustomerAnalytics.AppDownloadStatus();
                    _CustomerAppDownloadStatus.Total = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.HCUAccountSessionAccount.Any());
                    _CustomerAppDownloadStatus.Active = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.HCUAccountSessionAccount.OrderByDescending(a => a.LoginDate).Any(a => a.LastActivityDate > TDayStart && a.LastActivityDate < TDayEnd));
                    _CustomerAppDownloadStatus.Idle = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.HCUAccountSessionAccount.OrderByDescending(a => a.LoginDate).Any(a => a.LastActivityDate > T7DayStart && a.LastActivityDate < T7DayEnd));
                    _CustomerAppDownloadStatus.Dead = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.HCUAccountSessionAccount.OrderByDescending(a => a.LoginDate).Any(a => a.LastActivityDate < TDeadDayEnd));
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerAppDownloadStatus, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersStatusOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Method defined to get the customers top spenders.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomersTopSpenders(OAnalytics.Request _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    List<OCustomerAnalytics.User> Data = _HCoreContext.HCUAccount.Where(x =>
                     x.AccountTypeId == UserAccountType.Appuser
                     && x.CountryId == _Request.UserReference.CountryId
                     && x.StatusId == HelperStatus.Default.Active
                     && x.cmt_salecustomer.Any(a => a.status_id == HelperStatus.Transaction.Success && a.transaction_date > _Request.StartDate && a.transaction_date < _Request.EndDate) == true)
                         .Select(x => new OCustomerAnalytics.User
                         {
                             ReferenceId = x.Id,
                             ReferenceKey = x.Guid,
                             DisplayName = x.DisplayName,
                             MobileNumber = x.MobileNumber,
                             IconUrl = x.IconStorage.Path,
                             InvoiceAmount = x.cmt_salecustomer.Where(a => a.status_id == HelperStatus.Transaction.Success && a.transaction_date > _Request.StartDate && a.transaction_date < _Request.EndDate).Sum(a => a.invoice_amount),
                         })
                         .OrderByDescending(x => x.InvoiceAmount)
                         .Skip(_Request.Offset)
                         .Take(_Request.Limit)
                         .ToList();
                    if (Data.Count > 0)
                    {
                        foreach (var TopSpender in Data)
                        {
                            if (!string.IsNullOrEmpty(TopSpender.IconUrl))
                            {
                                TopSpender.IconUrl = _AppConfig.StorageUrl + TopSpender.IconUrl;
                            }
                            else
                            {
                                TopSpender.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "HC0001", "Details loaded");
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersTopSpenders", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Method defined to get the customers top category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomersTopCategory(OAnalytics.Request _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    List<OCustomerAnalytics.Category> Data = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.TransactionDate > _Request.StartDate && x.TransactionDate < _Request.EndDate
                     && x.Customer.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Transaction.Success)
                        .GroupBy(x => x.Parent.PrimaryCategory.Name)
                        .Select(x => new OCustomerAnalytics.Category
                        {
                            Name = x.Key,
                            Count = x.Select(a => a.CustomerId).Distinct().Count(),
                        }).OrderByDescending(x => x.Count)
                        .Skip(0)
                        .Take(5)
                        .ToList();

                    OList.Response _OResponse = HCoreHelper.GetListResponse(Data.Count, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001", "Details loaded");

                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetTopCategory", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Method defined to get the customers top visitors.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomersTopVisitors(OAnalytics.Request _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    List<OCustomerAnalytics.User> Data = _HCoreContext.HCUAccount.Where(x =>
                  x.AccountTypeId == UserAccountType.Appuser
                && x.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Default.Active
                  && x.cmt_salecustomer.Any(a => a.status_id == HelperStatus.Transaction.Success && a.transaction_date > _Request.StartDate && a.transaction_date < _Request.EndDate) == true)
                     .Select(x => new OCustomerAnalytics.User
                     {
                         ReferenceId = x.Id,
                         ReferenceKey = x.Guid,
                         DisplayName = x.DisplayName,
                         MobileNumber = x.MobileNumber,
                         IconUrl = x.IconStorage.Path,
                         Visits = x.cmt_salecustomer.Count(a => a.status_id == HelperStatus.Transaction.Success && a.transaction_date > _Request.StartDate && a.transaction_date < _Request.EndDate),
                     })
                     .OrderByDescending(x => x.Visits)
                     .Skip(_Request.Offset)
                     .Take(_Request.Limit)
                     .ToList();
                    if (Data.Count > 0)
                    {
                        foreach (var TopVisitor in Data)
                        {
                            if (!string.IsNullOrEmpty(TopVisitor.IconUrl))
                            {
                                TopVisitor.IconUrl = _AppConfig.StorageUrl + TopVisitor.IconUrl;
                            }
                            else
                            {
                                TopVisitor.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "HC0001", "Details loaded");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC0001", "Details loaded");
                        #endregion
                    }
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersTopVisitors", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Method defined to get the customers gender.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomersGender(OAnalytics.Request _Request)
        {
            _CustomerAnalytics = new OCustomerAnalytics.Response();
            _CustomerAnalyticsCount = new OCustomerAnalytics.Count();
            _LoyaltyOverview = new OAnalytics.Loyalty();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime TodayTime = HCoreHelper.GetGMTDateTime();
                    _CustomerAnalyticsRange = new List<OCustomerAnalytics.Range>();
                    _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                    {
                        Title = "Male",

                        Count = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.GenderId == Helpers.Gender.Male && x.HCUAccountTransactionCustomer.Any(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate)),

                        Visits = _HCoreContext.HCUAccountTransaction.Count(x =>
                        x.Account.AccountTypeId == UserAccountType.Appuser
                        && x.Account.CountryId == _Request.UserReference.CountryId
                        && x.Account.GenderId == Helpers.Gender.Male
                       && x.StatusId == HelperStatus.Transaction.Success
                        && x.TransactionDate > _Request.StartDate
                        && x.TransactionDate < _Request.EndDate),

                        InvoiceAmount = _HCoreContext.cmt_sale.Where(x =>
                         x.status_id == HelperStatus.Transaction.Success
                       && x.customer.GenderId == Helpers.Gender.Male
                       && x.transaction_date > _Request.StartDate
                       && x.transaction_date < _Request.EndDate)
                        .Sum(x => x.invoice_amount),
                    });
                    _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                    {
                        Title = "Female",

                        Count = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.CountryId && x.GenderId == Helpers.Gender.Female && x.HCUAccountTransactionCustomer.Any(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate)),

                        Visits = _HCoreContext.HCUAccountTransaction.Count(x =>
                        x.Account.AccountTypeId == UserAccountType.Appuser
                        && x.Account.CountryId == _Request.UserReference.CountryId
                        && x.Account.GenderId == Helpers.Gender.Female
                       && x.StatusId == HelperStatus.Transaction.Success
                        && x.TransactionDate > _Request.StartDate
                        && x.TransactionDate < _Request.EndDate),

                        InvoiceAmount = _HCoreContext.cmt_sale.Where(x =>
                         x.status_id == HelperStatus.Transaction.Success
                       && x.customer.GenderId == Helpers.Gender.Male
                       && x.transaction_date > _Request.StartDate
                       && x.transaction_date < _Request.EndDate)
                        .Sum(x => x.invoice_amount),
                    });
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerAnalyticsRange, "HC0001", "Details loaded");
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersGender", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Method defined to get the customers age group.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomersAgeGroup(OAnalytics.Request _Request)
        {
            try
            {
                DateTime TodayTime = HCoreHelper.GetGMTDateTime();
                using (_HCoreContext = new HCoreContext())
                {
                    _CustomerAnalyticsRange = new List<OCustomerAnalytics.Range>();
                    _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                    {
                        Title = "18-29",
                        Count = _HCoreContext.HCUAccount.Count(x =>
                    x.AccountTypeId == UserAccountType.Appuser
                   && x.CountryId == _Request.UserReference.CountryId
                   && x.DateOfBirth != null
                    && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 17 && (TodayTime.Year - x.DateOfBirth.Value.Year) < 30)
                    && x.HCUAccountTransactionAccount.Any(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate) == true),
                        Visits = _HCoreContext.HCUAccountTransaction.Count(x =>
                       x.Account.AccountTypeId == UserAccountType.Appuser
                        && x.StatusId == HelperStatus.Transaction.Success
                       && x.Account.GenderId == Helpers.Gender.Female
                        && x.Account.DateOfBirth != null
                    && ((TodayTime.Year - x.Account.DateOfBirth.Value.Year) > 17 && (TodayTime.Year - x.Account.DateOfBirth.Value.Year) < 30)
                       && x.TransactionDate > _Request.StartDate
                       && x.TransactionDate < _Request.EndDate),
                    });
                    _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                    {
                        Title = "30-49",
                        Count = _HCoreContext.HCUAccount.Count(x =>
                    x.AccountTypeId == UserAccountType.Appuser
                    && x.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Transaction.Success
                    && x.DateOfBirth != null
                    && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 29 && (TodayTime.Year - x.DateOfBirth.Value.Year) < 50)
                    && x.HCUAccountTransactionAccount.Any(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate) == true),

                        Visits = _HCoreContext.HCUAccountTransaction.Count(x =>
                       x.Account.AccountTypeId == UserAccountType.Appuser
                     && x.Account.CountryId == _Request.UserReference.CountryId && x.Account.GenderId == Helpers.Gender.Female
                       && x.StatusId == HelperStatus.Transaction.Success
                        && x.Account.DateOfBirth != null
                    && ((TodayTime.Year - x.Account.DateOfBirth.Value.Year) > 29 && (TodayTime.Year - x.Account.DateOfBirth.Value.Year) < 50)
                       && x.TransactionDate > _Request.StartDate
                       && x.TransactionDate < _Request.EndDate),
                    });
                    _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                    {
                        Title = "50 ABOVE",
                        Count = _HCoreContext.HCUAccount.Count(x =>
                    x.AccountTypeId == UserAccountType.Appuser
                   && x.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Transaction.Success
                    && x.DateOfBirth != null
                    && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 49)
                    && x.HCUAccountTransactionAccount.Any(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate) == true),

                        Visits = _HCoreContext.HCUAccountTransaction.Count(x =>
                       x.Account.AccountTypeId == UserAccountType.Appuser
                   && x.Account.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Transaction.Success
                       && x.Account.GenderId == Helpers.Gender.Female
                        && x.Account.DateOfBirth != null
                    && ((TodayTime.Year - x.Account.DateOfBirth.Value.Year) > 49)
                       && x.TransactionDate > _Request.StartDate
                       && x.TransactionDate < _Request.EndDate),
                    });
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerAnalyticsRange, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Method defined to get the customers age group gender.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomersAgeGroupGender(OAnalytics.Request _Request)
        {
            try
            {
                DateTime TodayTime = HCoreHelper.GetGMTDateTime();
                using (_HCoreContext = new HCoreContext())
                {
                    _CustomerAnalyticsRange = new List<OCustomerAnalytics.Range>();
                    _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                    {
                        Title = "18-29",
                        Count = _HCoreContext.HCUAccount.Count(x =>
                        x.AccountTypeId == UserAccountType.Appuser
                        && x.DateOfBirth != null
                       && x.CountryId == _Request.UserReference.CountryId
                       && x.CreateDate > _Request.StartDate
                        && x.CreateDate < _Request.EndDate
                        && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 17 && (TodayTime.Year - x.DateOfBirth.Value.Year) < 30)),

                        Male = _HCoreContext.HCUAccount.Count(x =>
                       x.AccountTypeId == UserAccountType.Appuser
                      && x.CountryId == _Request.UserReference.CountryId
                      && x.GenderId == Gender.Male
                       && x.DateOfBirth != null
                       && x.CreateDate > _Request.StartDate
                       && x.CreateDate < _Request.EndDate
                       && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 17 && (TodayTime.Year - x.DateOfBirth.Value.Year) < 30)),


                        Female = _HCoreContext.HCUAccount.Count(x =>
                        x.AccountTypeId == UserAccountType.Appuser
                       && x.CountryId == _Request.UserReference.CountryId
                       && x.GenderId == Gender.Female
                        && x.DateOfBirth != null
                        && x.CreateDate > _Request.StartDate
                        && x.CreateDate < _Request.EndDate
                        && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 17 && (TodayTime.Year - x.DateOfBirth.Value.Year) < 30)),
                    });


                    _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                    {
                        Title = "30-39",
                        Count = _HCoreContext.HCUAccount.Count(x =>
                        x.AccountTypeId == UserAccountType.Appuser
                        && x.CountryId == _Request.UserReference.CountryId
                        && x.DateOfBirth != null
                        && x.CreateDate > _Request.StartDate
                        && x.CreateDate < _Request.EndDate
                        && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 29 && (TodayTime.Year - x.DateOfBirth.Value.Year) < 40)),

                        Male = _HCoreContext.HCUAccount.Count(x =>
                       x.AccountTypeId == UserAccountType.Appuser
                       && x.CountryId == _Request.UserReference.CountryId
                       && x.GenderId == Gender.Male
                       && x.DateOfBirth != null
                       && x.CreateDate > _Request.StartDate
                       && x.CreateDate < _Request.EndDate
                       && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 29 && (TodayTime.Year - x.DateOfBirth.Value.Year) < 40)),


                        Female = _HCoreContext.HCUAccount.Count(x =>
                        x.AccountTypeId == UserAccountType.Appuser
                        && x.CountryId == _Request.UserReference.CountryId
                        && x.GenderId == Gender.Female
                        && x.DateOfBirth != null
                        && x.CreateDate > _Request.StartDate
                        && x.CreateDate < _Request.EndDate
                        && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 29 && (TodayTime.Year - x.DateOfBirth.Value.Year) < 40)),
                    });



                    _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                    {
                        Title = "40-49",
                        Count = _HCoreContext.HCUAccount.Count(x =>
                        x.AccountTypeId == UserAccountType.Appuser
                        && x.CountryId == _Request.UserReference.CountryId
                        && x.DateOfBirth != null
                        && x.CreateDate > _Request.StartDate
                        && x.CreateDate < _Request.EndDate
                        && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 39 && (TodayTime.Year - x.DateOfBirth.Value.Year) < 50)),

                        Male = _HCoreContext.HCUAccount.Count(x =>
                       x.AccountTypeId == UserAccountType.Appuser
                       && x.GenderId == Gender.Male
                     && x.CountryId == _Request.UserReference.CountryId
                     && x.DateOfBirth != null
                       && x.CreateDate > _Request.StartDate
                       && x.CreateDate < _Request.EndDate
                       && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 39 && (TodayTime.Year - x.DateOfBirth.Value.Year) < 50)),


                        Female = _HCoreContext.HCUAccount.Count(x =>
                        x.AccountTypeId == UserAccountType.Appuser
                        && x.GenderId == Gender.Female
                        && x.CountryId == _Request.UserReference.CountryId
                        && x.DateOfBirth != null
                        && x.CreateDate > _Request.StartDate
                        && x.CreateDate < _Request.EndDate
                        && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 39 && (TodayTime.Year - x.DateOfBirth.Value.Year) < 50)),
                    });



                    _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                    {
                        Title = "50 ABOVE",
                        Count = _HCoreContext.HCUAccount.Count(x =>
                    x.AccountTypeId == UserAccountType.Appuser
                     && x.StatusId == HelperStatus.Transaction.Success
                    && x.DateOfBirth != null
                    && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 49)),


                        Male = _HCoreContext.HCUAccount.Count(x =>
                    x.AccountTypeId == UserAccountType.Appuser
                  && x.CountryId == _Request.UserReference.CountryId
                  && x.GenderId == Gender.Male
                     && x.StatusId == HelperStatus.Transaction.Success
                    && x.DateOfBirth != null
                    && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 49)),

                        Female = _HCoreContext.HCUAccount.Count(x =>
                  x.AccountTypeId == UserAccountType.Appuser
               && x.CountryId == _Request.UserReference.CountryId
               && x.GenderId == Gender.Female
                   && x.StatusId == HelperStatus.Transaction.Success
                  && x.DateOfBirth != null
                  && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 49)),


                    });
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerAnalyticsRange, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersAgeGroupGender", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Method defined to get the customers spend range.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomersSpendRange(OAnalytics.Request _Request)
        {
            _CustomerAnalytics = new OCustomerAnalytics.Response();
            _CustomerAnalyticsCount = new OCustomerAnalytics.Count();
            _LoyaltyOverview = new OAnalytics.Loyalty();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _CustomerAnalyticsRange = new List<OCustomerAnalytics.Range>();
                    _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                    {
                        Title = "< 30k",
                        Count = _HCoreContext.HCUAccount.Count(x =>
                     x.AccountTypeId == UserAccountType.Appuser
                     && x.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Transaction.Success
                     && x.HCUAccountTransactionCustomer.Where(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate
                     && a.SourceId == TransactionSource.TUC).Sum(a => a.PurchaseAmount) < 30000),
                    });
                    _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                    {
                        Title = "30k - 50k",
                        Count = _HCoreContext.HCUAccount.Count(x =>
                    x.AccountTypeId == UserAccountType.Appuser
                    && x.CountryId == _Request.UserReference.CountryId && x.HCUAccountTransactionCustomer.Where(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate
                    && a.SourceId == TransactionSource.TUC).Sum(a => a.PurchaseAmount) > 30000
                    && x.HCUAccountTransactionCustomer.Where(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate
                    && a.SourceId == TransactionSource.TUC).Sum(a => a.PurchaseAmount) < 50000
                    ),
                    });
                    _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                    {
                        Title = "50k - 75k",
                        Count = _HCoreContext.HCUAccount.Count(x =>
                    x.AccountTypeId == UserAccountType.Appuser
                    && x.CountryId == _Request.UserReference.CountryId && x.HCUAccountTransactionCustomer.Where(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate
                    && a.SourceId == TransactionSource.TUC).Sum(a => a.PurchaseAmount) > 50000
                    && x.HCUAccountTransactionCustomer.Where(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate
                    && a.SourceId == TransactionSource.TUC).Sum(a => a.PurchaseAmount) < 75000
                  ),
                    });
                    _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                    {
                        Title = "75k +",
                        Count = _HCoreContext.HCUAccount.Count(x =>
                    x.AccountTypeId == UserAccountType.Appuser
                    && x.CountryId == _Request.UserReference.CountryId && x.HCUAccountTransactionCustomer.Where(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate
                    && a.SourceId == TransactionSource.TUC).Sum(a => a.PurchaseAmount) > 75000),
                    });
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerAnalyticsRange, "HC0001", "Details loaded");
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersSpendRange", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Method defined to get the sale range.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSaleRange(OAnalytics.Request _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _SaleRange = new List<OCustomerAnalytics.SaleRange>();
                    List<OCustomerAnalytics.Account> Data = _HCoreContext.cmt_sale
                       .Where(x => x.transaction_date > _Request.StartDate && x.transaction_date < _Request.EndDate
                       && x.type.ParentId == TransactionTypeCategory.Reward
                       && x.customer.CountryId == _Request.UserReference.CountryId && x.status_id == HelperStatus.Transaction.Success)
                        .GroupBy(x => x.customer_id)
                        .Select(x => new OCustomerAnalytics.Account
                        {
                            ReferenceId = x.Key,
                            InvoiceAmount = x.Sum(a => a.invoice_amount),
                        })
                        .ToList();
                    if (Data.Count > 0)
                    {
                        double MinimumRange = Data.OrderBy(x => x.InvoiceAmount).FirstOrDefault().InvoiceAmount;
                        double MaximumRange = Data.OrderByDescending(x => x.InvoiceAmount).FirstOrDefault().InvoiceAmount;
                        double Difference = RoundValueToNext100((MaximumRange - MinimumRange) / 4);
                        double LowLimit = 0;
                        double HighLimit = Difference;
                        for (int i = 1; i <= 4; i++)
                        {
                            if (i == 4)
                            {
                                long Merchnats = Data.Where(a => a.InvoiceAmount > LowLimit).Count();
                                _SaleRange.Add(new OCustomerAnalytics.SaleRange
                                {
                                    MinimumRange = LowLimit,
                                    MaximumRange = HighLimit,
                                    Count = Merchnats
                                });
                            }
                            else
                            {
                                long Merchnats = Data.Where(a => a.InvoiceAmount > LowLimit && a.InvoiceAmount < HighLimit).Count();
                                _SaleRange.Add(new OCustomerAnalytics.SaleRange
                                {
                                    MinimumRange = LowLimit,
                                    MaximumRange = HighLimit,
                                    Count = Merchnats
                                });
                            }
                            LowLimit += Difference;
                            HighLimit += Difference;
                        }
                        //foreach (var DataItem in Data)
                        //{
                        //    DataItem.ReferenceKey = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.ReferenceKey).FirstOrDefault();
                        //    DataItem.IconUrl = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.IconUrl).FirstOrDefault();
                        //    DataItem.DisplayName = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.DisplayName).FirstOrDefault();
                        //    DataItem.CreateDate = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.CreateDate).FirstOrDefault();
                        //}
                    }


                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SaleRange, "HC0001", "Details loaded");
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetSaleRange", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Rounds the value to next100.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>System.Int32.</returns>
        private static int RoundValueToNext100(double value)
        {
            int result = (int)Math.Round(value / 100, 0, MidpointRounding.AwayFromZero);

            if (value > 0 && result == 0)
            {
                result = 1;
            }
            return (int)result * 100;
        }
        /// <summary>
        /// Description: Method defined to get the reward range.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRewardRange(OAnalytics.Request _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    #region Old Code
                    //_SaleRange = new List<OCustomerAnalytics.SaleRange>();
                    //List<OCustomerAnalytics.Account> Data = _HCoreContext.HCUAccountTransaction
                    //   .Where(x => (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.RedeemReward || x.TypeId == TransactionType.TUCPayReward)
                    //   //x.LoyaltyTypeId == Helpers.Loyalty.Reward
                    //   && x.Customer.AccountTypeId == UserAccountType.Appuser
                    //   && x.Customer.CountryId == _Request.UserReference.CountryId && x.Type.SubParentId == TransactionTypeCategory.Reward
                    //   && x.TransactionDate > _Request.StartDate && x.TransactionDate < _Request.EndDate
                    //   && x.StatusId == HelperStatus.Transaction.Success)
                    //    .GroupBy(x => x.CustomerId)
                    //    .Select(x => new OCustomerAnalytics.Account
                    //    {
                    //        ReferenceId = x.Key,
                    //        Amount = x.Sum(a => a.TotalAmount),
                    //        RewardAmount = x.Sum(x => x.ReferenceAmount),
                    //        InvoiceAmount = x.Sum(x => x.PurchaseAmount)
                    //    })
                    //    .ToList();
                    //if (Data.Count > 0)
                    //{
                    //    double? MinimumRange = Data.OrderBy(x => x.RewardAmount).FirstOrDefault().RewardAmount;
                    //    double? MaximumRange = Data.OrderByDescending(x => x.RewardAmount).FirstOrDefault().RewardAmount;
                    //    double Difference = RoundValueToNext100((double)((MaximumRange - MinimumRange) / 4));
                    //    double? LowLimit = 0;
                    //    double? HighLimit = Difference;
                    //    for (int i = 1; i <= 4; i++)
                    //    {
                    //        if (i == 4)
                    //        {
                    //            long Merchnats = Data.Where(a => a.RewardAmount > LowLimit).Count();
                    //            _SaleRange.Add(new OCustomerAnalytics.SaleRange
                    //            {
                    //                MinimumRange = LowLimit,
                    //                MaximumRange = HighLimit,
                    //                Count = Merchnats
                    //            });
                    //        }
                    //        else
                    //        {
                    //            long Merchnats = Data.Where(a => a.RewardAmount > LowLimit && a.RewardAmount < HighLimit).Count();
                    //            _SaleRange.Add(new OCustomerAnalytics.SaleRange
                    //            {
                    //                MinimumRange = LowLimit,
                    //                MaximumRange = HighLimit,
                    //                Count = Merchnats
                    //            });
                    //        }
                    //        LowLimit += Difference;
                    //        HighLimit += Difference;
                    //    }
                    //    //foreach (var DataItem in Data)
                    //    //{
                    //    //    DataItem.ReferenceKey = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.ReferenceKey).FirstOrDefault();
                    //    //    DataItem.IconUrl = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.IconUrl).FirstOrDefault();
                    //    //    DataItem.DisplayName = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.DisplayName).FirstOrDefault();
                    //    //    DataItem.CreateDate = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.CreateDate).FirstOrDefault();
                    //    //}
                    //}
                    #endregion

                    #region New Code
                    _CustomerAnalyticsRange = new List<OCustomerAnalytics.Range>();

                    _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                    {
                        Title = "< 30k",
                        Count = _HCoreContext.HCUAccount.Count(x =>
                        x.AccountTypeId == UserAccountType.Appuser
                        && x.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Transaction.Success
                        && x.HCUAccountTransactionCustomer.Where(m => (m.TypeId == TransactionType.CardReward || m.TypeId == TransactionType.CashReward
                        || m.TypeId == TransactionType.Loyalty.TUCRedeem.RedeemReward || m.TypeId == TransactionType.TUCPayReward)
                        && m.Customer.AccountTypeId == UserAccountType.Appuser
                        && m.Customer.CountryId == _Request.UserReference.CountryId && m.Type.SubParentId == TransactionTypeCategory.Reward
                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                        && m.StatusId == HelperStatus.Transaction.Success).Sum(a => a.ReferenceAmount) < 30000),
                    });

                    _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                    {
                        Title = "30k - 50k",
                        Count = _HCoreContext.HCUAccount.Count(x =>
                    x.AccountTypeId == UserAccountType.Appuser
                    && x.CountryId == _Request.UserReference.CountryId && x.HCUAccountTransactionCustomer.Where(m => (m.TypeId == TransactionType.CardReward || m.TypeId == TransactionType.CashReward
                        || m.TypeId == TransactionType.Loyalty.TUCRedeem.RedeemReward || m.TypeId == TransactionType.TUCPayReward)
                        && m.Customer.AccountTypeId == UserAccountType.Appuser
                        && m.Customer.CountryId == _Request.UserReference.CountryId && m.Type.SubParentId == TransactionTypeCategory.Reward
                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                        && m.StatusId == HelperStatus.Transaction.Success).Sum(a => a.ReferenceAmount) > 30000
                    && x.HCUAccountTransactionCustomer.Where(m => (m.TypeId == TransactionType.CardReward || m.TypeId == TransactionType.CashReward
                        || m.TypeId == TransactionType.Loyalty.TUCRedeem.RedeemReward || m.TypeId == TransactionType.TUCPayReward)
                        && m.Customer.AccountTypeId == UserAccountType.Appuser
                        && m.Customer.CountryId == _Request.UserReference.CountryId && m.Type.SubParentId == TransactionTypeCategory.Reward
                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                        && m.StatusId == HelperStatus.Transaction.Success).Sum(a => a.ReferenceAmount) < 50000
                    ),
                    });

                    _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                    {
                        Title = "50k - 75k",
                        Count = _HCoreContext.HCUAccount.Count(x =>
                    x.AccountTypeId == UserAccountType.Appuser
                    && x.CountryId == _Request.UserReference.CountryId && x.HCUAccountTransactionCustomer.Where(m => (m.TypeId == TransactionType.CardReward || m.TypeId == TransactionType.CashReward
                        || m.TypeId == TransactionType.Loyalty.TUCRedeem.RedeemReward || m.TypeId == TransactionType.TUCPayReward)
                        && m.Customer.AccountTypeId == UserAccountType.Appuser
                        && m.Customer.CountryId == _Request.UserReference.CountryId && m.Type.SubParentId == TransactionTypeCategory.Reward
                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                        && m.StatusId == HelperStatus.Transaction.Success).Sum(a => a.ReferenceAmount) > 50000
                    && x.HCUAccountTransactionCustomer.Where(m => (m.TypeId == TransactionType.CardReward || m.TypeId == TransactionType.CashReward
                        || m.TypeId == TransactionType.Loyalty.TUCRedeem.RedeemReward || m.TypeId == TransactionType.TUCPayReward)
                        && m.Customer.AccountTypeId == UserAccountType.Appuser
                        && m.Customer.CountryId == _Request.UserReference.CountryId && m.Type.SubParentId == TransactionTypeCategory.Reward
                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                        && m.StatusId == HelperStatus.Transaction.Success).Sum(a => a.ReferenceAmount) < 75000
                  ),
                    });

                    _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                    {
                        Title = "75k +",
                        Count = _HCoreContext.HCUAccount.Count(x =>
                    x.AccountTypeId == UserAccountType.Appuser
                    && x.CountryId == _Request.UserReference.CountryId && x.HCUAccountTransactionCustomer.Where(m => (m.TypeId == TransactionType.CardReward || m.TypeId == TransactionType.CashReward
                        || m.TypeId == TransactionType.Loyalty.TUCRedeem.RedeemReward || m.TypeId == TransactionType.TUCPayReward)
                        && m.Customer.AccountTypeId == UserAccountType.Appuser
                        && m.Customer.CountryId == _Request.UserReference.CountryId && m.Type.SubParentId == TransactionTypeCategory.Reward
                        && m.TransactionDate > _Request.StartDate && m.TransactionDate < _Request.EndDate
                        && m.StatusId == HelperStatus.Transaction.Success).Sum(a => a.ReferenceAmount) > 75000),
                    });
                    #endregion

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerAnalyticsRange, "HC0001", "Details loaded");
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetRewardRange", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
    }
}
