//==================================================================================
// FileName: AnaMerchant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to track merchant analytics
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Console.Analytics;
using HCore.TUC.Core.Object.Console.Analytics.Merchant;
using System;
using System.Collections.Generic;
using System.Linq;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.Framework.Console.Analytics.Merchant
{
    public class AnaMerchant
    {
        HCoreContext _HCoreContext;
        OAnaMerchant.StatusCount _MerchantStatusCount;
        OAnaMerchant.ActivityCount _MerchantActivityStatusCount;
        List<OAnaMerchant.SaleRange> _SaleRange;
        List<OAnaMerchant.Account> _TopRewards;

        /// <summary>
        /// Description: Gets the status count.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStatusCount(OAnaMerchant.Request _Request)
        {
            _MerchantStatusCount = new OAnaMerchant.StatusCount();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _MerchantStatusCount.Total = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant && x.CountryId == _Request.UserReference.CountryId && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate);
                    _MerchantStatusCount.Active = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant && x.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Default.Active && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate);
                    _MerchantStatusCount.Inactive = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant && x.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Default.Inactive && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate);
                    _MerchantStatusCount.Blocked = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant && x.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Default.Blocked && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate);
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _MerchantStatusCount, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetStatusCount", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

        /// <summary>
        /// Description: Gets the activity status count.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetActivityStatusCount(OAnaMerchant.Request _Request)
        {
            _MerchantActivityStatusCount = new OAnaMerchant.ActivityCount();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime BaseStartDate = _Request.StartDate.Date.AddHours(-1);
                    DateTime BaseEndDate = _Request.EndDate.Date.AddHours(24).AddHours(-1);

                    var baseDate = _Request.EndDate.Date;
                    var thisWeekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek).AddHours(-1);
                    var thisWeekEnd = thisWeekStart.AddDays(7).AddSeconds(-1).AddHours(-1);
                    var lastWeekStart = thisWeekStart.AddDays(-7).AddHours(-1);
                    var lastWeekEnd = thisWeekStart.AddSeconds(-1).AddHours(-1);
                    var thisMonthStart = baseDate.AddDays(1 - baseDate.Day).AddHours(-1);
                    var thisMonthEnd = thisMonthStart.AddMonths(1).AddSeconds(-1).AddHours(-1);
                    var lastMonthStart = thisMonthStart.AddMonths(-1).AddHours(-1);
                    var lastMonthEnd = thisMonthStart.AddSeconds(-1).AddHours(-1);

                    _MerchantActivityStatusCount.Total = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant && x.CountryId == _Request.UserReference.CountryId);
                    _MerchantActivityStatusCount.New = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant && x.CountryId == _Request.UserReference.CountryId && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate);
                    _MerchantActivityStatusCount.Active = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant && x.CountryId == _Request.UserReference.CountryId && x.LastTransactionDate > BaseStartDate && x.LastTransactionDate < BaseEndDate && x.StatusId == HelperStatus.Default.Active);
                    //_MerchantActivityStatusCount.Active = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant && x.CountryId == _Request.UserReference.CountryId && x.LastTransactionDate > _Request.StartDate && x.LastTransactionDate < _Request.EndDate && x.StatusId == HelperStatus.Default.Active);
                    _MerchantActivityStatusCount.Idle = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant && x.CountryId == _Request.UserReference.CountryId && x.LastTransactionDate > thisWeekStart && x.LastTransactionDate < thisWeekEnd && x.StatusId == HelperStatus.Default.Suspended);
                    //_MerchantActivityStatusCount.Idle = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant && x.CountryId == _Request.UserReference.CountryId && x.LastTransactionDate > _Request.StartDate && x.LastTransactionDate < _Request.EndDate && x.StatusId == HelperStatus.Default.Suspended);
                    _MerchantActivityStatusCount.Dead = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant && x.CountryId == _Request.UserReference.CountryId && x.LastTransactionDate > lastMonthStart && x.LastTransactionDate < lastMonthEnd && x.StatusId == HelperStatus.Default.Blocked);
                    //_MerchantActivityStatusCount.Dead = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant && x.CountryId == _Request.UserReference.CountryId && x.LastTransactionDate > _Request.StartDate && x.LastTransactionDate < _Request.EndDate && x.StatusId == HelperStatus.Default.Blocked);
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _MerchantActivityStatusCount, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetActivityCount", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

        /// <summary>
        /// Description: Gets the top rewards.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTopRewards(OAnaMerchant.Request _Request)
        {
            #region
            _MerchantActivityStatusCount = new OAnaMerchant.ActivityCount();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    int Count = _HCoreContext.HCUAccountTransaction
                          .Where(x => (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)
                        && x.Parent.CountryId == _Request.UserReference.CountryId
                          && x.Type.SubParentId == TransactionTypeCategory.Reward
                          && (x.TransactionDate > _Request.StartDate && x.TransactionDate < _Request.EndDate)
                          && x.StatusId == HelperStatus.Transaction.Success)
                        .GroupBy(m => m.ParentId)
                        .Select(x => new OAnaMerchant.Account
                        {
                            ReferenceId = x.Key,
                            Amount = x.Sum(a => a.TotalAmount),
                            InvoiceAmount = x.Sum(a => a.PurchaseAmount),
                            RewardAmount = x.Sum(a => a.ReferenceAmount),
                            Count = x.Count(),
                            Transactions = x.Count(),
                        })
                          .Count();
                    List<OAnaMerchant.Account> Data = _HCoreContext.HCUAccountTransaction
                        .Where(x => (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)
                        && x.Parent.CountryId == _Request.UserReference.CountryId
                        && x.Type.SubParentId == TransactionTypeCategory.Reward
                        && (x.TransactionDate > _Request.StartDate && x.TransactionDate < _Request.EndDate)
                        && x.StatusId == HelperStatus.Transaction.Success)
                        .GroupBy(m => m.ParentId)
                        .Select(x => new OAnaMerchant.Account
                        {
                            ReferenceId = x.Key,
                            Amount = x.Sum(a => a.TotalAmount),
                            RewardAmount = x.Sum(a => a.ReferenceAmount),
                            InvoiceAmount = x.Sum(a => a.PurchaseAmount),
                            Count = x.Count(),
                            Transactions = x.Count(),
                        })
                        .OrderByDescending(x => x.Amount)
                        .Skip(0)
                        .Take(5)
                        .ToList();

                    foreach (var DataItem in Data)
                    {
                        DataItem.ReferenceKey = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.ReferenceKey).FirstOrDefault();
                        DataItem.IconUrl = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.IconUrl).FirstOrDefault();
                        DataItem.DisplayName = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.DisplayName).FirstOrDefault();
                        DataItem.CreateDate = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.CreateDate).FirstOrDefault();
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(Count, Data, _Request.Offset, _Request.Limit);
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001", "Details loaded");
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetTopRewards", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the top visits.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTopVisits(OAnaMerchant.Request _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    int Count = _HCoreContext.HCUAccountTransaction
                       .Where(x => x.TransactionDate > _Request.StartDate && x.TransactionDate < _Request.EndDate
                    && x.Parent.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Transaction.Success)
                       .GroupBy(x => x.ParentId)
                       .Select(x => new OAnaMerchant.Account
                       {
                           ReferenceId = x.Key,
                           Amount = x.Sum(a => a.Amount),
                           InvoiceAmount = x.Sum(a => a.PurchaseAmount),
                           Count = x.Count(),
                           Transactions = x.Count(),
                       })
                       .Count();
                    List<OAnaMerchant.Account> Data = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.TransactionDate > _Request.StartDate && x.TransactionDate < _Request.EndDate
                       && x.Parent.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Transaction.Success)
                        .GroupBy(x => x.ParentId)
                        .Select(x => new OAnaMerchant.Account
                        {
                            ReferenceId = x.Key,
                            Amount = x.Sum(a => a.Amount),
                            InvoiceAmount = x.Sum(a => a.PurchaseAmount),
                            Count = x.Count(),
                            Transactions = x.Count(),
                        })
                        .OrderByDescending(x => x.Count)
                        .Skip(0)
                        .Take(5)
                        .ToList();
                    foreach (var DataItem in Data)
                    {
                        DataItem.ReferenceKey = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.ReferenceKey).FirstOrDefault();
                        DataItem.IconUrl = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.IconUrl).FirstOrDefault();
                        DataItem.DisplayName = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.DisplayName).FirstOrDefault();
                        DataItem.CreateDate = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.CreateDate).FirstOrDefault();
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(Count, Data, _Request.Offset, _Request.Limit);
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001", "Details loaded");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetTopVisits", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

        //internal OResponse GetTopSale(OAnaMerchant.Request _Request)
        //{
        //    try
        //    {
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            int Count = _HCoreContext.HCUAccountTransaction
        //               .Where(x => x.TransactionDate > _Request.StartDate && x.TransactionDate < _Request.EndDate
        //               && x.Parent.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Transaction.Success)
        //               .OrderByDescending(x => x.PurchaseAmount)
        //               .Select(x => new OAnaMerchant.Account
        //               {
        //                   ReferenceId = x.Key,
        //                   //MerchantId = x.Id,
        //                   //InvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                   InvoiceAmount = x.PurchaseAmount,
        //               }).Count();
        //            List<OAnaMerchant.Account> Data = _HCoreContext.HCUAccountTransaction
        //               .Where(x => x.TransactionDate > _Request.StartDate && x.TransactionDate < _Request.EndDate
        //               && x.Parent.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Transaction.Success)
        //                .OrderByDescending(x => x.PurchaseAmount)
        //                .Select(x => new OAnaMerchant.Account
        //                {
        //                    ReferenceId = x.Key,
        //                    //InvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                    InvoiceAmount = x.PurchaseAmount,
        //                })
        //                .OrderByDescending(x => x.InvoiceAmount)
        //                .Skip(_Request.Offset)
        //                .Take(_Request.Limit)
        //                .ToList();
        //            if (Data.Count > 0)
        //            {
        //                foreach (var DataItem in Data)
        //                {
        //                    DataItem.ReferenceKey = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.ReferenceKey).FirstOrDefault();
        //                    DataItem.IconUrl = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.IconUrl).FirstOrDefault();
        //                    DataItem.DisplayName = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.DisplayName).FirstOrDefault();
        //                    DataItem.CreateDate = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.CreateDate).FirstOrDefault();
        //                }
        //            }
        //            OList.Response _OResponse = HCoreHelper.GetListResponse(Count, Data, _Request.Offset, _Request.Limit);
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001", "Details loaded");
        //            #endregion
        //        }

        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region Log Bug
        //        HCoreHelper.LogException("GetTopSale", _Exception, _Request.UserReference);
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
        //        #endregion
        //    }
        //}

        /// <summary>
        /// Description: Gets the top sale.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTopSale(OAnaMerchant.Request _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    int Count = _HCoreContext.HCUAccountTransaction
                       .Where(x => x.TransactionDate > _Request.StartDate && x.TransactionDate < _Request.EndDate
                       && x.Parent.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Transaction.Success)
                       .GroupBy(a => a.ParentId)
                       .Select(x => new OAnaMerchant.Account
                       {
                           //ReferenceId = x.Id,
                           ReferenceId = x.Key,
                           //MerchantId = x.Id,
                           //InvoiceAmount = x.Sum(a => a.InvoiceAmount),
                           InvoiceAmount = x.Sum(a => a.PurchaseAmount),
                       }).Count();
                    List<OAnaMerchant.Account> Data = _HCoreContext.HCUAccountTransaction
                       .Where(x => x.TransactionDate > _Request.StartDate && x.TransactionDate < _Request.EndDate
                       && x.Parent.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Transaction.Success)
                       .GroupBy(a => a.ParentId)
                        .Select(x => new OAnaMerchant.Account
                        {
                            //ReferenceId = x.Id,
                            ReferenceId = x.Key,
                            //InvoiceAmount = x.Sum(a => a.InvoiceAmount),
                            InvoiceAmount = x.Sum(a => a.PurchaseAmount),
                        })
                        .OrderByDescending(x => x.InvoiceAmount)
                        .Skip(_Request.Offset)
                        .Take(_Request.Limit)
                        .ToList();
                    if (Data.Count > 0)
                    {
                        foreach (var DataItem in Data)
                        {
                            DataItem.ReferenceKey = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.ReferenceKey).FirstOrDefault();
                            DataItem.IconUrl = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.IconUrl).FirstOrDefault();
                            DataItem.DisplayName = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.DisplayName).FirstOrDefault();
                            DataItem.CreateDate = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.CreateDate).FirstOrDefault();
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(Count, Data, _Request.Offset, _Request.Limit);
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001", "Details loaded");
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetTopSale", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

        /// <summary>
        /// Description: Gets the top category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTopCategory(OAnaMerchant.Request _Request)
        {
            _MerchantActivityStatusCount = new OAnaMerchant.ActivityCount();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    List<OAnaMerchant.Account> Data = _HCoreContext.TUCCategory
                        .Where(x => x.HCUAccountPrimaryCategory.Count > 0)
                        .OrderByDescending(x => x.HCUAccountPrimaryCategory.Count)
                        .Select(x => new OAnaMerchant.Account
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            Name = x.Name,
                            IconUrl = _AppConfig.StorageUrl + x.IconStorage.Path,
                            Count = x.HCUAccountPrimaryCategory.Count,
                        })
                        .Skip(0)
                        .Take(5)
                        .ToList();
                    //List<OAnaMerchant.Account> Data = _HCoreContext.HCUAccount
                    //    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                    //   && x.CountryId == _Request.UserReference.CountryId
                    //   && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate
                    //   && x.PrimaryCategoryId != null
                    //    && x.StatusId == HelperStatus.Default.Active)
                    //    .GroupBy(x => x.PrimaryCategory.Id)
                    //    .Select(x => new OAnaMerchant.Account
                    //    {
                    //        ReferenceId = x.Key,
                    //        ReferenceKey = x.FirstOrDefault().PrimaryCategory.Guid,
                    //        Name = x.FirstOrDefault().PrimaryCategory.Name,
                    //        Count = x.Select(a => a.PrimaryCategory.Id).Distinct().Count(),
                    //    }).OrderByDescending(x => x.Count)
                    //      .Skip(0)
                    //      .Take(5)
                    //      .ToList();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(Data.Count, Data, _Request.Offset, _Request.Limit);
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001", "Details loaded");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetTopCategory", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

        /// <summary>
        /// Description: Gets the sale range.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSaleRange(OAnaMerchant.Request _Request)
        {
            _MerchantActivityStatusCount = new OAnaMerchant.ActivityCount();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    #region Old Code
                    //_SaleRange = new List<OAnaMerchant.SaleRange>();
                    //List<OAnaMerchant.Account> Data = _HCoreContext.HCUAccountTransaction
                    //   .Where(x => x.TransactionDate > _Request.StartDate && x.TransactionDate < _Request.EndDate
                    //   && x.Parent.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.Transaction.Success)
                    //    .GroupBy(x => x.ParentId)
                    //    .Select(x => new OAnaMerchant.Account
                    //    {
                    //        ReferenceId = x.Key,
                    //        InvoiceAmount = x.Sum(a => a.PurchaseAmount),
                    //    })
                    //    .ToList();
                    //if (Data.Count > 0)
                    //{
                    //    double MinimumRange = Data.OrderBy(x => x.InvoiceAmount).FirstOrDefault().InvoiceAmount;
                    //    double MaximumRange = Data.OrderByDescending(x => x.InvoiceAmount).FirstOrDefault().InvoiceAmount;
                    //    double Difference = RoundValueToNext100((MaximumRange - MinimumRange) / 4);
                    //    double LowLimit = 0;
                    //    double HighLimit = Difference;
                    //    for (int i = 1; i <= 4; i++)
                    //    {
                    //        if (i == 4)
                    //        {
                    //            long Merchnats = Data.Where(a => a.InvoiceAmount > LowLimit).Count();
                    //            _SaleRange.Add(new OAnaMerchant.SaleRange
                    //            {
                    //                MinimumRange = LowLimit,
                    //                MaximumRange = HighLimit,
                    //                Count = Merchnats
                    //            });
                    //        }
                    //        else
                    //        {
                    //            long Merchnats = Data.Where(a => a.InvoiceAmount > LowLimit && a.InvoiceAmount < HighLimit).Count();
                    //            _SaleRange.Add(new OAnaMerchant.SaleRange
                    //            {
                    //                MinimumRange = LowLimit,
                    //                MaximumRange = HighLimit,
                    //                Count = Merchnats
                    //            });
                    //        }
                    //        LowLimit += Difference;
                    //        HighLimit += Difference;

                    //    }
                    //    foreach (var DataItem in Data)
                    //    {
                    //        DataItem.ReferenceKey = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.ReferenceKey).FirstOrDefault();
                    //        DataItem.IconUrl = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.IconUrl).FirstOrDefault();
                    //        DataItem.DisplayName = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.DisplayName).FirstOrDefault();
                    //        DataItem.CreateDate = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.CreateDate).FirstOrDefault();
                    //    }
                    //}


                    //#region Send Response
                    //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SaleRange, "HC0001", "Details loaded");
                    //#endregion
                    #endregion

                    #region New Coode
                    _SaleRange = new List<OAnaMerchant.SaleRange>();
                    _SaleRange.Add(new OAnaMerchant.SaleRange
                    {
                        Title = "1M - 2M",
                        Count = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant
                                                                         && x.CountryId == _Request.UserReference.CountryId
                                                                         && x.HCUAccountTransactionParent.Where(m => m.TransactionDate > _Request.StartDate
                                                                         && m.TransactionDate < _Request.EndDate
                                                                         && m.Parent.CountryId == _Request.UserReference.CountryId
                                                                         && m.StatusId == HelperStatus.Transaction.Success).Sum(a => a.PurchaseAmount) < 1000000
                                                                         && x.HCUAccountTransactionParent.Where(m => m.TransactionDate > _Request.StartDate
                                                                         && m.TransactionDate < _Request.EndDate
                                                                         && m.Parent.CountryId == _Request.UserReference.CountryId
                                                                         && m.StatusId == HelperStatus.Transaction.Success).Sum(a => a.PurchaseAmount) < 2000000),

                    });
                    _SaleRange.Add(new OAnaMerchant.SaleRange
                    {
                        Title = "2M - 3M",
                        Count = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant
                                                                         && x.CountryId == _Request.UserReference.CountryId
                                                                         && x.HCUAccountTransactionParent.Where(m => m.TransactionDate > _Request.StartDate
                                                                         && m.TransactionDate < _Request.EndDate
                                                                         && m.Parent.CountryId == _Request.UserReference.CountryId
                                                                         && m.StatusId == HelperStatus.Transaction.Success).Sum(a => a.PurchaseAmount) > 2000000
                                                                         && x.HCUAccountTransactionParent.Where(m => m.TransactionDate > _Request.StartDate
                                                                         && m.TransactionDate < _Request.EndDate
                                                                         && m.Parent.CountryId == _Request.UserReference.CountryId
                                                                         && m.StatusId == HelperStatus.Transaction.Success).Sum(a => a.PurchaseAmount) < 3000000),

                    });
                    _SaleRange.Add(new OAnaMerchant.SaleRange
                    {
                        Title = "3M - 4M",
                        Count = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant
                                                                         && x.CountryId == _Request.UserReference.CountryId
                                                                         && x.HCUAccountTransactionParent.Where(m => m.TransactionDate > _Request.StartDate
                                                                         && m.TransactionDate < _Request.EndDate
                                                                         && m.Parent.CountryId == _Request.UserReference.CountryId
                                                                         && m.StatusId == HelperStatus.Transaction.Success).Sum(a => a.PurchaseAmount) > 3000000
                                                                         && x.HCUAccountTransactionParent.Where(m => m.TransactionDate > _Request.StartDate
                                                                         && m.TransactionDate < _Request.EndDate
                                                                         && m.Parent.CountryId == _Request.UserReference.CountryId
                                                                         && m.StatusId == HelperStatus.Transaction.Success).Sum(a => a.PurchaseAmount) < 4000000),

                    });
                    _SaleRange.Add(new OAnaMerchant.SaleRange
                    {
                        Title = "4M < ",
                        Count = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant
                                                                         && x.CountryId == _Request.UserReference.CountryId
                                                                         && x.HCUAccountTransactionParent.Where(m => m.TransactionDate > _Request.StartDate
                                                                         && m.TransactionDate < _Request.EndDate
                                                                         && m.Parent.CountryId == _Request.UserReference.CountryId
                                                                         && m.StatusId == HelperStatus.Transaction.Success).Sum(a => a.PurchaseAmount) > 4000000),

                    });
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SaleRange, "HC0001", "Details loaded");
                    #endregion
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetSaleRange", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

        /// <summary>
        /// Description: Rounds the value to next100.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>System.Int32.</returns>
        private static int RoundValueToNext100(double value)
        {
            int result = (int)Math.Round(value / 100, 0, MidpointRounding.AwayFromZero);

            if (value > 0 && result == 0)
            {
                result = 1;
            }
            return (int)result * 100;
        }

        /// <summary>
        /// Description: Gets the reward range.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRewardRange(OAnaMerchant.Request _Request)
        {
            _MerchantActivityStatusCount = new OAnaMerchant.ActivityCount();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    #region Old Code
                    _SaleRange = new List<OAnaMerchant.SaleRange>();
                    //List<OAnaMerchant.Account> Data = _HCoreContext.TUCLoyalty
                    //   .Where(x =>
                    //   x.LoyaltyTypeId == Helpers.Loyalty.Reward
                    //   && x.Merchant.CountryId == _Request.UserReference.CountryId && x.TransactionDate > _Request.StartDate && x.TransactionDate < _Request.EndDate
                    //   && x.StatusId == HelperStatus.Transaction.Success)
                    //    .GroupBy(x => x.MerchantId)
                    //    .Select(x => new OAnaMerchant.Account
                    //    {
                    //        ReferenceId = x.Key,
                    //        Amount = x.Sum(a => a.TotalAmount),
                    //    })
                    //    .ToList();
                    //if (Data.Count > 0)
                    //{
                    //    double MinimumRange = Data.OrderBy(x => x.Amount).FirstOrDefault().InvoiceAmount;
                    //    double MaximumRange = Data.OrderByDescending(x => x.Amount).FirstOrDefault().InvoiceAmount;
                    //    double Difference = RoundValueToNext100((MaximumRange - MinimumRange) / 4);
                    //    double LowLimit = 0; 
                    //    double HighLimit = Difference;
                    //    for (int i = 1; i <= 4; i++)
                    //    {
                    //        if (i == 4)
                    //        {
                    //            long Merchnats = Data.Where(a => a.InvoiceAmount > LowLimit ).Count();
                    //            _SaleRange.Add(new OAnaMerchant.SaleRange
                    //            {
                    //                MinimumRange = LowLimit,
                    //                MaximumRange = HighLimit,
                    //                Count = Merchnats
                    //            });
                    //        }
                    //        else
                    //        {
                    //            long Merchnats = Data.Where(a => a.InvoiceAmount > LowLimit && a.InvoiceAmount < HighLimit).Count();
                    //            _SaleRange.Add(new OAnaMerchant.SaleRange
                    //            {
                    //                MinimumRange = LowLimit,
                    //                MaximumRange = HighLimit,
                    //                Count = Merchnats
                    //            });
                    //        }     

                    //        LowLimit += Difference;
                    //        HighLimit += Difference;
                    //    }
                    //    //foreach (var DataItem in Data)
                    //    //{
                    //    //    DataItem.ReferenceKey = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.ReferenceKey).FirstOrDefault();
                    //    //    DataItem.IconUrl = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.IconUrl).FirstOrDefault();
                    //    //    DataItem.DisplayName = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.DisplayName).FirstOrDefault();
                    //    //    DataItem.CreateDate = HCore.Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == DataItem.ReferenceId).Select(x => x.CreateDate).FirstOrDefault();
                    //    //}
                    //}
                    #endregion

                    #region New Code
                    _SaleRange = new List<OAnaMerchant.SaleRange>();
                    _SaleRange.Add(new OAnaMerchant.SaleRange
                    {
                        Title = "5k - 10k",
                        Count = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant
                                                              && x.CountryId == _Request.UserReference.CountryId
                                                              && x.HCUAccountTransactionParent.Where(a => a.TransactionDate > _Request.StartDate
                                                              && a.TransactionDate < _Request.EndDate && a.StatusId == HelperStatus.Transaction.Success
                                                              && a.Type.SubParentId == TransactionTypeCategory.Reward
                                                              && a.ModeId == TransactionMode.Credit
                                                              && a.TypeId == TransactionType.CardReward || a.TypeId == TransactionType.CashReward
                                                              && (((a.TypeId != TransactionType.ThankUCashPlusCredit && a.TypeId != TransactionType.TucSuperCash)
                                                              && (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUCBlack))
                                                              || (a.SourceId == TransactionSource.ThankUCashPlus && a.SourceId != TransactionSource.TUCSuperCash))
                                                              ).Sum(m => m.ReferenceAmount) < 5000
                                                              && x.HCUAccountTransactionParent.Where(a => a.TransactionDate > _Request.StartDate
                                                              && a.TransactionDate < _Request.EndDate && a.StatusId == HelperStatus.Transaction.Success
                                                              && a.Type.SubParentId == TransactionTypeCategory.Reward
                                                              && a.ModeId == TransactionMode.Credit
                                                              && a.TypeId == TransactionType.CardReward || a.TypeId == TransactionType.CashReward
                                                              && (((a.TypeId != TransactionType.ThankUCashPlusCredit && a.TypeId != TransactionType.TucSuperCash)
                                                              && (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUCBlack))
                                                              || (a.SourceId == TransactionSource.ThankUCashPlus && a.SourceId != TransactionSource.TUCSuperCash))
                                                              ).Sum(m => m.ReferenceAmount) < 10000),
                    });
                    _SaleRange.Add(new OAnaMerchant.SaleRange
                    {
                        Title = "10k - 15k",
                        Count = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant
                                                              && x.CountryId == _Request.UserReference.CountryId
                                                              && x.HCUAccountTransactionParent.Where(a => a.TransactionDate > _Request.StartDate
                                                              && a.TransactionDate < _Request.EndDate && a.StatusId == HelperStatus.Transaction.Success
                                                              && a.Type.SubParentId == TransactionTypeCategory.Reward
                                                              && a.ModeId == TransactionMode.Credit
                                                              && a.TypeId == TransactionType.CardReward || a.TypeId == TransactionType.CashReward
                                                              && (((a.TypeId != TransactionType.ThankUCashPlusCredit && a.TypeId != TransactionType.TucSuperCash)
                                                              && (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUCBlack))
                                                              || (a.SourceId == TransactionSource.ThankUCashPlus && a.SourceId != TransactionSource.TUCSuperCash))
                                                              ).Sum(m => m.ReferenceAmount) > 10000
                                                              && x.HCUAccountTransactionParent.Where(a => a.TransactionDate > _Request.StartDate
                                                              && a.TransactionDate < _Request.EndDate && a.StatusId == HelperStatus.Transaction.Success
                                                              && a.Type.SubParentId == TransactionTypeCategory.Reward
                                                              && a.ModeId == TransactionMode.Credit
                                                              && a.TypeId == TransactionType.CardReward || a.TypeId == TransactionType.CashReward
                                                              && (((a.TypeId != TransactionType.ThankUCashPlusCredit && a.TypeId != TransactionType.TucSuperCash)
                                                              && (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUCBlack))
                                                              || (a.SourceId == TransactionSource.ThankUCashPlus && a.SourceId != TransactionSource.TUCSuperCash))
                                                              ).Sum(m => m.ReferenceAmount) < 15000),
                    });
                    _SaleRange.Add(new OAnaMerchant.SaleRange
                    {
                        Title = "15k - 20k",
                        Count = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant
                                                              && x.CountryId == _Request.UserReference.CountryId
                                                              && x.HCUAccountTransactionParent.Where(a => a.TransactionDate > _Request.StartDate
                                                              && a.TransactionDate < _Request.EndDate && a.StatusId == HelperStatus.Transaction.Success
                                                              && a.Type.SubParentId == TransactionTypeCategory.Reward
                                                              && a.ModeId == TransactionMode.Credit
                                                              && a.TypeId == TransactionType.CardReward || a.TypeId == TransactionType.CashReward
                                                              && (((a.TypeId != TransactionType.ThankUCashPlusCredit && a.TypeId != TransactionType.TucSuperCash)
                                                              && (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUCBlack))
                                                              || (a.SourceId == TransactionSource.ThankUCashPlus && a.SourceId != TransactionSource.TUCSuperCash))
                                                              ).Sum(m => m.ReferenceAmount) > 15000
                                                              && x.HCUAccountTransactionParent.Where(a => a.TransactionDate > _Request.StartDate
                                                              && a.TransactionDate < _Request.EndDate && a.StatusId == HelperStatus.Transaction.Success
                                                              && a.Type.SubParentId == TransactionTypeCategory.Reward
                                                              && a.ModeId == TransactionMode.Credit
                                                              && a.TypeId == TransactionType.CardReward || a.TypeId == TransactionType.CashReward
                                                              && (((a.TypeId != TransactionType.ThankUCashPlusCredit && a.TypeId != TransactionType.TucSuperCash)
                                                              && (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUCBlack))
                                                              || (a.SourceId == TransactionSource.ThankUCashPlus && a.SourceId != TransactionSource.TUCSuperCash))
                                                              ).Sum(m => m.ReferenceAmount) < 20000),
                    });
                    _SaleRange.Add(new OAnaMerchant.SaleRange
                    {
                        Title = "20k < ",
                        Count = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Merchant
                                                              && x.CountryId == _Request.UserReference.CountryId
                                                              && x.HCUAccountTransactionParent.Where(a => a.TransactionDate > _Request.StartDate
                                                              && a.TransactionDate < _Request.EndDate && a.StatusId == HelperStatus.Transaction.Success
                                                              && a.Type.SubParentId == TransactionTypeCategory.Reward
                                                              && a.ModeId == TransactionMode.Credit
                                                              && a.TypeId == TransactionType.CardReward || a.TypeId == TransactionType.CashReward
                                                              && (((a.TypeId != TransactionType.ThankUCashPlusCredit && a.TypeId != TransactionType.TucSuperCash)
                                                              && (a.SourceId == TransactionSource.TUC || a.SourceId == TransactionSource.TUCBlack))
                                                              || (a.SourceId == TransactionSource.ThankUCashPlus && a.SourceId != TransactionSource.TUCSuperCash))
                                                              ).Sum(m => m.ReferenceAmount) > 20000),
                    });
                    #endregion

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SaleRange, "HC0001", "Details loaded");
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetRewardRange", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
    }
}
