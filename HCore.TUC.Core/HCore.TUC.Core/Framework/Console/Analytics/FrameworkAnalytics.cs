//==================================================================================
// FileName: FrameworkAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to sales and loyalty analystics
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Operations;
using HCore.Helper;
using HCore.TUC.Core.Object.Console.Analytics;
using HCore.TUC.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.Framework.Console.Analytics
{
    public class FrameworkAnalytics
    {
        private class OTrList
        {
            public double RewardAmount { get; set; }
            public double RedeemAmount { get; set; }
            public DateTime TransactionDate { get; set; }
            public double InvoiceAmount { get; set; }
            public long? UserAccountId { get; set; }
            public double? CommissionAmount { get; set; }
        }
        List<OTrList> _OTrList;
        OAnalytics.SalesOverview _SalesOverview;
        OAnalytics.Loyalty _LoyaltyOverview;
        OCustomerAnalytics.Response _CustomerAnalytics;
        OCustomerAnalytics.Count _CustomerAnalyticsCount;
        List<OCustomerAnalytics.User> _CustomerAnalyticsUsers;
        List<OCustomerAnalytics.Range> _CustomerAnalyticsRange;
        HCoreContext _HCoreContext;
        OMerchantAnalytics.Response _MerchantAnalyticsResponse;
        OMerchantAnalytics.Count _MerchantAnalyticsCount;
        List<OAnalytics.Sale> _SalesHistory;
        OCustomerAnalytics.StatusCount _CustomerStatusCount;
        OCustomerAnalytics.AppDownloadStatus _CustomerAppDownloadStatus;
        OCustomerAnalytics.ThankUCashCustomersStatus _ThankUCashCustomersStatus;
        OCustomerAnalytics.MerchantCustomersStatus _MerchantCustomersStatus;

        /// <summary>
        /// Description: Gets the sales overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSalesOverview(OAnalytics.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    _SalesOverview = new OAnalytics.SalesOverview();
                    _SalesOverview.ActiveMerchants = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Merchant && x.HCUAccountTransactionParent.Any(a => a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate && x.StatusId == HelperStatus.Default.Active));
                    _SalesOverview.TotalMerchants = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Merchant);
                    #region Sales Overiew
                    _SalesOverview.Transactions = (long)_HCoreContext.HCUAccountTransaction
                         .Count(x => x.Account.CountryId == _Request.UserReference.SystemCountry
                         && x.Account.AccountTypeId == UserAccountType.Appuser
                         && x.TransactionDate > _Request.StartDate && x.TransactionDate < _Request.EndDate
                                                      && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus)
                                                      && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay));
                    if (_SalesOverview.Transactions > 0)
                    {
                        _SalesOverview.Customers = (long)_HCoreContext.HCUAccountTransaction.Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry
                         && x.Account.AccountTypeId == UserAccountType.Appuser
                        && x.TransactionDate > _Request.StartDate && x.TransactionDate < _Request.EndDate
                                                     && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus)
                                                     && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay))
                            .Select(x => x.Customer).Distinct().Count();
                        _SalesOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                           .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry
                         && x.Account.AccountTypeId == UserAccountType.Appuser
                         && x.TransactionDate > _Request.StartDate && x.TransactionDate < _Request.EndDate
                                                      && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus)
                                                      && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay))
                            .Sum(x => (double?)x.PurchaseAmount) ?? 0;
                        //_SalesOverview.Customers = (long?)_HCoreContext.HCUAccountTransaction
                        //                       .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                        //                    && x.TransactionDate.AddHours(1) > _Request.StartDate
                        //                    && x.TransactionDate.AddHours(1) < _Request.EndDate
                        //                    && x.StatusId == HelperStatus.Transaction.Success
                        //                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus)
                        //                    && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)).Select(x => x.AccountId).Distinct().Count() ?? 0;
                        //_SalesOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                        //                    .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                        //                    && x.TransactionDate.AddHours(1) > _Request.StartDate
                        //                    && x.TransactionDate.AddHours(1) < _Request.EndDate
                        //                    && x.StatusId == HelperStatus.Transaction.Success
                        //                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus)
                        //                    && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                        if (_Request.AmountDistribution)
                        {
                            //_SalesOverview.CardTransaction = (long)_HCoreContext.anxsale_paymentmethod.Where(x => x.PaymentModeId == TransactionType.CardReward && x.Date > _Request.StartDate && x.Date < _Request.EndDate).Sum(x => x.Customers);
                            _SalesOverview.CardTransaction = (long)_HCoreContext.cmt_sale.Where(x => x.merchant.CountryId == _Request.UserReference.SystemCountry && x.status_id == HelperStatus.Transaction.Success && x.payment_mode_id == TransactionType.CardReward && x.transaction_date > _Request.StartDate && x.transaction_date < _Request.EndDate).Count();
                            //_SalesOverview.CardInvoiceAmount = _HCoreContext.anxsale_paymentmethod.Where(x => x.PaymentModeId == TransactionType.CardReward && x.Date > _Request.StartDate && x.Date < _Request.EndDate).Sum(x => x.InvoiceAmount) ?? 0;
                            _SalesOverview.CardInvoiceAmount = _HCoreContext.cmt_sale.Where(x => x.payment_mode_id == TransactionType.CardReward && x.status_id == HelperStatus.Transaction.Success && x.transaction_date > _Request.StartDate && x.transaction_date < _Request.EndDate).Sum(x => (double?)x.invoice_amount) ?? 0;

                            _SalesOverview.CashTransaction = (long)_HCoreContext.cmt_sale.Where(x => x.merchant.CountryId == _Request.UserReference.SystemCountry && x.status_id == HelperStatus.Transaction.Success && x.payment_mode_id == TransactionType.CashReward && x.transaction_date > _Request.StartDate && x.transaction_date < _Request.EndDate).Count();
                            //_SalesOverview.CashTransaction = (long)_HCoreContext.anxsale_paymentmethod.Where(x => x.PaymentModeId == TransactionType.CashReward && x.Date > _Request.StartDate && x.Date < _Request.EndDate).Sum(x => x.Customers);
                            //_SalesOverview.CashInvoiceAmount = _HCoreContext.anxsale_paymentmethod.Where(x => x.PaymentModeId == TransactionType.CashReward && x.StatusId == HelperStatus.Transaction.Success && x.Date > _Request.StartDate && x.Date < _Request.EndDate).Sum(x => x.InvoiceAmount) ?? 0;
                            _SalesOverview.CashInvoiceAmount = _HCoreContext.cmt_sale.Where(x => x.payment_mode_id == TransactionType.CashReward && x.transaction_date > _Request.StartDate && x.status_id == HelperStatus.Transaction.Success && x.transaction_date < _Request.EndDate).Sum(x => (double?)x.invoice_amount) ?? 0;



                            //_SalesOverview.CardTransaction = _HCoreContext.HCUAccountTransaction
                            //                                      .Count(m => m.TransactionDate > _Request.StartDate
                            //                                       && m.TransactionDate < _Request.EndDate
                            //                                       && m.TypeId == TransactionType.CardReward
                            //                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                            //                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                            //                                       && m.StatusId == HelperStatus.Transaction.Success
                            //                                     );
                            //if (_SalesOverview.CardTransaction > 0)
                            //{
                            //    _SalesOverview.CardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                            //                                    .Where(m => m.TransactionDate > _Request.StartDate
                            //                                     && m.TransactionDate < _Request.EndDate
                            //                                     && m.TypeId == TransactionType.CardReward
                            //                                     && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                            //                                     || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                            //                                     && m.StatusId == HelperStatus.Transaction.Success
                            //                                   ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                            //}
                            //_SalesOverview.CashTransaction = _HCoreContext.HCUAccountTransaction
                            //                                   .Count(m => m.TransactionDate > _Request.StartDate
                            //                                    && m.TransactionDate < _Request.EndDate
                            //                                    && m.TypeId == TransactionType.CashReward
                            //                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                            //                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                            //                                    && m.StatusId == HelperStatus.Transaction.Success
                            //                                  );
                            //if (_SalesOverview.CashTransaction > 0)
                            //{
                            //    _SalesOverview.CashInvoiceAmount = _HCoreContext.HCUAccountTransaction
                            //                                    .Where(m => m.TransactionDate > _Request.StartDate
                            //                                     && m.TransactionDate < _Request.EndDate
                            //                                     && m.TypeId == TransactionType.CashReward
                            //                                     && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                            //                                     || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                            //                                     && m.StatusId == HelperStatus.Transaction.Success
                            //                                   ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                            //}
                        }
                    }

                    //_SalesOverview.Transactions = (long?)_HCoreContext.HCUAccountTransaction
                    //                              .Count(x => x.Account.AccountTypeId == UserAccountType.Appuser
                    //                          && x.TransactionDate.AddHours(1) > _Request.StartDate
                    //                          && x.TransactionDate.AddHours(1) < _Request.EndDate
                    //                          && x.StatusId == HelperStatus.Transaction.Success
                    //                          && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus)
                    //                          && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)) ?? 0;
                    //if (_SalesOverview.Transactions > 0)
                    //{
                    //    _SalesOverview.Customers = (long?)_HCoreContext.HCUAccountTransaction
                    //                           .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                    //                        && x.TransactionDate.AddHours(1) > _Request.StartDate
                    //                        && x.TransactionDate.AddHours(1) < _Request.EndDate
                    //                        && x.StatusId == HelperStatus.Transaction.Success
                    //                        && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus)
                    //                        && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)).Select(x => x.AccountId).Distinct().Count() ?? 0;
                    //    _SalesOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                        .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                    //                        && x.TransactionDate.AddHours(1) > _Request.StartDate
                    //                        && x.TransactionDate.AddHours(1) < _Request.EndDate
                    //                        && x.StatusId == HelperStatus.Transaction.Success
                    //                        && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus)
                    //                        && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward)).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                    //    if (_Request.AmountDistribution)
                    //    {
                    //        _SalesOverview.CardTransaction = _HCoreContext.HCUAccountTransaction
                    //                                              .Count(m => m.TransactionDate > _Request.StartDate
                    //                                               && m.TransactionDate < _Request.EndDate
                    //                                               && m.TypeId == TransactionType.CardReward
                    //                                               && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                    //                                               || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                               && m.StatusId == HelperStatus.Transaction.Success
                    //                                             );
                    //        if (_SalesOverview.CardTransaction > 0)
                    //        {
                    //            _SalesOverview.CardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                                            .Where(m => m.TransactionDate > _Request.StartDate
                    //                                             && m.TransactionDate < _Request.EndDate
                    //                                             && m.TypeId == TransactionType.CardReward
                    //                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                    //                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                             && m.StatusId == HelperStatus.Transaction.Success
                    //                                           ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                    //        }
                    //        _SalesOverview.CashTransaction = _HCoreContext.HCUAccountTransaction
                    //                                           .Count(m => m.TransactionDate > _Request.StartDate
                    //                                            && m.TransactionDate < _Request.EndDate
                    //                                            && m.TypeId == TransactionType.CashReward
                    //                                            && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                    //                                            || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                            && m.StatusId == HelperStatus.Transaction.Success
                    //                                          );
                    //        if (_SalesOverview.CashTransaction > 0)
                    //        {
                    //            _SalesOverview.CashInvoiceAmount = _HCoreContext.HCUAccountTransaction
                    //                                            .Where(m => m.TransactionDate > _Request.StartDate
                    //                                             && m.TransactionDate < _Request.EndDate
                    //                                             && m.TypeId == TransactionType.CashReward
                    //                                             && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                    //                                             || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                    //                                             && m.StatusId == HelperStatus.Transaction.Success
                    //                                           ).Sum(m => (double?)m.PurchaseAmount) ?? 0;
                    //        }
                    //    }
                    //}

                    #endregion
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SalesOverview, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetSalesOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the loyalty overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLoyaltyOverview(OAnalytics.Request _Request)
        {
            _LoyaltyOverview = new OAnalytics.Loyalty();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var Transactions = _HCoreContext.HCUAccountTransaction
                                 .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                 && x.Account.CountryId == _Request.UserReference.SystemCountry
                                 && x.TransactionDate > _Request.StartDate
                                 && x.TransactionDate < _Request.EndDate
                                        && x.StatusId == HelperStatus.Transaction.Success
                                 && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus)
                                 && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward))
                                 .Select(x => new
                                 {
                                     UserAccountId = x.AccountId,
                                     InvoiceAmount = x.PurchaseAmount
                                 }).ToList();
                    var TM = Transactions.GroupBy(x => x.UserAccountId)
                               .Select(x => new
                               {
                                   UserAccountId = x.Key,
                                   Count = x.Count(),
                                   InvoiceAmount = x.Sum(a => a.InvoiceAmount),

                               }).ToList();
                    _LoyaltyOverview.NewCustomers = TM.Count(x => x.Count < 2);
                    _LoyaltyOverview.NewCustomerInvoiceAmount = TM.Where(x => x.Count < 2).Sum(x => x.InvoiceAmount);
                    _LoyaltyOverview.RepeatingCustomers = TM.Count(x => x.Count > 1);
                    _LoyaltyOverview.VisitsByRepeatingCustomers = TM.Sum(x => x.Count);
                    _LoyaltyOverview.RepeatingCustomerInvoiceAmount = TM.Where(x => x.Count > 1).Sum(x => x.InvoiceAmount);
                    _LoyaltyOverview.Transaction = Transactions.Count();
                    _LoyaltyOverview.TransactionInvoiceAmount = Transactions.Sum(x => x.InvoiceAmount);
                    _LoyaltyOverview.RewardTransaction = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TransactionDate > _Request.StartDate
                                                        && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && (((m.ModeId == TransactionMode.Credit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                      ).Count();
                    if (_LoyaltyOverview.RewardTransaction > 0)
                    {
                        _LoyaltyOverview.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TransactionDate > _Request.StartDate
                                                        && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && (((m.ModeId == TransactionMode.Credit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                       ).Sum(x => (double?)x.TotalAmount) ?? 0;

                        _LoyaltyOverview.RewardCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.TransactionDate > _Request.StartDate
                                                      && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                      && m.TransactionDate < _Request.EndDate
                                                      && m.TotalAmount > 0
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                      && (((m.ModeId == TransactionMode.Credit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                     ).Sum(x => (double?)x.ParentTransaction.ComissionAmount) ?? 0;
                        _LoyaltyOverview.RewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TransactionDate > _Request.StartDate
                                                      && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && (((m.ModeId == TransactionMode.Credit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                       ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                    }
                    _LoyaltyOverview.TucRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TransactionDate > _Request.StartDate
                                                      && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                      ).Count();
                    if (_LoyaltyOverview.TucRewardTransaction > 0)
                    {
                        _LoyaltyOverview.TucRewardAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TransactionDate > _Request.StartDate
                                                      && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                      ).Sum(x => (double?)x.TotalAmount) ?? 0;

                        _LoyaltyOverview.TucRewardCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.TransactionDate > _Request.StartDate
                                                      && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                       && m.TransactionDate < _Request.EndDate
                                                       && m.TotalAmount > 0
                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                       && (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                     ).Sum(x => (double?)x.ParentTransaction.ComissionAmount) ?? 0;

                        _LoyaltyOverview.TucRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TransactionDate > _Request.StartDate
                                                      && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                      ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                    }

                    _LoyaltyOverview.TucPlusRewardTransaction = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TransactionDate > _Request.StartDate
                                                      && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                      ).Count();
                    if (_LoyaltyOverview.TucPlusRewardTransaction > 0)
                    {
                        _LoyaltyOverview.TucPlusRewardAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TransactionDate > _Request.StartDate
                                                      && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                     ).Sum(x => (double?)x.TotalAmount) ?? 0;

                        _LoyaltyOverview.TucPlusRewardCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.TransactionDate > _Request.StartDate
                                                      && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                      && m.TransactionDate < _Request.EndDate
                                                      && m.TotalAmount > 0
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                      && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                   ).Sum(x => (double?)x.ParentTransaction.ComissionAmount) ?? 0;
                        _LoyaltyOverview.TucPlusRewardInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TransactionDate > _Request.StartDate
                                                      && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus
                                                     ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                    }


                    _LoyaltyOverview.TucPlusRewardClaimTransaction = _HCoreContext.HCUAccountTransaction
                                                       .Count(m => m.TransactionDate > _Request.StartDate
                                                      && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.ThankUCashPlus
                                                     );
                    if (_LoyaltyOverview.TucPlusRewardClaimTransaction > 0)
                    {
                        _LoyaltyOverview.TucPlusRewardClaimAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TransactionDate > _Request.StartDate
                                                      && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.ThankUCashPlus
                                                     ).Sum(x => (double?)x.TotalAmount) ?? 0;

                        _LoyaltyOverview.TucPlusRewardClaimInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TransactionDate > _Request.StartDate
                                                      && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.ThankUCashPlus
                                                     ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                    }




                    _LoyaltyOverview.RedeemTransaction = _HCoreContext.HCUAccountTransaction
                                                       .Count(m => m.TransactionDate > _Request.StartDate
                                                      && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                         && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                        && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC
                                                     );
                    if (_LoyaltyOverview.RedeemTransaction > 0)
                    {
                        _LoyaltyOverview.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TransactionDate > _Request.StartDate
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                         && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                        && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC
                                                     ).Sum(x => (double?)x.TotalAmount) ?? 0;
                        _LoyaltyOverview.RedeemInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TransactionDate > _Request.StartDate
                                                      && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                         && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                        && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC
                                                     ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                    }
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _LoyaltyOverview, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetLoyaltyOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

        /// <summary>
        /// Description: Gets the customers status overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomersStatusOverview(OList.Request _Request)
        {
            try
            {
                _CustomerStatusCount = new OCustomerAnalytics.StatusCount();
                _ThankUCashCustomersStatus = new OCustomerAnalytics.ThankUCashCustomersStatus();
                _MerchantCustomersStatus = new OCustomerAnalytics.MerchantCustomersStatus();
                _CustomerAppDownloadStatus = new OCustomerAnalytics.AppDownloadStatus();

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
                DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
                DateTime T7DayStart = TDayStart.AddDays(-7);
                DateTime T7DayEnd = TDayStart.AddSeconds(1);
                DateTime TDeadDayEnd = T7DayStart;

                using (_HCoreContext = new HCoreContext())
                {

                    if (_Request.Type == "thankucash")
                    {
                        _ThankUCashCustomersStatus = new OCustomerAnalytics.ThankUCashCustomersStatus();
                        _ThankUCashCustomersStatus.Total = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.CountryId && (x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber));
                        _ThankUCashCustomersStatus.Active = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.CountryId && (x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber) && x.StatusId == HelperStatus.Default.Active);
                        _ThankUCashCustomersStatus.Idle = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.CountryId && (x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber) && x.StatusId == HelperStatus.Default.Suspended);
                        _ThankUCashCustomersStatus.Dead = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.CountryId && (x.AccountTypeId == UserAccountType.Appuser && x.DisplayName != x.MobileNumber) && x.StatusId == HelperStatus.Default.Blocked);
                        _CustomerStatusCount.ThankUCashCustomers = _ThankUCashCustomersStatus;
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ThankUCashCustomersStatus, "HC0001", "Details loaded");
                        #endregion
                    }
                    if (_Request.Type == "merchant")
                    {
                        _MerchantCustomersStatus = new OCustomerAnalytics.MerchantCustomersStatus();
                        _MerchantCustomersStatus.Total = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.CountryId && (x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber));
                        _MerchantCustomersStatus.Active = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.CountryId && (x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber) && x.StatusId == HelperStatus.Default.Active);
                        _MerchantCustomersStatus.Idle = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.CountryId && (x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber) && x.StatusId == HelperStatus.Default.Suspended);
                        _MerchantCustomersStatus.Dead = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.CountryId && (x.AccountTypeId == UserAccountType.Appuser && x.DisplayName == x.MobileNumber) && x.StatusId == HelperStatus.Default.Blocked);
                        _CustomerStatusCount.MerchantCustomers = _MerchantCustomersStatus;
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _MerchantCustomersStatus, "HC0001", "Details loaded");
                        #endregion
                    }
                    if (_Request.Type == "appdownloads")
                    {
                        _CustomerAppDownloadStatus = new OCustomerAnalytics.AppDownloadStatus();
                        _CustomerAppDownloadStatus.Total = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.CountryId && x.AccountTypeId == UserAccountType.Appuser && x.HCUAccountSessionAccount.Any());
                        _CustomerAppDownloadStatus.Active = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.CountryId && x.AccountTypeId == UserAccountType.Appuser && (x.HCUAccountSessionAccount.OrderByDescending(a => a.LoginDate).Any(a => a.LastActivityDate > TDayStart && a.LastActivityDate < TDayEnd)));
                        _CustomerAppDownloadStatus.Idle = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.CountryId && x.AccountTypeId == UserAccountType.Appuser && (x.HCUAccountSessionAccount.OrderByDescending(a => a.LoginDate).Any(a => a.LastActivityDate > T7DayStart && a.LastActivityDate < T7DayEnd)));
                        _CustomerAppDownloadStatus.Dead = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.CountryId && x.AccountTypeId == UserAccountType.Appuser && (x.HCUAccountSessionAccount.OrderByDescending(a => a.LoginDate).Any(a => a.LastActivityDate < TDeadDayEnd)));
                        _CustomerStatusCount.AppDownloads = _CustomerAppDownloadStatus;
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerAppDownloadStatus, "HC0001", "Details loaded");
                        #endregion
                    }

                    else
                    {
                        _CustomerStatusCount.Total = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.CountryId && (x.AccountTypeId == UserAccountType.Appuser || x.AccountTypeId == UserApplicationStatus.AppInstalled));
                        _CustomerStatusCount.Active = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.CountryId && (x.AccountTypeId == UserAccountType.Appuser || x.AccountTypeId == UserApplicationStatus.AppInstalled) && x.StatusId == HelperStatus.Default.Active);
                        _CustomerStatusCount.Suspended = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.CountryId && (x.AccountTypeId == UserAccountType.Appuser || x.AccountTypeId == UserApplicationStatus.AppInstalled) && x.StatusId == HelperStatus.Default.Suspended);
                        _CustomerStatusCount.Blocked = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.CountryId && (x.AccountTypeId == UserAccountType.Appuser || x.AccountTypeId == UserApplicationStatus.AppInstalled) && x.StatusId == HelperStatus.Default.Blocked);
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerStatusCount, "HC0001", "Details loaded");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersStatusOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the customers overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomersOverview(OAnalytics.Request _Request)
        {
            _CustomerAnalytics = new OCustomerAnalytics.Response();
            _CustomerAnalyticsCount = new OCustomerAnalytics.Count();
            _LoyaltyOverview = new OAnalytics.Loyalty();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.Type == "lite")
                    {
                        _CustomerAnalyticsCount.Total = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Appuser);
                        _CustomerAnalyticsCount.Active = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Appuser && x.HCUAccountTransactionAccount.Any(a => a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate) == true);
                    }
                    else
                    {
                        _CustomerAnalyticsCount.Total = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Appuser);
                        _CustomerAnalyticsCount.New = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Appuser && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate);
                        _CustomerAnalyticsCount.Active = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Appuser && x.HCUAccountTransactionAccount.Any(a => a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate) == true);
                        _CustomerAnalyticsCount.Tuc = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Appuser && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate && x.DisplayName != x.MobileNumber);
                        _CustomerAnalyticsCount.NonTuc = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Appuser && x.CreateDate > _Request.StartDate && x.CreateDate < _Request.EndDate && x.DisplayName == x.MobileNumber);
                        _CustomerAnalytics.Counts = _CustomerAnalyticsCount;
                        _CustomerAnalytics.TopSpenders = _HCoreContext.HCUAccount.Where(x => x.CountryId == _Request.UserReference.SystemCountry &&
                        x.AccountTypeId == UserAccountType.Appuser
                        && x.StatusId == HelperStatus.Default.Active
                        && x.HCUAccountTransactionAccount.Any(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate) == true)
                           .Select(x => new OCustomerAnalytics.User
                           {
                               ReferenceId = x.Id,
                               ReferenceKey = x.Guid,
                               DisplayName = x.DisplayName,
                               MobileNumber = x.MobileNumber,
                               IconUrl = x.IconStorage.Path,
                               InvoiceAmount = x.HCUAccountTransactionAccount.Where(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate).Sum(a => a.PurchaseAmount),
                           })
                           .OrderByDescending(x => x.InvoiceAmount)
                           .Skip(0)
                           .Take(5)
                           .ToList();
                        if (_CustomerAnalytics.TopSpenders.Count > 0)
                        {
                            foreach (var TopSpender in _CustomerAnalytics.TopSpenders)
                            {
                                if (!string.IsNullOrEmpty(TopSpender.IconUrl))
                                {
                                    TopSpender.IconUrl = _AppConfig.StorageUrl + TopSpender.IconUrl;
                                }
                                else
                                {
                                    TopSpender.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                        }
                        _CustomerAnalytics.TopVisitors = _HCoreContext.HCUAccount.Where(x =>
                      x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Appuser
                        && x.StatusId == HelperStatus.Default.Active
                        && x.HCUAccountTransactionAccount.Any(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate) == true)
                         .Select(x => new OCustomerAnalytics.User
                         {
                             ReferenceId = x.Id,
                             ReferenceKey = x.Guid,
                             DisplayName = x.DisplayName,
                             MobileNumber = x.MobileNumber,
                             IconUrl = x.IconStorage.Path,
                             Visits = x.HCUAccountTransactionAccount.Count(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate),
                         })
                         .OrderByDescending(x => x.Visits)
                         .Skip(0)
                         .Take(5)
                         .ToList();
                        if (_CustomerAnalytics.TopVisitors.Count > 0)
                        {
                            foreach (var TopVisitor in _CustomerAnalytics.TopVisitors)
                            {
                                if (!string.IsNullOrEmpty(TopVisitor.IconUrl))
                                {
                                    TopVisitor.IconUrl = _AppConfig.StorageUrl + TopVisitor.IconUrl;
                                }
                                else
                                {
                                    TopVisitor.IconUrl = _AppConfig.Default_Icon;
                                }
                            }
                        }
                        DateTime TodayTime = HCoreHelper.GetGMTDateTime();
                        _CustomerAnalyticsRange = new List<OCustomerAnalytics.Range>();
                        _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                        {
                            Title = "Male",
                            Count = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Appuser && x.GenderId == Helpers.Gender.Male && x.HCUAccountTransactionAccount.Any(a => a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate) == true),
                            Visits = _HCoreContext.HCUAccountTransaction.Count(x =>
                            x.Account.AccountTypeId == UserAccountType.Appuser
                            && x.Account.GenderId == Helpers.Gender.Male
                           && x.StatusId == HelperStatus.Transaction.Success
                            && x.TransactionDate > _Request.StartDate
                            && x.TransactionDate < _Request.EndDate),
                            InvoiceAmount = _HCoreContext.HCUAccountTransaction.Where(x =>
                           x.Account.AccountTypeId == UserAccountType.Appuser
                            && x.StatusId == HelperStatus.Transaction.Success
                           && x.Account.GenderId == Helpers.Gender.Male
                           && x.TransactionDate > _Request.StartDate
                           && x.TransactionDate < _Request.EndDate)
                            .Sum(x => x.PurchaseAmount),
                        });
                        _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                        {
                            Title = "Female",
                            Count = _HCoreContext.HCUAccount.Count(x => x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Appuser && x.GenderId == Helpers.Gender.Female && x.HCUAccountTransactionAccount.Any(a => a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate) == true),
                            Visits = _HCoreContext.HCUAccountTransaction.Count(x =>
                            x.Account.AccountTypeId == UserAccountType.Appuser
                            && x.Account.GenderId == Helpers.Gender.Female
                             && x.StatusId == HelperStatus.Transaction.Success
                            && x.TransactionDate > _Request.StartDate
                            && x.TransactionDate < _Request.EndDate),
                            InvoiceAmount = _HCoreContext.HCUAccountTransaction.Where(x =>
                          x.Account.CountryId == _Request.UserReference.SystemCountry && x.Account.AccountTypeId == UserAccountType.Appuser
                            && x.StatusId == HelperStatus.Transaction.Success
                             && x.StatusId == HelperStatus.Transaction.Success
                           && x.Account.GenderId == Helpers.Gender.Female
                           && x.TransactionDate > _Request.StartDate
                           && x.TransactionDate < _Request.EndDate)
                           .Sum(x => x.PurchaseAmount),
                        });
                        _CustomerAnalytics.GenderRange = _CustomerAnalyticsRange;
                        _CustomerAnalyticsRange = new List<OCustomerAnalytics.Range>();
                        _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                        {
                            Title = "18-29",
                            Count = _HCoreContext.HCUAccount.Count(x =>
                       x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Appuser
                        && x.DateOfBirth != null
                        && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 17 && (TodayTime.Year - x.DateOfBirth.Value.Year) < 30)
                        && x.HCUAccountTransactionAccount.Any(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate) == true),

                            Visits = _HCoreContext.HCUAccountTransaction.Count(x =>
                        x.Account.CountryId == _Request.UserReference.SystemCountry && x.Account.AccountTypeId == UserAccountType.Appuser
                            && x.StatusId == HelperStatus.Transaction.Success
                           && x.Account.GenderId == Helpers.Gender.Female
                            && x.Account.DateOfBirth != null
                        && ((TodayTime.Year - x.Account.DateOfBirth.Value.Year) > 17 && (TodayTime.Year - x.Account.DateOfBirth.Value.Year) < 30)
                           && x.TransactionDate > _Request.StartDate
                           && x.TransactionDate < _Request.EndDate),
                        });
                        _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                        {
                            Title = "30-49",
                            Count = _HCoreContext.HCUAccount.Count(x =>
                   x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Appuser
                         && x.StatusId == HelperStatus.Transaction.Success
                        && x.DateOfBirth != null
                        && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 29 && (TodayTime.Year - x.DateOfBirth.Value.Year) < 50)
                        && x.HCUAccountTransactionAccount.Any(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate) == true),

                            Visits = _HCoreContext.HCUAccountTransaction.Count(x =>
                         x.Account.CountryId == _Request.UserReference.SystemCountry && x.Account.AccountTypeId == UserAccountType.Appuser
                           && x.Account.GenderId == Helpers.Gender.Female
                           && x.StatusId == HelperStatus.Transaction.Success
                            && x.Account.DateOfBirth != null
                        && ((TodayTime.Year - x.Account.DateOfBirth.Value.Year) > 29 && (TodayTime.Year - x.Account.DateOfBirth.Value.Year) < 50)
                           && x.TransactionDate > _Request.StartDate
                           && x.TransactionDate < _Request.EndDate),
                        });
                        _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                        {
                            Title = "50 ABOVE",
                            Count = _HCoreContext.HCUAccount.Count(x =>
                      x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Appuser
                         && x.StatusId == HelperStatus.Transaction.Success
                        && x.DateOfBirth != null
                        && ((TodayTime.Year - x.DateOfBirth.Value.Year) > 49)
                        && x.HCUAccountTransactionAccount.Any(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate) == true),

                            Visits = _HCoreContext.HCUAccountTransaction.Count(x =>
                           x.Account.AccountTypeId == UserAccountType.Appuser
                        && x.StatusId == HelperStatus.Transaction.Success
                           && x.Account.GenderId == Helpers.Gender.Female
                            && x.Account.DateOfBirth != null
                        && ((TodayTime.Year - x.Account.DateOfBirth.Value.Year) > 49)
                           && x.TransactionDate > _Request.StartDate
                           && x.TransactionDate < _Request.EndDate),
                        });
                        _CustomerAnalytics.AgeRange = _CustomerAnalyticsRange;


                        _CustomerAnalyticsRange = new List<OCustomerAnalytics.Range>();
                        _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                        {
                            Title = "< 30k",
                            Count = _HCoreContext.HCUAccount.Count(x =>
                       x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Appuser
                         && x.StatusId == HelperStatus.Transaction.Success
                        && x.HCUAccountTransactionAccount.Where(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate
                        && a.SourceId == TransactionSource.TUC).Sum(a => a.PurchaseAmount) < 30000),
                        });
                        _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                        {
                            Title = "30k - 50k",
                            Count = _HCoreContext.HCUAccount.Count(x =>
                      x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Appuser
                        && x.HCUAccountTransactionAccount.Where(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate
                        && a.SourceId == TransactionSource.TUC).Sum(a => a.PurchaseAmount) > 30000
                        && x.HCUAccountTransactionAccount.Where(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate
                        && a.SourceId == TransactionSource.TUC).Sum(a => a.PurchaseAmount) < 50000
                        ),
                        });
                        _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                        {
                            Title = "50k - 75k",
                            Count = _HCoreContext.HCUAccount.Count(x =>
                      x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Appuser
                        && x.HCUAccountTransactionAccount.Where(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate
                        && a.SourceId == TransactionSource.TUC).Sum(a => a.PurchaseAmount) > 50000
                        && x.HCUAccountTransactionAccount.Where(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate
                        && a.SourceId == TransactionSource.TUC).Sum(a => a.PurchaseAmount) < 75000
                      ),
                        });
                        _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                        {
                            Title = "75k +",
                            Count = _HCoreContext.HCUAccount.Count(x =>
                     x.CountryId == _Request.UserReference.SystemCountry && x.AccountTypeId == UserAccountType.Appuser
                        && x.HCUAccountTransactionAccount.Where(a => a.StatusId == HelperStatus.Transaction.Success && a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate
                        && a.SourceId == TransactionSource.TUC).Sum(a => a.PurchaseAmount) > 75000),
                        });
                        _CustomerAnalytics.SpendRange = _CustomerAnalyticsRange;
                        // _CustomerAnalyticsRange = new List<OCustomerAnalytics.Range>();
                        //   _CustomerAnalyticsRange.Add(new OCustomerAnalytics.Range
                        //   {
                        //       Title = "18-29",
                        //       Count = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.GenderId == Helpers.Gender.Female && x.HCUAccountTransactionAccount.Any(a => a.TransactionDate > _Request.StartDate && a.TransactionDate < _Request.EndDate) == true),
                        //       Visits = _HCoreContext.HCUAccountTransaction.Count(x =>
                        //       x.Account.AccountTypeId == UserAccountType.Appuser
                        //       && x.Account.GenderId == Helpers.Gender.Female
                        //       && x.TransactionDate > _Request.StartDate
                        //       && x.TransactionDate < _Request.EndDate),
                        //       InvoiceAmount = _HCoreContext.HCUAccountTransaction.Where(x =>
                        //      x.Account.AccountTypeId == UserAccountType.Appuser
                        //      && x.Account.GenderId == Helpers.Gender.Female
                        //      && x.TransactionDate > _Request.StartDate
                        //      && x.TransactionDate < _Request.EndDate)
                        //.Sum(x => x.PurchaseAmount),
                        //   });
                    }
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerAnalytics, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

        //internal OResponse GetTransactionHistory(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_Request.SearchCondition))
        //        {
        //            HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
        //        }
        //        if (string.IsNullOrEmpty(_Request.SortExpression))
        //        {
        //            HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
        //        }
        //        if (_Request.Limit < 1)
        //        {
        //            _Request.Limit = _AppConfig.DefaultRecordsLimit;
        //        }
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            long DaysDifference = (_Request.EndDate.Value.Date - _Request.StartDate.Value.Date).Days;
        //            _OTrList = new List<OTrList>();
        //            _OTrList = _HCoreContext.HCUAccountTransaction
        //                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
        //                                && x.TransactionDate > _Request.StartDate
        //                                && x.TransactionDate < _Request.EndDate
        //                               && x.StatusId == HelperStatus.Transaction.Success
        //                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus)
        //                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward))
        //                               .Select(x => new OTrList
        //                               {
        //                                   UserAccountId = x.AccountId,
        //                                   TransactionDate = x.TransactionDate.AddHours(1),
        //                                   InvoiceAmount = x.PurchaseAmount,
        //                                   CommissionAmount = x.ParentTransaction.ComissionAmount,
        //                               }).ToList();
        //            if (DaysDifference > 365)
        //            {

        //                var TM = _OTrList.GroupBy(x => x.TransactionDate.Year)
        //               .Select(x => new OAnalytics.SalesMetrics
        //               {
        //                   Title = x.Key.ToString(),
        //                   Year = x.Key,
        //                   TotalTransaction = x.Count(),
        //                   TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
        //                   TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                   NewCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() == 1).Count(),
        //                   RepeatingCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() > 1).Count(),
        //                   CommissionAmount = x.Sum(a => a.CommissionAmount),
        //               }).ToList();
        //                foreach (var item in TM)
        //                {
        //                    item.NewCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() < 2).Count();

        //                    item.NewCustomerInvoiceAmount = _OTrList
        //                       .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
        //                       .GroupBy(x => x.UserAccountId)
        //                       .Where(x => x.Count() < 2)
        //                       .Select(x => new
        //                       {
        //                           UserAccount = x.Key,
        //                           InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                       }).Sum(g => g.InvoiceAmount);

        //                    item.RepeatingCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() > 1)
        //                        .Count();

        //                    item.RepeatingCustomerInvoiceAmount = _OTrList
        //                      .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
        //                      .GroupBy(x => x.UserAccountId)
        //                      .Where(x => x.Count() > 1)
        //                      .Select(x => new
        //                      {
        //                          UserAccount = x.Key,
        //                          InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                      }).Sum(g => g.InvoiceAmount);
        //                    item.VisitsByRepeatingCustomers = _OTrList
        //                     .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
        //                     .GroupBy(x => x.UserAccountId)
        //                     .Where(x => x.Count() > 1)
        //                     .Sum(x => x.Count());
        //                }
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
        //                #endregion
        //            }
        //            else if (DaysDifference < 366 && DaysDifference > 31)
        //            {
        //                //var TM = _OTrList.GroupBy(x => new
        //                //{
        //                //    x.TransactionDate.Date.Year,
        //                //    x.TransactionDate.Date.Month,
        //                //})
        //                //.Select(x => new OAnalytics.SalesMetrics
        //                //{
        //                //    Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
        //                //    Year = x.Key.Year,
        //                //    Month = x.Key.Month,
        //                //    TotalTransaction = x.Count(),
        //                //    TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                //}).ToList();
        //                // return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");


        //                var TM = _OTrList.GroupBy(x => new
        //                {
        //                    x.TransactionDate.Date.Year,
        //                    x.TransactionDate.Date.Month,
        //                })
        //             .Select(x => new OAnalytics.SalesMetrics
        //             {
        //                 Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
        //                 Year = x.Key.Year,
        //                 Month = x.Key.Month,
        //                 TotalTransaction = x.Count(),
        //                 TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
        //                 TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                 NewCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() == 1).Count(),
        //                 RepeatingCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() > 1).Count(),
        //                 CommissionAmount = x.Sum(a => a.CommissionAmount),
        //             }).ToList();
        //                foreach (var item in TM)
        //                {
        //                    item.NewCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() < 2).Count();

        //                    item.NewCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
        //                       .GroupBy(x => x.UserAccountId)
        //                       .Where(x => x.Count() < 2)
        //                       .Select(x => new
        //                       {
        //                           UserAccount = x.Key,
        //                           InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                       }).Sum(g => g.InvoiceAmount);

        //                    item.RepeatingCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() > 1)
        //                        .Count();

        //                    item.RepeatingCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
        //                      .GroupBy(x => x.UserAccountId)
        //                      .Where(x => x.Count() > 1)
        //                      .Select(x => new
        //                      {
        //                          UserAccount = x.Key,
        //                          InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                      }).Sum(g => g.InvoiceAmount);
        //                    item.VisitsByRepeatingCustomers = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
        //                     .GroupBy(x => x.UserAccountId)
        //                     .Where(x => x.Count() > 1)
        //                     .Sum(x => x.Count());
        //                }
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
        //                #endregion
        //            }
        //            else if (DaysDifference < 2)
        //            {
        //                var TM = _OTrList.GroupBy(x => x.TransactionDate.Hour)
        //                 .Select(x => new OAnalytics.SalesMetrics
        //                 {
        //                     Title = x.Key.ToString(),
        //                     Hour = x.Key,
        //                     TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
        //                     TotalTransaction = x.Count(),
        //                     TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                     CommissionAmount = x.Sum(a => a.CommissionAmount),
        //                 }).ToList();
        //                foreach (var item in TM)
        //                {
        //                    var hours = Convert.ToInt32(item.Title);
        //                    int minutes = 0;
        //                    var amPmDesignator = "AM";
        //                    if (hours == 0)
        //                        hours = 12;
        //                    else if (hours == 12)
        //                        amPmDesignator = "PM";
        //                    else if (hours > 12)
        //                    {
        //                        hours -= 12;
        //                        amPmDesignator = "PM";
        //                    }
        //                    item.Title = String.Format("{0}:{1:00} {2}", hours, minutes, amPmDesignator);


        //                    item.NewCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Hour == item.Hour)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() < 2).Count();

        //                    item.NewCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Hour == item.Hour)
        //                       .GroupBy(x => x.UserAccountId)
        //                       .Where(x => x.Count() < 2)
        //                       .Select(x => new
        //                       {
        //                           UserAccount = x.Key,
        //                           InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                       }).Sum(g => g.InvoiceAmount);

        //                    item.RepeatingCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Hour == item.Hour)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() > 1)
        //                        .Count();

        //                    item.RepeatingCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Hour == item.Hour)
        //                      .GroupBy(x => x.UserAccountId)
        //                      .Where(x => x.Count() > 1)
        //                      .Select(x => new
        //                      {
        //                          UserAccount = x.Key,
        //                          InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                      }).Sum(g => g.InvoiceAmount);
        //                    item.VisitsByRepeatingCustomers = _OTrList
        //                        .Where(x => x.TransactionDate.Hour == item.Hour)
        //                     .GroupBy(x => x.UserAccountId)
        //                     .Where(x => x.Count() > 1)
        //                     .Sum(x => x.Count());
        //                }

        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
        //            }
        //            else
        //            {
        //                var TM = _OTrList.GroupBy(x => x.TransactionDate.Date)
        //                 .Select(x => new OAnalytics.SalesMetrics
        //                 {
        //                     Title = x.Key.ToString("dd-MM-yyyy"),
        //                     Date = x.Key,
        //                     TotalTransaction = x.Count(),
        //                     TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
        //                     TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                     CommissionAmount = x.Sum(a => a.CommissionAmount),
        //                 }).ToList();

        //                foreach (var item in TM)
        //                {
        //                    item.NewCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Date == item.Date)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() < 2).Count();

        //                    item.NewCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Date == item.Date)
        //                       .GroupBy(x => x.UserAccountId)
        //                       .Where(x => x.Count() < 2)
        //                       .Select(x => new
        //                       {
        //                           UserAccount = x.Key,
        //                           InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                       }).Sum(g => g.InvoiceAmount);

        //                    item.RepeatingCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Date == item.Date)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() > 1)
        //                        .Count();

        //                    item.RepeatingCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Date == item.Date)
        //                      .GroupBy(x => x.UserAccountId)
        //                      .Where(x => x.Count() > 1)
        //                      .Select(x => new
        //                      {
        //                          UserAccount = x.Key,
        //                          InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                      }).Sum(g => g.InvoiceAmount);
        //                    item.VisitsByRepeatingCustomers = _OTrList
        //                        .Where(x => x.TransactionDate.Date == item.Date)
        //                     .GroupBy(x => x.UserAccountId)
        //                     .Where(x => x.Count() > 1)
        //                     .Sum(x => x.Count());
        //                }

        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
        //            }
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException("GetLoyaltyHistory", _Exception, _Request.UserReference);
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
        //    }
        //    #endregion
        //}

        //internal OResponse GetRewardHistory(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_Request.SearchCondition))
        //        {
        //            HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
        //        }
        //        if (string.IsNullOrEmpty(_Request.SortExpression))
        //        {
        //            HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
        //        }
        //        if (_Request.Limit < 1)
        //        {
        //            _Request.Limit = _AppConfig.DefaultRecordsLimit;
        //        }
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            long DaysDifference = (_Request.EndDate.Value.Date - _Request.StartDate.Value.Date).Days;
        //            _OTrList = new List<OTrList>();
        //            _OTrList = _HCoreContext.HCUAccountTransaction
        //                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
        //                                && x.TransactionDate != null
        //                                && x.ParentTransactionId != null
        //                                && x.TransactionDate > _Request.StartDate
        //                                && x.TransactionDate < _Request.EndDate
        //                                && x.TotalAmount > 0
        //                                && x.StatusId == HelperStatus.Transaction.Success
        //                                && x.ModeId == TransactionMode.Credit
        //                                && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus)
        //                                && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward))
        //                               .Select(x => new OTrList
        //                               {
        //                                   UserAccountId = x.AccountId,
        //                                   TransactionDate = x.TransactionDate.AddHours(1),
        //                                   InvoiceAmount = x.PurchaseAmount,
        //                                   RewardAmount = x.ParentTransaction.TotalAmount,
        //                               }).ToList();
        //            if (DaysDifference > 364)
        //            {
        //                //var TM = _OTrList.GroupBy(x => x.TransactionDate.Date.Year)
        //                // .Select(x => new OAnalytics.SalesMetrics
        //                // {
        //                //     Title = x.Key.ToString(),
        //                //     Year = x.Key,
        //                //     TotalTransaction = x.Count(),
        //                //     TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                // }).ToList();
        //                //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
        //                var TM = _OTrList.GroupBy(x => x.TransactionDate.Year)
        //               .Select(x => new OAnalytics.SalesMetrics
        //               {
        //                   Title = x.Key.ToString(),
        //                   Year = x.Key,
        //                   TotalTransaction = x.Count(),
        //                   TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
        //                   TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                   NewCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() == 1).Count(),
        //                   RepeatingCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() > 1).Count(),
        //                   RewardAmount = x.Sum(a => a.RewardAmount),
        //               }).ToList();
        //                foreach (var item in TM)
        //                {
        //                    item.NewCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() < 2).Count();

        //                    item.NewCustomerInvoiceAmount = _OTrList
        //                       .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
        //                       .GroupBy(x => x.UserAccountId)
        //                       .Where(x => x.Count() < 2)
        //                       .Select(x => new
        //                       {
        //                           UserAccount = x.Key,
        //                           InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                       }).Sum(g => g.InvoiceAmount);

        //                    item.RepeatingCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() > 1)
        //                        .Count();

        //                    item.RepeatingCustomerInvoiceAmount = _OTrList
        //                      .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
        //                      .GroupBy(x => x.UserAccountId)
        //                      .Where(x => x.Count() > 1)
        //                      .Select(x => new
        //                      {
        //                          UserAccount = x.Key,
        //                          InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                      }).Sum(g => g.InvoiceAmount);
        //                    item.VisitsByRepeatingCustomers = _OTrList
        //                     .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
        //                     .GroupBy(x => x.UserAccountId)
        //                     .Where(x => x.Count() > 1)
        //                     .Sum(x => x.Count());
        //                }
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
        //                #endregion
        //            }
        //            else if (DaysDifference < 365 && DaysDifference > 31)
        //            {
        //                //var TM = _OTrList.GroupBy(x => new
        //                //{
        //                //    x.TransactionDate.Date.Year,
        //                //    x.TransactionDate.Date.Month,
        //                //})
        //                //.Select(x => new OAnalytics.SalesMetrics
        //                //{
        //                //    Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
        //                //    Year = x.Key.Year,
        //                //    Month = x.Key.Month,
        //                //    TotalTransaction = x.Count(),
        //                //    TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                //}).ToList();
        //                // return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");


        //                var TM = _OTrList.GroupBy(x => new
        //                {
        //                    x.TransactionDate.Date.Year,
        //                    x.TransactionDate.Date.Month,
        //                })
        //             .Select(x => new OAnalytics.SalesMetrics
        //             {
        //                 Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
        //                 Year = x.Key.Year,
        //                 Month = x.Key.Month,
        //                 TotalTransaction = x.Count(),
        //                 TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
        //                 TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                 NewCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() == 1).Count(),
        //                 RepeatingCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() > 1).Count(),
        //                 CommissionAmount = x.Sum(a => a.CommissionAmount),
        //                 RewardAmount = x.Sum(a => a.RewardAmount),

        //             }).ToList();
        //                foreach (var item in TM)
        //                {
        //                    item.NewCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() < 2).Count();

        //                    item.NewCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
        //                       .GroupBy(x => x.UserAccountId)
        //                       .Where(x => x.Count() < 2)
        //                       .Select(x => new
        //                       {
        //                           UserAccount = x.Key,
        //                           InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                       }).Sum(g => g.InvoiceAmount);

        //                    item.RepeatingCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() > 1)
        //                        .Count();

        //                    item.RepeatingCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
        //                      .GroupBy(x => x.UserAccountId)
        //                      .Where(x => x.Count() > 1)
        //                      .Select(x => new
        //                      {
        //                          UserAccount = x.Key,
        //                          InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                      }).Sum(g => g.InvoiceAmount);
        //                    item.VisitsByRepeatingCustomers = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
        //                     .GroupBy(x => x.UserAccountId)
        //                     .Where(x => x.Count() > 1)
        //                     .Sum(x => x.Count());
        //                }
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
        //                #endregion
        //            }
        //            else if (DaysDifference < 2)
        //            {
        //                var TM = _OTrList.GroupBy(x => x.TransactionDate.Hour)
        //                 .Select(x => new OAnalytics.SalesMetrics
        //                 {
        //                     Title = x.Key.ToString(),
        //                     Hour = x.Key,
        //                     TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
        //                     TotalTransaction = x.Count(),
        //                     TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                     CommissionAmount = x.Sum(a => a.CommissionAmount),
        //                     RewardAmount = x.Sum(a => a.RewardAmount),
        //                 }).ToList();
        //                foreach (var item in TM)
        //                {
        //                    var hours = Convert.ToInt32(item.Title);
        //                    int minutes = 0;
        //                    var amPmDesignator = "AM";
        //                    if (hours == 0)
        //                        hours = 12;
        //                    else if (hours == 12)
        //                        amPmDesignator = "PM";
        //                    else if (hours > 12)
        //                    {
        //                        hours -= 12;
        //                        amPmDesignator = "PM";
        //                    }
        //                    item.Title = String.Format("{0}:{1:00} {2}", hours, minutes, amPmDesignator);


        //                    item.NewCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Hour == item.Hour)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() < 2).Count();

        //                    item.NewCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Hour == item.Hour)
        //                       .GroupBy(x => x.UserAccountId)
        //                       .Where(x => x.Count() < 2)
        //                       .Select(x => new
        //                       {
        //                           UserAccount = x.Key,
        //                           InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                       }).Sum(g => g.InvoiceAmount);

        //                    item.RepeatingCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Hour == item.Hour)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() > 1)
        //                        .Count();

        //                    item.RepeatingCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Hour == item.Hour)
        //                      .GroupBy(x => x.UserAccountId)
        //                      .Where(x => x.Count() > 1)
        //                      .Select(x => new
        //                      {
        //                          UserAccount = x.Key,
        //                          InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                      }).Sum(g => g.InvoiceAmount);
        //                    item.VisitsByRepeatingCustomers = _OTrList
        //                        .Where(x => x.TransactionDate.Hour == item.Hour)
        //                     .GroupBy(x => x.UserAccountId)
        //                     .Where(x => x.Count() > 1)
        //                     .Sum(x => x.Count());
        //                }

        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
        //            }
        //            else
        //            {
        //                var TM = _OTrList.GroupBy(x => x.TransactionDate.Date)
        //                 .Select(x => new OAnalytics.SalesMetrics
        //                 {
        //                     Title = x.Key.ToString("dd-MM-yyyy"),
        //                     Date = x.Key,
        //                     TotalTransaction = x.Count(),
        //                     TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
        //                     TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                     CommissionAmount = x.Sum(a => a.CommissionAmount),
        //                     RewardAmount = x.Sum(a => a.RewardAmount),
        //                 }).ToList();

        //                foreach (var item in TM)
        //                {
        //                    item.NewCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Date == item.Date)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() < 2).Count();

        //                    item.NewCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Date == item.Date)
        //                       .GroupBy(x => x.UserAccountId)
        //                       .Where(x => x.Count() < 2)
        //                       .Select(x => new
        //                       {
        //                           UserAccount = x.Key,
        //                           InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                       }).Sum(g => g.InvoiceAmount);

        //                    item.RepeatingCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Date == item.Date)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() > 1)
        //                        .Count();

        //                    item.RepeatingCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Date == item.Date)
        //                      .GroupBy(x => x.UserAccountId)
        //                      .Where(x => x.Count() > 1)
        //                      .Select(x => new
        //                      {
        //                          UserAccount = x.Key,
        //                          InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                      }).Sum(g => g.InvoiceAmount);
        //                    item.VisitsByRepeatingCustomers = _OTrList
        //                        .Where(x => x.TransactionDate.Date == item.Date)
        //                     .GroupBy(x => x.UserAccountId)
        //                     .Where(x => x.Count() > 1)
        //                     .Sum(x => x.Count());
        //                }

        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
        //            }
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException("GetLoyaltyHistory", _Exception, _Request.UserReference);
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
        //    }
        //    #endregion
        //}
        //internal OResponse GetRedeemHistory(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_Request.SearchCondition))
        //        {
        //            HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
        //        }
        //        if (string.IsNullOrEmpty(_Request.SortExpression))
        //        {
        //            HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
        //        }
        //        if (_Request.Limit < 1)
        //        {
        //            _Request.Limit = _AppConfig.DefaultRecordsLimit;
        //        }
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            long DaysDifference = (_Request.EndDate.Value.Date - _Request.StartDate.Value.Date).Days;
        //            _OTrList = new List<OTrList>();
        //            _OTrList = _HCoreContext.HCUAccountTransaction
        //                                .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
        //                                && x.TransactionDate > _Request.StartDate
        //                                && x.TransactionDate < _Request.EndDate
        //                                && x.TotalAmount > 0
        //                                && x.StatusId == HelperStatus.Transaction.Success
        //                                && (x.SourceId == TransactionSource.TUC)
        //                                && (x.ModeId == TransactionMode.Debit)
        //                                && x.TypeId == TransactionType.OnlineRedeem)
        //                               .Select(x => new OTrList
        //                               {
        //                                   UserAccountId = x.AccountId,
        //                                   TransactionDate = x.TransactionDate.AddHours(1),
        //                                   InvoiceAmount = x.PurchaseAmount,
        //                                   RedeemAmount = x.TotalAmount,
        //                               }).ToList();
        //            if (DaysDifference > 365)
        //            {
        //                //var TM = _OTrList.GroupBy(x => x.TransactionDate.Date.Year)
        //                // .Select(x => new OAnalytics.SalesMetrics
        //                // {
        //                //     Title = x.Key.ToString(),
        //                //     Year = x.Key,
        //                //     TotalTransaction = x.Count(),
        //                //     TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                // }).ToList();
        //                //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
        //                var TM = _OTrList.GroupBy(x => x.TransactionDate.Year)
        //               .Select(x => new OAnalytics.SalesMetrics
        //               {
        //                   Title = x.Key.ToString(),
        //                   Year = x.Key,
        //                   TotalTransaction = x.Count(),
        //                   TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
        //                   TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                   NewCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() == 1).Count(),
        //                   RepeatingCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() > 1).Count(),
        //                   CommissionAmount = x.Sum(a => a.CommissionAmount),
        //                   RedeemAmount = x.Sum(a => a.RedeemAmount),
        //               }).ToList();
        //                foreach (var item in TM)
        //                {
        //                    item.NewCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() < 2).Count();

        //                    item.NewCustomerInvoiceAmount = _OTrList
        //                       .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
        //                       .GroupBy(x => x.UserAccountId)
        //                       .Where(x => x.Count() < 2)
        //                       .Select(x => new
        //                       {
        //                           UserAccount = x.Key,
        //                           InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                       }).Sum(g => g.InvoiceAmount);

        //                    item.RepeatingCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() > 1)
        //                        .Count();

        //                    item.RepeatingCustomerInvoiceAmount = _OTrList
        //                      .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
        //                      .GroupBy(x => x.UserAccountId)
        //                      .Where(x => x.Count() > 1)
        //                      .Select(x => new
        //                      {
        //                          UserAccount = x.Key,
        //                          InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                      }).Sum(g => g.InvoiceAmount);
        //                    item.VisitsByRepeatingCustomers = _OTrList
        //                     .Where(x => x.TransactionDate.Year == item.Date.Value.Year)
        //                     .GroupBy(x => x.UserAccountId)
        //                     .Where(x => x.Count() > 1)
        //                     .Sum(x => x.Count());
        //                }
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
        //                #endregion
        //            }
        //            else if (DaysDifference < 366 && DaysDifference > 31)
        //            {
        //                //var TM = _OTrList.GroupBy(x => new
        //                //{
        //                //    x.TransactionDate.Date.Year,
        //                //    x.TransactionDate.Date.Month,
        //                //})
        //                //.Select(x => new OAnalytics.SalesMetrics
        //                //{
        //                //    Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
        //                //    Year = x.Key.Year,
        //                //    Month = x.Key.Month,
        //                //    TotalTransaction = x.Count(),
        //                //    TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                //}).ToList();
        //                // return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");


        //                var TM = _OTrList.GroupBy(x => new
        //                {
        //                    x.TransactionDate.Date.Year,
        //                    x.TransactionDate.Date.Month,
        //                })
        //             .Select(x => new OAnalytics.SalesMetrics
        //             {
        //                 Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
        //                 Year = x.Key.Year,
        //                 Month = x.Key.Month,
        //                 TotalTransaction = x.Count(),
        //                 TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
        //                 TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                 NewCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() == 1).Count(),
        //                 RepeatingCustomer = x.GroupBy(a => a.UserAccountId).Where(b => b.Count() > 1).Count(),
        //                 CommissionAmount = x.Sum(a => a.CommissionAmount),
        //                 RedeemAmount = x.Sum(a => a.RedeemAmount),
        //             }).ToList();
        //                foreach (var item in TM)
        //                {
        //                    item.NewCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() < 2).Count();

        //                    item.NewCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
        //                       .GroupBy(x => x.UserAccountId)
        //                       .Where(x => x.Count() < 2)
        //                       .Select(x => new
        //                       {
        //                           UserAccount = x.Key,
        //                           InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                       }).Sum(g => g.InvoiceAmount);

        //                    item.RepeatingCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() > 1)
        //                        .Count();

        //                    item.RepeatingCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
        //                      .GroupBy(x => x.UserAccountId)
        //                      .Where(x => x.Count() > 1)
        //                      .Select(x => new
        //                      {
        //                          UserAccount = x.Key,
        //                          InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                      }).Sum(g => g.InvoiceAmount);
        //                    item.VisitsByRepeatingCustomers = _OTrList
        //                        .Where(x => x.TransactionDate.Year == item.Date.Value.Year && x.TransactionDate.Month == item.Date.Value.Month)
        //                     .GroupBy(x => x.UserAccountId)
        //                     .Where(x => x.Count() > 1)
        //                     .Sum(x => x.Count());
        //                }
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
        //                #endregion
        //            }
        //            else if (DaysDifference < 2)
        //            {
        //                var TM = _OTrList.GroupBy(x => x.TransactionDate.Hour)
        //                 .Select(x => new OAnalytics.SalesMetrics
        //                 {
        //                     Title = x.Key.ToString(),
        //                     Hour = x.Key,
        //                     TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
        //                     TotalTransaction = x.Count(),
        //                     TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                     CommissionAmount = x.Sum(a => a.CommissionAmount),
        //                     RedeemAmount = x.Sum(a => a.RedeemAmount),
        //                 }).ToList();
        //                foreach (var item in TM)
        //                {
        //                    var hours = Convert.ToInt32(item.Title);
        //                    int minutes = 0;
        //                    var amPmDesignator = "AM";
        //                    if (hours == 0)
        //                        hours = 12;
        //                    else if (hours == 12)
        //                        amPmDesignator = "PM";
        //                    else if (hours > 12)
        //                    {
        //                        hours -= 12;
        //                        amPmDesignator = "PM";
        //                    }
        //                    item.Title = String.Format("{0}:{1:00} {2}", hours, minutes, amPmDesignator);


        //                    item.NewCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Hour == item.Hour)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() < 2).Count();

        //                    item.NewCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Hour == item.Hour)
        //                       .GroupBy(x => x.UserAccountId)
        //                       .Where(x => x.Count() < 2)
        //                       .Select(x => new
        //                       {
        //                           UserAccount = x.Key,
        //                           InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                       }).Sum(g => g.InvoiceAmount);

        //                    item.RepeatingCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Hour == item.Hour)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() > 1)
        //                        .Count();

        //                    item.RepeatingCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Hour == item.Hour)
        //                      .GroupBy(x => x.UserAccountId)
        //                      .Where(x => x.Count() > 1)
        //                      .Select(x => new
        //                      {
        //                          UserAccount = x.Key,
        //                          InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                      }).Sum(g => g.InvoiceAmount);
        //                    item.VisitsByRepeatingCustomers = _OTrList
        //                        .Where(x => x.TransactionDate.Hour == item.Hour)
        //                     .GroupBy(x => x.UserAccountId)
        //                     .Where(x => x.Count() > 1)
        //                     .Sum(x => x.Count());
        //                }

        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
        //            }
        //            else
        //            {
        //                var TM = _OTrList.GroupBy(x => x.TransactionDate.Date)
        //                 .Select(x => new OAnalytics.SalesMetrics
        //                 {
        //                     Title = x.Key.ToString("dd-MM-yyyy"),
        //                     Date = x.Key,
        //                     TotalTransaction = x.Count(),
        //                     TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
        //                     TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
        //                     CommissionAmount = x.Sum(a => a.CommissionAmount),
        //                     RedeemAmount = x.Sum(a => a.RedeemAmount),
        //                 }).ToList();

        //                foreach (var item in TM)
        //                {
        //                    item.NewCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Date == item.Date)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() < 2).Count();

        //                    item.NewCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Date == item.Date)
        //                       .GroupBy(x => x.UserAccountId)
        //                       .Where(x => x.Count() < 2)
        //                       .Select(x => new
        //                       {
        //                           UserAccount = x.Key,
        //                           InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                       }).Sum(g => g.InvoiceAmount);

        //                    item.RepeatingCustomer = _OTrList
        //                        .Where(x => x.TransactionDate.Date == item.Date)
        //                        .GroupBy(x => x.UserAccountId)
        //                        .Where(x => x.Count() > 1)
        //                        .Count();

        //                    item.RepeatingCustomerInvoiceAmount = _OTrList
        //                        .Where(x => x.TransactionDate.Date == item.Date)
        //                      .GroupBy(x => x.UserAccountId)
        //                      .Where(x => x.Count() > 1)
        //                      .Select(x => new
        //                      {
        //                          UserAccount = x.Key,
        //                          InvoiceAmount = x.Sum(d => d.InvoiceAmount)
        //                      }).Sum(g => g.InvoiceAmount);
        //                    item.VisitsByRepeatingCustomers = _OTrList
        //                        .Where(x => x.TransactionDate.Date == item.Date)
        //                     .GroupBy(x => x.UserAccountId)
        //                     .Where(x => x.Count() > 1)
        //                     .Sum(x => x.Count());
        //                }

        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
        //            }
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException("GetLoyaltyHistory", _Exception, _Request.UserReference);
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
        //    }
        //    #endregion
        //}

        /// <summary>
        /// Description: Gets the revenue overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRevenueOverview(OAnalytics.Request _Request)
        {
            _MerchantAnalyticsResponse = new OMerchantAnalytics.Response();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _MerchantAnalyticsCount = new OMerchantAnalytics.Count();
                    _MerchantAnalyticsCount.Transactions = _HCoreContext.HCUAccountTransaction
                                                       .Count(m => m.TransactionDate > _Request.StartDate
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && (((m.ModeId == TransactionMode.Credit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                       );
                    _MerchantAnalyticsCount.CommissionAmount = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.TransactionDate > _Request.StartDate
                                                        && m.TransactionDate < _Request.EndDate
                                                        && m.TotalAmount > 0
                                                        && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                        && (((m.ModeId == TransactionMode.Credit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                       ).Sum(x => (double?)x.ParentTransaction.ComissionAmount) ?? 0;

                    _MerchantAnalyticsCount.RewardTransactions = _HCoreContext.HCUAccountTransaction
                                                             .Where(m => m.TransactionDate > _Request.StartDate
                                                              && m.TransactionDate < _Request.EndDate
                                                              && m.TotalAmount > 0
                                                        && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                              && m.StatusId == HelperStatus.Transaction.Success
                                                              && (((m.ModeId == TransactionMode.Credit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                              || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                            ).Count();
                    if (_MerchantAnalyticsCount.RewardTransactions > 0)
                    {
                        _MerchantAnalyticsCount.RewardAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                        && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                && (((m.ModeId == TransactionMode.Credit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                               ).Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    else
                    {
                        _MerchantAnalyticsCount.RewardTransactions = 0;
                    }

                    _MerchantAnalyticsCount.RedeemTransactions = _HCoreContext.HCUAccountTransaction
                                                               .Count(m => m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                        && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC
                                                             );
                    if (_MerchantAnalyticsCount.RedeemTransactions > 0)
                    {
                        _MerchantAnalyticsCount.RedeemAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(m => m.TransactionDate > _Request.StartDate
                                                                && m.TransactionDate < _Request.EndDate
                                                                && m.TotalAmount > 0
                                                        && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                                && m.StatusId == HelperStatus.Transaction.Success
                                                                 && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                && m.ModeId == TransactionMode.Debit && m.SourceId == TransactionSource.TUC
                                                             ).Sum(x => (double?)x.TotalAmount) ?? 0;
                    }
                    else
                    {
                        _MerchantAnalyticsCount.RedeemAmount = 0;
                    }
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _MerchantAnalyticsCount, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetMerchantsOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the todays overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTodaysOverview(OAnalytics.Request _Request)
        {
            _MerchantAnalyticsResponse = new OMerchantAnalytics.Response();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {

                    _MerchantAnalyticsResponse.NewMerchants = _HCoreContext.HCUAccount
                            .Where(x => x.CountryId == _Request.UserReference.CountryId && x.AccountTypeId == UserAccountType.Merchant)
                        .OrderByDescending(x => x.CreateDate)
                        .Select(x => new OMerchantAnalytics.Account
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            DisplayName = x.DisplayName,
                            IconUrl = x.IconStorage.Path,
                            Date = x.CreateDate,
                        }).Skip(0)
                        .Take(5)
                        .ToList();
                    if (_MerchantAnalyticsResponse.NewMerchants.Count > 0)
                    {
                        foreach (var TopVisitor in _MerchantAnalyticsResponse.NewMerchants)
                        {
                            if (!string.IsNullOrEmpty(TopVisitor.IconUrl))
                            {
                                TopVisitor.IconUrl = _AppConfig.StorageUrl + TopVisitor.IconUrl;
                            }
                            else
                            {
                                TopVisitor.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                    }
                    _MerchantAnalyticsResponse.LowBalancewMerchants = _HCoreContext.HCUAccount.Where(x => x.CountryId == _Request.UserReference.CountryId && x.AccountTypeId == UserAccountType.Merchant)
                        .Select(x => new OMerchantAnalytics.Account
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            DisplayName = x.DisplayName,
                            IconUrl = x.IconStorage.Path,
                            Balance = 0,
                            Date = x.CreateDate,
                        }).Skip(0)
                        .Take(5)
                        .ToList();
                    if (_MerchantAnalyticsResponse.LowBalancewMerchants.Count > 0)
                    {
                        foreach (var TopVisitor in _MerchantAnalyticsResponse.LowBalancewMerchants)
                        {
                            if (!string.IsNullOrEmpty(TopVisitor.IconUrl))
                            {
                                TopVisitor.IconUrl = _AppConfig.StorageUrl + TopVisitor.IconUrl;
                            }
                            else
                            {
                                TopVisitor.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                    }


                    _MerchantAnalyticsResponse.NewTerminals = _HCoreContext.TUCTerminal
                        .Where(x => x.Provider.CountryId == _Request.UserReference.CountryId)
                      .OrderByDescending(x => x.CreateDate)
                      .Select(x => new OMerchantAnalytics.Terminal
                      {
                          ReferenceId = x.Id,
                          ReferenceKey = x.Guid,
                          TerminalId = x.DisplayName,
                          Address = x.Store.Address,
                          ProviderDisplayName = x.Provider.DisplayName,
                          AcquirerDisplayName = x.Acquirer.DisplayName,
                          IconUrl = x.Provider.IconStorage.Path,
                          CreateDate = x.CreateDate
                      }).Skip(0)
                      .Take(5)
                      .ToList();
                    if (_MerchantAnalyticsResponse.NewTerminals.Count > 0)
                    {
                        foreach (var TopVisitor in _MerchantAnalyticsResponse.NewTerminals)
                        {
                            if (!string.IsNullOrEmpty(TopVisitor.IconUrl))
                            {
                                TopVisitor.IconUrl = _AppConfig.StorageUrl + TopVisitor.IconUrl;
                            }
                            else
                            {
                                TopVisitor.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                    }
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _MerchantAnalyticsResponse, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetTodaysOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

        public class OTSale
        {
            public DateTime? Date { get; set; }
            public long Year { get; set; }
            public long Month { get; set; }
            public long Day { get; set; }
            public long Week { get; set; }
            public DayOfWeek? DayOfWeek { get; set; }
            public double? Amount { get; set; }
            public double? InvoiceAmount { get; set; }
            public long? Transactions { get; set; }
            public long? Customers { get; set; }
        }
        OAnalytics.Response _AnalyticsResponse;
        /// <summary>
        /// Description: Gets the sales history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSalesHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _AnalyticsResponse = new OAnalytics.Response();
                long DaysDifference = (_Request.EndDate.Value.Date - _Request.StartDate.Value.Date).Days;
                // var _OTrList = HCore.TUC.Data.Store.Analytics._Sales.Where(x => x.Date > _Request.StartDate
                //&& x.Date < _Request.EndDate)
                //     .OrderBy(x => x.Date)
                //     .Select(x => new OTSale
                //     {
                //         Date = x.Date,
                //         Year = x.Year,
                //         Month = x.Month,
                //         Day = x.Day,
                //         DayOfWeek = x.DayOfWeek,
                //         Transactions = x.Transactions,
                //         InvoiceAmount = x.InvoiceAmount,
                //         Customers = x.Customers,
                //     }).ToList();
                using (_HCoreContext = new HCoreContext())
                {
                    //var _OTrList = _HCoreContext.anxsaledaily.Where(x => x.Date > _Request.StartDate
                    // && x.Date < _Request.EndDate)
                    //      .OrderBy(x => x.Date)
                    //      .Select(x => new OTSale
                    //      {
                    //          Date = x.Date,
                    //          Year = x.Date.Value.Year,
                    //          Month = x.Date.Value.Month,
                    //          Day = x.Date.Value.Day,
                    //          DayOfWeek = x.Date.Value.DayOfWeek,
                    //          Transactions = x.Transactions,
                    //          InvoiceAmount = x.InvoiceAmount,
                    //          Customers = x.Customers,
                    //      }).ToList();


                    var _OTrList = _HCoreContext.cmt_sale
                        .Where(x => x.merchant.CountryId == _Request.UserReference.SystemCountry)
                        .Where(x => x.transaction_date.Date > _Request.StartDate && x.transaction_date.Date < _Request.EndDate)
                        .GroupBy(x => x.transaction_date.Date)
                         .OrderBy(x => x.Key)
                         .Select(x => new OTSale
                         {
                             Date = x.Key,
                             Year = x.Key.Year,
                             Month = x.Key.Month,
                             Day = x.Key.Day,
                             DayOfWeek = x.Key.DayOfWeek,
                             Transactions = x.Count(),
                             InvoiceAmount = x.Sum(x => x.invoice_amount),
                             Customers = x.Select(a => a.customer_id).Distinct().Count(),
                         }).ToList();

                    //var _OTrList = _HCoreContext.HCUAccountTransaction
                    //     .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                    //     && x.TransactionDate.Date > _Request.StartDate
                    //     && x.TransactionDate.Date < _Request.EndDate
                    //                                  && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus)
                    //                                  && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.TUCPay))

                    //       .OrderBy(x => x.TransactionDate.Date)
                    //       .Select(x => new OTSale
                    //       {
                    //           //Date = x.TransactionDate,
                    //           //Year = x.Date.Value.Year,
                    //           //Month = x.Date.Value.Month,
                    //           //Day = x.Date.Value.Day,
                    //           //DayOfWeek = x.Date.Value.DayOfWeek,
                    //           //Transactions = x.Transactions,
                    //           //InvoiceAmount = x.InvoiceAmount,
                    //           //Customers = x.Customers,
                    //       }).ToList();
                    if (DaysDifference > 365)
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => x.Year)
                       .Select(x => new OAnalytics.Data
                       {
                           Title = x.Key.ToString(),
                           TotalTransaction = x.Sum(a => a.Transactions),
                           TotalCustomer = x.Sum(a => a.Customers),
                           TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                       }).ToList();
                    }
                    else if (DaysDifference < 366 && DaysDifference > 31)
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => new
                        {
                            x.Year,
                            x.Month,
                        })
                         .Select(x => new OAnalytics.Data
                         {
                             Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
                             TotalTransaction = x.Sum(a => a.Transactions),
                             TotalCustomer = x.Sum(a => a.Customers),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         }).ToList();
                    }
                    else if (DaysDifference == 6)
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => x.DayOfWeek)
                         .Select(x => new OAnalytics.Data
                         {
                             Title = x.Key.ToString(),
                             TotalTransaction = x.Sum(a => a.Transactions),
                             TotalCustomer = x.Sum(a => a.Customers),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         }).ToList();
                    }
                    else
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => x.Date)
                         .Select(x => new OAnalytics.Data
                         {
                             Title = x.Key.Value.ToString("dd-MM-yyyy"),
                             TotalTransaction = x.Sum(a => a.Transactions),
                             TotalCustomer = x.Sum(a => a.Customers),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         }).ToList();
                    }
                    if (_AnalyticsResponse.Data.Count > 0)
                    {
                        _AnalyticsResponse.TotalTransaction = _AnalyticsResponse.Data.Sum(x => x.TotalTransaction);
                        _AnalyticsResponse.TotalCustomer = _AnalyticsResponse.Data.Sum(x => x.TotalCustomer);
                        _AnalyticsResponse.TotalInvoiceAmount = _AnalyticsResponse.Data.Sum(x => x.TotalInvoiceAmount);
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AnalyticsResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetSalesHistory", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the reward history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRewardHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _AnalyticsResponse = new OAnalytics.Response();
                    long DaysDifference = (_Request.EndDate.Value.Date - _Request.StartDate.Value.Date).Days;
                    var _OTrList = _HCoreContext.cmt_loyalty
                        .Where(x => x.merchant.CountryId == _Request.UserReference.SystemCountry)
                        .Where(x => x.transaction_date.Date > _Request.StartDate && x.transaction_date.Date < _Request.EndDate
                        && x.loyalty_type_id == Helpers.Loyalty.Reward)
                         .GroupBy(x => x.transaction_date.Date)
                       .OrderBy(x => x.Key)
                        .Select(x => new OTSale
                        {
                            Date = x.Key,
                            Year = x.Key.Year,
                            Month = x.Key.Month,
                            Day = x.Key.Day,
                            DayOfWeek = x.Key.DayOfWeek,
                            Transactions = x.Count(),
                            Amount = x.Sum(x => x.amount),
                            InvoiceAmount = x.Sum(x => x.invoice_amount),
                            Customers = x.Select(a => a.customer_id).Distinct().Count(),
                        }).ToList();
                    // var _OTrList = _HCoreContext.anxrewarddaily.Where(x => x.Date > _Request.StartDate
                    //&& x.Date < _Request.EndDate)
                    //     .OrderBy(x => x.Date)
                    //     .Select(x => new OTSale
                    //     {
                    //         Date = x.Date,
                    //         Year = x.Date.Value.Year,
                    //         Month = x.Date.Value.Month,
                    //         Day = x.Date.Value.Day,
                    //         DayOfWeek = x.Date.Value.DayOfWeek,
                    //         Transactions = x.Transactions,
                    //         Amount = x.Amount,
                    //         InvoiceAmount = x.InvoiceAmount,
                    //         Customers = x.Customers,
                    //     }).ToList();
                    if (DaysDifference > 365)
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => x.Year)
                       .Select(x => new OAnalytics.Data
                       {
                           Title = x.Key.ToString(),
                           TotalTransaction = x.Sum(a => a.Transactions),
                           TotalCustomer = x.Sum(a => a.Customers),
                           TotalAmount = x.Sum(a => a.Amount),
                           TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                       }).ToList();
                    }
                    else if (DaysDifference < 366 && DaysDifference > 31)
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => new
                        {
                            x.Year,
                            x.Month,
                        })
                         .Select(x => new OAnalytics.Data
                         {
                             Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
                             TotalTransaction = x.Sum(a => a.Transactions),
                             TotalCustomer = x.Sum(a => a.Customers),
                             TotalAmount = x.Sum(a => a.Amount),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         }).ToList();
                    }
                    else if (DaysDifference == 6)
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => x.DayOfWeek)
                         .Select(x => new OAnalytics.Data
                         {
                             Title = x.Key.ToString(),
                             TotalTransaction = x.Sum(a => a.Transactions),
                             TotalCustomer = x.Sum(a => a.Customers),
                             TotalAmount = x.Sum(a => a.Amount),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         }).ToList();
                    }
                    else
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => x.Date)
                         .Select(x => new OAnalytics.Data
                         {
                             Title = x.Key.Value.ToString("dd-MM-yyyy"),
                             TotalTransaction = x.Sum(a => a.Transactions),
                             TotalCustomer = x.Sum(a => a.Customers),
                             TotalAmount = x.Sum(a => a.Amount),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         }).ToList();
                    }
                    if (_AnalyticsResponse.Data.Count > 0)
                    {
                        _AnalyticsResponse.TotalTransaction = _AnalyticsResponse.Data.Sum(x => x.TotalTransaction);
                        _AnalyticsResponse.TotalCustomer = _AnalyticsResponse.Data.Sum(x => x.TotalCustomer);
                        _AnalyticsResponse.TotalAmount = _AnalyticsResponse.Data.Sum(x => x.TotalAmount);
                        _AnalyticsResponse.TotalInvoiceAmount = _AnalyticsResponse.Data.Sum(x => x.TotalInvoiceAmount);
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AnalyticsResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardHistory", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the redeem history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRedeemHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _AnalyticsResponse = new OAnalytics.Response();
                    long DaysDifference = (_Request.EndDate.Value.Date - _Request.StartDate.Value.Date).Days;
                    // var _OTrList = HCore.TUC.Data.Store.Analytics._Redeems.Where(x => x.Date > _Request.StartDate
                    //&& x.Date < _Request.EndDate)
                    //     .OrderBy(x => x.Date)
                    //     .Select(x => new OTSale
                    //     {
                    //         Date = x.Date,
                    //         Year = x.Year,
                    //         Month = x.Month,
                    //         Day = x.Day,
                    //         DayOfWeek = x.DayOfWeek,
                    //         Transactions = x.Transactions,
                    //         Amount = x.Amount,
                    //         InvoiceAmount = x.InvoiceAmount,
                    //         Customers = x.Customers,
                    //     }).ToList();
                    // var _OTrList = _HCoreContext.anxredeemdaily.Where(x => x.Date > _Request.StartDate
                    //&& x.Date < _Request.EndDate)
                    //     .OrderBy(x => x.Date)
                    //     .Select(x => new OTSale
                    //     {
                    //         Date = x.Date,
                    //         Year = x.Date.Value.Year,
                    //         Month = x.Date.Value.Month,
                    //         Day = x.Date.Value.Day,
                    //         DayOfWeek = x.Date.Value.DayOfWeek,
                    //         Transactions = x.Transactions,
                    //         Amount = x.Amount,
                    //         InvoiceAmount = x.InvoiceAmount,
                    //         Customers = x.Customers,
                    //     }).ToList();

                    var _OTrList = _HCoreContext.cmt_loyalty
                        .Where(x => x.merchant.CountryId == _Request.UserReference.SystemCountry)
                        .Where(x => x.transaction_date.Date > _Request.StartDate && x.transaction_date.Date < _Request.EndDate && x.loyalty_type_id == Helpers.Loyalty.Redeem)
                      .GroupBy(x => x.transaction_date.Date)
                      .OrderBy(x => x.Key)
                     .Select(x => new OTSale
                     {
                         Date = x.Key,
                         Year = x.Key.Year,
                         Month = x.Key.Month,
                         Day = x.Key.Day,
                         DayOfWeek = x.Key.DayOfWeek,
                         Transactions = x.Count(),
                         Amount = x.Sum(x => x.amount),
                         InvoiceAmount = x.Sum(x => x.invoice_amount),
                         Customers = x.Select(a => a.customer_id).Distinct().Count(),
                         //Date = x.Date,
                         //Year = x.Date.Value.Year,
                         //Month = x.Date.Value.Month,
                         //Day = x.Date.Value.Day,
                         //DayOfWeek = x.Date.Value.DayOfWeek,
                         //Transactions = x.Transactions,
                         //Amount = x.Amount,
                         //InvoiceAmount = x.InvoiceAmount,
                         //Customers = x.Customers,
                     }).ToList();
                    if (DaysDifference > 365)
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => x.Year)
                       .Select(x => new OAnalytics.Data
                       {
                           Title = x.Key.ToString(),
                           TotalTransaction = x.Sum(a => a.Transactions),
                           TotalCustomer = x.Sum(a => a.Customers),
                           TotalAmount = x.Sum(a => a.Amount),
                           TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                       }).ToList();
                    }
                    else if (DaysDifference < 366 && DaysDifference > 31)
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => new
                        {
                            x.Year,
                            x.Month,
                        })
                         .Select(x => new OAnalytics.Data
                         {
                             Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
                             TotalTransaction = x.Sum(a => a.Transactions),
                             TotalCustomer = x.Sum(a => a.Customers),
                             TotalAmount = x.Sum(a => a.Amount),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         }).ToList();
                    }
                    else if (DaysDifference == 6)
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => x.DayOfWeek)
                         .Select(x => new OAnalytics.Data
                         {
                             Title = x.Key.ToString(),
                             TotalTransaction = x.Sum(a => a.Transactions),
                             TotalCustomer = x.Sum(a => a.Customers),
                             TotalAmount = x.Sum(a => a.Amount),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         }).ToList();
                    }
                    else
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => x.Date)
                         .Select(x => new OAnalytics.Data
                         {
                             Title = x.Key.Value.ToString("dd-MM-yyyy"),
                             TotalTransaction = x.Sum(a => a.Transactions),
                             TotalCustomer = x.Sum(a => a.Customers),
                             TotalAmount = x.Sum(a => a.Amount),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         }).ToList();
                    }
                    if (_AnalyticsResponse.Data.Count > 0)
                    {
                        _AnalyticsResponse.TotalTransaction = _AnalyticsResponse.Data.Sum(x => x.TotalTransaction);
                        _AnalyticsResponse.TotalCustomer = _AnalyticsResponse.Data.Sum(x => x.TotalCustomer);
                        _AnalyticsResponse.TotalAmount = _AnalyticsResponse.Data.Sum(x => x.TotalAmount);
                        _AnalyticsResponse.TotalInvoiceAmount = _AnalyticsResponse.Data.Sum(x => x.TotalInvoiceAmount);
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AnalyticsResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRedeemHistory", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the reward claim history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRewardClaimHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {

                    _AnalyticsResponse = new OAnalytics.Response();
                    long DaysDifference = (_Request.EndDate.Value.Date - _Request.StartDate.Value.Date).Days;
                    var _OTrList = _HCoreContext.cmt_loyalty
                        .Where(x => x.merchant.CountryId == _Request.UserReference.CountryId)
                        .Where(x => x.transaction_date.Date > _Request.StartDate && x.transaction_date.Date < _Request.EndDate && x.loyalty_type_id == Helpers.Loyalty.RewardClaim)
                        .GroupBy(x => x.transaction_date.Date)
                       .OrderBy(x => x.Key)
                       .Select(x => new OTSale
                       {
                           Date = x.Key,
                           Year = x.Key.Year,
                           Month = x.Key.Month,
                           Day = x.Key.Day,
                           DayOfWeek = x.Key.DayOfWeek,
                           Transactions = x.Count(),
                           Amount = x.Sum(x => x.commission_amount),
                           InvoiceAmount = x.Sum(x => x.invoice_amount),
                           Customers = x.Select(a => a.customer_id).Distinct().Count(),
                       }).ToList();

                    // var _OTrList = _HCoreContext.anxrewardclaimdaily.Where(x => x.Date > _Request.StartDate
                    //&& x.Date < _Request.EndDate)
                    //     .OrderBy(x => x.Date)
                    //     .Select(x => new OTSale
                    //     {
                    //         Date = x.Date,
                    //         Year = x.Date.Value.Year,
                    //         Month = x.Date.Value.Month,
                    //         Day = x.Date.Value.Day,
                    //         DayOfWeek = x.Date.Value.DayOfWeek,
                    //         Transactions = x.Transactions,
                    //         Amount = x.Amount,
                    //         InvoiceAmount = x.InvoiceAmount,
                    //         Customers = x.Customers,
                    //     }).ToList();
                    if (DaysDifference > 365)
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => x.Year)
                       .Select(x => new OAnalytics.Data
                       {
                           Title = x.Key.ToString(),
                           TotalTransaction = x.Sum(a => a.Transactions),
                           TotalCustomer = x.Sum(a => a.Customers),
                           TotalAmount = x.Sum(a => a.Amount),
                           TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                       }).ToList();
                    }
                    else if (DaysDifference < 366 && DaysDifference > 31)
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => new
                        {
                            x.Year,
                            x.Month,
                        })
                         .Select(x => new OAnalytics.Data
                         {
                             Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
                             TotalTransaction = x.Sum(a => a.Transactions),
                             TotalCustomer = x.Sum(a => a.Customers),
                             TotalAmount = x.Sum(a => a.Amount),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         }).ToList();
                    }
                    else if (DaysDifference == 6)
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => x.DayOfWeek)
                         .Select(x => new OAnalytics.Data
                         {
                             Title = x.Key.ToString(),
                             TotalTransaction = x.Sum(a => a.Transactions),
                             TotalCustomer = x.Sum(a => a.Customers),
                             TotalAmount = x.Sum(a => a.Amount),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         }).ToList();
                    }
                    else
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => x.Date)
                         .Select(x => new OAnalytics.Data
                         {
                             Title = x.Key.Value.ToString("dd-MM-yyyy"),
                             TotalTransaction = x.Sum(a => a.Transactions),
                             TotalCustomer = x.Sum(a => a.Customers),
                             TotalAmount = x.Sum(a => a.Amount),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         }).ToList();
                    }
                    if (_AnalyticsResponse.Data.Count > 0)
                    {
                        _AnalyticsResponse.TotalTransaction = _AnalyticsResponse.Data.Sum(x => x.TotalTransaction);
                        _AnalyticsResponse.TotalCustomer = _AnalyticsResponse.Data.Sum(x => x.TotalCustomer);
                        _AnalyticsResponse.TotalAmount = _AnalyticsResponse.Data.Sum(x => x.TotalAmount);
                        _AnalyticsResponse.TotalInvoiceAmount = _AnalyticsResponse.Data.Sum(x => x.TotalInvoiceAmount);
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AnalyticsResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRewardClaimHistory", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the commission history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCommissionHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _AnalyticsResponse = new OAnalytics.Response();
                    long DaysDifference = (_Request.EndDate.Value.Date - _Request.StartDate.Value.Date).Days;
                    var _OTrList = _HCoreContext.cmt_loyalty
                        .Where(x => x.merchant.CountryId == _Request.UserReference.SystemCountry && x.transaction_date.Date > _Request.StartDate && x.transaction_date.Date < _Request.EndDate)
                       .GroupBy(x => x.transaction_date.Date)
                       .OrderBy(x => x.Key)
                       .Select(x => new OTSale
                       {
                           Date = x.Key,
                           Year = x.Key.Year,
                           Month = x.Key.Month,
                           Day = x.Key.Day,
                           DayOfWeek = x.Key.DayOfWeek,
                           Transactions = x.Count(),
                           Amount = x.Sum(x => x.commission_amount),
                           InvoiceAmount = x.Sum(x => x.invoice_amount),
                           Customers = x.Select(a => a.customer_id).Distinct().Count(),
                       }).ToList();
                    // var _OTrList =_HCoreContext.anxcommissiondaily.Where(x => x.Date > _Request.StartDate
                    //&& x.Date < _Request.EndDate)
                    //     .OrderBy(x => x.Date)
                    //     .Select(x => new OTSale
                    //     {
                    //         Date = x.Date,
                    //         Year = x.Date.Value.Year,
                    //         Month = x.Date.Value.Month,
                    //         Day = x.Date.Value.Day,
                    //         DayOfWeek = x.Date.Value.DayOfWeek,
                    //         Transactions = x.Transactions,
                    //         Amount = x.Amount,
                    //         InvoiceAmount = x.InvoiceAmount,
                    //         Customers = x.Customers,
                    //     }).ToList();
                    if (DaysDifference > 365)
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => x.Year)
                       .Select(x => new OAnalytics.Data
                       {
                           Title = x.Key.ToString(),
                           TotalTransaction = x.Sum(a => a.Transactions),
                           TotalCustomer = x.Sum(a => a.Customers),
                           TotalAmount = x.Sum(a => a.Amount),
                           TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                       }).ToList();
                    }
                    else if (DaysDifference < 366 && DaysDifference > 31)
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => new
                        {
                            x.Year,
                            x.Month,
                        })
                         .Select(x => new OAnalytics.Data
                         {
                             Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
                             TotalTransaction = x.Sum(a => a.Transactions),
                             TotalCustomer = x.Sum(a => a.Customers),
                             TotalAmount = x.Sum(a => a.Amount),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         }).ToList();
                    }
                    else if (DaysDifference == 6)
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => x.DayOfWeek)
                         .Select(x => new OAnalytics.Data
                         {
                             Title = x.Key.ToString(),
                             TotalTransaction = x.Sum(a => a.Transactions),
                             TotalCustomer = x.Sum(a => a.Customers),
                             TotalAmount = x.Sum(a => a.Amount),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         }).ToList();
                    }
                    else
                    {
                        _AnalyticsResponse.Data = _OTrList.GroupBy(x => x.Date)
                         .Select(x => new OAnalytics.Data
                         {
                             Title = x.Key.Value.ToString("dd-MM-yyyy"),
                             TotalTransaction = x.Sum(a => a.Transactions),
                             TotalCustomer = x.Sum(a => a.Customers),
                             TotalAmount = x.Sum(a => a.Amount),
                             TotalInvoiceAmount = x.Sum(a => a.InvoiceAmount),
                         }).ToList();
                    }
                    if (_AnalyticsResponse.Data.Count > 0)
                    {
                        _AnalyticsResponse.TotalTransaction = _AnalyticsResponse.Data.Sum(x => x.TotalTransaction);
                        _AnalyticsResponse.TotalCustomer = _AnalyticsResponse.Data.Sum(x => x.TotalCustomer);
                        _AnalyticsResponse.TotalAmount = _AnalyticsResponse.Data.Sum(x => x.TotalAmount);
                        _AnalyticsResponse.TotalInvoiceAmount = _AnalyticsResponse.Data.Sum(x => x.TotalInvoiceAmount);
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AnalyticsResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCommissionHistory", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
            #endregion
        }
    }
}
