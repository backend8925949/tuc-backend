//==================================================================================
// FileName: AnaCustom.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to customer analytics
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Console.Analytics;
using HCore.TUC.Core.Object.Console.Analytics.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.Framework.Console.Analytics.Custom
{
    internal class AnaCustom
    {
        private class OTrList
        {
            public DateTime CreateDate { get; set; }
            public double RewardAmount { get; set; }
            public double RedeemAmount { get; set; }
            public DateTime TransactionDate { get; set; }
            public double InvoiceAmount { get; set; }
            public long? UserAccountId { get; set; }
            public double? CommissionAmount { get; set; }
        }
        List<OTrList> _OTrList;
        HCoreContext _HCoreContext;
        OAnaCustom.CustomerCount _OAnaCustomerCount;
        OAnaCustom.Balance _OAnaBalanceOverview;
        /// <summary>
        /// Description: Gets the customers count.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomersCount(OAnaCustom.Request _Request)
        {
            _OAnaCustomerCount = new OAnaCustom.CustomerCount();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime BaseStartDate = _Request.BaseDate.Date.AddHours(-1);
                    DateTime BaseEndDate = _Request.BaseDate.Date.AddHours(24).AddHours(-1);

                    var baseDate = _Request.BaseDate.Date;
                    var thisWeekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek).AddHours(-1);
                    var thisWeekEnd = thisWeekStart.AddDays(7).AddSeconds(-1).AddHours(-1);
                    var lastWeekStart = thisWeekStart.AddDays(-7).AddHours(-1);
                    var lastWeekEnd = thisWeekStart.AddSeconds(-1).AddHours(-1);
                    var thisMonthStart = baseDate.AddDays(1 - baseDate.Day).AddHours(-1);
                    var thisMonthEnd = thisMonthStart.AddMonths(1).AddSeconds(-1).AddHours(-1);
                    var lastMonthStart = thisMonthStart.AddMonths(-1).AddHours(-1);
                    var lastMonthEnd = thisMonthStart.AddSeconds(-1).AddHours(-1);

                    _OAnaCustomerCount.TotalCustomers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.SystemCountry && x.CreateDate < BaseEndDate);
                    _OAnaCustomerCount.DailyActiveCustomers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.SystemCountry && x.LastTransactionDate > BaseStartDate && x.LastTransactionDate < BaseEndDate);
                    _OAnaCustomerCount.WeeklyActiveCustomers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.SystemCountry && x.LastTransactionDate > thisWeekStart && x.LastTransactionDate < thisWeekEnd);
                    _OAnaCustomerCount.MonthlyActiveCustomers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.SystemCountry && x.LastTransactionDate > thisMonthStart && x.LastTransactionDate < thisMonthEnd);

                    _OAnaCustomerCount.ThisMonthNewCustomers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.SystemCountry && x.CreateDate > thisMonthStart && x.CreateDate < thisMonthEnd);
                    _OAnaCustomerCount.ThisMonthIdleCustomers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.SystemCountry
                                                                                                    && x.HCUAccountTransactionAccount.Any(a => a.TransactionDate > thisMonthStart && a.TransactionDate < thisMonthEnd) == false
                                                                                                    && x.HCUAccountTransactionAccount.Any(a => a.TransactionDate > lastMonthStart && a.TransactionDate < lastMonthEnd) == true);
                    _OAnaCustomerCount.ThisMonthLostCustomers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.SystemCountry && x.LastTransactionDate < lastMonthStart);


                    //_OAnaCustomerCount.TotalCustomers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CreateDate < BaseEndDate);
                    //_OAnaCustomerCount.DailyActiveCustomers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.HCUAccountTransactionAccount.Any(a => a.TransactionDate > BaseStartDate && a.TransactionDate < BaseEndDate) == true);
                    //_OAnaCustomerCount.WeeklyActiveCustomers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.HCUAccountTransactionAccount.Any(a => a.TransactionDate > thisWeekStart && a.TransactionDate < thisWeekEnd) == true);
                    //_OAnaCustomerCount.MonthlyActiveCustomers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.HCUAccountTransactionAccount.Any(a => a.TransactionDate > thisMonthStart && a.TransactionDate < thisMonthEnd) == true);

                    //_OAnaCustomerCount.ThisMonthNewCustomers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser && x.CreateDate > thisMonthStart && x.CreateDate < thisMonthEnd);
                    //_OAnaCustomerCount.ThisMonthIdleCustomers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                                                                && x.HCUAccountTransactionAccount.Any(a => a.TransactionDate > thisMonthStart && a.TransactionDate < thisMonthEnd) == false
                    //                                                                                && x.HCUAccountTransactionAccount.Any(a => a.TransactionDate > lastMonthStart && a.TransactionDate < lastMonthEnd) == true);
                    //_OAnaCustomerCount.ThisMonthLostCustomers = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == UserAccountType.Appuser
                    //                                                                                && x.HCUAccountTransactionAccount.Any(a => a.TransactionDate > lastMonthStart && a.TransactionDate < thisMonthEnd) == false);
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OAnaCustomerCount, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersCount", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the customers balance overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomersBalanceOverview(OAnaCustom.Request _Request)
        {
            _OAnaCustomerCount = new OAnaCustom.CustomerCount();
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime BaseStartDate = _Request.BaseDate.Date.AddHours(-1);
                    DateTime BaseEndDate = _Request.BaseDate.Date.AddHours(24).AddHours(-1);
                    _OAnaBalanceOverview = new OAnaCustom.Balance();
                    var baseDate = _Request.BaseDate.Date;
                    var thisWeekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek).AddHours(-1);
                    var thisWeekEnd = thisWeekStart.AddDays(7).AddSeconds(-1).AddHours(-1);
                    var lastWeekStart = thisWeekStart.AddDays(-7).AddHours(-1);
                    var lastWeekEnd = thisWeekStart.AddSeconds(-1).AddHours(-1);
                    var thisMonthStart = baseDate.AddDays(1 - baseDate.Day).AddHours(-1);
                    var thisMonthEnd = thisMonthStart.AddMonths(1).AddSeconds(-1).AddHours(-1);
                    var lastMonthStart = thisMonthStart.AddMonths(-1).AddHours(-1);
                    var lastMonthEnd = thisMonthStart.AddSeconds(-1).AddHours(-1);
                    //long TucTransactions = _HCoreContext.HCUAccountTransaction
                    //    .Count(x => x.Account.AccountTypeId == UserAccountType.Appuser
                    //    && x.SourceId == TransactionSource.TUC
                    //    && (x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit)
                    //    && x.StatusId == HelperStatus.Transaction.Success
                    //    && x.TransactionDate > thisMonthStart && x.TransactionDate < thisMonthEnd);
                    _OAnaBalanceOverview.AverageRevenue = 0;
                    _OAnaBalanceOverview.AverageWalletBalance = 0;


                    long TucCustomers = _HCoreContext.HCUAccountTransaction
                       .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                        && x.Account.CountryId == _Request.UserReference.SystemCountry
                       && x.SourceId == TransactionSource.TUC
                       && (x.ModeId == TransactionMode.Credit || x.ModeId == TransactionMode.Debit)
                       && x.StatusId == HelperStatus.Transaction.Success
                       && x.TransactionDate > thisMonthStart && x.TransactionDate < thisMonthEnd)
                       .Select(x => x.AccountId).Distinct().Count();
                    if (TucCustomers > 0)
                    {
                        double TucCredit = _HCoreContext.HCUAccountTransaction
                  .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                  && x.Account.CountryId == _Request.UserReference.SystemCountry && x.SourceId == TransactionSource.TUC
                  && x.ModeId == TransactionMode.Credit
                  && x.StatusId == HelperStatus.Transaction.Success
                  && x.TransactionDate > thisMonthStart && x.TransactionDate < thisMonthEnd)
                  .Sum(x => x.TotalAmount);
                        double TucDebit = _HCoreContext.HCUAccountTransaction
                       .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                     && x.Account.CountryId == _Request.UserReference.SystemCountry && x.SourceId == TransactionSource.TUC
                       && x.ModeId == TransactionMode.Debit
                       && x.StatusId == HelperStatus.Transaction.Success
                       && x.TransactionDate > thisMonthStart && x.TransactionDate < thisMonthEnd)
                       .Sum(x => x.TotalAmount);
                        _OAnaBalanceOverview.AverageWalletBalance = Math.Round((TucCredit - TucDebit) / TucCustomers, 2);
                    }
                    if (_OAnaBalanceOverview.AverageWalletBalance < 0)
                    {
                        _OAnaBalanceOverview.AverageWalletBalance = 0;
                    }
                    long RewardCustomers = _HCoreContext.HCUAccountTransaction
                                                     .Where(m => m.TransactionDate > thisMonthStart
                                                      && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                      && m.TransactionDate < thisMonthEnd
                                                      && m.TotalAmount > 0
                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                      && (((m.ModeId == TransactionMode.Credit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                      || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                    ).Select(x => x.AccountId).Distinct().Count();
                    if (RewardCustomers > 0)
                    {
                        double RewardCommissionAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(m => m.TransactionDate > thisMonthStart
                                                      && m.Account.CountryId == _Request.UserReference.SystemCountry
                                                     && m.TransactionDate < thisMonthEnd
                                                 && m.TotalAmount > 0
                                                 && m.StatusId == HelperStatus.Transaction.Success
                                                 && (((m.ModeId == TransactionMode.Credit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                ).Sum(x => (double?)x.ParentTransaction.ComissionAmount) ?? 0;
                        _OAnaBalanceOverview.AverageRevenue = Math.Round(RewardCommissionAmount / RewardCustomers, 2);
                    }

                    if (_OAnaBalanceOverview.AverageWalletBalance < 0)
                    {
                        _OAnaBalanceOverview.AverageWalletBalance = 0;
                    }
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OAnaBalanceOverview, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCustomersBalanceOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the customer registration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomerRegistration(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    long DaysDifference = (_Request.EndDate.Value.Date - _Request.StartDate.Value.Date).Days;
                    _OTrList = new List<OTrList>();
                    _OTrList = _HCoreContext.HCUAccount
                                        .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                      && x.CountryId == _Request.UserReference.CountryId
                                        && x.CreateDate > _Request.StartDate
                                        && x.CreateDate < _Request.EndDate)
                                       .Select(x => new OTrList
                                       {
                                           CreateDate = x.CreateDate
                                       }).ToList();
                    if (DaysDifference > 364)
                    {
                        var TM = _OTrList.GroupBy(x => x.CreateDate.Year)
                       .Select(x => new OAnalytics.SalesMetrics
                       {
                           Title = x.Key.ToString(),
                           Year = x.Key,
                           Total = x.Count(),
                       }).ToList();
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                        #endregion
                    }
                    else if (DaysDifference < 365 && DaysDifference > 31)
                    {
                        var TM = _OTrList.GroupBy(x => new
                        {
                            x.CreateDate.Date.Year,
                            x.CreateDate.Date.Month,
                        })
                     .Select(x => new OAnalytics.SalesMetrics
                     {
                         Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
                         Year = x.Key.Year,
                         Month = x.Key.Month,
                         Total = x.Count(),
                     }).ToList();
                        foreach (var item in TM)
                        {

                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                        #endregion
                    }
                    else if (DaysDifference > 2 && DaysDifference < 8)
                    {
                        var TM = _OTrList
                            .GroupBy(x => x.CreateDate.DayOfWeek)
                         .Select(x => new OAnalytics.SalesMetrics
                         {
                             Title = x.Key.ToString(),
                             Total = x.Count(),
                         }).ToList();
                        foreach (var item in TM)
                        {

                        }

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                    }
                    else if (DaysDifference < 2)
                    {
                        var TM = _OTrList.GroupBy(x => x.CreateDate.Hour)
                         .Select(x => new OAnalytics.SalesMetrics
                         {
                             Title = x.Key.ToString(),
                             Hour = x.Key,
                             Total = x.Count(),
                         }).ToList();
                        foreach (var item in TM)
                        {
                            var hours = Convert.ToInt32(item.Title);
                            int minutes = 0;
                            var amPmDesignator = "AM";
                            if (hours == 0)
                                hours = 12;
                            else if (hours == 12)
                                amPmDesignator = "PM";
                            else if (hours > 12)
                            {
                                hours -= 12;
                                amPmDesignator = "PM";
                            }
                            item.Title = String.Format("{0}:{1:00} {2}", hours, minutes, amPmDesignator);
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                    }
                    else
                    {
                        var TM = _OTrList.GroupBy(x => x.CreateDate.Date)
                         .Select(x => new OAnalytics.SalesMetrics
                         {
                             Title = x.Key.ToString("dd-MM-yyyy"),
                             Date = x.Key,
                             Total = x.Count(),
                         }).ToList();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCustomerRegistration", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the application downloads.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAppDownloads(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    long DaysDifference = (_Request.EndDate.Value.Date - _Request.StartDate.Value.Date).Days;
                    _OTrList = new List<OTrList>();
                    _OTrList = _HCoreContext.HCUAccountSession.Where(x => x.Account.CountryId == _Request.UserReference.CountryId && x.Account.AccountTypeId == UserAccountType.Appuser
                    && x.LoginDate > _Request.StartDate && x.LoginDate < _Request.EndDate)
                        .Select(x => new OTrList
                        {
                            CreateDate = x.LoginDate
                        }).ToList();
                    //_OTrList = _HCoreContext.HCUAccount
                    //                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                    //                    && x.HCUAccountSessionAccount.Any(a=>a.LoginDate.Date ==  x.CreateDate.Date) == true)
                    //                   .Select(x => new OTrList
                    //                   {
                    //                       CreateDate = x.CreateDate
                    //                   }).ToList();
                    if (DaysDifference > 364)
                    {
                        var TM = _OTrList.GroupBy(x => x.CreateDate.Year)
                           .Select(x => new OAnalytics.SalesMetrics
                           {
                               Title = x.Key.ToString(),
                               Year = x.Key,
                               Total = x.Count(),
                           }).ToList();
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                        #endregion
                    }
                    else if (DaysDifference < 365 && DaysDifference > 31)
                    {
                        var TM = _OTrList.GroupBy(x => new
                        {
                            x.CreateDate.Date.Year,
                            x.CreateDate.Date.Month,
                        })
                     .Select(x => new OAnalytics.SalesMetrics
                     {
                         Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
                         Year = x.Key.Year,
                         Month = x.Key.Month,
                         Total = x.Count(),
                     }).ToList();
                        foreach (var item in TM)
                        {

                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                        #endregion
                    }
                    else if (DaysDifference > 2 && DaysDifference < 8)
                    {
                        var TM = _OTrList
                            .GroupBy(x => x.CreateDate.DayOfWeek)
                         .Select(x => new OAnalytics.SalesMetrics
                         {
                             Title = x.Key.ToString(),
                             Total = x.Count(),
                         }).ToList();
                        foreach (var item in TM)
                        {

                        }

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                    }
                    else if (DaysDifference < 2)
                    {
                        var TM = _OTrList.GroupBy(x => x.TransactionDate.Hour)
                         .Select(x => new OAnalytics.SalesMetrics
                         {
                             Title = x.Key.ToString(),
                             Hour = x.Key,
                             Total = x.Count(),
                         }).ToList();
                        foreach (var item in TM)
                        {
                            var hours = Convert.ToInt32(item.Title);
                            int minutes = 0;
                            var amPmDesignator = "AM";
                            if (hours == 0)
                                hours = 12;
                            else if (hours == 12)
                                amPmDesignator = "PM";
                            else if (hours > 12)
                            {
                                hours -= 12;
                                amPmDesignator = "PM";
                            }
                            item.Title = String.Format("{0}:{1:00} {2}", hours, minutes, amPmDesignator);
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                    }
                    else
                    {
                        var TM = _OTrList.GroupBy(x => x.TransactionDate.Date)
                         .Select(x => new OAnalytics.SalesMetrics
                         {
                             Title = x.Key.ToString("dd-MM-yyyy"),
                             Date = x.Key,
                             Total = x.Count(),
                         }).ToList();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCustomerRegistration", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the customer visits.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomerVisits(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    long DaysDifference = (_Request.EndDate.Value.Date - _Request.StartDate.Value.Date).Days;
                    _OTrList = new List<OTrList>();
                    _OTrList = _HCoreContext.HCUAccountTransaction
                                        .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                                      && x.Account.CountryId == _Request.UserReference.SystemCountry
                                        && x.TransactionDate > _Request.StartDate
                                        && x.TransactionDate < _Request.EndDate
                                        && x.StatusId == HelperStatus.Transaction.Success
                                        && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus)
                                        && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward))
                                       .Select(x => new OTrList
                                       {
                                           UserAccountId = x.AccountId,
                                           TransactionDate = x.TransactionDate.AddHours(1),
                                       }).ToList();
                    if (DaysDifference > 365)
                    {
                        var TM = _OTrList.GroupBy(x => x.TransactionDate.Year)
                       .Select(x => new OAnalytics.SalesMetrics
                       {
                           Title = x.Key.ToString(),
                           Year = x.Key,
                           TotalTransaction = x.Count(),
                       }).ToList();
                        foreach (var item in TM)
                        {
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                        #endregion
                    }
                    else if (DaysDifference < 366 && DaysDifference > 31)
                    {
                        var TM = _OTrList.GroupBy(x => new
                        {
                            x.TransactionDate.Date.Year,
                            x.TransactionDate.Date.Month,
                        })
                     .Select(x => new OAnalytics.SalesMetrics
                     {
                         Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
                         Year = x.Key.Year,
                         Month = x.Key.Month,
                         Total = x.Count(),
                     }).ToList();
                        foreach (var item in TM)
                        {

                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                        #endregion
                    }
                    else if (DaysDifference > 2 && DaysDifference < 8)
                    {
                        var TM = _OTrList.GroupBy(x => x.TransactionDate.Date.DayOfWeek)
                   .Select(x => new OAnalytics.SalesMetrics
                   {
                       Title = x.Key.ToString(),
                       Total = x.Count(),
                   }).ToList();
                        foreach (var item in TM)
                        {

                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                        #endregion
                    }
                    else if (DaysDifference < 2)
                    {
                        var TM = _OTrList.GroupBy(x => x.TransactionDate.Hour)
                         .Select(x => new OAnalytics.SalesMetrics
                         {
                             Title = x.Key.ToString(),
                             Hour = x.Key,
                             Total = x.Count(),
                         }).ToList();
                        foreach (var item in TM)
                        {
                            var hours = Convert.ToInt32(item.Title);
                            int minutes = 0;
                            var amPmDesignator = "AM";
                            if (hours == 0)
                                hours = 12;
                            else if (hours == 12)
                                amPmDesignator = "PM";
                            else if (hours > 12)
                            {
                                hours -= 12;
                                amPmDesignator = "PM";
                            }
                            item.Title = String.Format("{0}:{1:00} {2}", hours, minutes, amPmDesignator);
                        }

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                    }
                    else
                    {
                        var TM = _OTrList.GroupBy(x => x.TransactionDate.Date)
                         .Select(x => new OAnalytics.SalesMetrics
                         {
                             Title = x.Key.ToString("dd-MM-yyyy"),
                             Date = x.Key,
                             Total = x.Count(),
                         }).ToList();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCustomerVisits", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

    }
}
