﻿using System;
using HCore.Helper;
using HCore.Data;
using HCore.Data.Models;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.TUC.Core.Object.Console.Analytics;
using static HCore.TUC.Core.Object.Console.Analytics.OReports;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant;
// using HCore.Integration.BNPLHelpers;
using Akka.Actor;

namespace HCore.TUC.Core.Framework.Console.Analytics
{
    public class FrameworkReports
    {
        HCoreContext _HCoreContext;
        List<OReports.SalesReport> _SalesReport;
        List<OReports.RewardReport> _RewardReport;
        List<OReports.RedeemReport> _RedeemReport;
        List<OReports.RewardClaimReport> _RewardClaimReport;
        List<OReports.DownloadReports> _DownloadReports;

        internal OResponse GetSalesReport(OList.Request _Request)
        {
            try
            {
                _SalesReport = new List<OReports.SalesReport>();
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorDownloadSalesReport");
                    var _ActorNotify = _Actor.ActorOf<ActorDownloadSalesReport>("ActorDownloadSalesReport");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "MerchantId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                && (x.HCUAccountTransactionAccount.Any() || x.HCUAccountTransactionParent.Any()))
                                                .Select(x => new OReports.SalesReport
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    MerchantId = x.Id,
                                                    MerchantKey = x.Guid,
                                                    MerchantDisplayName = x.DisplayName,
                                                    MerchantAddress = x.Address,

                                                    Stores = _HCoreContext.HCUAccount.Where(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore)
                                                             .Select(m => new Stores
                                                             {
                                                                 ReferenceId = m.Id,
                                                                 ReferenceKey = m.Guid,
                                                                 DisplayName = m.DisplayName,
                                                                 Address = m.Address,

                                                                 CardTransactions = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.TypeId == TransactionType.CardReward).Count(),
                                                                 CardTransactionsAmount = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.TypeId == TransactionType.CardReward).Sum(a => a.TotalAmount),

                                                                 CashTransactions = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.TypeId == TransactionType.CashReward).Count(),
                                                                 CashTransactionsAmount = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.TypeId == TransactionType.CashReward).Sum(a => a.TotalAmount),

                                                                 SuccessfullTransactions = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.StatusId == HelperStatus.Transaction.Success).Count(),
                                                                 SuccessfullTransactionsAmount = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.StatusId == HelperStatus.Transaction.Success).Sum(a => a.TotalAmount),

                                                                 FailedTransactions = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.StatusId == HelperStatus.Transaction.Failed).Count(),
                                                                 FailedTransactionsAmount = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.StatusId == HelperStatus.Transaction.Failed).Sum(a => a.TotalAmount),
                                                             }).ToList(),

                                                    TotalStores = _HCoreContext.HCUAccount.Count(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore),

                                                    CardTransactions = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.TypeId == TransactionType.CardReward).Count(),
                                                    CardTransactionsAmount = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.TypeId == TransactionType.CardReward).Sum(a => a.TotalAmount),

                                                    CashTransactions = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.TypeId == TransactionType.CashReward).Count(),
                                                    CashTransactionsAmount = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.TypeId == TransactionType.CashReward).Sum(a => a.TotalAmount),

                                                    SuccessfullTransactions = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.StatusId == HelperStatus.Transaction.Success).Count(),
                                                    SuccessfullTransactionsAmount = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.StatusId == HelperStatus.Transaction.Success).Sum(a => a.TotalAmount),

                                                    FailedTransactions = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.StatusId == HelperStatus.Transaction.Failed).Count(),
                                                    FailedTransactionsAmount = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.StatusId == HelperStatus.Transaction.Failed).Sum(a => a.TotalAmount),
                                                }).Where(_Request.SearchCondition)
                                                .Count();
                    }

                    _SalesReport = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                           && (x.HCUAccountTransactionAccount.Any() || x.HCUAccountTransactionParent.Any()))
                                                           .Select(x => new OReports.SalesReport
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,

                                                               MerchantId = x.Id,
                                                               MerchantKey = x.Guid,
                                                               MerchantDisplayName = x.DisplayName,
                                                               MerchantAddress = x.Address,

                                                               Stores = _HCoreContext.HCUAccount.Where(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore)
                                                             .Select(m => new Stores
                                                             {
                                                                 ReferenceId = m.Id,
                                                                 ReferenceKey = m.Guid,
                                                                 DisplayName = m.DisplayName,
                                                                 Address = m.Address,

                                                                 CardTransactions = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.TypeId == TransactionType.CardReward).Count(),
                                                                 CardTransactionsAmount = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.TypeId == TransactionType.CardReward).Sum(a => a.TotalAmount),

                                                                 CashTransactions = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.TypeId == TransactionType.CashReward).Count(),
                                                                 CashTransactionsAmount = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.TypeId == TransactionType.CashReward).Sum(a => a.TotalAmount),

                                                                 SuccessfullTransactions = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.StatusId == HelperStatus.Transaction.Success).Count(),
                                                                 SuccessfullTransactionsAmount = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.StatusId == HelperStatus.Transaction.Success).Sum(a => a.TotalAmount),

                                                                 FailedTransactions = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.StatusId == HelperStatus.Transaction.Failed).Count(),
                                                                 FailedTransactionsAmount = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.StatusId == HelperStatus.Transaction.Failed).Sum(a => a.TotalAmount),
                                                             }).ToList(),

                                                               TotalStores = _HCoreContext.HCUAccount.Count(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore),

                                                               CardTransactions = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.TypeId == TransactionType.CardReward).Count(),
                                                               CardTransactionsAmount = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.TypeId == TransactionType.CardReward).Sum(a => a.TotalAmount),

                                                               CashTransactions = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.TypeId == TransactionType.CashReward).Count(),
                                                               CashTransactionsAmount = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.TypeId == TransactionType.CashReward).Sum(a => a.TotalAmount),

                                                               SuccessfullTransactions = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.StatusId == HelperStatus.Transaction.Success).Count(),
                                                               SuccessfullTransactionsAmount = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.StatusId == HelperStatus.Transaction.Success).Sum(a => a.TotalAmount),

                                                               FailedTransactions = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.StatusId == HelperStatus.Transaction.Failed).Count(),
                                                               FailedTransactionsAmount = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.StatusId == HelperStatus.Transaction.Failed).Sum(a => a.TotalAmount),
                                                           }).Where(_Request.SearchCondition)
                                   .OrderBy(_Request.SortExpression)
                                   .Skip(_Request.Offset)
                                   .Take(_Request.Limit)
                                   .ToList();

                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _SalesReport, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetSalesReport", _Exception, _Request.UserReference, "HCR0500", "Unable to get sales report.");
            }
        }

        internal OResponse GetRewardReport(OList.Request _Request)
        {
            try
            {
                _RewardReport = new List<OReports.RewardReport>();
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorDownloadRewardReport");
                    var _ActorNotify = _Actor.ActorOf<ActorDownloadRewardReport>("ActorDownloadRewardReport");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                && (x.HCUAccountTransactionAccount.Any() || x.HCUAccountTransactionParent.Any()))
                                                .Select(x => new OReports.RewardReport
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    MerchantId = x.Id,
                                                    MerchantKey = x.Guid,
                                                    MerchantDisplayName = x.DisplayName,
                                                    MerchantAddress = x.Address,

                                                    Stores = _HCoreContext.HCUAccount.Where(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore)
                                                             .Select(m => new Stores
                                                             {
                                                                 ReferenceId = m.Id,
                                                                 ReferenceKey = m.Guid,
                                                                 DisplayName = m.DisplayName,
                                                                 Address = m.Address,

                                                                 RewardTransactions = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.SubParentId == m.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Count(),

                                                                 RewardTransactionsAmount = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.SubParentId == m.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount),

                                                                 CustomerRewardAmount = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.SubParentId == m.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount),

                                                                 TUCCommission = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.SubParentId == m.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => (a.ReferenceAmount - a.TotalAmount)),
                                                             }).ToList(),

                                                    TotalStores = _HCoreContext.HCUAccount.Count(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore),

                                                    RewardTransactions = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Count(),

                                                    RewardTransactionsAmount = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount),

                                                    CustomerRewardAmount = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount),

                                                    TUCCommission = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => (a.ReferenceAmount - a.TotalAmount)),
                                                }).Where(_Request.SearchCondition)
                                                .Count();
                    }

                    _RewardReport = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                           && (x.HCUAccountTransactionAccount.Any() || x.HCUAccountTransactionParent.Any()))
                                                           .Select(x => new OReports.RewardReport
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,

                                                               MerchantId = x.Id,
                                                               MerchantKey = x.Guid,
                                                               MerchantDisplayName = x.DisplayName,
                                                               MerchantAddress = x.Address,

                                                               Stores = _HCoreContext.HCUAccount.Where(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore)
                                                             .Select(m => new Stores
                                                             {
                                                                 ReferenceId = m.Id,
                                                                 ReferenceKey = m.Guid,
                                                                 DisplayName = m.DisplayName,
                                                                 Address = m.Address,

                                                                 RewardTransactions = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.SubParentId == m.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Count(),

                                                                 RewardTransactionsAmount = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.SubParentId == m.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount),

                                                                 CustomerRewardAmount = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.SubParentId == m.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount),

                                                                 TUCCommission = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.SubParentId == m.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => (a.ReferenceAmount - a.TotalAmount)),
                                                             }).ToList(),

                                                               TotalStores = _HCoreContext.HCUAccount.Count(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore),

                                                               RewardTransactions = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Count(),

                                                               RewardTransactionsAmount = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount),

                                                               CustomerRewardAmount = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount),

                                                               TUCCommission = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => (a.ReferenceAmount - a.TotalAmount)),
                                                           }).Where(_Request.SearchCondition)
                                   .OrderBy(_Request.SortExpression)
                                   .Skip(_Request.Offset)
                                   .Take(_Request.Limit)
                                   .ToList();

                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _RewardReport, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetRewardReport", _Exception, _Request.UserReference, "HCR0500", "Unable to get reward report.");
            }
        }

        internal OResponse GetRedeemReport(OList.Request _Request)
        {
            try
            {
                _RedeemReport = new List<OReports.RedeemReport>();
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorDownloadRedeemReport");
                    var _ActorNotify = _Actor.ActorOf<ActorDownloadRedeemReport>("ActorDownloadRedeemReport");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                && (x.HCUAccountTransactionAccount.Any() || x.HCUAccountTransactionParent.Any()))
                                                .Select(x => new OReports.RedeemReport
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    MerchantId = x.Id,
                                                    MerchantKey = x.Guid,
                                                    MerchantDisplayName = x.DisplayName,
                                                    MerchantAddress = x.Address,

                                                    Stores = _HCoreContext.HCUAccount.Where(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore)
                                                             .Select(m => new Stores
                                                             {
                                                                 ReferenceId = m.Id,
                                                                 ReferenceKey = m.Guid,
                                                                 DisplayName = m.DisplayName,
                                                                 Address = m.Address,

                                                                 RedeemTransactions = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.SubParentId == m.Id
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Debit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                                      && z.Type.SubParentId == TransactionTypeCategory.Redeem).Count(),

                                                                 RedeemTransactionsAmount = m.HCUAccountTransactionSubParent
                                                                                            .Where(z => z.SubParentId == m.Id
                                                                                            && z.ParentId == x.Id
                                                                                            && z.ModeId == TransactionMode.Debit
                                                                                            && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                                            && z.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.TotalAmount),

                                                                 CustomerRedeemAmount = m.HCUAccountTransactionSubParent
                                                                                       .Where(z => z.SubParentId == m.Id
                                                                                       && z.ParentId == x.Id
                                                                                       && z.StatusId == HelperStatus.Transaction.Success
                                                                                       && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                       && z.ModeId == TransactionMode.Debit
                                                                                       && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                                       && z.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.TotalAmount),
                                                             }).ToList(),

                                                    TotalStores = _HCoreContext.HCUAccount.Count(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore),

                                                    RedeemTransactions = x.HCUAccountTransactionParent
                                                                         .Where(z => z.ParentId == x.Id
                                                                         && z.ModeId == TransactionMode.Debit
                                                                         && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                         && z.Type.SubParentId == TransactionTypeCategory.Redeem).Count(),

                                                    RedeemTransactionsAmount = x.HCUAccountTransactionParent
                                                                               .Where(z => z.ParentId == x.Id
                                                                               && z.ModeId == TransactionMode.Debit
                                                                               && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                               && z.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.TotalAmount),

                                                    CustomerRedeemAmount = x.HCUAccountTransactionParent
                                                                           .Where(z => z.ParentId == x.Id
                                                                           && z.StatusId == HelperStatus.Transaction.Success
                                                                           && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                           && z.ModeId == TransactionMode.Debit
                                                                           && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                           && z.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.TotalAmount),
                                                }).Where(_Request.SearchCondition)
                                                .Count();
                    }

                    _RedeemReport = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                           && (x.HCUAccountTransactionAccount.Any() || x.HCUAccountTransactionParent.Any()))
                                                           .Select(x => new OReports.RedeemReport
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,

                                                               MerchantId = x.Id,
                                                               MerchantKey = x.Guid,
                                                               MerchantDisplayName = x.DisplayName,
                                                               MerchantAddress = x.Address,

                                                               Stores = _HCoreContext.HCUAccount.Where(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore)
                                                             .Select(m => new Stores
                                                             {
                                                                 ReferenceId = m.Id,
                                                                 ReferenceKey = m.Guid,
                                                                 DisplayName = m.DisplayName,
                                                                 Address = m.Address,

                                                                 RedeemTransactions = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.SubParentId == m.Id
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Debit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                                      && z.Type.SubParentId == TransactionTypeCategory.Redeem).Count(),

                                                                 RedeemTransactionsAmount = m.HCUAccountTransactionSubParent
                                                                                            .Where(z => z.SubParentId == m.Id
                                                                                            && z.ParentId == x.Id
                                                                                            && z.ModeId == TransactionMode.Debit
                                                                                            && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                                            && z.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.TotalAmount),

                                                                 CustomerRedeemAmount = m.HCUAccountTransactionSubParent
                                                                                       .Where(z => z.SubParentId == m.Id
                                                                                       && z.ParentId == x.Id
                                                                                       && z.StatusId == HelperStatus.Transaction.Success
                                                                                       && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                       && z.ModeId == TransactionMode.Debit
                                                                                       && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                                       && z.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.TotalAmount),
                                                             }).ToList(),

                                                               TotalStores = _HCoreContext.HCUAccount.Count(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore),

                                                               RedeemTransactions = x.HCUAccountTransactionParent
                                                                         .Where(z => z.ParentId == x.Id
                                                                         && z.ModeId == TransactionMode.Debit
                                                                         && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                         && z.Type.SubParentId == TransactionTypeCategory.Redeem).Count(),

                                                               RedeemTransactionsAmount = x.HCUAccountTransactionParent
                                                                               .Where(z => z.ParentId == x.Id
                                                                               && z.ModeId == TransactionMode.Debit
                                                                               && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                               && z.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.TotalAmount),

                                                               CustomerRedeemAmount = x.HCUAccountTransactionParent
                                                                           .Where(z => z.ParentId == x.Id
                                                                           && z.StatusId == HelperStatus.Transaction.Success
                                                                           && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                           && z.ModeId == TransactionMode.Debit
                                                                           && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                           && z.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.TotalAmount),
                                                           }).Where(_Request.SearchCondition)
                                   .OrderBy(_Request.SortExpression)
                                   .Skip(_Request.Offset)
                                   .Take(_Request.Limit)
                                   .ToList();

                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _RedeemReport, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetRedeemReport", _Exception, _Request.UserReference, "HCR0500", "Unable to get redeem report.");
            }
        }

        internal OResponse GetRewardClaimReport(OList.Request _Request)
        {
            try
            {
                _RewardClaimReport = new List<OReports.RewardClaimReport>();
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorDownloadRewardClaimReport");
                    var _ActorNotify = _Actor.ActorOf<ActorDownloadRewardClaimReport>("ActorDownloadRewardClaimReport");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                && (x.HCUAccountTransactionAccount.Any() || x.HCUAccountTransactionParent.Any()))
                                                .Select(x => new OReports.RewardClaimReport
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    MerchantId = x.Id,
                                                    MerchantKey = x.Guid,
                                                    MerchantDisplayName = x.DisplayName,
                                                    MerchantAddress = x.Address,

                                                    Stores = _HCoreContext.HCUAccount.Where(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore)
                                                             .Select(m => new Stores
                                                             {
                                                                 ReferenceId = m.Id,
                                                                 ReferenceKey = m.Guid,
                                                                 DisplayName = m.DisplayName,
                                                                 Address = m.Address,

                                                                 TucplusRewardTransactions = m.HCUAccountTransactionSubParent
                                                                                            .Where(z => z.SubParentId == m.Id
                                                                                            && z.ParentId == x.Id
                                                                                            && z.ModeId == TransactionMode.Debit
                                                                                            && z.SourceId == TransactionSource.ThankUCashPlus).Count(),

                                                                 TucplusRewardTransactionsAmount = m.HCUAccountTransactionSubParent
                                                                                                   .Where(z => z.SubParentId == m.Id
                                                                                                   && z.ParentId == x.Id
                                                                                                   && z.ModeId == TransactionMode.Debit
                                                                                                   && z.SourceId == TransactionSource.ThankUCashPlus).Sum(a => a.TotalAmount),

                                                                 RewardClaimAmount = m.HCUAccountTransactionSubParent
                                                                                     .Where(z => z.SubParentId == m.Id
                                                                                     && z.ParentId == x.Id
                                                                                     && z.ModeId == TransactionMode.Debit
                                                                                     && z.SourceId == TransactionSource.ThankUCashPlus).Sum(z => z.TotalAmount),

                                                             }).ToList(),

                                                    TotalStores = _HCoreContext.HCUAccount.Count(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore),

                                                    TucplusRewardTransactions = x.HCUAccountTransactionParent
                                                                                .Where(z => z.ParentId == x.Id
                                                                                && z.ModeId == TransactionMode.Debit
                                                                                && z.SourceId == TransactionSource.ThankUCashPlus).Count(),

                                                    TucplusRewardTransactionsAmount = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Debit
                                                                                      && z.SourceId == TransactionSource.ThankUCashPlus).Sum(a => a.TotalAmount),

                                                    RewardClaimAmount = x.HCUAccountTransactionParent
                                                                        .Where(z => z.ParentId == x.Id
                                                                        && z.ModeId == TransactionMode.Debit
                                                                        && z.SourceId == TransactionSource.ThankUCashPlus).Sum(z => z.TotalAmount),
                                                }).Where(_Request.SearchCondition)
                                                .Count();
                    }

                    _RewardClaimReport = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                           && (x.HCUAccountTransactionAccount.Any() || x.HCUAccountTransactionParent.Any()))
                                                           .Select(x => new OReports.RewardClaimReport
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,

                                                               MerchantId = x.Id,
                                                               MerchantKey = x.Guid,
                                                               MerchantDisplayName = x.DisplayName,
                                                               MerchantAddress = x.Address,

                                                               Stores = _HCoreContext.HCUAccount.Where(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore)
                                                             .Select(m => new Stores
                                                             {
                                                                 ReferenceId = m.Id,
                                                                 ReferenceKey = m.Guid,
                                                                 DisplayName = m.DisplayName,
                                                                 Address = m.Address,

                                                                 TucplusRewardTransactions = m.HCUAccountTransactionSubParent
                                                                                            .Where(z => z.SubParentId == m.Id
                                                                                            && z.ParentId == x.Id
                                                                                            && z.ModeId == TransactionMode.Debit
                                                                                            && z.SourceId == TransactionSource.ThankUCashPlus).Count(),

                                                                 TucplusRewardTransactionsAmount = m.HCUAccountTransactionSubParent
                                                                                                   .Where(z => z.SubParentId == m.Id
                                                                                                   && z.ParentId == x.Id
                                                                                                   && z.ModeId == TransactionMode.Debit
                                                                                                   && z.SourceId == TransactionSource.ThankUCashPlus).Sum(a => a.TotalAmount),

                                                                 RewardClaimAmount = m.HCUAccountTransactionSubParent
                                                                                     .Where(z => z.SubParentId == m.Id
                                                                                     && z.ParentId == x.Id
                                                                                     && z.ModeId == TransactionMode.Debit
                                                                                     && z.SourceId == TransactionSource.ThankUCashPlus).Count(),

                                                             }).ToList(),

                                                               TotalStores = _HCoreContext.HCUAccount.Count(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore),

                                                               TucplusRewardTransactions = x.HCUAccountTransactionParent
                                                                                .Where(z => z.ParentId == x.Id
                                                                                && z.ModeId == TransactionMode.Debit
                                                                                && z.SourceId == TransactionSource.ThankUCashPlus).Count(),

                                                               TucplusRewardTransactionsAmount = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Debit
                                                                                      && z.SourceId == TransactionSource.ThankUCashPlus).Sum(a => a.TotalAmount),

                                                               RewardClaimAmount = x.HCUAccountTransactionParent
                                                                        .Where(z => z.ParentId == x.Id
                                                                        && z.ModeId == TransactionMode.Debit
                                                                        && z.SourceId == TransactionSource.ThankUCashPlus).Count(),
                                                           }).Where(_Request.SearchCondition)
                                   .OrderBy(_Request.SortExpression)
                                   .Skip(_Request.Offset)
                                   .Take(_Request.Limit)
                                   .ToList();

                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _RewardClaimReport, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetRewardClaimReport", _Exception, _Request.UserReference, "HCR0500", "Unable to get reward claim report.");
            }
        }

        internal void DownloadSalesReport(OList.Request _Request)
        {
            try
            {
                _Request.Limit = 800;
                _DownloadReports = new List<OReports.DownloadReports>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "MerchantId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _Request.TotalRecords = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                && (x.HCUAccountTransactionAccount.Any() || x.HCUAccountTransactionParent.Any()))
                                                .Select(x => new OReports.SalesReport
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    MerchantId = x.Id,
                                                    MerchantKey = x.Guid,
                                                    MerchantDisplayName = x.DisplayName,
                                                    MerchantAddress = x.Address,

                                                    Stores = _HCoreContext.HCUAccount.Where(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore)
                                                             .Select(m => new Stores
                                                             {
                                                                 ReferenceId = m.Id,
                                                                 ReferenceKey = m.Guid,
                                                                 DisplayName = m.DisplayName,
                                                                 Address = m.Address,

                                                                 CardTransactions = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.TypeId == TransactionType.CardReward).Count(),
                                                                 CardTransactionsAmount = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.TypeId == TransactionType.CardReward).Sum(a => a.TotalAmount),

                                                                 CashTransactions = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.TypeId == TransactionType.CashReward).Count(),
                                                                 CashTransactionsAmount = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.TypeId == TransactionType.CashReward).Sum(a => a.TotalAmount),

                                                                 SuccessfullTransactions = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.StatusId == HelperStatus.Transaction.Success).Count(),
                                                                 SuccessfullTransactionsAmount = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.StatusId == HelperStatus.Transaction.Success).Sum(a => a.TotalAmount),

                                                                 FailedTransactions = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.StatusId == HelperStatus.Transaction.Failed).Count(),
                                                                 FailedTransactionsAmount = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.StatusId == HelperStatus.Transaction.Failed).Sum(a => a.TotalAmount),
                                                             }).ToList(),

                                                    TotalStores = _HCoreContext.HCUAccount.Count(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore),

                                                    CardTransactions = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.TypeId == TransactionType.CardReward).Count(),
                                                    CardTransactionsAmount = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.TypeId == TransactionType.CardReward).Sum(a => a.TotalAmount),

                                                    CashTransactions = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.TypeId == TransactionType.CashReward).Count(),
                                                    CashTransactionsAmount = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.TypeId == TransactionType.CashReward).Sum(a => a.TotalAmount),

                                                    SuccessfullTransactions = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.StatusId == HelperStatus.Transaction.Success).Count(),
                                                    SuccessfullTransactionsAmount = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.StatusId == HelperStatus.Transaction.Success).Sum(a => a.TotalAmount),

                                                    FailedTransactions = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.StatusId == HelperStatus.Transaction.Failed).Count(),
                                                    FailedTransactionsAmount = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.StatusId == HelperStatus.Transaction.Failed).Sum(a => a.TotalAmount),
                                                }).Where(_Request.SearchCondition)
                                                .Count();
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                           && (x.HCUAccountTransactionAccount.Any() || x.HCUAccountTransactionParent.Any()))
                                                           .Select(x => new OReports.SalesReport
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,

                                                               MerchantId = x.Id,
                                                               MerchantKey = x.Guid,
                                                               MerchantDisplayName = x.DisplayName,
                                                               MerchantAddress = x.Address,

                                                               Stores = _HCoreContext.HCUAccount.Where(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore)
                                                             .Select(m => new Stores
                                                             {
                                                                 ReferenceId = m.Id,
                                                                 ReferenceKey = m.Guid,
                                                                 DisplayName = m.DisplayName,
                                                                 Address = m.Address,

                                                                 CardTransactions = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.TypeId == TransactionType.CardReward).Count(),
                                                                 CardTransactionsAmount = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.TypeId == TransactionType.CardReward).Sum(a => a.TotalAmount),

                                                                 CashTransactions = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.TypeId == TransactionType.CashReward).Count(),
                                                                 CashTransactionsAmount = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.TypeId == TransactionType.CashReward).Sum(a => a.TotalAmount),

                                                                 SuccessfullTransactions = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.StatusId == HelperStatus.Transaction.Success).Count(),
                                                                 SuccessfullTransactionsAmount = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.StatusId == HelperStatus.Transaction.Success).Sum(a => a.TotalAmount),

                                                                 FailedTransactions = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.StatusId == HelperStatus.Transaction.Failed).Count(),
                                                                 FailedTransactionsAmount = m.HCUAccountTransactionSubParent.Where(z => z.SubParentId == m.Id && z.ParentId == x.Id && z.StatusId == HelperStatus.Transaction.Failed).Sum(a => a.TotalAmount),
                                                             }).ToList(),

                                                               TotalStores = _HCoreContext.HCUAccount.Count(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore),

                                                               CardTransactions = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.TypeId == TransactionType.CardReward).Count(),
                                                               CardTransactionsAmount = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.TypeId == TransactionType.CardReward).Sum(a => a.TotalAmount),

                                                               CashTransactions = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.TypeId == TransactionType.CashReward).Count(),
                                                               CashTransactionsAmount = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.TypeId == TransactionType.CashReward).Sum(a => a.TotalAmount),

                                                               SuccessfullTransactions = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.StatusId == HelperStatus.Transaction.Success).Count(),
                                                               SuccessfullTransactionsAmount = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.StatusId == HelperStatus.Transaction.Success).Sum(a => a.TotalAmount),

                                                               FailedTransactions = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.StatusId == HelperStatus.Transaction.Failed).Count(),
                                                               FailedTransactionsAmount = x.HCUAccountTransactionParent.Where(a => a.ParentId == x.Id && a.StatusId == HelperStatus.Transaction.Failed).Sum(a => a.TotalAmount),
                                                           }).Where(_Request.SearchCondition)
                                   .OrderBy(_Request.SortExpression)
                                   .Skip(_Request.Offset)
                                   .Take(_Request.Limit)
                                   .ToList();
                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            _DownloadReports.Add(new OReports.DownloadReports
                            {
                                MerchantId = _DataItem.MerchantId,
                                MerchantKey = _DataItem.MerchantKey,
                                MerchantDisplayName = _DataItem.MerchantDisplayName,
                                MerchantAddress = _DataItem.MerchantAddress,

                                Stores = _DataItem.Stores,

                                CardTransactions = _DataItem.CardTransactions,
                                CardTransactionsAmount = _DataItem.CardTransactionsAmount,

                                CashTransactions = _DataItem.CashTransactions,
                                CashTransactionsAmount = _DataItem.CashTransactionsAmount,

                                SuccessfullTransactions = _DataItem.SuccessfullTransactions,
                                SuccessfullTransactionsAmount = _DataItem.SuccessfullTransactionsAmount,

                                FailedTransactions = _DataItem.FailedTransactions,
                                FailedTransactionsAmount = _DataItem.FailedTransactionsAmount
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("Sales Report", _DownloadReports, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DownloadSalesReport", _Exception);
            }
        }

        internal void DownloadRewardReport(OList.Request _Request)
        {
            try
            {
                _Request.Limit = 800;
                _DownloadReports = new List<OReports.DownloadReports>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "MerchantId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _Request.TotalRecords =
                        _Request.TotalRecords = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                && (x.HCUAccountTransactionAccount.Any() || x.HCUAccountTransactionParent.Any()))
                                                .Select(x => new OReports.RewardReport
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    MerchantId = x.Id,
                                                    MerchantKey = x.Guid,
                                                    MerchantDisplayName = x.DisplayName,
                                                    MerchantAddress = x.Address,

                                                    Stores = _HCoreContext.HCUAccount.Where(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore)
                                                             .Select(m => new Stores
                                                             {
                                                                 ReferenceId = m.Id,
                                                                 ReferenceKey = m.Guid,
                                                                 DisplayName = m.DisplayName,
                                                                 Address = m.Address,

                                                                 RewardTransactions = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.ParentId == m.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Count(),

                                                                 RewardTransactionsAmount = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.ParentId == m.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount),

                                                                 CustomerRewardAmount = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.ParentId == m.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount),

                                                                 TUCCommission = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.ParentId == m.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => (a.ReferenceAmount - a.TotalAmount)),
                                                             }).ToList(),

                                                    TotalStores = _HCoreContext.HCUAccount.Count(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore),

                                                    RewardTransactions = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Count(),

                                                    RewardTransactionsAmount = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount),

                                                    CustomerRewardAmount = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount),

                                                    TUCCommission = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => (a.ReferenceAmount - a.TotalAmount)),
                                                }).Where(_Request.SearchCondition)
                                                .Count();
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                           && (x.HCUAccountTransactionAccount.Any() || x.HCUAccountTransactionParent.Any()))
                                                           .Select(x => new OReports.RewardReport
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,

                                                               MerchantId = x.Id,
                                                               MerchantKey = x.Guid,
                                                               MerchantDisplayName = x.DisplayName,
                                                               MerchantAddress = x.Address,

                                                               Stores = _HCoreContext.HCUAccount.Where(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore)
                                                             .Select(m => new Stores
                                                             {
                                                                 ReferenceId = m.Id,
                                                                 ReferenceKey = m.Guid,
                                                                 DisplayName = m.DisplayName,
                                                                 Address = m.Address,

                                                                 RewardTransactions = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.ParentId == m.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Count(),

                                                                 RewardTransactionsAmount = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.ParentId == m.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount),

                                                                 CustomerRewardAmount = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.ParentId == m.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount),

                                                                 TUCCommission = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.ParentId == m.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => (a.ReferenceAmount - a.TotalAmount)),
                                                             }).ToList(),

                                                               TotalStores = _HCoreContext.HCUAccount.Count(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore),

                                                               RewardTransactions = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Count(),

                                                               RewardTransactionsAmount = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount),

                                                               CustomerRewardAmount = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => a.ReferenceAmount),

                                                               TUCCommission = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id && z.StatusId != HCoreConstant.HelperStatus.Transaction.Pending
                                                                                      && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                      && z.ModeId == TransactionMode.Credit
                                                                                      && z.ParentTransaction.TotalAmount > 0
                                                                                      && z.CampaignId == null
                                                                                      && ((z.TypeId != TransactionType.ThankUCashPlusCredit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.TUCBlack))
                                                                                      || z.SourceId == TransactionSource.ThankUCashPlus)).Sum(a => (a.ReferenceAmount - a.TotalAmount)),
                                                           }).Where(_Request.SearchCondition)
                                   .OrderBy(_Request.SortExpression)
                                   .Skip(_Request.Offset)
                                   .Take(_Request.Limit)
                                   .ToList();
                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            _DownloadReports.Add(new OReports.DownloadReports
                            {
                                MerchantId = _DataItem.MerchantId,
                                MerchantKey = _DataItem.MerchantKey,
                                MerchantDisplayName = _DataItem.MerchantDisplayName,
                                MerchantAddress = _DataItem.MerchantAddress,

                                Stores = _DataItem.Stores,

                                RewardTransactions = _DataItem.RewardTransactions,
                                RewardTransactionsAmount = _DataItem.RewardTransactionsAmount,
                                CustomerRewardAmount = _DataItem.CustomerRewardAmount,
                                TUCCommission = _DataItem.TUCCommission
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("Reward Report", _DownloadReports, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DownloadRewardReport", _Exception);
            }
        }

        internal void DownloadRedeemReport(OList.Request _Request)
        {
            try
            {
                _Request.Limit = 800;
                _DownloadReports = new List<OReports.DownloadReports>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "MerchantId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _Request.TotalRecords = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                && (x.HCUAccountTransactionAccount.Any() || x.HCUAccountTransactionParent.Any()))
                                                .Select(x => new OReports.RedeemReport
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    MerchantId = x.Id,
                                                    MerchantKey = x.Guid,
                                                    MerchantDisplayName = x.DisplayName,
                                                    MerchantAddress = x.Address,

                                                    Stores = _HCoreContext.HCUAccount.Where(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore)
                                                             .Select(m => new Stores
                                                             {
                                                                 ReferenceId = m.Id,
                                                                 ReferenceKey = m.Guid,
                                                                 DisplayName = m.DisplayName,
                                                                 Address = m.Address,

                                                                 RedeemTransactions = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.SubParentId == m.Id
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Debit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                                      && z.Type.SubParentId == TransactionTypeCategory.Redeem).Count(),

                                                                 RedeemTransactionsAmount = m.HCUAccountTransactionSubParent
                                                                                            .Where(z => z.SubParentId == m.Id
                                                                                            && z.ParentId == x.Id
                                                                                            && z.ModeId == TransactionMode.Debit
                                                                                            && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                                            && z.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.TotalAmount),

                                                                 CustomerRedeemAmount = m.HCUAccountTransactionSubParent
                                                                                       .Where(z => z.SubParentId == m.Id
                                                                                       && z.ParentId == x.Id
                                                                                       && z.StatusId == HelperStatus.Transaction.Success
                                                                                       && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                       && z.ModeId == TransactionMode.Debit
                                                                                       && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                                       && z.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.TotalAmount),
                                                             }).ToList(),

                                                    TotalStores = _HCoreContext.HCUAccount.Count(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore),

                                                    RedeemTransactions = x.HCUAccountTransactionParent
                                                                         .Where(z => z.ParentId == x.Id
                                                                         && z.ModeId == TransactionMode.Debit
                                                                         && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                         && z.Type.SubParentId == TransactionTypeCategory.Redeem).Count(),

                                                    RedeemTransactionsAmount = x.HCUAccountTransactionParent
                                                                               .Where(z => z.ParentId == x.Id
                                                                               && z.ModeId == TransactionMode.Debit
                                                                               && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                               && z.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.TotalAmount),

                                                    CustomerRedeemAmount = x.HCUAccountTransactionParent
                                                                           .Where(z => z.ParentId == x.Id
                                                                           && z.StatusId == HelperStatus.Transaction.Success
                                                                           && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                           && z.ModeId == TransactionMode.Debit
                                                                           && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                           && z.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.TotalAmount),
                                                }).Where(_Request.SearchCondition)
                                                .Count();
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                           && (x.HCUAccountTransactionAccount.Any() || x.HCUAccountTransactionParent.Any()))
                                                           .Select(x => new OReports.RedeemReport
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,

                                                               MerchantId = x.Id,
                                                               MerchantKey = x.Guid,
                                                               MerchantDisplayName = x.DisplayName,
                                                               MerchantAddress = x.Address,

                                                               Stores = _HCoreContext.HCUAccount.Where(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore)
                                                             .Select(m => new Stores
                                                             {
                                                                 ReferenceId = m.Id,
                                                                 ReferenceKey = m.Guid,
                                                                 DisplayName = m.DisplayName,
                                                                 Address = m.Address,

                                                                 RedeemTransactions = m.HCUAccountTransactionSubParent
                                                                                      .Where(z => z.SubParentId == m.Id
                                                                                      && z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Debit
                                                                                      && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                                      && z.Type.SubParentId == TransactionTypeCategory.Redeem).Count(),

                                                                 RedeemTransactionsAmount = m.HCUAccountTransactionSubParent
                                                                                            .Where(z => z.SubParentId == m.Id
                                                                                            && z.ParentId == x.Id
                                                                                            && z.ModeId == TransactionMode.Debit
                                                                                            && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                                            && z.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.TotalAmount),

                                                                 CustomerRedeemAmount = m.HCUAccountTransactionSubParent
                                                                                       .Where(z => z.SubParentId == m.Id
                                                                                       && z.ParentId == x.Id
                                                                                       && z.StatusId == HelperStatus.Transaction.Success
                                                                                       && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                                       && z.ModeId == TransactionMode.Debit
                                                                                       && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                                       && z.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.TotalAmount),
                                                             }).ToList(),

                                                               TotalStores = _HCoreContext.HCUAccount.Count(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore),

                                                               RedeemTransactions = x.HCUAccountTransactionParent
                                                                         .Where(z => z.ParentId == x.Id
                                                                         && z.ModeId == TransactionMode.Debit
                                                                         && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                         && z.Type.SubParentId == TransactionTypeCategory.Redeem).Count(),

                                                               RedeemTransactionsAmount = x.HCUAccountTransactionParent
                                                                               .Where(z => z.ParentId == x.Id
                                                                               && z.ModeId == TransactionMode.Debit
                                                                               && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                               && z.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.TotalAmount),

                                                               CustomerRedeemAmount = x.HCUAccountTransactionParent
                                                                           .Where(z => z.ParentId == x.Id
                                                                           && z.StatusId == HelperStatus.Transaction.Success
                                                                           && z.Account.AccountTypeId == UserAccountType.Appuser
                                                                           && z.ModeId == TransactionMode.Debit
                                                                           && (z.SourceId == TransactionSource.TUC || z.SourceId == TransactionSource.GiftPoints || z.SourceId == TransactionSource.TUCBlack)
                                                                           && z.Type.SubParentId == TransactionTypeCategory.Redeem).Sum(a => a.TotalAmount),
                                                           }).Where(_Request.SearchCondition)
                                   .OrderBy(_Request.SortExpression)
                                   .Skip(_Request.Offset)
                                   .Take(_Request.Limit)
                                   .ToList();
                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            _DownloadReports.Add(new OReports.DownloadReports
                            {
                                MerchantId = _DataItem.MerchantId,
                                MerchantKey = _DataItem.MerchantKey,
                                MerchantDisplayName = _DataItem.MerchantDisplayName,
                                MerchantAddress = _DataItem.MerchantAddress,

                                Stores = _DataItem.Stores,

                                RedeemTransactions = _DataItem.RedeemTransactions,
                                RedeemTransactionsAmount = _DataItem.RedeemTransactionsAmount,
                                CustomerRedeemAmount = _DataItem.CustomerRedeemAmount
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("Redeem Report", _DownloadReports, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DownloadRedeemReport", _Exception);
            }
        }

        internal void DownloadRewardClaimReport(OList.Request _Request)
        {
            try
            {
                _Request.Limit = 800;
                _DownloadReports = new List<OReports.DownloadReports>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "MerchantId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _Request.TotalRecords = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                && (x.HCUAccountTransactionAccount.Any() || x.HCUAccountTransactionParent.Any()))
                                                .Select(x => new OReports.RewardClaimReport
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    MerchantId = x.Id,
                                                    MerchantKey = x.Guid,
                                                    MerchantDisplayName = x.DisplayName,
                                                    MerchantAddress = x.Address,

                                                    Stores = _HCoreContext.HCUAccount.Where(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore)
                                                             .Select(m => new Stores
                                                             {
                                                                 ReferenceId = m.Id,
                                                                 ReferenceKey = m.Guid,
                                                                 DisplayName = m.DisplayName,
                                                                 Address = m.Address,

                                                                 TucplusRewardTransactions = m.HCUAccountTransactionSubParent
                                                                                            .Where(z => z.SubParentId == m.Id
                                                                                            && z.ParentId == x.Id
                                                                                            && z.ModeId == TransactionMode.Debit
                                                                                            && z.SourceId == TransactionSource.ThankUCashPlus).Count(),

                                                                 TucplusRewardTransactionsAmount = m.HCUAccountTransactionSubParent
                                                                                                   .Where(z => z.SubParentId == m.Id
                                                                                                   && z.ParentId == x.Id
                                                                                                   && z.ModeId == TransactionMode.Debit
                                                                                                   && z.SourceId == TransactionSource.ThankUCashPlus).Sum(a => a.TotalAmount),

                                                                 RewardClaimAmount = m.HCUAccountTransactionSubParent
                                                                                     .Where(z => z.SubParentId == m.Id
                                                                                     && z.ParentId == x.Id
                                                                                     && z.ModeId == TransactionMode.Debit
                                                                                     && z.SourceId == TransactionSource.ThankUCashPlus).Count(),

                                                             }).ToList(),

                                                    TotalStores = _HCoreContext.HCUAccount.Count(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore),

                                                    TucplusRewardTransactions = x.HCUAccountTransactionParent
                                                                                .Where(z => z.ParentId == x.Id
                                                                                && z.ModeId == TransactionMode.Debit
                                                                                && z.SourceId == TransactionSource.ThankUCashPlus).Count(),

                                                    TucplusRewardTransactionsAmount = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Debit
                                                                                      && z.SourceId == TransactionSource.ThankUCashPlus).Sum(a => a.TotalAmount),

                                                    RewardClaimAmount = x.HCUAccountTransactionParent
                                                                        .Where(z => z.ParentId == x.Id
                                                                        && z.ModeId == TransactionMode.Debit
                                                                        && z.SourceId == TransactionSource.ThankUCashPlus).Count(),
                                                }).Where(_Request.SearchCondition)
                                                .Count();
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                           && (x.HCUAccountTransactionAccount.Any() || x.HCUAccountTransactionParent.Any()))
                                                           .Select(x => new OReports.RewardClaimReport
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,

                                                               MerchantId = x.Id,
                                                               MerchantKey = x.Guid,
                                                               MerchantDisplayName = x.DisplayName,
                                                               MerchantAddress = x.Address,

                                                               Stores = _HCoreContext.HCUAccount.Where(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore)
                                                             .Select(m => new Stores
                                                             {
                                                                 ReferenceId = m.Id,
                                                                 ReferenceKey = m.Guid,
                                                                 DisplayName = m.DisplayName,
                                                                 Address = m.Address,

                                                                 TucplusRewardTransactions = m.HCUAccountTransactionSubParent
                                                                                            .Where(z => z.SubParentId == m.Id
                                                                                            && z.ParentId == x.Id
                                                                                            && z.ModeId == TransactionMode.Debit
                                                                                            && z.SourceId == TransactionSource.ThankUCashPlus).Count(),

                                                                 TucplusRewardTransactionsAmount = m.HCUAccountTransactionSubParent
                                                                                                   .Where(z => z.SubParentId == m.Id
                                                                                                   && z.ParentId == x.Id
                                                                                                   && z.ModeId == TransactionMode.Debit
                                                                                                   && z.SourceId == TransactionSource.ThankUCashPlus).Sum(a => a.TotalAmount),

                                                                 RewardClaimAmount = m.HCUAccountTransactionSubParent
                                                                                     .Where(z => z.SubParentId == m.Id
                                                                                     && z.ParentId == x.Id
                                                                                     && z.ModeId == TransactionMode.Debit
                                                                                     && z.SourceId == TransactionSource.ThankUCashPlus).Count(),

                                                             }).ToList(),

                                                               TotalStores = _HCoreContext.HCUAccount.Count(a => a.OwnerId == x.Id && a.AccountTypeId == UserAccountType.MerchantStore),

                                                               TucplusRewardTransactions = x.HCUAccountTransactionParent
                                                                                .Where(z => z.ParentId == x.Id
                                                                                && z.ModeId == TransactionMode.Debit
                                                                                && z.SourceId == TransactionSource.ThankUCashPlus).Count(),

                                                               TucplusRewardTransactionsAmount = x.HCUAccountTransactionParent
                                                                                      .Where(z => z.ParentId == x.Id
                                                                                      && z.ModeId == TransactionMode.Debit
                                                                                      && z.SourceId == TransactionSource.ThankUCashPlus).Sum(a => a.TotalAmount),

                                                               RewardClaimAmount = x.HCUAccountTransactionParent
                                                                        .Where(z => z.ParentId == x.Id
                                                                        && z.ModeId == TransactionMode.Debit
                                                                        && z.SourceId == TransactionSource.ThankUCashPlus).Count(),
                                                           }).Where(_Request.SearchCondition)
                                   .OrderBy(_Request.SortExpression)
                                   .Skip(_Request.Offset)
                                   .Take(_Request.Limit)
                                   .ToList();
                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            _DownloadReports.Add(new OReports.DownloadReports
                            {
                                MerchantId = _DataItem.MerchantId,
                                MerchantKey = _DataItem.MerchantKey,
                                MerchantDisplayName = _DataItem.MerchantDisplayName,
                                MerchantAddress = _DataItem.MerchantAddress,

                                Stores = _DataItem.Stores,

                                TucplusRewardTransactions = _DataItem.TucplusRewardTransactions,
                                TucplusRewardTransactionsAmount = _DataItem.TucplusRewardTransactionsAmount,
                                RewardClaimAmount = _DataItem.RewardClaimAmount
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("Sales Report", _DownloadReports, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DownloadSalesReport", _Exception);
            }
        }
    }

    internal class ActorDownloadSalesReport : ReceiveActor
    {
        public ActorDownloadSalesReport()
        {
            FrameworkReports _FrameworkReports;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkReports = new FrameworkReports();
                _FrameworkReports.DownloadSalesReport(_Request);
            });
        }
    }

    internal class ActorDownloadRewardReport : ReceiveActor
    {
        public ActorDownloadRewardReport()
        {
            FrameworkReports _FrameworkReports;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkReports = new FrameworkReports();
                _FrameworkReports.DownloadRewardReport(_Request);
            });
        }
    }

    internal class ActorDownloadRedeemReport : ReceiveActor
    {
        public ActorDownloadRedeemReport()
        {
            FrameworkReports _FrameworkReports;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkReports = new FrameworkReports();
                _FrameworkReports.DownloadRedeemReport(_Request);
            });
        }
    }

    internal class ActorDownloadRewardClaimReport : ReceiveActor
    {
        public ActorDownloadRewardClaimReport()
        {
            FrameworkReports _FrameworkReports;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkReports = new FrameworkReports();
                _FrameworkReports.DownloadRewardClaimReport(_Request);
            });
        }
    }
}

