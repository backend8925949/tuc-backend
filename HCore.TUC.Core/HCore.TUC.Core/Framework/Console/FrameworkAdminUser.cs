//==================================================================================
// FileName: FrameworkAdminUser.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to admin user
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Logging;
using HCore.Helper;
using HCore.TUC.Core.CoreAccount;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
namespace HCore.TUC.Core.Framework.Console
{
    internal class FrameworkAdminUser
    {
        HCoreContext _HCoreContext;
        CoreAdmin _CoreAdmin;
        OCoreAdmin.Request _OCoreAdminRequest;
        OCoreAdmin.AuthUpdate.Request _OCoreAdminAuthUpdateRequest;
        /// <summary>
        /// Description: Gets the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccount(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Admin && x.CountryId == _Request.UserReference.CountryId)
                                                .Select(x => new OAdminUser.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Name = x.Name,

                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    GenderName = x.Gender.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    LastActivityDate = x.LastActivityDate,

                                                    CountryId = x.CountryId,
                                                    CountryKey = x.Country.Guid,
                                                    CountryName = x.Country.Name,

                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    RoleId = x.RoleId,
                                                    RoleKey = x.Role.Guid,
                                                    RoleName = x.Role.Name,
                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.Name
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OAdminUser.List> _List = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Admin && x.CountryId == _Request.UserReference.CountryId)
                                                .Select(x => new OAdminUser.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    GenderName = x.Gender.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    LastActivityDate = x.LastActivityDate,

                                                    CountryId = x.CountryId,
                                                    CountryKey = x.Country.Guid,
                                                    CountryName = x.Country.Name,

                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    RoleId = x.RoleId,
                                                    RoleKey = x.Role.Guid,
                                                    RoleName = x.Role.Name,
                                                    OwnerId = x.OwnerId,
                                                    OwnerKey = x.Owner.Guid,
                                                    OwnerDisplayName = x.Owner.Name
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _List)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetAdminUser", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccount(OAdminUser.Details.Request _Request)
        {
            if (_Request.ReferenceId == 0)
            {
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
            }
            else if (string.IsNullOrEmpty(_Request.ReferenceKey))
            {
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
            }
            else
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCUAccount
                        .Where(x => x.AccountTypeId == UserAccountType.Admin && x.CountryId == _Request.UserReference.SystemCountry
                        && x.Id == _Request.ReferenceId
                        && x.Guid == _Request.ReferenceKey)
                        .Select(x => new OAdminUser.Details.Response
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            DisplayName = x.DisplayName,
                            Name = x.Name,
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            EmailAddress = x.EmailAddress,
                            MobileNumber = x.MobileNumber,
                            GenderCode = x.Gender.SystemName,
                            GenderName = x.Gender.Name,
                            DateOfBirth = x.DateOfBirth,
                            RoleKey = x.Role.SystemName,
                            RoleName = x.Role.Name,
                            IconUrl = x.IconStorage.Path,
                            OwnerId = x.OwnerId,
                            OwnerKey = x.Owner.Guid,
                            OwnerDisplayName = x.Owner.Name,
                            Address = x.Address,
                            LastActivityDate = x.LastActivityDate,

                            CountryId = x.CountryId,
                            CountryKey = x.Country.Guid,
                            CountryName = x.Country.Name,

                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                            ModifyById = x.ModifyById,
                            ModifyByKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,
                            ModifyDate = x.ModifyDate,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                            UserName = x.User.Username,
                        }).FirstOrDefault();
                    if (_Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Details.IconUrl))
                        {
                            _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                        }
                        else
                        {
                            _Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
        }
        /// <summary>
        /// Description: Saves the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveAccount(OAdminUser.Request _Request)
        {
            _OCoreAdminRequest = new OCoreAdmin.Request();
            _OCoreAdminRequest.FirstName = _Request.FirstName;
            _OCoreAdminRequest.LastName = _Request.LastName;
            _OCoreAdminRequest.MobileNumber = _Request.MobileNumber;
            _OCoreAdminRequest.EmailAddress = _Request.EmailAddress;
            _OCoreAdminRequest.StatusCode = _Request.StatusCode;
            _OCoreAdminRequest.GenderCode = _Request.GenderCode;
            _OCoreAdminRequest.RoleKey = _Request.RoleKey;
            _OCoreAdminRequest.OwnerKey = _Request.OwnerKey;
            _OCoreAdminRequest.DateOfBirth = _Request.DateOfBirth;
            _OCoreAdminRequest.IconContent = _Request.IconContent;
            _OCoreAdminRequest.CountryKey = _Request.CountryKey;
            _OCoreAdminRequest.TimeZoneKey = _Request.TimeZoneKey;
            _OCoreAdminRequest.Address = _Request.Address;
            _OCoreAdminRequest.UserReference = _Request.UserReference;
            _CoreAdmin = new CoreAdmin();
            OCoreAdmin.Response _Response = _CoreAdmin.SaveAdmin(_OCoreAdminRequest);
            if (_Response.Status == HCoreConstant.ResponseStatus.Success)
            {
                var Details = new
                {
                    ReferenceId = _Response.ReferenceId,
                    ReferenceKey = _Response.ReferenceKey
                };
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, Details, _Response.StatusCode, _Response.Message);
            }
            else
            {
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, null, _Response.StatusCode, _Response.Message);
            }
        }
        /// <summary>
        /// Description: Updates the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateAccount(OAdminUser.Request _Request)
        {
            _OCoreAdminRequest = new OCoreAdmin.Request();
            _OCoreAdminRequest.ReferenceId = _Request.ReferenceId;
            _OCoreAdminRequest.ReferenceKey = _Request.ReferenceKey;
            _OCoreAdminRequest.OwnerKey = _Request.OwnerKey;
            _OCoreAdminRequest.DisplayName = _Request.DisplayName;
            _OCoreAdminRequest.FirstName = _Request.FirstName;
            _OCoreAdminRequest.LastName = _Request.LastName;
            _OCoreAdminRequest.MobileNumber = _Request.MobileNumber;
            _OCoreAdminRequest.EmailAddress = _Request.EmailAddress;
            _OCoreAdminRequest.GenderCode = _Request.GenderCode;
            _OCoreAdminRequest.DateOfBirth = _Request.DateOfBirth;
            _OCoreAdminRequest.Address = _Request.Address;
            _OCoreAdminRequest.RoleKey = _Request.RoleKey;
            _OCoreAdminRequest.IconContent = _Request.IconContent;
            _OCoreAdminRequest.StatusCode = _Request.StatusCode;
            _OCoreAdminRequest.CountryKey = _Request.CountryKey;
            _OCoreAdminRequest.TimeZoneKey = _Request.TimeZoneKey;
            _OCoreAdminRequest.UserReference = _Request.UserReference;
            _CoreAdmin = new CoreAdmin();
            OCoreAdmin.Response _Response = _CoreAdmin.UpdateAdmin(_OCoreAdminRequest);
            if (_Response.Status == HCoreConstant.ResponseStatus.Success)
            {
                var Details = new
                {
                    ReferenceId = _Response.ReferenceId,
                    ReferenceKey = _Response.ReferenceKey
                };
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, Details, _Response.StatusCode);
            }
            else
            {
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, null, _Response.StatusCode);
            }

        }
        #region Expire Session
        /// <summary>
        /// Description: Expires the account session.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ExpireAccountSession(OAdminUser.Session.Request _Request)
        {

            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCAM052");
                    #endregion
                }
                else
                {
                    #region Perform Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        var _ValidateDetails = (from n in _HCoreContext.HCUAccountSession
                                                where n.Guid == _Request.ReferenceKey
                                                && n.Id == _Request.ReferenceId
                                                select n).FirstOrDefault();
                        if (_ValidateDetails != null)
                        {
                            if (_ValidateDetails.StatusId == HelperStatus.Default.Active)
                            {
                                _ValidateDetails.LogoutDate = HCoreHelper.GetGMTDateTime();
                                _ValidateDetails.StatusId = HelperStatus.DealCodes.Blocked;
                                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                _HCoreContext.SaveChanges();
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCAM049");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCAM051");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCAM050");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("ExpireSystemUserSession", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        #endregion
        #region Get
        /// <summary>
        /// Description: Gets the account session.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountSession(OList.Request _Request)
        {

            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "LoginDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {

                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = (from n in _HCoreContext.HCUAccountSession
                                                 where n.AccountId == _Request.ReferenceId
                                                 && n.Account.Guid == _Request.ReferenceKey
                                                 select new OAdminUser.Session.Response
                                                 {
                                                     ReferenceId = n.Id,
                                                     ReferenceKey = n.Guid,
                                                     //AppName = n.AppVersion.Parent.Name,
                                                     //AppVersionName = n.AppVersion.Name,
                                                     LastActivityDate = n.LastActivityDate,
                                                     LoginDate = n.LoginDate,
                                                     LogoutDate = n.LogoutDate,
                                                     IpAddress = n.IPAddress,
                                                     Latitude = n.Latitude,
                                                     Longitude = n.Longitude,
                                                     StatusCode = n.Status.SystemName,
                                                     StatusName = n.Status.Name
                                                 })
                                            .Where(_Request.SearchCondition)
                                    .Count();
                        #endregion
                    }

                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = _Request.TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = 10;
                    }
                    #endregion
                    #region Get Data
                    List<OAdminUser.Session.Response> Data = (from n in _HCoreContext.HCUAccountSession
                                                              where n.AccountId == _Request.ReferenceId
                                                            && n.Account.Guid == _Request.ReferenceKey
                                                              select new OAdminUser.Session.Response
                                                              {
                                                                  ReferenceId = n.Id,
                                                                  ReferenceKey = n.Guid,
                                                                  //AppName = n.AppVersion.Parent.Name,
                                                                  //AppVersionName = n.AppVersion.Name,
                                                                  LastActivityDate = n.LastActivityDate,
                                                                  LoginDate = n.LoginDate,
                                                                  LogoutDate = n.LogoutDate,
                                                                  IpAddress = n.IPAddress,
                                                                  Latitude = n.Latitude,
                                                                  Longitude = n.Longitude,
                                                                  StatusCode = n.Status.SystemName,
                                                                  StatusName = n.Status.Name,
                                                                  IconUrl = n.Account.IconStorage.Path,
                                                              })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var _DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(_DataItem.IconUrl))
                        {
                            _DataItem.IconUrl = _AppConfig.StorageUrl + _DataItem.IconUrl;
                        }
                        else
                        {
                            _DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAccountSession", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        #endregion

        /// <summary>
        /// Description: Updates the admin username.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateAdminUsername(OAdminUser.AuthUpdate.Request _Request)
        {
            _OCoreAdminAuthUpdateRequest = new OCoreAdmin.AuthUpdate.Request();
            _OCoreAdminAuthUpdateRequest.AccountId = _Request.AccountId;
            _OCoreAdminAuthUpdateRequest.AccountKey = _Request.AccountKey;
            _OCoreAdminAuthUpdateRequest.UserName = _Request.UserName;
            _OCoreAdminAuthUpdateRequest.OldPassword = _Request.OldPassword;
            _OCoreAdminAuthUpdateRequest.NewPassword = _Request.NewPassword;
            _OCoreAdminAuthUpdateRequest.OldPin = _Request.OldPin;
            _OCoreAdminAuthUpdateRequest.NewPin = _Request.NewPin;
            _OCoreAdminAuthUpdateRequest.UserReference = _Request.UserReference;
            _CoreAdmin = new CoreAdmin();
            OCoreAdmin.AuthUpdate.Response _Response = _CoreAdmin.UpdateAdminUsername(_OCoreAdminAuthUpdateRequest);
            if (_Response.Status == HCoreConstant.ResponseStatus.Success)
            {
                var Details = new
                {
                    NewUsername = _Response.UserName,
                };
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, Details, _Response.StatusCode, _Response.Message);
            }
            else
            {
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, null, _Response.StatusCode, _Response.Message);
            }
        }
        /// <summary>
        /// Description: Updates the admin password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateAdminPassword(OAdminUser.AuthUpdate.Request _Request)
        {
            _OCoreAdminAuthUpdateRequest = new OCoreAdmin.AuthUpdate.Request();
            _OCoreAdminAuthUpdateRequest.AccountId = _Request.AccountId;
            _OCoreAdminAuthUpdateRequest.AccountKey = _Request.AccountKey;
            _OCoreAdminAuthUpdateRequest.OldPassword = _Request.OldPassword;
            _OCoreAdminAuthUpdateRequest.NewPassword = _Request.NewPassword;
            _OCoreAdminAuthUpdateRequest.UserReference = _Request.UserReference;
            _CoreAdmin = new CoreAdmin();
            OCoreAdmin.AuthUpdate.Response _Response = _CoreAdmin.UpdateAdminPassword(_OCoreAdminAuthUpdateRequest);
            if (_Response.Status == HCoreConstant.ResponseStatus.Success)
            {
                var Details = new
                {
                    NewPassword = _Response.NewPassword,
                };
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, Details, _Response.StatusCode);
            }
            else
            {
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, null, _Response.StatusCode);
            }
        }
        /// <summary>
        /// Description: Updates the admin reset password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateAdminResetPassword(OAdminUser.AuthUpdate.Request _Request)
        {
            _OCoreAdminAuthUpdateRequest = new OCoreAdmin.AuthUpdate.Request();
            _OCoreAdminAuthUpdateRequest.AccountId = _Request.AccountId;
            _OCoreAdminAuthUpdateRequest.AccountKey = _Request.AccountKey;
            _OCoreAdminAuthUpdateRequest.UserReference = _Request.UserReference;
            _CoreAdmin = new CoreAdmin();
            OCoreAdmin.AuthUpdate.Response _Response = _CoreAdmin.UpdateAdminResetPassword(_OCoreAdminAuthUpdateRequest);
            if (_Response.Status == HCoreConstant.ResponseStatus.Success)
            {
                var Details = new
                {
                    NewPassword = _Response.NewPassword,
                };
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, Details, _Response.StatusCode);
            }
            else
            {
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, null, _Response.StatusCode);
            }
        }
        /// <summary>
        /// Description: Updates the admin pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateAdminPin(OAdminUser.AuthUpdate.Request _Request)
        {
            _OCoreAdminAuthUpdateRequest = new OCoreAdmin.AuthUpdate.Request();
            _OCoreAdminAuthUpdateRequest.AccountId = _Request.AccountId;
            _OCoreAdminAuthUpdateRequest.AccountKey = _Request.AccountKey;
            _OCoreAdminAuthUpdateRequest.OldPin = _Request.OldPin;
            _OCoreAdminAuthUpdateRequest.NewPin = _Request.NewPin;
            _OCoreAdminAuthUpdateRequest.UserReference = _Request.UserReference;
            _CoreAdmin = new CoreAdmin();
            OCoreAdmin.AuthUpdate.Response _Response = _CoreAdmin.UpdateAdminPin(_OCoreAdminAuthUpdateRequest);
            if (_Response.Status == HCoreConstant.ResponseStatus.Success)
            {
                var Details = new
                {
                    NewPin = _Response.NewPin,
                };
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, Details, _Response.StatusCode);
            }
            else
            {
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, null, _Response.StatusCode);
            }
        }
        /// <summary>
        /// Description: Updates the admin reset pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateAdminResetPin(OAdminUser.AuthUpdate.Request _Request)
        {
            _OCoreAdminAuthUpdateRequest = new OCoreAdmin.AuthUpdate.Request();
            _OCoreAdminAuthUpdateRequest.AccountId = _Request.AccountId;
            _OCoreAdminAuthUpdateRequest.AccountKey = _Request.AccountKey;
            _OCoreAdminAuthUpdateRequest.UserReference = _Request.UserReference;
            _CoreAdmin = new CoreAdmin();
            OCoreAdmin.AuthUpdate.Response _Response = _CoreAdmin.UpdateAdminResetPin(_OCoreAdminAuthUpdateRequest);
            if (_Response.Status == HCoreConstant.ResponseStatus.Success)
            {
                var Details = new
                {
                    NewPin = _Response.NewPin,
                };
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, Details, _Response.StatusCode);
            }
            else
            {
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, null, _Response.StatusCode);
            }
        }


        /// <summary>
        /// Description: Set App Logo.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SetAppLogo(OAdminUser.AuthUpdate.LogoRequest _Request)
        {
            try
            {
                return null;
                //long? ImageStorageId = null;
                //#region Manage Operations

                //using (_HCoreContext = new HCoreContext())
                //{

                //    if (_Request.ImageContent != null)
                //    {

                //        if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                //        {
                //            ImageStorageId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, null, _Request.UserReference);
                //        }
                //        if (ImageStorageId != null)
                //        {
                //            using (_HCoreContext = new HCoreContext())
                //            {
                //                var SaveDetails = _HCoreContext.LogoSettings.Where(x => x.Status == 2 && x.Panel == _Request.Panel).FirstOrDefault();
                //                if (SaveDetails != null)
                //                {
                //                    if (ImageStorageId != null)
                //                    {
                //                        SaveDetails.ImageStorageId = (long)ImageStorageId;
                //                        SaveDetails.Uploader = _Request.UserReference.EmailAddress;
                //                        SaveDetails.UploadDate = DateTime.Now;
                //                    }
                //                    _HCoreContext.SaveChanges();
                //                    _HCoreContext.Dispose();
                //                }
                //                else
                //                {
                //                    var newlogo = new LogoSettings
                //                    {
                //                        ImageStorageId = ImageStorageId,
                //                        UploadDate = DateTime.Now,
                //                        Uploader = _Request.UserReference.EmailAddress,
                //                        Status = 2,
                //                        Panel = _Request.Panel
                //                    };

                //                    _HCoreContext.LogoSettings.Add(newlogo);
                //                    _HCoreContext.SaveChanges();
                //                }
                //            }
                //        }
                //        else
                //        {
                //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1548, TUCCoreResource.CA1548M);
                //        }
                //    }



                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1545, TUCCoreResource.CA1545M);

                //}
                //#endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SetAppLogo", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }


        /// <summary>
        /// Description: Gets app Logo.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAppLogo(OAdminUser.AuthUpdate.LogoRequest _Request)
        {
            try
            {
                #region Manage Operations
                return null;
                //using (_HCoreContext = new HCoreContext())
                //{
                //    var _Details = _HCoreContext.LogoSettings.Where(x => x.Panel == _Request.Panel && x.Status == 2)
                //        .Select(x => new OAdminUser.AuthUpdate.LogoResponse
                //        {

                //            ImageUrl = x.ImageStorage.Path,
                //            UploadedBy = x.Uploader,
                //            UploadDate = x.UploadDate

                //        }).FirstOrDefault();

                //    if (_Details != null)
                //    {
                //        if (!string.IsNullOrEmpty(_Details.ImageUrl))
                //        {
                //            _Details.ImageUrl = _AppConfig.StorageUrl + _Details.ImageUrl;
                //        }
                //        else
                //        {
                //            _Details.ImageUrl = _AppConfig.Default_Icon;
                //        }
                //        _HCoreContext.Dispose();
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA1547, TUCCoreResource.CA1547M);
                //    }
                //    else
                //    {
                //        _HCoreContext.Dispose();
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                //    }
                //}
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GetAdminLogo", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }


    }
}
