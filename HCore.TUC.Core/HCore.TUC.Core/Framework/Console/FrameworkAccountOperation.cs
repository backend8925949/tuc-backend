//==================================================================================
// FileName: FrameworkAccountOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to to save and get customer,acquirer,pssp,merchant functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Resource;
using HCore.TUC.Core.CoreAccount;
using HCore.TUC.Core.Object.Console;

namespace HCore.TUC.Core.Framework.Console
{   
    internal class FrameworkAccountOperation
    {
        #region Declare
        //Random _Random;
        //HCUser _HCUser;
        //HCUAccount _HCUAccount;
        ////HCUAccountOwner _HCUAccountOwner;
        //HCoreContext _HCoreContext;
        ////HCUAccountParameter _HCUAccountParameter;
        //List<HCUAccount> _HCUAccounts;
        ////List<HCUAccountOwner> _HCUAccountOwners;
        //List<HCUAccountParameter> _HCUAccountParameters;
        //TUCTerminal _TUCTerminal;
        CorePartner _CorePartner;       
        CoreAcquirer _CoreAcquirer;       
        CoreMerchant _CoreMerchant;
        CorePssp _CorePssp;        
        CorePtsp _CorePtsp;        
        OCorePartner.Request _CorePartnerRequest;        
        OCoreAcquirer.Request _CoreAcquirerRequest;        
        OCoreMerchant.Request _CoreMerchantRequest;       
        OCorePssp.Request _CorePsspRequest;        
        OCorePtsp.Request _CorePtspRequest;
        #endregion        
        OCoreCustomer.Request _OCoreCustomerRequest;        
        HCoreContext _HCoreContext;        
        CoreCustomer _CoreCustomer;

        /// <summary>
        /// Description: Updates the customer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateCustomer(OCustomer.Request _Request)
        {
            _OCoreCustomerRequest = new OCoreCustomer.Request();
            _OCoreCustomerRequest.ReferenceId = _Request.ReferenceId;
            _OCoreCustomerRequest.ReferenceKey = _Request.ReferenceKey;
            _OCoreCustomerRequest.FirstName = _Request.FirstName;
            _OCoreCustomerRequest.LastName = _Request.LastName;
            _OCoreCustomerRequest.ReferralCode = _Request.ReferralCode;
            if (!string.IsNullOrEmpty(_Request.DisplayName))
            {
                _OCoreCustomerRequest.DisplayName = _Request.DisplayName;
            }
            else
            {
                _OCoreCustomerRequest.DisplayName = _Request.FirstName;
            }
            _OCoreCustomerRequest.EmailAddress = _Request.EmailAddress;
            _OCoreCustomerRequest.GenderCode = _Request.GenderCode;
            _OCoreCustomerRequest.DateOfBirth = _Request.DateOfBirth;
            _OCoreCustomerRequest.Address = _Request.Address;
            _OCoreCustomerRequest.IconContent = _Request.IconContent;
            _OCoreCustomerRequest.StatusCode = _Request.StatusCode;
            _OCoreCustomerRequest.UserReference = _Request.UserReference;
            _CoreCustomer = new CoreCustomer();
            OCoreCustomer.Response _Response = _CoreCustomer.UpdateCustomer(_OCoreCustomerRequest);
            if (_Response.Status == HCoreConstant.ResponseStatus.Success)
            {
                var Details = new
                {
                    ReferenceId = _Response.ReferenceId,
                    ReferenceKey = _Response.ReferenceKey
                };
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, Details, _Response.StatusCode);
            }
            else
            {
                return HCoreHelper.SendResponse(_Request.UserReference, _Response.Status, null, _Response.StatusCode);
            }

        }

        /// <summary>
        /// Description: Updates the account status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateAccountStatus(OAccount.ManageStatus.Request _Request)
        {
            if (_Request.AccountId == 0)
            {
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
            }
            else if (string.IsNullOrEmpty(_Request.AccountKey))
            {
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
            }
            else if (string.IsNullOrEmpty(_Request.StatusCode))
            {
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1266, TUCCoreResource.CA1266M);
            }
            else if (string.IsNullOrEmpty(_Request.Comment))
            {
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1267, TUCCoreResource.CA1267M);
            }
            else
            {
                if (_Request.AccountId == _Request.UserReference.AccountId)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1268, TUCCoreResource.CA1268M);
                }

                int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }
                if (StatusId != HelperStatus.Default.Active && StatusId != HelperStatus.Default.Blocked && StatusId != HelperStatus.Default.Suspended)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1269, TUCCoreResource.CA1269M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey)
                        .FirstOrDefault();
                    if (_Details != null)
                    {
                        if (_Details.StatusId == StatusId)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1270, TUCCoreResource.CA1270M);
                        }
                        if (_Details.AccountTypeId == UserAccountType.Merchant)
                        {
                            _Details.StatusId = StatusId;
                            _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            _Details.ModifyById = _Request.UserReference.AccountId;
                            _HCoreContext.SaveChanges();

                            bool Details = _HCoreContext.HCUAccount
                                        .Any(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey
                                               && (x.StatusId == HelperStatus.Default.Blocked || x.StatusId == HelperStatus.Default.Active));
                            if (Details == true)
                            {
                                var UpsateStatuss = _HCoreContext.HCUAccount.Where(x => (x.OwnerId == _Request.AccountId && x.Owner.Guid == _Request.AccountKey)
                                || (x.Owner.OwnerId == _Request.AccountId && x.Owner.Owner.Guid == _Request.AccountKey)).ToList();
                                foreach (var UpsateStatus in UpsateStatuss)
                                {
                                    if (UpsateStatus.AccountTypeId == UserAccountType.MerchantStore || UpsateStatus.AccountTypeId == UserAccountType.MerchantCashier || UpsateStatus.AccountTypeId == UserAccountType.MerchantSubAccount)
                                    {
                                        UpsateStatus.StatusId = StatusId;
                                        UpsateStatus.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        UpsateStatus.ModifyById = _Request.UserReference.AccountId;
                                    }
                                }
                                _HCoreContext.SaveChanges();
                            }
                        }
                        else if (_Details.AccountTypeId == UserAccountType.Manager)
                        {
                            _Details.StatusId = StatusId;
                            _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            _Details.ModifyById = _Request.UserReference.AccountId;
                            _HCoreContext.SaveChanges();

                            var TUCBranchAccountDetails = _HCoreContext.TUCBranchAccount
                                        .Where(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey)
                                        .FirstOrDefault();
                            if (TUCBranchAccountDetails != null)
                            {

                                TUCBranchAccountDetails.StatusId = StatusId;
                                TUCBranchAccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                TUCBranchAccountDetails.ModifyById = _Request.UserReference.AccountId;
                                _HCoreContext.SaveChanges();

                            }
                               
                            
                        }
                        else
                        {
                            _Details.StatusId = StatusId;
                            _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            _Details.ModifyById = _Request.UserReference.AccountId;
                            _HCoreContext.SaveChanges();
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1271, TUCCoreResource.CA1271M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
        }

        /// <summary>
        /// Description: Saves the partner.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SavePartner(OPartner.Request _Request)
        {
            #region Manage Exception
            try
            {
                _CorePartnerRequest = new OCorePartner.Request();
                _CorePartnerRequest.DisplayName = _Request.DisplayName;
                _CorePartnerRequest.Name = _Request.Name;
                _CorePartnerRequest.ContactNumber = _Request.ContactNumber;
                _CorePartnerRequest.EmailAddress = _Request.EmailAddress;
                _CorePartnerRequest.ReferralCode = _Request.ReferralCode;
                _CorePartnerRequest.WebsiteUrl = _Request.WebsiteUrl;
                _CorePartnerRequest.Description = _Request.Description;
                _CorePartnerRequest.UserName = _Request.UserName;
                _CorePartnerRequest.Password = _Request.Password;
                _CorePartnerRequest.StatusCode = _Request.StatusCode;
                _CorePartnerRequest.StartDate = _Request.StartDate;
                _CorePartnerRequest.IconContent = _Request.IconContent;
                _CorePartnerRequest.Address = _Request.AddressComponent;
                _CorePartnerRequest.ContactPerson = _Request.ContactPerson;
                _CorePartnerRequest.UserReference = _Request.UserReference;
                _CorePartner = new CorePartner();
                var _Response = _CorePartner.SavePartner(_CorePartnerRequest);
                if (_Response.Status == ResponseStatus.Success)
                {
                    var ResponseDetails = new
                    {
                        ReferenceId = _Response.ReferenceId,
                        ReferenceKey = _Response.ReferenceKey,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ResponseDetails, _Response.StatusCode, _Response.Message);
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _Response.StatusCode, _Response.Message);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SavePartner", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the acquirer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveAcquirer(OAcquirer.Request _Request)
        {
            #region Manage Exception
            try
            {
                _CoreAcquirerRequest = new OCoreAcquirer.Request();
                _CoreAcquirerRequest.DisplayName = _Request.DisplayName;
                _CoreAcquirerRequest.Name = _Request.Name;
                _CoreAcquirerRequest.ContactNumber = _Request.ContactNumber;
                _CoreAcquirerRequest.EmailAddress = _Request.EmailAddress;
                _CoreAcquirerRequest.ReferralCode = _Request.ReferralCode;
                _CoreAcquirerRequest.WebsiteUrl = _Request.WebsiteUrl;
                _CoreAcquirerRequest.Description = _Request.Description;
                _CoreAcquirerRequest.UserName = _Request.UserName;
                _CoreAcquirerRequest.Password = _Request.Password;
                _CoreAcquirerRequest.StatusCode = _Request.StatusCode;
                _CoreAcquirerRequest.StartDate = _Request.StartDate;
                _CoreAcquirerRequest.IconContent = _Request.IconContent;
                _CoreAcquirerRequest.Address = _Request.AddressComponent;
                _CoreAcquirerRequest.ContactPerson = _Request.ContactPerson;
                _CoreAcquirerRequest.UserReference = _Request.UserReference;
                _CoreAcquirer = new CoreAcquirer();
                var _Response = _CoreAcquirer.SaveAcquirer(_CoreAcquirerRequest);
                if (_Response.Status == ResponseStatus.Success)
                {
                    var ResponseDetails = new
                    {
                        ReferenceId = _Response.ReferenceId,
                        ReferenceKey = _Response.ReferenceKey,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ResponseDetails, _Response.StatusCode, _Response.Message);
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _Response.StatusCode, _Response.Message);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SaveAcquirer", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Savepssps the specified request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse Savepssp(OPssp.Request _Request)
        {
            #region Manage Exception
            try
            {
                _CorePsspRequest = new OCorePssp.Request();
                _CorePsspRequest.DisplayName = _Request.DisplayName;
                _CorePsspRequest.Name = _Request.Name;
                _CorePsspRequest.ContactNumber = _Request.ContactNumber;
                _CorePsspRequest.EmailAddress = _Request.EmailAddress;
                _CorePsspRequest.SecondaryEmailAddress = _Request.SecondaryEmailAddress;
                _CorePsspRequest.ReferralCode = _Request.ReferralCode;
                _CorePsspRequest.WebsiteUrl = _Request.WebsiteUrl;
                _CorePsspRequest.Description = _Request.Description;
                _CorePsspRequest.UserName = _Request.UserName;
                _CorePsspRequest.Password = _Request.Password;
                _CorePsspRequest.StatusCode = _Request.StatusCode;
                _CorePsspRequest.StartDate = _Request.StartDate;
                _CorePsspRequest.IconContent = _Request.IconContent;
                _CorePsspRequest.Address = _Request.AddressComponent;
                _CorePsspRequest.ContactPerson = _Request.ContactPerson;
                _CorePsspRequest.UserReference = _Request.UserReference;
                _CorePssp = new CorePssp();
                var _Response = _CorePssp.SavePssp(_CorePsspRequest);
                if (_Response.Status == ResponseStatus.Success)
                {
                    var ResponseDetails = new
                    {
                        ReferenceId = _Response.ReferenceId,
                        ReferenceKey = _Response.ReferenceKey,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ResponseDetails, _Response.StatusCode, _Response.Message);
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _Response.StatusCode, _Response.Message);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Savepssp", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the PTSP.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SavePtsp(OPtsp.Request _Request)
        {
            #region Manage Exception
            try
            {
                _CorePtspRequest = new OCorePtsp.Request();
                _CorePtspRequest.DisplayName = _Request.DisplayName;
                _CorePtspRequest.Name = _Request.Name;
                _CorePtspRequest.ContactNumber = _Request.ContactNumber;
                _CorePtspRequest.EmailAddress = _Request.EmailAddress;
                _CorePtspRequest.ReferralCode = _Request.ReferralCode;
                _CorePtspRequest.WebsiteUrl = _Request.WebsiteUrl;
                _CorePtspRequest.Description = _Request.Description;
                _CorePtspRequest.UserName = _Request.UserName;
                _CorePtspRequest.Password = _Request.Password;
                _CorePtspRequest.StatusCode = _Request.StatusCode;
                _CorePtspRequest.StartDate = _Request.StartDate;
                _CorePtspRequest.IconContent = _Request.IconContent;
                _CorePtspRequest.Address = _Request.AddressComponent;
                _CorePtspRequest.ContactPerson = _Request.ContactPerson;
                _CorePtspRequest.UserReference = _Request.UserReference;
                _CorePtsp = new CorePtsp();
                var _Response = _CorePtsp.SavePtsp(_CorePtspRequest);
                if (_Response.Status == ResponseStatus.Success)
                {
                    var ResponseDetails = new
                    {
                        ReferenceId = _Response.ReferenceId,
                        ReferenceKey = _Response.ReferenceKey,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ResponseDetails, _Response.StatusCode, _Response.Message);
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _Response.StatusCode, _Response.Message);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SavePtsp", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveMerchant(OMerchant.Request _Request)
        {
            #region Manage Exception
            try
            { 
                _CoreMerchantRequest = new OCoreMerchant.Request();
                _CoreMerchantRequest.OwnerId = _Request.OwnerId;
                _CoreMerchantRequest.OwnerKey = _Request.OwnerKey;
                _CoreMerchantRequest.AcquirerId = _Request.AcquirerId;
                _CoreMerchantRequest.AcquirerKey = _Request.AcquirerKey;
                _CoreMerchantRequest.AccountOperationTypeCode = _Request.AccountOperationTypeCode;
                _CoreMerchantRequest.DisplayName = _Request.DisplayName;
                _CoreMerchantRequest.FirstName = _Request.BusinessOwnerName;
                _CoreMerchantRequest.CompanyName = _Request.CompanyName;
                _CoreMerchantRequest.ContactNumber = _Request.ContactNumber;
                _CoreMerchantRequest.MobileNumber = _Request.MobileNumber;
                _CoreMerchantRequest.EmailAddress = _Request.EmailAddress;
                _CoreMerchantRequest.ReferralCode = _Request.ReferralCode;
                _CoreMerchantRequest.UserName = _Request.UserName;
                _CoreMerchantRequest.Password = _Request.Password;
                _CoreMerchantRequest.WebsiteUrl = _Request.WebsiteUrl;
                _CoreMerchantRequest.Description = _Request.Description;
                _CoreMerchantRequest.BranchId = _Request.BranchId;
                _CoreMerchantRequest.BranchKey = _Request.BranchKey;
                _CoreMerchantRequest.RmId = _Request.RmId;
                _CoreMerchantRequest.RmKey = _Request.RmKey;
                _CoreMerchantRequest.StartDate = _Request.StartDate;
                _CoreMerchantRequest.IsDealMerchant = _Request.IsDealMerchant;
                if (_Request.Configurations != null)
                {
                    OCoreMerchant.Configuration _MConfig = new OCoreMerchant.Configuration();
                    _MConfig.RewardDeductionTypeCode = _Request.Configurations.RewardDeductionTypeCode;
                    _MConfig.RewardPercentage = _Request.Configurations.RewardPercentage;
                    _CoreMerchantRequest.Configurations = _MConfig;
                }
                _CoreMerchantRequest.Address = _Request.AddressComponent;
                _CoreMerchantRequest.ContactPerson = _Request.ContactPerson;
                if (_Request.Stores != null && _Request.Stores.Count > 0)
                {
                    List<OCoreMerchant.Store> _CoreStores = new List<OCoreMerchant.Store>();
                    foreach (var item in _Request.Stores)
                    {
                        List<OCoreMerchant.Terminal> _CoreTerminals = new List<OCoreMerchant.Terminal>();
                        if (item.Terminals != null && item.Terminals.Count > 0)
                        {
                            foreach (var TerminalItem in item.Terminals)
                            {
                                _CoreTerminals.Add(new OCoreMerchant.Terminal
                                {
                                    AcquirerId = TerminalItem.AcquirerId,
                                    AcquirerKey = TerminalItem.AcquirerKey,
                                    ProviderId = TerminalItem.ProviderId,
                                    ProviderKey = TerminalItem.ProviderKey,
                                    SerialNumber = TerminalItem.SerialNumber,
                                    TerminalId = TerminalItem.TerminalId,
                                });
                            }
                        }
                        _CoreStores.Add(new OCoreMerchant.Store
                        {
                            DisplayName = item.DisplayName,
                            Name = item.Name,
                            ContactNumber = item.ContactNumber,
                            EmailAddress = item.EmailAddress,
                            AccountOperationTypeCode = item.AccountOperationTypeCode,
                            WebsiteUrl = item.WebsiteUrl,
                            Description = item.Description,
                            StartDate = item.StartDate,
                            Address = item.AddressComponent,
                            ContactPerson = item.ContactPerson,
                            Categories = item.Categories,
                            Terminals = _CoreTerminals,
                        });
                    }
                    _CoreMerchantRequest.Stores = _CoreStores;
                }
                _CoreMerchantRequest.StatusCode = _Request.StatusCode;
                _CoreMerchantRequest.Categories = _Request.Categories;
                _CoreMerchantRequest.UserReference = _Request.UserReference;
                _CoreMerchant = new CoreMerchant();
                var _Response = _CoreMerchant.SaveMerchant(_CoreMerchantRequest);
                if (_Response.Status == ResponseStatus.Success)
                {
                    var ResponseDetails = new
                    {
                        ReferenceId = _Response.ReferenceId,
                        ReferenceKey = _Response.ReferenceKey,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ResponseDetails, _Response.StatusCode, _Response.Message);
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _Response.StatusCode, _Response.Message);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SaveMerchant", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
            #endregion
        }
    }
}
