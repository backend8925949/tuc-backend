//==================================================================================
// FileName: FrameworkCustomer.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for credit supercash and point purchase functionality of customer.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.Framework.Console
{
    internal class FrameworkCustomer
    {
        HCoreContext _HCoreContext;
        OCustomer.Overview _Overview;
        internal class TransactionTypeReward
        {
            internal const int PointPurchase = 567;
        }

        /// <summary>
        /// Description: Gets the point purchase history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPointPurchaseHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId > 0 && !string.IsNullOrEmpty(_Request.AccountKey))
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                .Where(x => x.AccountId == _Request.AccountId
                                && x.Account.Guid == _Request.AccountKey
                                && x.TypeId == TransactionTypeReward.PointPurchase
                                && x.ModeId == TransactionMode.Credit
                                 && x.SourceId == TransactionSource.TUC)
                                                    .Select(x => new OCustomer.PointPurchaseHistory
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        Amount = x.Amount,
                                                        Charge = x.Charge,
                                                        TotalAmount = x.TotalAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        PaymentMethodCode = x.PaymentMethod.SystemName,
                                                        PaymentMethodName = x.PaymentMethod.Name,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OCustomer.PointPurchaseHistory> _List = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                .Where(x => x.AccountId == _Request.AccountId
                                && x.Account.Guid == _Request.AccountKey
                                && x.TypeId == TransactionTypeReward.PointPurchase
                                && x.ModeId == TransactionMode.Credit
                                 && x.SourceId == TransactionSource.TUC)
                                                    .Select(x => new OCustomer.PointPurchaseHistory
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        Amount = x.Amount,
                                                        Charge = x.Charge,
                                                        TotalAmount = x.TotalAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        PaymentMethodCode = x.PaymentMethod.SystemName,
                                                        PaymentMethodName = x.PaymentMethod.Name,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        //foreach (var DataItem in _List)
                        //{
                        //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        //    {
                        //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        //    }
                        //    else
                        //    {
                        //        DataItem.IconUrl = _AppConfig.Default_Icon;
                        //    }
                        //}
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                .Where(x => x.ModeId == TransactionMode.Credit
                                && x.TypeId == TransactionType.TUCWalletTopup
                                 && x.SourceId == TransactionSource.TUC)
                                                    .Select(x => new OCustomer.PointPurchaseHistory
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        Amount = x.Amount,
                                                        Charge = x.Charge,
                                                        TotalAmount = x.TotalAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        PaymentMethodCode = x.PaymentMethod.SystemName,
                                                        PaymentMethodName = x.PaymentMethod.Name,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountMobileNumber = x.Account.MobileNumber,
                                                        AccountIconUrl = x.Account.IconStorage.Path,

                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OCustomer.PointPurchaseHistory> _List = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                               .Where(x => x.ModeId == TransactionMode.Credit
                                && x.TypeId == TransactionType.TUCWalletTopup
                                 && x.SourceId == TransactionSource.TUC)
                                                    .Select(x => new OCustomer.PointPurchaseHistory
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        Amount = x.Amount,
                                                        Charge = x.Charge,
                                                        TotalAmount = x.TotalAmount,
                                                        TransactionDate = x.TransactionDate,
                                                        ReferenceNumber = x.ReferenceNumber,
                                                        PaymentMethodCode = x.PaymentMethod.SystemName,
                                                        PaymentMethodName = x.PaymentMethod.Name,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountMobileNumber = x.Account.MobileNumber,
                                                        AccountIconUrl = x.Account.IconStorage.Path,

                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        //foreach (var DataItem in _List)
                        //{
                        //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        //    {
                        //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        //    }
                        //    else
                        //    {
                        //        DataItem.IconUrl = _AppConfig.Default_Icon;
                        //    }
                        //}
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }

                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetPointPurchaseHistory", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the point purchase overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPointPurchaseOverview(OReference _Request)
        {
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId > 0 && !string.IsNullOrEmpty(_Request.AccountKey))
                    {
                        _Overview = new OCustomer.Overview();
                        _Overview.Total = _HCoreContext.HCUAccountTransaction
                                          .Count(x => x.Account.CountryId == _Request.UserReference.SystemCountry
                                          && x.AccountId == _Request.AccountId
                                          && x.Account.Guid == _Request.AccountKey
                                          && x.TypeId == TransactionTypeReward.PointPurchase
                                          && x.ModeId == TransactionMode.Credit
                                          && x.SourceId == TransactionSource.TUC);
                        _Overview.Amount = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry
                                          && x.AccountId == _Request.AccountId
                                          && x.Account.Guid == _Request.AccountKey
                                          && x.TypeId == TransactionTypeReward.PointPurchase
                                          && x.ModeId == TransactionMode.Credit
                                          && x.SourceId == TransactionSource.TUC).Sum(a => a.Amount);

                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Overview, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _Overview = new OCustomer.Overview();
                        _Overview.Total = _HCoreContext.HCUAccountTransaction
                                          .Count(x => x.Account.CountryId == _Request.UserReference.SystemCountry
                                          && x.ModeId == TransactionMode.Credit
                                          && x.TypeId == TransactionType.TUCWalletTopup
                                          && x.SourceId == TransactionSource.TUC);
                        _Overview.Amount = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry
                                          && x.ModeId == TransactionMode.Credit
                                          && x.TypeId == TransactionType.TUCWalletTopup
                                          && x.SourceId == TransactionSource.TUC).Sum(a => a.Amount);

                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Overview, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetPointPurchaseOverview", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
    }
}
