//==================================================================================
// FileName: FrameworkSystemAdministration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to system administration
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Logging;
using HCore.Helper;
using HCore.TUC.Core.CoreAccount;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.Framework.Console
{
    internal class FrameworkSystemAdministration
    {
        HCoreContext _HCoreContext;
        HCCoreParameter _HCCoreParameter;
        List<HCCoreParameter> _HCCoreParameters;
        HCCoreCommon _HCCoreCommon;
        HCoreContextLogging _HCoreContextLogging;
        HCUAccountParameter _HCUAccountParameter;

        //#region Category Manager
        //internal OResponse SaveCategory(OSystemAdministration.Category.Request _Request)
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_Request.Name))
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1229, TUCCoreResource.CA1229M);
        //        }
        //        else if (_Request.IconContent == null || string.IsNullOrEmpty(_Request.IconContent.Content))
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1230, TUCCoreResource.CA1230M);
        //        }
        //        else if (string.IsNullOrEmpty(_Request.StatusCode))
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
        //        }
        //        else
        //        {
        //            int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
        //            if (StatusId == 0)
        //            {
        //                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
        //            }
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                bool _DetailCheck = _HCoreContext.HCCoreCommon.Any(x => x.TypeId == HelperType.MerchantCategory && x.Name == _Request.Name);
        //                if (_DetailCheck)
        //                {
        //                    _HCoreContext.Dispose();
        //                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1203, TUCCoreResource.CA1203M);
        //                }
        //                _HCCoreCommon = new HCCoreCommon();
        //                _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
        //                _HCCoreCommon.TypeId = HelperType.MerchantCategory;
        //                _HCCoreCommon.Name = _Request.Name;
        //                _HCCoreCommon.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
        //                _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
        //                _HCCoreCommon.CreatedById = _Request.UserReference.AccountId;
        //                _HCCoreCommon.StatusId = StatusId;
        //                _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
        //                _HCoreContext.SaveChanges();
        //                long? IconStorageId = null;
        //                if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
        //                {
        //                    IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
        //                }
        //                if (IconStorageId != null)
        //                {
        //                    using (_HCoreContext = new HCoreContext())
        //                    {
        //                        var UserAccDetails = _HCoreContext.HCCoreCommon.Where(x => x.Id == _HCCoreCommon.Id).FirstOrDefault();
        //                        if (UserAccDetails != null)
        //                        {
        //                            if (IconStorageId != null)
        //                            {
        //                                UserAccDetails.IconStorageId = IconStorageId;
        //                            }
        //                            _HCoreContext.SaveChanges();
        //                        }
        //                    }
        //                }
        //                var _Response = new
        //                {
        //                    ReferenceId = _HCCoreCommon.Id,
        //                    ReferenceKey = _HCCoreCommon.Guid,
        //                };
        //                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, TUCCoreResource.CA1204, TUCCoreResource.CA1204M);
        //            }
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        return HCoreHelper.LogException("SaveCategory", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
        //    }
        //}
        //internal OResponse UpdateCategory(OSystemAdministration.Category.Request _Request)
        //{
        //    try
        //    {
        //        if (_Request.ReferenceId < 1)
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
        //        }
        //        else if (string.IsNullOrEmpty(_Request.ReferenceKey))
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
        //        }
        //        else
        //        {
        //            int StatusId = 0;
        //            if (!string.IsNullOrEmpty(_Request.StatusCode))
        //            {
        //                StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
        //                if (StatusId == 0)
        //                {
        //                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
        //                }
        //            }

        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                var _Details = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.MerchantCategory && x.Name == _Request.Name).FirstOrDefault();
        //                if (_Details == null)
        //                {
        //                    _HCoreContext.Dispose();
        //                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
        //                }

        //                if (!string.IsNullOrEmpty(_Request.Name) && _Request.Name != _Details.Name)
        //                {
        //                    bool _DetailCheck = _HCoreContext.HCCoreCommon.Any(x => x.TypeId == HelperType.MerchantCategory && x.Name == _Request.Name);
        //                    if (_DetailCheck)
        //                    {
        //                        _HCoreContext.Dispose();
        //                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1231, TUCCoreResource.CA1231M);
        //                    }
        //                    _Details.Name = _Request.Name;
        //                    _Details.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
        //                }
        //                _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                _Details.ModifyById = _Request.UserReference.AccountId;
        //                if (StatusId != 0)
        //                {
        //                    _Details.StatusId = StatusId;
        //                }
        //                _HCoreContext.SaveChanges();

        //                long? IconStorageId = null;
        //                if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
        //                {
        //                    IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, _Details.IconStorageId, _Request.UserReference);
        //                }
        //                if (IconStorageId != null)
        //                {
        //                    using (_HCoreContext = new HCoreContext())
        //                    {
        //                        var UserAccDetails = _HCoreContext.HCCoreCommon.Where(x => x.Id == _HCCoreCommon.Id).FirstOrDefault();
        //                        if (UserAccDetails != null)
        //                        {
        //                            if (IconStorageId != null)
        //                            {
        //                                UserAccDetails.IconStorageId = IconStorageId;
        //                            }
        //                            _HCoreContext.SaveChanges();
        //                        }
        //                    }
        //                }
        //                var _Response = new
        //                {
        //                    ReferenceId = _Details.Id,
        //                    ReferenceKey = _Details.Guid,
        //                };
        //                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, TUCCoreResource.CA1232, TUCCoreResource.CA1232M);
        //            }
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        return HCoreHelper.LogException("UpdateCategory", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
        //    }
        //}
        //internal OResponse GetCategory(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_Request.SearchCondition))
        //        {
        //            HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
        //        }
        //        if (string.IsNullOrEmpty(_Request.SortExpression))
        //        {
        //            HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
        //        }
        //        if (_Request.Limit < 1)
        //        {
        //            _Request.Limit = _AppConfig.DefaultRecordsLimit;
        //        }
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            if (_Request.RefreshCount)
        //            {
        //                #region Total Records
        //                _Request.TotalRecords = _HCoreContext.HCCoreCommon
        //                                        .Where(x => x.TypeId == HelperType.MerchantCategory)
        //                                        .Select(x => new OSystemAdministration.Category.List
        //                                        {
        //                                            ReferenceId = x.Id,
        //                                            ReferenceKey = x.Guid,

        //                                            Name = x.Name,

        //                                            IconUrl = _AppConfig.StorageUrl + x.IconStorage.Path,

        //                                            CreateDate = x.CreateDate,
        //                                            CreatedById = x.CreatedById,
        //                                            CreatedByKey = x.CreatedBy.Guid,
        //                                            CreatedByDisplayName = x.CreatedBy.DisplayName,

        //                                            ModifyDate = x.ModifyDate,
        //                                            ModifyById = x.ModifyById,
        //                                            ModifyByKey = x.ModifyBy.Guid,
        //                                            ModifyByDisplayName = x.ModifyBy.DisplayName,

        //                                            StatusCode = x.Status.SystemName,
        //                                            StatusName = x.Status.Name,

        //                                        })
        //                                       .Where(_Request.SearchCondition)
        //                               .Count();
        //                #endregion
        //            }
        //            #region Get Data
        //            List<OSystemAdministration.Category.List> _List = _HCoreContext.HCCoreCommon
        //                                        .Where(x => x.TypeId == HelperType.MerchantCategory)
        //                                        .Select(x => new OSystemAdministration.Category.List
        //                                        {
        //                                            ReferenceId = x.Id,
        //                                            ReferenceKey = x.Guid,

        //                                            Name = x.Name,

        //                                            IconUrl = _AppConfig.StorageUrl + x.IconStorage.Path,

        //                                            CreateDate = x.CreateDate,
        //                                            CreatedById = x.CreatedById,
        //                                            CreatedByKey = x.CreatedBy.Guid,
        //                                            CreatedByDisplayName = x.CreatedBy.DisplayName,

        //                                            ModifyDate = x.ModifyDate,
        //                                            ModifyById = x.ModifyById,
        //                                            ModifyByKey = x.ModifyBy.Guid,
        //                                            ModifyByDisplayName = x.ModifyBy.DisplayName,

        //                                            StatusCode = x.Status.SystemName,
        //                                            StatusName = x.Status.Name,
        //                                        })
        //                                     .Where(_Request.SearchCondition)
        //                                     .OrderBy(_Request.SortExpression)
        //                                     .Skip(_Request.Offset)
        //                                     .Take(_Request.Limit)
        //                                     .ToList();
        //            #endregion
        //            #region Create  Response Object
        //            _HCoreContext.Dispose();
        //            //foreach (var DataItem in _List)
        //            //{
        //            //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
        //            //    {
        //            //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
        //            //    }
        //            //    else
        //            //    {
        //            //        DataItem.IconUrl = _AppConfig.Default_Icon;
        //            //    }
        //            //}
        //            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
        //            #endregion
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //            #endregion
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        return HCoreHelper.LogException("GetCategory", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
        //    }
        //    #endregion
        //}
        //#endregion

        #region System Log
        /// <summary>
        /// Description: Gets the system log.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSystemLog(OSystemAdministration.ExceptionLog.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operation
                    using (_HCoreContextLogging = new HCoreContextLogging())
                    {

                        #region Get Data
                        OSystemAdministration.ExceptionLog.Details Details = (from n in _HCoreContextLogging.HCLCoreLog
                                                                              where n.Id == _Request.ReferenceId
                                                                              select new OSystemAdministration.ExceptionLog.Details
                                                                              {
                                                                                  ReferenceId = n.Id,
                                                                                  //ReferenceKey = n.Guid,    
                                                                                  TypeId = 1,
                                                                                  // TypeCode = _HCoreContext.HCCoreHelper.Where(x => x.Id == n.TypeId).Select(x => x.SystemName).FirstOrDefault(),
                                                                                  //  TypeName = _HCoreContext.HCCoreHelper.Where(x => x.Id == n.TypeId).Select(x => x.Name).FirstOrDefault(),


                                                                                  Title = n.Title,
                                                                                  Description = n.Message,
                                                                                  //Comment = n.Comment,
                                                                                  UserReferenceContent = n.RequestReference,
                                                                                  Data = n.Data,

                                                                                  CreateDate = n.CreateDate,
                                                                                  //  CreatedByKey = _HCoreContext.HCUAccount.Where(x => x.Id == n.CreatedById).Select(x => x.Guid).FirstOrDefault(),
                                                                                  //  CreatedByDisplayName = _HCoreContext.HCUAccount.Where(x => x.Id == n.CreatedById).Select(x => x.DisplayName).FirstOrDefault(),


                                                                                  //ModifyDate = n.ModifyDate,
                                                                                  //   ModifyByKey = _HCoreContext.HCUAccount.Where(x => x.Id == n.ModifyById).Select(x => x.Guid).FirstOrDefault(),
                                                                                  //   ModifyByDisplayName = _HCoreContext.HCUAccount.Where(x => x.Id == n.ModifyById).Select(x => x.DisplayName).FirstOrDefault(),



                                                                                  //StatusId = n.StatusId,
                                                                                  //  StatusCode = _HCoreContext.HCCoreHelper.Where(x => x.Id == n.StatusId).Select(x => x.SystemName).FirstOrDefault(),
                                                                                  //  StatusName = _HCoreContext.HCCoreHelper.Where(x => x.Id == n.StatusId).Select(x => x.Name).FirstOrDefault(),

                                                                              })
                                         .FirstOrDefault();
                        _HCoreContextLogging.Dispose();
                        if (Details != null)
                        {
                            //#region Format List
                            //if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                            //{
                            //    Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                            //}
                            //else
                            //{
                            //    Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                            //}
                            //if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                            //{
                            //    Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                            //}
                            //else
                            //{
                            //    Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                            //}
                            //#endregion

                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC0001");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, Details, "HC0002");
                            #endregion
                        }
                        #endregion

                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCoreLog", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the system log.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSystemLog(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from n in _HCoreContextLogging.HCLCoreLog
                                                 select new OSystemAdministration.ExceptionLog.List
                                                 {
                                                     ReferenceId = n.Id,
                                                     TypeId = 1,
                                                     Name = n.Title,
                                                     Description = n.Message,
                                                     CreateDate = n.CreateDate,
                                                 })
                                      .Where(_Request.SearchCondition)
                              .Count();
                    }

                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = _Request.TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OSystemAdministration.ExceptionLog.List> Data = (from n in _HCoreContextLogging.HCLCoreLog
                                                                          select new OSystemAdministration.ExceptionLog.List
                                                                          {
                                                                              ReferenceId = n.Id,
                                                                              TypeId = 1,
                                                                              Name = n.Title,
                                                                              Description = n.Message,
                                                                              CreateDate = n.CreateDate,
                                                                          })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var Details in Data)
                    {
                        if (!string.IsNullOrEmpty(Details.Description))
                        {
                            if (Details.Description.Length > 60)
                            {
                                Details.Description.Substring(0, 59);
                            }
                        }
                    }
                    _HCoreContextLogging.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetLogs", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        #endregion

        #region Response Code Manager
        /// <summary>
        /// Description: Gets the response code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetResponseCode(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCCoreCommon
                                                .Where(x => x.TypeId == HelperType.ResponseCodes)
                                                .Select(x => new OSystemAdministration.ResponseCode.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    Message = x.Value,
                                                    Description = x.Description,
                                                    CreateDate = x.CreateDate,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OSystemAdministration.ResponseCode.List> _List = _HCoreContext.HCCoreCommon
                                                .Where(x => x.TypeId == HelperType.ResponseCodes)
                                                .Select(x => new OSystemAdministration.ResponseCode.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    Message = x.Value,
                                                    Description = x.Description,
                                                    CreateDate = x.CreateDate,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    //foreach (var DataItem in _List)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //    {
                    //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.IconUrl = _AppConfig.Default_Icon;
                    //    }
                    //}
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetConfiguration", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the response code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateResponseCode(OSystemAdministration.ResponseCode.Request _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                if (string.IsNullOrEmpty(_Request.Message))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1225, TUCCoreResource.CA1225M);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var _Details = _HCoreContext.HCCoreCommon.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.TypeId == HelperType.ResponseCodes).FirstOrDefault();
                        if (_Details == null)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                        if (!string.IsNullOrEmpty(_Request.Message) && _Request.Message != _Details.Value)
                        {
                            _Details.Value = _Request.Message;
                        }
                        if (!string.IsNullOrEmpty(_Request.Description) && _Request.Description != _Details.Description)
                        {
                            _Details.Description = _Request.Description;
                        }
                        _Details.ModifyById = _Request.UserReference.AccountId;
                        _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1226, TUCCoreResource.CA1226M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateConfiguration", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        #endregion

        #region Configuration Manager
        /// <summary>
        /// Description: Saves the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveConfiguration(OSystemAdministration.Configuration.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1213, TUCCoreResource.CA1213M);
                }
                else if (string.IsNullOrEmpty(_Request.SystemName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1214, TUCCoreResource.CA1214M);
                }
                else if (string.IsNullOrEmpty(_Request.DataTypeCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1215, TUCCoreResource.CA1215M);
                }
                else if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                else if (string.IsNullOrEmpty(_Request.Value))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1216, TUCCoreResource.CA1216M);
                }
                else
                {
                    int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                    }
                    int DataTypeId = HCoreHelper.GetHelperId(_Request.DataTypeCode);
                    if (DataTypeId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1217, TUCCoreResource.CA1217M);
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        bool _DetailCheck = _HCoreContext.HCCoreCommon.Any(x => x.TypeId == HelperType.Configuration && (x.SystemName == _Request.SystemName || x.Name == _Request.Name));
                        if (_DetailCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1218, TUCCoreResource.CA1218M);
                        }
                        _HCCoreCommon = new HCCoreCommon();
                        _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                        _HCCoreCommon.TypeId = HelperType.Configuration;
                        _HCCoreCommon.Name = _Request.Name;
                        _HCCoreCommon.SystemName = _Request.SystemName;
                        _HCCoreCommon.HelperId = DataTypeId;
                        _HCCoreCommon.Value = _Request.Value;
                        _HCCoreCommon.Description = _Request.Description;
                        _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCCoreCommon.CreatedById = _Request.UserReference.AccountId;
                        _HCCoreCommon.StatusId = StatusId;
                        _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            ReferenceId = _HCCoreCommon.Id,
                            ReferenceKey = _HCCoreCommon.Guid,
                        };
                        using (_HCoreContext = new HCoreContext())
                        {
                            _HCCoreCommon = new HCCoreCommon();
                            _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                            _HCCoreCommon.TypeId = HelperType.ConfigurationValue;
                            _HCCoreCommon.ParentId = _Response.ReferenceId;
                            _HCCoreCommon.Value = _Request.Value;
                            _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCCoreCommon.CreatedById = _Request.UserReference.AccountId;
                            _HCCoreCommon.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                            _HCoreContext.SaveChanges();
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, TUCCoreResource.CA1219, TUCCoreResource.CA1219M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveConfiguration", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Updates the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateConfiguration(OSystemAdministration.Configuration.Request _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                else
                {
                    int StatusId = 0;
                    if (!string.IsNullOrEmpty(_Request.StatusCode))
                    {
                        StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                        if (StatusId == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1149, TUCCoreResource.CA1149M);
                        }
                    }

                    int DataTypeId = 0;
                    if (!string.IsNullOrEmpty(_Request.DataTypeCode))
                    {
                        DataTypeId = HCoreHelper.GetHelperId(_Request.DataTypeCode);
                        if (DataTypeId == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1217, TUCCoreResource.CA1217M);
                        }
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        var _Details = _HCoreContext.HCCoreCommon.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.TypeId == HelperType.Configuration).FirstOrDefault();
                        if (_Details == null)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                        if (!string.IsNullOrEmpty(_Request.Name) && _Request.Name != _Details.Name)
                        {
                            bool _DetailCheck = _HCoreContext.HCCoreCommon.Any(x => x.TypeId == HelperType.Configuration && x.Name == _Request.Name);
                            if (_DetailCheck)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1227, TUCCoreResource.CA1227M);
                            }
                            _Details.Name = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.SystemName) && _Request.SystemName != _Details.SystemName)
                        {
                            bool _DetailCheck = _HCoreContext.HCCoreCommon.Any(x => x.TypeId == HelperType.Configuration && x.SystemName == _Request.SystemName);
                            if (_DetailCheck)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1228, TUCCoreResource.CA1228M);
                            }
                            _Details.SystemName = _Request.SystemName;
                        }
                        if (!string.IsNullOrEmpty(_Request.Description) && _Request.Description != _Details.Description)
                        {
                            _Details.Description = _Request.Description;
                        }
                        if (DataTypeId != 0)
                        {
                            _Details.HelperId = DataTypeId;
                        }
                        if (StatusId != 0)
                        {
                            _Details.StatusId = StatusId;
                        }
                        if (_Details.Value != _Request.Value)
                        {
                            _Details.Value = _Request.Value;
                            var ActiveConfigs = _HCoreContext.HCCoreCommon.Where(x => x.ParentId == _Request.ReferenceId && x.StatusId == HelperStatus.Default.Active).ToList();
                            foreach (var ActiveConfig in ActiveConfigs)
                            {
                                ActiveConfig.StatusId = HelperStatus.Default.Blocked;
                                ActiveConfig.ModifyById = _Request.UserReference.AccountId;
                                ActiveConfig.ModifyDate = HCoreHelper.GetGMTDateTime();
                            }
                            _HCCoreCommon = new HCCoreCommon();
                            _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                            _HCCoreCommon.TypeId = HelperType.ConfigurationValue;
                            _HCCoreCommon.ParentId = _Details.Id;
                            _HCCoreCommon.Value = _Request.Value;
                            _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCCoreCommon.StatusId = HelperStatus.Default.Active;
                            _HCCoreCommon.CreatedById = _Request.UserReference.AccountId;
                            _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                        }
                        _Details.ModifyById = _Request.UserReference.AccountId;
                        _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1220, TUCCoreResource.CA1220M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateConfiguration", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Updates the configuration value.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateConfigurationValue(OSystemAdministration.ConfigurationValue.Request _Request)
        {
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                else if (string.IsNullOrEmpty(_Request.Value))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1211, TUCCoreResource.CA1211M);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x => x.Id == _Request.ReferenceId
                        && x.Guid == _Request.ReferenceKey
                        && x.TypeId == HelperType.Configuration)
                            .FirstOrDefault();
                        if (ConfigurationDetails == null)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                        var ActiveConfigs = _HCoreContext.HCCoreCommon.Where(x => x.ParentId == _Request.ReferenceId && x.StatusId == HelperStatus.Default.Active).ToList();
                        foreach (var ActiveConfig in ActiveConfigs)
                        {
                            ActiveConfig.StatusId = HelperStatus.Default.Blocked;
                            ActiveConfig.ModifyById = _Request.UserReference.AccountId;
                            ActiveConfig.ModifyDate = HCoreHelper.GetGMTDateTime();
                        }
                        _HCCoreCommon = new HCCoreCommon();
                        _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                        _HCCoreCommon.TypeId = HelperType.ConfigurationValue;
                        _HCCoreCommon.ParentId = ConfigurationDetails.Id;
                        _HCCoreCommon.Value = _Request.Value;
                        _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCCoreCommon.StatusId = HelperStatus.Default.Active;
                        _HCCoreCommon.CreatedById = _Request.UserReference.AccountId;
                        _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                        ConfigurationDetails.Value = _Request.Value;
                        ConfigurationDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        ConfigurationDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        //var _Response = new
                        //{
                        //    ReferenceId = _HCCoreParameter.Id,
                        //    ReferenceKey = _HCCoreParameter.Guid,
                        //};
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1212, TUCCoreResource.CA1212M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateConfigurationValue", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Updates the configuration value.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateConfigurationValue(OSystemAdministration.AccountConfiguration.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.ConfigurationKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1304, TUCCoreResource.CA1304M);
                }
                else if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                else if (string.IsNullOrEmpty(_Request.Value))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1211, TUCCoreResource.CA1211M);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x => x.Guid == _Request.ConfigurationKey
                        && x.TypeId == HelperType.Configuration)
                            .Select(x => x.Id)
                            .FirstOrDefault(); ;
                        if (ConfigurationDetails == 0)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                        var ActiveConfigs = _HCoreContext.HCUAccountParameter.Where(x => x.Common.SystemName == _Request.ConfigurationKey
                        && x.AccountId == _Request.AccountId
                        && x.Account.Guid == _Request.AccountKey
                        && x.StatusId == HelperStatus.Default.Active
                        ).ToList();
                        if (ActiveConfigs.Count > 0)
                        {
                            foreach (var ActiveConfig in ActiveConfigs)
                            {
                                ActiveConfig.StatusId = HelperStatus.Default.Blocked;
                                ActiveConfig.ModifyById = _Request.UserReference.AccountId;
                                ActiveConfig.ModifyDate = HCoreHelper.GetGMTDateTime();
                            }
                        }

                        _HCUAccountParameter = new HCUAccountParameter();
                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                        _HCUAccountParameter.CommonId = ConfigurationDetails;
                        _HCUAccountParameter.AccountId = _Request.AccountId;
                        _HCUAccountParameter.Value = _Request.Value;
                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                        _HCoreContext.SaveChanges();
                        //var _Response = new
                        //{
                        //    ReferenceId = _HCCoreParameter.Id,
                        //    ReferenceKey = _HCCoreParameter.Guid,
                        //};
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1212, TUCCoreResource.CA1212M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateConfigurationValue", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Updates the configuration value.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateConfigurationValue(OSystemAdministration.AccountConfiguration.BulkRequest _Request)
        {
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAACCREFKEY, TUCCoreResource.CAACCREFKEYM);
                }
                else if (_Request.Configurations == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1211, TUCCoreResource.CA1211M);
                }
                else
                {
                    foreach (var Configuration in _Request.Configurations)
                    {

                        using (_HCoreContext = new HCoreContext())
                        {
                            var ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == Configuration.ConfigurationKey
                            && x.TypeId == HelperType.Configuration)
                                .Select(x => x.Id)
                                .FirstOrDefault(); ;
                            if (ConfigurationDetails == 0)
                            {
                                _HCoreContext.Dispose();
                            }
                            else
                            {
                                var ActiveConfigs = _HCoreContext.HCUAccountParameter.Where(x => x.Common.SystemName == Configuration.ConfigurationKey
                          && x.AccountId == _Request.AccountId
                          && x.Account.Guid == _Request.AccountKey
                          && x.StatusId == HelperStatus.Default.Active
                          ).ToList();
                                if (ActiveConfigs.Count > 0)
                                {
                                    foreach (var ActiveConfig in ActiveConfigs)
                                    {
                                        ActiveConfig.StatusId = HelperStatus.Default.Blocked;
                                        ActiveConfig.ModifyById = _Request.UserReference.AccountId;
                                        ActiveConfig.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    }
                                }
                                _HCUAccountParameter = new HCUAccountParameter();
                                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                _HCUAccountParameter.CommonId = ConfigurationDetails;
                                _HCUAccountParameter.AccountId = _Request.AccountId;
                                _HCUAccountParameter.Value = Configuration.Value;
                                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                _HCoreContext.SaveChanges();
                                //var _Response = new
                                //{
                                //    ReferenceId = _HCCoreParameter.Id,
                                //    ReferenceKey = _HCCoreParameter.Guid,
                                //};
                            }

                        }

                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1305, TUCCoreResource.CA1305M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateConfigurationValue", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Deletes the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteConfiguration(OSystemAdministration.Configuration.Request _Request)
        {
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var _Details = _HCoreContext.HCCoreCommon.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.TypeId == HelperType.Configuration).FirstOrDefault();
                        if (_Details == null)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                        bool RoleAccount = _HCoreContext.HCCoreParameter.Any(x => x.CommonId == _Request.ReferenceId);
                        if (RoleAccount)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1223, TUCCoreResource.CA1223M);
                        }

                        var RoleFeatures = _HCoreContext.HCCoreCommon.Where(x => x.ParentId == _Details.Id).ToList();
                        if (RoleFeatures.Count > 0)
                        {
                            _HCoreContext.HCCoreCommon.RemoveRange(RoleFeatures);
                            _HCoreContext.SaveChanges();
                            var _DeleteItem = _HCoreContext.HCCoreCommon.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.TypeId == HelperType.Role).FirstOrDefault();
                            if (_DeleteItem != null)
                            {
                                _HCoreContext.HCCoreCommon.Remove(_DeleteItem);
                                _HCoreContext.SaveChanges();
                            }
                        }
                        else
                        {
                            _HCoreContext.HCCoreCommon.Remove(_Details);
                            _HCoreContext.SaveChanges();
                        }
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1224, TUCCoreResource.CA1224M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateRole", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Gets the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetConfiguration(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCCoreCommon
                                                .Where(x => x.TypeId == HelperType.Configuration)
                                                .Select(x => new OSystemAdministration.Configuration.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    SystemName = x.SystemName,
                                                    DataTypeCode = x.Helper.SystemName,
                                                    DataTypeName = x.Helper.Name,
                                                    Value = x.Value,
                                                    Description = x.Description,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OSystemAdministration.Configuration.List> _List = _HCoreContext.HCCoreCommon
                                                .Where(x => x.TypeId == HelperType.Configuration)
                                                .Select(x => new OSystemAdministration.Configuration.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    SystemName = x.SystemName,
                                                    DataTypeCode = x.Helper.SystemName,
                                                    DataTypeName = x.Helper.Name,
                                                    Value = x.Value,
                                                    Description = x.Description,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    //foreach (var DataItem in _List)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //    {
                    //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.IconUrl = _AppConfig.Default_Icon;
                    //    }
                    //}
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetConfiguration", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the configuration account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetConfigurationAccount(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountParameter
                                                .Where(x => x.TypeId == HelperType.ConfigurationValue
                                                && x.StatusId == HelperStatus.Default.Active
                                                && x.AccountId == _Request.AccountId
                                                && x.Account.Guid == _Request.AccountKey)
                                                .Select(x => new OSystemAdministration.Configuration.List
                                                {
                                                    ReferenceId = x.Common.Id,
                                                    ReferenceKey = x.Common.Guid,
                                                    Name = x.Common.Name,
                                                    SystemName = x.Common.SystemName,
                                                    DataTypeCode = x.Common.Helper.SystemName,
                                                    DataTypeName = x.Common.Helper.Name,
                                                    Value = x.Value,
                                                    Description = x.Description,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OSystemAdministration.Configuration.List> _List = _HCoreContext.HCUAccountParameter
                                                .Where(x => x.TypeId == HelperType.ConfigurationValue
                                                && x.StatusId == HelperStatus.Default.Active
                                                && x.AccountId == _Request.AccountId
                                                && x.Account.Guid == _Request.AccountKey)
                                                .Select(x => new OSystemAdministration.Configuration.List
                                                {
                                                    ReferenceId = x.Common.Id,
                                                    ReferenceKey = x.Common.Guid,
                                                    Name = x.Common.Name,
                                                    SystemName = x.Common.SystemName,
                                                    DataTypeCode = x.Common.Helper.SystemName,
                                                    DataTypeName = x.Common.Helper.Name,
                                                    Value = x.Value,
                                                    Description = x.Description,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    //foreach (var DataItem in _List)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //    {
                    //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.IconUrl = _AppConfig.Default_Icon;
                    //    }
                    //}
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetConfiguration", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the configuration history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetConfigurationHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCCoreCommon
                                                .Where(x => x.TypeId == HelperType.ConfigurationValue
                                                && x.ParentId == _Request.ReferenceId
                                                && x.Parent.Guid == _Request.ReferenceKey)
                                                .Select(x => new OSystemAdministration.ConfigurationHistory.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Value = x.Value,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OSystemAdministration.ConfigurationHistory.List> _List = _HCoreContext.HCCoreCommon
                                                .Where(x => x.TypeId == HelperType.ConfigurationValue
                                                && x.ParentId == _Request.ReferenceId
                                                && x.Parent.Guid == _Request.ReferenceKey)
                                                .Select(x => new OSystemAdministration.ConfigurationHistory.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Value = x.Value,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    //foreach (var DataItem in _List)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //    {
                    //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.IconUrl = _AppConfig.Default_Icon;
                    //    }
                    //}
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetConfigurationHistory", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        #endregion

        #region Role Manager
        /// <summary>
        /// Description: Gets the feature.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetFeature(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.Type == "object")
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", ">");
                    }
                    if (string.IsNullOrEmpty(_Request.SortExpression))
                    {
                        HCoreHelper.GetSortCondition(_Request, "CreateDate", "asc");
                    }
                    if (_Request.Limit < 1)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCCoreCommon
                                                    .Where(x => x.TypeId == HelperType.FeatureOption)
                                                    //&& x.ParentId != null
                                                    //&& x.Parent.ParentId != null
                                                    //&& x.Parent.Parent.HelperId == UserAccountType.Admin
                                                    //&& x.Parent.Parent.StatusId == HelperStatus.Default.Active
                                                    //&& x.Parent.StatusId == HelperStatus.Default.Active
                                                    //&& x.StatusId == HelperStatus.Default.Active
                                                    .Select(x => new OSystemAdministration.Feature.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        Name = x.Parent.Parent.Name + " - " + x.Parent.Name + " - " + x.Name,
                                                        SystemName = x.SystemName,
                                                        ActionTypeCode = x.Helper.SystemName,
                                                        ActionTypeName = x.Helper.Name,
                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        StatusSystemName = x.Status.SystemName,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OSystemAdministration.Feature.List> _List = _HCoreContext.HCCoreCommon
                                                    .Where(x => x.TypeId == HelperType.FeatureOption)
                                                    //&& x.ParentId != null
                                                    //&& x.Parent.ParentId != null
                                                    //&& x.Parent.Parent.HelperId == UserAccountType.Admin
                                                    //&& x.Parent.Parent.StatusId == HelperStatus.Default.Active
                                                    //&& x.Parent.StatusId == HelperStatus.Default.Active
                                                    //&& x.StatusId == HelperStatus.Default.Active
                                                    .Select(x => new OSystemAdministration.Feature.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        //Name =   x.Name,
                                                        Name = x.Parent.Parent.Name + " - " + x.Parent.Name + " - " + x.Name,
                                                        SystemName = x.SystemName,
                                                        ActionTypeCode = x.Helper.SystemName,
                                                        ActionTypeName = x.Helper.Name,
                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                        StatusSystemName = x.Status.SystemName,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 //.Skip(_Request.Offset)
                                                 //.Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        //foreach (var DataItem in _List)
                        //{
                        //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        //    {
                        //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        //    
                        //    else
                        //    {
                        //        DataItem.IconUrl = _AppConfig.Default_Icon;
                        //    }
                        //}
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }

                }
                else
                {

                    using (_HCoreContext = new HCoreContext())
                    {
                        #region Get Data
                        List<OSystemAdministration.AuthFeatureCategory> _List = _HCoreContext.HCCoreCommon
                                                    .Where(x => x.TypeId == HelperType.FeatureCategory
                                                    && x.HelperId == UserAccountType.Admin
                                                    && x.StatusId == HelperStatus.Default.Active)
                                                    .Select(x => new OSystemAdministration.AuthFeatureCategory
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        Name = x.Name,
                                                        SystemName = x.SystemName,
                                                    })
                                                 .OrderBy(a => a.Name)
                                                 .ToList();
                        foreach (var _ListItem in _List)
                        {
                            _ListItem.Features = _HCoreContext.HCCoreCommon
                                                    .Where(x => x.TypeId == HelperType.Feature
                                                    && x.ParentId == _ListItem.ReferenceId
                                                    && x.StatusId == HelperStatus.Default.Active)
                                                    .Select(x => new OSystemAdministration.AuthFeature
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        Name = x.Name,
                                                        SystemName = x.SystemName,
                                                    })
                                                 .OrderBy(a => a.Name)
                                                 .ToList();
                            foreach (var FeatureItem in _ListItem.Features)
                            {
                                FeatureItem.Permissions = _HCoreContext.HCCoreCommon
                                                   .Where(x => x.TypeId == HelperType.FeatureOption
                                                   && x.ParentId == _ListItem.ReferenceId
                                                   && x.StatusId == HelperStatus.Default.Active)
                                                   .Select(x => new OSystemAdministration.AuthFeatureOption
                                                   {
                                                       ReferenceId = x.Id,
                                                       ReferenceKey = x.Guid,
                                                       Name = x.Name,
                                                       SystemName = x.SystemName,
                                                       ActionTypeCode = x.Helper.SystemName,
                                                       ActionTypeName = x.Helper.Name,
                                                   })
                                                .OrderBy(a => a.Name)
                                                .ToList();
                            }
                        }
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _List, "HC0001");
                        #endregion
                    }

                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDepartment", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the department.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDepartment(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCCoreCommon
                                                .Where(x => x.TypeId == HelperType.Department)
                                                .Select(x => new OSystemAdministration.Department.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Name = x.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OSystemAdministration.Department.List> _List = _HCoreContext.HCCoreCommon
                                                .Where(x => x.TypeId == HelperType.Department)
                                                .Select(x => new OSystemAdministration.Department.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Name = x.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    //foreach (var DataItem in _List)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //    {
                    //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.IconUrl = _AppConfig.Default_Icon;
                    //    }
                    //}
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDepartment", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the role.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRole(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCCoreRole
                                                .Where(x => x.HelperId == UserAccountType.Admin)
                                                .Select(x => new OSystemAdministration.Role.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Name = x.Name,

                                                    DepartmentKey = x.Common.Guid,
                                                    DepartmentName = x.Common.Name,

                                                    Features = _HCoreContext.HCCoreRoleFeature.Count(y => y.RoleId == x.Id && y.StatusId == HelperStatus.Default.Active),

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OSystemAdministration.Role.List> _List = _HCoreContext.HCCoreRole
                                              .Where(x => x.HelperId == UserAccountType.Admin)
                                                .Select(x => new OSystemAdministration.Role.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Name = x.Name,

                                                    DepartmentKey = x.Common.Guid,
                                                    DepartmentName = x.Common.Name,
                                                    Features = _HCoreContext.HCCoreRoleFeature.Count(y => y.RoleId == x.Id && y.StatusId == HelperStatus.Default.Active),

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    //foreach (var DataItem in _List)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //    {
                    //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.IconUrl = _AppConfig.Default_Icon;
                    //    }
                    //}
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetRole", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the role feature.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRoleFeature(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCCoreParameter
                                                .Where(x => x.TypeId == HelperType.RoleFeatures
                                                && x.ParentId == _Request.ReferenceId
                                                && x.Parent.Guid == _Request.ReferenceKey)
                                                .Select(x => new OSystemAdministration.Feature.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Common.Parent.Parent.Name + " - " + x.Common.Parent.Name + " - " + x.Common.Name,
                                                    SystemName = x.Common.SystemName,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OSystemAdministration.Feature.List> _List = _HCoreContext.HCCoreParameter
                                               .Where(x => x.TypeId == HelperType.RoleFeatures
                                                && x.ParentId == _Request.ReferenceId
                                                && x.Parent.Guid == _Request.ReferenceKey)
                                                .Select(x => new OSystemAdministration.Feature.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Common.Parent.Parent.Name + " - " + x.Common.Parent.Name + " - " + x.Common.Name,
                                                    SystemName = x.Common.SystemName,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    //foreach (var DataItem in _List)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //    {
                    //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.IconUrl = _AppConfig.Default_Icon;
                    //    }
                    //}
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetRoleFeature", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the role details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRoleDetails(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OSystemAdministration.Feature.RoleDetails details = _HCoreContext.HCCoreRole
                                               .Where(x => x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OSystemAdministration.Feature.RoleDetails
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    DepartmentKey = x.Common.Guid,
                                                    DepartmentName = x.Common.Name,
                                                    StatusCode = x.Status.SystemName
                                                })
                                               .FirstOrDefault();
                    if (details != null)
                    {
                        List<OSystemAdministration.Feature.FeatureList> _List = _HCoreContext.HCCoreFeature
                                              .Where(x => x.TypeId == HelperType.FeatureCategory
                                                && x.StatusId == HelperStatus.Default.Active
                                                && x.HelperId == UserAccountType.Admin
                                                )
                                              .Select(w => new OSystemAdministration.Feature.FeatureList
                                              {
                                                  ReferenceId = w.Id,
                                                  ReferenceKey = w.Guid,
                                                  Name = w.Name,
                                                  isTab = w.isTab,
                                                  isDelete = _HCoreContext.HCCoreRoleFeature.Where(y => y.RoleId == _Request.ReferenceId && y.FeatureId == w.Id).FirstOrDefault().isDelete ?? false,
                                                  isAdd = _HCoreContext.HCCoreRoleFeature.Where(y => y.RoleId == _Request.ReferenceId && y.FeatureId == w.Id).FirstOrDefault().isAdd ?? false,
                                                  isEdit = _HCoreContext.HCCoreRoleFeature.Where(y => y.RoleId == _Request.ReferenceId && y.FeatureId == w.Id).FirstOrDefault().isUpdate ?? false,
                                                  isView = _HCoreContext.HCCoreRoleFeature.Where(y => y.RoleId == _Request.ReferenceId && y.FeatureId == w.Id).FirstOrDefault().isView ?? false,
                                                  Child = (_HCoreContext.HCCoreFeature
                                                              .Where(y => y.StatusId == HelperStatus.Default.Active
                                                                         && y.ParentId == w.Id
                                                                         && y.ParentId != null)
                                                                              .Select(x => new OSystemAdministration.Feature.FeatureList
                                                                              {
                                                                                  ReferenceId = x.Id,
                                                                                  ReferenceKey = x.Guid,
                                                                                  Name = x.Name,
                                                                                  isTab = x.isTab,
                                                                                  isDelete = _HCoreContext.HCCoreRoleFeature.Where(y => y.RoleId == _Request.ReferenceId && y.FeatureId == x.Id).FirstOrDefault().isDelete ?? false,
                                                                                  isAdd = _HCoreContext.HCCoreRoleFeature.Where(y => y.RoleId == _Request.ReferenceId && y.FeatureId == x.Id).FirstOrDefault().isAdd ?? false,
                                                                                  isEdit = _HCoreContext.HCCoreRoleFeature.Where(y => y.RoleId == _Request.ReferenceId && y.FeatureId == x.Id).FirstOrDefault().isUpdate ?? false,
                                                                                  isView = _HCoreContext.HCCoreRoleFeature.Where(y => y.RoleId == _Request.ReferenceId && y.FeatureId == x.Id).FirstOrDefault().isView ?? false,
                                                                                  Child = (_HCoreContext.HCCoreFeature
                                                                                 .Where(y => y.StatusId == HelperStatus.Default.Active
                                                                                  && y.ParentId == x.Id
                                                                                  && y.ParentId != null)
                                                                                 .Select(y => new OSystemAdministration.Feature.FeatureList
                                                                                 {
                                                                                     ReferenceId = y.Id,
                                                                                     ReferenceKey = y.Guid,
                                                                                     Name = y.Name,
                                                                                     isTab = y.isTab,
                                                                                     isDelete = _HCoreContext.HCCoreRoleFeature.Where(z => z.RoleId == _Request.ReferenceId && z.FeatureId == y.Id).FirstOrDefault().isDelete ?? false,
                                                                                     isAdd = _HCoreContext.HCCoreRoleFeature.Where(z => z.RoleId == _Request.ReferenceId && z.FeatureId == y.Id).FirstOrDefault().isAdd ?? false,
                                                                                     isEdit = _HCoreContext.HCCoreRoleFeature.Where(z => z.RoleId == _Request.ReferenceId && z.FeatureId == y.Id).FirstOrDefault().isUpdate ?? false,
                                                                                     isView = _HCoreContext.HCCoreRoleFeature.Where(z => z.RoleId == _Request.ReferenceId && z.FeatureId == y.Id).FirstOrDefault().isView ?? false,
                                                                                     Child = (_HCoreContext.HCCoreFeature
                                                                                        .Where(z => z.StatusId == HelperStatus.Default.Active
                                                                                         && z.ParentId == y.Id
                                                                                         && z.ParentId != null)
                                                                                              .Select(z => new OSystemAdministration.Feature.FeatureList
                                                                                              {
                                                                                                  ReferenceId = z.Id,
                                                                                                  ReferenceKey = z.Guid,
                                                                                                  Name = z.Name,
                                                                                                  isTab = z.isTab,
                                                                                                  isDelete = _HCoreContext.HCCoreRoleFeature.Where(w => w.RoleId == _Request.ReferenceId && w.FeatureId == z.Id).FirstOrDefault().isDelete ?? false,
                                                                                                  isAdd = _HCoreContext.HCCoreRoleFeature.Where(w => w.RoleId == _Request.ReferenceId && w.FeatureId == z.Id).FirstOrDefault().isAdd ?? false,
                                                                                                  isEdit = _HCoreContext.HCCoreRoleFeature.Where(w => w.RoleId == _Request.ReferenceId && w.FeatureId == z.Id).FirstOrDefault().isUpdate ?? false,
                                                                                                  isView = _HCoreContext.HCCoreRoleFeature.Where(w => w.RoleId == _Request.ReferenceId && w.FeatureId == z.Id).FirstOrDefault().isView ?? false,
                                                                                              }).ToList())
                                                                                 }).ToList())
                                                                              }).ToList())
                                              })
                                           .ToList();
                        #endregion
                        #region Create  Response Object

                        details.featureLists = _List;

                    }
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();

                    //   OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, details, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, details, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetRoleDetails", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the role.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveRole(OSystemAdministration.Role.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1198, TUCCoreResource.CA1198M);
                }
                else if (string.IsNullOrEmpty(_Request.DepartmentKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1199, TUCCoreResource.CA1199M);
                }
                else if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1201, TUCCoreResource.CA1201M);
                }
                else if (_Request.Features == null || _Request.Features.Count < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1200, TUCCoreResource.CA1200M);
                }
                else
                {
                    int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1149, TUCCoreResource.CA1149M);
                    }
                    long DepartmentId = HCoreHelper.GetCoreCommonId(_Request.DepartmentKey, HelperType.Department);
                    if (DepartmentId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1202, TUCCoreResource.CA1202M);
                    }
                    long ReferenceId;
                    string ReferenceKey;
                    using (_HCoreContext = new HCoreContext())
                    {
                        bool _DetailCheck = _HCoreContext.HCCoreParameter.Any(x => x.Name == _Request.Name && x.CommonId == DepartmentId);
                        if (_DetailCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1203, TUCCoreResource.CA1203M);
                        }

                        HCCoreRole hCCoreRole = new HCCoreRole();
                        hCCoreRole.Guid = HCoreHelper.GenerateGuid();
                        hCCoreRole.Name = _Request.Name;
                        hCCoreRole.HelperId = UserAccountType.Admin;
                        hCCoreRole.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                        hCCoreRole.CommonId = DepartmentId;
                        hCCoreRole.CreateDate = HCoreHelper.GetGMTDateTime();
                        hCCoreRole.CreatedById = _Request.UserReference.AccountId;
                        hCCoreRole.StatusId = StatusId;
                        _HCoreContext.HCCoreRole.Add(hCCoreRole);
                        _HCoreContext.SaveChanges();
                        ReferenceId = hCCoreRole.Id;
                        ReferenceKey = hCCoreRole.Guid;
                        _HCoreContext.Dispose();
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        List<HCCoreRoleFeature> features = new List<HCCoreRoleFeature>();
                        foreach (var list in _Request.Features)
                        {
                            HCCoreRoleFeature hCCoreRoleFeature = new HCCoreRoleFeature();
                            hCCoreRoleFeature.Guid = HCoreHelper.GenerateGuid();
                            hCCoreRoleFeature.isAdd = list.isAdd;
                            hCCoreRoleFeature.isUpdate = list.isEdit;
                            hCCoreRoleFeature.isDelete = list.isDelete;
                            hCCoreRoleFeature.isView = list.isView;
                            hCCoreRoleFeature.isViewAll = list.isView;
                            bool isTab = Convert.ToBoolean(_HCoreContext.HCCoreFeature.Where(x => x.Id == list.ReferenceId).FirstOrDefault().isTab);
                            if (isTab)
                            {
                                hCCoreRoleFeature.isAdd = false;
                                hCCoreRoleFeature.isUpdate = false;
                                hCCoreRoleFeature.isDelete = false;
                            }
                            hCCoreRoleFeature.RoleId = ReferenceId;
                            hCCoreRoleFeature.FeatureId = list.ReferenceId;
                            hCCoreRoleFeature.CreateDate = HCoreHelper.GetGMTDateTime();
                            hCCoreRoleFeature.CreatedById = _Request.UserReference.AccountId;
                            hCCoreRoleFeature.StatusId = StatusId;
                            features.Add(hCCoreRoleFeature);
                        }
                        _HCoreContext.HCCoreRoleFeature.AddRange(features);
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_HCoreContext.HCCoreFeature.Any(x => _Request.Features.Select(x => x.ReferenceId).Contains(x.Id) && x.isTab == true))
                        {
                            List<HCCoreRoleFeature> features = new List<HCCoreRoleFeature>();
                            List<HCCoreRoleFeature> updatefeatures = new List<HCCoreRoleFeature>();
                            var ParentId = _HCoreContext.HCCoreFeature.Where(x => _Request.Features.Select(x => x.ReferenceId).Contains(x.Id) && x.StatusId == HelperStatus.Default.Active).Select(x => x.ParentId).Distinct().ToList();

                            foreach (var items in ParentId)
                            {

                                features = new List<HCCoreRoleFeature>();
                                var parentdetails = _HCoreContext.HCCoreRoleFeature.Where(x => x.RoleId == ReferenceId && x.FeatureId == items).FirstOrDefault();
                                if (parentdetails != null)
                                {
                                    parentdetails.isView = true;
                                    parentdetails.isViewAll = true;
                                    updatefeatures.Add(parentdetails);
                                }
                                else
                                {
                                    HCCoreRoleFeature subhCCoreRoleFeature = new HCCoreRoleFeature();
                                    subhCCoreRoleFeature.Guid = HCoreHelper.GenerateGuid();
                                    subhCCoreRoleFeature.isAdd = false;
                                    subhCCoreRoleFeature.isUpdate = false;
                                    subhCCoreRoleFeature.isDelete = false;
                                    subhCCoreRoleFeature.isView = true;
                                    subhCCoreRoleFeature.isViewAll = true;
                                    subhCCoreRoleFeature.RoleId = ReferenceId;
                                    subhCCoreRoleFeature.FeatureId = items;
                                    subhCCoreRoleFeature.CreateDate = HCoreHelper.GetGMTDateTime();
                                    subhCCoreRoleFeature.CreatedById = _Request.UserReference.AccountId;
                                    subhCCoreRoleFeature.StatusId = StatusId;
                                    features.Add(subhCCoreRoleFeature);
                                }
                            }
                            _HCoreContext.HCCoreRoleFeature.UpdateRange(updatefeatures);
                            _HCoreContext.HCCoreRoleFeature.AddRange(features);
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();
                        }



                        var _Response = new
                        {
                            ReferenceId = ReferenceId,
                            ReferenceKey = ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, TUCCoreResource.CA1204, TUCCoreResource.CA1204M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveRole", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Updates the role.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateRole(OSystemAdministration.Role.Request _Request)
        {
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                else
                {
                    int StatusId = 0;
                    if (!string.IsNullOrEmpty(_Request.StatusCode))
                    {
                        StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                        if (StatusId == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1149, TUCCoreResource.CA1149M);
                        }
                    }

                    long DepartmentId = 0;
                    if (!string.IsNullOrEmpty(_Request.DepartmentKey))
                    {
                        DepartmentId = HCoreHelper.GetCoreCommonId(_Request.DepartmentKey, HelperType.Department);
                        if (DepartmentId == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1202, TUCCoreResource.CA1202M);
                        }
                    }

                    List<HCCoreRoleFeature> addfeatures = new List<HCCoreRoleFeature>();
                    List<HCCoreRoleFeature> updatefeatures = new List<HCCoreRoleFeature>();
                    HCCoreRole _Details;
                    using (_HCoreContext = new HCoreContext())
                    {
                        _Details = _HCoreContext.HCCoreRole.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (_Details == null)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                        if (DepartmentId == 0)
                        {
                            DepartmentId = (long)_Details.CommonId;
                        }
                        if (!string.IsNullOrEmpty(_Request.Name) && _Request.Name != _Details.Name)
                        {
                            bool _DetailCheck = _HCoreContext.HCCoreRole.Any(x => x.Name == _Request.Name && x.CommonId == DepartmentId);
                            if (_DetailCheck)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1203, TUCCoreResource.CA1203M);
                            }

                            _Details.Name = _Request.Name;
                            _Details.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                        }

                        if (DepartmentId != 0 && _Details.CommonId != DepartmentId)
                        {
                            _Details.CommonId = DepartmentId;
                        }
                        if (StatusId != 0)
                        {
                            _Details.StatusId = StatusId;
                        }
                        _Details.ModifyById = _Request.UserReference.AccountId;
                        _Details.ModifyDate = HCoreHelper.GetGMTDateTime();

                        foreach (var list in _Request.Features)
                        {
                            var roledetails = _HCoreContext.HCCoreRoleFeature.Where(x => x.RoleId == _Details.Id && x.FeatureId == list.ReferenceId).FirstOrDefault();
                            if (roledetails != null)
                            {
                                roledetails.isAdd = list.isAdd;
                                roledetails.isUpdate = list.isEdit;
                                roledetails.isDelete = list.isDelete;
                                roledetails.isView = list.isView;
                                roledetails.isViewAll = list.isView;
                                bool isTab = Convert.ToBoolean(_HCoreContext.HCCoreFeature.Where(x => x.Id == roledetails.FeatureId).FirstOrDefault().isTab);
                                if (isTab)
                                {
                                    roledetails.isAdd = false;
                                    roledetails.isUpdate = false;
                                    roledetails.isDelete = false;
                                }
                                if (list.isAdd == false && list.isEdit == false && list.isDelete == false && list.isView == false)
                                {
                                    roledetails.StatusId = HelperStatus.Default.Inactive;
                                }
                                else if (list.isView == false && isTab)
                                {
                                    roledetails.StatusId = HelperStatus.Default.Inactive;
                                }
                                else
                                {
                                    roledetails.StatusId = HelperStatus.Default.Active;
                                }
                                roledetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                roledetails.ModifyById = _Request.UserReference.AccountId;


                                updatefeatures.Add(roledetails);
                            }
                            else
                            {

                                HCCoreRoleFeature hCCoreRoleFeature = new HCCoreRoleFeature();
                                bool isTab = Convert.ToBoolean(_HCoreContext.HCCoreFeature.Where(x => x.Id == list.ReferenceId).FirstOrDefault().isTab);
                                hCCoreRoleFeature.Guid = HCoreHelper.GenerateGuid();
                                hCCoreRoleFeature.isAdd = list.isAdd;
                                hCCoreRoleFeature.isUpdate = list.isEdit;
                                hCCoreRoleFeature.isDelete = list.isDelete;
                                hCCoreRoleFeature.isView = list.isView;
                                hCCoreRoleFeature.isViewAll = list.isView;
                                if (isTab)
                                {
                                    hCCoreRoleFeature.isAdd = false;
                                    hCCoreRoleFeature.isUpdate = false;
                                    hCCoreRoleFeature.isDelete = false;
                                }
                                hCCoreRoleFeature.RoleId = _Details.Id;
                                hCCoreRoleFeature.FeatureId = list.ReferenceId;
                                hCCoreRoleFeature.CreateDate = HCoreHelper.GetGMTDateTime();
                                hCCoreRoleFeature.CreatedById = _Request.UserReference.AccountId;
                                hCCoreRoleFeature.StatusId = StatusId;
                                addfeatures.Add(hCCoreRoleFeature);

                            }

                        }
                        _HCoreContext.HCCoreRole.Update(_Details);
                        _HCoreContext.HCCoreRoleFeature.UpdateRange(updatefeatures);
                        _HCoreContext.HCCoreRoleFeature.AddRange(addfeatures);
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        addfeatures = new List<HCCoreRoleFeature>();
                        updatefeatures = new List<HCCoreRoleFeature>();
                        if (_HCoreContext.HCCoreFeature.Any(x => _Request.Features.Select(y => y.ReferenceId).Contains(x.Id) && x.isTab == true))
                        {
                            var ParentId = _HCoreContext.HCCoreFeature.Where(x => _Request.Features.Select(y => y.ReferenceId).Contains(x.Id) && x.StatusId == HelperStatus.Default.Active).Select(x => x.ParentId).Distinct().ToList();
                            foreach (var item in ParentId)
                            {

                                var parentdetails = _HCoreContext.HCCoreRoleFeature.Where(x => x.RoleId == _Details.Id && x.FeatureId == item).FirstOrDefault();
                                if (parentdetails != null)
                                {
                                    parentdetails.isView = true;
                                    parentdetails.isViewAll = true;
                                    updatefeatures.Add(parentdetails);
                                }
                                else
                                {
                                    HCCoreRoleFeature hCCoreRoleFeature = new HCCoreRoleFeature();
                                    hCCoreRoleFeature.Guid = HCoreHelper.GenerateGuid();
                                    hCCoreRoleFeature.isAdd = false;
                                    hCCoreRoleFeature.isUpdate = false;
                                    hCCoreRoleFeature.isDelete = false;
                                    hCCoreRoleFeature.isView = true;
                                    hCCoreRoleFeature.isViewAll = true;
                                    hCCoreRoleFeature.RoleId = _Details.Id;
                                    hCCoreRoleFeature.FeatureId = item;
                                    hCCoreRoleFeature.CreateDate = HCoreHelper.GetGMTDateTime();
                                    hCCoreRoleFeature.CreatedById = _Request.UserReference.AccountId;
                                    hCCoreRoleFeature.StatusId = StatusId;
                                    addfeatures.Add(hCCoreRoleFeature);
                                }
                            }
                            _HCoreContext.HCCoreRoleFeature.UpdateRange(updatefeatures);
                            _HCoreContext.HCCoreRoleFeature.AddRange(addfeatures);
                            _HCoreContext.SaveChanges();
                        }


                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1205, TUCCoreResource.CA1205M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateRole", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Saves the role feature.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveRoleFeature(OSystemAdministration.RoleFeature.Request _Request)
        {
            try
            {

                if (_Request.RoleId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1207, TUCCoreResource.CA1207M);
                }
                else if (string.IsNullOrEmpty(_Request.RoleKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1208, TUCCoreResource.CA1208M);
                }
                else
                {

                    if (_Request.ReferenceId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                    }
                    else if (string.IsNullOrEmpty(_Request.ReferenceKey))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        var _ValidateDetails = _HCoreContext.HCCoreParameter.Any(x => x.TypeId == HelperType.RoleFeatures
                        && x.ParentId == _Request.RoleId
                        && x.Parent.Guid == _Request.RoleKey
                        && x.CommonId == _Request.ReferenceId
                        && x.Common.Guid == _Request.ReferenceKey);
                        if (_ValidateDetails)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1209, TUCCoreResource.CA1209M);
                        }
                        _HCCoreParameter = new HCCoreParameter();
                        _HCCoreParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCCoreParameter.ParentId = _Request.RoleId;
                        _HCCoreParameter.TypeId = HelperType.RoleFeatures;
                        _HCCoreParameter.CommonId = _Request.ReferenceId;
                        _HCCoreParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCCoreParameter.CreatedById = _Request.UserReference.AccountId;
                        _HCCoreParameter.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.HCCoreParameter.Add(_HCCoreParameter);
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1210, TUCCoreResource.CA1210M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveRoleFeature", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Removes the role feature.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse RemoveRoleFeature(OSystemAdministration.RoleFeature.Request _Request)
        {
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                else
                {

                    using (_HCoreContext = new HCoreContext())
                    {
                        var _Details = _HCoreContext.HCCoreParameter.Where(x => x.TypeId == HelperType.RoleFeatures
                        && x.Id == _Request.ReferenceId
                        && x.Guid == _Request.ReferenceKey
                        && x.ParentId == _Request.RoleId
                        && x.Parent.Guid == _Request.RoleKey).FirstOrDefault();
                        if (_Details == null)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                        _HCoreContext.HCCoreParameter.Remove(_Details);
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1205, TUCCoreResource.CA1205M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("RemoveRoleFeature", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Deletes the role.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteRole(OSystemAdministration.Role.Request _Request)
        {
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var _Details = _HCoreContext.HCCoreRole.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (_Details == null)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                        bool RoleAccount = _HCoreContext.HCUAccountParameter.Any(x => x.ParameterId == _Request.ReferenceId);
                        if (RoleAccount)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1221, TUCCoreResource.CA1221M);
                        }

                        var RoleFeatures = _HCoreContext.HCCoreRoleFeature.Where(x => x.RoleId == _Details.Id).ToList();
                        if (RoleFeatures.Count > 0)
                        {
                            _HCoreContext.HCCoreRoleFeature.RemoveRange(RoleFeatures);
                            _HCoreContext.SaveChanges();
                            var _DeleteItem = _HCoreContext.HCCoreRole.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                            if (_DeleteItem != null)
                            {
                                _HCoreContext.HCCoreRole.Remove(_DeleteItem);
                                _HCoreContext.SaveChanges();
                            }
                        }
                        else
                        {
                            _HCoreContext.HCCoreRole.Remove(_Details);
                            _HCoreContext.SaveChanges();
                        }
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1222, TUCCoreResource.CA1222M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteRole", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Gets the feature list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetFeatureList(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", ">");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCCoreFeature
                                                .Where(x => x.TypeId == HelperType.FeatureCategory
                                                  && x.StatusId == HelperStatus.Default.Active
                                                  && x.HelperId == UserAccountType.Admin)
                                                .Select(x => new OSystemAdministration.Feature.FeatureList
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data                    

                    List<OSystemAdministration.Feature.FeatureList> _List = _HCoreContext.HCCoreFeature
                                                .Where(x => x.TypeId == HelperType.FeatureCategory
                                                  && x.StatusId == HelperStatus.Default.Active
                                                  && x.HelperId == UserAccountType.Admin
                                                  )
                                                .Select(y => new OSystemAdministration.Feature.FeatureList
                                                {

                                                    ReferenceId = y.Id,
                                                    ReferenceKey = y.Guid,
                                                    Name = y.Name,
                                                    isDelete = false,
                                                    isAdd = false,
                                                    isEdit = false,
                                                    isView = false,
                                                    isTab = y.isTab,
                                                    Child = (_HCoreContext.HCCoreFeature
                                                            .Where(x => x.StatusId == HelperStatus.Default.Active
                                                                    && x.ParentId == y.Id
                                                                 && x.ParentId != null)
                                                            .Select(x => new OSystemAdministration.Feature.FeatureList
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                Name = x.Name,
                                                                isDelete = false,
                                                                isAdd = false,
                                                                isEdit = false,
                                                                isView = false,
                                                                isTab = x.isTab,
                                                                Child = (_HCoreContext.HCCoreFeature
                                                      .Where(y => y.StatusId == HelperStatus.Default.Active
                                                                 && y.ParentId == x.Id
                                                                 && y.ParentId != null)
                                                                      .Select(y => new OSystemAdministration.Feature.FeatureList
                                                                      {
                                                                          ReferenceId = y.Id,
                                                                          ReferenceKey = y.Guid,
                                                                          Name = y.Name,
                                                                          isDelete = false,
                                                                          isAdd = false,
                                                                          isEdit = false,
                                                                          isView = false,
                                                                          isTab = y.isTab,
                                                                          Child = (_HCoreContext.HCCoreFeature
                                                                                    .Where(z => z.StatusId == HelperStatus.Default.Active
                                                                                    && z.ParentId == y.Id
                                                                                    && z.ParentId != null)
                                                                                         .Select(z => new OSystemAdministration.Feature.FeatureList
                                                                                         {
                                                                                             ReferenceId = z.Id,
                                                                                             ReferenceKey = z.Guid,
                                                                                             Name = z.Name,
                                                                                             isDelete = false,
                                                                                             isAdd = false,
                                                                                             isEdit = false,
                                                                                             isView = false,
                                                                                             isTab = z.isTab
                                                                                         }).ToList())
                                                                      }).ToList())
                                                            }).ToList())
                                                })
                                             .Where(_Request.SearchCondition)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }


            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetFeatureList", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        #endregion

        #region Otp Manager
        /// <summary>
        /// Description: Gets the otp request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetOtpRequest(OSystemAdministration.SystemVerification.Request _Request)
        {
            #region Declare

            #endregion
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OSystemAdministration.SystemVerification.Details Data = (from n in _HCoreContext.HCCoreVerification
                                                                             where n.Guid == _Request.ReferenceKey
                                                                             select new OSystemAdministration.SystemVerification.Details
                                                                             {
                                                                                 ReferenceId = n.Id,
                                                                                 ReferenceKey = n.Guid,
                                                                                 //TypeCode = n.VerificationType.SystemName,
                                                                                 //TypeName = n.VerificationType.Name,
                                                                                 //CountryIsd = n.CountryIsd,
                                                                                 MobileNumber = n.MobileNumber,
                                                                                 //EmailAddress = n.EmailAddress,k
                                                                                 AccessKey = n.AccessKey,
                                                                                 AccessCode = n.AccessCode,
                                                                                 AccessCodeStart = n.AccessCodeStart,
                                                                                 //EmailMessage = n.EmailMessage,
                                                                                 //MobileMessage = n.MobileMessage,
                                                                                 ExpiaryDate = n.ExpiaryDate,
                                                                                 VerifyDate = n.VerifyDate,
                                                                                 RequestIpAddress = n.RequestIpAddress,
                                                                                 RequestLatitude = n.RequestLatitude,
                                                                                 RequestLongitude = n.RequestLongitude,
                                                                                 RequestLocation = n.RequestLocation,
                                                                                 VerifyAttemptCount = n.VerifyAttemptCount,
                                                                                 VerifyIpAddress = n.VerifyIpAddress,
                                                                                 VerifyLatitude = n.VerifyLatitude,
                                                                                 VerifyLongitude = n.VerifyLongitude,
                                                                                 VerifyAddress = n.VerifyAddress,
                                                                                 ExternalReferenceKey = n.ReferenceKey,
                                                                                 RequestDate = n.CreateDate,
                                                                                 //Status = n.StatusId,
                                                                                 StatusCode = n.Status.SystemName,
                                                                                 StatusName = n.Status.Name
                                                                             }).FirstOrDefault();
                    if (Data != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, Data, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetOtpRequest", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the otp request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetOtpRequest(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "RequestDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from n in _HCoreContext.HCCoreVerification
                                                 where n.CountryIsd == _Request.UserReference.CountryIsd
                                                 select new OSystemAdministration.SystemVerification.List
                                                 {
                                                     ReferenceId = n.Id,
                                                     ReferenceKey = n.Guid,
                                                     MobileNumber = n.MobileNumber,
                                                     RequestIpAddress = n.RequestIpAddress,
                                                     RequestDate = n.CreateDate,
                                                     VerifyDate = n.VerifyDate,
                                                     VerifyAttemptCount = n.VerifyAttemptCount,
                                                     VerifyIpAddress = n.VerifyIpAddress,
                                                     StatusCode = n.Status.SystemName,
                                                     StatusName = n.Status.Name
                                                 })
                                       .Where(_Request.SearchCondition)
                               .Count();
                    }
                    #endregion
                    #region Get Data
                    var Data = (from n in _HCoreContext.HCCoreVerification
                                where n.CountryIsd == _Request.UserReference.CountryIsd
                                select new OSystemAdministration.SystemVerification.List
                                {
                                    ReferenceId = n.Id,
                                    ReferenceKey = n.Guid,
                                    MobileNumber = n.MobileNumber,
                                    RequestIpAddress = n.RequestIpAddress,
                                    RequestDate = n.CreateDate,
                                    VerifyDate = n.VerifyDate,
                                    VerifyAttemptCount = n.VerifyAttemptCount,
                                    VerifyIpAddress = n.VerifyIpAddress,
                                    StatusCode = n.Status.SystemName,
                                    StatusName = n.Status.Name
                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                return HCoreHelper.LogException("GetOtpRequest", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        #endregion

        #region Slider Manager
        /// <summary>
        /// Description: Saves the slider image.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveSliderImage(OSystemAdministration.AppSlider.Request _Request)
        {
            try
            {
                //if (string.IsNullOrEmpty(_Request.Name))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1229, TUCCoreResource.CA1229M);
                //}
                //else 
                if (_Request.IconContent == null || string.IsNullOrEmpty(_Request.IconContent.Content))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1457, TUCCoreResource.CA1457M);
                }
                else if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                else
                {
                    int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        //bool _DetailCheck = _HCoreContext.HCCoreCommon.Any(x => x.TypeId == HelperType.MerchantCategory && x.Name == _Request.Name);
                        //if (_DetailCheck)
                        //{
                        //    _HCoreContext.Dispose();
                        //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1203, TUCCoreResource.CA1203M);
                        //}
                        _HCCoreParameter = new HCCoreParameter();
                        _HCCoreParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCCoreParameter.TypeId = HelperType.SliderImage;
                        _HCCoreParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCCoreParameter.CreatedById = _Request.UserReference.AccountId;
                        _HCCoreParameter.StatusId = StatusId;
                        _HCoreContext.HCCoreParameter.Add(_HCCoreParameter);
                        _HCoreContext.SaveChanges();
                        long? IconStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                        }
                        if (IconStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var UserAccDetails = _HCoreContext.HCCoreParameter.Where(x => x.Id == _HCCoreParameter.Id).FirstOrDefault();
                                if (UserAccDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        UserAccDetails.IconStorageId = IconStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        var _Response = new
                        {
                            ReferenceId = _HCCoreParameter.Id,
                            ReferenceKey = _HCCoreParameter.Guid,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, TUCCoreResource.CA1234, TUCCoreResource.CA1234M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveSliderImage", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Updates the slider image status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateSliderImageStatus(OSystemAdministration.AppSlider.Request _Request)
        {
            try
            {
                //if (string.IsNullOrEmpty(_Request.Name))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1229, TUCCoreResource.CA1229M);
                //}
                //else 
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                else
                {
                    int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        var _DetailCheck = _HCoreContext.HCCoreParameter.Where(x => x.TypeId == HelperType.SliderImage && x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (_DetailCheck == null)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                        _DetailCheck.StatusId = StatusId;
                        _DetailCheck.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _DetailCheck.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1234, TUCCoreResource.CA1234M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveSliderImage", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Gets the slider image.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSliderImage(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCCoreParameter
                                                .Where(x => x.TypeId == HelperType.SliderImage)
                                                .Select(x => new OSystemAdministration.AppSlider.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    IconUrl = _AppConfig.StorageUrl + x.IconStorage.Path,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OSystemAdministration.AppSlider.List> _List = _HCoreContext.HCCoreParameter
                                                .Where(x => x.TypeId == HelperType.SliderImage)
                                                .Select(x => new OSystemAdministration.AppSlider.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    IconUrl = _AppConfig.StorageUrl + x.IconStorage.Path,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    //foreach (var DataItem in _List)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //    {
                    //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.IconUrl = _AppConfig.Default_Icon;
                    //    }
                    //}
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetSliderImage", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the slider image.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteSliderImage(OSystemAdministration.AppSlider.Request _Request)
        {
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var _Details = _HCoreContext.HCCoreParameter.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.TypeId == HelperType.SliderImage).FirstOrDefault();
                        if (_Details == null)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                        HCoreHelper.DeleteStorage(_Details.IconStorageId, _Request.UserReference);
                        _HCoreContext.HCCoreParameter.Remove(_Details);
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1233, TUCCoreResource.CA1233M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteSliderImage", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        #endregion

        #region Sms Notifications
        /// <summary>
        /// Description: Gets the SMS notification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSmsNotification(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContextLogging.HCLSmsNotification
                            .Select(x => new OSystemAdministration.SmsNotification.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                SourceName = x.Source,
                                MobileNumber = x.MobileNumber,
                                Message = x.Message,
                                ReferenceNumber = x.ReferenceNumber,
                                StatusCode = x.StatusCode,
                                StatusName = x.ReferenceGroupName,
                                SendDate = x.SendDate,
                                DeliveryDate = x.DeliveryDate,
                                Amount = x.Amount,
                                CreateDate = x.CreateDate,
                            })
                            .Where(_Request.SearchCondition)
                            .Count();
                    }

                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = _Request.TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OSystemAdministration.SmsNotification.List> Data = _HCoreContextLogging.HCLSmsNotification
                            .Select(x => new OSystemAdministration.SmsNotification.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                SourceName = x.Source,
                                MobileNumber = x.MobileNumber,
                                Message = x.Message,
                                ReferenceNumber = x.ReferenceNumber,
                                StatusCode = x.StatusCode,
                                StatusName = x.ReferenceGroupName,
                                SendDate = x.SendDate,
                                DeliveryDate = x.DeliveryDate,
                                Amount = x.Amount,
                                CreateDate = x.CreateDate,
                            })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    //foreach (var Details in Data)
                    //{
                    //    if (!string.IsNullOrEmpty(Details.Description))
                    //    {
                    //        if (Details.Description.Length > 60)
                    //        {
                    //            Details.Description.Substring(0, 59);
                    //        }
                    //    }
                    //}
                    _HCoreContextLogging.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetSmsNotification", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        #endregion

    }
}
