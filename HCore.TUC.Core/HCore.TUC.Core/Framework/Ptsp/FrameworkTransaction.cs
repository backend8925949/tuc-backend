//==================================================================================
// FileName: FrameworkTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to transactions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Reflection;
using Akka.Actor;
using ClosedXML.Excel;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Store;
using HCore.Helper;
using HCore.TUC.Core.Object.Ptsp;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.Framework.Ptsp
{
    internal class ActorCommissionHistoryDownloader : ReceiveActor
    {
        public ActorCommissionHistoryDownloader()
        {
            Receive<OList.Request>(_Request =>
            {
                FrameworkTransaction _FrameworkTransactions = new FrameworkTransaction();
                _FrameworkTransactions.GetCommissionHistoryDownload(_Request);
            });
        }
    }
 
    public class FrameworkTransaction
    {
        HCoreContext _HCoreContext;
        /// <summary>
        /// Description: Formats the transactions.
        /// </summary>
        /// <param name="_Sales">The sales.</param>
        /// <returns>List&lt;OTransaction.Sale&gt;.</returns>
        private List<OTransaction.Sale>  FormatTransactions( List<OTransaction.Sale> _Sales)
        {
            #region Create  Response Object
            foreach (var DataItem in _Sales)
            {
                DataItem.TypeName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.TypeId).Select(q => q.Name).FirstOrDefault();
                DataItem.CardBankName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBankId).Select(q => q.Name).FirstOrDefault();
                DataItem.CardBrandName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBrandId).Select(q => q.Name).FirstOrDefault();
                DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.StatusId).Select(q => q.Name).FirstOrDefault();
                var ParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ParentId);
                if (ParentDetails != null)
                {
                    DataItem.ParentKey = ParentDetails.ReferenceKey;
                    DataItem.ParentDisplayName = ParentDetails.DisplayName;
                    DataItem.ParentIconUrl = ParentDetails.IconUrl;
                }
                var SubParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.StoreReferenceId);
                if (SubParentDetails != null)
                {
                    DataItem.StoreReferenceKey = SubParentDetails.ReferenceKey;
                    DataItem.StoreDisplayName = SubParentDetails.DisplayName;
                }
                var AcquirerDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.AcquirerId);
                if (AcquirerDetails != null)
                {
                    DataItem.AcquirerKey = AcquirerDetails.ReferenceKey;
                    DataItem.AcquirerDisplayName = AcquirerDetails.DisplayName;
                    DataItem.AcquirerIconUrl = AcquirerDetails.IconUrl;
                }
                var ProviderDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ProviderId);
                if (ProviderDetails != null)
                {
                    DataItem.ProviderKey = ProviderDetails.ReferenceKey;
                    DataItem.ProviderDisplayName = ProviderDetails.DisplayName;
                    DataItem.ProviderIconUrl = AcquirerDetails.IconUrl;
                }
                var CashierDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CashierId);
                if (CashierDetails != null)
                {
                    DataItem.CashierKey = CashierDetails.ReferenceKey;
                    DataItem.CashierCode = CashierDetails.DisplayName;
                    DataItem.CashierDisplayName = CashierDetails.Name;
                    DataItem.CashierMobileNumber = CashierDetails.MobileNumber;
                }
                var CreatedByDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CreatedById);
                if (CreatedByDetails != null)
                {
                    DataItem.CreatedByKey = CreatedByDetails.ReferenceKey;
                    DataItem.CreatedByDisplayName = CreatedByDetails.DisplayName;
                    DataItem.CreatedByAccountTypeCode = CreatedByDetails.AccountTypeCode;
                }

                if (!string.IsNullOrEmpty(DataItem.TerminalId))
                {
                    var TerminalDetails = HCoreDataStore.Terminals.FirstOrDefault(q => q.DisplayName == DataItem.TerminalId);
                    if (TerminalDetails != null)
                    {
                        DataItem.TerminalId = TerminalDetails.DisplayName;
                        DataItem.TerminalReferenceId = TerminalDetails.ReferenceId;
                        DataItem.TerminalReferenceKey = TerminalDetails.ReferenceKey;
                    }
                }


                if (!string.IsNullOrEmpty(DataItem.TypeName))
                {
                    DataItem.TypeName = DataItem.TypeName.Replace("Reward", "").Replace("Rewards", "");
                }
            }
            return _Sales;
            #endregion
        }

        /// <summary>
        /// Description: Formats the transactions.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <param name="_Sales">The sales.</param>
        /// <returns>OList.Response.</returns>
        private OList.Response FormatTransactions(OList.Request _Request, List<OTransaction.Sale> _Sales)
        {
            #region Create  Response Object
            foreach (var DataItem in _Sales)
            {
                DataItem.TypeName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.TypeId).Select(q => q.Name).FirstOrDefault();
                DataItem.CardBankName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBankId).Select(q => q.Name).FirstOrDefault();
                DataItem.CardBrandName = HCoreDataStore.SystemBinParameters.Where(q => q.ReferenceId == DataItem.CardBrandId).Select(q => q.Name).FirstOrDefault();
                DataItem.StatusName = HCoreDataStore.SystemHelpers.Where(q => q.ReferenceId == DataItem.StatusId).Select(q => q.Name).FirstOrDefault();
                var ParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ParentId);
                if (ParentDetails != null)
                {
                    DataItem.ParentKey = ParentDetails.ReferenceKey;
                    DataItem.ParentDisplayName = ParentDetails.DisplayName;
                    DataItem.ParentIconUrl = ParentDetails.IconUrl;
                }
                var SubParentDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.StoreReferenceId);
                if (SubParentDetails != null)
                {
                    DataItem.StoreReferenceKey = SubParentDetails.ReferenceKey;
                    DataItem.StoreDisplayName = SubParentDetails.DisplayName;
                }
                var AcquirerDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.AcquirerId);
                if (AcquirerDetails != null)
                {
                    DataItem.AcquirerKey = AcquirerDetails.ReferenceKey;
                    DataItem.AcquirerDisplayName = AcquirerDetails.DisplayName;
                    DataItem.AcquirerIconUrl = AcquirerDetails.IconUrl;
                }
                var ProviderDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.ProviderId);
                if (ProviderDetails != null)
                {
                    DataItem.ProviderKey = ProviderDetails.ReferenceKey;
                    DataItem.ProviderDisplayName = ProviderDetails.DisplayName;
                    DataItem.ProviderIconUrl = AcquirerDetails.IconUrl;
                }
                var CashierDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CashierId);
                if (CashierDetails != null)
                {
                    DataItem.CashierKey = CashierDetails.ReferenceKey;
                    DataItem.CashierCode = CashierDetails.DisplayName;
                    DataItem.CashierDisplayName = CashierDetails.Name;
                    DataItem.CashierMobileNumber = CashierDetails.MobileNumber;
                }
                var CreatedByDetails = HCoreDataStore.Accounts.FirstOrDefault(q => q.ReferenceId == DataItem.CreatedById);
                if (CreatedByDetails != null)
                {
                    DataItem.CreatedByKey = CreatedByDetails.ReferenceKey;
                    DataItem.CreatedByDisplayName = CreatedByDetails.DisplayName;
                    DataItem.CreatedByAccountTypeCode = CreatedByDetails.AccountTypeCode;
                }

                if (!string.IsNullOrEmpty(DataItem.TerminalId))
                {
                    var TerminalDetails = HCoreDataStore.Terminals.FirstOrDefault(q => q.DisplayName == DataItem.TerminalId);
                    if (TerminalDetails != null)
                    {
                        DataItem.TerminalId = TerminalDetails.DisplayName;
                        DataItem.TerminalReferenceId = TerminalDetails.ReferenceId;
                        DataItem.TerminalReferenceKey = TerminalDetails.ReferenceKey;
                    }
                }


                if (!string.IsNullOrEmpty(DataItem.TypeName))
                {
                    DataItem.TypeName = DataItem.TypeName.Replace("Reward", "").Replace("Rewards", "");
                }
            }
            return HCoreHelper.GetListResponse(_Request.TotalRecords, _Sales, _Request.Offset, _Request.Limit);
            #endregion
        }
        List<OTransaction.SaleDownload> _SaleDownload;
        List<OTransaction.Sale> _Sales;
        HCUAccountParameter _HCUAccountParameter;
        /// <summary>
        /// Description: Gets the commission history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCommissionHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime MinDate = new DateTime(2021, 7, 1, 0, 0, 0).AddHours(-1).AddSeconds(-1);
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorCommissionHistoryDownloader");
                    var _ActorNotify = _Actor.ActorOf<ActorCommissionHistoryDownloader>("ActorCommissionHistoryDownloader");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => (x.Account.AccountTypeId == UserAccountType.PosAccount || x.Account.AccountTypeId == UserAccountType.PgAccount)
                                                && x.AccountId == _Request.AccountId
                                                && x.Account.Guid == _Request.AccountKey
                                                && x.TotalAmount > 0
                                                && x.TransactionDate > MinDate
                                                && x.SourceId == TransactionSource.Settlement
                                                && x.ModeId ==  TransactionMode.Credit)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CommissionAmount = x.TotalAmount,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    ParentId = x.ParentId,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    _Sales = _HCoreContext.HCUAccountTransaction
                                                .Where(x => (x.Account.AccountTypeId == UserAccountType.PosAccount || x.Account.AccountTypeId == UserAccountType.PgAccount)
                                                && x.AccountId == _Request.AccountId
                                                && x.Account.Guid == _Request.AccountKey
                                                && x.TotalAmount > 0
                                                && x.TransactionDate > MinDate
                                                && x.SourceId == TransactionSource.Settlement
                                                && x.ModeId == TransactionMode.Credit)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CommissionAmount = x.TotalAmount,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ParentId = x.ParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusId = x.StatusId,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                    #endregion
                    #region Send Response
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FormatTransactions(_Request, _Sales), "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCommissionHistory", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the commission history download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetCommissionHistoryDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                DateTime MinDate = new DateTime(2021, 7, 1, 0, 0, 0).AddHours(-1).AddSeconds(-1);

                _SaleDownload = new List<OTransaction.SaleDownload>();
                _Sales = new List<OTransaction.Sale>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                long StorageId = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HCoreConstant.HelperType.Downloads;
                    _HCUAccountParameter.Name = "Sales_Sheet";
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    _HCUAccountParameter.AccountId = _Request.UserReference.AccountId;
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Inactive;
                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                    _HCoreContext.SaveChanges();
                    StorageId = _HCUAccountParameter.Id;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => (x.Account.AccountTypeId == UserAccountType.PosAccount || x.Account.AccountTypeId == UserAccountType.PgAccount)
                                                && x.AccountId == _Request.AccountId
                                                && x.Account.Guid == _Request.AccountKey
                                                && x.TotalAmount > 0
                                                && x.TransactionDate > MinDate
                                                && x.SourceId == TransactionSource.Settlement
                                                && x.ModeId == TransactionMode.Credit)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CommissionAmount = x.ComissionAmount,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ParentId = x.ParentId,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                })
                                                .Count(_Request.SearchCondition);
                        #endregion
                    }
                    #region Get Data
                    double Iterations = Math.Round((double)_Request.TotalRecords / 1000, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        _Sales.AddRange(_HCoreContext.HCUAccountTransaction
                                                .Where(x => (x.Account.AccountTypeId == UserAccountType.PosAccount || x.Account.AccountTypeId == UserAccountType.PgAccount)
                                                && x.AccountId == _Request.AccountId
                                                && x.Account.Guid == _Request.AccountKey
                                                && x.TransactionDate > MinDate
                                                && x.TotalAmount > 0
                                                && x.SourceId == TransactionSource.Settlement
                                                && x.ModeId == TransactionMode.Credit)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CommissionAmount = x.ComissionAmount,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    TypeId = x.TypeId,
                                                    ParentId = x.ParentId,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(1000)
                                                     .ToList());
                        _Request.Offset += 1000;
                    }
                    #endregion
                    _Sales = FormatTransactions(   _Sales);
                    foreach (var SalesItem in _Sales)
                    {
                        _SaleDownload.Add(new OTransaction.SaleDownload
                        {
                            ReferenceId = SalesItem.ReferenceId,
                            TransactionDate = HCoreHelper.GetGMTToNigeria(SalesItem.TransactionDate),
                            User = SalesItem.UserDisplayName,
                            MobileNumber = SalesItem.UserMobileNumber,
                            TypeName = SalesItem.TypeName,
                            InvoiceAmount = SalesItem.InvoiceAmount,
                            CardNumber = SalesItem.AccountNumber,
                            CardBrand = SalesItem.CardBrandName,
                            CardBank = SalesItem.CardBankName,
                            Merchant = SalesItem.ParentDisplayName,
                            Store = SalesItem.StoreDisplayName,
                            CashierId = SalesItem.CashierCode,
                            CashierDisplayName = SalesItem.CashierDisplayName,
                            Bank = SalesItem.AcquirerDisplayName,
                            Provider = SalesItem.ProviderDisplayName,
                            Issuer = SalesItem.CreatedByDisplayName,
                            TerminalId = SalesItem.TerminalId,
                            Status = SalesItem.StatusName,
                        });
                    }
                    using (var _XLWorkbook = new XLWorkbook())
                    {
                        var _WorkSheet = _XLWorkbook.Worksheets.Add("Commission_History");
                        PropertyInfo[] properties = _SaleDownload.First().GetType().GetProperties();
                        List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                        for (int i = 0; i < headerNames.Count; i++)
                        {
                            _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                        }
                        _WorkSheet.Cell(2, 1).InsertData(_SaleDownload);
                        MemoryStream _MemoryStream = new MemoryStream();
                        _XLWorkbook.SaveAs(_MemoryStream);
                        long? _StorageId = HCoreHelper.SaveStorage("Commission_History", "xlsx", _MemoryStream, _Request.UserReference);
                        if (_StorageId != null && _StorageId != 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var TItem = _HCoreContext.HCUAccountParameter.Where(x => x.Id == StorageId).FirstOrDefault();
                                if (TItem != null)
                                {
                                    TItem.IconStorageId = _StorageId;
                                    TItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    TItem.ModifyById = _Request.UserReference.AccountId;
                                    TItem.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }
                    #region Send Response
                    _HCoreContext.Dispose();
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCommissionHistoryDownload", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }
        
        OTransactionOverview _OTransactionOverview;
        /// <summary>
        /// Description: Gets the commission history overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCommissionHistoryOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime MinDate = new DateTime(2021, 7, 1, 0, 0, 0).AddHours(-1).AddSeconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    }
                    _OTransactionOverview = new OTransactionOverview();
                    _OTransactionOverview.Transactions = _HCoreContext.HCUAccountTransaction
                                                .Where(x => (x.Account.AccountTypeId == UserAccountType.PosAccount || x.Account.AccountTypeId == UserAccountType.PgAccount)
                                                && x.AccountId == _Request.AccountId
                                                && x.Account.Guid == _Request.AccountKey
                                                && x.TotalAmount > 0
                                                && x.TransactionDate > MinDate
                                                && x.SourceId == TransactionSource.Settlement
                                                && x.ModeId == TransactionMode.Credit)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CommissionAmount = x.TotalAmount,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    ParentId = x.ParentId,
                                                    TypeId = x.TypeId,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();
                    _OTransactionOverview.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => (x.Account.AccountTypeId == UserAccountType.PosAccount || x.Account.AccountTypeId == UserAccountType.PgAccount)
                                                && x.AccountId == _Request.AccountId
                                                && x.Account.Guid == _Request.AccountKey
                                                && x.TotalAmount > 0
                                                && x.TransactionDate > MinDate
                                                && x.SourceId == TransactionSource.Settlement
                                                && x.ModeId == TransactionMode.Credit)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CommissionAmount = x.TotalAmount,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    ParentId = x.ParentId,
                                                    TypeId = x.TypeId,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.InvoiceAmount);
                    _OTransactionOverview.CommissionAmount = _HCoreContext.HCUAccountTransaction
                                                .Where(x => (x.Account.AccountTypeId == UserAccountType.PosAccount || x.Account.AccountTypeId == UserAccountType.PgAccount)
                                                && x.AccountId == _Request.AccountId
                                                && x.Account.Guid == _Request.AccountKey
                                                && x.TotalAmount > 0
                                                && x.TransactionDate > MinDate
                                                && x.SourceId == TransactionSource.Settlement
                                                && x.ModeId == TransactionMode.Credit)
                                                .Select(x => new OTransaction.Sale
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CommissionAmount = x.TotalAmount,
                                                    InvoiceAmount = x.PurchaseAmount,
                                                    TransactionDate = x.TransactionDate,
                                                    ParentId = x.ParentId,
                                                    TypeId = x.TypeId,
                                                    CardBankId = x.CardBankId,
                                                    CardBrandId = x.CardBrandId,
                                                    AccountNumber = x.AccountNumber,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    UserAccountId = x.AccountId,
                                                    UserAccountKey = x.Account.Guid,
                                                    UserDisplayName = x.Account.DisplayName,
                                                    UserMobileNumber = x.Account.MobileNumber,
                                                    StoreReferenceId = x.SubParentId,
                                                    ProviderId = x.ProviderId,
                                                    AcquirerId = x.BankId,
                                                    CreatedById = x.CreatedById,
                                                    CashierId = x.CashierId,
                                                    TerminalId = x.Terminal.IdentificationNumber,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Sum(x => x.CommissionAmount);
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OTransactionOverview, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCommissionHistoryOverview", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

    }
}
