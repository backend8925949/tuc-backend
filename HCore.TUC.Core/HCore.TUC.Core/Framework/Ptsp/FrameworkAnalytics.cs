//==================================================================================
// FileName: FrameworkAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to analystics
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Ptsp;
namespace HCore.TUC.Core.Framework.Ptsp
{
    internal class FrameworkAnalytics
    {
        HCoreContext _HCoreContext;
        OAnalytics.Counts _Counts;
        List<OAnalytics.DateRange> _DateRanges;
        OAnalytics.DateRangeResponse _DateRangeResponse;
        OAnalytics.TerminalStatus _TerminalStatus;
        /// <summary>
        /// Description: Gets the terminal activity history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTerminalActivityHistory(OAnalytics.Request _Request)
        {
            _DateRangeResponse = new OAnalytics.DateRangeResponse();
            _DateRanges = new List<OAnalytics.DateRange>();
            try
            {
                int Days = (_Request.EndTime - _Request.StartTime).Days;
                DateTime StartTime = _Request.StartTime;
                DateTime EndTime = StartTime.AddDays(1);

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.SubAccountId > 0)
                    {
                        long AccountTypeId = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.SubAccountId).Select(x => x.AccountTypeId).FirstOrDefault();
                        if (AccountTypeId == UserAccountType.Merchant)
                        {
                            for (int i = 0; i < Days; i++)
                            {
                                DateTime TDayStart = StartTime;
                                DateTime TDayEnd = EndTime.AddHours(24).AddSeconds(-1);
                                DateTime T7DayStart = TDayStart.Date.AddDays(-7);
                                DateTime T7DayEnd = TDayStart.Date.AddSeconds(-1);
                                DateTime TDeadDayEnd = TDayStart.AddDays(-7);
                                _TerminalStatus = new OAnalytics.TerminalStatus();
                                _TerminalStatus.Active = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.SubAccountId && x.ProviderId == _Request.AccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd));
                                _TerminalStatus.Idle = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.SubAccountId && x.ProviderId == _Request.AccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd));
                                _TerminalStatus.Dead = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.SubAccountId && x.ProviderId == _Request.AccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate < TDeadDayEnd));
                                //_TerminalStatus.Active = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.SubAccountId && x.ProviderId == _Request.AccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > StartTime && a.TransactionDate < EndTime));
                                //_TerminalStatus.Idle = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.SubAccountId && x.ProviderId == _Request.AccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > StartTime && a.TransactionDate < EndTime));
                                //_TerminalStatus.Dead = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.SubAccountId && x.ProviderId == _Request.AccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate < EndTime));
                                _TerminalStatus.Inactive = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.SubAccountId && x.ProviderId == _Request.AccountId && !x.HCUAccountTransaction.Any());
                                _DateRanges.Add(new OAnalytics.DateRange
                                {
                                    Data = _TerminalStatus,
                                    Date = StartTime,
                                    StartDate = StartTime,
                                    EndDate = EndTime,
                                });
                                StartTime = StartTime.AddDays(1);
                                EndTime = EndTime.AddDays(1);
                            }
                        }
                        else if (AccountTypeId == UserAccountType.MerchantStore)
                        {
                            for (int i = 0; i < Days; i++)
                            {
                                DateTime TDayStart = StartTime;
                                DateTime TDayEnd = EndTime.AddHours(24).AddSeconds(-1);
                                DateTime T7DayStart = TDayStart.Date.AddDays(-7);
                                DateTime T7DayEnd = TDayStart.Date.AddSeconds(-1);
                                DateTime TDeadDayEnd = TDayStart.AddDays(-7);
                                _TerminalStatus = new OAnalytics.TerminalStatus();
                                _TerminalStatus.Active = _HCoreContext.TUCTerminal.Count(x => x.StoreId == _Request.SubAccountId && x.ProviderId == _Request.AccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd));
                                _TerminalStatus.Idle = _HCoreContext.TUCTerminal.Count(x => x.StoreId == _Request.SubAccountId && x.ProviderId == _Request.AccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd));
                                _TerminalStatus.Dead = _HCoreContext.TUCTerminal.Count(x => x.StoreId == _Request.SubAccountId && x.ProviderId == _Request.AccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate < TDeadDayEnd));
                                _TerminalStatus.Inactive = _HCoreContext.TUCTerminal.Count(x => x.StoreId == _Request.SubAccountId && x.ProviderId == _Request.AccountId && !x.HCUAccountTransaction.Any());
                                _DateRanges.Add(new OAnalytics.DateRange
                                {
                                    Data = _TerminalStatus,
                                    Date = StartTime,
                                    StartDate = StartTime,
                                    EndDate = EndTime,
                                });
                                StartTime = StartTime.AddDays(1);
                                EndTime = EndTime.AddDays(1);
                            }
                        }

                    }
                    else
                    {
                        for (int i = 0; i < Days; i++)
                        {
                            DateTime TDayStart = StartTime;
                            DateTime TDayEnd = EndTime.AddHours(24).AddSeconds(-1);
                            DateTime T7DayStart = TDayStart.Date.AddDays(-7);
                            DateTime T7DayEnd = TDayStart.Date.AddSeconds(-1);
                            DateTime TDeadDayEnd = TDayStart.AddDays(-7);
                            _TerminalStatus = new OAnalytics.TerminalStatus();
                            _TerminalStatus.Active = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == _Request.AccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd));
                            _TerminalStatus.Idle = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == _Request.AccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd));
                            _TerminalStatus.Dead = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == _Request.AccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate < TDeadDayEnd));
                            _TerminalStatus.Inactive = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == _Request.AccountId && !x.HCUAccountTransaction.Any());
                            _DateRanges.Add(new OAnalytics.DateRange
                            {
                                Data = _TerminalStatus,
                                Date = StartTime,
                                StartDate = StartTime,
                                EndDate = EndTime,
                            });
                            StartTime = StartTime.AddDays(1);
                            EndTime = EndTime.AddDays(1);
                        }
                    }
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DateRanges, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetTerminalActivityHistory", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

        /// <summary>
        /// Description: Gets the account overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountOverview(OAnalytics.Request _Request)
        {
            _Counts = new OAnalytics.Counts();
            _Counts.TotalMerchants = 0;
            _Counts.ActiveMerchants = 0;
            _Counts.TotalStores = 0;
            _Counts.ActiveStores = 0;
            _Counts.TotalTerminals = 0;
            _Counts.ActiveTerminals = 0;
            _Counts.ActivePosUpgradeRequest = 0;
            _Counts.TotalTransactions = 0;
            _Counts.TotalSale = 0;
            _Counts.AverageTransactions = 0;
            _Counts.AverageTransactionAmount = 0;
            _Counts.CardTransactions = 0;
            _Counts.CardTransactionsAmount = 0;
            _Counts.CashTransactions = 0;
            _Counts.CashTransactionAmount = 0;
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long SubUserAccountId = 0;
                    long SubUserAccountTypeId = 0;
                    if (!string.IsNullOrEmpty(_Request.SubAccountKey) && _Request.SubAccountId > 0)
                    {
                        var SubUserAccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.SubAccountKey && x.Id == _Request.SubAccountId)
                         .Select(x => new
                         {
                             UserAccountId = x.Id,
                             AccountTypeId = x.AccountTypeId,
                         }).FirstOrDefault();
                        var SubAccountDetails = _HCoreContext.TUCTerminal.Where(x => x.Guid == _Request.SubAccountKey && x.Id == _Request.SubAccountId)
                         .Select(x => new
                         {
                             UserAccountId = x.Id,
                         }).FirstOrDefault();
                        if (SubUserAccountDetails != null)
                        {
                            SubUserAccountId = SubUserAccountDetails.UserAccountId;
                            SubUserAccountTypeId = SubUserAccountDetails.AccountTypeId;
                        }
                        else if (SubAccountDetails != null)
                        {
                            SubUserAccountId = SubAccountDetails.UserAccountId;
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1086");
                            #endregion
                        }
                    }
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey && x.AccountTypeId == UserAccountType.PosAccount)
                       .Select(x => new
                       {
                           UserAccountId = x.Id,
                           AccountTypeId = x.AccountTypeId,
                           OwnerId = x.OwnerId,
                       }).FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        DateTime TDayStart = HCoreHelper.GetGMTDate();
                        DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                        DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                        DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                        DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);

                        if (SubUserAccountTypeId == UserAccountType.Merchant)
                        {
                            #region  Overview
                            _Counts.TotalStores = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                    && x.OwnerId == _Request.SubAccountId
                                                    && (x.Owner.OwnerId == _Request.AccountId
                                                    || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == x.Id && m.ProviderId == _Request.AccountId))).Select(x => x.Id).Distinct().Count();

                            _Counts.ActiveStores = _HCoreContext.HCUAccount
                                                  .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                    && x.OwnerId == _Request.SubAccountId
                                                   && x.HCUAccountTransactionSubParent.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd && a.ProviderId == _Request.AccountId)
                                                  && (x.Owner.OwnerId == _Request.AccountId
                                                  || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == x.Id && m.ProviderId == _Request.AccountId))).Select(x => x.Id).Distinct().Count();

                            _Counts.TotalTerminals = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.SubAccountId && x.ProviderId == AccountDetails.UserAccountId);
                            _Counts.ActiveTerminals = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.SubAccountId && x.ProviderId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd));
                            _TerminalStatus = new OAnalytics.TerminalStatus();
                            _TerminalStatus.Total = _Counts.TotalTerminals;
                            _TerminalStatus.Active = _Counts.ActiveTerminals;
                            _TerminalStatus.Idle = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.SubAccountId && x.ProviderId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd));
                            _TerminalStatus.Dead = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.SubAccountId && x.ProviderId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate < TDeadDayEnd));
                            _TerminalStatus.Inactive = _HCoreContext.TUCTerminal.Count(x => x.MerchantId == _Request.SubAccountId && x.ProviderId == AccountDetails.UserAccountId && !x.HCUAccountTransaction.Any());
                            _Counts.TerminalStatus = _TerminalStatus;



                            _Counts.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ProviderId == _Request.AccountId
                                                            && m.ParentId == _Request.SubAccountId
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                           && m.ModeId == TransactionMode.Credit
                                                           && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                           || m.SourceId == TransactionSource.ThankUCashPlus)
                                                       ).Count();

                            _Counts.TotalSale = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.ProviderId == _Request.AccountId
                                                            && m.ParentId == _Request.SubAccountId
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                          && m.ModeId == TransactionMode.Credit
                                                          && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || m.SourceId == TransactionSource.ThankUCashPlus)
                                                      ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            if (_Counts.ActiveTerminals > 0)
                            {
                                _Counts.AverageTransactions = _Counts.TotalTransactions / _Counts.ActiveTerminals;
                            }
                            else
                            {
                                _Counts.AverageTransactions = 0;
                            }
                            if (_Counts.ActiveTerminals > 0)
                            {
                                _Counts.AverageTransactionAmount = _Counts.TotalSale / _Counts.ActiveTerminals;
                            }
                            else
                            {
                                _Counts.AverageTransactionAmount = 0;
                            }
                            _Counts.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ProviderId == _Request.AccountId
                                                                    && m.ParentId == _Request.SubAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && m.TypeId == TransactionType.CardReward
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                            _Counts.CardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ProviderId == _Request.AccountId
                                                                    && m.ParentId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            _Counts.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ProviderId == _Request.AccountId
                                                                    && m.ParentId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Count();
                            _Counts.CashTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ProviderId == _Request.AccountId
                                                                    && m.ParentId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            #endregion
                            _Counts.CardTypeSale = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                       .Select(x => new OAnalytics.CardTypeSale
                                       {
                                           ReferenceId = x.Id,
                                           Name = x.Name,
                                           Transactions = 0,
                                           Amount = 0
                                       }).ToList();

                            foreach (var CardBrand in _Counts.CardTypeSale)
                            {

                                CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.ProviderId == _Request.AccountId
                                                          && m.ParentId == _Request.SubAccountId
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && m.CardBrandId == CardBrand.ReferenceId
                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                        ).Count();

                                CardBrand.Amount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.ProviderId == _Request.AccountId
                                                                 && m.ParentId == _Request.SubAccountId
                                                                 && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                 && m.CardBrandId == CardBrand.ReferenceId
                                                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                 && m.StatusId == HelperStatus.Transaction.Success
                                                               ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }


                        }
                        else if (SubUserAccountTypeId == UserAccountType.MerchantStore)
                        {
                            #region  Overview
                            _Counts.TotalTerminals = _HCoreContext.TUCTerminal.Count(x => x.StoreId == _Request.SubAccountId && x.ProviderId == AccountDetails.UserAccountId);
                            _Counts.ActiveTerminals = _HCoreContext.TUCTerminal.Count(x => x.StoreId == _Request.SubAccountId && x.ProviderId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd));
                            _TerminalStatus = new OAnalytics.TerminalStatus();
                            _TerminalStatus.Total = _Counts.TotalTerminals;
                            _TerminalStatus.Active = _Counts.ActiveTerminals;
                            _TerminalStatus.Idle = _HCoreContext.TUCTerminal.Count(x => x.StoreId == _Request.SubAccountId && x.ProviderId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd));
                            _TerminalStatus.Dead = _HCoreContext.TUCTerminal.Count(x => x.StoreId == _Request.SubAccountId && x.ProviderId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate < TDeadDayEnd));
                            _TerminalStatus.Inactive = _HCoreContext.TUCTerminal.Count(x => x.StoreId == _Request.SubAccountId && x.ProviderId == AccountDetails.UserAccountId && !x.HCUAccountTransaction.Any());
                            _Counts.TerminalStatus = _TerminalStatus;


                            _Counts.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ProviderId == _Request.AccountId
                                                            && m.SubParentId == _Request.SubAccountId
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                           && m.ModeId == TransactionMode.Credit
                                                           && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                           || m.SourceId == TransactionSource.ThankUCashPlus)
                                                       ).Count();

                            _Counts.TotalSale = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.ProviderId == _Request.AccountId
                                                            && m.SubParentId == _Request.SubAccountId
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                          && m.ModeId == TransactionMode.Credit
                                                          && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || m.SourceId == TransactionSource.ThankUCashPlus)
                                                      ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            if (_Counts.ActiveTerminals > 0)
                            {
                                _Counts.AverageTransactions = _Counts.TotalTransactions / _Counts.ActiveTerminals;
                            }
                            else
                            {
                                _Counts.AverageTransactions = 0;
                            }
                            if (_Counts.ActiveTerminals > 0)
                            {
                                _Counts.AverageTransactionAmount = _Counts.TotalSale / _Counts.ActiveTerminals;
                            }
                            else
                            {
                                _Counts.AverageTransactionAmount = 0;
                            }
                            _Counts.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ProviderId == _Request.AccountId
                                                                    && m.SubParentId == _Request.SubAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && m.TypeId == TransactionType.CardReward
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                            _Counts.CardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ProviderId == _Request.AccountId
                                                                    && m.SubParentId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            _Counts.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ProviderId == _Request.AccountId
                                                                    && m.SubParentId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Count();
                            _Counts.CashTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ProviderId == _Request.AccountId
                                                                    && m.SubParentId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            #endregion
                            _Counts.CardTypeSale = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                       .Select(x => new OAnalytics.CardTypeSale
                                       {
                                           ReferenceId = x.Id,
                                           Name = x.Name,
                                           Transactions = 0,
                                           Amount = 0
                                       }).ToList();

                            foreach (var CardBrand in _Counts.CardTypeSale)
                            {

                                CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.ProviderId == _Request.AccountId
                                                          && m.SubParentId == _Request.SubAccountId
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && m.CardBrandId == CardBrand.ReferenceId
                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                        ).Count();

                                CardBrand.Amount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.ProviderId == _Request.AccountId
                                                                 && m.SubParentId == _Request.SubAccountId
                                                                 && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                 && m.CardBrandId == CardBrand.ReferenceId
                                                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                 && m.StatusId == HelperStatus.Transaction.Success
                                                               ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }


                        }
                        else if (SubUserAccountTypeId == UserAccountType.Terminal)
                        {

                            #region  Overview
                            _Counts.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ProviderId == _Request.AccountId
                                                            && m.TerminalId == _Request.SubAccountId
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                           && m.ModeId == TransactionMode.Credit
                                                           && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                           || m.SourceId == TransactionSource.ThankUCashPlus)
                                                       ).Count();

                            _Counts.TotalSale = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.ProviderId == _Request.AccountId
                                                            && m.TerminalId == _Request.SubAccountId
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                          && m.ModeId == TransactionMode.Credit
                                                          && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || m.SourceId == TransactionSource.ThankUCashPlus)
                                                      ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            if (_Counts.ActiveTerminals > 0)
                            {
                                _Counts.AverageTransactions = _Counts.TotalTransactions / _Counts.ActiveTerminals;
                            }
                            else
                            {
                                _Counts.AverageTransactions = 0;
                            }
                            if (_Counts.ActiveTerminals > 0)
                            {
                                _Counts.AverageTransactionAmount = _Counts.TotalSale / _Counts.ActiveTerminals;
                            }
                            else
                            {
                                _Counts.AverageTransactionAmount = 0;
                            }
                            _Counts.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ProviderId == _Request.AccountId
                                                                    && m.TerminalId == _Request.SubAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && m.TypeId == TransactionType.CardReward
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                            _Counts.CardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ProviderId == _Request.AccountId
                                                                    && m.TerminalId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            _Counts.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ProviderId == _Request.AccountId
                                                                    && m.TerminalId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Count();
                            _Counts.CashTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ProviderId == _Request.AccountId
                                                                    && m.TerminalId == _Request.SubAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            #endregion
                            _Counts.CardTypeSale = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                       .Select(x => new OAnalytics.CardTypeSale
                                       {
                                           ReferenceId = x.Id,
                                           Name = x.Name,
                                           Transactions = 0,
                                           Amount = 0
                                       }).ToList();

                            foreach (var CardBrand in _Counts.CardTypeSale)
                            {

                                CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.ProviderId == _Request.AccountId
                                                          && m.TerminalId == _Request.SubAccountId
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && m.CardBrandId == CardBrand.ReferenceId
                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                        ).Count();

                                CardBrand.Amount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.ProviderId == _Request.AccountId
                                                                 && m.TerminalId == _Request.SubAccountId
                                                                 && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                 && m.CardBrandId == CardBrand.ReferenceId
                                                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                 && m.StatusId == HelperStatus.Transaction.Success
                                                               ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }
                        }
                        else
                        {
                            #region  Overview
                            _Counts.TotalMerchants = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && (x.OwnerId == _Request.AccountId
                                                    || _HCoreContext.TUCTerminal.Any(m => m.MerchantId == x.Id && m.ProviderId == _Request.AccountId))).Select(x => x.Id).Distinct().Count();

                            _Counts.ActiveMerchants = _HCoreContext.HCUAccount
                                                   .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                   && x.HCUAccountTransactionParent.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd && a.ProviderId == _Request.AccountId)
                                                   && (x.OwnerId == _Request.AccountId
                                                   || _HCoreContext.TUCTerminal.Any(m => m.MerchantId == x.Id && m.ProviderId == _Request.AccountId))).Select(x => x.Id).Distinct().Count();

                            _Counts.TotalStores = _HCoreContext.HCUAccount
                                                        .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                        && (x.Owner.OwnerId == _Request.AccountId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == _Request.AccountId && m.StoreId == x.Id))).Count();

                            _Counts.ActiveStores = _HCoreContext.HCUAccount
                                                  .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                   && x.HCUAccountTransactionSubParent.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd && a.ProviderId == _Request.AccountId)
                                                  && (x.OwnerId == _Request.AccountId
                                                  || _HCoreContext.TUCTerminal.Any(m => m.StoreId == x.Id && m.ProviderId == _Request.AccountId))).Select(x => x.Id).Distinct().Count();


                            _Counts.TotalTerminals = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == AccountDetails.UserAccountId);
                            _Counts.ActiveTerminals = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd));
                            _TerminalStatus = new OAnalytics.TerminalStatus();
                            _TerminalStatus.Total = _Counts.TotalTerminals;
                            _TerminalStatus.Active = _Counts.ActiveTerminals;
                            _TerminalStatus.Idle = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd));
                            _TerminalStatus.Dead = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate < TDeadDayEnd));
                            _TerminalStatus.Inactive = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == AccountDetails.UserAccountId && !x.HCUAccountTransaction.Any());
                            _Counts.TerminalStatus = _TerminalStatus;





                            _Counts.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ProviderId == _Request.AccountId
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.TransactionDate > _Request.StartTime
                                                            && m.TransactionDate < _Request.EndTime
                                                           && m.ModeId == TransactionMode.Credit
                                                           && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                           || m.SourceId == TransactionSource.ThankUCashPlus)
                                                       ).Count();

                            _Counts.TotalSale = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.ProviderId == _Request.AccountId
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                          && m.ModeId == TransactionMode.Credit
                                                          && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || m.SourceId == TransactionSource.ThankUCashPlus)
                                                      ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            if (_Counts.ActiveTerminals > 0)
                            {
                                _Counts.AverageTransactions = _Counts.TotalTransactions / _Counts.ActiveTerminals;
                            }
                            else
                            {
                                _Counts.AverageTransactions = 0;
                            }
                            if (_Counts.ActiveTerminals > 0)
                            {
                                _Counts.AverageTransactionAmount = _Counts.TotalSale / _Counts.ActiveTerminals;
                            }
                            else
                            {
                                _Counts.AverageTransactionAmount = 0;
                            }
                            _Counts.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ProviderId == _Request.AccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && m.TypeId == TransactionType.CardReward
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                            _Counts.CardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ProviderId == _Request.AccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            _Counts.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ProviderId == _Request.AccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Count();
                            _Counts.CashTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ProviderId == _Request.AccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            #endregion
                            _Counts.CardTypeSale = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand)
                                       .Select(x => new OAnalytics.CardTypeSale
                                       {
                                           ReferenceId = x.Id,
                                           Name = x.Name,
                                           Transactions = 0,
                                           Amount = 0
                                       }).ToList();

                            foreach (var CardBrand in _Counts.CardTypeSale)
                            {

                                CardBrand.Transactions = _HCoreContext.HCUAccountTransaction
                                                         .Where(m => m.ProviderId == _Request.AccountId
                                                          && m.TransactionDate > _Request.StartTime
                                                          && m.TransactionDate < _Request.EndTime
                                                          && m.CardBrandId == CardBrand.ReferenceId
                                                          && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                        ).Count();

                                CardBrand.Amount = _HCoreContext.HCUAccountTransaction
                                                                .Where(m => m.ProviderId == _Request.AccountId
                                                                 && m.TransactionDate > _Request.StartTime
                                                                 && m.TransactionDate < _Request.EndTime
                                                                 && m.CardBrandId == CardBrand.ReferenceId
                                                                 && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                                 || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                 && m.StatusId == HelperStatus.Transaction.Success
                                                               ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            }
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Counts, "HC0001", "Details loaded");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001", "Details not found");
                        #endregion
                    }

                }


            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

        /// <summary>
        /// Description: Gets the account overview pn.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountOverviewPn(OAnalytics.Request _Request)
        {
            _Counts = new OAnalytics.Counts();
            _Counts.TotalMerchants = 0;
            _Counts.ActiveMerchants = 0;
            _Counts.TotalStores = 0;
            _Counts.ActiveStores = 0;
            _Counts.TotalTerminals = 0;
            _Counts.ActiveTerminals = 0;
            _Counts.ActivePosUpgradeRequest = 0;
            _Counts.TotalTransactions = 0;
            _Counts.TotalSale = 0;
            _Counts.AverageTransactions = 0;
            _Counts.AverageTransactionAmount = 0;
            _Counts.CardTransactions = 0;
            _Counts.CardTransactionsAmount = 0;
            _Counts.CashTransactions = 0;
            _Counts.CashTransactionAmount = 0;
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long SubUserAccountId = 0;
                    long SubUserAccountTypeId = 0;
                    if (!string.IsNullOrEmpty(_Request.SubAccountKey) && _Request.SubAccountId > 0)
                    {
                        var SubUserAccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.SubAccountKey && x.Id == _Request.SubAccountId)
                         .Select(x => new
                         {
                             UserAccountId = x.Id,
                             AccountTypeId = x.AccountTypeId,
                         }).FirstOrDefault();
                        if (SubUserAccountDetails != null)
                        {
                            SubUserAccountId = SubUserAccountDetails.UserAccountId;
                            SubUserAccountTypeId = SubUserAccountDetails.AccountTypeId;
                        }
                        //else
                        //{
                        //    #region Send Response
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1086");
                        //    #endregion
                        //}
                    }
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey && x.AccountTypeId == UserAccountType.PosAccount)
                   .Select(x => new
                   {
                       UserAccountId = x.Id,
                       AccountTypeId = x.AccountTypeId,
                       OwnerId = x.OwnerId,
                   }).FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        //DateTime DiffStartTime = _Request.StartTime.AddDays(-1);
                        //DateTime DiffEndTime = _Request.EndTime.AddDays(-1);
                        DateTime TDayStart = HCoreHelper.GetGMTDate();
                        DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                        DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                        DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                        DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);
                        if (SubUserAccountTypeId == UserAccountType.Merchant)
                        {
                            #region  Overview
                            _Counts.TotalStores = _HCoreContext.HCUAccount
                                                  .Count(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                  && x.OwnerId == SubUserAccountId
                                                  && (x.Owner.OwnerId == AccountDetails.UserAccountId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == AccountDetails.UserAccountId && m.StoreId == x.Id)));

                            _Counts.ActiveStores = _HCoreContext.HCUAccount
                                                .Count(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                  && x.OwnerId == SubUserAccountId
                                                && x.HCUAccountTransactionProvider.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd)
                                                && (x.Owner.OwnerId == AccountDetails.UserAccountId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == AccountDetails.UserAccountId && m.StoreId == x.Id)));

                            _Counts.TotalTerminals = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == AccountDetails.UserAccountId && x.MerchantId == _Request.SubAccountId);
                            _Counts.ActiveTerminals = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == AccountDetails.UserAccountId && x.MerchantId == _Request.SubAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd));

                            _Counts.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                       && m.ParentId == SubUserAccountId
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.TransactionDate > _Request.StartTime
                                                                     && m.TransactionDate < _Request.EndTime
                                                           && m.ModeId == TransactionMode.Credit
                                                           && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                           || m.SourceId == TransactionSource.ThankUCashPlus)
                                                       ).Count();

                            _Counts.TotalSale = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                       && m.ParentId == SubUserAccountId
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                          && m.ModeId == TransactionMode.Credit
                                                          && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || m.SourceId == TransactionSource.ThankUCashPlus)
                                                      ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            _Counts.AverageTransactions = _Counts.TotalTransactions / _Counts.ActiveTerminals;
                            _Counts.AverageTransactionAmount = _Counts.TotalSale / _Counts.ActiveTerminals;

                            _Counts.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                       && m.ParentId == SubUserAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && m.TypeId == TransactionType.CardReward
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                            _Counts.CardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                       && m.ParentId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            _Counts.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                       && m.ParentId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Count();
                            _Counts.CashTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                       && m.ParentId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            #endregion

                        }
                        else if (SubUserAccountTypeId == UserAccountType.MerchantStore)
                        {

                            #region  Overview

                            _Counts.TotalTerminals = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == AccountDetails.UserAccountId && x.StoreId == SubUserAccountId);
                            _Counts.ActiveTerminals = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == AccountDetails.UserAccountId && x.StoreId == SubUserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd));

                            _Counts.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                       && m.SubParentId == SubUserAccountId
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.TransactionDate > _Request.StartTime
                                                                     && m.TransactionDate < _Request.EndTime
                                                           && m.ModeId == TransactionMode.Credit
                                                           && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                           || m.SourceId == TransactionSource.ThankUCashPlus)
                                                       ).Count();

                            _Counts.TotalSale = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                       && m.SubParentId == SubUserAccountId
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                          && m.ModeId == TransactionMode.Credit
                                                          && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || m.SourceId == TransactionSource.ThankUCashPlus)
                                                      ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            _Counts.AverageTransactions = _Counts.TotalTransactions / _Counts.ActiveTerminals;
                            _Counts.AverageTransactionAmount = _Counts.TotalSale / _Counts.ActiveTerminals;

                            _Counts.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                       && m.SubParentId == SubUserAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && m.TypeId == TransactionType.CardReward
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                            _Counts.CardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                       && m.SubParentId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            _Counts.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                       && m.SubParentId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Count();
                            _Counts.CashTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                       && m.SubParentId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            #endregion

                        }
                        else if (SubUserAccountTypeId == UserAccountType.Terminal)
                        {

                            #region  Overview

                            _Counts.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                        && m.TerminalId == SubUserAccountId
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.TransactionDate > _Request.StartTime
                                                                     && m.TransactionDate < _Request.EndTime
                                                           && m.ModeId == TransactionMode.Credit
                                                           && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                           || m.SourceId == TransactionSource.ThankUCashPlus)
                                                       ).Count();

                            _Counts.TotalSale = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                        && m.TerminalId == SubUserAccountId
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                          && m.ModeId == TransactionMode.Credit
                                                          && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || m.SourceId == TransactionSource.ThankUCashPlus)
                                                      ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            _Counts.AverageTransactions = _Counts.TotalTransactions / _Counts.ActiveTerminals;
                            _Counts.AverageTransactionAmount = _Counts.TotalSale / _Counts.ActiveTerminals;

                            _Counts.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                        && m.TerminalId == SubUserAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && m.TypeId == TransactionType.CardReward
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                            _Counts.CardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                        && m.TerminalId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            _Counts.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                        && m.TerminalId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Count();
                            _Counts.CashTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                        && m.CreatedById == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            #endregion

                        }
                        else
                        {

                            #region  Overview
                            _Counts.TotalMerchants = _HCoreContext.HCUAccount
                                                    .Count(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && (x.OwnerId == AccountDetails.UserAccountId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == AccountDetails.UserAccountId && m.StoreId == x.Id)));

                            _Counts.ActiveMerchants = _HCoreContext.HCUAccount
                                                   .Count(x => x.AccountTypeId == UserAccountType.Merchant
                                                && x.HCUAccountTransactionProvider.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd)
                                                   && (x.OwnerId == AccountDetails.UserAccountId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == AccountDetails.UserAccountId && m.StoreId == x.Id)));


                            _Counts.TotalStores = _HCoreContext.HCUAccount
                                                  .Count(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                  && (x.Owner.OwnerId == AccountDetails.UserAccountId || _HCoreContext.HCUAccount.Any(m => m.OwnerId == AccountDetails.UserAccountId && m.SubOwnerId == x.Id)));

                            _Counts.ActiveStores = _HCoreContext.HCUAccount
                                                .Count(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                && x.HCUAccountTransactionProvider.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd)
                                                && (x.Owner.OwnerId == AccountDetails.UserAccountId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == AccountDetails.UserAccountId && m.StoreId == x.Id)));

                            _Counts.TotalTerminals = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == AccountDetails.UserAccountId);
                            _Counts.ActiveTerminals = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd));

                            _Counts.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.TransactionDate > _Request.StartTime
                                                                     && m.TransactionDate < _Request.EndTime
                                                           && m.ModeId == TransactionMode.Credit
                                                           && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                           || m.SourceId == TransactionSource.ThankUCashPlus)
                                                       ).Count();

                            _Counts.TotalSale = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                          && m.ModeId == TransactionMode.Credit
                                                          && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || m.SourceId == TransactionSource.ThankUCashPlus)
                                                      ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            if (_Counts.ActiveTerminals > 0)
                            {
                                _Counts.AverageTransactions = _Counts.TotalTransactions / _Counts.ActiveTerminals;
                            }
                            else
                            {
                                _Counts.AverageTransactions = 0;
                            }
                            if (_Counts.ActiveTerminals > 0)
                            {
                                _Counts.AverageTransactionAmount = _Counts.TotalSale / _Counts.ActiveTerminals;
                            }
                            else
                            {
                                _Counts.AverageTransactionAmount = 0;
                            }
                            _Counts.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && m.TypeId == TransactionType.CardReward
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                            _Counts.CardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            _Counts.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Count();
                            _Counts.CashTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            #endregion

                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Counts, "HC0001", "Details loaded");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001", "Details not found");
                        #endregion
                    }

                }


            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Gets the account overview o.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountOverviewO(OAnalytics.Request _Request)
        {
            _Counts = new OAnalytics.Counts();
            _Counts.TotalMerchants = 0;
            _Counts.ActiveMerchants = 0;
            _Counts.TotalStores = 0;
            _Counts.ActiveStores = 0;
            _Counts.TotalTerminals = 0;
            _Counts.ActiveTerminals = 0;
            _Counts.ActivePosUpgradeRequest = 0;
            _Counts.TotalTransactions = 0;
            _Counts.TotalSale = 0;
            _Counts.AverageTransactions = 0;
            _Counts.AverageTransactionAmount = 0;
            _Counts.CardTransactions = 0;
            _Counts.CardTransactionsAmount = 0;
            _Counts.CashTransactions = 0;
            _Counts.CashTransactionAmount = 0;
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long SubUserAccountId = 0;
                    long SubUserAccountTypeId = 0;
                    if (!string.IsNullOrEmpty(_Request.SubAccountKey) && _Request.SubAccountId > 0)
                    {
                        var SubUserAccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.SubAccountKey && x.Id == _Request.SubAccountId)
                         .Select(x => new
                         {
                             UserAccountId = x.Id,
                             AccountTypeId = x.AccountTypeId,
                         }).FirstOrDefault();
                        if (SubUserAccountDetails != null)
                        {
                            SubUserAccountId = SubUserAccountDetails.UserAccountId;
                            SubUserAccountTypeId = SubUserAccountDetails.AccountTypeId;
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1086");
                            #endregion
                        }
                    }
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey && x.AccountTypeId == UserAccountType.PosAccount)
                   .Select(x => new
                   {
                       UserAccountId = x.Id,
                       AccountTypeId = x.AccountTypeId,
                       OwnerId = x.OwnerId,
                   }).FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        DateTime DiffStartTime = _Request.StartTime.AddDays(-1);
                        DateTime DiffEndTime = _Request.EndTime.AddDays(-1);
                        DateTime TDayStart = HCoreHelper.GetGMTDate();
                        DateTime TDayEnd = TDayStart.AddHours(24).AddSeconds(-1);
                        DateTime T7DayStart = HCoreHelper.GetGMTDate().AddDays(-7);
                        DateTime T7DayEnd = HCoreHelper.GetGMTDate().AddSeconds(-1);
                        DateTime TDeadDayEnd = HCoreHelper.GetGMTDate().AddDays(-7);
                        if (SubUserAccountTypeId == UserAccountType.Merchant)
                        {
                            #region  Overview
                            _Counts.TotalStores = _HCoreContext.HCUAccount
                                                  .Count(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                  && x.OwnerId == SubUserAccountId
                                                  && (x.Owner.OwnerId == AccountDetails.UserAccountId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == AccountDetails.UserAccountId && m.StoreId == x.Id)));

                            _Counts.ActiveStores = _HCoreContext.HCUAccount
                                                .Count(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                  && x.OwnerId == SubUserAccountId
                                                && x.HCUAccountTransactionProvider.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd)
                                                && (x.Owner.OwnerId == AccountDetails.UserAccountId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == AccountDetails.UserAccountId && m.StoreId == x.Id)));

                            _Counts.TotalTerminals = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == AccountDetails.UserAccountId);
                            _Counts.ActiveTerminals = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd));

                            _Counts.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                       && m.ParentId == SubUserAccountId
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.TransactionDate > _Request.StartTime
                                                                     && m.TransactionDate < _Request.EndTime
                                                           && m.ModeId == TransactionMode.Credit
                                                           && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                           || m.SourceId == TransactionSource.ThankUCashPlus)
                                                       ).Count();

                            _Counts.TotalSale = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                       && m.ParentId == SubUserAccountId
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                          && m.ModeId == TransactionMode.Credit
                                                          && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || m.SourceId == TransactionSource.ThankUCashPlus)
                                                      ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            _Counts.AverageTransactions = _Counts.TotalTransactions / _Counts.ActiveTerminals;
                            _Counts.AverageTransactionAmount = _Counts.TotalSale / _Counts.ActiveTerminals;

                            _Counts.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                       && m.ParentId == SubUserAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && m.TypeId == TransactionType.CardReward
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                            _Counts.CardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                       && m.ParentId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            _Counts.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                       && m.ParentId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Count();
                            _Counts.CashTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                       && m.ParentId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            #endregion

                        }
                        else if (SubUserAccountTypeId == UserAccountType.MerchantStore)
                        {

                            #region  Overview

                            _Counts.TotalTerminals = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == AccountDetails.UserAccountId && x.StoreId == SubUserAccountId);
                            _Counts.ActiveTerminals = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == AccountDetails.UserAccountId && x.StoreId == SubUserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd));

                            _Counts.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                       && m.SubParentId == SubUserAccountId
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.TransactionDate > _Request.StartTime
                                                                     && m.TransactionDate < _Request.EndTime
                                                           && m.ModeId == TransactionMode.Credit
                                                           && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                           || m.SourceId == TransactionSource.ThankUCashPlus)
                                                       ).Count();

                            _Counts.TotalSale = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                       && m.SubParentId == SubUserAccountId
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                          && m.ModeId == TransactionMode.Credit
                                                          && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || m.SourceId == TransactionSource.ThankUCashPlus)
                                                      ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            _Counts.AverageTransactions = _Counts.TotalTransactions / _Counts.ActiveTerminals;
                            _Counts.AverageTransactionAmount = _Counts.TotalSale / _Counts.ActiveTerminals;

                            _Counts.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                       && m.SubParentId == SubUserAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && m.TypeId == TransactionType.CardReward
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                            _Counts.CardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                       && m.SubParentId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            _Counts.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                       && m.SubParentId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Count();
                            _Counts.CashTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                       && m.SubParentId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            #endregion

                        }
                        else if (SubUserAccountTypeId == UserAccountType.Terminal)
                        {

                            #region  Overview

                            _Counts.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                        && m.TerminalId == SubUserAccountId
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.TransactionDate > _Request.StartTime
                                                                     && m.TransactionDate < _Request.EndTime
                                                           && m.ModeId == TransactionMode.Credit
                                                           && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                           || m.SourceId == TransactionSource.ThankUCashPlus)
                                                       ).Count();

                            _Counts.TotalSale = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                        && m.TerminalId == SubUserAccountId
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                          && m.ModeId == TransactionMode.Credit
                                                          && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || m.SourceId == TransactionSource.ThankUCashPlus)
                                                      ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            _Counts.AverageTransactions = _Counts.TotalTransactions / _Counts.ActiveTerminals;
                            _Counts.AverageTransactionAmount = _Counts.TotalSale / _Counts.ActiveTerminals;

                            _Counts.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                        && m.TerminalId == SubUserAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && m.TypeId == TransactionType.CardReward
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                            _Counts.CardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                        && m.TerminalId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            _Counts.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                        && m.CreatedById == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Count();
                            _Counts.CashTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                        && m.TerminalId == SubUserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            #endregion

                        }
                        else
                        {

                            #region  Overview
                            _Counts.TotalMerchants = _HCoreContext.HCUAccount
                                                    .Count(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && (x.OwnerId == AccountDetails.UserAccountId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == AccountDetails.UserAccountId && m.MerchantId == x.Id)));

                            _Counts.ActiveMerchants = _HCoreContext.HCUAccount
                                                   .Count(x => x.AccountTypeId == UserAccountType.Merchant
                                                && x.HCUAccountTransactionProvider.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd)
                                                   && (x.OwnerId == AccountDetails.UserAccountId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == AccountDetails.UserAccountId && m.MerchantId == x.Id)));


                            _Counts.TotalStores = _HCoreContext.HCUAccount
                                                  .Count(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                  && (x.Owner.OwnerId == AccountDetails.UserAccountId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == AccountDetails.UserAccountId && m.StoreId == x.Id)));

                            _Counts.ActiveStores = _HCoreContext.HCUAccount
                                                .Count(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                && x.HCUAccountTransactionProvider.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd)
                                                && (x.Owner.OwnerId == AccountDetails.UserAccountId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == AccountDetails.UserAccountId && m.StoreId == x.Id)));

                            _Counts.TotalTerminals = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == AccountDetails.UserAccountId);
                            _Counts.ActiveTerminals = _HCoreContext.TUCTerminal.Count(x => x.ProviderId == AccountDetails.UserAccountId && x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd));

                            _Counts.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                       .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                            && m.TransactionDate > _Request.StartTime
                                                                     && m.TransactionDate < _Request.EndTime
                                                           && m.ModeId == TransactionMode.Credit
                                                           && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                           || m.SourceId == TransactionSource.ThankUCashPlus)
                                                       ).Count();

                            _Counts.TotalSale = _HCoreContext.HCUAccountTransaction
                                                      .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                           && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                          && m.ModeId == TransactionMode.Credit
                                                          && ((m.TypeId != TransactionType.ThankUCashPlusCredit && m.SourceId == TransactionSource.TUC)
                                                          || m.SourceId == TransactionSource.ThankUCashPlus)
                                                      ).Sum(m => (double?)m.PurchaseAmount) ?? 0;

                            _Counts.AverageTransactions = _Counts.TotalTransactions / _Counts.ActiveTerminals;
                            _Counts.AverageTransactionAmount = _Counts.TotalSale / _Counts.ActiveTerminals;

                            _Counts.CardTransactions = _HCoreContext.HCUAccountTransaction
                                                                  .Where(m => m.ProviderId == AccountDetails.UserAccountId
                                                                   && m.TransactionDate > _Request.StartTime
                                                                   && m.TransactionDate < _Request.EndTime
                                                                   && m.TypeId == TransactionType.CardReward
                                                                   && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                   || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                   && m.StatusId == HelperStatus.Transaction.Success
                                                                 ).Count();
                            _Counts.CardTransactionsAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CardReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;



                            _Counts.CashTransactions = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                  ).Count();
                            _Counts.CashTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                                   .Where(m => m.AccountId == AccountDetails.UserAccountId
                                                                    && m.TransactionDate > _Request.StartTime
                                                                    && m.TransactionDate < _Request.EndTime
                                                                    && m.TypeId == TransactionType.CashReward
                                                                    && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.SourceId == TransactionSource.TUC && m.TypeId != TransactionType.ThankUCashPlusCredit)
                                                                    || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                    && m.StatusId == HelperStatus.Transaction.Success
                                                                   ).Sum(x => (double?)x.PurchaseAmount) ?? 0;

                            #endregion

                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Counts, "HC0001", "Details loaded");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001", "Details not found");
                        #endregion
                    }

                }


            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserAccountOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

    }
}
