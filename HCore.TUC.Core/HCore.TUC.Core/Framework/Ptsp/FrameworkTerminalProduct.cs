//==================================================================================
// FileName: FrameworkTerminalProduct.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to terminal products
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.Ptsp;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Core.Framework.Ptsp
{
    internal class FrameworkTerminalProduct
    {

        HCoreContext _HCoreContext;
        TUCTerminalProduct _TUCTerminalProduct;

        /// <summary>
        /// Description: Saves the terminal product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveTerminalProduct(OTerminalProduct.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Sku))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1001, TUCCoreResource.CA1001M);
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1000, TUCCoreResource.CA1000M);
                }
                
                if (string.IsNullOrEmpty(_Request.AccountKey) || _Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1005, TUCCoreResource.CA1005M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                }
                int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                }
                long AccountId = HCoreHelper.GetUserAccountId(_Request.AccountId ,  _Request.AccountKey, Helpers.UserAccountType.Merchant , _Request.UserReference);
                if (AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1004, TUCCoreResource.CA1004M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    bool _DetailsCheck = _HCoreContext.TUCTerminalProduct.Any(x => x.AccountId == AccountId && x.SystemName == SystemName);
                    if (!_DetailsCheck)
                    {
                        _TUCTerminalProduct = new TUCTerminalProduct();
                        _TUCTerminalProduct.Guid = HCoreHelper.GenerateGuid();
                        _TUCTerminalProduct.Sku = _Request.Sku;
                        _TUCTerminalProduct.ReferenceNumber = _Request.ReferenceNumber;
                        _TUCTerminalProduct.AccountId = AccountId;
                        _TUCTerminalProduct.Name = _Request.Name;
                        _TUCTerminalProduct.SystemName = SystemName;
                        _TUCTerminalProduct.Description = _Request.Description;
                        _TUCTerminalProduct.CategoryName = _Request.CategoryName;
                        _TUCTerminalProduct.SubCategoryName = _Request.SubCategoryName;
                        _TUCTerminalProduct.ActualPrice = _Request.ActualPrice;
                        _TUCTerminalProduct.SellingPrice = _Request.SellingPrice;
                        _TUCTerminalProduct.TotalStock = _Request.TotalStock;
                        _TUCTerminalProduct.RewardPercentage = _Request.RewardPercentage;
                        _TUCTerminalProduct.AvailableStock = _Request.AvailableStock;
                        _TUCTerminalProduct.CreateDate = HCoreHelper.GetGMTDateTime();
                        _TUCTerminalProduct.CreatedById = _Request.UserReference.AccountId;
                        _TUCTerminalProduct.StatusId =StatusId;
                        _HCoreContext.TUCTerminalProduct.Add(_TUCTerminalProduct);
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            ReferenceId = _TUCTerminalProduct.Id,
                            ReferenceKey = _TUCTerminalProduct.Guid,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1007, TUCCoreResource.CA1007M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1006, TUCCoreResource.CA1006M);
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveTerminalProduct", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the terminal product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateTerminalProduct(OTerminalProduct.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    int StatusId = 0;
                    if (!string.IsNullOrEmpty(_Request.StatusCode))
                    {
                        StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                        if (StatusId == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1009, TUCCoreResource.CA1009M);
                        }
                    }
                    var _Details = _HCoreContext.TUCTerminalProduct.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_Details != null)
                    {

                        if (!string.IsNullOrEmpty(_Request.Name) && _Details.Name != _Request.Name)
                        {
                            string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                            bool NameCheck = _HCoreContext.TUCTerminalProduct
                                                        .Any(x =>
                                                        x.AccountId == _Details.AccountId
                                                        && x.SystemName == SystemName);
                            if (NameCheck)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1010, TUCCoreResource.CA1010M);
                            }
                            _Details.Name = _Request.Name;
                            _Details.SystemName = SystemName;
                        }
                        if (!string.IsNullOrEmpty(_Request.Sku) && _Details.Sku != _Request.Sku)
                        {
                            _Details.Sku = _Request.Sku;
                        }
                        if (!string.IsNullOrEmpty(_Request.ReferenceNumber) && _Details.ReferenceNumber != _Request.ReferenceNumber)
                        {
                            _Details.ReferenceNumber = _Request.ReferenceNumber;
                        }
                        
                       
                        if (!string.IsNullOrEmpty(_Request.Description) && _Details.Description != _Request.Description)
                        {
                            _Details.Description = _Request.Description;
                        }
                        if (!string.IsNullOrEmpty(_Request.CategoryName) && _Details.CategoryName != _Request.CategoryName)
                        {
                            _Details.CategoryName = _Request.CategoryName;
                        }
                        if (!string.IsNullOrEmpty(_Request.SubCategoryName) && _Details.SubCategoryName != _Request.SubCategoryName)
                        {
                            _Details.SubCategoryName = _Request.SubCategoryName;
                        }
                        if (  _Details.ActualPrice != _Request.ActualPrice && _Request.ActualPrice > 0)
                        {
                            _Details.ActualPrice = _Request.ActualPrice;
                        }
                        if (_Details.SellingPrice != _Request.SellingPrice && _Request.SellingPrice > 0)
                        {
                            _Details.SellingPrice = _Request.SellingPrice;
                        }
                        if (_Details.TotalStock != _Request.TotalStock && _Request.TotalStock > -1)
                        {
                            _Details.TotalStock = _Request.TotalStock;
                        }
                        if (_Details.RewardPercentage != _Request.RewardPercentage && _Request.RewardPercentage > -1)
                        {
                            _Details.RewardPercentage = _Request.RewardPercentage;
                        }
                       
                        if (_Details.AvailableStock != _Request.AvailableStock && _Request.AvailableStock > -1)
                        {
                            _Details.AvailableStock = _Request.AvailableStock;
                        }
                        if (StatusId != 0 && _Details.StatusId != StatusId)
                        {
                            _Details.StatusId = StatusId;
                        }
                        _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _Details.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA0201, TUCCoreResource.CA0201M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateTerminalProduct", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
        }
        /// <summary>
        /// Description: Deletes the terminal product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteTerminalProduct(OTerminalProduct.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    TUCTerminalProduct Details = _HCoreContext.TUCTerminalProduct.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        _HCoreContext.TUCTerminalProduct.Remove(Details);
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1008, TUCCoreResource.CA1008M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                 HCoreHelper.LogException("DeleteTerminalProduct", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the terminal product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTerminalProduct(OTerminalProduct.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OTerminalProduct.Details _Details = (from x in _HCoreContext.TUCTerminalProduct
                                               where x.Guid == _Request.ReferenceKey
                                               && x.Id == _Request.ReferenceId
                                               select new OTerminalProduct.Details
                                               {

                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,

                                                   Sku = x.Sku,
                                                   ReferenceNumber = x.ReferenceNumber,

                                                   Name = x.Name,
                                                   SystemName = x.SystemName,

                                                   Description = x.Description,

                                                   CategoryName = x.CategoryName,
                                                   SubCategoryName = x.SubCategoryName,

                                                   ActualPrice = x.ActualPrice,
                                                   SellingPrice = x.SellingPrice,

                                                   RewardPercentage = x.RewardPercentage,

                                                   TotalStock = x.TotalStock,
                                                   AvailableStock = x.AvailableStock,

                                                   CreateDate = x.CreateDate,
                                                   CreatedById = x.CreatedById,
                                                   CreatedByKey = x.CreatedBy.Guid,
                                                   CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                   
                                                   ModifyDate = x.ModifyDate,
                                                   ModifyById = x.ModifyById,
                                                   ModifyByKey = x.ModifyBy.Guid,
                                                   ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                   StatusCode = x.Status.SystemName,
                                                   StatusName = x.Status.Name
                                               })
                                              .FirstOrDefault();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    if (_Details != null)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        #endregion
                    }

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetTerminalProduct", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the terminal product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTerminalProduct(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.TUCTerminalProduct
                                                 where x.AccountId == _Request.ReferenceId
                                                 && x.Account.Guid == _Request.ReferenceKey
                                                 select new OTerminalProduct.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     Sku = x.Sku,
                                                     ReferenceNumber = x.ReferenceNumber,
                                                     Name = x.Name,
                                                     CategoryName   = x.CategoryName,
                                                     SellingPrice = x.SellingPrice,
                                                     AvailableStock = x.AvailableStock,
                                                     RewardPercentage = x.RewardPercentage,
                                                     CreateDate = x.CreateDate,
                                                     CreatedById = x.CreatedById,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                            .Where(_Request.SearchCondition)
                                            .Count();
                    }
                    #endregion
                    #region Get Data
                    List<OTerminalProduct.List> Data = (from x in _HCoreContext.TUCTerminalProduct
                                                        where x.AccountId == _Request.ReferenceId
                                                && x.Account.Guid == _Request.ReferenceKey
                                                        select new OTerminalProduct.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     ReferenceNumber = x.ReferenceNumber,
                                                     Sku = x.Sku,
                                                     Name = x.Name,
                                                     CategoryName = x.CategoryName,
                                                     SellingPrice = x.SellingPrice,
                                                     AvailableStock = x.AvailableStock,
                                                     RewardPercentage = x.RewardPercentage,
                                                     CreateDate = x.CreateDate,
                                                     CreatedById = x.CreatedById,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse,  TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetTerminalProduct", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }
}
