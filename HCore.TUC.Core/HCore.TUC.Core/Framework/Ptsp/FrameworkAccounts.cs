//==================================================================================
// FileName: FrameworkAccounts.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to accounts
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Core.Object.Ptsp;
using HCore.TUC.Core.Resource;

namespace HCore.TUC.Core.Framework.Ptsp
{
    internal class FrameworkAccounts
    {
        #region Declare
        HCoreContext _HCoreContext;
        OAccounts.Merchant.Details _MerchantDetails;
        OAccounts.Store.Details _StoreDetails;
        OAccounts.Terminal.Details _TerminalDetails;
        List<OAccounts.Merchant.List> _Merchants;
        List<OAccounts.Store.List> _Stores;
        List<OAccounts.Terminal.List> _Terminals;
        #endregion
        /// <summary>
        /// Description: Gets the merchant details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchant(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    _MerchantDetails = new OAccounts.Merchant.Details();
                    _MerchantDetails = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                && x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OAccounts.Merchant.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    AccountCode = x.AccountCode,
                                                    DisplayName = x.DisplayName,
                                                    Name = x.Name,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.IconStorage.Path,
                                                    Address = x.Address,
                                                    ReferralCode = x.ReferralCode,
                                                    Stores = _HCoreContext.HCUAccount.Count(m => m.OwnerId == _Request.ReferenceId && m.AccountTypeId == UserAccountType.MerchantStore),
                                                    Terminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == _Request.ReferenceId),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.MerchantId == _Request.ReferenceId && m.LastTransactionDate > ActivityDate),
                                                    RewardPercentage = x.AccountPercentage,
                                                    LastActivityDate = x.LastActivityDate,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).FirstOrDefault();
                    if (_MerchantDetails != null)
                    {
                        _MerchantDetails.ContactPerson = _HCoreContext.HCUAccount.Where(x => x.Id == _MerchantDetails.ReferenceId)
                            .Select(x => new OAccounts.ContactDetails
                            {
                                Name = x.FirstName + " " + x.LastName,
                                FirstName = x.FirstName,
                                LastName = x.LastName,
                                MobileNumber = x.MobileNumber,
                                EmailAddress = x.EmailAddress,
                            }).FirstOrDefault();

                        _MerchantDetails.Rm = _HCoreContext.TUCBranchAccount.Where(x => x.MerchantId == _MerchantDetails.ReferenceId)
                        .Select(x => new OAccounts.ContactDetails
                        {
                            ReferenceId = x.AccountId,
                            Name = x.Account.Name,
                            MobileNumber = x.Account.MobileNumber,
                            EmailAddress = x.Account.EmailAddress,
                        }).FirstOrDefault();

                        _MerchantDetails.Categories = _HCoreContext.TUCCategoryAccount.Where(x => x.AccountId == _MerchantDetails.ReferenceId
                     && x.StatusId == HelperStatus.Default.Active)
                         .Select(x => new OAccounts.Category
                         {
                             ReferenceId = x.Id,
                             ReferenceKey = x.Guid,
                             Name = x.Category.Name,
                         }).ToList();
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _MerchantDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetMerchant", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the merchant list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchant(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
                DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
                DateTime T7DayStart = TDayStart.AddDays(-7);
                DateTime T7DayEnd = TDayStart.AddSeconds(1);
                DateTime TDeadDayEnd = T7DayStart;
                _Merchants = new List<OAccounts.Merchant.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-25).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                && (x.OwnerId == _Request.ReferenceId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == _Request.ReferenceId && m.MerchantId == x.Id)))
                                                .Select(x => new OAccounts.Merchant.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    Address = x.Address,
                                                    CityName = x.City.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    ActivityStatusCode = x.ActivityStatus.SystemName,
                                                    ActivityStatusName = x.ActivityStatus.Name,
                                                    Stores = x.HCUAccountOwnerOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantStore),
                                                    Terminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == _Request.ReferenceId && m.MerchantId == x.Id),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == _Request.ReferenceId && m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                                    //RmDisplayName = "n/a",

                                                    RmId = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                    RmName = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                    RmMobileNumber = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                    RmEmailAddress = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                    RmDisplayName = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _Merchants = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                && (x.OwnerId == _Request.ReferenceId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == _Request.ReferenceId && m.MerchantId == x.Id)))
                                                .Select(x => new OAccounts.Merchant.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    EmailAddress = x.EmailAddress,
                                                    ContactNumber = x.ContactNumber,
                                                    Address = x.Address,
                                                    CityName = x.City.Name,
                                                    IconUrl = x.IconStorage.Path,
                                                    LastTransactionDate = x.LastTransactionDate,
                                                    ActivityStatusCode = x.ActivityStatus.SystemName,
                                                    ActivityStatusName = x.ActivityStatus.Name,
                                                    Stores = x.HCUAccountOwnerOwner.Count(m => m.AccountTypeId == UserAccountType.MerchantStore),
                                                    Terminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == _Request.ReferenceId && m.MerchantId == x.Id),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == _Request.ReferenceId && m.MerchantId == x.Id && m.LastTransactionDate > ActivityDate),
                                                    //RmDisplayName = "n/a",

                                                    RmId = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                    RmName = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                    RmMobileNumber = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                    RmEmailAddress = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                    RmDisplayName = x.TUCBranchAccountMerchant.Where(a => a.MerchantId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Merchants)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Merchants, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetMerchant", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the store details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStore(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    _StoreDetails = new OAccounts.Store.Details();
                    _StoreDetails = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                && x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OAccounts.Store.Details
                                                {
                                                    MerchantId = x.OwnerId,
                                                    MerchantKey = x.Owner.Guid,
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    AccountCode = x.AccountCode,
                                                    DisplayName = x.DisplayName,
                                                    ContactNumber = x.ContactNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    IconUrl = x.Owner.IconStorage.Path,
                                                    Address = x.Address,
                                                    Terminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == _Request.ReferenceId),
                                                    ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.StoreId == _Request.ReferenceId && m.LastTransactionDate > ActivityDate),
                                                    RewardPercentage = 0,
                                                    LastActivityDate = x.LastActivityDate,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).FirstOrDefault();
                    if (_StoreDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_StoreDetails.IconUrl))
                        {
                            _StoreDetails.IconUrl = _AppConfig.StorageUrl + _StoreDetails.IconUrl;
                        }
                        else
                        {
                            _StoreDetails.IconUrl = _AppConfig.Default_Icon;
                        }

                        _StoreDetails.ContactPerson = _HCoreContext.HCUAccount.Where(x => x.Id == _StoreDetails.ReferenceId)
                            .Select(x => new OAccounts.ContactDetails
                            {
                                Name = x.FirstName + " " + x.LastName,
                                FirstName = x.FirstName,
                                LastName = x.LastName,
                                MobileNumber = x.MobileNumber,
                                EmailAddress = x.EmailAddress,
                            }).FirstOrDefault();
                        _StoreDetails.Rm = _HCoreContext.TUCBranchAccount.Where(x => x.StoreId == _StoreDetails.ReferenceId)
                           .Select(x => new OAccounts.ContactDetails
                           {
                               ReferenceId = x.AccountId,
                               Name = x.Account.Name,
                               MobileNumber = x.Account.MobileNumber,
                               EmailAddress = x.Account.EmailAddress,
                           }).FirstOrDefault();
                        _StoreDetails.Categories = _HCoreContext.TUCCategoryAccount.Where(x => x.AccountId == _StoreDetails.MerchantId
                        && x.StatusId == HelperStatus.Default.Active)
                            .Select(x => new OAccounts.Category
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                Name = x.Category.Name,
                            }).ToList();
                        _HCoreContext.Dispose();
                        _StoreDetails.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("rewardpercentage", _StoreDetails.ReferenceId, _Request.UserReference));
                        if (_StoreDetails.RewardPercentage < 1)
                        {
                            _StoreDetails.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfigurationValueByUserAccount("rewardpercentage", (long)_StoreDetails.MerchantId, _Request.UserReference));
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _StoreDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetStore", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the store list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStore(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
                DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
                DateTime T7DayStart = TDayStart.AddDays(-7);
                DateTime T7DayEnd = TDayStart.AddSeconds(1);
                DateTime TDeadDayEnd = T7DayStart;

                _Stores = new List<OAccounts.Store.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                DateTime ActivityDate = HCoreHelper.GetGMTDateTime().AddHours(-24).AddMilliseconds(-1);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.Type == "merchant")
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                    && x.OwnerId == _Request.SubReferenceId
                                                    && (x.Owner.OwnerId == _Request.ReferenceId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == _Request.ReferenceId && m.StoreId == x.Id)))
                                                    .Select(x => new OAccounts.Store.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        MerchantId = x.Owner.Id,
                                                        MerchantKey = x.Owner.DisplayName,
                                                        MerchantStatusId = x.Owner.StatusId,
                                                        MerchantStatusCode = x.Owner.Status.SystemName,
                                                        MerchantStatusName = x.Owner.Status.Name,
                                                        DisplayName = x.DisplayName,
                                                        EmailAddress = x.EmailAddress,
                                                        ContactNumber = x.ContactNumber,
                                                        Address = x.Address,
                                                        CountryId = x.CountryId,
                                                        CountryKey = x.Country.Guid,
                                                        CountryName = x.Country.Name,
                                                        StateId = x.StateId,
                                                        StateKey = x.State.Guid,
                                                        StateName = x.State.Name,
                                                        CityId = x.CityId,
                                                        CityKey = x.City.Guid,
                                                        CityName = x.City.Name,
                                                        CityAreaId = x.CityAreaId,
                                                        CityAreaKey = x.CityArea.Guid,
                                                        CityAreaName = x.CityArea.Name,
                                                        IconUrl = x.Owner.IconStorage.Path,
                                                        Terminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == _Request.ReferenceId && m.StoreId == x.Id),
                                                        ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == _Request.ReferenceId && m.StoreId == x.Id && m.LastTransactionDate > ActivityDate),
                                                        //RmDisplayName = "n/a",

                                                        RmId = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                        RmName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                        RmMobileNumber = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                        RmEmailAddress = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                        RmDisplayName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                        RewardPercentage = x.AccountPercentage,
                                                        CreateDate = x.CreateDate,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        _Stores = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                    && x.OwnerId == _Request.SubReferenceId
                                                    && (x.Owner.OwnerId == _Request.ReferenceId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == _Request.ReferenceId && m.StoreId == x.Id)))
                                                    .Select(x => new OAccounts.Store.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        MerchantId = x.Owner.Id,
                                                        MerchantKey = x.Owner.DisplayName,
                                                        MerchantStatusId = x.Owner.StatusId,
                                                        MerchantStatusCode = x.Owner.Status.SystemName,
                                                        MerchantStatusName = x.Owner.Status.Name,
                                                        DisplayName = x.DisplayName,
                                                        EmailAddress = x.EmailAddress,
                                                        ContactNumber = x.ContactNumber,
                                                        Address = x.Address,
                                                        CountryId = x.CountryId,
                                                        CountryKey = x.Country.Guid,
                                                        CountryName = x.Country.Name,
                                                        StateId = x.StateId,
                                                        StateKey = x.State.Guid,
                                                        StateName = x.State.Name,
                                                        CityId = x.CityId,
                                                        CityKey = x.City.Guid,
                                                        CityName = x.City.Name,
                                                        CityAreaId = x.CityAreaId,
                                                        CityAreaKey = x.CityArea.Guid,
                                                        CityAreaName = x.CityArea.Name,
                                                        IconUrl = x.Owner.IconStorage.Path,
                                                        Terminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == _Request.ReferenceId && m.StoreId == x.Id),
                                                        ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == _Request.ReferenceId && m.StoreId == x.Id && m.LastTransactionDate > ActivityDate),
                                                        //RmDisplayName = "n/a",

                                                        RmId = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                        RmName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                        RmMobileNumber = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                        RmEmailAddress = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                        RmDisplayName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                        RewardPercentage = x.AccountPercentage,
                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var DataItem in _Stores)
                        {
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            else
                            {
                                DataItem.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Stores, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                    && (x.Owner.OwnerId == _Request.ReferenceId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == _Request.ReferenceId && m.StoreId == x.Id)))
                                                    .Select(x => new OAccounts.Store.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        MerchantId = x.Owner.Id,
                                                        MerchantKey = x.Owner.DisplayName,
                                                        MerchantStatusId = x.Owner.StatusId,
                                                        MerchantStatusCode = x.Owner.Status.SystemName,
                                                        MerchantStatusName = x.Owner.Status.Name,
                                                        DisplayName = x.DisplayName,
                                                        EmailAddress = x.EmailAddress,
                                                        ContactNumber = x.ContactNumber,
                                                        Address = x.Address,
                                                        CityName = x.City.Name,
                                                        IconUrl = x.Owner.IconStorage.Path,
                                                        Terminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == _Request.ReferenceId && m.StoreId == x.Id),
                                                        ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == _Request.ReferenceId && m.StoreId == x.Id && m.LastTransactionDate > ActivityDate),
                                                        //RmDisplayName = "n/a",

                                                        RmId = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                        RmName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                        RmMobileNumber = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                        RmEmailAddress = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                        RmDisplayName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                        RewardPercentage = x.AccountPercentage,
                                                        CreateDate = x.CreateDate,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        _Stores = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                    && (x.Owner.OwnerId == _Request.ReferenceId || _HCoreContext.TUCTerminal.Any(m => m.ProviderId == _Request.ReferenceId && m.StoreId == x.Id)))
                                                    .Select(x => new OAccounts.Store.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        MerchantId = x.Owner.Id,
                                                        MerchantKey = x.Owner.DisplayName,
                                                        MerchantStatusId = x.Owner.StatusId,
                                                        MerchantStatusCode = x.Owner.Status.SystemName,
                                                        MerchantStatusName = x.Owner.Status.Name,
                                                        DisplayName = x.DisplayName,
                                                        EmailAddress = x.EmailAddress,
                                                        ContactNumber = x.ContactNumber,
                                                        Address = x.Address,
                                                        CityName = x.City.Name,
                                                        IconUrl = x.Owner.IconStorage.Path,
                                                        Terminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == _Request.ReferenceId && m.StoreId == x.Id),
                                                        ActiveTerminals = _HCoreContext.TUCTerminal.Count(m => m.ProviderId == _Request.ReferenceId && m.StoreId == x.Id && m.LastTransactionDate > ActivityDate),
                                                        //RmDisplayName = "n/a",

                                                        RmId = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.AccountId).FirstOrDefault(),
                                                        RmName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.Name).FirstOrDefault(),
                                                        RmMobileNumber = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                        RmEmailAddress = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                        RmDisplayName = x.TUCBranchAccountStore.Where(a => a.StoreId == x.Id).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                        RewardPercentage = x.AccountPercentage,
                                                        CreateDate = x.CreateDate,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var DataItem in _Stores)
                        {
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            else
                            {
                                DataItem.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Stores, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetStore", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the terminal details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTerminal(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREF, TUCCoreResource.CAREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAREFKEY, TUCCoreResource.CAREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _TerminalDetails = new OAccounts.Terminal.Details();
                    _TerminalDetails = _HCoreContext.TUCTerminal
                                                .Where(x => x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OAccounts.Terminal.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    TerminalId = x.DisplayName,
                                                    IdentificationNumber = x.IdentificationNumber,

                                                    ProviderId = x.ProviderId,
                                                    ProviderKey = x.Provider.Guid,
                                                    ProviderName = x.Provider.DisplayName,
                                                    ProviderIconUrl = x.Provider.IconStorage.Path,

                                                    MerchantId = x.MerchantId,
                                                    MerchantKey = x.Merchant.Guid,
                                                    MerchantName = x.Merchant.DisplayName,
                                                    MerchantIconUrl = x.Merchant.IconStorage.Path,

                                                    AcquirerId = x.AcquirerId,
                                                    AcquirerKey = x.Acquirer.Guid,
                                                    AcquirerName = x.Acquirer.DisplayName,
                                                    AcquirerIconUrl = x.Acquirer.IconStorage.Path,

                                                    StoreId = x.StoreId,
                                                    StoreKey = x.Store.Guid,
                                                    StoreName = x.Store.DisplayName,
                                                    StoreAddress = x.Store.Address,
                                                    StoreLatitude = x.Store.Latitude,
                                                    StoreLongitude = x.Store.Longitude,

                                                    LastActivityDate = x.LastActivityDate,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    TypeId = x.TypeId,
                                                    TypeCode = x.Type.SystemName,
                                                    TypeName = x.Type.Name,
                                                }).FirstOrDefault();
                    if (_TerminalDetails != null)
                    {
                        _TerminalDetails.InvoiceAmount = _HCoreContext.HCUAccountTransaction
                                      .Where(a => a.TerminalId == _Request.ReferenceId
                                      && a.StatusId == HelperStatus.Transaction.Success
                                      && a.ModeId == TransactionMode.Credit
                                      && ((a.TypeId != TransactionType.ThankUCashPlusCredit
                                      && a.SourceId == TransactionSource.TUC)
                                      || a.SourceId == TransactionSource.ThankUCashPlus))
                                     .Sum(m => m.PurchaseAmount);

                        _TerminalDetails.Store = _HCoreContext.TUCTerminal.Where(x => x.StoreId == _TerminalDetails.StoreId)
                            .Select(x => new OAccounts.StoreDetails
                            {
                                Name = x.Store.DisplayName,
                                MobileNumber = x.Store.MobileNumber,
                                EmailAddress = x.Store.EmailAddress,
                            }).FirstOrDefault();
                        //_TerminalDetails.ContactPerson = _HCoreContext.HCUAccount.Where(x => x.Id == _TerminalDetails.MerchantId)
                        //    .Select(x => new OAccounts.ContactDetails
                        //    {
                        //        Name = x.FirstName + " " + x.LastName,
                        //        MobileNumber = x.MobileNumber,
                        //        EmailAddress = x.EmailAddress,
                        //    }).FirstOrDefault();

                        _TerminalDetails.Rm = _HCoreContext.TUCBranchAccount.Where(x => x.StoreId == _TerminalDetails.StoreId)
                           .Select(x => new OAccounts.ContactDetails
                           {
                               ReferenceId = x.AccountId,
                               Name = x.Account.Name,
                               MobileNumber = x.Account.MobileNumber,
                               EmailAddress = x.Account.EmailAddress,
                           }).FirstOrDefault();


                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _TerminalDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetTerminal", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the terminal list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTerminal(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime TDayStart = HCoreHelper.GetNigeriaToGMT(HCoreHelper.GetGMTDate()).AddSeconds(-1);
                DateTime TDayEnd = TDayStart.AddDays(1).AddSeconds(1);
                DateTime T7DayStart = TDayStart.AddDays(-7);
                DateTime T7DayEnd = TDayStart.AddSeconds(1);
                DateTime TDeadDayEnd = T7DayStart;
                _Terminals = new List<OAccounts.Terminal.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.Type == "active")
                    {
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                .Where(x => x.ProviderId == _Request.ReferenceId)
                                                    .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd))
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,

                                                        MerchantId = x.Merchant.Id,
                                                        MerchantKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,

                                                        StoreId = x.Store.Id,
                                                        StoreKey = x.Store.Guid,
                                                        StoreName = x.Store.DisplayName,
                                                        StoreContactNumber = x.Store.ContactNumber,
                                                        StoreAddress = x.Store.Address,
                                                        StoreLatitude = x.Store.Latitude,
                                                        StoreLongitude = x.Store.Longitude,

                                                        AcquirerId = x.Acquirer.Id,
                                                        AcquirerKey = x.Acquirer.Guid,
                                                        AcquirerName = x.Acquirer.DisplayName,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                        RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                        RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                        RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                        RmDisplayName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                        }
                        #region Get Data
                        _Terminals = _HCoreContext.TUCTerminal
                                                .Where(x => x.ProviderId == _Request.ReferenceId)
                                                    .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate > TDayStart && a.TransactionDate < TDayEnd))
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,

                                                        MerchantId = x.Merchant.Id,
                                                        MerchantKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,

                                                        StoreId = x.Store.Id,
                                                        StoreKey = x.Store.Guid,
                                                        StoreName = x.Store.DisplayName,
                                                        StoreContactNumber = x.Store.ContactNumber,
                                                        StoreAddress = x.Store.Address,
                                                        StoreLatitude = x.Store.Latitude,
                                                        StoreLongitude = x.Store.Longitude,

                                                        AcquirerId = x.Acquirer.Id,
                                                        AcquirerKey = x.Acquirer.Guid,
                                                        AcquirerName = x.Acquirer.DisplayName,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                        RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                        RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                        RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                        RmDisplayName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                    }
                    else if (_Request.Type == "idle")
                    {
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                .Where(x => x.ProviderId == _Request.ReferenceId)
                                                    .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd))
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,

                                                        MerchantId = x.Merchant.Id,
                                                        MerchantKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,

                                                        StoreId = x.Store.Id,
                                                        StoreKey = x.Store.Guid,
                                                        StoreName = x.Store.DisplayName,
                                                        StoreContactNumber = x.Store.ContactNumber,
                                                        StoreAddress = x.Store.Address,
                                                        StoreLatitude = x.Store.Latitude,
                                                        StoreLongitude = x.Store.Longitude,

                                                        AcquirerId = x.Acquirer.Id,
                                                        AcquirerKey = x.Acquirer.Guid,
                                                        AcquirerName = x.Acquirer.DisplayName,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                        RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                        RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                        RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                        RmDisplayName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                        }
                        #region Get Data
                        _Terminals = _HCoreContext.TUCTerminal
                                                .Where(x => x.ProviderId == _Request.ReferenceId)
                                                    .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate > T7DayStart && a.TransactionDate < T7DayEnd))
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,

                                                        MerchantId = x.Merchant.Id,
                                                        MerchantKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,

                                                        StoreId = x.Store.Id,
                                                        StoreKey = x.Store.Guid,
                                                        StoreName = x.Store.DisplayName,
                                                        StoreContactNumber = x.Store.ContactNumber,
                                                        StoreAddress = x.Store.Address,
                                                        StoreLatitude = x.Store.Latitude,
                                                        StoreLongitude = x.Store.Longitude,

                                                        AcquirerId = x.Acquirer.Id,
                                                        AcquirerKey = x.Acquirer.Guid,
                                                        AcquirerName = x.Acquirer.DisplayName,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                        RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                        RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                        RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                        RmDisplayName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                    }
                    else if (_Request.Type == "dead")
                    {
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                .Where(x => x.ProviderId == _Request.ReferenceId)
                                                    .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate < TDeadDayEnd))
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,

                                                        MerchantId = x.Merchant.Id,
                                                        MerchantKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,

                                                        StoreId = x.Store.Id,
                                                        StoreKey = x.Store.Guid,
                                                        StoreName = x.Store.DisplayName,
                                                        StoreContactNumber = x.Store.ContactNumber,
                                                        StoreAddress = x.Store.Address,
                                                        StoreLatitude = x.Store.Latitude,
                                                        StoreLongitude = x.Store.Longitude,

                                                        AcquirerId = x.Acquirer.Id,
                                                        AcquirerKey = x.Acquirer.Guid,
                                                        AcquirerName = x.Acquirer.DisplayName,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                        RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                        RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                        RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                        RmDisplayName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                        }
                        #region Get Data
                        _Terminals = _HCoreContext.TUCTerminal
                                                .Where(x => x.ProviderId == _Request.ReferenceId)
                                                    .Where(x => x.HCUAccountTransaction.Any(a => a.TransactionDate < TDeadDayEnd))
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,

                                                        MerchantId = x.Merchant.Id,
                                                        MerchantKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,

                                                        StoreId = x.Store.Id,
                                                        StoreKey = x.Store.Guid,
                                                        StoreName = x.Store.DisplayName,
                                                        StoreContactNumber = x.Store.ContactNumber,
                                                        StoreAddress = x.Store.Address,
                                                        StoreLatitude = x.Store.Latitude,
                                                        StoreLongitude = x.Store.Longitude,

                                                        AcquirerId = x.Acquirer.Id,
                                                        AcquirerKey = x.Acquirer.Guid,
                                                        AcquirerName = x.Acquirer.DisplayName,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                        RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                        RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                        RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                        RmDisplayName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                    }
                    else if (_Request.Type == "unused")
                    {
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                .Where(x => x.ProviderId == _Request.ReferenceId)
                                                    .Where(x => x.HCUAccountTransaction.Any() == false)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,

                                                        MerchantId = x.Merchant.Id,
                                                        MerchantKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,

                                                        StoreId = x.Store.Id,
                                                        StoreKey = x.Store.Guid,
                                                        StoreName = x.Store.DisplayName,
                                                        StoreContactNumber = x.Store.ContactNumber,
                                                        StoreAddress = x.Store.Address,
                                                        StoreLatitude = x.Store.Latitude,
                                                        StoreLongitude = x.Store.Longitude,

                                                        AcquirerId = x.Acquirer.Id,
                                                        AcquirerKey = x.Acquirer.Guid,
                                                        AcquirerName = x.Acquirer.DisplayName,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                        RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                        RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                        RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                        RmDisplayName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                        }
                        #region Get Data
                        _Terminals = _HCoreContext.TUCTerminal
                                                .Where(x => x.ProviderId == _Request.ReferenceId)
                                                    .Where(x => x.HCUAccountTransaction.Any() == false)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,

                                                        MerchantId = x.Merchant.Id,
                                                        MerchantKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,

                                                        StoreId = x.Store.Id,
                                                        StoreKey = x.Store.Guid,
                                                        StoreName = x.Store.DisplayName,
                                                        StoreContactNumber = x.Store.ContactNumber,
                                                        StoreAddress = x.Store.Address,
                                                        StoreLatitude = x.Store.Latitude,
                                                        StoreLongitude = x.Store.Longitude,

                                                        AcquirerId = x.Acquirer.Id,
                                                        AcquirerKey = x.Acquirer.Guid,
                                                        AcquirerName = x.Acquirer.DisplayName,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                        RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                        RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                        RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                        RmDisplayName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.TUCTerminal
                                                .Where(x => x.ProviderId == _Request.ReferenceId)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,

                                                        MerchantId = x.Merchant.Id,
                                                        MerchantKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,

                                                        StoreId = x.Store.Id,
                                                        StoreKey = x.Store.Guid,
                                                        StoreName = x.Store.DisplayName,
                                                        StoreContactNumber = x.Store.ContactNumber,
                                                        StoreAddress = x.Store.Address,
                                                        StoreLatitude = x.Store.Latitude,
                                                        StoreLongitude = x.Store.Longitude,

                                                        AcquirerId = x.Acquirer.Id,
                                                        AcquirerKey = x.Acquirer.Guid,
                                                        AcquirerName = x.Acquirer.DisplayName,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                        RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                        RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                        RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                        RmDisplayName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        _Terminals = _HCoreContext.TUCTerminal
                                                .Where(x => x.ProviderId == _Request.ReferenceId)
                                                    .Select(x => new OAccounts.Terminal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        TerminalId = x.DisplayName,
                                                        IdentificationNumber = x.IdentificationNumber,

                                                        MerchantId = x.Merchant.Id,
                                                        MerchantKey = x.Merchant.Guid,
                                                        MerchantDisplayName = x.Merchant.DisplayName,

                                                        StoreId = x.Store.Id,
                                                        StoreKey = x.Store.Guid,
                                                        StoreName = x.Store.DisplayName,
                                                        StoreContactNumber = x.Store.ContactNumber,
                                                        StoreAddress = x.Store.Address,
                                                        StoreLatitude = x.Store.Latitude,
                                                        StoreLongitude = x.Store.Longitude,

                                                        AcquirerId = x.Acquirer.Id,
                                                        AcquirerKey = x.Acquirer.Guid,
                                                        AcquirerName = x.Acquirer.DisplayName,
                                                        LastTransactionDate = x.LastTransactionDate,
                                                        ActivityStatusCode = x.ActivityStatus.SystemName,
                                                        ActivityStatusName = x.ActivityStatus.Name,

                                                        RmId = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.AccountId).FirstOrDefault(),
                                                        RmName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.Name).FirstOrDefault(),
                                                        RmMobileNumber = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.MobileNumber).FirstOrDefault(),
                                                        RmEmailAddress = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.EmailAddress).FirstOrDefault(),
                                                        RmDisplayName = x.Store.TUCBranchAccountStore.Where(a => a.StoreId == x.StoreId).Select(m => m.Account.DisplayName).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        TypeId = x.TypeId,
                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Terminals)
                    {
                        if (DataItem.LastTransactionDate > TDayStart && DataItem.LastTransactionDate < TDayEnd)
                        {
                            DataItem.ActivityStatusCode = "active";
                            DataItem.ActivityStatusName = "active";
                        }
                        else if (DataItem.LastTransactionDate > T7DayStart && DataItem.LastTransactionDate < T7DayEnd)
                        {
                            DataItem.ActivityStatusCode = "idle";
                            DataItem.ActivityStatusName = "idle";
                        }
                        else if (DataItem.LastTransactionDate < TDeadDayEnd)
                        {
                            DataItem.ActivityStatusCode = "dead";
                            DataItem.ActivityStatusName = "dead";
                        }
                        else if (DataItem.LastTransactionDate == null)
                        {
                            DataItem.ActivityStatusCode = "unused";
                            DataItem.ActivityStatusName = "unused";
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Terminals, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetTerminal", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }
}
