//==================================================================================
// FileName: FrameworkBuyPoint.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to payment
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Integration.Paystack;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Core.Object.CustomerWeb;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Resource;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant.HelperStatus;
using HCore.Integration.Flutterwave;
using HCore.Integration.Flutterwave.Objects;
using Microsoft.EntityFrameworkCore;

namespace HCore.TUC.Core.Framework.CustomerWeb
{
    internal class FrameworkBuyPoint
    {
        OBuyPoint.Initialize.Response _BuyPointResponse;
        HCoreContext _HCoreContext;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        ManageCoreTransaction _ManageCoreTransaction;
        List<OBuyPoint.Initialize.BanksList> _BankList;
        HCUAccountParameter _HCUAccountParameter;

        /// <summary>
        /// Description: Coral pay initialize.
        /// </summary>
        /// <param name="TransactionId">The transaction identifier.</param>
        /// <param name="Amount">The amount.</param>
        /// <param name="UserReference">The user reference.</param>
        /// <returns>OCoralPay.OResponseDetails.</returns>
        private static OCoralPay.OResponseDetails CoralPayInitialize(string TransactionId, double Amount, OUserReference UserReference)
        {
            try
            {
                //var CoralPayObejct = new
                //{
                //    RequestHeader = new
                //    {
                //        Username = "smashlabs",
                //        Password = "2730122919@001#4"
                //    },
                //    RequestDetails = new
                //    {
                //        TerminalId = "4101LCC2",
                //        Channel = "USSD",
                //        MerchantId = "4101LCC20000001",
                //        TransactionType = "0",
                //        SubMerchantName = "LCC",
                //        TraceID = TransactionId,
                //        Amount = Amount,
                //    }
                //};
                var CoralPayObejct = new
                {
                    RequestHeader = new
                    {
                        Username = "thankucash",
                        Password = "1321013021@007#5"
                    },
                    RequestDetails = new
                    {
                        TerminalId = "4101TUC1",
                        Channel = "USSD",
                        MerchantId = "4101TUC10000001",
                        TransactionType = "0",
                        SubMerchantName = "ThankUCash",
                        TraceID = TransactionId,
                        Amount = Amount,
                    }
                };
                if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                {
                    CoralPayObejct = new
                    {
                        RequestHeader = new
                        {
                            Username = "SmashLabs",
                            Password = "1100125719@006#5"
                        },
                        RequestDetails = new
                        {
                            TerminalId = "1057SP01",
                            Channel = "USSD",
                            MerchantId = "1057SP010000001",
                            TransactionType = "0",
                            SubMerchantName = "ThankUCash",
                            TraceID = TransactionId,
                            Amount = Amount,
                        }
                    };
                }
                string NetworkRequest = JsonConvert.SerializeObject(CoralPayObejct);
                string ExtPublicKeyPathData = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Keys/CoralPay/Cconnect.Publicx.Key_Live.Txt");
                if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                {
                    ExtPublicKeyPathData = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Keys/CoralPay/Cconnect.Publicx.Key_Test.Txt");
                }
                var ENC = CoralPay.Cryptography.PGP.Actions.Data.Encrypt(new CoralPay.Cryptography.PGP.Models.EncryptionParam { ToEncryptText = NetworkRequest, ExternalPublicKeyPath = ExtPublicKeyPathData }).Result;
                if (ENC.Header.ResponseCode == "00")
                {
                    var EncryptedText = ENC.Encryption;

                    string PostUrl = "https://testdev.coralpay.com/cgateproxy/api/invokereference";
                    if (HCoreConstant.HostEnvironment == HostEnvironmentType.Live)
                    {
                        PostUrl = "https://cgateweb.coralpay.com:567/api/InvokeReference";
                    }
                    HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(PostUrl);
                    byte[] bytes = Encoding.ASCII.GetBytes(EncryptedText);
                    _HttpWebRequest.ContentType = "text/plain";
                    _HttpWebRequest.ContentLength = bytes.Length;
                    _HttpWebRequest.Method = "POST";
                    Stream _RequestStream = _HttpWebRequest.GetRequestStream();
                    _RequestStream.Write(bytes, 0, bytes.Length);
                    _RequestStream.Close();
                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        Stream responseStream = _HttpWebResponse.GetResponseStream();
                        string responseStr = new StreamReader(responseStream).ReadToEnd();
                        string SMLPublicKey = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Keys/CoralPay/SmashLabs.Cconnect.Publicx.Key_Test.Txt");
                        string SMLPrivateKey = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Keys/CoralPay/SmashLabs.Cconnect.Privatex.Key_Test.Txt");
                        string Password = "ThankUCash@123";
                        if (HostEnvironment == HostEnvironmentType.Live)
                        {
                            SMLPublicKey = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Keys/CoralPay/SmashLabs.Cconnect.Publicx.Key_Live.Txt");
                            SMLPrivateKey = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Keys/CoralPay/SmashLabs.Cconnect.Privatex.Key_Live.Txt");
                            Password = "ThankUCash@123#1321013021@007#5";
                        }
                        var Dec = CoralPay.Cryptography.PGP.Actions.Data.Decrypt(new CoralPay.Cryptography.PGP.Models.DecryptionParam
                        {
                            EncryptedData = responseStr,
                            InternalKeyPassword = Password,
                            InternalPrivateKey = SMLPrivateKey,
                            InternalPublicKey = SMLPublicKey,
                        }).Result;
                        if (Dec.Header.ResponseCode == "00")
                        {
                            var ResponseContent = Dec.Decryption;
                            OCoralPay.OPaymentInitializeResponse _Response = JsonConvert.DeserializeObject<OCoralPay.OPaymentInitializeResponse>(ResponseContent);
                            if (_Response != null && _Response.ResponseDetails != null && _Response.ResponseHeader != null && _Response.ResponseHeader.ResponseCode == "00")
                            {
                                return _Response.ResponseDetails;
                            }
                            else
                            {
                                HCoreHelper.LogData(HCoreConstant.LogType.Log, "QuickPayInitialize", "INVALID RESPONSE 003 128", null, null);
                                return null;
                            }
                        }
                        else
                        {
                            HCoreHelper.LogData(HCoreConstant.LogType.Log, "QuickPayInitialize", "INVALID RESPONSE 003 135", JsonConvert.SerializeObject(Dec), null);
                            return null;
                        }
                    }
                    else
                    {
                        HCoreHelper.LogData(HCoreConstant.LogType.Log, "QuickPayInitialize", "INVALID RESPONSE 003 140", null, null);
                        return null;
                    }
                }
                else
                {
                    HCoreHelper.LogData(HCoreConstant.LogType.Log, "QuickPayInitialize", "INVALID RESPONSE 003 146", null, null);
                    return null;
                }
            }
            catch (Exception _Exception)
            {
                //#region  Log Exception
                HCoreHelper.LogException("CoralPayInitialize", _Exception, UserReference);
                //#endregion
                return null;
            }

        }

        /// <summary>
        /// Description: Coral pay verify.
        /// </summary>
        /// <param name="ReferenceNumber">The reference number.</param>
        /// <param name="Amount">The amount.</param>
        /// <param name="UserReference">The user reference.</param>
        /// <returns>OCoralPay.OPaymentDetails.</returns>
        private static OCoralPay.OPaymentDetails CoralPayVerify(string ReferenceNumber, double Amount, OUserReference UserReference)
        {
            #region Manage Exception
            try
            {
                var CoralPayObejct = new
                {
                    RequestHeader = new
                    {
                        Username = "SmashLabs",
                        Password = "1100125719@006#5"
                    },
                    RequestDetails = new
                    {
                        TerminalId = "1057SP01",
                        MerchantId = "1057SP010000001",
                        Amount = Amount,
                        TransactionID = ReferenceNumber,
                    }
                };
                if (HCoreConstant.HostEnvironment == HostEnvironmentType.Live)
                {
                    CoralPayObejct = new
                    {
                        RequestHeader = new
                        {
                            Username = "thankucash",
                            Password = "1321013021@007#5"
                        },
                        RequestDetails = new
                        {
                            TerminalId = "4101TUC1",
                            MerchantId = "4101TUC10000001",
                            Amount = Amount,
                            TransactionID = ReferenceNumber,
                        }
                    };
                }
                string NetworkRequest = JsonConvert.SerializeObject(CoralPayObejct);
                string ExtPublicKeyPathData = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Keys/CoralPay/Cconnect.Publicx.Key_Test.Txt");
                if (HCoreConstant.HostEnvironment == HostEnvironmentType.Live)
                {
                    ExtPublicKeyPathData = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Keys/CoralPay/Cconnect.Publicx.Key_Live.Txt");
                }
                var ENC = CoralPay.Cryptography.PGP.Actions.Data.Encrypt(new CoralPay.Cryptography.PGP.Models.EncryptionParam { ToEncryptText = NetworkRequest, ExternalPublicKeyPath = ExtPublicKeyPathData }).Result;
                if (ENC.Header.ResponseCode == "00")
                {
                    var EncryptedText = ENC.Encryption;
                    string PostUrl = "https://testdev.coralpay.com/cgateproxy/api/StatusQuery";
                    if (HCoreConstant.HostEnvironment == HostEnvironmentType.Live)
                    {
                        PostUrl = "https://cgateweb.coralpay.com:567/api/StatusQuery";
                    }
                    HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(PostUrl);
                    byte[] bytes = Encoding.ASCII.GetBytes(EncryptedText);
                    _HttpWebRequest.ContentType = "text/plain";
                    _HttpWebRequest.ContentLength = bytes.Length;
                    _HttpWebRequest.Method = "POST";
                    Stream _RequestStream = _HttpWebRequest.GetRequestStream();
                    _RequestStream.Write(bytes, 0, bytes.Length);
                    _RequestStream.Close();
                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        Stream responseStream = _HttpWebResponse.GetResponseStream();
                        string responseStr = new StreamReader(responseStream).ReadToEnd();
                        string SMLPublicKey = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Keys/CoralPay/SmashLabs.Cconnect.Publicx.Key_Test.Txt");
                        string SMLPrivateKey = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Keys/CoralPay/SmashLabs.Cconnect.Privatex.Key_Test.Txt");
                        string Password = "ThankUCash@123";
                        if (HCoreConstant.HostEnvironment == HostEnvironmentType.Live)
                        {
                            SMLPublicKey = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Keys/CoralPay/SmashLabs.Cconnect.Publicx.Key_Live.Txt");
                            SMLPrivateKey = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Keys/CoralPay/SmashLabs.Cconnect.Privatex.Key_Live.Txt");
                            Password = "ThankUCash@123#1321013021@007#5";
                        }
                        var Dec = CoralPay.Cryptography.PGP.Actions.Data.Decrypt(new CoralPay.Cryptography.PGP.Models.DecryptionParam
                        {
                            EncryptedData = responseStr,
                            InternalKeyPassword = Password,
                            InternalPrivateKey = SMLPrivateKey,
                            InternalPublicKey = SMLPublicKey,
                        }).Result;
                        if (Dec.Header.ResponseCode == "00")
                        {
                            var ResponseContent = Dec.Decryption;
                            OCoralPay.OPaymentDetails _Response = JsonConvert.DeserializeObject<OCoralPay.OPaymentDetails>(ResponseContent);
                            if (_Response.responseCode == "00")
                            {
                                return _Response;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            #region  Log Exception
                            HCoreHelper.LogData(HCoreConstant.LogType.Log, "CoralPayVerify", "INVALID KEYGEN 002", null, null);
                            #endregion
                            return null;
                        }
                    }
                    else
                    {
                        #region  Log Exception
                        HCoreHelper.LogData(HCoreConstant.LogType.Log, "CoralPayVerify", "INVALID RESPONSE CODE", null, null);
                        #endregion
                        return null;
                    }
                }
                else
                {
                    #region  Log Exception
                    HCoreHelper.LogData(HCoreConstant.LogType.Log, "CoralPayVerify", "INVALID KEYGEN 001", null, null);
                    #endregion
                    return null;
                }



            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("CoralPayVerify", _Exception, UserReference);
                return null;
                #endregion
            }
            #endregion

        }

        /// <summary>
        /// Description: Buy point payment initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse BuyPointInitialize(OBuyPoint.Initialize.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.Amount <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1382, TUCCoreResource.CA1382M);
                }
                if (string.IsNullOrEmpty(_Request.PaymentMode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1383, TUCCoreResource.CA1383M);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var CustomerDetails = _HCoreContext.HCUAccount
                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                        && x.Id == _Request.AuthAccountId)
                           .FirstOrDefault();
                        if (CustomerDetails != null)
                        {
                            if (CustomerDetails.StatusId == HelperStatus.Default.Active)
                            {
                                string GroupKey = "TPP" + _Request.AuthAccountId + "O" + HCoreHelper.GenerateDateString();
                                string ReferenceNumber = GroupKey;
                                string ReferenceCode = null;
                                string ExtInvNo = null;
                                if (_Request.PaymentMode == "ussd")
                                {
                                    OCoralPay.OResponseDetails CoralPayResponse = CoralPayInitialize(GroupKey, _Request.Amount, _Request.UserReference);
                                    if (CoralPayResponse != null)
                                    {
                                        ExtInvNo = CoralPayResponse.TransactionID;
                                        ReferenceCode = CoralPayResponse.Reference;
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1242, TUCCoreResource.CA1242M);
                                    }
                                }
                                _BuyPointResponse = new OBuyPoint.Initialize.Response();
                                if (_Request.PaymentMode == "flutterwave")
                                {
                                    var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.MerchantId && (x.AccountTypeId == UserAccountType.Merchant || x.AccountTypeId == UserAccountType.MerchantStore))
                                                          .Select(x => new
                                                          {
                                                              x.DisplayName,
                                                              x.IconStorage.Path
                                                          }).FirstOrDefault();
                                    if (MerchantDetails == null)
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "M0404", "Merchant details not found.");
                                    }

                                    string? DealTitle = _HCoreContext.MDDeal.Where(x => x.Id == _Request.DealId && x.Guid == _Request.DealKey).Select(x => x.Title).FirstOrDefault();
                                    if (DealTitle == null)
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "D0404", "Deal details not found.");
                                    }

                                    _BuyPointResponse.MobileNumber = CustomerDetails.MobileNumber;
                                    _BuyPointResponse.DisplayName = CustomerDetails.DisplayName;
                                    _BuyPointResponse.MerchantDisplayName = MerchantDetails.DisplayName;
                                    _BuyPointResponse.PaymentDescription = "Payment for " + DealTitle;

                                    if (!string.IsNullOrEmpty(MerchantDetails.Path))
                                    {
                                        _BuyPointResponse.IconUrl = _AppConfig.StorageUrl + MerchantDetails.Path;
                                    }
                                    else
                                    {
                                        _BuyPointResponse.IconUrl = _AppConfig.Default_Icon;
                                    }
                                }

                                _BuyPointResponse.Amount = _Request.Amount;
                                double Amount = _Request.Amount;

                                if (!string.IsNullOrEmpty(_Request.PromoCode))
                                {
                                    var PromoCodeDetails = _HCoreContext.MDDealPromoCode.Where(x => x.Code == _Request.PromoCode && x.StatusId == HelperStatus.Default.Active)
                                        .Select(x => new
                                        {
                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,
                                            AccountId = x.AccountId,
                                            CategoryId = x.CategoryId,
                                            DealId = x.DealId,
                                            PromoCode = x.Code,
                                            IsForAllDeals = x.IsAllDeals,
                                            ValueType = x.ValueType.SystemName,
                                            Value = x.Value
                                        }).FirstOrDefault();
                                    if (PromoCodeDetails != null)
                                    {
                                        if (PromoCodeDetails.AccountId > 0)
                                        {
                                            if (PromoCodeDetails.AccountId != CustomerDetails.Id)
                                            {
                                                _HCoreContext.Dispose();
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCPINVCODE", "Invalid promo code used");
                                            }
                                            if (PromoCodeDetails.ValueType == "promocodevaluetype.amount")
                                            {
                                                Amount = _Request.Amount - PromoCodeDetails.Value;
                                                _BuyPointResponse.Amount = Amount;
                                                _BuyPointResponse.PromoCodeId = PromoCodeDetails.ReferenceId;
                                                _BuyPointResponse.PromoCodeKey = PromoCodeDetails.ReferenceKey;
                                            }
                                            if (PromoCodeDetails.ValueType == "promocodevaluetype.percentage")
                                            {
                                                double? PercentageAmount = (_Request.Amount * PromoCodeDetails.Value) / 100;
                                                Amount = (double)(_Request.Amount - PercentageAmount);
                                                _BuyPointResponse.Amount = Amount;
                                                _BuyPointResponse.PromoCodeId = PromoCodeDetails.ReferenceId;
                                                _BuyPointResponse.PromoCodeKey = PromoCodeDetails.ReferenceKey;
                                            }
                                        }
                                        else if (PromoCodeDetails.CategoryId > 0)
                                        {
                                            int? CategoryId = _HCoreContext.MDDeal.Where(x => x.Id == _Request.DealId && x.Guid == _Request.DealKey).Select(x => x.CategoryId).FirstOrDefault();
                                            if (PromoCodeDetails.CategoryId != CategoryId)
                                            {
                                                _HCoreContext.Dispose();
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCPINVCODE", "Invalid promo code used");
                                            }
                                            if (PromoCodeDetails.ValueType == "promocodevaluetype.amount")
                                            {
                                                Amount = _Request.Amount - PromoCodeDetails.Value;
                                                _BuyPointResponse.Amount = Amount;
                                                _BuyPointResponse.PromoCodeId = PromoCodeDetails.ReferenceId;
                                                _BuyPointResponse.PromoCodeKey = PromoCodeDetails.ReferenceKey;
                                            }
                                            if (PromoCodeDetails.ValueType == "promocodevaluetype.percentage")
                                            {
                                                double? PercentageAmount = (_Request.Amount * PromoCodeDetails.Value) / 100;
                                                Amount = (double)(_Request.Amount - PercentageAmount);
                                                _BuyPointResponse.Amount = Amount;
                                                _BuyPointResponse.PromoCodeId = PromoCodeDetails.ReferenceId;
                                                _BuyPointResponse.PromoCodeKey = PromoCodeDetails.ReferenceKey;
                                            }
                                        }
                                        else if (PromoCodeDetails.DealId > 0)
                                        {
                                            var _Deals = _HCoreContext.MDDealPromoCode.Where(x => x.Code == PromoCodeDetails.PromoCode).ToList();
                                            if (_Deals.Count > 0)
                                            {
                                                bool IsThisDeal = _Deals.Any(x => x.DealId == _Request.DealId);
                                                if (!IsThisDeal)
                                                {
                                                    _HCoreContext.Dispose();
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCPINVCODE", "Invalid promo code used");
                                                }
                                                if (PromoCodeDetails.ValueType == "promocodevaluetype.amount")
                                                {
                                                    Amount = _Request.Amount - PromoCodeDetails.Value;
                                                    _BuyPointResponse.Amount = Amount;
                                                    _BuyPointResponse.PromoCodeId = PromoCodeDetails.ReferenceId;
                                                    _BuyPointResponse.PromoCodeKey = PromoCodeDetails.ReferenceKey;
                                                }
                                                if (PromoCodeDetails.ValueType == "promocodevaluetype.percentage")
                                                {
                                                    double? PercentageAmount = (_Request.Amount * PromoCodeDetails.Value) / 100;
                                                    Amount = (double)(_Request.Amount - PercentageAmount);
                                                    _BuyPointResponse.Amount = Amount;
                                                    _BuyPointResponse.PromoCodeId = PromoCodeDetails.ReferenceId;
                                                    _BuyPointResponse.PromoCodeKey = PromoCodeDetails.ReferenceKey;
                                                }
                                            }
                                            else
                                            {
                                                _HCoreContext.Dispose();
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCPINVCODE", "Invalid promo code used");
                                            }
                                        }
                                        else if (PromoCodeDetails.IsForAllDeals == 1)
                                        {
                                            if (PromoCodeDetails.ValueType == "promocodevaluetype.amount")
                                            {
                                                Amount = _Request.Amount - PromoCodeDetails.Value;
                                                _BuyPointResponse.Amount = Amount;
                                                _BuyPointResponse.PromoCodeId = PromoCodeDetails.ReferenceId;
                                                _BuyPointResponse.PromoCodeKey = PromoCodeDetails.ReferenceKey;
                                            }
                                            if (PromoCodeDetails.ValueType == "promocodevaluetype.percentage")
                                            {
                                                double? PercentageAmount = (_Request.Amount * PromoCodeDetails.Value) / 100;
                                                Amount = (double)(_Request.Amount - PercentageAmount);
                                                _BuyPointResponse.Amount = Amount;
                                                _BuyPointResponse.PromoCodeId = PromoCodeDetails.ReferenceId;
                                                _BuyPointResponse.PromoCodeKey = PromoCodeDetails.ReferenceKey;
                                            }
                                        }
                                        else
                                        {
                                            _HCoreContext.Dispose();
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCPINVCODE", "Invalid promo code used");
                                        }
                                    }
                                    else
                                    {
                                        _HCoreContext.Dispose();
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCPINVCODE", "Invalid promo code used");
                                    }
                                }

                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.CustomerId = _Request.AuthAccountId;
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Initialized;
                                _CoreTransactionRequest.GroupKey = GroupKey;
                                _CoreTransactionRequest.ParentId = SystemAccounts.ThankUCashSystemId;
                                _CoreTransactionRequest.InvoiceAmount = Amount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = Amount;
                                _CoreTransactionRequest.ReferenceNumber = ReferenceNumber;
                                _CoreTransactionRequest.InvoiceNumber = ExtInvNo;
                                _CoreTransactionRequest.CreatedById = _Request.AuthAccountId;
                                _CoreTransactionRequest.ReferenceAmount = Amount;
                                if (_Request.PaymentMode == "ussd")
                                {
                                    _CoreTransactionRequest.PaymentMethodId = PaymentMethod.Ussd;
                                }
                                else
                                {
                                    _CoreTransactionRequest.PaymentMethodId = PaymentMethod.Card;
                                }
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = SystemAccounts.TUCVas,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = TransactionType.TUCWalletTopup,
                                    SourceId = TransactionSource.GiftPoints,
                                    Amount = Amount,
                                    Charge = 0,
                                    TotalAmount = Amount,
                                    Comment = ReferenceCode,
                                });
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = _Request.AuthAccountId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionType.TUCWalletTopup,
                                    SourceId = TransactionSource.TUC,
                                    Amount = Amount,
                                    Charge = 0,
                                    TotalAmount = Amount,
                                    Comment = ReferenceCode,
                                });
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                    _BuyPointResponse.Charge = 0;
                                    if (string.IsNullOrEmpty(CustomerDetails.EmailAddress))
                                    {
                                        _BuyPointResponse.EmailAddress = _HCoreContext.HCCoreAddress.Where(x => x.AccountId == CustomerDetails.Id).Select(s => s.EmailAddress).FirstOrDefault();
                                    }
                                    else { _BuyPointResponse.EmailAddress = CustomerDetails.EmailAddress; }

                                    _BuyPointResponse.TotalAmount = _BuyPointResponse.Amount + _BuyPointResponse.Charge;
                                    _BuyPointResponse.PaymentMode = _Request.PaymentMode;
                                    _BuyPointResponse.PaymentReference = TransactionResponse.GroupKey;
                                    _BuyPointResponse.PaymentReference = TransactionResponse.GroupKey;
                                    if (HostEnvironment == HostEnvironmentType.Live)
                                    {
                                        _BuyPointResponse.FlutterwaveKey = "FLWPUBK-ee5bad9eb83038f3624d909dac59874b-X";

                                        if (CustomerDetails.CountryId == 1) // NG
                                        {
                                            _BuyPointResponse.PaystackKey = "pk_live_4acd36db0e852af843e16e83e59e7dc0f89efe12";
                                        }
                                        else // GH
                                        {
                                            _BuyPointResponse.PaystackKey = "pk_live_e4d4c71f582f3bcbf1843ddce1574ca208792c3a";
                                        }
                                    }
                                    else if (HostEnvironment == HostEnvironmentType.Test || HostEnvironment == HostEnvironmentType.Tech || HostEnvironment == HostEnvironmentType.Dev)
                                    {
                                        _BuyPointResponse.FlutterwaveKey = "FLWPUBK_TEST-a6e984618172d190fd60d61b15673910-X";

                                        if (CustomerDetails.CountryId == 1) // NG
                                        {
                                            _BuyPointResponse.PaystackKey = "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6";
                                        }
                                        else // GH
                                        {
                                            _BuyPointResponse.PaystackKey = "pk_test_db0fe4ae38aea0e08b400500e38aaa22834140ac";
                                        }
                                    }

                                    if (_Request.PaymentMode == "ussd")
                                    {
                                        #region Banks List
                                        _BankList = new List<OBuyPoint.Initialize.BanksList>();
                                        _BankList.Add(new OBuyPoint.Initialize.BanksList
                                        {
                                            SystemName = "accessbank",
                                            Name = "Access ",
                                            IconUrl = "http://cdn.thankucash.com/banks/access.png",
                                            LocalUrl = "../../../assets/contents/banks/access.png",
                                            Code = "*901*000*REFCODE#",
                                            BankCode = "*901#",
                                            DialCode = "*901*000*" + ReferenceCode + "#",
                                        });
                                        _BankList.Add(new OBuyPoint.Initialize.BanksList
                                        {
                                            SystemName = "ecobank",
                                            Name = "Eco Bank",
                                            IconUrl = "http://cdn.thankucash.com/banks/eco.png",
                                            LocalUrl = "../../../assets/contents/banks/eco.png",
                                            Code = "*326*000*REFCODE#",
                                            BankCode = "*326#",
                                            DialCode = "*326*000*" + ReferenceCode + "#",
                                        });
                                        _BankList.Add(new OBuyPoint.Initialize.BanksList
                                        {
                                            SystemName = "first",
                                            Name = "First",
                                            IconUrl = "http://cdn.thankucash.com/banks/first.png",
                                            LocalUrl = "../../../assets/contents/banks/first.png",
                                            Code = "*894*000*REFCODE#",
                                            BankCode = "*894#",
                                            DialCode = "*894*000*" + ReferenceCode + "#",
                                        });
                                        _BankList.Add(new OBuyPoint.Initialize.BanksList
                                        {
                                            SystemName = "fidelity",
                                            Name = "Fidelity",
                                            IconUrl = "http://cdn.thankucash.com/banks/fidelity.jpeg",
                                            LocalUrl = "../../../assets/contents/banks/fidelity.jpeg",
                                            Code = "*770*000*REFCODE#",
                                            BankCode = "*770#",
                                            DialCode = "*770*000*" + ReferenceCode + "#",
                                        });
                                        _BankList.Add(new OBuyPoint.Initialize.BanksList
                                        {
                                            SystemName = "gtbank",
                                            Name = "GT",
                                            IconUrl = "http://cdn.thankucash.com/banks/gtbank.png",
                                            LocalUrl = "../../../assets/contents/banks/gt.png",
                                            Code = "*737*000*REFCODE#",
                                            BankCode = "*737#",
                                            DialCode = "*737*000*" + ReferenceCode + "#",
                                        });
                                        _BankList.Add(new OBuyPoint.Initialize.BanksList
                                        {
                                            SystemName = "heritage",
                                            Name = "Heritage",
                                            IconUrl = "http://cdn.thankucash.com/banks/heritage.png",
                                            LocalUrl = "../../../assets/contents/banks/heritage.png",
                                            Code = "*745*000*REFCODE#",
                                            BankCode = "*745#",
                                            DialCode = "*745*000*" + ReferenceCode + "#",
                                        });
                                        _BankList.Add(new OBuyPoint.Initialize.BanksList
                                        {
                                            SystemName = "keystone",
                                            Name = "Keystone",
                                            IconUrl = "http://cdn.thankucash.com/banks/keystone.png",
                                            LocalUrl = "../../../assets/contents/banks/keystone.png",
                                            Code = "*7111*000*REFCODE#",
                                            BankCode = "*7111#",
                                            DialCode = "*7111*000*" + ReferenceCode + "#",
                                        });
                                        _BankList.Add(new OBuyPoint.Initialize.BanksList
                                        {
                                            SystemName = "stanbic",
                                            Name = "Stanbic",
                                            IconUrl = "http://cdn.thankucash.com/banks/stanbic.png",
                                            LocalUrl = "../../../assets/contents/banks/stanbic.png",
                                            Code = "*909*000*REFCODE#",
                                            BankCode = "*909#",
                                            DialCode = "*909*000*" + ReferenceCode + "#",
                                        });
                                        _BankList.Add(new OBuyPoint.Initialize.BanksList
                                        {
                                            SystemName = "sterling",
                                            Name = "Sterling",
                                            IconUrl = "http://cdn.thankucash.com/banks/sterling.png",
                                            LocalUrl = "../../../assets/contents/banks/sterling.png",
                                            Code = "*822*000*REFCODE#",
                                            BankCode = "*822#",
                                            DialCode = "*822*000*" + ReferenceCode + "#",
                                        });
                                        _BankList.Add(new OBuyPoint.Initialize.BanksList
                                        {
                                            SystemName = "uba",
                                            Name = "Uba",
                                            IconUrl = "http://cdn.thankucash.com/banks/uba.png",
                                            LocalUrl = "../../../assets/contents/banks/uba.png",
                                            Code = "*919*000*REFCODE#",
                                            BankCode = "*919#",
                                            DialCode = "*919*000*" + ReferenceCode + "#",
                                        });
                                        _BankList.Add(new OBuyPoint.Initialize.BanksList
                                        {
                                            SystemName = "unity",
                                            Name = "Unity",
                                            IconUrl = "http://cdn.thankucash.com/banks/unity.png",
                                            LocalUrl = "../../../assets/contents/banks/unity.png",
                                            Code = "*7799*000*REFCODE#",
                                            BankCode = "*7799#",
                                            DialCode = "*7799*000*" + ReferenceCode + "#",
                                        });
                                        _BankList.Add(new OBuyPoint.Initialize.BanksList
                                        {
                                            SystemName = "wema",
                                            Name = "Wema",
                                            IconUrl = "http://cdn.thankucash.com/banks/wema.png",
                                            LocalUrl = "../../../assets/contents/banks/wema.png",
                                            Code = "*945*000*REFCODE#",
                                            BankCode = "*945#",
                                            DialCode = "*945*000*" + ReferenceCode + "#",
                                        });
                                        _BankList.Add(new OBuyPoint.Initialize.BanksList
                                        {
                                            SystemName = "zenith",
                                            Name = "Zenith",
                                            IconUrl = "http://cdn.thankucash.com/banks/zenith.png",
                                            LocalUrl = "../../../assets/contents/banks/zenith.png",
                                            Code = "*966*000*REFCODE#",
                                            BankCode = "*966#",
                                            DialCode = "*966*000*" + ReferenceCode + "#",
                                        });
                                        #endregion
                                        _BuyPointResponse.Banks = _BankList;
                                    }
                                    else
                                    {
                                        //using (_HCoreContext = new HCoreContext())
                                        //{
                                        //    _BuyPointResponse.Cards = _HCoreContext.HCUAccountParameter.Where(x => x.TypeId == 465
                                        //    && x.AccountId == _Request.AuthAccountId
                                        //    && x.StatusId == HelperStatus.Default.Active)
                                        //       .OrderByDescending(x => x.ModifyDate)
                                        //       .Select(x => new OBuyPoint.Initialize.Card
                                        //       {
                                        //           ReferenceId = x.Id,
                                        //           ReferenceKey = x.Guid,
                                        //           CardBrand = x.Name,
                                        //           CardBin = x.SystemName,
                                        //           CardLast4 = x.Value,
                                        //           ExpMonth = x.SubValue,
                                        //           ExpYear = x.Description
                                        //       }).ToList();
                                        //    _HCoreContext.Dispose();
                                        //}
                                    }
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BuyPointResponse, TUCCoreResource.CA1238, TUCCoreResource.CA1238M);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1385, TUCCoreResource.CA1385M);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1384, TUCCoreResource.CA1384M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "BuyPointInitialize", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Verifies the payment.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse BuyPointVerify(OBuyPoint.Verify.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.PaymentReference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1239, TUCCoreResource.CA1239M);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var PaymentStatus = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.Guid == _Request.PaymentReference)
                            .Select(x => new
                            {
                                Amount = x.Amount,
                                Charge = x.Charge,
                                TotalAmount = x.TotalAmount,
                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                                TransactionDate = x.TransactionDate,
                                PaymentReference = x.Guid,
                                ReferenceId = x.Id,
                                PaymentModeId = x.PaymentMethodId,
                                ExtInvNo = x.InoviceNumber,
                                UserAccountId = x.AccountId,
                            })
                            .FirstOrDefault();
                        if (PaymentStatus != null)
                        {

                            //var Transactions = _HCoreContext.HCUAccountTransaction
                            //  .Where(x => x.GroupKey == _Request.PaymentReference)
                            //  .ToList();
                            //foreach (var Transaction in Transactions)
                            //{
                            //    Transaction.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //    Transaction.StatusId = HelperStatus.Transaction.Success;
                            //}
                            if (PaymentStatus.StatusId == Transaction.Success)
                            {
                                _HCoreContext.Dispose();
                                OBalance.Request _BalanceRequest = new OBalance.Request();
                                _BalanceRequest.UserAccountId = (long)PaymentStatus.UserAccountId;
                                _BalanceRequest.UserAccountKey = _Request.UserReference.AccountKey;
                                _BalanceRequest.Source = "transaction.source.app";
                                _BalanceRequest.UserReference = _Request.UserReference;

                                _ManageCoreTransaction = new ManageCoreTransaction();
                                var BalanceResponse = _ManageCoreTransaction.GetUserAccountBalance(_BalanceRequest);
                                var _ResponseData = new
                                {
                                    Amount = PaymentStatus.Amount,
                                    Charge = PaymentStatus.Charge,
                                    TotalAmount = PaymentStatus.TotalAmount,
                                    ReferenceId = PaymentStatus.ReferenceId,
                                    PaymentReference = PaymentStatus.PaymentReference,
                                    TransactionDate = PaymentStatus.TransactionDate,
                                    StatusName = PaymentStatus.StatusName,
                                    StatusCode = PaymentStatus.StatusCode,
                                    Balance = BalanceResponse.Result
                                };
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ResponseData, TUCCoreResource.CA1243, TUCCoreResource.CA1243M);
                            }
                            else
                            {
                                if (PaymentStatus.StatusId == Transaction.Initialized)
                                {
                                    if (PaymentStatus.PaymentModeId == PaymentMethod.Ussd)
                                    {
                                        var _CpResponse = CoralPayVerify(PaymentStatus.ExtInvNo, PaymentStatus.Amount, _Request.UserReference);
                                        if (_CpResponse != null && _CpResponse.responseCode == "00" && _CpResponse.responsemessage == "Success")
                                        {
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                var Transactions = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.ReferenceNumber == _Request.PaymentReference && x.StatusId == Transaction.Initialized)
                                                    .ToList();
                                                if (Transactions != null && Transactions.Count > 0)
                                                {
                                                    var fItem = Transactions.FirstOrDefault();
                                                    if (fItem.StatusId == Transaction.Success)
                                                    {
                                                        _HCoreContext.Dispose();
                                                        OBalance.Request _BalanceRequest = new OBalance.Request();
                                                        _BalanceRequest.UserAccountId = _Request.UserReference.AccountId;
                                                        _BalanceRequest.UserAccountKey = _Request.UserReference.AccountKey;
                                                        _BalanceRequest.Source = "transaction.source.app";
                                                        _BalanceRequest.UserReference = _Request.UserReference;
                                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                                        var BalanceResponse = _ManageCoreTransaction.GetUserAccountBalance(_BalanceRequest);
                                                        var _ResponseDataN = new
                                                        {
                                                            Amount = PaymentStatus.Amount,
                                                            Charge = PaymentStatus.Charge,
                                                            TotalAmount = PaymentStatus.TotalAmount,
                                                            ReferenceId = PaymentStatus.ReferenceId,
                                                            PaymentReference = PaymentStatus.PaymentReference,
                                                            TransactionDate = PaymentStatus.TransactionDate,
                                                            StatusName = "Success",
                                                            StatusCode = "transaction.success",
                                                            Balance = BalanceResponse.Result
                                                        };
                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ResponseDataN, TUCCoreResource.CA1243, TUCCoreResource.CA1243M);
                                                    }
                                                    else if (fItem.StatusId == Transaction.Initialized)
                                                    {
                                                        foreach (var Transaction in Transactions)
                                                        {
                                                            Transaction.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                            Transaction.StatusId = HelperStatus.Transaction.Success;
                                                        }
                                                        _HCoreContext.SaveChanges();
                                                        var CustomerPayment = Transactions.Where(x => x.Guid != x.GroupKey).FirstOrDefault();
                                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                                        double Balance = _ManageCoreTransaction.GetAppUserBalance((long)CustomerPayment.AccountId, true);
                                                        using (_HCoreContext = new HCoreContext())
                                                        {
                                                            var CustomerDeviceDetails = _HCoreContext.HCUAccountSession
                                                            .Where(x => x.AccountId == Transactions.FirstOrDefault().CustomerId)
                                                            .Select(x => new
                                                            {
                                                                NotificationUrl = x.NotificationUrl,
                                                                UserAccountId = x.AccountId,
                                                            }).FirstOrDefault();
                                                            if (CustomerDeviceDetails != null)
                                                            {
                                                                var _ResponseDataItem = new
                                                                {
                                                                    Amount = CustomerPayment.Amount,
                                                                    Charge = CustomerPayment.Charge,
                                                                    TotalAmount = CustomerPayment.TotalAmount,
                                                                    ReferenceId = CustomerPayment.Id,
                                                                    PaymentReference = CustomerPayment.ReferenceNumber,
                                                                    TransactionDate = HCoreHelper.GetGMTDateTime(),
                                                                    StatusName = "Success",
                                                                    StatusCode = "transaction.success"
                                                                };
                                                                if (!string.IsNullOrEmpty(CustomerDeviceDetails.NotificationUrl))
                                                                {
                                                                    HCoreHelper.SendPushToDevice(CustomerDeviceDetails.NotificationUrl, "pointpurchasereceipt", "N" + CustomerPayment.Amount + " points credited", "N" + CustomerPayment.Amount + " points credited to your account", "pointpurchasereceipt", CustomerPayment.Id, CustomerPayment.Guid, "View details", true, _ResponseDataItem, null);
                                                                }
                                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ResponseDataItem, TUCCoreResource.CA1243, TUCCoreResource.CA1243M);
                                                            }
                                                        }
                                                    }
                                                }
                                            }


                                        }
                                    }
                                    else if (PaymentStatus.PaymentModeId == PaymentMethod.Card)
                                    {
                                        if (_Request.PaymentMode == "paystack")
                                        {
                                            var _StatusCheck = PaystackTransaction.GetTransaction(PaymentStatus.PaymentReference, _AppConfig.PaystackPrivateKey);
                                            if (_StatusCheck != null)
                                            {
                                                if (_StatusCheck.status == "success")
                                                {
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        var Transactions = _HCoreContext.HCUAccountTransaction
                                                            .Where(x => x.ReferenceNumber == _Request.PaymentReference)
                                                            .ToList();
                                                        if (Transactions != null && Transactions.Count > 0)
                                                        {
                                                            var fItem = Transactions.FirstOrDefault();
                                                            if (fItem.StatusId == Transaction.Success)
                                                            {
                                                                _HCoreContext.Dispose();
                                                                OBalance.Request _BalanceRequest = new OBalance.Request();
                                                                _BalanceRequest.UserAccountId = _Request.UserReference.AccountId;
                                                                _BalanceRequest.UserAccountKey = _Request.UserReference.AccountKey;
                                                                _BalanceRequest.Source = "transaction.source.app";
                                                                _BalanceRequest.UserReference = _Request.UserReference;
                                                                _ManageCoreTransaction = new ManageCoreTransaction();
                                                                var BalanceResponse = _ManageCoreTransaction.GetUserAccountBalance(_BalanceRequest);
                                                                var _ResponseDataN = new
                                                                {
                                                                    Amount = PaymentStatus.Amount,
                                                                    Charge = PaymentStatus.Charge,
                                                                    TotalAmount = PaymentStatus.TotalAmount,
                                                                    ReferenceId = PaymentStatus.ReferenceId,
                                                                    PaymentReference = PaymentStatus.PaymentReference,
                                                                    TransactionDate = PaymentStatus.TransactionDate,
                                                                    StatusName = "Success",
                                                                    StatusCode = "transaction.success",
                                                                    Balance = BalanceResponse.Result
                                                                };
                                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ResponseDataN, TUCCoreResource.CA1243, TUCCoreResource.CA1243M);
                                                            }
                                                            else if (fItem.StatusId == Transaction.Initialized)
                                                            {
                                                                foreach (var Transaction in Transactions)
                                                                {
                                                                    Transaction.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                                    Transaction.StatusId = HelperStatus.Transaction.Success;
                                                                }
                                                                _HCoreContext.SaveChanges();
                                                                var CustomerPayment = Transactions.Where(x => x.Guid != x.GroupKey).FirstOrDefault();
                                                                _ManageCoreTransaction = new ManageCoreTransaction();
                                                                double Balance = _ManageCoreTransaction.GetAppUserBalance((long)CustomerPayment.AccountId, true);
                                                                using (_HCoreContext = new HCoreContext())
                                                                {
                                                                    var CustomerDeviceDetails = _HCoreContext.HCUAccountSession
                                                                    .Where(x => x.AccountId == Transactions.FirstOrDefault().CustomerId)
                                                                    .Select(x => new
                                                                    {
                                                                        NotificationUrl = x.NotificationUrl,
                                                                        UserAccountId = x.AccountId,
                                                                    }).FirstOrDefault();
                                                                    if (CustomerDeviceDetails != null)
                                                                    {
                                                                        var _ResponseDataItem = new
                                                                        {
                                                                            Amount = CustomerPayment.Amount,
                                                                            Charge = CustomerPayment.Charge,
                                                                            TotalAmount = CustomerPayment.TotalAmount,
                                                                            ReferenceId = CustomerPayment.Id,
                                                                            PaymentReference = CustomerPayment.ReferenceNumber,
                                                                            TransactionDate = HCoreHelper.GetGMTDateTime(),
                                                                            StatusName = "Success",
                                                                            StatusCode = "transaction.success"
                                                                        };
                                                                        if (!string.IsNullOrEmpty(CustomerDeviceDetails.NotificationUrl))
                                                                        {
                                                                            HCoreHelper.SendPushToDevice(CustomerDeviceDetails.NotificationUrl, "pointpurchasereceipt", "N" + CustomerPayment.Amount + " points credited", "N" + CustomerPayment.Amount + " points credited to your account", "pointpurchasereceipt", CustomerPayment.Id, CustomerPayment.Guid, "View details", true, _ResponseDataItem, null);
                                                                        }
                                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ResponseDataItem, TUCCoreResource.CA1243, TUCCoreResource.CA1243M);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                        else if (_Request.PaymentMode == "flutterwave")
                                        {
                                            VerifyTransactionRequest? _VerifyTransactionRequest = new VerifyTransactionRequest();
                                            _VerifyTransactionRequest.Id = _Request.RefTransactionId;
                                            _VerifyTransactionRequest.UserReference = _Request.UserReference;
                                            var _TransactionStatusCheck = FlutterwavePayments.VerifyTransaction(_VerifyTransactionRequest);
                                            if (_TransactionStatusCheck != null)
                                            {
                                                if (_TransactionStatusCheck.data.status == "successful")
                                                {
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        var Transactions = _HCoreContext.HCUAccountTransaction
                                                            .Where(x => x.ReferenceNumber == _Request.PaymentReference)
                                                            .ToList();
                                                        if (Transactions != null && Transactions.Count > 0)
                                                        {
                                                            var fItem = Transactions.FirstOrDefault();
                                                            if (fItem.StatusId == Transaction.Success)
                                                            {
                                                                _HCoreContext.Dispose();
                                                                OBalance.Request _BalanceRequest = new OBalance.Request();
                                                                _BalanceRequest.UserAccountId = _Request.UserReference.AccountId;
                                                                _BalanceRequest.UserAccountKey = _Request.UserReference.AccountKey;
                                                                _BalanceRequest.Source = "transaction.source.app";
                                                                _BalanceRequest.UserReference = _Request.UserReference;
                                                                _ManageCoreTransaction = new ManageCoreTransaction();
                                                                var BalanceResponse = _ManageCoreTransaction.GetUserAccountBalance(_BalanceRequest);
                                                                var _ResponseDataN = new
                                                                {
                                                                    Amount = PaymentStatus.Amount,
                                                                    Charge = PaymentStatus.Charge,
                                                                    TotalAmount = PaymentStatus.TotalAmount,
                                                                    ReferenceId = PaymentStatus.ReferenceId,
                                                                    PaymentReference = PaymentStatus.PaymentReference,
                                                                    TransactionDate = PaymentStatus.TransactionDate,
                                                                    StatusName = "Success",
                                                                    StatusCode = "transaction.success",
                                                                    Balance = BalanceResponse.Result
                                                                };
                                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ResponseDataN, TUCCoreResource.CA1243, TUCCoreResource.CA1243M);
                                                            }
                                                            else if (fItem.StatusId == Transaction.Initialized)
                                                            {
                                                                foreach (var Transaction in Transactions)
                                                                {
                                                                    Transaction.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                                    Transaction.StatusId = HelperStatus.Transaction.Success;
                                                                }
                                                                _HCoreContext.SaveChanges();
                                                                var CustomerPayment = Transactions.Where(x => x.Guid != x.GroupKey).FirstOrDefault();
                                                                _ManageCoreTransaction = new ManageCoreTransaction();
                                                                double Balance = _ManageCoreTransaction.GetAppUserBalance((long)CustomerPayment.AccountId, true);
                                                                using (_HCoreContext = new HCoreContext())
                                                                {
                                                                    var CustomerDeviceDetails = _HCoreContext.HCUAccountSession
                                                                    .Where(x => x.AccountId == Transactions.FirstOrDefault().CustomerId)
                                                                    .Select(x => new
                                                                    {
                                                                        NotificationUrl = x.NotificationUrl,
                                                                        UserAccountId = x.AccountId,
                                                                    }).FirstOrDefault();
                                                                    if (CustomerDeviceDetails != null)
                                                                    {
                                                                        var _ResponseDataItem = new
                                                                        {
                                                                            Amount = CustomerPayment.Amount,
                                                                            Charge = CustomerPayment.Charge,
                                                                            TotalAmount = CustomerPayment.TotalAmount,
                                                                            ReferenceId = CustomerPayment.Id,
                                                                            PaymentReference = CustomerPayment.ReferenceNumber,
                                                                            TransactionDate = HCoreHelper.GetGMTDateTime(),
                                                                            StatusName = "Success",
                                                                            StatusCode = "transaction.success"
                                                                        };
                                                                        if (!string.IsNullOrEmpty(CustomerDeviceDetails.NotificationUrl))
                                                                        {
                                                                            HCoreHelper.SendPushToDevice(CustomerDeviceDetails.NotificationUrl, "pointpurchasereceipt", "N" + CustomerPayment.Amount + " points credited", "N" + CustomerPayment.Amount + " points credited to your account", "pointpurchasereceipt", CustomerPayment.Id, CustomerPayment.Guid, "View details", true, _ResponseDataItem, null);
                                                                        }
                                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ResponseDataItem, TUCCoreResource.CA1243, TUCCoreResource.CA1243M);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                var _ResponseData = new
                                {
                                    Amount = PaymentStatus.Amount,
                                    Charge = PaymentStatus.Charge,
                                    TotalAmount = PaymentStatus.TotalAmount,
                                    ReferenceId = PaymentStatus.ReferenceId,
                                    PaymentReference = PaymentStatus.PaymentReference,
                                    TransactionDate = PaymentStatus.TransactionDate,
                                    StatusName = PaymentStatus.StatusName,
                                    StatusCode = PaymentStatus.StatusCode,
                                };
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ResponseData, TUCCoreResource.CA1244, TUCCoreResource.CA1244M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "BuyPointVerify", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Cancels the payment.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse BuyPointCancel(OBuyPoint.Cancel.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.PaymentReference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1239, TUCCoreResource.CA1239M);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var Transactions = _HCoreContext.HCUAccountTransaction.Where(x => x.StatusId == HelperStatus.Transaction.Initialized && x.GroupKey == _Request.PaymentReference && x.CustomerId == _Request.AuthAccountId).ToList();
                        if (Transactions.Count > 0)
                        {
                            if (Transactions.FirstOrDefault().StatusId == HelperStatus.Transaction.Initialized)
                            {
                                foreach (var Transaction in Transactions)
                                {
                                    Transaction.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    Transaction.ModifyById = _Request.AuthAccountId;
                                    Transaction.StatusId = HelperStatus.Transaction.Cancelled;
                                }
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1387, TUCCoreResource.CA1387M);
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1386, TUCCoreResource.CA1386M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }

                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "BuyPointCancel", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Buypoint confirm paystack.
        /// </summary>
        /// <param name="ResponseContent">Content of the response.</param>
        internal void BuyPointConfirmPaystack(string ResponseContent)
        {
            #region Manage Exception
            try
            {
                // HCoreHelper.LogData(HCoreConstant.LogType.Log, "WEBHOOKRECEIVED", HCoreHelper.GenerateDateString(), ResponseContent, HCoreHelper.GenerateDateString());
                OPaystackPayment.Request _Response = JsonConvert.DeserializeObject<OPaystackPayment.Request>(ResponseContent);
                if (_Response.data != null)
                {
                    if (_Response.data.reference.StartsWith("TPP"))
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var Transactions = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.GroupKey == _Response.data.reference && x.StatusId == Transaction.Initialized)
                                .ToList();
                            if (Transactions != null && Transactions.Count > 0)
                            {
                                var CustomerPayment = Transactions.Where(x => x.Guid != x.GroupKey).FirstOrDefault();
                                if (_Response.data.status == "success")
                                {
                                    if (_Response.data.authorization != null)
                                    {
                                        if (_Response.data.authorization.reusable)
                                        {
                                            var CardDetailsCheck = _HCoreContext.HCUAccountParameter.Where(x => x.AccountId == CustomerPayment.AccountId
                                            && x.TypeId == 465
                                            && x.RequestKey == _Response.data.authorization.signature
                                            && x.Value == _Response.data.authorization.last4).FirstOrDefault();
                                            if (CardDetailsCheck == null)
                                            {
                                                _HCUAccountParameter = new HCUAccountParameter();
                                                _HCUAccountParameter.Guid = _Response.data.authorization.authorization_code;
                                                _HCUAccountParameter.TypeId = 465;
                                                _HCUAccountParameter.AccountId = CustomerPayment.AccountId;
                                                _HCUAccountParameter.Name = _Response.data.authorization.brand;
                                                _HCUAccountParameter.SystemName = _Response.data.authorization.bin;
                                                _HCUAccountParameter.Value = _Response.data.authorization.last4;
                                                _HCUAccountParameter.SubValue = _Response.data.authorization.exp_month;
                                                _HCUAccountParameter.Description = _Response.data.authorization.exp_year;
                                                _HCUAccountParameter.Data = _Response.data.authorization.channel;
                                                _HCUAccountParameter.RequestKey = _Response.data.authorization.signature;
                                                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                                _HCUAccountParameter.CreatedById = CustomerPayment.AccountId;
                                                _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                                _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                            }
                                            else
                                            {
                                                CardDetailsCheck.Guid = _Response.data.authorization.authorization_code;
                                                CardDetailsCheck.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            }
                                        }
                                    }
                                    foreach (var Transaction in Transactions)
                                    {
                                        Transaction.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        Transaction.StatusId = HelperStatus.Transaction.Success;
                                    }
                                    _HCoreContext.SaveChanges();
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    double Balance = _ManageCoreTransaction.GetAppUserBalance((long)CustomerPayment.AccountId, true);
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        var CustomerDeviceDetails = _HCoreContext.HCUAccountSession
                                        .Where(x => x.AccountId == Transactions.FirstOrDefault().CustomerId)
                                        .Select(x => new
                                        {
                                            NotificationUrl = x.NotificationUrl,
                                            UserAccountId = x.AccountId,
                                        }).FirstOrDefault();
                                        if (CustomerDeviceDetails != null)
                                        {
                                            var _ResponseData = new
                                            {
                                                Amount = CustomerPayment.Amount,
                                                Charge = CustomerPayment.Charge,
                                                TotalAmount = CustomerPayment.TotalAmount,
                                                ReferenceId = CustomerPayment.Id,
                                                PaymentReference = CustomerPayment.ReferenceNumber,
                                                TransactionDate = HCoreHelper.GetGMTDateTime(),
                                                StatusName = "Success",
                                                StatusCode = "transaction.success"
                                            };
                                            if (!string.IsNullOrEmpty(CustomerDeviceDetails.NotificationUrl))
                                            {
                                                HCoreHelper.SendPushToDevice(CustomerDeviceDetails.NotificationUrl, "pointpurchasereceipt", "N" + CustomerPayment.Amount + " points credited", "N" + CustomerPayment.Amount + " points credited to your account", "pointpurchasereceipt", CustomerPayment.Id, CustomerPayment.Guid, "View details", true, _ResponseData, null);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (var Transaction in Transactions)
                                    {
                                        Transaction.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        Transaction.StatusId = HelperStatus.Transaction.Failed;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "BuyPointConfirmPaystack", _Exception, null);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Buypoint confirm coral pay.
        /// </summary>
        /// <param name="PaymentRequest">The payment request.</param>
        internal void BuyPointConfirmCoralPay(string PaymentRequest)
        {
            try
            {
                string SMLPublicKey = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Keys/CoralPay/SmashLabs.Cconnect.Publicx.Key_Test.Txt");
                string SMLPrivateKey = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Keys/CoralPay/SmashLabs.Cconnect.Privatex.Key_Test.Txt");
                string Password = "ThankUCash@123";
                if (HCoreConstant.HostEnvironment == HostEnvironmentType.Live)
                {
                    SMLPublicKey = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Keys/CoralPay/SmashLabs.Cconnect.Publicx.Key_Live.Txt");
                    SMLPrivateKey = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Keys/CoralPay/SmashLabs.Cconnect.Privatex.Key_Live.Txt");
                    Password = "ThankUCash@123#1321013021@007#5";
                }
                var Dec = CoralPay.Cryptography.PGP.Actions.Data.Decrypt(new CoralPay.Cryptography.PGP.Models.DecryptionParam
                {
                    EncryptedData = PaymentRequest,
                    InternalKeyPassword = Password,
                    InternalPrivateKey = SMLPrivateKey,
                    InternalPublicKey = SMLPublicKey,
                }).Result;
                if (Dec.Header.ResponseCode == "00")
                {
                    var ResponseContent = Dec.Decryption;
                    OCoralPay.OPaymentDetails _Response = JsonConvert.DeserializeObject<OCoralPay.OPaymentDetails>(ResponseContent);
                    if (_Response.responseCode == "00")
                    {
                        HCoreHelper.LogData(HCoreConstant.LogType.Log, "notifycpreceived", HCoreHelper.GetGMTDateTime().ToString(), ResponseContent, null);
                        using (_HCoreContext = new HCoreContext())
                        {
                            var Transactions = _HCoreContext.HCUAccountTransaction
                                .Where(x => x.InoviceNumber == _Response.TransactionID && x.ReferenceNumber == _Response.TraceID && x.StatusId == Transaction.Initialized)
                                .ToList();
                            if (Transactions != null && Transactions.Count > 0)
                            {
                                foreach (var Transaction in Transactions)
                                {
                                    Transaction.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    Transaction.StatusId = HelperStatus.Transaction.Success;
                                }
                                _HCoreContext.SaveChanges();
                                var CustomerPayment = Transactions.Where(x => x.Guid != x.GroupKey).FirstOrDefault();
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                double Balance = _ManageCoreTransaction.GetAppUserBalance((long)CustomerPayment.AccountId, true);
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var CustomerDeviceDetails = _HCoreContext.HCUAccountSession
                                    .Where(x => x.AccountId == Transactions.FirstOrDefault().CustomerId)
                                    .Select(x => new
                                    {
                                        NotificationUrl = x.NotificationUrl,
                                        UserAccountId = x.AccountId,
                                    }).FirstOrDefault();
                                    if (CustomerDeviceDetails != null)
                                    {
                                        var _ResponseData = new
                                        {
                                            Amount = CustomerPayment.Amount,
                                            Charge = CustomerPayment.Charge,
                                            TotalAmount = CustomerPayment.TotalAmount,
                                            ReferenceId = CustomerPayment.Id,
                                            PaymentReference = CustomerPayment.ReferenceNumber,
                                            TransactionDate = HCoreHelper.GetGMTDateTime(),
                                            StatusName = "Success",
                                            StatusCode = "transaction.success"
                                        };
                                        if (!string.IsNullOrEmpty(CustomerDeviceDetails.NotificationUrl))
                                        {
                                            HCoreHelper.SendPushToDevice(CustomerDeviceDetails.NotificationUrl, "pointpurchasereceipt", "N" + CustomerPayment.Amount + " points credited", "N" + CustomerPayment.Amount + " points credited to your account", "pointpurchasereceipt", CustomerPayment.Id, CustomerPayment.Guid, "View details", true, _ResponseData, null);
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("BuyPointConfirmCoralPay", _Exception, null);
                #endregion
            }
        }

        /// <summary>
        /// Description: Gets the paymentcharge.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse BuyPointCharge(OBuyPoint.Charge.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAACCREF, TUCCoreResource.CAACCREFM);
                }
                else if (string.IsNullOrEmpty(_Request.PaymentReference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1239, TUCCoreResource.CA1239M);
                }
                else if (string.IsNullOrEmpty(_Request.AuthMode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1261, TUCCoreResource.CA1261M);
                }
                else if (string.IsNullOrEmpty(_Request.AuthPin))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1262, TUCCoreResource.CA1262M);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AuthAccountId
                        && x.Guid == _Request.UserReference.AccountKey)
                            .Select(x => new
                            {
                                Pin = x.AccessPin,
                                StatusId = x.StatusId,
                                AccountCode = x.AccountCode,
                            })
                            .FirstOrDefault();
                        if (CustomerDetails.StatusId != HelperStatus.Default.Active)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1263, TUCCoreResource.CA1263M);
                        }

                        if (_Request.AuthMode == "device")
                        {
                            if (CustomerDetails.AccountCode != _Request.AuthPin)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1265, TUCCoreResource.CA1265M);
                            }

                        }
                        else if (_Request.AuthMode == "pin")
                        {
                            if (HCoreEncrypt.DecryptHash(CustomerDetails.Pin) != _Request.AuthPin)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1265, TUCCoreResource.CA1265M);
                            }
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1264, TUCCoreResource.CA1264M);
                        }


                        var PaymentStatus = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.Guid == _Request.PaymentReference)
                            .Select(x => new
                            {
                                Amount = x.Amount,
                                Charge = x.Charge,
                                TotalAmount = x.TotalAmount,
                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                                TransactionDate = x.TransactionDate,
                                PaymentReference = x.Guid,
                                ReferenceId = x.Id,
                            })
                            .FirstOrDefault();
                        if (PaymentStatus != null)
                        {
                            if (PaymentStatus.StatusId == Transaction.Initialized)
                            {
                                var ChargeDetails = _HCoreContext.HCUAccountParameter.Where(x => x.Id == _Request.ReferenceId && x.AccountId == _Request.AuthAccountId)
                                    .Select(x => new { AuthCode = x.Guid, x.Account.EmailAddress }).FirstOrDefault();
                                if (ChargeDetails != null)
                                {
                                    var CInfo = _HCoreContext.HCUAccountParameter.Where(x => x.Id == _Request.ReferenceId && x.AccountId == _Request.AuthAccountId)
                                    .FirstOrDefault();
                                    if (CInfo != null)
                                    {
                                        CInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        CInfo.ModifyById = _Request.AuthAccountId;
                                        _HCoreContext.SaveChanges();
                                    }
                                    OChargeData _PayStackResponseData = PaystackTransaction.GetPayStackChargeAuthorization(_AppConfig.PaystackPrivateKey, ChargeDetails.EmailAddress, (double)PaymentStatus.Amount, ChargeDetails.AuthCode, _Request.PaymentReference);
                                    if (_PayStackResponseData.status == true)
                                    {
                                        if (_PayStackResponseData.data.status == "success")
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1254, TUCCoreResource.CA1254M);
                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1253, TUCCoreResource.CA1253M);
                                        }
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1251, TUCCoreResource.CA1251M);
                                    }
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC0001");
                                    #endregion
                                }




                                //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1243, TUCCoreResource.CA1243M);
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1241, TUCCoreResource.CA1241M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "BuyPointVerify", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        internal async Task BuyPointConfirmFlutterwave(string Request)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request))
                {
                    OBuyPoint.WebHook.Response? _Response = JsonConvert.DeserializeObject<OBuyPoint.WebHook.Response?>(Request);
                    if (_Response != null)
                    {
                        if (_Response.txRef.StartsWith("TPP"))
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var Transactions = await _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.GroupKey == _Response.txRef)
                                    .ToListAsync();
                                if (Transactions != null && Transactions.Count > 0)
                                {
                                    var CustomerPayment = Transactions.Where(x => x.Guid != x.GroupKey).FirstOrDefault();
                                    if (_Response.status == "successful")
                                    {
                                        if (_Response.entity != null)
                                        {
                                            var CardDetailsCheck = await _HCoreContext.HCUAccountParameter.Where(x => x.AccountId == CustomerPayment.AccountId
                                            && x.TypeId == 465
                                            && x.Value == _Response.entity.card_last4).FirstOrDefaultAsync();
                                            if (CardDetailsCheck == null)
                                            {
                                                _HCUAccountParameter = new HCUAccountParameter();
                                                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                                _HCUAccountParameter.TypeId = 465;
                                                _HCUAccountParameter.AccountId = CustomerPayment.AccountId;
                                                _HCUAccountParameter.SystemName = _Response.entity.card6;
                                                _HCUAccountParameter.Value = _Response.entity.card_last4;
                                                _HCUAccountParameter.RequestKey = HCoreHelper.GenerateGuid();
                                                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                                _HCUAccountParameter.CreatedById = CustomerPayment.AccountId;
                                                _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                                _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                            }
                                            else
                                            {
                                                CardDetailsCheck.Guid = HCoreHelper.GenerateGuid();
                                                CardDetailsCheck.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            }
                                        }
                                        foreach (var Transaction in Transactions)
                                        {
                                            Transaction.Comment = (_Response.amount).ToString() + "|||" + Transaction.Amount;
                                            Transaction.Amount = (double)(_Response.amount);
                                            Transaction.TotalAmount = (double)_Response.amount;
                                            Transaction.Amount = (double)_Response.amount;
                                            Transaction.PurchaseAmount = (double)_Response.amount;
                                            Transaction.ReferenceAmount = _Response.amount;
                                            Transaction.ReferenceInvoiceAmount = _Response.amount;
                                            Transaction.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            Transaction.StatusId = HelperStatus.Transaction.Success;
                                        }
                                        await _HCoreContext.SaveChangesAsync();
                                        await _HCoreContext.DisposeAsync();
                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                        double Balance = _ManageCoreTransaction.GetAppUserBalance((long)CustomerPayment.AccountId, true);
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var CustomerDeviceDetails = await _HCoreContext.HCUAccountSession
                                            .Where(x => x.AccountId == Transactions.FirstOrDefault().CustomerId)
                                            .Select(x => new
                                            {
                                                NotificationUrl = x.NotificationUrl,
                                                UserAccountId = x.AccountId,
                                            }).FirstOrDefaultAsync();
                                            if (CustomerDeviceDetails != null)
                                            {
                                                var _ResponseData = new
                                                {
                                                    Amount = CustomerPayment.Amount,
                                                    Charge = CustomerPayment.Charge,
                                                    TotalAmount = CustomerPayment.TotalAmount,
                                                    ReferenceId = CustomerPayment.Id,
                                                    PaymentReference = CustomerPayment.ReferenceNumber,
                                                    TransactionDate = HCoreHelper.GetGMTDateTime(),
                                                    StatusName = "Success",
                                                    StatusCode = "transaction.success"
                                                };
                                                if (!string.IsNullOrEmpty(CustomerDeviceDetails.NotificationUrl))
                                                {
                                                    HCoreHelper.SendPushToDevice(CustomerDeviceDetails.NotificationUrl, "pointpurchasereceipt", "N" + CustomerPayment.Amount + " points credited", "N" + CustomerPayment.Amount + " points credited to your account", "pointpurchasereceipt", CustomerPayment.Id, CustomerPayment.Guid, "View details", true, _ResponseData, null);
                                                }
                                            }
                                            await _HCoreContext.DisposeAsync();
                                        }
                                    }
                                    else
                                    {
                                        foreach (var Transaction in Transactions)
                                        {
                                            Transaction.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            Transaction.StatusId = HelperStatus.Transaction.Failed;
                                        }
                                        await _HCoreContext.SaveChangesAsync();
                                        await _HCoreContext.DisposeAsync();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateFlutterwavePaymentStatus", _Exception);
            }
        }
    }
}
