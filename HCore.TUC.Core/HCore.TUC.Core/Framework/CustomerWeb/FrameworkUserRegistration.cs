﻿using System;
using System.Linq;
using System.Text;
using Delivery.Object.Response.Shipments;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Integration.Mailerlite;
using HCore.Integration.Mailerlite.Requests;
using HCore.Integration.Mailerlite.Responses;
using HCore.Operations;
using HCore.Operations.Framework;
using HCore.TUC.Core.Object.CustomerWeb;
using HCore.TUC.Core.Resource;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.TUC.Core.Object.Customer.OOperation;
using static HCore.TUC.Core.Object.CustomerWeb.OUserRegistration;

namespace HCore.TUC.Core.Framework.CustomerWeb
{
    public class FrameworkUserRegistration
    {
        HCUAccountSession _HCUAccountSession;
        UserProfileDetails _OAccount;
        HCoreContext _HCoreContext;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        Random _Random;
        HCCoreVerification _HCCoreVerification;
        OUserRegistration.UserVerificationResponse _VerificationResponse;
        OUserRegistration.ChangeUserAccountPinResponse _UpdatePinResponse;
        ResetUserAccountPinResponse _ResetPinResponse;

        private UserProfileDetails GetUserProfile(long AccountId, bool IsNewAccount, OUserReference _UserReference)
        {
            try
            {
                _OAccount = new UserProfileDetails();
                _OAccount.IsNewAccount = IsNewAccount;
                string UserAccountSessionKey = HCoreHelper.GenerateGuid();
                using (_HCoreContext = new HCoreContext())
                {
                    var UserDetails = (from n in _HCoreContext.HCUAccount
                                       where n.Id == AccountId
                                       select new
                                       {
                                           UserId = n.UserId,
                                           UserKey = n.Guid,

                                           UserAccountId = n.Id,
                                           UserAccountKey = n.Guid,
                                           AccountTypeId = n.AccountTypeId,
                                           DisplayName = n.DisplayName,
                                           Name = n.Name,
                                           CountryId = n.CountryId,
                                           IconUrl = n.IconStorage.Path,
                                           EmailAddress = n.EmailAddress,
                                           MobileNumber = n.MobileNumber,
                                           MobileVerification = n.NumberVerificationStatus,
                                           CreatedDate = n.CreateDate,
                                           ReferralCode = n.ReferralCode,
                                       }).FirstOrDefault();
                    if (UserDetails != null)
                    {
                        string? RequestToken = _HCoreContext.HCCoreVerification.Where(x => x.MobileNumber == UserDetails.MobileNumber).Select(x => x.Guid).FirstOrDefault();
                        if (!string.IsNullOrEmpty(RequestToken))
                        {
                            _OAccount.RequestToken = RequestToken;
                        }
                        _OAccount.Name = UserDetails.Name;
                        _OAccount.DisplayName = UserDetails.DisplayName;
                        _OAccount.IconUrl = UserDetails.IconUrl;
                        _OAccount.EmailAddress = UserDetails.EmailAddress;
                        _OAccount.Country = (from n in _HCoreContext.HCCoreCountry
                                             where n.Id == UserDetails.CountryId
                                             select new OCountry
                                             {
                                                 Iso = n.Iso,
                                                 Isd = n.Isd,
                                                 Name = n.Name,
                                                 Symbol = n.CurrencyHex,
                                             }).FirstOrDefault();
                        if (!string.IsNullOrEmpty(UserDetails.IconUrl))
                        {
                            _OAccount.IconUrl = _AppConfig.StorageUrl + UserDetails.IconUrl;
                        }
                        else
                        {
                            _OAccount.IconUrl = _AppConfig.Default_Icon;
                        }
                        long UserSessionId = 0;
                        DateTime _LogOutDate = HCoreHelper.GetGMTDateTime();
                        var UserActiveSessions = (from n in _HCoreContext.HCUAccountSession
                                                  where n.Account.Id == AccountId && n.StatusId == HelperStatus.Default.Active
                                                  select n).ToList();
                        foreach (var UserActiveSession in UserActiveSessions)
                        {
                            UserActiveSession.LogoutDate = _LogOutDate;
                            UserActiveSession.StatusId = HelperStatus.Default.Active;
                        }
                        _HCoreContext.SaveChanges();
                        using (_HCoreContext = new HCoreContext())
                        {
                            _HCUAccountSession = new HCUAccountSession();
                            _HCUAccountSession.Guid = UserAccountSessionKey;
                            _HCUAccountSession.AccountId = UserDetails.UserAccountId;
                            _HCUAccountSession.PlatformId = 754;
                            _HCUAccountSession.AppVersionId = _UserReference.AppVersionId;
                            _HCUAccountSession.LastActivityDate = HCoreHelper.GetGMTDate();
                            _HCUAccountSession.LoginDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountSession.IPAddress = _UserReference.RequestIpAddress;
                            _HCUAccountSession.Latitude = _UserReference.RequestLatitude;
                            _HCUAccountSession.Longitude = _UserReference.RequestLongitude;
                            _HCUAccountSession.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.HCUAccountSession.Add(_HCUAccountSession);
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();
                            UserSessionId = _HCUAccountSession.Id;
                        }
                        _OAccount.LoginTime = HCoreHelper.GetGMTDateTime();
                        _OAccount.AccountKey = UserDetails.UserAccountKey;
                        _OAccount.AccountId = UserDetails.UserAccountId;
                        _OAccount.CreatedAt = UserDetails.CreatedDate;
                        _OAccount.Key = HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(UserSessionId + "|" + UserAccountSessionKey));
                        if (UserDetails.MobileVerification == 1)
                        {
                            _OAccount.IsMobileNumberVerified = true;
                        }
                        else
                        {
                            _OAccount.IsMobileNumberVerified = false;
                        }
                        using (_HCoreContext = new HCoreContext())
                        {
                            var UserAccountD = (from n in _HCoreContext.HCUAccount
                                                where n.Id == UserDetails.UserAccountId
                                                select n).FirstOrDefault();
                            if (UserAccountD != null)
                            {
                                UserAccountD.LastLoginDate = HCoreHelper.GetGMTDateTime();
                                _HCoreContext.SaveChanges();
                                _HCoreContext.Dispose();
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                            }
                        }
                        if (!string.IsNullOrEmpty(UserDetails.ReferralCode))
                        {
                            _OAccount.ReferralCode = UserDetails.ReferralCode;
                        }
                        else
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var UserAccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == UserDetails.UserAccountId).FirstOrDefault();
                                if (UserAccountDetails != null)
                                {
                                    string? ReferralCode = HCoreHelper.GenerateRandomNumber(10);
                                    UserAccountDetails.ReferralCode = ReferralCode;
                                    UserAccountDetails.ReferralUrl = _AppConfig.WebsiteUrl + $"?refby={ReferralCode}";
                                    _OAccount.ReferralCode = ReferralCode;
                                    _HCoreContext.SaveChanges();
                                    _HCoreContext.Dispose();
                                }
                                else
                                {
                                    _HCoreContext.Dispose();
                                }
                            }
                        }
                        return _OAccount;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetUserProfile", ex);
                return null;
            }
        }

        internal OResponse UserLogIn(OUserRegistration.UserLogInRequest _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR001", "Mobile number required.");
                }
                if (string.IsNullOrEmpty(_Request.AccessPin))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR002", "Access pin required.");
                }
                if (string.IsNullOrEmpty(_Request.CountryIsd))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR003", "Country Isd required.");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    string VMobileNumber = FormatMobileNumber(_Request.MobileNumber);
                    string MobileNumber = HCoreHelper.FormatMobileNumber(_Request.CountryIsd, VMobileNumber, VMobileNumber.Length);
                    string UserName = _AppConfig.AppUserPrefix + MobileNumber;
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => (x.MobileNumber == MobileNumber || x.User.Username == UserName) && x.AccountTypeId == UserAccountType.Appuser).FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        string Pin = HCoreEncrypt.DecryptHash(AccountDetails.AccessPin);
                        if (Pin == _Request.AccessPin)
                        {
                            var UserDetails = GetUserProfile(AccountDetails.Id, false, _Request.UserReference);
                            _HCoreContext.Dispose();
                            //
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, UserDetails, "USR006", "Log in successful.");
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR005", "Invalid login pin.");
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR004", "Invalid mobile number.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UserLogIn", _Exception, _Request.UserReference, "USR0500", "Unable to process your request. Please try after some time.");
            }
        }

        internal OResponse UserRegistration(OUserRegistration.UserRegistration _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR007", "Name required.");
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR007", "Email required.");
                }
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR007", "Mobile Number required.");
                }
                if (string.IsNullOrEmpty(_Request.AccountPin))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR007", "Pin required.");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    // Format mobile number
                    string VMobileNumber = FormatMobileNumber(_Request.MobileNumber);
                    string MobileNumber = HCoreHelper.FormatMobileNumber(_Request.CountryIsd, VMobileNumber, VMobileNumber.Length);
                    string UserName = _AppConfig.AppUserPrefix + MobileNumber;

                    // Check user exists or not
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => (x.MobileNumber == MobileNumber || x.User.Username == UserName) && x.AccountTypeId == UserAccountType.Appuser).FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        _HCoreContext.Dispose();
                        var UserDetails = GetUserProfile(AccountDetails.Id, true, _Request.UserReference);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, UserDetails, "HCV036", "User already exists with this mobile number. Try login into your account or use different mobile number.");
                    }
                    else
                    {
                        // Split name
                        string[] FullName = _Request.Name.Split(" ");
                        string FirstName = string.Empty, LastName = string.Empty, MiddleName = string.Empty;
                        if (FullName.Length == 0 || FullName.Length == 1)
                        {
                            FirstName = _Request.Name;
                        }
                        else if (FullName.Length == 2)
                        {
                            FirstName = FullName[0];
                            LastName = FullName[1];
                        }
                        else if (FullName.Length == 3)
                        {
                            FirstName = FullName[0];
                            MiddleName = FullName[1];
                            LastName = FullName[2];
                        }

                        // Country Details
                        int? CountryId = _HCoreContext.HCCoreCountry.Where(x => x.Isd == _Request.CountryIsd).Select(x => x.Id).FirstOrDefault();

                        // Create username and password
                        _HCUAccountAuth = new HCUAccountAuth();
                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountAuth.Username = UserName;
                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.AccountPin);
                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                        _HCUAccountAuth.CreatedById = 1;
                        _HCoreContext.HCUAccountAuth.Add(_HCUAccountAuth);

                        // Create user account
                        _HCUAccount = new HCUAccount();
                        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                        _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                        _HCUAccount.MobileNumber = MobileNumber;
                        _HCUAccount.DisplayName = _Request.Name;
                        _HCUAccount.Name = _Request.Name;
                        _HCUAccount.FirstName = FirstName;
                        if (!string.IsNullOrEmpty(MiddleName))
                        {
                            _HCUAccount.MiddleName = MiddleName;
                        }
                        if(!string.IsNullOrEmpty(_Request.ReferredBy))
                        {
                            long? OwnerId = _HCoreContext.HCUAccount.Where(x => x.ReferralCode == _Request.ReferredBy && x.AccountTypeId == UserAccountType.Appuser).Select(x => x.Id).FirstOrDefault();
                            if(OwnerId > 0)
                            {
                                _HCUAccount.OwnerId = OwnerId;
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "INREFCODE", "Invalid referral code used.");
                            }
                        }
                        else
                        {
                            _HCUAccount.OwnerId = 3;
                        }
                        string? ReferralCode = HCoreHelper.GenerateRandomNumber(10);
                        _HCUAccount.LastName = LastName;
                        _HCUAccount.EmailAddress = _Request.EmailAddress;
                        _HCUAccount.ReferralCode = ReferralCode;
                        _HCUAccount.ReferralUrl = _AppConfig.WebsiteUrl + $"?refby={ReferralCode}";
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(_Request.AccountPin);
                        _HCUAccount.ContactNumber = MobileNumber;
                        _HCUAccount.CountryId = CountryId;
                        _HCUAccount.EmailVerificationStatus = 1;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.NumberVerificationStatus = 0;

                        _Random = new Random();
                        string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");

                        _HCUAccount.AccountCode = AccountCode;
                        _HCUAccount.RegistrationSourceId = RegistrationSource.DealsWebsite;
                        _HCUAccount.CreatedById = 1;
                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                        _HCUAccount.User = _HCUAccountAuth;
                        if (!string.IsNullOrEmpty(_Request.ReferredBy))
                        {
                            _HCUAccount.ReferrerId = _Request.ReferredBy;
                        }
                        
                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                        _HCoreContext.SaveChanges();


                        _Random = new Random();
                        //String CodeStart = ;
                        String NumberVerificationToken = _Random.Next(1000, 9999).ToString();

                        string AccessKey = HCoreHelper.GenerateGuid();
                        string TMessage = null;
                        TMessage = NumberVerificationToken + " is your verification code";
                        if (MobileNumber == "2349009009000" || MobileNumber == "2348008008000" || MobileNumber == "2347007007000" || MobileNumber == "2346006006000" || MobileNumber == "2345005005000" || MobileNumber == "2344004004000" || MobileNumber == "2341001001000")
                        {
                            NumberVerificationToken = "1247";
                        }
                        if (HostEnvironment == HostEnvironmentType.Dev || HostEnvironment == HostEnvironmentType.Tech || HostEnvironment == HostEnvironmentType.Test)
                        {
                            NumberVerificationToken = "1247";
                        }
                        _HCCoreVerification = new HCCoreVerification();
                        _HCCoreVerification.Guid = HCoreHelper.GenerateGuid();
                        _HCCoreVerification.VerificationTypeId = 1;
                        _HCCoreVerification.CountryIsd = _Request.CountryIsd;
                        _HCCoreVerification.MobileNumber = MobileNumber;
                        _HCCoreVerification.MobileMessage = TMessage;
                        _HCCoreVerification.AccessKey = AccessKey;
                        _HCCoreVerification.AccessCodeStart = "CA";
                        _HCCoreVerification.AccessCode = NumberVerificationToken;
                        _HCCoreVerification.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCCoreVerification.VerifyAttemptCount = 0;
                        _HCCoreVerification.ExpiaryDate = _HCCoreVerification.CreateDate.AddDays(1);
                        _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
                        _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
                        _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
                        _HCCoreVerification.StatusId = 1;
                        _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
                        _HCoreContext.SaveChanges();

                        if (HostEnvironment == HostEnvironmentType.Live)
                        {
                            HCoreHelper.SendSMS(SmsType.Transaction, _Request.CountryIsd, MobileNumber, TMessage, _HCUAccount.Id, null);
                            HCoreHelper.SendSMS(SmsType.Transaction, _Request.CountryIsd, MobileNumber, "Welcome to Thank U Cash: Your Bal: N0. Your redeeming PIN is: " + _Request.AccountPin + ".", _HCUAccount.Id, _HCUAccount.Guid);
                        }

                        _VerificationResponse = new OUserRegistration.UserVerificationResponse();
                        _VerificationResponse.IsNewAccount = true;
                        _VerificationResponse.ReferenceId = _HCCoreVerification.Id;
                        _VerificationResponse.MobileNumber = _HCCoreVerification.MobileNumber;
                        _VerificationResponse.RequestToken = _HCCoreVerification.Guid;

                        #region Mailerlite Injection
                        var mailerlite = new MailerliteImple();
                        var emailObj = new CreateListRequest
                        {
                            name = "New User Onboarding Email",
                            emails = new string[] { _Request.EmailAddress }
                        };
                        var mailerliteResponse = mailerlite.AddEmailToMailerList(emailObj);
                        if (mailerliteResponse != null)
                        {
                            if (mailerliteResponse.StatusCode.Equals(StatusCodes.Status201Created))
                            {
                                _HCoreContext.Dispose();
                            }
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV036", "Verification code sent to  your mobile number. Enter verification code to verify your mobile.");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UserRegistration", _Exception, _Request.UserReference, "USR0500", "Unable to process your request please try after some time.");
            }
        }

        internal OResponse UserVerification(OUserRegistration.UserVerificationRequest _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.AccessCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV038", "Verification code missing");
                }

                if (string.IsNullOrEmpty(_Request.RequestToken))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV039", "Verification token missing");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var RequestDetails = _HCoreContext.HCCoreVerification.Where(x => x.Guid == _Request.RequestToken).FirstOrDefault();
                    if (RequestDetails != null)
                    {
                        if (RequestDetails.StatusId == HelperStatus.Default.Inactive)
                        {
                            DateTime StartDate = HCoreHelper.GetGMTDateTime();
                            if (RequestDetails.ExpiaryDate > StartDate)
                            {
                                if (RequestDetails.AccessCode == _Request.AccessCode)
                                {
                                    string UserName = _AppConfig.AppUserPrefix + RequestDetails.MobileNumber;
                                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && (x.User.Username == UserName || x.MobileNumber == RequestDetails.MobileNumber)).FirstOrDefault();

                                    RequestDetails.StatusId = HelperStatus.Default.Active;
                                    RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                    RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                    RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                    RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                    RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;

                                    AccountDetails.NumberVerificationStatus = 1;
                                    AccountDetails.NumberVerificationStatusDate = HCoreHelper.GetGMTDateTime();

                                    _HCoreContext.SaveChanges();

                                    var UserDetails = GetUserProfile(AccountDetails.Id, true, _Request.UserReference);
                                    _HCoreContext.Dispose();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, UserDetails, "HCV040", "Verification successful");
                                }
                                else
                                {
                                    RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                    _HCoreContext.SaveChanges();
                                    _HCoreContext.Dispose();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV041", "Invalid verification code");
                                }

                            }
                            else
                            {
                                RequestDetails.StatusId = 3;
                                RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                _HCoreContext.SaveChanges();

                                _VerificationResponse = new OUserRegistration.UserVerificationResponse();
                                _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV042", "Verification code expired. Please process verification again");
                            }
                        }
                        else if (RequestDetails.StatusId == 2)
                        {
                            _VerificationResponse = new OUserRegistration.UserVerificationResponse();
                            _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV065", "Invalid status code. Please process verification again");
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV066", "Invalid status code. Please process verification again");
                        }

                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV043", "Invalid request. Please process verification again.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UserVerification", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV044", "Unable to verify request. Please try after some time.");
            }
        }

        internal OResponse UpdateAccountPin(OUserRegistration.ChangeUserAccountPinRequest _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.AuthAccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "USR008", "Account key missing.");
                }
                if (string.IsNullOrEmpty(_Request.OldPin))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "USR009", "Old pin missing");
                }
                if (_Request.OldPin.Length != 4)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "USR010", "Invalid old pin.");
                }
                if (string.IsNullOrEmpty(_Request.NewPin))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "USR011", "New pin missing");
                }
                if (_Request.NewPin.Length != 4)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "USR012", "Invalid new pin.");
                }
                if (_Request.NewPin == _Request.OldPin)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR013", "The new pin and old pin must not be the same.");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.Guid == _Request.AuthAccountKey).FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        string ExistingPin = HCoreEncrypt.DecryptHash(AccountDetails.AccessPin);
                        if (ExistingPin == _Request.NewPin)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR013", "The new pin you have used previously. Please use the pin which you haven't used before.");
                        }
                        if (ExistingPin != _Request.OldPin)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "USR013", "Invalid old pin. Please enter the correct old pin.");
                        }
                        AccountDetails.AccessPin = HCoreEncrypt.EncryptHash(_Request.NewPin);
                        AccountDetails.ModifyById = _Request.UserReference.AccountId;
                        AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();
                        _UpdatePinResponse = new OUserRegistration.ChangeUserAccountPinResponse();
                        _UpdatePinResponse.NewPin = _Request.NewPin;
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _UpdatePinResponse, "USR013", "Your account pin has been updated successfully.");
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "USR0404", "User details not found. It may be removed or not available at the moment.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateAccountPin", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC5000", "Unable to process your request please try after some time.");
            }
            #endregion
        }

        internal OResponse ResendUserverificationPin(OUserRegistration.ResendUserVerificationRequest _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.RequestToken))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR016", "Request token missing.");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
                    DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);

                    string VMobileNumber = FormatMobileNumber(_Request.MobileNumber);
                    string MobileNumber = HCoreHelper.FormatMobileNumber(_Request.CountryIsd, VMobileNumber, VMobileNumber.Length);
                    string UserName = _AppConfig.AppUserPrefix + MobileNumber;

                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => (x.MobileNumber == MobileNumber || x.User.Username == UserName)
                                         && x.AccountTypeId == UserAccountType.Appuser)
                                         .Select(x => new
                                         {
                                             AccountId = x.Id,
                                             AccountKey = x.Guid,
                                             Pin = x.AccessPin
                                         }).FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        string AccountPin = HCoreEncrypt.DecryptHash(AccountDetails.Pin);
                        var Details = _HCoreContext.HCCoreVerification
                                      .Where(x => x.Guid == _Request.RequestToken
                                      && x.CreateDate > StartDate && x.CreateDate < EndDate
                                      && x.StatusId == HelperStatus.Default.Inactive
                                      && x.VerifyDate == null)
                                      .Select(x => new
                                      {
                                          ReferenceId = x.Id,
                                          RequestToken = x.Guid,
                                          AccessCode = x.AccessCode,
                                          AccessCodeStart = x.AccessCodeStart,
                                          MobileMessage = x.MobileMessage,
                                          MobileNumber = x.MobileNumber,
                                          ReferenceKey = x.ReferenceKey,
                                          CountryIsd = x.CountryIsd,
                                          Type = x.VerificationTypeId,
                                      }).FirstOrDefault();
                        if (Details != null)
                        {
                            if (HostEnvironment == HostEnvironmentType.Live)
                            {
                                HCoreHelper.SendSMS(SmsType.Transaction, Details.CountryIsd, Details.MobileNumber, Details.MobileMessage, AccountDetails.AccountId, null, null);
                            }
                            _VerificationResponse = new OUserRegistration.UserVerificationResponse();
                            _VerificationResponse.ReferenceId = Details.ReferenceId;
                            _VerificationResponse.MobileNumber = Details.MobileNumber;
                            _VerificationResponse.RequestToken = Details.RequestToken;
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV035", "Verification code sent to your mobile number. Please enter code to verify mobile");
                        }
                        else
                        {
                            _Random = new Random();
                            String NumberVerificationToken = _Random.Next(1000, 9999).ToString();

                            string AccessKey = HCoreHelper.GenerateGuid();
                            string TMessage = null;
                            if (HostEnvironment == HostEnvironmentType.Dev || HostEnvironment == HostEnvironmentType.Tech || HostEnvironment == HostEnvironmentType.Test)
                            {
                                NumberVerificationToken = "1247";
                            }
                            TMessage = NumberVerificationToken + " is your verification code";
                            _HCCoreVerification = new HCCoreVerification();
                            _HCCoreVerification.Guid = HCoreHelper.GenerateGuid();
                            _HCCoreVerification.VerificationTypeId = 1;
                            _HCCoreVerification.CountryIsd = _Request.CountryIsd;
                            _HCCoreVerification.MobileNumber = MobileNumber;
                            _HCCoreVerification.MobileMessage = TMessage;
                            _HCCoreVerification.AccessKey = AccessKey;
                            _HCCoreVerification.AccessCodeStart = "CA";
                            _HCCoreVerification.AccessCode = NumberVerificationToken;
                            _HCCoreVerification.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCCoreVerification.VerifyAttemptCount = 0;
                            _HCCoreVerification.ExpiaryDate = _HCCoreVerification.CreateDate.AddDays(1);
                            _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
                            _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
                            _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
                            _HCCoreVerification.StatusId = 1;
                            _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
                            _HCoreContext.SaveChanges();

                            if (HostEnvironment == HostEnvironmentType.Live)
                            {
                                HCoreHelper.SendSMS(SmsType.Transaction, _Request.CountryIsd, MobileNumber, TMessage, AccountDetails.AccountId, null);
                                HCoreHelper.SendSMS(SmsType.Transaction, _Request.CountryIsd, MobileNumber, "Welcome to Thank U Cash: Your Bal: N0. Your redeeming PIN is: " + AccountPin + ".", AccountDetails.AccountId, AccountDetails.AccountKey);
                            }

                            _VerificationResponse = new OUserRegistration.UserVerificationResponse();
                            _VerificationResponse.IsNewAccount = true;
                            _VerificationResponse.ReferenceId = _HCCoreVerification.Id;
                            _VerificationResponse.MobileNumber = _HCCoreVerification.MobileNumber;
                            _VerificationResponse.RequestToken = _HCCoreVerification.Guid;

                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV035", "Verification code sent to your mobile number. Please enter code to verify mobile");
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "USR0404", "User details not found. It may be removed or not available at the moment.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ResendUserverificationPin", _Exception, _Request.UserReference, "HC5000", "Unable to process your request please try after some time.");
            }
        }

        internal OResponse ResetUserAccountPin(OUserRegistration.ResetUserAccountPinRequest _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "USR020", "Mobile number required.");
                }
                if (string.IsNullOrEmpty(_Request.CountryIsd))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "USR020", "Mobile number required.");
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        string VMobileNumber = FormatMobileNumber(_Request.MobileNumber);
                        string MobileNumber = HCoreHelper.FormatMobileNumber(_Request.CountryIsd, VMobileNumber, VMobileNumber.Length);
                        string UserName = _AppConfig.AppUserPrefix + MobileNumber;

                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && (x.User.Username == UserName || x.MobileNumber == MobileNumber)).FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            string Guid = HCoreHelper.GenerateGuid();
                            DateTime CurrentTime = HCoreHelper.GetGMTDateTime();

                            _Random = new Random();
                            String CodeStart = "FP";
                            String TemporaryAccountPin = _Random.Next(1000, 9999).ToString();

                            if (HostEnvironment == HostEnvironmentType.Test || HostEnvironment == HostEnvironmentType.Tech || HostEnvironment == HostEnvironmentType.Dev)
                            {
                                TemporaryAccountPin = "1234";
                            }
                            string AccessKey = HCoreHelper.GenerateGuid();
                            string TMessage = null;
                            TMessage = TemporaryAccountPin + " is your temporary login pin. Use this temporary pin to create a new pin.";
                            AccountDetails.AccessPin = HCoreEncrypt.EncryptHash(TemporaryAccountPin);
                            AccountDetails.IsTempPin = true;
                            _HCoreContext.SaveChanges();

                            if (HostEnvironment == HostEnvironmentType.Live)
                            {
                                HCoreHelper.SendSMS(SmsType.Transaction, _Request.CountryIsd, MobileNumber, TMessage, AccountDetails.Id, null);
                            }

                            _VerificationResponse = new OUserRegistration.UserVerificationResponse();
                            _VerificationResponse.MobileNumber = MobileNumber;
                            _VerificationResponse.RequestToken = Guid;
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV008", "An temporary login pin sent to  your mobile number. Use the temporary log in pin to log in and create a new pin.");
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "USR0404", "User details not found. It may be removed or not available at the moment.");
                        }
                    }

                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("", _Exception, _Request.UserReference, "HC5000", "Unable to process your request please try after some time.");
            }
        }

        internal OResponse ResetAccountPin(OUserRegistration.UpdateAccountPinRequest _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC161");
                }
                if (string.IsNullOrEmpty(_Request.TempAccessPin))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "USR009", "Old pin missing");
                }
                if (_Request.TempAccessPin.Length != 4)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "USR010", "Invalid temporary pin.");
                }
                if (string.IsNullOrEmpty(_Request.NewAccessPin))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "USR011", "New pin missing");
                }
                if (_Request.NewAccessPin.Length != 4)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "USR012", "Invalid new pin.");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    string VMobileNumber = FormatMobileNumber(_Request.MobileNumber);
                    string MobileNumber = HCoreHelper.FormatMobileNumber(_Request.CountryIsd, VMobileNumber, VMobileNumber.Length);
                    string UserName = _AppConfig.AppUserPrefix + MobileNumber;

                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && (x.User.Username == UserName || x.MobileNumber == MobileNumber) && x.IsTempPin == true).FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        string TempPin = HCoreEncrypt.DecryptHash(AccountDetails.AccessPin);
                        if (TempPin != _Request.TempAccessPin)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "USR013", "Invalid temporary pin. Please enter the correct temporary pin.");
                        }
                        else
                        {
                            AccountDetails.AccessPin = HCoreEncrypt.EncryptHash(_Request.NewAccessPin);
                            AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            AccountDetails.ModifyById = _Request.UserReference.AccountId;
                            AccountDetails.IsTempPin = false;
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1180", "Account pin Updated Successfully.");
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC164", "Invalid mobile number.");

                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ResetAccountPin", _Exception, _Request.UserReference, "HC5000", "Unable to process your request please try after some time.");
            }
        }

        private static string FormatMobileNumber(string MobileNumber)
        {
            if (!string.IsNullOrEmpty(MobileNumber))
            {
                var _StringBuilder = new StringBuilder(MobileNumber);
                _StringBuilder.Remove(0, 1);
                MobileNumber = _StringBuilder.ToString();
                return MobileNumber;
            }
            else
            {
                return MobileNumber;
            }
        }
    }
}