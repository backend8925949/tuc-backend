//==================================================================================
// FileName: FrameworkVerification.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to verfication process
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Object.CustomerWeb;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.Framework.CustomerWeb
{
    public class FrameworkVerification
    {
        HCUAccountSession _HCUAccountSession;
        OAccountAccess _OAccount;
        OCoreVerificationManager.Response _VerificationResponse;
        HCoreContext _HCoreContext;
        HCCoreVerification _HCCoreVerification;
        Random _Random;
        HCUAccount _HCUAccount;
        HCUAccountAuth _HCUAccountAuth;

        #region Manage Requests
        #region Get User Profile
        /// <summary>
        /// Gets the user profile.
        /// </summary>
        /// <param name="AccountId">The account identifier.</param>
        /// <param name="IsNewAccount">if set to <c>true</c> [is new account].</param>
        /// <param name="_UserReference">The user reference.</param>
        /// <returns>OAccountAccess.</returns>
        private OAccountAccess GetUserProfile(long AccountId, bool IsNewAccount, OUserReference _UserReference)
        {
            try
            {
                _OAccount = new OAccountAccess();
                _OAccount.IsNewAccount = IsNewAccount;
                #region Declare 
                string UserAccountSessionKey = HCoreHelper.GenerateGuid();
                #endregion
                #region Build Details
                using (_HCoreContext = new HCoreContext())
                {
                    var UserDetails = (from n in _HCoreContext.HCUAccount
                                       where n.Id == AccountId
                                       select new
                                       {
                                           UserId = n.UserId,
                                           UserKey = n.Guid,

                                           UserAccountId = n.Id,
                                           UserAccountKey = n.Guid,
                                           AccountTypeId = n.AccountTypeId,
                                           DisplayName = n.DisplayName,
                                           Name = n.Name,
                                           CountryId = n.CountryId,
                                           IconUrl = n.IconStorage.Path,
                                           EmailAddress = n.EmailAddress,
                                       }).FirstOrDefault();
                    if (UserDetails != null)
                    {

                        #region Set User Object
                        _OAccount.Name = UserDetails.Name;
                        _OAccount.DisplayName = UserDetails.DisplayName;
                        _OAccount.IconUrl = UserDetails.IconUrl;
                        _OAccount.EmailAddress = UserDetails.EmailAddress;
                        #endregion
                        #region Country
                        _OAccount.Country = (from n in _HCoreContext.HCCoreCountry
                                             where n.Id == UserDetails.CountryId
                                             select new OCountry
                                             {
                                                 Iso = n.Iso,
                                                 Isd = n.Isd,
                                                 Name = n.Name,
                                                 Symbol = n.CurrencyHex,
                                                 //CurrencySymbol = n.CurrencySymbol,
                                             }).FirstOrDefault();
                        #endregion
                        #region Set user account
                        if (!string.IsNullOrEmpty(UserDetails.IconUrl))
                        {
                            _OAccount.IconUrl = _AppConfig.StorageUrl + UserDetails.IconUrl;
                        }
                        else
                        {
                            _OAccount.IconUrl = _AppConfig.Default_Icon;
                        }
                        #endregion
                        #region Save User Account Session
                        long UserSessionId = 0;
                        DateTime _LogOutDate = HCoreHelper.GetGMTDateTime();
                        var UserActiveSessions = (from n in _HCoreContext.HCUAccountSession
                                                  where n.Account.Id == AccountId && n.StatusId == HelperStatus.Default.Active
                                                  select n).ToList();
                        foreach (var UserActiveSession in UserActiveSessions)
                        {
                            UserActiveSession.LogoutDate = _LogOutDate;
                            UserActiveSession.StatusId = HelperStatus.Default.Active;
                        }
                        _HCoreContext.SaveChanges();
                        using (_HCoreContext = new HCoreContext())
                        {
                            _HCUAccountSession = new HCUAccountSession();
                            _HCUAccountSession.Guid = UserAccountSessionKey;
                            _HCUAccountSession.AccountId = UserDetails.UserAccountId;
                            _HCUAccountSession.PlatformId = 754;
                            _HCUAccountSession.AppVersionId = _UserReference.AppVersionId;
                            _HCUAccountSession.LastActivityDate = HCoreHelper.GetGMTDate();
                            _HCUAccountSession.LoginDate = HCoreHelper.GetGMTDateTime();
                            _HCUAccountSession.IPAddress = _UserReference.RequestIpAddress;
                            _HCUAccountSession.Latitude = _UserReference.RequestLatitude;
                            _HCUAccountSession.Longitude = _UserReference.RequestLongitude;
                            _HCUAccountSession.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.HCUAccountSession.Add(_HCUAccountSession);
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();
                            UserSessionId = _HCUAccountSession.Id;
                        }
                        #region Set User Access Key
                        _OAccount.LoginTime = HCoreHelper.GetGMTDateTime();
                        _OAccount.Key = HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(UserSessionId + "|" + UserAccountSessionKey));
                        #endregion
                        #endregion
                        #region Dispose Conntection
                        using (_HCoreContext = new HCoreContext())
                        {
                            var UserAccountD = (from n in _HCoreContext.HCUAccount
                                                where n.Id == UserDetails.UserAccountId
                                                select n).FirstOrDefault();
                            if (UserAccountD != null)
                            {
                                UserAccountD.LastLoginDate = HCoreHelper.GetGMTDateTime();
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                            }
                        }
                        #endregion
                        //_UserAuthResponse.UserAccount = _ObjectUserAccount;
                        //_UserAuthResponse.LoginTime = HCoreHelper.GetGMTDateTime();
                        //_UserAuthResponse.AccessKey = UserAccessKey;
                        //_UserAuthResponse.PublicKey = UserAccountSessionKey;
                        #region Send Response
                        return _OAccount;
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return null;
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetUserProfile", ex);
                return null;
            }
        }
        #endregion
        /// <summary>
        /// Description: Requests the otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse RequestOtp(OCoreVerificationManager.Request _Request)
        {
            try
            {
                #region Manage Operations
                if (string.IsNullOrEmpty(_Request.CountryIsd))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV002", "Country code missing");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV003", "Mobile number missing");
                    #endregion
                }
                using (_HCoreContext = new HCoreContext())
                {
                    string MobileNumber = HCoreHelper.FormatMobileNumber(_Request.CountryIsd, _Request.MobileNumber);
                    string UserName = _AppConfig.AppUserPrefix + MobileNumber;
                    long AccountId = 0;
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.User.Username == UserName)
                        .Select(x => new
                        {
                            StatusId = x.StatusId,
                            x.Id,
                        })
                        .FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        AccountId = AccountDetails.Id;
                        if (AccountDetails.StatusId != HelperStatus.Default.Active)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV000", "Your account is not active. Please contact support to activate account");
                        }
                    }

                    string VMobileNumber = null;
                    #region Set V Mode
                    VMobileNumber = _Request.MobileNumber;
                    var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Isd == _Request.CountryIsd).Select(x => new { CountryIsd = x.Isd }).FirstOrDefault();
                    if (CountryDetails != null)
                    {
                        VMobileNumber = HCoreHelper.FormatMobileNumber(CountryDetails.CountryIsd, VMobileNumber);
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV006", "Country details not found");
                        #endregion
                    }
                    #endregion
                    #region  Process Request
                    #region Mobile Number Verification
                    DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
                    DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                    var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
                                           where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
                                           select new
                                           {
                                               ReferenceId = n.Id,
                                               RequestToken = n.Guid,
                                               AccessCode = n.AccessCode,
                                               AccessCodeStart = n.AccessCodeStart,
                                               MobileMessage = n.MobileMessage,
                                               MobileNumber = n.MobileNumber,
                                               ReferenceKey = n.ReferenceKey,
                                               CountryIsd = n.CountryIsd,
                                               Type = n.VerificationTypeId,
                                           }).FirstOrDefault();
                    if (PreviousRequest != null)
                    {
                        if (VMobileNumber != "2349009009000" && VMobileNumber != "2348008008000" && VMobileNumber != "2347007007000" && VMobileNumber != "2346006006000" && VMobileNumber != "2345005005000" && VMobileNumber != "2344004004000" && VMobileNumber != "2341001001000")
                        {
                            #region Send Message
                            HCoreHelper.SendSMS(SmsType.Transaction, PreviousRequest.CountryIsd, PreviousRequest.MobileMessage, PreviousRequest.MobileMessage, AccountId, null, null);
                            #endregion
                        }
                        #region Build Response
                        _VerificationResponse = new OCoreVerificationManager.Response();
                        _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
                        _VerificationResponse.MobileNumber = PreviousRequest.MobileNumber;
                        _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV007", "Verification code sent to your mobile number. Please enter code to verify mobile");
                        #endregion
                    }
                    else
                    {
                        #region Get code
                        string Guid = HCoreHelper.GenerateGuid(); ;
                        DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                        _Random = new Random();
                        String CodeStart = "CA";
                        String NumberVerificationToken = _Random.Next(1000, 9999).ToString();
                        if (VMobileNumber == "2349009009000" || VMobileNumber == "2348008008000" || VMobileNumber == "2347007007000" || VMobileNumber == "2346006006000" || VMobileNumber == "2345005005000" || VMobileNumber == "2344004004000" || VMobileNumber == "2341001001000")
                        {
                            NumberVerificationToken = "1247";
                        }
                        string AccessKey = HCoreHelper.GenerateGuid();
                        #endregion
                        #region Build Message
                        string TMessage = null;
                        TMessage = NumberVerificationToken + " is your verification code";
                        #endregion
                        if (VMobileNumber != "2349009009000" && VMobileNumber != "2348008008000" && VMobileNumber != "2347007007000" && VMobileNumber != "2346006006000" && VMobileNumber != "2345005005000" && VMobileNumber != "2344004004000" || VMobileNumber == "2341001001000")
                        {
                            #region Send Message
                            HCoreHelper.SendSMS(SmsType.Transaction, _Request.CountryIsd, VMobileNumber, TMessage, AccountId, null);
                            #endregion
                        }
                        #region Save Reqest
                        _HCCoreVerification = new HCCoreVerification();
                        _HCCoreVerification.Guid = Guid;
                        _HCCoreVerification.VerificationTypeId = 1;
                        _HCCoreVerification.CountryIsd = _Request.CountryIsd;
                        _HCCoreVerification.MobileNumber = VMobileNumber;
                        _HCCoreVerification.MobileMessage = TMessage;
                        _HCCoreVerification.AccessKey = AccessKey;
                        _HCCoreVerification.AccessCodeStart = CodeStart;
                        _HCCoreVerification.AccessCode = NumberVerificationToken;
                        _HCCoreVerification.CreateDate = CurrentTime;
                        _HCCoreVerification.VerifyAttemptCount = 0;
                        _HCCoreVerification.ExpiaryDate = CurrentTime.AddDays(1);
                        _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
                        _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
                        _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
                        _HCCoreVerification.StatusId = 1;
                        _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
                        _HCoreContext.SaveChanges();
                        long VerificationId = _HCCoreVerification.Id;
                        #endregion
                        #region Build Response
                        _VerificationResponse = new OCoreVerificationManager.Response();
                        _VerificationResponse.ReferenceId = VerificationId;
                        _VerificationResponse.MobileNumber = VMobileNumber;
                        _VerificationResponse.CountryIsd = _Request.CountryIsd;
                        _VerificationResponse.RequestToken = Guid;
                        if (AccountId > 0)
                        {
                            _VerificationResponse.IsNewAccount = false;
                        }
                        else
                        {
                            _VerificationResponse.IsNewAccount = true;
                        }
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV008", "Verification code sent to  your mobile number. Enter verification code to verify your mobile.");
                        #endregion
                    }
                    #endregion
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("RequestOtp", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV014", "Unable to start verification. Please try after some time");
                #endregion
            }
        }
        /// <summary>
        /// Description: Verifies the otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse VerifyOtp(OCoreVerificationManager.RequestVerify _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    #region Declare

                    #endregion
                    #region Process Request
                    if (string.IsNullOrEmpty(_Request.RequestToken))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV015", "Verification token missing");
                        #endregion
                    }
                    else
                    {
                        //string CheckMobiApiKey = HCoreHelper.GetConfiguration("checkmobiapikey");
                        using (_HCoreContext = new HCoreContext())
                        {
                            var RequestDetails = (from n in _HCoreContext.HCCoreVerification
                                                  where n.Guid == _Request.RequestToken
                                                  select n).FirstOrDefault();
                            if (RequestDetails != null)
                            {
                                if (RequestDetails.StatusId == 1)
                                {
                                    if (RequestDetails.VerificationTypeId == 1 && string.IsNullOrEmpty(_Request.AccessCode))
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV016", "Verification code missing");
                                        #endregion
                                    }
                                    else
                                    {
                                        DateTime StartDate = HCoreHelper.GetGMTDateTime();
                                        if (RequestDetails.ExpiaryDate > StartDate)
                                        {
                                            if (RequestDetails.AccessCode == _Request.AccessCode)
                                            {
                                                string MobileNumber = HCoreHelper.FormatMobileNumber(RequestDetails.CountryIsd, RequestDetails.MobileNumber);
                                                string UserName = _AppConfig.AppUserPrefix + MobileNumber;
                                                var AccountId = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.User.Username == UserName).Select(x => x.Id).FirstOrDefault();
                                                #region Update Request
                                                RequestDetails.StatusId = 2;
                                                RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                                RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                                RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                                RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                                RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                                _HCoreContext.SaveChanges();
                                                #endregion
                                                #region Build Response
                                                _VerificationResponse = new OCoreVerificationManager.Response();
                                                _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                                if (AccountId > 0)
                                                {
                                                    _VerificationResponse.IsNewAccount = false;
                                                }
                                                else
                                                {
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        string NewPin = HCoreHelper.GenerateRandomNumber(4);
                                                        HCUAccountAuth _HCUAccountAuth = new HCUAccountAuth();
                                                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                                        _HCUAccountAuth.Username = UserName;
                                                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(MobileNumber);
                                                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                                                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                                        _HCUAccountAuth.CreatedById = 1;
                                                        HCUAccount _HCUAccount = new HCUAccount();
                                                        _HCUAccount.Guid = HCoreHelper.GenerateGuid(); ;
                                                        _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                                                        _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                                                        _HCUAccount.MobileNumber = MobileNumber;
                                                        _HCUAccount.DisplayName = MobileNumber;
                                                        _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                                                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(NewPin);
                                                        _HCUAccount.MobileNumber = MobileNumber;
                                                        _HCUAccount.ContactNumber = MobileNumber;
                                                        _HCUAccount.CountryId = 1;
                                                        _HCUAccount.EmailVerificationStatus = 0;
                                                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                                        _HCUAccount.NumberVerificationStatus = 0;
                                                        _HCUAccount.NumberVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                                        _Random = new Random();
                                                        string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                                                        _HCUAccount.AccountCode = AccountCode;
                                                        _HCUAccount.ReferralCode = MobileNumber;
                                                        _HCUAccount.RegistrationSourceId = RegistrationSource.DealsWebsite;
                                                        _HCUAccount.CreatedById = 1;
                                                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                                                        _HCUAccount.User = _HCUAccountAuth;
                                                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                                                        _HCoreContext.SaveChanges();
                                                        AccountId = _HCUAccount.Id;
                                                        if (HostEnvironment == HostEnvironmentType.Live)
                                                        {
                                                            HCoreHelper.SendSMS(SmsType.Transaction, "234", MobileNumber, "Welcome to Thank U Cash: Your Bal: N0. Your redeeming PIN is: " + NewPin + ".", _HCUAccount.Id, _HCUAccount.Guid);
                                                        }
                                                    }
                                                    _VerificationResponse.IsNewAccount = true;
                                                }
                                                #endregion
                                                var AccountDetails = GetUserProfile(AccountId, _VerificationResponse.IsNewAccount, _Request.UserReference);
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, AccountDetails, "HCV018", "Verification successful");
                                                #endregion
                                            }
                                            else
                                            {
                                                #region Update Request
                                                RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                                _HCoreContext.SaveChanges();
                                                #endregion
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV019", "Invalid verification code");
                                                #endregion
                                            }

                                        }
                                        else
                                        {
                                            #region Update Request
                                            RequestDetails.StatusId = 3;
                                            RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                            RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                            RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                            RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                            RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                            _HCoreContext.SaveChanges();
                                            #endregion
                                            #region Send Response
                                            _VerificationResponse = new OCoreVerificationManager.Response();
                                            _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                            #endregion
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV022", "Verification code expired. Please process verification again");
                                            #endregion
                                        }
                                    }
                                }
                                else if (RequestDetails.StatusId == 2)
                                {
                                    #region Build Response
                                    _VerificationResponse = new OCoreVerificationManager.Response();
                                    _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                    #endregion
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV023", "Verification already done");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV024", "Invalid status code");
                                    #endregion
                                }

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV025", "Invalid request. Please process verification again.");
                                #endregion
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("VerifyOtp", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV026", "Unable to verify request. Please try after some time.");
                #endregion
            }
        }
        #endregion


        #region New Signin & signup

        /// <summary>
        /// Description: Requests the signup otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse RequestSignupOtp(OCoreVerificationManager.Request _Request)
        {
            try
            {
                #region Manage Operations
                if (string.IsNullOrEmpty(_Request.CountryIsd))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV030", "Country code missing");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV031", "Mobile number missing");
                    #endregion
                }
                using (_HCoreContext = new HCoreContext())
                {
                    string MobileNumber = HCoreHelper.FormatMobileNumber(_Request.CountryIsd, _Request.MobileNumber);
                    string UserName = _AppConfig.AppUserPrefix + MobileNumber;
                    long AccountId = 0;
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.User.Username == UserName)
                        .Select(x => new
                        {
                            StatusId = x.StatusId,
                            x.Id,
                        })
                        .FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        AccountId = AccountDetails.Id;
                        if (AccountDetails.StatusId != HelperStatus.Default.Active)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV032", "Your account is not active. Please contact support to activate account");
                        }
                    }

                    string VMobileNumber = null;
                    #region Set V Mode
                    VMobileNumber = _Request.MobileNumber;
                    var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Isd == _Request.CountryIsd).Select(x => new { CountryIsd = x.Isd }).FirstOrDefault();
                    if (CountryDetails != null)
                    {
                        VMobileNumber = HCoreHelper.FormatMobileNumber(CountryDetails.CountryIsd, VMobileNumber);
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV034", "Country details not found");
                        #endregion
                    }
                    #endregion
                    #region  Process Request
                    #region Mobile Number Verification
                    DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
                    DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                    var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
                                           where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
                                           select new
                                           {
                                               ReferenceId = n.Id,
                                               RequestToken = n.Guid,
                                               AccessCode = n.AccessCode,
                                               AccessCodeStart = n.AccessCodeStart,
                                               MobileMessage = n.MobileMessage,
                                               MobileNumber = n.MobileNumber,
                                               ReferenceKey = n.ReferenceKey,
                                               CountryIsd = n.CountryIsd,
                                               Type = n.VerificationTypeId,
                                           }).FirstOrDefault();
                    if (PreviousRequest != null)
                    {
                        if (VMobileNumber != "2349009009000" && VMobileNumber != "2348008008000" && VMobileNumber != "2347007007000" && VMobileNumber != "2346006006000" && VMobileNumber != "2345005005000" && VMobileNumber != "2344004004000" && VMobileNumber != "2341001001000")
                        {
                            #region Send Message
                            HCoreHelper.SendSMS(SmsType.Transaction, PreviousRequest.CountryIsd, PreviousRequest.MobileNumber, PreviousRequest.MobileMessage, AccountId, null, null);
                            #endregion
                        }
                        #region Build Response
                        _VerificationResponse = new OCoreVerificationManager.Response();
                        _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
                        _VerificationResponse.MobileNumber = PreviousRequest.MobileNumber;
                        _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV035", "Verification code sent to your mobile number. Please enter code to verify mobile");
                        #endregion
                    }
                    else
                    {
                        #region Get code
                        string Guid = HCoreHelper.GenerateGuid(); ;
                        DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                        _Random = new Random();
                        String CodeStart = "CA";
                        String NumberVerificationToken = _Random.Next(1000, 9999).ToString();
                        if (VMobileNumber == "2349009009000" || VMobileNumber == "2348008008000" || VMobileNumber == "2347007007000" || VMobileNumber == "2346006006000" || VMobileNumber == "2345005005000" || VMobileNumber == "2344004004000" || VMobileNumber == "2341001001000")
                        {
                            NumberVerificationToken = "1247";
                        }
                        string AccessKey = HCoreHelper.GenerateGuid();
                        #endregion
                        #region Build Message
                        string TMessage = null;
                        TMessage = NumberVerificationToken + " is your verification code";
                        #endregion
                        if (VMobileNumber != "2349009009000" && VMobileNumber != "2348008008000" && VMobileNumber != "2347007007000" && VMobileNumber != "2346006006000" && VMobileNumber != "2345005005000" && VMobileNumber != "2344004004000" || VMobileNumber == "2341001001000")
                        {
                            #region Send Message
                            HCoreHelper.SendSMS(SmsType.Transaction, _Request.CountryIsd, VMobileNumber, TMessage, AccountId, null);
                            #endregion
                        }
                        #region Save Reqest
                        if (HostEnvironment == HostEnvironmentType.Dev || HostEnvironment == HostEnvironmentType.Tech || HostEnvironment == HostEnvironmentType.Test)
                        {
                            NumberVerificationToken = "1247";
                        }
                        _HCCoreVerification = new HCCoreVerification();
                        _HCCoreVerification.Guid = Guid;
                        _HCCoreVerification.VerificationTypeId = 1;
                        _HCCoreVerification.CountryIsd = _Request.CountryIsd;
                        _HCCoreVerification.MobileNumber = VMobileNumber;
                        _HCCoreVerification.MobileMessage = TMessage;
                        _HCCoreVerification.AccessKey = AccessKey;
                        _HCCoreVerification.AccessCodeStart = CodeStart;
                        _HCCoreVerification.AccessCode = NumberVerificationToken;
                        _HCCoreVerification.CreateDate = CurrentTime;
                        _HCCoreVerification.VerifyAttemptCount = 0;
                        _HCCoreVerification.ExpiaryDate = CurrentTime.AddDays(1);
                        _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
                        _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
                        _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
                        _HCCoreVerification.StatusId = 1;
                        _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
                        _HCoreContext.SaveChanges();
                        long VerificationId = _HCCoreVerification.Id;
                        #endregion
                        #region Build Response
                        _VerificationResponse = new OCoreVerificationManager.Response();
                        _VerificationResponse.ReferenceId = VerificationId;
                        _VerificationResponse.MobileNumber = VMobileNumber;
                        _VerificationResponse.CountryIsd = _Request.CountryIsd;
                        _VerificationResponse.RequestToken = Guid;
                        if (AccountId > 0)
                        {
                            _VerificationResponse.IsNewAccount = false;
                        }
                        else
                        {
                            _VerificationResponse.IsNewAccount = true;
                        }

                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV036", "Verification code sent to  your mobile number. Enter verification code to verify your mobile.");
                        #endregion
                    }
                    #endregion
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("RequestSignupOtp", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV037", "Unable to start verification. Please try after some time");
                #endregion
            }
        }

        /// <summary>
        /// Description: Verifies the signup otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse VerifySignupOtp(OCoreVerificationManager.RequestVerify _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {

                    #region Process Request

                    if (string.IsNullOrEmpty(_Request.AccessCode))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV038", "Verification code missing");
                        #endregion
                    }

                    if (string.IsNullOrEmpty(_Request.RequestToken))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV039", "Verification token missing");
                        #endregion
                    }

                    using (_HCoreContext = new HCoreContext())
                    {
                        var RequestDetails = (from n in _HCoreContext.HCCoreVerification
                                              where n.Guid == _Request.RequestToken
                                              select n).FirstOrDefault();
                        if (RequestDetails != null)
                        {
                            if (RequestDetails.StatusId == 1)
                            {

                                DateTime StartDate = HCoreHelper.GetGMTDateTime();
                                if (RequestDetails.ExpiaryDate > StartDate)
                                {
                                    if (RequestDetails.AccessCode == _Request.AccessCode)
                                    {
                                        string MobileNumber = HCoreHelper.FormatMobileNumber(RequestDetails.CountryIsd, RequestDetails.MobileNumber);
                                        string UserName = _AppConfig.AppUserPrefix + MobileNumber;
                                        var accountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.User.Username == UserName).FirstOrDefault();
                                        long AccountId = 0;
                                        #region Update Request
                                        RequestDetails.StatusId = 2;
                                        RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                        RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                        RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                        RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                        RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                        _HCoreContext.SaveChanges();
                                        #endregion
                                        #region Build Response
                                        _VerificationResponse = new OCoreVerificationManager.Response();
                                        _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                        _VerificationResponse.ReferenceId = RequestDetails.Id;
                                        _VerificationResponse.RequestToken = RequestDetails.Guid;
                                        if (accountDetails != null)
                                        {
                                            AccountId = accountDetails.Id;
                                            _VerificationResponse.IsNewAccount = false;
                                            _VerificationResponse.FirstName = accountDetails.FirstName;
                                            _VerificationResponse.LastName = accountDetails.LastName;
                                            _VerificationResponse.EmailAddress = accountDetails.EmailAddress;
                                        }
                                        else
                                        {
                                            _VerificationResponse.IsNewAccount = true;
                                        }

                                        #endregion
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV040", "Verification successful");
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Update Request
                                        RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                        _HCoreContext.SaveChanges();
                                        #endregion
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV041", "Invalid verification code");
                                        #endregion
                                    }

                                }
                                else
                                {
                                    #region Update Request
                                    RequestDetails.StatusId = 3;
                                    RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                    RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                    RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                    RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                    RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                    _HCoreContext.SaveChanges();
                                    #endregion
                                    #region Send Response
                                    _VerificationResponse = new OCoreVerificationManager.Response();
                                    _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                    #endregion
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV042", "Verification code expired. Please process verification again");
                                    #endregion
                                }
                            }
                            else if (RequestDetails.StatusId == 2)
                            {
                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV065", "Invalid status code. Please process verification again");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV066", "Invalid status code. Please process verification again");
                                #endregion
                            }

                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV043", "Invalid request. Please process verification again.");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("VerifySignupOtp", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV044", "Unable to verify request. Please try after some time.");
                #endregion
            }
        }

        /// <summary>
        /// Description: Signups the user.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SignupUser(OCoreVerificationManager.RequestSignupUser _Request)
        {
            try
            {
                #region Validate Request
                if (string.IsNullOrEmpty(_Request.FirstName))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV045", "First name is missing");
                    #endregion
                }

                if (string.IsNullOrEmpty(_Request.LastName))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV046", "Last name is missing");
                    #endregion
                }

                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV047", "Email address is missing");
                    #endregion
                }

                if (string.IsNullOrEmpty(_Request.Password))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV048", "Password criteria not matched");
                    #endregion
                }

                if (string.IsNullOrEmpty(_Request.RequestToken))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV049", "Verification token missing");
                    #endregion
                }
                #endregion

                #region Process Request
                using (_HCoreContext = new HCoreContext())
                {
                    var RequestDetailss = _HCoreContext.HCCoreVerification.Where(x => x.Guid == _Request.RequestToken)
                                         .Select(x => new
                                         {
                                             Id = x.Id,
                                             Guid = x.Guid,
                                             CountryIsd = x.CountryIsd,
                                             MobileNumber = x.MobileNumber,
                                             StatusId = x.StatusId,
                                         }).FirstOrDefault();
                    if (RequestDetailss != null)
                    {
                        if (RequestDetailss.StatusId == HelperStatus.Default.Active)
                        {
                            string MobileNumber = HCoreHelper.FormatMobileNumber(RequestDetailss.CountryIsd, RequestDetailss.MobileNumber);
                            string UserName = _AppConfig.AppUserPrefix + MobileNumber;
                            var AccountInfo = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.User.Username == UserName).FirstOrDefault();
                            var PasswordInfo = _HCoreContext.HCUAccountAuth.Where(x => x.Username == UserName).FirstOrDefault();

                            #region Build Response
                            _VerificationResponse = new OCoreVerificationManager.Response();
                            _VerificationResponse.MobileNumber = MobileNumber;
                            _VerificationResponse.ReferenceId = RequestDetailss.Id;
                            _VerificationResponse.RequestToken = RequestDetailss.Guid;
                            long AccountId = 0;
                            if (AccountInfo != null)
                            {
                                //Update password for existing user
                                //AccountInfo.User.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                                AccountInfo.DisplayName = _Request.FirstName;
                                AccountInfo.FirstName = _Request.FirstName;
                                AccountInfo.LastName = _Request.LastName;
                                AccountInfo.EmailAddress = _Request.EmailAddress;
                                AccountInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                                AccountInfo.ModifyById = 1;
                                _HCoreContext.SaveChanges();
                                AccountId = AccountInfo.Id;

                                if (PasswordInfo != null)
                                {
                                    if (!string.IsNullOrEmpty(_Request.Password))
                                    {
                                        PasswordInfo.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                                        PasswordInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        PasswordInfo.ModifyById = 1;
                                        _HCoreContext.SaveChanges();
                                    }
                                }

                            }
                            else
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    string NewPin = HCoreHelper.GenerateRandomNumber(4);
                                    if (HostEnvironment == HostEnvironmentType.Dev || HostEnvironment == HostEnvironmentType.Tech || HostEnvironment == HostEnvironmentType.Test)
                                    {
                                        NewPin = "1234";
                                    }
                                    _HCUAccountAuth = new HCUAccountAuth();
                                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                    _HCUAccountAuth.Username = UserName;
                                    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                    _HCUAccountAuth.CreatedById = 1;
                                    _HCoreContext.HCUAccountAuth.Add(_HCUAccountAuth);
                                    _HCUAccount = new HCUAccount();
                                    _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                                    _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                                    _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                                    _HCUAccount.MobileNumber = MobileNumber;
                                    _HCUAccount.DisplayName = _Request.FirstName;
                                    _HCUAccount.FirstName = _Request.FirstName;
                                    _HCUAccount.LastName = _Request.LastName;
                                    _HCUAccount.EmailAddress = _Request.EmailAddress;
                                    _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(NewPin);
                                    _HCUAccount.ContactNumber = MobileNumber;
                                    _HCUAccount.CountryId = 1;
                                    _HCUAccount.EmailVerificationStatus = 0;
                                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccount.NumberVerificationStatus = 0;
                                    _HCUAccount.NumberVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                    _Random = new Random();
                                    string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                                    _HCUAccount.AccountCode = AccountCode;
                                    _HCUAccount.ReferralCode = MobileNumber;
                                    _HCUAccount.RegistrationSourceId = RegistrationSource.DealsWebsite;
                                    _HCUAccount.CreatedById = 1;
                                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccount.StatusId = HelperStatus.Default.Active;
                                    _HCUAccount.User = _HCUAccountAuth;
                                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                                    _HCoreContext.SaveChanges();
                                    AccountId = _HCUAccount.Id;
                                    _VerificationResponse.IsNewAccount = true;
                                    if (HostEnvironment == HostEnvironmentType.Live)
                                    {
                                        HCoreHelper.SendSMS(SmsType.Transaction, "234", MobileNumber, "Welcome to Thank U Cash: Your Bal: N0. Your redeeming PIN is: " + NewPin + ".", _HCUAccount.Id, _HCUAccount.Guid);
                                    }
                                }
                            }
                            #endregion
                            var AccountDetails = GetUserProfile(AccountId, _VerificationResponse.IsNewAccount, _Request.UserReference);
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, AccountDetails, "HCV050", "Signup successful");
                            #endregion

                        }

                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV051", "Verification is pending. Please process verification again.");
                            #endregion
                        }

                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV052", "Invalid request. Please process verification again.");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SignupUser", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV053", "Unable to Signup. Please try after some time.");
                #endregion
            }
        }

        /// <summary>
        /// Description: Signins the user.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SigninUser(OCoreVerificationManager.RequestSignIn _Request)
        {

            #region Manage Exception
            try
            {
                if (!string.IsNullOrEmpty(_Request.CountryIsd))
                {
                    _Request.CountryIsd = _Request.CountryIsd.Trim();
                }
                else
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV054", "Country code missing");
                    #endregion
                }

                if (!string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    _Request.MobileNumber = _Request.MobileNumber.Trim();
                }
                else
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV055", "Mobile number is missing");
                    #endregion
                }

                if (!string.IsNullOrEmpty(_Request.Password))
                {
                    _Request.Password = _Request.Password.Trim();
                }
                else
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV056", "Password is missing");
                    #endregion
                }

                string MobileNumber = HCoreHelper.FormatMobileNumber(_Request.CountryIsd, _Request.MobileNumber);
                #region Manage Operations
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get User Details
                    var AccountInfo = (from n in _HCoreContext.HCUAccount
                                       where n.MobileNumber == MobileNumber
                                       select new
                                       {
                                           Id = n.Id,
                                           UserId = n.User.Id,
                                           UserKey = n.User.Guid,

                                           Password = n.User.Password,
                                           AccountStatus = n.StatusId,
                                           UserStatus = n.User.StatusId,
                                           CreateDate = n.CreateDate,
                                           ModifyDate = n.ModifyDate,
                                       }).FirstOrDefault();

                    if (AccountInfo != null)
                    {
                        string UserPassword = "";

                        if (!string.IsNullOrEmpty(AccountInfo.Password))
                        {
                            UserPassword = HCoreEncrypt.DecryptHash(AccountInfo.Password);
                        }

                        if (_Request.Password == UserPassword)
                        {
                            if (AccountInfo.UserStatus == HelperStatus.Default.Active)
                            {
                                if (AccountInfo.AccountStatus == HelperStatus.Default.Active)
                                {
                                    _VerificationResponse = new OCoreVerificationManager.Response();
                                    _VerificationResponse.MobileNumber = MobileNumber;
                                    var AccountDetails = GetUserProfile(AccountInfo.Id, _VerificationResponse.IsNewAccount, _Request.UserReference);
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, AccountDetails, "HCV057", "Login successful");
                                    #endregion
                                }
                                else if (AccountInfo.AccountStatus == HelperStatus.Default.Inactive)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV058", "Your account is not active. Please contact support to activate account");
                                    #endregion
                                }
                                else if (AccountInfo.AccountStatus == HelperStatus.Default.Blocked)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV059", "Your account is not active. Please contact support to activate account");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV060", "Your account is not active. Please contact support to activate account");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV061", "Your account is not active. Please contact support to activate account");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV062", "Invalid mobile number or password.");
                            #endregion
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV063", "Invalid mobile number or password.");
                        #endregion
                    }

                    #endregion
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SigninUser", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV064", "Unable to signin. Please try after some time.");
                #endregion
            }
        }
        #endregion

        /// <summary>
        /// Description: Guest checkout.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GuestCheckout(OCoreVerificationManager.RequestGuestCheckout _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {

                    if (string.IsNullOrEmpty(_Request.FirstName))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV101", "FirstName is missing");
                    }
                    if (string.IsNullOrEmpty(_Request.LastName))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV102", "LastName is missing");
                    }
                    if (string.IsNullOrEmpty(_Request.EmailAddress))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV103", "EmailAddress is missing");
                    }

                    if (string.IsNullOrEmpty(_Request.CountryIsd))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV104", "Country code missing");
                        #endregion
                    }
                    if (string.IsNullOrEmpty(_Request.MobileNumber))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV105", "Mobile number missing");
                        #endregion
                    }

                    if (!string.IsNullOrEmpty(_Request.AddressLine1))
                    {
                        if (_Request.StateId < 1)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV106", "State is missing missing");
                        }
                        if (_Request.CityId < 1)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV107", "City is missing");
                        }
                    }

                    string MobileNumber = HCoreHelper.FormatMobileNumber(_Request.CountryIsd, _Request.MobileNumber);
                    string UserName = _AppConfig.AppUserPrefix + MobileNumber;
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.User.Username == UserName).FirstOrDefault();

                    bool IsNewAccount = false;
                    if (AccountDetails == null)
                    {
                        IsNewAccount = true;
                        string NewPin = HCoreHelper.GenerateRandomNumber(4);
                        HCUAccountAuth _HCUAccountAuth = new HCUAccountAuth();
                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountAuth.Username = UserName;
                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(MobileNumber);
                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                        _HCUAccountAuth.CreatedById = 1;
                        HCUAccount _HCUAccount = new HCUAccount();
                        _HCUAccount.Guid = HCoreHelper.GenerateGuid(); ;
                        _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                        _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                        _HCUAccount.MobileNumber = MobileNumber;
                        _HCUAccount.DisplayName = _Request.FirstName;
                        _HCUAccount.FirstName = _Request.FirstName;
                        _HCUAccount.LastName = _Request.LastName;
                        _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(NewPin);
                        _HCUAccount.MobileNumber = MobileNumber;
                        _HCUAccount.ContactNumber = MobileNumber;
                        _HCUAccount.EmailAddress = _Request.EmailAddress;
                        _HCUAccount.CountryId = 1;
                        _HCUAccount.EmailVerificationStatus = 0;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.NumberVerificationStatus = 0;
                        _HCUAccount.NumberVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _Random = new Random();
                        string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                        _HCUAccount.AccountCode = AccountCode;
                        _HCUAccount.ReferralCode = MobileNumber;
                        _HCUAccount.RegistrationSourceId = RegistrationSource.DealsWebsite;
                        _HCUAccount.CreatedById = 1;
                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                        _HCUAccount.User = _HCUAccountAuth;
                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                        _HCoreContext.SaveChanges();
                        //if (HostEnvironment == HostEnvironmentType.Live)
                        //{
                        //    HCoreHelper.SendSMS(SmsType.Transaction, "234", MobileNumber, "Welcome to Thank U Cash: Your Bal: N0. Your redeeming PIN is: " + NewPin + ".", _HCUAccount.Id, _HCUAccount.Guid);
                        //}
                        AccountDetails = _HCUAccount;
                    }

                    long AddressId = 0;
                    if (!string.IsNullOrEmpty(_Request.AddressLine1))
                    {
                        if (AccountDetails.Id > 0)
                        {
                            var addresslist = _HCoreContext.HCCoreAddress
                               .Where(x => x.AccountId == AccountDetails.Id);
                            if (addresslist != null)
                            {
                                foreach (var address in addresslist)
                                {
                                    if (_Request.AddressLine1.Equals(address.AddressLine1) && _Request.CityId == address.CityId && _Request.StateId == address.StateId)
                                    {
                                        AddressId = address.Id;
                                        break;
                                    }
                                }
                            }
                        }
                        if (AddressId == 0)
                        {
                            HCCoreAddress _HCCoreAddress = new HCCoreAddress();
                            _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                            _HCCoreAddress.AccountId = AccountDetails.Id;

                            if (!string.IsNullOrEmpty(_Request.DisplayName))
                            {
                                _HCCoreAddress.DisplayName = _Request.DisplayName;
                            }
                            else
                            {
                                _HCCoreAddress.DisplayName = "Other";
                            }
                            if (_Request.LocationTypeId > 0)
                            {
                                _HCCoreAddress.LocationTypeId = _Request.LocationTypeId;
                            }
                            else
                            {
                                _HCCoreAddress.LocationTypeId = 800;
                            }

                            if (!string.IsNullOrEmpty(_Request.Name))
                            {
                                _HCCoreAddress.Name = _Request.Name;
                            }


                            if (!string.IsNullOrEmpty(_Request.ContactNumber))
                            {
                                _HCCoreAddress.ContactNumber = _Request.ContactNumber;
                            }
                            else
                            {
                                _HCCoreAddress.ContactNumber = AccountDetails.MobileNumber;
                            }


                            if (!string.IsNullOrEmpty(_Request.EmailAddress))
                            {
                                _HCCoreAddress.EmailAddress = _Request.EmailAddress;
                            }
                            else
                            {
                                _HCCoreAddress.EmailAddress = AccountDetails.EmailAddress;
                            }

                            _HCCoreAddress.AddressLine1 = _Request.AddressLine1;
                            _HCCoreAddress.AddressLine2 = _Request.AddressLine2;
                            _HCCoreAddress.ZipCode = _Request.ZipCode;
                            _HCCoreAddress.Landmark = _Request.Landmark;
                            if (_Request.CityAreaId > 0)
                            {
                                _HCCoreAddress.CityAreaId = _Request.CityAreaId;
                            }
                            if (_Request.CityId > 0)
                            {
                                _HCCoreAddress.CityId = _Request.CityId;
                            }
                            if (_Request.StateId > 0)
                            {
                                _HCCoreAddress.StateId = _Request.StateId;
                            }
                            _HCCoreAddress.MapAddress = _Request.MapAddress;
                            _HCCoreAddress.CountryId = (int)AccountDetails.CountryId;
                            _HCCoreAddress.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCCoreAddress.CreatedById = AccountDetails.Id;
                            _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.HCCoreAddress.Add(_HCCoreAddress);
                            _HCoreContext.SaveChanges();
                            AddressId = _HCCoreAddress.Id;
                        }
                    }


                    var AccountDet = GetUserProfile(AccountDetails.Id, IsNewAccount, _Request.UserReference);
                    var response = new
                    {
                        AccountId = AccountDetails.Id,
                        Key = AccountDet.Key,
                        AddressId = AddressId
                    };

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, response, "HCV108", "Guest checkout initialized");
                    #endregion

                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GuestCheckout", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV109", "Unable to process request. Please try after some time.");
                #endregion
            }
        }

    }
}
