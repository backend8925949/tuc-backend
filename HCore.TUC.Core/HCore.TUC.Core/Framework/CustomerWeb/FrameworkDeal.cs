//==================================================================================
// FileName: FrameworkDeal.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to deals
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Core.Object.CustomerWeb;
using HCore.TUC.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.Framework.CustomerWeb
{
    internal class FrameworkDeal
    {
        HCoreContext _HCoreContext;
        MDDealView _MDDealView;
        ManageCoreTransaction _ManageCoreTransaction;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        MDDealCode _MDDealCode;
        List<ODeal.Purchase.List> _PurchaseHistory;
        List<ODeal.List> _Deals;
        ODeal.Details _DealDetails;
        List<ODeal.ImageUrlItem> _DealDetailImages;
        /// <summary>
        /// Description: Saves the deal view.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveDealView(ODeal.View _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.DealKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1373, TUCCoreResource.CA1373M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _DealDetails = _HCoreContext.MDDeal
                                                  .Where(x => x.Guid == _Request.DealKey)
                                                  .FirstOrDefault();
                    if (_DealDetails != null)
                    {
                        _DealDetails.Views += 1;
                        _DealDetails.TView = _DealDetails.Views * 3;
                        _DealDetails.TPurchase = _DealDetails.Views * 2;
                        _DealDetails.TLike = _DealDetails.Views * 2;
                        _MDDealView = new MDDealView();
                        _MDDealView.DealId = _DealDetails.Id;
                        _MDDealView.CustomerId = _Request.AuthAccountId;
                        _MDDealView.Latitude = _Request.UserReference.RequestLatitude;
                        _MDDealView.Longitude = _Request.UserReference.RequestLongitude;

                        #region PlateformId
                        int? PlateformId = 0;
                        if (_Request.AccountSessionId > 0)
                        {
                            var Details = _HCoreContext.HCUAccountSession.Where(x => x.Id == _Request.AccountSessionId && x.Guid == _Request.AccountSessionKey)
                                .Select(x => new
                                {
                                    x.PlatformId
                                }).FirstOrDefault();
                            if (Details != null)
                            {
                                PlateformId = Details.PlatformId;
                            }
                        }
                        #endregion

                        _MDDealView.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.MDDealView.Add(_MDDealView);
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1374, TUCCoreResource.CA1374M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveDealView", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Buys the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse BuyDeal(ODeal.Purchase.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AuthAccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1375, TUCCoreResource.CA1375M);
                }
                if (string.IsNullOrEmpty(_Request.DealKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1376, TUCCoreResource.CA1376M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _DealDetails = _HCoreContext.MDDeal
                                                 .Where(x =>
                                                  x.Guid == _Request.DealKey)
                                                 .Select(x => new
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,


                                                     AccountId = x.AccountId,

                                                     EndDate = x.EndDate,

                                                     Title = x.Title,
                                                     Description = x.Description,
                                                     MerchantDisplayName = x.Account.DisplayName,

                                                     ImageUrl = x.PosterStorage.Path,

                                                     UsageTypeId = x.UsageTypeId,
                                                     CodeValidityDays = x.CodeValidtyDays,
                                                     CodeValidityStartDate = x.CodeValidityStartDate,
                                                     CodeValidityEndDate = x.CodeValidityEndDate,
                                                     Terms = x.Terms,
                                                     Amount = x.Amount,
                                                     Charge = x.Charge,
                                                     TotalAmount = x.TotalAmount,
                                                     MaximumUnitSale = x.MaximumUnitSale,
                                                     MaximumUnitSalePerDay = x.MaximumUnitSalePerDay,
                                                     StatusId = x.StatusId
                                                 }).FirstOrDefault();
                    if (_DealDetails != null)
                    {
                        long? SoldDealsCount = _HCoreContext.MDDealCode.Where(x => x.DealId == _DealDetails.ReferenceId).Count();
                        long? AvailableDealsCount = _DealDetails.MaximumUnitSale - SoldDealsCount;
                        if (AvailableDealsCount <= 0)
                        {
                            AvailableDealsCount = 0;
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1538, TUCCoreResource.CA1538M);
                        }
                        if (_DealDetails.StatusId != HelperStatus.Deals.Published)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1377, TUCCoreResource.CA1377M);
                        }
                        _ManageCoreTransaction = new ManageCoreTransaction();
                        var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AuthAccountId)
                            .Select(x => new
                            {
                                ReferenceId = x.Id,
                                DisplayName = x.DisplayName,
                                MobileNumber = x.MobileNumber,
                                EmailAddress = x.EmailAddress,
                                AccountStatusId = x.StatusId,
                            }).FirstOrDefault();
                        double AccountBalance = _ManageCoreTransaction.GetAccountBalance(CustomerDetails.ReferenceId, TransactionSource.TUC);
                        if (AccountBalance < _DealDetails.Amount)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1378, TUCCoreResource.CA1378M);
                        }
                        if (CustomerDetails.AccountStatusId != HelperStatus.Default.Active)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1379, TUCCoreResource.CA1379M);
                        }
                        string GroupKey = HCoreHelper.GenerateGuid();
                        _CoreTransactionRequest = new OCoreTransaction.Request();
                        _CoreTransactionRequest.CustomerId = _Request.AuthAccountId;
                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                        _CoreTransactionRequest.GroupKey = GroupKey;
                        _CoreTransactionRequest.ParentId = _DealDetails.AccountId;
                        _CoreTransactionRequest.InvoiceAmount = (double)_DealDetails.Amount;
                        _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.InvoiceAmount;
                        //_CoreTransactionRequest.AccountNumber = _PayStackResponseData.authorization.bin;
                        _CoreTransactionRequest.ReferenceNumber = _DealDetails.ReferenceKey;
                        _CoreTransactionRequest.CreatedById = _Request.AuthAccountId;
                        _CoreTransactionRequest.ReferenceAmount = _CoreTransactionRequest.InvoiceAmount;
                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = _Request.AuthAccountId,
                            SourceId = TransactionSource.TUC,
                            ModeId = TransactionMode.Debit,
                            TypeId = TransactionType.Deal.DealPurchaseRedeem,
                            Amount = _CoreTransactionRequest.InvoiceAmount,
                            Charge = 0,
                            TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                        });
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = _DealDetails.AccountId,
                            SourceId = TransactionSource.Deals,
                            ModeId = TransactionMode.Credit,
                            TypeId = TransactionType.Deal.DealPurchase,
                            Amount = _CoreTransactionRequest.InvoiceAmount,
                            Charge = 0,
                            TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                        });
                        _CoreTransactionRequest.Transactions = _TransactionItems;
                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                        {
                            _MDDealCode = new MDDealCode();
                            _MDDealCode.Guid = GroupKey;
                            _MDDealCode.DealId = _DealDetails.ReferenceId;
                            _MDDealCode.AccountId = _Request.AuthAccountId;
                            _MDDealCode.ItemCode = "12" + HCoreHelper.GenerateRandomNumber(9);
                            _MDDealCode.ItemPin = HCoreHelper.GenerateRandomNumber(4);
                            _MDDealCode.ItemAmount = _DealDetails.Amount;
                            _MDDealCode.AvailableAmount = _DealDetails.Amount;
                            if (_DealDetails.UsageTypeId == Helpers.DealCodeUsageType.HoursAfterPurchase)
                            {
                                _MDDealCode.StartDate = HCoreHelper.GetGMTDateTime();
                                _MDDealCode.EndDate = _MDDealCode.StartDate.Value.AddHours((double)_DealDetails.CodeValidityDays);
                            }
                            else
                            {
                                _MDDealCode.StartDate = HCoreHelper.GetGMTDateTime();
                                _MDDealCode.EndDate = _DealDetails.CodeValidityEndDate;
                            }
                            _MDDealCode.UseCount = 0;
                            _MDDealCode.UseAttempts = 0;
                            _MDDealCode.CreateDate = HCoreHelper.GetGMTDateTime();
                            _MDDealCode.CreatedById = _Request.AuthAccountId;
                            _MDDealCode.StatusId = HelperStatus.DealCodes.Unused;
                            _HCoreContext.MDDealCode.Add(_MDDealCode);
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();

                            var _Response = new
                            {
                                ReferenceId = _MDDealCode.Id,
                                ReferenceKey = _MDDealCode.Guid,
                            };

                            using (_HCoreContext = new HCoreContext())
                            {
                                string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _Request.AuthAccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                if (!string.IsNullOrEmpty(UserNotificationUrl))
                                {
                                    HCoreHelper.SendPushToDevice(UserNotificationUrl, "dealpurchasedetails", "Deal Purchase Successful", _DealDetails.Title + " deal puchase successful for " + _DealDetails.MerchantDisplayName, "dealpurchasedetails", _Response.ReferenceId, _Response.ReferenceKey, "View details", true, null);
                                }

                                if (!string.IsNullOrEmpty(CustomerDetails.EmailAddress))
                                {
                                    var _BDetails = new
                                    {
                                        DisplayName = CustomerDetails.DisplayName,
                                        DealCode = _MDDealCode.ItemCode,
                                        MerchantDisplayName = _DealDetails.MerchantDisplayName,
                                        StartDate = _MDDealCode.StartDate.Value.ToString("dd-MM-yyyy HH:mm"),
                                        EndDate = _MDDealCode.EndDate.Value.AddHours(1).ToString("dd-MM-yyyy HH:mm"),
                                        Title = _DealDetails.Title,
                                        Description = _DealDetails.Description,
                                        Terms = _DealDetails.Terms,
                                        ImageUrl = _AppConfig.StorageUrl + _DealDetails.ImageUrl,
                                        Amount = _DealDetails.Amount,
                                    };
                                    HCoreHelper.BroadCastEmail("d-cfb071d1a1174d6d9b08ce8fdee6b6dd", "ThankUCash", CustomerDetails.EmailAddress, _BDetails, _Request.UserReference);
                                }
                            }
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1380, TUCCoreResource.CA1380M);
                            //var _Response = new
                            //{
                            //    ReferenceId = TransactionResponse.ReferenceId,
                            //    ReferenceKey = TransactionResponse.ReferenceKey,
                            //    Amount = _DealDetails.Amount,
                            //};
                            //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.HCP0110, TUCCoreResource.HCP0110M);
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1381, TUCCoreResource.CA1381M);
                            #endregion
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("BuyDeal_Confirm", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the purchase history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPurchaseHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AuthAccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1375, TUCCoreResource.CA1375M);
                }
                _PurchaseHistory = new List<ODeal.Purchase.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.MDDealCode
                                        .Where(x => x.AccountId == _Request.UserReference.AccountId)
                                                .Select(x => new ODeal.Purchase.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    DealCode = x.ItemCode,

                                                    PurchaseDate = x.CreateDate,
                                                    ExpiaryDate = x.EndDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    Quantity = x.ItemCount,

                                                    DealId = x.DealId,
                                                    DealKey = x.Deal.Guid,

                                                    DealTypeId = x.Deal.DealTypeId,
                                                    DealTypeCode = x.Deal.DealType.SystemName,
                                                    DealTypeName = x.Deal.DealType.Name,

                                                    DeliveryTypeId = x.Deal.DeliveryTypeId,
                                                    DeliveryTypeCode = x.Deal.DeliveryType.SystemName,
                                                    DeliveryTypeName = x.Deal.DeliveryType.Name,

                                                    Title = x.Deal.Title,

                                                    MerchantKey = x.Deal.Account.Guid,
                                                    MerchantName = x.Deal.Account.Name,
                                                    MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                                                    CategoryKey = x.Deal.Category.Guid,
                                                    CategoryName = x.Deal.Category.Name,

                                                    DealCreateDate = x.Deal.CreateDate,
                                                    DealEndDate = x.Deal.EndDate,

                                                    ImageUrl = x.Deal.PosterStorage.Path,

                                                    ActualPrice = x.Deal.ActualPrice,
                                                    SellingPrice = x.Deal.SellingPrice,
                                                    Address = x.Deal.Account.Address,
                                                    CityAreaName = x.Deal.Account.CityArea.Name,
                                                    CityName = x.Deal.Account.City.Name,
                                                    StateName = x.Deal.Account.State.Name,

                                                    DealStatusId = x.Deal.StatusId,
                                                    DealStatusCode = x.Deal.Status.SystemName,
                                                    DealStatusName = x.Deal.Status.Name,
                                                })
                                                .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _PurchaseHistory = _HCoreContext.MDDealCode
                                        .Where(x => x.AccountId == _Request.UserReference.AccountId)
                                                .Select(x => new ODeal.Purchase.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    DealCode = x.ItemCode,

                                                    PurchaseDate = x.CreateDate,
                                                    ExpiaryDate = x.EndDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    Quantity = x.ItemCount,

                                                    DealId = x.DealId,
                                                    DealKey = x.Deal.Guid,

                                                    DealTypeId = x.Deal.DealTypeId,
                                                    DealTypeCode = x.Deal.DealType.SystemName,
                                                    DealTypeName = x.Deal.DealType.Name,

                                                    DeliveryTypeId = x.Deal.DeliveryTypeId,
                                                    DeliveryTypeCode = x.Deal.DeliveryType.SystemName,
                                                    DeliveryTypeName = x.Deal.DeliveryType.Name,

                                                    Title = x.Deal.Title,

                                                    MerchantKey = x.Deal.Account.Guid,
                                                    MerchantName = x.Deal.Account.Name,
                                                    MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                                                    CategoryKey = x.Deal.Category.Guid,
                                                    CategoryName = x.Deal.Category.Name,

                                                    DealCreateDate = x.Deal.CreateDate,
                                                    DealEndDate = x.Deal.EndDate,

                                                    ImageUrl = x.Deal.PosterStorage.Path,

                                                    ActualPrice = x.Deal.ActualPrice,
                                                    SellingPrice = x.Deal.SellingPrice,
                                                    Address = x.Deal.Account.Address,
                                                    CityAreaName = x.Deal.Account.CityArea.Name,
                                                    CityName = x.Deal.Account.City.Name,
                                                    StateName = x.Deal.Account.State.Name,

                                                    DealStatusId = x.Deal.StatusId,
                                                    DealStatusCode = x.Deal.Status.SystemName,
                                                    DealStatusName = x.Deal.Status.Name,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    foreach (var DataItem in _PurchaseHistory)
                    {
                        if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                        {
                            DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                        }
                        else
                        {
                            DataItem.ImageUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.MerchantIconUrl))
                        {
                            DataItem.MerchantIconUrl = _AppConfig.StorageUrl + DataItem.MerchantIconUrl;
                        }
                        else
                        {
                            DataItem.MerchantIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _PurchaseHistory, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);

                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetPurchaseHistory", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the deal code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealCode(ODeal.Purchase.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AuthAccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1375, TUCCoreResource.CA1375M);
                }
                if (string.IsNullOrEmpty(_Request.DealCodeKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1396, TUCCoreResource.CA1396M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _DealCode = _HCoreContext.MDDealCode
                                                  .Where(x =>
                                                  x.AccountId == _Request.AuthAccountId
                                                  && x.Guid == _Request.DealCodeKey)
                                                  .Select(x => new ODeal.Purchase.Details
                                                  {
                                                      ReferenceId = x.Id,
                                                      DealCodeId = x.Guid,
                                                      DealReferenceId = x.DealId,
                                                      DealId = x.Deal.Guid,
                                                      ItemCode = x.ItemCode,

                                                      //AccountId = x.AccountId,
                                                      //AccountKey = x.Account.Guid,
                                                      //AccountDisplayName = x.Account.DisplayName,
                                                      //AccountMobileNumber = x.Account.MobileNumber,
                                                      //AccountIconUrl = x.Account.IconStorage.Path,



                                                      StartDate = x.StartDate,
                                                      EndDate = x.EndDate,

                                                      //Amount = x.ItemAmount,
                                                      TTotalAmount = x.ItemAmount,

                                                      Title = x.Deal.Title,
                                                      Description = x.Deal.Description,
                                                      UsageInformation = x.Deal.UsageInformation,
                                                      Terms = x.Deal.Terms,
                                                      ImageUrl = x.Deal.PosterStorage.Path,

                                                      MerchantReferenceId = x.Deal.AccountId,
                                                      MerchantId = x.Deal.Account.Guid,
                                                      MerchantDisplayName = x.Deal.Account.DisplayName,
                                                      MerchantIconUrl = x.Deal.Account.IconStorage.Path,
                                                      MerchantContactNumber = x.Deal.Account.MobileNumber,
                                                      MerchantEmailAddress = x.Deal.Account.EmailAddress,

                                                      UseDate = x.LastUseDate,
                                                      UseLocationId = x.LastUseLocationId,
                                                      UseLocationKey = x.LastUseLocation.Guid,
                                                      UseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                      UseLocationAddress = x.LastUseLocation.Address,

                                                      //SharedCustomerId = x.SecondaryAccount.Id,
                                                      //SharedCustomerKey = x.SecondaryAccount.Guid,
                                                      //SharedCustomerDisplayName = x.SecondaryAccount.DisplayName,
                                                      //SharedCustomerMobileNumber = x.SecondaryAccount.MobileNumber,
                                                      //SharedCustomerIconUrl = x.SecondaryAccount.IconStorage.Path,

                                                      //SecondaryCode = x.SecondaryCode,
                                                      //SecondaryCodeMessage = x.SecondaryMessage,


                                                      CreateDate = x.CreateDate,
                                                      StatusCode = x.Status.SystemName,
                                                      StatusName = x.Status.Name,
                                                  })
                                                  .FirstOrDefault();
                    if (_DealCode != null)
                    {
                        if (_DealCode.TTotalAmount != null)
                        {
                            _DealCode.TotalAmount = Convert.ToInt64(_DealCode.TTotalAmount * 100);
                        }
                        else
                        {
                            _DealCode.TotalAmount = 0;
                        }
                        if (!string.IsNullOrEmpty(_DealCode.ImageUrl))
                        {
                            _DealCode.ImageUrl = _AppConfig.StorageUrl + _DealCode.ImageUrl;
                        }
                        else
                        {
                            _DealCode.ImageUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(_DealCode.MerchantIconUrl))
                        {
                            _DealCode.MerchantIconUrl = _AppConfig.StorageUrl + _DealCode.MerchantIconUrl;
                        }
                        else
                        {
                            _DealCode.MerchantIconUrl = _AppConfig.Default_Icon;
                        }
                        _DealCode.Locations = _HCoreContext.MDDealLocation.Where(x => x.DealId == _DealCode.DealReferenceId)
                            .Select(x => new ODeal.Purchase.DealCodeLocation
                            {
                                Address = x.Location.Address,
                                DisplayName = x.Location.DisplayName,
                                Latitude = x.Location.Latitude,
                                Longitude = x.Location.Longitude,
                            }).ToList();
                        if (_DealCode.Locations.Count == 0)
                        {
                            _DealCode.Locations = _HCoreContext.HCUAccount.Where(x => x.OwnerId == _DealCode.MerchantReferenceId
                            && x.StatusId == HelperStatus.Default.Active
                            && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore)
                           .Select(x => new ODeal.Purchase.DealCodeLocation
                           {
                               Address = x.Address,
                               DisplayName = x.DisplayName,
                               Latitude = x.Latitude,
                               Longitude = x.Longitude,
                           }).ToList();
                        }

                        //if (!string.IsNullOrEmpty(_DealCode.ImageUrl))
                        //{
                        //    _DealCode.ImageUrl = _AppConfig.StorageUrl + _DealCode.ImageUrl;
                        //}
                        //else
                        //{
                        //    _DealCode.ImageUrl = _AppConfig.Default_Icon;
                        //}
                        //if (!string.IsNullOrEmpty(_DealCode.MerchantIconUrl))
                        //{
                        //    _DealCode.MerchantIconUrl = _AppConfig.StorageUrl + _DealCode.MerchantIconUrl;
                        //}
                        //else
                        //{
                        //    _DealCode.MerchantIconUrl = _AppConfig.Default_Icon;
                        //}
                        _DealCode.RedeemInstruction = "- You can redeem deal code through Pos machine or ask merchant to scan QR code. \n - POS Redeem : On pos select ThankUCash Redeem Option. Enter deal code and your pin. Pos will validate your code and print receipt for redeem. \n - Other Option  : Ask merchant to scan qr code to redeem. /n - Merchant will scan code and redeem your coupon to avail deal.";
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DealCode, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDealCode", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the deals.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDeals(ODeal.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime currentdatetime = HCoreHelper.GetGMTDateTime();
                _Deals = new List<ODeal.List>();
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (!string.IsNullOrEmpty(_Request.CategoryId))
                    {
                        #region Total Records
                        int TotalRecords = _HCoreContext.MDDeal
                                                .Where(x =>
                                                 x.StatusId == HelperStatus.Deals.Published
                                                 && x.EndDate < currentdatetime
                                                 && x.Category.Guid == _Request.CategoryId)
                                                .Select(x => new ODeal.List
                                                {
                                                    DealId = x.Guid,
                                                    Title = x.Title,
                                                    ImageUrl = _AppConfig.StorageUrl + x.PosterStorage.Path,
                                                    Description = x.Description,
                                                    MerchantId = x.Guid,
                                                    MerchantDisplayName = x.Account.DisplayName,
                                                    MerchantIconUrl = _AppConfig.StorageUrl + x.Account.IconStorage.Path,
                                                    CategoryId = x.Category.Guid,
                                                    CategoryName = x.Category.Name,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    TActualPrice = x.ActualPrice,
                                                    TSellingPrice = x.SellingPrice,
                                                    TAmount = x.Amount,
                                                })
                                       .Count();
                        #endregion
                        #region Get Data
                        _Deals = _HCoreContext.MDDeal
                                                .Where(x =>
                                                 x.StatusId == HelperStatus.Deals.Published
                                                 && x.Category.Guid == _Request.CategoryId
                                                 && x.Subcategory.ParentCategoryId == x.CategoryId)
                                                .Select(x => new ODeal.List
                                                {
                                                    DealId = x.Guid,
                                                    Title = x.Title,
                                                    ImageUrl = _AppConfig.StorageUrl + x.PosterStorage.Path,
                                                    Description = x.Description,
                                                    MerchantId = x.Guid,
                                                    MerchantDisplayName = x.Account.DisplayName,
                                                    MerchantIconUrl = _AppConfig.StorageUrl + x.Account.IconStorage.Path,
                                                    CategoryId = x.Category.Guid,
                                                    CategoryName = x.Category.Name,
                                                    //SubCategoryId=x.SubcategoryId,s
                                                    //SubCategoryName=x.MDDealSubcategory.
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    TActualPrice = x.ActualPrice,
                                                    TSellingPrice = x.SellingPrice,
                                                    TAmount = x.Amount,
                                                })
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var DataItem in _Deals)
                        {
                            if (DataItem.TActualPrice != null)
                            {
                                DataItem.ActualPrice = Convert.ToInt64(DataItem.TActualPrice * 100);
                            }
                            else
                            {
                                DataItem.ActualPrice = 0;
                            }
                            if (DataItem.TSellingPrice != null)
                            {
                                DataItem.SellingPrice = Convert.ToInt64(DataItem.TSellingPrice * 100);
                            }
                            else
                            {
                                DataItem.ActualPrice = 0;
                            }
                            DataItem.Amount = Convert.ToInt64(DataItem.TAmount * 100);
                            //if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            //{
                            //    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            //}
                            //else
                            //{
                            //    DataItem.IconUrl = _AppConfig.Default_Icon;
                            //}
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, _Deals, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                        #endregion
                    }
                    else
                    {
                        #region Total Records
                        int TotalRecords = _HCoreContext.MDDeal
                                                .Where(x =>
                                                 x.StatusId == HelperStatus.Deals.Published)
                                                .Select(x => new ODeal.List
                                                {
                                                    DealId = x.Guid,
                                                    Title = x.Title,
                                                    ImageUrl = _AppConfig.StorageUrl + x.PosterStorage.Path,
                                                    Description = x.Description,
                                                    MerchantId = x.Guid,
                                                    MerchantDisplayName = x.Account.DisplayName,
                                                    MerchantIconUrl = _AppConfig.StorageUrl + x.Account.IconStorage.Path,
                                                    CategoryId = x.Category.Guid,
                                                    CategoryName = x.Category.Name,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    TActualPrice = x.ActualPrice,
                                                    TSellingPrice = x.SellingPrice,
                                                    TAmount = x.Amount,
                                                })
                                       .Count();
                        #endregion
                        #region Get Data
                        _Deals = _HCoreContext.MDDeal
                                                .Where(x =>
                                                 x.StatusId == HelperStatus.Deals.Published)
                                                .Select(x => new ODeal.List
                                                {
                                                    DealId = x.Guid,
                                                    Title = x.Title,
                                                    ImageUrl = _AppConfig.StorageUrl + x.PosterStorage.Path,
                                                    Description = x.Description,
                                                    MerchantId = x.Guid,
                                                    MerchantDisplayName = x.Account.DisplayName,
                                                    MerchantIconUrl = _AppConfig.StorageUrl + x.Account.IconStorage.Path,
                                                    CategoryId = x.Category.Guid,
                                                    CategoryName = x.Category.Name,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    TActualPrice = x.ActualPrice,
                                                    TSellingPrice = x.SellingPrice,
                                                    TAmount = x.Amount,
                                                })
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var DataItem in _Deals)
                        {
                            if (DataItem.TActualPrice != null)
                            {
                                DataItem.ActualPrice = Convert.ToInt64(DataItem.TActualPrice * 100);
                            }
                            else
                            {
                                DataItem.ActualPrice = 0;
                            }
                            if (DataItem.TSellingPrice != null)
                            {
                                DataItem.SellingPrice = Convert.ToInt64(DataItem.TSellingPrice * 100);
                            }
                            else
                            {
                                DataItem.ActualPrice = 0;
                            }
                            DataItem.Amount = Convert.ToInt64(DataItem.TAmount * 100);

                            //if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            //{
                            //    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            //}
                            //else
                            //{
                            //    DataItem.IconUrl = _AppConfig.Default_Icon;
                            //}
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, _Deals, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                        #endregion
                    }

                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDeals", _Exception, _Request.UserReference, _OResponse, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDeal(ODeal.Request _Request)
        {
            #region Manage Exception
            try
            {
                _DealDetails = new ODeal.Details();
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    _DealDetails = _HCoreContext.MDDeal
                                            .Where(x => x.Guid == _Request.DealId)
                                            .Select(x => new ODeal.Details
                                            {
                                                DealReferenceId = x.Id,
                                                DealId = x.Guid,
                                                Title = x.Title,
                                                ImageUrl = _AppConfig.StorageUrl + x.PosterStorage.Path,
                                                Description = x.Description,
                                                MerchantReferenceId = x.Id,
                                                MerchantId = x.Guid,
                                                MerchantDisplayName = x.Account.DisplayName,
                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Account.IconStorage.Path,
                                                CategoryId = x.Category.Guid,
                                                CategoryName = x.Category.Name,
                                                StartDate = x.StartDate,
                                                EndDate = x.EndDate,
                                                TActualPrice = x.ActualPrice,
                                                TSellingPrice = x.SellingPrice,
                                                TAmount = x.Amount,
                                                Terms = x.Terms,
                                                TotalPurchase = x.MDDealCode.Count,
                                            }).FirstOrDefault();

                    #endregion
                    if (_DealDetails != null)
                    {
                        _DealDetails.Locations = _HCoreContext.MDDealLocation.Where(x => x.DealId == _DealDetails.DealReferenceId)
                           .Select(x => new ODeal.Location
                           {
                               LocationId = x.Guid,
                               Address = x.Location.Address,
                               DisplayName = x.Location.DisplayName,
                               Latitude = x.Location.Latitude,
                               Longitude = x.Location.Longitude,
                           }).ToList();
                        if (_DealDetails.Locations.Count == 0)
                        {
                            _DealDetails.Locations = _HCoreContext.HCUAccount.Where(x => x.OwnerId == _DealDetails.MerchantReferenceId
                            && x.StatusId == HelperStatus.Default.Active
                            && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore)
                           .Select(x => new ODeal.Location
                           {
                               LocationId = x.Guid,
                               Address = x.Address,
                               DisplayName = x.DisplayName,
                               Latitude = x.Latitude,
                               Longitude = x.Longitude,
                           }).ToList();
                        }

                        if (!string.IsNullOrEmpty(_DealDetails.ImageUrl))
                        {
                            _DealDetailImages = new List<ODeal.ImageUrlItem>();
                            _DealDetailImages.Add(new ODeal.ImageUrlItem
                            {
                                ImageUrl = _DealDetails.ImageUrl
                            });
                            _DealDetails.Images = _DealDetailImages;
                        }

                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        if (_DealDetails.TActualPrice != null)
                        {
                            _DealDetails.ActualPrice = Convert.ToInt64(_DealDetails.TActualPrice * 100);
                        }
                        else
                        {
                            _DealDetails.ActualPrice = 0;
                        }
                        if (_DealDetails.TSellingPrice != null)
                        {
                            _DealDetails.SellingPrice = Convert.ToInt64(_DealDetails.TSellingPrice * 100);
                        }
                        else
                        {
                            _DealDetails.ActualPrice = 0;
                        }
                        _DealDetails.Amount = Convert.ToInt64(_DealDetails.TAmount * 100);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DealDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                        #endregion
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DealDetails, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Shares the deal code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ShareDealCode(ODeal.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.DealCodeId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1397, TUCCoreResource.CA1397M);
                }
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1398, TUCCoreResource.CA1398M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _DealCode = _HCoreContext.MDDealCode
                                                  .Where(x =>
                                                  x.AccountId == _Request.AuthAccountId
                                                  && x.Guid == _Request.DealCodeId)
                                                  .FirstOrDefault();
                    if (_DealCode != null)
                    {
                        if (_DealCode.StatusId == HelperStatus.DealCodes.Unused)
                        {

                            string FormattedMobileNumber = HCoreHelper.FormatMobileNumber("234", _Request.MobileNumber);
                            long AccountId = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.SystemCountry && x.User.Username == FormattedMobileNumber).Select(x => x.Id).FirstOrDefault();
                            if (AccountId > 0)
                            {
                                if (AccountId != _Request.AuthAccountId)
                                {
                                    if (AccountId == _Request.UserReference.AccountId)
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1403, TUCCoreResource.CA1403M);
                                    }
                                    _DealCode.SecondaryAccountId = AccountId;
                                    _DealCode.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    _HCoreContext.SaveChanges();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1404, TUCCoreResource.CA1404M);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1399, TUCCoreResource.CA1399M);
                                }

                            }
                            else
                            {
                                ManageCoreUserAccess _ManageCoreUserAccess;
                                OAppProfile.Request _AppProfileRequest;
                                _AppProfileRequest = new OAppProfile.Request();
                                _AppProfileRequest.OwnerId = _Request.UserReference.AccountId;
                                _AppProfileRequest.CreatedById = _Request.UserReference.AccountId;
                                _AppProfileRequest.MobileNumber = _Request.MobileNumber;
                                _AppProfileRequest.DisplayName = _Request.MobileNumber;
                                _AppProfileRequest.UserReference = _Request.UserReference;
                                _ManageCoreUserAccess = new ManageCoreUserAccess();
                                OAppProfile.Response _AppUserCreateResponse = _ManageCoreUserAccess.CreateAppUserAccount(_AppProfileRequest);
                                if (_AppUserCreateResponse.Status == ResponseStatus.Success)
                                {

                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        var DealCode = _HCoreContext.MDDealCode
                                             .Where(x =>
                                               x.AccountId == _Request.AuthAccountId
                                              && x.Guid == _Request.DealCodeId)
                                             .FirstOrDefault();
                                        DealCode.SecondaryAccountId = _AppUserCreateResponse.AccountId;
                                        DealCode.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        _HCoreContext.SaveChanges();
                                    }
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1401, TUCCoreResource.CA1401M);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1400, TUCCoreResource.CA1400M);
                                }

                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1402, TUCCoreResource.CA1402M);
                        }
                        //}
                        //else
                        //{
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1399, TUCCoreResource.CA1399M);
                        //}
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDealCode", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }
}
