//==================================================================================
// FileName: FrameworkAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to accounts
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Object.CustomerWeb;
using HCore.TUC.Core.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;


namespace HCore.TUC.Core.Framework.CustomerWeb
{
    public class FrameworkAccount
    {
        ManageCoreTransaction _ManageCoreTransaction;
        HCoreContext _HCoreContext;
        /// <summary>
        /// Description: Updates the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateAccount(Object.CustomerWeb.OAccount.Request _Request)
        {
            try
            {
                #region Manage Operations
                if (_Request.AuthAccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1371, TUCCoreResource.CA1371M);
                }
                if (string.IsNullOrEmpty(_Request.FirstName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1367, TUCCoreResource.CA1367M);
                }
                if (string.IsNullOrEmpty(_Request.LastName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1368, TUCCoreResource.CA1368M);
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1369, TUCCoreResource.CA1369M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.Id == _Request.AuthAccountId).FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.FirstName))
                        {
                            AccountDetails.DisplayName = _Request.FirstName;
                            AccountDetails.FirstName = _Request.FirstName;
                        }
                        if (!string.IsNullOrEmpty(_Request.MiddleName))
                        {
                            AccountDetails.MiddleName = _Request.MiddleName;
                        }
                        if (!string.IsNullOrEmpty(_Request.LastName))
                        {
                            AccountDetails.LastName = _Request.LastName;
                        }
                        if (!string.IsNullOrEmpty(_Request.FirstName) && !string.IsNullOrEmpty(_Request.LastName))
                        {
                            AccountDetails.Name = _Request.FirstName + " " + _Request.LastName;
                        }
                        if (!string.IsNullOrEmpty(_Request.EmailAddress))
                        {
                            AccountDetails.EmailAddress = _Request.EmailAddress;
                        }
                        if (_Request.DateOfBirth != null)
                        {
                            AccountDetails.DateOfBirth = _Request.DateOfBirth;
                        }
                        if (!string.IsNullOrEmpty(_Request.Address))
                        {
                            AccountDetails.Address = _Request.Address;
                        }
                        if (!string.IsNullOrEmpty(_Request.Gender))
                        {
                            if (_Request.Gender == "male")
                            {
                                AccountDetails.GenderId = Gender.Male;
                            }
                            if (_Request.Gender == "female")
                            {
                                AccountDetails.GenderId = Gender.Female;
                            }
                            if (_Request.Gender == "other")
                            {
                                AccountDetails.GenderId = Gender.Other;
                            }
                        }
                        AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();
                        long? ImageStorageId = null;
                        if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                        {
                            ImageStorageId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, AccountDetails.IconStorageId, _Request.UserReference);
                        }
                        if (ImageStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var Details = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AuthAccountId).FirstOrDefault();
                                if (Details != null)
                                {
                                    if (ImageStorageId != null)
                                    {
                                        Details.IconStorageId = ImageStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1370, TUCCoreResource.CA1370M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("UpdateAccount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }


        /// <summary>
        /// Description: Updates referral Id.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateReferralId(Object.CustomerWeb.OAccount.Request _Request)
        {
            try
            {
                #region Manage Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.Id == _Request.AuthAccountId).FirstOrDefault();
                    if (AccountDetails != null)
                    {

                        if (!string.IsNullOrEmpty(_Request.ReferralCode))
                        {
                            var existingReferalCodeCheck = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.Id == _Request.AuthAccountId && x.ReferralCode == _Request.ReferralCode).Count();

                            if (existingReferalCodeCheck > 0)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1177, TUCCoreResource.CA1177M);
                            }
                            else
                            {
                                AccountDetails.ReferralCode = _Request.ReferralCode;
                            }

                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1174, TUCCoreResource.CA1174M);
                        }

                        AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1178, TUCCoreResource.CA1178M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("UpdateAccount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }

        /// <summary>
        /// Description: Set App Logo.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SetAppLogo(HCore.TUC.Core.Object.Console.OAdminUser.AuthUpdate.LogoRequest _Request)
        {
            try
            {
                return null;
                //long? ImageStorageId = null;
                //#region Manage Operations

                //using (_HCoreContext = new HCoreContext())
                //{

                //    if (_Request.ImageContent != null)
                //    {

                //        if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                //        {
                //            ImageStorageId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, null, _Request.UserReference);
                //        }
                //        if (ImageStorageId != null)
                //        {
                //            using (_HCoreContext = new HCoreContext())
                //            {
                //                var SaveDetails = _HCoreContext.LogoSettings.Where(x => x.Status == 2 && x.Panel == _Request.Panel).FirstOrDefault();
                //                if (SaveDetails != null)
                //                {
                //                    if (ImageStorageId != null)
                //                    {
                //                        SaveDetails.ImageStorageId = (long)ImageStorageId;
                //                    }
                //                    _HCoreContext.SaveChanges();
                //                    _HCoreContext.Dispose();
                //                }
                //                else
                //                {
                //                    var newlogo = new LogoSettings
                //                    {
                //                        ImageStorageId = ImageStorageId,
                //                        UploadDate = DateTime.Now,
                //                        Uploader = _Request.UserReference.EmailAddress,
                //                        Status = 2,
                //                        Panel = _Request.Panel
                //                    };

                //                    _HCoreContext.LogoSettings.Add(newlogo);
                //                    _HCoreContext.SaveChanges();
                //                }
                //            }
                //        }
                //        else
                //        {
                //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1548, TUCCoreResource.CA1548M);
                //        }
                //    }



                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCCoreResource.CA1545, TUCCoreResource.CA1545M);

                //}
                //#endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SetAppLogo", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }


        /// <summary>
        /// Description: Gets app Logo.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAppLogo(OAdminUser.AuthUpdate.LogoRequest _Request)
        {
            try
            {
                return null;
                //#region Manage Operations

                //using (_HCoreContext = new HCoreContext())
                //{
                //    var _Details = _HCoreContext.LogoSettings.Where(x => x.Panel == _Request.Panel && x.Status == 2)
                //        .Select(x => new OAdminUser.AuthUpdate.LogoResponse
                //        {

                //            ImageUrl = x.ImageStorage.Path,
                //            UploadedBy = x.Uploader,
                //            UploadDate = x.UploadDate

                //        }).FirstOrDefault();

                //    if (_Details != null)
                //    {
                //        if (!string.IsNullOrEmpty(_Details.ImageUrl))
                //        {
                //            _Details.ImageUrl = _AppConfig.StorageUrl + _Details.ImageUrl;
                //        }
                //        else
                //        {
                //            _Details.ImageUrl = _AppConfig.Default_Icon;
                //        }
                //        _HCoreContext.Dispose();
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCCoreResource.CA1547, TUCCoreResource.CA1547M);
                //    }
                //    else
                //    {
                //        _HCoreContext.Dispose();
                //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                //    }
                //}
                //#endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GetAdminLogo", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }



        /// <summary>
        /// Description: Fetchess referral Amount Details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetReferralAmount(Object.CustomerWeb.OAccount.ReferralAmountRequest _Request)
        {
            try
            {
                #region Manage Operations
                if (_Request.AuthAccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1371, TUCCoreResource.CA1371M);
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1369, TUCCoreResource.CA1369M);
                }
                if (_Request.Amount <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1382, TUCCoreResource.CA1382M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var ReferralDetails = _HCoreContext.ReferralSettings.Where(x => x.Status == HelperStatus.Default.Active)
                        .Select(x => new OReferralAmountResponse
                        {
                            ResponseCode = "00",
                            ResponseMessage = "Referral Amount Fetched Successfully",
                            Amount = x.Amount,
                            CreatedBy = x.SetBy,

                        })
                        .FirstOrDefault();
                    if (ReferralDetails != null)
                    {

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ReferralDetails, TUCCoreResource.CA1540, TUCCoreResource.CA1540M);

                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1539, TUCCoreResource.CA1539M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GetReferralAmount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }


        /// <summary>
        /// Description: Sets Referral Amount.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SetReferralAmount(Object.CustomerWeb.OAccount.ReferralAmountRequest _Request)
        {
            try
            {
                #region Manage Operations
                if (_Request.AuthAccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1371, TUCCoreResource.CA1371M);
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1369, TUCCoreResource.CA1369M);
                }
                if (_Request.Amount <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1382, TUCCoreResource.CA1382M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var ReferralDetails = _HCoreContext.ReferralSettings.Where(x => x.Status == HelperStatus.Default.Active).ToList();
                    foreach (var records in ReferralDetails)
                    {
                        records.Status = 0;
                    }

                    var newReferralSettings = new ReferralSettings
                    {
                        Amount = _Request.Amount,
                        DateSet = DateTime.Now,
                        SetBy = _Request.EmailAddress,
                        Status = 2
                    };

                    _HCoreContext.ReferralSettings.Add(newReferralSettings);
                    _HCoreContext.SaveChanges();

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ReferralDetails, TUCCoreResource.CA1541, TUCCoreResource.CA1541M);

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SetReferralAmount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }


        /// <summary>
        /// Description: Gets Referral Bonus History.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetBonusHistory(Object.CustomerWeb.OAccount.ReferralAmountRequest _Request)
        {
            try
            {
                #region Manage Operations
                if (_Request.AuthAccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1371, TUCCoreResource.CA1371M);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var ReferralDetails = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == _Request.AuthAccountId && x.ReferenceNumber.Contains("Ref")).ToList();
                    if (ReferralDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ReferralDetails, TUCCoreResource.CA1543, TUCCoreResource.CA1543M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, ReferralDetails, TUCCoreResource.CA1542, TUCCoreResource.CA1542M);
                    }

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GetBonusHistory", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }


        /// <summary>
        /// Description: Gets the account details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccount(Object.CustomerWeb.OAccount.Request _Request)
        {
            try
            {
                if (_Request.AuthAccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1371, TUCCoreResource.CA1371M);
                }
                #region Manage Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount
                        .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.Id == _Request.AuthAccountId)
                        .Select(x => new Object.CustomerWeb.OAccount.Response
                        {
                            DisplayName = x.DisplayName,
                            FirstName = x.FirstName,
                            MiddleName = x.MiddleName,
                            LastName = x.LastName,
                            Name = x.Name,
                            EmailAddress = x.EmailAddress,
                            Address = x.Address,
                            IconUrl = x.IconStorage.Path,
                            DateOfBirth = x.DateOfBirth,
                            Gender = x.Gender.Name,
                            MobileNumber = x.MobileNumber,
                            ReferralCode = x.ReferralCode,
                        })
                        .FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        if (!string.IsNullOrEmpty(AccountDetails.Gender))
                        {
                            AccountDetails.Gender = AccountDetails.Gender.ToLower();
                        }
                        if (!string.IsNullOrEmpty(AccountDetails.IconUrl))
                        {
                            AccountDetails.IconUrl = _AppConfig.StorageUrl + AccountDetails.IconUrl;
                        }
                        else
                        {
                            AccountDetails.IconUrl = _AppConfig.Default_Icon;
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, AccountDetails, TUCCoreResource.CA0200, TUCCoreResource.CA0200M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("UpdateAccount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }

        Object.CustomerWeb.OAccount.OBalance _Balance;
        /// <summary>
        /// Description: Gets the account balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountBalance(Object.CustomerWeb.OAccount.Request _Request)
        {
            try
            {
                #region Manage Operations
                if (_Request.AuthAccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1371, TUCCoreResource.CA1371M);
                }
                _ManageCoreTransaction = new ManageCoreTransaction();
                double AccountBalance = _ManageCoreTransaction.GetAppUserBalance(_Request.AuthAccountId, true);
                _Balance = new Object.CustomerWeb.OAccount.OBalance();
                _Balance.Balance = Convert.ToInt64(Math.Round(AccountBalance, 2) * 100);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Balance, TUCCoreResource.CA1372, TUCCoreResource.CA1372M);
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GetAccountBalance", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }


        /// <summary>
        /// Description: Gets the account balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse FundWallet(Object.CustomerWeb.OAccount.Request _Request)
        {
            try
            {
                #region Manage Operations
                if (_Request.AuthAccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1371, TUCCoreResource.CA1371M);
                }
                _ManageCoreTransaction = new ManageCoreTransaction();
                double AccountBalance = _ManageCoreTransaction.GetAppUserBalance(_Request.AuthAccountId, true);
                _Balance = new Object.CustomerWeb.OAccount.OBalance();
                _Balance.Balance = Convert.ToInt64(Math.Round(AccountBalance, 2) * 100);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Balance, TUCCoreResource.CA1372, TUCCoreResource.CA1372M);
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GetAccountBalance", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }

        /// <summary>
        /// Description: Validates the pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ValidatePin(Object.CustomerWeb.OAccount.ValidatePin.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AuthAccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1371, TUCCoreResource.CA1371M);
                }
                else if (string.IsNullOrEmpty(_Request.Pin))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1442, TUCCoreResource.CA1442M);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x =>
                        x.AccountTypeId == UserAccountType.Appuser &&
                        x.Id == _Request.AuthAccountId)
                        .FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            string NewPin = HCoreEncrypt.DecryptHash(AccountDetails.AccessPin);
                            if (NewPin == _Request.Pin)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCCoreResource.CA1444, TUCCoreResource.CA1444M);
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA1443, TUCCoreResource.CA1443M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0404, TUCCoreResource.CA0404M);
                        }
                    }

                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ValidatePin", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the account balance open.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountBalanceOpen(Object.CustomerWeb.OAccount.OCustReq.Req _Request)
        {
            try
            {

                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, TUCCoreResource.CA1435, TUCCoreResource.CA1435M);
                }
                else if (string.IsNullOrEmpty(_Request.Pin))
                {
                    return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, TUCCoreResource.CA1436, TUCCoreResource.CA1436M);
                }
                else
                {
                    string FormatM = _AppConfig.AppUserPrefix + HCoreHelper.FormatMobileNumber("234", _Request.MobileNumber);
                    using (HCoreContext _HCoreContext = new HCoreContext())
                    {
                        var UAccount = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.StatusId == HelperStatus.Default.Active && (x.User.Username == FormatM || x.AccountCode == _Request.MobileNumber))
                            .Select(x => new
                            {
                                AccountId = x.Id,
                                Pin = x.AccessPin,
                            }).FirstOrDefault();
                        if (UAccount != null)
                        {
                            string DApin = HCoreEncrypt.DecryptHash(UAccount.Pin);
                            if (DApin == _Request.Pin)
                            {
                                if (_Request.Host == "accessrewards.thankucash.com")
                                {
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    var ProgramDetails = _ManageCoreTransaction.GetLoyaltyProgramBalance(UAccount.AccountId, 0, 48415);
                                    if (ProgramDetails != null)
                                    {
                                        _Balance = new Object.CustomerWeb.OAccount.OBalance();
                                        _Balance.Balance = Convert.ToInt64(Math.Round(ProgramDetails.Balance, 2) * 100);
                                        return HCoreHelper.SendResponse(null, ResponseStatus.Success, _Balance, TUCCoreResource.CA1372, TUCCoreResource.CA1372M);
                                    }
                                    else
                                    {
                                        _Balance = new Object.CustomerWeb.OAccount.OBalance();
                                        _Balance.Balance = 0;
                                        return HCoreHelper.SendResponse(null, ResponseStatus.Success, _Balance, TUCCoreResource.CA1372, TUCCoreResource.CA1372M);

                                    }
                                }
                                else
                                {
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    double AccountBalance = _ManageCoreTransaction.GetAppUserBalance(UAccount.AccountId, true);
                                    _Balance = new Object.CustomerWeb.OAccount.OBalance();
                                    _Balance.Balance = Convert.ToInt64(Math.Round(AccountBalance, 2) * 100);
                                    return HCoreHelper.SendResponse(null, ResponseStatus.Success, _Balance, TUCCoreResource.CA1372, TUCCoreResource.CA1372M);

                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, TUCCoreResource.CA1438, TUCCoreResource.CA1438M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, TUCCoreResource.CA1437, TUCCoreResource.CA1437M);
                        }
                    }
                }
                //if (_Request.AuthAccountId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1371, TUCCoreResource.CA1371M);
                //}

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GetAccountBalanceOpen", _Exception, null);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }
        /// <summary>
        /// Description: Registers the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse RegisterAccount(Object.CustomerWeb.OAccount.OCustRegReq.Req _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.FirstName))
                {
                    return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, TUCCoreResource.CAA1471, TUCCoreResource.CAA1471M);
                }
                if (string.IsNullOrEmpty(_Request.LastName))
                {
                    return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, TUCCoreResource.CAA1472, TUCCoreResource.CAA1472M);
                }
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, TUCCoreResource.CA1435, TUCCoreResource.CA1435M);
                }

                else if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, TUCCoreResource.CAA1473, TUCCoreResource.CAA1473);
                }
                else
                {
                    string FormatM = _AppConfig.AppUserPrefix + HCoreHelper.FormatMobileNumber("234", _Request.MobileNumber);
                    using (HCoreContext _HCoreContext = new HCoreContext())
                    {
                        var UAccount = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.User.Username == FormatM)
                            .Select(x => new
                            {
                                AccountId = x.Id,
                                Pin = x.AccessPin,
                            }).FirstOrDefault();
                        if (UAccount != null)
                        {
                            return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, TUCCoreResource.CAA1474, TUCCoreResource.CAA1474M);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(null, ResponseStatus.Success, null, TUCCoreResource.CAA1475, TUCCoreResource.CAA1475M);
                        }
                    }
                }
                //if (_Request.AuthAccountId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1371, TUCCoreResource.CA1371M);
                //}

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("RegisterAccount", _Exception, null);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
                #endregion
            }
        }
    }
}
