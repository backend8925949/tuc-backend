//==================================================================================
// FileName: CronMerchantCategorySync.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant.HelperStatus;




namespace HCore.TUC.Core.Cron
{

    internal class ActorCoreCronMerchantCategorySync : ReceiveActor
    {
        CoreCronMerchantCategorySync _CoreCronMerchantCategorySync;
        public ActorCoreCronMerchantCategorySync()
        {
            Receive<string>(_Request =>
            {
                _CoreCronMerchantCategorySync = new CoreCronMerchantCategorySync();
                _CoreCronMerchantCategorySync.UpdateMerchantCategory();
            });
        }
    }
    internal class CoreCronMerchantCategorySync
    {
        HCoreContext _HCoreContext;
        internal void UpdateMerchantCategory()
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {

                    //var MerchantCategories = _HCoreContext.HCUAccountParameter
                    //    .Where(x => x.TypeId == HelperType.MerchantCategory
                    //    && x.StatusId == Default.Active
                    //    && x.Account.AccountTypeId == UserAccountType.Merchant
                    //    && x.CategoryId != null
                    //    && x.Account.PrimaryCategoryId == null)
                    //    .Select(x => new
                    //    {
                    //        MerchantId = x.AccountId,
                    //        CategoryId = x.CategoryId,
                    //    })
                    //    .Skip(0)
                    //    .Take(100)
                    //    .ToList();
                    var MerchantCategories = _HCoreContext.TUCCategoryAccount
                       .Where(x => x.TypeId == 1
                       && x.StatusId == Default.Active
                       && x.Account.AccountTypeId == UserAccountType.Merchant
                       && x.Account.PrimaryCategoryId == null)
                       .Select(x => new
                       {
                           MerchantId = x.AccountId,
                           CategoryId = x.CategoryId,
                       })
                       .Skip(0)
                       .Take(100)
                       .ToList();

                    if (MerchantCategories.Count > 0)
                    {
                        foreach (var MerchantCategory in MerchantCategories)
                        {
                            var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Id == MerchantCategory.MerchantId).FirstOrDefault();
                            if (MerchantDetails != null)
                            {
                                MerchantDetails.PrimaryCategoryId = MerchantCategory.CategoryId;
                            }
                        }
                        _HCoreContext.SaveChanges();
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateMerchantCategory", _Exception);
            }
        }
    }

    public class AcquirerCampaign
    {
        HCoreContext _HCoreContext;
        public void UpdateAcquirerCampaignStatus()
        {
            DateTime CurrentDate = HCoreHelper.GetGMTDateTime().AddHours(1);
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var Campaigns = _HCoreContext.TUCampaign.Where(x => x.EndDate < CurrentDate && x.StatusId != HelperStatus.Campaign.Expired).ToList();
                    if (Campaigns.Count > 0)
                    {
                        foreach (var Campaign in Campaigns)
                        {
                            Campaign.StatusId = HelperStatus.Campaign.Expired;
                            Campaign.ModifyDate = HCoreHelper.GetGMTDateTime();
                        }
                    }
                    _HCoreContext.SaveChanges();
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateAcquirerCampaignStatus", _Exception);
            }
        }
    }
}
