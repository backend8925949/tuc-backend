//==================================================================================
// FileName: CoreSale.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.Core
{
    internal class ActorTestTransaction : ReceiveActor
    {
        internal class OTerminals
        {
            public long? StoreId { get; set; }
            public long TerminalId { get; set; }
            public long? ProviderId { get; set; }
            public long? AcquirerId { get; set; }
        }

        internal class OBin
        {
            public int Id { get; set; }
            public string? BinNumber { get; set; }
        }
        internal List<long> TCustomers = new List<long>();
        internal List<long> TCashiers = new List<long>();
        internal List<OBin> BinNumbers = new List<OBin>();
        internal List<OTerminals> Terminals = new List<OTerminals>();
        private bool IsTestTransactionProcessing = false;
        static readonly Random rnd = new Random();
        public static DateTime GetRandomDate(DateTime from, DateTime to)
        {
            var range = to - from;
            var randTimeSpan = new TimeSpan((long)(rnd.NextDouble() * range.Ticks));
            return from + randTimeSpan;
        }
        public ActorTestTransaction()
        {
            Receive<string>(_Request =>
            {
                try
                {
                    if (!IsTestTransactionProcessing)
                    {
                        IsTestTransactionProcessing = true;
                        using (HCoreContext _HCoreContext = new HCoreContext())
                        {
                            long MerchantId = 96415;
                            if (TCustomers.Count == 0)
                            {
                                TCustomers = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser).Select(x => x.Id).ToList();
                            }
                            DateTime LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.ParentId == 96415).OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();
                            if (Terminals.Count == 0)
                            {
                                Terminals = _HCoreContext.TUCTerminal
                             .Where(x => x.MerchantId == MerchantId
                             && x.StatusId == HelperStatus.Default.Active)
                             .Select(x => new OTerminals
                             {
                                 TerminalId = x.Id,
                                 StoreId = x.StoreId,
                                 ProviderId = x.ProviderId,
                                 AcquirerId = x.AcquirerId,
                             }).ToList();
                            }

                            if (TCashiers.Count == 0)
                            {
                                TCashiers = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.MerchantCashier && (x.SubOwner.OwnerId == MerchantId || x.OwnerId == MerchantId || x.Owner.OwnerId == MerchantId)).Select(x => x.Id).ToList();
                            }
                            if (BinNumbers.Count == 0)
                            {
                                BinNumbers = _HCoreContext.HCCoreBinNumber.Select(x => new OBin
                                {
                                    Id = x.Id,
                                    BinNumber = x.Bin,
                                }).ToList();
                            }
                            int[] TransactionModes = { 148, 155 };
                            _HCoreContext.Dispose();
                            Random _Random = new Random();
                            int Loop = _Random.Next(1, 8);
                            for (int i = 0; i < Loop; i++)
                            {
                                var random = new Random();
                                var TerminalInfo = Terminals[random.Next(Terminals.Count)];
                                var CashierId = TCashiers[random.Next(TCashiers.Count)]; //  Cashiers.OrderBy(s => random.NextDouble()).First();
                                var UserAccountId = TCustomers[random.Next(TCustomers.Count)];  /// Customers.OrderBy(s => random.NextDouble()).First();
                                var BinNumberId = BinNumbers[random.Next(BinNumbers.Count)]; // BinNumbers.OrderBy(s => random.NextDouble()).First();
                                int TransactionTypeId = TransactionModes[random.Next(TransactionModes.Length)];  //TransactionModes.OrderBy(s => random.NextDouble()).First();
                                var TransactionDate = GetRandomDate(LastTransactionDate, HCoreHelper.GetGMTDateTime());
                                OUserReference _UserReference = new OUserReference();
                                _UserReference.AccountId = (long)TerminalInfo.ProviderId;
                                _UserReference.AccountTypeId = UserAccountType.PosAccount;
                                Random rnd = new Random();
                                double InvoiceAmount = rnd.Next(800, 10000);
                                double RewardAmount = Math.Round(HCoreHelper.GetPercentage(InvoiceAmount, 5), 2);
                                double UserAmount = Math.Round(HCoreHelper.GetPercentage(RewardAmount, 70), 2);
                                double CommissionAmount = Math.Round(RewardAmount - UserAmount, 2);
                                double IssuerAmount = Math.Round(HCoreHelper.GetPercentage(CommissionAmount, 5), 2);
                                CommissionAmount = CommissionAmount - IssuerAmount;
                                OCoreTransaction.Request _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.CashierId = CashierId;
                                _CoreTransactionRequest.CustomerId = UserAccountId;
                                _CoreTransactionRequest.UserReference = _UserReference;
                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                _CoreTransactionRequest.ParentId = MerchantId;
                                _CoreTransactionRequest.InvoiceAmount = InvoiceAmount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = InvoiceAmount;
                                if (TransactionTypeId == TransactionType.CardReward)
                                {
                                    _CoreTransactionRequest.AccountNumber = BinNumberId.BinNumber + "XXXXXXX";
                                    _CoreTransactionRequest.BinNumberId = BinNumberId.Id;
                                }
                                _CoreTransactionRequest.ReferenceNumber = HCoreHelper.GenerateDateString();
                                _CoreTransactionRequest.TerminalId = TerminalInfo.TerminalId;
                                _CoreTransactionRequest.SubParentId = (long)TerminalInfo.StoreId;
                                if (TerminalInfo.AcquirerId != null)
                                {
                                    _CoreTransactionRequest.BankId = (long)TerminalInfo.AcquirerId;
                                }
                                _CoreTransactionRequest.ReferenceAmount = RewardAmount;
                                _CoreTransactionRequest.CreatedById = TerminalInfo.TerminalId;
                                List<OCoreTransaction.TransactionItem> _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = MerchantId,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.Merchant,

                                    Amount = UserAmount,
                                    Comission = CommissionAmount,
                                    TotalAmount = RewardAmount,
                                    TransactionDate = TransactionDate,
                                });
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = UserAccountId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.TUC,

                                    Amount = UserAmount,
                                    TotalAmount = UserAmount,
                                    TransactionDate = TransactionDate,
                                });
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = SystemAccounts.ThankUCashSystemId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.Settlement,

                                    Amount = CommissionAmount,
                                    TotalAmount = CommissionAmount,
                                    TransactionDate = TransactionDate,
                                });
                                //_TransactionItems.Add(new OCoreTransaction.TransactionItem
                                //{
                                //    UserAccountId = 7464,
                                //    ModeId = TransactionMode.Credit,
                                //    TypeId = TransactionType.ReferralBonus,
                                //    SourceId = TransactionSource.TUC,
                                //    Amount = IssuerAmount,
                                //    TotalAmount = IssuerAmount,
                                //    TransactionDate = TransactionDate,
                                //    StatusId = HelperStatus.Transaction.Pending,
                                //});
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                ManageCoreTransaction _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);

                            }
                        }
                        IsTestTransactionProcessing = false;
                    }
                }
                catch (Exception _Exception)
                {
                    IsTestTransactionProcessing = false;
                    HCoreHelper.LogException("TUCCORE-ActorTestTransaction", _Exception);
                }
            });
        }
    }


    internal class ActorManagmentTestTransaction : ReceiveActor
    {
        internal class OTerminals
        {
            public long? StoreId { get; set; }
            public long TerminalId { get; set; }
            public long? ProviderId { get; set; }
            public long? AcquirerId { get; set; }
            public long? MerchantId { get; set; }
            public DateTime? LastTransaction { get; set; }
        }
        internal class OCashier
        {
            public long? StoreId { get; set; }
            public long CashierId { get; set; }
            public long? MerchantId { get; set; }
            //public long? ProviderId { get; set; }
            //public long? AcquirerId { get; set; }
            //public DateTime? LastTransaction { get; set; }
        }
        internal class OBin
        {
            public int Id { get; set; }
            public string? BinNumber { get; set; }
        }
        internal class OStore
        {
            public long Id { get; set; }
            public long MerchantId { get; set; }
            public double? RewardPercentage { get; set; }
        }

        //internal List<long> TMerchants = new List<long>();
        internal List<long> TCustomers = new List<long>();
        internal List<OCashier> TCashiers = new List<OCashier>();
        internal List<OBin> BinNumbers = new List<OBin>();
        internal List<OTerminals> Terminals = new List<OTerminals>();
        internal List<OStore> Stores = new List<OStore>();
        private bool IsTestTransactionProcessing = false;
        static readonly Random rnd = new Random();

        HCUAccountTransaction _HCUAccountTransaction;
        List<HCUAccountTransaction> _HCUAccountTransactions;
        public static DateTime GetRandomDate(DateTime from, DateTime to)
        {
            var range = to - from;
            var randTimeSpan = new TimeSpan((long)(rnd.NextDouble() * range.Ticks));
            return from + randTimeSpan;
        }
        public ActorManagmentTestTransaction()
        {
            Receive<string>(_Request =>
            {
                try
                {
                    if (!IsTestTransactionProcessing)
                    {
                        IsTestTransactionProcessing = true;
                        using (HCoreContext _HCoreContext = new HCoreContext())
                        {
                            if (Stores.Count == 0)
                            {
                                Stores = _HCoreContext.HCUAccount
                                .Where(x => x.AccountTypeId == UserAccountType.MerchantStore && x.StatusId == HelperStatus.Default.Active)
                                .Select(x => new OStore
                                {
                                    Id = x.Id,
                                    MerchantId = x.Owner.Id,
                                    RewardPercentage = x.AccountPercentage
                                }).ToList();
                            }
                            if (TCustomers.Count == 0)
                            {
                                TCustomers = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser).Select(x => x.Id).ToList();
                            }
                            if (Terminals.Count == 0)
                            {
                                Terminals = _HCoreContext.TUCTerminal
                             .Where(x => x.StatusId == HelperStatus.Default.Active)
                             .Select(x => new OTerminals
                             {
                                 TerminalId = x.Id,
                                 StoreId = x.StoreId,
                                 ProviderId = x.ProviderId,
                                 AcquirerId = x.AcquirerId,
                                 MerchantId = x.MerchantId,
                                 LastTransaction = null,
                             }).ToList();
                            }
                            if (TCashiers.Count == 0)
                            {
                                //TCashiers = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.MerchantCashier && (x.SubOwner.OwnerId == MerchantId || x.OwnerId == MerchantId || x.Owner.OwnerId == MerchantId)).Select(x => x.Id).ToList();
                                TCashiers = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.MerchantCashier
                                ).Select(x => new OCashier
                                {
                                    CashierId = x.Id,
                                    StoreId = x.OwnerId,
                                    MerchantId = x.Owner.OwnerId,
                                }).ToList();
                            }
                            if (BinNumbers.Count == 0)
                            {
                                BinNumbers = _HCoreContext.HCCoreBinNumber.Select(x => new OBin
                                {
                                    Id = x.Id,
                                    BinNumber = x.Bin,
                                }).ToList();
                            }
                            int[] TransactionModes = { 148, 155 };
                            _HCoreContext.Dispose();
                            Random _Random = new Random();
                            int Loop = _Random.Next(1, 50);
                            for (int i = 0; i < Loop; i++)
                            {
                                var random = new Random();
                                var TerminalInfo = Terminals[random.Next(Terminals.Count)];
                                //if (TerminalInfo.LastTransaction == null)
                                //{
                                //    TerminalInfo.LastTransaction = HCoreHelper.GetGMTDateTime().AddDays(-1);
                                //}
                                //if (TerminalInfo.LastTransaction.Value.Year == 0001)
                                //{
                                //    TerminalInfo.LastTransaction = HCoreHelper.GetGMTDateTime().AddDays(-1);
                                //}
                                TerminalInfo.LastTransaction = HCoreHelper.GetGMTDateTime().AddMinutes(-1);
                                var MCashiers = TCashiers.Where(a => a.StoreId == TerminalInfo.StoreId || a.MerchantId == TerminalInfo.MerchantId).ToList();
                                long CashierId = 0;
                                if (MCashiers.Count > 0)
                                {
                                    CashierId = MCashiers[random.Next(MCashiers.Count)].CashierId; //  Cashiers.OrderBy(s => random.NextDouble()).First();
                                }
                                var UserAccountId = TCustomers[random.Next(TCustomers.Count)];  /// Customers.OrderBy(s => random.NextDouble()).First();
                                var BinNumberId = BinNumbers[random.Next(BinNumbers.Count)]; // BinNumbers.OrderBy(s => random.NextDouble()).First();
                                int TransactionTypeId = TransactionModes[random.Next(TransactionModes.Length)];  //TransactionModes.OrderBy(s => random.NextDouble()).First();
                                var TransactionDate = GetRandomDate((DateTime)TerminalInfo.LastTransaction, HCoreHelper.GetGMTDateTime());
                                OUserReference _UserReference = new OUserReference();
                                _UserReference.AccountId = (long)TerminalInfo.ProviderId;
                                _UserReference.AccountTypeId = UserAccountType.PosAccount;
                                Random rnd = new Random();
                                double InvoiceAmount = rnd.Next(800, 10000);
                                double RewardAmount = Math.Round(HCoreHelper.GetPercentage(InvoiceAmount, 5), 2);
                                double UserAmount = Math.Round(HCoreHelper.GetPercentage(RewardAmount, 70), 2);
                                double CommissionAmount = Math.Round(RewardAmount - UserAmount, 2);
                                double IssuerAmount = Math.Round(HCoreHelper.GetPercentage(CommissionAmount, 5), 2);
                                CommissionAmount = CommissionAmount - IssuerAmount;
                                OCoreTransaction.Request _CoreTransactionRequest = new OCoreTransaction.Request();
                                if (CashierId > 0)
                                {
                                    _CoreTransactionRequest.CashierId = CashierId;
                                }
                                _CoreTransactionRequest.CustomerId = UserAccountId;
                                _CoreTransactionRequest.UserReference = _UserReference;
                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                _CoreTransactionRequest.ParentId = (long)TerminalInfo.MerchantId;
                                _CoreTransactionRequest.InvoiceAmount = InvoiceAmount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = InvoiceAmount;
                                if (TransactionTypeId == TransactionType.CardReward)
                                {
                                    _CoreTransactionRequest.AccountNumber = BinNumberId.BinNumber + "XXXXXXX";
                                    _CoreTransactionRequest.BinNumberId = BinNumberId.Id;
                                }
                                _CoreTransactionRequest.ReferenceNumber = HCoreHelper.GenerateDateString();
                                _CoreTransactionRequest.TerminalId = TerminalInfo.TerminalId;
                                _CoreTransactionRequest.SubParentId = (long)TerminalInfo.StoreId;
                                if (TerminalInfo.AcquirerId != null)
                                {
                                    _CoreTransactionRequest.BankId = (long)TerminalInfo.AcquirerId;
                                }
                                _CoreTransactionRequest.ReferenceAmount = RewardAmount;
                                _CoreTransactionRequest.CreatedById = TerminalInfo.TerminalId;
                                List<OCoreTransaction.TransactionItem> _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = (long)TerminalInfo.MerchantId,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.Merchant,

                                    Amount = UserAmount,
                                    Comission = CommissionAmount,
                                    TotalAmount = RewardAmount,
                                    TransactionDate = TransactionDate,
                                });
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = UserAccountId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.TUC,

                                    Amount = UserAmount,
                                    TotalAmount = UserAmount,
                                    TransactionDate = TransactionDate,
                                });
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = SystemAccounts.ThankUCashSystemId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.Settlement,

                                    Amount = CommissionAmount,
                                    TotalAmount = CommissionAmount,
                                    TransactionDate = TransactionDate,
                                });
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                ManageCoreTransaction _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);

                            }
                        }
                        IsTestTransactionProcessing = false;
                    }
                }
                catch (Exception _Exception)
                {
                    IsTestTransactionProcessing = false;
                    HCoreHelper.LogException("TUCCORE-ActorTestTransaction", _Exception);
                }
            });
        }
    }






    //internal class ActorProcessSale : ReceiveActor
    //{
    //    private bool IsSaleProcessing = false;
    //    public ActorProcessSale()
    //    {
    //        Receive<string>(_Request =>
    //        {
    //            try
    //            {
    //                if (!IsSaleProcessing)
    //                {
    //                    IsSaleProcessing = true;
    //                    using (HCoreContext _HCoreContext = new HCoreContext())
    //                    {
    //                        //DateTime LastSaleUpdateDate = _HCoreContext.TUCSale.OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();
    //                        #region Get Data
    //                        var _Sales = _HCoreContext.HCUAccountTransaction
    //                                                      .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
    //                                                      && x.TUCSale.Any() == false
    //                                                      && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus)
    //                                                      && (x.TypeId == TransactionType.CardReward || x.TypeId == TransactionType.CashReward || x.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay))
    //                                                      .OrderBy(x => x.TransactionDate)
    //                                                      .Select(x => new
    //                                                      {
    //                                                          ReferenceId = x.Id,
    //                                                          Guid = x.Guid,
    //                                                          GroupKey = x.GroupKey,
    //                                                          InvoiceAmount = x.PurchaseAmount,
    //                                                          TransactionDate = x.TransactionDate,
    //                                                          SourceId = x.SourceId,
    //                                                          TypeId = x.TypeId,
    //                                                          AccountNumber = x.AccountNumber,
    //                                                          ReferenceNumber = x.ReferenceNumber,
    //                                                          CustomerId = x.AccountId,
    //                                                          MerchantId = x.ParentId,
    //                                                          StoreId = x.SubParentId,
    //                                                          ProviderId = x.ProviderId,
    //                                                          AcquirerId = x.BankId,
    //                                                          CreatedById = x.CreatedById,
    //                                                          CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
    //                                                          CashierId = x.CashierId,
    //                                                          InvoiceNumber = x.InoviceNumber,
    //                                                          CreateDate = x.CreateDate,
    //                                                          ModifyDate = x.ModifyDate,
    //                                                          ModifyById = x.ModifyById,
    //                                                          StatusId = x.StatusId,
    //                                                          BinNumberId = x.BinNumberId,
    //                                                          Comment = x.Comment,
    //                                                      })
    //                                                           .Skip(0)
    //                                                           .Take(400)
    //                                                           .ToList();
    //                        #endregion
    //                        if (_Sales.Count() > 0)
    //                        {
    //                            List<TUCSale> _TUCSales = new List<TUCSale>();
    //                            foreach (var SaleItem in _Sales)
    //                            {
    //                                TUCSale _TUCSale = new TUCSale();
    //                                if (!string.IsNullOrEmpty(SaleItem.GroupKey))
    //                                {
    //                                    _TUCSale.Guid = SaleItem.GroupKey;
    //                                }
    //                                else
    //                                {
    //                                    _TUCSale.Guid = SaleItem.Guid;
    //                                }
    //                                _TUCSale.TransactionId = SaleItem.ReferenceId;
    //                                _TUCSale.TransactionDate = SaleItem.TransactionDate;
    //                                _TUCSale.SourceId = SaleItem.SourceId;
    //                                _TUCSale.TypeId = SaleItem.TypeId;
    //                                _TUCSale.InvoiceAmount = SaleItem.InvoiceAmount;
    //                                _TUCSale.BinNumberId = SaleItem.BinNumberId;
    //                                _TUCSale.CustomerId = SaleItem.CustomerId;
    //                                _TUCSale.MerchantId = SaleItem.MerchantId;
    //                                _TUCSale.StoreId = SaleItem.StoreId;
    //                                _TUCSale.CashierId = SaleItem.CashierId;
    //                                _TUCSale.ReferenceNumber = SaleItem.ReferenceNumber;
    //                                _TUCSale.InvoiceNumber = SaleItem.InvoiceNumber;
    //                                _TUCSale.CreateDate = SaleItem.CreateDate;
    //                                _TUCSale.CreatedById = SaleItem.CreatedById;
    //                                _TUCSale.ModifyDate = SaleItem.ModifyDate;
    //                                _TUCSale.ModifyById = SaleItem.ModifyById;
    //                                _TUCSale.StatusId = SaleItem.StatusId;
    //                                _TUCSale.StatusMessage = SaleItem.Comment;
    //                                if (SaleItem.TypeId == TransactionType.CardReward)
    //                                {
    //                                    _TUCSale.PaymentModeId = TransactionPaymentMode.Card;
    //                                }
    //                                else if (SaleItem.TypeId == TransactionType.CashReward)
    //                                {
    //                                    _TUCSale.PaymentModeId = TransactionPaymentMode.Cash;
    //                                }
    //                                else if (SaleItem.TypeId == TransactionType.Loyalty.TUCRedeem.TUCPay)
    //                                {
    //                                    _TUCSale.PaymentModeId = TransactionPaymentMode.TUCWallet;
    //                                }
    //                                else
    //                                {
    //                                    _TUCSale.PaymentModeId = SaleItem.TypeId;
    //                                }
    //                                _TUCSales.Add(_TUCSale);
    //                            }
    //                            _HCoreContext.TUCSale.AddRange(_TUCSales);
    //                            _HCoreContext.SaveChanges();
    //                        }
    //                        else
    //                        {
    //                            _HCoreContext.Dispose();
    //                        }
    //                    }
    //                    IsSaleProcessing = false;
    //                }
    //            }
    //            catch (Exception _Exception)
    //            {
    //                IsSaleProcessing = false;
    //                HCoreHelper.LogException("TUCCORE-ActorProcessSale", _Exception);
    //            }
    //        });
    //    }
    //}
    //internal class ActorProcessLoyalty : ReceiveActor
    //{
    //    private bool IsLoyaltyProcessing = false;
    //    public class OOtem
    //    {
    //        public long Id { get; set; }
    //        public long ModeId { get; set; }
    //        public long TypeId { get; set; }
    //        public long SourceId { get; set; }
    //        public long? CampaignId { get; set; }
    //        public DateTime CreateDate { get; set; }
    //        public long? CreatedById { get; set; }
    //        public DateTime? ModifyDate { get; set; }
    //        public long? ModifyById { get; set; }
    //        public double? ReferenceAmount { get; set; }
    //        public DateTime TransactionDate { get; set; }
    //        public long? ParentId { get; set; }
    //        public long? UserAccountId { get; set; }
    //        public double? TotalAmount { get; set; }
    //        public double? InvoiceAmount { get; set; }
    //        public double? LoyaltyInvoiceAmount { get; set; }
    //        public long? SubParentId { get; set; }
    //        public long? CashierId { get; set; }
    //        public long? BankId { get; set; }
    //        public long? ProviderId { get; set; }
    //        public string? InoviceNumber { get; set; }
    //        public string? AccountNumber { get; set; }
    //        public string? ReferenceNumber { get; set; }
    //        public int StatusId { get; set; }
    //        public double? Amount { get; set; }
    //        public double? ComissionAmount { get; set; }
    //        //public double? ParentTransactionTotalAmount { get; set; }
    //    }
    //    public ActorProcessLoyalty()
    //    {
    //        Receive<string>(_Request =>
    //        {
    //            try
    //            {
    //                if (!IsLoyaltyProcessing)
    //                {
    //                    try
    //                    {
    //                        IsLoyaltyProcessing = true;
    //                        //Reward
    //                        using (HCoreContext _HCoreContext = new HCoreContext())
    //                        {
    //                            var RewardTransactions = _HCoreContext.HCUAccountTransaction
    //                                                    .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
    //                                                    && x.ModeId == TransactionMode.Credit
    //                                                    && x.TUCLoyalty.Any() == false
    //                                                    && x.ParentTransaction.TotalAmount > 0
    //                                                    && x.CampaignId == null
    //                                                    && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC) || x.SourceId == TransactionSource.ThankUCashPlus))
    //                                                        .OrderBy(x => x.TransactionDate)
    //                                                    .Select(x => new OOtem
    //                                                    {
    //                                                        Id = x.Id,
    //                                                        TransactionDate = x.TransactionDate,
    //                                                        ModeId = x.ModeId,
    //                                                        TypeId = x.TypeId,
    //                                                        SourceId = x.SourceId,
    //                                                        Amount = x.ParentTransaction.Amount,
    //                                                        ComissionAmount = x.ParentTransaction.ComissionAmount,
    //                                                        TotalAmount = x.ParentTransaction.TotalAmount,
    //                                                        InvoiceAmount = x.PurchaseAmount,
    //                                                        LoyaltyInvoiceAmount = x.ReferenceInvoiceAmount,
    //                                                        ReferenceAmount = x.ReferenceAmount,
    //                                                        UserAccountId = x.AccountId,
    //                                                        ParentId = x.ParentId,
    //                                                        SubParentId = x.SubParentId,
    //                                                        CashierId = x.CashierId,
    //                                                        ProviderId = x.ProviderId,
    //                                                        BankId = x.BankId,
    //                                                        InoviceNumber = x.InoviceNumber,
    //                                                        AccountNumber = x.AccountNumber,
    //                                                        ReferenceNumber = x.ReferenceNumber,
    //                                                        CreateDate = x.CreateDate,
    //                                                        CreatedById = x.CreatedById,
    //                                                        ModifyDate = x.ModifyDate,
    //                                                        ModifyById = x.ModifyById,
    //                                                        StatusId = x.StatusId,
    //                                                    }).Skip(0)
    //                                                    .Take(400)
    //                                                    .ToList();
    //                            if (RewardTransactions.Count > 0)
    //                            {
    //                                List<TUCLoyalty> _LoyaltyTransactions = new List<TUCLoyalty>();
    //                                foreach (var Transaction in RewardTransactions)
    //                                {
    //                                    if (Transaction.ReferenceAmount == null)
    //                                    {
    //                                        Transaction.ReferenceAmount = 0;
    //                                    }
    //                                    if (Transaction.TotalAmount == null)
    //                                    {
    //                                        Transaction.TotalAmount = 0;
    //                                    }
    //                                    if (Transaction.InvoiceAmount == null)
    //                                    {
    //                                        Transaction.InvoiceAmount = 0;
    //                                    }
    //                                    if (Transaction.LoyaltyInvoiceAmount == null)
    //                                    {
    //                                        Transaction.LoyaltyInvoiceAmount = 0;
    //                                    }
    //                                    if (Transaction.Amount == null)
    //                                    {
    //                                        Transaction.Amount = 0;
    //                                    }
    //                                    if (Transaction.ComissionAmount == null)
    //                                    {
    //                                        Transaction.ComissionAmount = 0;
    //                                    }
    //                                    Transaction.ReferenceAmount = Math.Round((double)Transaction.ReferenceAmount, 3);
    //                                    Transaction.TotalAmount = Math.Round((double)Transaction.TotalAmount, 3);
    //                                    Transaction.InvoiceAmount = Math.Round((double)Transaction.InvoiceAmount, 3);
    //                                    Transaction.LoyaltyInvoiceAmount = Math.Round((double)Transaction.LoyaltyInvoiceAmount, 3);
    //                                    Transaction.Amount = Math.Round((double)Transaction.Amount, 3);
    //                                    Transaction.ComissionAmount = Math.Round((double)Transaction.ComissionAmount, 3);
    //                                    _LoyaltyTransactions.Add(new TUCLoyalty
    //                                    {
    //                                        Guid = HCoreHelper.GenerateGuid(),
    //                                        TransactionDate = Transaction.TransactionDate,
    //                                        TransactionId = Transaction.Id,
    //                                        LoyaltyTypeId = Loyalty.Reward,
    //                                        TypeId = Transaction.TypeId,
    //                                        ModeId = Transaction.ModeId,
    //                                        SourceId = Transaction.SourceId,
    //                                        FromAccountId = (long)Transaction.ParentId,
    //                                        ToAccountId = (long)Transaction.UserAccountId,

    //                                        Amount = (double)Transaction.Amount,
    //                                        CommissionAmount = (double)Transaction.ComissionAmount,
    //                                        TotalAmount = (double)Transaction.TotalAmount,
    //                                        InvoiceAmount = (double)Transaction.InvoiceAmount,
    //                                        LoyaltyInvoiceAmount = (double)Transaction.LoyaltyInvoiceAmount,
    //                                        ToAccountBalance = 0,

    //                                        CustomerId = (long)Transaction.UserAccountId,
    //                                        MerchantId = Transaction.ParentId,
    //                                        StoreId = Transaction.SubParentId,
    //                                        CashierId = Transaction.CashierId,
    //                                        ProviderId = Transaction.ProviderId,
    //                                        AcquirerId = Transaction.BankId,
    //                                        InvoiceNumber = Transaction.InoviceNumber,
    //                                        AccountNumber = Transaction.AccountNumber,
    //                                        ReferenceNumber = Transaction.ReferenceNumber,
    //                                        CreateDate = Transaction.CreateDate,
    //                                        CreatedById = Transaction.CreatedById,
    //                                        ModifyDate = Transaction.ModifyDate,
    //                                        ModifyById = Transaction.ModifyById,
    //                                        StatusId = Transaction.StatusId,
    //                                    });
    //                                }
    //                                if (_LoyaltyTransactions.Count() > 0)
    //                                {
    //                                    _LoyaltyTransactions = _LoyaltyTransactions.OrderBy(x => x.CreateDate).ToList();
    //                                    _HCoreContext.TUCLoyalty.AddRange(_LoyaltyTransactions);
    //                                    _HCoreContext.SaveChanges();
    //                                }
    //                                else
    //                                {
    //                                    _HCoreContext.Dispose();
    //                                }

    //                            }
    //                            else
    //                            {
    //                                _HCoreContext.Dispose();
    //                            }
    //                        }
    //                        IsLoyaltyProcessing = false;
    //                    }
    //                    catch (Exception _Exception)
    //                    {
    //                        IsLoyaltyProcessing = false;
    //                        HCoreHelper.LogException("TUCCORE-ActorProcessLoyalty-REWARD", _Exception);
    //                    }





    //                    try
    //                    {
    //                        IsLoyaltyProcessing = true;
    //                        //Reward Campaign
    //                        using (HCoreContext _HCoreContext = new HCoreContext())
    //                        {
    //                            var RewardTransactions = _HCoreContext.HCUAccountTransaction
    //                                                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
    //                                                            && x.ModeId == TransactionMode.Credit
    //                                                            && x.ReferenceAmount > 0
    //                                                            && x.TUCLoyalty.Any() == false
    //                                                            && x.CampaignId != null
    //                                                            && ((x.TypeId != TransactionType.ThankUCashPlusCredit
    //                                                                && x.SourceId == TransactionSource.TUC)
    //                                                            || x.SourceId == TransactionSource.ThankUCashPlus))
    //                                                        .OrderBy(x => x.TransactionDate)
    //                                                      .Select(x => new OOtem
    //                                                      {
    //                                                          Id = x.Id,
    //                                                          TransactionDate = x.TransactionDate,
    //                                                          ModeId = x.ModeId,
    //                                                          TypeId = x.TypeId,
    //                                                          SourceId = x.SourceId,
    //                                                          Amount = x.ParentTransaction.Amount,
    //                                                          ComissionAmount = x.ParentTransaction.ComissionAmount,
    //                                                          TotalAmount = x.ParentTransaction.TotalAmount,
    //                                                          InvoiceAmount = x.PurchaseAmount,
    //                                                          LoyaltyInvoiceAmount = x.ReferenceInvoiceAmount,
    //                                                          ReferenceAmount = x.ReferenceAmount,
    //                                                          UserAccountId = x.AccountId,
    //                                                          ParentId = x.ParentId,
    //                                                          SubParentId = x.SubParentId,
    //                                                          CashierId = x.CashierId,
    //                                                          ProviderId = x.ProviderId,
    //                                                          BankId = x.BankId,
    //                                                          InoviceNumber = x.InoviceNumber,
    //                                                          AccountNumber = x.AccountNumber,
    //                                                          ReferenceNumber = x.ReferenceNumber,
    //                                                          CreateDate = x.CreateDate,
    //                                                          CreatedById = x.CreatedById,
    //                                                          ModifyDate = x.ModifyDate,
    //                                                          ModifyById = x.ModifyById,
    //                                                          StatusId = x.StatusId,
    //                                                      }).Skip(0)
    //                                                        .Take(400)
    //                                                        .ToList();
    //                            if (RewardTransactions.Count > 0)
    //                            {
    //                                List<TUCLoyalty> _LoyaltyTransactions = new List<TUCLoyalty>();
    //                                foreach (var Transaction in RewardTransactions)
    //                                {
    //                                    if (Transaction.ReferenceAmount == null)
    //                                    {
    //                                        Transaction.ReferenceAmount = 0;
    //                                    }
    //                                    if (Transaction.TotalAmount == null)
    //                                    {
    //                                        Transaction.TotalAmount = 0;
    //                                    }
    //                                    if (Transaction.InvoiceAmount == null)
    //                                    {
    //                                        Transaction.InvoiceAmount = 0;
    //                                    }
    //                                    if (Transaction.LoyaltyInvoiceAmount == null)
    //                                    {
    //                                        Transaction.LoyaltyInvoiceAmount = 0;
    //                                    }
    //                                    if (Transaction.Amount == null)
    //                                    {
    //                                        Transaction.Amount = 0;
    //                                    }
    //                                    if (Transaction.ComissionAmount == null)
    //                                    {
    //                                        Transaction.ComissionAmount = 0;
    //                                    }
    //                                    Transaction.ReferenceAmount = Math.Round((double)Transaction.ReferenceAmount, 3);
    //                                    Transaction.TotalAmount = Math.Round((double)Transaction.TotalAmount, 3);
    //                                    Transaction.InvoiceAmount = Math.Round((double)Transaction.InvoiceAmount, 3);
    //                                    Transaction.LoyaltyInvoiceAmount = Math.Round((double)Transaction.LoyaltyInvoiceAmount, 3);
    //                                    Transaction.Amount = Math.Round((double)Transaction.Amount, 3);
    //                                    Transaction.ComissionAmount = Math.Round((double)Transaction.ComissionAmount, 3);
    //                                    _LoyaltyTransactions.Add(new TUCLoyalty
    //                                    {
    //                                        Guid = HCoreHelper.GenerateGuid(),
    //                                        TransactionDate = Transaction.TransactionDate,
    //                                        TransactionId = Transaction.Id,
    //                                        LoyaltyTypeId = Loyalty.Reward,
    //                                        TypeId = Transaction.TypeId,
    //                                        ModeId = Transaction.ModeId,
    //                                        SourceId = Transaction.SourceId,
    //                                        FromAccountId = (long)Transaction.ParentId,
    //                                        ToAccountId = (long)Transaction.UserAccountId,

    //                                        Amount = (double)Transaction.Amount,
    //                                        CommissionAmount = (double)Transaction.ComissionAmount,
    //                                        TotalAmount = (double)Transaction.TotalAmount,
    //                                        InvoiceAmount = (double)Transaction.InvoiceAmount,
    //                                        LoyaltyInvoiceAmount = (double)Transaction.LoyaltyInvoiceAmount,
    //                                        ToAccountBalance = 0,

    //                                        CustomerId = (long)Transaction.UserAccountId,
    //                                        MerchantId = Transaction.ParentId,
    //                                        StoreId = Transaction.SubParentId,
    //                                        CashierId = Transaction.CashierId,
    //                                        ProviderId = Transaction.ProviderId,
    //                                        AcquirerId = Transaction.BankId,
    //                                        InvoiceNumber = Transaction.InoviceNumber,
    //                                        AccountNumber = Transaction.AccountNumber,
    //                                        ReferenceNumber = Transaction.ReferenceNumber,
    //                                        CreateDate = Transaction.CreateDate,
    //                                        CreatedById = Transaction.CreatedById,
    //                                        ModifyDate = Transaction.ModifyDate,
    //                                        ModifyById = Transaction.ModifyById,
    //                                        StatusId = Transaction.StatusId,
    //                                    });
    //                                }
    //                                if (_LoyaltyTransactions.Count() > 0)
    //                                {
    //                                    _LoyaltyTransactions = _LoyaltyTransactions.OrderBy(x => x.CreateDate).ToList();
    //                                    _HCoreContext.TUCLoyalty.AddRange(_LoyaltyTransactions);
    //                                    _HCoreContext.SaveChanges();
    //                                }
    //                                else
    //                                {
    //                                    _HCoreContext.Dispose();
    //                                }
    //                            }
    //                            else
    //                            {
    //                                _HCoreContext.Dispose();
    //                            }
    //                        }
    //                        IsLoyaltyProcessing = false;
    //                    }
    //                    catch (Exception _Exception)
    //                    {
    //                        IsLoyaltyProcessing = false;
    //                        HCoreHelper.LogException("TUCCORE-ActorProcessLoyalty-REWARD-CAMPAIGN", _Exception);
    //                    }
    //                    try
    //                    {
    //                        IsLoyaltyProcessing = true;
    //                        //Reward Claim
    //                        using (HCoreContext _HCoreContext = new HCoreContext())
    //                        {
    //                            var RewardTransactions = _HCoreContext.HCUAccountTransaction
    //                                                    .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
    //                                                    && x.TUCLoyalty.Any() == false
    //                                                    && x.ModeId == TransactionMode.Debit
    //                                                    && x.SourceId == TransactionSource.ThankUCashPlus)
    //                                                        .OrderBy(x => x.TransactionDate)
    //                                                    .Select(x => new OOtem
    //                                                    {
    //                                                        Id = x.Id,
    //                                                        TransactionDate = x.TransactionDate,
    //                                                        ModeId = x.ModeId,
    //                                                        TypeId = x.TypeId,
    //                                                        SourceId = x.SourceId,
    //                                                        Amount = x.Amount,
    //                                                        ComissionAmount = x.ComissionAmount,
    //                                                        TotalAmount = x.TotalAmount,
    //                                                        InvoiceAmount = x.PurchaseAmount,
    //                                                        LoyaltyInvoiceAmount = x.ReferenceInvoiceAmount,
    //                                                        ReferenceAmount = x.ReferenceAmount,
    //                                                        UserAccountId = x.AccountId,
    //                                                        ParentId = x.ParentId,
    //                                                        SubParentId = x.SubParentId,
    //                                                        CashierId = x.CashierId,
    //                                                        ProviderId = x.ProviderId,
    //                                                        BankId = x.BankId,
    //                                                        InoviceNumber = x.InoviceNumber,
    //                                                        AccountNumber = x.AccountNumber,
    //                                                        ReferenceNumber = x.ReferenceNumber,
    //                                                        CreateDate = x.CreateDate,
    //                                                        CreatedById = x.CreatedById,
    //                                                        ModifyDate = x.ModifyDate,
    //                                                        ModifyById = x.ModifyById,
    //                                                        StatusId = x.StatusId,
    //                                                    }).Skip(0)
    //                                                    .Take(400)
    //                                                    .ToList();
    //                            if (RewardTransactions.Count > 0)
    //                            {
    //                                List<TUCLoyalty> _LoyaltyTransactions = new List<TUCLoyalty>();
    //                                foreach (var Transaction in RewardTransactions)
    //                                {
    //                                    if (Transaction.ReferenceAmount == null)
    //                                    {
    //                                        Transaction.ReferenceAmount = 0;
    //                                    }
    //                                    if (Transaction.TotalAmount == null)
    //                                    {
    //                                        Transaction.TotalAmount = 0;
    //                                    }
    //                                    if (Transaction.InvoiceAmount == null)
    //                                    {
    //                                        Transaction.InvoiceAmount = 0;
    //                                    }
    //                                    if (Transaction.LoyaltyInvoiceAmount == null)
    //                                    {
    //                                        Transaction.LoyaltyInvoiceAmount = 0;
    //                                    }
    //                                    if (Transaction.Amount == null)
    //                                    {
    //                                        Transaction.Amount = 0;
    //                                    }
    //                                    if (Transaction.ComissionAmount == null)
    //                                    {
    //                                        Transaction.ComissionAmount = 0;
    //                                    }
    //                                    Transaction.ReferenceAmount = Math.Round((double)Transaction.ReferenceAmount, 3);
    //                                    Transaction.TotalAmount = Math.Round((double)Transaction.TotalAmount, 3);
    //                                    Transaction.InvoiceAmount = Math.Round((double)Transaction.InvoiceAmount, 3);
    //                                    Transaction.LoyaltyInvoiceAmount = Math.Round((double)Transaction.LoyaltyInvoiceAmount, 3);
    //                                    Transaction.Amount = Math.Round((double)Transaction.Amount, 3);
    //                                    Transaction.ComissionAmount = Math.Round((double)Transaction.ComissionAmount, 3);
    //                                    _LoyaltyTransactions.Add(new TUCLoyalty
    //                                    {
    //                                        Guid = HCoreHelper.GenerateGuid(),
    //                                        TransactionDate = Transaction.TransactionDate,
    //                                        TransactionId = Transaction.Id,
    //                                        LoyaltyTypeId = Loyalty.RewardClaim,
    //                                        TypeId = Transaction.TypeId,
    //                                        ModeId = Transaction.ModeId,
    //                                        SourceId = Transaction.SourceId,
    //                                        FromAccountId = (long)Transaction.ParentId,
    //                                        ToAccountId = (long)Transaction.UserAccountId,

    //                                        Amount = (double)Transaction.Amount,
    //                                        CommissionAmount = (double)Transaction.ComissionAmount,
    //                                        TotalAmount = (double)Transaction.TotalAmount,
    //                                        InvoiceAmount = (double)Transaction.InvoiceAmount,
    //                                        LoyaltyInvoiceAmount = (double)Transaction.LoyaltyInvoiceAmount,
    //                                        ToAccountBalance = 0,

    //                                        CustomerId = (long)Transaction.UserAccountId,
    //                                        MerchantId = Transaction.ParentId,
    //                                        StoreId = Transaction.SubParentId,
    //                                        CashierId = Transaction.CashierId,
    //                                        ProviderId = Transaction.ProviderId,
    //                                        AcquirerId = Transaction.BankId,
    //                                        InvoiceNumber = Transaction.InoviceNumber,
    //                                        AccountNumber = Transaction.AccountNumber,
    //                                        ReferenceNumber = Transaction.ReferenceNumber,
    //                                        CreateDate = Transaction.CreateDate,
    //                                        CreatedById = Transaction.CreatedById,
    //                                        ModifyDate = Transaction.ModifyDate,
    //                                        ModifyById = Transaction.ModifyById,
    //                                        StatusId = Transaction.StatusId,
    //                                    });
    //                                }
    //                                if (_LoyaltyTransactions.Count() > 0)
    //                                {
    //                                    _LoyaltyTransactions = _LoyaltyTransactions.OrderBy(x => x.CreateDate).ToList();
    //                                    _HCoreContext.TUCLoyalty.AddRange(_LoyaltyTransactions);
    //                                    _HCoreContext.SaveChanges();
    //                                }
    //                                else
    //                                {
    //                                    _HCoreContext.Dispose();
    //                                }
    //                            }
    //                            else
    //                            {
    //                                _HCoreContext.Dispose();
    //                            }
    //                        }
    //                        IsLoyaltyProcessing = false;
    //                    }
    //                    catch (Exception _Exception)
    //                    {
    //                        IsLoyaltyProcessing = false;
    //                        HCoreHelper.LogException("TUCCORE-ActorProcessLoyalty-REWARD-CLAIM", _Exception);
    //                    }
    //                    try
    //                    {
    //                        IsLoyaltyProcessing = true;
    //                        //Redeem
    //                        using (HCoreContext _HCoreContext = new HCoreContext())
    //                        {
    //                            var RewardTransactions = _HCoreContext.HCUAccountTransaction
    //                                                                  .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
    //                                                                    && x.ModeId == TransactionMode.Debit
    //                                                                    && x.TotalAmount > 0
    //                                                                    && x.TUCLoyalty.Any() == false
    //                                                                    && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints)
    //                                                                    && x.Type.SubParentId == TransactionTypeCategory.Redeem)
    //                                                        .OrderBy(x => x.TransactionDate)
    //                                                        .Select(x => new OOtem
    //                                                        {
    //                                                            Id = x.Id,
    //                                                            TransactionDate = x.TransactionDate,
    //                                                            ModeId = x.ModeId,
    //                                                            TypeId = x.TypeId,
    //                                                            SourceId = x.SourceId,
    //                                                            Amount = x.Amount,
    //                                                            ComissionAmount = x.ComissionAmount,
    //                                                            TotalAmount = x.TotalAmount,
    //                                                            InvoiceAmount = x.PurchaseAmount,
    //                                                            LoyaltyInvoiceAmount = x.ReferenceInvoiceAmount,
    //                                                            ReferenceAmount = x.ReferenceAmount,
    //                                                            UserAccountId = x.AccountId,
    //                                                            ParentId = x.ParentId,
    //                                                            SubParentId = x.SubParentId,
    //                                                            CashierId = x.CashierId,
    //                                                            ProviderId = x.ProviderId,
    //                                                            BankId = x.BankId,
    //                                                            InoviceNumber = x.InoviceNumber,
    //                                                            AccountNumber = x.AccountNumber,
    //                                                            ReferenceNumber = x.ReferenceNumber,
    //                                                            CreateDate = x.CreateDate,
    //                                                            CreatedById = x.CreatedById,
    //                                                            ModifyDate = x.ModifyDate,
    //                                                            ModifyById = x.ModifyById,
    //                                                            StatusId = x.StatusId,
    //                                                        }).Skip(0)
    //                                                        .Take(400)
    //                                                        .ToList();
    //                            if (RewardTransactions.Count > 0)
    //                            {
    //                                List<TUCLoyalty> _LoyaltyTransactions = new List<TUCLoyalty>();
    //                                foreach (var Transaction in RewardTransactions)
    //                                {
    //                                    if (Transaction.ReferenceAmount == null)
    //                                    {
    //                                        Transaction.ReferenceAmount = 0;
    //                                    }
    //                                    if (Transaction.TotalAmount == null)
    //                                    {
    //                                        Transaction.TotalAmount = 0;
    //                                    }
    //                                    if (Transaction.InvoiceAmount == null)
    //                                    {
    //                                        Transaction.InvoiceAmount = 0;
    //                                    }
    //                                    if (Transaction.LoyaltyInvoiceAmount == null)
    //                                    {
    //                                        Transaction.LoyaltyInvoiceAmount = 0;
    //                                    }
    //                                    if (Transaction.Amount == null)
    //                                    {
    //                                        Transaction.Amount = 0;
    //                                    }
    //                                    if (Transaction.ComissionAmount == null)
    //                                    {
    //                                        Transaction.ComissionAmount = 0;
    //                                    }
    //                                    Transaction.ReferenceAmount = Math.Round((double)Transaction.ReferenceAmount, 3);
    //                                    Transaction.TotalAmount = Math.Round((double)Transaction.TotalAmount, 3);
    //                                    Transaction.InvoiceAmount = Math.Round((double)Transaction.InvoiceAmount, 3);
    //                                    Transaction.LoyaltyInvoiceAmount = Math.Round((double)Transaction.LoyaltyInvoiceAmount, 3);
    //                                    Transaction.Amount = Math.Round((double)Transaction.Amount, 3);
    //                                    Transaction.ComissionAmount = Math.Round((double)Transaction.ComissionAmount, 3);
    //                                    _LoyaltyTransactions.Add(new TUCLoyalty
    //                                    {
    //                                        Guid = HCoreHelper.GenerateGuid(),
    //                                        TransactionDate = Transaction.TransactionDate,
    //                                        TransactionId = Transaction.Id,
    //                                        LoyaltyTypeId = Loyalty.Redeem,
    //                                        TypeId = Transaction.TypeId,
    //                                        ModeId = Transaction.ModeId,
    //                                        SourceId = Transaction.SourceId,
    //                                        FromAccountId = (long)Transaction.ParentId,
    //                                        ToAccountId = (long)Transaction.UserAccountId,

    //                                        Amount = (double)Transaction.Amount,
    //                                        CommissionAmount = (double)Transaction.ComissionAmount,
    //                                        TotalAmount = (double)Transaction.TotalAmount,
    //                                        InvoiceAmount = (double)Transaction.InvoiceAmount,
    //                                        LoyaltyInvoiceAmount = (double)Transaction.LoyaltyInvoiceAmount,
    //                                        ToAccountBalance = 0,



    //                                        CustomerId = (long)Transaction.UserAccountId,
    //                                        MerchantId = Transaction.ParentId,
    //                                        StoreId = Transaction.SubParentId,
    //                                        CashierId = Transaction.CashierId,
    //                                        ProviderId = Transaction.ProviderId,
    //                                        AcquirerId = Transaction.BankId,
    //                                        InvoiceNumber = Transaction.InoviceNumber,
    //                                        AccountNumber = Transaction.AccountNumber,
    //                                        ReferenceNumber = Transaction.ReferenceNumber,
    //                                        CreateDate = Transaction.CreateDate,
    //                                        CreatedById = Transaction.CreatedById,
    //                                        ModifyDate = Transaction.ModifyDate,
    //                                        ModifyById = Transaction.ModifyById,
    //                                        StatusId = Transaction.StatusId,
    //                                    });
    //                                }
    //                                if (_LoyaltyTransactions.Count() > 0)
    //                                {
    //                                    _LoyaltyTransactions = _LoyaltyTransactions.OrderBy(x => x.CreateDate).ToList();
    //                                    _HCoreContext.TUCLoyalty.AddRange(_LoyaltyTransactions);
    //                                    _HCoreContext.SaveChanges();
    //                                }
    //                                else
    //                                {
    //                                    _HCoreContext.Dispose();
    //                                }
    //                            }
    //                            else
    //                            {
    //                                _HCoreContext.Dispose();
    //                            }
    //                        }
    //                        IsLoyaltyProcessing = false;
    //                    }
    //                    catch (Exception _Exception)
    //                    {
    //                        IsLoyaltyProcessing = false;
    //                        HCoreHelper.LogException("TUCCORE-ActorProcessLoyalty-REDEEM", _Exception);
    //                    }
    //                }
    //            }
    //            catch (Exception _Exception)
    //            {
    //                IsLoyaltyProcessing = false;
    //                HCoreHelper.LogException("TUCCORE-ActorProcessLoyalty", _Exception);
    //            }
    //        });
    //    }
    //    //public ActorProcessLoyalty()
    //    //{
    //    //    Receive<string>(_Request =>
    //    //    {
    //    //        try
    //    //        {
    //    //            if (!IsLoyaltyProcessing)
    //    //            {
    //    //                IsLoyaltyProcessing = true;
    //    //                using (HCoreContext _HCoreContext = new HCoreContext())
    //    //                {
    //    //                    List<TUCLoyalty> _LoyaltyTransactions = new List<TUCLoyalty>();
    //    //                    var Transactions = _HCoreContext.HCUAccountTransaction
    //    //               .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser && x.TUCLoyalty.Any() == false)
    //    //               .Select(x => new OOtem
    //    //               {
    //    //                   Id = x.Id,
    //    //                   ModeId = x.ModeId,
    //    //                   TypeId = x.TypeId,
    //    //                   SourceId = x.SourceId,
    //    //                   CampaignId = x.CampaignId,
    //    //                   CreateDate = x.CreateDate,
    //    //                   CreatedById = x.CreatedById,
    //    //                   ModifyDate = x.ModifyDate,
    //    //                   ModifyById = x.ModifyById,
    //    //                   ReferenceAmount = x.ReferenceAmount,
    //    //                   TransactionDate = x.TransactionDate,
    //    //                   ParentId = x.ParentId,
    //    //                   UserAccountId = x.AccountId,
    //    //                   TotalAmount = x.TotalAmount,
    //    //                   PurchaseAmount = x.PurchaseAmount,
    //    //                   ReferenceInvoiceAmount = x.ReferenceInvoiceAmount,
    //    //                   SubParentId = x.SubParentId,
    //    //                   CashierId = x.CashierId,
    //    //                   BankId = x.BankId,
    //    //                   ProviderId = x.ProviderId,
    //    //                   InoviceNumber = x.InoviceNumber,
    //    //                   AccountNumber = x.AccountNumber,
    //    //                   ReferenceNumber = x.ReferenceNumber,
    //    //                   StatusId = x.StatusId,
    //    //                   Amount = x.Amount,
    //    //                   ComissionAmount = x.ComissionAmount,
    //    //                   ParentTransactionTotalAmount = x.ParentTransaction.TotalAmount,
    //    //               })
    //    //               .Skip(0)
    //    //               .Take(1000)
    //    //               .ToList();
    //    //                    foreach (var Transaction in Transactions)
    //    //                    {
    //    //                        if (Transaction.ReferenceAmount == null)
    //    //                        {
    //    //                            Transaction.ReferenceAmount = 0;
    //    //                        }
    //    //                        if (Transaction.TotalAmount == null)
    //    //                        {
    //    //                            Transaction.TotalAmount = 0;
    //    //                        }
    //    //                        if (Transaction.PurchaseAmount == null)
    //    //                        {
    //    //                            Transaction.PurchaseAmount = 0;
    //    //                        }
    //    //                        if (Transaction.ReferenceInvoiceAmount == null)
    //    //                        {
    //    //                            Transaction.ReferenceInvoiceAmount = 0;
    //    //                        }
    //    //                        if (Transaction.ParentTransactionTotalAmount == null)
    //    //                        {
    //    //                            Transaction.ParentTransactionTotalAmount = 0;
    //    //                        }
    //    //                        if (Transaction.Amount == null)
    //    //                        {
    //    //                            Transaction.Amount = 0;
    //    //                        }
    //    //                        if (Transaction.ComissionAmount == null)
    //    //                        {
    //    //                            Transaction.ComissionAmount = 0;
    //    //                        }
    //    //                        if (Transaction.ParentTransactionTotalAmount == null)
    //    //                        {
    //    //                            Transaction.ParentTransactionTotalAmount = 0;
    //    //                        }

    //    //                    }
    //    //                    var _RewardTransactions = Transactions
    //    //                                                .OrderBy(x => x.CreateDate)
    //    //                                           .Where(x => x.ModeId == TransactionMode.Credit
    //    //                                           && x.ParentTransactionTotalAmount > 0
    //    //                                           && x.CampaignId == null
    //    //                                           && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC) || x.SourceId == TransactionSource.ThankUCashPlus))
    //    //                                           .Select(x => new TUCLoyalty
    //    //                                           {
    //    //                                               TransactionDate = x.TransactionDate,
    //    //                                               TransactionId = x.Id,
    //    //                                               LoyaltyTypeId = Loyalty.Reward,
    //    //                                               TypeId = x.TypeId,
    //    //                                               ModeId = x.ModeId,
    //    //                                               SourceId = x.SourceId,
    //    //                                               FromAccountId = (long)x.ParentId,
    //    //                                               ToAccountId = (long)x.UserAccountId,
    //    //                                               Amount = (double)x.TotalAmount,
    //    //                                               CommissionAmount = (double)(x.ReferenceAmount - x.TotalAmount),
    //    //                                               TotalAmount = (double)x.ReferenceAmount,
    //    //                                               InvoiceAmount = (double)x.PurchaseAmount,
    //    //                                               LoyaltyInvoiceAmount = (double)x.ReferenceInvoiceAmount,
    //    //                                               Balance = 0,
    //    //                                               CustomerId = (long)x.UserAccountId,
    //    //                                               MerchantId = x.ParentId,
    //    //                                               StoreId = x.SubParentId,
    //    //                                               CashierId = x.CashierId,
    //    //                                               ProviderId = x.ProviderId,
    //    //                                               AcquirerId = x.BankId,
    //    //                                               InvoiceNumber = x.InoviceNumber,
    //    //                                               AccountNumber = x.AccountNumber,
    //    //                                               ReferenceNumber = x.ReferenceNumber,
    //    //                                               CreateDate = x.CreateDate,
    //    //                                               CreatedById = x.CreatedById,
    //    //                                               ModifyDate = x.ModifyDate,
    //    //                                               ModifyById = x.ModifyById,
    //    //                                               StatusId = x.StatusId,
    //    //                                           })
    //    //                                                .OrderBy(x => x.CreateDate)
    //    //                                                .ToList();
    //    //                    foreach (var _RewardTransaction in _RewardTransactions)
    //    //                    {
    //    //                        _RewardTransaction.Guid = HCoreHelper.GenerateGuid();
    //    //                    }
    //    //                    _LoyaltyTransactions.AddRange(_RewardTransactions);
    //    //                    var _RewardClaimTransactions = Transactions
    //    //                                                  .OrderBy(x => x.CreateDate)
    //    //                     .Where(x => x.ModeId == TransactionMode.Debit
    //    //                     && x.SourceId == TransactionSource.ThankUCashPlus)
    //    //                     .Select(x => new TUCLoyalty
    //    //                     {
    //    //                         TransactionDate = x.TransactionDate,
    //    //                         TransactionId = x.Id,
    //    //                         LoyaltyTypeId = Loyalty.RewardClaim,
    //    //                         TypeId = x.TypeId,
    //    //                         ModeId = x.ModeId,
    //    //                         SourceId = x.SourceId,
    //    //                         FromAccountId = (long)x.UserAccountId,
    //    //                         ToAccountId = (long)x.UserAccountId,
    //    //                         Amount = (double)x.Amount,
    //    //                         CommissionAmount = (double)x.ComissionAmount,
    //    //                         TotalAmount = (double)x.TotalAmount,
    //    //                         InvoiceAmount = (double)x.PurchaseAmount,
    //    //                         LoyaltyInvoiceAmount = (double)x.PurchaseAmount,
    //    //                         Balance = 0,
    //    //                         CustomerId = (long)x.UserAccountId,
    //    //                         MerchantId = x.ParentId,
    //    //                         StoreId = x.SubParentId,
    //    //                         CashierId = x.CashierId,
    //    //                         ProviderId = x.ProviderId,
    //    //                         AcquirerId = x.BankId,
    //    //                         InvoiceNumber = x.InoviceNumber,
    //    //                         AccountNumber = x.AccountNumber,
    //    //                         ReferenceNumber = x.ReferenceNumber,
    //    //                         CreateDate = x.CreateDate,
    //    //                         CreatedById = x.CreatedById,
    //    //                         ModifyDate = x.ModifyDate,
    //    //                         ModifyById = x.ModifyById,
    //    //                         StatusId = x.StatusId,
    //    //                     })
    //    //                                                  .OrderBy(x => x.CreateDate)
    //    //                                                  .ToList();
    //    //                    foreach (var _RewardClaimTransaction in _RewardClaimTransactions)
    //    //                    {
    //    //                        _RewardClaimTransaction.Guid = HCoreHelper.GenerateGuid();
    //    //                    }
    //    //                    _LoyaltyTransactions.AddRange(_RewardClaimTransactions);
    //    //                    var _RedeemTransactions = Transactions
    //    //                                                 .OrderBy(x => x.CreateDate)
    //    //                                        .Where(x => x.ModeId == TransactionMode.Debit
    //    //                                         && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints))
    //    //                                        .Select(x => new TUCLoyalty
    //    //                                        {
    //    //                                            TransactionDate = x.TransactionDate,
    //    //                                            TransactionId = x.Id,
    //    //                                            LoyaltyTypeId = Loyalty.Redeem,
    //    //                                            TypeId = x.TypeId,
    //    //                                            ModeId = x.ModeId,
    //    //                                            SourceId = x.SourceId,
    //    //                                            FromAccountId = (long)x.UserAccountId,
    //    //                                            ToAccountId = (long)x.ParentId,
    //    //                                            Amount = (double)x.Amount,
    //    //                                            CommissionAmount = (double)x.ComissionAmount,
    //    //                                            TotalAmount = (double)x.TotalAmount,
    //    //                                            InvoiceAmount = (double)x.PurchaseAmount,
    //    //                                            LoyaltyInvoiceAmount = (double)x.PurchaseAmount,
    //    //                                            Balance = 0,
    //    //                                            CustomerId = (long)x.UserAccountId,
    //    //                                            MerchantId = x.ParentId,
    //    //                                            StoreId = x.SubParentId,
    //    //                                            CashierId = x.CashierId,
    //    //                                            ProviderId = x.ProviderId,
    //    //                                            AcquirerId = x.BankId,
    //    //                                            InvoiceNumber = x.InoviceNumber,
    //    //                                            AccountNumber = x.AccountNumber,
    //    //                                            ReferenceNumber = x.ReferenceNumber,
    //    //                                            CreateDate = x.CreateDate,
    //    //                                            CreatedById = x.CreatedById,
    //    //                                            ModifyDate = x.ModifyDate,
    //    //                                            ModifyById = x.ModifyById,

    //    //                                            StatusId = x.StatusId,
    //    //                                        })
    //    //                                                 .OrderBy(x => x.CreateDate)
    //    //                                        .ToList();
    //    //                    foreach (var _RedeemTransaction in _RedeemTransactions)
    //    //                    {
    //    //                        _RedeemTransaction.Guid = HCoreHelper.GenerateGuid();
    //    //                    }
    //    //                    _LoyaltyTransactions.AddRange(_RedeemTransactions);


    //    //                    var _CampaignTransactions = Transactions
    //    //                                                   .OrderBy(x => x.CreateDate)
    //    //                                         .Where(x => x.ModeId == TransactionMode.Credit
    //    //                                         && x.CampaignId != null
    //    //                                         && x.ReferenceAmount > 0
    //    //                                         && ((x.TypeId != TransactionType.ThankUCashPlusCredit
    //    //                                             && x.SourceId == TransactionSource.TUC)
    //    //                                         || x.SourceId == TransactionSource.ThankUCashPlus))
    //    //                                         .Select(x => new TUCLoyalty
    //    //                                         {

    //    //                                             TransactionDate = x.TransactionDate,
    //    //                                             TransactionId = x.Id,
    //    //                                             LoyaltyTypeId = Loyalty.Reward,
    //    //                                             TypeId = x.TypeId,
    //    //                                             ModeId = x.ModeId,
    //    //                                             SourceId = x.SourceId,
    //    //                                             FromAccountId = (long)x.ParentId,
    //    //                                             ToAccountId = (long)x.UserAccountId,
    //    //                                             Amount = (double)x.TotalAmount,
    //    //                                             CommissionAmount = (double)(x.ReferenceAmount - x.TotalAmount),
    //    //                                             TotalAmount = (double)x.ReferenceAmount,
    //    //                                             InvoiceAmount = (double)x.PurchaseAmount,
    //    //                                             LoyaltyInvoiceAmount = (double)x.ReferenceInvoiceAmount,
    //    //                                             Balance = 0,

    //    //                                             CustomerId = (long)x.UserAccountId,
    //    //                                             MerchantId = x.ParentId,
    //    //                                             StoreId = x.SubParentId,
    //    //                                             CashierId = x.CashierId,
    //    //                                             ProviderId = x.ProviderId,
    //    //                                             AcquirerId = x.BankId,

    //    //                                             InvoiceNumber = x.InoviceNumber,
    //    //                                             AccountNumber = x.AccountNumber,
    //    //                                             ReferenceNumber = x.ReferenceNumber,

    //    //                                             CreateDate = x.CreateDate,
    //    //                                             CreatedById = x.CreatedById,
    //    //                                             ModifyDate = x.ModifyDate,
    //    //                                             ModifyById = x.ModifyById,

    //    //                                             StatusId = x.StatusId,
    //    //                                         })
    //    //                                                   .OrderBy(x => x.CreateDate)
    //    //                                          .ToList();
    //    //                    foreach (var _CampaignTransaction in _CampaignTransactions)
    //    //                    {
    //    //                        _CampaignTransaction.Guid = HCoreHelper.GenerateGuid();
    //    //                    }
    //    //                    _LoyaltyTransactions.AddRange(_CampaignTransactions);
    //    //                    if (_LoyaltyTransactions.Count() > 0)
    //    //                    {
    //    //                        _LoyaltyTransactions = _LoyaltyTransactions.OrderBy(x => x.CreateDate).ToList();
    //    //                        _LoyaltyTransactions = _LoyaltyTransactions.OrderBy(x => x.CreateDate).ToList();
    //    //                        _HCoreContext.TUCLoyalty.AddRange(_LoyaltyTransactions);
    //    //                        _HCoreContext.SaveChanges();
    //    //                    }
    //    //                    else
    //    //                    {
    //    //                        _HCoreContext.Dispose();
    //    //                    }
    //    //                    IsLoyaltyProcessing = false;
    //    //                }
    //    //            }
    //    //        }
    //    //        catch (Exception _Exception)
    //    //        {
    //    //            IsLoyaltyProcessing = false;
    //    //            HCoreHelper.LogException("TUCCORE-ActorProcessLoyalty", _Exception);
    //    //        }
    //    //    });
    //    //}
    //}
    //internal class ActorProcessLoyaltyMerchantCustomer : ReceiveActor
    //{
    //    private bool IsLoyaltyProcessing = false;
    //    public class OOtem
    //    {
    //        public long Id { get; set; }
    //        public DateTime? TransactionDate { get; set; }
    //        public long LoyaltyTypeId { get; set; }
    //        public long TypeId { get; set; }
    //        public long SourceId { get; set; }
    //        public long ModeId { get; set; }
    //        public long? MerchantId { get; set; }
    //        public double Amount { get; set; }
    //        public double InvoiceAmount { get; set; }
    //        public long? CustomerId { get; set; }
    //        //public long Id { get; set; }
    //        //public long? CampaignId { get; set; }
    //        //public DateTime CreateDate { get; set; }
    //        //public long? CreatedById { get; set; }
    //        //public DateTime? ModifyDate { get; set; }
    //        //public long? ModifyById { get; set; }
    //        //public double? ReferenceAmount { get; set; }
    //        //public DateTime TransactionDate { get; set; }
    //        //public long? ParentId { get; set; }
    //        //public long? UserAccountId { get; set; }
    //        //public double? TotalAmount { get; set; }
    //        //public double? InvoiceAmount { get; set; }
    //        //public double? LoyaltyInvoiceAmount { get; set; }
    //        //public long? SubParentId { get; set; }
    //        //public long? CashierId { get; set; }
    //        //public long? BankId { get; set; }
    //        //public long? ProviderId { get; set; }
    //        //public string? InoviceNumber { get; set; }
    //        //public string? AccountNumber { get; set; }
    //        //public string? ReferenceNumber { get; set; }
    //        //public int StatusId { get; set; }
    //        //public double? Amount { get; set; }
    //        //public double? ComissionAmount { get; set; }
    //        //public double? ParentTransactionTotalAmount { get; set; }
    //    }
    //    public ActorProcessLoyaltyMerchantCustomer()
    //    {
    //        Receive<string>(_Request =>
    //        {
    //            try
    //            {
    //                if (!IsLoyaltyProcessing)
    //                {
    //                    try
    //                    {
    //                        IsLoyaltyProcessing = true;
    //                        using (HCoreContext _HCoreContext = new HCoreContext())
    //                        {
    //                            DateTime LastTransactionDate = new DateTime(2010, 1, 1, 0, 0, 0);
    //                            var Ltr = _HCoreContext.TUCLoyaltyMerchantCustomer.OrderByDescending(a => a.ModifyDate).Select(x => x.ModifyDate).FirstOrDefault();
    //                            if (Ltr != null)
    //                            {
    //                                if (Ltr.Value.Year > 2010)
    //                                {
    //                                    LastTransactionDate = Ltr.Value;
    //                                }
    //                            }
    //                            var LoyaltyTransactions = _HCoreContext.TUCLoyalty
    //                                                    .Where(x =>
    //                                                    x.StatusId == HelperStatus.Transaction.Success
    //                                                    && x.SyncDate > LastTransactionDate)
    //                                                    .OrderBy(x => x.TransactionDate)
    //                                                    .Select(x => new OOtem
    //                                                    {
    //                                                        Id = x.Id,
    //                                                        TransactionDate = x.TransactionDate,
    //                                                        ModeId = x.ModeId,
    //                                                        SourceId = x.SourceId,
    //                                                        Amount = x.Amount,
    //                                                        InvoiceAmount = x.InvoiceAmount,
    //                                                        MerchantId = x.MerchantId,
    //                                                        CustomerId = x.CustomerId,
    //                                                    }).Skip(0)
    //                                                    .Take(1000)
    //                                                    .ToList();
    //                            if (LoyaltyTransactions.Count > 0)
    //                            {
    //                                List<TUCLoyaltyMerchantCustomer> _LoyaltyTransactions = new List<TUCLoyaltyMerchantCustomer>();
    //                                foreach (var Transaction in LoyaltyTransactions)
    //                                {
    //                                    var TransactionCheck = _HCoreContext.TUCLoyaltyMerchantCustomer.Where(x => x.CustomerId == Transaction.CustomerId
    //                                    && x.MerchantId == Transaction.MerchantId
    //                                    && x.SourceId == Transaction.SourceId)
    //                                    .FirstOrDefault();
    //                                    if (TransactionCheck != null)
    //                                    {
    //                                        if (Transaction.ModeId == TransactionMode.Credit)
    //                                        {
    //                                            TransactionCheck.Credit = Transaction.Amount;
    //                                            TransactionCheck.Balance = TransactionCheck.Balance + Transaction.Amount;
    //                                        }
    //                                        else
    //                                        {
    //                                            TransactionCheck.Debit = Transaction.Amount;
    //                                            TransactionCheck.Balance = TransactionCheck.Balance - Transaction.Amount;
    //                                        }
    //                                        TransactionCheck.Transactions = TransactionCheck.Transactions + 1;
    //                                        TransactionCheck.InvoiceAmount = TransactionCheck.InvoiceAmount + Transaction.InvoiceAmount;
    //                                        TransactionCheck.LastTransactionDate = Transaction.TransactionDate;
    //                                    }
    //                                    else
    //                                    {
    //                                        TUCLoyaltyMerchantCustomer _TUCLoyaltyMerchantCustomer = new TUCLoyaltyMerchantCustomer();
    //                                        _TUCLoyaltyMerchantCustomer.Guid = HCoreHelper.GenerateGuid();
    //                                        _TUCLoyaltyMerchantCustomer.MerchantId = (long)Transaction.MerchantId;
    //                                        _TUCLoyaltyMerchantCustomer.CustomerId = Transaction.CustomerId;
    //                                        _TUCLoyaltyMerchantCustomer.SourceId = Transaction.SourceId;
    //                                        if (Transaction.ModeId == TransactionMode.Credit)
    //                                        {
    //                                            _TUCLoyaltyMerchantCustomer.Credit = Transaction.Amount;
    //                                            _TUCLoyaltyMerchantCustomer.Balance = Transaction.Amount;
    //                                        }
    //                                        else
    //                                        {
    //                                            _TUCLoyaltyMerchantCustomer.Debit = Transaction.Amount;
    //                                            _TUCLoyaltyMerchantCustomer.Balance = 0;
    //                                        }
    //                                        _TUCLoyaltyMerchantCustomer.InvoiceAmount = Transaction.InvoiceAmount;
    //                                        _TUCLoyaltyMerchantCustomer.Transactions = 1;
    //                                        _TUCLoyaltyMerchantCustomer.LastTransactionDate = Transaction.TransactionDate;
    //                                        _TUCLoyaltyMerchantCustomer.CreateDate = HCoreHelper.GetGMTDateTime();
    //                                        _LoyaltyTransactions.Add(_TUCLoyaltyMerchantCustomer);
    //                                    }
    //                                }
    //                                if (_LoyaltyTransactions.Count() > 0)
    //                                {
    //                                    _LoyaltyTransactions = _LoyaltyTransactions.OrderBy(x => x.CreateDate).ToList();
    //                                    _HCoreContext.TUCLoyaltyMerchantCustomer.AddRange(_LoyaltyTransactions);
    //                                }
    //                                _HCoreContext.SaveChanges();
    //                            }
    //                        }
    //                        IsLoyaltyProcessing = false;
    //                    }
    //                    catch (Exception _Exception)
    //                    {
    //                        IsLoyaltyProcessing = false;
    //                        HCoreHelper.LogException("TUCCORE-ActorProcessLoyaltyMerchantCustomer", _Exception);
    //                    }
    //                }
    //            }
    //            catch (Exception _Exception)
    //            {
    //                IsLoyaltyProcessing = false;
    //                HCoreHelper.LogException("TUCCORE-ActorProcessLoyaltyMerchantCustomer", _Exception);
    //            }
    //        });
    //    }
    //    //public ActorProcessLoyalty()
    //    //{
    //    //    Receive<string>(_Request =>
    //    //    {
    //    //        try
    //    //        {
    //    //            if (!IsLoyaltyProcessing)
    //    //            {
    //    //                IsLoyaltyProcessing = true;
    //    //                using (HCoreContext _HCoreContext = new HCoreContext())
    //    //                {
    //    //                    List<TUCLoyalty> _LoyaltyTransactions = new List<TUCLoyalty>();
    //    //                    var Transactions = _HCoreContext.HCUAccountTransaction
    //    //               .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser && x.TUCLoyalty.Any() == false)
    //    //               .Select(x => new OOtem
    //    //               {
    //    //                   Id = x.Id,
    //    //                   ModeId = x.ModeId,
    //    //                   TypeId = x.TypeId,
    //    //                   SourceId = x.SourceId,
    //    //                   CampaignId = x.CampaignId,
    //    //                   CreateDate = x.CreateDate,
    //    //                   CreatedById = x.CreatedById,
    //    //                   ModifyDate = x.ModifyDate,
    //    //                   ModifyById = x.ModifyById,
    //    //                   ReferenceAmount = x.ReferenceAmount,
    //    //                   TransactionDate = x.TransactionDate,
    //    //                   ParentId = x.ParentId,
    //    //                   UserAccountId = x.AccountId,
    //    //                   TotalAmount = x.TotalAmount,
    //    //                   PurchaseAmount = x.PurchaseAmount,
    //    //                   ReferenceInvoiceAmount = x.ReferenceInvoiceAmount,
    //    //                   SubParentId = x.SubParentId,
    //    //                   CashierId = x.CashierId,
    //    //                   BankId = x.BankId,
    //    //                   ProviderId = x.ProviderId,
    //    //                   InoviceNumber = x.InoviceNumber,
    //    //                   AccountNumber = x.AccountNumber,
    //    //                   ReferenceNumber = x.ReferenceNumber,
    //    //                   StatusId = x.StatusId,
    //    //                   Amount = x.Amount,
    //    //                   ComissionAmount = x.ComissionAmount,
    //    //                   ParentTransactionTotalAmount = x.ParentTransaction.TotalAmount,
    //    //               })
    //    //               .Skip(0)
    //    //               .Take(1000)
    //    //               .ToList();
    //    //                    foreach (var Transaction in Transactions)
    //    //                    {
    //    //                        if (Transaction.ReferenceAmount == null)
    //    //                        {
    //    //                            Transaction.ReferenceAmount = 0;
    //    //                        }
    //    //                        if (Transaction.TotalAmount == null)
    //    //                        {
    //    //                            Transaction.TotalAmount = 0;
    //    //                        }
    //    //                        if (Transaction.PurchaseAmount == null)
    //    //                        {
    //    //                            Transaction.PurchaseAmount = 0;
    //    //                        }
    //    //                        if (Transaction.ReferenceInvoiceAmount == null)
    //    //                        {
    //    //                            Transaction.ReferenceInvoiceAmount = 0;
    //    //                        }
    //    //                        if (Transaction.ParentTransactionTotalAmount == null)
    //    //                        {
    //    //                            Transaction.ParentTransactionTotalAmount = 0;
    //    //                        }
    //    //                        if (Transaction.Amount == null)
    //    //                        {
    //    //                            Transaction.Amount = 0;
    //    //                        }
    //    //                        if (Transaction.ComissionAmount == null)
    //    //                        {
    //    //                            Transaction.ComissionAmount = 0;
    //    //                        }
    //    //                        if (Transaction.ParentTransactionTotalAmount == null)
    //    //                        {
    //    //                            Transaction.ParentTransactionTotalAmount = 0;
    //    //                        }

    //    //                    }
    //    //                    var _RewardTransactions = Transactions
    //    //                                                .OrderBy(x => x.CreateDate)
    //    //                                           .Where(x => x.ModeId == TransactionMode.Credit
    //    //                                           && x.ParentTransactionTotalAmount > 0
    //    //                                           && x.CampaignId == null
    //    //                                           && ((x.TypeId != TransactionType.ThankUCashPlusCredit && x.SourceId == TransactionSource.TUC) || x.SourceId == TransactionSource.ThankUCashPlus))
    //    //                                           .Select(x => new TUCLoyalty
    //    //                                           {
    //    //                                               TransactionDate = x.TransactionDate,
    //    //                                               TransactionId = x.Id,
    //    //                                               LoyaltyTypeId = Loyalty.Reward,
    //    //                                               TypeId = x.TypeId,
    //    //                                               ModeId = x.ModeId,
    //    //                                               SourceId = x.SourceId,
    //    //                                               FromAccountId = (long)x.ParentId,
    //    //                                               ToAccountId = (long)x.UserAccountId,
    //    //                                               Amount = (double)x.TotalAmount,
    //    //                                               CommissionAmount = (double)(x.ReferenceAmount - x.TotalAmount),
    //    //                                               TotalAmount = (double)x.ReferenceAmount,
    //    //                                               InvoiceAmount = (double)x.PurchaseAmount,
    //    //                                               LoyaltyInvoiceAmount = (double)x.ReferenceInvoiceAmount,
    //    //                                               Balance = 0,
    //    //                                               CustomerId = (long)x.UserAccountId,
    //    //                                               MerchantId = x.ParentId,
    //    //                                               StoreId = x.SubParentId,
    //    //                                               CashierId = x.CashierId,
    //    //                                               ProviderId = x.ProviderId,
    //    //                                               AcquirerId = x.BankId,
    //    //                                               InvoiceNumber = x.InoviceNumber,
    //    //                                               AccountNumber = x.AccountNumber,
    //    //                                               ReferenceNumber = x.ReferenceNumber,
    //    //                                               CreateDate = x.CreateDate,
    //    //                                               CreatedById = x.CreatedById,
    //    //                                               ModifyDate = x.ModifyDate,
    //    //                                               ModifyById = x.ModifyById,
    //    //                                               StatusId = x.StatusId,
    //    //                                           })
    //    //                                                .OrderBy(x => x.CreateDate)
    //    //                                                .ToList();
    //    //                    foreach (var _RewardTransaction in _RewardTransactions)
    //    //                    {
    //    //                        _RewardTransaction.Guid = HCoreHelper.GenerateGuid();
    //    //                    }
    //    //                    _LoyaltyTransactions.AddRange(_RewardTransactions);
    //    //                    var _RewardClaimTransactions = Transactions
    //    //                                                  .OrderBy(x => x.CreateDate)
    //    //                     .Where(x => x.ModeId == TransactionMode.Debit
    //    //                     && x.SourceId == TransactionSource.ThankUCashPlus)
    //    //                     .Select(x => new TUCLoyalty
    //    //                     {
    //    //                         TransactionDate = x.TransactionDate,
    //    //                         TransactionId = x.Id,
    //    //                         LoyaltyTypeId = Loyalty.RewardClaim,
    //    //                         TypeId = x.TypeId,
    //    //                         ModeId = x.ModeId,
    //    //                         SourceId = x.SourceId,
    //    //                         FromAccountId = (long)x.UserAccountId,
    //    //                         ToAccountId = (long)x.UserAccountId,
    //    //                         Amount = (double)x.Amount,
    //    //                         CommissionAmount = (double)x.ComissionAmount,
    //    //                         TotalAmount = (double)x.TotalAmount,
    //    //                         InvoiceAmount = (double)x.PurchaseAmount,
    //    //                         LoyaltyInvoiceAmount = (double)x.PurchaseAmount,
    //    //                         Balance = 0,
    //    //                         CustomerId = (long)x.UserAccountId,
    //    //                         MerchantId = x.ParentId,
    //    //                         StoreId = x.SubParentId,
    //    //                         CashierId = x.CashierId,
    //    //                         ProviderId = x.ProviderId,
    //    //                         AcquirerId = x.BankId,
    //    //                         InvoiceNumber = x.InoviceNumber,
    //    //                         AccountNumber = x.AccountNumber,
    //    //                         ReferenceNumber = x.ReferenceNumber,
    //    //                         CreateDate = x.CreateDate,
    //    //                         CreatedById = x.CreatedById,
    //    //                         ModifyDate = x.ModifyDate,
    //    //                         ModifyById = x.ModifyById,
    //    //                         StatusId = x.StatusId,
    //    //                     })
    //    //                                                  .OrderBy(x => x.CreateDate)
    //    //                                                  .ToList();
    //    //                    foreach (var _RewardClaimTransaction in _RewardClaimTransactions)
    //    //                    {
    //    //                        _RewardClaimTransaction.Guid = HCoreHelper.GenerateGuid();
    //    //                    }
    //    //                    _LoyaltyTransactions.AddRange(_RewardClaimTransactions);
    //    //                    var _RedeemTransactions = Transactions
    //    //                                                 .OrderBy(x => x.CreateDate)
    //    //                                        .Where(x => x.ModeId == TransactionMode.Debit
    //    //                                         && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.GiftPoints))
    //    //                                        .Select(x => new TUCLoyalty
    //    //                                        {
    //    //                                            TransactionDate = x.TransactionDate,
    //    //                                            TransactionId = x.Id,
    //    //                                            LoyaltyTypeId = Loyalty.Redeem,
    //    //                                            TypeId = x.TypeId,
    //    //                                            ModeId = x.ModeId,
    //    //                                            SourceId = x.SourceId,
    //    //                                            FromAccountId = (long)x.UserAccountId,
    //    //                                            ToAccountId = (long)x.ParentId,
    //    //                                            Amount = (double)x.Amount,
    //    //                                            CommissionAmount = (double)x.ComissionAmount,
    //    //                                            TotalAmount = (double)x.TotalAmount,
    //    //                                            InvoiceAmount = (double)x.PurchaseAmount,
    //    //                                            LoyaltyInvoiceAmount = (double)x.PurchaseAmount,
    //    //                                            Balance = 0,
    //    //                                            CustomerId = (long)x.UserAccountId,
    //    //                                            MerchantId = x.ParentId,
    //    //                                            StoreId = x.SubParentId,
    //    //                                            CashierId = x.CashierId,
    //    //                                            ProviderId = x.ProviderId,
    //    //                                            AcquirerId = x.BankId,
    //    //                                            InvoiceNumber = x.InoviceNumber,
    //    //                                            AccountNumber = x.AccountNumber,
    //    //                                            ReferenceNumber = x.ReferenceNumber,
    //    //                                            CreateDate = x.CreateDate,
    //    //                                            CreatedById = x.CreatedById,
    //    //                                            ModifyDate = x.ModifyDate,
    //    //                                            ModifyById = x.ModifyById,

    //    //                                            StatusId = x.StatusId,
    //    //                                        })
    //    //                                                 .OrderBy(x => x.CreateDate)
    //    //                                        .ToList();
    //    //                    foreach (var _RedeemTransaction in _RedeemTransactions)
    //    //                    {
    //    //                        _RedeemTransaction.Guid = HCoreHelper.GenerateGuid();
    //    //                    }
    //    //                    _LoyaltyTransactions.AddRange(_RedeemTransactions);


    //    //                    var _CampaignTransactions = Transactions
    //    //                                                   .OrderBy(x => x.CreateDate)
    //    //                                         .Where(x => x.ModeId == TransactionMode.Credit
    //    //                                         && x.CampaignId != null
    //    //                                         && x.ReferenceAmount > 0
    //    //                                         && ((x.TypeId != TransactionType.ThankUCashPlusCredit
    //    //                                             && x.SourceId == TransactionSource.TUC)
    //    //                                         || x.SourceId == TransactionSource.ThankUCashPlus))
    //    //                                         .Select(x => new TUCLoyalty
    //    //                                         {

    //    //                                             TransactionDate = x.TransactionDate,
    //    //                                             TransactionId = x.Id,
    //    //                                             LoyaltyTypeId = Loyalty.Reward,
    //    //                                             TypeId = x.TypeId,
    //    //                                             ModeId = x.ModeId,
    //    //                                             SourceId = x.SourceId,
    //    //                                             FromAccountId = (long)x.ParentId,
    //    //                                             ToAccountId = (long)x.UserAccountId,
    //    //                                             Amount = (double)x.TotalAmount,
    //    //                                             CommissionAmount = (double)(x.ReferenceAmount - x.TotalAmount),
    //    //                                             TotalAmount = (double)x.ReferenceAmount,
    //    //                                             InvoiceAmount = (double)x.PurchaseAmount,
    //    //                                             LoyaltyInvoiceAmount = (double)x.ReferenceInvoiceAmount,
    //    //                                             Balance = 0,
    //    //                                             CustomerId = (long)x.UserAccountId,
    //    //                                             MerchantId = x.ParentId,
    //    //                                             StoreId = x.SubParentId,
    //    //                                             CashierId = x.CashierId,
    //    //                                             ProviderId = x.ProviderId,
    //    //                                             AcquirerId = x.BankId,

    //    //                                             InvoiceNumber = x.InoviceNumber,
    //    //                                             AccountNumber = x.AccountNumber,
    //    //                                             ReferenceNumber = x.ReferenceNumber,

    //    //                                             CreateDate = x.CreateDate,
    //    //                                             CreatedById = x.CreatedById,
    //    //                                             ModifyDate = x.ModifyDate,
    //    //                                             ModifyById = x.ModifyById,

    //    //                                             StatusId = x.StatusId,
    //    //                                         })
    //    //                                                   .OrderBy(x => x.CreateDate)
    //    //                                          .ToList();
    //    //                    foreach (var _CampaignTransaction in _CampaignTransactions)
    //    //                    {
    //    //                        _CampaignTransaction.Guid = HCoreHelper.GenerateGuid();
    //    //                    }
    //    //                    _LoyaltyTransactions.AddRange(_CampaignTransactions);
    //    //                    if (_LoyaltyTransactions.Count() > 0)
    //    //                    {
    //    //                        _LoyaltyTransactions = _LoyaltyTransactions.OrderBy(x => x.CreateDate).ToList();
    //    //                        _LoyaltyTransactions = _LoyaltyTransactions.OrderBy(x => x.CreateDate).ToList();
    //    //                        _HCoreContext.TUCLoyalty.AddRange(_LoyaltyTransactions);
    //    //                        _HCoreContext.SaveChanges();
    //    //                    }
    //    //                    else
    //    //                    {
    //    //                        _HCoreContext.Dispose();
    //    //                    }
    //    //                    IsLoyaltyProcessing = false;
    //    //                }
    //    //            }
    //    //        }
    //    //        catch (Exception _Exception)
    //    //        {
    //    //            IsLoyaltyProcessing = false;
    //    //            HCoreHelper.LogException("TUCCORE-ActorProcessLoyalty", _Exception);
    //    //        }
    //    //    });
    //    //}
    //}

}

