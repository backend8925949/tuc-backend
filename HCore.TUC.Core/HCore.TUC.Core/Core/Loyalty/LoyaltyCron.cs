//==================================================================================
// FileName: LoyaltyCron.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using Akka.Actor;

namespace HCore.TUC.Core.Core.Loyalty
{
    public static class LoyaltyCron
    {
        public static void ExpireInitialisedTransaction()
        {
            var system = ActorSystem.Create("ActorExpireInitialisedTransaction");
            var greeter = system.ActorOf<ActorExpireInitialisedTransaction>("ActorExpireInitialisedTransaction");
            greeter.Tell(true);
        }
        public static void ProcessPendingTransaction()
        {
            var system = ActorSystem.Create("ActorProcessPendingTransaction");
            var greeter = system.ActorOf<ActorProcessPendingTransaction>("ActorProcessPendingTransaction");
            greeter.Tell(true);
        }
    }

    internal class ActorExpireInitialisedTransaction : ReceiveActor
    {
        FrameworkExpireTransaction _FrameworkExpireTransaction;
        public ActorExpireInitialisedTransaction()
        {
            Receive<bool>(_RequestContent =>
            {
                _FrameworkExpireTransaction = new FrameworkExpireTransaction();
                _FrameworkExpireTransaction.ExpireInitialisedTransaction();
            });
        }
    }

    internal class ActorProcessPendingTransaction : ReceiveActor
    {
        FrameworkProcessPendingTransaction _FrameworkProcessPendingTransaction;
        public ActorProcessPendingTransaction()
        {
            Receive<bool>(_RequestContent =>
            {
                _FrameworkProcessPendingTransaction = new FrameworkProcessPendingTransaction();
                _FrameworkProcessPendingTransaction.ProcessPendingTransaction();
            });
        }
    }
}
