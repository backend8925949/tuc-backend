//==================================================================================
// FileName: FrameworkProcessPendingTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using static HCore.CoreConstant.CoreHelperStatus;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.Core.Loyalty
{
    public class FrameworkProcessPendingTransaction
    {
        HCoreContext _HCoreContext;
        ManageCoreTransaction _ManageCoreTransaction;
        public void ProcessPendingTransaction()
        {
            try
            {
                //HCoreHelper.LogData(HCoreConstant.LogType.Log, "ProcessPendingTransaction-START", "Pending transaction started", "");
                //using (_HCoreContext = new HCoreContext())
                //{
                //    //var TransactionsL = _HCoreContext.HCUAccount
                //    //  .Where(x => x.AccountTypeId == UserAccountType.Merchant
                //    //  && x.StatusId  ==  HelperStatus.Default.Active
                //    //  && x.HCUAccountTransactionParent.Any(a=>a.SourceId == TransactionSource.Merchant
                //    //  && a.ModeId == TransactionMode.Debit
                //    //  && a.StatusId == HCoreConstant.HelperStatus.Transaction.Pending))
                //    //  .Select(x => new
                //    //  { 
                //    //      MerchantId = x.Id,
                //    //      TucPlusBalance = (_HCoreContext.HCUAccountTransaction
                //    //                                                                                    .Where(m => m.AccountId == x.Id
                //    //                                                                                    && (m.StatusId == HelperStatus.Transaction.Success || m.StatusId == HelperStatus.Transaction.Pending)
                //    //                                                                                    && m.ModeId == Helpers.TransactionMode.Credit
                //    //                                                                                    && m.SourceId == TransactionSource.Merchant)
                //    //                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0) - (_HCoreContext.HCUAccountTransaction
                //    //                                                                                    .Where(m => m.AccountId == x.Id
                //    //                                                                                    && (m.StatusId == HelperStatus.Transaction.Success || m.StatusId == HelperStatus.Transaction.Pending)
                //    //                                                                                    && m.ModeId == Helpers.TransactionMode.Debit
                //    //                                                                                    && m.SourceId == TransactionSource.Merchant)
                //    //                                                                                    .Sum(m => (double?)m.TotalAmount) ?? 0),
                //    //  })
                //    //  .OrderByDescending(x => x.Id)
                //    //  .ToList();

                //    //var Transactions = _HCoreContext.HCUAccountTransaction
                //    //   .Where(x => x.ModeId == TransactionMode.Debit
                //    //   && x.Account.AccountTypeId == UserAccountType.Merchant
                //    //   && x.SourceId == TransactionSource.Merchant
                //    //   && x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                //    //   )
                //    //   .Select(x => new
                //    //   {
                //    //       Id = x.Id,
                //    //       TotalAmount = x.TotalAmount,
                //    //       GroupKey = x.GroupKey,
                //    //       ParentId = x.ParentId,
                //    //       SourceId = x.SourceId,
                //    //   })
                //    //   .OrderByDescending(x => x.Id)
                //    //   .ToList();


                //    var Transactions = _HCoreContext.HCUAccountTransaction
                //        .Where(x => x.ModeId == TransactionMode.Debit
                //        && x.Account.AccountTypeId == UserAccountType.Merchant
                //        && x.AccountId == 113364
                //        && x.SourceId == TransactionSource.Merchant
                //        && x.StatusId == HCoreConstant.HelperStatus.Transaction.Error
                //        )
                //        .Select(x => new
                //        {
                //            Id = x.Id,
                //            TotalAmount = x.TotalAmount,
                //            GroupKey = x.GroupKey,
                //            ParentId = x.ParentId,
                //            SourceId = x.SourceId,
                //        })
                //        .OrderByDescending(x => x.Id)
                //        .ToList();
                //    //var Transactions = _HCoreContext.HCUAccountTransaction
                //    //    .Where(x => x.ModeId == TransactionMode.Debit
                //    //    && x.Account.AccountTypeId == UserAccountType.Merchant
                //    //    && x.SourceId == TransactionSource.Merchant
                //    //    && x.StatusId == HCoreConstant.HelperStatus.Transaction.Pending
                //    //    && ((_HCoreContext.HCUAccountTransaction
                //    //                         .Where(a => a.AccountId == x.AccountId &&
                //    //                                a.SourceId == x.SourceId &&
                //    //                                a.ModeId == TransactionMode.Credit &&
                //    //                                a.StatusId == Transaction.Success)
                //    //                         .Sum(a => a.TotalAmount) - _HCoreContext.HCUAccountTransaction
                //    //                         .Where(m => m.AccountId == x.AccountId &&
                //    //                                m.SourceId == x.SourceId &&
                //    //                                m.ModeId == TransactionMode.Credit &&
                //    //                                m.StatusId == Transaction.Success)
                //    //                         .Sum(m =>  m.TotalAmount)) > x.TotalAmount)
                //    //    )
                //    //    .Select(x=> new
                //    //    {
                //    //        Id = x.Id,
                //    //        TotalAmount = x.TotalAmount,
                //    //        GroupKey = x.GroupKey,
                //    //        ParentId = x.ParentId,
                //    //        SourceId = x.SourceId,
                //    //    })
                //    //    .Skip(0)
                //    //    .Take(10)
                //    //    .ToList();
                //    _HCoreContext.Dispose();
                //    if (Transactions.Count > 0)
                //    {

                //        foreach (var Transaction in Transactions)
                //        {
                //            _ManageCoreTransaction = new ManageCoreTransaction();
                //            double Balance = _ManageCoreTransaction.GetAccountBalance((long)Transaction.ParentId, Transaction.SourceId);
                //            if (Balance >= Transaction.TotalAmount)
                //            {
                //                using (_HCoreContext = new HCoreContext())
                //                {
                //                    var TransactionGroups = _HCoreContext.HCUAccountTransaction.Where(x => x.GroupKey == Transaction.GroupKey).ToList();
                //                    if (TransactionGroups.Count > 0)
                //                    {
                //                        foreach (var TransactionGroup in TransactionGroups)
                //                        {
                //                            TransactionGroup.InoviceNumber = "PTR" + TransactionGroup.Id;
                //                            TransactionGroup.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
                //                            TransactionGroup.ModifyDate = HCoreHelper.GetGMTDateTime();
                //                            TransactionGroup.ModifyById = HCoreConstant.SystemAccounts.ThankUCashSystemId;
                //                            if (!string.IsNullOrEmpty(TransactionGroup.Comment))
                //                            {
                //                                TransactionGroup.Comment = "Pending rewards credited";
                //                            }
                //                        }
                //                        long? CustomerId = TransactionGroups.FirstOrDefault().CustomerId;
                //                        _HCoreContext.SaveChanges();
                //                        if (CustomerId != null && CustomerId > 0)
                //                        {
                //                            _ManageCoreTransaction = new ManageCoreTransaction();
                //                            _ManageCoreTransaction.UpdateAccountBalance((long)Transaction.ParentId, Transaction.SourceId);
                //                        }
                //                    }
                //                    else
                //                    {
                //                        _HCoreContext.Dispose();
                //                    }
                //                }
                //            }
                //        }
                //        _HCoreContext.SaveChanges();
                //    }
                //    else
                //    {
                //        _HCoreContext.Dispose();
                //    }
                //}
            }
            catch (Exception _Exception)
            {
                HCore.Helper.HCoreHelper.LogException("ProcessPendingTransaction", _Exception);
            }

        }

    }
}
