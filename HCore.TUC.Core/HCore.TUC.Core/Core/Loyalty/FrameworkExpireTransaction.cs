//==================================================================================
// FileName: FrameworkExpireTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;

namespace HCore.TUC.Core.Core.Loyalty
{

    public class FrameworkExpireTransaction
    {
        HCoreContext _HCoreContext;
        public void ExpireInitialisedTransaction()
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime _ExpiaryTime = HCoreHelper.GetGMTDateTime().AddHours(-3);
                    var Transactions = _HCoreContext.HCUAccountTransaction
                        .Where(x => x.StatusId == HCoreConstant.HelperStatus.Transaction.Initialized
                        && x.CreateDate < _ExpiaryTime)
                        .Skip(0)
                        .Take(1000)
                        .ToList();
                    if (Transactions.Count > 0)
                    {
                        foreach (var Transaction in Transactions)
                        {
                            Transaction.StatusId = HCoreConstant.HelperStatus.Transaction.Cancelled;
                            Transaction.ModifyDate = HCoreHelper.GetGMTDateTime();
                            Transaction.ModifyById = HCoreConstant.SystemAccounts.ThankUCashSystemId;
                            Transaction.Comment = "Transaction timeout";
                        }
                        _HCoreContext.SaveChanges();
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCore.Helper.HCoreHelper.LogException("ExpireInitialisedTransaction", _Exception);
            }
           
        }
    }
}
