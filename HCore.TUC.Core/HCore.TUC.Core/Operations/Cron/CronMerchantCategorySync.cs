//==================================================================================
// FileName: CronMerchantCategorySync.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using Akka.Actor;
using HCore.TUC.Core.Cron;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.Cron
{
    public class CronMerchantCategorySync
    {
        HCore.TUC.Core.Cron.CoreCronMerchantCategorySync _CronMerchantCategorySync = new TUC.Core.Cron.CoreCronMerchantCategorySync();
        public void CoreCronMerchantCategorySync()
        {
            var _Actor = ActorSystem.Create("ActorCoreCronMerchantCategorySync");
            var _ActorNotify = _Actor.ActorOf<ActorCoreCronMerchantCategorySync>("ActorCoreCronMerchantCategorySync");
            _ActorNotify.Tell("ActorCoreCronMerchantCategorySync");
        }
    }

    public class AcquirerCampaignStatusUpdate
    {
        public void UpdateAcquirerCampaignStatus()
        {
            var _Actor = ActorSystem.Create("ActorCoreCronMerchantCategorySync");
            var _ActorNotify = _Actor.ActorOf<ActorAcquirerCampaignStatusUpdate>("ActorCoreCronMerchantCategorySync");
            _ActorNotify.Tell("ActorCoreCronMerchantCategorySync");
        }
    }

    internal class ActorAcquirerCampaignStatusUpdate : ReceiveActor
    {
        AcquirerCampaign _AcquirerCampaign;
        public ActorAcquirerCampaignStatusUpdate()
        {
            Receive<string>(_Request =>
            {
                _AcquirerCampaign = new AcquirerCampaign();
                _AcquirerCampaign.UpdateAcquirerCampaignStatus();
            });
        }
    }
}
