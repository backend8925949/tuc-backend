//==================================================================================
// FileName: ManageRm.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Acquirer;
using HCore.TUC.Core.Object.Acquirer;


namespace HCore.TUC.Core.Operations.Acquirer
{
    public class ManageRm
    {
        FrameworkRmManager _FrameworkRmManager;
        public OResponse SaveRmTarget(ORmManager.BulkTarget.Request _Request)
        {
            _FrameworkRmManager = new FrameworkRmManager();
            return _FrameworkRmManager.SaveRmTarget(_Request);
        }
        public OResponse EditRmTarget(ORmManager.BulkTarget.UpdateRequest _Request)
        {
            _FrameworkRmManager = new FrameworkRmManager();
            return _FrameworkRmManager.EditRmTarget(_Request);
        }

        
        public OResponse GetRmTarget(OList.Request _Request)
        {
            _FrameworkRmManager = new FrameworkRmManager();
            return _FrameworkRmManager.GetRmTarget(_Request);
        }
    }
}
