//==================================================================================
// FileName: ManageSubAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Acquirer;
using HCore.TUC.Core.Object.Acquirer;

namespace HCore.TUC.Core.Operations.Acquirer
{
    public class ManageSubAccount
    {
        FrameworkSubAccount _FrameworkSubAccount;
        /// <summary>
        /// Description: Method defined to save subaccount
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveSubAccount(OSubAccount.Request _Request)
        {
            _FrameworkSubAccount = new FrameworkSubAccount();
            return _FrameworkSubAccount.SaveSubAccount(_Request);
        }
        /// <summary>
        /// Description: Method defined to update subaccount
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateSubAccount(OSubAccount.Request _Request)
        {
            _FrameworkSubAccount = new FrameworkSubAccount();
            return _FrameworkSubAccount.UpdateSubAccount(_Request);
        }
        /// <summary>
        /// Description: Method defined to get details of subaccount
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetSubAccount(OReference _Request)
        {
            _FrameworkSubAccount = new FrameworkSubAccount();
            return _FrameworkSubAccount.GetSubAccount(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of subaccounts
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetSubAccount(OList.Request _Request)
        {
            _FrameworkSubAccount = new FrameworkSubAccount();
            return _FrameworkSubAccount.GetSubAccount(_Request);
        }
    }
}
