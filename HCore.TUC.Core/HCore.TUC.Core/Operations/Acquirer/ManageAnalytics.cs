//==================================================================================
// FileName: ManageAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Acquirer;
using HCore.TUC.Core.Object.Acquirer;

namespace HCore.TUC.Core.Operations.Acquirer
{
    public class ManageAnalytics
    {
        FrameworkAnalytics _FrameworkAnalytics;
        /// <summary>
        /// Description: Method defined to get terminal activity history
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetTerminalActivityHistory(OAnalytics.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetTerminalActivityHistory(_Request);
        }
        /// <summary>
        /// Description: Method defined to get account overview
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetAccountOverview(OAnalytics.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetAccountOverview(_Request);
        } 
    }
}
