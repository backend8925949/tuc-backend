//==================================================================================
// FileName: ManageAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Acquirer;
using HCore.TUC.Core.Object.Acquirer;

namespace HCore.TUC.Core.Operations.Acquirer
{
    public class ManageAccount
    {
        FrameworkAccounts _FrameworkAccounts;
        FrameworkAccountOperations _FrameworkAccountOperations;
        /// <summary>
        /// Description: Method defined to save merchant
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveMerchant(OAccounts.Merchant.Onboarding _Request)
        {
            _FrameworkAccountOperations = new FrameworkAccountOperations();
            return _FrameworkAccountOperations.SaveMerchant(_Request);
        }
        /// <summary>
        /// Description: Method defined to save store
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveStore(OAccounts.Store.Request _Request)
        {
            _FrameworkAccountOperations = new FrameworkAccountOperations();
            return _FrameworkAccountOperations.SaveStore(_Request);
        }
        /// <summary>
        /// Description: Method defined to update store
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateStore(OAccounts.Store.Request _Request)
        {
            _FrameworkAccountOperations = new FrameworkAccountOperations();
            return _FrameworkAccountOperations.UpdateStore(_Request);
        }

        /// <summary>
        /// Description: Method defined to get customer details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCustomer(OReference _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetCustomer(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of customers
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCustomer(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetCustomer(_Request);
        }

        /// <summary>
        /// Description: Method defined to get merchant details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetMerchant(OReference _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetMerchant(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of merchants
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetMerchant(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetMerchant(_Request);
        }
        /// <summary>
        /// Description: Method defined to get store details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetStore(OReference _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetStore(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of stores
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetStore(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetStore(_Request);
        }

        /// <summary>
        /// Description: Method defined to save terminal
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveTerminal(OAccounts.Terminal.Request _Request)
        {
            _FrameworkAccountOperations = new FrameworkAccountOperations();
            return _FrameworkAccountOperations.SaveTerminal(_Request);
        }
        /// <summary>
        /// Description: Method defined to save terminal
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveTerminal(OAccounts.Terminal.BulkRequest _Request)
        {
            _FrameworkAccountOperations = new FrameworkAccountOperations();
            return _FrameworkAccountOperations.SaveTerminal(_Request);
        }
        /// <summary>
        /// Description: Method defined to update status of terminal
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateTerminalStatus(OReference _Request)
        {
            _FrameworkAccountOperations = new FrameworkAccountOperations();
            return _FrameworkAccountOperations.UpdateTerminalStatus(_Request);
        }

        /// <summary>
        /// Description: Method defined to get terminal details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetTerminal(OReference _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetTerminal(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of terminals
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetTerminal(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetTerminal(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of acquirer program merchants
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetAcquirerProgramMerchant(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetAcquirerProgramMerchant(_Request);
        }
    }
}
