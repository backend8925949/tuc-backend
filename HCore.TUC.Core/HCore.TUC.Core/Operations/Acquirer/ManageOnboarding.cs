//==================================================================================
// FileName: ManageOnboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Akka.Actor;
using HCore.Helper;
using HCore.TUC.Core.Framework.Acquirer;
using HCore.TUC.Core.Framework.Acquirer.Actor;
using HCore.TUC.Core.Object.Acquirer;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.Acquirer
{
    public class ManageOnboarding
    {
        FrameworkOnboarding _FrameworkOnboarding;
        /// <summary>
        /// Description: Method defined to onboard merchants
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse OnboardMerchants(OOnboarding.Merchant.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboarding();
            return _FrameworkOnboarding.OnboardMerchants(_Request);
        }
        /// <summary>
        /// Description: Method defined to onboard customers
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse OnboardCustomers(OOnboarding.Customer.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboarding();
            return _FrameworkOnboarding.OnboardCustomers(_Request);
        }

        /// <summary>
        /// Description: Method defined to get list of onboarded merchants
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetOnboardedMerchants(OList.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboarding();
            return _FrameworkOnboarding.GetOnboardedMerchants(_Request);
        }
        /// <summary>
        /// Description: Method defined to upload terminal
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UploadTerminal(OAccounts.TerminalUpload.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboarding();
            return _FrameworkOnboarding.UploadTerminal(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of uploaded terminals
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetUploadedTerminal(OList.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboarding();
            return _FrameworkOnboarding.GetUploadedTerminal(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of onboarded customers
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetOnboardedCustomers(OList.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboarding();
            return _FrameworkOnboarding.GetOnboardedCustomers(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of onboarded customer files
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetOnboardedCustomerFiles(OList.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboarding();
            return _FrameworkOnboarding.GetOnboardedCustomerFiles(_Request);
        }
        /// <summary>
        /// 
        /// </summary>
        public void GetOnboardedCustomerFiles()
        {
            var system = ActorSystem.Create("ActorProcessOnboardCustomer");
            var greeter = system.ActorOf<ActorProcessOnboardCustomer>("ActorProcessOnboardCustomer");
            greeter.Tell(1);
        }
        public void ProcessOnboardMerchants()
        {
            var system = ActorSystem.Create("ActorProcessOnboardMerchant");
            var greeter = system.ActorOf<ActorProcessOnboardMerchant>("ActorProcessOnboardMerchant");
            greeter.Tell(1);
        }
        public void ProcessCustomerOnboarding()
        {
            _FrameworkOnboarding = new FrameworkOnboarding();
             _FrameworkOnboarding.ProcessCustomerOnboarding();
        }
    }
}
