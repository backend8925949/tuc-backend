//==================================================================================
// FileName: ManageBranch.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Acquirer;
using HCore.TUC.Core.Object.Acquirer;

namespace HCore.TUC.Core.Operations.Acquirer
{
    public class ManageBranch
    {
        FrameworkBranch _FrameworkBranch;
        /// <summary>
        /// Description: Method defined to save branch
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveBranch(OBranch.Request _Request)
        {
            _FrameworkBranch = new FrameworkBranch();
            return _FrameworkBranch.SaveBranch(_Request);
        }
        /// <summary>
        /// Description: Method defined to update branch
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateBranch(OBranch.Request _Request)
        {
            _FrameworkBranch = new FrameworkBranch();
            return _FrameworkBranch.UpdateBranch(_Request);
        }

        /// <summary>
        /// Description: Method defined to get branch details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetBranch(OReference _Request)
        {
            _FrameworkBranch = new FrameworkBranch();
            return _FrameworkBranch.GetBranch(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of branchs
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetBranch(OList.Request _Request)
        {
            _FrameworkBranch = new FrameworkBranch();
            return _FrameworkBranch.GetBranch(_Request);
        }

        /// <summary>
        /// Description: Method defined to save manager
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveManager(OManager.Request _Request)
        {
            _FrameworkBranch = new FrameworkBranch();
            return _FrameworkBranch.SaveManager(_Request);
        }
        /// <summary>
        /// Description: Method defined to update manager
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateManager(OManager.Request _Request)
        {
            _FrameworkBranch = new FrameworkBranch();
            return _FrameworkBranch.UpdateManager(_Request);
        }
        /// <summary>
        /// Description: Method defined to get manager details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetManager(OReference _Request)
        {
            _FrameworkBranch = new FrameworkBranch();
            return _FrameworkBranch.GetManager(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of managers
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetManager(OList.Request _Request)
        {
            _FrameworkBranch = new FrameworkBranch();
            return _FrameworkBranch.GetManager(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of branch managers
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetBranchManager(OList.Request _Request)
        {
            _FrameworkBranch = new FrameworkBranch();
            return _FrameworkBranch.GetBranchManager(_Request);
        }
    }
}
