//==================================================================================
// FileName: ManageProcessPendingReward.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using Akka.Actor;
using HCore.TUC.Core.Framework.Merchant;

namespace HCore.TUC.Core.Operations.Merchant
{
    //public class ManageProcessPendingReward
    //{
    //    public void ProcessPendingReward()
    //    {

    //        var _Actor = ActorSystem.Create("ActorProcessPendingReward");
    //        var _ActorNotify = _Actor.ActorOf<ActorProcessPendingReward>("ActorProcessPendingReward");
    //        _ActorNotify.Tell("ActorProcessPendingReward");
    //    }
    //}
    //internal class ActorProcessPendingReward : ReceiveActor
    //{
    //    FrameworkProcessPendingReward _FrameworkProcessPendingReward;
    //    public ActorProcessPendingReward()
    //    {
    //        Receive<string>(_Request =>
    //        {
    //            _FrameworkProcessPendingReward = new FrameworkProcessPendingReward();
    //            _FrameworkProcessPendingReward.ProcessPendingReward();
    //        });
    //    }
    //}
}
