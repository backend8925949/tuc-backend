//==================================================================================
// FileName: ManageRedeem.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Merchant.App;
using HCore.TUC.Core.Object.Merchant.App;
using System;
using System.Collections.Generic;
using System.Text;
namespace HCore.TUC.Core.Operations.Merchant.App
{
    public class ManageRedeem
    {
        FrameworkRedeem _FrameworkRedeem;
        /// <summary>
        /// Description: Redeem initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse Redeem_Initialize(ORedeem.Initialize.Request _Request)
        {
            _FrameworkRedeem = new FrameworkRedeem();
            return _FrameworkRedeem.Redeem_Initialize(_Request);
        }
        /// <summary>
        /// Description: Redeem confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse Redeem_Confirm(ORedeem.Confirm.Request _Request)
        {
            _FrameworkRedeem = new FrameworkRedeem();
            return _FrameworkRedeem.Redeem_Confirm(_Request);
        }
    }
}
