//==================================================================================
// FileName: ManageAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Merchant;
using HCore.TUC.Core.Framework.Merchant.Analytics;
using HCore.TUC.Core.Object.Merchant;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.Merchant
{
    public class ManageAnalytics
    {
        FrameworkOverview _FrameworkOverview;
        FrameworkAnalytics _FrameworkAnalytics;

        /// <summary>
        /// Description: Gets the sales overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSalesOverview(OAnalytics.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetSalesOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the customers overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomersOverview(OAnalytics.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetCustomersOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the sales history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSalesHistory(OAnalytics.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetSalesHistory(_Request);
        }

        /// <summary>
        /// Description: Gets the loyalty overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLoyaltyOverview(OAnalytics.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetLoyaltyOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the cashier position history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCashierPosHistory(OAnalytics.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetCashierPosHistory(_Request);
        }
        /// <summary>
        /// Description: Gets the cashiers overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCashiersOverview(OAnalytics.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetCashiersOverview(_Request);
        }


        /// <summary>
        /// Description: Gets the account summary.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountSummary(OAnalytics.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetAccountSummary(_Request);
        }

        //public OResponse GetLoyaltyHistory(OList.Request _Request)
        //{
        //    _FrameworkOverview = new FrameworkOverview();
        //    return _FrameworkOverview.GetLoyaltyHistory(_Request);
        //}

        /// <summary>
        /// Description: Gets the loyalty visit history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLoyaltyVisitHistory(OList.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetLoyaltyVisitHistory(_Request);
        }



        /// <summary>
        /// Description: Gets the account overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountOverview(OAnalytics.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetAccountOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the terminal activity history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTerminalActivityHistory(OAnalytics.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetTerminalActivityHistory(_Request);
        }

        /// <summary>
        /// Description: Gets the loyalty overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetGroupLoyaltyOverview(OAnalytics.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetGroupLoyaltyOverview(_Request);
        }
        /// Description: Gets the loyalty visit history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetGroupLoyaltyVisitHistory(OList.Request _Request)
        {
            _FrameworkOverview = new FrameworkOverview();
            return _FrameworkOverview.GetGroupLoyaltyVisitHistory(_Request);
        }
    }
}
