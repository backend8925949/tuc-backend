//==================================================================================
// FileName: ManageOnboardingV2.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Merchant;
using HCore.TUC.Core.Object.Merchant;
using static HCore.TUC.Core.Object.Merchant.OOnboardingV2;

namespace HCore.TUC.Core.Operations.Merchant
{
    public class ManageOnboardingV2
    {
        FrameworkOnboardingV2 _FrameworkOnboarding;

        public OResponse OnboardMerchant_Request(OOnboardingV2.Profile.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboardingV2();
            return _FrameworkOnboarding.OnboardMerchant_Request(_Request);
        }
        public OResponse OnboardMerchant_EmailUpdate(OOnboardingV2.EmailChange.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboardingV2();
            return _FrameworkOnboarding.OnboardMerchant_EmailUpdate(_Request);
        }
        public OResponse OnboardMerchant_ResendEmailCode(OOnboardingV2.EmailChange.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboardingV2();
            return _FrameworkOnboarding.OnboardMerchant_ResendEmailCode(_Request);
        }
        public OResponse OnboardMerchant_VerifyEmail(OOnboardingV2.EmailVerify.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboardingV2();
            return _FrameworkOnboarding.OnboardMerchant_VerifyEmail(_Request);
        }

        public OResponse OnboardMerchant_MobileUpdate(OOnboardingV2.MobileChange.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboardingV2();
            return _FrameworkOnboarding.OnboardMerchant_MobileUpdate(_Request);
        }
        public OResponse OnboardMerchant_Request_MobileVerification(OOnboardingV2.MobileVerify.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboardingV2();
            return _FrameworkOnboarding.OnboardMerchant_Request_MobileVerification(_Request);
        }
        public OResponse OnboardMerchant_Request_MobileVerificationConfirm(OOnboardingV2.MobileVerifyConfirm.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboardingV2();
            return _FrameworkOnboarding.OnboardMerchant_Request_MobileVerificationConfirm(_Request);
        }
        public OResponse OnboardMerchant_Request_UpdateBusinessDetails(OOnboardingV2.BusinessInformation _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboardingV2();
            return _FrameworkOnboarding.OnboardMerchant_Request_UpdateBusinessDetails(_Request);
        }
        public OResponse OnboardMerchant_Request_AccountConfiguration(OOnboardingV2.AccountType _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboardingV2();
            return _FrameworkOnboarding.OnboardMerchant_Request_AccountConfiguration(_Request);
        }
        public OResponse GetMerchantDetails(OReference _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboardingV2();
            return _FrameworkOnboarding.GetMerchantDetails(_Request);
        }

        public OResponse SaveMerchant(OOnboardingV2.BusinessInformation _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboardingV2();
            return _FrameworkOnboarding.SaveMerchant(_Request);
        }

        public OResponse GetOnboardMerchants(OList.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboardingV2();
            return _FrameworkOnboarding.GetOnboardMerchants(_Request);
        }

        public OResponse Merchant_Onboarding_VerifyDocument(VerifyDocument _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboardingV2();
            return _FrameworkOnboarding.Merchant_Onboarding_VerifyDocument(_Request);
        }
    }
}
    