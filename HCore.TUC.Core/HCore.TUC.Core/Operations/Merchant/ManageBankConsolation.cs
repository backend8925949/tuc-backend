//==================================================================================
// FileName: ManageBankConsolation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Merchant;
using HCore.TUC.Core.Object.Merchant;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.Merchant
{
    public class ManageBankConsolation
    {
        FrameworkBankConsolation _FrameworkBankConsolation;
        /// <summary>
        /// Description: Synchronizes the store bank collections.
        /// </summary>
        public void SyncStoreBankCollections()
        {
            _FrameworkBankConsolation = new FrameworkBankConsolation();
            _FrameworkBankConsolation.SyncStoreBankCollections();
        }
        /// <summary>
        /// Description: Gets the store bank collection.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStoreBankCollection(OStoreBankCollection.Request _Request)
        {
            _FrameworkBankConsolation = new FrameworkBankConsolation();
            return _FrameworkBankConsolation.GetStoreBankCollection(_Request);
        }
        /// <summary>
        /// Description: Updates the received amount.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateReceivedAmount(OStoreBankCollection.Request _Request)
        {
            _FrameworkBankConsolation = new FrameworkBankConsolation();
            return _FrameworkBankConsolation.UpdateReceivedAmount(_Request);
        }
    }
}
