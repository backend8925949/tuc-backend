//==================================================================================
// FileName: ManageOnboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Merchant;
using HCore.TUC.Core.Object.Merchant;
using System;
using System.Collections.Generic;
using System.Text;
namespace HCore.TUC.Core.Operations.Merchant
{
    public class ManageOnboarding
    {
        FrameworkOnboarding _FrameworkOnboarding;

        /// <summary>
        /// Description: Onboard merchant ST1.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse OnboardMerchant_St1(OOnboarding.St1.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboarding();
            return _FrameworkOnboarding.OnboardMerchant_St1(_Request);
        }

        /// <summary>
        /// Description: Onboard merchant ST2 resend email.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse OnboardMerchant_St2_ResendEmail(OOnboarding.St2.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboarding();
            return _FrameworkOnboarding.OnboardMerchant_St2_ResendEmail(_Request);
        }

        /// <summary>
        /// Description: Onboard merchant ST3 verify email.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse OnboardMerchant_St3_VerifyEmail(OOnboarding.St3.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboarding();
            return _FrameworkOnboarding.OnboardMerchant_St3_VerifyEmail(_Request);
        }
        /// <summary>
        /// Description: Onboard merchant ST4 request mobile verification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse OnboardMerchant_St4_RequestMobileVerification(OOnboarding.St4.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboarding();
            return _FrameworkOnboarding.OnboardMerchant_St4_RequestMobileVerification(_Request);
        }

        /// <summary>
        /// Description: Onboard merchant ST5 verify mobile number.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse OnboardMerchant_St5_VerifyMobileNumber(OOnboarding.St5.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboarding();
            return _FrameworkOnboarding.OnboardMerchant_St5_VerifyMobileNumber(_Request);
        }

        /// <summary>
        /// Description: Onboard merchant ST6 complete setup.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse OnboardMerchant_St6_CompleteSetup(OOnboarding.St6.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboarding();
            return _FrameworkOnboarding.OnboardMerchant_St6_CompleteSetup(_Request);
        }

        /// <summary>
        /// Description: Onboard customers.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse OnboardCustomers(OOnboarding.Customer.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboarding();
            return _FrameworkOnboarding.OnboardCustomers(_Request);
        }
    }
}
