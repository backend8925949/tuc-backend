//==================================================================================
// FileName: ManageAccounts.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Merchant;
using HCore.TUC.Core.Object.Merchant;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.Merchant
{
    public class ManageAccounts
    {
        FrameworkAccounts _FrameworkAccounts;
        FrameworkAccountOperation _FrameworkAccountOperation;
        FrameworkMerchantOnboarding _FrameworkMerchantOnboarding;

        /// <summary>
        /// Description: Web pay initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse WebPay_Initialize(OAccounts.OCoreMerchantWebPay.Initialize.Request _Request)
        {
            _FrameworkMerchantOnboarding = new FrameworkMerchantOnboarding();
            return _FrameworkMerchantOnboarding.WebPay_Initialize(_Request);
        }
        /// <summary>
        /// Description: Web pay confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse WebPay_Confirm(OAccounts.OCoreMerchantWebPay.Confirm _Request)
        {
            _FrameworkMerchantOnboarding = new FrameworkMerchantOnboarding();
            return _FrameworkMerchantOnboarding.WebPay_Confirm(_Request);
        }
        //OnboardMerchant
        /// <summary>
        /// Description: Onboard Merchant
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse OnboardMerchantV4(OAccounts.MerchantOnboardingV4.Request _Request)
        {
            _FrameworkMerchantOnboarding = new FrameworkMerchantOnboarding();
            return _FrameworkMerchantOnboarding.OnboardMerchantV4(_Request);
        }

        public OResponse OnboardSafaricomMerchant(OAccounts.MerchantOnboarding.MerchantDetails _Request)
        {
            _FrameworkMerchantOnboarding = new FrameworkMerchantOnboarding();
            return _FrameworkMerchantOnboarding.OnboardSafaricomMerchant(_Request);
        }

        /// <summary>
        /// Description: Onboard Merchant
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse OnboardMerchant(OAccounts.MerchantOnboarding.MerchantDetails _Request)
        {
            _FrameworkMerchantOnboarding = new FrameworkMerchantOnboarding();
            return _FrameworkMerchantOnboarding.OnboardMerchant(_Request);
        }
        /// <summary>
        /// Description: Onboard Merchant verify number
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse OnboardMerchantVerifyNumber(OAccounts.MerchantOnboarding.VerifyOtp _Request)
        {
            _FrameworkMerchantOnboarding = new FrameworkMerchantOnboarding();
            return _FrameworkMerchantOnboarding.OnboardMerchantVerifyNumber(_Request);
        }
        /// <summary>
        /// Description: Onboard merchant request verification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse OnboardMerchantRequestVerification(OAccounts.MerchantOnboarding.VerifyOtp _Request)
        {
            _FrameworkMerchantOnboarding = new FrameworkMerchantOnboarding();
            return _FrameworkMerchantOnboarding.OnboardMerchantRequestVerification(_Request);
        }
        /// <summary>
        /// Description: Onboard merchant change verification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse OnboardMerchantChangeVerification(OAccounts.MerchantOnboarding.ChangeNumber _Request)
        {
            _FrameworkMerchantOnboarding = new FrameworkMerchantOnboarding();
            return _FrameworkMerchantOnboarding.OnboardMerchantChangeVerification(_Request);
        }

        /// <summary>
        /// Description: Saves the customer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveCustomer(OAccountOperations.Customer.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.SaveCustomer(_Request);
        }
        /// <summary>
        /// Description: Saves the store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveStore(OAccountOperations.Store.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.SaveStore(_Request);
        }
        /// <summary>
        /// Description: Saves the cashier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveCashier(OAccountOperations.Cashier.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.SaveCashier(_Request);
        }

        /// <summary>
        /// Description: Saves the terminal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveTerminal(OAccountOperations.Terminal.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.SaveTerminal(_Request);
        }
        /// <summary>
        /// Description: Saves the sub account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveSubAccount(OAccountOperations.SubAccount.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.SaveSubAccount(_Request);
        }

        /// <summary>
        /// Description: Updates the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateMerchant(OAccountOperations.Merchant.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.UpdateMerchant(_Request);
        }
        /// <summary>
        /// Description: Updates the store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateStore(OAccountOperations.Store.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.UpdateStore(_Request);
        }
        /// <summary>
        /// Description: Updates the cashier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateCashier(OAccountOperations.Cashier.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.UpdateCashier(_Request);
        }
        /// <summary>
        /// Description: Updates the sub account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateSubAccount(OAccountOperations.SubAccount.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.UpdateSubAccount(_Request);
        }

        /// <summary>
        /// Description: Resets the sub account password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ResetSubAccountPassword(OAccountOperations.SubAccount.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.ResetSubAccountPassword(_Request);
        }

        /// <summary>
        /// Description: Gets the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchant(OAccounts.Merchant.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetMerchant(_Request);
        }
        /// <summary>
        /// Description: Gets the cashier list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCashiers(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetCashiers(_Request);
        }
        /// <summary>
        /// Description: Gets the cashiers overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCashiersOverview(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetCashiersOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the cashier details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCashier(OAccounts.Cashier.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetCashier(_Request);
        }
        /// <summary>
        /// Description: Gets the store details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStore(OAccounts.Store.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetStore(_Request);
        }
        /// <summary>
        /// Description: Gets the store list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStores(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetStores(_Request);
        }
        /// <summary>
        /// Description: Gets the terminal list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTerminals(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetTerminals(_Request);
        }
        /// <summary>
        /// Description: Gets the terminals overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTerminalsOverview(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetTerminalsOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the terminal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTerminal(OAccounts.Terminal.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetTerminal(_Request);
        }



        /// <summary>
        /// Description: Gets the sub account list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSubAccount(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetSubAccount(_Request);
        }
        /// <summary>
        /// Description: Gets the sub account details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSubAccount(OAccounts.SubAccount.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetSubAccount(_Request);
        }


        /// <summary>
        /// The framework customer
        /// </summary>
        FrameworkCustomer _FrameworkCustomer;
        /// <summary>
        /// Description: Gets the customer list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomer(OList.Request _Request)
        {
            _FrameworkCustomer = new FrameworkCustomer();
            return _FrameworkCustomer.GetCustomer(_Request);
        }
        /// <summary>
        /// Description: Gets the customer overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomerOverview(OList.Request _Request)
        {
            _FrameworkCustomer = new FrameworkCustomer();
            return _FrameworkCustomer.GetCustomerOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the customer details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomer(OAccounts.Customer.Request _Request)
        {
            _FrameworkCustomer = new FrameworkCustomer();
            return _FrameworkCustomer.GetCustomer(_Request);
        }
        /// <summary>
        /// Description: Soft delete implementation
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteCustomer(OAccounts.Customer.Request _Request)
        {
            _FrameworkCustomer = new FrameworkCustomer();
            return _FrameworkCustomer.DeleteCustomer(_Request);
        }

        /// <summary>
        /// Description: Resets the merchant password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ResetMerchantPassword(OAccountOperations.Merchant.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.ResetMerchantPassword(_Request);
        }

        /// <summary>
        /// Description: Enables the terminal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse EnableTerminal(OAccountOperations.Terminal.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.EnableTerminal(_Request);
        }
        /// <summary>
        /// Description: Disables the terminal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DisableTerminal(OAccountOperations.Terminal.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.DisableTerminal(_Request);
        }

        /// <summary>
        /// Description: Enables the store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse EnableStore(OAccountOperations.Store.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.EnableStore(_Request);
        }
        /// <summary>
        /// Description: Disables the store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DisableStore(OAccountOperations.Store.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.DisableStore(_Request);
        }

        /// <summary>
        /// Description: Enables the cashier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse EnableCashier(OAccountOperations.Cashier.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.EnableCashier(_Request);
        }
        /// <summary>
        /// Description: Disables the cashier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DisableCashier(OAccountOperations.Cashier.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.DisableCashier(_Request);
        }

        /// <summary>
        /// Description: Resets the cashier password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ResetCashierPassword(OAccountOperations.Cashier.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.ResetCashierPassword(_Request);
        }


        /// <summary>
        /// Description: Enables the subaccount.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse EnableSubaccount(OAccountOperations.SubAccount.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.EnableSubaccount(_Request);
        }
        /// <summary>
        /// Description: Disables the subaccount.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DisableSubaccount(OAccountOperations.SubAccount.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.DisableSubaccount(_Request);
        }

        /// <summary>
        /// Description: Gets the store manager list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStoreManagers(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetStoreManagers(_Request);
        }

        public ResponseDto<CustomerAccountDetails> SaveMerchantAccountDetails(CustomerAccountDetails request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.SaveMerchantBankAccount(request);
        }

    }
}
