//==================================================================================
// FileName: ManageTransactions.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Merchant;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.Merchant
{
    public class ManageTransactions
    {
        FrameworkTransactions _FrameworkTransactions;
        /// <summary>
        /// Description: Updates the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateTransaction(OReference _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.UpdateTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the sale transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSaleTransaction(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetSaleTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the sale transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSaleTransactionOverview(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetSaleTransactionOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the reward transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRewardTransaction(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetRewardTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the reward transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRewardTransactionOverview(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetRewardTransactionOverview(_Request);
        }


        /// <summary>
        /// Description: Gets the pending reward transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPendingRewardTransaction(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetPendingRewardTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the pending reward transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPendingRewardTransactionOverview(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetPendingRewardTransactionOverview(_Request);
        }


        /// <summary>
        /// Description: Gets the reward claim transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRewardClaimTransaction(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetRewardClaimTransaction(_Request);
        }

        /// <summary>
        /// Description: Gets the reward claim transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRewardClaimTransactionOverview(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetRewardClaimTransactionOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the redeem transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRedeemTransaction(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetRedeemTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the redeem transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRedeemTransactionOverview(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetRedeemTransactionOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the campaign transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCampaignTransaction(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetCampaignTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the campaign transaction overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCampaignTransactionOverview(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetCampaignTransactionOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the transactions.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTransactions(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetTransactions(_Request);
        }
        /// <summary>
        /// Description: Gets the pending reward.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPendingReward(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetPendingReward(_Request);
        }
        /// <summary>
        /// Author : Ugochukwu Oluo
        /// Description: Get all redeem wallet from STORE AND 3RD PARTY
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetAllRedeemTransaction(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetAllRedeemTransaction(_Request);
        }

        /// <summary>
        /// Description: Gets the acquirer  transaction list.
        /// Authored by <b>Joshua Ajibade</b> <br/>
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAcquirerTransaction(OList.Request _Request)
        {
            _FrameworkTransactions = new FrameworkTransactions();
            return _FrameworkTransactions.GetAcquirerTransaction(_Request);
        }
    }
}
