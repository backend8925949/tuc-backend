//==================================================================================
// FileName: ManageCustomersOverview.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Console.Analytics.Customer;
using HCore.TUC.Core.Framework.Merchant.Analytics;
using HCore.TUC.Core.Object.Console.Analytics;
using HCore.TUC.Core.Object.Merchant;

namespace HCore.TUC.Core.Operations.Merchant
{
    public class ManageCustomersOverview
    {
        FrameworkCustomersOverview _FrameworkCustomersOverview;

        /// <summary>
        /// Description: Gets the customers gender.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomersGender(OCustomersOverview.OAnalytics.Request _Request)
        {
            _FrameworkCustomersOverview = new FrameworkCustomersOverview();
            return _FrameworkCustomersOverview.GetCustomersGender(_Request);
        }
        /// <summary>
        /// Description: Gets the customers age group gender.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomersAgeGroupGender(OCustomersOverview.OAnalytics.Request _Request)
        {
            _FrameworkCustomersOverview = new FrameworkCustomersOverview();
            return _FrameworkCustomersOverview.GetCustomersAgeGroupGender(_Request);
        }
        /// <summary>
        /// Description: Gets the customers top spenders.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomersTopSpenders(OCustomersOverview.OAnalytics.Request _Request)
        {
            _FrameworkCustomersOverview = new FrameworkCustomersOverview();
            return _FrameworkCustomersOverview.GetCustomersTopSpenders(_Request);
        }
        /// <summary>
        /// Description: Gets the customer rewards overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomerRewardsOverview(OReference _Request)
        {
            _FrameworkCustomersOverview = new FrameworkCustomersOverview();
            return _FrameworkCustomersOverview.GetCustomerRewardsOverview(_Request);
        }
    }
}

