//==================================================================================
// FileName: ManageSubscription.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Merchant;
using HCore.TUC.Core.Object.Merchant;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.Merchant
{
    public class ManageSubscription
    {
        FrameworkSubscription _FrameworkSubscription;
        /// <summary>
        /// Description: Topups the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse TopupAccount(OSubscription.Topup.Request _Request)
        {
            _FrameworkSubscription = new FrameworkSubscription();
            return _FrameworkSubscription.TopupAccount(_Request);
        }
        /// <summary>
        /// Description: Gets the topup history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTopupHistory(OList.Request _Request)
        {
            _FrameworkSubscription = new FrameworkSubscription();
            return _FrameworkSubscription.GetTopupHistory(_Request);
        }
        /// <summary>
        /// Description: Gets the account balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountBalance(OSubscription.Balance.Request _Request)
        {
            _FrameworkSubscription = new FrameworkSubscription();
            return _FrameworkSubscription.GetAccountBalance(_Request);
        }

        /// <summary>
        /// Description: Gets the top up overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTopUpOverview(OList.Request _Request)
        {
            _FrameworkSubscription = new FrameworkSubscription();
            return _FrameworkSubscription.GetTopUpOverview(_Request);
        }

    }
}
