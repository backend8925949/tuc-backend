//==================================================================================
// FileName: ManageCategory.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Merchant.Framework;
using HCore.TUC.Core.Merchant.Object;

namespace HCore.TUC.Core.Merchant
{
    public class ManageCategory
    {
        FrameworkCategory _FrameworkCategory;

        /// <summary>
        /// Description: Saves the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveCategory(OCategory.Save.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.SaveCategory(_Request);
        }
        /// <summary>
        /// Description: Updates the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateCategory(OCategory.Update _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.UpdateCategory(_Request);
        }
        /// <summary>
        /// Description: Deletes the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteCategory(OReference _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.DeleteCategory(_Request);
        }
        /// <summary>
        /// Description: Gets the categorydetails.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCategory(OReference _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.GetCategory(_Request);
        }

        /// <summary>
        /// Description: Gets the category list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCategory(OList.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.GetCategory(_Request);
        }

        /// <summary>
        /// Description: Gets all category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAllCategory(OList.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.GetAllCategory(_Request);
        }
    }
}
