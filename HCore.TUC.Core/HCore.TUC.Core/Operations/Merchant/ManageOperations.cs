//==================================================================================
// FileName: ManageOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Merchant;
using HCore.TUC.Core.Object.Merchant;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.Merchant
{
    public class ManageOperations
    {
        FrameworkOperations _FrameworkOperations;

        /// <summary>
        /// Description: Saves the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveConfiguration(OOperations.Configuration.Request _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.SaveConfiguration(_Request);
        }
        /// <summary>
        /// Description: Saves the configurations.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveConfigurations(OOperations.Configuration.BulkUpdate _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.SaveConfigurations(_Request);
        }

        /// <summary>
        /// Description: Gets the configuration details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetConfiguration(OOperations.Configuration.Config _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.GetConfiguration(_Request);
        }
        /// <summary>
        /// Description: Gets the loyalty configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLoyaltyConfiguration(OOperations.Configuration.Config _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.GetLoyaltyConfiguration(_Request);
        }
        /// <summary>
        /// Description: Gets the configuration list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetConfiguration(OList.Request _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.GetConfiguration(_Request);
        }
        /// <summary>
        /// Description: Gets the configuration history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetConfigurationHistory(OList.Request _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.GetConfigurationHistory(_Request);
        }
    }
}
