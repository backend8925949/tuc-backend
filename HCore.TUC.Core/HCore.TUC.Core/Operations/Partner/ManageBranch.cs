//==================================================================================
// FileName: ManageBranch.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Partner;
using HCore.TUC.Core.Object.Partner;

namespace HCore.TUC.Core.Operations.Partner
{
    public class ManageBranch
    {
        FrameworkBranch _FrameworkBranch;
        /// <summary>
        /// Description: Saves the branch.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveBranch(OBranch.Request _Request)
        {
            _FrameworkBranch = new FrameworkBranch();
            return _FrameworkBranch.SaveBranch(_Request);
        }
        /// <summary>
        /// Description: Updates the branch.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateBranch(OBranch.Request _Request)
        {
            _FrameworkBranch = new FrameworkBranch();
            return _FrameworkBranch.UpdateBranch(_Request);
        }

        /// <summary>
        /// Description: Gets the branch.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetBranch(OReference _Request)
        {
            _FrameworkBranch = new FrameworkBranch();
            return _FrameworkBranch.GetBranch(_Request);
        }
        /// <summary>
        /// Description: Gets the branch.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetBranch(OList.Request _Request)
        {
            _FrameworkBranch = new FrameworkBranch();
            return _FrameworkBranch.GetBranch(_Request);
        }

        /// <summary>
        /// Description: Saves the manager.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveManager(OManager.Request _Request)
        {
            _FrameworkBranch = new FrameworkBranch();
            return _FrameworkBranch.SaveManager(_Request);
        }
        /// <summary>
        /// Description: Updates the manager.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateManager(OManager.Request _Request)
        {
            _FrameworkBranch = new FrameworkBranch();
            return _FrameworkBranch.UpdateManager(_Request);
        }
        /// <summary>
        /// Description: Gets the manager.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetManager(OReference _Request)
        {
            _FrameworkBranch = new FrameworkBranch();
            return _FrameworkBranch.GetManager(_Request);
        }
        /// <summary>
        /// Description: Gets the manager.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetManager(OList.Request _Request)
        {
            _FrameworkBranch = new FrameworkBranch();
            return _FrameworkBranch.GetManager(_Request);
        }
    }
}
