//==================================================================================
// FileName: ManageOnboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Partner;
using HCore.TUC.Core.Object.Partner;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.Partner
{
    public class ManageOnboarding
    {
        FrameworkOnboarding _FrameworkOnboarding;
        /// <summary>
        /// Description: Method to onboard merchants.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse OnboardMerchants(OOnboarding.Merchant.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboarding();
            return _FrameworkOnboarding.OnboardMerchants(_Request);
        }

        /// <summary>
        /// Description: Gets the onboarded merchants.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetOnboardedMerchants(OList.Request _Request)
        {
            _FrameworkOnboarding = new FrameworkOnboarding();
            return _FrameworkOnboarding.GetOnboardedMerchants(_Request);
        }
    }
}
