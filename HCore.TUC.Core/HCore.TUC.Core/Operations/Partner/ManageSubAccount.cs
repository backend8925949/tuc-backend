//==================================================================================
// FileName: ManageSubAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Partner;
using HCore.TUC.Core.Object.Partner;

namespace HCore.TUC.Core.Operations.Partner
{
    public class ManageSubAccount
    {
        FrameworkSubAccount _FrameworkSubAccount;
        /// <summary>
        /// Description: Saves the sub account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveSubAccount(OSubAccount.Request _Request)
        {
            _FrameworkSubAccount = new FrameworkSubAccount();
            return _FrameworkSubAccount.SaveSubAccount(_Request);
        }
        /// <summary>
        /// Description: Updates the sub account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateSubAccount(OSubAccount.Request _Request)
        {
            _FrameworkSubAccount = new FrameworkSubAccount();
            return _FrameworkSubAccount.UpdateSubAccount(_Request);
        }
        /// <summary>
        /// Description: Gets the sub account details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSubAccount(OReference _Request)
        {
            _FrameworkSubAccount = new FrameworkSubAccount();
            return _FrameworkSubAccount.GetSubAccount(_Request);
        }
        /// <summary>
        /// Description: Gets the list of sub account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSubAccount(OList.Request _Request)
        {
            _FrameworkSubAccount = new FrameworkSubAccount();
            return _FrameworkSubAccount.GetSubAccount(_Request);
        }
    }
}
