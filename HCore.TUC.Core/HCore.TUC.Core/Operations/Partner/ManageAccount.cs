//==================================================================================
// FileName: ManageAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Partner;
using HCore.TUC.Core.Object.Partner;

namespace HCore.TUC.Core.Operations.Partner
{
    public class ManageAccount
    {
        FrameworkAccounts _FrameworkAccounts;        
        FrameworkAccountOperations _FrameworkAccountOperations;
        /// <summary>
        /// Description: Saves the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveMerchant(OAccounts.Merchant.Onboarding _Request)
        {
            _FrameworkAccountOperations = new FrameworkAccountOperations();
            return _FrameworkAccountOperations.SaveMerchant(_Request);
        }
        /// <summary>
        /// Description: Saves the store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveStore(OAccounts.Store.Request _Request)
        {
            _FrameworkAccountOperations = new FrameworkAccountOperations();
            return _FrameworkAccountOperations.SaveStore(_Request);
        }
        /// <summary>
        /// Description: Updates the store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateStore(OAccounts.Store.Request _Request)
        {
            _FrameworkAccountOperations = new FrameworkAccountOperations();
            return _FrameworkAccountOperations.UpdateStore(_Request);
        }
        /// <summary>
        /// Description: Gets the merchant details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchant(OReference _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetMerchant(_Request);
        }
        /// <summary>
        /// Description: Gets the  list of all merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchant(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetMerchant(_Request);
        }
        /// <summary>
        /// Description: Gets the store details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStore(OReference _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetStore(_Request);
        }
        /// <summary>
        /// Description: Gets the list of all store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStore(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetStore(_Request);
        }

        /// <summary>
        /// Description: Saves the terminal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveTerminal(OAccounts.Terminal.Request _Request)
        {
            _FrameworkAccountOperations = new FrameworkAccountOperations();
            return _FrameworkAccountOperations.SaveTerminal(_Request);
        }
        /// <summary>
        /// Description: Saves the terminal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveTerminal(OAccounts.Terminal.BulkRequest _Request)
        {
            _FrameworkAccountOperations = new FrameworkAccountOperations();
            return _FrameworkAccountOperations.SaveTerminal(_Request);
        }
        /// <summary>
        /// Description: Updates the terminal status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateTerminalStatus(OReference _Request)
        {
            _FrameworkAccountOperations = new FrameworkAccountOperations();
            return _FrameworkAccountOperations.UpdateTerminalStatus(_Request);
        }

        /// <summary>
        /// Description: Gets the terminal details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTerminal(OReference _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetTerminal(_Request);
        }
        /// <summary>
        /// Description: Gets the list of all terminal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTerminal(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetTerminal(_Request);
        }

        /// <summary>
        /// Description: Gets the customer details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomer(OReference _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetCustomer(_Request);
        }
        /// <summary>
        /// Description: Gets the list of all customer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomer(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetCustomer(_Request);
        }
    }
}
