//==================================================================================
// FileName: BackgroundActivityStatusManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

//﻿using System;
//using HCore.TUC.Core.CoreBackground;

//namespace HCore.TUC.Core.Operations.Background
//{
//    public class BackgroundActivityStatusManager
//    {
//        CoreActivityStatusUpdate _CoreActivityStatusUpdate;
//       public void UpdateAccountActivityStatus()
//        {
//            _CoreActivityStatusUpdate = new CoreActivityStatusUpdate();
//            _CoreActivityStatusUpdate.UpdateAccountActivityStatus();
//        }
//    }
//}
