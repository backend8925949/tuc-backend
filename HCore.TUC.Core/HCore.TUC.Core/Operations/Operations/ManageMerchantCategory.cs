//==================================================================================
// FileName: ManageMerchantCategory.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;
using HCore.TUC.Core.Object.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Core.Operations.Operations
{
    public class ManageMerchantCategory
    {

        FrameworkMerchantCategory _FrameworkMerchantCategory;

        /// <summary>
        /// Description: Method defined to save merchant category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveMerchantCategory(OMerchantCategory.Request _Request)
        {
            _FrameworkMerchantCategory = new FrameworkMerchantCategory();
            return _FrameworkMerchantCategory.SaveMerchantCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to update merchant category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateMerchantCategory(OMerchantCategory.Request _Request)
        {
            _FrameworkMerchantCategory = new FrameworkMerchantCategory();
            return _FrameworkMerchantCategory.UpdateMerchantCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to delete merchant category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteMerchantCategory(OMerchantCategory.Request _Request)
        {
            _FrameworkMerchantCategory = new FrameworkMerchantCategory();
            return _FrameworkMerchantCategory.DeleteMerchantCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to get details of merchant category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetMerchantCategory(OMerchantCategory.Request _Request)
        {
            _FrameworkMerchantCategory = new FrameworkMerchantCategory();
            return _FrameworkMerchantCategory.GetMerchantCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of merchant categories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetMerchantCategoryList(OList.Request _Request)
        {
            _FrameworkMerchantCategory = new FrameworkMerchantCategory();
            return _FrameworkMerchantCategory.GetMerchantCategoryList(_Request);
        }
        /// <summary>
        /// Description: Method to get list of root categories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetRootCategories(OList.Request _Request)
        {
            _FrameworkMerchantCategory = new FrameworkMerchantCategory();
            return _FrameworkMerchantCategory.GetRootCategories(_Request);
        }
    }
}
