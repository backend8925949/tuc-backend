//==================================================================================
// FileName: ManageCountryManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Core.Operations.Operations
{
    public class ManageCountryManager
    {
        FrameworkCountryManager _FrameworkCountryManager;

        /// <summary>
        /// Description:Method to get list of countries
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCountry(OList.Request _Request)
        {
            _FrameworkCountryManager = new FrameworkCountryManager();
            return _FrameworkCountryManager.GetCountry(_Request);
        }

        /// <summary>
        /// Description:Method to get list of countries
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCountries(OList.Request _Request)
        {
            _FrameworkCountryManager = new FrameworkCountryManager();
            return _FrameworkCountryManager.GetCountries(_Request);
        }
    }
}
