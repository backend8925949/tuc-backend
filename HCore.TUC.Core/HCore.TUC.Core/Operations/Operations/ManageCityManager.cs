//==================================================================================
// FileName: ManageCityManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;
using HCore.TUC.Core.Object.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Core.Operations.Operations
{
    public class ManageCityManager
    {
        FrameworkCityManager _FrameworkCityManager;
        FrameworkCityAreaManager _FrameworkCityAreaManager;

        /// <summary>
        /// Description: Method defined to save city
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveCity(OCityManager.Save.Request _Request)
        {
            _FrameworkCityManager = new FrameworkCityManager();
            return _FrameworkCityManager.SaveCity(_Request);
        }
        /// <summary>
        /// Description: Method defined to update city
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateCity(OCityManager.Update _Request)
        {
            _FrameworkCityManager = new FrameworkCityManager();
            return _FrameworkCityManager.UpdateCity(_Request);
        }
        /// <summary>
        /// Description: Method defined to delete city
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteCity(OReference _Request)
        {
            _FrameworkCityManager = new FrameworkCityManager();
            return _FrameworkCityManager.DeleteCity(_Request);
        }
        /// <summary>
        /// Description: Method defined to get city details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCity(OReference _Request)
        {
            _FrameworkCityManager = new FrameworkCityManager();
            return _FrameworkCityManager.GetCity(_Request);
        }
        /// <summary>
        /// Description: Method defined to get city list
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCity(OList.Request _Request)
        {
            _FrameworkCityManager = new FrameworkCityManager();
            return _FrameworkCityManager.GetCity(_Request);
        }

        /// <summary>
        /// Description: Method defined to get city list
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCities(OList.Request _Request)
        {
            _FrameworkCityManager = new FrameworkCityManager();
            return _FrameworkCityManager.GetCities(_Request);
        }



        /// <summary>
        /// Description: Method defined to save city area
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveCityArea(OCityAreaManager.Save.Request _Request)
        {
            _FrameworkCityAreaManager = new FrameworkCityAreaManager();
            return _FrameworkCityAreaManager.SaveCityArea(_Request);
        }
        /// <summary>
        /// Description: Method defined to update city area
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateCityArea(OCityAreaManager.Update _Request)
        {
            _FrameworkCityAreaManager = new FrameworkCityAreaManager();
            return _FrameworkCityAreaManager.UpdateCityArea(_Request);
        }
        /// <summary>
        /// Description: Method defined to delete city area
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteCityArea(OReference _Request)
        {
            _FrameworkCityAreaManager = new FrameworkCityAreaManager();
            return _FrameworkCityAreaManager.DeleteCityArea(_Request);
        }
        /// <summary>
        /// Description: Method defined to get city area details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCityArea(OReference _Request)
        {
            _FrameworkCityAreaManager = new FrameworkCityAreaManager();
            return _FrameworkCityAreaManager.GetCityArea(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of city areas
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCityArea(OList.Request _Request)
        {
            _FrameworkCityAreaManager = new FrameworkCityAreaManager();
            return _FrameworkCityAreaManager.GetCityArea(_Request);
        }

        /// <summary>
        /// Description: Method defined to get list of city areas
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCityAreas(OList.Request _Request)
        {
            _FrameworkCityAreaManager = new FrameworkCityAreaManager();
            return _FrameworkCityAreaManager.GetCityAreas(_Request);
        }
    }
}
