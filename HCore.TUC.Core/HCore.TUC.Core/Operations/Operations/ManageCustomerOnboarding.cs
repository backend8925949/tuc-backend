//==================================================================================
// FileName: ManageCustomerOnboarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;
using HCore.TUC.Core.Object.Operations;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.Operations
{
    public class ManageCustomerOnboarding
    {
        FrameworkCustomerOnboarding _FrameworkCustomerOnboarding;

        /// <summary>
        /// Description: Method defined to onboard customer
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse OnboardCustomer(OCustomerOboarding.Request _Request)
        {
            _FrameworkCustomerOnboarding = new FrameworkCustomerOnboarding();
            return _FrameworkCustomerOnboarding.OnboardCustomer(_Request);
        }
    }
}
