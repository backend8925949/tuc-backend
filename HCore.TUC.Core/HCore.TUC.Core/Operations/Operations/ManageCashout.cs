//==================================================================================
// FileName: ManageCashout.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;
using HCore.TUC.Core.Object.Operations;

namespace HCore.TUC.Core.Operations.Operations
{
    public class ManageCashout
    {
        FrameworkCashout? _FrameworkCashout;

        /// <summary>
        /// Description:Method to get cashout configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public async Task<OResponse> GetTucCashOutConfiguration(OCashOut.Configuration.Request _Request)
        {
            _FrameworkCashout = new FrameworkCashout();
            return await _FrameworkCashout.GetTucCashOutConfiguration(_Request);
        }
        /// <summary>
        /// Description:Method to intialize cashout.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public async Task<OResponse> TucCashOutInitialize(OCashOut.Initialize.Request _Request)
        {
            _FrameworkCashout = new FrameworkCashout();
            return await _FrameworkCashout.TucCashOutInitialize(_Request);
        }
        /// <summary>
        /// Description:Method to get list of cashouts
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public async Task<OResponse> GetTucCashout(OList.Request _Request)
        {
            _FrameworkCashout = new FrameworkCashout();
            return await _FrameworkCashout.GetTucCashout(_Request);
        }

        public void UpdateCashOutStatus(string _Request)
        {
            _FrameworkCashout = new FrameworkCashout();
            _FrameworkCashout.UpdateCashOutStatus(_Request);
        }
        /// <summary>
        /// Author:Priya Chavadiya
        /// Description:Method to get overview of redeem wallets
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCashoutOverview(OReference _Request)
        {
            _FrameworkCashout = new FrameworkCashout();
            return _FrameworkCashout.GetCashoutOverview(_Request);
        }
        /// <summary>
        /// Author:Priya Chavadiya
        /// Description:Method to get list of reward wallets
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetRewardWallets(OList.Request _Request)
        {
            _FrameworkCashout = new FrameworkCashout();
            return _FrameworkCashout.GetRewardWallets(_Request);
        }
        /// <summary>
        /// Author:Priya Chavadiya
        /// Description:Method to top up from Redeem wallet
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse RedeemFromWallet(OCashOut.Initialize.Request _Request)
        {
            _FrameworkCashout = new FrameworkCashout();
            return _FrameworkCashout.RedeemFromWallet(_Request);
        }
        /// <summary>
        /// Author:Priya Chavadiya
        /// Description:Method to get configuration of all cashouts i.e BNPL,Deals,Loyalty
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public async Task<OResponse> GetTucSummarizeCashOutConfiguration(OCashOut.Configuration.Request _Request)
        {
            _FrameworkCashout = new FrameworkCashout();
            return await _FrameworkCashout.GetTucSummarizeCashOutConfiguration(_Request);
        }
        /// <summary>
        /// Author:Priya Chavadiya
        /// Description:Method to get list of all cashouts i.e BNPL,Deals,Loyalty
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetTucSummarizeCashout(OList.Request _Request)
        {
            _FrameworkCashout = new FrameworkCashout();
            return _FrameworkCashout.GetTucSummarizeCashout(_Request);
        }
    }


    public class ManageCashoutOperation
    {
        FrameworkCashoutOperation _FrameworkCashoutOperation;
        /// <summary>
        /// Description:Method to get list for cashouts in Console Panel.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public async Task<OResponse> GetCashout(OList.Request _Request)
        {
            _FrameworkCashoutOperation = new FrameworkCashoutOperation();
            return await _FrameworkCashoutOperation.GetCashout(_Request);
        }
        /// <summary>
        /// Description:Method to get overview for cashouts in Console Panel.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCashoutOverview(OList.Request _Request)
        {
            _FrameworkCashoutOperation = new FrameworkCashoutOperation();
            return _FrameworkCashoutOperation.GetCashoutOverview(_Request);
        }
        /// <summary>
        /// Description:Method to get detail for cashouts in Console Panel.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public async Task<OResponse> GetCashout(OReference _Request)
        {
            _FrameworkCashoutOperation = new FrameworkCashoutOperation();
            return await _FrameworkCashoutOperation.GetCashout(_Request);
        }

        /// <summary>
        /// Description:Method to update status for cashout requests from Console Panel.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public async Task<OResponse> UpdateCashoutStatus(OReference _Request)
        {
            _FrameworkCashoutOperation = new FrameworkCashoutOperation();
            return await _FrameworkCashoutOperation.UpdateCashoutStatus(_Request);
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description:Method to get pending cashout counts.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetPendingCashoutCounts(OList.Request _Request)
        {
            _FrameworkCashoutOperation = new FrameworkCashoutOperation();
            return _FrameworkCashoutOperation.GetPendingCashoutCounts(_Request);
        }

        public async Task<OResponse> GetCashoutsOverview(OReference _Request)
        {
            _FrameworkCashoutOperation = new FrameworkCashoutOperation();
            return await _FrameworkCashoutOperation.GetCashoutsOverview(_Request);
        }
    }
}
