//==================================================================================
// FileName: ManageChangeLog.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
namespace HCore.TUC.Core.Operations.Operations
{
    using HCore.Helper;
    using HCore.TUC.Core.Framework.Operations;
    using HCore.TUC.Core.Object.Operations;

    public class ManageChangeLog
    {
        FrameworkChangeLog _FrameworkChangeLog;

        /// <summary>
        /// Description: Method defined to save change log
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveChangeLog(OCoreChangeLog.Save.Request _Request)
        {
            _FrameworkChangeLog = new FrameworkChangeLog();
            return _FrameworkChangeLog.SaveChangeLog(_Request);
        }
        /// <summary>
        /// Description: Method defined to update change log
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateChangeLog(OCoreChangeLog.Update _Request)
        {
            _FrameworkChangeLog = new FrameworkChangeLog();
            return _FrameworkChangeLog.UpdateChangeLog(_Request);
        }
        /// <summary>
        /// Description: Method defined to delete change log
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteChangeLog(OReference _Request)
        {
            _FrameworkChangeLog = new FrameworkChangeLog();
            return _FrameworkChangeLog.DeleteChangeLog(_Request);
        }
        /// <summary>
        /// Description: Method defined to get change log details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetChangeLog(OReference _Request)
        {
            _FrameworkChangeLog = new FrameworkChangeLog();
            return _FrameworkChangeLog.GetChangeLog(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of change logs
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetChangeLogs(OList.Request _Request)
        {
            _FrameworkChangeLog = new FrameworkChangeLog();
            return _FrameworkChangeLog.GetChangeLogs(_Request);
        }
    }
}
