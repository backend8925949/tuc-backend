//==================================================================================
// FileName: ManageDealCategory.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;
using HCore.TUC.Core.Object.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Core.Operations.Operations
{
    public class ManageDealCategory
    {
        FrameworkDealCategory _FrameworkDealCategory;

        /// <summary>
        /// Description: Method defined to save deal category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveDealCategory(ODealCategory.Request _Request)
        {
            _FrameworkDealCategory = new FrameworkDealCategory();
            return _FrameworkDealCategory.SaveDealCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to update deal category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateDealCategory(ODealCategory.Request _Request)
        {
            _FrameworkDealCategory = new FrameworkDealCategory();
            return _FrameworkDealCategory.UpdateDealCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to delete deal category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteDealCategory(ODealCategory.Request _Request)
        {
            _FrameworkDealCategory = new FrameworkDealCategory();
            return _FrameworkDealCategory.DeleteDealCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to get details of deal category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetDealCategory(ODealCategory.Request _Request)
        {
            _FrameworkDealCategory = new FrameworkDealCategory();
            return _FrameworkDealCategory.GetDealCategory(_Request);
        }
        /// <summary>
        /// Description: Method to get list of deal categories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetDealCategoryList(OList.Request _Request)
        {
            _FrameworkDealCategory = new FrameworkDealCategory();
            return _FrameworkDealCategory.GetDealCategoryList(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of root categories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetRootCategories(OList.Request _Request)
        {
            _FrameworkDealCategory = new FrameworkDealCategory();
            return _FrameworkDealCategory.GetRootCategories(_Request);
        }
    }
}
