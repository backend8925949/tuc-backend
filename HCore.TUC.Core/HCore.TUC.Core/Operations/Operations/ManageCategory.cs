//==================================================================================
// FileName: ManageCategory.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;
using HCore.TUC.Core.Object.Operations;
namespace HCore.TUC.Core.Operations.Operations
{
    public class ManageCategory
    {
        FrameworkCategory _FrameworkCategory;

        /// <summary>
        /// Description: Method defined to save category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveCategory(OCategory.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.SaveCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to update category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateCategory(OCategory.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.UpdateCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to delete category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteCategory(OCategory.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.DeleteCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to get category details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCategory(OCategory.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.GetCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of all categories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetAllCategory(OList.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.GetAllCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of  categories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCategory(OList.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.GetCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to save subcategory
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveSubCategory(OCategory.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.SaveSubCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to save multiple subcategories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveSubCategories(OCategory.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.SaveSubCategories(_Request);
        }
        /// <summary>
        /// Description: Method defined to update subcategory
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateSubCategory(OCategory.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.UpdateSubCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to delete category
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteSubCategory(OCategory.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.DeleteSubCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to get details of subcategory
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetSubCategory(OCategory.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.GetSubCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of  subcategories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetSubCategory(OList.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.GetSubCategory(_Request);
        }

 
    }
}

