//==================================================================================
// FileName: ManageSubscription.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Akka.Actor;
using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;
using HCore.TUC.Core.Object.Operations;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.Operations
{
    public class ManageSubscription
    {
        FrameworkSubscription _FrameworkSubscription;
        /// <summary>
        /// Description: Method defined to update subscription
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateSubscription(OSubscription.SubscriptionUpdate.Request _Request)
        {
            _FrameworkSubscription = new FrameworkSubscription();
            return _FrameworkSubscription.UpdateSubscription(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of subscriptions
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetSubscription(OList.Request _Request)
        {
            _FrameworkSubscription = new FrameworkSubscription();
            return _FrameworkSubscription.GetSubscription(_Request);
        }

        /// <summary>
        /// Description: Method defined to get list of account subscriptions
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetAccountSubscription(OList.Request _Request)
        {
            _FrameworkSubscription = new FrameworkSubscription();
            return _FrameworkSubscription.GetAccountSubscription(_Request);
        }
        /// <summary>
        /// Description: Method defined to get account subscription overview
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetAccountSubscriptionOverview(OList.Request _Request)
        {
            _FrameworkSubscription = new FrameworkSubscription();
            return _FrameworkSubscription.GetAccountSubscriptionOverview(_Request);
        }

        public void SubscriptionRemainder()
        {
            var _Actor = ActorSystem.Create("ActorSubscriptionRemainder");
            var _ActorNotify = _Actor.ActorOf<ActorSubscriptionRemainder>("ActorSubscriptionRemainder");
            _ActorNotify.Tell("ActorSubscriptionRemainder");
        }

        /// <summary>
        /// Description: Method defined to update  monthly subscription
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateMonthlySubscription(OSubscription.SubscriptionUpdate.MonthlySubscriptionRequest _Request)
        {
            _FrameworkSubscription = new FrameworkSubscription();
            return _FrameworkSubscription.UpdateMonthlySubscription(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list monthly account subscriptions store wise
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetAccountSubscriptionStorewise(OList.Request _Request)
        {
            _FrameworkSubscription = new FrameworkSubscription();
            return _FrameworkSubscription.GetAccountSubscriptionStorewise(_Request);
        }
        /// <summary>
        /// Description: Method to get amount paid to paystack for subscription monthly
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetAmountMonthly(OReference _Request)
        {
            _FrameworkSubscription = new FrameworkSubscription();
            return _FrameworkSubscription.GetAmountMonthly(_Request);
        }
        /// <summary>
        /// Description: Method defined to update subscription store
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateSubscriptionStoreWise(OSubscription.SubscriptionUpdate.MonthlySubscriptionRequest _Request)
        {
            _FrameworkSubscription = new FrameworkSubscription();
            return _FrameworkSubscription.UpdateSubscriptionStoreWise(_Request);
        }

        /// <summary>
        /// Description: Method defined to get list of stores who are not subscribed.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetStoreNotSubscribed(OList.Request _Request)
        {
            _FrameworkSubscription = new FrameworkSubscription();
            return _FrameworkSubscription.GetStoreNotSubscribed(_Request);
        }

        public void SubscriptionAutoRenewal()
        {
            var _Actor = ActorSystem.Create("ActorSubscriptionAutoRenewal");
            var _ActorNotify = _Actor.ActorOf<ActorSubscriptionAutoRenewal>("ActorSubscriptionAutoRenewal");
            _ActorNotify.Tell("ActorSubscriptionAutoRenewal");
        }

        //public bool AddAccountFreeSubscription(long AccountId , long AccountTypeId )
        //{
        //    _FrameworkSubscription = new FrameworkSubscription();
        //    return _FrameworkSubscription.AddAccountFreeSubscription(AccountId, AccountTypeId);
        //}
    }
}

public class ActorSubscriptionRemainder : ReceiveActor
{
    public ActorSubscriptionRemainder()
    {
        Receive<int>(_Request =>
        {
            FrameworkSubscription _FrameworkSubscription = new FrameworkSubscription();
            _FrameworkSubscription.SubscriptionRemainder();
        });
    }
}

public class ActorSubscriptionAutoRenewal : ReceiveActor
{
    public ActorSubscriptionAutoRenewal()
    {
        Receive<int>(_Request =>
        {
            FrameworkSubscription _FrameworkSubscription = new FrameworkSubscription();
            _FrameworkSubscription.SubscriptionAutoRenewal();
        });
    }
}