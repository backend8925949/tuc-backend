﻿//==================================================================================
// FileName: ManageProgram.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-10-2022      | Priya Chavadiya   : Developed API's
//
//==================================================================================
using Akka.Actor;
using HCore.Helper;
using HCore.TUC.Core.Framework.Merchant;
using HCore.TUC.Core.Framework.Operations;
using HCore.TUC.Core.Object.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Core.Operations.Operations
{
    public class ManageProgram
    {
        FrameworkProgram _FrameworkProgram;
        /// <summary>
        /// Description: Method defined to save Program
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveProgram(OProgram.Request _Request)
        {
            _FrameworkProgram = new FrameworkProgram();
            return _FrameworkProgram.SaveProgram(_Request);
        }
        /// <summary>
        /// Description: Method defined to update Program
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateProgram(OProgram.Request _Request)
        {
            _FrameworkProgram = new FrameworkProgram();
            return _FrameworkProgram.UpdateProgram(_Request);
        }
        /// <summary>
        /// Description: Method defined to delete Program
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteProgram(OProgram.Request _Request)
        {
            _FrameworkProgram = new FrameworkProgram();
            return _FrameworkProgram.DeleteProgram(_Request);
        }
        /// <summary>
        /// Description: Method defined to get Program details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetProgram(OProgram.Request _Request)
        {
            _FrameworkProgram = new FrameworkProgram();
            return _FrameworkProgram.GetProgram(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of all programs
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetAllProgram(OList.Request _Request)
        {
            _FrameworkProgram = new FrameworkProgram();
            return _FrameworkProgram.GetPrograms(_Request);
        }

        /// <summary>
        /// Description: Method defined to save program group
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveProgramGroup(OProgram.Request _Request)
        {
            _FrameworkProgram = new FrameworkProgram();
            return _FrameworkProgram.SaveProgramGroup(_Request);
        }
        /// <summary>
        /// Description: Method defined to update program group
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateProgramGroup(OProgram.Request _Request)
        {
            _FrameworkProgram = new FrameworkProgram();
            return _FrameworkProgram.UpdateProgramGroup(_Request);
        }
        
        /// <summary>
        /// Description: Method defined to get program group details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetProgramGroup(OProgram.Request _Request)
        {
            _FrameworkProgram = new FrameworkProgram();
            return _FrameworkProgram.GetProgramGroup(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of all program groups
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetProgramGroups(OList.Request _Request)
        {
            _FrameworkProgram = new FrameworkProgram();
            return _FrameworkProgram.GetProgramGroups(_Request);
        }

        public void UpdateProgramStatus()
        {
            try
            {
                var system = ActorSystem.Create("ActorUpdateProgramStatus");
                var greeter = system.ActorOf<ActorUpdateProgramStatus>("ActorUpdateProgramStatus");
                greeter.Tell("ActorUpdateProgramStatus");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ActorUpdateProgramStatus", _Exception, null);
            }
        }
        /// <summary>
        /// Description: Method defined to delete Program group
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteProgramGroup(OProgram.Request _Request)
        {
            _FrameworkProgram = new FrameworkProgram();
            return _FrameworkProgram.DeleteProgramGroup(_Request);
        }
        /// <summary>
        /// Description: Topups the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse TopupAccount(Object.Merchant.OSubscription.Topup.Request _Request)
        {
            _FrameworkProgram = new FrameworkProgram();
            return _FrameworkProgram.TopupAccount(_Request);
        }
        /// <summary>
        /// Description: Gets the account balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountBalance(Object.Merchant.OSubscription.Balance.Request _Request)
        {
            _FrameworkProgram = new FrameworkProgram();
            return _FrameworkProgram.GetAccountBalance(_Request);
        }
    }
    internal class ActorUpdateProgramStatus : ReceiveActor
    {
        public ActorUpdateProgramStatus()
        {
            Receive<string>(Request =>
            {
                FrameworkProgram FrameworkProgram = new FrameworkProgram();
                FrameworkProgram.UpdateProgramStatus();
            });
        }
    }
}
