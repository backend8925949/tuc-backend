//==================================================================================
// FileName: ManageConfiguration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Operation.Framework;
using HCore.Operation.Object;
using HCore.TUC.Core.Framework.Operations;
using HCore.TUC.Core.Object.Operations;

namespace HCore.TUC.Core.Operations.Operations
{
    public class ManageConfiguration
    {
        FrameworkCoreConfiguration _FrameworkCoreConfiguration;

        /// <summary>
        /// Description: Method defined to save configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveConfiguration(OCoreConfiguration.Save.Request _Request)
        {
            _FrameworkCoreConfiguration = new FrameworkCoreConfiguration();
            return _FrameworkCoreConfiguration.SaveConfiguration(_Request);
        }

        /// <summary>
        /// Description: Method defined to update configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateConfiguration(OCoreConfiguration.Update _Request)
        {
            _FrameworkCoreConfiguration = new FrameworkCoreConfiguration();
            return _FrameworkCoreConfiguration.UpdateConfiguration(_Request);
        }

        /// <summary>
        /// Description: Method defined to delete configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteConfiguration(OReference _Request)
        {
            _FrameworkCoreConfiguration = new FrameworkCoreConfiguration();
            return _FrameworkCoreConfiguration.DeleteConfiguration(_Request);
        }

        /// <summary>
        /// Description: Method defined to get details of configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetConfiguration(OReference _Request)
        {
            _FrameworkCoreConfiguration = new FrameworkCoreConfiguration();
            return _FrameworkCoreConfiguration.GetConfiguration(_Request);
        }

        /// <summary>
        /// Description: Method defined to get list of configurations
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetConfiguration(OList.Request _Request)
        {
            _FrameworkCoreConfiguration = new FrameworkCoreConfiguration();
            return _FrameworkCoreConfiguration.GetConfiguration(_Request);
        }

        /// <summary>
        /// Description: Method defined to get list of history configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetConfigurationHistory(OList.Request _Request)
        {
            _FrameworkCoreConfiguration = new FrameworkCoreConfiguration();
            return _FrameworkCoreConfiguration.GetConfigurationHistory(_Request);
        }

        /// <summary>
        /// Description: Method defined to save account configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveAccountConfiguration(OAccountConfiguration.Save.Request _Request)
        {
            _FrameworkCoreConfiguration = new FrameworkCoreConfiguration();
            return _FrameworkCoreConfiguration.SaveAccountConfiguration(_Request);
        }

        /// <summary>
        /// Decription: Method defined to update account configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateAccountConfiguration(OAccountConfiguration.Update _Request)
        {
            _FrameworkCoreConfiguration = new FrameworkCoreConfiguration();
            return _FrameworkCoreConfiguration.UpdateAccountConfiguration(_Request);
        }

        /// <summary>
        /// Description: Method defined to delete account configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteAccountConfiguration(OReference _Request)
        {
            _FrameworkCoreConfiguration = new FrameworkCoreConfiguration();
            return _FrameworkCoreConfiguration.DeleteAccountConfiguration(_Request);
        }

        /// <summary>
        /// Description: Method defined to get details of account configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetAccountConfiguration(OReference _Request)
        {
            _FrameworkCoreConfiguration = new FrameworkCoreConfiguration();
            return _FrameworkCoreConfiguration.GetAccountConfiguration(_Request);
        }

        /// <summary>
        /// Description: Method defined to get list of account configurations
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetAccountConfiguration(OList.Request _Request)
        {
            _FrameworkCoreConfiguration = new FrameworkCoreConfiguration();
            return _FrameworkCoreConfiguration.GetAccountConfiguration(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of history account configuration
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetAccountConfigurationHistory(OList.Request _Request)
        {
            _FrameworkCoreConfiguration = new FrameworkCoreConfiguration();
            return _FrameworkCoreConfiguration.GetAccountConfigurationHistory(_Request);
        }
    }
}
