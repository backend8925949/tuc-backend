//==================================================================================
// FileName: ManageStorage.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;
using HCore.TUC.Core.Object.Operations;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.Operations
{
  public  class ManageStorage
    {
        FrameworkStorage _FrameworkStorage;
        /// <summary>
        /// Description: Method defined to save store images,pdf any attachments
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveStorage(OStorageManager.Request _Request)
        {
            _FrameworkStorage = new FrameworkStorage();
            return _FrameworkStorage.SaveStorage(_Request);
        }
        /// <summary>
        /// Description: Method defined to delete stored images,pdf any attachments
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteStorage(OReference _Request)
        {
            _FrameworkStorage = new FrameworkStorage();
            return _FrameworkStorage.DeleteStorage(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of storage
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetStorage(OList.Request _Request)
        {
            _FrameworkStorage = new FrameworkStorage();
            return _FrameworkStorage.GetStorage(_Request);
        }
    }
}
