//==================================================================================
// FileName: ManageAddress.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;
using HCore.TUC.Core.Object.Operations;

namespace HCore.TUC.Core.Operations.Operations
{
    public class ManageAddress
    {
        FrameworkAddress _FrameworkAddress;
        /// <summary>
        /// Description: Method to save address
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveAddress(OAddressManager.Save.Request _Request)
        {
            _FrameworkAddress = new FrameworkAddress();
            return _FrameworkAddress.SaveAddress(_Request);
        }
        /// <summary>
        /// Description: Method to update address
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateAddress(OAddressManager.Save.Request _Request)
        {
            _FrameworkAddress = new FrameworkAddress();
            return _FrameworkAddress.UpdateAddress(_Request);
        }
        /// <summary>
        /// Description: Method to delete address
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteAddress(OAddressManager.Save.Request _Request)
        {
            _FrameworkAddress = new FrameworkAddress();
            return _FrameworkAddress.DeleteAddress(_Request);
        }
        /// <summary>
        /// Description: Method to get address details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetAddress(OReference _Request)
        {
            _FrameworkAddress = new FrameworkAddress();
            return _FrameworkAddress.GetAddress(_Request);
        }
        /// <summary>
        /// Description: Method to get list of address
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetAddress(OList.Request _Request)
        {
            _FrameworkAddress = new FrameworkAddress();
            return _FrameworkAddress.GetAddress(_Request);
        }
    }
}
