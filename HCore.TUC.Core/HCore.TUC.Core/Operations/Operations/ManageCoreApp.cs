//==================================================================================
// FileName: ManageCoreApp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;
using HCore.TUC.Core.Object.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Core.Operations.Operations
{
    public class ManageCoreApp
    {
        FrameworkCoreAppManager _FrameworkCoreAppManager;

        /// <summary>
        /// Description: Method defined to save core app
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveCoreApp(OCoreAppManager.Save.Request _Request)
        {
            _FrameworkCoreAppManager = new FrameworkCoreAppManager();
            return _FrameworkCoreAppManager.SaveCoreApp(_Request);
        }
        /// <summary>
        /// Description: Method defined to update core app
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateCoreApp(OCoreAppManager.Update _Request)
        {
            _FrameworkCoreAppManager = new FrameworkCoreAppManager();
            return _FrameworkCoreAppManager.UpdateCoreApp(_Request);
        }
        /// <summary>
        /// Description: Method defined to delete core app
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteCoreApp(OReference _Request)
        {
            _FrameworkCoreAppManager = new FrameworkCoreAppManager();
            return _FrameworkCoreAppManager.DeleteCoreApp(_Request);
        }
        /// <summary>
        /// Description: Method to get details of core app
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCoreApp(OReference _Request)
        {
            _FrameworkCoreAppManager = new FrameworkCoreAppManager();
            return _FrameworkCoreAppManager.GetCoreApp(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of core app
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCoreApps(OList.Request _Request)
        {
            _FrameworkCoreAppManager = new FrameworkCoreAppManager();
            return _FrameworkCoreAppManager.GetCoreApps(_Request);
        }



        /// <summary>
        /// Description: Method defined to save core app version
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveCoreAppVersion(OCoreAppVersionManager.Save.Request _Request)
        {
            _FrameworkCoreAppManager = new FrameworkCoreAppManager();
            return _FrameworkCoreAppManager.SaveCoreAppVersion(_Request);
        }
        /// <summary>
        /// Description: Method defined to update core app version status
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateCoreAppVersionStatus(OCoreAppVersionManager.UpdateStatus _Request)
        {
            _FrameworkCoreAppManager = new FrameworkCoreAppManager();
            return _FrameworkCoreAppManager.UpdateCoreAppVersionStatus(_Request);
        }
        /// <summary>
        /// Description: Method defined to delete core app version
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteCoreAppVersion(OReference _Request)
        {
            _FrameworkCoreAppManager = new FrameworkCoreAppManager();
            return _FrameworkCoreAppManager.DeleteCoreAppVersion(_Request);
        }
        /// <summary>
        /// Description: Method defined to get details of core app version
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCoreAppVersion(OReference _Request)
        {
            _FrameworkCoreAppManager = new FrameworkCoreAppManager();
            return _FrameworkCoreAppManager.GetCoreAppVersion(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of core app version
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCoreAppVersions(OList.Request _Request)
        {
            _FrameworkCoreAppManager = new FrameworkCoreAppManager();
            return _FrameworkCoreAppManager.GetCoreAppVersions(_Request);
        }
    }
}
