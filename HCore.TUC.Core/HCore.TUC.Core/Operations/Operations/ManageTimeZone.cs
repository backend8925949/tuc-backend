//==================================================================================
// FileName: ManageTimeZone.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;

namespace HCore.TUC.Core.Operations.Operations
{
	public class ManageTimeZone
    {
        FrameworkTimeZone _FrameworkTimeZone;

        /// <summary>
        /// Description: Method defined to save timezone
        /// </summary>
        /// <returns></returns>
        public OResponse SaveTimeZones()
        {
            _FrameworkTimeZone = new FrameworkTimeZone();
            return _FrameworkTimeZone.SaveTimeZones();
        }
        /// <summary>
        /// Description: Method defined to get list of timezones
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetTimeZones(OList.Request _Request)
        {
            _FrameworkTimeZone = new FrameworkTimeZone();
            return _FrameworkTimeZone.GetTimeZones(_Request);
        }
    }
}

