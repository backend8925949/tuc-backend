//==================================================================================
// FileName: ManagePromoCode.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;
using HCore.TUC.Core.Object.Operations;

namespace HCore.TUC.Core.Operations.Operations
{
    public class ManagePromoCode
    {
        FrameworkPromoCode _FrameworkPromoCode;

        /// <summary>
        /// Description: Method defined to get promocode conditions
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetPromoCodeConditions(OList.Request _Request)
        {
            _FrameworkPromoCode = new FrameworkPromoCode();
            return _FrameworkPromoCode.GetPromoCodeConditions(_Request);
        }

        /// <summary>
        /// Description: Method to get list of promocode accounts
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetPromoCodeAccounts(OList.Request _Request)
        {
            _FrameworkPromoCode = new FrameworkPromoCode();
            return _FrameworkPromoCode.GetPromoCodeAccounts(_Request);
        }
        /// <summary>
        /// Description: Method to get details of promocode account
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetPromoCodeAccount(OReference _Request)
        {
            _FrameworkPromoCode = new FrameworkPromoCode();
            return _FrameworkPromoCode.GetPromoCodeAccount(_Request);
        }

        /// <summary>
        /// Description: Method defined to save promocode
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SavePromoCode(OPromoCode.PromoCodeOperations.Save _Request)
        {
            _FrameworkPromoCode = new FrameworkPromoCode();
            return _FrameworkPromoCode.SavePromoCode(_Request);
        }
        /// <summary>
        /// Description: Method defined to update promocode
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdatePromoCode(OPromoCode.PromoCodeOperations.Update _Request)
        {
            _FrameworkPromoCode = new FrameworkPromoCode();
            return _FrameworkPromoCode.UpdatePromoCode(_Request);
        }
        /// <summary>
        /// Description: Method defined to delete promocode
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeletePromoCode(OReference _Request)
        {
            _FrameworkPromoCode = new FrameworkPromoCode();
            return _FrameworkPromoCode.DeletePromoCode(_Request);
        }
        /// <summary>
        /// Description: Method defined to get promocode details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetPromoCode(OReference _Request)
        {
            _FrameworkPromoCode = new FrameworkPromoCode();
            return _FrameworkPromoCode.GetPromoCode(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of promocodes
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetPromoCodes(OList.Request _Request)
        {
            _FrameworkPromoCode = new FrameworkPromoCode();
            return _FrameworkPromoCode.GetPromoCodes(_Request);
        }
        /// <summary>
        /// Description: Method defined to update promocode limit
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdatePromoCodeLimit(OPromoCode.PromoCodeOperations.Update _Request)
        {
            _FrameworkPromoCode = new FrameworkPromoCode();
            return _FrameworkPromoCode.UpdatePromoCodeLimit(_Request);
        }
        /// <summary>
        /// Description: Method defined to update promocode schedule
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdatePromoCodeShedule(OPromoCode.PromoCodeOperations.Update _Request)
        {
            _FrameworkPromoCode = new FrameworkPromoCode();
            return _FrameworkPromoCode.UpdatePromoCodeShedule(_Request);
        }
        /// <summary>
        /// Description: Method defined to get  promocode overview
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetPromocodeOverview(OList.Request _Request)
        {
            _FrameworkPromoCode = new FrameworkPromoCode();
            return _FrameworkPromoCode.GetPromocodeOverview(_Request);
        }
        /// <summary>
        /// Description: Method defined to get promocode usage overview
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetPromocodeUsageOverview(OReference _Request)
        {
            _FrameworkPromoCode = new FrameworkPromoCode();
            return _FrameworkPromoCode.GetPromocodeUsageOverview(_Request);
        }

        /// <summary>
        /// Description: Method defined to get list of  customers by referral
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCustomersByReferrals(OList.Request _Request)
        {
            _FrameworkPromoCode = new FrameworkPromoCode();
            return _FrameworkPromoCode.GetCustomersByReferrals(_Request);
        }
    }
}
