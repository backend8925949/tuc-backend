//==================================================================================
// FileName: ManageOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Akka.Actor;
using HCore.Helper;
using HCore.TUC.Core.Core;
using HCore.TUC.Core.Framework.Operations;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.Operations
{
    public class ManageOperations
    {
        FrameworkOperations _FrameworkOperations;
        public void AddTestTransaction()
        {
            var _Actor = ActorSystem.Create("ActorTestTransaction");
            var _ActorNotify = _Actor.ActorOf<ActorTestTransaction>("ActorTestTransaction");
            _ActorNotify.Tell("ActorTestTransaction");
        }
        public void AddManagementTestTransaction()
        {
            var _Actor = ActorSystem.Create("ActorManagmentTestTransaction");
            var _ActorNotify = _Actor.ActorOf<ActorManagmentTestTransaction>("ActorManagmentTestTransaction");
            _ActorNotify.Tell("ActorManagmentTestTransaction");
        }
        //public void ProcessSales()
        //{
        //    var _Actor = ActorSystem.Create("ActorProcessSale");
        //    var _ActorNotify = _Actor.ActorOf<ActorProcessSale>("ActorProcessSale");
        //    _ActorNotify.Tell("ACTOR");
        //}

        //public void ProcessLoyalty()
        //{
        //    var _Actor = ActorSystem.Create("ActorProcessLoyalty");
        //    var _ActorNotify = _Actor.ActorOf<ActorProcessLoyalty>("ActorProcessLoyalty");
        //    _ActorNotify.Tell("ACTOR");
        //}
        //public void ProcessLoyaltyMerchantCustomer()
        //{
        //    var _Actor = ActorSystem.Create("ActorProcessLoyaltyMerchantCustomer");
        //    var _ActorNotify = _Actor.ActorOf<ActorProcessLoyaltyMerchantCustomer>("ActorProcessLoyaltyMerchantCustomer");
        //    _ActorNotify.Tell("ACTOR");
        //}
        /// <summary>
        /// Description: Method defined to get list of categories
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCategories(OList.Request _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.GetCategories(_Request);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public OResponse GetCategories( )
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.GetCategories();
        }
        /// <summary>
        /// Description: Method defined to get list of city areas
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCityArea(OList.Request _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.GetCityArea(_Request);
        }

        /// <summary>
        /// Description: Method defined to get list of downloaded files
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetDownload(OList.Request _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.GetDownload(_Request);
        }
    }
}
