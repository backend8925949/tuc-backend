//==================================================================================
// FileName: ManageBank.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;
using HCore.TUC.Core.Object.Operations;

namespace HCore.TUC.Core.Operations.Operations
{
    public class ManageBank
    {
        FrameworkBank? _FrameworkBank;

        /// <summary>
        /// Description: Method defined to get list of bank codes
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public async Task<OResponse> GetBankCode(OList.Request _Request)
        {
            _FrameworkBank = new FrameworkBank();
            return await _FrameworkBank.GetBankCode(_Request);
        }

        /// <summary>
        /// Description: Method defined to get list of bank accounts
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public async Task<OResponse> GetBankAccount(OList.Request _Request)
        {
            _FrameworkBank = new FrameworkBank();
            return await _FrameworkBank.GetBankAccount(_Request);
        }

        /// <summary>
        /// Description: Method defined to delete bank account
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public async Task<OResponse> DeleteBankAccount(OBank.Request _Request)
        {
            _FrameworkBank = new FrameworkBank();
            return await _FrameworkBank.DeleteBankAccount(_Request);
        }

        /// <summary>
        /// Description: Method defined to save bank account
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public async Task<OResponse> SaveBankAccount(OBank.Request _Request)
        {
            _FrameworkBank = new FrameworkBank();
            return await _FrameworkBank.SaveBankAccount(_Request);
        }

        /// <summary>
        /// Description: Method defined to update user's bank account details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public async Task<OResponse> UpdateBankAccount(OBank.Request _Request)
        {
            _FrameworkBank = new FrameworkBank();
            return await _FrameworkBank.UpdateBankAccount(_Request);
        }

        /// <summary>
        /// Description: Method defined to get details of bank account
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public async Task<OResponse> GetBankAccount(OReference _Request)
        {
            _FrameworkBank = new FrameworkBank();
            return await _FrameworkBank.GetBankAccount(_Request);
        }

        /// <summary>
        /// Description: Method defined to save bank account
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public async Task<OResponse> QuickSaveBankAccount(OBank.QuickSave _Request)
        {
            _FrameworkBank = new FrameworkBank();
            return await _FrameworkBank.QuickSaveBankAccount(_Request);
        }
    }
}
