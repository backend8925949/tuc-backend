//==================================================================================
// FileName: ManageStateManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;
using HCore.TUC.Core.Object.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Core.Operations.Operations
{
    public class ManageStateManager
    {
        FrameworkStateManager _FrameworkStateManager;

        /// <summary>
        /// Description: Method defined to save state
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveState(OStateManager.Save.Request _Request)
        {
            _FrameworkStateManager = new FrameworkStateManager();
            return _FrameworkStateManager.SaveState(_Request);
        }
        /// <summary>
        /// Description: Method defined to update state
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateState(OStateManager.Update _Request)
        {
            _FrameworkStateManager = new FrameworkStateManager();
            return _FrameworkStateManager.UpdateState(_Request);
        }
        /// <summary>
        /// Description: Method defined to delete state
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteState(OReference _Request)
        {
            _FrameworkStateManager = new FrameworkStateManager();
            return _FrameworkStateManager.DeleteState(_Request);
        }
        /// <summary>
        /// Description: Method defined to get state details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetState(OReference _Request)
        {
            _FrameworkStateManager = new FrameworkStateManager();
            return _FrameworkStateManager.GetState(_Request);
        }

        /// <summary>
        /// Description: Method defined to get list of states
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetState(OList.Request _Request)
        {
            _FrameworkStateManager = new FrameworkStateManager();
            return _FrameworkStateManager.GetState(_Request);
        }


        /// <summary>
        /// Description: Method to get list of states
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetStates(OList.Request _Request)
        {
            _FrameworkStateManager = new FrameworkStateManager();
            return _FrameworkStateManager.GetStates(_Request);
        }
    }
}
