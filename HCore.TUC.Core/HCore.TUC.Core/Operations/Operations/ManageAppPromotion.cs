//==================================================================================
// FileName: ManageAppPromotion.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;
using HCore.TUC.Core.Object.Operations;

namespace HCore.TUC.Core.Operations.Operations
{
    public class ManageAppPromotion
    {
        FrameworkAppPromotion _FrameworkAppPromotion;

        /// <summary>
        /// Description: Method defined to save app promotion
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveAppPromotion(OAppPromotion.Save.Request _Request)
        {
            _FrameworkAppPromotion = new FrameworkAppPromotion();
            return _FrameworkAppPromotion.SaveAppPromotion(_Request);
        }
        /// <summary>
        /// Description: Method defined to update app promotion
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse UpdateAppPromotion(OAppPromotion.Update _Request)
        {
            _FrameworkAppPromotion = new FrameworkAppPromotion();
            return _FrameworkAppPromotion.UpdateAppPromotion(_Request);
        }
        /// <summary>
        /// Description: Method defined to delete app promotion
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse DeleteAppPromotion(OReference _Request)
        {
            _FrameworkAppPromotion = new FrameworkAppPromotion();
            return _FrameworkAppPromotion.DeleteAppPromotion(_Request);
        }
        /// <summary>
        /// Description: Method defined to get details of app promotion
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetAppPromotion(OReference _Request)
        {
            _FrameworkAppPromotion = new FrameworkAppPromotion();
            return _FrameworkAppPromotion.GetAppPromotion(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of app promotion
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetAppPromotions(OList.Request _Request)
        {
            _FrameworkAppPromotion = new FrameworkAppPromotion();
            return _FrameworkAppPromotion.GetAppPromotions(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of app sliders
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetAppSlider(OReference _Request)
        {
            _FrameworkAppPromotion = new FrameworkAppPromotion();
            return _FrameworkAppPromotion.GetAppSlider(_Request);
        }
        /// <summary>
        /// Description: Method defined to remove app promotion image
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse RemoveAppPromotionImage(OAppPromotion.Image _Request)
        {
            _FrameworkAppPromotion = new FrameworkAppPromotion();
            return _FrameworkAppPromotion.RemoveAppPromotionImage(_Request);
        }
    }
}
