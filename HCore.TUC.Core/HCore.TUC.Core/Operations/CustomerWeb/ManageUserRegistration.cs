﻿using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.CustomerWeb;
using HCore.TUC.Core.Object.CustomerWeb;

namespace HCore.TUC.Core.Operations.CustomerWeb
{
    public class ManageUserRegistration
    {
        FrameworkUserRegistration _FrameworkUserRegistration;

        public OResponse UserLogIn(OUserRegistration.UserLogInRequest _Request)
        {
            _FrameworkUserRegistration = new FrameworkUserRegistration();
            return _FrameworkUserRegistration.UserLogIn(_Request);
        }

        public OResponse UserRegistration(OUserRegistration.UserRegistration _Request)
        {
            _FrameworkUserRegistration = new FrameworkUserRegistration();
            return _FrameworkUserRegistration.UserRegistration(_Request);
        }

        public OResponse UserVerification(OUserRegistration.UserVerificationRequest _Request)
        {
            _FrameworkUserRegistration = new FrameworkUserRegistration();
            return _FrameworkUserRegistration.UserVerification(_Request);
        }

        public OResponse UpdateAccountPin(OUserRegistration.ChangeUserAccountPinRequest _Request)
        {
            _FrameworkUserRegistration = new FrameworkUserRegistration();
            return _FrameworkUserRegistration.UpdateAccountPin(_Request);
        }

        public OResponse ResendUserverificationPin(OUserRegistration.ResendUserVerificationRequest _Request)
        {
            _FrameworkUserRegistration = new FrameworkUserRegistration();
            return _FrameworkUserRegistration.ResendUserverificationPin(_Request);
        }

        public OResponse ResetUserAccountPin(OUserRegistration.ResetUserAccountPinRequest _Request)
        {
            _FrameworkUserRegistration = new FrameworkUserRegistration();
            return _FrameworkUserRegistration.ResetUserAccountPin(_Request);
        }

        public OResponse ResetAccountPin(OUserRegistration.UpdateAccountPinRequest _Request)
        {
            _FrameworkUserRegistration = new FrameworkUserRegistration();
            return _FrameworkUserRegistration.ResetAccountPin(_Request);
        }
    }
}

