//==================================================================================
// FileName: ManageLoyalty.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Akka.Actor;
using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Core.Operations.CustomerWeb
{
    public class ManageLoyalty
    {
        //FrameworkLoyalty _FrameworkLoyalty;

        ///// <summary>
        ///// Author: Priya Chavadiya
        ///// Description:Method to Send Monthly E-Statement to the customers of Customer's App.
        ///// </summary>
        //public void SendMonthlyCustomerEstatement()
        //{
        //    try
        //    {
        //        var system = ActorSystem.Create("ActorSendMonthlyCustomerEstatement");
        //        var greeter = system.ActorOf<ActorSendMonthlyCustomerEstatement>("ActorSendMonthlyCustomerEstatement");
        //        greeter.Tell("sendmonthlycustomerestatement");
        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException("SendMonthlyCustomerEstatement", _Exception, null);
        //    }
        //}
        ///// <summary>
        ///// Author: Priya Chavadiya
        ///// Description:Method to Send Monthly E-Statement to the customers of WakaNow.
        ///// </summary>
        //public void WakaNowMonthlyEStatement()
        //{
        //    try
        //    {
        //        var system = ActorSystem.Create("ActorMonthlyEStatement");
        //        var greeter = system.ActorOf<ActorMonthlyEStatement>("ActorMonthlyEStatement");
        //        greeter.Tell("monthlyestatement");
        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException("MonthlyEStatement", _Exception, null);
        //    }
        //}
    }
    //internal class ActorSendMonthlyCustomerEstatement : ReceiveActor
    //{
    //    public ActorSendMonthlyCustomerEstatement()
    //    {
    //        Receive<string>(Request =>
    //        {
    //            FrameworkLoyalty FrameworkLoyalty = new FrameworkLoyalty();
    //            FrameworkLoyalty.SendMonthlyCustomerEstatement();
    //        });
    //    }
    //}
    //internal class ActorMonthlyEStatement : ReceiveActor
    //{
    //    public ActorMonthlyEStatement()
    //    {
    //        Receive<string>(Request =>
    //        {
    //            FrameworkLoyalty FrameworkLoyalty = new FrameworkLoyalty();
    //            FrameworkLoyalty.WakaNowMonthlyEStatement();
    //        });
    //    }
    //}
}
