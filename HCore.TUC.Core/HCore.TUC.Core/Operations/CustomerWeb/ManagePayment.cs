//==================================================================================
// FileName: ManagePayment.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.CustomerWeb;
using HCore.TUC.Core.Object.CustomerWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Core.Operations.CustomerWeb
{
    public class ManagePayment
    {
        FrameworkBuyPoint _FrameworkBuyPoint;
        /// <summary>
        /// Description: Buy point payment initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse BuyPointInitialize(OBuyPoint.Initialize.Request _Request)
        {
            _FrameworkBuyPoint = new FrameworkBuyPoint();
            return _FrameworkBuyPoint.BuyPointInitialize(_Request);
        }
        /// <summary>
        /// Description: Verifies the payment.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse BuyPointVerify(OBuyPoint.Verify.Request _Request)
        {
            _FrameworkBuyPoint = new FrameworkBuyPoint();
            return _FrameworkBuyPoint.BuyPointVerify(_Request);
        }
        /// <summary>
        /// Description: Cancels the payment.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse BuyPointCancel(OBuyPoint.Cancel.Request _Request)
        {
            _FrameworkBuyPoint = new FrameworkBuyPoint();
            return _FrameworkBuyPoint.BuyPointCancel(_Request);
        }
        /// <summary>
        /// Description: Gets the paymentcharge.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse BuyPointCharge(OBuyPoint.Charge.Request _Request)
        {
            _FrameworkBuyPoint = new FrameworkBuyPoint();
            return _FrameworkBuyPoint.BuyPointCharge(_Request);
        }

        /// <summary>
        /// Description: Buypoint confirm paystack.
        /// </summary>
        /// <param name="_Request">The request.</param>
        public async Task BuyPointConfirmFlutterwave(string _Request)
        {
            _FrameworkBuyPoint = new FrameworkBuyPoint();
            await _FrameworkBuyPoint.BuyPointConfirmFlutterwave(_Request);
        }
    }
}
