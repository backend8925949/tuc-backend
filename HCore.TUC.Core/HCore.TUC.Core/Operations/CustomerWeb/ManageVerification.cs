//==================================================================================
// FileName: ManageVerification.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.CustomerWeb;
using HCore.TUC.Core.Object.CustomerWeb;

namespace HCore.TUC.Core.Operations.CustomerWeb
{
    public class ManageVerification
    {
        FrameworkVerification _FrameworkVerification;
        /// <summary>
        /// Description: Requests the otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RequestOtp(OCoreVerificationManager.Request _Request)
        {
            _FrameworkVerification = new FrameworkVerification();
            return _FrameworkVerification.RequestOtp(_Request);
        }
        /// <summary>
        /// Description: Verifies the otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse VerifyOtp(OCoreVerificationManager.RequestVerify _Request)
        {
            _FrameworkVerification = new FrameworkVerification();
            return _FrameworkVerification.VerifyOtp(_Request);
        }

        /// <summary>
        /// Description: Requests the signup otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RequestSignupOtp(OCoreVerificationManager.Request _Request)
        {
            _FrameworkVerification = new FrameworkVerification();
            return _FrameworkVerification.RequestSignupOtp(_Request);
        }
        /// <summary>
        /// Description: Verifies the signup otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse VerifySignupOtp(OCoreVerificationManager.RequestVerify _Request)
        {
            _FrameworkVerification = new FrameworkVerification();
            return _FrameworkVerification.VerifySignupOtp(_Request);
        }

        /// <summary>
        /// Description: Signups the user.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SignupUser(OCoreVerificationManager.RequestSignupUser _Request)
        {
            _FrameworkVerification = new FrameworkVerification();
            return _FrameworkVerification.SignupUser(_Request);
        }

        /// <summary>
        /// Description: Signins the user.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SigninUser(OCoreVerificationManager.RequestSignIn _Request)
        {
            _FrameworkVerification = new FrameworkVerification();
            return _FrameworkVerification.SigninUser(_Request);
        }

        /// <summary>
        /// Description: Guest checkout.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GuestCheckout(OCoreVerificationManager.RequestGuestCheckout _Request)
        {
            _FrameworkVerification = new FrameworkVerification();
            return _FrameworkVerification.GuestCheckout(_Request);
        }
    }
}
