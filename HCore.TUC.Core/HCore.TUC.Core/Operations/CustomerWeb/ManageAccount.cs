//==================================================================================
// FileName: ManageAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.CustomerWeb;
using HCore.TUC.Core.Object.CustomerWeb;

namespace HCore.TUC.Core.Operations.CustomerWeb
{
    public class ManageAccount
    {
        FrameworkAccount _FrameworkAccount;
        /// <summary>
        /// Description: Updates the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateAccount(OAccount.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.UpdateAccount(_Request);
        }

        /// <summary>
        /// Description: Updates the referal code of a user.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateReferralId(OAccount.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.UpdateReferralId(_Request);
        }

        /// <summary>
        /// Description: Sets App Logo.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SetAppLogo(HCore.TUC.Core.Object.Console.OAdminUser.AuthUpdate.LogoRequest _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.SetAppLogo(_Request);
        }

        /// <summary>
        /// Description: Gets App Logo.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAppLogo(HCore.TUC.Core.Object.Console.OAdminUser.AuthUpdate.LogoRequest _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.GetAppLogo(_Request);
        }

        /// <summary>
        /// Description: Updates the referal code of a user.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetReferralAmount(OAccount.ReferralAmountRequest _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.GetReferralAmount(_Request);
        }

        /// <summary>
        /// Description: Sets the referal amount.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SetReferralAmount(OAccount.ReferralAmountRequest _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.SetReferralAmount(_Request);
        }

        /// <summary>
        /// Description: Gets Referral Bonus History For User.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetBonusHistory(OAccount.ReferralAmountRequest _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.GetBonusHistory(_Request);
        }

        /// <summary>
        /// Description: Gets the account details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccount(OAccount.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.GetAccount(_Request);
        }
        /// <summary>
        /// Description: Gets the account balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountBalance(OAccount.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.GetAccountBalance(_Request);
        }
        /// <summary>
        /// Description: Gets the account balance open.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountBalanceOpen(OAccount.OCustReq.Req _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.GetAccountBalanceOpen(_Request);
        }

        /// <summary>
        /// Description: Validates the pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ValidatePin(OAccount.ValidatePin.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.ValidatePin(_Request);
        }
        /// <summary>
        /// Description: Registers the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RegisterAccount(OAccount.OCustRegReq.Req _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.RegisterAccount(_Request);
        }

    }
}
