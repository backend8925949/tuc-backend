//==================================================================================
// FileName: ManageDeal.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.CustomerWeb;
using HCore.TUC.Core.Object.CustomerWeb;


namespace HCore.TUC.Core.Operations.CustomerWeb
{
    public class ManageDeal
    {
        FrameworkDeal _FrameworkDeal;

        /// <summary>
        /// Description: Saves the deal view.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveDealView(ODeal.View _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.SaveDealView(_Request);
        }
        /// <summary>
        /// Description: Buys the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse BuyDeal(ODeal.Purchase.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.BuyDeal(_Request);
        }
        /// <summary>
        /// Description: Gets the deal code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDealCode(ODeal.Purchase.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.GetDealCode(_Request);
        }

        /// <summary>
        /// Description: Gets the purchase history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPurchaseHistory(OList.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.GetPurchaseHistory(_Request);
        }
        /// <summary>
        /// Description: Gets the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDeal(ODeal.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.GetDeal(_Request);
        }
        /// <summary>
        /// Description: Gets the deals.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDeals(ODeal.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.GetDeals(_Request);
        }

        /// <summary>
        /// Description: Shares the deal code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ShareDealCode(ODeal.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.ShareDealCode(_Request);
        }
    }
}
