//==================================================================================
// FileName: ManageTransactions.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Ptsp;

namespace HCore.TUC.Core.Operations.Ptsp
{
    public class ManageTransactions
    {
        FrameworkTransaction _FrameworkTransaction;
        /// <summary>
        /// Description: Gets the commission history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCommissionHistory(OList.Request _Request)
        {
            _FrameworkTransaction = new FrameworkTransaction();
            return _FrameworkTransaction.GetCommissionHistory(_Request);
        }
        /// <summary>
        /// Description: Gets the commission history overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCommissionHistoryOverview(OList.Request _Request)
        {
            _FrameworkTransaction = new FrameworkTransaction();
            return _FrameworkTransaction.GetCommissionHistoryOverview(_Request);
        }
    }
}
