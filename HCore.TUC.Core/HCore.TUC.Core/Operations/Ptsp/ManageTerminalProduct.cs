//==================================================================================
// FileName: ManageTerminalProduct.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Ptsp;
using HCore.TUC.Core.Object.Ptsp;

namespace HCore.TUC.Core.Operations.Ptsp
{
    public class ManageTerminalProduct
    {
        FrameworkTerminalProduct _FrameworkTerminalProduct;
        /// <summary>
        /// Description: Saves the terminal product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveTerminalProduct(OTerminalProduct.Request _Request)
        {
            _FrameworkTerminalProduct = new FrameworkTerminalProduct();
            return _FrameworkTerminalProduct.SaveTerminalProduct(_Request);
        }
        /// <summary>
        /// Description: Updates the terminal product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateTerminalProduct(OTerminalProduct.Request _Request)
        {
            _FrameworkTerminalProduct = new FrameworkTerminalProduct();
            return _FrameworkTerminalProduct.UpdateTerminalProduct(_Request);
        }
        /// <summary>
        /// Description: Deletes the terminal product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteTerminalProduct(OTerminalProduct.Request _Request)
        {
            _FrameworkTerminalProduct = new FrameworkTerminalProduct();
            return _FrameworkTerminalProduct.DeleteTerminalProduct(_Request);
        }
        /// <summary>
        /// Description: Gets the terminal product details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTerminalProduct(OTerminalProduct.Request _Request)
        {
            _FrameworkTerminalProduct = new FrameworkTerminalProduct();
            return _FrameworkTerminalProduct.GetTerminalProduct(_Request);
        }
        /// <summary>
        /// Description: Gets the terminal product list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTerminalProduct(OList.Request _Request)
        {
            _FrameworkTerminalProduct = new FrameworkTerminalProduct();
            return _FrameworkTerminalProduct.GetTerminalProduct(_Request);
        }
    }
}
