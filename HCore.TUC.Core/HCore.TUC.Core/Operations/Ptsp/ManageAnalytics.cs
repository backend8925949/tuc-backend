//==================================================================================
// FileName: ManageAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Ptsp;
using HCore.TUC.Core.Object.Ptsp;

namespace HCore.TUC.Core.Operations.Ptsp
{
    public class ManageAnalytics
    {
        FrameworkAnalytics _FrameworkAnalytics;
        /// <summary>
        /// Description: Gets the terminal activity history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTerminalActivityHistory(OAnalytics.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetTerminalActivityHistory(_Request);
        }
        /// <summary>
        /// Description: Gets the account overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountOverview(OAnalytics.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetAccountOverview(_Request);
        }
    }
}
