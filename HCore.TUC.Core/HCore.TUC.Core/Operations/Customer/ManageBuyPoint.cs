//==================================================================================
// FileName: ManageBuyPoint.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Customer;
using HCore.TUC.Core.Object.Customer;

namespace HCore.TUC.Core.Operations.Customer
{
    public class ManageBuyPoint
    {
        FrameworkBuyPoint _FrameworkBuyPoint;
        /// <summary>
        /// Description: Buy point initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse BuyPointInitialize(OBuyPoint.Initialize.Request _Request)
        {
            _FrameworkBuyPoint = new FrameworkBuyPoint();
            return _FrameworkBuyPoint.BuyPointInitialize(_Request);
        }
        /// <summary>
        /// Description: Buy point verify.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse BuyPointVerify(OBuyPoint.Verify.Request _Request)
        {
            _FrameworkBuyPoint = new FrameworkBuyPoint();
            return _FrameworkBuyPoint.BuyPointVerify(_Request);
        }
        /// <summary>
        /// Description: Buy point cancel.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse BuyPointCancel(OBuyPoint.Cancel.Request _Request)
        {
            _FrameworkBuyPoint = new FrameworkBuyPoint();
            return _FrameworkBuyPoint.BuyPointCancel(_Request);
        }
        /// <summary>
        /// Description: Buy point charge.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse BuyPointCharge(OBuyPoint.Charge.Request _Request)
        {
            _FrameworkBuyPoint = new FrameworkBuyPoint();
            return _FrameworkBuyPoint.BuyPointCharge(_Request);
        }


        /// <summary>
        /// Description: Gets the account point purchase history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountPointPurchaseHistory(OList.Request _Request)
        {
            _FrameworkBuyPoint = new FrameworkBuyPoint();
            return _FrameworkBuyPoint.GetAccountPointPurchaseHistory(_Request);
        }

        /// <summary>
        /// Description: Buypoint confirm paystack.
        /// </summary>
        /// <param name="_Request">The request.</param>
        public void BuyPointConfirmPaystack(string _Request)
        {
            _FrameworkBuyPoint = new FrameworkBuyPoint();
             _FrameworkBuyPoint.BuyPointConfirmPaystack(_Request);
        }
        /// <summary>
        /// Description: Buypoint confirm coral pay.
        /// </summary>
        /// <param name="_Request">The request.</param>
        public void BuyPointConfirmCoralPay(string _Request)
        {
            _FrameworkBuyPoint = new FrameworkBuyPoint();
            _FrameworkBuyPoint.BuyPointConfirmCoralPay(_Request);
        }
    }
}
