//==================================================================================
// FileName: ManageCard.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Customer;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.Customer
{
    public class ManageCard
    {
        FrameworkCard _FrameworkCard;
        /// <summary>
        /// Description: Gets the card.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCard(OList.Request _Request)
        {
            _FrameworkCard = new FrameworkCard();
            return _FrameworkCard.GetCard(_Request);
        }

        /// <summary>
        /// Description: Deletes the card.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteCard(OReference _Request)
        {
            _FrameworkCard = new FrameworkCard();
            return _FrameworkCard.DeleteCard(_Request);
        }
    }
}
