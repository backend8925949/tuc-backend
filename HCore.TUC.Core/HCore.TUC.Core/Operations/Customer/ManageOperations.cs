//==================================================================================
// FileName: ManageOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Customer;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Object.Customer;

namespace HCore.TUC.Core.Operations.Customer
{
    public class ManageOperations
    {
        FrameworkOperations _FrameworkOperations;
        /// <summary>
        /// Description: Gets the balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetBalance(OOperation.Balance.Request _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.GetBalance(_Request);
        }
        /// <summary>
        /// Description: Updates the referral code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateReferralCode(OOperation.ReferralCode.Request _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.UpdateReferralCode(_Request);
        }
        
        /// <summary>
        /// Description: Validates the referral bonus.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ValidateReferralBonus(OOperation.ReferralBonus.Request _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.ValidateReferralBonus(_Request);
        }
        /// <summary>
        /// Description: Updates the pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdatePin(OOperation.PinManager.Update.Request _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.UpdatePin(_Request);
        }
        /// <summary>
        /// Description: Resets the pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ResetPin(OOperation.PinManager.Update.Request _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.ResetPin(_Request);
        }

        /// <summary>
        /// Description: Gets App Logo.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAppLogo(OAdminUser.AuthUpdate.LogoRequest _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.GetAppLogo(_Request);
        }

    }
}
