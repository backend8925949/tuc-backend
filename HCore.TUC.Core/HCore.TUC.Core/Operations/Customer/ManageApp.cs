//==================================================================================
// FileName: ManageApp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Customer;
using HCore.TUC.Core.Object.Customer;
namespace HCore.TUC.Core.Operations.Customer
{
    public class ManageApp
    {
        FrameworkApp _FrameworkApp;
        /// <summary>
        /// Description: Gets the application configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAppConfiguration(OApp.Configuration.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetAppConfiguration(_Request);
        }
        /// <summary>
        /// Description: Gets the application slider.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAppSlider(OReference _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetAppSlider(_Request);
        }
    }
}
