//==================================================================================
// FileName: ManageMerchant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.OpenApi;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.OpenApi
{
    public class ManageMerchant
    {
        FrameworkMerchant _FrameworkMerchant;
        /// <summary>
        /// Description: Gets the list of merchant locations.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchant(OList.Request _Request)
        {

            _FrameworkMerchant = new FrameworkMerchant();
            return _FrameworkMerchant.GetMerchant(_Request);
        }
    }
}
