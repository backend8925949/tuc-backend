//==================================================================================
// FileName: ManageCustomer.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Console;
using HCore.TUC.Core.Object.Console;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.Console
{
    public class ManageCustomer
    {
        FrameworkCustomer _FrameworkCustomer;

        /// <summary>
        /// Description: Gets the point purchase history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPointPurchaseHistory(OList.Request _Request)
        {
            _FrameworkCustomer = new FrameworkCustomer();
            return _FrameworkCustomer.GetPointPurchaseHistory(_Request);
        }

        /// <summary>
        /// Description: Gets the point purchase overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPointPurchaseOverview(OReference _Request)
        {
            _FrameworkCustomer = new FrameworkCustomer();
            return _FrameworkCustomer.GetPointPurchaseOverview(_Request);
        }
    }
}
