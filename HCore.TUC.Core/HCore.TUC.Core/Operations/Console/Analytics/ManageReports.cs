﻿using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Console.Analytics;

namespace HCore.TUC.Core.Operations.Console.Analytics
{
    public class ManageReports
    {
        FrameworkReports _FrameworkReports;

        public OResponse GetSalesReport(OList.Request _Request)
        {
            _FrameworkReports = new FrameworkReports();
            return _FrameworkReports.GetSalesReport(_Request);
        }

        public OResponse GetRewardReport(OList.Request _Request)
        {
            _FrameworkReports = new FrameworkReports();
            return _FrameworkReports.GetRewardReport(_Request);
        }

        public OResponse GetRedeemReport(OList.Request _Request)
        {
            _FrameworkReports = new FrameworkReports();
            return _FrameworkReports.GetRedeemReport(_Request);
        }

        public OResponse GetRewardClaimReport(OList.Request _Request)
        {
            _FrameworkReports = new FrameworkReports();
            return _FrameworkReports.GetRewardClaimReport(_Request);
        }
    }
}

