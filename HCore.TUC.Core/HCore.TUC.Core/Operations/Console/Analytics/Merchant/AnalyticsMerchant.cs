//==================================================================================
// FileName: AnalyticsMerchant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Framework.Console.Analytics.Merchant;
using HCore.TUC.Core.Object.Console.Analytics;
using HCore.TUC.Core.Object.Console.Analytics.Merchant;
using System;
using System.Collections.Generic;
using System.Linq;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
namespace HCore.TUC.Core.Operations.Console.Analytics.Merchant
{
    public class AnalyticsMerchant
    {
       
        AnaMerchant _AnaMerchant;

        /// <summary>
        /// Description: Gets the status count.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStatusCount(OAnaMerchant.Request _Request)
        {
            _AnaMerchant = new AnaMerchant();
            return _AnaMerchant.GetStatusCount(_Request);
        }
        /// <summary>
        /// Description: Gets the activity status count.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetActivityStatusCount(OAnaMerchant.Request _Request)
        {
            _AnaMerchant = new AnaMerchant();
            return _AnaMerchant.GetActivityStatusCount(_Request);
        }
        /// <summary>
        /// Description: Gets the top rewards.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTopRewards(OAnaMerchant.Request _Request)
        {
            _AnaMerchant = new AnaMerchant();
            return _AnaMerchant.GetTopRewards(_Request);
        }
        /// <summary>
        /// Description: Gets the top visits.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTopVisits(OAnaMerchant.Request _Request)
        {
            _AnaMerchant = new AnaMerchant();
            return _AnaMerchant.GetTopVisits(_Request);
        }
        /// <summary>
        /// Description: Gets the top sale.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTopSale(OAnaMerchant.Request _Request)
        {
            _AnaMerchant = new AnaMerchant();
            return _AnaMerchant.GetTopSale(_Request);
        }
        /// <summary>
        /// Description: Gets the top category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTopCategory(OAnaMerchant.Request _Request)
        {
            _AnaMerchant = new AnaMerchant();
            return _AnaMerchant.GetTopCategory(_Request);
        }
        /// <summary>
        /// Description: Gets the sale range.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSaleRange(OAnaMerchant.Request _Request)
        {
            _AnaMerchant = new AnaMerchant();
            return _AnaMerchant.GetSaleRange(_Request);
        }
        /// <summary>
        /// Description: Gets the reward range.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRewardRange(OAnaMerchant.Request _Request)
        {
            _AnaMerchant = new AnaMerchant();
            return _AnaMerchant.GetRewardRange(_Request);
        }
    }
}
