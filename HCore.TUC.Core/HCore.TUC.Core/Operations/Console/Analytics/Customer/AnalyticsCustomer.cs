//==================================================================================
// FileName: AnalyticsCustomer.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Framework.Console.Analytics.Customer;
using HCore.TUC.Core.Object.Console.Analytics;
using HCore.TUC.Core.Object.Console.Analytics.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.Operations.Console.Analytics.Customer
{
    public class AnalyticsCustomer
    {
        AnaCustomer _AnaCustomer;
        /// <summary>
        /// Description: Method defined to get status count of customers
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCustomerStatusCount(OAnalytics.Request _Request)
        {
            _AnaCustomer = new AnaCustomer();
            return _AnaCustomer.GetCustomerStatusCount(_Request);
        }
        /// <summary>
        /// Description: Method defined to get payment modes of customers
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCustomersPaymentMode(OAnalytics.Request _Request)
        {
            _AnaCustomer = new AnaCustomer();
            return _AnaCustomer.GetCustomersPaymentMode(_Request);
        }
        /// <summary>
        /// Description: Method defined to get referral count of customers
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCustomerReferrerCount(OAnalytics.Request _Request)
        {
            _AnaCustomer = new AnaCustomer();
            return _AnaCustomer.GetCustomerReferrerCount(_Request);
        }
        /// <summary>
        /// Description: Method defined to get acitvity status count of customers
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCustomerActivityStatusCount(OAnalytics.Request _Request)
        {
            _AnaCustomer = new AnaCustomer();
            return _AnaCustomer.GetCustomerActivityStatusCount(_Request);
        }

        /// <summary>
        /// Description: Method defined to get activity status count of customer app
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCustomerAppActivityStatusCount(OAnalytics.Request _Request)
        {
            _AnaCustomer = new AnaCustomer();
            return _AnaCustomer.GetCustomerAppActivityStatusCount(_Request);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCustomersAppActivityStatusCount(OAnalytics.Request _Request)
        {
            _AnaCustomer = new AnaCustomer();
            return _AnaCustomer.GetCustomersAppActivityStatusCount(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of top senders customers
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCustomersTopSpenders(OAnalytics.Request _Request)
        {
            _AnaCustomer = new AnaCustomer();
            return _AnaCustomer.GetCustomersTopSpenders(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of top category customers
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCustomersTopCategory(OAnalytics.Request _Request)
        {
            _AnaCustomer = new AnaCustomer();
            return _AnaCustomer.GetCustomersTopCategory(_Request);
        }
        /// <summary>
        /// Description: Method defined to get list of top visitors customer
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCustomersTopVisitors(OAnalytics.Request _Request)
        {
            _AnaCustomer = new AnaCustomer();
            return _AnaCustomer.GetCustomersTopVisitors(_Request);
        }
        /// <summary>
        /// Description: Method defined to get  customer gender
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCustomersGender(OAnalytics.Request _Request)
        {
            _AnaCustomer = new AnaCustomer();
            return _AnaCustomer.GetCustomersGender(_Request);
        }
        /// <summary>
        /// Description: Method defined to get  customer age groups
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCustomersAgeGroup(OAnalytics.Request _Request)
        {
            _AnaCustomer = new AnaCustomer();
            return _AnaCustomer.GetCustomersAgeGroup(_Request);
        }
        /// <summary>
        /// Description: Method defined to get customers spend range
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCustomersSpendRange(OAnalytics.Request _Request)
        {
            _AnaCustomer = new AnaCustomer();
            return _AnaCustomer.GetCustomersSpendRange(_Request);
        }
        /// <summary>
        /// Description: Method defined to get  customer age groups by gender
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetCustomersAgeGroupGender(OAnalytics.Request _Request)
        {
            _AnaCustomer = new AnaCustomer();
            return _AnaCustomer.GetCustomersAgeGroupGender(_Request);
        }
        /// <summary>
        /// Description: Method defined to get  customers sales range
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetSaleRange(OAnalytics.Request _Request)
        {
            _AnaCustomer = new AnaCustomer();
            return _AnaCustomer.GetSaleRange(_Request);
        }
        /// <summary>
        /// Description: Method defined to get customers reward range
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetRewardRange(OAnalytics.Request _Request)
        {
            _AnaCustomer = new AnaCustomer();
            return _AnaCustomer.GetRewardRange(_Request);
        }
    }
}
