//==================================================================================
// FileName: ManageRefund.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Console;
using HCore.TUC.Core.Object.Console;
using System;
using System.Collections.Generic;
using System.Text;


namespace HCore.TUC.Core.Operations.Console
{
    public class ManageRefund
    {
        FrameworkRefund _FrameworkRefund;
        /// <summary>
        /// Description: Refunds the customer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RefundCustomer(ORefund.Request _Request)
        {
            _FrameworkRefund = new FrameworkRefund();
            return _FrameworkRefund.RefundCustomer(_Request);
        }
        /// <summary>
        /// Description: Gets the refunds.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRefunds(OList.Request _Request)
        {
            _FrameworkRefund = new FrameworkRefund();
            return _FrameworkRefund.GetRefunds(_Request);
        }
    }
}
