//==================================================================================
// FileName: ManageMerchant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Console;
namespace HCore.TUC.Core.Operations.Console
{
    public class ManageMerchant
    {
        FrameworkMerchant _FrameworkMerchant;
        /// <summary>
        /// Description: Gets the point purchase history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPointPurchaseHistory(OList.Request _Request)
        {
            _FrameworkMerchant = new FrameworkMerchant();
            return _FrameworkMerchant.GetMerchantTopupHistory(_Request);
        }
    }
}
