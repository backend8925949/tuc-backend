//==================================================================================
// FileName: ManageAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Console;
using HCore.TUC.Core.Object.Console;


namespace HCore.TUC.Core.Operations.Console
{
    public class ManageAccount
    {      
        FrameworkAccountOperation _FrameworkAccountOperation;       
        FrameworkAccounts _FrameworkAccounts;
        /// <summary>
        /// Description: Updates the customer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateCustomer(OCustomer.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.UpdateCustomer(_Request);
        }
        /// <summary>
        /// Description: Updates the account status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateAccountStatus(OAccount.ManageStatus.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.UpdateAccountStatus(_Request);
        }
        /// <summary>
        /// Description: Saves the partner.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SavePartner(OPartner.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.SavePartner(_Request);
        }
        /// <summary>
        /// Description: Saves the acquirer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveAcquirer(OAcquirer.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.SaveAcquirer(_Request);
        }
        /// <summary>
        /// Description: Savepssps the specified request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse Savepssp(OPssp.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.Savepssp(_Request);
        }
        /// <summary>
        /// Description: Saves the PTSP.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SavePtsp(OPtsp.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.SavePtsp(_Request);
        }
        /// <summary>
        /// Description: Saves the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveMerchant(OMerchant.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperation();
            return _FrameworkAccountOperation.SaveMerchant(_Request);
        }




        /// <summary>
        /// Description: Gets the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccount(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetAccount(_Request);
        }

        /// <summary>
        /// Saves the deal review.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse CancelDealCount(OList.CancelDealRequest _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.CancelDealCount(_Request);
        }

        /// <summary>
        /// Description: Gets the cashiers.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCashiers(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetCashiers(_Request);
        }
        /// <summary>
        /// Description: Gets the cashiers overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCashiersOverview(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetCashiersOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the cashier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCashier(OAccounts.Cashier.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetCashier(_Request);
        }



        /// <summary>
        /// Description: Gets the partner.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPartner(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetPartner(_Request);
        }
        /// <summary>
        /// Description: Gets the partner.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPartner(OReference _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetPartner(_Request);
        }
        /// <summary>
        /// Description: Gets the acquirer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAcquirer(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetAcquirer(_Request);
        }
        /// <summary>
        /// Description: Gets the acquirer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAcquirer(OReference _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetAcquirer(_Request);
        }
        /// <summary>
        /// Description: Gets the PTSP.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPtsp(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetPtsp(_Request);
        }
        /// <summary>
        /// Description: Gets the PTSP.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPtsp(OReference _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetPtsp(_Request);
        }
        /// <summary>
        /// Description: Gets the PSSP.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPssp(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetPssp(_Request);
        }
        /// <summary>
        /// Description: Gets the PSSP.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPssp(OReference _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetPssp(_Request);
        }
        /// <summary>
        /// Description: Gets the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchant(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetMerchant(_Request);
        }
        /// <summary>
        /// Description: Gets the merchant overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchantOverview(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetMerchantOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the store.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStore(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetStore(_Request);
        }
        /// <summary>
        /// Description: Gets the store overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStoreOverview(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetStoreOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the store location.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStoreLocation(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetStoreLocation(_Request);
        }
        /// <summary>
        /// Description: Gets the terminal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTerminal(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetTerminal(_Request);
        }

        /// <summary>
        /// Description: Gets the terminals overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTerminalsOverview(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetTerminalsOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the customer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomer(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetCustomer(_Request);
        }

        /// <summary>
        /// Description: Gets the group merchant list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetGroupMerchantList(OList.Request _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetGroupMerchantList(_Request);
        }

        /// <summary>
        /// Description: Updates the referal code of a user.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateReferralId(OList.ORequest _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.UpdateReferralId(_Request);
        }

        /// <summary>
        /// Description: Updates the referal code of a user.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetReferralAmount(HCore.TUC.Core.Object.CustomerWeb.OAccount.ReferralAmountRequest _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetReferralAmount(_Request);
        }

        /// <summary>
        /// Description: Sets the referal amount.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SetReferralAmount(HCore.TUC.Core.Object.CustomerWeb.OAccount.ReferralAmountRequest _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.SetReferralAmount(_Request);
        }

        /// <summary>
        /// Description: Gets Merchant's Referrals.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetBonusHistory(HCore.TUC.Core.Object.CustomerWeb.OAccount.MerchantReferralRequest _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.GetBonusHistory(_Request);
        }

        /// <summary>
        /// Description: Credits the wallet.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse FundWallet(OList.FRequest _Request)
        {
            _FrameworkAccounts = new FrameworkAccounts();
            return _FrameworkAccounts.FundWallet(_Request);
        }
    }
}
