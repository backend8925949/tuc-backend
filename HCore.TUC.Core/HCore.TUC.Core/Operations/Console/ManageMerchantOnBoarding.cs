//==================================================================================
// FileName: ManageMerchantOnBoarding.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Console;
using HCore.TUC.Core.Object.Console;

namespace HCore.TUC.Core.Operations.Console
{
    public class ManageMerchantOnBoarding
    {
        FrameworkMerchantOnBoarding _FrameworkMerchantOnBoarding;

        /// <summary>
        /// Description: Gets the merchant list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchantList(OList.Request _Request)
        {
            _FrameworkMerchantOnBoarding = new FrameworkMerchantOnBoarding();
            return _FrameworkMerchantOnBoarding.GetMerchantList(_Request);
        }

        /// <summary>
        /// Description: Resends the merchant verification email.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ResendMerchantVerificationEmail(OMerchantOnBoarding.EmailVerificationRequest _Request)
        {
            _FrameworkMerchantOnBoarding = new FrameworkMerchantOnBoarding();
            return _FrameworkMerchantOnBoarding.ResendMerchantVerificationEmail(_Request);
        }
        /// <summary>
        /// Description: Resends the mobile verification otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ResendMobileVerificationOtp(OMerchantOnBoarding.MobileVerificationRequest _Request)
        {
            _FrameworkMerchantOnBoarding = new FrameworkMerchantOnBoarding();
            return _FrameworkMerchantOnBoarding.ResendMobileVerificationOtp(_Request);
        }
    }
}
