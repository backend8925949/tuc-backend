﻿using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Console.Analytics;
using HCore.TUC.Core.Object.Console.Analytics;

namespace HCore.TUC.Core.Operations.Console.Analytics
{
    public class ManageCustomersOverview
    {
        FrameworkCustomersOverview _FrameworkCustomersOverview;

        public OResponse GetFrequentlyPurchasedCategories(OCustomerOverview.Request _Request)
        {
            _FrameworkCustomersOverview = new FrameworkCustomersOverview();
            return _FrameworkCustomersOverview.GetFrequentlyPurchasedCategories(_Request);
        }

        public OResponse GetFrequentlyVisitedPlaces(OCustomerOverview.Request _Request)
        {
            _FrameworkCustomersOverview = new FrameworkCustomersOverview();
            return _FrameworkCustomersOverview.GetFrequentlyVisitedPlaces(_Request);
        }

        public OResponse GetCustomersTransactions(OCustomerOverview.Request _Request)
        {
            _FrameworkCustomersOverview = new FrameworkCustomersOverview();
            return _FrameworkCustomersOverview.GetCustomersTransactions(_Request);
        }

        public OResponse GetTransactionsOverview(OCustomerOverview.Request _Request)
        {
            _FrameworkCustomersOverview = new FrameworkCustomersOverview();
            return _FrameworkCustomersOverview.GetTransactionsOverview(_Request);
        }
    }
}

