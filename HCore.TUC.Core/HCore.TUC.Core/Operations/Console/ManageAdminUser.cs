//==================================================================================
// FileName: ManageAdminUser.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Console;
using HCore.TUC.Core.Object.Console;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.Console
{
    public class ManageAdminUser
    {       
        FrameworkAdminUser _FrameworkAdminUser;
        /// <summary>
        /// Description: Gets the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccount(OList.Request _Request)
        {
            _FrameworkAdminUser = new FrameworkAdminUser();
            return _FrameworkAdminUser.GetAccount(_Request);
        }
        /// <summary>
        /// Description: Gets the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccount(OAdminUser.Details.Request _Request)
        {
            _FrameworkAdminUser = new FrameworkAdminUser();
            return _FrameworkAdminUser.GetAccount(_Request);
        }
        /// <summary>
        /// Description: Saves the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveAccount(OAdminUser.Request _Request)
        {
            _FrameworkAdminUser = new FrameworkAdminUser();
            return _FrameworkAdminUser.SaveAccount(_Request);
        }
        /// <summary>
        /// Description: Updates the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateAccount(OAdminUser.Request _Request)
        {
            _FrameworkAdminUser = new FrameworkAdminUser();
            return _FrameworkAdminUser.UpdateAccount(_Request);
        }
        /// <summary>
        /// Description: Expires the account session.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ExpireAccountSession(OAdminUser.Session.Request _Request)
        {
            _FrameworkAdminUser = new FrameworkAdminUser();
            return _FrameworkAdminUser.ExpireAccountSession(_Request);
        }
        /// <summary>
        /// Description: Gets the account session.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountSession(OList.Request _Request)
        {
            _FrameworkAdminUser = new FrameworkAdminUser();
            return _FrameworkAdminUser.GetAccountSession(_Request);
        }


        /// <summary>
        /// Description: Updates the admin username.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateAdminUsername(OAdminUser.AuthUpdate.Request _Request)
        {
            _FrameworkAdminUser = new FrameworkAdminUser();
            return _FrameworkAdminUser.UpdateAdminUsername(_Request);
        }
        /// <summary>
        /// Description: Updates the admin password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateAdminPassword(OAdminUser.AuthUpdate.Request _Request)
        {
            _FrameworkAdminUser = new FrameworkAdminUser();
            return _FrameworkAdminUser.UpdateAdminPassword(_Request);
        }
        /// <summary>
        /// Description: Updates the admin reset password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateAdminResetPassword(OAdminUser.AuthUpdate.Request _Request)
        {
            _FrameworkAdminUser = new FrameworkAdminUser();
            return _FrameworkAdminUser.UpdateAdminResetPassword(_Request);
        }
        /// <summary>
        /// Description: Updates the admin pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateAdminPin(OAdminUser.AuthUpdate.Request _Request)
        {
            _FrameworkAdminUser = new FrameworkAdminUser();
            return _FrameworkAdminUser.UpdateAdminPin(_Request);
        }
        /// <summary>
        /// Description: Updates the admin reset pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateAdminResetPin(OAdminUser.AuthUpdate.Request _Request)
        {
            _FrameworkAdminUser = new FrameworkAdminUser();
            return _FrameworkAdminUser.UpdateAdminResetPin(_Request);
        }

        /// <summary>
        /// Description: Sets the app logo
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SetAppLogo(OAdminUser.AuthUpdate.LogoRequest _Request)
        {
            _FrameworkAdminUser = new FrameworkAdminUser();
            return _FrameworkAdminUser.SetAppLogo(_Request);
        }

        /// <summary>
        /// Description: Gets the app logo
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAppLogo(OAdminUser.AuthUpdate.LogoRequest _Request)
        {
            _FrameworkAdminUser = new FrameworkAdminUser();
            return _FrameworkAdminUser.GetAppLogo(_Request);
        }
    }
}
