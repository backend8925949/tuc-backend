//==================================================================================
// FileName: ManageAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Console.Analytics;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Object.Console.Analytics;

namespace HCore.TUC.Core.Operations.Console
{ 
    public class ManageAnalytics
    {
        FrameworkAnalytics _FrameworkAnalytics;
        /// <summary>
        /// Description: Gets the sales overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSalesOverview(OAnalytics.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetSalesOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the loyalty overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLoyaltyOverview(OAnalytics.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetLoyaltyOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the customers overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomersOverview(OAnalytics.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetCustomersOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the customers status overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomersStatusOverview(OList.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetCustomersStatusOverview(_Request);
        }
        //public OResponse GetTransactionHistory(OList.Request _Request)
        //{
        //    _FrameworkAnalytics = new FrameworkAnalytics();
        //    return _FrameworkAnalytics.GetTransactionHistory(_Request);
        //}


        /// <summary>
        /// Description: Gets the revenue overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRevenueOverview(OAnalytics.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetRevenueOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the todays overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTodaysOverview(OAnalytics.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetTodaysOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the sales history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSalesHistory(OList.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetSalesHistory(_Request);
        }
        /// <summary>
        /// Description: Gets the reward history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRewardHistory(OList.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetRewardHistory(_Request);
        }

        /// <summary>
        /// Description: Gets the reward claim history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRewardClaimHistory(OList.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetRewardClaimHistory(_Request);
        }
        /// <summary>
        /// Description: Gets the redeem history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRedeemHistory(OList.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetRedeemHistory(_Request);
        }
        /// <summary>
        /// Description: Gets the commission history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCommissionHistory(OList.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetCommissionHistory(_Request);
        }
    }
}
