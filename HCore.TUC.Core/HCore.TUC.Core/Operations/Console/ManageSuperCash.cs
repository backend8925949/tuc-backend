//==================================================================================
// FileName: ManageSuperCash.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Console;
using HCore.TUC.Core.Object.Console;

namespace HCore.TUC.Core.Operations.Console
{
    public class ManageSuperCash
    {
        FrameworkUniversalPoints _FrameworkUniversalPoints;

        /// <summary>
        /// Description: Credits the super cash.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse CreditSuperCash(OUniversalPoint.Request _Request)
        {
            _FrameworkUniversalPoints = new FrameworkUniversalPoints();
            return _FrameworkUniversalPoints.CreditUniversalPoints(_Request);
        }
        /// <summary>
        /// Description: Gets the super cash.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSuperCash(OList.Request _Request)
        {
            _FrameworkUniversalPoints = new FrameworkUniversalPoints();
            return _FrameworkUniversalPoints.GetUniversalPoints(_Request);
        }
    }
}
