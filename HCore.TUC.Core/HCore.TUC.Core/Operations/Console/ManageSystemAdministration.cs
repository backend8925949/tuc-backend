//==================================================================================
// FileName: ManageSystemAdministration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Framework.Console;
using HCore.TUC.Core.Framework.SystemManager;
using HCore.TUC.Core.Object.Console;
using System;
using System.Collections.Generic;
using System.Text;


namespace HCore.TUC.Core.Operations.Console
{
    public class ManageSystemAdministration
    {
        FrameworkSystemAdministration _FrameworkSystemAdministration;       
        FrameworkApp _FrameworkApp;
        /// <summary>
        /// Description: Saves the slider image.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveSliderImage(OSystemAdministration.AppSlider.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.SaveSliderImage(_Request);
        }
        /// <summary>
        /// Description: Updates the slider image status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateSliderImageStatus(OSystemAdministration.AppSlider.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.UpdateSliderImageStatus(_Request);
        }
        /// <summary>
        /// Description: Deletes the slider image.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteSliderImage(OSystemAdministration.AppSlider.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.DeleteSliderImage(_Request);
        }
        /// <summary>
        /// Description: Gets the slider image.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSliderImage(OList.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.GetSliderImage(_Request);
        }
        /// <summary>
        /// Description: Gets the otp request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetOtpRequest(OSystemAdministration.SystemVerification.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.GetOtpRequest(_Request);
        }
        /// <summary>
        /// Description: Gets the otp request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetOtpRequest(OList.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.GetOtpRequest(_Request);
        }

        //public OResponse SaveCategory(OSystemAdministration.Category.Request _Request)
        //{
        //    _FrameworkSystemAdministration = new FrameworkSystemAdministration();
        //    return _FrameworkSystemAdministration.SaveCategory(_Request);
        //}
        //public OResponse UpdateCategory(OSystemAdministration.Category.Request _Request)
        //{
        //    _FrameworkSystemAdministration = new FrameworkSystemAdministration();
        //    return _FrameworkSystemAdministration.UpdateCategory(_Request);
        //}
        //public OResponse GetCategory(OList.Request _Request)
        //{
        //    _FrameworkSystemAdministration = new FrameworkSystemAdministration();
        //    return _FrameworkSystemAdministration.GetCategory(_Request);
        //}
        /// <summary>
        /// Description: Gets the system log.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSystemLog(OSystemAdministration.ExceptionLog.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.GetSystemLog(_Request);
        }
        /// <summary>
        /// Description: Gets the system log.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSystemLog(OList.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.GetSystemLog(_Request);
        }
        /// <summary>
        /// Description: Updates the response code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateResponseCode(OSystemAdministration.ResponseCode.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.UpdateResponseCode(_Request);
        }
        /// <summary>
        /// Description: Gets the response code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetResponseCode(OList.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.GetResponseCode(_Request);
        }
        /// <summary>
        /// Description: Saves the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveConfiguration(OSystemAdministration.Configuration.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.SaveConfiguration(_Request);
        }
        /// <summary>
        /// Description: Updates the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateConfiguration(OSystemAdministration.Configuration.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.UpdateConfiguration(_Request);
        }
        /// <summary>
        /// Description: Updates the configuration value.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateConfigurationValue(OSystemAdministration.ConfigurationValue.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.UpdateConfigurationValue(_Request);
        }
        /// <summary>
        /// Description: Updates the configuration value.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateConfigurationValue(OSystemAdministration.AccountConfiguration.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.UpdateConfigurationValue(_Request);
        }
        /// <summary>
        /// Description: Updates the configuration value.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateConfigurationValue(OSystemAdministration.AccountConfiguration.BulkRequest _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.UpdateConfigurationValue(_Request);
        }
        /// <summary>
        /// Description: Deletes the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteConfiguration(OSystemAdministration.Configuration.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.DeleteConfiguration(_Request);
        }
        /// <summary>
        /// Description: Gets the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetConfiguration(OList.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.GetConfiguration(_Request);
        }
        /// <summary>
        /// Description: Gets the configuration account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetConfigurationAccount(OList.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.GetConfigurationAccount(_Request);
        }
        /// <summary>
        /// Description: Gets the configuration history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetConfigurationHistory(OList.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.GetConfigurationHistory(_Request);
        }



        /// <summary>
        /// Description: Gets the feature.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetFeature(OList.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.GetFeature(_Request);
        }
        /// <summary>
        /// Description: Gets the department.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDepartment(OList.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.GetDepartment(_Request);
        }
        /// <summary>
        /// Description: Gets the role.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRole(OList.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.GetRole(_Request);
        }

        /// <summary>
        /// Description: Gets the role feature.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRoleFeature(OList.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.GetRoleFeature(_Request);
        }
        /// <summary>
        /// Description: Saves the role.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveRole(OSystemAdministration.Role.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.SaveRole(_Request);
        }
        /// <summary>
        /// Description: Updates the role.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateRole(OSystemAdministration.Role.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.UpdateRole(_Request);
        }
        /// <summary>
        /// Description: Saves the role feature.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveRoleFeature(OSystemAdministration.RoleFeature.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.SaveRoleFeature(_Request);
        }
        /// <summary>
        /// Description: Removes the role feature.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RemoveRoleFeature(OSystemAdministration.RoleFeature.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.RemoveRoleFeature(_Request);
        }
        /// <summary>
        /// Description: Deletes the role.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteRole(OSystemAdministration.Role.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.DeleteRole(_Request);
        }

        /// <summary>
        /// Description: Gets the application.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetApp(OList.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetApp(_Request);
        }
        /// <summary>
        /// Description: Saves the application.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveApp(OSystemAdministration.App.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.SaveApp(_Request);
        }
        /// <summary>
        /// Description: Updates the application.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateApp(OSystemAdministration.App.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.UpdateApp(_Request);
        }
        /// <summary>
        /// Description: Deletes the application.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteApp(OSystemAdministration.App.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.DeleteApp(_Request);
        }

        /// <summary>
        /// Description: Gets the application.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetApp(OSystemAdministration.App.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetApp(_Request);
        }
        /// <summary>
        /// Description: Resets the application key.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ResetAppKey(OSystemAdministration.App.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.ResetAppKey(_Request);
        }
        /// <summary>
        /// Description: Gets the application usage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAppUsage(OSystemAdministration.App.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetAppUsage(_Request);
        }
        /// <summary>
        /// Description: Gets the application usage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAppUsage(OList.Request _Request)
        {
            _FrameworkApp = new FrameworkApp();
            return _FrameworkApp.GetAppUsage(_Request);
        }

        /// <summary>
        /// Description: Gets the SMS notification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSmsNotification(OList.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.GetSmsNotification(_Request);
        }
        /// <summary>
        /// Description: Gets the feature list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetFeatureList(OList.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.GetFeatureList(_Request);
        }
        /// <summary>
        /// Description: Gets the role details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRoleDetails(OList.Request _Request)
        {
            _FrameworkSystemAdministration = new FrameworkSystemAdministration();
            return _FrameworkSystemAdministration.GetRoleDetails(_Request);
        }
    }

}
