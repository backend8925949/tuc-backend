//==================================================================================
// FileName: ManageProfile.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.Operations.Object;
using HCore.TUC.Core.Framework.Console;
using HCore.TUC.Core.Object.Console;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.Operations.Console
{
    public class ManageProfile
    {
        FrameworkProfile _FrameworkProfile;
        //public OResponse SaveAdmin(OAdmin.Request _Request)
        //{
        //    _FrameworkAdmin = new FrameworkAdmin();
        //    return _FrameworkAdmin.SaveAdmin(_Request);
        //}
        /// <summary>
        /// Description: Updates the profile.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateProfile(OProfile.Request _Request)
        {
            _FrameworkProfile = new FrameworkProfile();
            return _FrameworkProfile.UpdateProfile(_Request);
        }
        /// <summary>
        /// Description: Updates the password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdatePassword(OProfile.Password.Request _Request)
        {
            _FrameworkProfile = new FrameworkProfile();
            return _FrameworkProfile.UpdatePassword(_Request);
        }

        /// <summary>
        /// Description: Gets the profile.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetProfile(OProfile.Details.Request _Request)
        {
            _FrameworkProfile = new FrameworkProfile();
            return _FrameworkProfile.GetProfile(_Request);
        }

        /// <summary>
        /// Description: Removes the profile image.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RemoveProfileImage(OProfile.Profile _Request)
        {
            _FrameworkProfile = new FrameworkProfile();
            return _FrameworkProfile.RemoveProfileImage(_Request);
        }
    }
}
