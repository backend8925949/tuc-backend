//==================================================================================
// FileName: ManageAnalyticsCustom.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Console.Analytics;
using HCore.TUC.Core.Framework.Console.Analytics.Custom;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Object.Console.Analytics;
using HCore.TUC.Core.Object.Console.Analytics.Customer;

namespace HCore.TUC.Core.Operations.Console
{
    public class ManageAnalyticsCustom
    {      
        AnaCustom _AnaCustom;
        /// <summary>
        /// Description: Gets the customers count.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomersCount(OAnaCustom.Request _Request)
        {
            _AnaCustom = new AnaCustom();
            return _AnaCustom.GetCustomersCount(_Request);
        }
        /// <summary>
        /// Description: Gets the customers balance overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomersBalanceOverview(OAnaCustom.Request _Request)
        {
            _AnaCustom = new AnaCustom();
            return _AnaCustom.GetCustomersBalanceOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the application downloads.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAppDownloads(OList.Request _Request)
        {
            _AnaCustom = new AnaCustom();
            return _AnaCustom.GetAppDownloads(_Request);
        }

        /// <summary>
        /// Description: Gets the customer registration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomerRegistration(OList.Request _Request)
        {
            _AnaCustom = new AnaCustom();
            return _AnaCustom.GetCustomerRegistration(_Request);
        }

        /// <summary>
        /// Description: Gets the customer visits.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomerVisits(OList.Request _Request)
        {
            _AnaCustom = new AnaCustom();
            return _AnaCustom.GetCustomerVisits(_Request);
        }
    }
}
