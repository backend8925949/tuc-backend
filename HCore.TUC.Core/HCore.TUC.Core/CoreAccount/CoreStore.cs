//==================================================================================
// FileName: CoreStore.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.CoreAccount
{
    internal class CoreStore
    {
        Random _Random;
        HCUAccountAuth _HCUAccountAuth;
        HCoreContext _HCoreContext;
        OCoreStore.Response _StoreResponse;
        HCUAccount _HCUAccount;
        List<HCUAccountParameter> _HCUAccountParameters;
        List<TUCCategoryAccount> _AccountCategories;
        internal OCoreStore.Response SaveStore(OCoreStore.Request _Request)
        {
            _StoreResponse = new OCoreStore.Response();
            _StoreResponse.Status = HCoreConstant.ResponseStatus.Error;
            _StoreResponse.StatusCode = TUCCoreResource.CA0500;
            _StoreResponse.Message = TUCCoreResource.CA0500M;
            try
            {
                if (_Request.MerchantId < 0)
                {
                    _StoreResponse.StatusCode = TUCCoreResource.CA1136;
                    _StoreResponse.Message = TUCCoreResource.CA1136M;
                    return _StoreResponse;
                }
                if (string.IsNullOrEmpty(_Request.MerchantKey))
                {
                    _StoreResponse.StatusCode = TUCCoreResource.CA1137;
                    _StoreResponse.Message = TUCCoreResource.CA1137M;
                    return _StoreResponse;
                }
                if (string.IsNullOrEmpty(_Request.DisplayName))
                {
                    _StoreResponse.StatusCode = TUCCoreResource.CA1138;
                    _StoreResponse.Message = TUCCoreResource.CA1138M;
                    return _StoreResponse;
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    _StoreResponse.StatusCode = TUCCoreResource.CA1139;
                    _StoreResponse.Message = TUCCoreResource.CA1139M;
                    return _StoreResponse;
                }
                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.Address, _Request.UserReference);
                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.MerchantKey
                    && x.Id == _Request.MerchantId)
                       .Select(x => new
                       {
                           x.AccountOperationTypeId
                       }).FirstOrDefault();
                    if (MerchantDetails == null)
                    {
                        _HCoreContext.Dispose();
                        _StoreResponse.Status = HCoreConstant.ResponseStatus.Error;
                        _StoreResponse.StatusCode = TUCCoreResource.CA1140;
                        _StoreResponse.Message = TUCCoreResource.CA1140M;
                        return _StoreResponse;
                    }

                    bool Details = _HCoreContext.HCUAccount
                        .Any(x => x.Owner.Guid == _Request.MerchantKey
                        && x.OwnerId == _Request.MerchantId
                        && x.AccountTypeId == UserAccountType.MerchantStore
                        && x.DisplayName == _Request.DisplayName);
                    if (Details)
                    {
                        _HCoreContext.Dispose();
                        _StoreResponse.Status = HCoreConstant.ResponseStatus.Error;
                        _StoreResponse.StatusCode = TUCCoreResource.CA1077;
                        _StoreResponse.Message = TUCCoreResource.CA1077M;
                        return _StoreResponse;
                    }
                    else
                    {
                        _Random = new Random();
                        string AccountCode = _Random.Next(100000000, 999999999).ToString();
                        _HCUAccountAuth = new HCUAccountAuth();
                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(6);
                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                        }
                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;

                        _HCUAccount = new HCUAccount();
                        _HCUAccount.User = _HCUAccountAuth;
                        if (_Request.Categories != null && _Request.Categories.Count > 0)
                        {
                            _HCUAccountParameters = new List<HCUAccountParameter>();
                            _AccountCategories = new List<TUCCategoryAccount>();

                            foreach (var Category in _Request.Categories)
                            {
                                _AccountCategories.Add(new TUCCategoryAccount
                                {
                                    Guid = HCoreHelper.GenerateGuid(),
                                    TypeId = 1,
                                    CategoryId = Category.ReferenceId,
                                    CreateDate = HCoreHelper.GetGMTDateTime(),
                                    CreatedById = _Request.UserReference.AccountId,
                                    StatusId = HelperStatus.Default.Active,
                                });
                            }
                            _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                        }
                        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                        _HCUAccount.AccountOperationTypeId = MerchantDetails.AccountOperationTypeId;
                        if (_Request.AccountOperationTypeCode == "accountoperationtype.online")
                        {
                            _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                        }
                        if (_Request.AccountOperationTypeCode == "accountoperationtype.offline")
                        {
                            _HCUAccount.AccountOperationTypeId = AccountOperationType.Offline;
                        }
                        if (_Request.AccountOperationTypeCode == "accountoperationtype.onlineandoffline")
                        {
                            _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                        }
                        _HCUAccount.OwnerId = _Request.MerchantId;
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                        _HCUAccount.AccountCode = AccountCode;
                        if (_Request.DisplayName.Length > 30)
                        {
                            _HCUAccount.DisplayName = _Request.DisplayName.Substring(0, 29);
                        }
                        else
                        {
                            _HCUAccount.DisplayName = _Request.DisplayName;
                        }
                        if (_Request.OwnerId != 0)
                        {
                            _HCUAccount.OwnerId = _Request.OwnerId;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(_Request.ReferralCode))
                            {
                                long ReferrerId = _HCoreContext.HCUAccount
                                    .Where(x => x.ReferralCode == _Request.ReferralCode
                                    && (x.AccountTypeId == UserAccountType.Merchant || x.AccountTypeId == UserAccountType.Acquirer || x.AccountTypeId == UserAccountType.PgAccount || x.AccountTypeId == UserAccountType.PosAccount))
                                    .Select(x => x.Id).FirstOrDefault();
                                if (ReferrerId > 0)
                                {
                                    _HCUAccount.OwnerId = ReferrerId;
                                }
                                else
                                {
                                    _StoreResponse.StatusCode = TUCCoreResource.CA1458;
                                    _StoreResponse.Message = TUCCoreResource.CA1458M;
                                    return _StoreResponse;
                                }
                            }
                        }
                        _HCUAccount.Name = _Request.Name;
                        _HCUAccount.ContactNumber = _Request.ContactNumber;
                        _HCUAccount.EmailAddress = _Request.EmailAddress;
                        if (_Request.ContactPerson != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.FirstName))
                            {
                                _HCUAccount.FirstName = _Request.ContactPerson.FirstName;
                                _HCUAccount.CpFirstName = _Request.ContactPerson.FirstName;
                            }
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.LastName))
                            {
                                _HCUAccount.LastName = _Request.ContactPerson.LastName;
                                _HCUAccount.CpLastName = _Request.ContactPerson.LastName;
                            }
                            _HCUAccount.CpName = _Request.ContactPerson.FirstName + " " + _Request.ContactPerson.LastName;
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.MobileNumber))
                            {
                                _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber);
                                _HCUAccount.CpMobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber);
                            }
                            if (!string.IsNullOrEmpty(_Request.ContactPerson.EmailAddress))
                            {
                                _HCUAccount.SecondaryEmailAddress = _Request.ContactPerson.EmailAddress;
                                _HCUAccount.CpEmailAddress = _Request.ContactPerson.EmailAddress;
                            }
                        }
                        if (_Request.StartDate != null)
                        {
                            _HCUAccount.DateOfBirth = _Request.StartDate;
                        }
                        _HCUAccount.WebsiteUrl = _Request.WebsiteUrl;
                        _HCUAccount.Description = _Request.Description;
                        if (_AddressResponse != null)
                        {
                            if (!string.IsNullOrEmpty(_AddressResponse.Address))
                            {
                                _HCUAccount.Address = _AddressResponse.Address;
                            }
                            if (_AddressResponse.CityAreaId != 0)
                            {
                                _HCUAccount.CityAreaId = _AddressResponse.CityAreaId;
                            }
                            if (_AddressResponse.CityId != 0)
                            {
                                _HCUAccount.CityId = _AddressResponse.CityId;
                            }
                            if (_AddressResponse.StateId != 0)
                            {
                                _HCUAccount.StateId = _AddressResponse.StateId;
                            }
                            if (_AddressResponse.CountryId != 0)
                            {
                                _HCUAccount.CountryId = _AddressResponse.CountryId;
                            }
                            if (_AddressResponse.Latitude != 0)
                            {
                                _HCUAccount.Latitude = _AddressResponse.Latitude;
                            }
                            if (_AddressResponse.Longitude != 0)
                            {
                                _HCUAccount.Longitude = _AddressResponse.Longitude;
                            }
                        }
                        if (_HCUAccount.CountryId < 1)
                        {
                            _HCUAccount.CountryId = _Request.UserReference.SystemCountry;
                        }
                        _HCUAccount.EmailVerificationStatus = 0;
                        _HCUAccount.NumberVerificationStatus = 0;
                        _HCUAccount.RegistrationSourceId = RegistrationSource.System;
                        _HCUAccount.CountValue = 0;
                        _HCUAccount.AverageValue = 0;
                        if (_Request.UserReference.AppVersionId != 0)
                        {
                            _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                        }
                        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                        }
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                        _HCoreContext.SaveChanges();
                        OCoreStore.Response _Response = new OCoreStore.Response();
                        _Response.Status = ResponseStatus.Success;
                        _Response.StatusCode = TUCCoreResource.CA1135;
                        _Response.Message = TUCCoreResource.CA1135M;
                        _Response.ReferenceId = _HCUAccount.Id;
                        _Response.ReferenceKey = _HCUAccount.Guid;
                        return _Response;
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Core-SaveStore", _Exception, _Request.UserReference);
                return _StoreResponse;
            }
        }
    }
}
