//==================================================================================
// FileName: OCoreMerchant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Core.CoreAccount
{

    internal class OCoreMerchant
    {
        //internal class Category
        //{
        //    internal long ReferenceId { get; set; }
        //    internal string ReferenceKey { get; set; }
        //    internal string Name { get; set; }
        //    internal string IconUrl { get; set; }
        //}
        //internal class ContactPerson
        //{
        //    internal string FirstName { get; set; }
        //    internal string LastName { get; set; }
        //    internal string MobileNumber { get; set; }
        //    internal string EmailAddress { get; set; }
        //}

        internal class Request
        {
            internal long OwnerId { get; set; }
            internal string OwnerKey { get; set; }

            internal long AcquirerId { get; set; }
            internal string AcquirerKey { get; set; }

            internal string AccountOperationTypeCode { get; set; }

            internal string DisplayName { get; set; }
            internal string BusinessOwnerName { get; set; }
            internal string FirstName { get; set; }
            internal string CompanyName { get; set; }

            internal string ContactNumber { get; set; }
            internal string MobileNumber { get; set; }
            internal string EmailAddress { get; set; }

            internal string UserName { get; set; }
            internal string Password { get; set; }

            internal string WebsiteUrl { get; set; }
            internal string Description { get; set; }

            internal long BranchId { get; set; }
            internal string BranchKey { get; set; }
            internal string ReferralCode { get; set; }

            internal long RmId { get; set; }
            internal string RmKey { get; set; }

            internal bool IsDealMerchant { get; set; }

            internal DateTime? StartDate { get; set; }

            internal ContactPerson ContactPerson { get; set; }

            internal Configuration Configurations { get; set; }
            internal OAddress Address { get; set; }
            internal List<Store> Stores { get; set; }
            internal string StatusCode { get; set; }
            internal OUserReference UserReference { get; set; }
            internal List<Category> Categories { get; set; }
        }
        internal class Configuration
        {
            internal double RewardPercentage { get; set; }
            internal string RewardDeductionTypeCode { get; set; }
        }
        internal class Store
        {
            internal string DisplayName { get; set; }
            internal string Name { get; set; }
            internal string ContactNumber { get; set; }
            internal string EmailAddress { get; set; }
            internal string BusinessOwnerName { get; set; }
            internal string AccountOperationTypeCode { get; set; }
            internal string WebsiteUrl { get; set; }
            internal string Description { get; set; }
            internal DateTime? StartDate { get; set; }
            internal OAddress Address { get; set; }
            internal ContactPerson ContactPerson { get; set; }
            internal List<Terminal> Terminals { get; set; }
            internal List<Category> Categories { get; set; }
        }
        internal class Terminal
        {
            internal string TerminalId { get; set; }
            internal string SerialNumber { get; set; }
            internal long ProviderId { get; set; }
            internal string ProviderKey { get; set; }
            internal long AcquirerId { get; set; }
            internal string AcquirerKey { get; set; }
        }
        internal class Response
        {
            internal ResponseStatus Status { get; set; }
            internal string StatusCode { get; set; }
            internal string Message { get; set; }
            internal long ReferenceId { get; set; }
            internal string ReferenceKey { get; set; }
        }
    }




}
