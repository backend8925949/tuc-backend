//==================================================================================
// FileName: CoreCashier.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.CoreAccount
{
    internal class CoreCashier
    {
        Random _Random;
        HCoreContext _HCoreContext;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        OCoreCashier.Response _CashierResponse;
        internal OCoreCashier.Response SaveCashier(OCoreCashier.Request _Request)
        {
            _CashierResponse = new OCoreCashier.Response
            {
                Status = ResponseStatus.Error,
                StatusCode = TUCCoreResource.CA0500,
                Message = TUCCoreResource.CA0500M
            };
            try
            {
                if (_Request.MerchantId < 0)
                {
                    _CashierResponse.StatusCode = TUCCoreResource.CA1119;
                    _CashierResponse.Message = TUCCoreResource.CA1119M;
                    return _CashierResponse;
                }
                else if (_Request.StoreId < 0)
                {
                    _CashierResponse.StatusCode = TUCCoreResource.CA1120;
                    _CashierResponse.Message = TUCCoreResource.CA1120M;
                    return _CashierResponse;
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    _CashierResponse.StatusCode = TUCCoreResource.CA1121;
                    _CashierResponse.Message = TUCCoreResource.CA1121M;
                    return _CashierResponse;
                }
                else if (string.IsNullOrEmpty(_Request.FirstName))
                {
                    _CashierResponse.StatusCode = TUCCoreResource.CA1122;
                    _CashierResponse.Message = TUCCoreResource.CA1122M;
                    return _CashierResponse;
                }
                else if (string.IsNullOrEmpty(_Request.LastName))
                {
                    _CashierResponse.StatusCode = TUCCoreResource.CA1123;
                    _CashierResponse.Message = TUCCoreResource.CA1123M;
                    return _CashierResponse;
                }
                else
                {
                    OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.Address, _Request.UserReference);
                    using (_HCoreContext = new HCoreContext())
                    {
                        var MerchantDetails = _HCoreContext.HCUAccount.Any(x => x.Id == _Request.MerchantId && x.AccountTypeId == UserAccountType.Merchant);
                        if (MerchantDetails)
                        {
                            _CashierResponse.Status = ResponseStatus.Error;
                            _CashierResponse.StatusCode = TUCCoreResource.CA1124;
                            _CashierResponse.Message = TUCCoreResource.CA1124M;
                            return _CashierResponse;
                        }

                        var StoreDetails = _HCoreContext.HCUAccount.Any(x => x.Id == _Request.StoreId && x.OwnerId == _Request.MerchantId && x.AccountTypeId == UserAccountType.MerchantStore);
                        if (StoreDetails == false)
                        {
                            _CashierResponse.StatusCode = TUCCoreResource.CA1125;
                            _CashierResponse.Message = TUCCoreResource.CA1125M;
                            return _CashierResponse;
                        }
                        string CashierId = HCoreHelper.GenerateRandomNumber(4);
                        bool CashierCodeCheck = _HCoreContext.HCUAccount.Any(x =>
                                     x.OwnerId == _Request.MerchantId &&
                                     x.Owner.Guid == _Request.MerchantKey &&
                                     x.DisplayName == CashierId &&
                                     x.AccountTypeId == UserAccountType.MerchantCashier);
                        if (CashierCodeCheck)
                        {
                            CashierId = HCoreHelper.GenerateRandomNumber(4);
                        }

                        //long CashierCount = _HCoreContext.HCUAccount.Where(x =>
                        //             x.OwnerId == _Request.MerchantId &&
                        //             x.Owner.Guid == _Request.MerchantKey &&
                        //             x.AccountTypeId == UserAccountType.MerchantCashier)
                        //    .Select(x => x.Id)
                        //    .Count();
                        //CashierCount += 1;
                        //if (CashierCount < 10)
                        //{
                        //    CashierId = "100" + CashierCount;
                        //}
                        //else if (CashierCount > 9 && CashierCount < 100)
                        //{
                        //    CashierId = "10" + CashierCount;
                        //}
                        //else if (CashierCount > 99 && CashierCount < 1000)
                        //{
                        //    CashierId = "1" + CashierCount;
                        //}
                        //else if (CashierCount > 999)
                        //{
                        //    CashierId = CashierCount.ToString();
                        //}
                        #region User
                        _HCUAccountAuth = new HCUAccountAuth();
                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(6);
                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                        _HCUAccountAuth.SecondaryPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                        }
                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                        #endregion
                        #region Save Cashier Account
                        _Random = new Random();
                        string AccountCode = _Random.Next(1000, 9999).ToString() + _Random.Next(000000000, 999999999).ToString();
                        _HCUAccount = new HCUAccount();
                        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccount.AccountTypeId = UserAccountType.MerchantCashier;
                        _HCUAccount.AccountOperationTypeId = Helpers.AccountOperationType.Offline;
                        _HCUAccount.OwnerId = _Request.StoreId;
                        _HCUAccount.SubOwnerId = _Request.MerchantId;
                        _HCUAccount.DisplayName = CashierId;
                        _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                        _HCUAccount.AccountCode = AccountCode;
                        if (_Request.GenderCode == "gender.male")
                        {
                            _HCUAccount.GenderId = Gender.Male;
                        }
                        if (_Request.GenderCode == "gender.female")
                        {
                            _HCUAccount.GenderId = Gender.Female;
                        }
                        if (_Request.GenderCode == "gender.other")
                        {
                            _HCUAccount.GenderId = Gender.Other;
                        }
                        if (_Request.UserReference.AppVersionId != 0)
                        {
                            _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                        }
                        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                        }
                        _HCUAccount.Name = _Request.FirstName + " " + _Request.LastName;
                        _HCUAccount.ContactNumber = _Request.ContactNumber;
                        _HCUAccount.EmailAddress = _Request.EmailAddress;
                        _HCUAccount.ReferralCode = _Request.EmployeeCode;
                        if (!string.IsNullOrEmpty(_Request.FirstName))
                        {
                            _HCUAccount.FirstName = _Request.FirstName;
                        }
                        if (!string.IsNullOrEmpty(_Request.LastName))
                        {
                            _HCUAccount.LastName = _Request.LastName;
                        }
                        if (_AddressResponse != null)
                        {
                            if (!string.IsNullOrEmpty(_AddressResponse.Address))
                            {
                                _HCUAccount.Address = _AddressResponse.Address;
                            }
                            if (_AddressResponse.CityAreaId != 0)
                            {
                                _HCUAccount.CityAreaId = _AddressResponse.CityAreaId;
                            }
                            if (_AddressResponse.CityId != 0)
                            {
                                _HCUAccount.CityId = _AddressResponse.CityId;
                            }
                            if (_AddressResponse.StateId != 0)
                            {
                                _HCUAccount.StateId = _AddressResponse.StateId;
                            }
                            if (_AddressResponse.CountryId != 0)
                            {
                                _HCUAccount.CountryId = _AddressResponse.CountryId;
                            }
                            if (_AddressResponse.Latitude != 0)
                            {
                                _HCUAccount.Latitude = _AddressResponse.Latitude;
                            }
                            if (_AddressResponse.Longitude != 0)
                            {
                                _HCUAccount.Longitude = _AddressResponse.Longitude;
                            }
                        }

                        if (_HCUAccount.CountryId <  1)
                        {
                            _HCUAccount.CountryId = _Request.UserReference.SystemCountry;
                        }
                        _HCUAccount.EmailVerificationStatus = 0;
                        _HCUAccount.NumberVerificationStatus = 0;
                        _HCUAccount.RegistrationSourceId = RegistrationSource.System;
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.User = _HCUAccountAuth;
                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                        _HCoreContext.SaveChanges();
                        #endregion

                        long? IconStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                        }
                        if (IconStorageId != null)
                        {
                            using (HCoreContext _HCoreContextUpload = new HCoreContext())
                            {
                                var UserAccDetails = _HCoreContextUpload.HCUAccount.Where(x => x.Id == _HCUAccount.Id).FirstOrDefault();
                                if (UserAccDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        UserAccDetails.IconStorageId = IconStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        var Response = new
                        {
                            ReferenceId = _HCUAccount.Id,
                            ReferenceKey = _HCUAccount.Guid,
                        };
                        OCoreCashier.Response _Response = new OCoreCashier.Response();
                        _Response.Status = HCoreConstant.ResponseStatus.Success;
                        _Response.StatusCode = TUCCoreResource.CA1105;
                        _Response.Message = TUCCoreResource.CA1105M;
                        _Response.ReferenceId = _HCUAccount.Id;
                        _Response.ReferenceKey = _HCUAccount.Guid;
                        return _Response;
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Core-SaveCashier", _Exception, _Request.UserReference);
                return _CashierResponse;
            }
        }
        //internal static OCoreCashier.Response SaveCashier(OCoreCashier.BulkRequest _Request)
        //{
        //    try
        //    {
        //        foreach (var Cashier in _Request.Cashiers)
        //        {
        //            OAddressResponse _AddressResponse = HCoreHelper.GetAddressIds(Cashier.Address, _Request.UserReference);
        //            using (HCoreContext _HCoreContext = new HCoreContext())
        //            {
        //                string CashierId = "";
        //                long CashierCount = _HCoreContext.HCUAccount.Where(x => x.OwnerId == _Request.MerchantId
        //                            && x.Owner.Guid == _Request.MerchantKey
        //                            && x.AccountTypeId == UserAccountType.MerchantCashier)
        //                    .Select(x => x.Id)
        //                    .Count();
        //                CashierCount += 1;
        //                if (CashierCount < 10)
        //                {
        //                    CashierId = "100" + CashierCount;
        //                }
        //                else if (CashierCount > 9 && CashierCount < 100)
        //                {
        //                    CashierId = "10" + CashierCount;
        //                }
        //                else if (CashierCount > 99 && CashierCount < 1000)
        //                {
        //                    CashierId = "1" + CashierCount;
        //                }
        //                else if (CashierCount > 999)
        //                {
        //                    CashierId = CashierCount.ToString();
        //                }
        //                Random _Random = new Random();
        //                string AccountCode = _Random.Next(1000, 9999).ToString() + _Random.Next(000000000, 999999999).ToString();
        //                List<HCUAccount> _HCUAccounts = new List<HCUAccount>();
        //                HCUAccount _HCUAccount = new HCUAccount();
        //                _HCUAccount.Guid = HCoreHelper.GenerateGuid();
        //                _HCUAccount.AccountTypeId = UserAccountType.MerchantCashier;
        //                _HCUAccount.AccountOperationTypeId = Helpers.AccountOperationType.Offline;
        //                _HCUAccount.OwnerId = _Request.MerchantId;
        //                _HCUAccount.SubOwnerId = Cashier.StoreId;
        //                _HCUAccount.DisplayName = CashierId;
        //                _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
        //                _HCUAccount.AccountCode = AccountCode;
        //                if (Cashier.GenderCode == "gender.male")
        //                {
        //                    _HCUAccount.GenderId = Helpers.Gender.Male;
        //                }
        //                if (Cashier.GenderCode == "gender.female")
        //                {
        //                    _HCUAccount.GenderId = Helpers.Gender.Female;
        //                }
        //                if (Cashier.GenderCode == "gender.other")
        //                {
        //                    _HCUAccount.GenderId = Helpers.Gender.Other;
        //                }
        //                if (_Request.UserReference.AppVersionId != 0)
        //                {
        //                    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
        //                }
        //                _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
        //                if (_Request.UserReference.AccountId != 0)
        //                {
        //                    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
        //                }
        //                _HCUAccount.Name = Cashier.FirstName + " " + Cashier.LastName;
        //                _HCUAccount.ContactNumber = Cashier.ContactNumber;
        //                _HCUAccount.EmailAddress = Cashier.EmailAddress;
        //                _HCUAccount.ReferralCode = Cashier.EmployeeCode;
        //                if (!string.IsNullOrEmpty(Cashier.FirstName))
        //                {
        //                    _HCUAccount.FirstName = Cashier.FirstName;
        //                }
        //                if (!string.IsNullOrEmpty(Cashier.LastName))
        //                {
        //                    _HCUAccount.LastName = Cashier.LastName;
        //                }

        //                if (_AddressResponse != null)
        //                {
        //                    if (!string.IsNullOrEmpty(_AddressResponse.Address))
        //                    {
        //                        _HCUAccount.Address = _AddressResponse.Address;
        //                    }
        //                    if (_AddressResponse.CityAreaId != 0)
        //                    {
        //                        _HCUAccount.CityAreaId = _AddressResponse.CityAreaId;
        //                    }
        //                    if (_AddressResponse.CityId != 0)
        //                    {
        //                        _HCUAccount.CityId = _AddressResponse.CityId;
        //                    }
        //                    if (_AddressResponse.StateId != 0)
        //                    {
        //                        _HCUAccount.RegionId = _AddressResponse.StateId;
        //                    }
        //                    if (_AddressResponse.CountryId != 0)
        //                    {
        //                        _HCUAccount.CountryId = _AddressResponse.CountryId;
        //                    }
        //                    if (_AddressResponse.Latitude != 0)
        //                    {
        //                        _HCUAccount.Latitude = _AddressResponse.Latitude;
        //                    }
        //                    if (_AddressResponse.Longitude != 0)
        //                    {
        //                        _HCUAccount.Longitude = _AddressResponse.Longitude;
        //                    }
        //                }
        //                _HCUAccount.EmailVerificationStatus = 0;
        //                _HCUAccount.NumberVerificationStatus = 0;
        //                _HCUAccount.RegistrationSourceId = RegistrationSource.System;
        //                _HCUAccount.StatusId = HelperStatus.Default.Active;
        //                _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
        //                _HCUAccounts.Add(_HCUAccount);
        //                HCUAccountAuth _HCUAccountAuth = new HCUAccountAuth();
        //                _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
        //                _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(6);
        //                _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
        //                _HCUAccountAuth.SecondaryPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
        //                _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
        //                _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
        //                if (_Request.UserReference.AccountId != 0)
        //                {
        //                    _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
        //                }
        //                _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
        //                _HCUAccountAuth.HCUAccount = _HCUAccounts;
        //                _HCoreContext.HCUAccountAuth.Add(_HCUAccountAuth);
        //                _HCoreContext.SaveChanges();
        //                long? IconStorageId = null;
        //                if (Cashier.IconContent != null && !string.IsNullOrEmpty(Cashier.IconContent.Content))
        //                {
        //                    IconStorageId = HCoreHelper.SaveStorage(Cashier.IconContent.Name, Cashier.IconContent.Extension, Cashier.IconContent.Content, null, _Request.UserReference);
        //                }
        //                if (IconStorageId != null)
        //                {
        //                    using (HCoreContext _HCoreContextUpload = new HCoreContext())
        //                    {
        //                        var UserAccDetails = _HCoreContextUpload.HCUAccount.Where(x => x.Id == _HCUAccount.Id).FirstOrDefault();
        //                        if (UserAccDetails != null)
        //                        {
        //                            if (IconStorageId != null)
        //                            {
        //                                UserAccDetails.IconStorageId = IconStorageId;
        //                            }
        //                            _HCoreContext.SaveChanges();
        //                        }
        //                    }
        //                }

        //            }
        //        }
        //        OCoreCashier.Response _Response = new OCoreCashier.Response();
        //        _Response.Status = HCoreConstant.ResponseStatus.Success;
        //        _Response.StatusCode = TUCCoreResource.CA1106;
        //        _Response.Message = TUCCoreResource.CA1106M;
        //        return _Response;
        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException("Core-SaveCashier", _Exception, _Request.UserReference);
        //        OCoreCashier.Response _Response = new OCoreCashier.Response();
        //        _Response.Status = HCoreConstant.ResponseStatus.Error;
        //        _Response.StatusCode = TUCCoreResource.CA0500;
        //        _Response.Message = TUCCoreResource.CA0500M;
        //        return _Response;
        //    }
        //}
        //internal static OCoreCashier.Response UpdateCashier(OCoreCashier.Request _Request)
        //{
        //    try
        //    {
        //        OAddressResponse _AddressResponse = HCoreHelper.GetAddressIds(_Request.Address, _Request.UserReference);
        //        #region Operation
        //        using (HCoreContext _HCoreContext = new HCoreContext())
        //        {
        //            var Details = _HCoreContext.HCUAccount
        //                .Where(x => x.Guid == _Request.ReferenceKey
        //                && x.Id == _Request.ReferenceId
        //                && x.AccountTypeId == UserAccountType.MerchantCashier)
        //                .FirstOrDefault();
        //            if (Details != null)
        //            {
        //                if (!string.IsNullOrEmpty(_Request.GenderCode))
        //                {
        //                    if (_Request.GenderCode == "gender.male")
        //                    {
        //                        Details.GenderId = Gender.Male;
        //                    }
        //                    if (_Request.GenderCode == "gender.female")
        //                    {
        //                        Details.GenderId = Helpers.Gender.Female;
        //                    }
        //                    if (_Request.GenderCode == "gender.other")
        //                    {
        //                        Details.GenderId = Helpers.Gender.Other;
        //                    }
        //                }
        //                if (_Request.StoreId != 0)
        //                {
        //                    Details.OwnerId = _Request.StoreId;
        //                }
        //                if (!string.IsNullOrEmpty(_Request.FirstName) && Details.Name != _Request.FirstName)
        //                {
        //                    Details.FirstName = _Request.FirstName;
        //                }
        //                if (!string.IsNullOrEmpty(_Request.FirstName) && Details.Name != _Request.FirstName)
        //                {
        //                    Details.LastName = _Request.LastName;
        //                }
        //                Details.Name = Details.FirstName + " " + Details.LastName;
        //                if (!string.IsNullOrEmpty(_Request.ContactNumber) && Details.ContactNumber != _Request.ContactNumber)
        //                {
        //                    Details.ContactNumber = _Request.ContactNumber;
        //                }
        //                if (!string.IsNullOrEmpty(_Request.EmailAddress) && Details.EmailAddress != _Request.EmailAddress)
        //                {
        //                    Details.EmailAddress = _Request.EmailAddress;
        //                }
        //                if (!string.IsNullOrEmpty(_Request.EmployeeCode) && Details.ReferralCode != _Request.EmployeeCode)
        //                {
        //                    Details.ReferralCode = _Request.EmployeeCode;
        //                }
        //                if (_AddressResponse != null)
        //                {
        //                    if (!string.IsNullOrEmpty(_AddressResponse.Address) && Details.Address != _AddressResponse.Address)
        //                    {
        //                        Details.Address = _AddressResponse.Address;
        //                    }
        //                    if (_AddressResponse.CityAreaId != 0)
        //                    {
        //                        Details.CityAreaId = _AddressResponse.CityAreaId;
        //                    }
        //                    if (_AddressResponse.CityId != 0)
        //                    {
        //                        Details.CityId = _AddressResponse.CityId;
        //                    }
        //                    if (_AddressResponse.StateId != 0)
        //                    {
        //                        Details.RegionId = _AddressResponse.StateId;
        //                    }
        //                    if (_AddressResponse.CountryId != 0)
        //                    {
        //                        Details.CountryId = _AddressResponse.CountryId;
        //                    }
        //                }
        //                _HCoreContext.SaveChanges();
        //                long? IconStorageId = null;
        //                if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
        //                {
        //                    IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, Details.IconStorageId, _Request.UserReference);
        //                }
        //                if (IconStorageId != null)
        //                {
        //                    using (HCoreContext _HCoreContextUpdate = new HCoreContext())
        //                    {
        //                        var UserAccDetails = _HCoreContextUpdate.HCUAccount.Where(x => x.Id == _Request.ReferenceId).FirstOrDefault();
        //                        if (UserAccDetails != null)
        //                        {
        //                            if (IconStorageId != null)
        //                            {
        //                                UserAccDetails.IconStorageId = IconStorageId;
        //                            }
        //                            _HCoreContext.SaveChanges();
        //                        }
        //                    }
        //                }
        //                OCoreCashier.Response _Response = new OCoreCashier.Response();
        //                _Response.Status = HCoreConstant.ResponseStatus.Error;
        //                _Response.StatusCode = TUCCoreResource.CA1076;
        //                _Response.Message = TUCCoreResource.CA1076M;
        //                _Response.ReferenceId = _Response.ReferenceId;
        //                _Response.ReferenceKey = _Response.ReferenceKey;
        //                return _Response;
        //            }
        //            else
        //            {
        //                OCoreCashier.Response _Response = new OCoreCashier.Response();
        //                _Response.Status = HCoreConstant.ResponseStatus.Error;
        //                _Response.StatusCode = TUCCoreResource.CA0404;
        //                _Response.Message = TUCCoreResource.CA0404M;
        //                return _Response;
        //            }

        //        }
        //        #endregion
        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException("Core-SaveCashier", _Exception, _Request.UserReference);
        //        OCoreCashier.Response _Response = new OCoreCashier.Response();
        //        _Response.Status = HCoreConstant.ResponseStatus.Error;
        //        _Response.StatusCode = TUCCoreResource.CA0500;
        //        _Response.Message = TUCCoreResource.CA0500M;
        //        return _Response;
        //    }
        //}
    }
}
