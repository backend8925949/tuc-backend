//==================================================================================
// FileName: CoreAdmin.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Helper;
using HCore.TUC.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
namespace HCore.TUC.Core.CoreAccount
{
    public class CoreAdmin
    {
        Random _Random;
        HCUAccountAuth _HCUAccountAuth;
        HCoreContext _HCoreContext;
        OCoreAdmin.Response _AdminResponse;
        OCoreAdmin.AuthUpdate.Response _AdminAuthUpdateResponse;
        HCUAccount _HCUAccount;
        internal OCoreAdmin.Response SaveAdmin(OCoreAdmin.Request _Request)
        {
            _AdminResponse = new OCoreAdmin.Response();
            _AdminResponse.Status = HCoreConstant.ResponseStatus.Error;
            _AdminResponse.StatusCode = TUCCoreResource.CA0500;
            _AdminResponse.Message = TUCCoreResource.CA0500M;
            try
            {
                if (string.IsNullOrEmpty(_Request.OwnerKey))
                {
                    _AdminResponse.StatusCode = TUCCoreResource.CA1188;
                    _AdminResponse.Message = TUCCoreResource.CA1188M;
                    return _AdminResponse;
                }
                if (string.IsNullOrEmpty(_Request.FirstName))
                {
                    _AdminResponse.StatusCode = TUCCoreResource.CA1183;
                    _AdminResponse.Message = TUCCoreResource.CA1183M;
                    return _AdminResponse;
                }
                if (string.IsNullOrEmpty(_Request.LastName))
                {
                    _AdminResponse.StatusCode = TUCCoreResource.CA1184;
                    _AdminResponse.Message = TUCCoreResource.CA1184M;
                    return _AdminResponse;
                }
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    _AdminResponse.StatusCode = TUCCoreResource.CA1141;
                    _AdminResponse.Message = TUCCoreResource.CA1141M;
                    return _AdminResponse;
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    _AdminResponse.StatusCode = TUCCoreResource.CA1142;
                    _AdminResponse.Message = TUCCoreResource.CA1142M;
                    return _AdminResponse;
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    _AdminResponse.StatusCode = TUCCoreResource.CA1143;
                    _AdminResponse.Message = TUCCoreResource.CA1143M;
                    return _AdminResponse;
                }
                if (string.IsNullOrEmpty(_Request.GenderCode))
                {
                    _AdminResponse.StatusCode = TUCCoreResource.CA1185;
                    _AdminResponse.Message = TUCCoreResource.CA1185M;
                    return _AdminResponse;
                }
                if (string.IsNullOrEmpty(_Request.RoleKey))
                {
                    _AdminResponse.StatusCode = TUCCoreResource.CA1186;
                    _AdminResponse.Message = TUCCoreResource.CA1186M;
                    return _AdminResponse;
                }
                int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == 0)
                {
                    _AdminResponse.StatusCode = TUCCoreResource.CA1149;
                    _AdminResponse.Message = TUCCoreResource.CA1149M;
                    return _AdminResponse;
                }
                int GenderId = HCoreHelper.GetStatusId(_Request.GenderCode, _Request.UserReference);
                if (GenderId == 0)
                {
                    _AdminResponse.StatusCode = TUCCoreResource.CA1190;
                    _AdminResponse.Message = TUCCoreResource.CA1190M;
                    return _AdminResponse;
                }
                long RoleId = HCoreHelper.GetRoleId(_Request.RoleKey);
                if (RoleId == 0)
                {
                    _AdminResponse.StatusCode = TUCCoreResource.CA1197;
                    _AdminResponse.Message = TUCCoreResource.CA1197M;
                    return _AdminResponse;
                }


                using (_HCoreContext = new HCoreContext())
                {
                    bool Details = _HCoreContext.HCUAccount
                        .Any(x => x.User.Username == _Request.EmailAddress);
                    if (Details)
                    {
                        _HCoreContext.Dispose();
                        _AdminResponse.Status = HCoreConstant.ResponseStatus.Error;
                        _AdminResponse.StatusCode = TUCCoreResource.CA1187;
                        _AdminResponse.Message = TUCCoreResource.CA1187M;
                        return _AdminResponse;
                    }
                    long OwnerId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.OwnerKey && x.AccountTypeId == UserAccountType.Admin).Select(x => x.Id).FirstOrDefault();
                    if (OwnerId == 0)
                    {
                        _HCoreContext.Dispose();
                        _AdminResponse.Status = HCoreConstant.ResponseStatus.Error;
                        _AdminResponse.StatusCode = TUCCoreResource.CA1189;
                        _AdminResponse.Message = TUCCoreResource.CA1189M;
                        return _AdminResponse;
                    }
                    _Random = new Random();
                    string AccountCode = _Random.Next(10000000, 999999999).ToString() + "" + _Random.Next(10000000, 999999999).ToString();
                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = _Request.EmailAddress;
                    string Password = HCoreHelper.GenerateRandomNumber(10);
                    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(Password);
                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.OwnerId = OwnerId;
                    _HCUAccount.User = _HCUAccountAuth;
                    _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccount.AccountTypeId = UserAccountType.Admin;
                    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = AccountCode;
                    if (_Request.FirstName.Length > 30)
                    {
                        _HCUAccount.DisplayName = _Request.FirstName.Substring(0, 29);
                    }
                    else
                    {
                        _HCUAccount.DisplayName = _Request.FirstName;
                    }
                    _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                    _HCUAccount.Name = _Request.FirstName + " " + _Request.LastName;
                    _HCUAccount.FirstName = _Request.FirstName;
                    _HCUAccount.LastName = _Request.LastName;
                    _HCUAccount.MobileNumber = _Request.MobileNumber;
                    _HCUAccount.EmailAddress = _Request.EmailAddress;
                    _HCUAccount.Address = _Request.Address;
                    if (_Request.DateOfBirth != null)
                    {
                        _HCUAccount.DateOfBirth = _Request.DateOfBirth;
                    }
                    _HCUAccount.RoleId = RoleId;
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.RegistrationSourceId = RegistrationSource.System;
                    _HCUAccount.GenderId = GenderId;
                    _HCUAccount.CountValue = 0;
                    _HCUAccount.AverageValue = 0;
                    _HCUAccount.CountryId = _Request.UserReference.CountryId;
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    }
                    if (StatusId > 0)
                    {
                        _HCUAccount.StatusId = StatusId;
                    }
                    else
                    {
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                    }
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    _HCoreContext.SaveChanges();

                    long? IconStorageId = null;
                    if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                    {
                        IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                    }
                    if (IconStorageId != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var UserAccDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _HCUAccount.Id).FirstOrDefault();
                            if (UserAccDetails != null)
                            {
                                if (IconStorageId != null)
                                {
                                    UserAccDetails.IconStorageId = IconStorageId;
                                }
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    _AdminResponse.Status = ResponseStatus.Success;
                    _AdminResponse.StatusCode = TUCCoreResource.CA1152;
                    _AdminResponse.Message = TUCCoreResource.CA1152M;
                    _AdminResponse.ReferenceId = _HCUAccount.Id;
                    _AdminResponse.ReferenceKey = _HCUAccount.Guid;
                    var _EmailParameters = new
                    {
                        DisplayName = _HCUAccount.DisplayName,
                        UserName = _HCUAccountAuth.Username,
                        Password = Password,
                        AccessPin = HCoreEncrypt.DecryptHash(_HCUAccount.AccessPin)
                    };
                    HCoreHelper.BroadCastAdminEmail(HelperEmailTemplate.CredentailsEmail, _HCUAccount.DisplayName, _HCUAccount.EmailAddress, _EmailParameters, _Request.UserReference);
                    return _AdminResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Core-SaveAdmin", _Exception, _Request.UserReference);
                return _AdminResponse;
            }
        }
        internal OCoreAdmin.Response UpdateAdmin(OCoreAdmin.Request _Request)
        {
            _AdminResponse = new OCoreAdmin.Response();
            _AdminResponse.Status = HCoreConstant.ResponseStatus.Error;
            _AdminResponse.StatusCode = TUCCoreResource.CA0500;
            _AdminResponse.Message = TUCCoreResource.CA0500M;
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    _AdminResponse.StatusCode = TUCCoreResource.CAREF;
                    _AdminResponse.Message = TUCCoreResource.CAREFM;
                    return _AdminResponse;
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    _AdminResponse.StatusCode = TUCCoreResource.CAREFKEY;
                    _AdminResponse.Message = TUCCoreResource.CAREFKEYM;
                    return _AdminResponse;
                }

                int GenderId = 0;
                if (!string.IsNullOrEmpty(_Request.GenderCode))
                {
                    GenderId = HCoreHelper.GetStatusId(_Request.GenderCode, _Request.UserReference);
                    if (GenderId == 0)
                    {
                        _AdminResponse.StatusCode = TUCCoreResource.CA1190;
                        _AdminResponse.Message = TUCCoreResource.CA1190M;
                        return _AdminResponse;
                    }
                }
                long RoleId = 0;
                if (!string.IsNullOrEmpty(_Request.RoleKey))
                {
                    RoleId = HCoreHelper.GetRoleId(_Request.RoleKey);
                    if (RoleId == 0)
                    {
                        _AdminResponse.StatusCode = TUCCoreResource.CA1197;
                        _AdminResponse.Message = TUCCoreResource.CA1197M;
                        return _AdminResponse;
                    }
                }


                using (_HCoreContext = new HCoreContext())
                {
                    HCUAccount _Details = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.ReferenceId
                        && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_Details == null)
                    {
                        _HCoreContext.Dispose();
                        _AdminResponse.StatusCode = TUCCoreResource.CA0404;
                        _AdminResponse.Message = TUCCoreResource.CA0404M;
                        return _AdminResponse;
                    }

                    long OwnerId = 0;
                    if (!string.IsNullOrEmpty(_Request.OwnerKey))
                    {
                        OwnerId = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.OwnerKey && x.AccountTypeId == UserAccountType.Admin).Select(x => x.Id).FirstOrDefault();
                        if (OwnerId == 0)
                        {
                            _HCoreContext.Dispose();
                            _AdminResponse.Status = HCoreConstant.ResponseStatus.Error;
                            _AdminResponse.StatusCode = TUCCoreResource.CA1189;
                            _AdminResponse.Message = TUCCoreResource.CA1189M;
                            return _AdminResponse;
                        }
                    }


                    if (!string.IsNullOrEmpty(_Request.DisplayName) && _Details.DisplayName != _Request.DisplayName)
                    {
                        _Details.DisplayName = _Request.DisplayName;
                    }
                    if (!string.IsNullOrEmpty(_Request.FirstName) && _Details.FirstName != _Request.FirstName)
                    {
                        _Details.FirstName = _Request.FirstName;
                    }
                    if (!string.IsNullOrEmpty(_Request.LastName) && _Details.LastName != _Request.LastName)
                    {
                        _Details.LastName = _Request.LastName;
                    }
                    _Details.Name = _Details.FirstName + " " + _Details.LastName;
                    if (!string.IsNullOrEmpty(_Request.EmailAddress) && _Details.EmailAddress != _Request.EmailAddress)
                    {
                        _Details.EmailAddress = _Request.EmailAddress;
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Details.EmailAddress != _Request.MobileNumber)
                    {
                        _Details.MobileNumber = _Request.MobileNumber;
                        _Details.ContactNumber = _Request.MobileNumber;
                    }
                    if (!string.IsNullOrEmpty(_Request.Address) && _Details.Address != _Request.Address)
                    {
                        _Details.Address = _Request.Address;
                    }
                    if (_Request.DateOfBirth != null)
                    {
                        _Details.DateOfBirth = _Request.DateOfBirth;
                    }
                    if (GenderId > 0)
                    {
                        _Details.GenderId = GenderId;
                    }
                    if (RoleId > 0)
                    {
                        _Details.RoleId = RoleId;
                    }
                    if (OwnerId > 0)
                    {
                        _Details.OwnerId = OwnerId;
                    }
                    int StatusId = 0;
                    if (!string.IsNullOrEmpty(_Request.StatusCode))
                    {
                        StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                        if (StatusId == 0)
                        {
                            _AdminResponse.StatusCode = TUCCoreResource.CAINSTATUS;
                            _AdminResponse.Message = TUCCoreResource.CAINSTATUSM;
                            return _AdminResponse;
                        }
                        _Details.StatusId = StatusId;
                    }
                    _Details.ModifyById = _Request.UserReference.AccountId;
                    _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                    _HCoreContext.SaveChanges();
                    long? IconStorageId = null;
                    if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                    {
                        IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, _Details.IconStorageId, _Request.UserReference);
                    }
                    if (IconStorageId != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var UserAccDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Details.Id).FirstOrDefault();
                            if (UserAccDetails != null)
                            {
                                if (IconStorageId != null)
                                {
                                    UserAccDetails.IconStorageId = IconStorageId;
                                }
                                else if (IconStorageId == null)
                                {
                                    UserAccDetails.IconStorageId = null;
                                }
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    _AdminResponse.Status = ResponseStatus.Success;
                    _AdminResponse.StatusCode = TUCCoreResource.CA1191;
                    _AdminResponse.Message = TUCCoreResource.CA1191M;
                    _AdminResponse.ReferenceId = _Details.Id;
                    _AdminResponse.ReferenceKey = _Details.Guid;



                    return _AdminResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Core-UpdateAdmin", _Exception, _Request.UserReference);
                return _AdminResponse;
            }
        }
        internal OCoreAdmin.AuthUpdate.Response UpdateAdminUsername(OCoreAdmin.AuthUpdate.Request _Request)
        {
            _AdminAuthUpdateResponse = new OCoreAdmin.AuthUpdate.Response();
            _AdminAuthUpdateResponse.Status = ResponseStatus.Error;
            _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA0500;
            _AdminAuthUpdateResponse.Message = TUCCoreResource.CA0500M;
            try
            {
                if (_Request.AccountId == 0)
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CAACCREF;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CAACCREFM;
                    return _AdminAuthUpdateResponse;
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CAACCREFKEY;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CAACCREFKEYM;
                    return _AdminAuthUpdateResponse;
                }
                if (string.IsNullOrEmpty(_Request.UserName))
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1336;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1336M;
                    return _AdminAuthUpdateResponse;
                }
                if (_Request.UserName.Length < 8)
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1338;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1338M;
                    return _AdminAuthUpdateResponse;
                }
                if (_Request.UserName.Length > 128)
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1339;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1339M;
                    return _AdminAuthUpdateResponse;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    HCUAccount _Details = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.AccountId
                        && x.Guid == _Request.AccountKey
                        && x.AccountTypeId == UserAccountType.Admin).FirstOrDefault();
                    if (_Details == null)
                    {
                        _HCoreContext.Dispose();
                        _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA0404;
                        _AdminAuthUpdateResponse.Message = TUCCoreResource.CA0404M;
                        return _AdminAuthUpdateResponse;
                    }
                    var UserNameCheck = _HCoreContext.HCUAccountAuth.Any(x => x.Username == _Request.UserName);
                    if (UserNameCheck)
                    {
                        _HCoreContext.Dispose();
                        _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1337;
                        _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1337M;
                        return _AdminAuthUpdateResponse;
                    }
                    var User = _HCoreContext.HCUAccountAuth.Where(x => x.Id == _Details.UserId).FirstOrDefault();
                    if (_Details == null)
                    {
                        _HCoreContext.Dispose();
                        _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA0404;
                        _AdminAuthUpdateResponse.Message = TUCCoreResource.CA0404M;
                        return _AdminAuthUpdateResponse;
                    }
                    User.Username = _Request.UserName;
                    User.ModifyById = _Request.UserReference.AccountId;
                    User.ModifyDate = HCoreHelper.GetGMTDateTime();

                    _Details.ModifyById = _Request.UserReference.AccountId;
                    _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                    _HCoreContext.SaveChanges();
                    _AdminAuthUpdateResponse.Status = ResponseStatus.Success;
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1346;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1346M;
                    _AdminAuthUpdateResponse.UserName = _Request.UserName;
                    return _AdminAuthUpdateResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Core-UpdateAdminUsername", _Exception, _Request.UserReference);
                return _AdminAuthUpdateResponse;
            }
        }
        internal OCoreAdmin.AuthUpdate.Response UpdateAdminPassword(OCoreAdmin.AuthUpdate.Request _Request)
        {
            _AdminAuthUpdateResponse = new OCoreAdmin.AuthUpdate.Response();
            _AdminAuthUpdateResponse.Status = ResponseStatus.Error;
            _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA0500;
            _AdminAuthUpdateResponse.Message = TUCCoreResource.CA0500M;
            try
            {
                if (_Request.AccountId == 0)
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CAACCREF;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CAACCREFM;
                    return _AdminAuthUpdateResponse;
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CAACCREFKEY;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CAACCREFKEYM;
                    return _AdminAuthUpdateResponse;
                }
                if (string.IsNullOrEmpty(_Request.OldPassword))
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1340;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1340M;
                    return _AdminAuthUpdateResponse;
                }
                if (string.IsNullOrEmpty(_Request.NewPassword))
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1341;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1341M;
                    return _AdminAuthUpdateResponse;
                }
                if (_Request.NewPassword.Length < 8)
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1342;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1342M;
                    return _AdminAuthUpdateResponse;
                }
                if (_Request.NewPassword.Length > 128)
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1343;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1343M;
                    return _AdminAuthUpdateResponse;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    HCUAccount _Details = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.AccountId
                        && x.Guid == _Request.AccountKey
                        && x.AccountTypeId == UserAccountType.Admin).FirstOrDefault();
                    if (_Details == null)
                    {
                        _HCoreContext.Dispose();
                        _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA0404;
                        _AdminAuthUpdateResponse.Message = TUCCoreResource.CA0404M;
                        return _AdminAuthUpdateResponse;
                    }
                    //var UserNameCheck = _HCoreContext.HCUAccountAuth.Any(x => x.Username == _Request.UserName);
                    //if (UserNameCheck)
                    //{
                    //    _HCoreContext.Dispose();
                    //    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1337;
                    //    _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1337M;
                    //    return _AdminAuthUpdateResponse;
                    //}
                    var User = _HCoreContext.HCUAccountAuth.Where(x => x.Id == _Details.UserId).FirstOrDefault();
                    if (_Details == null)
                    {
                        _HCoreContext.Dispose();
                        _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA0404;
                        _AdminAuthUpdateResponse.Message = TUCCoreResource.CA0404M;
                        return _AdminAuthUpdateResponse;
                    }
                    var ActivePassword = HCoreEncrypt.DecryptHash(User.Password);
                    if (ActivePassword != _Request.OldPassword)
                    {
                        _HCoreContext.Dispose();
                        _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1344;
                        _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1344M;
                        return _AdminAuthUpdateResponse;
                    }


                    User.Password = HCoreEncrypt.EncryptHash(_Request.NewPassword);
                    User.ModifyById = _Request.UserReference.AccountId;
                    User.ModifyDate = HCoreHelper.GetGMTDateTime();

                    _Details.ModifyById = _Request.UserReference.AccountId;
                    _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                    _HCoreContext.SaveChanges();
                    _AdminAuthUpdateResponse.Status = ResponseStatus.Success;
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1345;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1345M;
                    _AdminAuthUpdateResponse.NewPassword = _Request.NewPassword;
                    return _AdminAuthUpdateResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Core-UpdateAdminPassword", _Exception, _Request.UserReference);
                return _AdminAuthUpdateResponse;
            }
        }
        internal OCoreAdmin.AuthUpdate.Response UpdateAdminResetPassword(OCoreAdmin.AuthUpdate.Request _Request)
        {
            _AdminAuthUpdateResponse = new OCoreAdmin.AuthUpdate.Response();
            _AdminAuthUpdateResponse.Status = ResponseStatus.Error;
            _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA0500;
            _AdminAuthUpdateResponse.Message = TUCCoreResource.CA0500M;
            try
            {
                if (_Request.AccountId == 0)
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CAACCREF;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CAACCREFM;
                    return _AdminAuthUpdateResponse;
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CAACCREFKEY;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CAACCREFKEYM;
                    return _AdminAuthUpdateResponse;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    HCUAccount _Details = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.AccountId
                        && x.Guid == _Request.AccountKey
                        && x.AccountTypeId == UserAccountType.Admin).FirstOrDefault();
                    if (_Details == null)
                    {
                        _HCoreContext.Dispose();
                        _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA0404;
                        _AdminAuthUpdateResponse.Message = TUCCoreResource.CA0404M;
                        return _AdminAuthUpdateResponse;
                    }
                    var User = _HCoreContext.HCUAccountAuth.Where(x => x.Id == _Details.UserId).FirstOrDefault();
                    if (_Details == null)
                    {
                        _HCoreContext.Dispose();
                        _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA0404;
                        _AdminAuthUpdateResponse.Message = TUCCoreResource.CA0404M;
                        return _AdminAuthUpdateResponse;
                    }
                    string NewPassword = HCoreHelper.GenerateRandomNumber(10);
                    User.Password = HCoreEncrypt.EncryptHash(NewPassword);
                    User.ModifyById = _Request.UserReference.AccountId;
                    User.ModifyDate = HCoreHelper.GetGMTDateTime();

                    _Details.ModifyById = _Request.UserReference.AccountId;
                    _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                    _HCoreContext.SaveChanges();
                    _AdminAuthUpdateResponse.Status = ResponseStatus.Success;
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1345;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1345M;
                    _AdminAuthUpdateResponse.NewPassword = NewPassword;
                    return _AdminAuthUpdateResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Core-UpdateAdminResetPassword", _Exception, _Request.UserReference);
                return _AdminAuthUpdateResponse;
            }
        }
        internal OCoreAdmin.AuthUpdate.Response UpdateAdminPin(OCoreAdmin.AuthUpdate.Request _Request)
        {
            _AdminAuthUpdateResponse = new OCoreAdmin.AuthUpdate.Response();
            _AdminAuthUpdateResponse.Status = ResponseStatus.Error;
            _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA0500;
            _AdminAuthUpdateResponse.Message = TUCCoreResource.CA0500M;
            try
            {
                if (_Request.AccountId == 0)
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CAACCREF;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CAACCREFM;
                    return _AdminAuthUpdateResponse;
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CAACCREFKEY;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CAACCREFKEYM;
                    return _AdminAuthUpdateResponse;
                }
                if (string.IsNullOrEmpty(_Request.OldPin))
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1347;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1347M;
                    return _AdminAuthUpdateResponse;
                }
                if (string.IsNullOrEmpty(_Request.NewPin))
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1348;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1348M;
                    return _AdminAuthUpdateResponse;
                }
                if (_Request.OldPin.Length != 4)
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1349;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1349M;
                    return _AdminAuthUpdateResponse;
                }
                if (_Request.NewPin.Length != 4)
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1349;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1349M;
                    return _AdminAuthUpdateResponse;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    HCUAccount _Details = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.AccountId
                        && x.Guid == _Request.AccountKey
                        && x.AccountTypeId == UserAccountType.Admin).FirstOrDefault();
                    if (_Details == null)
                    {
                        _HCoreContext.Dispose();
                        _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA0404;
                        _AdminAuthUpdateResponse.Message = TUCCoreResource.CA0404M;
                        return _AdminAuthUpdateResponse;
                    }
                    var ActivePin = HCoreEncrypt.DecryptHash(_Details.AccessPin);
                    if (ActivePin != _Request.OldPin)
                    {
                        _HCoreContext.Dispose();
                        _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1344;
                        _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1344M;
                        return _AdminAuthUpdateResponse;
                    }
                    _Details.AccessPin = HCoreEncrypt.EncryptHash(_Request.NewPin);
                    _Details.ModifyById = _Request.UserReference.AccountId;
                    _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                    _HCoreContext.SaveChanges();
                    _AdminAuthUpdateResponse.Status = ResponseStatus.Success;
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1350;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1350M;
                    _AdminAuthUpdateResponse.NewPin = _Request.NewPin;
                    return _AdminAuthUpdateResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Core-UpdateAdminPin", _Exception, _Request.UserReference);
                return _AdminAuthUpdateResponse;
            }
        }
        internal OCoreAdmin.AuthUpdate.Response UpdateAdminResetPin(OCoreAdmin.AuthUpdate.Request _Request)
        {
            _AdminAuthUpdateResponse = new OCoreAdmin.AuthUpdate.Response();
            _AdminAuthUpdateResponse.Status = ResponseStatus.Error;
            _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA0500;
            _AdminAuthUpdateResponse.Message = TUCCoreResource.CA0500M;
            try
            {
                if (_Request.AccountId == 0)
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CAACCREF;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CAACCREFM;
                    return _AdminAuthUpdateResponse;
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CAACCREFKEY;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CAACCREFKEYM;
                    return _AdminAuthUpdateResponse;
                }
                _Request.NewPin = HCoreHelper.GenerateRandomNumber(4);
                using (_HCoreContext = new HCoreContext())
                {
                    HCUAccount _Details = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.AccountId
                        && x.Guid == _Request.AccountKey
                        && x.AccountTypeId == UserAccountType.Admin).FirstOrDefault();
                    if (_Details == null)
                    {
                        _HCoreContext.Dispose();
                        _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA0404;
                        _AdminAuthUpdateResponse.Message = TUCCoreResource.CA0404M;
                        return _AdminAuthUpdateResponse;
                    }
                    _Details.AccessPin = HCoreEncrypt.EncryptHash(_Request.NewPin);
                    _Details.ModifyById = _Request.UserReference.AccountId;
                    _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                    _HCoreContext.SaveChanges();
                    _AdminAuthUpdateResponse.Status = ResponseStatus.Success;
                    _AdminAuthUpdateResponse.StatusCode = TUCCoreResource.CA1351;
                    _AdminAuthUpdateResponse.Message = TUCCoreResource.CA1351M;
                    _AdminAuthUpdateResponse.NewPin = _Request.NewPin;
                    return _AdminAuthUpdateResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Core-UpdateAdminResetPin", _Exception, _Request.UserReference);
                return _AdminAuthUpdateResponse;
            }
        }
    }
}
