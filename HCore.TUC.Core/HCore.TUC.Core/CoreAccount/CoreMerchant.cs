//==================================================================================
// FileName: CoreMerchant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Framework.Operations;
using HCore.TUC.Core.Resource;
using Pipedrive;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.CoreAccount
{
    internal class CoreMerchant
    {
        Random _Random;
        HCoreContext _HCoreContext;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        HCUAccountParameter _HCUAccountParameter;
        List<HCUAccountParameter> _HCUAccountParameters;
        TUCTerminal _TUCTerminal;
        TUCBranchAccount _TUCBranchAccount;
        OCoreMerchant.Response _MerchantResponse;
        OCoreMerchant.Configuration _MerchantConfiguration;
        List<TUCCategoryAccount> _TUCCategoryAccount;
        internal OCoreMerchant.Response SaveMerchant(OCoreMerchant.Request _Request)
        {
            long MerchantId = 0;
            _MerchantResponse = new OCoreMerchant.Response
            {
                Status = ResponseStatus.Error,
                StatusCode = TUCCoreResource.CA0500,
                Message = TUCCoreResource.CA0500M
            };
            try
            {

                if (string.IsNullOrEmpty(_Request.DisplayName))
                {
                    _MerchantResponse.StatusCode = TUCCoreResource.CA1126;
                    _MerchantResponse.Message = TUCCoreResource.CA1126M;
                    return _MerchantResponse;
                }
                if (string.IsNullOrEmpty(_Request.CompanyName))
                {
                    _MerchantResponse.StatusCode = TUCCoreResource.CA1127;
                    _MerchantResponse.Message = TUCCoreResource.CA1127M;
                    return _MerchantResponse;
                }
                if (string.IsNullOrEmpty(_Request.ContactNumber))
                {
                    _MerchantResponse.StatusCode = TUCCoreResource.CA1128;
                    _MerchantResponse.Message = TUCCoreResource.CA1128M;
                    return _MerchantResponse;
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    _MerchantResponse.StatusCode = TUCCoreResource.CA1129;
                    _MerchantResponse.Message = TUCCoreResource.CA1129M;
                    return _MerchantResponse;
                }
                if (_Request.Address == null)
                {
                    _MerchantResponse.StatusCode = TUCCoreResource.CA1130;
                    _MerchantResponse.Message = TUCCoreResource.CA1130M;
                    return _MerchantResponse;
                }
                if (_Request.Address.Latitude == 0 && _Request.Address.Longitude == 0)
                {
                    _MerchantResponse.StatusCode = TUCCoreResource.CA1130;
                    _MerchantResponse.Message = TUCCoreResource.CA1130M;
                    return _MerchantResponse;
                }

                int StatusId = HelperStatus.Default.Active;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        _MerchantResponse.StatusCode = TUCCoreResource.CAINSTATUS;
                        _MerchantResponse.Message = TUCCoreResource.CAINSTATUSM;
                        return _MerchantResponse;
                    }
                }
                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.Address, _Request.UserReference);
                using (_HCoreContext = new HCoreContext())
                {
                    //long AccountOperationType = _HCoreContext.hc
                    if (string.IsNullOrEmpty(_Request.UserName))
                    {
                        _Request.UserName = _Request.EmailAddress;
                    }
                    bool ValidateUserName = _HCoreContext.HCUAccountAuth.Any(x => x.Username == _Request.UserName);
                    if (ValidateUserName)
                    {
                        _MerchantResponse.StatusCode = TUCCoreResource.CA1131;
                        _MerchantResponse.Message = TUCCoreResource.CA1131M;
                        return _MerchantResponse;
                    }
                    bool ValidateCompanyName = _HCoreContext.HCUAccount.Any(x => x.Name == _Request.CompanyName && x.AccountTypeId == UserAccountType.Merchant);
                    if (ValidateCompanyName)
                    {
                        _MerchantResponse.StatusCode = TUCCoreResource.CA1132;
                        _MerchantResponse.Message = TUCCoreResource.CA1132M;
                        return _MerchantResponse;
                    }
                    if (_Request.Configurations == null)
                    {
                        _MerchantConfiguration = new OCoreMerchant.Configuration();
                        _MerchantConfiguration.RewardDeductionTypeCode = "rewarddeductiontype.prepay";
                        _MerchantConfiguration.RewardPercentage = 0;
                        _Request.Configurations = _MerchantConfiguration;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(_Request.Configurations.RewardDeductionTypeCode))
                        {
                            _Request.Configurations.RewardDeductionTypeCode = "rewarddeductiontype.prepay";
                        }

                    }
                    if (string.IsNullOrEmpty(_Request.AccountOperationTypeCode))
                    {
                        _Request.AccountOperationTypeCode = "accountoperationtype.onlineandoffline";
                    }
                    if (_Request.AccountOperationTypeCode != "accountoperationtype.online"
                        && _Request.AccountOperationTypeCode != "accountoperationtype.offline"
                        && _Request.AccountOperationTypeCode != "accountoperationtype.onlineandoffline")
                    {
                        _MerchantResponse.StatusCode = TUCCoreResource.CA1133;
                        _MerchantResponse.Message = TUCCoreResource.CA1133M;
                        return _MerchantResponse;
                    }

                    _Random = new Random();
                    string MerchantKey = HCoreHelper.GenerateGuid();
                    string AccountCode = _Random.Next(100, 999).ToString() + _Random.Next(000000000, 999999999).ToString();
                    #region Parameters
                    _HCUAccountParameters = new List<HCUAccountParameter>();
                    #region Reward Percentage
                    if (_Request.Configurations.RewardPercentage > 0)
                    {
                        _HCUAccountParameter = new HCUAccountParameter();
                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                        _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage").Select(x => x.Id).FirstOrDefault();
                        _HCUAccountParameter.Value = _Request.Configurations.RewardPercentage.ToString();
                        _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                        }
                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                        _HCUAccountParameters.Add(_HCUAccountParameter);
                    }
                    #endregion
                    #region Account Reward Deduction Type
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                    _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewarddeductiontype").Select(x => x.Id).FirstOrDefault();
                    _HCUAccountParameter.Value = "Prepay";
                    _HCUAccountParameter.HelperId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.AccountOperationTypeCode).Select(x => x.Id).FirstOrDefault();
                    _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                    _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccountParameters.Add(_HCUAccountParameter);
                    #endregion
                    #region Set Categories
                    _TUCCategoryAccount = new List<TUCCategoryAccount>();
                    if (_Request.Categories != null && _Request.Categories.Count > 0)
                    {
                        foreach (var Category in _Request.Categories)
                        {
                            _TUCCategoryAccount.Add(new TUCCategoryAccount
                            {
                                Guid = HCoreHelper.GenerateGuid(),
                                TypeId = 1,
                                CategoryId = Category.ReferenceId,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                CreatedById = _Request.UserReference.AccountId,
                                StatusId = HelperStatus.Default.Active,
                            });
                        }
                    }
                    #endregion
                    #endregion
                    #region User Object
                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    if (!string.IsNullOrEmpty(_Request.UserName))
                    {
                        _HCUAccountAuth.Username = _Request.UserName;
                    }
                    else
                    {
                        _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(6);
                    }
                    if (!string.IsNullOrEmpty(_Request.Password))
                    {
                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                    }
                    else
                    {
                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    }
                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    #endregion
                    #region Save UserAccount
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.Guid = MerchantKey;
                    _HCUAccount.AccountTypeId = UserAccountType.Merchant;
                    if (_TUCCategoryAccount.Count > 0)
                    {
                        _HCUAccount.PrimaryCategoryId = _Request.Categories.FirstOrDefault().ReferenceId;
                        _HCUAccount.TUCCategoryAccountAccount = _TUCCategoryAccount;
                    }
                    int AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                    if (_Request.AccountOperationTypeCode == "accountoperationtype.online")
                    {
                        AccountOperationTypeId = AccountOperationType.Online;
                    }
                    if (_Request.AccountOperationTypeCode == "accountoperationtype.offline")
                    {
                        AccountOperationTypeId = AccountOperationType.Offline;
                    }
                    if (_Request.AccountOperationTypeCode == "accountoperationtype.onlineandoffline")
                    {
                        AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                    }
                    _HCUAccount.AccountOperationTypeId = AccountOperationTypeId;
                    if (_Request.OwnerId != 0)
                    {
                        _HCUAccount.OwnerId = _Request.OwnerId;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(_Request.ReferralCode))
                        {
                            long ReferrerId = _HCoreContext.HCUAccount
                                .Where(x => x.ReferralCode == _Request.ReferralCode
                                && (x.AccountTypeId == UserAccountType.Merchant || x.AccountTypeId == UserAccountType.Acquirer || x.AccountTypeId == UserAccountType.PgAccount || x.AccountTypeId == UserAccountType.PosAccount))
                                .Select(x => x.Id).FirstOrDefault();
                            if (ReferrerId > 0)
                            {
                                _HCUAccount.OwnerId = ReferrerId;
                            }
                            else
                            {
                                _MerchantResponse.StatusCode = TUCCoreResource.CA1458;
                                _MerchantResponse.Message = TUCCoreResource.CA1458M;
                                return _MerchantResponse;
                            }
                        }
                    }
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
                    //_HCUAccount.BankId = _Request.AccountId;
                    _HCUAccount.DisplayName = _Request.DisplayName;
                    _HCUAccount.FirstName = _Request.FirstName;
                    _HCUAccount.ReferralCode = _Request.ReferralCode;
                    _HCUAccount.Name = _Request.CompanyName;

                    _HCUAccount.EmailAddress = _Request.EmailAddress;
                    _HCUAccount.MobileNumber = _Request.MobileNumber;
                    _HCUAccount.ContactNumber = _Request.ContactNumber;

                    if (_Request.ContactPerson != null)
                    {
                        _HCUAccount.CpName = _Request.ContactPerson.FirstName + " " + _Request.ContactPerson.LastName;
                        _HCUAccount.CpFirstName = _Request.ContactPerson.FirstName;
                        _HCUAccount.CpLastName = _Request.ContactPerson.LastName;
                        _HCUAccount.CpMobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber, _Request.ContactPerson.MobileNumber.Length);
                        _HCUAccount.CpEmailAddress = _Request.ContactPerson.EmailAddress;

                        _HCUAccount.FirstName = _Request.FirstName;
                        _HCUAccount.LastName = _Request.ContactPerson.LastName;
                        _HCUAccount.SecondaryEmailAddress = _Request.ContactPerson.EmailAddress;
                        _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber, _Request.MobileNumber.Length);
                    }


                    if (_Request.StartDate != null)
                    {
                        _HCUAccount.DateOfBirth = _Request.StartDate;
                    }

                    if (_AddressResponse != null)
                    {
                        _HCUAccount.Address = _AddressResponse.Address;
                        _HCUAccount.Latitude = _AddressResponse.Latitude;
                        _HCUAccount.Longitude = _AddressResponse.Longitude;
                        _HCUAccount.CountryId = _AddressResponse.CountryId;
                        if (_AddressResponse.StateId != 0)
                        {
                            _HCUAccount.StateId = _AddressResponse.StateId;
                        }
                        if (_AddressResponse.CityId != 0)
                        {
                            _HCUAccount.CityId = _AddressResponse.CityId;
                        }
                        if (_AddressResponse.CityAreaId != 0)
                        {
                            _HCUAccount.CityAreaId = _AddressResponse.CityAreaId;
                        }
                    }
                    else
                    {
                        if (_HCUAccount.CountryId < 1)
                        {
                            _HCUAccount.CountryId = _Request.UserReference.SystemCountry;
                        }
                    }
                    _HCUAccount.WebsiteUrl = _Request.WebsiteUrl;
                    _HCUAccount.Description = _Request.Description;
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccount.CountValue = 0;
                    _HCUAccount.AverageValue = 0;
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.Configurations.RewardPercentage > 0)
                    {
                        _HCUAccount.AccountPercentage = _Request.Configurations.RewardPercentage;
                    }
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccount.StatusId = StatusId;
                    _HCUAccount.User = _HCUAccountAuth;
                    _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                    #endregion
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    _HCoreContext.SaveChanges();
                    MerchantId = _HCUAccount.Id;
                    if (_Request.IsDealMerchant)
                    {
                        FrameworkSubscription _FrameworkSubscription = new FrameworkSubscription();
                        _FrameworkSubscription.SetSubscriptionId(MerchantId, 23);
                    }
                    else
                    {
                        FrameworkSubscription _FrameworkSubscription = new FrameworkSubscription();
                        _FrameworkSubscription.SetSubscriptionId(MerchantId, 2);
                    }
                    if (_Request.Stores != null && _Request.Stores.Count == 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            #region Save Store
                            _Random = new Random();
                            string StoreAccountCode = _Random.Next(100000000, 999999999).ToString();
                            _HCUAccountAuth = new HCUAccountAuth();
                            _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(6);
                            _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                            _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                            _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                            _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                            }
                            _HCUAccountAuth.StatusId = HelperStatus.Default.Active;

                            _HCUAccount = new HCUAccount();
                            _HCUAccount.User = _HCUAccountAuth;
                            #region Set Categories
                            if (_Request.Categories != null && _Request.Categories.Count > 0)
                            {
                                _TUCCategoryAccount = new List<TUCCategoryAccount>();
                                _HCUAccount.PrimaryCategoryId = _Request.Categories.FirstOrDefault().ReferenceId;
                                foreach (var Category in _Request.Categories)
                                {
                                    _TUCCategoryAccount.Add(new TUCCategoryAccount
                                    {
                                        Guid = HCoreHelper.GenerateGuid(),
                                        TypeId = 1,
                                        CategoryId = Category.ReferenceId,
                                        CreateDate = HCoreHelper.GetGMTDateTime(),
                                        CreatedById = _Request.UserReference.AccountId,
                                        StatusId = HelperStatus.Default.Active,
                                    });
                                }
                                _HCUAccount.TUCCategoryAccountAccount = _TUCCategoryAccount;
                            }
                            #endregion
                            //if (_Request.Categories != null && _Request.Categories.Count > 0)
                            //{
                            //    _HCUAccountParameters = new List<HCUAccountParameter>();
                            //    foreach (var Category in _Request.Categories)
                            //    {
                            //        _HCUAccountParameters.Add(new HCUAccountParameter
                            //        {
                            //            Guid = HCoreHelper.GenerateGuid(),
                            //            TypeId = HelperType.MerchantCategory,
                            //            CommonId = Category.ReferenceId,
                            //            CreateDate = HCoreHelper.GetGMTDateTime(),
                            //            StatusId = HelperStatus.Default.Active,
                            //        });
                            //    }
                            //    _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                            //}
                            _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                            _HCUAccount.AccountOperationTypeId = AccountOperationTypeId;
                            _HCUAccount.OwnerId = MerchantId;
                            _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                            _HCUAccount.AccountCode = StoreAccountCode;
                            if (_Request.DisplayName.Length > 30)
                            {
                                _HCUAccount.DisplayName = _Request.DisplayName.Substring(0, 29);
                            }
                            else
                            {
                                _HCUAccount.DisplayName = _Request.DisplayName;
                            }
                            _HCUAccount.ReferralCode = _Request.ReferralCode;
                            _HCUAccount.Name = _Request.CompanyName;
                            _HCUAccount.FirstName = _Request.BusinessOwnerName;
                            _HCUAccount.ContactNumber = _Request.ContactNumber;
                            _HCUAccount.EmailAddress = _Request.EmailAddress;
                            if (_Request.ContactPerson != null)
                            {
                                if (!string.IsNullOrEmpty(_Request.ContactPerson.FirstName))
                                {
                                    _HCUAccount.FirstName = _Request.BusinessOwnerName;
                                    _HCUAccount.CpFirstName = _Request.ContactPerson.FirstName;
                                }
                                if (!string.IsNullOrEmpty(_Request.ContactPerson.LastName))
                                {
                                    //_HCUAccount.LastName = _Request.ContactPerson.LastName;
                                    _HCUAccount.CpLastName = _Request.ContactPerson.LastName;
                                }
                                _HCUAccount.CpName = _Request.ContactPerson.FirstName + " " + _Request.ContactPerson.LastName;
                                if (!string.IsNullOrEmpty(_Request.ContactPerson.MobileNumber))
                                {
                                    _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber);
                                    _HCUAccount.CpMobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber);
                                }
                                if (!string.IsNullOrEmpty(_Request.ContactPerson.EmailAddress))
                                {
                                    _HCUAccount.SecondaryEmailAddress = _Request.ContactPerson.EmailAddress;
                                    _HCUAccount.CpEmailAddress = _Request.ContactPerson.EmailAddress;
                                }
                            }
                            if (_Request.StartDate != null)
                            {
                                _HCUAccount.DateOfBirth = _Request.StartDate;
                            }
                            _HCUAccount.WebsiteUrl = _Request.WebsiteUrl;
                            _HCUAccount.Description = _Request.Description;
                            if (_AddressResponse != null)
                            {
                                if (!string.IsNullOrEmpty(_AddressResponse.Address))
                                {
                                    _HCUAccount.Address = _AddressResponse.Address;
                                }
                                if (_AddressResponse.CityAreaId != 0)
                                {
                                    _HCUAccount.CityAreaId = _AddressResponse.CityAreaId;
                                }
                                if (_AddressResponse.CityId != 0)
                                {
                                    _HCUAccount.CityId = _AddressResponse.CityId;
                                }
                                if (_AddressResponse.StateId != 0)
                                {
                                    _HCUAccount.StateId = _AddressResponse.StateId;
                                }
                                if (_AddressResponse.CountryId != 0)
                                {
                                    _HCUAccount.CountryId = _AddressResponse.CountryId;
                                }
                                if (_AddressResponse.Latitude != 0)
                                {
                                    _HCUAccount.Latitude = _AddressResponse.Latitude;
                                }
                                if (_AddressResponse.Longitude != 0)
                                {
                                    _HCUAccount.Longitude = _AddressResponse.Longitude;
                                }
                            }
                            if (_HCUAccount.CountryId < 1)
                            {
                                _HCUAccount.CountryId = _Request.UserReference.SystemCountry;
                            }
                            _HCUAccount.EmailVerificationStatus = 0;
                            _HCUAccount.NumberVerificationStatus = 0;
                            _HCUAccount.RegistrationSourceId = RegistrationSource.System;
                            _HCUAccount.CountValue = 0;
                            _HCUAccount.AverageValue = 0;
                            if (_Request.UserReference.AppVersionId != 0)
                            {
                                _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                            }
                            _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                            }
                            _HCUAccount.StatusId = HelperStatus.Default.Active;
                            _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCoreContext.HCUAccount.Add(_HCUAccount);
                            _HCoreContext.SaveChanges();
                            long StoreId = _HCUAccount.Id;
                            #endregion
                            using (_HCoreContext = new HCoreContext())
                            {
                                #region User
                                _HCUAccountAuth = new HCUAccountAuth();
                                _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(6);
                                _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                                _HCUAccountAuth.SecondaryPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                                _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                                if (_Request.UserReference.AccountId != 0)
                                {
                                    _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                                }
                                _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                #endregion
                                #region Save Cashier Account
                                _Random = new Random();
                                string CashierAccountCode = _Random.Next(1000, 9999).ToString() + _Random.Next(000000000, 999999999).ToString();
                                _HCUAccount = new HCUAccount();
                                _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccount.AccountTypeId = UserAccountType.MerchantCashier;
                                _HCUAccount.AccountOperationTypeId = Helpers.AccountOperationType.Offline;
                                _HCUAccount.OwnerId = MerchantId;
                                _HCUAccount.SubOwnerId = StoreId;
                                _HCUAccount.DisplayName = "0001";
                                _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName); _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                                _HCUAccount.AccountCode = CashierAccountCode;
                                _HCUAccount.GenderId = Gender.Male;
                                if (_Request.UserReference.AppVersionId != 0)
                                {
                                    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                                }
                                _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                                if (_Request.UserReference.AccountId != 0)
                                {
                                    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                }
                                if (_HCUAccount.CountryId < 1)
                                {
                                    _HCUAccount.CountryId = _Request.UserReference.SystemCountry;
                                }
                                _HCUAccount.Name = "0001";
                                _HCUAccount.EmailVerificationStatus = 0;
                                _HCUAccount.NumberVerificationStatus = 0;
                                _HCUAccount.RegistrationSourceId = RegistrationSource.System;
                                _HCUAccount.StatusId = HelperStatus.Default.Active;
                                _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccount.User = _HCUAccountAuth;
                                _HCoreContext.HCUAccount.Add(_HCUAccount);
                                _HCoreContext.SaveChanges();
                                #endregion
                            }
                        }
                    }
                    else
                    {
                        foreach (var _Store in _Request.Stores)
                        {
                            OAddressResponse _StoreAddress = HCoreHelper.GetAddressComponent(_Store.Address, _Request.UserReference);
                            using (_HCoreContext = new HCoreContext())
                            {
                                #region Save Store
                                _Random = new Random();
                                string StoreAccountCode = _Random.Next(100000000, 999999999).ToString();
                                _HCUAccountAuth = new HCUAccountAuth();
                                _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(6);
                                _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                                _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                                if (_Request.UserReference.AccountId != 0)
                                {
                                    _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                                }
                                _HCUAccountAuth.StatusId = HelperStatus.Default.Active;

                                _HCUAccount = new HCUAccount();
                                _HCUAccount.User = _HCUAccountAuth;
                                if (_Store.Categories != null && _Store.Categories.Count > 0)
                                {
                                    _HCUAccount.PrimaryCategoryId = _Store.Categories.FirstOrDefault().ReferenceId;
                                    //_HCUAccountParameters = new List<HCUAccountParameter>();
                                    //foreach (var Category in _Store.Categories)
                                    //{
                                    //    _HCUAccountParameters.Add(new HCUAccountParameter
                                    //    {
                                    //        Guid = HCoreHelper.GenerateGuid(),
                                    //        TypeId = HelperType.MerchantCategory,
                                    //        CommonId = Category.ReferenceId,
                                    //        CreateDate = HCoreHelper.GetGMTDateTime(),
                                    //        StatusId = HelperStatus.Default.Active,
                                    //    });
                                    //}
                                    //_HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                                    _TUCCategoryAccount = new List<TUCCategoryAccount>();
                                    foreach (var Category in _Store.Categories)
                                    {
                                        _TUCCategoryAccount.Add(new TUCCategoryAccount
                                        {
                                            Guid = HCoreHelper.GenerateGuid(),
                                            TypeId = 1,
                                            CategoryId = Category.ReferenceId,
                                            CreateDate = HCoreHelper.GetGMTDateTime(),
                                            CreatedById = _Request.UserReference.AccountId,
                                            StatusId = HelperStatus.Default.Active,
                                        });
                                    }
                                    _HCUAccount.TUCCategoryAccountAccount = _TUCCategoryAccount;
                                }
                                else
                                {
                                    if (_Request.Categories != null && _Request.Categories.Count > 0)
                                    {
                                        _HCUAccount.PrimaryCategoryId = _Request.Categories.FirstOrDefault().ReferenceId;
                                        _TUCCategoryAccount = new List<TUCCategoryAccount>();
                                        foreach (var Category in _Request.Categories)
                                        {
                                            _TUCCategoryAccount.Add(new TUCCategoryAccount
                                            {
                                                Guid = HCoreHelper.GenerateGuid(),
                                                TypeId = 1,
                                                CategoryId = Category.ReferenceId,
                                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                                CreatedById = _Request.UserReference.AccountId,
                                                StatusId = HelperStatus.Default.Active,
                                            });
                                        }
                                        _HCUAccount.TUCCategoryAccountAccount = _TUCCategoryAccount;
                                        //_HCUAccountParameters = new List<HCUAccountParameter>();
                                        //foreach (var Category in _Request.Categories)
                                        //{
                                        //    _HCUAccountParameters.Add(new HCUAccountParameter
                                        //    {
                                        //        Guid = HCoreHelper.GenerateGuid(),
                                        //        TypeId = HelperType.MerchantCategory,
                                        //        CommonId = Category.ReferenceId,
                                        //        CreateDate = HCoreHelper.GetGMTDateTime(),
                                        //        StatusId = HelperStatus.Default.Active,
                                        //    });
                                        //}
                                        //_HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                                    }
                                }
                                _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                                _HCUAccount.AccountOperationTypeId = Helpers.AccountOperationType.OnlineAndOffline;
                                _HCUAccount.AccountOperationTypeId = AccountOperationTypeId;
                                //if (string.IsNullOrEmpty(_Store.AccountOperationTypeCode))
                                //{
                                //    _Store.AccountOperationTypeCode = _Request.AccountOperationTypeCode;
                                //}
                                //if (_Store.AccountOperationTypeCode == "accountoperationtype.online")
                                //{
                                //    _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                                //}
                                //if (_Store.AccountOperationTypeCode == "accountoperationtype.offline")
                                //{
                                //    _HCUAccount.AccountOperationTypeId = AccountOperationType.Offline;
                                //}
                                //if (_Store.AccountOperationTypeCode == "accountoperationtype.onlineandoffline")
                                //{
                                //    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                                //}
                                _HCUAccount.OwnerId = MerchantId;
                                _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                                _HCUAccount.AccountCode = StoreAccountCode;
                                if (_Store.DisplayName.Length > 30)
                                {
                                    _HCUAccount.DisplayName = _Store.DisplayName.Substring(0, 29);
                                }
                                else
                                {
                                    _HCUAccount.DisplayName = _Store.DisplayName;
                                }
                                _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                                _HCUAccount.Name = _Store.Name;
                                _HCUAccount.ContactNumber = _Store.ContactNumber;
                                _HCUAccount.EmailAddress = _Store.EmailAddress;
                                if (_Store.ContactPerson != null)
                                {
                                    if (!string.IsNullOrEmpty(_Store.ContactPerson.FirstName))
                                    {
                                        _HCUAccount.FirstName = _Store.ContactPerson.FirstName;
                                        _HCUAccount.CpFirstName = _Store.ContactPerson.FirstName;
                                    }
                                    if (!string.IsNullOrEmpty(_Store.ContactPerson.LastName))
                                    {
                                        _HCUAccount.LastName = _Store.ContactPerson.LastName;
                                        _HCUAccount.CpLastName = _Store.ContactPerson.LastName;
                                    }
                                    _HCUAccount.CpName = _Store.ContactPerson.FirstName + " " + _Store.ContactPerson.LastName;
                                    if (!string.IsNullOrEmpty(_Store.ContactPerson.MobileNumber))
                                    {
                                        _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Store.ContactPerson.MobileNumber);
                                        _HCUAccount.CpMobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Store.ContactPerson.MobileNumber);
                                    }
                                    if (!string.IsNullOrEmpty(_Store.ContactPerson.EmailAddress))
                                    {
                                        _HCUAccount.SecondaryEmailAddress = _Store.ContactPerson.EmailAddress;
                                        _HCUAccount.CpEmailAddress = _Store.ContactPerson.EmailAddress;
                                    }
                                }
                                if (_Store.StartDate != null)
                                {
                                    _HCUAccount.DateOfBirth = _Store.StartDate;
                                }
                                _HCUAccount.WebsiteUrl = _Store.WebsiteUrl;
                                _HCUAccount.Description = _Store.Description;
                                if (_StoreAddress != null)
                                {
                                    if (!string.IsNullOrEmpty(_StoreAddress.Address))
                                    {
                                        _HCUAccount.Address = _StoreAddress.Address;
                                    }
                                    if (_StoreAddress.CityAreaId != 0)
                                    {
                                        _HCUAccount.CityAreaId = _StoreAddress.CityAreaId;
                                    }
                                    if (_StoreAddress.CityId != 0)
                                    {
                                        _HCUAccount.CityId = _StoreAddress.CityId;
                                    }
                                    if (_StoreAddress.StateId != 0)
                                    {
                                        _HCUAccount.StateId = _StoreAddress.StateId;
                                    }
                                    if (_StoreAddress.CountryId != 0)
                                    {
                                        _HCUAccount.CountryId = _StoreAddress.CountryId;
                                    }
                                    if (_StoreAddress.Latitude != 0)
                                    {
                                        _HCUAccount.Latitude = _StoreAddress.Latitude;
                                    }
                                    if (_StoreAddress.Longitude != 0)
                                    {
                                        _HCUAccount.Longitude = _StoreAddress.Longitude;
                                    }
                                }
                                _HCUAccount.EmailVerificationStatus = 0;
                                _HCUAccount.NumberVerificationStatus = 0;
                                _HCUAccount.RegistrationSourceId = RegistrationSource.System;
                                _HCUAccount.CountValue = 0;
                                _HCUAccount.AverageValue = 0;
                                if (_Request.UserReference.AppVersionId != 0)
                                {
                                    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                                }
                                _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                                if (_Request.UserReference.AccountId != 0)
                                {
                                    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                }
                                _HCUAccount.StatusId = HelperStatus.Default.Active;
                                _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCoreContext.HCUAccount.Add(_HCUAccount);
                                _HCoreContext.SaveChanges();
                                long StoreId = _HCUAccount.Id;
                                #endregion
                                if (_Request.BranchId > 0)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        _TUCBranchAccount = new TUCBranchAccount();
                                        _TUCBranchAccount.Guid = HCoreHelper.GenerateGuid();
                                        _TUCBranchAccount.BranchId = _Request.BranchId;
                                        _TUCBranchAccount.OwnerId = _HCoreContext.TUCBranchAccount.Where(x => x.Id == _Request.RmId).Select(x => x.AccountId).FirstOrDefault();
                                        _TUCBranchAccount.AccountId = StoreId;
                                        _TUCBranchAccount.AccountLevelId = 9;
                                        _TUCBranchAccount.StartDate = HCoreHelper.GetGMTDateTime();
                                        _TUCBranchAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _TUCBranchAccount.CreatedById = _Request.UserReference.AccountId;
                                        _TUCBranchAccount.StatusId = HelperStatus.Default.Active;
                                        _HCoreContext.TUCBranchAccount.Add(_TUCBranchAccount);
                                        _HCoreContext.SaveChanges();
                                    }
                                }

                                if (_Store.Terminals != null && _Store.Terminals.Count > 0)
                                {
                                    foreach (var _Terminal in _Store.Terminals)
                                    {
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            bool TerminalIdCheck = _HCoreContext.TUCTerminal.Any(x => x.IdentificationNumber == _Terminal.TerminalId);
                                            if (!TerminalIdCheck)
                                            {
                                                //_HCUAccountAuth = new HCUAccountAuth();
                                                //_HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                                //_HCUAccountAuth.Username = _Terminal.TerminalId;
                                                //_HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Terminal.TerminalId);
                                                //_HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                                //_HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                                //_HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                                                //if (_Request.UserReference.AccountId != 0)
                                                //{
                                                //    _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                                                //}
                                                //_HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                                //#region Save UserAccount
                                                //_HCUAccount = new HCUAccount();
                                                //_HCUAccount.Guid = HCoreHelper.GenerateGuid();
                                                //_HCUAccount.AccountTypeId = UserAccountType.TerminalAccount;
                                                //_HCUAccount.AccountOperationTypeId = AccountOperationType.Offline;
                                                //_HCUAccount.OwnerId = _Terminal.ProviderId;
                                                //_HCUAccount.BankId = _Terminal.AcquirerId;
                                                //_HCUAccount.SubOwnerId = StoreId;
                                                //_HCUAccount.DisplayName = _Terminal.TerminalId;
                                                //_HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                                                //_HCUAccount.AccountCode = _Random.Next(100000, 999999).ToString() + _Random.Next(000000000, 999999999).ToString();
                                                //if (_Request.UserReference.AppVersionId != 0)
                                                //{
                                                //    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                                                //}
                                                //_HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                                                //_HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                                                //_HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                                //if (_Request.UserReference.AccountId != 0)
                                                //{
                                                //    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                                //}
                                                //_HCUAccount.StatusId = HelperStatus.Default.Active;
                                                //_HCUAccount.Name = _Terminal.TerminalId;
                                                //_HCUAccount.CountryId = _Request.UserReference.CountryId;
                                                //_HCUAccount.EmailVerificationStatus = 0;
                                                //_HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                                //_HCUAccount.NumberVerificationStatus = 0;
                                                //_HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                                //_HCUAccount.User = _HCUAccountAuth;
                                                //#endregion
                                                _TUCTerminal = new TUCTerminal();
                                                _TUCTerminal.Guid = HCoreHelper.GenerateGuid();
                                                _TUCTerminal.IdentificationNumber = _Terminal.TerminalId;
                                                _TUCTerminal.DisplayName = _Terminal.TerminalId;
                                                _TUCTerminal.SerialNumber = _Terminal.SerialNumber;
                                                _TUCTerminal.MerchantId = MerchantId;
                                                _TUCTerminal.StoreId = StoreId;
                                                _TUCTerminal.ProviderId = _Terminal.ProviderId;
                                                _TUCTerminal.CreateDate = HCoreHelper.GetGMTDateTime();
                                                _TUCTerminal.AcquirerId = _Terminal.AcquirerId;
                                                if (_Request.UserReference != null && _Request.UserReference.AccountId > 0)
                                                {
                                                    _TUCTerminal.CreatedById = _Request.UserReference.AccountId;
                                                }
                                                else
                                                {
                                                    _TUCTerminal.CreatedById = MerchantId;
                                                }
                                                _TUCTerminal.StatusId = HelperStatus.Default.Active;
                                                _HCoreContext.TUCTerminal.Add(_TUCTerminal);
                                                _HCoreContext.SaveChanges();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //HCore.TUC.Core.Framework.Operations.FrameworkSubscription _FrameworkSubscription = new Framework.Operations.FrameworkSubscription();
                    //_FrameworkSubscription.AddAccountFreeSubscription(MerchantId, UserAccountType.Merchant);

                    if (!string.IsNullOrEmpty(_Request.EmailAddress))
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var MerchantDetails = _HCoreContext.HCUAccount
                                    .Where(x => x.Id == MerchantId)
                                    .Select(x => new
                                    {
                                        UserName = x.User.Username,
                                        Pasword = x.User.Password,
                                        Name = x.Name,
                                        DisplayName = x.DisplayName,
                                        EmailAddress = x.EmailAddress,
                                        MobileNumber = x.MobileNumber,
                                    }).FirstOrDefault();
                            if (MerchantDetails != null)
                            {
                                try
                                {
                                    PipedriveClient client = new PipedriveClient(new ProductHeaderValue("0001"), new Uri("https://thankucashsales.pipedrive.com"))
                                    {
                                        Credentials = new Credentials("b4cda3bb82c8af5b1ef71f9c1152fb155b27f81f", AuthenticationType.ApiToken),
                                    };

                                    NewActivity _NewActivity = new NewActivity("New Merchant Created " + MerchantDetails.Name, "Merchant");
                                    _NewActivity.Subject = "New Merchant Registration : " + MerchantDetails.Name;
                                    _NewActivity.Done = 0;
                                    _NewActivity.Type = "call";
                                    _NewActivity.OrgId = 1;
                                    _NewActivity.Note = "Merchant Request <br> Name:" + MerchantDetails.Name + "<br>Contact Number : " + MerchantDetails.MobileNumber + "<br>Email Address : " + MerchantDetails.EmailAddress;
                                    client.Activity.Create(_NewActivity);
                                }
                                catch (Exception _Exception)
                                {
                                    HCoreHelper.LogException("PIPEDRIVE", _Exception);
                                }

                                var _EmailParameters = new
                                {
                                    UserDisplayName = MerchantDetails.DisplayName,
                                    UserName = MerchantDetails.UserName,
                                    Password = HCoreEncrypt.DecryptHash(MerchantDetails.Pasword),
                                    Name = MerchantDetails.Name,
                                };
                                HCoreHelper.BroadCastEmail("d-8a7bc7cd93d84cd7ac25684a3e851aea", MerchantDetails.DisplayName, MerchantDetails.EmailAddress, _EmailParameters, _Request.UserReference);
                            }
                        }
                    }
                    _MerchantResponse.Status = ResponseStatus.Success;
                    _MerchantResponse.StatusCode = TUCCoreResource.CA1134;
                    _MerchantResponse.Message = TUCCoreResource.CA1134M;
                    _MerchantResponse.ReferenceId = MerchantId;
                    _MerchantResponse.ReferenceKey = MerchantKey;
                    return _MerchantResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Core-SaveMerchant", _Exception, _Request.UserReference);
                return _MerchantResponse;
            }
        }


    }
}