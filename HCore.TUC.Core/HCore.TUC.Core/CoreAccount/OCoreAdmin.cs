//==================================================================================
// FileName: OCoreAdmin.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Core.CoreAccount
{
    internal class OCoreAdmin
    {
        internal class AuthUpdate
        {
            internal class Request
            {
                internal long AccountId { get; set; }
                internal string AccountKey { get; set; }

                internal string UserName { get; set; }

                internal string OldPassword { get; set; }
                internal string NewPassword { get; set; }

                internal string OldPin { get; set; }
                internal string NewPin { get; set; }

                internal OUserReference UserReference { get; set; }
            }
            internal class Response
            {
                internal ResponseStatus Status { get; set; }
                internal string StatusCode { get; set; }
                internal string Message { get; set; }
                internal string UserName { get; set; }
                internal string NewPassword { get; set; }
                internal string NewPin { get; set; }
            }
        }
        internal class Request
        {
            internal long ReferenceId { get; set; }
            internal string ReferenceKey { get; set; }
            internal string DisplayName { get; set; }
            internal string FirstName { get; set; }
            internal string LastName { get; set; }
            internal string MobileNumber { get; set; }
            internal string EmailAddress { get; set; }
            internal string StatusCode { get; set; }
            internal string GenderCode { get; set; }
            internal string RoleKey { get; set; }
            internal string OwnerKey { get; set; }
            internal string CountryKey { get; set; }
            internal string TimeZoneKey { get; set; }
            internal DateTime? DateOfBirth { get; set; }
            internal OStorageContent IconContent { get; set; }
            internal string Address { get; set; }
            internal OUserReference UserReference { get; set; }
        }
        internal class Response
        {
            internal ResponseStatus Status { get; set; }
            internal string StatusCode { get; set; }
            internal string Message { get; set; }
            internal long ReferenceId { get; set; }
            internal string ReferenceKey { get; set; }
        }
    }

}
