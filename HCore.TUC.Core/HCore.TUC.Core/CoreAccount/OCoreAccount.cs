//==================================================================================
// FileName: OCoreAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Core.CoreAccount
{

    internal class OCoreAccount
    {
        internal class ResetPassword
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public OUserReference? UserReference {get;set;}
            }
        }
    }

    //internal class OCoreAccount
    //{
    //    internal class Category
    //    {
    //        internal long ReferenceId { get; set; }
    //        internal string ReferenceKey { get; set; }
    //        internal string Name { get; set; }
    //        internal string IconUrl { get; set; }
    //    }
    //    internal class ContactPerson
    //    {
    //        internal string FirstName { get; set; }
    //        internal string LastName { get; set; }
    //        internal string MobileNumber { get; set; }
    //        internal string EmailAddress { get; set; }
    //    }
    //}
}
