//==================================================================================
// FileName: CoreCustomer.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.CoreAccount
{
    public class CoreCustomer
    {
        HCoreContext _HCoreContext;
        OCoreCustomer.Response _AdminResponse;
        internal OCoreCustomer.Response UpdateCustomer(OCoreCustomer.Request _Request)
        {
            _AdminResponse = new OCoreCustomer.Response();
            _AdminResponse.Status =  ResponseStatus.Error;
            _AdminResponse.StatusCode = TUCCoreResource.CA0500;
            _AdminResponse.Message = TUCCoreResource.CA0500M;
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    _AdminResponse.StatusCode = TUCCoreResource.CAREF;
                    _AdminResponse.Message = TUCCoreResource.CAREFM;
                    return _AdminResponse;
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    _AdminResponse.StatusCode = TUCCoreResource.CAREFKEY;
                    _AdminResponse.Message = TUCCoreResource.CAREFKEYM;
                    return _AdminResponse;
                }

                int GenderId = 0;
                if (!string.IsNullOrEmpty(_Request.GenderCode))
                {
                    GenderId = HCoreHelper.GetStatusId(_Request.GenderCode, _Request.UserReference);
                    if (GenderId == 0)
                    {
                        _AdminResponse.StatusCode = TUCCoreResource.CA1190;
                        _AdminResponse.Message = TUCCoreResource.CA1190M;
                        return _AdminResponse;
                    }
                }
                using (_HCoreContext = new HCoreContext())
                {
                    HCUAccount _Details = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.ReferenceId
                        && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_Details == null)
                    {
                        _HCoreContext.Dispose();
                        _AdminResponse.StatusCode = TUCCoreResource.CA0404;
                        _AdminResponse.Message = TUCCoreResource.CA0404M;
                        return _AdminResponse;
                    }
                    if (!string.IsNullOrEmpty(_Request.DisplayName) && _Details.DisplayName != _Request.DisplayName)
                    {
                        _Details.DisplayName = _Request.DisplayName;
                    }
                    if (!string.IsNullOrEmpty(_Request.FirstName) && _Details.FirstName != _Request.FirstName)
                    {
                        _Details.FirstName = _Request.FirstName;
                    }
                    if (!string.IsNullOrEmpty(_Request.LastName) && _Details.LastName != _Request.LastName)
                    {
                        _Details.LastName = _Request.LastName;
                    }
                    _Details.Name = _Details.FirstName + " " + _Details.LastName;
                    if (!string.IsNullOrEmpty(_Request.EmailAddress) && _Details.EmailAddress != _Request.EmailAddress)
                    {
                        _Details.EmailAddress = _Request.EmailAddress;
                    }
                    
                    if (!string.IsNullOrEmpty(_Request.Address) && _Details.Address != _Request.Address)
                    {
                        _Details.Address = _Request.Address;
                    }
                    if (GenderId > 0)
                    {
                        _Details.GenderId = GenderId;
                    }
                    
                    _Details.ModifyById = _Request.UserReference.AccountId;
                    _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                    _HCoreContext.SaveChanges();
                    long? IconStorageId = null;
                    if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                    {
                        IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, _Details.IconStorageId, _Request.UserReference);
                    }
                    if (IconStorageId != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var UserAccDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Details.Id).FirstOrDefault();
                            if (UserAccDetails != null)
                            {
                                if (IconStorageId != null)
                                {
                                    UserAccDetails.IconStorageId = IconStorageId;
                                }
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    _AdminResponse.Status = ResponseStatus.Success;
                    _AdminResponse.StatusCode = TUCCoreResource.CA1191;
                    _AdminResponse.Message = TUCCoreResource.CA1191M;
                    _AdminResponse.ReferenceId = _Details.Id;
                    _AdminResponse.ReferenceKey = _Details.Guid;
                    return _AdminResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Core-UpdateCustomer", _Exception, _Request.UserReference);
                return _AdminResponse;
            }
        }
    }
}
