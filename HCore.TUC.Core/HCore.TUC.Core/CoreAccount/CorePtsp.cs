//==================================================================================
// FileName: CorePtsp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Core.CoreAccount
{
    internal class CorePtsp
    {
        Random _Random;
        HCUAccountAuth _HCUAccountAuth;
        HCoreContext _HCoreContext;
        OCorePtsp.Response _PtspResponse;
        HCUAccount _HCUAccount;
        internal OCorePtsp.Response SavePtsp(OCorePtsp.Request _Request)
        {
            _PtspResponse = new OCorePtsp.Response();
            _PtspResponse.Status = HCoreConstant.ResponseStatus.Error;
            _PtspResponse.StatusCode = TUCCoreResource.CA0500;
            _PtspResponse.Message = TUCCoreResource.CA0500M;
            try
            {
                if (string.IsNullOrEmpty(_Request.DisplayName))
                {
                    _PtspResponse.StatusCode = TUCCoreResource.CA1138;
                    _PtspResponse.Message = TUCCoreResource.CA1138M;
                    return _PtspResponse;
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    _PtspResponse.StatusCode = TUCCoreResource.CA1139;
                    _PtspResponse.Message = TUCCoreResource.CA1139M;
                    return _PtspResponse;
                }
                if (string.IsNullOrEmpty(_Request.ContactNumber))
                {
                    _PtspResponse.StatusCode = TUCCoreResource.CA1141;
                    _PtspResponse.Message = TUCCoreResource.CA1141M;
                    return _PtspResponse;
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    _PtspResponse.StatusCode = TUCCoreResource.CA1142;
                    _PtspResponse.Message = TUCCoreResource.CA1142M;
                    return _PtspResponse;
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    _PtspResponse.StatusCode = TUCCoreResource.CA1143;
                    _PtspResponse.Message = TUCCoreResource.CA1143M;
                    return _PtspResponse;
                }
                if (_Request.ContactPerson == null)
                {
                    _PtspResponse.StatusCode = TUCCoreResource.CA1144;
                    _PtspResponse.Message = TUCCoreResource.CA1144M;
                    return _PtspResponse;
                }
                if (string.IsNullOrEmpty(_Request.ContactPerson.FirstName))
                {
                    _PtspResponse.StatusCode = TUCCoreResource.CA1145;
                    _PtspResponse.Message = TUCCoreResource.CA1145M;
                    return _PtspResponse;
                }
                if (string.IsNullOrEmpty(_Request.ContactPerson.LastName))
                {
                    _PtspResponse.StatusCode = TUCCoreResource.CA1146;
                    _PtspResponse.Message = TUCCoreResource.CA1146M;
                    return _PtspResponse;
                }
                if (string.IsNullOrEmpty(_Request.ContactPerson.MobileNumber))
                {
                    _PtspResponse.StatusCode = TUCCoreResource.CA1147;
                    _PtspResponse.Message = TUCCoreResource.CA1147M;
                    return _PtspResponse;
                }
                if (string.IsNullOrEmpty(_Request.ContactPerson.EmailAddress))
                {
                    _PtspResponse.StatusCode = TUCCoreResource.CA1148;
                    _PtspResponse.Message = TUCCoreResource.CA1148M;
                    return _PtspResponse;
                }

                int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == 0)
                {
                    _PtspResponse.StatusCode = TUCCoreResource.CA1149;
                    _PtspResponse.Message = TUCCoreResource.CA1149M;
                    return _PtspResponse;
                }

                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.Address, _Request.UserReference);
                using (_HCoreContext = new HCoreContext())
                {
                    bool Details = _HCoreContext.HCUAccount
                        .Any(x => x.AccountTypeId == UserAccountType.Acquirer
                        && (x.DisplayName == _Request.DisplayName || x.Name == _Request.Name || x.EmailAddress == _Request.EmailAddress));
                    if (Details)
                    {
                        _HCoreContext.Dispose();
                        _PtspResponse.Status = HCoreConstant.ResponseStatus.Error;
                        _PtspResponse.StatusCode = TUCCoreResource.CA1483;
                        _PtspResponse.Message = TUCCoreResource.CA1483M;
                        return _PtspResponse;
                    }
                    if (!string.IsNullOrEmpty(_Request.UserName))
                    {
                        bool UserNameCheck = _HCoreContext.HCUAccountAuth
                        .Any(x => x.Username == _Request.UserName);
                        if (UserNameCheck)
                        {
                            _HCoreContext.Dispose();
                            _PtspResponse.Status = HCoreConstant.ResponseStatus.Error;
                            _PtspResponse.StatusCode = TUCCoreResource.CA1485;
                            _PtspResponse.Message = TUCCoreResource.CA1485M;
                            return _PtspResponse;
                        }
                    }
                    else
                    {
                        _Request.UserName = HCoreHelper.GenerateRandomNumber(6);
                    }
                    if (string.IsNullOrEmpty(_Request.Password))
                    {
                        _Request.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(6));
                    }
                    else
                    {
                        _Request.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                    }
                    _Random = new Random();
                    string AccountCode = _Random.Next(10000000, 999999999).ToString() + "" + _Random.Next(10000000, 999999999).ToString();
                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = _Request.UserName;
                    _HCUAccountAuth.Password = _Request.Password;
                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.User = _HCUAccountAuth;
                    _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccount.AccountTypeId = UserAccountType.PosAccount;
                    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = AccountCode;
                    if (_Request.DisplayName.Length > 30)
                    {
                        _HCUAccount.DisplayName = _Request.DisplayName.Substring(0, 29);
                    }
                    else
                    {
                        _HCUAccount.DisplayName = _Request.DisplayName;
                    }
                    _HCUAccount.ReferralCode = _Request.ReferralCode;
                    _HCUAccount.Name = _Request.Name;
                    _HCUAccount.ContactNumber = _Request.ContactNumber;
                    _HCUAccount.EmailAddress = _Request.EmailAddress;
                    if (_Request.ContactPerson != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.ContactPerson.FirstName))
                        {
                            _HCUAccount.FirstName = _Request.ContactPerson.FirstName;
                            _HCUAccount.CpFirstName = _Request.ContactPerson.FirstName;
                        }
                        if (!string.IsNullOrEmpty(_Request.ContactPerson.LastName))
                        {
                            _HCUAccount.LastName = _Request.ContactPerson.LastName;
                            _HCUAccount.CpLastName = _Request.ContactPerson.LastName;
                        }
                        _HCUAccount.CpName = _Request.ContactPerson.FirstName + " " + _Request.ContactPerson.LastName;
                        if (!string.IsNullOrEmpty(_Request.ContactPerson.MobileNumber))
                        {
                            _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber, _Request.ContactPerson.MobileNumber.Length);
                            _HCUAccount.CpMobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber, _Request.ContactPerson.MobileNumber.Length);
                        }
                        if (!string.IsNullOrEmpty(_Request.ContactPerson.EmailAddress))
                        {
                            _HCUAccount.SecondaryEmailAddress = _Request.ContactPerson.EmailAddress;
                            _HCUAccount.CpEmailAddress = _Request.ContactPerson.EmailAddress;
                        }
                    }
                    if (_Request.StartDate != null)
                    {
                        _HCUAccount.DateOfBirth = _Request.StartDate;
                    }
                    _HCUAccount.WebsiteUrl = _Request.WebsiteUrl;
                    _HCUAccount.Description = _Request.Description;
                    if (_AddressResponse != null)
                    {
                        if (!string.IsNullOrEmpty(_AddressResponse.Address))
                        {
                            _HCUAccount.Address = _AddressResponse.Address;
                        }
                        if (_AddressResponse.CityAreaId != 0)
                        {
                            _HCUAccount.CityAreaId = _AddressResponse.CityAreaId;
                        }
                        if (_AddressResponse.CityId != 0)
                        {
                            _HCUAccount.CityId = _AddressResponse.CityId;
                        }
                        if (_AddressResponse.StateId != 0)
                        {
                            _HCUAccount.StateId = _AddressResponse.StateId;
                        }
                        if (_AddressResponse.CountryId != 0)
                        {
                            _HCUAccount.CountryId = _AddressResponse.CountryId;
                        }
                        if (_AddressResponse.Latitude != 0)
                        {
                            _HCUAccount.Latitude = _AddressResponse.Latitude;
                        }
                        if (_AddressResponse.Longitude != 0)
                        {
                            _HCUAccount.Longitude = _AddressResponse.Longitude;
                        }
                    }
                    if (_HCUAccount.CountryId < 1)
                    {
                        _HCUAccount.CountryId = _Request.UserReference.SystemCountry;
                    }
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.RegistrationSourceId = RegistrationSource.System;
                    _HCUAccount.CountValue = 0;
                    _HCUAccount.AverageValue = 0;
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    }
                    if (StatusId > 0)
                    {
                        _HCUAccount.StatusId = StatusId;
                    }
                    else
                    {
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                    }

                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    _HCoreContext.SaveChanges();
                    _PtspResponse.Status = HCoreConstant.ResponseStatus.Success;
                    _PtspResponse.StatusCode = TUCCoreResource.CA1484;
                    _PtspResponse.Message = TUCCoreResource.CA1484M;
                    _PtspResponse.ReferenceId = _HCUAccount.Id;
                    _PtspResponse.ReferenceKey = _HCUAccount.Guid;
                    long? IconStorageId = null;
                    if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                    {
                        IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                    }
                    if (IconStorageId != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var UserAccDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _HCUAccount.Id).FirstOrDefault();
                            if (UserAccDetails != null)
                            {
                                if (IconStorageId != null)
                                {
                                    UserAccDetails.IconStorageId = IconStorageId;
                                }
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    return _PtspResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Core-SavePTSP", _Exception, _Request.UserReference);
                return _PtspResponse;
            }
        }

    }
}
