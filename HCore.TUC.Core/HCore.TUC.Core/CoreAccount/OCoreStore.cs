//==================================================================================
// FileName: OCoreStore.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Core.CoreAccount
{
  internal  class OCoreStore
    {

        internal class Request
        {
            internal long MerchantId { get; set; }
            internal string MerchantKey { get; set; }
            internal long StoreId { get; set; }
            internal string StoreKey { get; set; }
            internal string DisplayName { get; set; }
            internal long OwnerId { get; set; }
            internal string ReferralCode { get; set; }
            internal string Name { get; set; }
            internal string ContactNumber { get; set; }
            internal string EmailAddress { get; set; }
            internal string AccountOperationTypeCode { get; set; }
            internal string WebsiteUrl { get; set; }
            internal string Description { get; set; }
            internal DateTime? StartDate { get; set; }
            internal OAddress Address { get; set; }
            internal ContactPerson ContactPerson { get; set; }
            internal OUserReference UserReference { get; set; }
            internal List<Category> Categories { get; set; }
        }
        internal class Response
        {
            internal ResponseStatus Status { get; set; }
            internal string StatusCode { get; set; }
            internal string Message { get; set; }
            internal long ReferenceId { get; set; }
            internal string ReferenceKey { get; set; }
        }
    }
}
