//==================================================================================
// FileName: OCoreCashier.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Core.CoreAccount
{
    internal class OCoreCashier
    {
        internal class BulkRequest
        {
            internal long MerchantId { get; set; }
            internal string MerchantKey { get; set; }
            internal List<RequestDetails> Cashiers { get; set; }
            internal OUserReference UserReference { get; set; }
        }
        internal class RequestDetails
        {
            internal long StoreId { get; set; }
            internal string StoreKey { get; set; }
            internal string FirstName { get; set; }
            internal string LastName { get; set; }
            internal string EmailAddress { get; set; }
            internal string GenderCode { get; set; }
            internal string MobileNumber { get; set; }
            internal string ContactNumber { get; set; }
            internal string EmployeeCode { get; set; }
            internal OAddress Address { get; set; }
            internal OStorageContent IconContent { get; set; }
        }


        internal class Request
        {
            internal long ReferenceId { get; set; }
            internal string ReferenceKey { get; set; }
            internal long MerchantId { get; set; }
            internal string MerchantKey { get; set; }
            internal long StoreId { get; set; }
            internal string StoreKey { get; set; }
            internal string FirstName { get; set; }
            internal string LastName { get; set; }
            internal string EmailAddress { get; set; }
            internal string GenderCode { get; set; }
            internal string MobileNumber { get; set; }
            internal string ContactNumber { get; set; }
            internal string EmployeeCode { get; set; }
            internal OAddress Address { get; set; }
            internal OStorageContent IconContent { get; set; }
            internal OUserReference UserReference { get; set; }
        }
        internal class Response
        {
            internal ResponseStatus Status { get; set; }
            internal string StatusCode { get; set; }
            internal string Message { get; set; }
            internal long ReferenceId { get; set; }
            internal string ReferenceKey { get; set; }
        }
    }
}
