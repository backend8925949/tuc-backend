﻿using System;
using HCore.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using HCore.ThankUCash.Gateway.Core;
using HCore.ThankUCash.Gateway.Object;
namespace HCoreX.App.TUC.Connect.Core
{
    public class HCoreXAuthConnector
    {
        private readonly RequestDelegate _next;
        public HCoreXAuthConnector(RequestDelegate next)
        {
            _next = next;
        }
        public async Task Invoke(HttpContext _HttpContext)
        {
            var _Request = _HttpContext.Request;
            string _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
            try
            {
                DateTime RequestTime = HCoreHelper.GetGMTDateTime();
                var Url = _Request.Path.Value;
                if (!string.IsNullOrEmpty(Url) && Url.Count(f => f == '/') == 4)
                {
                    string ApiName = Url.Split("/")[4];
                    string VersionName = Url.Split("/")[2];
                    if (VersionName == "v1")
                    {
                        var _Headers = _Request.Headers;
                        if (ApiName == "notifyscpayment")
                        {
                            string JsonString = null;
                            if (!string.IsNullOrEmpty(_RequestBody))
                            {
                                OThankUGateway.SCNotifyProductPaymentRequest _RequestBodyContent = JsonConvert.DeserializeObject<OThankUGateway.SCNotifyProductPaymentRequest>(_RequestBody);
                                if (_RequestBodyContent.eventType == "NEW_PAYMENT")
                                {
                                    JsonString = JsonConvert.SerializeObject(_RequestBodyContent);
                                    string SubJsonContent = JsonConvert.SerializeObject(_RequestBody);
                                    OAuth.Response _OAuthApp = HCoreAuth.Auth_Notify(_Request, SubJsonContent, _RequestBodyContent.till.terminalID, RequestTime, ApiName);
                                    if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                                    {
                                        JObject _JObject = JObject.Parse(_RequestBody);
                                        _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                        var Newjson = JsonConvert.SerializeObject(_JObject);
                                        var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                        Stream stream = await requestContent.ReadAsStreamAsync();
                                        _HttpContext.Request.Body = stream;
                                    }
                                    else
                                    {
                                        string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                        var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                                        var json = JsonConvert.SerializeObject(Data);
                                        _HttpContext.Response.StatusCode = 401;
                                        await _HttpContext.Response.WriteAsync(json);
                                        return;
                                    }
                                }
                                else
                                {
                                    JsonString = JsonConvert.SerializeObject(_RequestBodyContent);
                                    string SubJsonContent = JsonConvert.SerializeObject(_RequestBody);
                                    OAuth.Response _OAuthApp = HCoreAuth.Auth_Key(_Request, SubJsonContent, RequestTime, ApiName, null);
                                    if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                                    {
                                        JObject _JObject = JObject.Parse(_RequestBody);
                                        _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                        var Newjson = JsonConvert.SerializeObject(_JObject);
                                        var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                        Stream stream = await requestContent.ReadAsStreamAsync();
                                        _HttpContext.Request.Body = stream;
                                    }
                                    else
                                    {
                                        string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                        var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                                        var json = JsonConvert.SerializeObject(Data);
                                        _HttpContext.Response.StatusCode = 401;
                                        await _HttpContext.Response.WriteAsync(json);
                                        return;
                                    }
                                }
                            }
                            else
                            {
                                _HttpContext.Response.StatusCode = 401;
                                return;
                            }
                        }
                        else if (ApiName == "notifyscproduct")
                        {
                            string JsonString = null;
                            if (!string.IsNullOrEmpty(_RequestBody))
                            {
                                OThankUGateway.SCNotifyProductPaymentRequest _RequestBodyContent = JsonConvert.DeserializeObject<OThankUGateway.SCNotifyProductPaymentRequest>(_RequestBody);
                                JsonString = JsonConvert.SerializeObject(_RequestBodyContent);
                                string SubJsonContent = JsonConvert.SerializeObject(_RequestBody);
                                var HAuthorization = _Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value;
                                OAuth.Response _OAuthApp = HCoreAuth.Auth_Key(_Request, SubJsonContent, RequestTime, ApiName, null);
                                if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                                {
                                    JObject _JObject = JObject.Parse(_RequestBody);
                                    _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                    var Newjson = JsonConvert.SerializeObject(_JObject);
                                    var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                    Stream stream = await requestContent.ReadAsStreamAsync();
                                    _HttpContext.Request.Body = stream;
                                }
                                else
                                {
                                    string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                    var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                                    var json = JsonConvert.SerializeObject(Data);
                                    _HttpContext.Response.StatusCode = 401;
                                    await _HttpContext.Response.WriteAsync(json);
                                    return;
                                }
                            }
                            else
                            {
                                _HttpContext.Response.StatusCode = 401;
                                return;
                            }
                        }
                        else if (ApiName == "notifygatransaction")
                        {
                            string JsonString = null;
                            if (!string.IsNullOrEmpty(_RequestBody))
                            {
                                OThankUGateway.NotifyPayment.Request _RequestBodyContent = JsonConvert.DeserializeObject<OThankUGateway.NotifyPayment.Request>(_RequestBody);
                                JsonString = JsonConvert.SerializeObject(_RequestBodyContent);
                                string SubJsonContent = JsonConvert.SerializeObject(_RequestBody);
                                var HAuthorization = _Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value;
                                if (_RequestBodyContent.paymentInstrument != null)
                                {
                                    OAuth.Response _OAuthApp = HCoreAuth.Auth_Notify(_Request, SubJsonContent, _RequestBodyContent.paymentInstrument.userId, RequestTime, ApiName);
                                    if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                                    {
                                        JObject _JObject = JObject.Parse(_RequestBody);
                                        _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                        var Newjson = JsonConvert.SerializeObject(_JObject);
                                        var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                        Stream stream = await requestContent.ReadAsStreamAsync();
                                        _HttpContext.Request.Body = stream;
                                    }
                                    else
                                    {
                                        string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                        var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                                        var json = JsonConvert.SerializeObject(Data);
                                        _HttpContext.Response.StatusCode = 401;
                                        await _HttpContext.Response.WriteAsync(json);
                                        return;
                                    }
                                }
                                else
                                {
                                    _HttpContext.Response.StatusCode = 401;
                                    return;
                                }

                            }
                            else
                            {
                                _HttpContext.Response.StatusCode = 401;
                                return;
                            }
                        }
                        else if (ApiName == "claimreward")
                        {
                            string HAppKey = _Headers.Where(x => x.Key == "hcak").FirstOrDefault().Value;
                            if (!string.IsNullOrEmpty(HAppKey))
                            {
                                var _RequestBodyContent = JsonConvert.DeserializeObject<OAuth.Request>(_RequestBody);
                                var ZxContent = HCoreEncrypt.DecodeText(_RequestBodyContent.zx);
                                JObject _JObject = JObject.Parse(ZxContent);
                                #region Validate Request
                                if (!string.IsNullOrEmpty(_RequestBodyContent.zx))
                                {
                                    string JsonString = JsonConvert.SerializeObject(_RequestBodyContent);
                                    OAuth.Response _OAuthApp = HCoreAuth.Auth_App(_Request, JsonString, RequestTime, ApiName);
                                    if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                                    {

                                        OAuth.Response _OAuth = HCoreAuth.Auth_Request(_Request, _OAuthApp);
                                        if (_OAuth.Status == HCoreConstant.StatusSuccess)
                                        {
                                            OUserReference _UReference = _OAuth.UserReference;
                                            _JObject.Add("UserReference", JObject.FromObject(_UReference));
                                        }
                                        else
                                        {
                                            var Data = HCoreAuth.Auth_Response(_OAuth.UserResponse, _OAuth);
                                            var json = JsonConvert.SerializeObject(Data);
                                            _HttpContext.Response.StatusCode = 401; //UnAuthorized
                                            await _HttpContext.Response.WriteAsync(json);
                                            return;
                                        }
                                        OAuth.Request _AuthRequest = new OAuth.Request();
                                        _AuthRequest.fx = _RequestBodyContent.fx;
                                        _AuthRequest.vx = _RequestBodyContent.vx;
                                        _AuthRequest.zx = HCoreEncrypt.EncodeText(JsonConvert.SerializeObject(_JObject));

                                        var Newjson = JsonConvert.SerializeObject(_AuthRequest);
                                        var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                        Stream stream = await requestContent.ReadAsStreamAsync();
                                        _HttpContext.Request.Body = stream;
                                    }
                                    else
                                    {
                                        var Data = HCoreAuth.Auth_Response(_OAuthApp.UserResponse, _OAuthApp);
                                        var json = JsonConvert.SerializeObject(Data);
                                        _HttpContext.Response.StatusCode = 401;
                                        await _HttpContext.Response.WriteAsync(json);
                                        return;
                                    }
                                }
                                else
                                {
                                    _HttpContext.Response.StatusCode = 401;
                                    return;
                                }
                                #endregion
                            }
                            else
                            {
                                _HttpContext.Response.StatusCode = 401;
                                return;
                            }
                        }
                        else if (ApiName == "notifysettlements")
                        {
                            string JsonString = null;
                            OThankUGateway.NotifySettlement _RequestBodyContent = JsonConvert.DeserializeObject<OThankUGateway.NotifySettlement>(_RequestBody);
                            JsonString = JsonConvert.SerializeObject(_RequestBodyContent);
                            OAuth.Response _OAuthApp = HCoreAuth.Auth_OuterRequest(_Request, JsonString, RequestTime, ApiName);
                            if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                            {
                                JObject _JObject = JObject.Parse(_RequestBody);
                                _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                var Newjson = JsonConvert.SerializeObject(_JObject);
                                var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                Stream stream = await requestContent.ReadAsStreamAsync();
                                _HttpContext.Request.Body = stream;
                            }
                            else
                            {

                                string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                                var json = JsonConvert.SerializeObject(Data);
                                _HttpContext.Response.StatusCode = 401;
                                await _HttpContext.Response.WriteAsync(json);
                                return;
                            }
                        }
                        else
                        {
                            var ResponseType = _Headers.Where(x => x.Key == "ResponseType").FirstOrDefault().Value;
                            OAuth.Response _OAuthApp = HCoreAuth.Auth_Key(_Request, _RequestBody, RequestTime, ApiName, ResponseType);
                            if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                            {
                                JObject _JObject = JObject.Parse(_RequestBody);
                                _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                var Newjson = JsonConvert.SerializeObject(_JObject);
                                var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                Stream stream = await requestContent.ReadAsStreamAsync();
                                _HttpContext.Request.Body = stream;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(ResponseType) && ResponseType == "json")
                                {
                                    string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                    var Data = HCoreAuth.Auth_ResponseDefaultObject(_OAuthApp.UserResponse);
                                    var json = JsonConvert.SerializeObject(Data);
                                    _HttpContext.Response.StatusCode = 401;
                                    await _HttpContext.Response.WriteAsync(json);
                                    return;
                                }
                                else
                                {
                                    string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                    var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                                    var json = JsonConvert.SerializeObject(Data);
                                    _HttpContext.Response.StatusCode = 401;
                                    await _HttpContext.Response.WriteAsync(json);
                                    return;
                                }
                            }
                        }
                    }
                    else if (VersionName == "v2")
                    {
                        if (ApiName == "notifycppay")
                        {
                            var _Headers = _Request.Headers;
                            var HeaderHash = _Headers.Where(x => x.Key == "Hash").FirstOrDefault().Value;
                            if (!string.IsNullOrEmpty(HeaderHash))
                            {
                                if (!string.IsNullOrEmpty(_RequestBody))
                                {
                                    HCore.ThankUCash.Gateway.ObjectV2.OThankUGateway.CoralPay.Request _RequestBodyContent = JsonConvert.DeserializeObject<HCore.ThankUCash.Gateway.ObjectV2.OThankUGateway.CoralPay.Request>(_RequestBody);
                                    if (_RequestBodyContent != null && _RequestBodyContent.ResponseHeader != null)
                                    {
                                        string HasContent = _RequestBodyContent.ResponseHeader.BatchId + _RequestBodyContent.ResponseHeader.ClientId + _RequestBodyContent.TransactionList.Count() + "5a18f4af-fb6f-4def-8f34-b29731d099e6";
                                        string HashData = HCoreEncrypt.GetSha256FromString(HasContent);
                                        if (HeaderHash == HashData)
                                        {
                                        }
                                        else
                                        {
                                            _HttpContext.Response.StatusCode = 401;
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        _HttpContext.Response.StatusCode = 401;
                                        return;
                                    }
                                }
                                else
                                {
                                    _HttpContext.Response.StatusCode = 401;
                                    return;
                                }
                            }
                            else
                            {
                                _HttpContext.Response.StatusCode = 401;
                                return;
                            }
                        }
                        else
                        {
                            var _Headers = _Request.Headers;
                            var HAuthorization = _Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value;
                            if (!string.IsNullOrEmpty(HAuthorization) && HAuthorization.FirstOrDefault().Contains("Bearer"))
                            {
                                RequestV1 _RequestBodyContentV1 = JsonConvert.DeserializeObject<RequestV1>(_RequestBody);
                                string PrivateKey = "NDA5NiE8UlNBS2V5VmFsdWU+PE1vZHVsdXM+a0h5R2EvNVhpVXhhQjIyZWc3aUFmTDVMaEN4RW5kUHpKNHMwek5tN1VEUDdmNWY1SWdGcXFxcFVtVjJ2SlVNUGJKcFBWaFhRcUtzY2dlaFU2c3lmOFpqclRIbVhCUS8zdVl4b1F5ODNQaExqRW45TWRVdGsyN3BjdStuUXE3MlhzcjNscWk0dkR4ekViNkdHTmpEWFlvYzBQNHZXdVM5bHNQbCt5TXA1RXVYNWNkd3Nqa3hmalBKUUtON05tTWs2OXlKNHp3M3R4bFNmaG1DZXRObmgxS01XVFQySGsxREN6R1d5USs0SDdYWDhVQ241Q2xnTTdHODFRUDlDaHlpOHg3UmxDY2o4QVg4d1UyUDBycGovN1JOcmhGUGpLU3kreUhET3YrOXh6NEhiNFVmakhvdnJhdlpIU25WNU0rblpjLzBEYXVqbmJONHQxZjJIUnRCbjlPa3pLK2xXdno3WjY5MW5PMit2ZDJnaFRrQm1jRUplR3RHQUlkZFhjZkh2TjlNQmhWMUxmMzlBY3QrNGdUN0l6UWNDL0tndVorM3Z2Y1pLWFBxc2tNeXU3dmNTUi9WamU2d0xBMWp5UkZsc3MraWcxM25TUDhCZTY5aHI3SXpXYmd2YTZBN3NPOG1tN2ZZcFdEMkQxUHVralJqRG9pMFk1SklseExUWkFseXJobU1EdzRxTEViWVBUeWpzOG16aHZiWjVCR1hvb3JNbFhJcW9VaHRTbWFmb1E3RzNIa2RpOExFZ2xNNHlZZnhzWGJxMkluWWNnaUs1cExaOE1UM1ZMd2ZtY0dGUHZxbUNla2M5ZHJIaEFkaUJtNHc1c0xOL0NBcHpRbE9adFRwNWJQWUJmTWxkOFhuVjFjTSsydmxPS3JWbFRHbTZuZy9FZ0Rzb1MrS1dNQjA9PC9Nb2R1bHVzPjxFeHBvbmVudD5BUUFCPC9FeHBvbmVudD48UD5wenVLNyttSXBpelIzazJRbzNPN3FpVlZaeVluQndGaE9ndGJRVWwyQ3c2NHlTdFVWV245aENkQ2syN0x3YkpXTXg1dkF5dVdzREJUdkJSbG9yeVRYZ2k0OVpvUEh5aXNDZGkrUmtjalFoU3AzUlJUUjhCdWxPeTN0a0UwcjBobmN1bWhPQkgrTWFyblkzS1VGK09yeWFOdklaVGgrSXIvaEo0Sk5PdnZ5WTV3RGFFUWFidHdGZnhDQUdhWjlmdU03K3A4alBNVVVxYmtibVY0YVZ3Q2hXWFdseTVjaUlxdDIyNkx6MkdoeEpJa3BkWm9ZdHZNY1NFUzVQQ09SL2crc0hvamtlMEdrOWQ1WitIRDRzOWNnMWNDVnRtZFlsRDNBeTVzWEkzNWtIR0JxaGM1dlRmb2cxNGI1ekJhcUx2akV2aE5KdmlqUFQ0VktRR1Evbm9FMHc9PTwvUD48UT4zUzRjdi9nQUNXdFNKamZqcWRqWnhmMm5DYmg5ZHlaYk0wWXRVREFhY0xrazRKcTBWZnlyOWlFRFk1Y2tqVEt0Z2FlOXN6WUxTZWFUZXFmQ0VVb0FobkNjSnAyRmx2aFE0UVFXSXRUQkNEOWVoTVpBWUFPQ2p1cVg4Ni9SVDVPbTJQVmNUOVYzSjMzd2orcHZPMXhrbmtsd2ZtU2tNczlQMU16MlkvRzdjTENLdXRNb1FmZDBqMXBCQmQrQkI2b21aZklFQ21HRHBrVlVaVGVhVXdCS3U2eUIzWUpYZjRjcC9yendRUzVBVjB0TGtRUG1FZ1FpZHdoWW5ueVpPSFBRc3RNWWxVN0V3czFSMWkvMDB2UkVUMXdRSXFBUE9kcnpIalN1RW1FT21jTDd1N3dlWThNWUVJSjdFSENsUHk1YVBvd1BSUFJIYmczWmJBOW5jc0toVHc9PTwvUT48RFA+a1lacEF4a1FCcnRkN2J0RnNGY1g2Wm9QVlhHSDZMcTBwdFFYRnhzY0U4YzkyaUZRZy90MTF6aHE0TzA3ZGxVQVdmK1R6d2h4V3VUVEdRb08zNnlGK1FCU0k3ZHFqVXpSV01nNTIrSXZ0bU5RNWNlNEtPbzEwSGFsNzMyU29TMjZoKzd5a2w1bCswYWprSkMyZW90dWhFc3FlK2laTTdXUllSQTlKc1pocGQzRGVMaStGYVE4aDRVNjBpcDJzKzJ3MlY0T0FJSFBINVdIbHVIS0JMVkNwRjdZZkRxbDMxUCtzc0xoK3hjVXRJQVFVQmZpUXBUT3RXdDRpNjBodzFNdENsbGU5cTl5VjlqY0Z5QUpYQ2ZpYzVDVndvb2dRTnYvOTg3QUVWS2k1NlUrbG9zYmZhTlhQR242a2hZcjdrV0I5cElJWVc5Z2JmekdZU2F4ekNEV2JRPT08L0RQPjxEUT5kdHA5cG9iTTlrTE9mUzBSVU1idmwxNUxzWVNMSDBqa1B6Ulp5enBlS3U3Q3ExTld6dElkVWdEQmduWEl0WHRqRTBZaFFUa3NiY1RTd1ZsK2hxM2VoRi9rK1RjeWYwTG1xMVAzMVRYQW1QNzhlcHJTSXdZZjFVNm80dEU2eCtsUEFrMHZFdE5oMlQzcTFGc2s5S0Fjalpabk5mUTg2L1ROTUV3SHB3TlNOeWNYRlhlOHJWNGRLNTlUUzZXai80dG1ZT2ZoVmZhNnVOR09uV2w0cTF0L3pVUFE4VWhHWG9neWYrbmxyUkQra0NvczdEdE5HYWRSSWVmQlAwRXpVTXVqZlVURWRBOUUybTFjcVoxUmtUNmpxb1dOUWg0Q3NKTkFpNTNCWUt6NUhKVlRrcmx2RnlyU05xVDNIb2w1c3U3a25iSER6c2o1eldEeDBnaWhEV0ViTHc9PTwvRFE+PEludmVyc2VRPmtOSUxtZDJzM0N3UndqNUZ6YlBhRlExemlxU091aGJxaS9PNmY2RTJBWElvRE4yMmpuOUJybDBkNVcwS25zcE8wMGQ0REN6MW5iWGE5UDJ0WURFY3FzeVc4enlKa2JKNmhhYjhyL2cwZ2c4VTFsdXZyMmE1T3pEeWtzdUd4d1pIbmxMTE85Nk9EbXBRdTBHMUhHcXI5MUk5T2hBKy85YUZtSzVkSlZUbHZOWFBCWjJBSTFCYnVwYzZITFJaZVlLdnBqMHQySUNDZHZMRHJ4S2tGdEcwU2hqamJCSXJtQ29FRHZQWjQrd29uemVrUkFDMzRZbVRHYnlYTVQxM2ljRisxWFdndW0xVzY3RVVqaGYrRFN4VVFDbmRxY1FWNEVPSkJLbnNpRGJESGgwekZOaE5JK1VqQ1ZjN2E0SWd4YUo1ekdmbzNiUzIvRHdGZXd6VTVmNkIrQT09PC9JbnZlcnNlUT48RD5MeWwyZHltK1NRS3pJRm1YdEVjMUtZSzBBNkN2aFJUNStaNW16YmNFZlZsNmJsdWM1b3l2MUtWa2wrc01rSTBqSzcveXhaYTVWVlVGS1NJOUZhZFZUZmNzVnZvWllYMmt6dHdWN0ZndElBWHFPTTkwbFpUV2h5Sjl3cGkvbEU5K2hNbVhmY1UxZHlsY3RWTjBDNXhGc2RPUUttM3EyT2V2dWVTMmpiUUpjVEhBQ25wajBNNzlJWFMyMWFCa1c4clRoSGVzRjJua2d5aDhLZEMvL1NoaG5wYmRZTGtFMkFFMGxLTmRYTzhGaDIxTHZwNzB1M3VpUEVyUWh6Yks4SE5McTZRbWx0c1Q1eWt0RFF0RGd2cE9ENFNYUThwZUM4S2ZRNjRPem5mS0hvRzAxQVF6RG5WOWtweHhDTXp2a3hKeCtIcE44MGV5d3NhREQ2YUIwWXVWWXMwek9nWEhUTmZXRVBXcVBPWHNnUFlCL1pKUU1TTUszQ1MzVVRDTkRGc1UzVWs0TWF1ZmxueG94Yi92Q1lCRVNpS2Qxd0tHWk5jRG9JV2JWYWlKNWRPbUsySlVvTTIxVnNxWDJKcFF6Mm9lRTE4YXVHaUlZd0NWQTFYb3Ava1FFTkVKMmJQWXl5MU0rc0pQRUNpbmxqTGhjYTVMeW5pSFRRNEhhTytKU3hmaGRuQXRDcGRTMk9Mc2J6V2dZQkhlOTBReDBwUkxublRLMGUyZHJrSEJBNDlOcEV0M0hKY3VVWDFZNDRxQzBGcnNUSHNOOWxXeEFCM05sZDRrdW5rcEp6c0RCeGt1Q2U2eWpiL2dsMU1FQS9keFF5enJvR284cHphOEgwbDByS2p5MTMrb2tDNm1uUnR5d2R5VTJqMEM4U240OGwxV0Y0aGtFcWEwVm5oWlVjRT08L0Q+PC9SU0FLZXlWYWx1ZT4=";
                                if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Live)
                                {
                                    PrivateKey = "NTEyITxSU0FLZXlWYWx1ZT48TW9kdWx1cz5zZHhmOFo2TE92cVBxMGNIcmMyQWpoQ1JuSkFnQ2kyVDd6dyt4ckZ0OERCNVYxZ3VMRDFmcWhBTkQrb1g2bnZRQzA5czFqWXFIbXF1MExZbTR5VHU4UT09PC9Nb2R1bHVzPjxFeHBvbmVudD5BUUFCPC9FeHBvbmVudD48UD42QjN5Y0E2VVJPN0pqU05DREV3Zlo3YXhjUEg5dW5QdWF3VW8yT1VEQjJjPTwvUD48UT54Q2xNcjNwbzVUVDZOcmlnUHcvN2hZakJxU290MCtDVllXRFZqUTNwRitjPTwvUT48RFA+d2tYdVdDT2JyV1krZzBaelRWK1pHWEVyYW1EZ0FHSnZ0bHNIS1NIa094az08L0RQPjxEUT51OFdFNEp6ZWVEbkVJa21OSzVDVEJmb28xMVBHMW9DdXZhNUIvV29KYkpVPTwvRFE+PEludmVyc2VRPkpYVTM4RWpwTW5mSWQrZHVQRnlGdlhZR2tFNm55VzFCbHQ2TTljcDdNbGc9PC9JbnZlcnNlUT48RD5BUFh4eHJwV2dqRU1TWjZPbWcrOEhBaFc0UU9lU0svS1ZzWjZpRlE1QmMwYTZuVllNT1dONUhYUkIyanJYL0dDQ0J2ZWpvYTBKTGtNakFzR0RjWWd5UT09PC9EPjwvUlNBS2V5VmFsdWU+";
                                }
                                string DecContent = HCoreEncryptRsa.Decrypt(_RequestBodyContentV1.data, PrivateKey);
                                object _RequestBodyContent = JsonConvert.DeserializeObject<object>(DecContent);
                                string SubJsonContent = JsonConvert.SerializeObject(_RequestBody);
                                string JsonString = JsonConvert.SerializeObject(_RequestBodyContent);
                                OAuth.Response _OAuthApp = HCoreAuth.Auth_Key(_Request, SubJsonContent, RequestTime, ApiName, "json");
                                if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                                {
                                    _OAuthApp.UserReference.IsRsa = true;
                                    JObject _JObject = JObject.Parse(DecContent);
                                    _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                    var Newjson = JsonConvert.SerializeObject(_JObject);
                                    var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                    Stream stream = await requestContent.ReadAsStreamAsync();
                                    _HttpContext.Request.Body = stream;
                                }
                                else
                                {
                                    string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                    var Data = HCoreAuth.Auth_ResponseDefaultObjectRsa(_OAuthApp.UserResponse);
                                    var json = JsonConvert.SerializeObject(Data);
                                    _HttpContext.Response.StatusCode = 401;
                                    await _HttpContext.Response.WriteAsync(json);
                                    return;
                                }
                            }
                            else
                            {
                                //var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                                OThankUGateway.RequestT _RequestBodyContent = JsonConvert.DeserializeObject<OThankUGateway.RequestT>(_RequestBody);
                                string SubJsonContent = JsonConvert.SerializeObject(_RequestBody);
                                OAuth.Response _OAuthApp = HCoreAuth.Auth_Key(_Request, SubJsonContent, RequestTime, ApiName, "json");
                                if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                                {
                                    if (!string.IsNullOrEmpty(_RequestBodyContent.InvoiceAmount) && !System.Text.RegularExpressions.Regex.IsMatch(_RequestBodyContent.InvoiceAmount, "^[0-9]*$"))
                                    {
                                        #region Send Response
                                        var Response = HCoreHelper.SendResponse(_OAuthApp.UserReference, HCore.Helper.HCoreConstant.ResponseStatus.Error, null, "HCG150", CoreResources.HCG150);
                                        #endregion
                                        var json = JsonConvert.SerializeObject(HCoreAuth.Auth_ResponseDefaultObject(Response));
                                        _HttpContext.Response.StatusCode = 200;
                                        await _HttpContext.Response.WriteAsync(json);
                                        return;
                                    }

                                    JObject _JObject = JObject.Parse(_RequestBody);
                                    _OAuthApp.UserReference.IsRsa = false;
                                    _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                    var Newjson = JsonConvert.SerializeObject(_JObject);
                                    var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                    Stream stream = await requestContent.ReadAsStreamAsync();
                                    _HttpContext.Request.Body = stream;
                                }
                                else
                                {
                                    string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                    var Data = HCoreAuth.Auth_ResponseDefaultObject(_OAuthApp.UserResponse);
                                    var json = JsonConvert.SerializeObject(Data);
                                    _HttpContext.Response.StatusCode = 401;
                                    await _HttpContext.Response.WriteAsync(json);
                                    return;
                                }
                            }
                        }
                    }
                    else if (VersionName == "maddeals")
                    {
                        var _Headers = _Request.Headers;
                        //var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                        string SubJsonContent = JsonConvert.SerializeObject(_RequestBody);
                        OAuth.Response _OAuthApp = HCoreAuth.Auth_Key_Web_MadDeals(_Request, SubJsonContent, RequestTime, ApiName);
                        if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                        {
                            JObject _JObject = JObject.Parse(_RequestBody);
                            _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                            if (_OAuthApp.AccountId > 0)
                            {
                                _JObject.Add("AuthAccountId", _OAuthApp.AccountId);
                                _JObject.Add("AccountId", _OAuthApp.UserReference.AccountId);
                                _JObject.Add("AccountKey", _OAuthApp.UserReference.AccountKey);
                            }
                            var Newjson = JsonConvert.SerializeObject(_JObject);
                            var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                            Stream stream = await requestContent.ReadAsStreamAsync();
                            _HttpContext.Request.Body = stream;
                        }
                        else
                        {
                            string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                            var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                            var json = JsonConvert.SerializeObject(Data);
                            _HttpContext.Response.StatusCode = 401;
                            await _HttpContext.Response.WriteAsync(json);
                            return;
                        }
                    }
                    else if (VersionName == "opcon")
                    {
                        var _Headers = _Request.Headers;
                        //var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                        string SubJsonContent = JsonConvert.SerializeObject(_RequestBody);
                        OAuth.Response _OAuthApp = HCoreAuth.Auth_Key(_Request, SubJsonContent, RequestTime, ApiName, "json");
                        if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                        {
                            JObject _JObject = JObject.Parse(_RequestBody);
                            _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                            var Newjson = JsonConvert.SerializeObject(_JObject);
                            var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                            Stream stream = await requestContent.ReadAsStreamAsync();
                            _HttpContext.Request.Body = stream;
                        }
                        else
                        {
                            string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                            var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                            var json = JsonConvert.SerializeObject(Data);
                            _HttpContext.Response.StatusCode = 401;
                            await _HttpContext.Response.WriteAsync(json);
                            return;
                        }
                    }
                    else
                    {
                        _HttpContext.Response.StatusCode = 401;
                        return;
                    }
                }
                else
                {
                    _HttpContext.Response.StatusCode = 401;
                    return;
                }

                await _next.Invoke(_HttpContext);
            }
            catch (Exception _Exception)
            {
                //var _RequestBody = await new StreamReader(_Request.Body).ReadToEndAsync();
                if (!string.IsNullOrEmpty(_RequestBody))
                {
                    HCoreHelper.LogData(HCoreConstant.LogType.Exception, "INVOCE-EX-BODY", _RequestBody, String.Empty, null);
                }
                HCoreHelper.LogException("Invoke", _Exception);
                _HttpContext.Response.StatusCode = 401;
                return;
            }
        }
    }
    public static class HCoreXAuthConnectorExtension
    {
        public static IApplicationBuilder HCoreXAuth(this IApplicationBuilder app)
        {
            app.UseMiddleware<HCoreXAuthConnector>();
            return app;
        }
    }
}

