﻿using System;
using HCore.Data.Helper;
using HCore.Helper;

namespace HCoreX.App.TUC.Connect.Core
{
    public class WhiteListing
    {
        public string[]? Development { get; set; }
        public string[]? Qa { get; set; }
        public string[]? Staging { get; set; }
        public string[]? Production { get; set; }
    }
    internal static class HCoreXConfiguration
    {
        private static bool IsServerLoaded = false;
        internal static async Task ShedularStartAsync()
        {
            Jobs.HCoreXJobFactory _JobFactory = new Jobs.HCoreXJobFactory();
            await _JobFactory.StartJobAsync();
        }
        internal static void LoadConfiguration(string Host)
        {
            var configurationBuilder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").AddEnvironmentVariables();
            var configuration = configurationBuilder.Build();
            WhiteListing hostItems = configuration.GetSection("WhiteListing").Get<WhiteListing>()!;
            if (hostItems.Development.Any(x => x == Host))
            {
                HCoreConstant._AppConfig = configuration.GetSection("Development").Get<Globals>()!;
                HostHelper.Host = configuration["Development:Host:Master"]!;
                HostHelper.HostLogging = configuration["Development:Host:Logging"]!;
                HostHelper.HostOperations = configuration["Development:Host:Operations"]!;
            }
            else if (hostItems.Staging.Any(x => x == Host))
            {
                HCoreConstant.HostEnvironment = HCoreConstant.HostEnvironmentType.Test;
                HCoreConstant._AppConfig = configuration.GetSection("Staging").Get<Globals>()!;
                HostHelper.Host = configuration["Staging:Host:Master"]!;
                HostHelper.HostLogging = configuration["Staging:Host:Logging"]!;
                HostHelper.HostOperations = configuration["Staging:Host:Operations"]!;
            }
            else if (hostItems.Qa.Any(x => x == Host))
            {
                HCoreConstant.HostEnvironment = HCoreConstant.HostEnvironmentType.Tech;
                HCoreConstant._AppConfig = configuration.GetSection("Qa").Get<Globals>()!;
                HostHelper.Host = configuration["Qa:Host:Master"]!;
                HostHelper.HostLogging = configuration["Qa:Host:Logging"]!;
                HostHelper.HostOperations = configuration["Qa:Host:Operations"]!;
            }
            else if (hostItems.Production.Any(x => x == Host))
            {
                HCoreConstant.HostEnvironment = HCoreConstant.HostEnvironmentType.Live;
                HCoreConstant._AppConfig = configuration.GetSection("Production").Get<Globals>()!;
                HostHelper.Host = configuration["Production:Host:Master"]!;
                HostHelper.HostLogging = configuration["Production:Host:Logging"]!;
                HostHelper.HostOperations = configuration["Production:Host:Operations"]!;
            }
            else
            {
                HCoreConstant.HostEnvironment = HCoreConstant.HostEnvironmentType.Live;
                HCoreConstant._AppConfig = configuration.GetSection("Production").Get<Globals>()!;
                HostHelper.Host = configuration["Production:Host:Master"]!;
                HostHelper.HostLogging = configuration["Production:Host:Logging"]!;
                HostHelper.HostOperations = configuration["Production:Host:Operations"]!;
            }
        }
    }
}