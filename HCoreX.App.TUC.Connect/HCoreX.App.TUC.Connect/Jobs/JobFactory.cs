﻿using System;
using System.Reflection.Metadata;
using HCore.Data.Store;
using HCore.Helper;
using HCore.ThankUCash.Gateway;
using Quartz;
using Quartz.Impl;

namespace HCoreX.App.TUC.Connect.Jobs
{
    public class HCoreXJobFactory
    {
        public async Task StartJobAsync()
        {
            try
            {
                ISchedulerFactory _ISchedulerFactory = new StdSchedulerFactory();
                IScheduler _IScheduler = await _ISchedulerFactory.GetScheduler();
                await _IScheduler.Start();

                // 3 MIN
                #region 3 MIN Job
                IJobDetail _IJobDetail_JOB_3_MIN = JobBuilder.Create<JOB_3_MIN>()
                                        .WithIdentity("JOB_3_MIN", "JOB_3_MIN_JOB_Group")
                                        .Build();
                ITrigger _ITrigger_JOB_3_MIN = TriggerBuilder.Create()
                .WithIdentity("JOB_3_MIN", "JOB_3_MIN_TRIGGER_Group")
                .ForJob(_IJobDetail_JOB_3_MIN)
                .StartNow()
                .WithSimpleSchedule(x => x.WithIntervalInMinutes(3).RepeatForever())
                .Build();
                await _IScheduler.ScheduleJob(_IJobDetail_JOB_3_MIN, _ITrigger_JOB_3_MIN);
                #endregion
                #region 1 HOUR Job
                IJobDetail _IJobDetail_JOB_1_HOUR = JobBuilder.Create<JOB_1_HOUR>()
                                        .WithIdentity("JOB_1_HOUR", "JOB_1_HOUR_JOB_Group")
                                        .Build();
                ITrigger _ITrigger_JOB_1_HOUR = TriggerBuilder.Create()
                .WithIdentity("JOB_1_HOUR", "JOB_1_HOUR_TRIGGER_Group")
                .ForJob(_IJobDetail_JOB_1_HOUR)
                .StartNow()
                .WithCronSchedule("0 0 0/1 1/1 * ? *")
                .Build();
                await _IScheduler.ScheduleJob(_IJobDetail_JOB_1_HOUR, _ITrigger_JOB_1_HOUR);
                #endregion
            }
            catch (System.Exception ex)
            {
                HCoreHelper.LogException("StartJobAsync", ex);
            }

        }
        public class JOB_1_HOUR : IJob
        {
            public async Task Execute(IJobExecutionContext context)
            {
                await Task.Delay(0);
                ManageThankUCashGatewayStore.RefreshGatewayDataStoreSync();
                HCoreDataStoreManager.DataStore_Sync();
                HCore.ThankUCash.Gateway.ManageThankUCashGatewayStore.RefreshGatewayDataStore();
            }
        }
        public class JOB_3_MIN : IJob
        {
            public async Task Execute(IJobExecutionContext context)
            {
                await Task.Delay(0);
                if (HCoreConstant.HostName == "appconnect.thankucash.com" ||
                    HCoreConstant.HostName == "testappconnect.thankucash.com" ||
                    HCoreConstant.HostName == "testwebconnect.thankucash.com" ||
                    HCoreConstant.HostName == "connect.thankucash.com" ||
                    HCoreConstant.HostName == "testconnect.thankucash.com" ||
                    HCoreConstant.HostName == "localhost")
                {
                    HCore.TUC.Plugins.MadDeals.ManageCore _ManageCore = new HCore.TUC.Plugins.MadDeals.ManageCore();
                    _ManageCore.ProcessSync();
                }

            }
        }
    }
}

