﻿using HCore.Helper;
using HCore.TUC.Connect;
using HCore.TUC.Connect.Object;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCoreX.App.TUC.Connect.Connect.v1
{
    [Produces("application/json")]
    [Route("api/v1/accounts/[action]")]
    public class TUCAccountController : Controller
    {
        ManageAccount _ManageAccount;
        [HttpPost]
        [ActionName("connectaccount")]
        public object ConnectAccount([FromBody] OAccount.Profile.Request _ConnectAccountRequest)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.ConnectAccount(_ConnectAccountRequest);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getbalance")]
        public object GetAccountBalance([FromBody] OAccount.Balance.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetAccountBalance(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
        [HttpPost]
        [ActionName("updatepin")]
        public object UpdateAccountPin([FromBody] OAccount.UpdatePin.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.UpdateAccountPin(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
        [HttpPost]
        [ActionName("resetpin")]
        public object ResetAccountPin([FromBody] OAccount.ResetPin.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.ResetAccountPin(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }


        [HttpPost]
        [ActionName("topupaccount")]
        public object TopupAccount([FromBody] OAccount.Toupup.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.TopupAccount(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("transferpoint")]
        public object TransferPoint([FromBody] OAccount.Transfer.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.TransferPoint(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("gettransactions")]
        public object GetTransaction([FromBody] OAccount.Transaction.List.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetTransaction(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getmerchants")]
        public object GetMerchants([FromBody] OAccount.Merchant.List.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetMerchants(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getdeals")]
        public object GetDeals([FromBody] OAccount.Deal.List.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetDeals(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
        [HttpPost]
        [ActionName("getdeal")]
        public object GetDeal([FromBody] OAccount.Deal.Details.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetDeal(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
        [HttpPost]
        [ActionName("buydeal")]
        public object BuyDeal([FromBody] OAccount.Deal.Buy.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.BuyDeal(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
        [HttpPost]
        [ActionName("getpurchaseddeals")]
        public object GetDealCodes([FromBody] OAccount.Deal.Buy.List.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetDealCodes(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
        ManageOperations _ManageOperations;
        [HttpPost]
        [ActionName("getcategories")]
        public object GetCategories([FromBody] OAccount.Deal.Buy.Request _Request)
        {
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.GetCategories();
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}

