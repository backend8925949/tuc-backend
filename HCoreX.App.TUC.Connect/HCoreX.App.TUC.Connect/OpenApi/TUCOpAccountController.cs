﻿using System;
using HCore.Helper;
using HCore.ThankUCash.Gateway.OpenApi;
using Microsoft.AspNetCore.Mvc;

namespace HCoreX.App.TUC.Connect.OpenApi
{
    [Produces("application/json")]
    [Route("api/opcon/account/[action]")]
    public class TUCOpAccountController
    {
        ManageAccount _ManageAccount;
        [HttpPost]
        [ActionName("generateapprequest")]
        public object SaveUserAppRequest([FromBody] OOpenApi.ERequest _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.SaveUserAppRequest(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
        [HttpPost]
        [ActionName("getcountries")]
        public object GetCountries([FromBody] OOpenApi.ERequest _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetCountries(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
        [HttpPost]
        [ActionName("getaccount")]
        public object GetAccount([FromBody] OOpenApi.ERequest _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetAccount(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("updateaccount")]
        public object UpdateAccount([FromBody] OOpenApi.ERequest _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.UpdateAccount(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("resetpin")]
        public object ResetPin([FromBody] OOpenApi.Balance.ERequest _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.ResetPin(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getbalance")]
        public object GetBalance([FromBody] OOpenApi.Balance.ERequest _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetBalance(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
        [HttpPost]
        [ActionName("register")]
        public object Register([FromBody] OOpenApi.Register.ERequest _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.Register(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
        [HttpPost]
        [ActionName("registerweb")]
        public object RegisterWeb([FromBody] OOpenApi.Register.ERequest _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.RegisterWeb(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
        [HttpPost]
        [ActionName("deleteaccount")]
        public object Delete([FromBody] DeleteCustomerInfo.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            DeleteCustomerInfo.Response response = _ManageAccount.DeleteAccount(_Request);
            return response;
        }
    }
}
