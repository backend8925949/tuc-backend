﻿

using HCore.Helper;
using HCoreX.App.TUC.Connect.Core;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;


AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
var _WebApplicationBuilder = WebApplication.CreateBuilder(args);
_WebApplicationBuilder.Services.AddMvc(options => options.EnableEndpointRouting = false);
_WebApplicationBuilder.Services.AddControllers().AddNewtonsoftJson(opt =>
{
    opt.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
    opt.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
});
_WebApplicationBuilder.Services.AddControllers().AddNewtonsoftJson();
_WebApplicationBuilder.Services.Configure<KestrelServerOptions>(options =>
{
    options.AllowSynchronousIO = true;
});
_WebApplicationBuilder.Services.Configure<IISServerOptions>(options =>
{
    options.AllowSynchronousIO = true;
});

var _IApplicationBuilder = _WebApplicationBuilder.Build();
_IApplicationBuilder.Use(async (context, next) =>
{
    if (string.IsNullOrEmpty(HCoreConstant.HostName))
    {
        HCoreConstant.HostName = context.Request.Host.Host;
        HCoreXConfiguration.LoadConfiguration(context.Request.Host.Host);
        HCoreSystem.DataStore_InitializeSystem();
        HCore.Data.Store.HCoreDataStoreManager.DataStore_Start();
        if (HCoreConstant.HostEnvironment != HCoreConstant.HostEnvironmentType.Local)
        {
            HCoreXConfiguration.ShedularStartAsync().GetAwaiter().GetResult();
        }
    }
    await next.Invoke();
});
_IApplicationBuilder.UseCors(_Builder => _Builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
_IApplicationBuilder.UseRouting();
_IApplicationBuilder.HCoreXAuth();
_IApplicationBuilder.UseMvc();
_IApplicationBuilder.Run();

