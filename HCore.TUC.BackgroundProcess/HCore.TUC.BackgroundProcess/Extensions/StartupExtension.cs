﻿using HCore.TUC.BackgroundProcess.ContractImplementations;
using HCore.TUC.BackgroundProcess.Contracts;
using HCore.TUC.BackgroundProcess.Configs;
using HCore.TUC.Core.Framework.Merchant.App.AWS;

namespace HCore.TUC.BackgroundProcess.Extensions
{
    public static class StartupExtension
    {
        public static IServiceCollection CustomConfigDI(this IServiceCollection services, HostBuilderContext hostContext)
        {
            var config = hostContext.Configuration;
            var commonSettings = config.GetSection("CommonSettings").Get<CommonSettings>();
            if (commonSettings != null)
            {
                services.AddSingleton(commonSettings);
                AppSettings? appSettings;
                if (!string.IsNullOrEmpty(commonSettings.Environment))
                {
                    appSettings = commonSettings.Environment.ToUpper() switch
                    {
                        "LOCALHOST" or "TEST" or "DEV" => config.GetSection("DevSettings").Get<AppSettings>(),
                        "TECH" => config.GetSection("TechSettings").Get<AppSettings>(),
                        "LIVE" => config.GetSection("LiveSettings").Get<AppSettings>(),
                        _ => config.GetSection("DevSettings").Get<AppSettings>(),
                    };
                }
                else
                {
                    appSettings = config.GetSection("DevSettings").Get<AppSettings>();
                }

                if (appSettings != null)
                {
                    services.AddSingleton(appSettings);
                }

                services.AddMemoryCache();
                services.AddSingleton<IConsumerService, ConsumerService>();
                services.AddSingleton<ITransactionService, TransactionService>();
                services.AddSingleton<ICustomerService, CustomerService>();
                services.AddSingleton<IRewardService, RewardService>();
                services.AddSingleton<IConsumerSQSService, ConsumerSQSService>();

                AppConfiguration.SetupConfigs(commonSettings, appSettings);

                services.AddHostedService<Worker>();
            }

            return services;
        }
    }
}

