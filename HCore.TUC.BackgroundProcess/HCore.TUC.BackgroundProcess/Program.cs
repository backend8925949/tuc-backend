﻿using HCore.TUC.BackgroundProcess.Extensions;
using Serilog;


Log.Logger = CreateBootstrapLogger();

try
{
    IHost host = Host.CreateDefaultBuilder(args)
        .UseWindowsService()
        .ConfigureServices((hostContext, services) =>
        {
            services.CustomConfigDI(hostContext);
        })
        .UseSerilog((context, configuration) =>
        {
            configuration.ReadFrom.Configuration(context.Configuration);
        })
        .Build();

    host.Run();
}
catch(Exception ex)
{
    string type = ex.GetType().Name;
    if(type.Equals("StopTheHostException", StringComparison.OrdinalIgnoreCase))
    {
        throw;
    }
    Log.Fatal("Host terminated unexpectedly!");
}
finally
{
    Log.CloseAndFlush();
}


static Serilog.ILogger CreateBootstrapLogger()
{
    var configurationBuilder = new ConfigurationBuilder()
        .AddJsonFile("appsettings.json", false, true)
        .Build();

    return new LoggerConfiguration().ReadFrom.Configuration(configurationBuilder)
        .CreateBootstrapLogger();
}