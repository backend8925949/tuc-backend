﻿using Amazon.SQS.Model;
using HCore.TUC.BackgroundProcess.Configs;
using HCore.TUC.BackgroundProcess.Contracts;
using Newtonsoft.Json;

namespace HCore.TUC.BackgroundProcess.ContractImplementations
{
	public class ConsumerSQSService: IConsumerSQSService
    {
        private readonly ILogger<ConsumerSQSService> _logger;
        private readonly CommonSettings _commonSettings;

        public ConsumerSQSService(CommonSettings commonSettings, ILogger<ConsumerSQSService> logger)
        {
            _logger = logger;
            _commonSettings = commonSettings;
        }

        public async Task DeleteMessageAsync(Message message)
        {
            try
            {
                using var client = new Amazon.SQS.AmazonSQSClient(GetCredentials(), Amazon.RegionEndpoint.EUWest2);
                await client.DeleteMessageAsync(_commonSettings.SQSQueueUrl, message.ReceiptHandle);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "DeleteMessageAsync Error");
            }
        }

        public async Task<ReceiveMessageResponse> GetMessageAsync()
        {
            ReceiveMessageResponse response = new();
            try
            {
                using var client = new Amazon.SQS.AmazonSQSClient(GetCredentials(), Amazon.RegionEndpoint.EUWest2);
                response = await client.ReceiveMessageAsync(
                    new ReceiveMessageRequest
                    {
                        QueueUrl = _commonSettings.SQSQueueUrl,
                        MaxNumberOfMessages = 1,
                        WaitTimeSeconds = 1
                    });
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "GetMessageAsync Error");
            }
            return response;
        }

        public async Task<SendMessageResponse> SendMessageToSqsQueueAsync(object request)
        {
            SendMessageResponse sendMessageResponse = new SendMessageResponse();
            try
            {
                using var client = new Amazon.SQS.AmazonSQSClient(GetCredentials(), Amazon.RegionEndpoint.EUWest2);
                var sendMessageRequest = new SendMessageRequest
                {
                    QueueUrl = _commonSettings.SQSQueueUrl,
                    MessageBody = JsonConvert.SerializeObject(request),
                    MessageGroupId = _commonSettings.SQSMessageGroupId,
                    MessageDeduplicationId = _commonSettings.SQSMessageDupId
                };

                sendMessageResponse = await client.SendMessageAsync(sendMessageRequest);
                return sendMessageResponse;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "SendMessageToSqsQueueAsync Error");
            }
            return sendMessageResponse;
        }

        private Amazon.Runtime.BasicAWSCredentials GetCredentials()
        {
            return new Amazon.Runtime.BasicAWSCredentials(_commonSettings.SQSAccessKey, _commonSettings.SQSSecretKey);
        }
    }
}

