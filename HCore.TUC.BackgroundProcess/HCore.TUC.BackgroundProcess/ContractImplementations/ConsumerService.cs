﻿using HCore.Data.Operations;
using HCore.Data.Operations.Models;
using HCore.TUC.BackgroundProcess.Contracts;
using HCore.TUC.Core.Framework.Merchant.App.AWS;
using HCore.TUC.Core.Object.Merchant;

namespace HCore.TUC.BackgroundProcess.ContractImplementations
{
    public class ConsumerService : IConsumerService
    {
        private readonly ILogger<ConsumerService> _logger;
        private readonly IConsumerSQSService _sqsService;
        private readonly IRewardService _rewardService;

        public ConsumerService(ILogger<ConsumerService> logger,
            IConsumerSQSService sqsService,
            IRewardService rewardService)
        {
            _logger = logger;
            _sqsService = sqsService;
            _rewardService = rewardService;
        }

        public async Task<bool> ProcessFileUploadRequest(OUpload.RewardUpload.MQUploadRequest request)
        {
            var result = false;
            try
            {
                var requeueReq = new OUpload.RewardUpload.MQUploadRequest
                {
                    AccountId = request.AccountId,
                    AccountKey = request.AccountKey,
                    Customers = new List<OUpload.RewardUpload.CustomerData>(),
                    FileName = request.FileName,
                    UserReference = request.UserReference,
                    CountryIsd = request.CountryIsd,
                    MobileNumberLength = request.MobileNumberLength,
                    MerchantId = request.MerchantId
                };

                if (request.Customers != null && request.Customers.Any())
                {
                    var completed = 0;
                    var success = 0;
                    var count = request.Customers.Count;

                    var taskList = new List<Task<OUpload.RewardUpload.RewardCustomerResponse>>();
                    foreach (var customer in request.Customers)
                    {
                        OUpload.RewardUpload.Item dataItem = new()
                        {
                            FirstName = customer.FirstName,
                            MiddleName = customer.MiddleName,
                            LastName = customer.LastName,
                            MobileNumber = customer.MobileNumber,
                            EmailAddress = customer.EmailAddress,
                            Gender = customer.Gender,
                            InvoiceAmount = customer.InvoiceAmount,
                            RewardAmount = customer.RewardAmount,
                            UserReference = request.UserReference ?? new Helper.OUserReference(),
                            FileName = request.FileName,
                            CountryIsd = request.CountryIsd ?? "",
                            MerchantId = request.MerchantId,
                            Name = $"{customer.FirstName} {customer.LastName}",
                            MobileNumberLength = request.MobileNumberLength
                        };

                        var task = _rewardService.RewardCustomerAsync(dataItem);
                        taskList.Add(task);

                        completed++;
                    }

                    await Task.WhenAll(taskList);

                    for (int i = 0; i < taskList.Count; i++)
                    {
                        var task = taskList[i];
                        var response = task.Result;

                        if (response != null)
                        {
                            if (response.Status == OUpload.RewardUpload.RewardCustomerStatus.Completed ||
                                response.Status == OUpload.RewardUpload.RewardCustomerStatus.PartlyProcessed)
                            {
                                success++;
                            }
                        }
                    }

                    try
                    {
                        if (!string.IsNullOrEmpty(request.FileName))
                        {
                            var isRequeued = request.FileName.Contains("_Requeue");
                            var fileNameWithoutRequeue = isRequeued ? request.FileName.Replace("_Requeue", "") : request.FileName;

                            using var operationsCtx = new HCoreContextOperations();
                            var fileUpload = operationsCtx.HCOUploadFile.FirstOrDefault(x => x.Name.ToLower() == request.FileName.ToLower() ||
                                (x.Name.ToLower() == fileNameWithoutRequeue.ToLower() && isRequeued));

                            if (fileUpload != null)
                            {
                                if (isRequeued)
                                {
                                    fileUpload.Success += success;
                                }
                                else
                                {
                                    fileUpload.TotalRecord += request.Customers.Count;
                                    fileUpload.Completed += completed;
                                    fileUpload.Success += success;
                                }
                            }
                            else
                            {
                                fileUpload = new HCOUploadFile
                                {
                                    TypeId = 1,
                                    OwnerId = request.AccountId,
                                    Name = request.FileName ?? "",
                                    TotalRecord = request.Customers.Count,
                                    Pending = request.Customers.Count,
                                    Processing = 0,
                                    Completed = completed,
                                    Success = success,
                                    Error = 0,
                                    CreatedById = request.AccountId,
                                    StatusId = 1,
                                    CreateDate = DateTime.UtcNow
                                };

                                operationsCtx.HCOUploadFile.Add(fileUpload);
                            }

                            operationsCtx.SaveChanges();
                        }

                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, "HCOUploadFile error");
                    }

                    _logger.LogInformation("Message Reports for {0}...", request.FileName);
                    _logger.LogInformation("Customer Count: {0}. Successfully Processed: {1}.", count, success);
                    result = true;
                }

                if (requeueReq != null && requeueReq.Customers.Any())
                {
                    //Requeue
                    try
                    {
                        _logger.LogInformation("Requeue...");
                        if (!string.IsNullOrEmpty(requeueReq.FileName))
                        {
                            var isXlsx = requeueReq.FileName.Contains(".xlsx");
                            var fileNamePrefix = isXlsx ? requeueReq.FileName.Replace(".xlsx", "") :
                                requeueReq.FileName.Replace(".xls", "");
                            requeueReq.FileName = isXlsx ? $"{fileNamePrefix}_Requeue.xlsx" : $"{fileNamePrefix}_Requeue.xls";

                            await _sqsService.SendMessageToSqsQueueAsync(requeueReq);
                        }

                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, "Requeue error");
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while processing data");
            }

            return result;
        }
    }
}

