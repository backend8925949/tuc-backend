﻿using HCore.TUC.BackgroundProcess.Configs;
using HCore.TUC.BackgroundProcess.Contracts;
using HCore.TUC.Core.Object.Merchant;
using Newtonsoft.Json;

namespace HCore.TUC.BackgroundProcess;

public class Worker : BackgroundService
{
    private readonly ILogger<Worker> _logger;
    private readonly IConsumerService _consumerService;
    private readonly IConsumerSQSService _sqsService;
    private readonly CommonSettings _commonSettings;

    public Worker(ILogger<Worker> logger,
        IConsumerService consumerService,
        IConsumerSQSService sqsService,
        CommonSettings commonSettings)  
    {
        _logger = logger;
        _consumerService = consumerService;
        _sqsService = sqsService;
        _commonSettings = commonSettings;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("Starting....");
        var delayInSecs = 1;
        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                var messageResponse = await _sqsService.GetMessageAsync();
                if(messageResponse != null && messageResponse.Messages.Count > 0)
                {
                    try
                    {
                        var message = messageResponse.Messages[0];
                        var messageId = message.MessageId;
                        var messageBody = message.Body;
                        var rewardRequest = JsonConvert.DeserializeObject<OUpload.RewardUpload.MQUploadRequest>(messageBody);


                        if (rewardRequest != null && rewardRequest.AccountId > 0
                            && !string.IsNullOrEmpty(rewardRequest.AccountKey)
                            && rewardRequest.UserReference != null)
                        {
                            if(!string.IsNullOrEmpty(rewardRequest.Environment) && !string.IsNullOrEmpty(_commonSettings.Environment) &&
                                rewardRequest.Environment.ToUpper() == _commonSettings.Environment.ToUpper())
                            {
                                _logger.LogInformation($"Acknowledge message receipt, preprocessing. Removing message-{messageId} from queue...");
                                await _sqsService.DeleteMessageAsync(message);

                                _logger.LogInformation($"Processing message-{messageId} at {DateTime.UtcNow.ToString("yyyyMMddHHmmss")}");
                                bool _ = await _consumerService.ProcessFileUploadRequest(rewardRequest);
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        _logger.LogError(ex, "Bad Message");
                    }
                }
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "An exception occured while processing message.");
            }
            
            await Task.Delay(delayInSecs * 1000, stoppingToken);
        }
    }
}
