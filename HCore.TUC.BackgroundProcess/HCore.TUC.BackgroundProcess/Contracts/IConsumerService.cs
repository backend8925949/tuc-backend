﻿using HCore.TUC.Core.Object.Merchant;

namespace HCore.TUC.BackgroundProcess.Contracts
{
	public interface IConsumerService
	{
        Task<bool> ProcessFileUploadRequest(OUpload.RewardUpload.MQUploadRequest request);
    }
}

