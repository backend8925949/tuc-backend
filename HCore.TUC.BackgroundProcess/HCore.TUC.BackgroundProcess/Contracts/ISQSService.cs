﻿using Amazon.SQS.Model;

namespace HCore.TUC.BackgroundProcess.Contracts
{
	public interface IConsumerSQSService
	{
        Task<ReceiveMessageResponse> GetMessageAsync();
        Task DeleteMessageAsync(Message message);
        Task<SendMessageResponse> SendMessageToSqsQueueAsync(object request);
    }
}

