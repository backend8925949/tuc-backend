﻿namespace HCore.TUC.BackgroundProcess.Configs
{
    public class AppSettings
    {
        public string? PaystackPrivateKey { get; set; }
        public string? PaystackPublicKey { get; set; }
        public string? PaystackGhanaPrivateKey { get; set; }

        public string? VasCoralPayUrl { get; set; }
        public string? VasCoralPayUserName { get; set; }
        public string? VasCoralPayPassword { get; set; }

        public string? Shipmonk_Url { get; set; }
        public string? Shipmonk_PublicKey { get; set; }
        public string? Shipmonk_SecretKey { get; set; }

        public string? EvolveUrl { get; set; }
        public string? EvolveApiKey { get; set; }
        public int EvolveProductId { get; set; }


        public string ExpresspayUrl { get; set; } = string.Empty;
        public string ExpresspayAuthToken { get; set; } = string.Empty;
        public string ExpresspayApiKey { get; set; } = string.Empty;

        public string? DellymenUrl { get; set; }
        public string? DellymenApiKey { get; set; }
        public string DellymanWebhookSecret { get; set; } = string.Empty;

        public string? PickUpRequestedTime { get; set; }
        public string? DeliveryRequestedTime { get; set; }

        public string HostOperations { get; set; } = string.Empty;
        public string HostLogging { get; set; } = string.Empty;
        public string Host { get; set; } = string.Empty;
    }
}
