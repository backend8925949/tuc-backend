﻿using HCore.Data.Helper;
using HCore.Helper;

namespace HCore.TUC.BackgroundProcess.Configs
{
    public class AppConfiguration
    {
        public static void SetupConfigs(CommonSettings? commonSettings, AppSettings? appSettings)
        {
            if (commonSettings != null)
            {
                HCoreConstant._AppConfig.PanelUrl = new OPanelUrl();
                HCoreConstant._AppConfig.Default_Icon = commonSettings.Default_Icon;
                HCoreConstant._AppConfig.Default_Poster = commonSettings.Default_Poster;
                HCoreConstant._AppConfig.StorageUrl = commonSettings.StorageUrl;
                HCoreConstant._AppConfig.StorageLocation = commonSettings.StorageLocation;
                HCoreConstant._AppConfig.StorageAccessKey = commonSettings.StorageAccessKey;
                HCoreConstant._AppConfig.StoragePrivateKey = commonSettings.StoragePrivateKey;

                HCoreConstant._AppConfig.SendGridKey = commonSettings.SendGridKey;
                HCoreConstant._AppConfig.SendGridSenderEmail = commonSettings.SendGridSenderEmail;
                HCoreConstant._AppConfig.SendGridSenderName = commonSettings.SendGridSenderName;
                HCoreConstant._AppConfig.SendGridDefaultReceiverName = commonSettings.SendGridDefaultReceiverName;

                HCoreConstant._AppConfig.AppUserPrefix = commonSettings.AppUserPrefix;
                HCoreConstant._AppConfig.StorageSourceId = commonSettings.StorageSourceId;
                HCoreConstant._AppConfig.StorageSourceCode = commonSettings.StorageSourceCode;

                HCoreConstant._AppConfig.RabbitMQHostName = commonSettings.RBMQHostName;
                HCoreConstant._AppConfig.RabbitMQUsername = commonSettings.RBMQUsername;
                HCoreConstant._AppConfig.RabbitMQPassword = commonSettings.RBMQPassword;
            }

            if (appSettings != null)
            {
                HCoreConstant._AppConfig.PaystackPrivateKey = appSettings.PaystackPrivateKey;
                HCoreConstant._AppConfig.PaystackPublicKey = appSettings.PaystackPublicKey;
                HCoreConstant._AppConfig.PaystackGhanaPrivateKey = appSettings.PaystackGhanaPrivateKey;

                HCoreConstant._AppConfig.VasCoralPayUrl = appSettings.VasCoralPayUrl;
                HCoreConstant._AppConfig.VasCoralPayUserName = appSettings.VasCoralPayUserName;
                HCoreConstant._AppConfig.VasCoralPayPassword = appSettings.VasCoralPayPassword;

                HCoreConstant._AppConfig.Shipmonk_Url = appSettings.Shipmonk_Url;
                HCoreConstant._AppConfig.Shipmonk_PublicKey = appSettings.Shipmonk_PublicKey;
                HCoreConstant._AppConfig.Shipmonk_SecretKey = appSettings.Shipmonk_SecretKey;

                HCoreConstant._AppConfig.EvolveUrl = appSettings.EvolveUrl;
                HCoreConstant._AppConfig.EvolveApiKey = appSettings.EvolveApiKey;
                HCoreConstant._AppConfig.EvolveProductId = appSettings.EvolveProductId;

                HCoreConstant._AppConfig.ExpresspayUrl = appSettings.ExpresspayUrl;
                HCoreConstant._AppConfig.ExpresspayAuthToken = appSettings.ExpresspayAuthToken;
                HCoreConstant._AppConfig.ExpresspayApiKey = appSettings.ExpresspayApiKey;

                HCoreConstant._AppConfig.DellymenUrl = appSettings.DellymenUrl;
                HCoreConstant._AppConfig.DellymenApiKey = appSettings.DellymenApiKey;
                HCoreConstant._AppConfig.DellymanWebhookSecret = appSettings.DellymanWebhookSecret;

                HCoreConstant._AppConfig.PickUpRequestedTime = appSettings.PickUpRequestedTime;
                HCoreConstant._AppConfig.DeliveryRequestedTime = appSettings.DeliveryRequestedTime;

                HostHelper.HostOperations = appSettings.HostOperations;
                HostHelper.HostLogging = appSettings.HostLogging;
                HostHelper.Host = appSettings.Host;

            }
        }
    }
}
