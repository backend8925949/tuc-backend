﻿
namespace HCore.TUC.BackgroundProcess.Configs
{
    public class CommonSettings
    {
        public string? Environment { get; set; }

        public string? Default_Icon { get; set; }
        public string? Default_Poster { get; set; }
        public string? StorageUrl { get; set; }
        public string? StorageLocation { get; set; }
        public string? StorageAccessKey { get; set; }
        public string? StoragePrivateKey { get; set; }


        public string? SendGridKey { get; set; }
        public string? SendGridSenderEmail { get; set; }
        public string? SendGridSenderName { get; set; }
        public string? SendGridDefaultReceiverName { get; set; }

        public string? AppUserPrefix { get; set; }
        public int StorageSourceId { get; set; }
        public string? StorageSourceCode { get; set; }

        public string? RBMQHostName { get; set; }
        public string? RBMQUsername { get; set; }
        public string? RBMQPassword { get; set; }

        public int MaxRetries { get; set; }

        public string? SQSQueueUrl { get; set; }
        public string? SQSMessageGroupId { get; set; }
        public string? SQSMessageDupId { get; set; }
        public string? SQSAccessKey { get; set; }
        public string? SQSSecretKey { get; set; }
    }
}

