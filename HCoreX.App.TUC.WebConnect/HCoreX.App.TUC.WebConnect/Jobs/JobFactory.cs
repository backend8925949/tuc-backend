//==================================================================================
// FileName: JobFactory.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using HCore.ThankUCash;
using HCore.TUC.Core.Framework.Merchant.BulkRewards;
//using HCore.TUC.Core.Framework.Merchant.Upload;
//using HCore.TUC.Core.Operations.Background;
using HCore.TUC.Core.Operations.Cron;
using HCore.TUC.Core.Operations.CustomerWeb;
using HCore.TUC.Core.Operations.Operations;
using HCore.TUC.Plugins.Deals;
using HCore.TUC.Plugins.Loan;
using HCore.TUC.SmsCampaign;
using Quartz;
using Quartz.Impl;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace HCoreX.Api.Jobs
{
    public class HCoreXJobFactory
    {
        public async Task StartJobAsync()
        {

            ISchedulerFactory _ISchedulerFactory = new StdSchedulerFactory();
            IScheduler _IScheduler = await _ISchedulerFactory.GetScheduler();
            await _IScheduler.Start();

            // 1 HOUR
            #region 1 HOUR Job
            IJobDetail _IJobDetail_JOB_1_MIN = JobBuilder.Create<JOB_1_MIN>()
                                    .WithIdentity("JOB_1MIN", "JOB_1_MIN_JOB_Group")
                                    .Build();
            ITrigger _ITrigger_JOB_1_MIN = TriggerBuilder.Create()
            .WithIdentity("JOB_1_MIN", "JOB_1_MIN_TRIGGER_Group")
            .ForJob(_IJobDetail_JOB_1_MIN)
            .StartNow()
            .WithSimpleSchedule(x => x.WithIntervalInMinutes(3).RepeatForever())
            //.WithCronSchedule("0/60 * 0 ? * * *")
            .Build();
            await _IScheduler.ScheduleJob(_IJobDetail_JOB_1_MIN, _ITrigger_JOB_1_MIN);
            #endregion
            // _15 MIN
            #region _15 MIN Job
            IJobDetail _IJobDetail_JOB_15_MIN = JobBuilder.Create<JOB_15_MIN>()
                                    .WithIdentity("JOB_5MIN", "JOB_15_MIN_JOB_Group")
                                    .Build();
            ITrigger _ITrigger_JOB_15_MIN = TriggerBuilder.Create()
            .WithIdentity("JOB_15_MIN", "JOB_15_MIN_TRIGGER_Group")
            .ForJob(_IJobDetail_JOB_15_MIN)
            .StartNow()
            .WithSimpleSchedule(x => x.WithIntervalInMinutes(15).RepeatForever())
            .Build();
            await _IScheduler.ScheduleJob(_IJobDetail_JOB_15_MIN, _ITrigger_JOB_15_MIN);
            #endregion
            #region 1 HOUR Job
            IJobDetail _IJobDetail_JOB_1_HOUR = JobBuilder.Create<JOB_1_HOUR>()
                                    .WithIdentity("JOB_1_HOUR", "JOB_1_HOUR_JOB_Group")
                                    .Build();
            ITrigger _ITrigger_JOB_1_HOUR = TriggerBuilder.Create()
            .WithIdentity("JOB_1_HOUR", "JOB_1_HOUR_TRIGGER_Group")
            .ForJob(_IJobDetail_JOB_1_HOUR)
            .StartNow()
            .WithSimpleSchedule(x => x.WithIntervalInHours(1).RepeatForever())
            //.WithCronSchedule("0 0 0/1 1/1 * ? *")
            .Build();
            await _IScheduler.ScheduleJob(_IJobDetail_JOB_1_HOUR, _ITrigger_JOB_1_HOUR);
            #endregion




            #region 23 HOUR Job
            IJobDetail _IJobDetail_JOB_23_HOUR = JobBuilder.Create<JOB_23_HOUR>()
                                    .WithIdentity("JOB_23_HOUR", "JOB_23_HOUR_JOB_Group")
                                    .Build();
            ITrigger _ITrigger_JOB_23_HOUR = TriggerBuilder.Create()
            .WithIdentity("JOB_23_HOUR", "JOB_23_HOUR_TRIGGER_Group")
            .ForJob(_IJobDetail_JOB_23_HOUR)
            .StartNow()
            .WithSimpleSchedule(x => x.WithIntervalInHours(23).RepeatForever())
            //.WithCronSchedule("59 23 * * 1-7")
            .Build();
            await _IScheduler.ScheduleJob(_IJobDetail_JOB_23_HOUR, _ITrigger_JOB_23_HOUR);
            #endregion


            #region 24 HOURS Job
            IJobDetail _IJobDetail_JOB_24_HOURS = JobBuilder.Create<JOB_24_HOUR>()
                                    .WithIdentity("JOB_24_HOURS", "JOB_24_HOURS_JOB_Group")
                                    .Build();
            ITrigger _ITrigger_JOB_24_HOURS = TriggerBuilder.Create()
            .WithIdentity("JOB_24_HOURS", "JOB_24_HOURS_TRIGGER_Group")
            .ForJob(_IJobDetail_JOB_24_HOURS)
            .StartNow()
            .WithCronSchedule("0 0 12 1/1 * ? *")
            .Build();
            await _IScheduler.ScheduleJob(_IJobDetail_JOB_24_HOURS, _ITrigger_JOB_24_HOURS);
            #endregion



            //#region 24 HOUR Job
            //IJobDetail _IJobDetail_JOB_24_HOUR = JobBuilder.Create<JOB_24_HOUR>()
            //                        .WithIdentity("JOB_24_HOUR", "JOB_24_HOUR_JOB_Group")
            //                        .Build();
            //ITrigger _ITrigger_JOB_24_HOUR = TriggerBuilder.Create()
            //.WithIdentity("JOB_24_HOUR", "JOB_24_HOUR_TRIGGER_Group")
            //.ForJob(_IJobDetail_JOB_24_HOUR)
            //.StartNow()
            //.WithSimpleSchedule(x => x.WithIntervalInHours(24).RepeatForever())
            ////.WithCronSchedule("0 0 9/24 ? * * *")
            //.Build();
            //await _IScheduler.ScheduleJob(_IJobDetail_JOB_24_HOUR, _ITrigger_JOB_24_HOUR);
            //#endregion


        }
    }
    public class JOB_1_HOUR : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            HCore.Data.Store.HCoreDataStoreManager.DataStore_Sync();
            if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Live)
            {
                HCore.TUC.Plugins.Vas.ManageBackgroundSync _ManageBackgroundSync = new HCore.TUC.Plugins.Vas.ManageBackgroundSync();
                _ManageBackgroundSync.SyncVasItem();
                _ManageBackgroundSync.ProcessPendingVasPayments();
            }
            int CurrentHour = HCoreHelper.GetGMTDateTime().Hour;
            if (CurrentHour == 0 ||
                CurrentHour == 3 ||
                CurrentHour == 9 ||
                CurrentHour == 12 ||
                CurrentHour == 15 ||
                CurrentHour == 18 ||
                CurrentHour == 21
                )
            {
                HCore.TUC.Core.Core.Loyalty.LoyaltyCron.ProcessPendingTransaction();

                #region Update merchant category for merchants with empty category id
                CronMerchantCategorySync _CronMerchantCategorySync = new CronMerchantCategorySync();
                _CronMerchantCategorySync.CoreCronMerchantCategorySync();
                #endregion

            }

            // PROCESS SERVER ACTIVITY
            if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.BackgroudProcessor)
            {
                if (CurrentHour == 3)
                {
                    ManageOverview _ManageOverview = new ManageOverview();
                    _ManageOverview.CreateRewardSettlementInvoices();
                }
            }
            if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.BackgroudProcessor || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Test)
            {
                HCore.TUC.Plugins.Vas.ManageBackgroundSync _ManageBackgroundSync = new HCore.TUC.Plugins.Vas.ManageBackgroundSync();
                _ManageBackgroundSync.SyncVasItem();
                //BackgroundActivityStatusManager _BackgroundActivityStatusManager = new BackgroundActivityStatusManager();
                //_BackgroundActivityStatusManager.UpdateAccountActivityStatus();
            }
        }
    }
    public class JOB_1_MIN : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            await Task.Delay(0);
            //ManagaeBulkRewardProcess _ManagaeBulkRewardProcess = new ManagaeBulkRewardProcess();
            //_ManagaeBulkRewardProcess.ProcessBulkRewardCustomerReward();
            //#region DEALS SYNC CRON
            //if ((HCoreConstant.HostName == "appconnect.thankucash.com" ||
            // HCoreConstant.HostName == "testappconnect.thankucash.com" ||
            // HCoreConstant.HostName == "testwebconnect.thankucash.com" ||
            // HCoreConstant.HostName == "webconnect.thankucash.co") && HCoreConstant.HostName != "localhost")
            //{
            //    HCore.TUC.Plugins.MadDeals.ManageCore _ManageCore = new HCore.TUC.Plugins.MadDeals.ManageCore();
            //    _ManageCore.ProcessSync();
            //}
            //#endregion         
            if ((HCoreConstant.HostName == "webconnect.thankucash.com" || HCoreConstant.HostName == "testwebconnect.thankucash.com") && HCoreConstant.HostName != "localhost")
            {
                ManageSmsBroadcaster _ManageSmsBroadcaster = new ManageSmsBroadcaster();
                _ManageSmsBroadcaster.OpenCampaign();
            }
            if ((
                HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Local
               || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Live
               || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Test
               || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Tech
               || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Dev || HCoreConstant.HostName == "localhost"))
            {
                //HCore.TUC.Plugins.Deals.ManageBackgroundDealCheck _ManageDealStatus = new ManageBackgroundDealCheck();
                //_ManageDealStatus.ManageDealStatus();
                //_ManageDealStatus.ManageFlashDealStatus();
                //_ManageDealStatus.ManageDealCodeStatus();
                //_ManageDealStatus.ManagePromotionStatus();
                //_ManageDealStatus.ManagePromoCodeStatus();
                //HCore.TUC.Plugins.Deals.PromoCode.ManageDealPromoCode _ManageDealPromoCode = new HCore.TUC.Plugins.Deals.PromoCode.ManageDealPromoCode();
                //_ManageDealPromoCode.ManagePromoCodeStatus();

                AcquirerCampaignStatusUpdate _AcquirerCampaignStatusUpdate = new AcquirerCampaignStatusUpdate();
                _AcquirerCampaignStatusUpdate.UpdateAcquirerCampaignStatus();

                HCore.TUC.Core.Operations.Acquirer.ManageOnboarding _AcqOnboarding = new HCore.TUC.Core.Operations.Acquirer.ManageOnboarding();
                _AcqOnboarding.GetOnboardedCustomerFiles();
            }
            //#region Process customer onboarding
            //HCore.TUC.Core.Operations.Acquirer.ManageOnboarding _AcqOnboarding = new HCore.TUC.Core.Operations.Acquirer.ManageOnboarding();
            //_AcqOnboarding.ProcessOnboardMerchants();
            //_AcqOnboarding.ProcessCustomerOnboarding();
            //#endregion

            //if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Test)
            //{
            //    if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Test && HCoreConstant.HostName != "localhost")
            //    {
            //        HCore.TUC.Core.Operations.Operations.ManageOperations _ManageOperations = new HCore.TUC.Core.Operations.Operations.ManageOperations();
            //        _ManageOperations.AddTestTransaction();
            //    }
            //}
        }
    }
    public class JOB_15_MIN : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            await Task.Delay(0);
            HCore.TUC.Core.Core.Loyalty.LoyaltyCron.ExpireInitialisedTransaction();
        }
    }
    public class JOB_23_HOUR : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            await Task.Delay(0);
            if ((HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Local
               || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Live
               || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Test
               || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Tech
               || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Dev)
               && HCoreConstant.HostName != "localhost")
            {
                ManageBackgroundDealCheck _ManageBackgroundDealCheck = new ManageBackgroundDealCheck();
                _ManageBackgroundDealCheck.DealCodeExpireNotification();
            }
        }
    }
    public class JOB_24_HOUR : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {

            await Task.Delay(0);
            if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Live)
            {
                ManageSubscription _ManageSubscription = new ManageSubscription();
                _ManageSubscription.SubscriptionRemainder();
                //_ManageSubscription.SubscriptionAutoRenewal();
                // HCore.TUC.Plugins.Loan.ManageDisbursement _ManageDisbursement = new HCore.TUC.Plugins.Loan.ManageDisbursement();
                // _ManageDisbursement.CreditLoanAmount();
                // _ManageDisbursement.UpdateRepaymentStatus();
                // _ManageDisbursement.UpdateRepaymentStatus1();
                // _ManageDisbursement.CheckOverdueRepayments();

                //ManageSendMails manageSendMails = new ManageSendMails();
                //manageSendMails.SendWeeklyReportToMerchant();
            }

        }
    }
}

