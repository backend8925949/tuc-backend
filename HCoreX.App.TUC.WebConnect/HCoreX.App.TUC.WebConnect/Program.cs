//==================================================================================
// FileName: Program.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using Amazon.SQS;
using HCore.Helper;
using HCore.TUC.Core.Framework.Merchant.App.AWS;
using HCore.TUC.Core.Framework.Merchant.BulkRewards;
using HCoreX.App.TUC.WebConnect.Core;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RabbitMQ.Client;



AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
var _WebApplicationBuilder = WebApplication.CreateBuilder(args);
_WebApplicationBuilder.Services.AddMvc(options => options.EnableEndpointRouting = false);
_WebApplicationBuilder.Services.AddControllers().AddNewtonsoftJson(opt =>
{
    opt.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
    opt.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
});
_WebApplicationBuilder.Services.AddControllers().AddNewtonsoftJson();
_WebApplicationBuilder.Services.Configure<KestrelServerOptions>(options =>
{
    options.Limits.MaxRequestBodySize = null;
    options.Limits.MaxRequestBufferSize = long.MaxValue;
    options.AllowSynchronousIO = true;
});

_WebApplicationBuilder.Services.Configure<IISServerOptions>(options =>
{
    options.MaxRequestBodySize = null;
    options.MaxRequestBodyBufferSize = int.MaxValue;
    options.AllowSynchronousIO = true;
});

System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

var _IApplicationBuilder = _WebApplicationBuilder.Build();
_IApplicationBuilder.Use(async (context, next) =>
{
    if (string.IsNullOrEmpty(HCoreConstant.HostName))
    {
        HCoreConstant.HostName = context.Request.Host.Host;
        HCoreXConfiguration.LoadConfiguration(context.Request.Host.Host);
        HCoreSystem.DataStore_InitializeSystem();
        HCore.Data.Store.HCoreDataStoreManager.DataStore_Start();

        ManagaeBulkRewardProcess _ManagaeBulkRewardProcess = new ManagaeBulkRewardProcess();
        _ManagaeBulkRewardProcess.ProcessBulkRewardCustomerReward();

        if (HCoreConstant.HostName == "connect.thankucash.com" || HCoreConstant.HostName == "beta-connect.thankucash.com" || HCoreConstant.HostName == "webconnect.thankucash.com" || HCoreConstant.HostName == "appconnect.thankucash.com"
        || HCoreConstant.HostName == "connect.thankucash.dev" || HCoreConstant.HostName == "connect.thankucash.tech" || HCoreConstant.HostName == "connect.thankucash.co"
        || HCoreConstant.HostName == "webconnect.thankucash.dev" || HCoreConstant.HostName == "webconnect.thankucash.tech" || HCoreConstant.HostName == "webconnect.thankucash.co"
        || HCoreConstant.HostName == "appconnect.thankucash.dev" || HCoreConstant.HostName == "appconnect.thankucash.tech" || HCoreConstant.HostName == "appconnect.thankucash.co")
        {
            HCore.TUC.Plugins.MadDeals.ManageCore _ManageCore = new HCore.TUC.Plugins.MadDeals.ManageCore();
            _ManageCore.StartMadDealsStorage();
        }
        if (HCoreConstant.HostEnvironment != HCoreConstant.HostEnvironmentType.Local)
        {
            HCoreXConfiguration.ShedularStartAsync().GetAwaiter().GetResult();
            HCore.TUC.Plugins.MadDeals.ManageCore _ManageCore = new HCore.TUC.Plugins.MadDeals.ManageCore();
            _ManageCore.StartMadDealsStorage();
        }
    }
    await next.Invoke();
});
_IApplicationBuilder.UseCors(_Builder => _Builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
_IApplicationBuilder.UseRouting();
_IApplicationBuilder.HCoreXAuth();
_IApplicationBuilder.UseMvc();
_IApplicationBuilder.Run();


