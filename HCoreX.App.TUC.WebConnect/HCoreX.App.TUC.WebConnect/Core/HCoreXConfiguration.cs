//==================================================================================
// FileName: HCoreXConfiguration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using HCore.Data.Helper;
using HCore.Helper;
using RabbitMQ.Client;
using static HCore.Helper.HCoreConstant;
using static Quartz.Logging.OperationName;

namespace HCoreX.App.TUC.WebConnect.Core
{
    public class WhiteListing
    {
        public string[] Development { get; set; }
        public string[] Qa { get; set; }
        public string[] Staging { get; set; }
        public string[] Production { get; set; }
    }
    internal static class HCoreXConfiguration
    {
        private static bool IsServerLoaded = false;
        internal static async Task ShedularStartAsync()
        {
            HCoreX.Api.Jobs.HCoreXJobFactory _JobFactory = new Api.Jobs.HCoreXJobFactory();
            await _JobFactory.StartJobAsync();
        }
        internal static void LoadConfiguration(string Host)
        {
            var configurationBuilder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").AddEnvironmentVariables();
            var configuration = configurationBuilder.Build();
            WhiteListing hostItems = configuration.GetSection("WhiteListing").Get<WhiteListing>()!;
            if (hostItems.Development.Any(x => x == Host))
            {
                HCoreConstant._AppConfig = configuration.GetSection("Development").Get<Globals>()!;
                HostHelper.Host = configuration["Development:Host:Master"]!;
                HostHelper.HostLogging = configuration["Development:Host:Logging"]!;
                HostHelper.HostOperations = configuration["Development:Host:Operations"]!;
            }
            else if (hostItems.Staging.Any(x => x == Host))
            {
                HCoreConstant.HostEnvironment = HCoreConstant.HostEnvironmentType.Test;
                HCoreConstant._AppConfig = configuration.GetSection("Staging").Get<Globals>()!;
                HostHelper.Host = configuration["Staging:Host:Master"]!;
                HostHelper.HostLogging = configuration["Staging:Host:Logging"]!;
                HostHelper.HostOperations = configuration["Staging:Host:Operations"]!;
            }
            else if (hostItems.Qa.Any(x => x == Host))
            {
                HCoreConstant.HostEnvironment = HCoreConstant.HostEnvironmentType.Tech;
                HCoreConstant._AppConfig = configuration.GetSection("Qa").Get<Globals>()!;
                HostHelper.Host = configuration["Qa:Host:Master"]!;
                HostHelper.HostLogging = configuration["Qa:Host:Logging"]!;
                HostHelper.HostOperations = configuration["Qa:Host:Operations"]!;
            }
            else if (hostItems.Production.Any(x => x == Host))
            {
                HCoreConstant.HostEnvironment = HCoreConstant.HostEnvironmentType.Live;
                HCoreConstant._AppConfig = configuration.GetSection("Production").Get<Globals>()!;
                HostHelper.Host = configuration["Production:Host:Master"]!;
                HostHelper.HostLogging = configuration["Production:Host:Logging"]!;
                HostHelper.HostOperations = configuration["Production:Host:Operations"]!;
            }
            else
            {
                //HCoreConstant._AppConfig = configuration.GetSection("Development").Get<Globals>()!;
                //HostHelper.Host = configuration["Development:Host:Master"]!;
                //HostHelper.HostLogging = configuration["Development:Host:Logging"]!;
                //HostHelper.HostOperations = configuration["Development:Host:Operations"]!;
                HCoreConstant.HostEnvironment = HCoreConstant.HostEnvironmentType.Live;
                HCoreConstant._AppConfig = configuration.GetSection("Production").Get<Globals>()!;
                HostHelper.Host = configuration["Production:Host:Master"]!;
                HostHelper.HostLogging = configuration["Production:Host:Logging"]!;
                HostHelper.HostOperations = configuration["Production:Host:Operations"]!;
            }

            #region OpenApi
            HCoreXAuthConnector.openApi.Add("login");
            HCoreXAuthConnector.openApi.Add("sysloginrequest");
            HCoreXAuthConnector.openApi.Add("sysloginconfirm");
            HCoreXAuthConnector.openApi.Add("resetpassowrd");
            HCoreXAuthConnector.openApi.Add("resetpassowrdconfirm");
            HCoreXAuthConnector.openApi.Add("signin");
            HCoreXAuthConnector.openApi.Add("register");
            HCoreXAuthConnector.openApi.Add("saveninjaregistration");
            HCoreXAuthConnector.openApi.Add("processaccount");
            HCoreXAuthConnector.openApi.Add("controllersignin");
            HCoreXAuthConnector.openApi.Add("getappconfiguration");
            HCoreXAuthConnector.openApi.Add("appusersignin");
            HCoreXAuthConnector.openApi.Add("appusersigninv2");
            HCoreXAuthConnector.openApi.Add("appusersigninwithpin");
            HCoreXAuthConnector.openApi.Add("forgotpasswordrequest");
            HCoreXAuthConnector.openApi.Add("forgotpasswordverify");
            HCoreXAuthConnector.openApi.Add("getcashieraccess");
            HCoreXAuthConnector.openApi.Add("requestotp");
            HCoreXAuthConnector.openApi.Add("verifyotp");
            HCoreXAuthConnector.openApi.Add("getaccbalops");
            HCoreXAuthConnector.openApi.Add("requestotpcon");
            HCoreXAuthConnector.openApi.Add("verifyotpcon");
            HCoreXAuthConnector.openApi.Add("ob_merchant_request");
            HCoreXAuthConnector.openApi.Add("ob_merchant_emailupdate");
            HCoreXAuthConnector.openApi.Add("ob_merchant_resendemail");
            HCoreXAuthConnector.openApi.Add("ob_merchant_emailverify");
            HCoreXAuthConnector.openApi.Add("ob_merchant_updatemobile");
            HCoreXAuthConnector.openApi.Add("ob_merchant_requestmverfication");
            HCoreXAuthConnector.openApi.Add("ob_merchant_requestmverficationconfirm");
            HCoreXAuthConnector.openApi.Add("getbankcodes");
            HCoreXAuthConnector.openApi.Add("guestcheckout");
            HCoreXAuthConnector.openApi.Add("getdeliverypricing");
            HCoreXAuthConnector.openApi.Add("getappuserbalance");
            HCoreXAuthConnector.openApi.Add("registeraccops");
            HCoreXAuthConnector.openApi.Add("requestotpussd");
            HCoreXAuthConnector.openApi.Add("verifyotpussd");
            HCoreXAuthConnector.openApi.Add("getcountries");
            HCoreXAuthConnector.openApi.Add("getregions");
            HCoreXAuthConnector.openApi.Add("getcities");
            HCoreXAuthConnector.openApi.Add("webpayconfirm");
            HCoreXAuthConnector.openApi.Add("webpayinitialize");
            HCoreXAuthConnector.openApi.Add("onboardmerchantv4");
            HCoreXAuthConnector.openApi.Add("onboardmerchantchangenumber");
            HCoreXAuthConnector.openApi.Add("onboardmerchant");
            HCoreXAuthConnector.openApi.Add("onboardmerchantverifynumber");
            HCoreXAuthConnector.openApi.Add("onboardmerchantrequestverification");
            HCoreXAuthConnector.openApi.Add("getcityareas");
            HCoreXAuthConnector.openApi.Add("getcategories");
            HCoreXAuthConnector.openApi.Add("notifysystransaction");
            HCoreXAuthConnector.openApi.Add("updateterminaltransaction");
            HCoreXAuthConnector.openApi.Add("creditgiftpointstocustomerweb");
            HCoreXAuthConnector.openApi.Add("updatesubscription");
            HCoreXAuthConnector.openApi.Add("connectdevice");
            HCoreXAuthConnector.openApi.Add("creategiftcard");
            HCoreXAuthConnector.openApi.Add("getappuserbalance");
            HCoreXAuthConnector.openApi.Add("savegiftcarduserdrafts");
            HCoreXAuthConnector.openApi.Add("requestotpvoicecall");
            HCoreXAuthConnector.openApi.Add("getdealmerchants");
            #endregion
        }
    }
}

