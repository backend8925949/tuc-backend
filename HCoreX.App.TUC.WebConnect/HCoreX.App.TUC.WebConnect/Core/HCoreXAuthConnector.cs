//==================================================================================
// FileName: HCoreXAuthConnector.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Linq;
using System.Text;
using Delivery.Object.Response.Shipments;
using HCore.Helper;
using HCore.TUC.Core.Object.CustomerWeb;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Customer;
using HCore.TUC.Core.Operations.CustomerWeb;
using HCore.TUC.Core.Operations.Operations;
using HCore.TUC.Plugins.Delivery.Operations;
using HCore.TUC.SmsCampaign;
using HCore.TUC.SmsCampaign.Object;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static HCore.CoreConstant.CoreHelpers;
using static HCore.Helper.HCoreConstant;
namespace HCoreX.App.TUC.WebConnect.Core
{
    public class HCoreXAuthConnector
    {
        public static List<string> openApi = new List<string>();
        private readonly RequestDelegate _next;
        public HCoreXAuthConnector(RequestDelegate next)
        {
            _next = next;
        }
        public async Task Invoke(HttpContext _HttpContext)
        {
            try
            {
                DateTime RequestTime = HCoreHelper.GetGMTDateTime();
                var _Request = _HttpContext.Request;
                var Url = _Request.Path.Value;
                int UrlParts = Url.Count(f => f == '/');
                if (!string.IsNullOrEmpty(Url) && Url.Count(f => f == '/') == 4)
                {
                    string ApiName = Url.Split("/")[4];
                    var _Headers = _Request.Headers;
                    if (ApiName == "launch")
                    {
                    }
                    else
                    {
                        string HAuthorization = _Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value;
                        string HAppKey = _Headers.Where(x => x.Key == "hcak").FirstOrDefault().Value;
                        if (string.IsNullOrEmpty(HAuthorization) && !string.IsNullOrEmpty(HAppKey))
                        {
                            var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                            var _RequestBodyContent = JsonConvert.DeserializeObject<OAuth.Request>(_RequestBody);
                            var ZxContent = HCoreEncrypt.DecodeText(_RequestBodyContent.zx);
                            JObject _JObject = JObject.Parse(ZxContent);
                            #region Validate Request
                            if (!string.IsNullOrEmpty(_RequestBodyContent.zx))
                            {
                                string JsonString = JsonConvert.SerializeObject(_RequestBodyContent,
                              new JsonSerializerSettings()
                              {
                                  NullValueHandling = NullValueHandling.Ignore
                              });

                                OAuth.Response _OAuthApp = HCoreAuth.Auth_App(_Request, JsonString, RequestTime, ApiName);
                                if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                                {
                                    if (openApi.Contains(ApiName))
                                    {
                                        _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(_Request.Headers.Where(x => x.Key == "hcuak").FirstOrDefault().Value))
                                        {
                                            OAuth.Response _OAuth = HCoreAuth.Auth_Request(_Request, _OAuthApp);
                                            if (_OAuth.Status == HCoreConstant.StatusSuccess)
                                            {
                                                OUserReference _UReference = _OAuth.UserReference;
                                                _JObject.Add("UserReference", JObject.FromObject(_UReference));
                                            }
                                            else
                                            {
                                                var Data = HCoreAuth.Auth_Response(_OAuth.UserResponse, _OAuth);
                                                var json = JsonConvert.SerializeObject(Data);
                                                _HttpContext.Response.StatusCode = 401; //UnAuthorized
                                                await _HttpContext.Response.WriteAsync(json);
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                        }

                                    }
                                    OAuth.Request _AuthRequest = new OAuth.Request();
                                    _AuthRequest.fx = _RequestBodyContent.fx;
                                    _AuthRequest.vx = _RequestBodyContent.vx;
                                    _AuthRequest.zx = HCoreEncrypt.EncodeText(JsonConvert.SerializeObject(_JObject));

                                    var Newjson = JsonConvert.SerializeObject(_AuthRequest);
                                    var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                    Stream stream = await requestContent.ReadAsStreamAsync();
                                    _HttpContext.Request.Body = stream;
                                }
                                else
                                {
                                    var Data = HCoreAuth.Auth_Response(_OAuthApp.UserResponse, _OAuthApp);
                                    var json = JsonConvert.SerializeObject(Data);
                                    _HttpContext.Response.StatusCode = 401;
                                    await _HttpContext.Response.WriteAsync(json);
                                    return;
                                }
                            }
                            else
                            {
                                _HttpContext.Response.StatusCode = 401;
                                return;
                            }
                            #endregion
                        }
                        else if (!string.IsNullOrEmpty(HAuthorization) && string.IsNullOrEmpty(HAppKey))
                        {
                            string HMode = _Headers.Where(x => x.Key == "Mode").FirstOrDefault().Value;
                            if ((HMode == "demo" || HMode == "web") && (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Test || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Local || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Dev || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Tech))
                            {
                                var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                                JObject _JObject = JObject.Parse(_RequestBody);
                                OAuth.Response _OAuthApp = HCoreAuth.Auth_Key(_Request, _RequestBody, HCoreHelper.GetGMTDateTime(), ApiName, "json");
                                if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                                {
                                    if (openApi.Contains(ApiName))
                                    {
                                        _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                    }
                                    else
                                    {
                                        OUserReference _UReference = _OAuthApp.UserReference;
                                        _JObject.Add("UserReference", JObject.FromObject(_UReference));
                                    }
                                    OAuth.Request _AuthRequest = new OAuth.Request();
                                    _AuthRequest.fx = "xx";
                                    _AuthRequest.vx = "xx";
                                    _AuthRequest.zx = HCoreEncrypt.EncodeText(JsonConvert.SerializeObject(_JObject));
                                    var Newjson = JsonConvert.SerializeObject(_AuthRequest);
                                    var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                    Stream stream = await requestContent.ReadAsStreamAsync();
                                    _HttpContext.Request.Body = stream;
                                }
                                else
                                {
                                    var Data = HCoreAuth.Auth_Response(_OAuthApp.UserResponse, _OAuthApp);
                                    var json = JsonConvert.SerializeObject(Data);
                                    _HttpContext.Response.StatusCode = 401;
                                    await _HttpContext.Response.WriteAsync(json);
                                    return;
                                }
                            }
                            else
                            {
                                _HttpContext.Response.StatusCode = 401;
                                return;
                            }
                        }
                        else
                        {
                            OAuth.Response _OAuthApp = HCoreAuth.Auth_App(_Request, null, RequestTime, ApiName);
                            if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                            {
                                var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                                JObject _JObject = JObject.Parse(_RequestBody);
                                _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                var Newjson = JsonConvert.SerializeObject(_JObject);
                                var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                Stream stream = await requestContent.ReadAsStreamAsync();
                                _HttpContext.Request.Body = stream;
                            }
                            else
                            {
                                string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                                var json = JsonConvert.SerializeObject(Data);
                                _HttpContext.Response.StatusCode = 401;
                                await _HttpContext.Response.WriteAsync(json);
                                return;
                            }
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(Url) && Url.Count(f => f == '/') == 5)
                {
                    string ApiName = Url.Split("/")[5];
                    var _Headers = _Request.Headers;
                    if (ApiName == "notifycp")
                    {
                        var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                        HCoreHelper.LogData(HCoreConstant.LogType.Log, "notifycp", HCoreHelper.GetGMTDateTime().ToString(), _RequestBody, null);
                        ManageBuyPoint _ManageBuyPoint = new ManageBuyPoint();
                        _ManageBuyPoint.BuyPointConfirmCoralPay(_RequestBody);
                        _HttpContext.Response.StatusCode = 200;
                        return;
                    }
                    if (ApiName == "notifysms")
                    {
                        var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                        HCoreHelper.LogData(HCoreConstant.LogType.Log, "notifysms", HCoreHelper.GetGMTDateTime().ToString(), _RequestBody, null);
                        HCoreHelper.UpdateSmsNotification(_RequestBody);
                        _HttpContext.Response.StatusCode = 200;
                        return;
                    }
                    if (ApiName == "notifysmsdelivery")
                    {
                        var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                        if (!string.IsNullOrEmpty(_RequestBody))
                        {
                            HCoreHelper.LogData(HCoreConstant.LogType.Log, "notifysmsdelivery", HCoreHelper.GetGMTDateTime().ToString(), _RequestBody, null);
                            OSmsCallBack _RequestBodyContent = JsonConvert.DeserializeObject<OSmsCallBack>(_RequestBody);
                            ManageSmsBroadcaster _ManageSmsBroadcaster = new ManageSmsBroadcaster();
                            var d = _ManageSmsBroadcaster.UpdateSmsStatus(_RequestBodyContent);
                            if (d == System.Net.HttpStatusCode.OK)
                            {
                                _HttpContext.Response.StatusCode = 200;
                            }
                            else
                            {
                                _HttpContext.Response.StatusCode = 401;
                            }
                            return;
                        }
                        else
                        {
                            _HttpContext.Response.StatusCode = 401;
                            return;
                        }
                    }
                    if (ApiName == "notifywemadelivery")
                    {
                        var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                        if (!string.IsNullOrEmpty(_RequestBody))
                        {
                            HCoreHelper.LogData(HCoreConstant.LogType.Log, "notifywemadelivery", HCoreHelper.GetGMTDateTime().ToString(), _RequestBody, null);
                            OSmsCallBack _RequestBodyContent = JsonConvert.DeserializeObject<OSmsCallBack>(_RequestBody);
                            _HttpContext.Response.StatusCode = 200;
                            return;
                        }
                        else
                        {
                            _HttpContext.Response.StatusCode = 401;
                            return;
                        }
                    }
                    if (ApiName == "notifyps")
                    {
                        var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                        HCoreHelper.LogData(HCoreConstant.LogType.Log, "notifyps", HCoreHelper.GenerateDateString(), _RequestBody, null);
                        if (_RequestBody != null)
                        {
                            OPaystackPayment.Request _RequestBodyContent = JsonConvert.DeserializeObject<OPaystackPayment.Request>(_RequestBody);
                            if (_RequestBodyContent != null)
                            {
                                if (_RequestBodyContent.data != null)
                                {
                                    if (_RequestBodyContent.data.reference.StartsWith("TPP"))
                                    {
                                        ManageBuyPoint _ManageBuyPoint = new ManageBuyPoint();
                                        _ManageBuyPoint.BuyPointConfirmPaystack(_RequestBody);
                                        _HttpContext.Response.StatusCode = 200;
                                    }
                                    else if (_RequestBodyContent.data.reference.StartsWith("tsp"))
                                    {
                                        ManageCashout _ManageCashOut = new ManageCashout();
                                        _ManageCashOut.UpdateCashOutStatus(_RequestBody);
                                        _HttpContext.Response.StatusCode = 200;
                                    }
                                    else
                                    {
                                        _HttpContext.Response.StatusCode = 401;
                                        return;
                                    }
                                }
                                else
                                {
                                    _HttpContext.Response.StatusCode = 401;
                                    return;
                                }
                            }
                            else
                            {
                                _HttpContext.Response.StatusCode = 401;
                                return;
                            }
                        }
                        else
                        {
                            _HttpContext.Response.StatusCode = 401;
                            return;
                        }
                    }
                    if (ApiName == "updatedeliverystatus")
                    {
                        var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                        HCoreHelper.LogData(HCoreConstant.LogType.Log, "updatedeliverystatus", HCoreHelper.GenerateDateString(), _RequestBody, null);
                        if (_RequestBody != null)
                        {
                            GetShipment Response = JsonConvert.DeserializeObject<GetShipment>(_RequestBody);
                            if (Response != null)
                            {
                                if (Response.data != null)
                                {
                                    if (Response.data.events != null)
                                    {
                                        ManageShipment _ManageShipment = new ManageShipment();
                                        _ManageShipment.UpdateOrderStatus(_RequestBody);
                                        _HttpContext.Response.StatusCode = 200;
                                    }
                                }
                            }
                        }
                    }
                    if (ApiName == "notifyordrsts")
                    {
                        var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                        HCoreHelper.LogData(HCoreConstant.LogType.Log, "notifyordrsts", HCoreHelper.GenerateDateString(), _RequestBody, null);
                        if (_RequestBody != null)
                        {
                            HCore.Integration.DellymanIntegrations.DellymanObject.Response.WebhookResponse? _RequestBodyContent = JsonConvert.DeserializeObject<HCore.Integration.DellymanIntegrations.DellymanObject.Response.WebhookResponse>(_RequestBody);
                            if (_RequestBodyContent != null)
                            {
                                if (_RequestBodyContent.Order != null && (!string.IsNullOrEmpty(_RequestBodyContent.Order.OrderCode)))
                                {
                                    HCore.TUC.Plugins.Delivery.Operations.ManageOrderProcess _ManageOrderProcess = new HCore.TUC.Plugins.Delivery.Operations.ManageOrderProcess();
                                    _ManageOrderProcess.UpdateOrderStatus(_RequestBody, HCoreConstant.DeliveryPartners.Dellyman);
                                    _HttpContext.Response.StatusCode = 200;
                                }
                                else
                                {
                                    _HttpContext.Response.StatusCode = 401;
                                    return;
                                }
                            }
                            else
                            {
                                _HttpContext.Response.StatusCode = 401;
                                return;
                            }
                        }
                        else
                        {
                            _HttpContext.Response.StatusCode = 401;
                            return;
                        }
                    }
                    if (ApiName == "updateordersstatus")
                    {
                        var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                        HCoreHelper.LogData(HCoreConstant.LogType.Log, "updateorderstatus-goshiip", HCoreHelper.GenerateDateString(), _RequestBody, null);
                        if (_RequestBody != null)
                        {
                            HCore.Integration.DeliveryIntegration.GoShiip.Response.ShipWebhookResponse? _RequestBodyContent = JsonConvert.DeserializeObject<HCore.Integration.DeliveryIntegration.GoShiip.Response.ShipWebhookResponse>(_RequestBody);
                            if (_RequestBodyContent != null)
                            {
                                if (_RequestBodyContent.shipment != null)
                                {
                                    if(_RequestBodyContent.shipment.data != null && !string.IsNullOrEmpty(_RequestBodyContent.shipment.data.status))
                                    {
                                        HCore.TUC.Plugins.Delivery.Operations.ManageOrderProcess _ManageOrderProcess = new HCore.TUC.Plugins.Delivery.Operations.ManageOrderProcess();
                                        _ManageOrderProcess.UpdateOrderStatus(_RequestBody, HCoreConstant.DeliveryPartners.GoShiip);
                                        _HttpContext.Response.StatusCode = 200;
                                    }
                                    else
                                    {
                                        _HttpContext.Response.StatusCode = 401;
                                        return;
                                    }
                                }
                                else
                                {
                                    _HttpContext.Response.StatusCode = 401;
                                    return;
                                }
                            }
                            else
                            {
                                _HttpContext.Response.StatusCode = 401;
                                return;
                            }
                        }
                        else
                        {
                            _HttpContext.Response.StatusCode = 401;
                            return;
                        }
                    }
                    if (ApiName == "notifyflutterwavepaymentstatus")
                    {
                        string? SecretHash = _Headers.Where(x => x.Key == "verif-hash").FirstOrDefault().Value;
                        if (!string.IsNullOrEmpty(SecretHash))
                        {
                            var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                            HCoreHelper.LogData(HCoreConstant.LogType.Log, "notifyflutterwavepaymentstatus", HCoreHelper.GenerateDateString(), _RequestBody, null);
                            if (_RequestBody != null)
                            {
                                OBuyPoint.WebHook.Response? _Response = JsonConvert.DeserializeObject<OBuyPoint.WebHook.Response?>(_RequestBody);
                                if (_Response != null)
                                {
                                    ManagePayment _ManagePayment = new ManagePayment();
                                    await _ManagePayment.BuyPointConfirmFlutterwave(_RequestBody);
                                }
                                else
                                {
                                    _HttpContext.Response.StatusCode = 401;
                                    return;
                                }
                            }
                            else
                            {
                                _HttpContext.Response.StatusCode = 401;
                                return;
                            }
                        }
                        else
                        {
                            _HttpContext.Response.StatusCode = 401;
                            return;
                        }
                    }
                    //if (ApiName == "uploadcustomerrewards")
                    //{
                    //    string appKey = _Headers.Where(x => x.Key == "hcak").FirstOrDefault().Value;
                    //    string appVersionKey = _Headers.Where(x => x.Key == "hcavk").FirstOrDefault().Value;
                    //    string appAuthorization = _Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value;
                    //    if (!string.IsNullOrEmpty(appKey) && !string.IsNullOrEmpty(appVersionKey) && !string.IsNullOrEmpty(appAuthorization))
                    //    {
                    //        var auth = HCoreAuth.AuthorizeBulkRewardEP(appVersionKey, appKey);
                    //        if (auth.Status != HCoreConstant.StatusSuccess)
                    //        {
                    //            _HttpContext.Response.StatusCode = 401;
                    //            await _HttpContext.Response.WriteAsync("Unauthorized Request");
                    //            return;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        _HttpContext.Response.StatusCode = 401;
                    //        await _HttpContext.Response.WriteAsync("Unauthorized Request");
                    //        return;
                    //    }
                    //}
                    else
                    {
                        string HAppKey = _Headers.Where(x => x.Key == "hcak").FirstOrDefault().Value;
                        string HAppAuthorization = _Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value;
                        if (!string.IsNullOrEmpty(HAppKey) && string.IsNullOrEmpty(HAppAuthorization))
                        {
                            var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                            var _RequestBodyContent = JsonConvert.DeserializeObject<OAuth.Request>(_RequestBody);
                            var ZxContent = HCoreEncrypt.DecodeText(_RequestBodyContent.zx);
                            JObject _JObject = JObject.Parse(ZxContent);
                            if (!string.IsNullOrEmpty(_RequestBodyContent.zx))
                            {
                                string JsonString = JsonConvert.SerializeObject(_RequestBodyContent,
                              new JsonSerializerSettings()
                              {
                                  NullValueHandling = NullValueHandling.Ignore
                              });

                                OAuth.Response _OAuthApp = HCoreAuth.Auth_App(_Request, JsonString, RequestTime, ApiName);
                                if (_OAuthApp.Status == StatusSuccess)
                                {
                                    if (openApi.Contains(ApiName))
                                    {
                                        _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(_Request.Headers.Where(x => x.Key == "hcuak").FirstOrDefault().Value))
                                        {
                                            OAuth.Response _OAuth = HCoreAuth.Auth_Request(_Request, _OAuthApp);
                                            if (_OAuth.Status == HCoreConstant.StatusSuccess)
                                            {
                                                OUserReference _UReference = _OAuth.UserReference;
                                                _JObject.Add("UserReference", JObject.FromObject(_UReference));
                                            }
                                            else
                                            {
                                                var Data = HCoreAuth.Auth_Response(_OAuth.UserResponse, _OAuth);
                                                var json = JsonConvert.SerializeObject(Data);
                                                _HttpContext.Response.StatusCode = 401; //UnAuthorized
                                                await _HttpContext.Response.WriteAsync(json);
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                        }
                                    }
                                    OAuth.Request _AuthRequest = new OAuth.Request();
                                    _AuthRequest.fx = _RequestBodyContent.fx;
                                    _AuthRequest.vx = _RequestBodyContent.vx;
                                    _AuthRequest.zx = HCoreEncrypt.EncodeText(JsonConvert.SerializeObject(_JObject));

                                    var Newjson = JsonConvert.SerializeObject(_AuthRequest);
                                    var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                    Stream stream = await requestContent.ReadAsStreamAsync();
                                    _HttpContext.Request.Body = stream;

                                }
                                else
                                {
                                    var Data = HCoreAuth.Auth_Response(_OAuthApp.UserResponse, _OAuthApp);
                                    var json = JsonConvert.SerializeObject(Data);
                                    _HttpContext.Response.StatusCode = 401;
                                    await _HttpContext.Response.WriteAsync(json);
                                    return;
                                }
                            }
                            else
                            {
                                _HttpContext.Response.StatusCode = 401;
                                return;
                            }
                        }
                        else if (string.IsNullOrEmpty(HAppKey) && !string.IsNullOrEmpty(HAppAuthorization))
                        {
                            var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                            JObject _JObject = JObject.Parse(_RequestBody);
                            #region Validate Request
                            string JsonString = JsonConvert.SerializeObject(_JObject,
                              new JsonSerializerSettings()
                              {
                                  NullValueHandling = NullValueHandling.Ignore
                              });
                            OAuth.Response _OAuthApp = HCoreAuth.Auth_Key(_Request, JsonString, RequestTime, ApiName, "json");
                            if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                            {
                                string HMode = _Headers.Where(x => x.Key == "Mode").FirstOrDefault().Value;
                                if ((HMode == "dev" || HMode == "web") && (HCoreConstant.HostEnvironment == HostEnvironmentType.Live || HCoreConstant.HostEnvironment == HostEnvironmentType.Test || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Local || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Dev || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Tech))
                                {
                                    string HAuthorization = _Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value;
                                    string AccountType = _Headers.Where(x => x.Key == "AccountType").FirstOrDefault().Value;
                                    if (openApi.Contains(ApiName))
                                    {
                                        _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                    }
                                    else
                                    {
                                        OAuth.Response _OAuth = HCoreAuth.Auth_Request_Dev(_Request, _OAuthApp, AccountType);
                                        if (_OAuth.Status == HCoreConstant.StatusSuccess)
                                        {
                                            OUserReference _UReference = _OAuth.UserReference;
                                            _JObject.Add("UserReference", JObject.FromObject(_UReference));
                                        }
                                        else
                                        {
                                            var Data = HCoreAuth.Auth_Response(_OAuth.UserResponse, _OAuth);
                                            var json = JsonConvert.SerializeObject(Data);
                                            _HttpContext.Response.StatusCode = 401; //UnAuthorized
                                            await _HttpContext.Response.WriteAsync(json);
                                            return;
                                        }
                                    }
                                    OAuth.Request _AuthRequest = new OAuth.Request();
                                    _AuthRequest.fx = "xx";
                                    _AuthRequest.vx = "xx";
                                    _AuthRequest.zx = HCoreEncrypt.EncodeText(JsonConvert.SerializeObject(_JObject));
                                    var Newjson = JsonConvert.SerializeObject(_AuthRequest);
                                    var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                    Stream stream = await requestContent.ReadAsStreamAsync();
                                    _HttpContext.Request.Body = stream;
                                }
                                else
                                {
                                    var Data = HCoreAuth.Auth_Response(_OAuthApp.UserResponse, _OAuthApp);
                                    var json = JsonConvert.SerializeObject(Data);
                                    _HttpContext.Response.StatusCode = 401;
                                    await _HttpContext.Response.WriteAsync(json);
                                    return;
                                }
                            }
                            else
                            {
                                string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                                var json = JsonConvert.SerializeObject(Data);
                                _HttpContext.Response.StatusCode = 401;
                                await _HttpContext.Response.WriteAsync(json);
                                return;
                            }
                            #endregion
                        }
                        else
                        {
                            OAuth.Response _OAuthApp = HCoreAuth.Auth_App(_Request, null, RequestTime, ApiName);
                            if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                            {
                                var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                                JObject _JObject = JObject.Parse(_RequestBody);
                                _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                var Newjson = JsonConvert.SerializeObject(_JObject);
                                var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                Stream stream = await requestContent.ReadAsStreamAsync();
                                _HttpContext.Request.Body = stream;
                            }
                            else
                            {
                                string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                                var json = JsonConvert.SerializeObject(Data);
                                _HttpContext.Response.StatusCode = 401;
                                await _HttpContext.Response.WriteAsync(json);
                                return;
                            }
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(Url) && Url.Count(f => f == '/') == 6)
                {
                    string ApiName = Url.Split("/")[6];
                    if (ApiName == "notifywemadelivery")
                    {
                        var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                        if (!string.IsNullOrEmpty(_RequestBody))
                        {
                            HCoreHelper.LogData(HCoreConstant.LogType.Log, "notifywemadelivery", HCoreHelper.GetGMTDateTime().ToString(), _RequestBody, null);
                            OSmsCallBack _RequestBodyContent = JsonConvert.DeserializeObject<OSmsCallBack>(_RequestBody);
                            _HttpContext.Response.StatusCode = 200;
                            return;
                        }
                        else
                        {
                            _HttpContext.Response.StatusCode = 401;
                            return;
                        }
                    }


                    var _Headers = _Request.Headers;
                    string HAppAuthorization = _Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value;
                    if (!string.IsNullOrEmpty(HAppAuthorization))
                    {
                        var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                        JObject _JObject = JObject.Parse(_RequestBody);
                        #region Validate Request
                        string JsonString = JsonConvert.SerializeObject(_JObject,
                          new JsonSerializerSettings()
                          {
                              NullValueHandling = NullValueHandling.Ignore
                          });
                        OAuth.Response _OAuthApp = HCoreAuth.Auth_Key_Web_MadDeals(_Request, JsonString, RequestTime, ApiName);
                        if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                        {
                            _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                            if (_OAuthApp.AccountId > 0)
                            {
                                _JObject.Add("AuthAccountId", _OAuthApp.AccountId);
                            }
                            var Newjson = JsonConvert.SerializeObject(_JObject);
                            var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                            Stream stream = await requestContent.ReadAsStreamAsync();
                            _HttpContext.Request.Body = stream;
                        }
                        else
                        {
                            string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                            var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                            var json = JsonConvert.SerializeObject(Data);
                            _HttpContext.Response.StatusCode = 401;
                            await _HttpContext.Response.WriteAsync(json);
                            return;
                        }
                        #endregion
                    }
                    else
                    {
                        OAuth.Response _OAuthApp = HCoreAuth.Auth_App(_Request, null, RequestTime, ApiName);
                        if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                        {
                            var _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
                            JObject _JObject = JObject.Parse(_RequestBody);
                            _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                            var Newjson = JsonConvert.SerializeObject(_JObject);
                            var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                            Stream stream = await requestContent.ReadAsStreamAsync();
                            _HttpContext.Request.Body = stream;
                        }
                        else
                        {
                            string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                            var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                            var json = JsonConvert.SerializeObject(Data);
                            _HttpContext.Response.StatusCode = 401;
                            await _HttpContext.Response.WriteAsync(json);
                            return;
                        }
                    }
                }
                else
                {
                    _HttpContext.Response.StatusCode = 401;
                    return;
                }
                await _next.Invoke(_HttpContext);
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("HCoreXAuthConnector", _Exception);
                _HttpContext.Response.StatusCode = 401;
                return;
            }
        }
    }
    public static class HCoreXAuthConnectorExtension
    {
        public static IApplicationBuilder HCoreXAuth(this IApplicationBuilder app)
        {
            app.UseMiddleware<HCoreXAuthConnector>();
            return app;
        }
    }
}

