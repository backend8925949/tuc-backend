//==================================================================================
// FileName: CAProductController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for CA Product functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using static HCore.CoreConstant;
using HCore.ThankUCash;
using HCore.ThankUCash.Object;

namespace HCore.Api.App.V2
{
    [Produces("application/json")]
    [Route("api/v2/caproduct/[action]")]
    public class CAProductController
    {
        CAManageProduct _CAManageProduct;
        /// <summary>
        /// Description: Gets the product balance.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getproductbalance")]
        public object GetProductBalance([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCAProduct.BalanceRequest _Request = JsonConvert.DeserializeObject<OCAProduct.BalanceRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _CAManageProduct = new CAManageProduct();
            OResponse _Response = _CAManageProduct.GetProductBalance(_Request);
           return HCoreAuth.Auth_Response(_Response , _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Description: Credits the product account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("creditproductaccount")]
        public object CreditProductAccount([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCAProduct.CreditBalanceRequest _Request = JsonConvert.DeserializeObject<OCAProduct.CreditBalanceRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _CAManageProduct = new CAManageProduct();
            OResponse _Response = _CAManageProduct.CreditProductAccount(_Request);
           return HCoreAuth.Auth_Response(_Response , _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Description: Saves the product.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveproduct")]
        public object SaveProduct([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCAProduct.Manage _Request = JsonConvert.DeserializeObject<OCAProduct.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _CAManageProduct = new CAManageProduct();
            OResponse _Response = _CAManageProduct.SaveProduct(_Request);
           return HCoreAuth.Auth_Response(_Response , _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Description: Creates the quick gift card.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("createquickgiftcard")]
        public object CreateQuickGiftCard([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCAProduct.CreditBalanceRequest _Request = JsonConvert.DeserializeObject<OCAProduct.CreditBalanceRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _CAManageProduct = new CAManageProduct();
            OResponse _Response = _CAManageProduct.CreateQuickGiftCard(_Request);
           return HCoreAuth.Auth_Response(_Response , _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Description: Creates the gift card.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("creategiftcard")]
        public object CreateGiftCard([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCAProduct.CreditBalanceRequest _Request = JsonConvert.DeserializeObject<OCAProduct.CreditBalanceRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _CAManageProduct = new CAManageProduct();
            OResponse _Response = _CAManageProduct.CreateGiftCard(_Request);
           return HCoreAuth.Auth_Response(_Response , _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Description: Creates the gift card code.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("creategiftcardcode")]
        public object CreateGiftCardCode([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCAProduct.CreditBalanceRequest _Request = JsonConvert.DeserializeObject<OCAProduct.CreditBalanceRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _CAManageProduct = new CAManageProduct();
            OResponse _Response = _CAManageProduct.CreateGiftCardCode(_Request);
           return HCoreAuth.Auth_Response(_Response , _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Description: Gets the product code.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getproductcodes")]
        public object GetProductCode([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _CAManageProduct = new CAManageProduct();
            OResponse _Response = _CAManageProduct.GetProductCode(_Request);
           return HCoreAuth.Auth_Response(_Response , _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Description: Gets the product.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getproducts")]
        public object GetProduct([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _CAManageProduct = new CAManageProduct();
            OResponse _Response = _CAManageProduct.GetProduct(_Request);
           return HCoreAuth.Auth_Response(_Response , _Request.UserReference);
            #endregion
        }

        /// <summary>
        /// Description: Gets the reward product list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getrewardprductlist")]
        public object GetRewardProductList([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _CAManageProduct = new CAManageProduct();
            OResponse _Response = _CAManageProduct.GetRewardProductList(_Request);
           return HCoreAuth.Auth_Response(_Response , _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Description: Updates the prduct reward percentage.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateproductrewardpercentage")]
        public object UpdatePrductRewardPercentage([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCAProduct.RewardProductUpdate _Request = JsonConvert.DeserializeObject<OCAProduct.RewardProductUpdate>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _CAManageProduct = new CAManageProduct();
            OResponse _Response = _CAManageProduct.UpdatePrductRewardPercentage(_Request);
           return HCoreAuth.Auth_Response(_Response , _Request.UserReference);
            #endregion
        }
        
    }
}
