//==================================================================================
// FileName: ThankUCashTransactionController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for transaction functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 23-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using HCore.ThankU;
using HCore.ThankU.Object;
using static HCore.CoreConstant;
using HCore.ThankUCash;
using HCore.ThankUCash.Object;
namespace HCore.Api.App.V2
{
    [Produces("application/json")]
    [Route("api/v2/tuctranscore/[action]")]
    public class ThankUCashTransactionController : Controller
    {
        ManageTransactions _ManageTransactions;

        //


        /// <summary>
        /// Description: Gets the  reward claim transaction overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getrewardclaimtransactionsoverview")]
        public OAuth.Request GetRewardClaimTransactionOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetRewardClaimTransactionOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the  redeem transaction overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getredeemtransactionsoverview")]
        public OAuth.Request GetRedeemTransactionOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetRedeemTransactionOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Description: Gets the  sale transaction overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getsaletransactionsoverview")]
        public OAuth.Request GetSaleTransactionOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetSaleTransactionOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }



        /// <summary>
        /// Description: Gets the  reward transaction list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getrewardtransactions")]
        public OAuth.Request GetRewardTransaction([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetRewardTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the  reward transaction overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getrewardtransactionsoverview")]
        public OAuth.Request GetRewardTransactionOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetRewardTransactionOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the  pending reward transaction list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getpendingrewardtransactions")]
        public OAuth.Request GetPendingRewardTransaction([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetPendingRewardTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the  pending reward transaction overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getpendingrewardtransactionsoverview")]
        public OAuth.Request GetPendingRewardTransactionOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetPendingRewardTransactionOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }



        /// <summary>
        /// Description: Gets the  reward claim transaction list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getrewardclaimtransactions")]
        public OAuth.Request GetRewardClaimTransaction([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetRewardClaimTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the  redeem transaction.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getredeemtransactions")]
        public OAuth.Request GetRedeemTransaction([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetRedeemTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the  sale transaction list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getsaletransactions")]
        public OAuth.Request GetSaleTransaction([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetSaleTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the  acquirer merchant sales.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getacquirermerchantsales")]
        public OAuth.Request GetAcquirerMerchantSales([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetAcquirerMerchantSales(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Description: Gets the  transaction list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("gettransactions")]
        public OAuth.Request GetTransactions([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetTransactions(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the  transactions overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("gettransactionsoverview")]
        public OAuth.Request GetTransactionsOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetTransactionsOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the  transaction.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("gettransaction")]
        public OAuth.Request GetTransaction([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the reward transaction overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getacquirerrewardtransactionsoverview")]
        public OAuth.Request GetAcquirerRewardTransactionOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetAcquirerRewardTransactionOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Gets the redeem transaction overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getacquirerredeemtransactionsoverview")]
        public OAuth.Request GetAcquirerRedeemTransactionOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetAcquirerRedeemTransactionOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the acquirer redeem transaction list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getacquirerredeemtransactions")]
        public OAuth.Request GetAcquirerRedeemTransaction([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetAcquirerRedeemTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Gets the acquirer transaction.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getacquirertransaction")]
        public OAuth.Request GetAcquirerTransaction([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetAcquirerTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the  acquirer transaction overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getacquirertransactionoverview")]
        public OAuth.Request GetAcquirerTransactionOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetAcquirerTransactionOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Cancels the transaction
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("canceltransaction")]
        public OAuth.Request CancelTransaction([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.CancelTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the wallet history overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getwallethistoryoverview")]
        public OAuth.Request GetWalletHistoryOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetWalletHistoryOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


    }
}
