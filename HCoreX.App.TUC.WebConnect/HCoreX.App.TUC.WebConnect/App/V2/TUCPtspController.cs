//==================================================================================
// FileName: TUCPtspController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using static HCore.CoreConstant;
using HCore.ThankUCash;
using HCore.ThankUCash.Object;

namespace HCore.Api.App.V2
{
    [Produces("application/json")]
    [Route("api/v2/tucptsp/[action]")]
    public class TUCPtspController
    {
        //ManagePtspMerchant _ManagePtspMerchant;
        //[HttpPost]
        //[ActionName("getaccountoverview")]
        //public object GetAccountOverview([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OPtspManager.Analytics.Request _Request = JsonConvert.DeserializeObject<OPtspManager.Analytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManagePtspMerchant = new ManagePtspMerchant();
        //    OResponse _Response = _ManagePtspMerchant.GetAccountOverview(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //    #endregion
        //}




        //[HttpPost]
        //[ActionName("getmerchants")]
        //public object GetMerchants([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManagePtspMerchant = new ManagePtspMerchant();
        //    OResponse _Response = _ManagePtspMerchant.GetMerchant(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("getstores")]
        //public object GetStores([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManagePtspMerchant = new ManagePtspMerchant();
        //    OResponse _Response = _ManagePtspMerchant.GetStore(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //    #endregion
        //}


        //[HttpPost]
        //[ActionName("getterminals")]
        //public object GetTerminals([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManagePtspMerchant = new ManagePtspMerchant();
        //    OResponse _Response = _ManagePtspMerchant.GetTerminal(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //    #endregion
        //}
    }
}
