//==================================================================================
// FileName: TUCAnalyticsController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for analytics functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using static HCore.CoreConstant;
namespace HCore.Api.App.V2
{
    [Produces("application/json")]
    [Route("api/v2/tucanalytics/[action]")]
    public class TUCAnalyticsController
    {
        //AnalyticsTerminalSummary _AnalyticsTerminalSummary;
        //AnalyticsAccountSummary _AnalyticsAccountSummary;
        //AnalyticsCustomers _AnalyticsCustomers;


        /// <summary>
        /// Gets the account sales history hourly.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>


        /// <summary>
        /// Gets the terminal status count.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        //[HttpPost]
        //[ActionName("getterminalstatuscount")]
        //public OAuth.Request GetTerminalStatusCount([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _AnalyticsTerminalSummary = new AnalyticsTerminalSummary();
        //    OResponse _Response = _AnalyticsTerminalSummary.GetTerminalStatusCount(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        /// <summary>
        /// Gets the user counts.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        //[HttpPost]
        //[ActionName("getusercounts")]
        //public OAuth.Request GetUserCounts([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _AnalyticsAccountSummary = new AnalyticsAccountSummary();
        //    OResponse _Response = _AnalyticsAccountSummary.GetUserCounts(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}


        /// <summary>
        /// Gets the customer analytics.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        //[HttpPost]
        //[ActionName("getcustomeranalytics")]
        //public OAuth.Request GetCustomerAnalytics([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _AnalyticsCustomers = new AnalyticsCustomers();
        //    OResponse _Response = _AnalyticsCustomers.GetCustomerAnalytics(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

    }
}
