//==================================================================================
// FileName: HCCountryController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for country functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using static HCore.CoreConstant;
using HCore.ThankUCash;
using HCore.ThankUCash.Object;
using HCore.Operations;

namespace HCore.Api.App.V2
{
    [Produces("application/json")]
    [Route("api/v2/hccm/[action]")]
    public class HCCountryController
    {
        ManageCountry _ManageCountry;
        /// <summary>
        /// Description: Gets the country.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcountries")]
        public object GetCountry([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCountry = new ManageCountry();
            OResponse _Response = _ManageCountry.GetCountry(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Description: Gets the region.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getregions")]
        public object GetRegion([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCountry = new ManageCountry();
            OResponse _Response = _ManageCountry.GetRegion(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Description: Gets the city.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcities")]
        public object GetCity([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCountry = new ManageCountry();
            OResponse _Response = _ManageCountry.GetCity(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
        /// <summary>
        /// Description: Gets the city area.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcityareas")]
        public object GetCityArea([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCountry = new ManageCountry();
            OResponse _Response = _ManageCountry.GetCityArea(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
            #endregion
        }
    }
}
