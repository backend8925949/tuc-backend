//==================================================================================
// FileName: TUCCampaignController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for campaign functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 26-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using HCore.ThankU;
using HCore.ThankU.Object;
using static HCore.CoreConstant;
using HCore.ThankUCash;
using HCore.ThankUCash.Object;
namespace HCore.Api.App.V2
{
    [Produces("application/json")]
    [Route("api/v2/tuccampaign/[action]")]
    public class TUCCampaignController
    {


        ManageCampaign _ManageCampaign;
        ManageTransactions _ManageTransactions;

        /// <summary>
        /// Gets the campaign transactions.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcampaigntransactions")]
        public OAuth.Request GetCampaignTransactions([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetCampaignTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Duplicates the campaign.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("duplicatecampaign")]
        public OAuth.Request DuplicateCampaign([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCampaign.Request _Request = JsonConvert.DeserializeObject<OCampaign.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCampaign = new ManageCampaign();
            OResponse _Response = _ManageCampaign.DuplicateCampaign(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }



        /// <summary>
        /// Saves the campaign.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("savecampaign")]
        public OAuth.Request SaveCampaign([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCampaign.Request _Request = JsonConvert.DeserializeObject<OCampaign.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCampaign = new ManageCampaign();
            OResponse _Response = _ManageCampaign.SaveCampaign(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the campaign.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updatecampaign")]
        public OAuth.Request UpdateCampaign([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCampaign.Request _Request = JsonConvert.DeserializeObject<OCampaign.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCampaign = new ManageCampaign();
            OResponse _Response = _ManageCampaign.UpdateCampaign(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Deletes the campaign.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("deletecampaign")]
        public OAuth.Request DeleteCampaign([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCampaign.Request _Request = JsonConvert.DeserializeObject<OCampaign.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCampaign = new ManageCampaign();
            OResponse _Response = _ManageCampaign.DeleteCampaign(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Gets the campaign.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcampaign")]
        public OAuth.Request GetCampaign([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCampaign.Request _Request = JsonConvert.DeserializeObject<OCampaign.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCampaign = new ManageCampaign();
            OResponse _Response = _ManageCampaign.GetCampaign(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the campaigns.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcampaigns")]
        public OAuth.Request GetCampaigns([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCampaign = new ManageCampaign();
            OResponse _Response = _ManageCampaign.GetCampaign(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
    }
}
