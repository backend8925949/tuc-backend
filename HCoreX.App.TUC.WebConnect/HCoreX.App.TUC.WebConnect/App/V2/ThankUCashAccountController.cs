//==================================================================================
// FileName: ThankUCashAccountController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for thankucash account functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using HCore.ThankU;
using HCore.ThankU.Object;
using static HCore.CoreConstant;
using HCore.ThankUCash;
using HCore.ThankUCash.Object;
using HCore.Operations;
using HCore.Operations.Object;
namespace HCore.Api.App.V2
{
    [Produces("application/json")]
    [Route("api/v2/tucacccore/[action]")]
    public class ThankUCashAccountController : Controller
    {
        ManageAccounts _ManageAccounts;
        ManageCoreTransaction _ManageCoreTransaction;

        /// <summary>
        /// Description: Gets the user account balance.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuseraccountbalance")]
        public OAuth.Request GetUserAccountBalance([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OBalance.Request _Request = JsonConvert.DeserializeObject<OBalance.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreTransaction = new ManageCoreTransaction();
            OResponse _Response = _ManageCoreTransaction.GetUserAccountBalance(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the merchants.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getmerchants")]
        public OAuth.Request GetMerchants([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetMerchant(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Gets the stores.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getstores")]
        public OAuth.Request GetStores([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetStore(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Gets the stores lite.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getstoreslite")]
        public OAuth.Request GetStoresLite([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetStoreLite(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Gets the terminals.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getterminals")]
        public OAuth.Request GetTerminals([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetTerminal(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Gets the acquirers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getacquirers")]
        public OAuth.Request GetAcquirers([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetAcquirer(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Gets the cashiers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcashiers")]
        public OAuth.Request GetCashiers([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetCashier(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Gets the pg accounts.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getpgaccounts")]
        public OAuth.Request GetPgAccounts([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetPgAccount(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Gets the position accounts.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getposaccounts")]
        public OAuth.Request GetPosAccounts([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetPosAccount(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Description: Gets the application users.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getappusers")]
        public OAuth.Request GetAppUsers([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetAppUser(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Gets the application users lite.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getappuserslite")]
        public OAuth.Request GetAppUsersLite([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetAppUserLite(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Gets the application users visits.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getappuservisits")]
        public OAuth.Request GetAppUsersVisits([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetAppUsersVisit(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Description: Gets the application user cards.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getappusercards")]
        public OAuth.Request GetAppUserCards([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetAppUserCards(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Description: Gets the acquirer merchants.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getacquirermerchants")]
        public OAuth.Request GetAcquirerMerchants([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetAcquirerMerchants(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the relationship manager.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getrelationshipmanagers")]
        public OAuth.Request GetRelationshipManager([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetRelationshipManager(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }



        /// <summary>
        /// Description: Gets the sub account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getsubaccounts")]
        public OAuth.Request GetSubAccount([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.GetSubAccount(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

    }
}
