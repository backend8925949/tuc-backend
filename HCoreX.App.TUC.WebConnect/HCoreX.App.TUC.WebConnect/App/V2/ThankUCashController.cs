//==================================================================================
// FileName: ThankUCashController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for thank u cash.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.Object;
using Newtonsoft.Json;
using HCore.ThankU;
using HCore.ThankU.Object;
using static HCore.CoreConstant;
using HCore.ThankUCash;
using HCore.ThankUCash.Object;

namespace HCore.Api.App.v2
{
    [Produces("application/json")]
    [Route("api/v2/thankucash/[action]")]
    public class ThankUCashController : Controller
    {
        ManageUserInvoice _ManageUserInvoice;
        //ManageUserAccounts _ManageUserAccounts;
        //ManageUserTransactions _ManageUserTransactions;
        ManageOverview _ManageOverview;
        ManageLiveMerchants _ManageLiveMerchants;
        ManageLiveTerminals _ManageLiveTerminals;
        ManageMerchantOnboarding _ManageMerchantOnboarding;
        ManageCashier _ManageCashier;
        ManageMerchant _ManageMerchant;

        ManagePayments _ManagePayments;

        /// <summary>
        /// Description: Credits the merchant wallet.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("creditmerchantwallet")]
        public OAuth.Request CreditMerchantWallet([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OMerchantCredit.Request _Request = JsonConvert.DeserializeObject<OMerchantCredit    .Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchant = new ManageMerchant();
            OResponse _Response = _ManageMerchant.CreditMerchantWallet(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Description: Gets the bill payments.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getbillpayments")]
        public OAuth.Request GetBillPayments([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePayments = new ManagePayments();
            OResponse _Response = _ManagePayments.GetBillPayments(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        //[HttpPost]
        //[ActionName("getrewardtransactions")]
        //public OAuth.Request GetRewardTransactions([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserTransactions = new ManageUserTransactions();
        //    OResponse _Response = _ManageUserTransactions.GetRewardTransactions(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}
        //[HttpPost]
        //[ActionName("gettucrewardtransactions")]
        //public OAuth.Request GetTUCRewardTransactions([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserTransactions = new ManageUserTransactions();
        //    OResponse _Response = _ManageUserTransactions.GetTUCRewardTransactions(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("gettucplusrewardtransactions")]
        //public OAuth.Request GetTUCPlusRewardTransactions([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserTransactions = new ManageUserTransactions();
        //    OResponse _Response = _ManageUserTransactions.GetTUCPlusRewardTransactions(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}


        //[HttpPost]
        //[ActionName("gettucplusrewardtransfertransactions")]
        //public OAuth.Request GetTUCPlusTransferTransactions([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserTransactions = new ManageUserTransactions();
        //    OResponse _Response = _ManageUserTransactions.GetTUCPlusTransferTransactions(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("getredeemtransactions")]
        //public OAuth.Request GetRedeemTransactions([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserTransactions = new ManageUserTransactions();
        //    OResponse _Response = _ManageUserTransactions.GetRedeemTransactions(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}
        //[HttpPost]
        //[ActionName("getgifttransactions")]
        //public OAuth.Request GetGiftTransactions([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserTransactions = new ManageUserTransactions();
        //    OResponse _Response = _ManageUserTransactions.GetGiftTransactions(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}
        //[HttpPost]
        //[ActionName("getchangetransactions")]
        //public OAuth.Request GetChangeTransactions([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserTransactions = new ManageUserTransactions();
        //    OResponse _Response = _ManageUserTransactions.GetChangeTransactions(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}
        //[HttpPost]
        //[ActionName("getpaymenttransactions")]
        //public OAuth.Request GetPaymentTransactions([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserTransactions = new ManageUserTransactions();
        //    OResponse _Response = _ManageUserTransactions.GetPaymentTransactions(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}
        //[HttpPost]
        //[ActionName("getcommissiontransactions")]
        //public OAuth.Request GetCommissionTransactions([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserTransactions = new ManageUserTransactions();
        //    OResponse _Response = _ManageUserTransactions.GetCommissionTransactions(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("getsalestransactions")]
        //public OAuth.Request GetSalesTransactions([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserTransactions = new ManageUserTransactions();
        //    OResponse _Response = _ManageUserTransactions.GetSalesTransactions(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("gettransactions")]
        //public OAuth.Request GetTransactions([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserTransactions = new ManageUserTransactions();
        //    OResponse _Response = _ManageUserTransactions.GetTransactions(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}
        // [HttpPost]
        //[ActionName("getsaletransactions")]
        //public OAuth.Request GetSaleTransactions([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserTransactions = new ManageUserTransactions();
        //    OResponse _Response = _ManageUserTransactions.GetSaleTransactions(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}


        //[HttpPost]
        //[ActionName("getappusers")]
        //public OAuth.Request GetCoreHelpers([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserAccounts = new ManageUserAccounts();
        //    OResponse _Response = _ManageUserAccounts.GetAppUsers(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}


        //[HttpPost]
        //[ActionName("getmerchants")]
        //public OAuth.Request GetMerchants([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserAccounts = new ManageUserAccounts();
        //    OResponse _Response = _ManageUserAccounts.GetMerchants(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}


        //[HttpPost]
        //[ActionName("getacquirermerchants")]
        //public OAuth.Request GetAcquirerMerchants([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserAccounts = new ManageUserAccounts();
        //    OResponse _Response = _ManageUserAccounts.GetAcquirerMerchants(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("getstores")]
        //public OAuth.Request GetStores([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserAccounts = new ManageUserAccounts();
        //    OResponse _Response = _ManageUserAccounts.GetStores(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}
        /// <summary>
        /// Description: Gets the store bank collection.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getstorebankcollection")]
        public OAuth.Request GetStoreBankCollection([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OStoreBankCollection.Request _Request = JsonConvert.DeserializeObject<OStoreBankCollection.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetStoreBankCollection(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Updates the received amount.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updatereceivedamount")]
        public OAuth.Request UpdateReceivedAmount([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OStoreBankCollection.Request _Request = JsonConvert.DeserializeObject<OStoreBankCollection.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.UpdateReceivedAmount(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        //[HttpPost]
        //[ActionName("getacquirers")]
        //public OAuth.Request GetAcquirers([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserAccounts = new ManageUserAccounts();
        //    OResponse _Response = _ManageUserAccounts.GetAcquirers(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("getpgaccounts")]
        //public OAuth.Request GetPgAccounts([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserAccounts = new ManageUserAccounts();
        //    OResponse _Response = _ManageUserAccounts.GetPgAccounts(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("getposaccounts")]
        //public OAuth.Request GetPosAccounts([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserAccounts = new ManageUserAccounts();
        //    OResponse _Response = _ManageUserAccounts.GetPosAccounts(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}
        //[HttpPost]
        //[ActionName("posterminals")]
        //public OAuth.Request GetPosTerminals([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserAccounts = new ManageUserAccounts();
        //    OResponse _Response = _ManageUserAccounts.GetPosTerminals(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}
        //[HttpPost]
        //[ActionName("getcashiers")]
        //public OAuth.Request GetCashiers([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserAccounts = new ManageUserAccounts();
        //    OResponse _Response = _ManageUserAccounts.GetCashiers(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}
        //[HttpPost]
        //[ActionName("getappusersvisits")]
        //public OAuth.Request GetAppUsersVisits([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserAccounts = new ManageUserAccounts();
        //    OResponse _Response = _ManageUserAccounts.GetAppUsersVisits(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("getmerchantappusers")]
        //public OAuth.Request GetMerchantAppUsers([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserAccounts = new ManageUserAccounts();
        //    OResponse _Response = _ManageUserAccounts.GetMerchantAppUsers(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}


        /// <summary>
        /// Description: Gets the account balance overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getaccountbalanceoverview")]
        public OAuth.Request GetAccountBalanceOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetAccountBalanceOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        //[HttpPost]
        //[ActionName("getappusersoverview")]
        //public OAuth.Request GetAppUsersOverview([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageOverview = new ManageOverview();
        //    OResponse _Response = _ManageOverview.GetAppUsersOverview(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        /// <summary>
        /// Description: Gets the application overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getappuseroverview")]
        public OAuth.Request GetAppOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetAppOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        //[HttpPost]
        //[ActionName("getmerchantstoresaleoverview")]
        //public OAuth.Request GetMerchantStoreSaleOverview([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageOverview = new ManageOverview();
        //    OResponse _Response = _ManageOverview.GetMerchantStoreSaleOverview(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //ManageAgent _ManageAgent;
        //[HttpPost]
        //[ActionName("getownerpurchasesummarybydate")]
        //public OAuth.Request GetOwnerPurchaseSummaryByDate([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageAgent = new ManageAgent();
        //    OResponse _Response = _ManageAgent.GetOwnerPurchaseSummaryByDate(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("getmerchantsale")]
        //public OAuth.Request GetMerchantsSale([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageAgent = new ManageAgent();
        //    OResponse _Response = _ManageAgent.GetMerchantsSale(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        /// <summary>
        /// Description: Gets the overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getoverview")]
        public OAuth.Request GetOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the merchant overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getmerchantoverview")]
        public OAuth.Request GetMerchantOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetMerchantOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the store overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getstoreoverview")]
        public OAuth.Request GetStoreOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetStoreOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Description: Gets the merchant mini overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getmerchantminioverview")]
        public OAuth.Request GetMerchantMiniOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetMerchantMiniOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Gets the merchant overview lite.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getmerchantoverviewlite")]
        public OAuth.Request GetMerchantOverviewLite([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetMerchantOverviewLite(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Gets the acquirer overview lite.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getacquireroverviewlite")]
        public OAuth.Request GetAcquirerOverviewLite([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetAcquirerOverviewLite(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }



        /// <summary>
        /// Description: Gets the system overview lite.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getsystemoverviewlite")]
        public OAuth.Request GetSystemOverviewLite([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetSystemOverviewLite(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }



        /// <summary>
        /// Description: Gets the visit history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getvisithistory")]
        public OAuth.Request GetVisitHistory([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetVisitHistory(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the merchant application user overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getmerchantappuseroverview")]
        public OAuth.Request GetMerchantAppUserOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetMerchantAppUserOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }








        /// <summary>
        /// Description: Gets the account overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getaccountoverview")]
        public OAuth.Request GetAccountOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetAccountOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Gets the account overview lite.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getaccountoverviewlite")]
        public OAuth.Request GetAccountOverviewLite([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetAccountOverviewLite(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Description: Gets the position terminals status overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getposterminalsstatusoverview")]
        public OAuth.Request GetPosTerminalsStatusOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetPosTerminalsStatusOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        //[HttpPost]
        //[ActionName("getposterminalsoverviewlist")]
        //public OAuth.Request GetPosTerminalsOverviewList([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageOverview = new ManageOverview();
        //    OResponse _Response = _ManageOverview.GetPosTerminalsOverviewList(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}



        /// <summary>
        /// Description: Gets the reward overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getrewardoverview")]
        public OAuth.Request GetRewardOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetRewardOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the reward type overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getrewardtypeoverview")]
        public OAuth.Request GetRewardTypeOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetRewardTypeOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the redeem overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getredeemoverview")]
        public OAuth.Request GetRedeemOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetRedeemOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the application users overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getappusersoverview")]
        public OAuth.Request GetAppUsersOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OOverview.Request _Request = JsonConvert.DeserializeObject<OOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOverview = new ManageOverview();
            OResponse _Response = _ManageOverview.GetAppUsersOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }



        /// <summary>
        /// Description: Saves the merchant.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("savemerchant")]
        public OAuth.Request SaveMerchant([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserOnboarding.Merchant.Request _Request = JsonConvert.DeserializeObject<OUserOnboarding.Merchant.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new ManageMerchantOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.SaveMerchant(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }



        /// <summary>
        /// Description: Gets the user invoice.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuserinvoice")]
        public OAuth.Request GetUserInvoice([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OInvoice.Manage _Request = JsonConvert.DeserializeObject<OInvoice.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserInvoice = new ManageUserInvoice();
            OResponse _Response = _ManageUserInvoice.GetUserInvoice(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the user invoices.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuserinvoices")]
        public OAuth.Request GetUserInvoices([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserInvoice = new ManageUserInvoice();
            OResponse _Response = _ManageUserInvoice.GetUserInvoice(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Completes the payment invoice.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("completepaymentinvoice")]
        public OAuth.Request CompletePaymentInvoice([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OInvoice.Manage _Request = JsonConvert.DeserializeObject<OInvoice.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserInvoice = new ManageUserInvoice();
            OResponse _Response = _ManageUserInvoice.CompletePaymentInvoice(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Description: Gets the live merchants.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getlivemerchants")]
        public OAuth.Request GetLiveMerchants([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLiveMerchants = new ManageLiveMerchants();
            OResponse _Response = _ManageLiveMerchants.GetLiveMerchants(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the live terminals.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getliveterminals")]
        public OAuth.Request GetLiveTerminals([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLiveTerminals = new ManageLiveTerminals();
            OResponse _Response = _ManageLiveTerminals.GetLiveTerminals(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        ManagePostTransactionProcess _ManagePostTransactionProcess;
        /// <summary>
        /// Description: Updates the terminal transaction.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost] 
        [ActionName("notifysystransaction")]
        public OAuth.Request UpdateTerminalTransaction([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            HCore.Operations.Object.OCoreTransaction.Request _Request = JsonConvert.DeserializeObject<HCore.Operations.Object.OCoreTransaction.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePostTransactionProcess = new ManagePostTransactionProcess();
            OResponse _Response = _ManagePostTransactionProcess.ProcessTransactionDetails(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Generates the cashier identifier.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("generatecashierid")]
        public OAuth.Request GenerateCashierId([FromBody]OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashier = new ManageCashier();
            OResponse _Response = _ManageCashier.GenerateCashierId(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        

    }
}
