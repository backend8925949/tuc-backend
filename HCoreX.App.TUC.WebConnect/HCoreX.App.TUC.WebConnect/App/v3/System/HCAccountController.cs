//==================================================================================
// FileName: HCAccountController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for hcu account.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Account;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Core.Object.Acquirer;
using HCore.TUC.Core.Operations.Acquirer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using HCore.Account.Object;
namespace HCore.Api.App.v3.System
{
    [Produces("application/json")]
    [Route("api/v3/acc/manage/[action]")]
    public class HCUAccountController : Controller
    {
        ManageAccountOperation _ManageAccountOperation;

        /// <summary>
        /// Description: Resets the password initialize.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("resetpassowrd")]
        public object ResetPassword_Initialize([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperation.ResetPassword _Request = JsonConvert.DeserializeObject<OAccountOperation.ResetPassword>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccountOperation = new ManageAccountOperation();
            OResponse _Response = _ManageAccountOperation.ResetPassword_Initialize(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Resets the password confirm.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("resetpassowrdconfirm")]
        public object ResetPassword_Confirm([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperation.ResetPassword_Confirm _Request = JsonConvert.DeserializeObject<OAccountOperation.ResetPassword_Confirm>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccountOperation = new ManageAccountOperation();
            OResponse _Response = _ManageAccountOperation.ResetPassword_Confirm(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Resets the user name initialize.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("resetusername")]
        public object ResetUserName_Initialize([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperation.ResetUserName _Request = JsonConvert.DeserializeObject<OAccountOperation.ResetUserName>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccountOperation = new ManageAccountOperation();
            OResponse _Response = _ManageAccountOperation.ResetUserName_Initialize(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Resets the user name confirm.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("resetusernameconfirm")]
        public object ResetUserName_Confirm([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperation.ResetUserName_Confirm _Request = JsonConvert.DeserializeObject<OAccountOperation.ResetUserName_Confirm>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccountOperation = new ManageAccountOperation();
            OResponse _Response = _ManageAccountOperation.ResetUserName_Confirm(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Updates the password.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatepassword")]
        public object UpdatePassword([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperation.UpdatePassword _Request = JsonConvert.DeserializeObject<OAccountOperation.UpdatePassword>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccountOperation = new ManageAccountOperation();
            OResponse _Response = _ManageAccountOperation.UpdatePassword(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Updates the username.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateusername")]
        public object UpdateUsername([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountOperation.UpdateUsername _Request = JsonConvert.DeserializeObject<OAccountOperation.UpdateUsername>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccountOperation = new ManageAccountOperation();
            OResponse _Response = _ManageAccountOperation.UpdateUsername(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
