//==================================================================================
// FileName: TUCTransactionController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for transaction functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Operations.Merchant;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Merchant
{
    [Produces("application/json")]
    [Route("api/v3/merchant/trans/[action]")]
    public class TUCTransactionController : Controller
    {
        ManageTransactions _ManageTransactions;


        /// <summary>
        /// Description: Gets the reward claim transaction overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getrewardclaimtransactionsoverview")]
        public OAuth.Request GetRewardClaimTransactionOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetRewardClaimTransactionOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the redeem transaction overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getredeemtransactionsoverview")]
        public OAuth.Request GetRedeemTransactionOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetRedeemTransactionOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Description: Gets the sale transaction overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getsaletransactionsoverview")]
        public OAuth.Request GetSaleTransactionOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetSaleTransactionOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }



        /// <summary>
        /// Description: Gets the reward transaction.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getrewardtransactions")]
        public OAuth.Request GetRewardTransaction([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetRewardTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the reward transaction overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getrewardtransactionsoverview")]
        public OAuth.Request GetRewardTransactionOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetRewardTransactionOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Description: Gets the pending reward transaction.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getpendingrewardtransactions")]
        public OAuth.Request GetPendingRewardTransaction([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetPendingRewardTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the pending reward transaction overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getpendingrewardtransactionsoverview")]
        public OAuth.Request GetPendingRewardTransactionOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetPendingRewardTransactionOverview(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Description: Gets the reward claim transaction.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getrewardclaimtransactions")]
        public OAuth.Request GetRewardClaimTransaction([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetRewardClaimTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the redeem transaction.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getredeemtransactions")]
        public OAuth.Request GetRedeemTransaction([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetRedeemTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the sale transaction.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getsaletransactions")]
        public OAuth.Request GetSaleTransaction([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetSaleTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        //[HttpPost]
        //[ActionName("getacquirermerchantsales")]
        //public OAuth.Request GetAcquirerMerchantSales([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageTransactions = new ManageTransactions();
        //    OResponse _Response = _ManageTransactions.GetAcquirerMerchantSales(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}


        /// <summary>
        /// Description: Gets the transactions.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("gettransactions")]
        public OAuth.Request GetTransactions([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetTransactions(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Gets the pending reward.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getpendingrewards")]
        public OAuth.Request GetPendingReward([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetPendingReward(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Description: Updates the transaction.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updatetransaction")]
        public OAuth.Request UpdateTransaction([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.UpdateTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Endpoint to get redeem wallet transaction
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getredeemwalletfromparties")]
        public object GetRedeemWalletFromParties([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetAllRedeemTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the acquirer transaction.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getacquirertransaction")]
        public OAuth.Request GetAcquirerTransaction([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageTransactions = new ManageTransactions();
            OResponse _Response = _ManageTransactions.GetAcquirerTransaction(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
    }
}
