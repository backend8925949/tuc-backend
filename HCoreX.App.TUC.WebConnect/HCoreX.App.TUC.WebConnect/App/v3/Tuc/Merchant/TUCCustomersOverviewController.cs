//==================================================================================
// FileName: TUCCustomersOverviewController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for customer's overview functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Console.Analytics;
using HCore.TUC.Core.Object.Console.Analytics.Customer;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Operations.Console;
using HCore.TUC.Core.Operations.Console.Analytics.Customer;
using HCore.TUC.Core.Operations.Merchant;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Merchant
{
    [Produces("application/json")]
    [Route("api/v3/merchant/custoverview/[action]")]

    public class TUCCustomersOverviewController
    {
        ManageCustomersOverview _ManageCustomersOverview;

        /// <summary>
        /// Description: Gets the customers gender.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustbygender")]
        public object GetCustomersGender([FromBody] OAuth.Request _OAuthRequest)
        {
            OCustomersOverview.OAnalytics.Request _Request = JsonConvert.DeserializeObject<OCustomersOverview.OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomersOverview = new ManageCustomersOverview();
            OResponse _Response = _ManageCustomersOverview.GetCustomersGender(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the customers age group gender.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustbyagegroup")]
        public object GetCustomersAgeGroupGender([FromBody] OAuth.Request _OAuthRequest)
        {
            OCustomersOverview.OAnalytics.Request _Request = JsonConvert.DeserializeObject<OCustomersOverview.OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomersOverview = new ManageCustomersOverview();
            OResponse _Response = _ManageCustomersOverview.GetCustomersAgeGroupGender(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the customers top spenders.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustbytopspenders")]
        public object GetCustomersTopSpenders([FromBody] OAuth.Request _OAuthRequest)
        {
            OCustomersOverview.OAnalytics.Request _Request = JsonConvert.DeserializeObject<OCustomersOverview.OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomersOverview = new ManageCustomersOverview();
            OResponse _Response = _ManageCustomersOverview.GetCustomersTopSpenders(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the customer rewards overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomersrewardsoverview")]
        public object GetCustomerRewardsOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomersOverview = new ManageCustomersOverview();
            OResponse _Response = _ManageCustomersOverview.GetCustomerRewardsOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}

