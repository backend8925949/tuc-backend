//==================================================================================
// FileName: TUCAnalyticsController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for sales analytics.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Operations.Merchant;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;


namespace HCore.Api.App.v3.Tuc.Merchant
{
    [Produces("application/json")]
    [Route("api/v3/merchant/analytics/[action]")]
    public class TUCAnalyticsController
    {
        ManageAnalytics _ManageAnalytics;
        /// <summary>
        /// Description: Gets the account summary.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountsummary")]
        public object GetAccountSummary([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            OResponse _Response = _ManageAnalytics.GetAccountSummary(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the cashiers overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcashiersoverview")]
        public object GetCashiersOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            OResponse _Response = _ManageAnalytics.GetCashiersOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the sales overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsalesoverview")]
        public object GetSalesOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            OResponse _Response = _ManageAnalytics.GetSalesOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the customers overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomersoverview")]
        public object GetCustomersOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            OResponse _Response = _ManageAnalytics.GetCustomersOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the sales history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsaleshistory")]
        public object GetSalesHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            OResponse _Response = _ManageAnalytics.GetSalesHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the loyalty overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getloyaltyoverview")]
        public object GetLoyaltyOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            OResponse _Response = _ManageAnalytics.GetLoyaltyOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the cashier position history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcashierposhistory")]
        public object GetCashierPosHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            OResponse _Response = _ManageAnalytics.GetCashierPosHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        //[HttpPost]
        //[ActionName("getloyaltyhistory")]
        //public object GetLoyaltyHistory([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageAnalytics = new ManageAnalytics();
        //    OResponse _Response = _ManageAnalytics.GetLoyaltyHistory(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}

        /// <summary>
        /// Description: Gets the loyalty visit history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getloyaltyvisithistory")]
        public object GetLoyaltyVisitHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            OResponse _Response = _ManageAnalytics.GetLoyaltyVisitHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        ManageBankConsolation _ManageBankConsolation;
        /// <summary>
        /// Description: Gets the store bank collection.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getstorebankcollection")]
        public object GetStoreBankCollection([FromBody] OAuth.Request _OAuthRequest)
        {
            OStoreBankCollection.Request _Request = JsonConvert.DeserializeObject<OStoreBankCollection.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBankConsolation = new ManageBankConsolation();
            OResponse _Response = _ManageBankConsolation.GetStoreBankCollection(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Updates the received amount.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatereceivedamount")]
        public object UpdateReceivedAmount([FromBody] OAuth.Request _OAuthRequest)
        {
            OStoreBankCollection.Request _Request = JsonConvert.DeserializeObject<OStoreBankCollection.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBankConsolation = new ManageBankConsolation();
            OResponse _Response = _ManageBankConsolation.UpdateReceivedAmount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }




        /// <summary>
        /// Description: Gets the account overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountoverview")]
        public object GetAccountOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            OResponse _Response = _ManageAnalytics.GetAccountOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the terminal activity history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getterminalactivityhistory")]
        public object GetTerminalActivityHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            OResponse _Response = _ManageAnalytics.GetTerminalActivityHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Gets the loyalty overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getgrouployaltyoverview")]
        public object GetGroupLoyaltyOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            OResponse _Response = _ManageAnalytics.GetGroupLoyaltyOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }



        /// <summary>
        /// Description: Gets the loyalty visit history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getgrouployaltyvisithistory")]
        public object GetGroupLoyaltyVisitHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            OResponse _Response = _ManageAnalytics.GetGroupLoyaltyVisitHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
