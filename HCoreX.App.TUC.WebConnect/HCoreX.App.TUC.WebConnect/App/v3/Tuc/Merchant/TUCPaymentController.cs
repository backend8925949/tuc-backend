//==================================================================================
// FileName: TUCPaymentController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for cashout functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.App.v3.Tuc.Merchant
{
    [Produces("application/json")]
    [Route("api/v3/merchant/payments/[action]")]
    public class TUCPaymentController
    {
        ManageCashout? _ManageCashout;
        ManageCashoutOperation? _ManageCashoutOperation;

        /// <summary>
        /// Description:Method to get cashout configuration
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcashoutconfiguration")]
        public async Task<object> GetTucCashOutConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OCashOut.Configuration.Request? _Request = JsonConvert.DeserializeObject<OCashOut.Configuration.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = await _ManageCashout.GetTucCashOutConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description:Method to intialize cashout.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("cashoutinitialize")]
        public async Task<object> TucCashOutInitialize([FromBody] OAuth.Request _OAuthRequest)
        {
            OCashOut.Initialize.Request? _Request = JsonConvert.DeserializeObject<OCashOut.Initialize.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = await _ManageCashout.TucCashOutInitialize(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description:Method to get list of cashouts
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcashouts")]
        public async Task<object> GetTucCashout([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = await _ManageCashout.GetTucCashout(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Author:Priya Chavadiya
        /// Description:Method to get overview of redeem wallets
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getredeemwalletoverview")]
        public object GetCashoutsOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference? _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = _ManageCashout.GetCashoutOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Author:Priya Chavadiya
        /// Description:Method to get list of reward wallets
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getrewardwallets")]
        public object GetRewardWallets([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = _ManageCashout.GetRewardWallets(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Author:Priya Chavadiya
        /// Description:Method to top up from Redeem wallet
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("redeemfromwallet")]
        public object RedeemFromWallet([FromBody] OAuth.Request _OAuthRequest)
        {
            OCashOut.Initialize.Request? _Request = JsonConvert.DeserializeObject<OCashOut.Initialize.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = _ManageCashout.RedeemFromWallet(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Author:Priya Chavadiya
        /// Description:Method to get list of all cashouts i.e BNPL,Deals,Loyalty
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getsummarizecashouts")]
        public object GetTucSummarizeCashout([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = _ManageCashout.GetTucSummarizeCashout(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Author:Priya Chavadiya
        /// Description:Method to get configuration of all cashouts i.e BNPL,Deals,Loyalty
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getsummarizecashoutconfiguration")]
        public async Task<object> GetTucSummarizeCashOutConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OCashOut.Configuration.Request? _Request = JsonConvert.DeserializeObject<OCashOut.Configuration.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = await _ManageCashout.GetTucSummarizeCashOutConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("getcashoutsoverview")]
        public async Task<object> GetCashoutOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference? _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashoutOperation = new ManageCashoutOperation();
            OResponse _Response = await _ManageCashoutOperation.GetCashoutsOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
