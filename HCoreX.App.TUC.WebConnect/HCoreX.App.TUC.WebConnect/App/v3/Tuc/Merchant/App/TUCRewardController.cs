//==================================================================================
// FileName: TUCRewardController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for reward and redeem functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Framework.Merchant.App.AWS;
//using HCore.TUC.Core.Framework.Merchant.Upload;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Object.Merchant.App;
using HCore.TUC.Core.Operations.Merchant.App;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Merchant.App
{
    [Produces("application/json")]
    [Route("api/v3/merchant/app/[action]")]
    public class TUCRewardController
    {
        ManageReward _ManageReward;
        //private readonly IBulkUploadService _uploadService;

        //public TUCRewardController(IBulkUploadService uploadService)
        //{
        //    _uploadService = uploadService;
        //}

        /// <summary>
        /// Description: Reward initialize.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("rewardinitialize")]
        public object Reward_Initialize([FromBody] OAuth.Request _OAuthRequest)
        {
            OReward.Initialize.Request _Request = JsonConvert.DeserializeObject<OReward.Initialize.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageReward = new ManageReward();
            OResponse _Response = _ManageReward.Reward_Initialize(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Reward confirm.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("rewardconfirm")]
        public object Reward_Confirm([FromBody] OAuth.Request _OAuthRequest)
        {
            OReward.Confirm.Request _Request = JsonConvert.DeserializeObject<OReward.Confirm.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageReward = new ManageReward();
            OResponse _Response = _ManageReward.Reward_Confirm(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        ManageRedeem _ManageRedeem;
        /// <summary>
        /// Description: Redeem initialize.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("redeeminitialize")]
        public object Redeem_Initialize([FromBody] OAuth.Request _OAuthRequest)
        {
            ORedeem.Initialize.Request _Request = JsonConvert.DeserializeObject<ORedeem.Initialize.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageRedeem = new ManageRedeem();
            OResponse _Response = _ManageRedeem.Redeem_Initialize(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Redeem confirm.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("redeemconfirm")]
        public object Redeem_Confirm([FromBody] OAuth.Request _OAuthRequest)
        {
            ORedeem.Confirm.Request _Request = JsonConvert.DeserializeObject<ORedeem.Confirm.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageRedeem = new ManageRedeem();
            OResponse _Response = _ManageRedeem.Redeem_Confirm(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        //[HttpPost]
        //[ActionName("uploadcustomerrewards")]
        //public async Task<object> UploadBulkCustomerRewards([FromForm] OUpload.RewardUpload.UploadRequest request)
        //{
        //    _ManageReward = new ManageReward();
        //    return await _uploadService.UploadBulkCustomerRewardsFileAsync(request);
        //}
    }
}
