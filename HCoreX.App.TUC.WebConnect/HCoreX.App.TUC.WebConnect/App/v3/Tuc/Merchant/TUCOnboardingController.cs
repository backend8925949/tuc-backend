//==================================================================================
// FileName: TUCOnboardingController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for onbaording functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Operations.Merchant;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using static HCore.TUC.Core.Object.Merchant.OOnboardingV2;

namespace HCore.Api.App.v3.Tuc.Merchant
{
    [Produces("application/json")]
    [Route("api/v3/merchant/onboarding/[action]")]
    public class TUCOnboardingController
    {
        TUC.Core.Operations.Acquirer.ManageOnboarding _ManageOnboarding;        
        TUC.Core.Operations.Merchant.ManageOnboarding _ManageMerchantOnboarding;

        /// <summary>
        /// Description: Onboard Customers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardcustomers")]
        public object OnboardCustomers([FromBody] OAuth.Request _OAuthRequest)
        {
            OOnboarding.Customer.Request _Request = JsonConvert.DeserializeObject<OOnboarding.Customer.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new TUC.Core.Operations.Merchant.ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardCustomers(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Gets the onboarded customers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getonboardedcustomers")]
        public object GetOnboardedCustomers([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboarding = new TUC.Core.Operations.Acquirer.ManageOnboarding();
            OResponse _Response = _ManageOnboarding.GetOnboardedCustomers(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Gets the onboarded customer files.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getonboardedcustomerfiles")]
        public object GetOnboardedCustomerFiles([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboarding = new TUC.Core.Operations.Acquirer.ManageOnboarding();
            OResponse _Response = _ManageOnboarding.GetOnboardedCustomerFiles(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        ManageAccounts _ManageAccounts;
        /// <summary>
        /// Description: Webpay initialize.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("webpayinitialize")]
        public object WebPay_Initialize([FromBody] OAuth.Request _OAuthRequest)
        {
            HCore.TUC.Core.Object.Merchant.OAccounts.OCoreMerchantWebPay.Initialize.Request _Request = JsonConvert.DeserializeObject<HCore.TUC.Core.Object.Merchant.OAccounts.OCoreMerchantWebPay.Initialize.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.WebPay_Initialize(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Webpay confirm.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("webpayconfirm")]
        public object WebPay_Confirm([FromBody] OAuth.Request _OAuthRequest)
        {
            HCore.TUC.Core.Object.Merchant.OAccounts.OCoreMerchantWebPay.Confirm _Request = JsonConvert.DeserializeObject<HCore.TUC.Core.Object.Merchant.OAccounts.OCoreMerchantWebPay.Confirm>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccounts = new ManageAccounts();
            OResponse _Response = _ManageAccounts.WebPay_Confirm(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Onboard merchant.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchant_st1")]
        public object OnboardMerchant_St1([FromBody] OAuth.Request _OAuthRequest)
        {
            TUC.Core.Object.Merchant.OOnboarding.St1.Request _Request = JsonConvert.DeserializeObject<TUC.Core.Object.Merchant.OOnboarding.St1.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new TUC.Core.Operations.Merchant.ManageOnboarding();
             OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St1(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Onboard merchant ST2 resend email.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchant_st2")]
        public object OnboardMerchant_St2_ResendEmail([FromBody] OAuth.Request _OAuthRequest)
        {
            TUC.Core.Object.Merchant.OOnboarding.St2.Request _Request = JsonConvert.DeserializeObject<TUC.Core.Object.Merchant.OOnboarding.St2.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new TUC.Core.Operations.Merchant.ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St2_ResendEmail(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Onboard merchant ST3 resend email.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchant_st3")]
        public object OnboardMerchant_St3_ResendEmail([FromBody] OAuth.Request _OAuthRequest)
        {
            TUC.Core.Object.Merchant.OOnboarding.St3.Request _Request = JsonConvert.DeserializeObject<TUC.Core.Object.Merchant.OOnboarding.St3.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new TUC.Core.Operations.Merchant.ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St3_VerifyEmail(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Onboard merchant ST4 request mobile verification.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchant_st4")]
        public object OnboardMerchant_St4_RequestMobileVerification([FromBody] OAuth.Request _OAuthRequest)
        {
            TUC.Core.Object.Merchant.OOnboarding.St4.Request _Request = JsonConvert.DeserializeObject<TUC.Core.Object.Merchant.OOnboarding.St4.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new TUC.Core.Operations.Merchant.ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St4_RequestMobileVerification(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Onboard merchant ST5 verify mobile number.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchant_st5")]
        public object OnboardMerchant_St5_VerifyMobileNumber([FromBody] OAuth.Request _OAuthRequest)
        {
            TUC.Core.Object.Merchant.OOnboarding.St5.Request _Request = JsonConvert.DeserializeObject<TUC.Core.Object.Merchant.OOnboarding.St5.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new TUC.Core.Operations.Merchant.ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St5_VerifyMobileNumber(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Onboard merchant ST6 complete setup.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchant_st6")]
        public object OnboardMerchant_St6_CompleteSetup([FromBody] OAuth.Request _OAuthRequest)
        {
            TUC.Core.Object.Merchant.OOnboarding.St6.Request _Request = JsonConvert.DeserializeObject<TUC.Core.Object.Merchant.OOnboarding.St6.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new TUC.Core.Operations.Merchant.ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St6_CompleteSetup(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }


    [Produces("application/json")]
    [Route("api/v3/merchant/onboardingv2/[action]")]
    public class TUCOnboardingV2Controller
    {
        ManageOnboardingV2 _ManageOnboardingV2;
      
        TUC.Core.Operations.Acquirer.ManageOnboarding _ManageOnboarding;        
        TUC.Core.Operations.Merchant.ManageOnboarding _ManageMerchantOnboarding;
        /// <summary>
        /// Description: Onboard customers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("ob_merchant_request")]
        public object OnboardCustomers([FromBody] OAuth.Request _OAuthRequest)
        {
            OOnboardingV2.Profile.Request _Request = JsonConvert.DeserializeObject<OOnboardingV2.Profile.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboardingV2 = new ManageOnboardingV2();
            OResponse _Response = _ManageOnboardingV2.OnboardMerchant_Request(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Onboard merchant email update.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("ob_merchant_emailupdate")]
        public object OnboardMerchant_EmailUpdate([FromBody] OAuth.Request _OAuthRequest)
        {
            OOnboardingV2.EmailChange.Request _Request = JsonConvert.DeserializeObject<OOnboardingV2.EmailChange.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboardingV2 = new ManageOnboardingV2();
            OResponse _Response = _ManageOnboardingV2.OnboardMerchant_EmailUpdate(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Onboard merchant resend email code.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("ob_merchant_resendemail")]
        public object OnboardMerchant_ResendEmailCode([FromBody] OAuth.Request _OAuthRequest)
        {
            OOnboardingV2.EmailChange.Request _Request = JsonConvert.DeserializeObject<OOnboardingV2.EmailChange.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboardingV2 = new ManageOnboardingV2();
            OResponse _Response = _ManageOnboardingV2.OnboardMerchant_ResendEmailCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Onboard merchant verify email.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("ob_merchant_emailverify")]
        public object OnboardMerchant_VerifyEmail([FromBody] OAuth.Request _OAuthRequest)
        {
            OOnboardingV2.EmailVerify.Request _Request = JsonConvert.DeserializeObject<OOnboardingV2.EmailVerify.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboardingV2 = new ManageOnboardingV2();
            OResponse _Response = _ManageOnboardingV2.OnboardMerchant_VerifyEmail(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Onboard merchant mobile update.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("ob_merchant_updatemobile")]
        public object OnboardMerchant_MobileUpdate([FromBody] OAuth.Request _OAuthRequest)
        {
            OOnboardingV2.MobileChange.Request _Request = JsonConvert.DeserializeObject<OOnboardingV2.MobileChange.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboardingV2 = new ManageOnboardingV2();
            OResponse _Response = _ManageOnboardingV2.OnboardMerchant_MobileUpdate(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Onboard merchant request mobile verification.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("ob_merchant_requestmverfication")]
        public object OnboardMerchant_Request_MobileVerification([FromBody] OAuth.Request _OAuthRequest)
        {
            OOnboardingV2.MobileVerify.Request _Request = JsonConvert.DeserializeObject<OOnboardingV2.MobileVerify.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboardingV2 = new ManageOnboardingV2();
            OResponse _Response = _ManageOnboardingV2.OnboardMerchant_Request_MobileVerification(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Onboard merchant request mobile verification confirm.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("ob_merchant_requestmverficationconfirm")]
        public object OnboardMerchant_Request_MobileVerificationConfirm([FromBody] OAuth.Request _OAuthRequest)
        {
            OOnboardingV2.MobileVerifyConfirm.Request _Request = JsonConvert.DeserializeObject<OOnboardingV2.MobileVerifyConfirm.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboardingV2 = new ManageOnboardingV2();
            OResponse _Response = _ManageOnboardingV2.OnboardMerchant_Request_MobileVerificationConfirm(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Onboard merchant request update business details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("ob_merchant_requestupdatebusinessDetails")]
        public object OnboardMerchant_Request_UpdateBusinessDetails([FromBody] OAuth.Request _OAuthRequest)
        {
            OOnboardingV2.BusinessInformation _Request = JsonConvert.DeserializeObject<OOnboardingV2.BusinessInformation>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboardingV2 = new ManageOnboardingV2();
            OResponse _Response = _ManageOnboardingV2.OnboardMerchant_Request_UpdateBusinessDetails(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Onboard merchant request account configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("ob_merchant_requestaccountconfiguration")]
        public object OnboardMerchant_Request_AccountConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OOnboardingV2.AccountType _Request = JsonConvert.DeserializeObject<OOnboardingV2.AccountType>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboardingV2 = new ManageOnboardingV2();
            OResponse _Response = _ManageOnboardingV2.OnboardMerchant_Request_AccountConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the merchant details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchantdetails")]
        public object GetMerchantDetails([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboardingV2 = new ManageOnboardingV2();
            OResponse _Response = _ManageOnboardingV2.GetMerchantDetails(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }



        //[HttpPost]
        //[ActionName("getonboardedcustomers")]
        //public object GetOnboardedCustomers([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageOnboarding = new TUC.Core.Operations.Acquirer.ManageOnboarding();
        //    OResponse _Response = _ManageOnboarding.GetOnboardedCustomers(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}


        //[HttpPost]
        //[ActionName("getonboardedcustomerfiles")]
        //public object GetOnboardedCustomerFiles([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageOnboarding = new TUC.Core.Operations.Acquirer.ManageOnboarding();
        //    OResponse _Response = _ManageOnboarding.GetOnboardedCustomerFiles(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}


        //ManageAccounts _ManageAccounts;
        //[HttpPost]
        //[ActionName("webpayinitialize")]
        //public object WebPay_Initialize([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    HCore.TUC.Core.Object.Merchant.OAccounts.OCoreMerchantWebPay.Initialize.Request _Request = JsonConvert.DeserializeObject<HCore.TUC.Core.Object.Merchant.OAccounts.OCoreMerchantWebPay.Initialize.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageAccounts = new ManageAccounts();
        //    OResponse _Response = _ManageAccounts.WebPay_Initialize(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}
        //[HttpPost]
        //[ActionName("webpayconfirm")]
        //public object WebPay_Confirm([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    HCore.TUC.Core.Object.Merchant.OAccounts.OCoreMerchantWebPay.Confirm _Request = JsonConvert.DeserializeObject<HCore.TUC.Core.Object.Merchant.OAccounts.OCoreMerchantWebPay.Confirm>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageAccounts = new ManageAccounts();
        //    OResponse _Response = _ManageAccounts.WebPay_Confirm(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}


        /// <summary>
        /// Description: Onboard merchant ST1.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchant_st1")]
        public object OnboardMerchant_St1([FromBody] OAuth.Request _OAuthRequest)
        {
            TUC.Core.Object.Merchant.OOnboarding.St1.Request _Request = JsonConvert.DeserializeObject<TUC.Core.Object.Merchant.OOnboarding.St1.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new TUC.Core.Operations.Merchant.ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St1(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Onboard merchant ST2 resend email.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchant_st2")]
        public object OnboardMerchant_St2_ResendEmail([FromBody] OAuth.Request _OAuthRequest)
        {
            TUC.Core.Object.Merchant.OOnboarding.St2.Request _Request = JsonConvert.DeserializeObject<TUC.Core.Object.Merchant.OOnboarding.St2.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new TUC.Core.Operations.Merchant.ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St2_ResendEmail(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Onboard merchant ST3 resend email.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchant_st3")]
        public object OnboardMerchant_St3_ResendEmail([FromBody] OAuth.Request _OAuthRequest)
        {
            TUC.Core.Object.Merchant.OOnboarding.St3.Request _Request = JsonConvert.DeserializeObject<TUC.Core.Object.Merchant.OOnboarding.St3.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new TUC.Core.Operations.Merchant.ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St3_VerifyEmail(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Onboard merchant ST4 request mobile verification.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchant_st4")]
        public object OnboardMerchant_St4_RequestMobileVerification([FromBody] OAuth.Request _OAuthRequest)
        {
            TUC.Core.Object.Merchant.OOnboarding.St4.Request _Request = JsonConvert.DeserializeObject<TUC.Core.Object.Merchant.OOnboarding.St4.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new TUC.Core.Operations.Merchant.ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St4_RequestMobileVerification(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Onboard merchant ST5 verify mobile number.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchant_st5")]
        public object OnboardMerchant_St5_VerifyMobileNumber([FromBody] OAuth.Request _OAuthRequest)
        {
            TUC.Core.Object.Merchant.OOnboarding.St5.Request _Request = JsonConvert.DeserializeObject<TUC.Core.Object.Merchant.OOnboarding.St5.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new TUC.Core.Operations.Merchant.ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St5_VerifyMobileNumber(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Onboard merchant ST6 complete setup.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("onboardmerchant_st6")]
        public object OnboardMerchant_St6_CompleteSetup([FromBody] OAuth.Request _OAuthRequest)
        {
            TUC.Core.Object.Merchant.OOnboarding.St6.Request _Request = JsonConvert.DeserializeObject<TUC.Core.Object.Merchant.OOnboarding.St6.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new TUC.Core.Operations.Merchant.ManageOnboarding();
            OResponse _Response = _ManageMerchantOnboarding.OnboardMerchant_St6_CompleteSetup(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Saves the merchant.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savemerchant")]
        public object SaveMerchant([FromBody] OAuth.Request _OAuthRequest)
        {
            OOnboardingV2.BusinessInformation _Request = JsonConvert.DeserializeObject<OOnboardingV2.BusinessInformation>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboardingV2 = new ManageOnboardingV2();
            OResponse _Response = _ManageOnboardingV2.SaveMerchant(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        //[HttpPost]
        //[ActionName("getonboardmerchants")]
        //public object GetOnboardMerchants([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageOnboardingV2 = new ManageOnboardingV2();
        //    OResponse _Response = _ManageOnboardingV2.GetOnboardMerchants(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}

        //[HttpPost]
        //[ActionName("verifydocuments")]
        //public object Merchant_Onboarding_VerifyDocument([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    VerifyDocument _Request = JsonConvert.DeserializeObject<VerifyDocument>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageOnboardingV2 = new ManageOnboardingV2();
        //    OResponse _Response = _ManageOnboardingV2.Merchant_Onboarding_VerifyDocument(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}
    }





}
