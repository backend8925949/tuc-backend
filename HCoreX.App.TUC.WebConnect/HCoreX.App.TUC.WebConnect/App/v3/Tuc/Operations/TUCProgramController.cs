﻿//==================================================================================
// FileName: TUCProgramController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for Program Functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-10-2022      | Priya Chavadiya   : Developed API's
//
//==================================================================================
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Object.Merchant;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCoreX.App.TUC.WebConnect.App.v3.Tuc.Operations
{
    [Produces("application/json")]
    [Route("api/v3/tuc/program/[action]")]    
    public class TUCProgramController : Controller
    {
        ManageProgram _ManageProgram;
        /// <summary>
        /// Description: Method defined to save program
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("saveprogram")]
        public object SaveProgram([FromBody] OAuth.Request _OAuthRequest)
        {
            OProgram.Request _Request = JsonConvert.DeserializeObject<OProgram.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProgram = new ManageProgram();
            OResponse _Response = _ManageProgram.SaveProgram(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to update program
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updateprogram")]
        public object UpdateProgram([FromBody] OAuth.Request _OAuthRequest)
        {
            OProgram.Request _Request = JsonConvert.DeserializeObject<OProgram.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProgram = new ManageProgram();
            OResponse _Response = _ManageProgram.UpdateProgram(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to delete program
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("deleteprogram")]
        public object DeleteProgram([FromBody] OAuth.Request _OAuthRequest)
        {
            OProgram.Request _Request = JsonConvert.DeserializeObject<OProgram.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProgram = new ManageProgram();
            OResponse _Response = _ManageProgram.DeleteProgram(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get program details
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getprogram")]
        public object GetProgram([FromBody] OAuth.Request _OAuthRequest)
        {
            OProgram.Request _Request = JsonConvert.DeserializeObject<OProgram.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProgram = new ManageProgram();
            OResponse _Response = _ManageProgram.GetProgram(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get list of program
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getprograms")]
        public object GetPrograms([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProgram = new ManageProgram();
            OResponse _Response = _ManageProgram.GetAllProgram(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to save program group
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("saveprogramgroup")]
        public object SaveProgramGroup([FromBody] OAuth.Request _OAuthRequest)
        {
            OProgram.Request _Request = JsonConvert.DeserializeObject<OProgram.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProgram = new ManageProgram();
            OResponse _Response = _ManageProgram.SaveProgramGroup(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to update program group
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updateprogramgroup")]
        public object UpdateProgramGroup([FromBody] OAuth.Request _OAuthRequest)
        {
            OProgram.Request _Request = JsonConvert.DeserializeObject<OProgram.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProgram = new ManageProgram();
            OResponse _Response = _ManageProgram.UpdateProgramGroup(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        
        /// <summary>
        /// Description: Method defined to get program group details
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getprogramgroup")]
        public object GetProgramGroup([FromBody] OAuth.Request _OAuthRequest)
        {
            OProgram.Request _Request = JsonConvert.DeserializeObject<OProgram.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProgram = new ManageProgram();
            OResponse _Response = _ManageProgram.GetProgramGroup(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get list of program
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getprogramgroups")]
        public object GetProgramGroups([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProgram = new ManageProgram();
            OResponse _Response = _ManageProgram.GetProgramGroups(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to delete program group
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("deleteprogramgroup")]
        public object DeleteProgramGroup([FromBody] OAuth.Request _OAuthRequest)
        {
            OProgram.Request _Request = JsonConvert.DeserializeObject<OProgram.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProgram = new ManageProgram();
            OResponse _Response = _ManageProgram.DeleteProgramGroup(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Topups the account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("creditwallet")]
        public object TopupAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            HCore.TUC.Core.Object.Merchant.OSubscription.Topup.Request _Request = JsonConvert.DeserializeObject<HCore.TUC.Core.Object.Merchant.OSubscription.Topup.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProgram = new ManageProgram();
            OResponse _Response = _ManageProgram.TopupAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the account balance.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountbalance")]
        public object GetAccountBalance([FromBody] OAuth.Request _OAuthRequest)
        {
            HCore.TUC.Core.Object.Merchant.OSubscription.Balance.Request _Request = JsonConvert.DeserializeObject<HCore.TUC.Core.Object.Merchant.OSubscription.Balance.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageProgram = new ManageProgram();
            OResponse _Response = _ManageProgram.GetAccountBalance(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
