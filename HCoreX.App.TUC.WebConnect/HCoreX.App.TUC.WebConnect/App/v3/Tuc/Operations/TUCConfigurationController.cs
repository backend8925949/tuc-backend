//==================================================================================
// FileName: TUCConfigurationController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for account configuration functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Operation.Object;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Operations
{
    [Produces("application/json")]
    [Route("api/v3/tuc/config/[action]")]
    public class TUCConfigurationController : Controller
    {
        TUC.Core.Operations.Operations.ManageConfiguration _ManageConfiguration;
        /// <summary>
        /// Description: Method defined to save account configuration
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("saveaccountconfiguration")]
        public object SaveAccountConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountConfiguration.Save.Request _Request = JsonConvert.DeserializeObject<OAccountConfiguration.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new TUC.Core.Operations.Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.SaveAccountConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Decription: Method defined to update account configuration
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updateaccountconfiguration")]
        public object UpdateAccountConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccountConfiguration.Update _Request = JsonConvert.DeserializeObject<OAccountConfiguration.Update>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new TUC.Core.Operations.Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.UpdateAccountConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to delete account configuration
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("deleteacoountconfiguration")]
        public object DeleteAccountConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new TUC.Core.Operations.Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.DeleteAccountConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get details of account configuration
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getaccountconfiguration")]
        public object GetAccountConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new TUC.Core.Operations.Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.GetAccountConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get list of account configurations
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getaccountconfigurations")]
        public object GetAccountConfigurations([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new TUC.Core.Operations.Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.GetAccountConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get list of history account configuration
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getaccountconfigurationhistory")]
        public object GetAccountConfigurationHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new TUC.Core.Operations.Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.GetAccountConfigurationHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }





        /// <summary>
        /// Description: Method defined to save configuration
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("saveconfiguration")]
        public object SaveConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OCoreConfiguration.Save.Request _Request = JsonConvert.DeserializeObject<OCoreConfiguration.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new TUC.Core.Operations.Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.SaveConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to update configuration
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updateconfiguration")]
        public object UpdateConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OCoreConfiguration.Update _Request = JsonConvert.DeserializeObject<OCoreConfiguration.Update>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new TUC.Core.Operations.Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.UpdateConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to delete configuration
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("deleteconfiguration")]
        public object DeleteConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new TUC.Core.Operations.Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.DeleteConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get details of configuration
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getconfiguration")]
        public object GetConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new TUC.Core.Operations.Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.GetConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get list of configurations
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getconfigurations")]
        public object GetConfigurations([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new TUC.Core.Operations.Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.GetConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Method defined to get list of history configuration
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getconfigurationhistory")]
        public object GetConfigurationHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new TUC.Core.Operations.Operations.ManageConfiguration();
            OResponse _Response = _ManageConfiguration.GetConfigurationHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
