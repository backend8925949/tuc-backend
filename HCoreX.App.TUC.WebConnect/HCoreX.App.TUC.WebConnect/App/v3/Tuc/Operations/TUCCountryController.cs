//==================================================================================
// FileName: TUCCountryController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for Country Functionality
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Operation.Object;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Operations
{
    [Produces("application/json")]
    [Route("api/v3/tuc/country/[action]")]
    public class TUCCountryController : Controller
    {
        ManageCountryManager _ManageCountryManager;

        /// <summary>
        /// Description:Method to get list of countries
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcountries")]
        public object GetCountries([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCountryManager = new ManageCountryManager();
            OResponse _Response = _ManageCountryManager.GetCountry(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}