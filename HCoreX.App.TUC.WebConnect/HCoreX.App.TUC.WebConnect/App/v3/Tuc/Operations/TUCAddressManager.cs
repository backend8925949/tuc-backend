//==================================================================================
// FileName: TUCAddressManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for Address Functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Operation.Object;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Operations
{
    [Produces("application/json")]
    [Route("api/v3/tuc/address/[action]")]
    public class TUCAddressManager : Controller
    {
        ManageCountryManager _ManageCountryManager;
        ManageStateManager _ManageStateManager;
        ManageCityManager _ManageCityManager;


        /// <summary>
        /// Description:Method to get list of countries
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcountries")]
        public object GetCountries([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCountryManager = new ManageCountryManager();
            OResponse _Response = _ManageCountryManager.GetCountries(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method to get list of states
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getstates")]
        public object GetStates([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStateManager = new ManageStateManager();
            OResponse _Response = _ManageStateManager.GetStates(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method to get list of cities
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcities")]
        public object GetCities([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCityManager = new ManageCityManager();
            OResponse _Response = _ManageCityManager.GetCities(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method to get list of city areas
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcityareas")]
        public object GetCityAreas([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCityManager = new ManageCityManager();
            OResponse _Response = _ManageCityManager.GetCityAreas(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        ManageAddress _ManageAddress;

        /// <summary>
        /// Description: Method to save address
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("saveaddress")]
        public object SaveAddress([FromBody] OAuth.Request _OAuthRequest)
        {
            OAddressManager.Save.Request _Request = JsonConvert.DeserializeObject<OAddressManager.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAddress = new ManageAddress();
            OResponse _Response = _ManageAddress.SaveAddress(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method to update address
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updateaddress")]
        public object UpdateAddress([FromBody] OAuth.Request _OAuthRequest)
        {
            OAddressManager.Save.Request _Request = JsonConvert.DeserializeObject<OAddressManager.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAddress = new ManageAddress();
            OResponse _Response = _ManageAddress.UpdateAddress(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method to delete address
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("deleteaddress")]
        public object DeleteAddress([FromBody] OAuth.Request _OAuthRequest)
        {
            OAddressManager.Save.Request _Request = JsonConvert.DeserializeObject<OAddressManager.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAddress = new ManageAddress();
            OResponse _Response = _ManageAddress.DeleteAddress(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method to get address details
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getaddress")]
        public object GetAddress([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAddress = new ManageAddress();
            OResponse _Response = _ManageAddress.GetAddress(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method to get list of address
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getaddresslist")]
        public object GetAddressList([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAddress = new ManageAddress();
            OResponse _Response = _ManageAddress.GetAddress(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

    }
}
