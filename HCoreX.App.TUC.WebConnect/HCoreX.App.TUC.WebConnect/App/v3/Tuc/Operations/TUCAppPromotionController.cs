//==================================================================================
// FileName: TUCAppPromotionController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for App Promotion Functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Operation.Object;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Operations
{
    [Produces("application/json")]
    [Route("api/v3/tuc/apppromotion/[action]")]
    public class TUCAPPPromotionController : Controller
    {
        ManageAppPromotion _ManageAppPromotion;


        /// <summary>
        /// Description: Method defined to save app promotion
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("saveapppromotion")]
        public object SaveAppPromotion([FromBody] OAuth.Request _OAuthRequest)
        {
            OAppPromotion.Save.Request _Request = JsonConvert.DeserializeObject<OAppPromotion.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAppPromotion = new ManageAppPromotion();
            OResponse _Response = _ManageAppPromotion.SaveAppPromotion(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to update app promotion
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updateapppromotion")]
        public object UpdateAppPromotion([FromBody] OAuth.Request _OAuthRequest)
        {
            OAppPromotion.Update _Request = JsonConvert.DeserializeObject<OAppPromotion.Update>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAppPromotion = new ManageAppPromotion();
            OResponse _Response = _ManageAppPromotion.UpdateAppPromotion(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to delete app promotion
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("deleteapppromotion")]
        public object DeleteAppPromotion([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAppPromotion = new ManageAppPromotion();
            OResponse _Response = _ManageAppPromotion.DeleteAppPromotion(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get details of app promotion
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getapppromotion")]
        public object GetAppPromotion([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAppPromotion = new ManageAppPromotion();
            OResponse _Response = _ManageAppPromotion.GetAppPromotion(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get list of app promotion
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getapppromotions")]
        public object GetAppPromotions([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAppPromotion = new ManageAppPromotion();
            OResponse _Response = _ManageAppPromotion.GetAppPromotions(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get list of app sliders
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getappsliders")]
        public object GetAppSlider([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAppPromotion = new ManageAppPromotion();
            OResponse _Response = _ManageAppPromotion.GetAppSlider(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to remove app promotion image
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("removeapppromtoionimage")]
        public object RemoveAppPromotionImage([FromBody] OAuth.Request _OAuthRequest)
        {
            OAppPromotion.Image _Request = JsonConvert.DeserializeObject<OAppPromotion.Image>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAppPromotion = new ManageAppPromotion();
            OResponse _Response = _ManageAppPromotion.RemoveAppPromotionImage(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
