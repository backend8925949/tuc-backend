//==================================================================================
// FileName: TUCStateController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for State Functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Operation.Object;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Operations
{
    [Produces("application/json")]
    [Route("api/v3/tuc/state/[action]")]
    public class TUCStateController : Controller
    {
        ManageStateManager _ManageStateManager;
        /// <summary>
        /// Description: Method defined to save state
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("savestate")]
        public object SaveState([FromBody] OAuth.Request _OAuthRequest)
        {
            OStateManager.Save.Request _Request = JsonConvert.DeserializeObject<OStateManager.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStateManager = new ManageStateManager();
            OResponse _Response = _ManageStateManager.SaveState(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to update state
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updatestate")]
        public object UpdateState([FromBody] OAuth.Request _OAuthRequest)
        {
            OStateManager.Update _Request = JsonConvert.DeserializeObject<OStateManager.Update>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStateManager = new ManageStateManager();
            OResponse _Response = _ManageStateManager.UpdateState(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to delete state
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("deletestate")]
        public object DeleteState([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStateManager = new ManageStateManager();
            OResponse _Response = _ManageStateManager.DeleteState(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get state details
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getstate")]
        public object GetState([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStateManager = new ManageStateManager();
            OResponse _Response = _ManageStateManager.GetState(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Method defined to get list of states
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getstates")]
        public object GetStates([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStateManager = new ManageStateManager();
            OResponse _Response = _ManageStateManager.GetState(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}