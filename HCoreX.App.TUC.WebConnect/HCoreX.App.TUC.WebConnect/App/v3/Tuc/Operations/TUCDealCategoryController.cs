//==================================================================================
// FileName: TUCDealCategoryController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for deals category functionality
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Operations
{
    [Produces("application/json")]
    [Route("api/v3/tuc/dealcategories/[action]")]
    //[ApiController]
    public class TUCDealCategoryController
    {
        ManageDealCategory _ManageDealCategory;
        /// <summary>
        /// Description: Method defined to save deal category
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("saveDealcategory")]
        public object SaveDealCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            ODealCategory.Request _Request = JsonConvert.DeserializeObject<ODealCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealCategory = new ManageDealCategory();
            OResponse _Response = _ManageDealCategory.SaveDealCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to update deal category
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updateDealcategory")]
        public object UpdateDealCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            ODealCategory.Request _Request = JsonConvert.DeserializeObject<ODealCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealCategory = new ManageDealCategory();
            OResponse _Response = _ManageDealCategory.UpdateDealCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to delete deal category
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("deleteDealcategory")]
        public object DeleteDealCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            ODealCategory.Request _Request = JsonConvert.DeserializeObject<ODealCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealCategory = new ManageDealCategory();
            OResponse _Response = _ManageDealCategory.DeleteDealCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get details of deal category
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getDealcategory")]
        public object GetCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            ODealCategory.Request _Request = JsonConvert.DeserializeObject<ODealCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealCategory = new ManageDealCategory();
            OResponse _Response = _ManageDealCategory.GetDealCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method to get list of deal categories
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getDealcategories")]
        public object GetDealCategoryList([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealCategory = new ManageDealCategory();
            OResponse _Response = _ManageDealCategory.GetDealCategoryList(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get list of root categories
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getrootcategories")]
        public object GetRootCategories([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealCategory = new ManageDealCategory();
            OResponse _Response = _ManageDealCategory.GetRootCategories(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
