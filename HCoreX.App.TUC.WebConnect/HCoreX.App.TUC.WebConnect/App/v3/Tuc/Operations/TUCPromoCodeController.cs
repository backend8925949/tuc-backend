//==================================================================================
// FileName: TUCPromoCodeController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for promocode functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Operations
{
    [Produces("application/json")]
    [Route("api/v3/tuc/promocode/[action]")]
    public class TUCPromoCodeController : Controller
    {
        ManagePromoCode _ManagePromoCode;

        /// <summary>
        /// Description: Method defined to get promocode conditions
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getpromocodeconditions")]
        public object GetPromoCodeConditions([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePromoCode = new ManagePromoCode();
            OResponse _Response = _ManagePromoCode.GetPromoCodeConditions(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method to get list of promocode accounts
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getpromocodeaccounts")]
        public object GetPromoCodeAccounts([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePromoCode = new ManagePromoCode();
            OResponse _Response = _ManagePromoCode.GetPromoCodeAccounts(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method to get details of promocode account
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getpromocodeaccount")]
        public object GetPromoCodeAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePromoCode = new ManagePromoCode();
            OResponse _Response = _ManagePromoCode.GetPromoCodeAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to save promocode
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("savepromocode")]
        public object SavePromoCode([FromBody] OAuth.Request _OAuthRequest)
        {
            OPromoCode.PromoCodeOperations.Save _Request = JsonConvert.DeserializeObject<OPromoCode.PromoCodeOperations.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePromoCode = new ManagePromoCode();
            OResponse _Response = _ManagePromoCode.SavePromoCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to update promocode
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updatepromocode")]
        public object UpdatePromoCode([FromBody] OAuth.Request _OAuthRequest)
        {
            OPromoCode.PromoCodeOperations.Update _Request = JsonConvert.DeserializeObject<OPromoCode.PromoCodeOperations.Update>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePromoCode = new ManagePromoCode();
            OResponse _Response = _ManagePromoCode.UpdatePromoCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to delete promocode
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("deletepromocode")]
        public object DeletePromoCode([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePromoCode = new ManagePromoCode();
            OResponse _Response = _ManagePromoCode.DeletePromoCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get promocode details
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getpromocode")]
        public object GetPromoCode([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePromoCode = new ManagePromoCode();
            OResponse _Response = _ManagePromoCode.GetPromoCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get list of promocodes
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getpromocodes")]
        public object GetPromoCodes([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePromoCode = new ManagePromoCode();
            OResponse _Response = _ManagePromoCode.GetPromoCodes(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to update promocode limit
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updatepromocodelimit")]
        public object UpdatePromoCodeLimit([FromBody] OAuth.Request _OAuthRequest)
        {
            OPromoCode.PromoCodeOperations.Update _Request = JsonConvert.DeserializeObject<OPromoCode.PromoCodeOperations.Update>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePromoCode = new ManagePromoCode();
            OResponse _Response = _ManagePromoCode.UpdatePromoCodeLimit(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to update promocode schedule
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updatepromocodeshedule")]
        public object UpdatePromoCodeShedule([FromBody] OAuth.Request _OAuthRequest)
        {
            OPromoCode.PromoCodeOperations.Update _Request = JsonConvert.DeserializeObject<OPromoCode.PromoCodeOperations.Update>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePromoCode = new ManagePromoCode();
            OResponse _Response = _ManagePromoCode.UpdatePromoCodeShedule(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get  promocode overview
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getpromocodesoverview")]
        public object GetPromocodeOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePromoCode = new ManagePromoCode();
            OResponse _Response = _ManagePromoCode.GetPromocodeOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get promocode usage overview
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getpromocodeusageoverview")]
        public object GetPromocodeUsageOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePromoCode = new ManagePromoCode();
            OResponse _Response = _ManagePromoCode.GetPromocodeUsageOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get list of  customers by referral
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcustomersbyreferrals")]
        public object GetCustomersByReferrals([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePromoCode = new ManagePromoCode();
            OResponse _Response = _ManagePromoCode.GetCustomersByReferrals(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
