//==================================================================================
// FileName: TUCSubscriptionController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for getting details about subscription
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
// 10-11-2022      | Priya Chavadiya   : Added Subscription API's according to new design
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.App.v3.Tuc.Operations
{
    [Produces("application/json")]
    [Route("api/v3/tuc/subscriptions/[action]")]
    public class TUCSubscriptionController
    {
        ManageSubscription _ManageSubscription;
        /// <summary>
        /// Description: Method defined to update subscription
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updatesubscription")]
        public object UpdateSubscription([FromBody] OAuth.Request _OAuthRequest)
        {
            OSubscription.SubscriptionUpdate.Request _Request = JsonConvert.DeserializeObject<OSubscription.SubscriptionUpdate.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.UpdateSubscription(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get list of subscriptions
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getsubscriptions")]
        public object GetSubscription([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.GetSubscription(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get list of account subscriptions
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getaccountsubscriptions")]
        public object GetAccountSubscription([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.GetAccountSubscription(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get account subscription overview
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getaccountsubscriptionsoverview")]
        public object GetAccountSubscriptionOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.GetAccountSubscriptionOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to update monthly subscription
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updatemonthlysubscription")]
        public object UpdateMonthlySubscription([FromBody] OAuth.Request _OAuthRequest)
        {
            OSubscription.SubscriptionUpdate.MonthlySubscriptionRequest _Request = JsonConvert.DeserializeObject<OSubscription.SubscriptionUpdate.MonthlySubscriptionRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.UpdateMonthlySubscription(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get list of  monthly account subscription store wise.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getaccountsubscriptionstorewise")]
        public object GetAccountSubscriptionStorewise([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.GetAccountSubscriptionStorewise(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method to get amount paid to paystack for subscription monthly.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getamountmonthly")]
        public object GetAmountMonthly([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.GetAmountMonthly(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to update subscription store
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updatesubscriptionstorewise")]
        public object UpdateSubscriptionStoreWise([FromBody] OAuth.Request _OAuthRequest)
        {
            OSubscription.SubscriptionUpdate.MonthlySubscriptionRequest _Request = JsonConvert.DeserializeObject<OSubscription.SubscriptionUpdate.MonthlySubscriptionRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.UpdateSubscriptionStoreWise(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get list of  monthly account subscription store wise.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getstorenotsubscribed")]
        public object GetStoreNotSubscribed([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubscription = new ManageSubscription();
            OResponse _Response = _ManageSubscription.GetStoreNotSubscribed(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
