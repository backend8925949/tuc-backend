//==================================================================================
// FileName: TUCMerchantCategoryController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for merchant category functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Operations
{
    [Produces("application/json")]
    [Route("api/v3/tuc/merchantcategories/[action]")]
    //[ApiController]
    public class TUCMerchantCategoryController
    {
        ManageMerchantCategory _ManageMerchantCategory;
        /// <summary>
        /// Description: Method defined to save merchant category
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("savemerchantcategory")]
        public object SaveMerchantCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OMerchantCategory.Request _Request = JsonConvert.DeserializeObject<OMerchantCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantCategory = new ManageMerchantCategory();
            OResponse _Response = _ManageMerchantCategory.SaveMerchantCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to update merchant category
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updatemerchantcategory")]
        public object UpdateMerchantCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OMerchantCategory.Request _Request = JsonConvert.DeserializeObject<OMerchantCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantCategory = new ManageMerchantCategory();
            OResponse _Response = _ManageMerchantCategory.UpdateMerchantCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to delete merchant category
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("deletemerchantcategory")]
        public object DeleteMerchantCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OMerchantCategory.Request _Request = JsonConvert.DeserializeObject<OMerchantCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantCategory = new ManageMerchantCategory();
            OResponse _Response = _ManageMerchantCategory.DeleteMerchantCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get details of merchant category
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getmerchantcategory")]
        public object GetCategory([FromBody] OAuth.Request _OAuthRequest)
        {
            OMerchantCategory.Request _Request = JsonConvert.DeserializeObject<OMerchantCategory.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantCategory = new ManageMerchantCategory();
            OResponse _Response = _ManageMerchantCategory.GetMerchantCategory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get list of merchant categories
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getmerchantcategories")]
        public object GetMerchantCategoryList([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantCategory = new ManageMerchantCategory();
            OResponse _Response = _ManageMerchantCategory.GetMerchantCategoryList(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method to get list of root categories
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getrootcategories")]
        public object GetRootCategories([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantCategory = new ManageMerchantCategory();
            OResponse _Response = _ManageMerchantCategory.GetRootCategories(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
