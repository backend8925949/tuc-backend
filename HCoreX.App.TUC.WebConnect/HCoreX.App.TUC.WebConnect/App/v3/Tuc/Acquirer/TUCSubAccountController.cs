//==================================================================================
// FileName: TUCSubAccountController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for subaccount functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Acquirer;
using HCore.TUC.Core.Operations.Acquirer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.App.v3.Tuc.Acquirer
{
    [Produces("application/json")]
    [Route("api/v3/acquirer/subaccounts/[action]")]
    public class TUCSubAccountController : Controller
    {
        ManageSubAccount _ManageSubAccount;
        /// <summary>
        /// Description: Method defined to save subaccount
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("savesubaccount")]
        public object SaveSubAccount([FromBody]OAuth.Request _OAuthRequest)
        {
            OSubAccount.Request _Request = JsonConvert.DeserializeObject<OSubAccount.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubAccount = new ManageSubAccount();
            OResponse _Response = _ManageSubAccount.SaveSubAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to update subaccount
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updatesubaccount")]
        public object UpdateSubAccount([FromBody]OAuth.Request _OAuthRequest)
        {
            OSubAccount.Request _Request = JsonConvert.DeserializeObject<OSubAccount.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubAccount = new ManageSubAccount();
            OResponse _Response = _ManageSubAccount.UpdateSubAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get details of subaccount
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getsubaccount")]
        public object GetSubAccount([FromBody]OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubAccount = new ManageSubAccount();
            OResponse _Response = _ManageSubAccount.GetSubAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get list of subaccounts
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getsubaccounts")]
        public object GetSubAccounts([FromBody]OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSubAccount = new ManageSubAccount();
            OResponse _Response = _ManageSubAccount.GetSubAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
