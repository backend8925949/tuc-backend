//==================================================================================
// FileName: TUCBranchController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for branch,mananger and reporting manager Functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Acquirer;
using HCore.TUC.Core.Operations.Acquirer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.App.v3.Tuc.Acquirer
{
    [Produces("application/json")]
    [Route("api/v3/acquirer/branch/[action]")]
    public class TUCBranchController : Controller
    {
        ManageBranch _ManageBranch;
        /// <summary>
        /// Description: Method defined to save branch
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("savebranch")]
        public object SaveBranch([FromBody]OAuth.Request _OAuthRequest)
        {
            OBranch.Request _Request = JsonConvert.DeserializeObject<OBranch.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBranch = new ManageBranch();
            OResponse _Response = _ManageBranch.SaveBranch(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to update branch
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updatebranch")]
        public object UpdateBranch([FromBody]OAuth.Request _OAuthRequest)
        {
            OBranch.Request _Request = JsonConvert.DeserializeObject<OBranch.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBranch = new ManageBranch();
            OResponse _Response = _ManageBranch.UpdateBranch(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get branch details
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getbranch")]
        public object GetBranch([FromBody]OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBranch = new ManageBranch();
            OResponse _Response = _ManageBranch.GetBranch(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get list of branchs
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getbranches")]
        public object GetBranchs([FromBody]OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBranch = new ManageBranch();
            OResponse _Response = _ManageBranch.GetBranch(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to save manager
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("savemanager")]
        public object SaveManager([FromBody]OAuth.Request _OAuthRequest)
        {
            OManager.Request _Request = JsonConvert.DeserializeObject<OManager.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBranch = new ManageBranch();
            OResponse _Response = _ManageBranch.SaveManager(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to update manager
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updatemanager")]
        public object UpdateManager([FromBody]OAuth.Request _OAuthRequest)
        {
            OManager.Request _Request = JsonConvert.DeserializeObject<OManager.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBranch = new ManageBranch();
            OResponse _Response = _ManageBranch.UpdateManager(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Method defined to get manager details
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getmanager")]
        public object GetBGetManagerranch([FromBody]OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBranch = new ManageBranch();
            OResponse _Response = _ManageBranch.GetManager(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get list of managers
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getmanagers")]
        public object GetManagers([FromBody]OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBranch = new ManageBranch();
            OResponse _Response = _ManageBranch.GetManager(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get list of branch managers
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getbranchmanagers")]
        public object GetBranchManagers([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBranch = new ManageBranch();
            OResponse _Response = _ManageBranch.GetBranchManager(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        ManageRm _ManageRm;
        /// <summary>
        /// Description: Method defined to save reporting manager target
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("savermtarget")]
        public object SaveRmTarget([FromBody] OAuth.Request _OAuthRequest)
        {
            ORmManager.BulkTarget.Request _Request = JsonConvert.DeserializeObject<ORmManager.BulkTarget.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageRm = new ManageRm();
            OResponse _Response = _ManageRm.SaveRmTarget(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to update reporting manager target
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updatermtarget")]
        public object UpdateRmTarget([FromBody] OAuth.Request _OAuthRequest)
        {
            ORmManager.BulkTarget.UpdateRequest _Request = JsonConvert.DeserializeObject<ORmManager.BulkTarget.UpdateRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageRm = new ManageRm();
            OResponse _Response = _ManageRm.EditRmTarget(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get list of reporting manager target
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getrmtargets")]
        public object GetRmTarget([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageRm = new ManageRm();
            OResponse _Response = _ManageRm.GetRmTarget(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
