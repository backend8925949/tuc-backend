//==================================================================================
// FileName: TUCOnboardingController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for onboarding merchant and customer functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 13-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Acquirer;
using HCore.TUC.Core.Operations.Acquirer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Acquirer
{
    [Produces("application/json")]
    [Route("api/v3/acquirer/onboarding/[action]")]
    public class TUCOnboardingController : Controller
    {
        ManageOnboarding _ManageOnboarding;
        /// <summary>
        /// Description: Method defined to onboard merchants
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("onboardmerchants")]
        public object OnboardMerchants([FromBody] OAuth.Request _OAuthRequest)
        {
            OOnboarding.Merchant.Request _Request = JsonConvert.DeserializeObject<OOnboarding.Merchant.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboarding = new ManageOnboarding();
            OResponse _Response = _ManageOnboarding.OnboardMerchants(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to get list of onboarded merchants
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getonboardedmerchants")]
        public object GetOnboardedMerchants([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboarding = new ManageOnboarding();
            OResponse _Response = _ManageOnboarding.GetOnboardedMerchants(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Method defined to upload terminal
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("uploadterminals")]
        public object UploadTerminals([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.TerminalUpload.Request _Request = JsonConvert.DeserializeObject<OAccounts.TerminalUpload.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboarding = new ManageOnboarding();
            OResponse _Response = _ManageOnboarding.UploadTerminal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get list of uploaded terminals
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getuploadedterminals")]
        public object GetUploadedTerminal([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboarding = new ManageOnboarding();
            OResponse _Response = _ManageOnboarding.GetUploadedTerminal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method defined to onboard customers
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("onboardcustomers")]
        public object OnboardCustomers([FromBody] OAuth.Request _OAuthRequest)
        {
            OOnboarding.Customer.Request _Request = JsonConvert.DeserializeObject<OOnboarding.Customer.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboarding = new ManageOnboarding();
            OResponse _Response = _ManageOnboarding.OnboardCustomers(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Method defined to get list of onboarded customers
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getonboardedcustomers")]
        public object GetOnboardedCustomers([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboarding = new ManageOnboarding();
            OResponse _Response = _ManageOnboarding.GetOnboardedCustomers(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Method defined to get list of onboarded customer files
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getonboardedcustomerfiles")]
        public object GetOnboardedCustomerFiles([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOnboarding = new ManageOnboarding();
            OResponse _Response = _ManageOnboarding.GetOnboardedCustomerFiles(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
