//==================================================================================
// FileName: TUCPaymentController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for payment functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Object.CustomerWeb;
using HCore.TUC.Core.Operations.CustomerWeb;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.CustomerWeb
{
    [Produces("application/json")]
    [Route("api/v3/w/payment/connect/[action]")]
    public class TUCPaymentController
    {
        ManagePayment _ManagePayment;
        //ManageOperations _ManageOperations;
        /// <summary>
        /// Description: Buy point payment initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("paymentinitialize")]
        public object BuyPointInitialize([FromBody] OBuyPoint.Initialize.Request _Request)
        {
            _ManagePayment = new ManagePayment();
            OResponse _Response = _ManagePayment.BuyPointInitialize(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Verifies the payment.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("verifypayment")]
        public object BuyPointVerify([FromBody] OBuyPoint.Verify.Request _Request)
        {
            _ManagePayment = new ManagePayment();
            OResponse _Response = _ManagePayment.BuyPointVerify(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Cancels the payment.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("cancelpayment")]
        public object BuyPointCancel([FromBody] OBuyPoint.Cancel.Request _Request)
        {
            _ManagePayment = new ManagePayment();
            OResponse _Response = _ManagePayment.BuyPointCancel(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the paymentcharge.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("paymentcharge")]
        public object BuyPointCharge([FromBody] OBuyPoint.Charge.Request _Request)
        {
            _ManagePayment = new ManagePayment();
            OResponse _Response = _ManagePayment.BuyPointCharge(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

    }
}
