//==================================================================================
// FileName: TUCAnalyticsController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for terminal history and account overview functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Object.Partner;
using HCore.TUC.Core.Operations.Partner;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.App.v3.Tuc.Partner
{
    [Produces("application/json")]
    [Route("api/v3/partner/analytics/[action]")]
    public class TUCAnalyticsController : Controller
    {
        ManageAnalytics _ManageAnalytics;
        /// <summary>
        /// Description: Gets the account overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountoverview")]
        public object GetAccountOverview([FromBody]OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            OResponse _Response = _ManageAnalytics.GetAccountOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the terminal activity history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getterminalactivityhistory")]
        public object GetTerminalActivityHistory([FromBody]OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalytics = new ManageAnalytics();
            OResponse _Response = _ManageAnalytics.GetTerminalActivityHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        

        //
    }
}
