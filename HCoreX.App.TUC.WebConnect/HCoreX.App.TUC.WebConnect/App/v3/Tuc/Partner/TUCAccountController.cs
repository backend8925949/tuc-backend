//==================================================================================
// FileName: TUCAccountController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for merchant,terminals and stores functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Partner;
using HCore.TUC.Core.Operations.Partner;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Partner
{
    [Produces("application/json")]
    [Route("api/v3/partner/accounts/[action]")]
    public class TUCAccountController :Controller
    {        
        ManageAccount _ManageAccount;
        /// <summary>
        /// Description: Saves the merchant.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savemerchant")]
        public object SaveMerchant([FromBody]OAuth.Request _OAuthRequest)
        {
            OAccounts.Merchant.Onboarding _Request = JsonConvert.DeserializeObject<OAccounts.Merchant.Onboarding>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.SaveMerchant(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Saves the store.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savestore")]
        public object SaveStore([FromBody]OAuth.Request _OAuthRequest)
        {
            OAccounts.Store.Request _Request = JsonConvert.DeserializeObject<OAccounts.Store.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.SaveStore(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);  
        }
        /// <summary>
        /// Description: Updates the store.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatestore")]
        public object UpdateStore([FromBody]OAuth.Request _OAuthRequest)
        {
            OAccounts.Store.Request _Request = JsonConvert.DeserializeObject<OAccounts.Store.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.UpdateStore(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the merchant details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchant")]
        public object GetMerchant([FromBody]OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetMerchant(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the list of all merchants.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchants")]
        public object GetMerchants([FromBody]OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetMerchant(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the store details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getstore")]
        public object GetStore([FromBody]OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetStore(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the list of all stores.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getstores")]
        public object GetStores([FromBody]OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetStore(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Saves the terminal.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveterminal")]
        public object SaveTerminal([FromBody]OAuth.Request _OAuthRequest)
        {
            OAccounts.Terminal.Request _Request = JsonConvert.DeserializeObject<OAccounts.Terminal.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.SaveTerminal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Saves the terminals.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveterminals")]
        public object SaveTerminals([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.Terminal.BulkRequest _Request = JsonConvert.DeserializeObject<OAccounts.Terminal.BulkRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.SaveTerminal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Updates the terminal status.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateterminalstatus")]
        public object UpdateTerminalStatus([FromBody]OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.UpdateTerminalStatus(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the terminal details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getterminal")]
        public object GetTerminal([FromBody]OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetTerminal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the list of all terminals.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getterminals")]
        public object GetTerminals([FromBody]OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetTerminal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }





        /// <summary>
        /// Description: Gets the customer details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomer")]
        public object GetCustomer([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetCustomer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the  list of all customers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomers")]
        public object GetCustomers([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetCustomer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
