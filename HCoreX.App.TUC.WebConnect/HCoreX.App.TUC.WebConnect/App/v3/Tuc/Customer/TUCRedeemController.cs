//==================================================================================
// FileName: TUCRedeemController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for redeem functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Customer;
using HCore.TUC.Core.Operations.Customer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.App.v3.Tuc.Customer
{
    [Produces("application/json")]
    [Route("api/v3/cust/redeem/[action]")]
    public class TUCRedeemController
    {
        ManageRedeem _ManageRedeem;
        /// <summary>
        /// Description: Redeem initialize.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("redeeminitialize")]
        public object Redeem_Initialize([FromBody] OAuth.Request _OAuthRequest)
        {
            ORedeem.Initialize.Request _Request = JsonConvert.DeserializeObject<ORedeem.Initialize.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageRedeem = new ManageRedeem();
            OResponse _Response = _ManageRedeem.Redeem_Initialize(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Redeem confirm.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("redeemconfirm")]
        public object Redeem_Confirm([FromBody] OAuth.Request _OAuthRequest)
        {
            ORedeem.Confirm.Request _Request = JsonConvert.DeserializeObject<ORedeem.Confirm.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageRedeem = new ManageRedeem();
            OResponse _Response = _ManageRedeem.Redeem_Confirm(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
