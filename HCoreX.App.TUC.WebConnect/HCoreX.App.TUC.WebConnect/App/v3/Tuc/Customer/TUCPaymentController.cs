//==================================================================================
// FileName: TUCPaymentController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for payment functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Customer;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Customer;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.App.v3.Tuc.Customer
{
    [Produces("application/json")]
    [Route("api/v3/cust/payments/[action]")]
    public class TUCPaymentController
    {
        ManageBuyPoint? _ManageBuyPoint;
        //ManageOperations _ManageOperations;


        /// <summary>
        /// Description: Buypoint initialize.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("buypointinitialize")]
        public object BuyPointInitialize([FromBody] OAuth.Request _OAuthRequest)
        {
            OBuyPoint.Initialize.Request _Request = JsonConvert.DeserializeObject<OBuyPoint.Initialize.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBuyPoint = new ManageBuyPoint();
            OResponse _Response = _ManageBuyPoint.BuyPointInitialize(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Buypoint verify.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("buypointverify")]
        public object BuyPointVerify([FromBody] OAuth.Request _OAuthRequest)
        {
            OBuyPoint.Verify.Request _Request = JsonConvert.DeserializeObject<OBuyPoint.Verify.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBuyPoint = new ManageBuyPoint();
            OResponse _Response = _ManageBuyPoint.BuyPointVerify(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Buypoint cancel.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("buypointcancel")]
        public object BuyPointCancel([FromBody] OAuth.Request _OAuthRequest)
        {
            OBuyPoint.Cancel.Request _Request = JsonConvert.DeserializeObject<OBuyPoint.Cancel.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBuyPoint = new ManageBuyPoint();
            OResponse _Response = _ManageBuyPoint.BuyPointCancel(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Buypoint charge.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("buypointcharge")]
        public object BuyPointCharge([FromBody] OAuth.Request _OAuthRequest)
        {
            OBuyPoint.Charge.Request _Request = JsonConvert.DeserializeObject<OBuyPoint.Charge.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBuyPoint = new ManageBuyPoint();
            OResponse _Response = _ManageBuyPoint.BuyPointCharge(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the account point purchase history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpointpurchasehistory")]
        public object GetAccountPointPurchaseHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBuyPoint = new ManageBuyPoint();
            OResponse _Response = _ManageBuyPoint.GetAccountPointPurchaseHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        ManageCashout? _ManageCashout;

        ManageBank? _ManageBank;

        /// <summary>
        /// Description: Gets the bank code.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getbankcodes")]
        public async Task<object> GetBankCode([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBank = new ManageBank();
            OResponse _Response = await _ManageBank.GetBankCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the bank account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getbankaccounts")]
        public async Task<object> GetBankAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBank = new ManageBank();
            OResponse _Response = await _ManageBank.GetBankAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Deletes the bank account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletebankaccount")]
        public async Task<object> DeleteBankAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OBank.Request? _Request = JsonConvert.DeserializeObject<OBank.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBank = new ManageBank();
            OResponse _Response = await _ManageBank.DeleteBankAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Saves the bank account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savebankaccount")]
        public async Task<object> SaveBankAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OBank.Request? _Request = JsonConvert.DeserializeObject<OBank.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBank = new ManageBank();
            OResponse _Response = await _ManageBank.SaveBankAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        [HttpPost]
        [ActionName("quicksavebankaccount")]
        public async Task<object> QuickSaveBankAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OBank.QuickSave? request = JsonConvert.DeserializeObject<OBank.QuickSave>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageBank = new ManageBank();
            OResponse response = await _ManageBank.QuickSaveBankAccount(request);
            return HCoreAuth.Auth_Response(response, request.UserReference);
        }

        /// <summary>
        /// Description: Gets the tuc cash out configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcashoutconfiguration")]
        public async Task<object> GetTucCashOutConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OCashOut.Configuration.Request? _Request = JsonConvert.DeserializeObject<OCashOut.Configuration.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = await _ManageCashout.GetTucCashOutConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Tucs the cash out initialize.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("cashoutinitialize")]
        public async Task<object> TucCashOutInitialize([FromBody] OAuth.Request _OAuthRequest)
        {
            OCashOut.Initialize.Request? _Request = JsonConvert.DeserializeObject<OCashOut.Initialize.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = await _ManageCashout.TucCashOutInitialize(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the tuc cashout.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcashouts")]
        public async Task<object> GetTucCashout([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashout = new ManageCashout();
            OResponse _Response = await _ManageCashout.GetTucCashout(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
