//==================================================================================
// FileName: TUCDealsController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined to get list of cities,categories,merchants,deals and sliders.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Customer;
using HCore.TUC.Core.Operations.Customer;
using HCore.TUC.Plugins.MadDeals;
using HCore.TUC.Plugins.MadDeals.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.App.v3.Tuc.Customer
{
    [Produces("application/json")]
    [Route("api/v3/cust/deals/[action]")]
    public class TUCDealsController
    {
        ManageDeal _ManageDeal;        
        HCore.TUC.Core.Operations.CustomerWeb.ManageDeal _ManageDealOp;

        /// <summary>
        /// Description: Gets the categories.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcategories")]
        public object GetCategories([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetCategories(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the cities.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcities")]
        public object GetCities([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetCities(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Gets the merchants.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchants")]
        public object GetMerchants([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetMerchant(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the slider images.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsliderimages")]
        public object GetSliderImages([FromBody] OAuth.Request _OAuthRequest)
        {
            OMadDeals.Slider.Request _Request = JsonConvert.DeserializeObject<OMadDeals.Slider.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetSliderImages(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Gets the deals.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdeals")]
        public object GetDeals([FromBody] OAuth.Request _OAuthRequest)
        {
            TUC.Plugins.MadDeals.Object.OMadDeals.Deals.List.Request _Request = JsonConvert.DeserializeObject<TUC.Plugins.MadDeals.Object.OMadDeals.Deals.List.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        //[ActionName("")]
        //public object GetDeals([FromBody] OMadDeals.Deals.List.Request _Request)
        //{
        //    _ManageDeal = new ManageDeal();
        //    OResponse _Response = _ManageDeal.(_Request);
        //    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //}
        //[ActionName("getdeal")]
        //public object GetDeal([FromBody] OMadDeals.Deal.Request _Request)
        //{
        //    _ManageDeal = new ManageDeal();
        //    OResponse _Response = _ManageDeal.GetDeal(_Request);
        //    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //}
        //[HttpPost]
        //[ActionName("buydeal")]
        //public object BuyDeal([FromBody] ODeal.Purchase.Request _Request)
        //{
        //    _ManageDealOp = new TUC.Core.Operations.CustomerWeb.ManageDeal();
        //    OResponse _Response = _ManageDealOp.BuyDeal(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}
        //[HttpPost]
        //[ActionName("getdealcode")]
        //public object GetDealCode([FromBody] ODeal.Purchase.Request _Request)
        //{
        //    _ManageDealOp = new TUC.Core.Operations.CustomerWeb.ManageDeal();
        //    OResponse _Response = _ManageDealOp.GetDealCode(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}

        //ManageDealBookmark _ManageDealBookmark;
        //[ActionName("savebookmark")]
        //public object SaveBookMark([FromBody] ODealBookmark.Save.Request _Request)
        //{
        //    _ManageDealBookmark = new ManageDealBookmark();
        //    OResponse _Response = _ManageDealBookmark.SaveBookMark(_Request);
        //    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //}
        //[ActionName("getbookmarks")]
        //public object GetBookmark([FromBody] OList.Request _Request)
        //{
        //    _ManageDealBookmark = new ManageDealBookmark();
        //    OResponse _Response = _ManageDealBookmark.GetBookmark(_Request);
        //    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //}

        //ManageDealReview _ManageDealReview;
        //[ActionName("savedealreview")]
        //public object SaveDealReview([FromBody] ODealReview.Save _Request)
        //{
        //    _ManageDealReview = new ManageDealReview();
        //    OResponse _Response = _ManageDealReview.SaveDealReview(_Request);
        //    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //}

        //[ActionName("getdealreviews")]
        //public object GetDealReview([FromBody] OList.Request _Request)
        //{
        //    _ManageDealReview = new ManageDealReview();
        //    OResponse _Response = _ManageDealReview.GetDealReview(_Request);
        //    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //}
    }
}
