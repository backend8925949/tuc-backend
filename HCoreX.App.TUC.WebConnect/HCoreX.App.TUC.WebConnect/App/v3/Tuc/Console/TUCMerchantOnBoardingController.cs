//==================================================================================
// FileName: TUCMerchantOnBoardingController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for merchant onboarding functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Operations.Console;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Console
{
    [Produces("application/json")]
    [Route("api/v3/con/onboardmerchant/[action]")]

    public class TUCMerchantOnBoardingController
    {
        ManageMerchantOnBoarding _ManageMerchantOnBoarding;

        /// <summary>
        /// Description: Gets the merchant list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchantlist")]
        public object GetMerchantList([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnBoarding = new ManageMerchantOnBoarding();
            OResponse _Response = _ManageMerchantOnBoarding.GetMerchantList(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Resends the merchant verification email.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("resendemail")]
        public object ResendMerchantVerificationEmail([FromBody] OAuth.Request _OAuthRequest)
        {
            OMerchantOnBoarding.EmailVerificationRequest _Request = JsonConvert.DeserializeObject<OMerchantOnBoarding.EmailVerificationRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnBoarding = new ManageMerchantOnBoarding();
            OResponse _Response = _ManageMerchantOnBoarding.ResendMerchantVerificationEmail(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Resends the mobile verification otp.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("resendmobileotp")]
        public object ResendMobileVerificationOtp([FromBody] OAuth.Request _OAuthRequest)
        {
            OMerchantOnBoarding.MobileVerificationRequest _Request = JsonConvert.DeserializeObject<OMerchantOnBoarding.MobileVerificationRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnBoarding = new ManageMerchantOnBoarding();
            OResponse _Response = _ManageMerchantOnBoarding.ResendMobileVerificationOtp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
