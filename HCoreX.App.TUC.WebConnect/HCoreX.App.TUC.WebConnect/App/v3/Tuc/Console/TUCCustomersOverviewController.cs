﻿using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Console.Analytics;
using HCore.TUC.Core.Operations.Console.Analytics;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCoreX.App.TUC.WebConnect.App.v3.Tuc.Console
{
    [Produces("application/json")]
    [Route("api/v3/con/custoverview/[action]")]
    public class TUCCustomersOverviewController : Controller
    {
        ManageCustomersOverview _ManageCustomersOverview;

        [HttpPost]
        [ActionName("getfrequentlypurchasedcategories")]
        public object GetFrequentlyPurchasedCategories([FromBody] OAuth.Request _OAuthRequest)
        {
            OCustomerOverview.Request _Request = JsonConvert.DeserializeObject<OCustomerOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomersOverview = new ManageCustomersOverview();
            OResponse _Response = _ManageCustomersOverview.GetFrequentlyPurchasedCategories(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("getfrequentlyvisitedplaces")]
        public object GetFrequentlyVisitedPlaces([FromBody] OAuth.Request _OAuthRequest)
        {
            OCustomerOverview.Request _Request = JsonConvert.DeserializeObject<OCustomerOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomersOverview = new ManageCustomersOverview();
            OResponse _Response = _ManageCustomersOverview.GetFrequentlyVisitedPlaces(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("getcustomerstransactions")]
        public object GetCustomersTransactions([FromBody] OAuth.Request _OAuthRequest)
        {
            OCustomerOverview.Request _Request = JsonConvert.DeserializeObject<OCustomerOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomersOverview = new ManageCustomersOverview();
            OResponse _Response = _ManageCustomersOverview.GetCustomersTransactions(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("gettransactionsoverview")]
        public object GetTransactionsOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OCustomerOverview.Request _Request = JsonConvert.DeserializeObject<OCustomerOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomersOverview = new ManageCustomersOverview();
            OResponse _Response = _ManageCustomersOverview.GetTransactionsOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}

