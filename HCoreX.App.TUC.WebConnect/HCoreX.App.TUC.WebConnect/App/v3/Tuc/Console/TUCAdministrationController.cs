//==================================================================================
// FileName: TUCAdministrationController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for system administration functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Operations.Console;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;


namespace HCore.Api.App.v3.Tuc.Console
{ 
    [Produces("application/json")]
    [Route("api/v3/con/sys/[action]")]
    public class TUCAdministrationController
    {
        ManageSystemAdministration _ManageSystemAdministration;
        ManageRefund _ManageRefund;

        /// <summary>
        /// Description: Notifies the customers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("notifycustomers")]
        public object NotifyCustomers([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.PushNotification.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.PushNotification.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            if (!string.IsNullOrEmpty(_Request.Title) && !string.IsNullOrEmpty(_Request.Message))
            {
                HCoreHelper.SendPushToTopic("tuccustomer", "dashboard", _Request.Title, _Request.Message, "dashboard", 0, null, "Confirm", null , null , _Request.UserReference);
            }
            OResponse _Response = HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "HC200");
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Refunds the customer.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("refundcustomer")]
        public object RefundCustomer([FromBody] OAuth.Request _OAuthRequest)
        {
            ORefund.Request _Request = JsonConvert.DeserializeObject<ORefund.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageRefund = new ManageRefund();
            OResponse _Response = _ManageRefund.RefundCustomer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the refunds.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getrefunds")]
        public object GetRefunds([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageRefund = new ManageRefund();
            OResponse _Response = _ManageRefund.GetRefunds(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Saves the slider image.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savesliderimage")]
        public object SaveSliderImage([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.AppSlider.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.AppSlider.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.SaveSliderImage(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Updates the slider image status.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatesliderimagestatus")]
        public object UpdateSliderImageStatus([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.AppSlider.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.AppSlider.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.UpdateSliderImageStatus(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Deletes the slider image.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletesliderimage")]
        public object DeleteSliderImage([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.AppSlider.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.AppSlider.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.DeleteSliderImage(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the slider image.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsliderimages")]
        public object GetSliderImage([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetSliderImage(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the otp requests.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getotprequests")]
        public object GetOtpRequests([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetOtpRequest(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the otp request.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getotprequest")]
        public object GetOtpRequest([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.SystemVerification.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.SystemVerification.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetOtpRequest(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        //[HttpPost]
        //[ActionName("getcategories")]
        //public object GetCategories([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageSystemAdministration = new ManageSystemAdministration();
        //    OResponse _Response = _ManageSystemAdministration.GetCategory(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}
        //[HttpPost]
        //[ActionName("savecategory")]
        //public object SaveCategory([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    OSystemAdministration.Category.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.Category.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageSystemAdministration = new ManageSystemAdministration();
        //    OResponse _Response = _ManageSystemAdministration.SaveCategory(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}

        //[HttpPost]
        //[ActionName("updatecategory")]
        //public object UpdateCategory([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    OSystemAdministration.Category.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.Category.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageSystemAdministration = new ManageSystemAdministration();
        //    OResponse _Response = _ManageSystemAdministration.UpdateCategory(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}

        /// <summary>
        /// Description: Gets the system log.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsystemlog")]
        public object GetSystemLog([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.ExceptionLog.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.ExceptionLog.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetSystemLog(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Gets the system logs.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsystemlogs")]
        public object GetSystemLogs([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetSystemLog(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Updates the response code.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateresponsecode")]
        public object UpdateResponseCode([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.ResponseCode.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.ResponseCode.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.UpdateResponseCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the response code.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getresponsecodes")]
        public object GetResponseCode([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetResponseCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }



        /// <summary>
        /// Description: Saves the configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveconfiguration")]
        public object SaveConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.Configuration.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.Configuration.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.SaveConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Updates the configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateconfiguration")]
        public object UpdateConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.Configuration.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.Configuration.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.UpdateConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Updates the configuration value.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateconfigurationvalue")]
        public object UpdateConfigurationValue([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.ConfigurationValue.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.ConfigurationValue.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.UpdateConfigurationValue(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Updates the account configuration value.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateaccountconfigurationvalue")]
        public object UpdateAccountConfigurationValue([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.AccountConfiguration.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.AccountConfiguration.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.UpdateConfigurationValue(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Updates the account configuration values.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateaccountconfigurationvalues")]
        public object UpdateAccountConfigurationValues([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.AccountConfiguration.BulkRequest _Request = JsonConvert.DeserializeObject<OSystemAdministration.AccountConfiguration.BulkRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.UpdateConfigurationValue(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Deletes the configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deleteconfiguration")]
        public object DeleteConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.Configuration.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.Configuration.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.DeleteConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getconfigurations")]
        public object GetConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the configuration account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountconfigurations")]
        public object GetConfigurationAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetConfigurationAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }



        /// <summary>
        /// Description: Gets the configuration history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getconfigurationhistory")]
        public object GetConfigurationHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetConfigurationHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }






        /// <summary>
        /// Description: Gets the features.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getfeatures")]
        public object GetFeatures([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetFeature(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Gets the departments.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdepartments")]
        public object GetDepartments([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetDepartment(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the roles.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getroles")]
        public object GetRoles([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetRole(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the role features.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getrolefeatures")]
        public object GetRoleFeatures([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetRoleFeature(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Saves the role.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saverole")]
        public object SaveRole([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.Role.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.Role.Request > (HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.SaveRole(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Updates the role.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updaterole")]
        public object UpdateRole([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.Role.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.Role.Request > (HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.UpdateRole(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Saves the role feature.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saverolefeature")]
        public object SaveRoleFeature([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.RoleFeature.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.RoleFeature.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.SaveRoleFeature(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Removes the role feature.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deleterolefeature")]
        public object RemoveRoleFeature([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.RoleFeature.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.RoleFeature.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.RemoveRoleFeature(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Deletes the role.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deleterole")]
        public object DeleteRole([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.Role.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.Role.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.DeleteRole(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Gets the apps.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getapps")]
        public object GetApps([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetApp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the application.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getapp")]
        public object GetApp([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.App.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.App.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetApp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Saves the application.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveapp")]
        public object SaveApp([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.App.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.App.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.SaveApp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Updates the application.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateapp")]
        public object UpdateApp([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.App.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.App.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.UpdateApp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Deletes the application.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deleteapp")]
        public object DeleteApp([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.App.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.App.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.DeleteApp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Resets the application key.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("resetappkey")]
        public object ResetAppKey([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.App.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.App.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.ResetAppKey(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Gets the application usage.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getrequesthistory")]
        public object GetAppUsage([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetAppUsage(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the application usage details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getrequesthistorydetails")]
        public object GetAppUsageDetails([FromBody] OAuth.Request _OAuthRequest)
        {
            OSystemAdministration.App.Request _Request = JsonConvert.DeserializeObject<OSystemAdministration.App.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetAppUsage(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the SMS notification.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsmsnotifications")]
        public object GetSmsNotification([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetSmsNotification(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the feature list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getfeaturelist")]
        public object GetFeatureList([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetFeatureList(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the role details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getroledetails")]
        public object GetRoleDetails([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemAdministration = new ManageSystemAdministration();
            OResponse _Response = _ManageSystemAdministration.GetRoleDetails(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

    }
}
