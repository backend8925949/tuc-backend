//==================================================================================
// FileName: TUCCashoutController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for cashout functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Operations.Console;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Console
{
    [Produces("application/json")]
    [Route("api/v3/con/co/[action]")]
    public class TUCCashoutController
    {
        ManageCashoutOperation _ManageCashoutOperation;
        /// <summary>
        /// Description:Method to get list for cashouts in Console Panel.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcashouts")]
        public async Task<object> GetCashouts([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashoutOperation = new ManageCashoutOperation();
            OResponse _Response = await _ManageCashoutOperation.GetCashout(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description:Method to get overview for cashouts in Console Panel.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcashoutsoverview")]
        public object GetCashoutsOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashoutOperation = new ManageCashoutOperation();
            OResponse _Response = _ManageCashoutOperation.GetCashoutOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description:Method to get detail for cashouts in Console Panel.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcashout")]
        public async Task<object> GetCashout([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashoutOperation = new ManageCashoutOperation();
            OResponse _Response = await _ManageCashoutOperation.GetCashout(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description:Method to update status for cashout requests from Console Panel.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updatecashoutstatus")]
        public async Task<object> UpdateCashoutStatus([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashoutOperation = new ManageCashoutOperation();
            OResponse _Response = await _ManageCashoutOperation.UpdateCashoutStatus(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description:Method to get pending cashout counts.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getpendingcashoutcounts")]
        public object GetPendingCashoutCounts([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCashoutOperation = new ManageCashoutOperation();
            OResponse _Response = _ManageCashoutOperation.GetPendingCashoutCounts(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
