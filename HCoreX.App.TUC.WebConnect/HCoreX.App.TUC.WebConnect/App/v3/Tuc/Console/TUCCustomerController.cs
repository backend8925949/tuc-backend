//==================================================================================
// FileName: TUCCustomerController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for credit supercash and point purchase functionality of customer.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Operations.Console;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Console
{
    [Produces("application/json")]
    [Route("api/v3/con/cust/[action]")]
    public class TUCCustomerController
    {
        ManageCustomer _ManageCustomer;
        /// <summary>
        /// Description: Credits the super cash.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("creditsupercash")]
        public object CreditSuperCash([FromBody] OAuth.Request _OAuthRequest)
        {
            OUniversalPoint.Request _Request = JsonConvert.DeserializeObject<OUniversalPoint.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSuperCash = new ManageSuperCash();
            OResponse _Response = _ManageSuperCash.CreditSuperCash(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        ManageSuperCash _ManageSuperCash;
        /// <summary>
        /// Description: Gets the super cash.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getsupercashcredithistory")]
        public object GetSuperCash([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSuperCash = new ManageSuperCash();
            OResponse _Response = _ManageSuperCash.GetSuperCash(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the point purchase history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpointpurchasehistory")]
        public object GetPointPurchaseHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomer = new ManageCustomer();
            OResponse _Response = _ManageCustomer.GetPointPurchaseHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the point purchase overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpointpurchaseoverview")]
        public object GetPointPurchaseOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomer = new ManageCustomer();
            OResponse _Response = _ManageCustomer.GetPointPurchaseOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
