//==================================================================================
// FileName: TUCAdminController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for admin user Functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Operations.Console;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Console
{  
    [Produces("application/json")]
    [Route("api/v3/con/admin/[action]")]
    public class TUCAdminController
    {
        ManageAdminUser _ManageAdminUser;
        /// <summary>
        /// Description: Gets the accounts.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccounts")]
        public object GetAccounts([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAdminUser = new ManageAdminUser();
            OResponse _Response = _ManageAdminUser.GetAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccount")]
        public object GetAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OAdminUser.Details.Request _Request = JsonConvert.DeserializeObject<OAdminUser.Details.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAdminUser = new ManageAdminUser();
            OResponse _Response = _ManageAdminUser.GetAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Saves the account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveaccount")]
        public object SaveAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OAdminUser.Request _Request = JsonConvert.DeserializeObject<OAdminUser.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAdminUser = new ManageAdminUser();
            OResponse _Response = _ManageAdminUser.SaveAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Updates the account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateaccount")]
        public object UpdateAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            OAdminUser.Request _Request = JsonConvert.DeserializeObject<OAdminUser.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAdminUser = new ManageAdminUser();
            OResponse _Response = _ManageAdminUser.UpdateAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Expires the account session.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("expireaccountsession")]
        public object ExpireAccountSession([FromBody] OAuth.Request _OAuthRequest)
        {
            OAdminUser.Session.Request _Request = JsonConvert.DeserializeObject<OAdminUser.Session.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAdminUser = new ManageAdminUser();
            OResponse _Response = _ManageAdminUser.ExpireAccountSession(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the account session.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountsessions")]
        public object GetAccountSession([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAdminUser = new ManageAdminUser();
            OResponse _Response = _ManageAdminUser.GetAccountSession(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Updates the admin username.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateusername")]
        public object UpdateAdminUsername([FromBody] OAuth.Request _OAuthRequest)
        {
            OAdminUser.AuthUpdate.Request _Request = JsonConvert.DeserializeObject<OAdminUser.AuthUpdate.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAdminUser = new ManageAdminUser();
            OResponse _Response = _ManageAdminUser.UpdateAdminUsername(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Updates the admin password.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatepassword")]
        public object UpdateAdminPassword([FromBody] OAuth.Request _OAuthRequest)
        {
            OAdminUser.AuthUpdate.Request _Request = JsonConvert.DeserializeObject<OAdminUser.AuthUpdate.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAdminUser = new ManageAdminUser();
            OResponse _Response = _ManageAdminUser.UpdateAdminPassword(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Updates the admin reset password.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("resetpassword")]
        public object UpdateAdminResetPassword([FromBody] OAuth.Request _OAuthRequest)
        {
            OAdminUser.AuthUpdate.Request _Request = JsonConvert.DeserializeObject<OAdminUser.AuthUpdate.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAdminUser = new ManageAdminUser();
            OResponse _Response = _ManageAdminUser.UpdateAdminResetPassword(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Updates the admin pin.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatepin")]
        public object UpdateAdminPin([FromBody] OAuth.Request _OAuthRequest)
        {
            OAdminUser.AuthUpdate.Request _Request = JsonConvert.DeserializeObject<OAdminUser.AuthUpdate.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAdminUser = new ManageAdminUser();
            OResponse _Response = _ManageAdminUser.UpdateAdminPin(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Updates the admin reset pin.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("resetpin")]
        public object UpdateAdminResetPin([FromBody] OAuth.Request _OAuthRequest)
        {
            OAdminUser.AuthUpdate.Request _Request = JsonConvert.DeserializeObject<OAdminUser.AuthUpdate.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAdminUser = new ManageAdminUser();
            OResponse _Response = _ManageAdminUser.UpdateAdminResetPin(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Sets the App Logo
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("setapplogo")]
        public object SetAppLogo([FromBody] OAuth.Request _OAuthRequest)
        {
            OAdminUser.AuthUpdate.LogoRequest _Request = JsonConvert.DeserializeObject<OAdminUser.AuthUpdate.LogoRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAdminUser = new ManageAdminUser();
            OResponse _Response = _ManageAdminUser.SetAppLogo(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Gets the App Logo
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getapplogo")]
        public object GetAppLogo([FromBody] OAuth.Request _OAuthRequest)
        {
            OAdminUser.AuthUpdate.LogoRequest _Request = JsonConvert.DeserializeObject<OAdminUser.AuthUpdate.LogoRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAdminUser = new ManageAdminUser();
            OResponse _Response = _ManageAdminUser.GetAppLogo(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


    }
}
