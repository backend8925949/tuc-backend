//==================================================================================
// FileName: TUCAnalyticsCustomController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for customer analystics.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Console.Analytics;
using HCore.TUC.Core.Object.Console.Analytics.Customer;
using HCore.TUC.Core.Operations.Console;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.App.v3.Tuc.Console
{ 
    [Produces("application/json")]
    [Route("api/v3/con/anac/[action]")]
    public class TUCAnalyticsCustomController
    {
        ManageAnalyticsCustom _ManageAnalyticsCustom;
        /// <summary>
        /// Description: Gets the customers count.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomerscount")]
        public object GetCustomersCount([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnaCustom.Request _Request = JsonConvert.DeserializeObject<OAnaCustom.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalyticsCustom = new ManageAnalyticsCustom();
            OResponse _Response = _ManageAnalyticsCustom.GetCustomersCount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the customers balance overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomerbalanceoverview")]
        public object GetCustomersBalanceOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnaCustom.Request _Request = JsonConvert.DeserializeObject<OAnaCustom.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalyticsCustom = new ManageAnalyticsCustom();
            OResponse _Response = _ManageAnalyticsCustom.GetCustomersBalanceOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Gets the customer registration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomerregistrations")]
        public object GetCustomerRegistration([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalyticsCustom = new ManageAnalyticsCustom();
            OResponse _Response = _ManageAnalyticsCustom.GetCustomerRegistration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the application downloads.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getappdownloads")]
        public object GetAppDownloads([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalyticsCustom = new ManageAnalyticsCustom();
            OResponse _Response = _ManageAnalyticsCustom.GetAppDownloads(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the customer visits.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomervisits")]
        public object GetCustomerVisits([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageAnalyticsCustom = new ManageAnalyticsCustom();
            OResponse _Response = _ManageAnalyticsCustom.GetCustomerVisits(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
