﻿using System;
using HCore.Helper;
using HCore.TUC.Core.Operations.Console.Analytics;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCoreX.App.TUC.WebConnect.App.v3.Tuc.Console.Analytics
{
    [Produces("application/json")]
    [Route("api/v3/con/reports/[action]")]
    public class TUCReportsController : Controller
    {
        ManageReports _ManageReports;

        [HttpPost]
        [ActionName("getsalesreport")]
        public object GetSalesReport([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageReports = new ManageReports();
            OResponse _Response = _ManageReports.GetSalesReport(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("getrewardsreport")]
        public object GetRewardReport([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageReports = new ManageReports();
            OResponse _Response = _ManageReports.GetRewardReport(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("getredeemreport")]
        public object GetRedeemReport([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageReports = new ManageReports();
            OResponse _Response = _ManageReports.GetRedeemReport(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("getrewardclaimreport")]
        public object GetRewardClaimReport([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageReports = new ManageReports();
            OResponse _Response = _ManageReports.GetRewardClaimReport(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}

