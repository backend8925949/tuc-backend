//==================================================================================
// FileName: TUCMerchantController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for point purchase history.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 14-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Object.Console;
using HCore.TUC.Core.Operations.Console;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Console
{
    [Produces("application/json")]
    [Route("api/v3/con/m/[action]")]
    public class TUCMerchantController
    {


        ManageMerchant _ManageMerchant;
        /// <summary>
        /// Description: Gets the point purchase history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpointpurchasehistory")]
        public object GetPointPurchaseHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchant = new ManageMerchant();
            OResponse _Response = _ManageMerchant.GetPointPurchaseHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
