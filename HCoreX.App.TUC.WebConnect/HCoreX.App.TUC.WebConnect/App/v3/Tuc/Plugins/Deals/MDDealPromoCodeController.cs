﻿using System;
using HCore.Helper;
using HCore.TUC.Plugins.Deals.PromoCode;
using HCore.TUC.Plugins.Deals.PromoCode.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCoreX.App.TUC.WebConnect.App.v3.Tuc.Plugins.Deals
{
	[Produces("application/json")]
	[Route("api/v3/plugins/promocode/[action]")]
	public class MDDealPromoCodeController
	{
		ManageDealPromoCode? _ManageDealPromoCode;

        [HttpPost]
        [ActionName("cretatepromocode")]
		public async Task<object> CreatePromoCode([FromBody] OAuth.Request _OAuthRequest)
		{
			OPromoCode.CreatePromoCode.Request? _Request = JsonConvert.DeserializeObject<OPromoCode.CreatePromoCode.Request?>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
			_ManageDealPromoCode = new ManageDealPromoCode();
			OResponse _Response = await _ManageDealPromoCode.CreatePromoCode(_Request);
			return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("updatepromocode")]
        public async Task<object> UpdatePromoCodeStatus([FromBody] OAuth.Request _OAuthRequest)
        {
            OPromoCode.UpdatePromoCode.Request? _Request = JsonConvert.DeserializeObject<OPromoCode.UpdatePromoCode.Request?>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealPromoCode = new ManageDealPromoCode();
            OResponse _Response = await _ManageDealPromoCode.UpdatePromoCodeStatus(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("getpromocodes")]
        public async Task<object> GetPromoCodes([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request?>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealPromoCode = new ManageDealPromoCode();
            OResponse _Response = await _ManageDealPromoCode.GetPromoCodes(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}

