//==================================================================================
// FileName: TucDealOperationsController.cs
// Author : Harshal Gandole
// Created On : 
// Description : controller for deal related operation
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Deals;
using HCore.TUC.Plugins.Deals.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Plugins.Deals
{
    [Produces("application/json")]
    [Route("api/v3/plugins/dealsop/[action]")]
    public class TucDealOperationsController
    {
        ManageDealOperation _ManageDealOperation;
        /// <summary>
        /// Description: Gets the deal merchants.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchantdeals")]
        public object GetDealMerchants([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.GetDealMerchants(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the deal list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdeals")]
        public object GetDeals([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.GetDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the deal details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdeal")]
        public object GetDeal([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.GetDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Saves the deal view.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savedealview")]
        public object SaveDealView([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.SaveDealView(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Updates the deal comment.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatedealcodecomment")]
        public object UpdateDealComment([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.UpdateDealComment(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Buydeal initialize.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("buydeal_initizalize")]
        public object BuyDeal_Initialize([FromBody] OAuth.Request _OAuthRequest)
        {
            ODealOperation.DealPayment.Initialize _Request = JsonConvert.DeserializeObject<ODealOperation.DealPayment.Initialize>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.BuyDeal_Initialize(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Buydeal confirm.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("buydeal_confirm")]
        public object BuyDeal_Confirm([FromBody] OAuth.Request _OAuthRequest)
        {
            ODealOperation.DealPayment.Confirm _Request = JsonConvert.DeserializeObject<ODealOperation.DealPayment.Confirm>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.BuyDeal_Confirm(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Buydeal initialize v2.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("buydeal_initizalizev2")]
        public object BuyDeal_InitializeV2([FromBody] OAuth.Request _OAuthRequest)
        {
            ODealOperation.DealPurchase.Initialize.Request _Request = JsonConvert.DeserializeObject<ODealOperation.DealPurchase.Initialize.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.BuyDeal_InitializeV2(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Buydeal confirm v2.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("buydeal_confirmv2")]
        public object BuyDeal_ConfirmV2([FromBody] OAuth.Request _OAuthRequest)
        {
            ODealOperation.DealPurchase.Confirm.Request _Request = JsonConvert.DeserializeObject<ODealOperation.DealPurchase.Confirm.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.BuyDeal_ConfirmV2(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Gets the deal codes.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdealcodes")]
        public object GetDealCodes([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.GetDealCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the deal code details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdealcode")]
        public object GetDealCode([FromBody] OAuth.Request _OAuthRequest)
        {
            ODealOperation.DealCode.Request _Request = JsonConvert.DeserializeObject<ODealOperation.DealCode.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.GetDealCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the product deal.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getproductpurchasehistory")]
        public object GetProductDeal([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.GetProductDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Shares the deal code.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("sharedealcode")]
        public object ShareDealCode([FromBody] OAuth.Request _OAuthRequest)
        {
            ODealOperation.DealCode.Request _Request = JsonConvert.DeserializeObject<ODealOperation.DealCode.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.ShareDealCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Deal redeem initialize.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("dealredeeminitialize")]
        public object DealRedeem_Initialize([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.DealRedeem_Initialize(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Deal redeem confirm.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("dealredeemconfirm")]
        public object DealRedeem_Confirm([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.DealRedeem_Confirm(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the merchant redeem history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchantredeemhistory")]
        public object GetMerchantRedeemHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.GetMerchantRedeemHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the merchant deal code details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchantdealcodedetails")]
        public object GetMerchantDealCodeDetails([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.GetMerchantDealCodeDetails(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
