//==================================================================================
// FileName: BNPLCustomerOperationsController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for BNPL customer functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
// using HCore.Integration.Evolve.AppAPI;
// using HCore.Integration.Evolve.AppAPI.Framework;
using HCore.TUC.Plugins.Deals;
using HCore.TUC.Plugins.Deals.Object;
using HCore.TUC.Plugins.Loan;
using HCore.TUC.Plugins.Loan.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.App.v3.Tuc.Plugins.Loan
{
    [Produces("application/json")]
    [Route("api/v3/plugins/bnpl/[action]")]
    public class BNPLCustomerOperationsController
    {
        ManageCustomerOperation _ManageCustomerOperation;
        ManageDisbursement _ManageDisbursement;

        /// <summary>
        /// Description: Gets the customer configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcustomerconfiguration")]
        public object GetCustomerConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OCustomerOperation.Customer.Configuration.Request _Request = JsonConvert.DeserializeObject<OCustomerOperation.Customer.Configuration.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomerOperation = new ManageCustomerOperation();
            OResponse _Response = _ManageCustomerOperation.GetCustomerConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Saves the customer.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savecustomer")]
        public object SaveCustomer([FromBody] OAuth.Request _OAuthRequest)
        {
            OCustomerOperation.Customer.Profile.Request _Request = JsonConvert.DeserializeObject<OCustomerOperation.Customer.Profile.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomerOperation = new ManageCustomerOperation();
            OResponse _Response = _ManageCustomerOperation.SaveCustomer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the merchant list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchants")]
        public object GetMerchants([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomerOperation = new ManageCustomerOperation();
            OResponse _Response = _ManageCustomerOperation.GetMerchants(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the plan list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getplans")]
        public object GetPlans([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomerOperation = new ManageCustomerOperation();
            OResponse _Response = _ManageCustomerOperation.GetPlans(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Initializes the loan.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        // [HttpPost]
        // [ActionName("initializeloan")]
        // public object InitializeLoan([FromBody] OAuth.Request _OAuthRequest)
        // {
        //     OCustomerOperation.Loan.Initialize _Request = JsonConvert.DeserializeObject<OCustomerOperation.Loan.Initialize>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //     _ManageCustomerOperation = new ManageCustomerOperation();
        //     OResponse _Response = _ManageCustomerOperation.InitializeLoan(_Request);
        //     return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        // }

        /// <summary>
        /// Description: Gets the loan list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getloans")]
        public object GetLoans([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomerOperation = new ManageCustomerOperation();
            OResponse _Response = _ManageCustomerOperation.GetLoans(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the loan.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getloan")]
        public object GetLoan([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomerOperation = new ManageCustomerOperation();
            OResponse _Response = _ManageCustomerOperation.GetLoan(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the repayment list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getrepayments")]
        public object GetRepayments([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomerOperation = new ManageCustomerOperation();
            OResponse _Response = _ManageCustomerOperation.GetRepayments(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Checks the customer configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("checkconfiguration")]
        public object CheckCustomerConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OCustomerOperation.Configuration _Request = JsonConvert.DeserializeObject<OCustomerOperation.Configuration>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCustomerOperation = new ManageCustomerOperation();
            OResponse _Response = _ManageCustomerOperation.CheckCustomerConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
