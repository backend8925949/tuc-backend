//==================================================================================
// FileName: BNPLPlanController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for plan functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Api.App.v3.Tuc.Plugins.Loan;
using HCore.Helper;
// using HCore.Integration.Evolve.AppAPI;
// using HCore.Integration.Evolve.AppAPI.Framework;
using HCore.Operation.Object;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Operations;
using HCore.TUC.Plugins.Loan.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Operations
{
    [Produces("application/json")]
    [Route("api/v3/tuc/accountplan/[action]")]
    public class BNPLPlanController : Controller
    {
        ManagePlan _ManagePlan;

        /// <summary>
        /// Description: Saves the plan.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveplan")]
        public object SavePlan([FromBody] OAuth.Request _OAuthRequest)
        {
            OPlanOperation.Save.Request _Request = JsonConvert.DeserializeObject<OPlanOperation.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePlan = new ManagePlan();
            OResponse _Response = _ManagePlan.SavePlan(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Updates the plan.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateplan")]
        public object UpdatePlan([FromBody] OAuth.Request _OAuthRequest)
        {
            OPlanOperation.Update _Request = JsonConvert.DeserializeObject<OPlanOperation.Update>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePlan = new ManagePlan();
            OResponse _Response = _ManagePlan.UpdatePlan(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Deletes the plan.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deleteplan")]
        public object DeletePlan([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePlan = new ManagePlan();
            OResponse _Response = _ManagePlan.DeletePlan(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the plan.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getplan")]
        public object GetPlan([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePlan = new ManagePlan();
            OResponse _Response = _ManagePlan.GetPlan(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the plan list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getplanes")]
        public object GetPlans([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePlan = new ManagePlan();
            OResponse _Response = _ManagePlan.GetPlans(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


        /// <summary>
        /// Description: Saves the merchant.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savemerchant")]
        public object SaveMerchant([FromBody] OAuth.Request _OAuthRequest)
        {
            OMerchantOperation.Save.Request _Request = JsonConvert.DeserializeObject<OMerchantOperation.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePlan = new ManagePlan();
            OResponse _Response = _ManagePlan.SaveMerchant(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Updates the merchant.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatemerchant")]
        public object UpdateMerchant([FromBody] OAuth.Request _OAuthRequest)
        {
            OMerchantOperation.Update _Request = JsonConvert.DeserializeObject<OMerchantOperation.Update>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePlan = new ManagePlan();
            OResponse _Response = _ManagePlan.UpdateMerchant(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Updates the merchant status.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatemerchantstatus")]
        public object UpdateMerchantStatus([FromBody] OAuth.Request _OAuthRequest)
        {
            OMerchantOperation.Update _Request = JsonConvert.DeserializeObject<OMerchantOperation.Update>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePlan = new ManagePlan();
            OResponse _Response = _ManagePlan.UpdateMerchantStatus(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Updates the merchant settelement criteria.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatesettelementcriteria")]
        public object UpdateMerchantSettelementCriteria([FromBody] OAuth.Request _OAuthRequest)
        {
            OMerchantOperation.Update _Request = JsonConvert.DeserializeObject<OMerchantOperation.Update>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePlan = new ManagePlan();
            OResponse _Response = _ManagePlan.UpdateMerchantSettelementCriteria(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Deletes the merchant.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletemerchant")]
        public object DeleteMerchant([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePlan = new ManagePlan();
            OResponse _Response = _ManagePlan.DeleteMerchant(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the merchant.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchant")]
        public object GetMerchant([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePlan = new ManagePlan();
            OResponse _Response = _ManagePlan.GetBNPLMerchant(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the merchant list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getmerchants")]
        public object GetMerchants([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePlan = new ManagePlan();
            OResponse _Response = _ManagePlan.GetMerchants(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Manages the settlement.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("managesettelment")]
        public object ManageSettlement([FromBody] OAuth.Request _OAuthRequest)
        {
            OMerchantOperation.SettelmentRequest _Request = JsonConvert.DeserializeObject<OMerchantOperation.SettelmentRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManagePlan = new ManagePlan();
            OResponse _Response = _ManagePlan.ManageSettlement(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}