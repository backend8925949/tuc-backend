// //==================================================================================
// // FileName: EvolveController.cs
// // Author : Harshal Gandole
// // Created On : 
// // Description : Class defined for Evolve API functionality.
// //
// // COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
// //
// // Revision History:
// // Date             : Changed By        : Comments
// // ---------------------------------------------------------------------------------
// //                 | Harshal Gandole   : Created New File
// // 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
// //
// //==================================================================================

// using HCore.Helper;
// using HCore.Integration.Evolve;
// using HCore.Integration.Evolve.EvolveObject.Requests;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using Mono.EvolveObject;
// using Newtonsoft.Json;
// using System;

// namespace HCore.Api.App.v3.Tuc.Evolve
// {
//     [Produces("application/json")]
//     [Route("api/v3/evolve/evolve/[action]")]
//     [ApiController]
//     public class EvolveController : ControllerBase
//     {
//         EvolveOperations _evolveOperation = new EvolveOperations();


//         /// <summary>
//         /// Description: Checks the user exists.
//         /// </summary>
//         /// <param name="_OAuthRequest">The o authentication request.</param>
//         /// <returns>System.Object.</returns>
//         [HttpPost]
//         [ActionName("checkuserexists")]
//         public object CheckUserExists([FromBody] OAuth.Request _OAuthRequest)
//         {
//             CreateUserRequest _Request = JsonConvert.DeserializeObject<CreateUserRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//             OResponse _Response = _evolveOperation.CheckUserExists(_Request);
//             return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
//         }
//         /// <summary>
//         /// Description: Checks the credit limit.
//         /// </summary>
//         /// <param name="_OAuthRequest">The o authentication request.</param>
//         /// <returns>System.Object.</returns>
//         [HttpPost]
//         [ActionName("checkcreditlimit")]
//         public object CheckCreditLimit([FromBody] OAuth.Request _OAuthRequest)
//         {
//             CreateUserRequest _Request = JsonConvert.DeserializeObject<CreateUserRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//             OResponse _Response = _evolveOperation.CheckCreditLimit(_Request);
//             return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
//         }

//         /// <summary>
//         /// Description: Creates the user.
//         /// </summary>
//         /// <param name="_OAuthRequest">The o authentication request.</param>
//         /// <returns>System.Object.</returns>
//         [HttpPost]
//         [ActionName("createuser")]
//         public object CreateUser([FromBody] OAuth.Request _OAuthRequest)
//         {
//             CreateUserRequest _Request = JsonConvert.DeserializeObject<CreateUserRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//             OResponse _Response = _evolveOperation.CreateUser(_Request);
//             return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
//         }

//         //[HttpPost]
//         //[ActionName("addbankstatements")]
//         //public object AddBankStatement([FromBody] OAuth.Request _OAuthRequest, long UserId)
//         //{
//         //    AddBankStatementRequest _Request = JsonConvert.DeserializeObject<AddBankStatementRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//         //    OResponse _Response = _evolveOperation.AddBankStatement(_Request, UserId);
//         //    return HCoreAuth.Auth_Response(_Response, _Request.userReference);
//         //}

//         /// <summary>
//         /// Description: Creates the loan request.
//         /// </summary>
//         /// <param name="_OAuthRequest">The o authentication request.</param>
//         /// <returns>System.Object.</returns>
//         [HttpPost]
//         [ActionName("createloanrequest")]
//         public object CreateLoanRequest([FromBody] OAuth.Request _OAuthRequest)
//         {
//             LoanRequest _Request = JsonConvert.DeserializeObject<LoanRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//             OResponse _Response = _evolveOperation.CreateLoanRequest(_Request);
//             return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
//         }

//         //[HttpPost]
//         //[ActionName("submitloanrequest")]
//         //public object SubmitLoanRequest([FromBody] OAuth.Request _OAuthRequest, long productId, long loanRequestId)
//         //{
//         //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//         //    OResponse _Response = _evolveOperation.SubmitLoanRequest(productId,loanRequestId, _Request.UserReference);
//         //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
//         //}

//         //[HttpPost]
//         //[ActionName("adduserbankdetails")]
//         //public object AddUserbankDetails([FromBody] OAuth.Request _OAuthRequest, long productId, long loanRequestId)
//         //{
//         //    BankDetails _Request = JsonConvert.DeserializeObject<BankDetails>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//         //    OResponse _Response = _evolveOperation.AddUserbankDetails(_Request, productId,loanRequestId);
//         //    return HCoreAuth.Auth_Response(_Response, _Request.userReference);
//         //}

//         //[HttpPost]
//         //[ActionName("approveloanrequest")]
//         //public object ApproveLoanRequest([FromBody] OAuth.Request _OAuthRequest, long productId,string action, long loanRequestId)
//         //{
//         //    ApproveLoanRequest _Request = JsonConvert.DeserializeObject<ApproveLoanRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//         //    OResponse _Response = _evolveOperation.ApproveLoanRequest(_Request, productId,action,loanRequestId);
//         //    return HCoreAuth.Auth_Response(_Response, _Request.userReference);
//         //}

//         /// <summary>
//         /// Description: Sets up debit mandate.
//         /// </summary>
//         /// <param name="_OAuthRequest">The o authentication request.</param>
//         /// <param name="productId">The product identifier.</param>
//         /// <param name="loanRequestId">The loan request identifier.</param>
//         /// <returns>System.Object.</returns>
//         [HttpPost]
//         [ActionName("setupdebitmandate")]
//         public object SetUpDebitMandate([FromBody] OAuth.Request _OAuthRequest, long productId, long loanRequestId)
//         {
//             OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//             OResponse _Response = _evolveOperation.SetUpDebitMandate(productId,loanRequestId, _Request.UserReference);
//             return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
//         }

//         //[HttpPost]
//         //[ActionName("requestdebitmandateotp")]
//         //public object RequestDebitMandateOTP([FromBody] OAuth.Request _OAuthRequest, long productId, long loanRequestId)
//         //{
//         //    ApproveLoanRequest _Request = JsonConvert.DeserializeObject<ApproveLoanRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//         //    OResponse _Response = _evolveOperation.RequestDebitMandateOTP(productId,loanRequestId, _Request.userReference);
//         //    return HCoreAuth.Auth_Response(_Response, _Request.userReference);
//         //}

//         /// <summary>
//         /// Description: Validates the otp.
//         /// </summary>
//         /// <param name="_OAuthRequest">The o authentication request.</param>
//         /// <returns>System.Object.</returns>
//         [HttpPost]
//         [ActionName("validateotp")]
//         public object ValidateOTP([FromBody] OAuth.Request _OAuthRequest)
//         {
//             OTPValidationRequest _Request = JsonConvert.DeserializeObject<OTPValidationRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//             OResponse _Response = _evolveOperation.ValidateOTP(_Request);
//             return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
//         }

//         //[HttpPost]
//         //[ActionName("validateotpcore")]
//         //public object ValidateOTPCore([FromBody] OAuth.Request _OAuthRequest, long LoanRequestId)
//         //{
//         //    OTPValidationRequest _Request = JsonConvert.DeserializeObject<OTPValidationRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//         //    OResponse _Response = _evolveOperation.ValidateOTPCore(_Request, LoanRequestId);
//         //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
//         //}

//         //[HttpPost]
//         //[ActionName("getdebitmandate")]
//         //public object GetDebitMandate([FromBody] OAuth.Request _OAuthRequest, long productId, long loanRequestId)
//         //{
//         //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//         //    OResponse _Response = _evolveOperation.GetDebitMandate(productId,loanRequestId, _Request.UserReference);
//         //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
//         //}

//         /// <summary>
//         /// Description: Gets the loan repayment structure.
//         /// </summary>
//         /// <param name="_OAuthRequest">The o authentication request.</param>
//         /// <param name="productId">The product identifier.</param>
//         /// <param name="loanRequestId">The loan request identifier.</param>
//         /// <returns>System.Object.</returns>
//         [HttpPost]
//         [ActionName("getloanrepaymentstructure")]
//         public object GetLoanRepaymentStructure([FromBody] OAuth.Request _OAuthRequest,long productId, long loanRequestId)
//         {
//             OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//             OResponse _Response = _evolveOperation.GetLoanRepaymentStructure(productId,loanRequestId, _Request.UserReference);
//             return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
//         }

//         /// <summary>
//         /// Description: Adds the merchantbank details.
//         /// </summary>
//         /// <param name="_OAuthRequest">The o authentication request.</param>
//         /// <param name="productId">The product identifier.</param>
//         /// <param name="loanRequestId">The loan request identifier.</param>
//         /// <returns>System.Object.</returns>
//         [HttpPost]
//         [ActionName("addmerchantbankdetails")]
//         public object AddMerchantbankDetails([FromBody] OAuth.Request _OAuthRequest, long productId, long loanRequestId)
//         {
//             BankDetails _Request = JsonConvert.DeserializeObject<BankDetails>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//             OResponse _Response = _evolveOperation.AddMerchantbankDetails(_Request, productId,loanRequestId);
//             return HCoreAuth.Auth_Response(_Response, _Request.userReference);
//         }

//         //[HttpPost]
//         //[ActionName("getallrepayments")]
//         //public object GetAllRepayments([FromBody] OAuth.Request _OAuthRequest)
//         //{
//         //    GetAllRepaymentsRequest _Request = JsonConvert.DeserializeObject<GetAllRepaymentsRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//         //    OResponse _Response = _evolveOperation.GetAllRepayments(_Request);
//         //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
//         //}



//         /// <summary>
//         /// Description: Checks the mono identifier.
//         /// </summary>
//         /// <param name="_OAuthRequest">The o authentication request.</param>
//         /// <returns>System.Object.</returns>
//         [HttpPost]
//         [ActionName("checkmonoid")]
//         public object CheckMonoId([FromBody] OAuth.Request _OAuthRequest)
//         {
//             CreateUserRequest _Request = JsonConvert.DeserializeObject<CreateUserRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//             OResponse _Response = _evolveOperation.CheckMonoId(_Request);
//             return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
//         }

        
//     }
// }
