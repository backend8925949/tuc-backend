//==================================================================================
// FileName: BNPLLoanOperationController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for loan operation functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Plugins.Loan;
using HCore.TUC.Plugins.Loan.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Plugins.Operations
{
    [Produces("application/json")]
    [Route("api/v3/plugins/loan/[action]")]
    public class BNPLLoanOperationController
    {       
        ManageLoanOperation _ManageLoanOperation;


        /// <summary>
        /// Description: Gets the loan request.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getloanrequests")]
        public object GetLoanRequest([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetLoanRequest(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets all loan list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getallloans")]
        public object GetAllLoans([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetAllLoans(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the repayment list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getrepaymentlist")]
        public object GetRepayments([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetRepayments(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the loan request overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getloanrequestoverview")]
        public object GetLoanRequestOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OSummaryOverview.Request _Request = JsonConvert.DeserializeObject<OSummaryOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetLoanRequestOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the repayments overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getrepaymentsoverview")]
        public object GetRepaymentsOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OSummaryOverview.Request _Request = JsonConvert.DeserializeObject<OSummaryOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetRepaymentsOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets all loans overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getallloansoverview")]
        public object GetAllLoansOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OSummaryOverview.Request _Request = JsonConvert.DeserializeObject<OSummaryOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetAllLoansOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the loan details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getloandetails")]
        public object GetLoanDetails([FromBody] OAuth.Request _OAuthRequest)
        {
            OSummaryOverview.Request _Request = JsonConvert.DeserializeObject<OSummaryOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetLoanDetails(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the loan activity.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getloanactivity")]
        public object GetLoanActivity([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetLoanActivity(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the reconcilation report.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getreconcilationreport")]
        public object GetReconcilationReport([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetReconcilationReport(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the reconcilation report overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getreconcilationreportoverview")]
        public object GetReconcilationReportOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OSummaryOverview.Request _Request = JsonConvert.DeserializeObject<OSummaryOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetReconcilationReportOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the BNPL overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getbnploverview")]
        public object GetBNPLOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OSummaryOverview.Request _Request = JsonConvert.DeserializeObject<OSummaryOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetBNPLOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the disbursment distribution overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdisbursmentdistributionview")]
        public object GetDisbursmentDistributionOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OSummaryOverview.Request _Request = JsonConvert.DeserializeObject<OSummaryOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetDisbursmentDistributionOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the repayment status overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getrepaymentstatusoverview")]
        public object GetRepaymentStatusOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OSummaryOverview.Request _Request = JsonConvert.DeserializeObject<OSummaryOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetRepaymentStatusOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the loan disbursement.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getloandisbursement")]
        public object GetLoanDisbursement([FromBody] OAuth.Request _OAuthRequest)
        {
            OSummaryOverview.Request _Request = JsonConvert.DeserializeObject<OSummaryOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetLoanDisbursement(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the disbursement by month.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdisbursementbymonth")]
        public object GetDisbursementByMonth([FromBody] OAuth.Request _OAuthRequest)
        {
            OSummaryOverview.Request _Request = JsonConvert.DeserializeObject<OSummaryOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetDisbursementByMonth(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the account loan history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountloanhistory")]
        public object GetAccountLoanHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetAccountLoanHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the account repayments.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaccountrepayments")]
        public object GetAccountRepayments([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetAccountRepayments(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the loan providers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getloanproviders")]
        public object GetLoanProviders([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageLoanOperation = new ManageLoanOperation();
            OResponse _Response = _ManageLoanOperation.GetLoanProviders(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}