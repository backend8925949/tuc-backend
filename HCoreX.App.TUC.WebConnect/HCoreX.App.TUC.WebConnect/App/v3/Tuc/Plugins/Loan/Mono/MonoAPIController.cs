////==================================================================================
//// FileName: MonoAPIController.cs
//// Author : Harshal Gandole
//// Created On : 
//// Description : Class defined for Mono API functionality.
////
//// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
////
//// Revision History:
//// Date             : Changed By        : Comments
//// ---------------------------------------------------------------------------------
////                 | Harshal Gandole   : Created New File
//// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
////
////==================================================================================

//using HCore.Helper;
//using HCore.Integration.Mono;
//using HCore.Integration.Mono.Requests;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Mono;
//using Newtonsoft.Json;

//namespace HCore.Api.App.v3.Tuc.Mono
//{
//    [Produces("application/json")]
//    [Route("api/v3/mono/mono/[action]")]
//    [ApiController]
//    public class MonoAPIController : ControllerBase
//    {        
//        AccountAuthentication _Account = new AccountAuthentication();
//        //public MonoAPIController()
//        //{
//        //    _Account.SetEndPoints("https://api.withmono.com", "test_sk_vaL0x1KR1kE0ZMuyKTuG");
//        //}

//        /// <summary>
//        /// Description: Gets the authentication.
//        /// </summary>
//        /// <param name="_OAuthRequest">The o authentication request.</param>
//        /// <returns>System.Object.</returns>
//        [HttpPost]
//        [ActionName("authentication")]
//        public object GetAuth([FromBody] OAuth.Request _OAuthRequest)
//        {
//            OMono.AuthRequest _Request = JsonConvert.DeserializeObject<OMono.AuthRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//            OResponse _Response = _Account.Authentication(_Request);
//            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
//        }

//        /// <summary>
//        /// Description: Connects the mono.
//        /// </summary>
//        /// <param name="_OAuthRequest">The o authentication request.</param>
//        /// <returns>System.Object.</returns>
//        [HttpPost]
//        [ActionName("connectmono")]
//        public object ConnectMono([FromBody] OAuth.Request _OAuthRequest)
//        {
//            OMono.AuthRequest _Request = JsonConvert.DeserializeObject<OMono.AuthRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//            OResponse _Response = _Account.HCoreXMonoAuth(_Request);
//            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
//        }

//        /// <summary>
//        /// Description: Gets the account.
//        /// </summary>
//        /// <param name="_OAuthRequest">The o authentication request.</param>
//        /// <returns>System.Object.</returns>
//        [HttpPost]
//        [ActionName("getaccount")]
//        public object GetAccount([FromBody] OAuth.Request _OAuthRequest)
//        {
//            OMono.AccountRequest _Request = JsonConvert.DeserializeObject<OMono.AccountRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//            OResponse _Response = _Account.GetAccount(_Request);
//            return HCoreAuth.Auth_Response(_Response, _Request.userReference);
//        }

//        //[HttpPost]
//        //[ActionName("getidentity")]
//        //public object GetUserIdentityCore([FromBody] OAuth.Request _OAuthRequest)
//        //{
//        //    GetUserIdentityRequest.IdentityRequest _Request = JsonConvert.DeserializeObject<GetUserIdentityRequest.IdentityRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//        //    OResponse _Response = _Account.GetUserIdentityCore(_Request);
//        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
//        //}

//        /// <summary>
//        /// Description: Gets the account statement.
//        /// </summary>
//        /// <param name="_OAuthRequest">The o authentication request.</param>
//        /// <returns>System.Object.</returns>
//        [HttpPost]
//        [ActionName("getaccountstatement")]
//        public object GetAccountStatement([FromBody] OAuth.Request _OAuthRequest)
//        {
//            OMono.AccountRequest _Request = JsonConvert.DeserializeObject<OMono.AccountRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//            OResponse _Response = _Account.GetAccountStatement(_Request);
//            return HCoreAuth.Auth_Response(_Response, _Request.userReference);
//        }

//        /// <summary>
//        /// Description: Incomes the specified o authentication request.
//        /// </summary>
//        /// <param name="_OAuthRequest">The o authentication request.</param>
//        /// <returns>System.Object.</returns>
//        [HttpPost]
//        [ActionName("getincome")]
//        public object Income([FromBody] OAuth.Request _OAuthRequest)
//        {
//            OMono.AccountRequest _Request = JsonConvert.DeserializeObject<OMono.AccountRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
//            OResponse _Response = _Account.Income(_Request);
//            return HCoreAuth.Auth_Response(_Response, _Request.userReference);
//        }
//    }
//}
