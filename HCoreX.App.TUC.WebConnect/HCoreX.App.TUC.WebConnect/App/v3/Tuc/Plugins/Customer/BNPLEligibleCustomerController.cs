//==================================================================================
// FileName: BNPLEligibleCustomerController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for bnpl eligible customer functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Plugins.Loan;
using HCore.TUC.Plugins.Loan.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Plugins.Customer
{
    [Produces("application/json")]
    [Route("api/v3/plugins/customer/[action]")]
    //[ApiController]
    public class BNPLEligibleCustomerController : ControllerBase
    {
        ManageEligibleCustomer _ManageEligibleCustomer;


        /// <summary>
        /// Description: Gets the loan request.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("geteligiblecustomerlist")]
        public object GetLoanRequest([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageEligibleCustomer = new ManageEligibleCustomer();
            OResponse _Response = _ManageEligibleCustomer.GetEligibleCustomer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the eligible customer overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("geteligiblecustomeroverview")]
        public object GetEligibleCustomerOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OSummaryOverview.Request _Request = JsonConvert.DeserializeObject<OSummaryOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageEligibleCustomer = new ManageEligibleCustomer();
            OResponse _Response = _ManageEligibleCustomer.GetEligibleCustomerOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the eligible customers overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("geteligiblecustomersoverview")]
        public object GetEligibleCustomersOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageEligibleCustomer = new ManageEligibleCustomer();
            OResponse _Response = _ManageEligibleCustomer.GetEligibleCustomersOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
