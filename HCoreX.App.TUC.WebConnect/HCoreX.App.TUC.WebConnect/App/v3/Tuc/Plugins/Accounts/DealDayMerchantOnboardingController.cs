﻿using System;
using HCore.Helper;
using Microsoft.AspNetCore.Mvc;
using HCore.TUC.Plugins.Accounts;
using HCore.TUC.Plugins.Accounts.Objects;
using Newtonsoft.Json;

namespace HCoreX.App.TUC.WebConnect.App.v3.Tuc.Plugins.Accounts
{
	[Produces("application/json")]
	[Route("api/v3/deals/account/[action]")]
	public class DealDayMerchantOnboardingController
	{
		ManageMerchantOnboarding? _ManageMerchantOnboarding;

        [ActionName("onboardmerchantrequest")]
		public async Task<object> OnboardMerchantRequest([FromBody] OAuth.Request _OAuthRequest)
		{
			OAccounts.OnboardingRequest.Request _Request = JsonConvert.DeserializeObject<OAccounts.OnboardingRequest.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
			_ManageMerchantOnboarding = new ManageMerchantOnboarding();
			OResponse _Response = await _ManageMerchantOnboarding.OnboardMerchantRequest(_Request);
			return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [ActionName("updatemerchantdetails")]
        public async Task<object> UpdateMerchantDetails([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.UpdateMerchantDetails.Request _Request = JsonConvert.DeserializeObject<OAccounts.UpdateMerchantDetails.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new ManageMerchantOnboarding();
            OResponse _Response = await _ManageMerchantOnboarding.UpdateMerchantDetails(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [ActionName("forgotpassword")]
        public async Task<object> ForgotPassword([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.ForgotPassword.Request _Request = JsonConvert.DeserializeObject<OAccounts.ForgotPassword.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new ManageMerchantOnboarding();
            OResponse _Response = await _ManageMerchantOnboarding.ForgotPassword(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [ActionName("verifyemail")]
        public async Task<object> VerifyEmail([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.VerifyEmailRequest.Request _Request = JsonConvert.DeserializeObject<OAccounts.VerifyEmailRequest.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new ManageMerchantOnboarding();
            OResponse _Response = await _ManageMerchantOnboarding.VerifyEmail(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [ActionName("resendemailotp")]
        public async Task<object> ResendEmailOtp([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.ResendEmaiLOtpRequest.Request _Request = JsonConvert.DeserializeObject<OAccounts.ResendEmaiLOtpRequest.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new ManageMerchantOnboarding();
            OResponse _Response = await _ManageMerchantOnboarding.ResendEmailOtp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [ActionName("updatepassword")]
        public async Task<object> UpdatePassword([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.UpdatePasswordRequest.Request _Request = JsonConvert.DeserializeObject<OAccounts.UpdatePasswordRequest.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new ManageMerchantOnboarding();
            OResponse _Response = await _ManageMerchantOnboarding.UpdatePassword(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [ActionName("requesttwofactorauth")]
        public async Task<object> TwoFactorAuthentication([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.TwoFactorAuthRequest.Request _Request = JsonConvert.DeserializeObject<OAccounts.TwoFactorAuthRequest.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new ManageMerchantOnboarding();
            OResponse _Response = await _ManageMerchantOnboarding.TwoFactorAuthentication(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [ActionName("verifytwofactorauthotp")]
        public async Task<object> VerifyTwoFactorAuthentication([FromBody] OAuth.Request _OAuthRequest)
        {
            OAccounts.VerifyTwoFactorAuthRequest.Request _Request = JsonConvert.DeserializeObject<OAccounts.VerifyTwoFactorAuthRequest.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageMerchantOnboarding = new ManageMerchantOnboarding();
            OResponse _Response = await _ManageMerchantOnboarding.VerifyTwoFactorAuthentication(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}

