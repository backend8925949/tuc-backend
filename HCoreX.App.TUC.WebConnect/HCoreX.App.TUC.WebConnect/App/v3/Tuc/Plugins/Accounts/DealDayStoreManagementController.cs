﻿using System;
using HCore.Helper;
using Microsoft.AspNetCore.Mvc;
using HCore.TUC.Plugins.Accounts;
using HCore.TUC.Plugins.Accounts.Objects;
using Newtonsoft.Json;

namespace HCoreX.App.TUC.WebConnect.App.v3.Tuc.Plugins.Accounts
{
    [Produces("application/json")]
    [Route("api/v3/deals/store/[action]")]
	public class DealDayStoreManagementController
	{
        ManageStoreManagement? _ManageStoreManagement;

        [ActionName("savestore")]
        public async Task<object> SaveStore([FromBody] OAuth.Request _OAuthRequest)
        {
            OStoreManagement.CreateStore.Request? _Request = JsonConvert.DeserializeObject<OStoreManagement.CreateStore.Request?>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStoreManagement = new ManageStoreManagement();
            OResponse _Response = await _ManageStoreManagement.SaveStore(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [ActionName("updatestore")]
        public async Task<object> UpdateStore([FromBody] OAuth.Request _OAuthRequest)
        {
            OStoreManagement.UpdateStore.Request? _Request = JsonConvert.DeserializeObject<OStoreManagement.UpdateStore.Request?>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStoreManagement = new ManageStoreManagement();
            OResponse _Response = await _ManageStoreManagement.UpdateStore(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [ActionName("updatestorestatus")]
        public async Task<object> UpdateStoreStatus([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference? _Request = JsonConvert.DeserializeObject<OReference?>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStoreManagement = new ManageStoreManagement();
            OResponse _Response = await _ManageStoreManagement.UpdateStoreStatus(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [ActionName("deletestore")]
        public async Task<object> DeleteStore([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference? _Request = JsonConvert.DeserializeObject<OReference?>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStoreManagement = new ManageStoreManagement();
            OResponse _Response = await _ManageStoreManagement.DeleteStore(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [ActionName("getstore")]
        public async Task<object> GetStore([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference? _Request = JsonConvert.DeserializeObject<OReference?>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStoreManagement = new ManageStoreManagement();
            OResponse _Response = await _ManageStoreManagement.GetStore(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [ActionName("getstores")]
        public async Task<object> GetStores([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request?>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStoreManagement = new ManageStoreManagement();
            OResponse _Response = await _ManageStoreManagement.GetStores(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}

