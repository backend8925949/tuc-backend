//==================================================================================
// FileName: TUCOperationsController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for operations functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================


using HCore.Helper;
using HCore.TUC.Plugins.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Plugins.Operations
{
    [Produces("application/json")]
    [Route("api/v3/plugins/operations/[action]")]
    public class TUCOperationsController
    {
        ManageWallet _ManageWallet;
        /// <summary>
        /// Description: Gets the wallet balance.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getbalance")]
        public object GetWalletBalance([FromBody] OAuth.Request _OAuthRequest)
        {
            OOperations.Wallet.Balance.Request _Request = JsonConvert.DeserializeObject<OOperations.Wallet.Balance.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageWallet = new ManageWallet();
            OResponse _Response = _ManageWallet.GetWalletBalance(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Credits the wallet.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("creditaccount")]
        public object CreditWallet([FromBody] OAuth.Request _OAuthRequest)
        {
            OOperations.Wallet.Credit.Request _Request = JsonConvert.DeserializeObject<OOperations.Wallet.Credit.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageWallet = new ManageWallet();
            OResponse _Response = _ManageWallet.CreditWallet(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the wallet credit history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcredithistory")]
        public object GetWalletCreditHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageWallet = new ManageWallet();
            OResponse _Response = _ManageWallet.GetWalletCreditHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
