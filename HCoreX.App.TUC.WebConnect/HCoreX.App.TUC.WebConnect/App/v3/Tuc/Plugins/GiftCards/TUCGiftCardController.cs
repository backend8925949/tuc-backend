//==================================================================================
// FileName: TUCGiftCardController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for giftcard functionality
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================


using HCore.Helper;
using HCore.TUC.Plugins.Operations;
using HCore.TUC.Plugins.GiftCards;
using HCore.TUC.Plugins.GiftCards.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IO;

namespace HCore.Api.App.v3.Tuc.Plugins.GiftCards
{
    [Produces("application/json")]
    [Route("api/v3/plugins/gc/[action]")]
    public class TUCGiftCardController
    {
        ManageGiftCard _ManageGiftCard;
        /// <summary>
        /// Description: Method to create quick gift cards
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("createquickgiftcard")]

        public object GetWalletBalance([FromBody] OAuth.Request _OAuthRequest)
        {
            OGiftCard.QuickCard _Request = JsonConvert.DeserializeObject<OGiftCard.QuickCard>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.CreateGiftCard(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to create gift cards
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("creategiftcard")]
        public object SaveGiftCard([FromBody] OAuth.Request _OAuthRequest)
        {
            OGiftCard.CreateGiftCard _Request = JsonConvert.DeserializeObject<OGiftCard.CreateGiftCard>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.SaveGiftCard(_Request);
            return HCoreAuth.Auth_Response(_Response);
        }
        /// <summary>
        /// Description: Method to get giftcard overview
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getgiftcardoverview")]
        public object GetGiftCardOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetGiftCardOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method to all giftcard list
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getgiftcards")]
        public object GetGiftCards([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetGiftCards(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method to purchase history of giftcards
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getgiftcardssaleshistory")]
        public object GetGiftCardPurchaseHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetGiftCardPurchaseHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Method to redeem history of giftcards
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getgiftcardsredeemhistory")]
        public object GetGiftCardRedeemHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetGiftCardRedeemHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Author:Priya Chavadiya
        /// Description: Method to all giftcard overview for console panel
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("gethomegiftcardoverview")]
        public object GetHomeGiftCardOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetHomeGiftCardOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Author:Priya Chavadiya
        /// Description: Method to all giftcard purchase history for console panel
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("gethomegiftcardssaleshistory")]
        public object GetHomeGiftCardPurchaseHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OAnalytics.Request _Request = JsonConvert.DeserializeObject<OAnalytics.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetHomeGiftCardPurchaseHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to check customer balance from thankucash website.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getappuserbalance")]
        public object GetBalance([FromBody] OAuth.Request _OAuthRequest)
        {
            OGiftCard.Balance.Request _Request = JsonConvert.DeserializeObject<OGiftCard.Balance.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetBalance(_Request);
            return HCoreAuth.Auth_Response(_Response);
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get all the giftcard transaction details according to sender.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("gettransactions")]
        public object GetTransactions([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetTransactions(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get all issued giftcards history.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getgiftcardshistory")]
        public object GetGiftCardsHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetGiftCardsHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get list of redeemed giftcards.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getredeemhistory")]
        public object GetRedeemHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetRedeemHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get overview of giftcard transaction details according to sender.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("gettransactionsoverview")]
        public object GetTransactionOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetTransactionsOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get overivew of issued giftcards history.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getgiftcardshistoryoverview")]
        public object GetGiftCardsHistoryOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetGiftCardsHistoryOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get overview of redeemed giftcards.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getredeemhistoryoverview")]
        public object GetRedeemHistoryOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetRedeemHistoryOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get commission for issuing giftcards aacording to user.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("savegiftcarduserdrafts")]
        public object SaveGiftCardUserDrafts([FromBody] OAuth.Request _OAuthRequest)
        {
            OGiftCard.CreateGiftCard _Request = JsonConvert.DeserializeObject<OGiftCard.CreateGiftCard>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.SaveGiftCardUserDrafts(_Request);
            return HCoreAuth.Auth_Response(_Response);
        }

        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get commission for issuing giftcards aacording to user.
        /// </summary>
        /// <param name="_OAuthRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getbalance")]
        public object GetGiftCardBalance([FromBody] OAuth.Request _OAuthRequest)
        {
            OOperations.Wallet.Balance.Request _Request = JsonConvert.DeserializeObject<OOperations.Wallet.Balance.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageGiftCard = new ManageGiftCard();
            OResponse _Response = _ManageGiftCard.GetWalletBalance(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }


    }
}
