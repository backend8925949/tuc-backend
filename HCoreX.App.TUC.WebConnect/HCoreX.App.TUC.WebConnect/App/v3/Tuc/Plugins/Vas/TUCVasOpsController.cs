//==================================================================================
// FileName: TUCVasOpsController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for Vas Ops Functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Plugins.Vas;
using HCore.TUC.Plugins.Vas.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HCore.Api.App.v3.Tuc.Plugins.Vas
{
    [Produces("application/json")]
    [Route("api/v3/plugins/vasops/[action]")]
    public class TUCVasOpsController
    {
        ManageVasOps _ManageVasOps;
        /// <summary>
        /// Description: Gets the vas options.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getvasoptions")]
        public object GetVasOptions([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVasOps = new ManageVasOps();
            OResponse _Response = _ManageVasOps.GetVasOptions(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Vas payment initialize.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("vaspay_initialize")]
        public object VasPayment_Initialize([FromBody] OAuth.Request _OAuthRequest)
        {
            OVasOp.Purchase.Initialize.Request _Request = JsonConvert.DeserializeObject<OVasOp.Purchase.Initialize.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVasOps = new ManageVasOps();
            OResponse _Response = _ManageVasOps.VasPayment_Initialize(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Vas payment confirm.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("vaspay_confirm")]
        public object VasPayment_Confirm([FromBody] OAuth.Request _OAuthRequest)
        {
            OVasOp.Purchase.Confirm.Request _Request = JsonConvert.DeserializeObject<OVasOp.Purchase.Confirm.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVasOps = new ManageVasOps();
            OResponse _Response = _ManageVasOps.VasPayment_Confirm(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Vas payment cancel.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("vaspay_cancel")]
        public object VasPayment_Cancel([FromBody] OAuth.Request _OAuthRequest)
        {
            OVasOp.Purchase.Cancel.Request _Request = JsonConvert.DeserializeObject<OVasOp.Purchase.Cancel.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVasOps = new ManageVasOps();
            OResponse _Response = _ManageVasOps.VasPayment_Cancel(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
       
        ManageVas _ManageVas;
        /// <summary>
        /// Description: Gets the vas product purchase customer.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getvaspurchasehistory")]
        public object GetVasProductPurchaseCustomer([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageVas = new ManageVas();
            OResponse _Response = _ManageVas.GetVasProductPurchaseCustomer(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
