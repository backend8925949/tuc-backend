//==================================================================================
// FileName: LSOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for LS Operations functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Objects;
using HCore.TUC.Plugins.Delivery.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using static HCore.TUC.Plugins.Delivery.Objects.OOperations.Pricing;

namespace HCore.Api.App.v3.Tuc.Plugins.Delivery
{
    [Produces("application/json")]
    [Route("api/v3/plugins/lsops/[action]")]
    public class LSOperations : Controller
    {
        ManageOperations _ManageOperations;
        ManageShipment _ManageShipment;
        /// <summary>
        /// Description: Gets the pricing.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpricing")]
        //public object GetPricing([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    OOperations.Pricing.Request _Request = JsonConvert.DeserializeObject<OOperations.Pricing.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageOperations = new ManageOperations();
        //    OResponse _Response = _ManageOperations.GetPricing(_Request);
        //    return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        //}
        public async Task<object> GetDeliveryPricing([FromBody] OAuth.Request _OAuthRequest)
        {
            GetDeliveryPricing _Request = JsonConvert.DeserializeObject<GetDeliveryPricing>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = await _ManageOperations.GetDeliveryPricing(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the estimated pricing.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getestimatedpricing")]
        public object GetEstimatedPricing([FromBody] OAuth.Request _OAuthRequest)
        {
            OOperations.Pricing.Request _Request = JsonConvert.DeserializeObject<OOperations.Pricing.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.GetEstimatedPricing(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        /// <summary>
        /// Description: Gets the shipment pricing.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getdeliverypricing")]
        public object GetShipmentPricing([FromBody] OAuth.Request _OAuthRequest)
        {
            OOperations.Pricing.Request _Request = JsonConvert.DeserializeObject<OOperations.Pricing.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOperations = new ManageOperations();
            OResponse _Response = _ManageOperations.GetShipmentPricing(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the shipment.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getshipment")]
        public object GetShipment([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageShipment = new ManageShipment();
            OResponse _Response = _ManageShipment.GetShipment(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

    }
}
