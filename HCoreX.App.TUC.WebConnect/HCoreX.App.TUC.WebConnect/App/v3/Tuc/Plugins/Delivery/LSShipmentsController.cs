//==================================================================================
// FileName: LSShipmentsController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for shippment functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Objects;
using HCore.TUC.Plugins.Delivery.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Plugins.Delivery
{
    [Produces("application/json")]
    [Route("api/v3/plugins/lsshipment/[action]")]
    public class LSShipmentsController : Controller
    {
        ManageShipment? _ManageShipment;
        ManageOrderProcess? _ManageOrderProcess;

        /// <summary>
        /// Description: Gets the rate details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getrate")]
        public object GetRateDetails([FromBody] OAuth.Request _OAuthRequest)
        {
            OShipments.CheckRateDetails? _Request = JsonConvert.DeserializeObject<OShipments.CheckRateDetails>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageShipment = new ManageShipment();
            OResponse _Response = _ManageShipment.GetRateDetails(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Tracks the shipment.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("trackorder")]
        public async Task<object> TrackOrder([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference? _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageShipment = new ManageShipment();
            OResponse? _Response = await _ManageShipment.TrackOrder(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Cancels the shipment.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("cancelshipment")]
        public async Task<object> CancelShipment([FromBody] OAuth.Request _OAuthRequest)
        {
            //OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            //_ManageShipment = new ManageShipment();
            //OResponse _Response = _ManageShipment.CancelShipment(_Request);
            OOrderProcess.CancelOrder? _Request = JsonConvert.DeserializeObject<OOrderProcess.CancelOrder>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrderProcess = new ManageOrderProcess();
            OResponse _Response = await _ManageOrderProcess.CancelOrder(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the shipment.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getshipment")]
        public object GetShipment([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference? _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageShipment = new ManageShipment();
            OResponse _Response = _ManageShipment.GetShipment(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the shipment list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getshipments")]
        public object GetShipments([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageShipment = new ManageShipment();
            OResponse _Response = _ManageShipment.GetShipments(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the orders overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getordersovrview")]
        public object GetOrdersOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference? _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageShipment = new ManageShipment();
            OResponse _Response = _ManageShipment.GetOrdersOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getoverview")]
        public object GetOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference? _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageShipment = new ManageShipment();
            OResponse _Response = _ManageShipment.GetOverview(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Updates the order status.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("confirmorder")]
        public async Task<object> ConfirmOrder([FromBody] OAuth.Request _OAuthRequest)
        {
            //OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            OOrderProcess.ConfirmOrder? _Request = JsonConvert.DeserializeObject<OOrderProcess.ConfirmOrder>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            //_ManageShipment = new ManageShipment();
            //OResponse _Response = _ManageShipment.UpdateOrderStatus(_Request);
            _ManageOrderProcess = new ManageOrderProcess();
            OResponse _Response = await _ManageOrderProcess.ConfirmOrder(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the order history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getorderhistory")]
        public object GetOrderHistory([FromBody] OAuth.Request _OAuthRequest)
        {
            OShipments.Request? _Request = JsonConvert.DeserializeObject<OShipments.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageShipment = new ManageShipment();
            OResponse _Response = _ManageShipment.GetOrderHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the order list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getorders")]
        public object GetOrders([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request? _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageShipment = new ManageShipment();
            OResponse _Response = _ManageShipment.GetOrders(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("updateorderstatus")]
        public async Task<object> UpdateOrderStatus([FromBody] OAuth.Request _OAuthRequest)
        {
            OOrderProcess.ReadyToPickup? _Request = JsonConvert.DeserializeObject<OOrderProcess.ReadyToPickup>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrderProcess = new ManageOrderProcess();
            OResponse _Response = await _ManageOrderProcess.UpdateOrderStatus(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("getdelvieryinvoice")]
        public async Task<object> GetDelvieryInvoice([FromBody] OAuth.Request _OAuthRequest)
        {
            OOrderProcess.Invoice.Request? _Request = JsonConvert.DeserializeObject<OOrderProcess.Invoice.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageOrderProcess = new ManageOrderProcess();
            OResponse _Response = await _ManageOrderProcess.GetDelvieryInvoice(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}



