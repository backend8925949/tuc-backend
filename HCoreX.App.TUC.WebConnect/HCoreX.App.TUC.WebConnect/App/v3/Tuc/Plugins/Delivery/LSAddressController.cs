//==================================================================================
// FileName: LSAddressController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for LS Address functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Objects;
using HCore.TUC.Plugins.Delivery.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HCore.Api.App.v3.Tuc.Plugins.Delivery
{
    [Produces("application/json")]
    [Route("api/v3/plugins/lsaddress/[action]")]
    public class LSAddressController : Controller
    {
        ManageAddress _ManageCarriers;

        /// <summary>
        /// Description: Saves the address.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("saveaddress")]
        public object SaveAddress([FromBody] OAuth.Request _OAuthRequest)
        {
            ODeliveryAddress.Save.Request _Request = JsonConvert.DeserializeObject<ODeliveryAddress.Save.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCarriers = new ManageAddress();
            OResponse _Response = _ManageCarriers.SaveAddress(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Updates the address.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updateaddress")]
        public object UpdateAddress([FromBody] OAuth.Request _OAuthRequest)
        {
            ODeliveryAddress.Update.Request _Request = JsonConvert.DeserializeObject<ODeliveryAddress.Update.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCarriers = new ManageAddress();
            OResponse _Response = _ManageCarriers.UpdateAddress(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the address.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaddress")]
        public object GetAddress([FromBody] OAuth.Request _OAuthRequest)
        {
            OReference _Request = JsonConvert.DeserializeObject<OReference>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCarriers = new ManageAddress();
            OResponse _Response = _ManageCarriers.GetAddress(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the address list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getaddresses")]
        public object GetAddresses([FromBody] OAuth.Request _OAuthRequest)
        {
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCarriers = new ManageAddress();
            OResponse _Response = _ManageCarriers.GetAddresses(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}

