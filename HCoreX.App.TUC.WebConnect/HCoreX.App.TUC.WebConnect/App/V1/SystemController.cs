//==================================================================================
// FileName: SystemController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class defined for system functionality.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.Object;
using HCore.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using static HCore.CoreConstant;

namespace HCore.Api.App.v1
{

    [Produces("application/json")]
    [Route("api/v1/system/[action]")]
    public class SystemController : Controller
    {
        #region References
        ManageCoreHelper _ManageCoreHelper;
        ManageStorage _ManageStorage;
        ManageSystemVerification _ManageSystemVerification;
        ManageConfiguration _ManageConfiguration;
        ManageUser _ManageUser;
        ManageUserDevice _ManageUserDevice;
        ManageUserAccountAccess _ManageUserAccountAccess;
        ManageUserAccount _ManageUserAccount;
        ManageUserTransaction _ManageUserTransaction;
        #endregion
        #region Launch
        /// <summary>
        /// Description: Logins this instance.
        /// </summary>
        /// <returns>System.String.</returns>
        [HttpGet]
        [ActionName("launch")]
        public string? Login()
        {
            return "running_v0.0.52";
        }
        #endregion


        /// <summary>
        /// return static list of merchants 
        /// Note: API should be open as it will be access by third party
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getdealmerchants")]
        public object GetDealMerchants()
        {
            return _ManageCoreHelper.getdealmerchants(); ;
        }


        /// <summary>
        /// Description: Saves the core helper.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("savecorehelper")]
        public OAuth.Request SaveCoreHelper([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreHelper.Manage _Request = JsonConvert.DeserializeObject<OCoreHelper.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.SaveCoreHelper(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Saves the core common.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("savecorecommon")]
        public OAuth.Request SaveCoreCommon([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreCommon.Manage _Request = JsonConvert.DeserializeObject<OCoreCommon.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.SaveCoreCommon(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Saves the core parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("savecoreparameter")]
        public OAuth.Request SaveCoreParameter([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreParameter.Manage _Request = JsonConvert.DeserializeObject<OCoreParameter.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.SaveCoreParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }



        /// <summary>
        /// Description: Updates the core helper.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updatecorehelper")]
        public OAuth.Request UpdateCoreHelper([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreHelper.Manage _Request = JsonConvert.DeserializeObject<OCoreHelper.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.UpdateCoreHelper(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Description: Updates the core common.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updatecorecommon")]
        public OAuth.Request UpdateCoreCommon([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreCommon.Manage _Request = JsonConvert.DeserializeObject<OCoreCommon.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.UpdateCoreCommon(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Updates the core parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updatecoreparameter")]
        public OAuth.Request UpdateCoreParameter([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreParameter.Manage _Request = JsonConvert.DeserializeObject<OCoreParameter.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.UpdateCoreParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }



        /// <summary>
        /// Description: Deletes the core helper.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("deletecorehelper")]
        public OAuth.Request DeleteCoreHelper([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreHelper.Manage _Request = JsonConvert.DeserializeObject<OCoreHelper.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.DeleteCoreHelper(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Description: Deletes the core common.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("deletecorecommon")]
        public OAuth.Request DeleteCoreCommon([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreCommon.Manage _Request = JsonConvert.DeserializeObject<OCoreCommon.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.DeleteCoreCommon(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Deletes the core parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("deletecoreparameter")]
        public OAuth.Request DeleteCoreParameter([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreParameter.Manage _Request = JsonConvert.DeserializeObject<OCoreParameter.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.DeleteCoreParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }



        /// <summary>
        /// Description: Gets the core helper.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcorehelper")]
        public OAuth.Request GetCoreHelper([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreHelper.Manage _Request = JsonConvert.DeserializeObject<OCoreHelper.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.GetCoreHelper(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Description: Gets the core common.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcorecommon")]
        public OAuth.Request GetCoreCommon([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreCommon.Manage _Request = JsonConvert.DeserializeObject<OCoreCommon.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.GetCoreCommon(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the core parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcoreparameter")]
        public OAuth.Request GetCoreParameter([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreParameter.Manage _Request = JsonConvert.DeserializeObject<OCoreParameter.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.GetCoreParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the core helpers.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcorehelpers")]
        public OAuth.Request GetCoreHelpers([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.GetCoreHelper(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Gets the core commons.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcorecommons")]
        public OAuth.Request GetCoreCommons([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.GetCoreCommon(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Gets the core parameters.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcoreparameters")]
        public OAuth.Request GetCoreParameters([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.GetCoreParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the core usage details.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcoreusagedetails")]
        public OAuth.Request GetCoreUsageDetails([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OCoreUsage.Manage _Request = JsonConvert.DeserializeObject<OCoreUsage.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.GetCoreUsageDetails(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Description: Gets the core usage.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getcoreusage")]
        public OAuth.Request GetCoreUsage([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.GetCoreUsage(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Description: Gets the application configuration.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getappconfiguration")]
        public OAuth.Request GetAppConfiguration([FromBody] OAuth.Request _OAuthRequest)
        {
            OAppConfiguration.Request _Request = JsonConvert.DeserializeObject<OAppConfiguration.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageConfiguration = new ManageConfiguration();
            OResponse _Response = _ManageConfiguration.GetAppConfiguration(_Request);
            return HCoreAuth.Auth_Response(_Response);
        }

        #region System Level Code



        #region Manage Storage
        /// <summary>
        /// Saves the storage.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("savestorage")]
        public OAuth.Request SaveStorage([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OStorage.Save _Request = JsonConvert.DeserializeObject<OStorage.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStorage = new ManageStorage();
            OResponse _Response = _ManageStorage.SaveStorage(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Deletes the storage by reference identifier.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("deletestoragebyreferenceid")]
        public OAuth.Request DeleteStorageByReferenceId([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OStorage.Delete _Request = JsonConvert.DeserializeObject<OStorage.Delete>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStorage = new ManageStorage();
            OResponse _Response = _ManageStorage.DeleteStorageByReferenceId(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Deletes the storage by reference key.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("deletestoragebyreferencekey")]
        public OAuth.Request DeleteStorageByReferenceKey([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OStorage.Delete _Request = JsonConvert.DeserializeObject<OStorage.Delete>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStorage = new ManageStorage();
            OResponse _Response = _ManageStorage.DeleteStorageByReferenceKey(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the storage.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getstorage")]
        public OAuth.Request GetStorage([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OStorage.Details _Request = JsonConvert.DeserializeObject<OStorage.Details>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStorage = new ManageStorage();
            OResponse _Response = _ManageStorage.GetStorage(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the storage by reference identifier.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getstoragebyreferenceid")]
        public OAuth.Request GetStorageByReferenceId([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OStorage.Details _Request = JsonConvert.DeserializeObject<OStorage.Details>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStorage = new ManageStorage();
            OResponse _Response = _ManageStorage.GetStorageByReferenceId(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the storage by reference key.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getstoragebyreferencekey")]
        public OAuth.Request GetStorageByReferenceKey([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OStorage.Details _Request = JsonConvert.DeserializeObject<OStorage.Details>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStorage = new ManageStorage();
            OResponse _Response = _ManageStorage.GetStorageByReferenceKey(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the storage list.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getstoragelist")]
        public OAuth.Request GetStorageList([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageStorage = new ManageStorage();
            OResponse _Response = _ManageStorage.GetStorageList(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        #endregion

        #region Configurations
        /// <summary>
        /// Gets the configuration value.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getconfigurationvalue")]
        public OAuth.Request GetConfigurationValue([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OConfigurationValue.Request _Request = JsonConvert.DeserializeObject<OConfigurationValue.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageConfiguration = new ManageConfiguration();
            OResponse _Response = _ManageConfiguration.GetConfigurationValue(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        #endregion

        #region  Logs
        /// <summary>
        /// Gets the core log.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getlog")]
        public OAuth.Request GetCoreLog([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OCoreLog _Request = JsonConvert.DeserializeObject<OCoreLog>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.GetCoreLog(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Gets the core logs.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getlogs")]
        public OAuth.Request GetCoreLogs([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageCoreHelper = new ManageCoreHelper();
            OResponse _Response = _ManageCoreHelper.GetCoreLog(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        #endregion
        #region Manage System Level Devices
        #region System User Sessions
        /// <summary>
        /// Expires the controller session.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("expiresystemusersession")]
        public OAuth.Request ExpireControllerSession([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserSession _Request = JsonConvert.DeserializeObject<OUserSession>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUser = new ManageUser();
            OResponse _Response = _ManageUser.ExpireUserSession(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Gets the system user sessions.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getsystemusersessions")]
        public OAuth.Request GetSystemUserSessions([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUser = new ManageUser();
            OResponse _Response = _ManageUser.GetUserSessions(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        #endregion
        #region System User  Activity
        /// <summary>
        /// Gets the system user activity.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuseractivity")]
        public OAuth.Request GetSystemUserActivity([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUser = new ManageUser();
            OResponse _Response = _ManageUser.GetUserActivity(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        #endregion

        #region Manage System Verification
        /// <summary>
        /// Expires the verification request.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("expiresystemverificationrequest")]
        public OAuth.Request ExpireVerificationRequest([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OSystemVerificationDetails _Request = JsonConvert.DeserializeObject<OSystemVerificationDetails>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageSystemVerification = new ManageSystemVerification();
            OResponse _Response = _ManageSystemVerification.ExpireVerificationRequest(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the system verification.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getsystemverification")]
        public OAuth.Request GetSystemVerification([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OSystemVerificationDetails _Request = JsonConvert.DeserializeObject<OSystemVerificationDetails>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageSystemVerification = new ManageSystemVerification();
            OResponse _Response = _ManageSystemVerification.GetSystemVerification(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        /// <summary>
        /// Gets the system verifications.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getsystemverifications")]
        public OAuth.Request GetSystemVerifications([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageSystemVerification = new ManageSystemVerification();
            OResponse _Response = _ManageSystemVerification.GetSystemVerifications(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        #endregion

        #region User Login/ Signup 
        /// <summary>
        /// Users the login.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("login")]
        public OAuth.Request UserLogin([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserAccountAccess.ORequest _Request = JsonConvert.DeserializeObject<OUserAccountAccess.ORequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            #endregion
            #region Set User Reference
            _ManageUserAccountAccess = new ManageUserAccountAccess();
            OResponse _Response = _ManageUserAccountAccess.Login(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion

        }
        /// <summary>
        /// Admins the login request.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("sysloginrequest")]
        public OAuth.Request AdminLoginRequest([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserAccountAccess.ORequest _Request = JsonConvert.DeserializeObject<OUserAccountAccess.ORequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            #endregion
            #region Set User Reference
            _ManageUserAccountAccess = new ManageUserAccountAccess();
            OResponse _Response = _ManageUserAccountAccess.AdminLoginRequest(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion

        }

        /// <summary>
        /// Admins the login confirm.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("sysloginconfirm")]
        public OAuth.Request AdminLoginConfirm([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserAccountAccess.ORequest _Request = JsonConvert.DeserializeObject<OUserAccountAccess.ORequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            #endregion
            #region Set User Reference
            _ManageUserAccountAccess = new ManageUserAccountAccess();
            OResponse _Response = _ManageUserAccountAccess.AdminLoginConfirm(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion

        }

        /// <summary>
        /// Logins the application user.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("appusersignin")]
        public OAuth.Request LoginAppUser([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserAccountAccess.AppProcessRequest _Request = JsonConvert.DeserializeObject<OUserAccountAccess.AppProcessRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccountAccess = new ManageUserAccountAccess();
            return HCoreAuth.Auth_Response(_ManageUserAccountAccess.LoginAppUser(_Request));
            #endregion
        }
        /// <summary>
        /// Logins the application user v2.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("appusersigninv2")]
        public OAuth.Request LoginAppUserV2([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserAccountAccess.AppProcessRequest _Request = JsonConvert.DeserializeObject<OUserAccountAccess.AppProcessRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccountAccess = new ManageUserAccountAccess();
            return HCoreAuth.Auth_Response(_ManageUserAccountAccess.LoginAppUserV2(_Request));
            #endregion
        }
        /// <summary>
        /// Logins the application user with pin.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("appusersigninwithpin")]
        public OAuth.Request LoginAppUserWithPin([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserAccountAccess.AppProcessRequest _Request = JsonConvert.DeserializeObject<OUserAccountAccess.AppProcessRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccountAccess = new ManageUserAccountAccess();
            return HCoreAuth.Auth_Response(_ManageUserAccountAccess.LoginAppUserWithPin(_Request));
            #endregion
        }
        /// <summary>
        /// Applications the user register.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("appuserregister")]
        public OAuth.Request AppUserRegister([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserAccountAccess.AppProcessRequest _Request = JsonConvert.DeserializeObject<OUserAccountAccess.AppProcessRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccountAccess = new ManageUserAccountAccess();
            return HCoreAuth.Auth_Response(_ManageUserAccountAccess.AppUserRegister(_Request));
            #endregion
        }

        /// <summary>
        /// Users the register.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [Route(RouteUrl.V1.System + "createadvertiser")]
        [Route(RouteUrl.V1.System + "createmerchant")]
        [Route(RouteUrl.V1.System + "createstore")]
        [Route(RouteUrl.V1.System + "createcashier")]
        [Route(RouteUrl.V1.System + "register")]
        [ActionName("register")]
        public OAuth.Request UserRegister([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserAccountRegister _Request = JsonConvert.DeserializeObject<OUserAccountRegister>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccountAccess = new ManageUserAccountAccess();
            return HCoreAuth.Auth_Response(_ManageUserAccountAccess.Register(_Request));
            #endregion

        }

        /// <summary>
        /// Users the logout.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("logout")]
        public OAuth.Request UserLogout([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserAccountAccess.ORequest _Request = JsonConvert.DeserializeObject<OUserAccountAccess.ORequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccountAccess = new ManageUserAccountAccess();
            OResponse _Response = _ManageUserAccountAccess.Logout(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        #endregion

        #region User Devices
        /// <summary>
        /// Updates the user device.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuserdevice")]
        public OAuth.Request UpdateUserDevice([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserDevice _Request = JsonConvert.DeserializeObject<OUserDevice>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserDevice = new ManageUserDevice();
            OResponse _Response = _ManageUserDevice.UpdateUserDevice(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Deletes the user device.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("deleteuserdevice")]
        public OAuth.Request DeleteUserDevice([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserDevice _Request = JsonConvert.DeserializeObject<OUserDevice>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserDevice = new ManageUserDevice();
            OResponse _Response = _ManageUserDevice.DeleteUserDevice(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Gets the user device.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuserdevice")]
        public OAuth.Request GetUserDevice([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserDevice _Request = JsonConvert.DeserializeObject<OUserDevice>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserDevice = new ManageUserDevice();
            OResponse _Response = _ManageUserDevice.GetUserDevice(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Gets the user devices.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuserdevices")]
        public OAuth.Request GetUserDevices([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserDevice = new ManageUserDevice();
            OResponse _Response = _ManageUserDevice.GetUserDevices(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        #endregion

        #region User Accounts
        /// <summary>
        /// Updates the user account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuseraccount")]
        public OAuth.Request UpdateUserAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserAccount _Request = JsonConvert.DeserializeObject<OUserAccount>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.UpdateUserAccount(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Updates the user account status.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [Route(RouteUrl.V1.System + "updateadvertiserstatus")]
        [Route(RouteUrl.V1.System + "updatemerchantstatus")]
        [Route(RouteUrl.V1.System + "updatestorestatus")]
        [ActionName("updateuseraccountstatus")]
        public OAuth.Request UpdateUserAccountStatus([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserUpdateAccountStatus _Request = JsonConvert.DeserializeObject<OUserUpdateAccountStatus>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.UpdateUserAccountStatus(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        //[HttpPost]
        //[Route(RouteUrl.V1.System + "getadvertiseroverview")]
        //[Route(RouteUrl.V1.System + "getmerchantoverview")]
        //[Route(RouteUrl.V1.System + "getcarduseroverview")]
        //[Route(RouteUrl.V1.System + "getappuseroverview")]
        //[Route(RouteUrl.V1.System + "getcashieroverview")]
        //[Route(RouteUrl.V1.System + "getstoreoverview")]
        //[Route(RouteUrl.V1.System + "getuseraccountoverview")]
        //[ActionName("getuseraccountoverview")]
        //public OAuth.Request GetUserAccountOverview([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Manage Operations
        //    OUserOverview.Request _Request = JsonConvert.DeserializeObject<OUserOverview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

        //    _ManageUserAccount = new ManageUserAccount();
        //    OResponse _Response = _ManageUserAccount.GetUserAccountOverview(_Request);

        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        /// <summary>
        /// Gets the user account.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [Route(RouteUrl.V1.System + "getadvertiser")]
        [Route(RouteUrl.V1.System + "getmerchant")]
        [Route(RouteUrl.V1.System + "getcarduser")]
        [Route(RouteUrl.V1.System + "getappuser")]
        [Route(RouteUrl.V1.System + "getcashier")]
        [Route(RouteUrl.V1.System + "getstore")]
        [Route(RouteUrl.V1.System + "getuseraccount")]
        [ActionName("getuseraccount")]
        public OAuth.Request GetUserAccount([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserAccount _Request = JsonConvert.DeserializeObject<OUserAccount>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.GetUserAccount(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Gets the user account self.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuseraccountself")]
        public OAuth.Request GetUserAccountSelf([FromBody] OAuth.Request _OAuthRequest)
        {

            #region Manage Operations
            OUserAccount _Request = JsonConvert.DeserializeObject<OUserAccount>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.GetUserAccountSelf(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion

        }


        //[Route(RouteUrl.V1.System + "getadvertisers")]
        //[Route(RouteUrl.V1.System + "getmerchants")]
        //[Route(RouteUrl.V1.System + "getcardusers")]
        //[Route(RouteUrl.V1.System + "getappusers")]
        //[Route(RouteUrl.V1.System + "getcashiers")]
        //[Route(RouteUrl.V1.System + "getstores")]
        //[Route(RouteUrl.V1.System + "getuseraccounts")]
        //[ActionName("getuseraccounts")]
        //[HttpPost]
        //public OAuth.Request GetUserAccounts([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Manage Operations
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

        //    _ManageUserAccount = new ManageUserAccount();
        //    OResponse _Response = _ManageUserAccount.GetUserAccounts(_Request);

        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        /// <summary>
        /// Gets the user details change log.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuserdetailschangelog")]
        public OAuth.Request GetUserDetailsChangeLog([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.GetUserDetailsChangeLog(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        #endregion

        #region User operations
        /// <summary>
        /// Updates the user account self.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuseraccountself")]
        public OAuth.Request UpdateUserAccountSelf([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserAccount _Request = JsonConvert.DeserializeObject<OUserAccount>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.UpdateUserAccountSelf(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the name of the user user.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuserusername")]
        public OAuth.Request UpdateUserUserName([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserUserNameUpdate _Request = JsonConvert.DeserializeObject<OUserUserNameUpdate>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.UpdateUserUserName(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Updates the user user name self.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuserusernameself")]
        public OAuth.Request UpdateUserUserNameSelf([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserUserNameUpdate _Request = JsonConvert.DeserializeObject<OUserUserNameUpdate>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.UpdateUserUserNameSelf(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Updates the user password.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuserpassword")]
        public OAuth.Request UpdateUserPassword([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserPasswordUpdate _Request = JsonConvert.DeserializeObject<OUserPasswordUpdate>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.UpdateUserPassword(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the user password self.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuserpasswordself")]
        public OAuth.Request UpdateUserPasswordSelf([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserPasswordUpdate _Request = JsonConvert.DeserializeObject<OUserPasswordUpdate>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.UpdateUserPasswordSelf(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Updates the user email address.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuseremailaddress")]
        public OAuth.Request UpdateUserEmailAddress([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserEmailAddressUpdate _Request = JsonConvert.DeserializeObject<OUserEmailAddressUpdate>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.UpdateUserEmailAddress(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the user email address self.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuseremailaddressself")]
        public OAuth.Request UpdateUserEmailAddressSelf([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserEmailAddressUpdate _Request = JsonConvert.DeserializeObject<OUserEmailAddressUpdate>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.UpdateUserEmailAddressSelf(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the user email address verify.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuseremailaddressverify")]
        public OAuth.Request UpdateUserEmailAddressVerify([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserVerificationResponse _Request = JsonConvert.DeserializeObject<OUserVerificationResponse>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.UpdateUserEmailAddressVerify(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }


        /// <summary>
        /// Updates the user contact number.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateusercontactnumber")]
        public OAuth.Request UpdateUserContactNumber([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserContactNumberUpdate _Request = JsonConvert.DeserializeObject<OUserContactNumberUpdate>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.UpdateUserContactNumber(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the user contact number self.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateusercontactnumberself")]
        public OAuth.Request UpdateUserContactNumberSelf([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserContactNumberUpdate _Request = JsonConvert.DeserializeObject<OUserContactNumberUpdate>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));

            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.UpdateUserContactNumberSelf(_Request);

            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the user contact number verify.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateusercontactnumberverify")]
        public OAuth.Request UpdateUserContactNumberVerify([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserVerificationResponse _Request = JsonConvert.DeserializeObject<OUserVerificationResponse>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.UpdateUserContactNumberVerify(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Sets the user access pin.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("setuseraccesspin")]
        public OAuth.Request SetUserAccessPin([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserAccessPinUpdate _Request = JsonConvert.DeserializeObject<OUserAccessPinUpdate>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.SetUserAccessPin(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Sets the user access pin self.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("setuseraccesspinself")]
        public OAuth.Request SetUserAccessPinSelf([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserAccessPinUpdate _Request = JsonConvert.DeserializeObject<OUserAccessPinUpdate>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.SetUserAccessPinSelf(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the user access pin.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuseraccesspin")]
        public OAuth.Request UpdateUserAccessPin([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserAccessPinUpdate _Request = JsonConvert.DeserializeObject<OUserAccessPinUpdate>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.UpdateUserAccessPin(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Updates the user access pin self.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuseraccesspinself")]
        public OAuth.Request UpdateUserAccessPinSelf([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserAccessPinUpdate _Request = JsonConvert.DeserializeObject<OUserAccessPinUpdate>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.UpdateUserAccessPinSelf(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        #endregion

        #region Forgot Accesspin
        /// <summary>
        /// Forgets the user access pin request.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("forgotuseraccesspinrequest")]
        public OAuth.Request ForgetUserAccessPinRequest([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OForgotAccessPin.Request _Request = JsonConvert.DeserializeObject<OForgotAccessPin.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.ForgetUserAccessPinRequest(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Forgets the user access pin verify.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("forgetuseraccesspinverify")]
        public OAuth.Request ForgetUserAccessPinVerify([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OForgotAccessPin.Verify _Request = JsonConvert.DeserializeObject<OForgotAccessPin.Verify>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.ForgetUserAccessPinVerify(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        #endregion

        #region Forgot Password
        /// <summary>
        /// Forgots the password request.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("forgotpasswordrequest")]
        public OAuth.Request ForgotPasswordRequest([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            ForgotPassword.Request _Request = JsonConvert.DeserializeObject<ForgotPassword.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.ForgotPasswordRequest(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Forgots the password request verify.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("forgotpasswordverify")]
        public OAuth.Request ForgotPasswordRequestVerify([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            ForgotPassword.Verify _Request = JsonConvert.DeserializeObject<ForgotPassword.Verify>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.ForgotPasswordRequestVerify(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        #endregion

        #region Forgot Username
        /// <summary>
        /// Forgots the user name request.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("forgotusernamerequest")]
        public OAuth.Request ForgotUserNameRequest([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            ForgotUserName.Request _Request = JsonConvert.DeserializeObject<ForgotUserName.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.ForgotUserNameRequest(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Forgots the user name request verify.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("forgotusernamerequetverify")]
        public OAuth.Request ForgotUserNameRequestVerify([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            ForgotUserName.Verify _Request = JsonConvert.DeserializeObject<ForgotUserName.Verify>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = _ManageUserAccount.ForgotUserNameRequestVerify(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }
        #endregion

        /// <summary>
        /// Processes the transaction.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [Route(RouteUrl.V1.System + "processadvertisertransaction")]
        [Route(RouteUrl.V1.System + "processmerchanttransaction")]
        [Route(RouteUrl.V1.System + "processappusertransaction")]
        [Route(RouteUrl.V1.System + "processcardusertransaction")]
        [Route(RouteUrl.V1.System + "processstoretransaction")]
        [Route(RouteUrl.V1.System + "processcashiertransaction")]
        [Route(RouteUrl.V1.System + "processtransaction")]
        [ActionName("processtransaction")]
        public OAuth.Request ProcessTransaction([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OProcessTransaction _Request = JsonConvert.DeserializeObject<OProcessTransaction>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserTransaction = new ManageUserTransaction();
            return HCoreAuth.Auth_Response(_ManageUserTransaction.ProcessTransaction(_Request));
            #endregion
        }

        /// <summary>
        /// Gets the transaction.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [Route(RouteUrl.V1.System + "getadvertisertransaction")]
        [Route(RouteUrl.V1.System + "getmerchanttransaction")]
        [Route(RouteUrl.V1.System + "getappusertransaction")]
        [Route(RouteUrl.V1.System + "getstoretransaction")]
        [Route(RouteUrl.V1.System + "getcashiertransaction")]
        [Route(RouteUrl.V1.System + "getcardusertransaction")]
        [Route(RouteUrl.V1.System + "gettransaction")]
        [ActionName("gettransaction")]
        public OAuth.Request GetTransaction([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OTransaction.Details _Request = JsonConvert.DeserializeObject<OTransaction.Details>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserTransaction = new ManageUserTransaction();
            return HCoreAuth.Auth_Response(_ManageUserTransaction.GetTransaction(_Request));
            #endregion
        }

        /// <summary>
        /// Gets the transactions.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [Route(RouteUrl.V1.System + "getadvertisertransactions")]
        [Route(RouteUrl.V1.System + "getmerchanttransactions")]
        [Route(RouteUrl.V1.System + "getappusertransactions")]
        [Route(RouteUrl.V1.System + "getstoretransactions")]
        [Route(RouteUrl.V1.System + "getcashiertransactions")]
        [Route(RouteUrl.V1.System + "getcardusertransactions")]
        [Route(RouteUrl.V1.System + "gettransactions")]
        [ActionName("gettransactions")]
        public OAuth.Request GetTransactions([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserTransaction = new ManageUserTransaction();
            return HCoreAuth.Auth_Response(_ManageUserTransaction.GetTransactions(_Request));
            #endregion
        }

        /// <summary>
        /// Gets the transaction overview.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [Route(RouteUrl.V1.System + "getadvertisertransactionsoverview")]
        [Route(RouteUrl.V1.System + "getmerchanttransactionsoverview")]
        [Route(RouteUrl.V1.System + "getappusertransactionsoverview")]
        [Route(RouteUrl.V1.System + "getstoretransactionsoverview")]
        [Route(RouteUrl.V1.System + "getcashiertransactionsoverview")]
        [Route(RouteUrl.V1.System + "getcardusertransactionsoverview")]
        [Route(RouteUrl.V1.System + "gettransactionsoverview")]
        [ActionName("gettransactionsoverview")]
        public OAuth.Request GetTransactionOverview([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OTransaction.Overview.Request _Request = JsonConvert.DeserializeObject<OTransaction.Overview.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserTransaction = new ManageUserTransaction();
            return HCoreAuth.Auth_Response(_ManageUserTransaction.GetTransactionsOverview(_Request));
            #endregion
        }

        /// <summary>
        /// Gets the user account balance.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [Route(RouteUrl.V1.System + "getadvertiserbalance")]
        [Route(RouteUrl.V1.System + "getmerchantbalance")]
        [Route(RouteUrl.V1.System + "getappuserbalance")]
        [Route(RouteUrl.V1.System + "getstorebalance")]
        [Route(RouteUrl.V1.System + "getcashierbalance")]
        [Route(RouteUrl.V1.System + "getcarduserbalance")]
        [Route(RouteUrl.V1.System + "getuseraccountbalance")]
        [ActionName("getuseraccountbalance")]
        public OAuth.Request GetUserAccountBalance([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OTransaction.Balance.Request _Request = JsonConvert.DeserializeObject<OTransaction.Balance.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserTransaction = new ManageUserTransaction();
            return HCoreAuth.Auth_Response(_ManageUserTransaction.GetUserAccountBalance(_Request));
            #endregion
        }

        //[HttpPost]
        //[Route(RouteUrl.V1.System + "getadvertiseranalytics")]
        //[Route(RouteUrl.V1.System + "getmerchantanalytics")]
        //[Route(RouteUrl.V1.System + "getappuseranalytics")]
        //[Route(RouteUrl.V1.System + "getstoreanalytics")]
        //[Route(RouteUrl.V1.System + "getcashieranalytics")]
        //[Route(RouteUrl.V1.System + "getcarduseranalytics")]
        //[Route(RouteUrl.V1.System + "getuseranalytics")]
        //[ActionName("getuseranalytics")]
        //public OAuth.Request GetUserAnalytics([FromBody]OAuth.Request _OAuthRequest)
        //{
        //    #region Manage Operations
        //    OChart.Request _Request = JsonConvert.DeserializeObject<OChart.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageUserAccount = new ManageUserAccount();
        //    return HCoreAuth.Auth_Response(_ManageUserAccount.GetUserAnalytics(_Request));
        //    #endregion
        //}

        #endregion
        #endregion

        /// <summary>
        /// Description: Saves the user parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("saveuserparameter")]
        public OAuth.Request SaveUserParameter([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserParameter.Manage _Request = JsonConvert.DeserializeObject<OUserParameter.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUser = new ManageUser();
            OResponse _Response = _ManageUser.SaveUserParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Updates the user parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updateuserparameter")]
        public OAuth.Request UpdateUserParameter([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserParameter.Manage _Request = JsonConvert.DeserializeObject<OUserParameter.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUser = new ManageUser();
            OResponse _Response = _ManageUser.UpdateUserParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Deletes the user parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("deleteuserparameter")]
        public OAuth.Request DeleteUserParameter([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserParameter.Manage _Request = JsonConvert.DeserializeObject<OUserParameter.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUser = new ManageUser();
            OResponse _Response = _ManageUser.DeleteUserParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the user parameter.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuserparameter")]
        public OAuth.Request GetUserParameter([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OUserParameter.Manage _Request = JsonConvert.DeserializeObject<OUserParameter.Manage>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUser = new ManageUser();
            OResponse _Response = _ManageUser.GetUserParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Gets the user parameters.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("getuserparameters")]
        public OAuth.Request GetUserParameters([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Build Response
            OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUser = new ManageUser();
            OResponse _Response = _ManageUser.GetUserParameter(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Requests the otp.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("requestotp")]
        public OAuth.Request RequestOtp([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OSystemVerification.Request _Request = JsonConvert.DeserializeObject<OSystemVerification.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemVerification = new ManageSystemVerification();
            OResponse _Response = _ManageSystemVerification.RequestOtp(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        /// <summary>
        /// Description: Verifies the otp.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("verifyotp")]
        public OAuth.Request VerifyOtp([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OSystemVerification.RequestVerify _Request = JsonConvert.DeserializeObject<OSystemVerification.RequestVerify>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageSystemVerification = new ManageSystemVerification();
            OResponse _Response = _ManageSystemVerification.VerifyOtp(_Request);
            return HCoreAuth.Auth_Response(_Response);
            #endregion
        }

        //#region Country

        //ManagerCountry _ManagerCountry;
        //[HttpPost]
        //[ActionName("savecountry")]
        //public OAuth.Request SaveCountry([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OCountry.Save _Request = JsonConvert.DeserializeObject<OCountry.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    OResponse _Response = new OResponse();
        //    //_Response.Message = "error";
        //    if (!TryValidateModel(_Request, nameof(OCountry.Save)))
        //    {
        //        return HCoreAuth.Auth_Response(_Response);
        //    }
        //    _ManagerCountry = new ManagerCountry();
        //    _Response = _ManagerCountry.SaveCountry(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("updatecountry")]
        //public OAuth.Request UpdateCountry([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OCountry.Save _Request = JsonConvert.DeserializeObject<OCountry.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManagerCountry = new ManagerCountry();
        //    OResponse _Response = _ManagerCountry.UpdateCountry(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("deletecountry")]
        //public OAuth.Request DeleteCountry([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OCountry.Save _Request = JsonConvert.DeserializeObject<OCountry.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManagerCountry = new ManagerCountry();
        //    OResponse _Response = _ManagerCountry.DeleteCountry(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("getcountrydetails")]
        //public OAuth.Request GetCountryDetails([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OCountry.Request _Request = JsonConvert.DeserializeObject<OCountry.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManagerCountry = new ManagerCountry();
        //    OResponse _Response = _ManagerCountry.GetCountryDetails(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("getcountries")]
        //public OAuth.Request GetCountries([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManagerCountry = new ManagerCountry();
        //    OResponse _Response = _ManagerCountry.GetCountries(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}
        //#endregion

        //#region Region

        //ManageRegion _ManageRegion;
        //[HttpPost]
        //[ActionName("saveregion")]
        //public OAuth.Request SaveRegion([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    ORegion.Save _Request = JsonConvert.DeserializeObject<ORegion.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageRegion = new ManageRegion();
        //    OResponse _Response = _ManageRegion.SaveRegion(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("updateregion")]
        //public OAuth.Request UpdateRegion([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    ORegion.Save _Request = JsonConvert.DeserializeObject<ORegion.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageRegion = new ManageRegion();
        //    OResponse _Response = _ManageRegion.UpdateRegion(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("deleteregion")]
        //public OAuth.Request DeleteRegion([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    ORegion.Save _Request = JsonConvert.DeserializeObject<ORegion.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageRegion = new ManageRegion();
        //    OResponse _Response = _ManageRegion.DeleteRegion(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("getregionsbycountry")]
        //public OAuth.Request GetRegionsByCountry([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageRegion = new ManageRegion();
        //    OResponse _Response = _ManageRegion.GetRegionsByCountry(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("getregiondetails")]
        //public OAuth.Request GetRegionDetails([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    ORegion.Request _Request = JsonConvert.DeserializeObject<ORegion.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageRegion = new ManageRegion();
        //    OResponse _Response = _ManageRegion.GetRegionDetails(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //#endregion

        //#region RegionArea

        //ManageRegionArea _ManageRegionArea;
        //[HttpPost]
        //[ActionName("saveregionarea")]
        //public OAuth.Request SaveRegionArea([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    ORegionArea.Save _Request = JsonConvert.DeserializeObject<ORegionArea.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageRegionArea = new ManageRegionArea();
        //    OResponse _Response = _ManageRegionArea.SaveRegionArea(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("updateregionarea")]
        //public OAuth.Request UpdateRegionArea([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    ORegionArea.Save _Request = JsonConvert.DeserializeObject<ORegionArea.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageRegionArea = new ManageRegionArea();
        //    OResponse _Response = _ManageRegionArea.UpdateRegionArea(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("deleteregionarea")]
        //public OAuth.Request DeleteRegionArea([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region ReagionArea
        //    ORegionArea.Save _Request = JsonConvert.DeserializeObject<ORegionArea.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageRegionArea = new ManageRegionArea();
        //    OResponse _Response = _ManageRegionArea.DeleteRegionArea(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("getregionareabyregion")]
        //public OAuth.Request GetRegionAreaByRegion([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageRegionArea = new ManageRegionArea();
        //    OResponse _Response = _ManageRegionArea.GetRegionAreaByRegion(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("getregionareadetails")]
        //public OAuth.Request GetRegionAreaDetails([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    ORegionArea.Request _Request = JsonConvert.DeserializeObject<ORegionArea.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageRegionArea = new ManageRegionArea();
        //    OResponse _Response = _ManageRegionArea.GetRegionAreaDetails(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //#endregion

        //#region City

        //ManageCity _ManageCity;
        //[HttpPost]
        //[ActionName("savecity")]
        //public OAuth.Request SaveCity([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OCity.Save _Request = JsonConvert.DeserializeObject<OCity.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageCity = new ManageCity();
        //    OResponse _Response = _ManageCity.SaveCity(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("updatecity")]
        //public OAuth.Request UpdateCity([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OCity.Save _Request = JsonConvert.DeserializeObject<OCity.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageCity = new ManageCity();
        //    OResponse _Response = _ManageCity.UpdateCity(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("deletecity")]
        //public OAuth.Request DeleteCity([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OCity.Save _Request = JsonConvert.DeserializeObject<OCity.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageCity = new ManageCity();
        //    OResponse _Response = _ManageCity.DeleteCity(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //// [HttpPost]
        //// [ActionName("getcitydetailsbyregionareaid")]
        //// public OAuth.Request GetCityDetailsByRegionAreaId([FromBody] OAuth.Request _OAuthRequest)
        //// {
        ////     #region Build Response
        ////     OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.bx));
        ////     _ManageCity = new ManageCity();
        ////     OResponse _Response = _ManageCity.GetCityDetailsByRegionAreaId(_Request);
        ////     return HCoreAuth.Auth_Response(_Response);
        ////     #endregion
        //// }

        //[HttpPost]
        //[ActionName("getcitydetails")]
        //public OAuth.Request GetCityDetails([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OCity.Request _Request = JsonConvert.DeserializeObject<OCity.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageCity = new ManageCity();
        //    OResponse _Response = _ManageCity.GetCityDetails(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("getcitybyregionarea")]
        //public OAuth.Request GetCityByRegionArea([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageCity = new ManageCity();
        //    OResponse _Response = _ManageCity.GetCityByRegionArea(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //#endregion

        //#region CityArea

        //ManageCityArea _ManageCityArea;
        //[HttpPost]
        //[ActionName("savecityarea")]
        //public OAuth.Request SaveCityArea([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OCityArea.Save _Request = JsonConvert.DeserializeObject<OCityArea.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageCityArea = new ManageCityArea();
        //    OResponse _Response = _ManageCityArea.SaveCityArea(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("updatecityarea")]
        //public OAuth.Request UpdateCityArea([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OCityArea.Save _Request = JsonConvert.DeserializeObject<OCityArea.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageCityArea = new ManageCityArea();
        //    OResponse _Response = _ManageCityArea.UpdateCityArea(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("deletecityarea")]
        //public OAuth.Request DeleteCityArea([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OCityArea.Save _Request = JsonConvert.DeserializeObject<OCityArea.Save>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageCityArea = new ManageCityArea();
        //    OResponse _Response = _ManageCityArea.DeleteCityArea(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("getcityareabycity")]
        //public OAuth.Request GetCityAreaByCity([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OList.Request _Request = JsonConvert.DeserializeObject<OList.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageCityArea = new ManageCityArea();
        //    OResponse _Response = _ManageCityArea.GetCityAreaByCity(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //[HttpPost]
        //[ActionName("getcityareadetails")]
        //public OAuth.Request GetCityAreaDetails([FromBody] OAuth.Request _OAuthRequest)
        //{
        //    #region Build Response
        //    OCityArea.Request _Request = JsonConvert.DeserializeObject<OCityArea.Request>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
        //    _ManageCityArea = new ManageCityArea();
        //    OResponse _Response = _ManageCityArea.GetCityAreaDetails(_Request);
        //    return HCoreAuth.Auth_Response(_Response);
        //    #endregion
        //}

        //#endregion


        /// <summary>
        /// Updates the temporary pin.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>OAuth.Request.</returns>
        [HttpPost]
        [ActionName("updatetemppin")]
        public OAuth.Request UpdateTempPin([FromBody] OAuth.Request _OAuthRequest)
        {
            #region Manage Operations
            OUserAccountAccess.PinUpdateRequest _Request = JsonConvert.DeserializeObject<OUserAccountAccess.PinUpdateRequest>(HCoreEncrypt.DecodeText(_OAuthRequest.zx));
            _ManageUserAccountAccess = new ManageUserAccountAccess();
            return HCoreAuth.Auth_Response(_ManageUserAccountAccess.UpdateTempPin(_Request));
            #endregion
        }
    }

}
