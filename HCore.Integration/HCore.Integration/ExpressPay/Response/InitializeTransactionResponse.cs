﻿using System;
using Newtonsoft.Json;

namespace HCore.Integration.ExpressPay.Requests
{
    public class InitializeTransactionResponse
    {
        public int status { get; set; }

        [JsonProperty("status-text")]
        public string? StatusText { get; set; }

        [JsonProperty("reference-number")]
        public string? ReferenceNumber { get; set; }
        public string? balance { get; set; }
        public string? currency { get; set; }

        [JsonProperty("transaction-id")]
        public string? TransactionId { get; set; }

        [JsonProperty("receipt-number")]
        public string? ReceiptNumber { get; set; }
        public string? timestamp { get; set; }
    }
}

