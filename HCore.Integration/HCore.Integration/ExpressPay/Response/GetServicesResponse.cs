﻿using System;
using Newtonsoft.Json;

namespace HCore.Integration.ExpressPay.Requests
{
    public class GetServicesResponse
    {
        public int status { get; set; }

        [JsonProperty("status-text")]
        public string? StatusText { get; set; }
        public List<Service> services { get; set; }
    }

    public class Service
    {
        public string? service { get; set; }
        public string? name { get; set; }
        public string? category { get; set; }
        public string? query { get; set; }
    }
}

