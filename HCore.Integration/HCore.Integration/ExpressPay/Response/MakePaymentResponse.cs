﻿using System;
using Newtonsoft.Json;

namespace HCore.Integration.ExpressPay.Requests
{
    public class MakePaymentResponse
    {
        public int status { get; set; }

        [JsonProperty("status-text")]
        public string? StatusText { get; set; }

        public string? TransactionId { get; set; }
        public string? ReceiptNumber { get; set; }

        public string? TimeStamp { get; set; }
        public string? ReferenceNumber { get; set; }
        public string? Balance { get; set; }
        public string? Currency { get; set; }
        public string? ResponseContent { get; set; }
    }
}

