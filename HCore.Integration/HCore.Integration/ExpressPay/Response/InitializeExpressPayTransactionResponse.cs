﻿using System;
using Newtonsoft.Json;

namespace HCore.Integration.ExpressPay.Requests
{
    public class InitializeExpressPayTransactionResponse
    {
        public int status { get; set; }

        [JsonProperty("order-id")]
        public string? OrderId { get; set; }

        [JsonProperty("guest-checkout")]
        public string? GuestCheckout { get; set; }

        [JsonProperty("merchantservice-name")]
        public string? MerchantserviceName { get; set; }

        [JsonProperty("merchantservice-srvrtid")]
        public string? MerchantserviceSrvrtid { get; set; }
        public string? message { get; set; }
        public string? token { get; set; }

        [JsonProperty("redirect-url")]
        public string? RedirectUrl { get; set; }

        [JsonProperty("user-key")]
        public object UserKey { get; set; }

        [JsonProperty("merchant-name")]
        public string? MerchantName { get; set; }

        [JsonProperty("merchant-mcc")]
        public string? MerchantMcc { get; set; }

        [JsonProperty("merchant-city")]
        public string? MerchantCity { get; set; }

        [JsonProperty("merchant-countrycode")]
        public string? MerchantCountrycode { get; set; }
    }
}

