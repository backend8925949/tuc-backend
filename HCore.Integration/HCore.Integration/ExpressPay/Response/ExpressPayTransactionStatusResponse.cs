﻿using System;
using Newtonsoft.Json;

namespace HCore.Integration.ExpressPay.Requests
{
    public class ExpressPayTransactionStatusResponse
    {
        public int result { get; set; }

        [JsonProperty("result-text")]
        public string? ResultText { get; set; }

        [JsonProperty("order-id")]
        public string? OrderId { get; set; }
        public string? token { get; set; }
        public string? currency { get; set; }
        public string? amount { get; set; }
    }
}

