﻿using System;
using Newtonsoft.Json;

namespace HCore.Integration.ExpressPay.Requests
{
    public class GetTransactionsResponse
    {
        public int status { get; set; }

        [JsonProperty("status-text")]
        public string? StatusText { get; set; }
        public string? Reference_Number { get; set; }
        public string? Account_Info { get; set; }
        public Options Options { get; set; }
    }

    public class Options
    {
        public string? Name { get; set; }
        public string? Package { get; set; }
        public string? Amount { get; set; }
        public string? Currency { get; set; }

        [JsonProperty("due-date")]
        public string? DueDate { get; set; }

        [JsonProperty("fee-amount")]
        public string? FeeAmount { get; set; }

        [JsonProperty("fee-percent")]
        public string? FeePercent { get; set; }
    }
}

