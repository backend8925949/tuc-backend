﻿using System;
using Newtonsoft.Json;

namespace HCore.Integration.ExpressPay.Requests
{
    public class PaymentStatusResponse
    {
        public int status { get; set; }

        [JsonProperty("status-text")]
        public string? StatusText { get; set; }

        [JsonProperty("reference-number")]
        public string? ReferenceNumber { get; set; }

        [JsonProperty("transaction-id")]
        public string? TransactionId { get; set; }

        [JsonProperty("receipt-number")]
        public string? ReceiptNumber { get; set; }
        public string? timestamp { get; set; }
    }
}

