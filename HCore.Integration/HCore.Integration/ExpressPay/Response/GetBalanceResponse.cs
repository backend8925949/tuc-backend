﻿using System;
using Newtonsoft.Json;

namespace HCore.Integration.ExpressPay.Requests
{
    public class GetBalanceResponse
    {
        public int status { get; set; }

        [JsonProperty("status-text")]
        public string? StatusText { get; set; }
        public string? balance { get; set; }
        public string? currency { get; set; }
    }
}

