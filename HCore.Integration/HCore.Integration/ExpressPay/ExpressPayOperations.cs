//==================================================================================
// FileName: ExpressPayOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using Delivery.Object.Response.Shipments;
using HCore.Helper;
using HCore.Integration.CoralPayVas;
using HCore.Integration.ExpressPay.Requests;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using static HCore.Helper.HCoreConstant;

namespace HCore.Integration.ExpressPay
{
    public class ExpressPayOperations
    {
        public class OExpPayment
        {
            public int status { get; set; }

            public string? transactionid { get; set; }
            public string? receiptnumber { get; set; }

            public string? timestamp { get; set; }
            public string? referencenumber { get; set; }
            public string? balance { get; set; }
            public string? currency { get; set; }
            public string? responseContent { get; set; }
        }

        public GetServicesResponse GetServices(GetServicesRequest _Request)
        {
            try
            {
                string data = "username=" + _Request.UserName + "&auth-token=" + _AppConfig.ExpresspayAuthToken + "&type=" + _Request.Type;
                using (WebClient client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string result = client.UploadString(_AppConfig.ExpresspayUrl + "billpay/api.php", data);
                    string Response = result;
                    if (result != null)
                    {
                        GetServicesResponse _RequestBodyContent = JsonConvert.DeserializeObject<GetServicesResponse>(result);
                        return _RequestBodyContent;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                return null;
            }
        }

        public InitializeTransactionResponse InitializeTransaction(InitializeTransactionRequest _Request)
        {
            try
            {
                string data = "username=" + _Request.UserName + "&auth-token=" + _AppConfig.ExpresspayAuthToken + "&type=" + _Request.Type + "&service=" + _Request.Service + "&account-number=" + _Request.Account_Number + "&reference-number=" + _Request.Reference_Number + "&currency=" + _Request.Currency + "&amount=" + _Request.Amount + "&payer-name=" + _Request.PayerName + "&payer-phonenumber=" + _Request.PayerPhoneNumber;
                using (WebClient client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string result = client.UploadString(_AppConfig.ExpresspayUrl + "billpay/api.php?", data);
                    string Response = result;
                    if (result != null)
                    {
                        InitializeTransactionResponse _RequestBodyContent = JsonConvert.DeserializeObject<InitializeTransactionResponse>(result);
                        return _RequestBodyContent;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                return null;
            }
        }

        public PaymentStatusResponse PaymentStatus(PaymentStatusRequet _Request)
        {
            try
            {
                string data = "username=" + _Request.UserName + "&auth-token=" + _AppConfig.ExpresspayAuthToken + "&type=" + _Request.Type + "&transaction-id=" + _Request.Transaction_Id + "&reference-number=" + _Request.Reference_Number;
                using (WebClient client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string result = client.UploadString(_AppConfig.ExpresspayUrl + "billpay/api.php?", data);
                    string Response = result;
                    if (result != null)
                    {
                        PaymentStatusResponse _RequestBodyContent = JsonConvert.DeserializeObject<PaymentStatusResponse>(result);
                        return _RequestBodyContent;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                return null;
            }
        }

        public GetBalanceResponse GetBalance(GetBalanceRequest _Request)
        {
            try
            {
                string data = "username=" + _Request.UserName + "&auth-token=" + _AppConfig.ExpresspayAuthToken + "&type=" + _Request.Type + "&transaction-id=" + _Request.Reference_Number;
                using (WebClient client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string result = client.UploadString(_AppConfig.ExpresspayUrl + "billpay/api.php?", data);
                    string Response = result;
                    if (result != null)
                    {
                        GetBalanceResponse _RequestBodyContent = JsonConvert.DeserializeObject<GetBalanceResponse>(result);
                        return _RequestBodyContent;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                return null;
            }
        }

        public GetTransactionsResponse GetTransactions(GetTransactionsRequest _Request)
        {
            try
            {
                string data = "username=" + _Request.UserName + "&auth-token=" + _AppConfig.ExpresspayAuthToken + "&type=" + _Request.Type + "&service=" + _Request.Service + "&account-number=" + _Request.Account_Number + "&reference-number=" + _Request.Reference_Number;
                using (WebClient client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string result = client.UploadString(_AppConfig.ExpresspayUrl + "billpay/api.php?", data);
                    string Response = result;
                    if (result != null)
                    {
                        GetTransactionsResponse _RequestBodyContent = JsonConvert.DeserializeObject<GetTransactionsResponse>(result);
                        return _RequestBodyContent;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                return null;
            }
        }

        public static OExpPayment MakePayment(string Username, string? AuthToken, string? Servie, string? AccountNumber, string? ReferenceNumber, string? Amount)
        {
            try
            {
                string Url = "https://sandbox.expresspaygh.com/billpay/api.php";
                if (HostEnvironment == HostEnvironmentType.BackgroudProcessor || HostEnvironment == HostEnvironmentType.Live)
                {
                    Url = "https://expresspaygh.com/billpay/api.php";
                }
                var client = new RestClient(Url);
                var request = new RestRequest();
                request.Method = Method.Post;
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");

                request.AddParameter("username", Username);
                request.AddParameter("auth-token", AuthToken);

                request.AddParameter("type", "PAY");
                request.AddParameter("service", Servie);
                request.AddParameter("currency", "GHS");
                request.AddParameter("account-number", AccountNumber);
                request.AddParameter("reference-number", ReferenceNumber);
                request.AddParameter("amount", Amount);
                request.AddParameter("payer-name", "ThankUCash");
                request.AddParameter("payer-phonenumber", "2349087106997");
                var response = client.Execute(request);
                string Content = response.Content;
                OExpPayment _RequestBodyContent = JsonConvert.DeserializeObject<OExpPayment>(Content);
                _RequestBodyContent.responseContent = Content;
                return _RequestBodyContent;
                //Console.WriteLine(response.Content);
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        //public MakePaymentResponse MakePayment(MakePaymentRequest _Request)
        //{
        //    try
        //    {
        //        var client = new RestClient(_AppConfig.ExpresspayUrl + "");
        //        var request = new RestRequest();
        //        request.Method = Method.Post;
        //        request.AddHeader("Content-Type", "application/x-www-form-urlencoded");

        //        request.AddParameter("username", _Request.Username);
        //        request.AddParameter("auth-token", _AppConfig.ExpresspayAuthToken);

        //        request.AddParameter("type", "PAY");
        //        request.AddParameter("service", _Request.Servie);
        //        request.AddParameter("currency", "GHS");
        //        request.AddParameter("account-number", _Request.AccountNumber);
        //        request.AddParameter("reference-number", _Request.ReferenceNumber);
        //        request.AddParameter("amount", _Request.Amount);
        //        request.AddParameter("payer-name", "ThankUCash");
        //        request.AddParameter("payer-phonenumber", "2349087106997");
        //        var response = client.Execute(request);
        //        string Content = response.Content;
        //        MakePaymentResponse _RequestBodyContent = JsonConvert.DeserializeObject<MakePaymentResponse>(Content);
        //        _RequestBodyContent.ResponseContent = Content;
        //        return _RequestBodyContent;
        //        //Console.WriteLine(response.Content);
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }

        //}

        public InitializeExpressPayTransactionResponse InitializeExpressPayTransactionResponse(InitializeExpressPayTransactionRequest _Request)
        {
            try
            {
                string data = "merchant-id=" + _Request.MerchantId + "&api-key=" + _AppConfig.ExpresspayApiKey + "&firstname=" + _Request.FirstName + "&lastname=" + _Request.LastName + "&email=" + _Request.Email + "&phonenumber=" + _Request.PhoneNumber + "&username" + _Request.UserName + "&currency=" + _Request.Currency + "&amount=" + _Request.Amount + "&order-id=" + _Request.OrderId + "&redirect-url=" + _Request.RedirectUrl + "&post-url=" + _Request.PostUrl;
                using (WebClient client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string result = client.UploadString(_AppConfig.ExpresspayUrl + "api/submit.php", data);
                    string Response = result;
                    if (result != null)
                    {
                        InitializeExpressPayTransactionResponse _RequestBodyContent = JsonConvert.DeserializeObject<InitializeExpressPayTransactionResponse>(result);
                        return _RequestBodyContent;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                return null;
            }
        }

        public ExpressPayTransactionStatusResponse ExpressPayTransactionStatus(ExpressPayTransactionStatusRequest _Request)
        {
            try
            {
                string data = "merchant-id=" + _Request.MerchantId + "&api-key=" + _AppConfig.ExpresspayApiKey + "&token=" + _Request.Token;
                using (WebClient client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string result = client.UploadString(_AppConfig.ExpresspayUrl + "api/query.php", data);
                    string Response = result;
                    if (result != null)
                    {
                        ExpressPayTransactionStatusResponse _RequestBodyContent = JsonConvert.DeserializeObject<ExpressPayTransactionStatusResponse>(result);
                        return _RequestBodyContent;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                return null;
            }
        }

        public ExpressPayPendingTransactionStatusResponse ExpressPayPendingTransactionStatus(ExpressPayPendingTransactionStatusRequest _Request)
        {
            try
            {
                string data = "order-id=" + _Request.OrderId + "&token=" + _Request.Token;
                using (WebClient client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string result = client.UploadString(_AppConfig.ExpresspayUrl + "api/checkout.php", data);
                    string Response = result;
                    if (result != null)
                    {
                        ExpressPayPendingTransactionStatusResponse _RequestBodyContent = JsonConvert.DeserializeObject<ExpressPayPendingTransactionStatusResponse>(result);
                        return _RequestBodyContent;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                return null;
            }
        }
    }
}










