﻿using System;
namespace HCore.Integration.ExpressPay.Requests
{
    public class MakePaymentRequest
    {
        public string? Username { get; set; }
        public string? AuthToken { get; set; }
        public string? Servie { get; set; }
        public string? AccountNumber { get; set; }
        public string? ReferenceNumber { get; set; }
        public string? Amount { get; set; }
    }
}

