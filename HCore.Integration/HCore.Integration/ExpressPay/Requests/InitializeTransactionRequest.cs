﻿using System;
namespace HCore.Integration.ExpressPay.Requests
{
    public class InitializeTransactionRequest
    {
        public string? UserName { get; set; }
        public string? Type { get; set; }
        public string? Auth_Token { get; set; }
        public string? Service { get; set; }
        public string? Account_Number { get; set; }
        public string? Reference_Number { get; set; }
        public string? Currency { get; set; }
        public string? Amount { get; set; }
        public string? PayerName { get; set; }
        public string? PayerPhoneNumber { get; set; }
    }
}

