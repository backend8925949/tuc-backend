﻿using System;
namespace HCore.Integration.ExpressPay.Requests
{
    public class ExpressPayTransactionStatusRequest
    {
        public string? MerchantId { get; set; }
        public string? ApiKey { get; set; }
        public string? Token { get; set; }
    }
}

