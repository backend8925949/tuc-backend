﻿using System;
namespace HCore.Integration.ExpressPay.Requests
{
    public class ExpressPayPendingTransactionStatusRequest
    {
        public string? OrderId { get; set; }
        public string? Token { get; set; }
    }
}

