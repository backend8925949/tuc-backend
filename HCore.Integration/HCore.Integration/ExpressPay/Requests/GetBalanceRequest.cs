﻿using System;
namespace HCore.Integration.ExpressPay.Requests
{
    public class GetBalanceRequest
    {
        public string? UserName { get; set; }
        public string? Type { get; set; }
        public string? Url { get; set; }
        public string? Auth_Token { get; set; }
        public string? Reference_Number { get; set; }
    }
}

