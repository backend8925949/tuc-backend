﻿using System;
namespace HCore.Integration.ExpressPay.Requests
{
    public class InitializeExpressPayTransactionRequest
    {
        public string? MerchantId { get; set; }
        public string? ApiKey { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public string? UserName { get; set; }
        public string? Currency { get; set; }
        public string? Amount { get; set; }
        public string? OrderId { get; set; }
        public string? RedirectUrl { get; set; }
        public string? PostUrl { get; set; }
    }
}

