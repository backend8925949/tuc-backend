//==================================================================================
// FileName: RequestMandateOTPResponse.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Mono.EvolveObject
{
	public class RequestMandateOTPResponse
    {
        public string? StatusCode { get; set; }
        public List<AuthParameters> AuthParams { get; set; }
        public string? RequestId { get; set; }
        public string? MandateId { get; set; }
        public string? RemitaTransRef { get; set; }
        public string? Status { get; set; }
        public long LoanRequestId { get; set; }
        public string? Message { get; set; }

        public RequestMandateOTPResponse()
		{
		}
	}
}

