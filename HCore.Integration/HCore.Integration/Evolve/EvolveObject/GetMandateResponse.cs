//==================================================================================
// FileName: GetMandateResponse.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Mono.EvolveObject
{
	public class GetMandateResponse
	{
        public long Id { get; set; }
        public long User_Id { get; set; }
        public long Org_Id { get; set; }
        public long Loan_Request_Id { get; set; }
        public SetUp SetUp { get; set; }
        public SetUpResponse SetUp_Res { get; set; }
        public List<MandateResponseError> Mandate_OTP_Req_Res_Err { get; set; }
        public bool Is_Credit_Order { get; set; }
        public int Credit_Order_Id { get; set; }
        public string? Amount { get; set; }
        public string? Status { get; set; }
        public string? Message { get; set; }
        public bool Activated { get; set; }
        public string? Form_Url { get; set; }
        public string? Created_At { get; set; }

        public GetMandateResponse()
		{
		}
	}
}

