//==================================================================================
// FileName: PrequalifyUserResponse.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono.EvolveObject
{
    public class PrequalifyUserResponse
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
        public string? data { get; set; }
    }
}
