//==================================================================================
// FileName: AddUserbankdetails.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Mono.EvolveObject
{
	public class AddUserbankdetails
	{
		public long Id { get; set; }
		public long User_Id { get; set; }
		public long Org_Id { get; set; }
		public long Loan_Request_Id { get; set; }
		public string? Code { get; set; }
		public string? Account_Name { get; set; }
		public string? Account_Number { get; set; }
		public string? Bank_Name { get; set; }
		public bool Is_Deleted { get; set; }
		public string? Created_At { get; set; }
		public string? Updated_At { get; set; }

		public AddUserbankdetails()
		{
		}
	}
}

