//==================================================================================
// FileName: MandateResponseError.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Mono.EvolveObject
{
	public class MandateResponseError
	{
		public List<AuthParameters> AuthParams { get; set; }

		public MandateResponseError()
		{
		}
	}
}

