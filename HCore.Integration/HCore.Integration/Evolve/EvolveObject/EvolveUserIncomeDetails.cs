//==================================================================================
// FileName: EvolveUserIncomeDetails.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Mono.EvolveObject
{
	public class IncomeDetails
	{
		public long? Average_Income { get; set; }
		public long? Monthly_Income { get; set; }
		public long? Yearly_Income { get; set; }
		public long? Income_Sources { get; set; }

		public IncomeDetails()
		{
		}

		public IncomeDetails(long Average_Income, long Monthly_Income, long Yearly_Income, long Income_Sources)
		{
			this.Average_Income = Average_Income;
			this.Monthly_Income = Monthly_Income;
			this.Yearly_Income = Yearly_Income;
			this.Income_Sources = Income_Sources;
		}
	}
}

