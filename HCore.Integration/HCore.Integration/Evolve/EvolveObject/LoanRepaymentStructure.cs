//==================================================================================
// FileName: LoanRepaymentStructure.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Mono.EvolveObject
{
	public class LoanRepaymentStructure
	{
		public string? Due_Date { get; set; }
		public double Total_Outstanding { get; set; }
		public double Interest { get; set; }
		public double Repayment { get; set; }

		public LoanRepaymentStructure()
		{
		}
	}
}

