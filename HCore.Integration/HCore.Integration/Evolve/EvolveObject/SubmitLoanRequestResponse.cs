//==================================================================================
// FileName: SubmitLoanRequestResponse.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Mono.EvolveObject
{
	public class SubmitLoanRequestResponse
    {
        public long Id { get; set; }
        public long User_Id { get; set; }
        public long Org_Id { get; set; }
        public long Product_Id { get; set; }
        public string? Product_Name { get; set; }
        public string? Product_Slug { get; set; }
        public string? Lender_Name { get; set; }
        public string? Reference { get; set; }
        public string? FullName { get; set; }
        public long Amount { get; set; }
        public string? Purpose { get; set; }
        public Terms Terms { get; set; }
        public string? Status { get; set; }
        public string? Message { get; set; }
        public bool Active { get; set; }
        public string? Lender_Status { get; set; }
        public string? Debit_Mandate_Next_Step { get; set; }
        public string? Declined_At { get; set; }
        public string? Final_Approval_Date { get; set; }
        public string? Cancelled_By { get; set; }
        public long Cancelled_By_Id { get; set; }
        public string? Approved_At { get; set; }
        public string? User_Status { get; set; }
        public string? User_Accepted_Date { get; set; }
        public string? User_Declined_Date { get; set; }
        public string? Archived_Date { get; set; }
        public string? Unarchived_At { get; set; }
        public string? Disbursed_At { get; set; }
        public bool Credit_Score_Check_Done { get; set; }
        public string? Submitted_At { get; set; }
        public string? Deleted_At { get; set; }
        public string? Created_At { get; set; }
        public string? Updated_At { get; set; }

        public SubmitLoanRequestResponse()
		{
		}
	}
}

