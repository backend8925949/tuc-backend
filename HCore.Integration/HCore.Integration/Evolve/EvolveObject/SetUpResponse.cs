//==================================================================================
// FileName: SetUpResponse.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Mono.EvolveObject
{
	public class SetUpResponse
	{
		public string? StatusCode { get; set; }
		public string? RequestId { get; set; }
		public string? MandateId { get; set; }
		public string? Status { get; set; }
		public string? Message { get; set; }
		public SetUpResponse()
		{
		}
	}
}

