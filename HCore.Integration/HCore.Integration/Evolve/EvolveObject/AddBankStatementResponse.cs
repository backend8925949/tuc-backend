//==================================================================================
// FileName: AddBankStatementResponse.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Mono.EvolveObject
{
	public class AddBankStatementResponse
    {
            public string? Status { get; set; }
            public string? Message { get; set; }
            public PrequalifiedUserResponse Data { get; set; }
            public long Pre_Qualified_Amount { get; set; }

        public AddBankStatementResponse()
        {
        }
    }

    public class PrequalifiedUserResponse
    {
        public long Id { get; set; }
        public long User_Id { get; set; }
        public long Org_Id { get; set; }
        public long Loan_Request_Id { get; set; }
        public List<BankStatements> Transactions { get; set; }
        public string? Credit_Outliers { get; set; }
        public string? Debit_Outliers { get; set; }
        public long Balance { get; set; }
        public IncomeDetails Income { get; set; }
        public long Suggested_Loan_Amount { get; set; }
        public string? Updated_At { get; set; }
        public string? Created_At { get; set; }

        public PrequalifiedUserResponse()
        {
        }
    }
}


