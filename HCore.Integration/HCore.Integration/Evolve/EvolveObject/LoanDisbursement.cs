//==================================================================================
// FileName: LoanDisbursement.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Mono.EvolveObject
{
	public class LoanDisbursementResponse
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
        public Disbursementresponse Data { get; set; }
        public DisbursementResposneData Data1 { get; set; }
        public Disbursements Data3 { get; set; }

        public LoanDisbursementResponse()
        {
        }
    }

    public class Disbursementresponse
    {
        public long Id { get; set; }
        public long User_Id { get; set; }
        public long Org_Id { get; set; }
        public long Product_Id { get; set; }
        public string? Product_Name { get; set; }
        public string? Product_Slug { get; set; }
        public string? Lender_Name { get; set; }
        public string? Reference { get; set; }
        public string? FullName { get; set; }
        public long Amount { get; set; }
        public string? Purpose { get; set; }
        public Terms Terms { get; set; }
        public string? Status { get; set; }
        public string? Message { get; set; }
        public bool Active { get; set; }
        public string? Lender_Status { get; set; }
        public string? Debit_Mandate_Next_Step { get; set; }
        public string? Declined_At { get; set; }
        public string? Final_Approval_Date { get; set; }
        public long? Approved_By_Id { get; set; }
        public string? Approved_By { get; set; }
        public string? Cancelled_By { get; set; }
        public long Cancelled_By_Id { get; set; }
        public string? Approved_At { get; set; }
        public string? User_Status { get; set; }
        public string? User_Accepted_Date { get; set; }
        public string? User_Declined_Date { get; set; }
        public string? Archived_Date { get; set; }
        public string? Unarchived_At { get; set; }
        public string? Disbursed_At { get; set; }
        public string? Submitted_At { get; set; }
        public string? Deleted_At { get; set; }
        public string? Created_At { get; set; }
        public string? Updated_At { get; set; }
    }

    public class DisbursementResposneData
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
        public Disbursement Data { get; set; }
    }

    public class Disbursement
    {
        public long Id { get; set; }
        public string? Account_number { get; set; }
        public string? Bank_Code { get; set; }
        public string? Full_Name { get; set; }
        public string? Created_At { get; set; }
        public string? Currency { get; set; }
        public string? Debiy_Currency { get; set; }
        public long Amount { get; set; }
        public double Fee { get; set; }
        public string? Status { get; set; }
        public string? Reference { get; set; }
        public string? Meta { get; set; }
        public string? Narration { get; set; }
        public string? Complete_Message { get; set; }
        public int Requires_Approval { get; set; }
        public int Is_Approved { get; set; }
        public string? Bank_Name { get; set; }
    }

    public class Disbursements
    {
        public string? Start_Date { get; set; }
        public string? End_Date { get; set; }
        public long Loan_Id { get; set; }
    }
}

