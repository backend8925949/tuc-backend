//==================================================================================
// FileName: EvolveUser.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
namespace Mono.EvolveObject
{
    public class EvolveUser
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
        public string? Phone { get; set; }
        public int? GenderId { get; set; }

        public EvolveUser()
        {
        }

        public EvolveUser(string FirstName, string? LastName, string? Email, string? Password, string? Phone, int? GenderId)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Email = Email;
            this.Password = Password;
            this.Phone = Phone;
            this.GenderId = GenderId;
        }
        public string? bvn { get; set; }
    }
}
