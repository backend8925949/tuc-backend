//==================================================================================
// FileName: EvolveUserbankDetails.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using System;
namespace Mono.EvolveObject
{
    public class BankDetails
    {
        public BankDetails(string Code, string? Account_Name, string? Account_Number, string? Bank_Name)
        {
            code = Code;
            account_name = Account_Name;
            account_number = Account_Number;
            bank_name = Bank_Name;
        }

        public string? code { get; set; }
        public string? account_name { get; set; }
        public string? account_number { get; set; }
        public string? bank_name { get; set; }
        public OUserReference userReference { get; set; }
    }
}

