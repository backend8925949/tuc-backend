//==================================================================================
// FileName: OLoanActivity.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.Integration.Evolve.EvolveObject
{
    public class OLoanActivity
    {
        public class Save
        {
            public long? LoanId { get; set; }
            public string? LoanKey { get; set; }
            public string? Description { get; set; }
        }
    }
}
