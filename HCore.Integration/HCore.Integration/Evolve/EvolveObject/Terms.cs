//==================================================================================
// FileName: Terms.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Mono.EvolveObject
{
	public class Terms
    {
        public long Approved_Amount { get; set; }
        public long Rate { get; set; }
        public long Tenure { get; set; }
        public string? Tenured_In { get; set; }
        public string? Rate_Type { get; set; }
        public string? Frequency { get; set; }
        public long Grace_Period { get; set; }
        public bool Exclude_Weekends { get; set; }
        public bool Zero_Interest { get; set; }
        public long Estimated_Repayment { get; set; }
        public string? Note { get; set; }

        public Terms()
		{
		}
	}
}

