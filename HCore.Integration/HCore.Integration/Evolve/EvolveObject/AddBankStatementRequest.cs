//==================================================================================
// FileName: AddBankStatementRequest.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;

namespace Mono.EvolveObject
{
	public class AddBankStatementRequest
	{
		public List<BankStatements> transactions;
		public long balance;
		public IncomeDetails income;
		
		public AddBankStatementRequest(List<BankStatements> bankstatements, long Balance, IncomeDetails Income)
		{
			this.transactions = bankstatements;
			this.balance = Balance;
			this.income = Income;
		}
		public OUserReference userReference { get; set; }
	}
}

