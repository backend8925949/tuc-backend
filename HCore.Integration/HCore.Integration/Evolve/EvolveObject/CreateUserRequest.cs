//==================================================================================
// FileName: CreateUserRequest.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
namespace Mono.EvolveObject
{
    public class CreateUserRequest
    {
        public EvolveUser user;
        public string? bvn;
        public CreateUserRequest(EvolveUser user,String bvn)
        {
            this.user = user;
            this.bvn = bvn;
        }
        public long balance { get; set; }
        public string? monoReference { get; set; }

        public Bank Bank { get; set; }
        public OUserReference? UserReference { get; set; }
    }

    public class Bank
    {
        public string? Name { get; set; }
        public string? Code { get; set; }
        public string? Type { get; set; }
        public string? AccountName { get; set; }
        public string? AccountNumber { get; set; }
        public double Balance { get; set; }
    }
}
