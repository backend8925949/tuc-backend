//==================================================================================
// FileName: CreateUserResponse.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Mono.EvolveObject
{
    public class CreateUserResponse : EvolveUser
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
        public EvolveUserInfo Data { get; set; }

        public CreateUserResponse()
        {
        }
    }

    public class EvolveUserInfo : EvolveUser
    {
        public long Id { get; set; }
        public long GenderId { get; set; }
        public string? Reference { get; set; }
        public bool Is_Active { get; set; }
        public string? Source { get; set; }
        public string? Created_At { get; set; }
        public string? Updated_At { get; set; }

        public EvolveUserInfo()
        {
        }
    }
}
