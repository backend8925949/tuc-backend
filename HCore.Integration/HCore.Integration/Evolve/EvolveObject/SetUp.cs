//==================================================================================
// FileName: SetUp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Mono.EvolveObject
{
	public class SetUp
    {
        public string? MerchantId { get; set; }
        public string? ServiceTypeId { get; set; }
        public string? Hash { get; set; }
        public string? PayerName { get; set; }
        public string? PayerEmail { get; set; }
        public string? PayerPhone { get; set; }
        public string? PayerBankCode { get; set; }
        public string? PayerAccount { get; set; }
        public string? RequestId { get; set; }
        public string? Amount { get; set; }
        public string? StartDate { get; set; }
        public string? EndDate { get; set; }
        public string? MandateType { get; set; }
        public string? MaxNoOfDebits { get; set; }

        public SetUp()
		{
		}
	}
}

