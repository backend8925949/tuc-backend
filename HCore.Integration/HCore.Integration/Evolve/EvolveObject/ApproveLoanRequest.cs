//==================================================================================
// FileName: ApproveLoanRequest.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using HCore.Helper;
using Mono.EvolveObject;

namespace Mono.EvolveObject
{
    public class ApproveLoanRequest
    {
        public ApproveLoanRequest(double? Approved_Amount, double Rate, long Tenure, string? Tenured_In, string? Rate_Type, string? Frequency, bool Exclude_Weekends, long? Grace_Period, string? Note)
        {
            approved_amount = Approved_Amount;
            rate = Rate;
            tenure = Tenure;
            tenured_in = Tenured_In;
            rate_type = Rate_Type;
            exclude_weekends = Exclude_Weekends;
            grace_period = Grace_Period;
            note = Note;
            frequency = Frequency;
        }

        public double? approved_amount { get; set; }
        public double rate { get; set; }
        public long tenure { get; set; }
        public string? tenured_in { get; set; }
        public string? rate_type { get; set; }
        public string? frequency { get; set; }
        public bool exclude_weekends { get; set; }
        public long? grace_period { get; set; }
        public string? note { get; set; }
        //public OUserReference userReference { get; set; }
    }
}


