//==================================================================================
// FileName: GetRepayments.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using Mono;

namespace HCore.Integration.Evolve.EvolveObject.Requests
{
    public class GetRepaymentsStructure
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
        public List<GetRepaymentss> Data { get; set; }
    }

    public class GetRepaymentss
    {
        public long Id { get; set; }
        public long User_Id { get; set; }
        public long Org_Id { get; set; }
        public long Loan_Request_Id { get; set; }
        public long Credit_Order_Id { get; set; }
        public long Fulfilment_Id { get; set; }
        public long Loan_Id { get; set; }
        public long Disbursement_Id { get; set; }
        public Borrowers Borrower { get; set; }
        public double Amount { get; set; }
        public double Interest { get; set; }
        public Terms Terms { get; set; }
        public double Evolve_Fee { get; set; }
        public string? Status { get; set; }
        public bool Paid { get; set; }
        public string? Due_On { get; set; }
        public string? Repaid_On { get; set; }
        public bool Manual_Repayment { get; set; }
        public string? Note { get; set; }
        public string? Channel { get; set; }
        public string? Paystack_Ref { get; set; }
        public string? Debit_Instruction_Ref { get; set; }
        public bool Reminder { get; set; }
        public string? Collection_Method { get; set; }
        public string? Evidence_Of_Payment { get; set; }
        public string? Manually_Updated_By { get; set; }
        public double First_Reminder { get; set; }
        public double Second_Reminder { get; set; }
        public double Third_Reminder { get; set; }
        public string? Created_At { get; set; }

    }

    public class Borrowers
    {
        public long Id { get; set; }
        public long Org_Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
        public string? Phone { get; set; }
        public string? Reference { get; set; }
        public bool Is_Active { get; set; }
        public string? Source { get; set; }
        public string? Created_At { get; set; }
        public string? Updated_At { get; set; }
    }
}
