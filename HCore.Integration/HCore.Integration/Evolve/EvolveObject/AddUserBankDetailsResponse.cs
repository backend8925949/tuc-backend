//==================================================================================
// FileName: AddUserBankDetailsResponse.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Mono.EvolveObject
{
	public class AddUserBankDetailsResponse
	{
		public string? Status { get; set; }
		public string? Message { get; set; }
		public AddUserbankdetails Data { get; set; }

		public AddUserBankDetailsResponse()
		{
		}
	}
}

