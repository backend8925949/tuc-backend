//==================================================================================
// FileName: AuthParameters.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Mono.EvolveObject
{
	public class AuthParameters
    {
        public string? Param1 { get; set; }
        public string? Description1 { get; set; }
        public string? Param2 { get; set; }
        public string? Description2 { get; set; }
        public string? Param3 { get; set; }
        public string? Description3 { get; set; }
        public string? Param4 { get; set; }
        public string? Description4 { get; set; }

        public AuthParameters()
		{
		}
	}
}

