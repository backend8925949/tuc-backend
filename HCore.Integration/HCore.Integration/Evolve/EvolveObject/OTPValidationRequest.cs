//==================================================================================
// FileName: OTPValidationRequest.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using System;
namespace Mono.EvolveObject
{
    public class OTPValidationRequest
    {
        public OTPValidationRequest(int Request, string? Param1, string? Value1, string? Param2, string? Value2, string? Param3, string? Value3, string? Param4, string? Value4)
        {
            reqs = Request;
            param1 = Param1;
            value1 = Value1;
            param2 = Param2;
            value2 = Value2;
            param3 = Param3;
            value3 = Value3;
            param4 = Param4;
            value4 = Value4;
        }

        public int reqs { get; set; }
        public string? param1 { get; set; }
        public string? value1 { get; set; }
        public string? param2 { get; set; }
        public string? value2 { get; set; }
        public string? param3 { get; set; }
        public string? value3 { get; set; }
        public string? param4 { get; set; }
        public string? value4 { get; set; }
        public long loanRequestId { get; set; }
        public OUserReference? UserReference { get; set; }
    }
}

