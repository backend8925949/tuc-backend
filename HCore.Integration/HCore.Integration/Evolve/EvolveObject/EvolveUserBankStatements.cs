//==================================================================================
// FileName: EvolveUserBankStatements.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
namespace Mono.EvolveObject
{
    public class BankStatements
    {
        public string? Type { get; set; }
        public string? Narration { get; set; }
        public long Amount { get; set; }
        public string? Date { get; set; }

        public BankStatements(string Type, string? Narration, long Amount, string? Date)
        {
            this.Type = Type;
            this.Narration = Narration;
            this.Amount = Amount;
            this.Date = Date;
        }
    }
}

