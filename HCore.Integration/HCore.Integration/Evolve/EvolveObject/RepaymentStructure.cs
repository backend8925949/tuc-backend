//==================================================================================
// FileName: RepaymentStructure.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Mono.EvolveObject
{
	public class GetLoanRepaymentResponse
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
        public List<LoanRepaymentStructure> Data { get; set; }

        public GetLoanRepaymentResponse()
        {

        }
    }
}

