//==================================================================================
// FileName: LoanRequest.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using System;
namespace Mono.EvolveObject
{
    public class LoanRequest
    {
        public LoanRequest(double? Amount, string? Purpose, long merchantid)
        {
            amount = Amount;
            purpose = Purpose;
            MerchantId = merchantid;
        }

        public double? amount { get; set; }
        public string? purpose { get; set; }
        public long MerchantId { get; set; }
        public int PlanId { get; set; }
        public OUserReference? UserReference { get; set; }
    }
}

