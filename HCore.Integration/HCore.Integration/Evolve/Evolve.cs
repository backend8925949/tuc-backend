// //==================================================================================
// // FileName: Evolve.cs
// // Author : Harshal Gandole
// // Created On : 
// // Description : Class for defining database related functions
// //
// // COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
// //
// // Revision History:
// // Date             : Changed By        : Comments
// // ---------------------------------------------------------------------------------
// //                 | Harshal Gandole   : Created New File
// //
// //
// //==================================================================================

// using System;
// using System.Collections.Generic;
// using System.IO;
// using System.Net;
// using System.Text;
// using Newtonsoft.Json;

// namespace Mono
// {
//     public class EvolveUsers
//     {
//         public static CreateUserResponse CreateUserWithBVN(string FirstName, string? LastName, string? Email, string? Password, string? Phone, string? Gender, string? API_KEY_TEST, string? BVN)
//         {
//             try
//             {
//                 string EvolveUrl = "https://configure-abierta-test.herokuapp.com/api/v1/user/create/with_bvn" + "?product-id=119";
//                 var User = new
//                 {
//                     firstname = FirstName,
//                     lastname = LastName,
//                     email = Email,
//                     password = Password,
//                     phone = Phone,
//                     gender = Gender,
//                 };
//                 var _Request = new
//                 {
//                     user = User,
//                     bvn = BVN,
//                 };
//                 string RequestContent = JsonConvert.SerializeObject(_Request);
//                 HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(EvolveUrl);
//                 byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
//                 _HttpWebRequest.ContentType = "application/json";
//                 _HttpWebRequest.ContentLength = bytes.Length;
//                 _HttpWebRequest.Method = "POST";
//                 _HttpWebRequest.Headers["Authorization"] = "Bearer " + API_KEY_TEST;
//                 Stream _RequestStream = _HttpWebRequest.GetRequestStream();
//                 _RequestStream.Write(bytes, 0, bytes.Length);
//                 _RequestStream.Close();
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     CreateUserResponse _RequestBodyContent = JsonConvert.DeserializeObject<CreateUserResponse>(_Response);
//                     if (_RequestBodyContent != null)
//                     {
//                         return _RequestBodyContent;
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return null;
//             }
//         }

//         public static BankStatementResponse AddBankStatements(long UserId, string? Type, string? Narration, long Amount, long Balance, long Average_Income, long Monthly_Income, long Yearly_Income, string? API_KEY_TEST)
//         {
//             try
//             {
//                 List<TTransaction> _Transaction = new List<TTransaction>();
//                 _Transaction.Add(new TTransaction
//                 {
//                     type = Type,
//                     narration = Narration,
//                     amount = Amount,
//                     date = DateTime.UtcNow.ToString("s") + "Z"

//                 });
//                 _Transaction.Add(new TTransaction
//                 {
//                     type = Type,
//                     narration = Narration,
//                     amount = Amount,
//                     date = DateTime.UtcNow.ToString("s") + "Z"

//                 });
//                 _Transaction.Add(new TTransaction
//                 {
//                     type = Type,
//                     narration = Narration,
//                     amount = Amount,
//                     date = DateTime.UtcNow.ToString("s") + "Z"

//                 });
//                 _Transaction.Add(new TTransaction
//                 {
//                     type = Type,
//                     narration = Narration,
//                     amount = Amount,
//                     date = DateTime.UtcNow.ToString("s") + "Z"

//                 });
//                 var _Income = new
//                 {
//                     average_income = Average_Income,
//                     monthly_income = Monthly_Income,
//                     yearly_income = Yearly_Income,
//                 };
//                 string STransactions = JsonConvert.SerializeObject(_Transaction);
//                 string SIncome = JsonConvert.SerializeObject(_Income);
//                 string EvolveUrl = "https://configure-abierta-test.herokuapp.com/api/v1/user/" + UserId + "/bank_statements?product-id=119";

//                 var _Request = new
//                 {
//                     transactions = _Transaction,
//                     balance = Balance,
//                     income = _Income,
//                 };
//                 string RequestContent = JsonConvert.SerializeObject(_Request);
//                 HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(EvolveUrl);
//                 byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
//                 _HttpWebRequest.ContentType = "application/json";
//                 _HttpWebRequest.ContentLength = bytes.Length;
//                 _HttpWebRequest.Method = "POST";
//                 _HttpWebRequest.Headers["Authorization"] = "Bearer " + API_KEY_TEST;
//                 Stream _RequestStream = _HttpWebRequest.GetRequestStream();
//                 _RequestStream.Write(bytes, 0, bytes.Length);
//                 _RequestStream.Close();
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     BankStatementResponse _RequestBodyContent = JsonConvert.DeserializeObject<BankStatementResponse>(_Response);
//                     if (_RequestBodyContent != null)
//                     {
//                         return _RequestBodyContent;
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return null;
//             }
//         }

//         public static AddMerchantBankDetailsResponse AddMerchantBankDetails(long LoanRequestId, string? Code, string? Account_Name, string? Account_Number, string? Bank_Name, string? API_KEY_TEST)
//         {
//             try
//             {
//                 string EvolveUrl = "https://configure-abierta-test.herokuapp.com/api/v1/loan_request/" + LoanRequestId + "/merchant_bank_details?product-id=119";
//                 var _Request = new
//                 {
//                     code = Code,
//                     account_name = Account_Name,
//                     account_number = Account_Number,
//                     bank_name = Bank_Name,
//                 };
//                 string RequestContent = JsonConvert.SerializeObject(_Request);
//                 HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(EvolveUrl);
//                 byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
//                 _HttpWebRequest.ContentType = "application/json";
//                 _HttpWebRequest.ContentLength = bytes.Length;
//                 _HttpWebRequest.Method = "POST";
//                 _HttpWebRequest.Headers["Authorization"] = "Bearer " + API_KEY_TEST;
//                 Stream _RequestStream = _HttpWebRequest.GetRequestStream();
//                 _RequestStream.Write(bytes, 0, bytes.Length);
//                 _RequestStream.Close();
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     AddMerchantBankDetailsResponse _RequestBodyContent = JsonConvert.DeserializeObject<AddMerchantBankDetailsResponse>(_Response);
//                     if (_RequestBodyContent != null)
//                     {
//                         return _RequestBodyContent;
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return null;
//             }
//         }

//         public static AddUserBankDetailsResponse AddUserBankDetails(long LoanRequestId, string? Code, string? Account_Name, string? Account_Number, string? Bank_Name, string? API_KEY_TEST)
//         {
//             try
//             {
//                 string EvolveUrl = "https://configure-abierta-test.herokuapp.com/api/v1/loan_request/" + LoanRequestId + "/bank_details?product-id=119";
//                 var _Request = new
//                 {
//                     code = Code,
//                     account_name = Account_Name,
//                     account_number = Account_Number,
//                     bank_name = Bank_Name,
//                 };
//                 string RequestContent = JsonConvert.SerializeObject(_Request);
//                 HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(EvolveUrl);
//                 byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
//                 _HttpWebRequest.ContentType = "application/json";
//                 _HttpWebRequest.ContentLength = bytes.Length;
//                 _HttpWebRequest.Method = "POST";
//                 _HttpWebRequest.Headers["Authorization"] = "Bearer " + API_KEY_TEST;
//                 Stream _RequestStream = _HttpWebRequest.GetRequestStream();
//                 _RequestStream.Write(bytes, 0, bytes.Length);
//                 _RequestStream.Close();
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     AddUserBankDetailsResponse _RequestBodyContent = JsonConvert.DeserializeObject<AddUserBankDetailsResponse>(_Response);
//                     if (_RequestBodyContent != null)
//                     {
//                         return _RequestBodyContent;
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return null;
//             }
//         }
//     }

//     public class Loan
//     {
//         public static LoanResponse CreateLoanRequest(long Amount, string? Purpose, long Id, string? API_KEY_TEST)
//         {

//             try
//             {
//                 string EvolveUrl = "https://configure-abierta-test.herokuapp.com/api/v1/user/" + Id + "/loan_request?product-id=119";
//                 var _Request = new
//                 {
//                     amount = Amount,
//                     purpose = Purpose,
//                 };
//                 string RequestContent = JsonConvert.SerializeObject(_Request);
//                 HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(EvolveUrl);
//                 byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
//                 _HttpWebRequest.ContentType = "application/json";
//                 _HttpWebRequest.ContentLength = bytes.Length;
//                 _HttpWebRequest.Method = "POST";
//                 _HttpWebRequest.Headers["Authorization"] = "Bearer " + API_KEY_TEST;
//                 Stream _RequestStream = _HttpWebRequest.GetRequestStream();
//                 _RequestStream.Write(bytes, 0, bytes.Length);
//                 _RequestStream.Close();
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     LoanResponse _RequestBodyContent = JsonConvert.DeserializeObject<LoanResponse>(_Response);
//                     if (_RequestBodyContent != null)
//                     {
//                         return _RequestBodyContent;
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return null;
//             }
//         }

//         public static SubmitLoanResponse SubmitLoan(long Loan_Request_Id, string? API_KEY_TEST)
//         {
//             try
//             {
//                 string EvolveUrl = "https://configure-abierta-test.herokuapp.com/api/v1/loan_request/" + Loan_Request_Id + "/submit?product-id=119";

//                 HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(EvolveUrl);
//                 _HttpWebRequest.ContentType = "application/json";
//                 _HttpWebRequest.Method = "PUT";
//                 _HttpWebRequest.Headers["Authorization"] = "Bearer " + API_KEY_TEST;
//                 Stream _RequestStream = _HttpWebRequest.GetRequestStream();
//                 _RequestStream.Close();
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     SubmitLoanResponse _RequestBodyContent = JsonConvert.DeserializeObject<SubmitLoanResponse>(_Response);
//                     if (_RequestBodyContent != null)
//                     {
//                         return _RequestBodyContent;
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return null;
//             }
//         }

//         public static ApproveLoanResponse ApproveLoan(long LoanRequestId, long Approved_Amount, double Rate, long Tenure, string? Tenured_In, string? Rate_Type, bool Exclude_Weekends, long Grace_Period, string? Note, string? API_KEY_TEST)
//         {
//             try
//             {
//                 string EvolveUrl = "https://configure-abierta-test.herokuapp.com/api/v1/loan_request/" + LoanRequestId + "/action?action=approve&product-id=119";
//                 var _Request = new
//                 {
//                     approved_amount = Approved_Amount,
//                     rate = Rate,
//                     tenure = Tenure,
//                     tenured_in = Tenured_In,
//                     rate_type = Rate_Type,
//                     exclude_weekends = Exclude_Weekends,
//                     grace_period = Grace_Period,
//                     note = Note
//                 };
//                 string RequestContent = JsonConvert.SerializeObject(_Request);
//                 HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(EvolveUrl);
//                 byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
//                 _HttpWebRequest.ContentType = "application/json";
//                 _HttpWebRequest.ContentLength = bytes.Length;
//                 _HttpWebRequest.Method = "POST";
//                 _HttpWebRequest.Headers["Authorization"] = "Bearer " + API_KEY_TEST;
//                 Stream _RequestStream = _HttpWebRequest.GetRequestStream();
//                 _RequestStream.Write(bytes, 0, bytes.Length);
//                 _RequestStream.Close();
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     ApproveLoanResponse _RequestBodyContent = JsonConvert.DeserializeObject<ApproveLoanResponse>(_Response);
//                     if (_RequestBodyContent != null)
//                     {
//                         return _RequestBodyContent;
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return null;
//             }
//         }

//         public static DeclineLoanResponse DeclineLoan(long LoanRequestId, long Approved_Amount, double Rate, long Tenure, string? Tenured_In, string? Rate_Type, bool Exclude_Weekends, long Grace_Period, string? Note, string? API_KEY_TEST)
//         {
//             try
//             {
//                 string EvolveUrl = "https://configure-abierta-test.herokuapp.com/api/v1/loan_request/" + LoanRequestId + "/action?action=decline&product-id=119";
//                 var _Request = new
//                 {
//                     approved_amount = Approved_Amount,
//                     rate = Rate,
//                     tenure = Tenure,
//                     tenured_in = Tenured_In,
//                     rate_type = Rate_Type,
//                     exclude_weekends = Exclude_Weekends,
//                     grace_period = Grace_Period,
//                     note = Note
//                 };
//                 string RequestContent = JsonConvert.SerializeObject(_Request);
//                 HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(EvolveUrl);
//                 byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
//                 _HttpWebRequest.ContentType = "application/json";
//                 _HttpWebRequest.ContentLength = bytes.Length;
//                 _HttpWebRequest.Method = "POST";
//                 _HttpWebRequest.Headers["Authorization"] = "Bearer " + API_KEY_TEST;
//                 Stream _RequestStream = _HttpWebRequest.GetRequestStream();
//                 _RequestStream.Write(bytes, 0, bytes.Length);
//                 _RequestStream.Close();
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     DeclineLoanResponse _RequestBodyContent = JsonConvert.DeserializeObject<DeclineLoanResponse>(_Response);
//                     if (_RequestBodyContent != null)
//                     {
//                         return _RequestBodyContent;
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return null;
//             }
//         }

//         public static GetLoanRepaymentResponse GetRepaymentStructure(long Loan_Request_Id, string? API_KEY_TEST)
//         {
//             try
//             {
//                 string EvolveUrl = "https://configure-abierta-test.herokuapp.com/api/v1/loan_request/" + Loan_Request_Id + "/repayment?product-id=119";
//                 HttpWebRequest request = (HttpWebRequest)WebRequest.Create(EvolveUrl);
//                 request.ContentType = "application/json";
//                 request.Method = "GET";
//                 request.Headers["Authorization"] = "Bearer " + API_KEY_TEST;
//                 request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
//                 using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
//                 using (Stream stream = response.GetResponseStream())
//                 using (StreamReader reader = new StreamReader(stream))
//                 {
//                     string _Response = reader.ReadToEnd();
//                     GetLoanRepaymentResponse _RequestBodyContent = JsonConvert.DeserializeObject<GetLoanRepaymentResponse>(_Response);
//                     if (_RequestBodyContent != null)
//                     {
//                         return _RequestBodyContent;
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return null;
//             }
//         }
//     }

//     public class DebitMandate
//     {
//         public static SetUpDebitMandateResponse SetUpDebitMandate(long Loan_Request_Id, string? API_KEY_TEST)
//         {
//             try
//             {
//                 string EvolveUrl = "https://configure-abierta-test.herokuapp.com/api/v1/" + Loan_Request_Id + "/debit_mandate?product-id=119&action=setup&vendor=remita&resource-name=loan_request_id";

//                 HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(EvolveUrl);
//                 _HttpWebRequest.ContentType = "application/json";
//                 _HttpWebRequest.Method = "POST";
//                 _HttpWebRequest.Headers["Authorization"] = "Bearer " + API_KEY_TEST;
//                 Stream _RequestStream = _HttpWebRequest.GetRequestStream();
//                 _RequestStream.Close();
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     SetUpDebitMandateResponse _RequestBodyContent = JsonConvert.DeserializeObject<SetUpDebitMandateResponse>(_Response);
//                     if (_RequestBodyContent != null)
//                     {
//                         return _RequestBodyContent;
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return null;
//             }
//         }

//         public static RequestDebitMandateResponse RequestDebitMandateOtp(long Resource_Id, string? API_KEY_TEST)
//         {
//             try
//             {
//                 string EvolveUrl = "https://configure-abierta-test.herokuapp.com/api/v1/" + Resource_Id + "/debit_mandate?vendor=remita&resource-name=loan_request_id&action=request-otp&product-id=119";

//                 HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(EvolveUrl);
//                 _HttpWebRequest.ContentType = "application/json";
//                 _HttpWebRequest.Method = "POST";
//                 _HttpWebRequest.Headers["Authorization"] = "Bearer " + API_KEY_TEST;
//                 Stream _RequestStream = _HttpWebRequest.GetRequestStream();
//                 _RequestStream.Close();
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     RequestDebitMandateResponse _RequestBodyContent = JsonConvert.DeserializeObject<RequestDebitMandateResponse>(_Response);
//                     if (_RequestBodyContent != null)
//                     {
//                         return _RequestBodyContent;
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return null;
//             }
//         }

//         public static GetDebitMandateResponse GetDebitMandate(long Resource_Id, string? API_KEY_TEST)
//         {
//             try
//             {
//                 string EvolveUrl = "https://configure-abierta-test.herokuapp.com/api/v1/" + Resource_Id + "/debit_mandate?vendor=remita&resource-name=loan_request_id&action=get-mandate&product-id=119";

//                 HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(EvolveUrl);
//                 _HttpWebRequest.ContentType = "application/json";
//                 _HttpWebRequest.Method = "POST";
//                 _HttpWebRequest.Headers["Authorization"] = "Bearer " + API_KEY_TEST;
//                 Stream _RequestStream = _HttpWebRequest.GetRequestStream();
//                 _RequestStream.Close();
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     GetDebitMandateResponse _RequestBodyContent = JsonConvert.DeserializeObject<GetDebitMandateResponse>(_Response);
//                     if (_RequestBodyContent != null)
//                     {
//                         return _RequestBodyContent;
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return null;
//             }
//         }

//         public static ValidateOtpResponse ValidateOtp(long Request, string? OTP, string? Card_Number, string? Param2, string? Value2, string? Param3, string? Value3, string? Param4, string? Value4, long Resource_Id, string? API_KEY_TEST)
//         {
//             try
//             {
//                 string EvolveUrl = "https://configure-abierta-test.herokuapp.com/api/v1/" + Resource_Id + "/debit_mandate?vendor=remita&resource-name=loan_request_id&product-id=119&action=validate-otp";
//                 var _Request = new
//                 {
//                     reqs = Request,
//                     param1 = OTP,
//                     value1 = Card_Number,
//                     param2 = Param2,
//                     value2 = Value2,
//                     param3 = Param3,
//                     value3 = Value3,
//                     param4 = Param4,
//                     value4 = Value4,
//                 };
//                 string RequestContent = JsonConvert.SerializeObject(_Request);
//                 HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(EvolveUrl);
//                 byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
//                 _HttpWebRequest.ContentType = "application/json";
//                 _HttpWebRequest.ContentLength = bytes.Length;
//                 _HttpWebRequest.Method = "POST";
//                 _HttpWebRequest.Headers["Authorization"] = "Bearer " + API_KEY_TEST;
//                 Stream _RequestStream = _HttpWebRequest.GetRequestStream();
//                 _RequestStream.Write(bytes, 0, bytes.Length);
//                 _RequestStream.Close();
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     Console.WriteLine(_Response);
//                     ValidateOtpResponse _RequestBodyContent = JsonConvert.DeserializeObject<ValidateOtpResponse>(_Response);
//                     if (_RequestBodyContent != null)
//                     {
//                         return _RequestBodyContent;
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return null;
//             }
//         }
//     }

//     public class LoanDisbursement
//     {
//         public static DisbursementResponse DisburseLoan(long Loan_Request_Id, string? Note, string? API_KEY_TEST)
//         {
//             try
//             {
//                 string EvolveUrl = "https://configure-abierta-test.herokuapp.com/api/v1/loan_request/" + Loan_Request_Id + "/disburse?channel=disbursement-wallet" + "&note=" + Note;

//                 HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(EvolveUrl);
//                 _HttpWebRequest.ContentType = "application/json";
//                 _HttpWebRequest.Method = "POST";
//                 _HttpWebRequest.Headers["Authorization"] = "Bearer " + API_KEY_TEST;
//                 Stream _RequestStream = _HttpWebRequest.GetRequestStream();
//                 _RequestStream.Close();
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     DisbursementResponse _RequestBodyContent = JsonConvert.DeserializeObject<DisbursementResponse>(_Response);
//                     if (_RequestBodyContent != null)
//                     {
//                         return _RequestBodyContent;
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return null;
//             }
//         }
//     }
// }
