// //==================================================================================
// // FileName: ManageAPIResponse.cs
// // Author : Harshal Gandole
// // Created On : 
// // Description : Class for defining database related functions
// //
// // COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
// //
// // Revision History:
// // Date             : Changed By        : Comments
// // ---------------------------------------------------------------------------------
// //                 | Harshal Gandole   : Created New File
// //
// //
// //==================================================================================

// using HCore.Helper;
// using HCore.Integration.Evolve.AppAPI.Framework;
// using Mono.EvolveObject;
// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Text;
// using System.Threading.Tasks;

// namespace HCore.Integration.Evolve.AppAPI
// {
//     public class ManageAPIResponse
//     {
//         FrameworkAPIResponse _frameworkAPIResponse;
//         public void SaveDebitMandate(SetUpDebitMandateResponse setUpDebitMandateResponse, OUserReference UserReference)
//         {
//             _frameworkAPIResponse = new FrameworkAPIResponse();
//             _frameworkAPIResponse.SaveDebitMandate(setUpDebitMandateResponse, UserReference);
//         }

//         public void SaveLoanProcess(CreateLoanRequestResponse createLoanRequestResponse, OUserReference UserReference)
//         {
//             _frameworkAPIResponse = new FrameworkAPIResponse();
//             _frameworkAPIResponse.SaveLoanProcess(createLoanRequestResponse, UserReference);
//         }
//         public void SubmitLoanResponse(CreateLoanRequestResponse createLoanRequestResponse, OUserReference UserReference)
//         {
//             _frameworkAPIResponse = new FrameworkAPIResponse();
//             _frameworkAPIResponse.SubmitLoanResponse(createLoanRequestResponse, UserReference);
//         }
//         public void ApproveLoanResponse(ApproveLoanResponse approveLoanResponse, OUserReference UserReference)
//         {
//             _frameworkAPIResponse = new FrameworkAPIResponse();
//             _frameworkAPIResponse.ApproveLoanResponse(approveLoanResponse, UserReference);
//         }
//         public void UpdatePrequalification(BankDetails bankDetails, AddUserBankDetailsResponse addUserBankDetailsResponse, OUserReference UserReference)
//         {
//             _frameworkAPIResponse = new FrameworkAPIResponse();
//             _frameworkAPIResponse.UpdatePrequalification(bankDetails, addUserBankDetailsResponse, UserReference);
//         }
//         public void SaveUser(CreateUserResponse createUserResponse, OUserReference UserReference, string? bvn)
//         {
//             _frameworkAPIResponse = new FrameworkAPIResponse();
//             _frameworkAPIResponse.SaveUser(createUserResponse, UserReference, bvn);
//         }
//         public bool CheckUserExists(CreateUserRequest createUserRequest)
//         {
//             _frameworkAPIResponse = new FrameworkAPIResponse();
//             return _frameworkAPIResponse.CheckUserExists(createUserRequest);
//         }
//         public void SavePrequalification(AddBankStatementResponse addBankStatementResponse, OUserReference UserReference)
//         {
//             _frameworkAPIResponse = new FrameworkAPIResponse();
//             _frameworkAPIResponse.SavePrequalification(addBankStatementResponse, UserReference);
//         }
//     }
// }
