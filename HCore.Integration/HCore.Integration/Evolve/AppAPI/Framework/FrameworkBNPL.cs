// //==================================================================================
// // FileName: FrameworkBNPL.cs
// // Author : Harshal Gandole
// // Created On : 
// // Description : Class for defining database related functions
// //
// // COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
// //
// // Revision History:
// // Date             : Changed By        : Comments
// // ---------------------------------------------------------------------------------
// //                 | Harshal Gandole   : Created New File
// //
// //
// //==================================================================================

// ﻿using System;
// using HCore.Data;
// using HCore.Data.Models;
// using HCore.Helper;
// using static HCore.Helper.HCoreConstant;
// using Mono.EvolveObject;
// using System.Linq;
// using static HCore.Helper.HCoreConstant.Helpers;
// using HCore.Integration.BNPLHelpers;
// using System.Collections.Generic;
// using HCore.Integration.Evolve.EvolveObject;
// using HCore.Integration.Evolve.EvolveObject.Requests;

// namespace HCore.Integration.Evolve.AppAPI.Framework
// {
//     public class FrameworkBNPL
//     {
//         HCUAccount hcuaccount;
//         BNPLAccount bnplaccount;
//         BNPLAccountLoan bnplaccountloan;
//         BNPLAccountLoanPayment bnplaccountloanpayment;
//         BNPLLoanProcess _BNPLLoanProcess;
//         BNPLPrequalification bnplprequalification;
//         BNPLAccountLoanActivity _BNPLAccountLoanActivity;
//         HCoreContext _HCoreContext;
//         OUserReference UserReference;
//         EvolveOperations _EvolveOperations;
//         HCore.Integration.Mono.AccountAuthentication _AccountAuthentication;
//         OLoanActivity.Save _SaveLoanActivity;

//         internal OResponse SaveCustomer(CreateUserRequest Request, CreateUserResponse Response)
//         {
//             try
//             {
//                 _AccountAuthentication = new Mono.AccountAuthentication();
//                 var _AccountStatements = _AccountAuthentication.GetAccountStatement(Request.monoReference);
//                 if (_AccountStatements != null)
//                 {
//                     IncomeDetails _Income = new IncomeDetails
//                     {
//                         Average_Income = 0,
//                         Monthly_Income = 0,
//                         Yearly_Income = 0
//                     };
//                     var _AccountIncome = _AccountAuthentication.GetIncome(Request.monoReference);
//                     if (_AccountIncome != null)
//                     {
//                         _Income.Average_Income = _AccountIncome.Average_Income;
//                         _Income.Monthly_Income = _AccountIncome.Average_Income;
//                         _Income.Yearly_Income = _AccountIncome.Average_Income;
//                     }
//                     List<BankStatements> _BankStatements = new List<BankStatements>();
//                     foreach (var item in _AccountStatements.data)
//                     {
//                         BankStatements BankStatementsItem = new BankStatements(item.Type, item.Narration, item.Amount, item.Date);
//                         _BankStatements.Add(BankStatementsItem);
//                     }

//                     List<BankStatements> bankStatements = new List<BankStatements>();
//                     AddBankStatementRequest _AddBankStatementRequest = new AddBankStatementRequest(_BankStatements, Request.balance, _Income);
//                     _EvolveOperations = new EvolveOperations();
//                     var BankStatementResponse = _EvolveOperations.AddBankStatementCore(_AddBankStatementRequest, Response.Data.Id, 119);
//                     if (BankStatementResponse != null)
//                     {
//                         //if(BankStatementResponse.Pre_Qualified_Amount <= 0)
//                         //{
//                         //    return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPLELIGIBLE", "You are not eligible for By Now, Pay Later. Because your credit limit is &#8358; " + BankStatementResponse.Pre_Qualified_Amount + ". For getting eligible for By Now, Pay Later your credit limit should be greater than 0");
//                         //}
//                         using (_HCoreContext = new HCoreContext())
//                         {
//                             bool Exists = _HCoreContext.BNPLAccount.Any(a => a.EmailAddress == Response.Data.Email);
//                             if (Exists)
//                             {
//                                 return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPLEXIST", "Your account already linked to BNPL.");
//                             }
//                             var Details = _HCoreContext.HCUAccount.Where(z => z.Id == Request.UserReference.AccountId && z.AccountTypeId == UserAccountType.Appuser).FirstOrDefault();
//                             bnplaccount = new BNPLAccount();
//                             bnplaccount.Guid = HCoreHelper.GenerateGuid();
//                             bnplaccount.AccountId = Details.Id;
//                             bnplaccount.VenderUserId = Response.Data.Id;
//                             bnplaccount.CreateDate = HCoreHelper.GetGMTDateTime();
//                             bnplaccount.CreatedById = Details.Id;
//                             bnplaccount.FirstName = Details.FirstName;
//                             bnplaccount.LastName = Details.LastName;
//                             bnplaccount.EmailAddress = Details.EmailAddress;
//                             bnplaccount.MobileNumber = Details.MobileNumber;
//                             bnplaccount.Address = Details.Address;
//                             bnplaccount.Latitude = Details.Latitude;
//                             bnplaccount.Longitude = Details.Longitude;
//                             bnplaccount.BvnNumber = Request.bvn;
//                             bnplaccount.MonoReferenceNumber = Request.monoReference;
//                             bnplaccount.MonoReferenceBalance = Request.balance;

//                             bnplaccount.BankName = Request.Bank.Name;
//                             bnplaccount.BankCode = Request.Bank.Code;
//                             bnplaccount.BankType = Request.Bank.Type;
//                             bnplaccount.BankAccountName = Request.Bank.AccountName;
//                             bnplaccount.BankAccountNumber = Request.Bank.AccountNumber;

//                             if (BankStatementResponse.Pre_Qualified_Amount > 0)
//                             {
//                                 bnplaccount.CreditLimit = BankStatementResponse.Pre_Qualified_Amount;
//                                 bnplaccount.AvailableCreditLimit = BankStatementResponse.Pre_Qualified_Amount;
//                                 bnplaccount.StatusId = HelperStatus.Default.Active;
//                             }
//                             else
//                             {
//                                 bnplaccount.ModifyDate = HCoreHelper.GetGMTDateTime().AddMonths(3);
//                                 bnplaccount.StatusId = HelperStatus.Default.Inactive;
//                             }
//                             _HCoreContext.BNPLAccount.Add(bnplaccount);
//                             _HCoreContext.SaveChanges();
//                             _HCoreContext.Dispose();

//                             if (BankStatementResponse.Pre_Qualified_Amount <= 0)
//                             {
//                                 return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPLELIGIBLE", "Oops! Based on your current bank statement, we are unable to process your request. But you can try after 3 months.");
//                             }

//                             if (!string.IsNullOrEmpty(Details.EmailAddress))
//                             {
//                                 var _EmailParameters = new
//                                 {
//                                     AccountCode = Details.AccountCode,
//                                     DisplayName = Details.FirstName + " " + Details.LastName,
//                                     CreditLimnit = bnplaccount.CreditLimit,
//                                 };
//                                 HCoreHelper.BroadCastEmail(NotificationTemplates.CheckCreditLimit, Details.FirstName, Details.EmailAddress, _EmailParameters, UserReference);
//                                 //HCoreHelper.BroadCastEmail(NotificationTemplates.CheckCreditLimit, Details.FirstName, "omkar@thankucash.com", _EmailParameters, UserReference);
//                             }
//                             var response = new
//                             {
//                                 ReferenceId = bnplaccount.Id,
//                                 ReferenceKey = bnplaccount.Guid,
//                                 AccountId = Details.Id,
//                                 AccountKey = Details.Guid
//                             };
//                             return HCoreHelper.SendResponse(UserReference, ResponseStatus.Success, response, "BNPLSAVED", "Customer details saved successfully.");
//                         }
//                     }
//                     else
//                     {
//                         return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPLABS", "Unable to verify your bank statement. Please try with other bank");
//                     }
//                 }
//                 else
//                 {
//                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPLACCSTAT", "Unable to verify your bank account. Please try with other bank");
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return HCoreHelper.LogException("Save Customer", _Exception, UserReference);
//             }
//         }

//         internal OResponse UpdateCustomer(AddBankStatementResponse Response)
//         {
//             try
//             {
//                 if (Response.Pre_Qualified_Amount <= 0)
//                 {
//                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL002", "Prequalified amount shuold be greater than 0");
//                 }

//                 using (_HCoreContext = new HCoreContext())
//                 {
//                     bool Exists = _HCoreContext.BNPLAccount.Any(x => x.VenderUserId == Response.Data.User_Id);
//                     if (!Exists)
//                     {
//                         return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL003", "User doesn't exists.");
//                     }

//                     var Details = _HCoreContext.BNPLAccount.Where(x => x.VenderUserId == Response.Data.User_Id).FirstOrDefault();
//                     if (Details != null)
//                     {
//                         if (Response.Pre_Qualified_Amount >= 0)
//                         {
//                             Details.CreditLimit = Response.Pre_Qualified_Amount;
//                         }
//                         Details.ModifyDate = HCoreHelper.GetGMTDateTime();
//                         Details.ModifyById = Details.AccountId;
//                         _HCoreContext.SaveChanges();

//                         var response = new
//                         {
//                             ReferenceId = Details.Id,
//                             ReferenceKey = Details.Guid
//                         };
//                         return HCoreHelper.SendResponse(UserReference, ResponseStatus.Success, response, "BNPLUPDATE", "Customer details updated successfully.");
//                     }
//                     else
//                     {
//                         return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL404", "Details not found.");
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return HCoreHelper.LogException("Update Customer", _Exception, UserReference);
//             }
//         }

//         internal OResponse SaveLoan(CreateLoanRequestResponse Response, LoanRequest Request)
//         {
//             try
//             {
//                 if (Response.Data.Id <= 0)
//                 {
//                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL004", "Loan request doesn't exists.");
//                 }
//                 if (Request.amount <= 0)
//                 {
//                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL005", "Loan request amount should be greater than 0.");
//                 }

//                 //SubmitLoanRequest
//                 _EvolveOperations = new EvolveOperations();
//                 var _ResponseItem = _EvolveOperations.SubmitLoanRequestCore(119, Response.Data.Id, UserReference);
//                 if (_ResponseItem != null)
//                 {
//                     using (_HCoreContext = new HCoreContext())
//                     {
//                         //long MerchantId = _HCoreContext.BNPLMerchant.Where(x => x.AccountId == Request.MerchantId).Select(a => a.Id).FirstOrDefault();
//                         var Details = _HCoreContext.BNPLAccount.Where(a => a.VenderUserId == Response.Data.User_Id).FirstOrDefault();
//                         var PlanDetails = _HCoreContext.BNPLPlan.Where(m => m.Id == Request.PlanId).FirstOrDefault();
//                         bool IsExists = _HCoreContext.BNPLAccount.Any(x => x.VenderUserId == Response.Data.User_Id);
//                         if (!IsExists)
//                         {
//                             return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL006", "User doesnt exists.");
//                         }

//                         #region Save data is BNPLAccountLoan table
//                         bnplaccountloan = new BNPLAccountLoan();
//                         bnplaccountloan.Guid = HCoreHelper.GenerateGuid();
//                         bnplaccountloan.AccountId = Details.Id;
//                         bnplaccountloan.MerchantId = Request.MerchantId;
//                         bnplaccountloan.Amount = Response.Data.Amount;
//                         bnplaccountloan.VenderLoanId = Response.Data.Id;
//                         bnplaccountloan.PlanId = PlanDetails.Id;
//                         bnplaccountloan.Charge = (Response.Data.Amount / 100) * 2;
//                         bnplaccountloan.TotalAmount = bnplaccountloan.Charge + bnplaccountloan.Amount;
//                         bnplaccountloan.LoanCode = "45" + HCoreHelper.GenerateRandomNumber(9);
//                         bnplaccountloan.LoanPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
//                         bnplaccountloan.CreateDate = HCoreHelper.GetGMTDateTime();
//                         bnplaccountloan.CreatedById = Details.AccountId;
//                         bnplaccountloan.StatusId = BNPLHelper.StatusHelper.Loan.Submitted;
//                         _HCoreContext.BNPLAccountLoan.Add(bnplaccountloan);
//                         _HCoreContext.SaveChanges();
//                         #endregion

//                         #region Save data is BNPLAccountLoan table
//                         _BNPLLoanProcess = new BNPLLoanProcess();
//                         _BNPLLoanProcess.Guid = HCoreHelper.GenerateGuid();
//                         _BNPLLoanProcess.Loan = bnplaccountloan;
//                         _BNPLLoanProcess.UserId = Response.Data.User_Id;
//                         _BNPLLoanProcess.Loan_Product_Id = Response.Data.Product_Id;
//                         _BNPLLoanProcess.Loan_Plan_Id = (int?)Response.Data.Product_Id;
//                         _BNPLLoanProcess.Vendor_Loan_Reference = Response.Data.Reference;
//                         _BNPLLoanProcess.Vendor_Loan_Amount = Response.Data.Amount;
//                         _BNPLLoanProcess.Vendor_Loan_Purpose = Response.Data.Purpose;
//                         _BNPLLoanProcess.Vendor_Loan_Status = Response.Data.Status;
//                         _BNPLLoanProcess.Lender_Name = Response.Data.Lender_Name;
//                         _BNPLLoanProcess.Lender_Status = Response.Data.Lender_Status;
//                         _BNPLLoanProcess.Organization_Id = Response.Data.Org_Id;
//                         _BNPLLoanProcess.Debit_Mandate_Next_Step = Response.Data.Debit_Mandate_Next_Step;
//                         _BNPLLoanProcess.Loan_Submite_Date = DateTime.Parse(Response.Data.Submitted_At);
//                         _BNPLLoanProcess.Create_Date = HCoreHelper.GetGMTDateTime();
//                         _BNPLLoanProcess.Created_By_Id = Details.AccountId;
//                         _BNPLLoanProcess.Status_Id = BNPLHelper.StatusHelper.Loan.Submitted;
//                         _HCoreContext.BNPLLoanProcess.Add(_BNPLLoanProcess);
//                         _HCoreContext.SaveChanges();
//                         #endregion

//                         #region Update Credit Limit
//                         if(bnplaccountloan.Amount > 0)
//                         {
//                             Details.AvailableCreditLimit = Details.CreditLimit - bnplaccountloan.Amount;
//                         }
//                         #endregion

//                         #region Save Loan Activity
//                         _BNPLAccountLoanActivity = new BNPLAccountLoanActivity();
//                         _BNPLAccountLoanActivity.Guid = HCoreHelper.GenerateGuid();
//                         _BNPLAccountLoanActivity.Loan = bnplaccountloan;
//                         _BNPLAccountLoanActivity.IsSystemActivity = 1;
//                         _BNPLAccountLoanActivity.CreateDate = HCoreHelper.GetGMTDateTime();
//                         _BNPLAccountLoanActivity.StatusId = HCoreConstant.HelperStatus.Default.Active;
//                         if (bnplaccountloan.StatusId == BNPLHelper.StatusHelper.Loan.Submitted)
//                         {
//                             _BNPLAccountLoanActivity.Description = "Loan Request Raised";
//                         }
//                         _HCoreContext.BNPLAccountLoanActivity.Add(_BNPLAccountLoanActivity);
//                         _HCoreContext.SaveChanges();
//                         #endregion
//                         var MerchantDetails = _HCoreContext.BNPLAccountLoan.Where(x => x.Id == bnplaccountloan.Id).Select(a => new
//                         {
//                             MerchantName = a.Merchant.Account.DisplayName
//                         }).FirstOrDefault();
//                         _HCoreContext.Dispose();

//                         if (!string.IsNullOrEmpty(Details.EmailAddress))
//                         {
//                             var _EmailParameters = new
//                             {
//                                 DisplayName = Details.FirstName + " " + Details.LastName,
//                                 LoanAmount = bnplaccountloan.Amount,
//                                 InterestRate = PlanDetails.ProviderInterestRate,
//                                 PlanName = PlanDetails.Name
//                             };
//                             HCoreHelper.BroadCastEmail(NotificationTemplates.CreateLoanRequest, Details.FirstName, Details.EmailAddress, _EmailParameters, UserReference);
//                             //HCoreHelper.BroadCastEmail(NotificationTemplates.CreateLoanRequest, Details.FirstName, "omkar@thankucash.com", _EmailParameters, UserReference);
//                         }

//                         var response = new
//                         {
//                             Id = _BNPLLoanProcess.Id,
//                             Key = _BNPLLoanProcess.Guid,
//                             ReferenceId = bnplaccountloan.Id,
//                             ReferenceKey = bnplaccountloan.Guid,
//                             PartnerId = Response.Data.Id,
//                         };
//                         double? LoanAMount = (Response.Data.Amount / 4) * 3;
//                         var _ApproveLoanReq = new ApproveLoanRequest(LoanAMount, PlanDetails.ProviderInterestRate, PlanDetails.Tenture, PlanDetails.TenuredIn, "compound-intereset", PlanDetails.Frequency, false, PlanDetails.PlanGracePeriod, "Loan");
//                         var _BankReq = new BankDetails(Details.BankCode, Details.BankAccountName, Details.BankAccountNumber, Details.BankName);
//                         var _ResponseItemBank = _EvolveOperations.AddUserbankDetailsCore(_BankReq, 119, Response.Data.Id);
//                         if (_ResponseItemBank != null)
//                         {
//                             var _ResponseApproveLoanItem = _EvolveOperations.AddMerchantbankDetailsCore(119, Response.Data.Id);
//                             if (_ResponseApproveLoanItem != null)
//                             {
//                                 var _ResponseApproveLoan = _EvolveOperations.ApproveLoanRequestCore(_ApproveLoanReq, 119, Response.Data.Id);
//                                 if (_ResponseApproveLoan != null)
//                                 {
//                                     var _ResponseItems = ApproveLoan(_ResponseApproveLoan);
//                                     if (_ResponseItems.Status == StatusSuccess)
//                                     {
//                                         var _ResponseSetupDebitMandate = _EvolveOperations.SetUpDebitMandate(Response.Data.Id);
//                                         if (_ResponseSetupDebitMandate != null)
//                                         {
//                                             var _SetupdebitMandate = SetUpDebitMandate(_ResponseSetupDebitMandate);
//                                             if (_SetupdebitMandate != null)
//                                             {
//                                                 var _RequestDebitMandateOTP = _EvolveOperations.RequestDebitMandateOTPCore(119, Response.Data.Id);
//                                                 if (_RequestDebitMandateOTP != null)
//                                                 {
//                                                     _RequestDebitMandateOTP.Data.LoanRequestId = Response.Data.Id;
//                                                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Success, _RequestDebitMandateOTP, "BNPLSAVED", "Loan approved");
//                                                 }
//                                                 else
//                                                 {
//                                                     //RequestDebitMandateOTPResponse _RequestDebitMandateOTPResponse = new RequestDebitMandateOTPResponse();
//                                                     //_RequestDebitMandateOTPResponse.Status = "00";
//                                                     //_RequestDebitMandateOTPResponse.Message = "Success";
//                                                     //List<AuthParameters> AuthParams = new List<AuthParameters>();
//                                                     //AuthParams.Add(new AuthParameters
//                                                     //{
//                                                     //    Param1 = "OTP",
//                                                     //    Description1 = "Please enter your Bank OTP",
//                                                     //    Param2 = "Please specify the last 4 digits of your bank card",
//                                                     //    Description2 = "",
//                                                     //    Param3 = "",
//                                                     //    Description3 = "",
//                                                     //    Param4 = "",
//                                                     //    Description4 = "",
//                                                     //});
//                                                     //_RequestDebitMandateOTPResponse.Data = new RequestMandateOTPResponse();
//                                                     //_RequestDebitMandateOTPResponse.Data.AuthParams = AuthParams;
//                                                     //_RequestDebitMandateOTPResponse.Data.Status = "00";
//                                                     //_RequestDebitMandateOTPResponse.Data.StatusCode = "00";
//                                                     //_RequestDebitMandateOTPResponse.Data.RequestId = "uyvkbyu78";
//                                                     //_RequestDebitMandateOTPResponse.Data.MandateId = "MNDT78978";
//                                                     //_RequestDebitMandateOTPResponse.Data.LoanRequestId = Response.Data.Id;
//                                                     //_RequestDebitMandateOTPResponse.Data.RemitaTransRef = "hu8gg7h98g";
//                                                     //_RequestDebitMandateOTPResponse.Data.Message = "OTP sent successfully";

//                                                     //return HCoreHelper.SendResponse(UserReference, ResponseStatus.Success, _RequestDebitMandateOTPResponse, "BNPLSAVED", "Loan Approved");
//                                                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPLSAVED", "Your bank could not be reached to setup your debit mandate, please try again after some time");
//                                                 }
//                                             }
//                                             else
//                                             {
//                                                 return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPLSAVED", "Set up debit mandate failed. Please try after some time");
//                                             }
//                                         }
//                                         else
//                                         {
//                                             return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPLSAVED", "Set up debit mandate failed. Please try after some time");
//                                         }

//                                     }
//                                     else
//                                     {
//                                         return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPLSAVED", "Loan approval failed. Please try after some time");
//                                     }
//                                 }
//                                 else
//                                 {
//                                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPLSAVED", "Loan approval failed. Please try after some time");
//                                 }
//                             }
//                             else
//                             {
//                                 return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPLSAVED", "Loan approval failed. Please try after some time");
//                             }
//                         }
//                         else
//                         {
//                             return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPLSAVED", "Unable to add user bank details. Please try after some time.");
//                         }
//                     }
//                 }
//                 else
//                 {
//                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL005", "Unable to request loan amount. Please try after some time");
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return HCoreHelper.LogException("Save Loan", _Exception, UserReference);
//             }
//         }

//         internal OResponse SubmitLoan(CreateLoanRequestResponse Response)
//         {
//             try
//             {
//                 if (Response.Data1.Terms == null)
//                 {
//                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL007", "Loan terms should not be empty.");
//                 }

//                 using (_HCoreContext = new HCoreContext())
//                 {
//                     var AccountDetails = _HCoreContext.BNPLAccount.Where(x => x.VenderUserId == Response.Data1.User_Id).FirstOrDefault();
//                     var details = _HCoreContext.BNPLLoanProcess.Where(a => a.Loan.VenderLoanId == Response.Data1.Id && a.Status_Id == BNPLHelper.StatusHelper.Loan.Requested).FirstOrDefault();
//                     var Details = _HCoreContext.BNPLAccountLoan.Where(x => x.VenderLoanId == Response.Data1.Id && x.StatusId == BNPLHelper.StatusHelper.Loan.Requested).FirstOrDefault();
//                     if (Details != null && details != null)
//                     {
//                         if (Details.StatusId > 0 && Details.StatusId != BNPLHelper.StatusHelper.Loan.Submitted
//                             && details.Status_Id > 0 && details.Status_Id != BNPLHelper.StatusHelper.Loan.Submitted)
//                         {
//                             Details.StatusId = BNPLHelper.StatusHelper.Loan.Submitted;
//                             details.Status_Id = BNPLHelper.StatusHelper.Loan.Submitted;
//                         }
//                         Details.ModifyDate = HCoreHelper.GetGMTDateTime();
//                         Details.ModifyById = AccountDetails.AccountId;
//                         details.Modify_Date = HCoreHelper.GetGMTDateTime();
//                         details.Modify_By_Id = details.Loan.Account.AccountId;
//                         _HCoreContext.SaveChanges();

//                         var response = new
//                         {
//                             ReferenceId = Details.Id,
//                             ReferenceKey = Details.Guid,
//                             Id = details.Id,
//                             Key = details.Guid
//                         };
//                         return HCoreHelper.SendResponse(UserReference, ResponseStatus.Success, response, "BNPLUPDATE", "Loan details updated successfully.");
//                     }
//                     else
//                     {
//                         return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL404", "Details not found.");
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return HCoreHelper.LogException("Update Loan", _Exception, UserReference);
//             }
//         }

//         internal string CreateLoanStructureHtml(List<LoanRepaymentStructure> LoanStructure)
//         {
//             string res = "";
//             if (LoanStructure != null && LoanStructure.Count > 0)
//             {

//                 string tblhead = "<thead><tr><th>#</th><th>DueDate</th><th>Amount</th><th>Interest</th><th>Balance</th></tr><tbody>";
//                 string footer = "</tbody>";
//                 string body = "";
//                 string row = "<tr><td>{{index}}</td><td>{{date}}</td><td>{{repay}}</td><td>{{interest}}</td><td>{{balance}}</td></tr>";

//                 for (int index = 0; index < LoanStructure.Count; index++)
//                 {
//                     DateTime date = DateTime.Parse(LoanStructure[index].Due_Date);
//                     string x = row.Replace("{{index}}", (index + 1).ToString())
//                         .Replace("{{date}}", date.ToShortDateString())
//                         .Replace("{{repay}}", "&#8358; " + LoanStructure[index].Repayment.ToString())
//                         .Replace("{{interest}}", "&#8358; " + LoanStructure[index].Interest.ToString())
//                         .Replace("{{balance}}", "&#8358; " + LoanStructure[index].Total_Outstanding.ToString());
//                     body += x;

//                 }
//                 res = tblhead + body + footer;
//             }

//             return res;
//         }
//         internal OResponse ApproveLoan(ApproveLoanResponse Response)
//         {
//             try
//             {
//                 if (Response.Data.Terms == null)
//                 {
//                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL007", "Loan terms should not be empty.");
//                 }

//                 using (_HCoreContext = new HCoreContext())
//                 {
//                     var details = _HCoreContext.BNPLLoanProcess.Where(a => a.Loan.VenderLoanId == Response.Data.Id && a.Status_Id == BNPLHelper.StatusHelper.Loan.Submitted).FirstOrDefault();
//                     var Details = _HCoreContext.BNPLAccountLoan.Where(x => x.VenderLoanId == Response.Data.Id && x.StatusId == BNPLHelper.StatusHelper.Loan.Submitted).FirstOrDefault();
//                     var AccountDetails = _HCoreContext.BNPLAccount.Where(x => x.VenderUserId == Response.Data.User_Id).FirstOrDefault();
//                     if (Details != null && details != null)
//                     {
//                         if (Details.StatusId > 0 && Details.StatusId != BNPLHelper.StatusHelper.Loan.Approved
//                             && details.Status_Id > 0 && details.Status_Id != BNPLHelper.StatusHelper.Loan.Approved)
//                         {
//                             Details.StatusId = BNPLHelper.StatusHelper.Loan.Approved;
//                             details.Status_Id = BNPLHelper.StatusHelper.Loan.Approved;
//                         }
//                         if (Response.Data.Approved_By_Id != null)
//                         {
//                             details.Loan_Approved_By_Id = Response.Data.Approved_By_Id;
//                         }
//                         if (Response.Data.Approved_At != null)
//                         {
//                             details.Loan_Approved_Date = DateTime.Parse(Response.Data.Approved_At);
//                         }
//                         Details.ModifyDate = HCoreHelper.GetGMTDateTime();
//                         Details.ModifyById = AccountDetails.AccountId;
//                         details.Modify_Date = HCoreHelper.GetGMTDateTime();
//                         details.Modify_By_Id = details.Loan.Account.AccountId;
//                         _HCoreContext.SaveChanges();

//                         #region Save Loan Activity
//                         _BNPLAccountLoanActivity = new BNPLAccountLoanActivity();
//                         _BNPLAccountLoanActivity.Guid = HCoreHelper.GenerateGuid();
//                         _BNPLAccountLoanActivity.LoanId = Details.Id;
//                         _BNPLAccountLoanActivity.IsSystemActivity = 1;
//                         _BNPLAccountLoanActivity.CreateDate = HCoreHelper.GetGMTDateTime();
//                         _BNPLAccountLoanActivity.StatusId = HCoreConstant.HelperStatus.Default.Active;
//                         if (Details.StatusId == BNPLHelper.StatusHelper.Loan.Approved)
//                         {
//                             _BNPLAccountLoanActivity.Description = "Approved and Loan code credited successfully to customer";
//                         }
//                         _HCoreContext.BNPLAccountLoanActivity.Add(_BNPLAccountLoanActivity);
//                         _HCoreContext.SaveChanges();
//                         #endregion

//                         string _Schedule = CreateLoanStructureHtml(Response.Data1);


//                         string PlanName = null;
//                         if (Response.Data.Terms.Frequency == "monthly")
//                         {
//                             PlanName = "Pay in 4";
//                         }
//                         else
//                         {
//                             PlanName = "Pay in 7";
//                         }
//                         if (!string.IsNullOrEmpty(AccountDetails.EmailAddress))
//                         {
//                             var _EmailParameters = new
//                             {
//                                 DisplayName = AccountDetails.FirstName + " " + AccountDetails.LastName,
//                                 LoanAmount = Details.Amount,
//                                 InterestRate = Response.Data.Terms.Rate,
//                                 Plan = PlanName,
//                                 Schedule = _Schedule,
//                             };
//                             HCoreHelper.BroadCastEmail(NotificationTemplates.ApprovedLoanRequest, AccountDetails.FirstName, AccountDetails.EmailAddress, _EmailParameters, UserReference);
//                             //HCoreHelper.BroadCastEmail(NotificationTemplates.ApprovedLoanRequest, AccountDetails.FirstName, "omkar@thankucash.com", _EmailParameters, UserReference);
//                         }

//                         var response = new
//                         {
//                             ReferenceId = Details.Id,
//                             ReferenceKey = Details.Guid,
//                             Id = details.Id,
//                             Key = details.Guid
//                         };
//                         return HCoreHelper.SendResponse(UserReference, ResponseStatus.Success, response, "BNPLUPDATE", "Loan details updated successfully.");
//                     }
//                     else
//                     {
//                         return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL404", "Details not found.");
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return HCoreHelper.LogException("Update Loan", _Exception, UserReference);
//             }
//         }

//         internal OResponse SaveLoanRepaymentsStructure(GetLoanRepaymentResponse Response, long LoanRequestId)
//         {
//             try
//             {
//                 if (Response.Data == null || Response.Data.Count == 0)
//                 {
//                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL008", "Loan repayment structure not generated yet.");
//                 }

//                 foreach (var DataItem in Response.Data)
//                 {
//                     using (_HCoreContext = new HCoreContext())
//                     {
//                         var Details = _HCoreContext.BNPLAccountLoan.Where(a => a.VenderLoanId == LoanRequestId).FirstOrDefault();
//                         bool Exists = _HCoreContext.BNPLAccountLoan.Any(x => x.VenderLoanId == LoanRequestId);
//                         if (!Exists)
//                         {
//                             _HCoreContext.Dispose();
//                             return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL013", "Loanrequest doesn't exists.");
//                         }
//                         else
//                         {
//                             bnplaccountloanpayment = new BNPLAccountLoanPayment();
//                             bnplaccountloanpayment.Guid = HCoreHelper.GenerateGuid();
//                             bnplaccountloanpayment.LoanId = Details.Id;
//                             bnplaccountloanpayment.Amount = DataItem.Repayment;
//                             bnplaccountloanpayment.TotalOutStanding = (long)DataItem.Total_Outstanding;
//                             bnplaccountloanpayment.Charge = DataItem.Interest;
//                             bnplaccountloanpayment.TotalAmount = DataItem.Repayment + DataItem.Interest;
//                             bnplaccountloanpayment.PrincipalAmount = Details.Amount;
//                             bnplaccountloanpayment.InterestAmount = DataItem.Interest;
//                             bnplaccountloanpayment.SystemAmount = DataItem.Interest;
//                             bnplaccountloanpayment.ProviderAmount = DataItem.Interest;
//                             bnplaccountloanpayment.PaymentDate = DateTime.Parse(DataItem.Due_Date);
//                             bnplaccountloanpayment.CreateDate = HCoreHelper.GetGMTDateTime();
//                             bnplaccountloanpayment.CreatedById = 16;
//                             bnplaccountloanpayment.StatusId = BNPLHelper.StatusHelper.LoanPayment.Successful;
//                             _HCoreContext.BNPLAccountLoanPayment.Add(bnplaccountloanpayment);
//                             _HCoreContext.SaveChanges();


//                             #region Save Loan Activity
//                             _BNPLAccountLoanActivity = new BNPLAccountLoanActivity();
//                             _BNPLAccountLoanActivity.Guid = HCoreHelper.GenerateGuid();
//                             _BNPLAccountLoanActivity.LoanId = Details.Id;
//                             _BNPLAccountLoanActivity.IsSystemActivity = 1;
//                             _BNPLAccountLoanActivity.CreateDate = HCoreHelper.GetGMTDateTime();
//                             _BNPLAccountLoanActivity.StatusId = HCoreConstant.HelperStatus.Default.Active;
//                             int i = 0;
//                             for (i = 0; i <= 7; i++)
//                             {
//                                 if (i == 2)
//                                 {
//                                     _BNPLAccountLoanActivity.Description = "2nd" + "Repayment" + "₦" + bnplaccountloanpayment.Amount + "paid";
//                                 }
//                                 else if (i == 3)
//                                 {
//                                     _BNPLAccountLoanActivity.Description = "3rd" + "Repayment" + "₦" + bnplaccountloanpayment.Amount + "paid";
//                                 }
//                                 else if (i == 4)
//                                 {
//                                     _BNPLAccountLoanActivity.Description = "4th" + "Repayment" + "₦" + bnplaccountloanpayment.Amount + "paid";
//                                 }
//                                 else if (i > 4)
//                                 {
//                                     _BNPLAccountLoanActivity.Description = i + "Repayment" + "₦" + bnplaccountloanpayment.Amount + "paid";
//                                 }
//                             }
//                             _HCoreContext.BNPLAccountLoanActivity.Add(_BNPLAccountLoanActivity);
//                             _HCoreContext.SaveChanges();
//                             #endregion

//                             //var response = new
//                             //{
//                             //    ReferenceId = bnplaccountloanpayment.Id,
//                             //    ReferenceKey = bnplaccountloanpayment.Guid,
//                             //};

//                             //return HCoreHelper.SendResponse(UserReference, ResponseStatus.Success, response, "BNPLSAVED", "Loan repayments structure created successfully.");
//                         }
//                     }
//                 }
//                 return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL404", "Details not found.");
//             }
//             catch (Exception _Exception)
//             {
//                 return HCoreHelper.LogException("SaveLoanRepaymentsStructure", _Exception, UserReference);
//             }
//         }

//         internal OResponse OAproveLoan(LoanDisbursementResponse Response)
//         {
//             try
//             {
//                 if (Response.Data.Terms == null)
//                 {
//                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL007", "Loan terms should not be empty.");
//                 }

//                 using (_HCoreContext = new HCoreContext())
//                 {
//                     var AccountDetails = _HCoreContext.BNPLAccount.Where(x => x.VenderUserId == Response.Data.User_Id).FirstOrDefault();
//                     var details = _HCoreContext.BNPLLoanProcess.Where(a => a.Loan.VenderLoanId == Response.Data.Id && a.Status_Id == BNPLHelper.StatusHelper.Loan.Approved).FirstOrDefault();
//                     var Details = _HCoreContext.BNPLAccountLoan.Where(x => x.VenderLoanId == Response.Data.Id && x.StatusId == BNPLHelper.StatusHelper.Loan.Approved).FirstOrDefault();
//                     var MerchantDetails = _HCoreContext.BNPLAccountLoan.Where(x => x.VenderLoanId == Response.Data.Id && x.StatusId == BNPLHelper.StatusHelper.Loan.Approved)
//                         .Select(x => new
//                         {
//                             LoanId = x.Id,
//                             CustomerName = x.Account.FirstName + "" + x.Account.LastName,
//                             MerchantId = x.Merchant.AccountId,
//                             MerchantName = x.Merchant.Account.FirstName + "" + x.Merchant.Account.LastName,
//                             MerchantEmailAddress = x.Merchant.Account.EmailAddress
//                         }).FirstOrDefault();
//                     if (Details != null && details != null)
//                     {
//                         if (Details.StatusId > 0 && Details.StatusId != BNPLHelper.StatusHelper.Loan.Running
//                             && details.Status_Id > 0 && details.Status_Id != BNPLHelper.StatusHelper.Loan.Running)
//                         {
//                             Details.StatusId = BNPLHelper.StatusHelper.Loan.Running;
//                             details.Status_Id = BNPLHelper.StatusHelper.Loan.Running;
//                         }
//                         if (Details.StartDate == null)
//                         {
//                             Details.StartDate = DateTime.Parse(Response.Data3.Start_Date);
//                         }
//                         if (Details.EndDate == null)
//                         {
//                             Details.EndDate = DateTime.Parse(Response.Data3.End_Date);
//                         }
//                         if (Details.ProviderId <= 0 || Details.ProviderId == null)
//                         {
//                             Details.ProviderId = 10203;
//                         }
//                         Details.ModifyDate = HCoreHelper.GetGMTDateTime();
//                         Details.ModifyById = AccountDetails.AccountId;
//                         details.Modify_Date = HCoreHelper.GetGMTDateTime();
//                         details.Modify_By_Id = details.Loan.Account.AccountId;
//                         _HCoreContext.SaveChanges();

//                         var response = new
//                         {
//                             ReferenceId = Details.Id,
//                             ReferenceKey = Details.Guid,
//                             Id = details.Id,
//                             Key = details.Guid
//                         };
//                         return HCoreHelper.SendResponse(UserReference, ResponseStatus.Success, response, "BNPLUPDATE", "Loan details updated successfully.");
//                     }
//                     else
//                     {
//                         return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL404", "Details not found.");
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return HCoreHelper.LogException("Update Loan", _Exception, UserReference);
//             }
//         }

//         internal OResponse SetUpDebitMandate(SetUpDebitMandateResponse Response)
//         {
//             try
//             {
//                 if (string.IsNullOrEmpty(Response.Data.SetUp.MerchantId))
//                 {
//                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL015", "Mandate not started yet.");
//                 }
//                 using (_HCoreContext = new HCoreContext())
//                 {
//                     var AccountDetails = _HCoreContext.BNPLAccount.Where(x => x.VenderUserId == Response.Data.User_Id).FirstOrDefault();
//                     var Details = _HCoreContext.BNPLLoanProcess.Where(a => a.Loan.VenderLoanId == Response.Data.Loan_Request_Id && a.Status_Id == BNPLHelper.StatusHelper.Loan.Approved).FirstOrDefault();
//                     if (Details != null)
//                     {
//                         if (!string.IsNullOrEmpty(Response.Data.SetUp.MerchantId))
//                         {
//                             Details.Merchant_Id = Response.Data.SetUp.MerchantId;
//                         }
//                         if (!string.IsNullOrEmpty(Response.Data.SetUp.ServiceTypeId))
//                         {
//                             Details.Service_Type_Id = Response.Data.SetUp.ServiceTypeId;
//                         }
//                         if (!string.IsNullOrEmpty(Response.Data.SetUp.Amount))
//                         {
//                             Details.Repayment_Amount = Response.Data.SetUp.Amount;
//                         }
//                         if (!string.IsNullOrEmpty(Response.Data.SetUp.StartDate))
//                         {
//                             Details.Mandate_Start_Date = DateTime.ParseExact(Response.Data.SetUp.StartDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
//                         }
//                         if (!string.IsNullOrEmpty(Response.Data.SetUp.EndDate))
//                         {
//                             Details.Mandate_End_Date = DateTime.ParseExact(Response.Data.SetUp.EndDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
//                         }
//                         if (!string.IsNullOrEmpty(Response.Data.SetUp.MandateType))
//                         {
//                             Details.Mandate_Type = Response.Data.SetUp.MandateType;
//                         }
//                         if (!string.IsNullOrEmpty(Response.Data.SetUp.MaxNoOfDebits))
//                         {
//                             Details.Max_No_Of_Debits = Response.Data.SetUp.MaxNoOfDebits;
//                         }

//                         Details.Modify_Date = HCoreHelper.GetGMTDateTime();
//                         Details.Modify_By_Id = AccountDetails.AccountId;
//                         _HCoreContext.SaveChanges();

//                         var response = new
//                         {
//                             ReferenceId = Details.Id,
//                             ReferenceKey = Details.Guid
//                         };
//                         return HCoreHelper.SendResponse(UserReference, ResponseStatus.Success, response, "BNPLUPDATE", "Loan details updated successfully.");
//                     }
//                     else
//                     {
//                         return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL014", "Details not found");
//                     }

//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return HCoreHelper.LogException("Update Loan", _Exception, UserReference);
//             }
//         }

//         internal OResponse SaveUserBankDetails(AddUserBankDetailsResponse Response, long LoanRequestId)
//         {
//             try
//             {
//                 if (Response.Data.Account_Number == null)
//                 {
//                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL016", "Bank details not found");
//                 }

//                 using (_HCoreContext = new HCoreContext())
//                 {
//                     var details = _HCoreContext.BNPLAccountLoan.Where(x => x.VenderLoanId == LoanRequestId).FirstOrDefault();
//                     var Details = _HCoreContext.BNPLLoanProcess.Where(x => x.Loan.VenderLoanId == LoanRequestId).FirstOrDefault();
//                     bool Exists = _HCoreContext.BNPLLoanProcess.Any(a => a.Loan.VenderLoanId == LoanRequestId);
//                     if (!Exists)
//                     {
//                         return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL016", "Bank details not found");
//                     }

//                     bnplprequalification = new BNPLPrequalification();
//                     bnplprequalification.Guid = HCoreHelper.GenerateGuid();
//                     bnplprequalification.AccountId = details.AccountId;
//                     bnplprequalification.LoanId = Details.Id;
//                     bnplprequalification.BankAccountNumber = Response.Data.Account_Number;
//                     bnplprequalification.BankCode = Response.Data.Code;
//                     bnplprequalification.BankName = Response.Data.Bank_Name;
//                     bnplprequalification.CreateDate = HCoreHelper.GetGMTDateTime();
//                     bnplprequalification.CreatedById = details.AccountId;
//                     bnplprequalification.StatusId = HCoreConstant.HelperStatus.Default.Active;
//                     _HCoreContext.BNPLPrequalification.Add(bnplprequalification);
//                     _HCoreContext.SaveChanges();
//                     _HCoreContext.Dispose();

//                     var response = new
//                     {
//                         ReferenceId = bnplprequalification.Id,
//                         ReferenceKey = bnplprequalification.Guid
//                     };
//                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Success, response, "BNPLSAVED", "Bank details saved successfully.");
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return HCoreHelper.LogException("Save Bank Details", _Exception, UserReference);
//             }
//         }

//         internal OResponse CheckMonoId(CreateUserRequest Request)
//         {
//             try
//             {
//                 using (_HCoreContext = new HCoreContext())
//                 {
//                     var Details = _HCoreContext.BNPLLoanProcess.Where(a => a.Loan.Account.EmailAddress == Request.user.Email).Select(m => m.MonoId).FirstOrDefault();
//                     if (Details != null)
//                     {
//                         return HCoreHelper.SendResponse(UserReference, ResponseStatus.Success, Details, "BNPL018", "Details loaded successfully.");
//                     }
//                     else
//                     {
//                         return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "BNPL404", "Details not found.");
//                     }
//                 }
//             }
//             catch (Exception ex)
//             {
//                 return HCoreHelper.LogException("CheckMonoId", ex, UserReference);
//             }
//         }

//         internal bool CheckCreditLimit(CreateUserRequest Request)
//         {
//             try
//             {
//                 using (_HCoreContext = new HCoreContext())
//                 {
//                     bool Checked = _HCoreContext.BNPLAccount.Any(a => a.EmailAddress == Request.user.Email && a.CreditLimit != null);
//                     if (Checked)
//                     {
//                         return true;
//                     }
//                     else
//                     {
//                         return false;
//                     }
//                 }
//             }
//             catch (Exception ex)
//             {
//                 HCoreHelper.LogException("CheckCreditLimit", ex);
//                 return false;
//             }
//         }

//         internal OResponse SaveLoanActivity(OLoanActivity.Save _Request)
//         {
//             try
//             {
//                 if (_Request.LoanId == null)
//                 {
//                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "LACT001", "Loanid required.");
//                 }
//                 if (string.IsNullOrEmpty(_Request.LoanKey))
//                 {
//                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "LACT002", "Loankey required.");
//                 }
//                 if (string.IsNullOrEmpty(_Request.Description))
//                 {
//                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Error, null, "LACT003", "Loan description required.");
//                 }

//                 using (_HCoreContext = new HCoreContext())
//                 {
//                     _BNPLAccountLoanActivity = new BNPLAccountLoanActivity();
//                     _BNPLAccountLoanActivity.Guid = HCoreHelper.GenerateGuid();
//                     _BNPLAccountLoanActivity.LoanId = (long)_Request.LoanId;
//                     _BNPLAccountLoanActivity.Description = _Request.Description;
//                     _BNPLAccountLoanActivity.CreateDate = HCoreHelper.GetGMTDateTime();
//                     _BNPLAccountLoanActivity.StatusId = HCoreConstant.HelperStatus.Default.Active;
//                     _HCoreContext.BNPLAccountLoanActivity.Add(_BNPLAccountLoanActivity);
//                     _HCoreContext.SaveChanges();
//                     _HCoreContext.Dispose();

//                     var _Response = new
//                     {
//                         ReferenceId = _BNPLAccountLoanActivity.Id,
//                         ReferenceKey = _BNPLAccountLoanActivity.Guid
//                     };

//                     return HCoreHelper.SendResponse(UserReference, ResponseStatus.Success, _Response, "LACTSAVED", "Loan activity details saved successfully.");
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 return HCoreHelper.LogException("SaveLoanActivity", _Exception, UserReference);
//             }
//         }
//     }
// }
