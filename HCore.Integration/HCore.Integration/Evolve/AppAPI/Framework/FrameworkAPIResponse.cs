// //==================================================================================
// // FileName: FrameworkAPIResponse.cs
// // Author : Harshal Gandole
// // Created On : 
// // Description : Class for defining database related functions
// //
// // COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
// //
// // Revision History:
// // Date             : Changed By        : Comments
// // ---------------------------------------------------------------------------------
// //                 | Harshal Gandole   : Created New File
// //
// //
// //==================================================================================

// ﻿using HCore.Data;
// using HCore.Data.Models;
// using HCore.Helper;
// using Mono.EvolveObject;
// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Text;
// using System.Threading.Tasks;
// using static HCore.Helper.HCoreConstant;

// namespace HCore.Integration.Evolve.AppAPI.Framework
// {
//     public class FrameworkAPIResponse
//     {
//         HCoreContext _HCoreContext;
//         public void SaveDebitMandate(SetUpDebitMandateResponse setUpDebitMandateResponse, OUserReference UserReference)
//         {
//             try
//             {
//                 using (_HCoreContext = new HCoreContext())
//                 {
//                     var data = _HCoreContext
//                           .BNPLLoanProcess.Where(x => x.LoanId == setUpDebitMandateResponse.Data.Id)
//                           .FirstOrDefault();
//                     data.Merchant_Id = setUpDebitMandateResponse.Data.SetUp.MerchantId;
//                     data.Service_Type_Id = setUpDebitMandateResponse.Data.SetUp.ServiceTypeId;
//                     data.Repayment_Amount = setUpDebitMandateResponse.Data.SetUp.Amount;
//                     data.Mandate_Start_Date = DateTime.ParseExact(setUpDebitMandateResponse.Data.SetUp.StartDate, "dd/MM/yyyy",
//                                        System.Globalization.CultureInfo.InvariantCulture);
//                     data.Mandate_End_Date = DateTime.ParseExact(setUpDebitMandateResponse.Data.SetUp.EndDate, "dd/MM/yyyy",
//                                        System.Globalization.CultureInfo.InvariantCulture);
//                     data.Mandate_Type = setUpDebitMandateResponse.Data.SetUp.MandateType;
//                     data.Max_No_Of_Debits = setUpDebitMandateResponse.Data.SetUp.MaxNoOfDebits;
//                     data.Modify_By_Id = UserReference.AccountId;
//                     data.Modify_Date = HCoreHelper.GetGMTDate();
//                     _HCoreContext.BNPLLoanProcess.Update(data);
//                     _HCoreContext.SaveChanges();
//                 }
//             }
//             catch (Exception ex)
//             {
//                 HCoreHelper.LogException("SaveDebitMandate", ex);
//             }

//         }

//         public void SaveLoanProcess(CreateLoanRequestResponse createLoanRequestResponse, OUserReference UserReference)
//         {
//             try
//             {
//                 using (_HCoreContext = new HCoreContext())
//                 {
//                     BNPLLoanProcess bNPLLoanProcess = new BNPLLoanProcess();
//                     bNPLLoanProcess.Guid = HCoreHelper.GenerateGuid();
//                     bNPLLoanProcess.LoanId = createLoanRequestResponse.Data.Id;
//                     bNPLLoanProcess.UserId = createLoanRequestResponse.Data.User_Id;
//                     bNPLLoanProcess.Loan_Product_Id = createLoanRequestResponse.Data.Product_Id;
//                     bNPLLoanProcess.Loan_Plan_Id = createLoanRequestResponse.Data.Product_Id==119?1:0;
//                     bNPLLoanProcess.Vendor_Loan_Reference = createLoanRequestResponse.Data.Reference;
//                     bNPLLoanProcess.Vendor_Loan_Amount = createLoanRequestResponse.Data.Amount;
//                     bNPLLoanProcess.Vendor_Loan_Purpose = createLoanRequestResponse.Data.Purpose;
//                     bNPLLoanProcess.Vendor_Loan_Status = createLoanRequestResponse.Data.Status;
//                     bNPLLoanProcess.Lender_Name = createLoanRequestResponse.Data.Lender_Name;
//                     //bNPLLoanProcess.Lender_Status = createLoanRequestResponse.Data.Lender_Status;
//                     bNPLLoanProcess.Organization_Id = createLoanRequestResponse.Data.Org_Id;
//                     //bNPLLoanProcess.BNPLDebit_Mandate_Id = UserReference.DebitMandateId;
//                     bNPLLoanProcess.Debit_Mandate_Next_Step = createLoanRequestResponse.Data.Debit_Mandate_Next_Step;
//                     bNPLLoanProcess.Loan_Submite_Date = DateTime.Parse(createLoanRequestResponse.Data.Submitted_At);     
//                     bNPLLoanProcess.Create_Date = HCoreHelper.GetGMTDate();
//                     bNPLLoanProcess.Created_By_Id = UserReference.AccountId;
//                     bNPLLoanProcess.Modify_By_Id = UserReference.AccountId;
//                     bNPLLoanProcess.Modify_Date = HCoreHelper.GetGMTDate();
//                     bNPLLoanProcess.Status_Id = HelperStatus.Default.Active;
//                     _HCoreContext.BNPLLoanProcess.Add(bNPLLoanProcess);
//                     _HCoreContext.SaveChanges();
//                 }
//             }
//             catch (Exception ex) {
//                 HCoreHelper.LogException("SaveLoanProcess", ex);
//             }
//         }

//         public void SubmitLoanResponse(CreateLoanRequestResponse createLoanRequestResponse, OUserReference UserReference) {
//             try {
//                 using (_HCoreContext = new HCoreContext()) {
//                     var data = _HCoreContext
//                         .BNPLLoanProcess.Where(x => x.LoanId == createLoanRequestResponse.Data1.Id)
//                         .FirstOrDefault();
//                     data.Credit_Score_Check_Done = (sbyte?)(createLoanRequestResponse.Data1.Credit_Score_Check_Done==true?1:0);
//                     data.Loan_Submite_Date = DateTime.Parse(createLoanRequestResponse.Data1.Submitted_At);
//                     data.Modify_By_Id = UserReference.AccountId;
//                     data.Modify_Date = HCoreHelper.GetGMTDate();
//                     _HCoreContext.BNPLLoanProcess.Update(data);
//                     _HCoreContext.SaveChanges();
//                 }
//             }
//             catch (Exception ex) {
//                 HCoreHelper.LogException("SubmitLoanResponse", ex);
//             }
//         }

//         public void ApproveLoanResponse(ApproveLoanResponse approveLoanResponse, OUserReference UserReference)
//         {
//             try
//             {
//                 using (_HCoreContext = new HCoreContext())
//                 {
//                     var data = _HCoreContext
//                         .BNPLLoanProcess.Where(x => x.LoanId == approveLoanResponse.Data.Id)
//                         .FirstOrDefault();
//                     data.Loan_Approved_By_Id = approveLoanResponse.Data.Approved_By_Id;
//                     data.Loan_Approved_Date = DateTime.Parse(approveLoanResponse.Data.Final_Approval_Date);
//                     data.Modify_By_Id = UserReference.AccountId;
//                     data.Modify_Date = HCoreHelper.GetGMTDate();
//                     _HCoreContext.BNPLLoanProcess.Update(data);
//                     _HCoreContext.SaveChanges();
//                 }
//             }
//             catch (Exception ex)
//             {
//                 HCoreHelper.LogException("ApproveLoanResponse", ex);
//             }
//         }

//         public void UpdatePrequalification(BankDetails bankDetails,AddUserBankDetailsResponse addUserBankDetailsResponse, OUserReference UserReference)
//         {
//             try
//             {
//                 using (_HCoreContext = new HCoreContext())
//                 {
//                     var data = _HCoreContext.BNPLPrequalification
//                       .Where(x => x.AccountId == addUserBankDetailsResponse.Data.User_Id).FirstOrDefault();
//                     data.BankCode = bankDetails.code;
//                     data.BankName = bankDetails.bank_name;
//                     data.BankAccountNumber = bankDetails.account_number;                 
//                     data.LoanId = addUserBankDetailsResponse.Data.Loan_Request_Id;
//                     data.ModifyByid = UserReference.AccountId;
//                     data.ModifyDate = HCoreHelper.GetGMTDate();
//                     _HCoreContext.BNPLPrequalification.Update(data);
//                     _HCoreContext.SaveChanges();
//                 }
//             }
//             catch (Exception ex)
//             {
//                 HCoreHelper.LogException("UpdatePrequalification", ex);
//             }
//         }

//         public void SaveUser(CreateUserResponse createUserResponse, OUserReference UserReference,string bvn)
//         {
//             try
//             {
//                 using (_HCoreContext = new HCoreContext())
//                 {
//                     BNPLAccount bNPLAccount = new BNPLAccount();
//                     bNPLAccount.Guid = HCoreHelper.GenerateGuid();
//                     bNPLAccount.AccountId = UserReference.AccountId;
//                     bNPLAccount.FirstName = createUserResponse.Data.FirstName;
//                     bNPLAccount.LastName = createUserResponse.Data.LastName;
//                     bNPLAccount.EmailAddress = createUserResponse.Data.Email;
//                     bNPLAccount.WorkEmailAddress = createUserResponse.Data.Email;
//                     bNPLAccount.MobileNumber = createUserResponse.Data.Phone;
//                     bNPLAccount.BvnNumber = bvn;
//                     bNPLAccount.CreateDate = HCoreHelper.GetGMTDateTime();
//                     bNPLAccount.CreatedById = UserReference.AccountId;
//                     bNPLAccount.StatusId = HelperStatus.Default.Active;
//                     _HCoreContext.BNPLAccount.Add(bNPLAccount);
//                     _HCoreContext.SaveChanges();
//                 }
//             }
//             catch (Exception ex)
//             {
//                 HCoreHelper.LogException("SaveUser", ex);
//             }
//         }

//         public bool CheckUserExists(CreateUserRequest createUserRequest)
//         {
//             try {
//                 using (_HCoreContext = new HCoreContext()) {
//                     bool isExists = _HCoreContext.BNPLAccount.Any(x => x.EmailAddress == createUserRequest.user.Email);
//                     return isExists;
//                 }
//             }
//             catch (Exception ex) {
//                 HCoreHelper.LogException("CheckUserExists", ex);
//                 return false;
//             }
//         }

//         public void SavePrequalification(AddBankStatementResponse addBankStatementResponse, OUserReference UserReference)
//         {
//             try
//             {
//                 using (_HCoreContext = new HCoreContext())
//                 {
//                     BNPLPrequalification bNPLPrequalification = new BNPLPrequalification();
//                     bNPLPrequalification.Guid = HCoreHelper.GenerateGuid();                  
//                     bNPLPrequalification.AccountId = addBankStatementResponse.Data.User_Id;
//                     bNPLPrequalification.Yearly_Income = addBankStatementResponse.Data.Income.Yearly_Income;
//                     bNPLPrequalification.Monthly_Income = addBankStatementResponse.Data.Income.Monthly_Income;
//                     bNPLPrequalification.Income_Sources = Convert.ToInt32(addBankStatementResponse.Data.Income.Income_Sources);
//                     bNPLPrequalification.Average_Income = addBankStatementResponse.Data.Income.Average_Income;
//                     bNPLPrequalification.CreatedById = UserReference.AccountId;
//                     bNPLPrequalification.CreateDate = HCoreHelper.GetGMTDate();
//                     bNPLPrequalification.ModifyByid = UserReference.AccountId;
//                     bNPLPrequalification.ModifyDate = HCoreHelper.GetGMTDate();
//                     bNPLPrequalification.StatusId = HelperStatus.Default.Active;

//                     _HCoreContext.BNPLPrequalification.Add(bNPLPrequalification);
//                     _HCoreContext.SaveChanges();
//                 }
//             }
//             catch (Exception ex)
//             {
//                 HCoreHelper.LogException("SavePrequalification", ex);
//             }
//         }
//     }
// }
