// //==================================================================================
// // FileName: ManageBNPL.cs
// // Author : Harshal Gandole
// // Created On : 
// // Description : Class for defining database related functions
// //
// // COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
// //
// // Revision History:
// // Date             : Changed By        : Comments
// // ---------------------------------------------------------------------------------
// //                 | Harshal Gandole   : Created New File
// //
// //
// //==================================================================================

// ﻿using System;
// using HCore.Helper;
// using HCore.Integration.Evolve.AppAPI.Framework;
// using HCore.Integration.Evolve.EvolveObject;
// using HCore.Integration.Evolve.EvolveObject.Requests;
// using Mono.EvolveObject;

// namespace HCore.Integration.Evolve.AppAPI
// {
//     public class ManageBNPL
//     {
//         FrameworkBNPL frameworkbnpl;

//         public OResponse SaveCustomer(CreateUserRequest Request, CreateUserResponse Response)
//         {
//             frameworkbnpl = new FrameworkBNPL();
//             return frameworkbnpl.SaveCustomer(Request, Response);
//         }
//         public OResponse UpdateCustomer(AddBankStatementResponse Response)
//         {
//             frameworkbnpl = new FrameworkBNPL();
//             return frameworkbnpl.UpdateCustomer(Response);
//         }
//         public OResponse SaveLoan(CreateLoanRequestResponse Response, LoanRequest Request)
//         {
//             frameworkbnpl = new FrameworkBNPL();
//             return frameworkbnpl.SaveLoan(Response, Request);
//         }
//         public OResponse SubmitLoan(CreateLoanRequestResponse Response)
//         {
//             frameworkbnpl = new FrameworkBNPL();
//             return frameworkbnpl.SubmitLoan(Response);
//         }
//         public OResponse ApproveLoan(ApproveLoanResponse Response)
//         {
//             frameworkbnpl = new FrameworkBNPL();
//             return frameworkbnpl.ApproveLoan(Response);
//         }
//         public OResponse OAproveLoan(LoanDisbursementResponse Response)
//         {
//             frameworkbnpl = new FrameworkBNPL();
//             return frameworkbnpl.OAproveLoan(Response);
//         }
//         public OResponse SaveLoanRepaymentsStructure(GetLoanRepaymentResponse Response, long LoanRequestId)
//         {
//             frameworkbnpl = new FrameworkBNPL();
//             return frameworkbnpl.SaveLoanRepaymentsStructure(Response, LoanRequestId);
//         }
//         public OResponse SetUpDebitMandate(SetUpDebitMandateResponse Response)
//         {
//             frameworkbnpl = new FrameworkBNPL();
//             return frameworkbnpl.SetUpDebitMandate(Response);
//         }
//         public OResponse SaveUserBankDetails(AddUserBankDetailsResponse Response, long LoanRequestId)
//         {
//             frameworkbnpl = new FrameworkBNPL();
//             return frameworkbnpl.SaveUserBankDetails(Response, LoanRequestId);
//         }
//         public OResponse CheckMonoId(CreateUserRequest Request)
//         {
//             frameworkbnpl = new FrameworkBNPL();
//             return frameworkbnpl.CheckMonoId(Request);
//         }
//         public bool CheckCreditLimit(CreateUserRequest createUserRequest)
//         {
//             frameworkbnpl = new FrameworkBNPL();
//             return frameworkbnpl.CheckCreditLimit(createUserRequest);
//         }
//         public OResponse SaveLoanActivity(OLoanActivity.Save _Request)
//         {
//             frameworkbnpl = new FrameworkBNPL();
//             return frameworkbnpl.SaveLoanActivity(_Request);
//         }
//     }
// }
