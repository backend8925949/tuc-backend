//==================================================================================
// FileName: OEvolveOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Mono
{
    #region Evolve Credit
    public class CreateUserRequest
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
        public string? Phone { get; set; }
        public int? GenderId { get; set; }
        public string? BVN { get; set; }
        public string? API_KEY_TEST { get; set; }
    }
    public class CreateUserResponse
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
        public Response Data { get; set; }
    }
    public class Response
    {
        public long Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public string? Gender { get; set; }
        public string? Reference { get; set; }
        public bool Is_Active { get; set; }
        public string? Source { get; set; }
        public string? Created_At { get; set; }
        public string? Updated_At { get; set; }
    }

    public class BankStatementRequest
    {
        public long User_Id { get; set; }
        public List<TransactionDetails> Transactions { get; set; }
        public string? Balance { get; set; }
        public IncomeDetails Income { get; set; }
        public string? API_KEY_TEST { get; set; }
    }
    public class TransactionDetails
    {
        public string? Type { get; set; }
        public string? Narration { get; set; }
        public long Amount { get; set; }
        public string? Date { get; set; }
    }
    public class IncomeDetails
    {
        public long Average_Income { get; set; }
        public long Monthly_Income { get; set; }
        public long Yearly_Income { get; set; }
    }
    public class BankStatementResponse
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
        public BankStatementResponseData Data { get; set; }
        public long Pre_Qualified_Amount { get; set; }
    }
    public class BankStatementResponseData
    {
        public long Id { get; set; }
        public long User_Id { get; set; }
        public long Org_Id { get; set; }
        public long Loan_Request_Id { get; set; }
        public List<TransactionDetails> Transactions { get; set; }
        public string? Credit_Outliers { get; set; }
        public string? Debit_Outliers { get; set; }
        public long Balance { get; set; }
        public IncomeDetails Income { get; set; }
        public long Suggested_Loan_Amount { get; set; }
        public string? Updated_At { get; set; }
        public string? Created_At { get; set; }
    }

    public class TTransaction
    {
        public string? type { get; set; }
        public string? narration { get; set; }
        public long amount { get; set; }
        public string? date { get; set; }
    }

    public class AddUserBankDetailsRequest
    {
        public long Loan_Request_Id { get; set; }
        public string? Code { get; set; }
        public string? Account_Name { get; set; }
        public string? Account_Number { get; set; }
        public string? Bank_Name { get; set; }
        public bool Has_Custom_Fields { get; set; }
        public string? API_KEY_TEST { get; set; }
    }
    public class AddUserBankDetailsResponse
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
        public string? Data { get; set; }
    }
    public class AddUserBankDetailsResponseData
    {
        public long Id { get; set; }
        public long User_Id { get; set; }
        public long Org_Id { get; set; }
        public long Product_Id { get; set; }
        public string? Product_Name { get; set; }
        public string? Product_Slug { get; set; }
        public string? Lender_Name { get; set; }
        public string? Reference { get; set; }
        public string? FullName { get; set; }
        public long Amount { get; set; }
        public string? Purpose { get; set; }
        public Terms Terms { get; set; }
        public string? Status { get; set; }
        public bool Active { get; set; }
        public string? Progress { get; set; }
        public string? Lender_Status { get; set; }
        public string? Debit_Mandate_Next_Step { get; set; }
        public string? Offer_Letter { get; set; }
        public string? Declined_At { get; set; }
        public string? Final_Approval_Date { get; set; }
        public long Approved_By_Id { get; set; }
        public string? Approved_By { get; set; }
        public string? Approved_At { get; set; }
        public string? User_Status { get; set; }
        public string? User_Accepted_Date { get; set; }
        public string? User_Declined_Date { get; set; }
        public string? Archived_Date { get; set; }
        public string? Unarchived_At { get; set; }
        public string? Disbursed_At { get; set; }
        public string? Submitted_At { get; set; }
        public string? Deleted_At { get; set; }
        public string? Created_At { get; set; }
        public string? Updated_At { get; set; }
    }

    public class AddMerchantBankDetailsRequest
    {
        public long Loan_Request_Id { get; set; }
        public string? Code { get; set; }
        public string? Account_Name { get; set; }
        public string? Account_Number { get; set; }
        public string? Bank_Name { get; set; }
        public string? API_KEY_TEST { get; set; }
    }
    public class AddMerchantBankDetailsCustome
    {
        public string? label { get; set; }
        public string? field { get; set; }
        public string? data_type { get; set; }
        public string? string_value { get; set; }
        public long number_value { get; set; }
        public bool boolean_value { get; set; }
        public bool is_money { get; set; }
        public bool is_custom { get; set; }
    }
    public class AddMerchantBankDetailsResponse
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
        public AddMerchantBankDetailsResponseData Data { get; set; }
    }
    public class AddMerchantBankDetailsResponseData
    {
        public long Id { get; set; }
        public long User_Id { get; set; }
        public long Org_Id { get; set; }
        public long Loan_Request_Id { get; set; }
        public string? Code { get; set; }
        public string? Account_Name { get; set; }
        public string? Account_Number { get; set; }
        public string? Bank_Name { get; set; }
        public bool Is_Deleted { get; set; }
        public bool Has_Custom_Fields { get; set; }
        public string? Updated_At { get; set; }
        public string? Created_At { get; set; }
    }
    
    public class LoanRequest
    {
        public long Id { get; set; }
        public long Amount { get; set; }
        public string? Purpose { get; set; }
        public string? API_KEY_TEST { get; set; }
    }
    public class LoanResponse
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
        public ResponseData Data { get; set; }
    }
    public class ResponseData
    {
        public long Id { get; set; }
        public long User_Id { get; set; }
        public long Org_Id { get; set; }
        public long Product_Id { get; set; }
        public string? Product_Name { get; set; }
        public string? Product_Slug { get; set; }
        public string? Lender_Name { get; set; }
        public string? Reference { get; set; }
        public string? FullName { get; set; }
        public long Amount { get; set; }
        public string? Purpose { get; set; }
        public Terms Terms { get; set; }
        public string? Status { get; set; }
        public bool Active { get; set; }
        public string? Lender_Status { get; set; }
        public string? Debit_Mandate_Next_Step { get; set; }
        public string? Declined_At { get; set; }
        public string? Final_Approval_Date { get; set; }
        public string? Cancelled_By { get; set; }
        public long Cancelled_By_Id { get; set; }
        public string? Approved_At { get; set; }
        public string? User_Status { get; set; }
        public string? User_Accepted_Date { get; set; }
        public string? User_Declined_Date { get; set; }
        public string? Archived_Date { get; set; }
        public string? Unarchived_At { get; set; }
        public string? Disbursed_At { get; set; }
        public string? Submitted_At { get; set; }
        public string? Deleted_At { get; set; }
        public string? Created_At { get; set; }
        public string? Updated_At { get; set; }
    }
    public class Terms
    {
        public long Approved_Amount { get; set; }
        public long Rate { get; set; }
        public long Tenure { get; set; }
        public string? Tenured_In { get; set; }
        public string? Rate_Type { get; set; }
        public long Grace_Period { get; set; }
        public bool Exclude_Weekends { get; set; }
        public bool Zero_Interest { get; set; }
        public long Estimated_Repayment { get; set; }
        public string? Note { get; set; }
    }
    public class SubmitLoanRequest
    {
        public long Loan_Request_Id { get; set; }
        public string? API_KEY_TEST { get; set; }
    }
    public class SubmitLoanResponse
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
        public string? Data { get; set; }
        public SubmitLoanResponseData Data1 { get; set; }
    }
    public class SubmitLoanResponseData
    {
        public long Id { get; set; }
        public long User_Id { get; set; }
        public long Org_Id { get; set; }
        public long Product_Id { get; set; }
        public string? Product_Name { get; set; }
        public string? Product_Slug { get; set; }
        public string? Lender_Name { get; set; }
        public string? Reference { get; set; }
        public string? FullName { get; set; }
        public long Amount { get; set; }
        public string? Purpose { get; set; }
        public Terms Terms { get; set; }
        public string? Status { get; set; }
        public bool Active { get; set; }
        public string? Lender_Status { get; set; }
        public string? Debit_Mandate_Next_Step { get; set; }
        public long Declined_By_Id { get; set; }
        public string? Declined_By { get; set; }
        public string? Declined_At { get; set; }
        public string? Final_Approval_Date { get; set; }
        public string? Cancelled_By { get; set; }
        public long Cancelled_By_Id { get; set; }
        public string? Approved_At { get; set; }
        public string? User_Status { get; set; }
        public string? User_Accepted_Date { get; set; }
        public string? User_Declined_Date { get; set; }
        public string? Archived_Date { get; set; }
        public string? Unarchived_At { get; set; }
        public string? Disbursed_At { get; set; }
        public string? Submitted_At { get; set; }
        public string? Deleted_At { get; set; }
        public string? Created_At { get; set; }
        public string? Updated_At { get; set; }
    }
    public class ApproveLoanRequest
    {
        public long Approved_Amount { get; set; }
        public double Rate { get; set; }
        public long Tenure { get; set; }
        public string? Tenured_In { get; set; }
        public string? Rate_Type { get; set; }
        public bool Exclude_Weekends { get; set; }
        public long Grace_Period { get; set; }
        public string? Note { get; set; }
        public long Loan_Request_Id { get; set; }
        public string? API_KEY_TEST { get; set; }
    }
    public class ApproveLoanResponse
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
        public ApproveLoanResponseData Data { get; set; }
        public List<ApproveLoanrepaymentStructure> Data1 { get; set; }
    }
    public class ApproveLoanResponseData
    {
        public long Id { get; set; }
        public long User_Id { get; set; }
        public long Org_Id { get; set; }
        public long Product_Id { get; set; }
        public string? Product_Name { get; set; }
        public string? Product_Slug { get; set; }
        public string? Lender_Name { get; set; }
        public string? Reference { get; set; }
        public string? FullName { get; set; }
        public long Amount { get; set; }
        public string? Purpose { get; set; }
        public Terms Terms { get; set; }
        public string? Status { get; set; }
        public bool Active { get; set; }
        public string? Lender_Status { get; set; }
        public string? Debit_Mandate_Next_Step { get; set; }
        public string? Declined_At { get; set; }
        public string? Final_Approval_Date { get; set; }
        public long Approved_By_Id { get; set; }
        public string? Approved_By { get; set; }
        public string? Cancelled_By { get; set; }
        public long Cancelled_By_Id { get; set; }
        public string? Approved_At { get; set; }
        public string? User_Status { get; set; }
        public string? User_Accepted_Date { get; set; }
        public string? User_Declined_Date { get; set; }
        public string? Archived_Date { get; set; }
        public string? Unarchived_At { get; set; }
        public string? Disbursed_At { get; set; }
        public string? Submitted_At { get; set; }
        public string? Deleted_At { get; set; }
        public string? Created_At { get; set; }
        public string? Updated_At { get; set; }
    }
    public class ApproveLoanrepaymentStructure
    {
        public string? Due_Date { get; set; }
        public long Total_Outstanding { get; set; }
        public long Interest { get; set; }
        public long Repayment { get; set; }
    }

    public class DisbursementRequest
    {
        public long Loan_Request_Id { get; set; }
        public string? Note { get; set; }
        public string? API_KEY_TEST { get; set; }
    }
    public class DisbursementResponse
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
    }

    public class DeclineLoanRequest
    {
        public long Approved_Amount { get; set; }
        public double Rate { get; set; }
        public long Tenure { get; set; }
        public string? Tenured_In { get; set; }
        public string? Rate_Type { get; set; }
        public bool Exclude_Weekends { get; set; }
        public long Grace_Period { get; set; }
        public string? Note { get; set; }
        public long Loan_Request_Id { get; set; }
    }
    public class DeclineLoanResponse
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
    }

    public class SetUpDebitMandateRequest
    {
        public string? API_KEY_TEST { get; set; }
        public long Resource_Id { get; set; }
        public string? Resource_Name { get; set; }
        public long Product_Id { get; set; }
        public string? Action { get; set; }
        public string? Vendor { get; set; }
        public long UserId { get; set; }
    }
    public class SetUpDebitMandateResponse
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
        public SetUpDebitMandateResponseData Data { get; set; }
        public SetUpDebitMandateResponseData2 Data2 { get; set; }
    }
    public class SetUpDebitMandateResponseData
    {
        public long Id { get; set; }
        public long User_Id { get; set; }
        public long Org_Id { get; set; }
        public long Loan_Request_Id { get; set; }
        public SetUp SetUp { get; set; }
        public SetUpResponse SetUp_Res { get; set; }
        public bool Is_Credit_Order { get; set; }
        public int Credit_Order_Id { get; set; }
        public string? Amount { get; set; }
        public string? Status { get; set; }
        public bool Activated { get; set; }
        public string? Form_Url { get; set; }
        public string? Created_At { get; set; }
    }
    public class SetUpDebitMandateResponseData2
    {
        public string? MerchantId { get; set; }
        public string? ServiceTypeId { get; set; }
        public string? Hash { get; set; }
        public string? PayerName { get; set; }
        public string? PayerEmail { get; set; }
        public string? PayerPhone { get; set; }
        public string? PayerBankCode { get; set; }
        public string? PayerAccount { get; set; }
        public string? RequestId { get; set; }
        public string? Amount { get; set; }
        public string? StartDate { get; set; }
        public string? EndDate { get; set; }
        public string? MandateType { get; set; }
        public string? MaxNoOfDebits { get; set; }
    }
    public class SetUp
    {
        public string? MerchantId { get; set; }
        public string? ServiceTypeId { get; set; }
        public string? Hash { get; set; }
        public string? PayerName { get; set; }
        public string? PayerEmail { get; set; }
        public string? PayerPhone { get; set; }
        public string? PayerBankCode { get; set; }
        public string? PayerAccount { get; set; }
        public string? RequestId { get; set; }
        public string? Amount { get; set; }
        public string? StartDate { get; set; }
        public string? EndDate { get; set; }
        public string? MandateType { get; set; }
        public string? MaxNoOfDebits { get; set; }
    }
    public class SetUpResponse
    {
        public string? StatusCode { get; set; }
        public string? RequestId { get; set; }
        public string? MandateId { get; set; }
        public string? Status { get; set; }
    }

    public class GetDebitMandateRequest
    {
        public string? API_KEY_TEST { get; set; }
        public long Resource_Id { get; set; }
        public string? Resource_Name { get; set; }
        public long Product_Id { get; set; }
        public string? Action { get; set; }
        public long UserId { get; set; }
    }
    public class GetDebitMandateResponse
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
    }

    public class RequestDebitMandateRequest
    {
        public string? API_KEY_TEST { get; set; }
        public long Resource_Id { get; set; }
        public string? Resource_Name { get; set; }
        public long Product_Id { get; set; }
        public string? Action { get; set; }
        public long UserId { get; set; }
    }
    public class RequestDebitMandateResponse
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
        public RequestDebitMandateResponseData Data { get; set; }
    }
    public class RequestDebitMandateResponseData
    {
        public string? StatusCode { get; set; }
        public List<AuthParamsData> AuthParams { get; set; }
        public string? RequestId { get; set; }
        public string? MandateId { get; set; }
        public string? RemitaTransRef { get; set; }
        public string? Status { get; set; }
    }
    public class AuthParamsData
    {
        public string? Param1 { get; set; }
        public string? Description1 { get; set; }
        public string? Param2 { get; set; }
        public string? Description2 { get; set; }
        public string? Param3 { get; set; }
        public string? Description3 { get; set; }
        public string? Param4 { get; set; }
        public string? Description4 { get; set; }
    }

    public class ValidateOtpRequest
    {
        public string? API_KEY_TEST { get; set; }
        public long Resource_Id { get; set; }
    }
    public class ValidateOtpResponse
    {
        public string? StatusCode { get; set; }
        public string? MandateId { get; set; }
        public string? Status { get; set; }
    }

    public class GetLoanRepaymentResponse
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
        public List<GetLoanRepaymentResponseData> Data { get; set; }
    }
    public class GetLoanRepaymentResponseData
    {
        public string? Due_Date { get; set; }
        public long Total_Outstanding { get; set; }
        public double Interest { get; set; }
        public long Repayment { get; set; }
    }
    #endregion
}
