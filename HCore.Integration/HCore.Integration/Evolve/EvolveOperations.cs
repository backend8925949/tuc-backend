// //==================================================================================
// // FileName: EvolveOperations.cs
// // Author : Harshal Gandole
// // Created On : 
// // Description : Class for defining database related functions
// //
// // COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
// //
// // Revision History:
// // Date             : Changed By        : Comments
// // ---------------------------------------------------------------------------------
// //                 | Harshal Gandole   : Created New File
// //
// //
// //==================================================================================

// using HCore.Integration.Evolve.Resource;
// using HCore.Helper;
// using HCore.Integration.Evolve.AppAPI;
// using Mono.EvolveObject;
// using Newtonsoft.Json;
// using System;
// using System.Collections.Generic;
// using System.IO;
// using System.Linq;
// using System.Net;
// using System.Text;
// using System.Threading.Tasks;
// using static HCore.Helper.HCoreConstant;
// using HCore.Data;
// using HCore.Data.Models;
// using HCore.Integration.Evolve.EvolveObject.Requests;

// namespace HCore.Integration.Evolve
// {
//     public class EvolveOperations
//     {
//         //public EvolveOperations()
//         //{
//         //    SetEndPoints("https://configure-abierta-test.herokuapp.com", "cnfg_tst_0Jw0ajYT144LMQWJwiLTdlL4L");
//         //}

//         ManageAPIResponse _manageAPIResponse;
//         ManageBNPL managebnpl;
//         /// <summary>
//         /// It is an Api key required for tell to the API server that the request it received came from you
//         /// </summary>
//         //public string? API_KEY_TEST { get; private set; }
//         ErrorResponse errorResponse;
//         /// <summary>
//         /// it is an url to call the api
//         /// </summary>
//         //public string? EvolveEndpoint { get; private set; }

//         public string? ResposeMessage { get; private set; }

//         public long Product_Id = 119;
//         public long ProductId
//         {
//             get { return Product_Id; }
//             set { Product_Id = value; }
//         }

//         /// <summary>
//         /// Gets the evolve end point.
//         /// </summary>
//         /// <param name="url"></param>
//         /// <param name="ApiKey"></param>
//         //public void SetEndPoints(string url, String ApiKey)
//         //{
//         //    EvolveEndpoint = url + "/api/v1/";
//         //    API_KEY_TEST = ApiKey;
//         //}

//         /// <summary>
//         ///  Creates the web request.
//         /// </summary>
//         /// <param name="RequestType">It will be POST, GET or PUT</param>
//         /// <param name="url">The url for sending the request</param>
//         /// <param name="payload">The payload will be array of bytes</param>
//         /// <returns></returns>
//         private HttpWebRequest CreateWebRequest(string RequestType, string? url, byte[] payload)
//         {
//             var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
//             httpWebRequest.ContentType = "application/json";
//             httpWebRequest.Method = RequestType;
//             httpWebRequest.Headers["Authorization"] = "Bearer " + _AppConfig.EvolveApiKey;

//             if (payload != null)
//             {
//                 httpWebRequest.ContentLength = payload.Length;
//                 Stream _RequestStream = httpWebRequest.GetRequestStream();
//                 _RequestStream.Write(payload, 0, payload.Length);
//                 _RequestStream.Close();
//             }

//             return httpWebRequest;
//         }

//         /// <summary>
//         ///  
//         /// </summary>
//         /// <param name="request"></param>
//         /// <param name="productId"></param>
//         /// <returns></returns>
//         HCoreContext _HCoreContext;
//         public OResponse CreateUser(CreateUserRequest request)
//         {
//             string request_url = _AppConfig.EvolveUrl + "user/create/with_bvn?product-id=" + _AppConfig.EvolveProductId;
//             using (_HCoreContext = new HCoreContext())
//             {
//                 var _Details = _HCoreContext.HCUAccount.Where(x => x.Id == request.UserReference.AccountId)
//                                   .Select(x => new
//                                   {
//                                       FirstName = x.FirstName,
//                                       LastName = x.LastName,
//                                       EmailAddress = x.EmailAddress,
//                                       MobileNumber = x.MobileNumber,
//                                       Gender = x.Gender.Name
//                                   }).FirstOrDefault();
//                 var _ItemReq = new
//                 {
//                     user = new
//                     {
//                         firstname = _Details.FirstName,
//                         lastname = _Details.LastName,
//                         email = _Details.EmailAddress,
//                         phone = _Details.MobileNumber,
//                         gender = _Details.Gender,
//                         password = HCoreHelper.GenerateGuid(),
//                     },
//                     bvn = request.bvn,
//                 };
//                 string request_json = JsonConvert.SerializeObject(_ItemReq);

//                 byte[] bytes = Encoding.ASCII.GetBytes(request_json);

//                 var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
//                 try
//                 {
//                     HttpWebResponse _HttpWebResponse = null;
//                     HttpStatusCode rescode = HttpStatusCode.Unauthorized;
//                     string _Response = null;
//                     try
//                     {
//                         _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();
//                         using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                         {
//                             _Response = streamReader.ReadToEnd();
//                             rescode = _HttpWebResponse.StatusCode;
//                             Console.WriteLine(_Response);
//                             CreateUserResponse response = JsonConvert.DeserializeObject<CreateUserResponse>(_Response);
//                             if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                             {
//                                 managebnpl = new ManageBNPL();
//                                 var CreateResponse = managebnpl.SaveCustomer(request, response);
//                                 if (CreateResponse.Status == "Success")
//                                 {
//                                     return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Success, response, "BNPL0502", Resource.Resource.BNPL0502);
//                                 }
//                                 else
//                                 {
//                                     return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, null, CreateResponse.ResponseCode, CreateResponse.Message);
//                                 }
//                             }
//                             else
//                             {
//                                 ResposeMessage = response.Message;
//                                 return null;
//                             }
//                         }
//                     }
//                     catch (WebException ex)
//                     {
//                         using (var reader = new StreamReader(ex.Response.GetResponseStream()))
//                         {
//                             _Response = reader.ReadToEnd();
//                             Console.WriteLine(_Response);
//                             CreateUserResponse createuserresponse = JsonConvert.DeserializeObject<CreateUserResponse>(_Response);
//                             if (createuserresponse != null)
//                             {
//                                 //_manageAPIResponse = new ManageAPIResponse();
//                                 //_manageAPIResponse.SaveUser(createuserresponse, request.UserReference, request.bvn);
//                                 managebnpl = new ManageBNPL();
//                                 return managebnpl.SaveCustomer(request, createuserresponse);
//                                 //return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, createuserresponse, "BNPL0200", Resource.Resource.BNPL0200);
//                             }
//                             else
//                             {
//                                 ResposeMessage = createuserresponse.Message;
//                                 return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, createuserresponse, "BNPL0501", Resource.Resource.BNPL0501);
//                                 //return null;
//                             }
//                         }

//                     }
//                 }
//                 catch (WebException ex)
//                 {
//                     return GetWebException<CreateUserResponse>(ex, request.UserReference, "CreateUser");
//                 }
//             }
//         }



//         /// <summary>
//         /// Add Bank Statement the user for BNPL
//         /// </summary>
//         /// <param name="prequalifyUserRequest"></param>
//         /// <param name="ProductId"></param>
//         /// <param name="UserId"></param>
//         /// <returns></returns>
//         public OResponse AddBankStatement(AddBankStatementRequest request, long UserId, long PlanId = 199)
//         {
//             string request_url = _AppConfig.EvolveUrl + "user/" + UserId + "/bank_statements?product-id=" + _AppConfig.EvolveProductId;

//             string request_json = JsonConvert.SerializeObject(request);
//             byte[] bytes = Encoding.ASCII.GetBytes(request_json);

//             var httpwebrequest = CreateWebRequest("POST", request_url, bytes);

//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();
//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     AddBankStatementResponse response = JsonConvert.DeserializeObject<AddBankStatementResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.Created)
//                     {
//                         //_manageAPIResponse = new ManageAPIResponse();
//                         //_manageAPIResponse.SavePrequalification(response, prequalifyUserRequest.userReference);
//                         managebnpl = new ManageBNPL();
//                         managebnpl.UpdateCustomer(response);
//                         #region Send Response
//                         return HCoreHelper.SendResponse(request.userReference, ResponseStatus.Success, response, "BNPL0200", Resource.Resource.BNPL0200);
//                         #endregion
//                     }
//                     else
//                     {
//                         return HCoreHelper.SendResponse(request.userReference, ResponseStatus.Error, null, response.Status, response.Message);
//                     }
//                 }
//             }
//             catch (WebException ex)
//             {
//                 return GetWebException<AddBankStatementResponse>(ex, request.userReference, "AddBankStatement");
//             }
//         }
//         public AddBankStatementResponse AddBankStatementCore(AddBankStatementRequest request, long UserId, long PlanId)
//         {
//             string request_url = _AppConfig.EvolveUrl + "user/" + UserId + "/bank_statements?product-id=" + _AppConfig.EvolveProductId;
//             string request_json = JsonConvert.SerializeObject(request);
//             byte[] bytes = Encoding.ASCII.GetBytes(request_json);
//             var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();
//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     AddBankStatementResponse response = JsonConvert.DeserializeObject<AddBankStatementResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.Created)
//                     {
//                         //_manageAPIResponse = new ManageAPIResponse();
//                         //_manageAPIResponse.SavePrequalification(response, prequalifyUserRequest.userReference);
//                         //managebnpl = new ManageBNPL();
//                         //managebnpl.UpdateCustomer(response);
//                         return response;
//                     }
//                     else
//                     {
//                         return null;
//                         //return HCoreHelper.SendResponse(request.userReference, ResponseStatus.Error, null, response.Status, response.Message);
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 //GetWebException<AddBankStatementResponse>(ex, request.userReference, "AddBankStatement");
//                 HCoreHelper.LogException("AddBankStatementsCore", _Exception, null);
//                 return null;
//             }
//         }


//         /// <summary>
//         /// Create the loan request if user has prequalified for BNPL
//         /// </summary>
//         /// <param name="authrequest"></param>
//         /// <param name="ProductId"></param>
//         /// <param name="UserId"></param>
//         /// <returns></returns>
//         public OResponse CreateLoanRequest(LoanRequest request)
//         {
//             long UserId = 0;
//             using (_HCoreContext = new HCoreContext())
//             {
//                 //var Userdetails = _HCoreContext.BNPLAccount.Where(a => a.AccountId == 587939).FirstOrDefault();
//                 // var Userdetails = _HCoreContext.BNPLAccount.Where(a => a.AccountId == 11116).FirstOrDefault();
//                 var Userdetails = _HCoreContext.BNPLAccount.Where(a => a.AccountId == request.UserReference.AccountId).FirstOrDefault();
//                 if (Userdetails != null)
//                 {
//                     UserId = Userdetails.VenderUserId;
//                 }
//             }
//             //request.amount = (request.amount / 4) * 3;
//             string request_url = _AppConfig.EvolveUrl + "user/" + UserId + "/loan_request?product-id=" + _AppConfig.EvolveProductId;
//             string request_json = JsonConvert.SerializeObject(request);
//             byte[] bytes = Encoding.ASCII.GetBytes(request_json);

//             var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     CreateLoanRequestResponse response = JsonConvert.DeserializeObject<CreateLoanRequestResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         managebnpl = new ManageBNPL();
//                         var _SaveResponse = managebnpl.SaveLoan(response, request);
//                         #region Send Response
//                         return _SaveResponse;
//                         #endregion
//                     }
//                     else
//                     {
//                         return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, null, response.Status, response.Message);
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 //return GetWebException<CreateLoanRequestResponse>(ex, request.UserReference, "CreateLoanRequest");
//                 CreateLoanRequestResponse _Response = new CreateLoanRequestResponse();
//                 _Response.Status = "Error";
//                 _Response.Message = _Exception.Message;
//                 HCoreHelper.LogException("CreateLoanRequest", _Exception, request.UserReference);
//                 return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, _Response, _Response.Status, _Response.Message);
//             }
//         }

//         /// <summary>
//         ///  Submits the loan request after succesfully created
//         /// </summary>
//         /// <param name="ProductId"></param>
//         /// <param name="LoanRequestId"></param>
//         /// <returns></returns>
//         public OResponse SubmitLoanRequest(long ProductId, long LoanRequestId, OUserReference userReference)
//         {
//             string request_url = _AppConfig.EvolveUrl + "loan_request/" + LoanRequestId + "/submit?product-id=" + _AppConfig.EvolveProductId;

//             var httpwebrequest = CreateWebRequest("PUT", request_url, null);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {

//                     string _Response = streamReader.ReadToEnd();
//                     Console.WriteLine(_Response);
//                     CreateLoanRequestResponse response = JsonConvert.DeserializeObject<CreateLoanRequestResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         //_manageAPIResponse = new ManageAPIResponse();
//                         //_manageAPIResponse.SubmitLoanResponse(response, userReference);
//                         managebnpl = new ManageBNPL();
//                         managebnpl.SubmitLoan(response);
//                         #region Send Response
//                         return HCoreHelper.SendResponse(userReference, ResponseStatus.Success, response, "BNPL0200", Resource.Resource.BNPL0200);
//                         #endregion
//                     }
//                     else
//                     {
//                         return HCoreHelper.SendResponse(userReference, ResponseStatus.Error, null, response.Status, response.Message);
//                     }
//                 }
//             }
//             catch (WebException ex)
//             {
//                 return GetWebException<CreateLoanRequestResponse>(ex, userReference, "SubmitLoanRequest");
//             }
//         }
//         public CreateLoanRequestResponse SubmitLoanRequestCore(long ProductId, long LoanRequestId, OUserReference userReference)
//         {
//             string request_url = _AppConfig.EvolveUrl + "loan_request/" + LoanRequestId + "/submit?product-id=" + _AppConfig.EvolveProductId;

//             var httpwebrequest = CreateWebRequest("PUT", request_url, null);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {

//                     string _Response = streamReader.ReadToEnd();
//                     Console.WriteLine(_Response);
//                     CreateLoanRequestResponse response = JsonConvert.DeserializeObject<CreateLoanRequestResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         return response;
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (WebException ex)
//             {
//                 GetWebException<CreateLoanRequestResponse>(ex, userReference, "SubmitLoanRequest");
//                 return null;
//             }
//         }

//         /// <summary>
//         /// Adds user bank details for loan reuest
//         /// </summary>
//         /// <param name="bankDetails"></param>
//         /// <param name="ProductId"></param>
//         /// <param name="LoanRequestId"></param>
//         /// <returns></returns>
//         public OResponse AddUserbankDetails(BankDetails request, long ProductId, long LoanRequestId)
//         {
//             string request_url = _AppConfig.EvolveUrl + "loan_request/" + LoanRequestId + "/bank_details?product-id=" + _AppConfig.EvolveProductId;

//             string request_json = JsonConvert.SerializeObject(request);
//             byte[] bytes = Encoding.ASCII.GetBytes(request_json);

//             var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {

//                     string _Response = streamReader.ReadToEnd();
//                     Console.WriteLine(_Response);
//                     AddUserBankDetailsResponse response = JsonConvert.DeserializeObject<AddUserBankDetailsResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         //_manageAPIResponse = new ManageAPIResponse();
//                         //_manageAPIResponse.UpdatePrequalification(bankDetails, response, bankDetails.userReference);
//                         managebnpl = new ManageBNPL();
//                         managebnpl.SaveUserBankDetails(response, LoanRequestId);
//                         #region Send Response
//                         return HCoreHelper.SendResponse(request.userReference, ResponseStatus.Success, response, "BNPL0200", Resource.Resource.BNPL0200);
//                         #endregion
//                     }
//                     else
//                     {
//                         return HCoreHelper.SendResponse(request.userReference, ResponseStatus.Error, null, response.Status, response.Message);
//                     }
//                 }
//             }
//             catch (WebException ex)
//             {
//                 return GetWebException<AddUserBankDetailsResponse>(ex, request.userReference, "AddUserbankDetails");
//             }
//         }
//         public AddUserBankDetailsResponse AddUserbankDetailsCore(BankDetails request, long ProductId, long LoanRequestId)
//         {
//             string request_url = _AppConfig.EvolveUrl + "loan_request/" + LoanRequestId + "/bank_details?product-id=" + _AppConfig.EvolveProductId;

//             string request_json = JsonConvert.SerializeObject(request);
//             byte[] bytes = Encoding.ASCII.GetBytes(request_json);

//             var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();
//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {

//                     string _Response = streamReader.ReadToEnd();
//                     Console.WriteLine(_Response);
//                     AddUserBankDetailsResponse response = JsonConvert.DeserializeObject<AddUserBankDetailsResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         //_manageAPIResponse = new ManageAPIResponse();
//                         //_manageAPIResponse.UpdatePrequalification(bankDetails, response, bankDetails.userReference);
//                         managebnpl = new ManageBNPL();
//                         var _Respose = managebnpl.SaveUserBankDetails(response, LoanRequestId);
//                         #region Send Response
//                         return response;
//                         #endregion
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (WebException ex)
//             {
//                 GetWebException<AddUserBankDetailsResponse>(ex, request.userReference, "AddUserbankDetails");
//                 return null;
//             }
//         }

//         /// <summary>
//         /// Approves the loan request when successfully added user bank details
//         /// </summary>
//         /// <param name="authrequest"></param>
//         /// <param name="ProductId"></param>
//         /// <param name="action"></param>
//         /// <param name="LoanRequestId"></param>
//         /// <returns></returns>
//         public OResponse ApproveLoanRequest(ApproveLoanRequest request, long ProductId, long LoanRequestId)
//         {
//             //long Approved_Amount = (Amount / 4) * 3;
//             string request_url = _AppConfig.EvolveUrl + "loan_request/" + LoanRequestId + "/action?action=approve" + "&product-id=" + _AppConfig.EvolveProductId;

//             string request_json = JsonConvert.SerializeObject(request);
//             byte[] bytes = Encoding.ASCII.GetBytes(request_json);

//             var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {

//                     string _Response = streamReader.ReadToEnd();
//                     Console.WriteLine(_Response);
//                     ApproveLoanResponse response = JsonConvert.DeserializeObject<ApproveLoanResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         //_manageAPIResponse = new ManageAPIResponse();
//                         //_manageAPIResponse.ApproveLoanResponse(response, request.userReference);
//                         managebnpl = new ManageBNPL();
//                         managebnpl.ApproveLoan(response);
//                         //managebnpl.OAproveLoan(response);
//                         #region Send Response
//                         return HCoreHelper.SendResponse(null, ResponseStatus.Success, response, "BNPL0200", Resource.Resource.BNPL0200);
//                         #endregion
//                     }
//                     else
//                     {
//                         return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, response.Status, response.Message);
//                     }
//                 }
//             }
//             catch (WebException ex)
//             {
//                 return GetWebException<ApproveLoanResponse>(ex, null, "ApproveLoanRequest");
//             }
//         }
//         public ApproveLoanResponse ApproveLoanRequestCore(ApproveLoanRequest request, long ProductId, long LoanRequestId)
//         {
//             //long Approved_Amount = (Amount / 4) * 3;
//             string request_url = _AppConfig.EvolveUrl + "loan_request/" + LoanRequestId + "/action?action=approve" + "&product-id=" + _AppConfig.EvolveProductId;

//             string request_json = JsonConvert.SerializeObject(request);
//             byte[] bytes = Encoding.ASCII.GetBytes(request_json);

//             var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {

//                     string _Response = streamReader.ReadToEnd();
//                     ApproveLoanResponse response = JsonConvert.DeserializeObject<ApproveLoanResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         //_manageAPIResponse = new ManageAPIResponse();
//                         //_manageAPIResponse.ApproveLoanResponse(response, request.userReference);

//                         return response;
//                         //managebnpl.OAproveLoan(response);
//                         //#region Send Response
//                         //return HCoreHelper.SendResponse(request.userReference, ResponseStatus.Success, response, "BNPL0200", Resource.Resource.BNPL0200);
//                         //#endregion
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (WebException ex)
//             {
//                 return null;
//             }
//         }


//         /// <summary>
//         /// SetUp the debit mandate after loan request approved
//         /// </summary>
//         /// <param name="ProductId"></param>
//         /// <param name="LoanRequestId"></param>
//         /// <returns></returns>
//         public OResponse SetUpDebitMandate(long ProductId, long LoanRequestId, OUserReference userReference)
//         {
//             string request_url = _AppConfig.EvolveUrl + LoanRequestId + "/debit_mandate?product-id=" + _AppConfig.EvolveProductId + "&action=setup&vendor=remita&resource-name=loan_request_id";

//             var httpwebrequest = CreateWebRequest("POST", request_url, null);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {

//                     string _Response = streamReader.ReadToEnd();
//                     Console.WriteLine(_Response);
//                     SetUpDebitMandateResponse response = JsonConvert.DeserializeObject<SetUpDebitMandateResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         //_manageAPIResponse = new ManageAPIResponse();
//                         //_manageAPIResponse.SaveDebitMandate(response, userReference);
//                         managebnpl = new ManageBNPL();
//                         managebnpl.SetUpDebitMandate(response);
//                         #region Send Response
//                         return HCoreHelper.SendResponse(userReference, ResponseStatus.Success, response, "BNPL0200", Resource.Resource.BNPL0200);
//                         #endregion
//                     }
//                     else
//                     {
//                         return HCoreHelper.SendResponse(userReference, ResponseStatus.Error, null, response.Status, response.Message);
//                     }
//                 }
//             }
//             catch (WebException ex)
//             {
//                 return GetWebException<SetUpDebitMandateResponse>(ex, userReference, "SetUpDebitMandate");
//             }
//         }

//         public SetUpDebitMandateResponse SetUpDebitMandateCore(long ProductId, long LoanRequestId)
//         {
//             string request_url = _AppConfig.EvolveUrl + LoanRequestId + "/debit_mandate?product-id=" + _AppConfig.EvolveProductId + "&action=setup&vendor=remita&resource-name=loan_request_id";
//             var httpwebrequest = CreateWebRequest("POST", request_url, null);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();
//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     SetUpDebitMandateResponse response = JsonConvert.DeserializeObject<SetUpDebitMandateResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         //_manageAPIResponse = new ManageAPIResponse();
//                         //_manageAPIResponse.SaveDebitMandate(response, userReference);

//                         return response;
//                         //#region Send Response
//                         //return HCoreHelper.SendResponse(null, ResponseStatus.Success, response, "BNPL0200", Resource.Resource.BNPL0200);
//                         //#endregion
//                     }
//                     else
//                     {
//                         return null;
//                         //return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, response.Status, response.Message);
//                     }
//                 }
//             }
//             catch (WebException ex)
//             {
//                 GetWebException<SetUpDebitMandateResponse>(ex, null, "SetUpDebitMandateCore");
//                 return null;
//             }
//         }

//         public SetUpDebitMandateResponse SetUpDebitMandate(long loanrequestid)
//         {
//             string request_url = _AppConfig.EvolveUrl + loanrequestid + "/debit_mandate?product-id=" + _AppConfig.EvolveProductId + "&action=setup&vendor=remita&resource-name=loan_request_id";
//             var httpwebrequest = CreateWebRequest("POST", request_url, null);
//             try
//             {
//                 HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();
//                 using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
//                 {
//                     string webresponse = streamReader.ReadToEnd();
//                     Console.WriteLine(webresponse);
//                     SetUpDebitMandateResponse response = JsonConvert.DeserializeObject<SetUpDebitMandateResponse>(webresponse);
//                     if (httpwebresponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         return response;
//                     }
//                     else
//                     {
//                         ResposeMessage = response.Message;
//                         return null;
//                     }
//                 }
//             }
//             catch (Exception ex)
//             {
//                 Console.WriteLine(ex.ToString());
//                 return null;
//             }
//         }

//         /// <summary>
//         /// Requests the OTP for activating the user account for debit mandate
//         /// </summary>
//         /// <param name="ProductId"></param>
//         /// <param name="LoanRequestId"></param>
//         /// <returns></returns>
//         public OResponse RequestDebitMandateOTP(long ProductId, long LoanRequestId, OUserReference userReference)
//         {
//             string request_url = _AppConfig.EvolveUrl + LoanRequestId + "/debit_mandate?vendor=remita&resource-name=loan_request_id&action=request-otp&product-id=" + _AppConfig.EvolveProductId;

//             var httpwebrequest = CreateWebRequest("POST", request_url, null);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {

//                     string _Response = streamReader.ReadToEnd();
//                     Console.WriteLine(_Response);
//                     RequestDebitMandateOTPResponse response = JsonConvert.DeserializeObject<RequestDebitMandateOTPResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         #region Send Response
//                         return HCoreHelper.SendResponse(userReference, ResponseStatus.Success, response, "BNPL0200", Resource.Resource.BNPL0200);
//                         #endregion
//                     }
//                     else
//                     {
//                         return HCoreHelper.SendResponse(userReference, ResponseStatus.Error, null, response.Status, response.Message);
//                     }
//                 }
//             }
//             catch (WebException ex)
//             {
//                 return GetWebException<RequestDebitMandateOTPResponse>(ex, userReference, "RequestDebitMandateOTP");
//             }
//         }


//         public RequestDebitMandateOTPResponse RequestDebitMandateOTPCore(long ProductId, long LoanRequestId)
//         {
//             string request_url = _AppConfig.EvolveUrl + LoanRequestId + "/debit_mandate?vendor=remita&resource-name=loan_request_id&action=request-otp&product-id=" + _AppConfig.EvolveProductId;

//             var httpwebrequest = CreateWebRequest("POST", request_url, null);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {

//                     string _Response = streamReader.ReadToEnd();
//                     Console.WriteLine(_Response);
//                     RequestDebitMandateOTPResponse response = JsonConvert.DeserializeObject<RequestDebitMandateOTPResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         #region Send Response
//                         return response;
//                         #endregion
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (WebException _Exception)
//             {
//                 HCoreHelper.LogException("RequestDebitMandateOTPCore", _Exception, null);
//                 return null;
//             }
//         }

//         /// <summary>
//         /// Validates the OTP for user bank account activation to complete the debit mandate process
//         /// </summary>
//         /// <param name="authrequest"></param>
//         /// <param name="ProductId"></param>
//         /// <param name="LoanRequestId"></param>
//         /// <returns></returns>
//         public OResponse ValidateOTP(OTPValidationRequest request)
//         {
//             string request_url = _AppConfig.EvolveUrl + request.loanRequestId + "/debit_mandate?vendor=remita&resource-name=loan_request_id&product-id=" + _AppConfig.EvolveProductId + "&action=validate-otp";
//             var _Req = new
//             {
//                 reqs = request.reqs,
//                 param1 = request.param1,
//                 value1 = request.value1,
//                 param2 = request.param2,
//                 value2 = request.value2,
//                 param3 = request.param3,
//                 value3 = request.value3,
//                 param4 = request.param4,
//                 value4 = request.value4
//             };

//             string request_json = JsonConvert.SerializeObject(_Req);
//             byte[] bytes = Encoding.ASCII.GetBytes(request_json);
//             var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     OTPValidationResponse response = JsonConvert.DeserializeObject<OTPValidationResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         using (_HCoreContext = new HCoreContext())
//                         {
//                             var Prox = _HCoreContext.BNPLLoanProcess.Where(x => x.Loan.VenderLoanId == request.loanRequestId).FirstOrDefault();
//                             if (Prox != null)
//                             {
//                                 Prox.DebitMandateStatusId = 2;
//                                 _HCoreContext.SaveChanges();
//                             }
//                         }
//                         #region Send Response
//                         return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Success, response, "BNPL0200", "Debit mandate activated successfully");
//                         #endregion
//                     }
//                     else
//                     {
//                         //OTPValidationResponse OTPValidationResponse = new OTPValidationResponse();
//                         //OTPValidationResponse.MandateId = "MNDT89229";
//                         //OTPValidationResponse.StatusCode = "00";
//                         //OTPValidationResponse.Status = "Success";
//                         //OTPValidationResponse.Message = "Debit Mandate Activated Successfully.";
//                         //return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Success, OTPValidationResponse, OTPValidationResponse.Status, OTPValidationResponse.Message);
//                         return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, null, response.Status, response.Message);
//                     }
//                 }
//             }
//             catch (Exception _Exception)
//             {
//                 //OTPValidationResponse OTPValidationResponse = new OTPValidationResponse();
//                 //OTPValidationResponse.MandateId = "MNDT89229";
//                 //OTPValidationResponse.StatusCode = "00";
//                 //OTPValidationResponse.Status = "Success";
//                 //OTPValidationResponse.Message = "Debit Mandate Activated Successfully.";
//                 //return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Success, OTPValidationResponse, OTPValidationResponse.Status, OTPValidationResponse.Message);
//                 return HCoreHelper.LogException("ValidateOTP", _Exception, null, "HC0500", "Internal Server Error Occuured. Please Try After Some Time");
//             }
//         }

//         public GetAllRepayments GetAllRepayments(long ProductId)
//         {
//             DateTime Date = DateTime.UtcNow.Date;
//             string request_url = _AppConfig.EvolveUrl + "repayment?product-id=" + _AppConfig.EvolveProductId + "&id=all&start=2021-05-01&end=" + Date.Date.Date.ToString("yyyy-MM-dd");
//             var httpwebrequest = CreateWebRequest("GET", request_url, null);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     GetAllRepayments response = JsonConvert.DeserializeObject<GetAllRepayments>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         return response;
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (Exception ex)
//             {
//                 HCoreHelper.LogException("GetAllRepayments", ex, null);
//                 return null;
//             }
//         }

//         /// <summary>
//         /// Gets the debit mandate process
//         /// </summary>
//         /// <param name="ProductId"></param>
//         /// <param name="LoanRequestId"></param>
//         /// <returns></returns>
//         public OResponse GetDebitMandate(long ProductId, long LoanRequestId, OUserReference userReference)
//         {
//             string request_url = _AppConfig.EvolveUrl + LoanRequestId + "/debit_mandate?vendor=remita&resource-name=loan_request_id&action=get-mandate&product-id=" + _AppConfig.EvolveProductId;

//             var httpwebrequest = CreateWebRequest("POST", request_url, null);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {

//                     string _Response = streamReader.ReadToEnd();
//                     Console.WriteLine(_Response);
//                     GetDebitMandateResponse response = JsonConvert.DeserializeObject<GetDebitMandateResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         #region Send Response
//                         return HCoreHelper.SendResponse(userReference, ResponseStatus.Success, response, "BNPL0200", Resource.Resource.BNPL0200);
//                         #endregion
//                     }
//                     else
//                     {
//                         return HCoreHelper.SendResponse(userReference, ResponseStatus.Error, null, response.Status, response.Message);
//                     }
//                 }
//             }
//             catch (WebException ex)
//             {
//                 return GetWebException<GetDebitMandateResponse>(ex, userReference, "GetDebitMandate");
//             }
//         }

//         /// <summary>
//         /// It will generates the the loan repayment structure afer user successfully completes the debit mandate process
//         /// </summary>
//         /// <param name="ProductId"></param>
//         /// <param name="LoanRequestId"></param>
//         /// <returns></returns>
//         public OResponse GetLoanRepaymentStructure(long ProductId, long LoanRequestId, OUserReference userReference)
//         {
//             try
//             {
//                 string request_url = _AppConfig.EvolveUrl + "loan_request/" + LoanRequestId + "/repayment?product-id=" + _AppConfig.EvolveProductId;
//                 var httpwebrequest = CreateWebRequest("GET", request_url, null);
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {

//                     string _Response = streamReader.ReadToEnd();
//                     GetLoanRepaymentResponse response = JsonConvert.DeserializeObject<GetLoanRepaymentResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         //managebnpl = new ManageBNPL();
//                         //managebnpl.SaveLoanRepaymentsStructure(response, LoanRequestId);
//                         #region Send Response
//                         return HCoreHelper.SendResponse(userReference, ResponseStatus.Success, response, "BNPL0200", Resource.Resource.BNPL0200);
//                         #endregion
//                     }
//                     else
//                     {
//                         return HCoreHelper.SendResponse(userReference, ResponseStatus.Error, null, response.Status, response.Message);
//                     }
//                 }
//             }
//             catch (WebException ex)
//             {
//                 return GetWebException<GetLoanRepaymentResponse>(ex, userReference, "GetLoanRepaymentStructure");
//             }
//         }

//         public GetRepaymentsStructure GetLoanRepaymentStructureCore(long ProductId, long LoanRequestId)
//         {
//             try
//             {
//                 string request_url = _AppConfig.EvolveUrl + "loan/" + LoanRequestId + "/repayment?product-id=" + _AppConfig.EvolveProductId + "&section=repayment";
//                 var httpwebrequest = CreateWebRequest("GET", request_url, null);
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {

//                     string _Response = streamReader.ReadToEnd();
//                     GetRepaymentsStructure response = JsonConvert.DeserializeObject<GetRepaymentsStructure>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         //managebnpl = new ManageBNPL();
//                         //managebnpl.SaveLoanRepaymentsStructure(response, LoanRequestId);
//                         #region Send Response
//                         return response;
//                         #endregion
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (WebException ex)
//             {
//                 return null;
//             }
//         }

//         /// <summary>
//         /// After the loan repayment structure generated you will nedd to add Merchant bank details for disbursing the loan
//         /// </summary>
//         /// <param name="authrequest"></param>
//         /// <param name="ProductId"></param>
//         /// <param name="LoanRequestId"></param>
//         /// <returns></returns>
//         public OResponse AddMerchantbankDetails(BankDetails request, long ProductId, long LoanRequestId)
//         {
//             string request_url = _AppConfig.EvolveUrl + "loan_request/" + LoanRequestId + "/merchant_bank_details?product-id=" + _AppConfig.EvolveProductId;
//             string request_json = JsonConvert.SerializeObject(request);
//             byte[] bytes = Encoding.ASCII.GetBytes(request_json);

//             var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {

//                     string _Response = streamReader.ReadToEnd();
//                     Console.WriteLine(_Response);
//                     AddMerchantBankDetailsResponse response = JsonConvert.DeserializeObject<AddMerchantBankDetailsResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         #region Send Response
//                         return HCoreHelper.SendResponse(request.userReference, ResponseStatus.Success, response, "BNPL0200", Resource.Resource.BNPL0200);
//                         #endregion
//                     }
//                     else
//                     {
//                         return HCoreHelper.SendResponse(request.userReference, ResponseStatus.Error, null, response.Status, response.Message);
//                     }
//                 }
//             }
//             catch (WebException ex)
//             {
//                 return GetWebException<AddMerchantBankDetailsResponse>(ex, request.userReference, "AddMerchantbankDetails");
//             }
//         }
//         public AddMerchantBankDetailsResponse AddMerchantbankDetailsCore(long ProductId, long LoanRequestId)
//         {
//             string request_url = _AppConfig.EvolveUrl + "loan_request/" + LoanRequestId + "/merchant_bank_details?product-id=" + _AppConfig.EvolveProductId;

//             var _ReqItem = new
//             {
//                 account_name = "Connected analytics limited",
//                 account_number = "1300213877",
//                 bank_name = "Providus bank",
//                 code = "101",
//             };
//             string request_json = JsonConvert.SerializeObject(_ReqItem);
//             byte[] bytes = Encoding.ASCII.GetBytes(request_json);

//             var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {

//                     string _Response = streamReader.ReadToEnd();
//                     AddMerchantBankDetailsResponse response = JsonConvert.DeserializeObject<AddMerchantBankDetailsResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         return response;
//                     }
//                     else
//                     {
//                         return null;
//                     }
//                 }
//             }
//             catch (WebException ex)
//             {
//                 GetWebException<AddMerchantBankDetailsResponse>(ex, null, "AddMerchantbankDetails");
//                 return null;
//             }
//         }

//         /// <summary>
//         /// It will disburse the loan
//         /// </summary>
//         /// <param name="channel"></param>
//         /// <param name="note"></param>
//         /// <param name="LoanRequestId"></param>
//         /// <returns></returns>
//         public LoanDisbursementResponse DisburseLoan(string note, long LoanRequestId, OUserReference userReference)
//         {
//             string request_url = _AppConfig.EvolveUrl + "loan_request/" + LoanRequestId + "/disburse?channel=disbursement-wallet&note=" + note;

//             var httpwebrequest = CreateWebRequest("POST", request_url, null);
//             try
//             {
//                 HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

//                 using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
//                 {
//                     string _Response = streamReader.ReadToEnd();
//                     LoanDisbursementResponse response = JsonConvert.DeserializeObject<LoanDisbursementResponse>(_Response);
//                     if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
//                     {
//                         #region Send Response
//                         managebnpl = new ManageBNPL();
//                         var Respone = managebnpl.OAproveLoan(response);
//                         HCoreHelper.SendResponse(userReference, ResponseStatus.Success, response, "BNPL0200", Resource.Resource.BNPL0200);
//                         return response;
//                         #endregion
//                     }
//                     else
//                     {
//                         HCoreHelper.SendResponse(userReference, ResponseStatus.Error, null, response.Status, response.Message);
//                         return null;
//                     }
//                 }
//             }
//             catch (WebException ex)
//             {
//                 HCoreHelper.LogException("CheckUserExists", ex, userReference, null, "BNPL0500", Resource.Resource.BNPL0500);
//                 return null;
//             }
//         }

//         /// <summary>
//         /// It will check that the user exists or not.
//         /// </summary>
//         /// <param name="request"></param>
//         /// <returns></returns>
//         public OResponse CheckUserExists(CreateUserRequest request)
//         {
//             try
//             {
//                 _manageAPIResponse = new ManageAPIResponse();
//                 bool isExists = _manageAPIResponse.CheckUserExists(request);
//                 return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Success, isExists, "BNPL0409", Resource.Resource.BNPL0200);
//             }
//             catch (Exception ex)
//             {
//                 return HCoreHelper.LogException("CheckUserExists", ex, request.UserReference, null, "BNPL0500", Resource.Resource.BNPL0500);
//             }
//         }

//         /// <summary>
//         /// 
//         /// </summary>
//         /// <param name="request"></param>
//         /// <returns></returns>
//         public OResponse CheckMonoId(CreateUserRequest request)
//         {
//             try
//             {
//                 managebnpl = new ManageBNPL();
//                 var Response = managebnpl.CheckMonoId(request);
//                 return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Success, Response, "BNPL0409", Resource.Resource.BNPL0200);
//             }
//             catch (Exception ex)
//             {
//                 return HCoreHelper.LogException("CheckUserExists", ex, request.UserReference, null, "BNPL0500", Resource.Resource.BNPL0500);
//             }
//         }

//         /// <summary>
//         /// 
//         /// </summary>
//         /// <param name="request"></param>
//         /// <returns></returns>
//         public OResponse CheckCreditLimit(CreateUserRequest request)
//         {
//             try
//             {
//                 managebnpl = new ManageBNPL();
//                 bool Exists = managebnpl.CheckCreditLimit(request);
//                 return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Success, Exists, "BNPL0409", Resource.Resource.BNPL0200);
//             }
//             catch (Exception ex)
//             {
//                 return HCoreHelper.LogException("CheckUserExists", ex, request.UserReference, null, "BNPL0500", Resource.Resource.BNPL0500);
//             }
//         }



//         /// <summary>
//         /// It will handle web exception.
//         /// </summary>
//         /// <typeparam name="T"></typeparam>
//         /// <param name="ex"></param>
//         /// <param name="userReference"></param>
//         /// <param name="methodname"></param>
//         /// <returns></returns>
//         private OResponse GetWebException<T>(WebException ex, OUserReference userReference, string? methodname) where T : class
//         {
//             try
//             {
//                 using (var reader = new StreamReader(ex.Response.GetResponseStream()))
//                 {
//                     string _Response = reader.ReadToEnd();
//                     T response = JsonConvert.DeserializeObject<T>(_Response);
//                     if (response != null)
//                     {
//                         #region Send Response
//                         return HCoreHelper.SendResponse(userReference, ResponseStatus.Error, response, "BNPL0200", "Unable to process your request. Please try after some time");
//                         #endregion
//                     }
//                     else
//                     {
//                         HCoreHelper.LogException(methodname, ex, userReference);
//                         return HCoreHelper.SendResponse(userReference, ResponseStatus.Error, null, Resource.Resource.BNPL0500);
//                     }
//                 }
//             }
//             catch (Exception exception)
//             {
//                 HCoreHelper.LogException(methodname, ex, userReference);
//                 return HCoreHelper.SendResponse(userReference, ResponseStatus.Error, null, Resource.Resource.BNPL0500);
//             }
//         }
//     }
// }
