//==================================================================================
// FileName: PaystackTransfer.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
namespace HCore.Integration.Paystack
{
    public class PSBank
    {
        public class Response
        {
            public bool status { get; set; }
            public string? message { get; set; }
            public List<List> data { get; set; }
        }
        public class List
        {
            public long id { get; set; }
            public string? name { get; set; }
            public string? slug { get; set; }
            public string? code { get; set; }
            public string? longcode { get; set; }
            public string? gateway { get; set; }
            //public bool active { get; set; }
            //public bool pay_with_bank { get; set; }
            //public bool is_deleted { get; set; }
            public string? country { get; set; }
            public string? currency { get; set; }
            public string? type { get; set; }
            public string? typeid { get; set; }
            public DateTime? createdAt { get; set; }
            public DateTime? updatedAt { get; set; }
        }
    }
    public class PSAccountDetails
    {
        public class Response
        {
            public bool status { get; set; }
            public string? message { get; set; }
            public Details data { get; set; }
        }
        public class Details
        {
            public string? account_number { get; set; }
            public string? account_name { get; set; }
            public string? bank_id { get; set; }
        }
    }
    public class PSAccountTransfer
    {

        public class Request
        {
            public long amount { get; set; }
            public string? recipient { get; set; }
            public string? reason { get; set; }
            public string? reference { get; set; }
            public string? source { get; set; }
        }

        public class Response
        {
            public bool status { get; set; }
            public string? message { get; set; }
            public Data data { get; set; }
        }
        public class Data
        {
            public bool active { get; set; }
            public DateTime? createdAt { get; set; }
            public string? currency { get; set; }
            public string? domain { get; set; }
            public string? id { get; set; }
            public long integration { get; set; }
            public string? name { get; set; }
            public string? recipient_code { get; set; }
            public string? transfer_code { get; set; }
            public DateTime? updatedAt { get; set; }
            public bool is_deleted { get; set; }
            public Details details { get; set; }

            public string? request { get; set; }
            public string? response { get; set; }

        }
        public class Details
        {
            public string? authorization_code { get; set; }
            public string? account_number { get; set; }
            public string? account_name { get; set; }
            public string? bank_code { get; set; }
            public string? bank_name { get; set; }

        }
    }

    public class PSAccountTransferInitialize
    {

        public class Request
        {
            public string? source { get; set; }
            public double amount { get; set; }
            public string? recipient { get; set; }
            public string? reference { get; set; }
        }

        public class Response
        {
            public bool status { get; set; }
            public string? message { get; set; }
            public Data data { get; set; }
        }
        public class Data
        {
            public bool active { get; set; }
            public DateTime? createdAt { get; set; }
            public string? currency { get; set; }
            public string? domain { get; set; }
            public long id { get; set; }
            public long integration { get; set; }
            public string? name { get; set; }
            public string? recipient_code { get; set; }
            public DateTime? updatedAt { get; set; }
            public bool is_deleted { get; set; }
            public Details details { get; set; }
        }
        public class Details
        {
            public string? authorization_code { get; set; }
            public string? account_number { get; set; }
            public string? account_name { get; set; }
            public string? bank_code { get; set; }
            public string? bank_name { get; set; }

        }
    }


    public static class PaystackTransfer
    {
        public static List<PSBank.List> GetBanksList(string PrivateKey)
        {
            try
            {
                string PayStackUrl = "https://api.paystack.co/bank";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(PayStackUrl);
                request.ContentType = "application/json";
                request.Headers["Authorization"] = "Bearer " + PrivateKey;
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    string _Response = reader.ReadToEnd();
                    PSBank.Response _RequestBodyContent = JsonConvert.DeserializeObject<PSBank.Response>(_Response);
                    if (_RequestBodyContent != null)
                    {
                        if (_RequestBodyContent.status == true)
                        {
                            if (_RequestBodyContent.data != null)
                            {
                                return _RequestBodyContent.data;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                return null;
            }
        }
        public static PSAccountDetails.Details VerifyAccountNumber(string PrivateKey, string? AccountNumber, string? BankCode)
        {
            try
            {
                string PayStackUrl = "https://api.paystack.co/bank/resolve?account_number=" + AccountNumber + "&bank_code=" + BankCode;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(PayStackUrl);
                request.ContentType = "application/json";
                request.Headers["Authorization"] = "Bearer " + PrivateKey;
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    string _Response = reader.ReadToEnd();
                    PSAccountDetails.Response _RequestBodyContent = JsonConvert.DeserializeObject<PSAccountDetails.Response>(_Response);
                    if (_RequestBodyContent != null)
                    {
                        if (_RequestBodyContent.status == true)
                        {
                            if (_RequestBodyContent.data != null)
                            {
                                return _RequestBodyContent.data;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                return null;
            }
        }
        public static PSAccountTransfer.Data CreateRecepient(string PrivateKey, string? Name, string AccountNumber, string? BankCode)
        {
            try
            {
                string ServerUrl = "https://api.paystack.co/transferrecipient";
                //PSAccountTransfer.Request _Request = new PSAccountTransfer.Request();
                //          var _Req = new object
                //             "type" = "nuban",
                //              "name": "Zombie",
                //"description": "Zombier",
                //"account_number": "01000000010",
                //"bank_code": "044",
                //"currency": "NGN"
                //          };
                var _Request = new
                {
                    type = "nuban",
                    description = "TUC Customer",
                    name = Name,
                    account_number = AccountNumber,
                    bank_code = BankCode,
                    currency = "NGN",
                };
                //_Request.recipient = RecipientCode;
                //_Request.reason = Description;
                //_Request.reference = ReferenceNumber;
                string RequestContent = JsonConvert.SerializeObject(_Request);
                HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ServerUrl);
                byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
                _HttpWebRequest.ContentType = "application/json";
                _HttpWebRequest.ContentLength = bytes.Length;
                _HttpWebRequest.Method = "POST";
                _HttpWebRequest.Headers["Authorization"] = "Bearer " + PrivateKey;
                Stream _RequestStream = _HttpWebRequest.GetRequestStream();
                _RequestStream.Write(bytes, 0, bytes.Length);
                _RequestStream.Close();
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                if (_HttpWebResponse.StatusCode == HttpStatusCode.OK || _HttpWebResponse.StatusCode == HttpStatusCode.Created)
                {
                    Stream responseStream = _HttpWebResponse.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();
                    PSAccountTransfer.Response _RequestBodyContent = JsonConvert.DeserializeObject<PSAccountTransfer.Response>(responseStr);
                    if (_RequestBodyContent.status == true)
                    {
                        return _RequestBodyContent.data;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception _Exception)
            {
                return null;
            }
        }

        public static PSAccountTransfer.Data CreateTransfer(string PrivateKey, string? ReferenceNumber, string? RecipientCode, double Amount, string? Description)
        {
            string RequestContent = null;
            try
            {
                string ServerUrl = "https://api.paystack.co/transfer";
                PSAccountTransfer.Request _Request = new PSAccountTransfer.Request();
                _Request.amount = Convert.ToInt64(Amount * 100);
                _Request.recipient = RecipientCode;
                _Request.reason = Description;
                _Request.reference = ReferenceNumber.ToLower();
                _Request.source = "balance";

                RequestContent = JsonConvert.SerializeObject(_Request);
                HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ServerUrl);
                byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
                _HttpWebRequest.ContentType = "application/json";
                _HttpWebRequest.ContentLength = bytes.Length;
                _HttpWebRequest.Method = "POST";
                _HttpWebRequest.Headers["Authorization"] = "Bearer " + PrivateKey;
                Stream _RequestStream = _HttpWebRequest.GetRequestStream();
                _RequestStream.Write(bytes, 0, bytes.Length);
                _RequestStream.Close();
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = _HttpWebResponse.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();
                    HCore.Helper.HCoreHelper.LogData(Helper.HCoreConstant.LogType.Log, "PAYOUTRESPONSE", "PAYOUT", responseStr, null);
                    PSAccountTransfer.Response _RequestBodyContent = JsonConvert.DeserializeObject<PSAccountTransfer.Response>(responseStr);
                    if (_RequestBodyContent.status == true)
                    {
                        _RequestBodyContent.data.request = RequestContent;
                        _RequestBodyContent.data.response = responseStr;
                        return _RequestBodyContent.data;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogData(HCoreConstant.LogType.Exception, "CreateTransfer", RequestContent, _Exception.ToString(), null);
                HCoreHelper.LogException("CreateTransfer", _Exception);
                return null;
            }
        }
        //public static PSAccountTransfer.Data InitiateTransfer(string PrivateKey, string? AccountName, string? AccountNumber, string? BankCode, string? Description)
        //{
        //    try
        //    {
        //        string ServerUrl = "https://testapi.topuplcc.com/api/v2/getaccount";
        //        PSAccountTransfer.Request _Request = new PSAccountTransfer.Request();
        //        _Request.amount = "nuban";
        //        _Request.name = AccountName;
        //        _Request.account_number = AccountNumber;
        //        _Request.bank_code = BankCode;
        //        string RequestContent = JsonConvert.SerializeObject(_Request);
        //        HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ServerUrl);
        //        byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
        //        _HttpWebRequest.ContentType = "application/json";
        //        _HttpWebRequest.ContentLength = bytes.Length;
        //        _HttpWebRequest.Method = "POST";
        //        _HttpWebRequest.Headers["Authorization"] = "Bearer " + PrivateKey;
        //        Stream _RequestStream = _HttpWebRequest.GetRequestStream();
        //        _RequestStream.Write(bytes, 0, bytes.Length);
        //        _RequestStream.Close();
        //        HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
        //        if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
        //        {
        //            Stream responseStream = _HttpWebResponse.GetResponseStream();
        //            string responseStr = new StreamReader(responseStream).ReadToEnd();
        //            PSAccountTransfer.Response _RequestBodyContent = JsonConvert.DeserializeObject<PSAccountTransfer.Response>(responseStr);
        //            if (_RequestBodyContent.status == true)
        //            {
        //                return _RequestBodyContent.data;
        //            }
        //            else
        //            {
        //                return null;
        //            }
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        return null;
        //    }
        //}

    }
}
