//==================================================================================
// FileName: PaystackTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;

namespace HCore.Integration.Paystack
{

    public class OPayStackCharge
    {
        public string? status { get; set; }
        public long amount { get; set; }
        public OAuthorization authorization { get; set; }
    }
    public class OChargeData
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public OChargeInformation data { get; set; }

    }
    public class OChargeInformation
    {
        public long id { get; set; }
        public string? domain { get; set; }
        public string? status { get; set; }
        public string? reference { get; set; }
        public long amount { get; set; }
        public string? message { get; set; }
        public string? gateway_response { get; set; }
    }
    public class OPayStackResponse
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public OPayStackResponseData data { get; set; }
    }
    public class OPayStackResponseData
    {
        public long id { get; set; }
        public string? domain { get; set; }
        public string? reference { get; set; }
        public string? status { get; set; }
        public long amount { get; set; }
        public string? message { get; set; }
        public string? gateway_response { get; set; }
        public DateTime? paid_at { get; set; }
        public DateTime? created_at { get; set; }
        public string? channel { get; set; }
        public string? currency { get; set; }
        public string? ip_address { get; set; }
        public double? fees { get; set; }
        public string? order_id { get; set; }
        public OAuthorization authorization { get; set; }
    }
    public class OAuthorization
    {
        public string? authorization_code { get; set; }
        public string? bin { get; set; }
        public string? last4 { get; set; }
        public string? exp_month { get; set; }
        public string? exp_year { get; set; }
        public string? channel { get; set; }
        public string? card_type { get; set; }
        public string? bank { get; set; }
        public string? country_code { get; set; }
        public string? brand { get; set; }
        public bool reusable { get; set; }
        public string? signature { get; set; }
        public string? account_name { get; set; }
        public string? receiver_bank_account_number { get; set; }
        public string? receiver_bank { get; set; }
    }


    public class OPaystackRefund
    {
        public class Response
        {
            public bool status { get; set; }
            public string? message { get; set; }
            public ResponseParameter.Data data { get; set; }
        }
        public class ResponseParameter
        {
            public class Data
            {
                public long integration { get; set; }
                public double deducted_amount { get; set; }
                public string? channel { get; set; }
                public string? merchant_note { get; set; }
                public string? customer_note { get; set; }
                public string? status { get; set; }
                public string? refunded_by { get; set; }
                public DateTime? expected_at { get; set; }
                public string? currency { get; set; }
                public string? domain { get; set; }
                public double amount { get; set; }
                public bool fully_deducted { get; set; }
                public long id { get; set; }
                public DateTime? createdAt { get; set; }
                public DateTime? updatedAt { get; set; }
                public Transaction transaction { get; set; }
            }
            public class Transaction
            {
                public long id { get; set; }
                public string? reference { get; set; }
                public double amount { get; set; }
                public DateTime? paid_at { get; set; }
                public string? channel { get; set; }
                public string? currency { get; set; }
                public Authorization authorization { get; set; }

            }
            public class Authorization
            {
                public string? exp_month { get; set; }
                public string? exp_year { get; set; }
                public string? account_name { get; set; }
            }
        }
    }

    //private static readonly HttpClient client = new HttpClient();
    public static class PaystackTransaction
    {
        public static OPayStackResponseData GetTransaction(string PaymentReference, string? Key)
        {
            try
            {
                string PayStackUrl = "https://api.paystack.co/transaction/verify/";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(PayStackUrl + PaymentReference);
                request.ContentType = "application/json";
                request.Headers["Authorization"] = "Bearer " + Key;
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    string _Response = reader.ReadToEnd();
                    OPayStackResponse _RequestBodyContent = JsonConvert.DeserializeObject<OPayStackResponse>(_Response);
                    if (_RequestBodyContent != null)
                    {
                        if (_RequestBodyContent.status == true)
                        {
                            if (_RequestBodyContent.data != null)
                            {
                                return _RequestBodyContent.data;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                return null;
            }
        }
        //public static OPaystackRefund.Response Refund(string PaymentReference, long Amount, string? Key)
        //{
        //    try
        //    {
        //        using(HttpClient _Client = new HttpClient())
        //        {
        //            var values = new Dictionary<string, string?>
        //                        {
        //                            { "thing1", "hello" },
        //                            { "thing2", "world" }
        //                        };
        //            var content = new FormUrlEncodedContent(values);
        //            var response = _Client.PostAsync("http://www.example.com/recepticle.aspx", content);
        //            var responseString =  response.Content.ReadAsStringAsync();
        //        }
        //        string PayStackUrl = "https://api.paystack.co/transaction/refund/";
        //        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(PayStackUrl + PaymentReference);
        //        request.ContentType = "application/json";
        //        request.Headers["Authorization"] = "Bearer " + Key;
        //        request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
        //        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        //        using (Stream stream = response.GetResponseStream())
        //        using (StreamReader reader = new StreamReader(stream))
        //        {
        //            string _Response = reader.ReadToEnd();
        //            OPaystackRefund.Response _RequestBodyContent = JsonConvert.DeserializeObject<OPaystackRefund.Response>(_Response);
        //            if (_RequestBodyContent != null)
        //            {
        //                return _RequestBodyContent;
        //            }
        //            else
        //            {
        //                return null;
        //            }
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        return null;
        //    }
        //}
        public static OChargeData GetPayStackChargeAuthorization(string Key, string? EmailAddress, double Amount, string? authorization_code, string? PaymentReference)
        {
            try
            {
                string PayStackUrl = "https://api.paystack.co/transaction/charge_authorization";
                RestSharp.RestClient client = new RestSharp.RestClient(PayStackUrl);
                var request = new RestSharp.RestRequest();
                request.AddHeader("Authorization", "Bearer " + Key);
                request.AddParameter("reference", PaymentReference);
                request.AddParameter("authorization_code", authorization_code);
                request.AddParameter("email", EmailAddress);
                request.AddParameter("amount", Amount * 100);
                var response = client.Post(request);
                var content = response.Content; // raw content as string
                OChargeData _RequestBodyContent = JsonConvert.DeserializeObject<OChargeData>(content);
                if (_RequestBodyContent != null)
                {
                    return _RequestBodyContent;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception _Exception)
            {
                return null;
            }
        }
        public static OChargeData GetPayStackRecuringCharge(string Key, string? EmailAddress, double Amount, string? authorization_code)
        {
            try
            {
                string PayStackUrl = "https://api.paystack.co/transaction/charge_authorization";
                RestSharp.RestClient client = new RestSharp.RestClient(PayStackUrl);
                var request = new RestSharp.RestRequest();
                request.AddHeader("Authorization", "Bearer " + Key);
                var jsonbody = new
                {
                    authorization_code = authorization_code,
                    email = EmailAddress,
                    amount = Amount.ToString()
                };

                var body = JsonConvert.SerializeObject(jsonbody);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = client.Post(request);
                var content = response.Content; // raw content as string
                OChargeData _RequestBodyContent = JsonConvert.DeserializeObject<OChargeData>(content);
                if (_RequestBodyContent != null)
                {
                    return _RequestBodyContent;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception _Exception)
            {
                return null;
            }
        }
    }
}
