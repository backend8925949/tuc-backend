﻿using System;
namespace HCore.Integration.Mailerlite.Requests
{
    public class CreateListRequest
    {
        public string name { get; set; }
        public string[] emails { get; set; }
    }

}

