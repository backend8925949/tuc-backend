﻿using System;
using HCore.Helper;

namespace HCore.Integration.Mailerlite.Responses
{
    public class CreateListResponseData
    {
        public int id { get; set; }
        public int account_id { get; set; }
        public int user_id { get; set; }
        public string name { get; set; }
        public string source { get; set; }
        public string type { get; set; }
        public int count { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public CreateListResponseStatus status { get; set; }
        public CreateListResponseStatistics statistics { get; set; }
    }

    public class CreateListResponse
    {
        public CreateListResponseData data { get; set; }
    }

    public class CreateListResponseStatistics
    {
        public int valid { get; set; }
        public int syntax_error { get; set; }
        public int typo { get; set; }
        public int mailbox_not_found { get; set; }
        public int catch_all { get; set; }
        public int mailbox_full { get; set; }
        public int disposable { get; set; }
        public int role { get; set; }
        public int past_delivery_issues { get; set; }
        public int blocked { get; set; }
        public int error { get; set; }
        public int unknown { get; set; }
    }

    public class CreateListResponseStatus
    {
        public string name { get; set; }
        public int count { get; set; }
    }


    
}

