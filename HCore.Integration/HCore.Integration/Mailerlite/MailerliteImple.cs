﻿using System;
using RestSharp;
using HCore.Integration.Mailerlite.Requests;
using static System.Runtime.InteropServices.JavaScript.JSType;
using System.Globalization;
using static HCore.Helper.HCoreConstant;
using HCore.Helper;

namespace HCore.Integration.Mailerlite
{
    public class MailerliteImple
    {

        public RestResponse AddEmailToMailerList(CreateListRequest req)
        {
            RestResponse response = null;
            try
            {
                var client = new RestClient("https://app.mailercheck.com/api");
                var request = new RestRequest("/lists", Method.Post);
                request.AddHeader("Authorization", "Bearer " + MailerliteKey.API_TOKEN);
                string date = DateTime.Now.ToString("dd/MM/yyyy");
                string today = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                request.AddHeader("X-Version", today);
                request.AddHeader("Content-Type", "application/json");
               request.AddJsonBody(req);
                 response = client.Execute(request);
               return response;
            }
            catch(Exception ex)
            {
                return null;
            }
        }


    }
}

