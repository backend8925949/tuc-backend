﻿using System;
using System.Net;
using HCore.Helper;
using HCore.Integration.BillPayment.Request;
using HCore.Integration.BillPayment.Response;
using HCore.Integration.Cellulant.Response;
using HCore.Integration.Cellulant.Response.DirectCardAPIResponse;
using HCore.Integration.Cellulant.Response.DirectCardAPIRequest;
using HCore.Integration.Cellulant.Request.FetchPaymentOptionsRequest;
using HCore.Integration.Cellulant.Request.FetchPaymentOptionsResponse;
using HCore.Integration.Cellulant.Request.FetchFixRateRequest;
using HCore.Integration.Cellulant.Request.FetchFixRateResponse;
using static HCore.Helper.HCoreConstant;
using Newtonsoft.Json;
using System.Text;
using HCore.Integration.Cellulant.Request;
using System.Security.Cryptography;

namespace HCore.Integration.BillPayment
{
    public class BillPayment
    {
        #region Create HTTP web request
        private HttpWebRequest CreateWebRequest(string RequestType, string? url, byte[] payload)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = RequestType;
            //httpWebRequest.Headers["Authorization"] = "Bearer " + _AppConfig.CellulantApiKey;

            if (payload != null)
            {
                httpWebRequest.ContentLength = payload.Length;
                Stream _RequestStream = httpWebRequest.GetRequestStream();
                _RequestStream.Write(payload, 0, payload.Length);
                _RequestStream.Close();
            }

            return httpWebRequest;
        }
        #endregion

        #region Beep.ValidateAccount: Validate account: Used to validate account details before start of the payment
        public ValidateAccountResponse ValidateAccount(ValidateAccountRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantUrl;
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    ValidateAccountResponse? validateaccountresponse = JsonConvert.DeserializeObject<ValidateAccountResponse>(response);
                    return validateaccountresponse;
                }
                else
                {
                    ValidateAccountResponse? response = new ValidateAccountResponse();
                    response.authStatus.authStatusCode = (int)httpweresponse.StatusCode;
                    response.authStatus.authStatusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ValidateAccount", _Exception);
                ValidateAccountResponse? response = new ValidateAccountResponse();
                response.authStatus.authStatusCode = 00;
                response.authStatus.authStatusDescription = _Exception.Message;
                return response;
            }
        }
        #endregion

        #region Beep.QueryFloatBalance: Query Float Balance: Used to query float balance for a particular service within the payment gateway.
        public QueryFloatBalanceResponse QueryFloatBalance(QueryFloatBalanceRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantUrl;
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    QueryFloatBalanceResponse? balanceresponse = JsonConvert.DeserializeObject<QueryFloatBalanceResponse>(response);
                    return balanceresponse;
                }
                else
                {
                    QueryFloatBalanceResponse? response = new QueryFloatBalanceResponse();
                    response.authStatus.authStatusCode = (int)httpweresponse.StatusCode;
                    response.authStatus.authStatusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("QueryFloatBalance", _Exception);
                QueryFloatBalanceResponse? response = new QueryFloatBalanceResponse();
                response.authStatus.authStatusCode = 00;
                response.authStatus.authStatusDescription = _Exception.Message;
                return response;
            }
        }
        #endregion

        #region Beep.PostPayment: Used to request payment/transaction on the beep server for bank payouts, airtime urchasespprchase, and mobile money transferstransfer.

        #region Bill Payments: Used for paying bills of water, electricity, etc.
        public BillPaymentResponse BillPayments(BillPaymentRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantUrl;
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    BillPaymentResponse? billpaymentresponse = JsonConvert.DeserializeObject<BillPaymentResponse>(response);
                    return billpaymentresponse;
                }
                else
                {
                    BillPaymentResponse? response = new BillPaymentResponse();
                    response.authStatus.authStatusCode = (int)httpweresponse.StatusCode;
                    response.authStatus.authStatusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("BillPayment", _Exception);
                BillPaymentResponse? response = new BillPaymentResponse();
                response.authStatus.authStatusCode = 00;
                response.authStatus.authStatusDescription = _Exception.Message;
                return response;
            }
        }
        #endregion

        #region Send Money (Payout) - B2C: Used for money transfer.
        public SendMoneyResponse SendMoneyPayouts(SendMoney request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantUrl;
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    SendMoneyResponse? sendmoneyresponse = JsonConvert.DeserializeObject<SendMoneyResponse>(response);
                    return sendmoneyresponse;
                }
                else
                {
                    SendMoneyResponse? response = new SendMoneyResponse();
                    response.authStatus.authStatusCode = (int)httpweresponse.StatusCode;
                    response.authStatus.authStatusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("BillPayment", _Exception);
                SendMoneyResponse? response = new SendMoneyResponse();
                response.authStatus.authStatusCode = 00;
                response.authStatus.authStatusDescription = _Exception.Message;
                return response;
            }
        }
        #endregion

        #region Bank Payouts: Used for bank payouts.
        public BankPayoutsResponse BankPayouts(BankPayouts request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantUrl;
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    BankPayoutsResponse? banpayoutresponse = JsonConvert.DeserializeObject<BankPayoutsResponse>(response);
                    return banpayoutresponse;
                }
                else
                {
                    BankPayoutsResponse? response = new BankPayoutsResponse();
                    response.authStatus.authStatusCode = (int)httpweresponse.StatusCode;
                    response.authStatus.authStatusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("BillPayment", _Exception);
                BankPayoutsResponse? response = new BankPayoutsResponse();
                response.authStatus.authStatusCode = 00;
                response.authStatus.authStatusDescription = _Exception.Message;
                return response;
            }
        }
        #endregion

        #region Vend Airtime: Used for airtime purchase.
        public AirtimePurchaseresponse AirtimePurchase(AirtimePurchase request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantUrl;
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    AirtimePurchaseresponse? airtimepurchaseresponse = JsonConvert.DeserializeObject<AirtimePurchaseresponse>(response);
                    return airtimepurchaseresponse;
                }
                else
                {
                    AirtimePurchaseresponse? response = new AirtimePurchaseresponse();
                    response.authStatus.authStatusCode = (int)httpweresponse.StatusCode;
                    response.authStatus.authStatusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("BillPayment", _Exception);
                AirtimePurchaseresponse? response = new AirtimePurchaseresponse();
                response.authStatus.authStatusCode = 00;
                response.authStatus.authStatusDescription = _Exception.Message;
                return response;
            }
        }
        #endregion

        #endregion

        #region Beep.QueryBill: This method is used to GET Bill Information i.e. Due Date and amount for present to the customer before payment.

        #region Fetch data plans: Used to fetch Data bundle plans to present the customers for selection before payment
        public FetchDataPlanResponse FetchDataPlans(FetchDataPlanRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantUrl;

                var httpwebrequest = CreateWebRequest("GET", apiurl, null);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    FetchDataPlanResponse? fetchdataplanresponse = JsonConvert.DeserializeObject<FetchDataPlanResponse>(response);
                    return fetchdataplanresponse;
                }
                else
                {
                    FetchDataPlanResponse? response = new FetchDataPlanResponse();
                    response.authStatus.authStatusCode = (int)httpweresponse.StatusCode;
                    response.authStatus.authStatusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("FetchDataPlans", _Exception);
                FetchDataPlanResponse? response = new FetchDataPlanResponse();
                response.authStatus.authStatusCode = 00;
                response.authStatus.authStatusDescription = _Exception.Message;
                return response;
            }
        }
        #endregion

        #region Fetch Bill: Used to fetch the bill Due date and Amount and present it to customer
        public List<FetchBillResponse> FetchBill(FetchBillRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantUrl;
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("GET", apiurl, null);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    List<FetchBillResponse>? fetchbillresponse = JsonConvert.DeserializeObject<List<FetchBillResponse>>(response);
                    return fetchbillresponse;
                }
                else
                {
                    List<FetchBillResponse> response = new List<FetchBillResponse>();
                    response.Add(new FetchBillResponse
                    {
                        statusCode = (int)httpweresponse.StatusCode,
                        statusDescription = httpweresponse.StatusDescription
                    });
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("FetchBill", _Exception);
                List<FetchBillResponse> response = new List<FetchBillResponse>();
                response.Add(new FetchBillResponse
                {
                    statusCode = 00,
                    statusDescription = _Exception.Message
                });
                return response;
            }
        }
        #endregion

        #endregion

        #region Beep.FetchInvoices: Fetch Invoicet: This API function is used to fetch Invoices Information by MSISDN/Mobile Number.
        public FetchInvoicesResponse FetchInvoice(FetchInvoicesRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantUrl;

                var httpwebrequest = CreateWebRequest("GET", apiurl, null);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    FetchInvoicesResponse? fetchinvoicesresponse = JsonConvert.DeserializeObject<FetchInvoicesResponse>(response);
                    return fetchinvoicesresponse;
                }
                else
                {
                    FetchInvoicesResponse? response = new FetchInvoicesResponse();
                    response.authStatus.authStatusCode = (int)httpweresponse.StatusCode;
                    response.authStatus.authStatusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("FetchInvoice", _Exception);
                FetchInvoicesResponse? response = new FetchInvoicesResponse();
                response.authStatus.authStatusCode = 00;
                response.authStatus.authStatusDescription = _Exception.Message;
                return response;
            }
        }
        #endregion

        #region Beep.FetchInvoiceByAccount: Fetch Invoice By Account: This API function is used to fetch Invoice Information by account number.
        public FetchInvoiceByAccountResponse FetchInvoiceByAccount(FetchInvoiceByAccountRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantUrl;

                var httpwebrequest = CreateWebRequest("GET", apiurl, null);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    FetchInvoiceByAccountResponse? fetchinvoicebyaccountresponse = JsonConvert.DeserializeObject<FetchInvoiceByAccountResponse>(response);
                    return fetchinvoicebyaccountresponse;
                }
                else
                {
                    FetchInvoiceByAccountResponse? response = new FetchInvoiceByAccountResponse();
                    response.authStatus.authStatusCode = (int)httpweresponse.StatusCode;
                    response.authStatus.authStatusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("FetchInvoiceByAccount", _Exception);
                FetchInvoiceByAccountResponse? response = new FetchInvoiceByAccountResponse();
                response.authStatus.authStatusCode = 00;
                response.authStatus.authStatusDescription = _Exception.Message;
                return response;
            }
        }
        #endregion


    }
    public class CheckOut
    {
        #region Create HTTP web request
        private HttpWebRequest CreateWebRequest(string RequestType, string? url, byte[] payload)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = RequestType;
            //httpWebRequest.Headers["Authorization"] = "Bearer " + _AppConfig.CellulantApiKey;

            if (payload != null)
            {
                httpWebRequest.ContentLength = payload.Length;
                Stream _RequestStream = httpWebRequest.GetRequestStream();
                _RequestStream.Write(payload, 0, payload.Length);
                _RequestStream.Close();
            }

            return httpWebRequest;
        }
        #endregion

        #region Checkout
        public AuthenticationResponse Authentication(AuthenticationRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantCustomUrl + "oauth/token";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    AuthenticationResponse? _Response = JsonConvert.DeserializeObject<AuthenticationResponse>(response);
                    return _Response;
                }
                else
                {
                    AuthenticationResponse? response = new AuthenticationResponse();
                    response.error = httpweresponse.StatusCode.ToString();
                    response.message = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Authentication", _Exception);
                AuthenticationResponse? response = new AuthenticationResponse();
                response.error = "00";
                response.message = _Exception.Message;
                return response;
            }
        }

        public CheckoutResponse CheckoutRequest(CheckoutRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantCustomUrl + "requests/initiate";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    CheckoutResponse? _Response = JsonConvert.DeserializeObject<CheckoutResponse>(response);
                    return _Response;
                }
                else
                {
                    CheckoutResponse? response = new CheckoutResponse();
                    response.status.statusCode = (int)httpweresponse.StatusCode;
                    response.status.statusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CheckoutRequest", _Exception);
                CheckoutResponse? response = new CheckoutResponse();
                response.status.statusCode = 00;
                response.status.statusDescription = _Exception.Message;
                return response;
            }
        }

        public ChargeResponse ChargeRequest(ChargeRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantCustomUrl + "requests/charge";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    ChargeResponse? _Response = JsonConvert.DeserializeObject<ChargeResponse>(response);
                    return _Response;
                }
                else
                {
                    ChargeResponse? response = new ChargeResponse();
                    response.status.statusCode = (int)httpweresponse.StatusCode;
                    response.status.statusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ChargeRequest", _Exception);
                ChargeResponse? response = new ChargeResponse();
                response.status.statusCode = 00;
                response.status.statusDescription = _Exception.Message;
                return response;
            }
        }

        public ValidateChargeResponse ValidateChargeRequest(ValidateChargeRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantCustomUrl + "requests/validate-charge";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    ValidateChargeResponse? _Response = JsonConvert.DeserializeObject<ValidateChargeResponse>(response);
                    return _Response;
                }
                else
                {
                    ValidateChargeResponse? response = new ValidateChargeResponse();
                    response.status.statusCode = (int)httpweresponse.StatusCode;
                    response.status.statusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ValidateChargeRequest", _Exception);
                ValidateChargeResponse? response = new ValidateChargeResponse();
                response.status.statusCode = 00;
                response.status.statusDescription = _Exception.Message;
                return response;
            }
        }

        public QueryStatusResponse QueryRequestStatus(QueryStatusRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantCustomUrl + "requests/query-status";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    QueryStatusResponse? _Response = JsonConvert.DeserializeObject<QueryStatusResponse>(response);
                    return _Response;
                }
                else
                {
                    QueryStatusResponse? response = new QueryStatusResponse();
                    response.status.statusCode = (int)httpweresponse.StatusCode;
                    response.status.statusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("QueryRequestStatus", _Exception);
                QueryStatusResponse? response = new QueryStatusResponse();
                response.status.statusCode = 00;
                response.status.statusDescription = _Exception.Message;
                return response;
            }
        }

        public CapturePaymemntsResponse CapturePayments(CapturePaymemntsRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantCustomUrl + "requests/acknowledge";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    CapturePaymemntsResponse? _Response = JsonConvert.DeserializeObject<CapturePaymemntsResponse>(response);
                    return _Response;
                }
                else
                {
                    CapturePaymemntsResponse? response = new CapturePaymemntsResponse();
                    response.status.statusCode = (int)httpweresponse.StatusCode;
                    response.status.statusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CapturePayments", _Exception);
                CapturePaymemntsResponse? response = new CapturePaymemntsResponse();
                response.status.statusCode = 00;
                response.status.statusDescription = _Exception.Message;
                return response;
            }
        }

        public InitiateRefundResponse InitiateRefund(InitiateRefundRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantCustomUrl + "requests/initiate-refund";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    InitiateRefundResponse? _Response = JsonConvert.DeserializeObject<InitiateRefundResponse>(response);
                    return _Response;
                }
                else
                {
                    InitiateRefundResponse? response = new InitiateRefundResponse();
                    response.status.statusCode = (int)httpweresponse.StatusCode;
                    response.status.statusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("InitiateRefund", _Exception);
                InitiateRefundResponse? response = new InitiateRefundResponse();
                response.status.statusCode = 00;
                response.status.statusDescription = _Exception.Message;
                return response;
            }
        }

        public AuthorizationWith3DSResponse AuthorizationWith3DS(AuthorizationWith3DSRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantCustomUrl + "requests/direct-card-charge";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    AuthorizationWith3DSResponse? _Response = JsonConvert.DeserializeObject<AuthorizationWith3DSResponse>(response);
                    return _Response;
                }
                else
                {
                    AuthorizationWith3DSResponse? response = new AuthorizationWith3DSResponse();
                    response.statusCode = (int)httpweresponse.StatusCode;
                    response.statusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("AuthorizationWith3DS", _Exception);
                AuthorizationWith3DSResponse? response = new AuthorizationWith3DSResponse();
                response.statusCode = 00;
                response.statusDescription = _Exception.Message;
                return response;
            }
        }

        public AuthorizationWith3DSCVVLessResponse AuthorizationWith3DSCVVLess(AuthorizationWith3DSCVVLessRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantCustomUrl + "requests/direct-card-charge";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    AuthorizationWith3DSCVVLessResponse? _Response = JsonConvert.DeserializeObject<AuthorizationWith3DSCVVLessResponse>(response);
                    return _Response;
                }
                else
                {
                    AuthorizationWith3DSCVVLessResponse? response = new AuthorizationWith3DSCVVLessResponse();
                    response.statusCode = (int)httpweresponse.StatusCode;
                    response.statusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("AuthorizationWith3DSCVVLess", _Exception);
                AuthorizationWith3DSCVVLessResponse? response = new AuthorizationWith3DSCVVLessResponse();
                response.statusCode = 00;
                response.statusDescription = _Exception.Message;
                return response;
            }
        }

        public AuthorizationWithout3DSResponse AuthorizationWithout3DS(AuthorizationWithout3DSRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantCustomUrl + "requests/direct-card-charge";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    AuthorizationWithout3DSResponse? _Response = JsonConvert.DeserializeObject<AuthorizationWithout3DSResponse>(response);
                    return _Response;
                }
                else
                {
                    AuthorizationWithout3DSResponse? response = new AuthorizationWithout3DSResponse();
                    response.statusCode = (int)httpweresponse.StatusCode;
                    response.statusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("AuthorizationWithout3DS", _Exception);
                AuthorizationWithout3DSResponse? response = new AuthorizationWithout3DSResponse();
                response.statusCode = 00;
                response.statusDescription = _Exception.Message;
                return response;
            }
        }

        public AuthorizationWithout3DSCVVLessResponse AuthorizationWithout3DSCVVLess(AuthorizationWithout3DSCVVLessRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantCustomUrl + "requests/direct-card-charge";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    AuthorizationWithout3DSCVVLessResponse? _Response = JsonConvert.DeserializeObject<AuthorizationWithout3DSCVVLessResponse>(response);
                    return _Response;
                }
                else
                {
                    AuthorizationWithout3DSCVVLessResponse? response = new AuthorizationWithout3DSCVVLessResponse();
                    response.statusCode = (int)httpweresponse.StatusCode;
                    response.statusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("AuthorizationWithout3DSCVVLess", _Exception);
                AuthorizationWithout3DSCVVLessResponse? response = new AuthorizationWithout3DSCVVLessResponse();
                response.statusCode = 00;
                response.statusDescription = _Exception.Message;
                return response;
            }
        }

        public OTPValidationResponse.InitiateOTPResponse InitiateOTP(OTPValidationRequest.InitiateOtp request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantCustomUrl + "requests/initiateOTP";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    OTPValidationResponse.InitiateOTPResponse? _Response = JsonConvert.DeserializeObject<OTPValidationResponse.InitiateOTPResponse>(response);
                    return _Response;
                }
                else
                {
                    OTPValidationResponse.InitiateOTPResponse? response = new OTPValidationResponse.InitiateOTPResponse();
                    response.status.statusCode = (int)httpweresponse.StatusCode;
                    response.status.statusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("InitiateOTP", _Exception);
                OTPValidationResponse.InitiateOTPResponse? response = new OTPValidationResponse.InitiateOTPResponse();
                response.status.statusCode = 00;
                response.status.statusDescription = _Exception.Message;
                return response;
            }
        }

        public OTPValidationResponse.ValidateOTPResponse ValidateOTP(OTPValidationRequest.ValidateOTP request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantCustomUrl + "requests/validateOTP";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    OTPValidationResponse.ValidateOTPResponse? _Response = JsonConvert.DeserializeObject<OTPValidationResponse.ValidateOTPResponse>(response);
                    return _Response;
                }
                else
                {
                    OTPValidationResponse.ValidateOTPResponse? response = new OTPValidationResponse.ValidateOTPResponse();
                    response.status.statusCode = (int)httpweresponse.StatusCode;
                    response.status.statusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ValidateOTP", _Exception);
                OTPValidationResponse.ValidateOTPResponse? response = new OTPValidationResponse.ValidateOTPResponse();
                response.status.statusCode = 00;
                response.status.statusDescription = _Exception.Message;
                return response;
            }
        }

        public CancelRequestResponse CancelRequests(CancelRequestRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantCustomUrl + "requests/cancel";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    CancelRequestResponse? _Response = JsonConvert.DeserializeObject<CancelRequestResponse>(response);
                    return _Response;
                }
                else
                {
                    CancelRequestResponse? response = new CancelRequestResponse();
                    response.status.statusCode = (int)httpweresponse.StatusCode;
                    response.status.statusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CancelRequests", _Exception);
                CancelRequestResponse? response = new CancelRequestResponse();
                response.status.statusCode = 00;
                response.status.statusDescription = _Exception.Message;
                return response;
            }
        }

        public ConversionRateResponse FetchConversationRate(ConversionRateRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantCustomUrl + "requests/acknowledgement-rate";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    ConversionRateResponse? _Response = JsonConvert.DeserializeObject<ConversionRateResponse>(response);
                    return _Response;
                }
                else
                {
                    ConversionRateResponse? response = new ConversionRateResponse();
                    response.status.statusCode = (int)httpweresponse.StatusCode;
                    response.status.statusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("FetchConversationRate", _Exception);
                ConversionRateResponse? response = new ConversionRateResponse();
                response.status.statusCode = 00;
                response.status.statusDescription = _Exception.Message;
                return response;
            }
        }

        public PaymentOptionsResponse RequestPaymentOptions(PaymentOptionsRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantCustomUrl + "requests/options?checkoutRequestID=" + request.checkoutRequestID + "&merchantTransactionID=" + request.merchantTransactionID;

                var httpwebrequest = CreateWebRequest("GET", apiurl, null);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    PaymentOptionsResponse? fetchinvoicebyaccountresponse = JsonConvert.DeserializeObject<PaymentOptionsResponse>(response);
                    return fetchinvoicebyaccountresponse;
                }
                else
                {
                    PaymentOptionsResponse? response = new PaymentOptionsResponse();
                    response.status.statusCode = (int)httpweresponse.StatusCode;
                    response.status.statusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("RequestPaymentOptions", _Exception);
                PaymentOptionsResponse? response = new PaymentOptionsResponse();
                response.status.statusCode = 00;
                response.status.statusDescription = _Exception.Message;
                return response;
            }
        }

        public ServicePaymentOptionsResponse RequestServicePaymentOptions(ServicePaymentOptionsRequest request)
        {
            try
            {
                string apiurl = _AppConfig.CellulantCustomUrl + "requests/service-options?serviceCode=" + request.serviceCode + "&countryCode=" + request.countryCode;

                var httpwebrequest = CreateWebRequest("GET", apiurl, null);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    ServicePaymentOptionsResponse? fetchinvoicebyaccountresponse = JsonConvert.DeserializeObject<ServicePaymentOptionsResponse>(response);
                    return fetchinvoicebyaccountresponse;
                }
                else
                {
                    ServicePaymentOptionsResponse? response = new ServicePaymentOptionsResponse();
                    response.status.statusCode = (int)httpweresponse.StatusCode;
                    response.status.statusDescription = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("RequestServicePaymentOptions", _Exception);
                ServicePaymentOptionsResponse? response = new ServicePaymentOptionsResponse();
                response.status.statusCode = 00;
                response.status.statusDescription = _Exception.Message;
                return response;
            }
        }


        public string? EncryptCheckoutPayload(EncryptPayload request)
        {
            try
            {
                CellulantEncryption encryption = new CellulantEncryption();
                string json = JsonConvert.SerializeObject(request.payload).Replace("/", "\\/");
                Console.WriteLine(json);
                return encryption.Encrypt(json, request.ivKey, request.secretKey);
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CheckoutRequest", _Exception);
                CheckoutResponse? response = new CheckoutResponse();
                response.status.statusCode = 00;
                response.status.statusDescription = _Exception.Message;
                return null;
            }
        }
        #endregion
    }



    internal class CellulantEncryption
    {

        //public string? ivKey;
        //public string? secretKey;

        //public Encryption(string ivKey, string? secretKey)
        //{
        //    this.ivKey = ivKey;
        //    this.secretKey = secretKey;
        //}

        private string HashString(string str)
        {
            var stringBuilder = new StringBuilder();
            using (var hash = SHA256.Create())
            {
                var result = hash.ComputeHash(Encoding.UTF8.GetBytes(str));
                foreach (var x in result)
                {
                    stringBuilder.Append(x.ToString("x2"));
                }
            }

            return stringBuilder.ToString();
        }

        public string? Encrypt(string payload, string? ivKey, string? secretKey)
        {

            byte[] encrypted;

            using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
            {
                aes.KeySize = 256;
                aes.BlockSize = 128;
                aes.Key = Encoding.UTF8.GetBytes(HashString(ivKey).Substring(0, 16));
                aes.IV = Encoding.UTF8.GetBytes(HashString(secretKey).Substring(0, 16));
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;

                ICryptoTransform enc = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, enc, CryptoStreamMode.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(cs))
                        {
                            sw.Write(payload);
                        }

                        encrypted = ms.ToArray();
                    }
                }
            }
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(Convert.ToBase64String(encrypted)));
        }
    }


}

