﻿using System;
namespace HCore.Integration.BillPayment.Request
{
    public class FetchBillRequest
    {
        public string? countryCode { get; set; }
        public string? function { get; set; }
        public FetchBillPayload payload { get; set; }
    }

    public class FetchBillPacket
    {
        public string? serviceCode { get; set; }
        public string? MSISDN { get; set; }
        public string? accountNumber { get; set; }
        public string? payerTransactionID { get; set; }
        public string? extraData { get; set; }
    }

    public class FetchBillPayload
    {
        public Credentials credentials { get; set; }
        public List<FetchBillPacket> packet { get; set; }
    }
}

