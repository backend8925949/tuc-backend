﻿using System;
namespace HCore.Integration.BillPayment.Request
{
    public class SendMoney
    {
        public string? countryCode { get; set; }
        public string? function { get; set; }
        public SendMoneyPayload payload { get; set; }
    }

    public class SendMoneyExtraData
    {
        public string? callbackUrl { get; set; }
    }

    public class SendMoneyPacket
    {
        public string? serviceCode { get; set; }
        public string? MSISDN { get; set; }
        public string? invoiceNumber { get; set; }
        public string? accountNumber { get; set; }
        public string? payerTransactionID { get; set; }
        public int amount { get; set; }
        public string? hubID { get; set; }
        public string? narration { get; set; }
        public string? datePaymentReceived { get; set; }
        public SendMoneyExtraData extraData { get; set; }
        public string? currencyCode { get; set; }
        public string? customerNames { get; set; }
        public string? paymentMode { get; set; }
    }

    public class SendMoneyPayload
    {
        public Credentials credentials { get; set; }
        public List<SendMoneyPacket> packet { get; set; }
    }
}

