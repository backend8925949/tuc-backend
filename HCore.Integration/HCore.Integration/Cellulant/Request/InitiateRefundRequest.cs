﻿using System;
namespace HCore.Integration.Cellulant.Request
{
    public class InitiateRefundRequest
    {
        public class FullRefund
        {
            public string? merchantTransactionID { get; set; }
            public string? checkoutRequestID { get; set; }
            public string? refundType { get; set; }
            public int refundAmount { get; set; }
            public string? currencyCode { get; set; }
            public string? narration { get; set; }
            public string? extraDetails { get; set; }
        }

        public class PartialRefund
        {
            public string? merchantTransactionID { get; set; }
            public string? checkoutRequestID { get; set; }
            public string? refundType { get; set; }
            public int refundAmount { get; set; }
            public string? currencyCode { get; set; }
            public string? narration { get; set; }
            public string? extraDetails { get; set; }
        }
    }
}

