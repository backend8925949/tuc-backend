﻿using System;
namespace HCore.Integration.BillPayment.Request
{
    public class QueryFloatBalanceRequest
    {
        public string? countryCode { get; set; }
        public string? function { get; set; }
        public QueryFloatBalancePayload payload { get; set; }
    }

    public class QueryFloatBalancePacket
    {
        public string? serviceCode { get; set; }
        public string? narration { get; set; }
    }

    public class QueryFloatBalancePayload
    {
        public Credentials credentials { get; set; }
        public List<QueryFloatBalancePacket> packet { get; set; }
    }
}

