﻿using System;
namespace HCore.Integration.BillPayment.Request
{
    public class FetchDataPlanRequest
    {
        public string? countryCode { get; set; }
        public string? function { get; set; }
        public FetchDataPlanPayload payload { get; set; }
    }

    public class FetchDataPlanPacket
    {
        public string? serviceCode { get; set; }
        public string? MSISDN { get; set; }
        public string? accountNumber { get; set; }
        public string? payerTransactionID { get; set; }
        public string? extraData { get; set; }
    }

    public class FetchDataPlanPayload
    {
        public Credentials credentials { get; set; }
        public List<FetchDataPlanPacket> packet { get; set; }
    }
}

