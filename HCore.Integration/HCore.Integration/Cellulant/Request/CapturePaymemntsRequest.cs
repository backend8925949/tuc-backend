﻿using System;
namespace HCore.Integration.Cellulant.Request
{
    public class CapturePaymemntsRequest
    {
        public string? merchantTransactionID { get; set; }
        public int checkoutRequestID { get; set; }
        public string? receiptNumber { get; set; }
        public int statusCode { get; set; }
        public string? statusDescription { get; set; }
    }
}

