﻿using System;
namespace HCore.Integration.Cellulant.Request
{
    public class ValidateChargeRequest
    {
        public string? merchantTransactionID { get; set; }
        public double checkoutRequestID { get; set; }
        public int chargeRequestID { get; set; }
        public string? validationToken { get; set; }
    }
}

