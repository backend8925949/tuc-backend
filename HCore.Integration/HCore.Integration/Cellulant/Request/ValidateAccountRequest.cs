﻿using System;
namespace HCore.Integration.BillPayment.Request
{
    public class ValidateAccountRequest
    {
        public string? function { get; set; }
        public string? countryCode { get; set; }
        public ValidateAccountPayload payload { get; set; }
    }

    public class ValidateAccountPacket
    {
        public string? serviceCode { get; set; }
        public string? currencyCode { get; set; }
        public string? countryCode { get; set; }
        public string? accountNumber { get; set; }
        public string? extraData { get; set; }
    }

    public class ValidateAccountPayload
    {
        public Credentials credentials { get; set; }
        public List<ValidateAccountPacket> packet { get; set; }
    }
}

