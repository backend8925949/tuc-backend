﻿using System;
namespace HCore.Integration.BillPayment.Request
{
    public class FetchInvoicesRequest
    {
        public string? function { get; set; }
        public string? countryCode { get; set; }
        public FetchInvoicesPayload payload { get; set; }
    }

    public class FetchInvoicesPacket
    {
        public string? MSISDN { get; set; }
        public int serviceID { get; set; }
    }

    public class FetchInvoicesPayload
    {
        public Credentials credentials { get; set; }
        public List<FetchInvoicesPacket> packet { get; set; }
    }
}

