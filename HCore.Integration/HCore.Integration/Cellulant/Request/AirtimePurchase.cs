﻿using System;
namespace HCore.Integration.BillPayment.Request
{
    public class AirtimePurchase
    {
        public string? countryCode { get; set; }
        public string? function { get; set; }
        public Payload payload { get; set; }
    }
    public class AirtimePurchaseExtraData
    {
        public string? callbackUrl { get; set; }
    }

    public class AirtimePurchasePacket
    {
        public string? serviceCode { get; set; }
        public string? MSISDN { get; set; }
        public string? invoiceNumber { get; set; }
        public string? accountNumber { get; set; }
        public string? payerTransactionID { get; set; }
        public int amount { get; set; }
        public string? hubID { get; set; }
        public string? narration { get; set; }
        public string? datePaymentReceived { get; set; }
        public AirtimePurchaseExtraData extraData { get; set; }
        public string? currencyCode { get; set; }
        public string? customerNames { get; set; }
        public string? paymentMode { get; set; }
    }

    public class AirtimePurchasePayload
    {
        public Credentials credentials { get; set; }
        public List<AirtimePurchasePacket> packet { get; set; }
    }
}

