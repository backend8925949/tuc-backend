﻿using System;
namespace HCore.Integration.BillPayment.Request
{
    public class BankPayouts
    {
        public string? countryCode { get; set; }
        public string? function { get; set; }
        public Payload payload { get; set; }
    }
    public class BankPayoutsExtraData
    {
        public string? callbackUrl { get; set; }
        public string? destinationBankCode { get; set; }
        public string? destinationAccountName { get; set; }
        public string? destinationAccountNo { get; set; }
        public string? destinationBank { get; set; }
    }

    public class BankPayoutsPacket
    {
        public string? serviceCode { get; set; }
        public string? MSISDN { get; set; }
        public string? accountNumber { get; set; }
        public string? payerTransactionID { get; set; }
        public int amount { get; set; }
        public string? narration { get; set; }
        public string? datePaymentReceived { get; set; }
        public BankPayoutsExtraData extraData { get; set; }
        public string? paymentMode { get; set; }
        public string? currencyCode { get; set; }
        public string? customerNames { get; set; }
    }

    public class Payload
    {
        public Credentials credentials { get; set; }
        public List<BankPayoutsPacket> packet { get; set; }
    }
}

