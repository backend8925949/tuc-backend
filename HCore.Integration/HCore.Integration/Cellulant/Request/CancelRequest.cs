﻿using System;
namespace HCore.Integration.Cellulant.Request
{
    public class CancelRequestRequest
    {
        public string? merchantTransactionID { get; set; }
        public string? serviceCode { get; set; }
        public int checkoutRequestID { get; set; }
    }
}

