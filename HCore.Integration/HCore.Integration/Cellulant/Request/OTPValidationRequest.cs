﻿using System;
namespace HCore.Integration.Cellulant.Request
{
    public class OTPValidationRequest
    {
        public class InitiateOtp
        {
            public string? MSISDN { get; set; }
            public string? checkoutRequestID { get; set; }
        }

        public class ValidateOTP
        {
            public string? MSISDN { get; set; }
            public string? checkoutRequestID { get; set; }
            public string? token { get; set; }
        }
    }
}

