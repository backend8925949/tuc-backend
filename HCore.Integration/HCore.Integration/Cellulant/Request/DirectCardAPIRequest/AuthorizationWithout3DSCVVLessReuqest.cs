﻿using System;
using static HCore.Integration.Cellulant.Response.DirectCardAPIRequest.AuthorizationWithout3DSCVVLessRequest;

namespace HCore.Integration.Cellulant.Response.DirectCardAPIRequest
{
    public class AuthorizationWithout3DSCVVLessRequest
    {
        public string? merchantTransactionID { get; set; }
        public AuthorizationWithout3DSCVVLessOrder order { get; set; }
        public AuthorizationWithout3DSCVVLessSourceOfFunds sourceOfFunds { get; set; }
        public AuthorizationWithout3DSCVVLessBillingDetails billingDetails { get; set; }
    }

    public class AuthorizationWithout3DSCVVLessSourceOfFunds
    {
        public AuthorizationWithout3DSCVVLessCard card { get; set; }
    }

    public class AuthorizationWithout3DSCVVLessAddress
    {
        public string? city { get; set; }
        public string? countryCode { get; set; }
    }

    public class AuthorizationWithout3DSCVVLessBillingDetails
    {
        public AuthorizationWithout3DSCVVLessAddress address { get; set; }
        public AuthorizationWithout3DSCVVLessCustomer customer { get; set; }
    }

    public class AuthorizationWithout3DSCVVLessCard
    {
        public string? nameOnCard { get; set; }
        public string? number { get; set; }
        public string? cvv { get; set; }
        public AuthorizationWithout3DSCVVLessExpiry expiry { get; set; }
    }

    public class AuthorizationWithout3DSCVVLessCustomer
    {
        public string? firstName { get; set; }
        public string? emailAddress { get; set; }
        public string? surname { get; set; }
        public string? mobileNumber { get; set; }
    }

    public class AuthorizationWithout3DSCVVLessExpiry
    {
        public string? month { get; set; }
        public string? year { get; set; }
    }

    public class AuthorizationWithout3DSCVVLessOrder
    {
        public string? accountNumber { get; set; }
        public string? chargeAmount { get; set; }
        public string? currencyCode { get; set; }
        public string? requestDescription { get; set; }
    }
}

