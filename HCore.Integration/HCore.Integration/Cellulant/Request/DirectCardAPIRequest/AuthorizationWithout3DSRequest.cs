﻿using System;
namespace HCore.Integration.Cellulant.Response.DirectCardAPIRequest
{
    public class AuthorizationWithout3DSRequest
    {
        public string? merchantTransactionID { get; set; }
        public Order order { get; set; }
        public SourceOfFunds sourceOfFunds { get; set; }
        public BillingDetails billingDetails { get; set; }
    }

    public class Address
    {
        public string? city { get; set; }
        public string? countryCode { get; set; }
    }

    public class BillingDetails
    {
        public Address address { get; set; }
        public Customer customer { get; set; }
    }

    public class Card
    {
        public string? nameOnCard { get; set; }
        public string? number { get; set; }
        public string? cvv { get; set; }
        public Expiry expiry { get; set; }
    }

    public class Customer
    {
        public string? firstName { get; set; }
        public string? emailAddress { get; set; }
        public string? surname { get; set; }
        public string? mobileNumber { get; set; }
    }

    public class Expiry
    {
        public string? month { get; set; }
        public string? year { get; set; }
    }

    public class Order
    {
        public string? accountNumber { get; set; }
        public string? chargeAmount { get; set; }
        public string? currencyCode { get; set; }
        public string? requestDescription { get; set; }
    }

    public class SourceOfFunds
    {
        public Card card { get; set; }
    }
}

