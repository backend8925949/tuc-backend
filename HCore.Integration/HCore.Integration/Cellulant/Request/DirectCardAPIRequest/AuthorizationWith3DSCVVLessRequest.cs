﻿using System;
namespace HCore.Integration.Cellulant.Response.DirectCardAPIRequest
{
    public class AuthorizationWith3DSCVVLessRequest
    {
        public string? merchantTransactionID { get; set; }
        public AuthorizationWith3DSCVVLessOrder order { get; set; }
        public AuthorizationWith3DSCVVLessSourceOfFunds sourceOfFunds { get; set; }
        public AuthorizationWith3DSCVVLessBillingDetails billingDetails { get; set; }
        public AuthorizationWith3DSCVVLessBrowserDetails browserDetails { get; set; }
    }

    public class AuthorizationWith3DSCVVLessAddress
    {
        public string? city { get; set; }
        public string? countryCode { get; set; }
    }

    public class AuthorizationWith3DSCVVLessBillingDetails
    {
        public AuthorizationWith3DSCVVLessAddress address { get; set; }
        public AuthorizationWith3DSCVVLessCustomer customer { get; set; }
    }

    public class AuthorizationWith3DSCVVLessBrowserDetails
    {
        public string? acceptHeader { get; set; }
        public string? screenColorDepth { get; set; }
        public string? language { get; set; }
        public string? screenHeight { get; set; }
        public string? screenWidth { get; set; }
        public string? timezone { get; set; }
        public string? javaEnabled { get; set; }
        public string? javascriptEnabled { get; set; }
        public string? ipAddress { get; set; }
        public string? userAgent { get; set; }
    }

    public class AuthorizationWith3DSCVVLessCard
    {
        public string? nameOnCard { get; set; }
        public string? number { get; set; }
        public string? cvv { get; set; }
        public AuthorizationWith3DSCVVLessExpiry expiry { get; set; }
    }

    public class AuthorizationWith3DSCVVLessCustomer
    {
        public string? firstName { get; set; }
        public string? emailAddress { get; set; }
        public string? surname { get; set; }
        public string? mobileNumber { get; set; }
    }

    public class AuthorizationWith3DSCVVLessExpiry
    {
        public string? month { get; set; }
        public string? year { get; set; }
    }

    public class AuthorizationWith3DSCVVLessOrder
    {
        public string? accountNumber { get; set; }
        public string? chargeAmount { get; set; }
        public string? currencyCode { get; set; }
        public string? requestDescription { get; set; }
    }

    public class AuthorizationWith3DSCVVLessSourceOfFunds
    {
        public AuthorizationWith3DSCVVLessCard card { get; set; }
    }
}

