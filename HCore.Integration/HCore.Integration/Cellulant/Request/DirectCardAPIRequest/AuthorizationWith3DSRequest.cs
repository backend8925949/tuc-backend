﻿using System;
namespace HCore.Integration.Cellulant.Response.DirectCardAPIRequest
{
    public class AuthorizationWith3DSRequest
    {
        public string? merchantTransactionID { get; set; }
        public AuthorizationWith3DSOrder order { get; set; }
        public AuthorizationWith3DSSourceOfFunds sourceOfFunds { get; set; }
        public AuthorizationWith3DSBillingDetails billingDetails { get; set; }
        public AuthorizationWith3DSBrowserDetails browserDetails { get; set; }
    }

    public class AuthorizationWith3DSAddress
    {
        public string? city { get; set; }
        public string? countryCode { get; set; }
    }

    public class AuthorizationWith3DSBillingDetails
    {
        public Address address { get; set; }
        public Customer customer { get; set; }
    }

    public class AuthorizationWith3DSBrowserDetails
    {
        public string? acceptHeader { get; set; }
        public string? screenColorDepth { get; set; }
        public string? language { get; set; }
        public string? screenHeight { get; set; }
        public string? screenWidth { get; set; }
        public string? timezone { get; set; }
        public string? javaEnabled { get; set; }
        public string? javascriptEnabled { get; set; }
        public string? ipAddress { get; set; }
        public string? userAgent { get; set; }
    }

    public class AuthorizationWith3DSCard
    {
        public string? nameOnCard { get; set; }
        public string? number { get; set; }
        public string? cvv { get; set; }
        public AuthorizationWith3DSExpiry expiry { get; set; }
    }

    public class AuthorizationWith3DSCustomer
    {
        public string? firstName { get; set; }
        public string? emailAddress { get; set; }
        public string? surname { get; set; }
        public string? mobileNumber { get; set; }
    }

    public class AuthorizationWith3DSExpiry
    {
        public string? month { get; set; }
        public string? year { get; set; }
    }

    public class AuthorizationWith3DSOrder
    {
        public string? accountNumber { get; set; }
        public string? chargeAmount { get; set; }
        public string? currencyCode { get; set; }
        public string? requestDescription { get; set; }
    }

    public class AuthorizationWith3DSSourceOfFunds
    {
        public AuthorizationWith3DSCard card { get; set; }
    }
}

