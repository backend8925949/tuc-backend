﻿using System;
namespace HCore.Integration.Cellulant.Request
{
    public class QueryStatusRequest
    {
        public string? merchantTransactionID { get; set; }
        public string? serviceCode { get; set; }
        public string? checkoutRequestID { get; set; }
    }
}

