﻿using System;
namespace HCore.Integration.BillPayment.Request
{
    public class FetchInvoiceByAccountRequest
    {
        public string? function { get; set; }
        public string? countryCode { get; set; }
        public FetchInvoiceByAccountPayload payload { get; set; }
    }

    public class FetchInvoiceByAccountPacket
    {
        public int serviceID { get; set; }
        public string? accountNumber { get; set; }
    }

    public class FetchInvoiceByAccountPayload
    {
        public Credentials credentials { get; set; }
        public List<FetchInvoiceByAccountPacket> packet { get; set; }
    }
}

