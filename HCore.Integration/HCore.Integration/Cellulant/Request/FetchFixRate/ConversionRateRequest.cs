﻿using System;
namespace HCore.Integration.Cellulant.Request.FetchFixRateRequest
{
    public class ConversionRateRequest
    {
        public string? baseCurrencyCode { get; set; }
        public string? exchangeCurrencyCode { get; set; }
        public string? serviceCode { get; set; }
    }
}

