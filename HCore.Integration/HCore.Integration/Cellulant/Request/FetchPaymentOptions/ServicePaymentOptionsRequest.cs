﻿using System;
namespace HCore.Integration.Cellulant.Request.FetchPaymentOptionsRequest
{
    public class ServicePaymentOptionsRequest
    {
        public string? serviceCode { get; set; }
        public string? countryCode { get; set; }
    }
}

