﻿using System;
namespace HCore.Integration.Cellulant.Request.FetchPaymentOptionsRequest
{
    public class PaymentOptionsRequest
    {
        public int? checkoutRequestID { get; set; }
        public string? merchantTransactionID { get; set; }
    }
}

