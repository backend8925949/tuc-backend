﻿using System;
namespace HCore.Integration.Cellulant.Response
{
    public class AuthenticationRequest
    {
        public string? grant_type { get; set; }
        public string? client_id { get; set; }
        public string? client_secret { get; set; }
    }
}

