﻿using System;
namespace HCore.Integration.Cellulant.Objects
{
    public class ResponseStatus
    {
        public int statusCode { get; set; }
        public string? statusDescription { get; set; }
    }
}

