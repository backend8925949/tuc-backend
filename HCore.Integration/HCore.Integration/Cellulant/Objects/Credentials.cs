﻿using System;
namespace HCore.Integration.BillPayment.Request
{
    public class Credentials
    {
        public string? password { get; set; }
        public string? username { get; set; }
    }
}

