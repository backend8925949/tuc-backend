﻿using System;
namespace HCore.Integration.BillPayment.Objects
{
    public class AuthStatus
    {
        public int authStatusCode { get; set; }
        public string? authStatusDescription { get; set; }
    }
}

