﻿using System;
using HCore.Integration.BillPayment.Objects;

namespace HCore.Integration.BillPayment.Request
{
    public class QueryFloatBalanceResponse
    {
        public AuthStatus authStatus { get; set; }
        public QueryFloatBalanceResults results { get; set; }
    }

    public class QueryFloatBalanceResults
    {
        public int statusCode { get; set; }
        public string? statusDescription { get; set; }
        public int balance { get; set; }
        public string? floatAccountName { get; set; }
        public string? currencyCode { get; set; }
    }
}

