﻿using System;
using HCore.Integration.Cellulant.Objects;

namespace HCore.Integration.Cellulant.Request.FetchPaymentOptionsResponse
{
    public class PaymentOptionsResponse
    {
        public ResponseStatus status { get; set; }
        public List<PaymentOptionsResponsePaymentOption> paymentOptions { get; set; }
    }

    public class PaymentOptionsResponsePaymentOption
    {
        public int paymentModeID { get; set; }
        public string? payerClientName { get; set; }
        public int payerModeID { get; set; }
        public string? paymentOptionCode { get; set; }
        public string? payerClientCode { get; set; }
        public string? countryCode { get; set; }
        public string? clientLogo { get; set; }
        public int minChargeAmount { get; set; }
        public int maxChargeAmount { get; set; }
        public string? currencyCode { get; set; }
        public string? paymentMode { get; set; }
        public string? paymentInstructions { get; set; }
        public string? languageCode { get; set; }
    }
}

