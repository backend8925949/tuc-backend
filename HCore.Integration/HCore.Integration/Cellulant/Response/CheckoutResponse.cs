﻿using System;
using HCore.Integration.Cellulant.Objects;

namespace HCore.Integration.Cellulant.Response
{
    public class CheckoutResponse
    {
        public ResponseStatus status { get; set; }
        public CheckoutResults results { get; set; }
    }

    public class CheckoutPaymentOption
    {
        public string? paymentModeName { get; set; }
        public int paymentModeID { get; set; }
        public string? serviceCode { get; set; }
        public string? payerClientName { get; set; }
        public int payerModeID { get; set; }
        public string? paymentOptionCode { get; set; }
        public string? payerClientCode { get; set; }
        public string? countryCode { get; set; }
        public string? clientLogo { get; set; }
        public string? serviceName { get; set; }
        public int minChargeAmount { get; set; }
        public int maxChargeAmount { get; set; }
        public string? currencyCode { get; set; }
        public string? paymentInstructions { get; set; }
        public string? languageCode { get; set; }
    }

    public class CheckoutResults
    {
        public int checkoutRequestID { get; set; }
        public string? merchantTransactionID { get; set; }
        public int conversionRate { get; set; }
        public string? originalCurrencyCode { get; set; }
        public int requestAmount { get; set; }
        public string? convertedCurrencyCode { get; set; }
        public int convertedAmount { get; set; }
        public List<CheckoutPaymentOption> paymentOptions { get; set; }
    }
}

