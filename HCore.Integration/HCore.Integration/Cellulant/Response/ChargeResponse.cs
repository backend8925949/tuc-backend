﻿using System;
using HCore.Integration.Cellulant.Objects;

namespace HCore.Integration.Cellulant.Response
{
    public class ChargeResponse
    {
        public ResponseStatus status { get; set; }
        public ChargeResults results { get; set; }
    }

    public class ChargeResults
    {
        public string? checkoutRequestID { get; set; }
        public string? merchantTransactionID { get; set; }
        public int chargeRequestID { get; set; }
        public string? paymentInstructions { get; set; }
        public string? languageCode { get; set; }
        public long chargeMsisdn { get; set; }
        public int chargeAmount { get; set; }
        public string? chargeRequestDate { get; set; }
        public string? redirectUrl { get; set; }
    }
}

