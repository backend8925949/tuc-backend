﻿using System;
namespace HCore.Integration.Cellulant.Response.DirectCardAPIResponse
{
    public class AuthorizationWith3DSResponse
    {
        public int statusCode { get; set; }
        public string? statusDescription { get; set; }
        public AuthorizationWith3DSResults results { get; set; }
    }

    public class AuthorizationWith3DSResults
    {
        public string? redirectUrl { get; set; }
        public string? type { get; set; }
    }
}

