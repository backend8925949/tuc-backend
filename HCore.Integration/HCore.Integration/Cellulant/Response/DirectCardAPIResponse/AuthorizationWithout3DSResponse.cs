﻿using System;
namespace HCore.Integration.Cellulant.Response.DirectCardAPIResponse
{
    public class AuthorizationWithout3DSResponse
    {
        public int statusCode { get; set; }
        public string? statusDescription { get; set; }
        public string? accountNumber { get; set; }
        public string? currencyCode { get; set; }
        public int checkoutRequestID { get; set; }
        public double amountPaid { get; set; }
        public string? MSISDN { get; set; }
        public int cpgTransactionID { get; set; }
        public string? datePaymentReceived { get; set; }
        public string? payerTransactionID { get; set; }
        public string? RRNo { get; set; }
        public string? authorizationCode { get; set; }
        public string? cardPrefix { get; set; }
    }
}

