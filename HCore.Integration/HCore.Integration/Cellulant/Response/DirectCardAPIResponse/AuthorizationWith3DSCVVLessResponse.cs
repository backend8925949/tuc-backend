﻿using System;
namespace HCore.Integration.Cellulant.Response.DirectCardAPIResponse
{
    public class AuthorizationWith3DSCVVLessResponse
    {
        public int statusCode { get; set; }
        public string? statusDescription { get; set; }
        public AuthorizationWith3DSCVVLessResults results { get; set; }
    }

    public class AuthorizationWith3DSCVVLessResults
    {
        public string? redirectUrl { get; set; }
        public string? type { get; set; }
    }
}

