﻿using System;
namespace HCore.Integration.BillPayment.Request
{
    public class FetchBillResponse
    {
        public string? accountNumber { get; set; }
        public int serviceID { get; set; }
        public string? serviceCode { get; set; }
        public string? dueDate { get; set; }
        public double dueAmount { get; set; }
        public string? currency { get; set; }
        public string? customerName { get; set; }
        public string? responseExtraData { get; set; }
        public int statusCode { get; set; }
        public string? statusDescription { get; set; }
    }
}

