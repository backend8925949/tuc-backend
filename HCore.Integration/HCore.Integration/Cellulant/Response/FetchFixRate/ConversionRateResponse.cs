﻿using System;
using HCore.Integration.Cellulant.Objects;

namespace HCore.Integration.Cellulant.Request.FetchFixRateResponse
{
    public class ConversionRateResponse
    {
        public ResponseStatus status { get; set; }
        public Results results { get; set; }
    }

    public class Results
    {
        public string? baseCurrencyCode { get; set; }
        public string? exchangeCurrencyCode { get; set; }
        public double exchangeRate { get; set; }
    }
}

