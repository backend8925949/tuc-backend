﻿using System;
using HCore.Integration.BillPayment.Objects;
using Newtonsoft.Json;

namespace HCore.Integration.BillPayment.Request
{
    public class FetchDataPlanResponse
    {
        public AuthStatus authStatus { get; set; }
        public List<FetchDataPlanResult> results { get; set; }
    }

    public class FetchDataPlanResult
    {
        public string? accountNumber { get; set; }
        public int serviceID { get; set; }
        public string? serviceCode { get; set; }
        public string? dueDate { get; set; }
        public string? dueAmount { get; set; }
        public string? currency { get; set; }
        public string? customerName { get; set; }
        public string? responseExtraData { get; set; }
        public int statusCode { get; set; }
        public string? statusDescription { get; set; }
    }

    public class GHMTNDataBundle
    {
        public string? bundleName { get; set; }
        public string? value { get; set; }
        public string? price { get; set; }
        public string? validity { get; set; }
        public string? extraData { get; set; }
    }

    public class ResponseExtraData
    {
        [JsonProperty("GH-MTN-DataBundle")]
        public List<GHMTNDataBundle> GHMTNDataBundle { get; set; }
    }

}

