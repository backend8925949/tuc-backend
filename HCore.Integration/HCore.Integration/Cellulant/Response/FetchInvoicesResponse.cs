﻿using System;
using HCore.Integration.BillPayment.Objects;

namespace HCore.Integration.BillPayment.Request
{
    public class FetchInvoicesResponse
    {
        public List<FetchInvoicesResult> results { get; set; }
        public AuthStatus authStatus { get; set; }
    }

    public class FetchInvoicesResult
    {
        public int statusCode { get; set; }
        public string? statusDescription { get; set; }
        public int serviceID { get; set; }
        public string? serviceCode { get; set; }
        public string? clientCode { get; set; }
        public string? clientName { get; set; }
        public string? invoiceNumber { get; set; }
        public string? currencyCode { get; set; }
        public string? accountNumber { get; set; }
        public int amount { get; set; }
        public string? narration { get; set; }
        public string? dueDate { get; set; }
        public int beepTransactionID { get; set; }
        public string? exactPaymentRequired { get; set; }
        public string? MSISDN { get; set; }
        public string? invoiceExtraData { get; set; }
    }
}

