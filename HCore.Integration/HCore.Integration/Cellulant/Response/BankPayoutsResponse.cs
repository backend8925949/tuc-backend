﻿using System;
using HCore.Integration.BillPayment.Objects;

namespace HCore.Integration.BillPayment.Response
{
    public class BankPayoutsResponse
    {
        public AuthStatus authStatus { get; set; }
        public List<BankPayoutsResult> results { get; set; }
    }
    public class BankPayoutsResult
    {
        public int statusCode { get; set; }
        public string? statusDescription { get; set; }
        public string? payerTransactionID { get; set; }
        public string? beepTransactionID { get; set; }
    }
}

