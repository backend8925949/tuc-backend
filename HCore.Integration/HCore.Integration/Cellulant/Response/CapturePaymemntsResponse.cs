﻿using System;
using HCore.Integration.Cellulant.Objects;

namespace HCore.Integration.Cellulant.Response
{
    public class CapturePaymemntsResponse
    {
        public ResponseStatus status { get; set; }
        public CapturePaymemntsResults results { get; set; }
    }

    public class CapturePaymemntsPayment
    {
        public int paymentID { get; set; }
        public int checkoutRequestID { get; set; }
        public string? cpgTransactionID { get; set; }
        public string? payerTransactionID { get; set; }
        public long MSISDN { get; set; }
        public string? accountNumber { get; set; }
        public string? customerName { get; set; }
        public int amountPaid { get; set; }
        public string? merchantReceipt { get; set; }
        public object payerNarration { get; set; }
        public string? receiverNarration { get; set; }
        public int hubOverallStatus { get; set; }
        public string? statusCodeDesc { get; set; }
        public string? payerClientCode { get; set; }
        public string? payerClientName { get; set; }
        public string? ownerClientCode { get; set; }
        public string? ownerClientName { get; set; }
        public string? merchantTransactionID { get; set; }
        public int convertedAmount { get; set; }
        public int totalPayableAmount { get; set; }
        public string? datePaymentReceived { get; set; }
        public object datePaymentAcknowledged { get; set; }
        public string? dateCreated { get; set; }
        public string? currencyCode { get; set; }
        public string? countryCode { get; set; }
    }

    public class CapturePaymemntsResults
    {
        public int checkoutRequestID { get; set; }
        public string? merchantTransactionID { get; set; }
        public int requestStatusCode { get; set; }
        public List<CapturePaymemntsPayment> payments { get; set; }
    }
}

