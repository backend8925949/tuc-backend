﻿using System;
using HCore.Integration.BillPayment.Objects;

namespace HCore.Integration.BillPayment.Response
{
    public class SendMoneyResponse
    {
        public AuthStatus authStatus { get; set; }
        public List<SendMoneyResult> results { get; set; }
    }
    public class SendMoneyResult
    {
        public int statusCode { get; set; }
        public string? statusDescription { get; set; }
        public string? payerTransactionID { get; set; }
        public string? beepTransactionID { get; set; }
    }
}

