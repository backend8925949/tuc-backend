﻿using System;
using HCore.Integration.BillPayment.Objects;

namespace HCore.Integration.BillPayment.Request
{
    public class ValidateAccountResponse
    {
        public AuthStatus authStatus { get; set; }
        public List<ValidateAccountResult> results { get; set; }
    }

    public class ValidateAccountResult
    {
        public int statusCode { get; set; }
        public string? statusDescription { get; set; }
        public int serviceID { get; set; }
        public string? accountNumber { get; set; }
        public string? active { get; set; }
        public string? customerName { get; set; }
        public string? responseExtraData { get; set; }
    }
}

