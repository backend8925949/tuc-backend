﻿using System;
using HCore.Integration.Cellulant.Objects;

namespace HCore.Integration.Cellulant.Response
{
    public class InitiateRefundResponse
    {
        public ResponseStatus status { get; set; }
        public InitiateRefundResults results { get; set; }
    }

    public class InitiateRefundResults
    {
        public int checkoutRequestID { get; set; }
        public string? merchantTransactionID { get; set; }
    }
}

