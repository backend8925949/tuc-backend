﻿using System;
using HCore.Integration.Cellulant.Objects;

namespace HCore.Integration.Cellulant.Response
{
    public class CancelRequestResponse
    {
        public ResponseStatus status { get; set; }
        public CancelRequestResults results { get; set; }
    }

    public class CancelRequestResults
    {
        public int checkoutRequestID { get; set; }
        public string? merchantTransactionID { get; set; }
        public int requestStatusCode { get; set; }
    }
}

