﻿using System;
using HCore.Integration.BillPayment.Objects;

namespace HCore.Integration.BillPayment.Response
{
    public class BillPaymentResponse
    {
        public AuthStatus authStatus { get; set; }
        public List<BillPaymentResult> results { get; set; }
    }

    public class BillPaymentResult
    {
        public int statusCode { get; set; }
        public string? statusDescription { get; set; }
        public string? payerTransactionID { get; set; }
        public string? beepTransactionID { get; set; }
        public string? receiptNumber { get; set; }
    }
}

