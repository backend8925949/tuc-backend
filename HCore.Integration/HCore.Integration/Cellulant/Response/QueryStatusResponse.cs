﻿using System;
using HCore.Integration.Cellulant.Objects;

namespace HCore.Integration.Cellulant.Response
{
    public class QueryStatusResponse
    {
        public ResponseStatus status { get; set; }
        public QueryStatusResults results { get; set; }
    }

    public class QueryStatusPayment
    {
        public string? payerTransactionID { get; set; }
        public long MSISDN { get; set; }
        public string? accountNumber { get; set; }
        public string? customerName { get; set; }
        public int amountPaid { get; set; }
        public string? payerClientCode { get; set; }
        public string? cpgTransactionID { get; set; }
        public string? paymentDate { get; set; }
        public string? clientName { get; set; }
        public string? clientDisplayName { get; set; }
        public string? currencyCode { get; set; }
        public int currencyID { get; set; }
        public int paymentID { get; set; }
        public int hubOverallStatus { get; set; }
        public int clientCategoryID { get; set; }
        public string? clientCategoryName { get; set; }
        public object payerNarration { get; set; }
    }

    public class QueryStatusResults
    {
        public int checkoutRequestID { get; set; }
        public string? merchantTransactionID { get; set; }
        public long MSISDN { get; set; }
        public string? accountNumber { get; set; }
        public string? requestDate { get; set; }
        public int requestStatusCode { get; set; }
        public string? serviceName { get; set; }
        public string? serviceCode { get; set; }
        public string? requestCurrencyCode { get; set; }
        public int requestAmount { get; set; }
        public string? paymentCurrencyCode { get; set; }
        public int amountPaid { get; set; }
        public string? shortUrl { get; set; }
        public string? redirectTrigger { get; set; }
        public List<QueryStatusPayment> payments { get; set; }
        public List<object> failedPayments { get; set; }
    }
}

