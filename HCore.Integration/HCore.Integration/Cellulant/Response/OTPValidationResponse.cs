﻿using System;
using HCore.Integration.Cellulant.Objects;

namespace HCore.Integration.Cellulant.Response
{
    public class OTPValidationResponse
    {
        public class InitiateOTPResponse
        {
            public ResponseStatus status { get; set; }
        }

        public class ValidateOTPResponse
        {
            public ResponseStatus status { get; set; }
        }
    }
}

