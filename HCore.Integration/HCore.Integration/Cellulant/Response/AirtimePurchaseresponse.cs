﻿using System;
using HCore.Integration.BillPayment.Objects;

namespace HCore.Integration.BillPayment.Response
{
    public class AirtimePurchaseresponse
    {
        public AuthStatus authStatus { get; set; }
        public List<AirtimePurchaseResult> results { get; set; }
    }
    public class AirtimePurchaseResult
    {
        public int statusCode { get; set; }
        public string? statusDescription { get; set; }
        public string? payerTransactionID { get; set; }
        public string? beepTransactionID { get; set; }
    }
}

