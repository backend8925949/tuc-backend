﻿using System;
using HCore.Integration.Cellulant.Objects;

namespace HCore.Integration.Cellulant.Response
{
    public class ValidateChargeResponse
    {
        public ResponseStatus status { get; set; }
        public ValidateChargeResults results { get; set; }
    }

    public class ValidateChargeResults
    {
        public string? checkoutRequestID { get; set; }
        public string? merchantTransactionID { get; set; }
        public int chargeRequestID { get; set; }
        public string? postValidationInstructions { get; set; }
    }
}

