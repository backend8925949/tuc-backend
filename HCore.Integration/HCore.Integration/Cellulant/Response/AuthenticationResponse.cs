﻿using System;
namespace HCore.Integration.Cellulant.Response
{
    public class AuthenticationResponse
    {
        public string? error { get; set; }
        public string? message { get; set; }
        public string? token_type { get; set; }
        public int expires_in { get; set; }
        public string? access_token { get; set; }
    }
}

