﻿
using System;
namespace HCore.Integration.Flutterwave.Objects
{
	public class InitializePaymentRequest
    {
        public string? tx_ref { get; set; }
        public string? amount { get; set; }
        public string? currency { get; set; }
        public string? redirect_url { get; set; }
        public Meta? meta { get; set; }
        public Customer? customer { get; set; }
        public Customizations? customizations { get; set; }

        public class Customer
        {
            public string? email { get; set; }
            public string? phonenumber { get; set; }
            public string? name { get; set; }
        }

        public class Customizations
        {
            public string? title { get; set; }
            public string? logo { get; set; }
        }

        public class Meta
        {
            public int? consumer_id { get; set; }
            public string? consumer_mac { get; set; }
        }
    }
}

