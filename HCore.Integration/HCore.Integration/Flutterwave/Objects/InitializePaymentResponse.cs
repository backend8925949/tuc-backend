﻿using System;
namespace HCore.Integration.Flutterwave.Objects
{
	public class InitializePaymentResponse
    {
        public string? status { get; set; }
        public string? message { get; set; }
        public Data? data { get; set; }

        public class Data
        {
            public string? link { get; set; }
        }
    }
}

