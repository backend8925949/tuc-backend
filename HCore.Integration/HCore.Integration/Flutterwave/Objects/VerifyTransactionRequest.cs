﻿using System;
using HCore.Helper;

namespace HCore.Integration.Flutterwave.Objects
{
	public class VerifyTransactionRequest
    {
        public string? Id { get; set; }
        public OUserReference? UserReference { get; set; }
    }
}

