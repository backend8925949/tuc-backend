﻿using System;
using HCore.Helper;
using HCore.Integration.Flutterwave.Objects;
using System.Net;
using Newtonsoft.Json;
using System.Text;
using static HCore.Helper.HCoreConstant;

namespace HCore.Integration.Flutterwave
{
	public class FlutterwavePayments
	{
		private static HttpWebRequest CreateWebRequest(string? Type, string? Url, byte[] PayloadBytes)
		{
			try
			{
				var HttpWebRequest = (HttpWebRequest)WebRequest.Create(Url);
				HttpWebRequest.ContentType = "application/json";
				HttpWebRequest.Method = Type;
				HttpWebRequest.Headers["Authorization"] = "Bearer " + _AppConfig.FlutterwaveApiKey;

				if(PayloadBytes != null)
				{
					HttpWebRequest.ContentLength = PayloadBytes.Length;
					Stream RequestStream = HttpWebRequest.GetRequestStream();
					RequestStream.WriteAsync(PayloadBytes, 0, PayloadBytes.Length);
					RequestStream.Close();
				}

				return HttpWebRequest;
			}
			catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreateWebRequest", _Exception);
				return null;
            }
		}

		public static InitializePaymentResponse InitializeFlutterwavePayment(InitializePaymentRequest _Request)
		{
			try
            {
                string? RequestUrl = _AppConfig.FlutterwaveUrl + "/payments";

				string? RequestBody = JsonConvert.SerializeObject(_Request);
				byte[] RequestBytes = Encoding.ASCII.GetBytes(RequestBody);

				var HttpWebRequest = CreateWebRequest("POST", RequestUrl, RequestBytes);

                HttpWebResponse? _HttpWebResponse = (HttpWebResponse?)HttpWebRequest.GetResponse();

                if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
				{
					var StreamReader = new StreamReader(_HttpWebResponse.GetResponseStream());
					string? _Response = StreamReader.ReadToEnd();
					InitializePaymentResponse? PaymentResponse = JsonConvert.DeserializeObject<InitializePaymentResponse>(_Response);
					return PaymentResponse;
				}
				else
                {
                    InitializePaymentResponse _Response = new InitializePaymentResponse();
                    _Response.status = "Error";
                    _Response.message = "Unable to process your payment.";
                    return _Response;
                }
            }
			catch (Exception _Exception)
			{
				HCoreHelper.LogException("InitializeFlutterwavePayment", _Exception);
				InitializePaymentResponse _Response = new InitializePaymentResponse();
				_Response.status = "Error";
				_Response.message = "Unable to process your payment request. please try after some time.";
				return _Response;
            }
		}

		public static VerifyTransactionResponse VerifyTransaction(VerifyTransactionRequest _Request)
		{
			try
            {
                string? RequestUrl = _AppConfig.FlutterwaveUrl + "/transactions/" + _Request.Id + "/verify";

                string? RequestBody = JsonConvert.SerializeObject(_Request);
                byte[] RequestBytes = Encoding.ASCII.GetBytes(RequestBody);

                var HttpWebRequest = CreateWebRequest("GET", RequestUrl, null);

                HttpWebResponse? _HttpWebResponse = (HttpWebResponse?)HttpWebRequest.GetResponse();

                if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var StreamReader = new StreamReader(_HttpWebResponse.GetResponseStream());
                    string? _Response = StreamReader.ReadToEnd();
                    VerifyTransactionResponse? VerifyTransactionResponse = JsonConvert.DeserializeObject<VerifyTransactionResponse>(_Response);
                    return VerifyTransactionResponse;
                }
                else
                {
                    VerifyTransactionResponse? _Response = new VerifyTransactionResponse();
                    _Response.status = "Error";
                    _Response.message = "Unable to process your payment.";
                    return _Response;
                }
            }
			catch (Exception _Exception)
            {
                HCoreHelper.LogException("InitializeFlutterwavePayment", _Exception, _Request.UserReference);
                VerifyTransactionResponse? _Response = new VerifyTransactionResponse();
                _Response.status = "Error";
                _Response.message = _Exception.Message;
                return _Response;
            }
		}
	}
}

