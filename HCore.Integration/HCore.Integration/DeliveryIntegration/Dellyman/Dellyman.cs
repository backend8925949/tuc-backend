//==================================================================================
// FileName: Dellyman.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Dellyman.DellymanObject;
using Dellyman.DellymanObject.Request;
using Dellyman.DellymanObject.Response;
using HCore.Helper;
using HCore.Integration.DellymanIntegrations.DellymanObject.Request;
using HCore.Integration.DellymanIntegrations.DellymanObject.Response;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;

namespace Dellyman
{
    public class DellymanOrders
    {
        private HttpWebRequest CreateWebRequest(string RequestType, string? url, byte[] payload)
        {
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = RequestType;
                httpWebRequest.Headers["Authorization"] = "Bearer " + _AppConfig.DellymenApiKey;

                if (payload != null)
                {
                    httpWebRequest.ContentLength = payload.Length;
                    Stream _RequestStream = httpWebRequest.GetRequestStream();
                    _RequestStream.Write(payload, 0, payload.Length);
                    _RequestStream.Close();
                }

                return httpWebRequest;
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreateWebRequest", _Exception);
                return null;
            }
        }

        public LogInResponse LogIn(LogInRequest request)
        {
            try
            {
                string apiurl = _AppConfig.DellymenUrl + "Login";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    LogInResponse? loginresponse = JsonConvert.DeserializeObject<LogInResponse>(response);
                    return loginresponse;
                }
                else
                {
                    LogInResponse? response = new LogInResponse();
                    response.ResponseCode = (int)httpweresponse.StatusCode;
                    response.ResponseMessage = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("LogIn", _Exception);
                LogInResponse? response = new LogInResponse();
                response.ResponseCode = 00;
                response.ResponseMessage = _Exception.Message;
                return response;
            }
        }

        public GetQuotesResponse GetQuotes(GetQuotesRequest request)
        {
            try
            {
                string apiurl = _AppConfig.DellymenUrl + "GetQuotes";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    GetQuotesResponse? loginresponse = JsonConvert.DeserializeObject<GetQuotesResponse>(response);
                    return loginresponse;
                }
                else
                {
                    GetQuotesResponse? response = new GetQuotesResponse();
                    response.ResponseCode = (int)httpweresponse.StatusCode;
                    response.ResponseMessage = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetQuotes", _Exception);
                GetQuotesResponse? response = new GetQuotesResponse();
                response.ResponseCode = 00;
                response.ResponseMessage = _Exception.Message;
                return response;
            }
        }

        public BookOrderResponse BookOrder(BookOrderRequest request)
        {
            try
            {
                string apiurl = _AppConfig.DellymenUrl + "BookOrder";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    BookOrderResponse? loginresponse = JsonConvert.DeserializeObject<BookOrderResponse>(response);
                    return loginresponse;
                }
                else
                {
                    BookOrderResponse? response = new BookOrderResponse();
                    response.ResponseCode = (int)httpweresponse.StatusCode;
                    response.ResponseMessage = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("BookOrder", _Exception);
                BookOrderResponse? response = new BookOrderResponse();
                response.ResponseCode = 00;
                response.ResponseMessage = _Exception.Message;
                return response;
            }
        }

        public GetOrderResponse GetOrder(GetOrderRequest request)
        {
            try
            {
                string apiurl = _AppConfig.DellymenUrl + "GetOrder";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    GetOrderResponse? loginresponse = JsonConvert.DeserializeObject<GetOrderResponse>(response);
                    return loginresponse;
                }
                else
                {
                    GetOrderResponse? response = new GetOrderResponse();
                    response.ResponseCode = (int)httpweresponse.StatusCode;
                    response.ResponseMessage = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetOrder", _Exception);
                GetOrderResponse? response = new GetOrderResponse();
                response.ResponseCode = 00;
                response.ResponseMessage = _Exception.Message;
                return response;
            }
        }

        public FetchOrderResponse FetchOrder(FetchOrderRequest request)
        {
            try
            {
                string apiurl = _AppConfig.DellymenUrl + "FetchOrders";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    FetchOrderResponse? loginresponse = JsonConvert.DeserializeObject<FetchOrderResponse>(response);
                    return loginresponse;
                }
                else
                {
                    FetchOrderResponse? response = new FetchOrderResponse();
                    response.ResponseCode = (int)httpweresponse.StatusCode;
                    response.ResponseMessage = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("FetchOrder", _Exception);
                FetchOrderResponse? response = new FetchOrderResponse();
                response.ResponseCode = 00;
                response.ResponseMessage = _Exception.Message;
                return response;
            }
        }

        public TrackOrderResponse TrackOrder(TrackOrderRequest request)
        {
            try
            {
                string apiurl = _AppConfig.DellymenUrl + "TrackOrder";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    TrackOrderResponse? loginresponse = JsonConvert.DeserializeObject<TrackOrderResponse>(response);
                    return loginresponse;
                }
                else
                {
                    TrackOrderResponse? response = new TrackOrderResponse();
                    response.ResponseCode = (int)httpweresponse.StatusCode;
                    response.ResponseMessage = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("TrackOrder", _Exception);
                TrackOrderResponse? response = new TrackOrderResponse();
                response.ResponseCode = 00;
                response.ResponseMessage = _Exception.Message;
                return response;
            }
        }

        public GetVehicles GetVehicles()
        {
            try
            {
                string apiurl = _AppConfig.DellymenUrl + "Vehicles";

                var httpwebrequest = CreateWebRequest("GET", apiurl, null);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    GetVehicles? loginresponse = JsonConvert.DeserializeObject<GetVehicles>(response);
                    return loginresponse;
                }
                else
                {
                    GetVehicles? response = new GetVehicles();
                    response.ResponseCode = (int)httpweresponse.StatusCode;
                    response.ResponseMessage = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetVehicles", _Exception);
                GetVehicles? response = new GetVehicles();
                response.ResponseCode = 00;
                response.ResponseMessage = _Exception.Message;
                return response;
            }
        }

        public LogOutResponse LogOut(LogOutRequest request)
        {
            try
            {
                string apiurl = _AppConfig.DellymenUrl + "Logout";
                string requestbody = JsonConvert.SerializeObject(request);
                byte[] length = Encoding.ASCII.GetBytes(requestbody);

                var httpwebrequest = CreateWebRequest("POST", apiurl, length);

                HttpWebResponse? httpweresponse = (HttpWebResponse)httpwebrequest.GetResponse();
                if (httpweresponse.StatusCode == HttpStatusCode.OK)
                {
                    var streamreader = new StreamReader(httpweresponse.GetResponseStream());
                    string response = streamreader.ReadToEnd();
                    LogOutResponse? logoutresponse = JsonConvert.DeserializeObject<LogOutResponse>(response);
                    return logoutresponse;
                }
                else
                {
                    LogOutResponse? response = new LogOutResponse();
                    response.ResponseCode = (int)httpweresponse.StatusCode;
                    response.ResponseMessage = httpweresponse.StatusDescription;
                    return response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("LogOut", _Exception);
                LogOutResponse? response = new LogOutResponse();
                response.ResponseCode = 00;
                response.ResponseMessage = _Exception.Message;
                return response;
            }
        }
    }
}

