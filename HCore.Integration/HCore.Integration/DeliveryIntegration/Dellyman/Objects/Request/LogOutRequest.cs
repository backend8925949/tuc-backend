﻿using System;
namespace HCore.Integration.DellymanIntegrations.DellymanObject.Request
{
    public class LogOutRequest
    {
        public int CustomerID { get; set; }
        public string? CustomerAuth { get; set; }
    }
}

