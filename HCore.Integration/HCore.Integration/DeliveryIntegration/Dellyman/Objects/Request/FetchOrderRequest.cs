//==================================================================================
// FileName: FetchOrderRequest.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Dellyman.DellymanObject.Request
{
	public class FetchOrderRequest
    {
        public string? Status { get; set; }
        public string? FilterFrom { get; set; }
        public string? FilterTo { get; set; }
    }
}

