//==================================================================================
// FileName: BookOrderRequest.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Dellyman.DellymanObject.Request
{
	public class BookOrderRequest
    {
        public string? OrderRef { get; set; }
        public int? CompanyID { get; set; }
        public string? PaymentMode { get; set; }
        public int? Vehicle { get; set; }
        public string? PickUpContactName { get; set; }
        public string? PickUpContactNumber { get; set; }
        public string? PickUpGooglePlaceAddress { get; set; }
        public string? PickUpLandmark { get; set; }
        public int? IsInstantDelivery { get; set; }
        public string? PickUpRequestedDate { get; set; }
        public string? PickUpRequestedTime { get; set; }
        public string? DeliveryRequestedTime { get; set; }
        public List<BookOrderPackage> Packages { get; set; }
    }

    public class BookOrderPackage
    {
        public string? PackageDescription { get; set; }
        public string? DeliveryContactName { get; set; }
        public string? DeliveryContactNumber { get; set; }
        public string? DeliveryGooglePlaceAddress { get; set; }
        public string? DeliveryLandmark { get; set; }
    }
}

