//==================================================================================
// FileName: GetQuotesRequest.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Dellyman.DellymanObject.Request
{
	public class GetQuotesRequest
    {
        public string? PaymentMode { get; set; }
        public int? VehicleID { get; set; }
        public string? PickupRequestedTime { get; set; }
        public string? PickupRequestedDate { get; set; }
        public string? PickupAddress { get; set; }
        public List<string> DeliveryAddress { get; set; }
        public List<int?> ProductAmount { get; set; }
        public List<int?> PackageWeight { get; set; }
        public int? IsProductOrder { get; set; }
        public int? IsInstantDelivery { get; set; }
    }
}

