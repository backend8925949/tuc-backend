//==================================================================================
// FileName: TrackOrderRequest.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Dellyman.DellymanObject.Request
{
	public class TrackOrderRequest
    {
        public string? OrderCode { get; set; }
        public int? OrderID { get; set; }
    }
}

