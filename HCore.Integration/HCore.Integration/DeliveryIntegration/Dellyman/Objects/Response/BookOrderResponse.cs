//==================================================================================
// FileName: BookOrderResponse.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Dellyman.DellymanObject.Response
{
	public class BookOrderResponse
    {
        public int? ResponseCode { get; set; }
        public string? ResponseMessage { get; set; }
        public long? OrderID { get; set; }
        public long? TrackingID { get; set; }
        public string? Reference { get; set; }
    }
}

