//==================================================================================
// FileName: FetchOrderResponse.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Dellyman.DellymanObject.Response
{
	public class FetchOrderResponse
    {
        public int ResponseCode { get; set; }
        public string? ResponseMessage { get; set; }
        public Orders Orders { get; set; }
    }

    public class Orders
    {
        public List<object> Pending { get; set; }
        public List<object> Assigned { get; set; }
        public List<object> Intransit { get; set; }
        public List<object> Completed { get; set; }
        public List<object> Cancelled { get; set; }
    }
}

