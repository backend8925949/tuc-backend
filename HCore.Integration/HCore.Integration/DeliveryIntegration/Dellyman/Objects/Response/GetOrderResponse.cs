//==================================================================================
// FileName: GetOrderResponse.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Dellyman.DellymanObject.Response
{
	public class GetOrderResponse
    {
        public int OrderID { get; set; }
        public string? OrderCode { get; set; }
        public string? OrderStatus { get; set; }
        public int OrderPrice { get; set; }
        public object FixedDeliveryCharge { get; set; }
        public string? PayAt { get; set; }
        public string? IsInstantDelivery { get; set; }
        public int IsProductOrder { get; set; }
        public int IsPostpaid { get; set; }
        public object BankCode { get; set; }
        public object BankName { get; set; }
        public object AccountNumber { get; set; }
        public string? OrderedAt { get; set; }
        public object AssignedAt { get; set; }
        public object PickedUpAt { get; set; }
        public object DeliveredAt { get; set; }
        public object CancelledAt { get; set; }
        public object RejectedAt { get; set; }
        public List<GetOrderPackage> Packages { get; set; }
        public int? ResponseCode { get; set; }
        public string? ResponseMessage { get; set; }
    }

    public class GetOrderPackage
    {
        public int PackageID { get; set; }
        public int IsDelivered { get; set; }
        public string? PackageDescription { get; set; }
        public int PackageDistance { get; set; }
        public int PackageWeight { get; set; }
        public object ProductAmount { get; set; }
        public string? PickUpContactName { get; set; }
        public string? PickUpContactNumber { get; set; }
        public string? PickUpGooglePlaceAddress { get; set; }
        public string? PickUpLandmark { get; set; }
        public double PickUpLatitude { get; set; }
        public double PickUpLongitude { get; set; }
        public string? PickUpExpectedAt { get; set; }
        public string? DeliveryContactName { get; set; }
        public string? DeliveryContactNumber { get; set; }
        public string? DeliveryGooglePlaceAddress { get; set; }
        public string? DeliveryLandmark { get; set; }
        public double DeliveryLatitude { get; set; }
        public double DeliveryLongitude { get; set; }
        public string? DeliveryExpectedAt { get; set; }
        public int DeliveryCode { get; set; }
    }
}

