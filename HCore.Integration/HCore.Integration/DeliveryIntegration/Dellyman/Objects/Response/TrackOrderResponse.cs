//==================================================================================
// FileName: TrackOrderResponse.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Dellyman.DellymanObject.Response
{
	public class TrackOrderResponse
    {
        public int? OrderID { get; set; }
        public string? OrderCode { get; set; }
        public string? OrderStatus { get; set; }
        public string? OrderedAt { get; set; }
        public string? AssignedAt { get; set; }
        public string? PickedUpAt { get; set; }
        public string? DeliveredAt { get; set; }
        public string? CancelledAt { get; set; }
        public string? RejectedAt { get; set; }
        public string? Note { get; set; }
        public string? RiderName { get; set; }
        public double? RiderLatitude { get; set; }
        public double? RiderLongitude { get; set; }
        public List<TrackOrderPackage>? Packages { get; set; }
        public int? ResponseCode { get; set; }
        public string? ResponseMessage { get; set; }
    }

    public class TrackOrderPackage
    {
        public string? PackageDescription { get; set; }
        public int? IsDelivered { get; set; }
        public double? PickupLatitude { get; set; }
        public double? PickUpLongitude { get; set; }
        public double? DeliveryLatitude { get; set; }
        public double? DeliveryLongitude { get; set; }
        public int? Distance { get; set; }
    }
}

