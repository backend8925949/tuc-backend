﻿using System;
namespace HCore.Integration.DellymanIntegrations.DellymanObject.Response
{
    public class GetVehicles
    {
        public int? ResponseCode { get; set; }
        public string? ResponseMessage { get; set; }
        public _1 _1 { get; set; }
        public _2 _2 { get; set; }
        public _3 _3 { get; set; }
        public _4 _4 { get; set; }
        public _5 _5 { get; set; }
    }

    public class _1
    {
        public string? VehicleID { get; set; }
        public string? Name { get; set; }
    }

    public class _2
    {
        public string? VehicleID { get; set; }
        public string? Name { get; set; }
    }

    public class _3
    {
        public string? VehicleID { get; set; }
        public string? Name { get; set; }
    }

    public class _4
    {
        public string? VehicleID { get; set; }
        public string? Name { get; set; }
    }

    public class _5
    {
        public string? VehicleID { get; set; }
        public string? Name { get; set; }
    }
}

