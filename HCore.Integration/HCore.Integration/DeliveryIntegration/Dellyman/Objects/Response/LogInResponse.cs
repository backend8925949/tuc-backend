//==================================================================================
// FileName: LogInResponse.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Dellyman.DellymanObject
{
	public class LogInResponse
    {
        public int ResponseCode { get; set; }
        public string? ResponseMessage { get; set; }
        public int CustomerID { get; set; }
        public string? Email { get; set; }
        public string? Name { get; set; }
        public string? PhoneNumber { get; set; }
        public int IsActive { get; set; }
        public string? Currency { get; set; }
        public string? CustomerAuth { get; set; }
    }
}

