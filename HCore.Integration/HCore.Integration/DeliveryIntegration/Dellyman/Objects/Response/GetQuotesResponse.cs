using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Dellyman.DellymanObject.Response
{
    public class GetQuotesResponse
    {
        public int ResponseCode { get; set; }
        public string? ResponseMessage { get; set; }
        public List<DeliveryPartners>? Companies { get; set; }
        public RejectedCompanies? RejectedCompanies { get; set; }
        public Products? Products { get; set; }
        public double Distance { get; set; }
    }

    public class ProductAmount
    {
        public int Amount { get; set; }
        public int Commission { get; set; }
        public int Settlement { get; set; }
    }

    public class Products
    {
        public int TotalAmount { get; set; }
        public int TotalCommission { get; set; }
        public int TotalSettlement { get; set; }
        public List<ProductAmount>? ProductAmounts { get; set; }
    }

    public class RejectedCompanies
    {
        public List<int>? outOfCoverage { get; set; }
    }

    public class DeliveryPartners
    {
        public int CompanyID { get; set; }
        public string? Name { get; set; }
        public int TotalPrice { get; set; }
        public int OriginalPrice { get; set; }
        public int SavedPrice { get; set; }
        public int SameDayPrice { get; set; }
        public int NextDayPrice { get; set; }

        [JsonProperty("48-72HoursDeliveryPrice")]
        public int _4872HoursDeliveryPrice { get; set; }
        public int DeductablePrice { get; set; }
        public int AvgRating { get; set; }
        public int NumberOfOrders { get; set; }
        public int NumberOfRating { get; set; }
    }
}

