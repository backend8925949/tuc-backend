﻿using System;
namespace HCore.Integration.DellymanIntegrations.DellymanObject.Response
{
    public class WebhookResponse
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public Orders? Order { get; set; }
    }

    public class Orders
    {
        public int? OrderID { get; set; }
        public string? OrderCode { get; set; }
        public int? CustomerID { get; set; }
        public int? CompanyID { get; set; }
        public long? TrackingID { get; set; }
        public string? OrderDate { get; set; }
        public string? OrderStatus { get; set; }
        public double? OrderPrice { get; set; }
        public string? AssignedAt { get; set; }
        public string? PickedUpAt { get; set; }
        public string? DeliveredAt { get; set; }
        public string? Note { get; set; }
    }
}

