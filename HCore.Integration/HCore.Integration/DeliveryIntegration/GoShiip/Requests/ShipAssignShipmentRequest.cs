﻿using System;
namespace HCore.Integration.DeliveryIntegration.GoShiip.Requests
{
	public class ShipAssignShipmentRequest
    {
        public string? type { get; set; }
        public string? rate_id { get; set; }
        public string? redis_key { get; set; }
        public int? user_id { get; set; }
        public string? shipment_id { get; set; }
        public string? toPhoneNumber { get; set; }
        public string? fromPhoneNumber { get; set; }
    }
}

