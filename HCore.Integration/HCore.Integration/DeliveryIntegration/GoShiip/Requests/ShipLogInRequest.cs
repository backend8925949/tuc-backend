﻿using System;
namespace HCore.Integration.DeliveryIntegration.GoShiip.Requests
{
	public class ShipLogInRequest
    {
        public string? email_phone { get; set; }
        public string? password { get; set; }
    }
}

