﻿using System;
namespace HCore.Integration.DeliveryIntegration.GoShiip.Requests
{
	public class ShipGetRatesRequest
	{
        public string? type { get; set; }
        public List<Item>? items { get; set; }
        public Parcels? parcels { get; set; }
        public FromAddress? fromAddress { get; set; }
        public ToAddress? toAddress { get; set; }

        public class FromAddress
        {
            public string? name { get; set; }
            public string? email { get; set; }
            public string? phone { get; set; }
            public string? country { get; set; }
            public string? country_code { get; set; }
            public string? state { get; set; }
            public string? city { get; set; }
            public string? zip { get; set; }
            public string? address { get; set; }
            public string? latitude { get; set; }
            public string? longitude { get; set; }
        }

        public class Item
        {
            public string? category { get; set; }
            public string? name { get; set; }
            public string? quantity { get; set; }
            public int? weight_id { get; set; }
            public double? weight { get; set; }
            public double? height { get; set; }
            public double? width { get; set; }
            public double? length { get; set; }
            public string? amount { get; set; }
            public string? description { get; set; }
            public string? pickup_date { get; set; }
        }

        public class Parcels
        {
            public double? weight { get; set; }
            public double? length { get; set; }
            public double? height { get; set; }
            public double? width { get; set; }
            public string? date { get; set; }
        }

        public class ToAddress
        {
            public string? name { get; set; }
            public string? email { get; set; }
            public string? phone { get; set; }
            public string? country { get; set; }
            public string? country_code { get; set; }
            public string? state { get; set; }
            public string? city { get; set; }
            public string? zip { get; set; }
            public string? address { get; set; }
            public string? latitude { get; set; }
            public string? longitude { get; set; }
        }
    }
}

