﻿using System;
namespace HCore.Integration.DeliveryIntegration.GoShiip.Requests
{
	public class ShipBookShipmentRequest
    {
        public string? redis_key { get; set; }
        public int? user_id { get; set; }
        public string? rate_id { get; set; }
        public string? platform { get; set; }
    }
}

