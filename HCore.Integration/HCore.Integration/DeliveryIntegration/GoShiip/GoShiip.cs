﻿using System;
using HCore.Helper;
using System.IO;
using System.Text;
using System.Net;
using HCore.Integration.DeliveryIntegration.GoShiip.Requests;
using HCore.Integration.DeliveryIntegration.GoShiip.Response;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using static HCore.Helper.HCoreConstant;

namespace HCore.Integration.DeliveryIntegration.GoShiip
{
    public class GoShiip
    {
        private async Task<HttpWebRequest> CreateWebrequest(string Type, string? URL, byte[] PayloadBytes, string? ApiKey)
        {
            try
            {
                HttpWebRequest? _HttpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                _HttpWebRequest.ContentType = "application/json";
                _HttpWebRequest.Method = Type;
                if (!string.IsNullOrEmpty(ApiKey))
                {
                    _HttpWebRequest.Headers["Authorization"] = "Bearer " + ApiKey;
                }

                if (PayloadBytes != null)
                {
                    _HttpWebRequest.ContentLength = PayloadBytes.Length;
                    Stream _Stream = await _HttpWebRequest.GetRequestStreamAsync();
                    await _Stream.WriteAsync(PayloadBytes, 0, PayloadBytes.Length);
                    _Stream.Close();
                }

                return _HttpWebRequest;
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreateWebrequest-GoSiip", _Exception);
                return null;
            }
        }

        public async Task<ShipLogInResponse> ShipLogIn(ShipLogInRequest _Request)
        {
            try
            {
                string RequestUrl = _AppConfig.ShiipUrl + "/auth/login";
                string RequestBody = JsonConvert.SerializeObject(_Request);
                byte[] bytes = Encoding.ASCII.GetBytes(RequestBody);

                var _HttpWebRequest = await CreateWebrequest("POST", RequestUrl, bytes, null);
                HttpWebResponse? _HttpWebResponse = (HttpWebResponse)await _HttpWebRequest.GetResponseAsync();

                if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    StreamReader _StreamReader = new StreamReader(_HttpWebResponse.GetResponseStream());
                    string? _Response = await _StreamReader.ReadToEndAsync();
                    ShipLogInResponse? _LogInResponse = JsonConvert.DeserializeObject<ShipLogInResponse>(_Response);
                    return _LogInResponse;
                }
                else
                {
                    ShipLogInResponse? _LogInResponse = new ShipLogInResponse();
                    _LogInResponse.status = false;
                    _LogInResponse.message = "Log-in failed to the Shiip platform.";
                    return _LogInResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ShipLogIn", _Exception);
                ShipLogInResponse? _LogInResponse = new ShipLogInResponse();
                _LogInResponse.status = false;
                _LogInResponse.message = "Log-in failed to the Shiip platform.";
                return _LogInResponse;
            }
        }

        public async Task<ShipGetRatesResponse> ShipGetRates(ShipGetRatesRequest _Request, string? ApiKey)
        {
            try
            {
                string RequestUrl = _AppConfig.ShiipUrl + "/tariffs/allprice";
                string RequestBody = JsonConvert.SerializeObject(_Request);
                byte[] bytes = Encoding.ASCII.GetBytes(RequestBody);

                var _HttpWebRequest = await CreateWebrequest("POST", RequestUrl, bytes, ApiKey);
                HttpWebResponse? _HttpWebResponse = (HttpWebResponse)await _HttpWebRequest.GetResponseAsync();

                if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    StreamReader _StreamReader = new StreamReader(_HttpWebResponse.GetResponseStream());
                    string? _Response = await _StreamReader.ReadToEndAsync();
                    ShipGetRatesResponse? _ShipmentRatesResponse = JsonConvert.DeserializeObject<ShipGetRatesResponse>(_Response);
                    return _ShipmentRatesResponse;
                }
                else
                {
                    ShipGetRatesResponse? _ShipmentRatesResponse = new ShipGetRatesResponse();
                    _ShipmentRatesResponse.status = false;
                    _ShipmentRatesResponse.message = "Failed to get the rates.";
                    return _ShipmentRatesResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ShipGetRates", _Exception);
                ShipGetRatesResponse? _ShipmentRatesResponse = new ShipGetRatesResponse();
                _ShipmentRatesResponse.status = false;
                _ShipmentRatesResponse.message = "Failed to get the rates.";
                return _ShipmentRatesResponse;
            }
        }

        public async Task<ShipValidateAddressResponse> ShipValidateAddress(string? Address, string? City, string? State, string? Country, string? ApiKey)
        {
            try
            {
                string RequestUrl = _AppConfig.ShiipUrl + "/geoaddress/" + Address + ", " + City + ", " + State + ", " + Country;

                var _HttpWebRequest = await CreateWebrequest("GET", RequestUrl,  null, ApiKey);

                HttpWebResponse? _HttpWebResponse = (HttpWebResponse)await _HttpWebRequest.GetResponseAsync();

                if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    StreamReader _StreamReader = new StreamReader(_HttpWebResponse.GetResponseStream());
                    string? _Response = await _StreamReader.ReadToEndAsync();
                    ShipValidateAddressResponse? _ValidateAddressResponse = JsonConvert.DeserializeObject<ShipValidateAddressResponse>(_Response);
                    return _ValidateAddressResponse;
                }
                else
                {
                    ShipValidateAddressResponse? _ValidateAddressResponse = new ShipValidateAddressResponse();
                    _ValidateAddressResponse.status = false;
                    _ValidateAddressResponse.message = "Not able to validate the address provided.";
                    return _ValidateAddressResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ShipValidateAddress", _Exception);
                ShipValidateAddressResponse? _ValidateAddressResponse = new ShipValidateAddressResponse();
                _ValidateAddressResponse.status = false;
                _ValidateAddressResponse.message = "Not able to validate the address provided.";
                return _ValidateAddressResponse;
            }
        }

        public async Task<ShipBookShipmentResponse> ShipBookShipment(ShipBookShipmentRequest _Request, string? ApiKey)
        {
            try
            {
                string RequestUrl = _AppConfig.ShiipUrl + "/shipments";
                string RequestBody = JsonConvert.SerializeObject(_Request);
                byte[] bytes = Encoding.ASCII.GetBytes(RequestBody);

                var _HttpWebRequest = await CreateWebrequest("POST", RequestUrl, bytes, ApiKey);
                HttpWebResponse? _HttpWebResponse = (HttpWebResponse)await _HttpWebRequest.GetResponseAsync();

                if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    StreamReader _StreamReader = new StreamReader(_HttpWebResponse.GetResponseStream());
                    string? _Response = await _StreamReader.ReadToEndAsync();
                    ShipBookShipmentResponse? _BookShipmentResponse = JsonConvert.DeserializeObject<ShipBookShipmentResponse>(_Response);
                    return _BookShipmentResponse;
                }
                else
                {
                    ShipBookShipmentResponse? _BookShipmentResponse = new ShipBookShipmentResponse();
                    _BookShipmentResponse.status = false;
                    _BookShipmentResponse.message = "Failed to book the shipment.";
                    return _BookShipmentResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ShipBookShipment", _Exception);
                ShipBookShipmentResponse? _BookShipmentResponse = new ShipBookShipmentResponse();
                _BookShipmentResponse.status = false;
                _BookShipmentResponse.message = "Failed to book the shipment.";
                return _BookShipmentResponse;
            }
        }

        public async Task<ShipTrackShipmentResponse> ShipTrackShipment(string? ShipmentReference, string? ApiKey)
        {
            try
            {
                string RequestUrl = _AppConfig.ShiipUrl + "/shipments/track" + "?reference=" + ShipmentReference;

                var _HttpWebRequest = await CreateWebrequest("GET", RequestUrl, null, ApiKey);
                HttpWebResponse? _HttpWebResponse = (HttpWebResponse)await _HttpWebRequest.GetResponseAsync();

                if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    StreamReader _StreamReader = new StreamReader(_HttpWebResponse.GetResponseStream());
                    string? _Response = await _StreamReader.ReadToEndAsync();
                    ShipTrackShipmentResponse? _TrackShipmentResponse = JsonConvert.DeserializeObject<ShipTrackShipmentResponse>(_Response);
                    return _TrackShipmentResponse;
                }
                else
                {
                    ShipTrackShipmentResponse? _TrackShipmentResponse = new ShipTrackShipmentResponse();
                    _TrackShipmentResponse.status = false;
                    _TrackShipmentResponse.message = "Failed to track the shipment.";
                    return _TrackShipmentResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ShipTrackShipment", _Exception);
                ShipTrackShipmentResponse? _TrackShipmentResponse = new ShipTrackShipmentResponse();
                _TrackShipmentResponse.status = false;
                _TrackShipmentResponse.message = "Failed to track the shipment.";
                return _TrackShipmentResponse;
            }
        }

        public async Task<ShipAssignShipmentResponse> ShipAssignShipment(ShipAssignShipmentRequest _Request, string? ApiKey)
        {
            try
            {
                string RequestUrl = _AppConfig.ShiipUrl + "/shipments/externalAssignment/" + _Request.shipment_id;
                string RequestBody = JsonConvert.SerializeObject(_Request);
                byte[] bytes = Encoding.ASCII.GetBytes(RequestBody);

                var _HttpWebRequest = await CreateWebrequest("POST", RequestUrl, bytes, ApiKey);
                HttpWebResponse? _HttpWebResponse = (HttpWebResponse)await _HttpWebRequest.GetResponseAsync();

                if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    StreamReader _StreamReader = new StreamReader(_HttpWebResponse.GetResponseStream());
                    string? _Response = await _StreamReader.ReadToEndAsync();
                    ShipAssignShipmentResponse? _AssignShipmentResponse = JsonConvert.DeserializeObject<ShipAssignShipmentResponse>(_Response);
                    return _AssignShipmentResponse;
                }
                else
                {
                    ShipAssignShipmentResponse? _AssignShipmentResponse = new ShipAssignShipmentResponse();
                    _AssignShipmentResponse.status = false;
                    _AssignShipmentResponse.message = "Failed to assign the shipment.";
                    return _AssignShipmentResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ShipAssignShipment", _Exception);
                ShipAssignShipmentResponse? _AssignShipmentResponse = new ShipAssignShipmentResponse();
                _AssignShipmentResponse.status = false;
                _AssignShipmentResponse.message = _Exception.Message;
                return _AssignShipmentResponse;
            }
        }

        public async Task<ShipCancelShipmentResponse> ShipCancelShipment(string? ShipmentId, string? ApiKey)
        {
            try
            {
                string RequestUrl = _AppConfig.ShiipUrl + "/shipments/externalCancelOrder/" + ShipmentId;

                var _HttpWebRequest = await CreateWebrequest("GET", RequestUrl, null, ApiKey);
                HttpWebResponse? _HttpWebResponse = (HttpWebResponse)await _HttpWebRequest.GetResponseAsync();

                if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    StreamReader _StreamReader = new StreamReader(_HttpWebResponse.GetResponseStream());
                    string? _Response = await _StreamReader.ReadToEndAsync();
                    ShipCancelShipmentResponse? _CancelShipmentResponse = JsonConvert.DeserializeObject<ShipCancelShipmentResponse>(_Response);
                    return _CancelShipmentResponse;
                }
                else
                {
                    ShipCancelShipmentResponse? _CancelShipmentResponse = new ShipCancelShipmentResponse();
                    _CancelShipmentResponse.status = false;
                    _CancelShipmentResponse.message = "Failed to cancel the shipment.";
                    return _CancelShipmentResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ShipCancelShipment", _Exception);
                ShipCancelShipmentResponse? _CancelShipmentResponse = new ShipCancelShipmentResponse();
                _CancelShipmentResponse.status = false;
                _CancelShipmentResponse.message = "Failed to cancel the shipment.";
                return _CancelShipmentResponse;
            }
        }
    }
}

