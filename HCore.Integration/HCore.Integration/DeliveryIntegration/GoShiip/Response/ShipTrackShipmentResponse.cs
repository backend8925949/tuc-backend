﻿using System;
using DocumentFormat.OpenXml.Spreadsheet;

namespace HCore.Integration.DeliveryIntegration.GoShiip.Response
{
	public class ShipTrackShipmentResponse
    {
        public Data? data { get; set; }
        public bool status { get; set; }
        public string? message { get; set; }

        public class Data
        {
            public bool status { get; set; }
            public string? message { get; set; }
            public Items? items { get; set; }
        }

        public class Dropoff
        {
            public int? id { get; set; }
            public int? state_id { get; set; }
            public int? country_id { get; set; }
            public string? name { get; set; }
            public string? state_code { get; set; }
            public string? country_code { get; set; }
            public string? latitude { get; set; }
            public string? longitude { get; set; }
            public int? flag { get; set; }
            public string? wikiDataId { get; set; }
        }

        public class Item
        {
            public string? pickup_date { get; set; }
            public string? category { get; set; }
            public string? name { get; set; }
            public string? quantity { get; set; }
            public int? weight_id { get; set; }
            public int? weight { get; set; }
            public double? height { get; set; }
            public double? width { get; set; }
            public double? length { get; set; }
            public string? amount { get; set; }
            public string? description { get; set; }
            public int? id { get; set; }
            public string? reference { get; set; }
            public int? user_id { get; set; }
            public string? carrier { get; set; }
            public string? dropoff_address { get; set; }
            public string? pickup_address { get; set; }
            public string? status { get; set; }
            public string? current_location { get; set; }
            public List<Item>? items { get; set; }
            public string? user_meta { get; set; }
            public string? meta { get; set; }
            public DateTime? created_at { get; set; }
            public DateTime? updated_at { get; set; }
            public string? processor { get; set; }
            public object? assigned_by { get; set; }
            public object? assigned_on { get; set; }
            public string? shipment_data { get; set; }
            public string? platform { get; set; }
            public object? platform_ref { get; set; }
            public object? company { get; set; }
            public Dropoff? dropoff { get; set; }
            public Pickup? pickup { get; set; }
        }

        public class Pickup
        {
            public int? id { get; set; }
            public int? state_id { get; set; }
            public int? country_id { get; set; }
            public string? name { get; set; }
            public string? state_code { get; set; }
            public string? country_code { get; set; }
            public string? latitude { get; set; }
            public string? longitude { get; set; }
            public int? flag { get; set; }
            public string? wikiDataId { get; set; }
        }

        public class Weight
        {
            public int? id { get; set; }
            public string? weight { get; set; }
        }


    }
}

