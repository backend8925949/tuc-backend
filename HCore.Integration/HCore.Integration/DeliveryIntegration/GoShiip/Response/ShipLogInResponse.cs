﻿using System;
namespace HCore.Integration.DeliveryIntegration.GoShiip.Response
{
	public class ShipLogInResponse
    {
        public Data? data { get; set; }
        public bool? status { get; set; }
        public string? message { get; set; }

        public class Data
        {
            public User? user { get; set; }
            public string? token { get; set; }
        }

        public class User
        {
            public int? id { get; set; }
            public long? delivery_company_id { get; set; }
            public string? firstname { get; set; }
            public string? lastname { get; set; }
            public string? email { get; set; }
            public string? username { get; set; }
            public string? role { get; set; }
            public string? phone { get; set; }
            public string? country_code { get; set; }
            public object? email_token { get; set; }
            public int? verified { get; set; }
            public int? email_is_verified { get; set; }
            public object? email_verified_at { get; set; }
            public DateTime? created_at { get; set; }
            public DateTime? updated_at { get; set; }
            public string? balance { get; set; }
            public object? device_token { get; set; }
        }
    }
}

