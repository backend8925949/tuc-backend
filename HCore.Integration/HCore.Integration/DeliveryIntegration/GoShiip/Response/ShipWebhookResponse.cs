﻿using System;
namespace HCore.Integration.DeliveryIntegration.GoShiip.Response
{
	public class ShipWebhookResponse
    {
        public Shipment? shipment { get; set; }

        public class Data
        {
            public string? destination { get; set; }
            public string? origin { get; set; }
            public string? reference { get; set; }
            public string? status { get; set; }
            public string? extra { get; set; }
        }

        public class Shipment
        {
            public string? secretKey { get; set; }
            public Data? data { get; set; }
            public string? carrier { get; set; }
        }
    }
}

