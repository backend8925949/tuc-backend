﻿using System;
using Newtonsoft.Json;

namespace HCore.Integration.DeliveryIntegration.GoShiip.Response
{
	public class ShipGetRatesResponse
    {
        public Data? data { get; set; }
        public bool? status { get; set; }
        public string? message { get; set; }

        public class Courier
        {
            public string? id { get; set; }
            public string? name { get; set; }
            public string? icon { get; set; }
        }

        public class Data
        {
            public List<Rate>? rates { get; set; }
            public string? get_rates_key { get; set; }
            public string? kwik_key { get; set; }
            public string? redis_key { get; set; }
            public List<object>? Rates { get; set; }
        }

        public class Rate
        {
            public Courier? courier { get; set; }
            public double? amount { get; set; }
            public int? service_charge { get; set; }
            public double? actual_amount { get; set; }
            public string? delivery_note { get; set; }
            public string? id { get; set; }
            public string? type { get; set; }
            public bool? status { get; set; }
            public string? currency { get; set; }
            public string? estimated_days { get; set; }
            
            public object? message1 { get; set; }
            public object? message2 { get; set; }
        }
    }
}

