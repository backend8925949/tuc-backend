﻿using System;
namespace HCore.Integration.DeliveryIntegration.GoShiip.Response
{
	public class ShipBookShipmentResponse
    {
        public Data? data { get; set; }
        public bool? status { get; set; }
        public string? message { get; set; }

        public class Data
        {
            public int? shipmentId { get; set; }
            public int? code { get; set; }
            public bool? status { get; set; }
            public string? message { get; set; }
        }
    }
}

