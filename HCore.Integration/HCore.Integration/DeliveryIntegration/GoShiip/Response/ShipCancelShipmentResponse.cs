﻿using System;
namespace HCore.Integration.DeliveryIntegration.GoShiip.Response
{
	public class ShipCancelShipmentResponse
    {
        public Data? data { get; set; }
        public bool? status { get; set; }
        public string? message { get; set; }

        public class Data
        {
            public bool? status { get; set; }
            public string? message { get; set; }
        }
    }
}

