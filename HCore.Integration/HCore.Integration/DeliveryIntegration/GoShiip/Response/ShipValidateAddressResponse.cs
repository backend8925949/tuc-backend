﻿using System;
namespace HCore.Integration.DeliveryIntegration.GoShiip.Response
{
	public class ShipValidateAddressResponse
    {
        public bool? status { get; set; }
        public string? message { get; set; }
        public Data? data { get; set; }

        public class Data
        {
            public string? lat { get; set; }
            public string? lng { get; set; }
            public string? zipCode { get; set; }
            public string? shortAddr { get; set; }
            public string? country { get; set; }
            public string? state { get; set; }
            public string? city { get; set; }
            public string? cityId { get; set; }
            public string? wikiDataId { get; set; }
            public string? state_id { get; set; }
            public string? country_id { get; set; }
            public string? state_code { get; set; }
            public string? country_code { get; set; }
        }
    }
}

