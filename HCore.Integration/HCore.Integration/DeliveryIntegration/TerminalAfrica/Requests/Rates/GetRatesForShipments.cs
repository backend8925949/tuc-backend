//==================================================================================
// FileName: GetRatesForShipments.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
namespace Delivery.Object.Requests.Rates
{
    public class GetRatesForShipmentsRequest
    {
        public string? currency { get; set; }
        public string? delivery_address { get; set; }
        public string? parcel_id { get; set; }
        public string? pickup_address { get; set; }
        public string? shipment_id { get; set; }

        public GetRatesForShipmentsRequest(string currency, string? delivery_address, string? parcel_id, string? pickup_address, string? shipment_id)//SH-08900130969
        {
            this.currency = currency;
            this.delivery_address = delivery_address;
            this.parcel_id = parcel_id;
            this.pickup_address = pickup_address;
            this.shipment_id = shipment_id;
        }
    }
    public class GetRatesRequest
    {
        public string? parcel_id { get; set; }
        public string? fromaddress_id { get; set; }
        public string? toaddress_id { get; set; }
        public string? shipment_id { get; set; }

        public GetRatesRequest(string parcel_id, string? shipment_id, string? fromaddress_id, string? toaddress_id)//SH-08900130969
        {
            this.parcel_id = parcel_id;
            this.shipment_id = shipment_id;
            this.fromaddress_id = fromaddress_id;
            this.toaddress_id = toaddress_id;
        }
    }
}

