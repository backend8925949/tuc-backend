//==================================================================================
// FileName: GetRate.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Delivery.Object.Requests.Rates
{
	public class GetRateRequest
	{
        public GetRateRequest(string rate_id)
        {
            this.rate_id = rate_id;
        }

        public string? rate_id { get; set; }
	}
}

