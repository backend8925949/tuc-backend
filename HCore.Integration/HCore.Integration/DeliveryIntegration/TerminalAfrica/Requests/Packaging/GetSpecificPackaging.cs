//==================================================================================
// FileName: GetSpecificPackaging.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Delivery.Object.Requests.Packaging
{
	public class GetSpecificPackagingRequest
	{
        public GetSpecificPackagingRequest(string packaging_id)
        {
            this.packaging_id = packaging_id;
        }

        public string? packaging_id { get; set; }
		
	}
}

