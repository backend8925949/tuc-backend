//==================================================================================
// FileName: CreatePackaging.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
namespace Delivery.Object.Requests.Packaging
{
    public class CreatePackagingRequest
    {
        public CreatePackagingRequest(double? height, double? length, string? name, string? size_unit, string? type, double? width, double? weight, string? weight_unit)
        {
            this.height = height;
            this.length = length;
            this.name = name;
            this.size_unit = size_unit;
            this.type = type;
            this.width = width;
            this.weight = weight;
            this.weight_unit = weight_unit;
        }

        public double? height { get; set; }
        public double? length { get; set; }
        public string? name { get; set; }
        public string? size_unit { get; set; }
        public string? type { get; set; }
        public double? width { get; set; }
        public double? weight { get; set; }
        public string? weight_unit { get; set; }

    }
}

