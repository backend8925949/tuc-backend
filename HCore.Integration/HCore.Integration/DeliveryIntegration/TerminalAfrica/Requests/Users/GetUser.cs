//==================================================================================
// FileName: GetUser.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Delivery.Object.Requests.Users
{
	public class GetUserRequest
	{
        public GetUserRequest(string user_id)
        {
            this.user_id = user_id;
        }

        public string? user_id { get; set; }
	}
}

