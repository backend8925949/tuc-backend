//==================================================================================
// FileName: Get_TrackShipment.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Delivery.Object.Requests.Shipments
{
	public class Get_TrackShipment
	{
		public string? shipment_id { get; set; }

        public Get_TrackShipment(string shipment_id)
        {
            this.shipment_id = shipment_id;
        }
    }
}

