//==================================================================================
// FileName: CreateShipment.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
namespace Delivery.Object.Requests.Shipments
{
    public class CreateShipmentRequest
    {
        public string? address_from { get; set; }
        public string? address_to { get; set; }
        public string? address_return { get; set; }
        public string? meatdata { get; set; }
        public string? parcel { get; set; }

        public CreateShipmentRequest(string address_from, string? address_to, string? address_return, string? meatdata, string? parcel)
        {
            this.address_from = address_from;
            this.address_to = address_to;
            this.address_return = address_return;
            this.meatdata = meatdata;
            this.parcel = parcel;
        }
    }
}

