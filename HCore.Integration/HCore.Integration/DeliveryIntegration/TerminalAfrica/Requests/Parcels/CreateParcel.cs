//==================================================================================
// FileName: CreateParcel.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;

namespace Delivery.Object.Requests.Parcels
{
    public class CreateParcelRequest
    {
        public CreateParcelRequest(string description, string? packaging, string? weight_unit, List<ParcelItems> items, ParcelMetaData metadata)
        {
            this.description = description;
            this.packaging = packaging;
            this.weight_unit = weight_unit;
            this.items = items;
            this.metadata = metadata;
        }

        public string? description { get; set; }
        public string? packaging { get; set; }
        public string? weight_unit { get; set; }
        public List<ParcelItems> items { get; set; }
        public ParcelMetaData metadata { get; set; }
    }

    public class ParcelMetaData
    {

    }

    public class ParcelItems
    {
        public ParcelItems(string description, string? name, string? currency, long? value, long? quantity, double? weight)
        {
            this.description = description;
            this.name = name;
            this.currency = currency;
            this.value = value;
            this.quantity = quantity;
            this.weight = weight;
        }

        public ParcelItems()
        {

        }

        public string? description { get; set; }
        public string? name { get; set; }
        public string? currency { get; set; }
        public long? value { get; set; }
        public long? quantity { get; set; }
        public double? weight { get; set; }
    }
}

