//==================================================================================
// FileName: GetParcel.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Delivery.Object.Requests.Parcels
{
	public class GetParcelRequest
	{
        public GetParcelRequest(string parcel_id)
        {
            this.parcel_id = parcel_id;
        }

        public string? parcel_id { get; set; }
		
	}
}

