//==================================================================================
// FileName: GetTransactions.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
namespace Delivery.Object.Requests.Treansactions
{
    public class GetTransactionsRequest
    {
        public long perPage { get; set; }
        public string? wallet { get; set; }
        public long page { get; set; }

        public GetTransactionsRequest(long perPage, string? wallet, long page)
        {
            this.perPage = perPage;
            this.wallet = wallet;
            this.page = page;
        }
    }
}

