//==================================================================================
// FileName: GetTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Delivery.Object.Requests.Treansactions
{
	public class GetTransactionRequest
	{
		public string? id { get; set; }

        public GetTransactionRequest(string id)
        {
            this.id = id;
        }
    }
}

