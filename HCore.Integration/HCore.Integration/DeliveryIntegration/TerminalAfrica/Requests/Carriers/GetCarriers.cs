//==================================================================================
// FileName: GetCarriers.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
namespace Delivery.Object.Requests.Carriers
{
    public class GetCarriersRequest
    {
        public bool active { get; set; }
        public string? type { get; set; }
        public long perPage { get; set; }

        public GetCarriersRequest(bool active, string? type, long perPage)
        {
            this.active = active;
            this.type = type;
            this.perPage = perPage;
        }

        public GetCarriersRequest()
        {

        }
    }
}

