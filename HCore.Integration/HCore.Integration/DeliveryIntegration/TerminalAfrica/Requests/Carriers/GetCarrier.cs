//==================================================================================
// FileName: GetCarrier.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Delivery.Object.Requests.Carriers
{
	public class GetCarrierRequest
	{
        public GetCarrierRequest(string carrier_id)
        {
            this.carrier_id = carrier_id;
        }

        public string? carrier_id { get; set; }
		
	}
}

