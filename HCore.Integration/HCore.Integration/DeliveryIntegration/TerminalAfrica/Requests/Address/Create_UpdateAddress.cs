//==================================================================================
// FileName: Create_UpdateAddress.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
namespace Delivery.Object.Requests
{
    public class Create_UpdateAddress
    {
        public string? city { get; set; }
        public string? country { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public bool is_residential { get; set; }
        public string? last_name { get; set; }
        public string? line1 { get; set; }
        public string? line2 { get; set; }
        public Metadata metadata { get; set; }
        public string? name { get; set; }
        public string? phone { get; set; }
        public string? state { get; set; }
        public string? zip { get; set; }

        public Create_UpdateAddress(string city, string? country, string? email, string? first_name, bool is_residential, string? last_name, string? line1, string? line2, Metadata metadata, string? name, string? phone, string? state, string? zip)
        {
            this.city = city;
            this.country = country;
            this.email = email;
            this.first_name = first_name;
            this.is_residential = is_residential;
            this.last_name = last_name;
            this.line1 = line1;
            this.line2 = line2;
            this.metadata = metadata;
            this.name = name;
            this.phone = phone;
            this.state = state;
            this.zip = zip;
        }
    }

    public class Metadata
    {

    }
}

