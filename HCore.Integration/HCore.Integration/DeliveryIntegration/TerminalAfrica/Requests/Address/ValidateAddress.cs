//==================================================================================
// FileName: ValidateAddress.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
namespace Delivery.Object.Requests.Address
{
    public class ValidateAddressRequest
    {
        public ValidateAddressRequest(string city, string? country, string? line1, string? line2, string? state, string? zip)
        {
            this.city = city;
            this.country = country;
            this.line1 = line1;
            this.line2 = line2;
            this.state = state;
            this.zip = zip;
        }

        public string? city { get; set; }
        public string? country { get; set; }
        public string? line1 { get; set; }
        public string? line2 { get; set; }
        public string? state { get; set; }
        public string? zip { get; set; }
    }
}

