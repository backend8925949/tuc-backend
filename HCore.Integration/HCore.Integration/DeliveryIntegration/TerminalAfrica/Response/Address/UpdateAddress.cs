//==================================================================================
// FileName: UpdateAddress.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Delivery.Object.Response.Address
{
	public class UpdateAddress
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public UpdateAddressData data { get; set; }


        public UpdateAddress()
		{
		}
    }

    public class UpdateAddressData
    {
        public string? _id { get; set; }
        public string? user { get; set; }
        public string? city { get; set; }
        public Coordinates coordinates { get; set; }
        public string? country { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public bool is_residential { get; set; }
        public string? last_name { get; set; }
        public string? line1 { get; set; }
        public string? line2 { get; set; }
        public string? phone { get; set; }
        public string? state { get; set; }
        public string? zip { get; set; }
        public string? address_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public string? id { get; set; }
    }

}

