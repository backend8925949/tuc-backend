//==================================================================================
// FileName: GetAddresses.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Delivery.Object.Response.Address
{
    public class GetAddresses
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public GetAddressesData data { get; set; }

        public GetAddresses()
        {
        }
    }

    public class GetAddressesData
    {
        public List<Address> addresses { get; set; }
        public Pagination pagination { get; set; }
    }

    public class Address
    {
        public string? user { get; set; }
        public string? city { get; set; }
        public Coordinates coordinates { get; set; }
        public string? country { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public bool is_residential { get; set; }
        public string? last_name { get; set; }
        public string? line1 { get; set; }
        public string? line2 { get; set; }
        public string? phone { get; set; }
        public string? state { get; set; }
        public string? zip { get; set; }
        public string? address_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public GetAddressesMetadata metadata { get; set; }
    }

    public class Pagination
    {
        public int page { get; set; }
        public int perPage { get; set; }
        public object prevPage { get; set; }
        public object nextPage { get; set; }
        public int currentPage { get; set; }
        public int total { get; set; }
        public int pageCount { get; set; }
        public int pagingCounter { get; set; }
        public bool hasPrevPage { get; set; }
        public bool hasNextPage { get; set; }
    }

    public class GetAddressesMetadata
    {

    }
}

