//==================================================================================
// FileName: ValidateAddress.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Delivery.Object.Response.Address
{
	public class ValidateAddress
    {
            public bool status { get; set; }
            public string? message { get; set; }
            public ValidateAddressData data { get; set; }



        public ValidateAddress()
		{
		}
	}

    public class ValidateAddressData
    {
        public string? city { get; set; }
        public string? country { get; set; }
        public string? line1 { get; set; }
        public string? line2 { get; set; }
        public string? state { get; set; }
        public string? zip { get; set; }
        public string? user { get; set; }
        public ValidationResults validation_results { get; set; }
    }

    public class ValidationResults
    {
    }
}

