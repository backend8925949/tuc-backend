//==================================================================================
// FileName: GetUseWalletBalance.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Delivery.Object.Response.Users
{
	public class GetUseWalletBalance
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public GetUseWalletBalanceData data { get; set; }

        public GetUseWalletBalance()
		{
		}
    }

    public class GetUseWalletBalanceData
    {
        public DateTime created_at { get; set; }
        public bool active { get; set; }
        public double amount { get; set; }
        public string? currency { get; set; }
        public string? name { get; set; }
        public string? user { get; set; }
        public DateTime updated_at { get; set; }
    }
}

