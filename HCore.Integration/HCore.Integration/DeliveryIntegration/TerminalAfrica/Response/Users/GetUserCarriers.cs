//==================================================================================
// FileName: GetUserCarriers.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Delivery.Object.Response.Users
{
	public class GetUserCarrierss
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public GetUserCarrierssData data { get; set; }

        public GetUserCarrierss()
		{
		}
    }

    public class GetUserCarrierssCarrier
    {
        public bool active { get; set; }
        public List<string> available_countries { get; set; }
        public GetUserCarrierssContact contact { get; set; }
        public bool domestic { get; set; }
        public bool international { get; set; }
        public string? logo { get; set; }
        public string? name { get; set; }
        public bool regional { get; set; }
        public bool requires_invoice { get; set; }
        public bool requires_waybill { get; set; }
        public string? slug { get; set; }
        public string? carrier_id { get; set; }
        public int __v { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }

    public class GetUserCarrierssContact
    {
        public string? email { get; set; }
        public string? phone { get; set; }
    }

    public class GetUserCarrierssData
    {
        public List<GetUserCarrierssCarrier> carriers { get; set; }
        public GetUserCarrierssPagination pagination { get; set; }
    }

    public class GetUserCarrierssPagination
    {
        public int page { get; set; }
        public int perPage { get; set; }
        public object prevPage { get; set; }
        public object nextPage { get; set; }
        public int currentPage { get; set; }
        public int total { get; set; }
        public int pageCount { get; set; }
        public int pagingCounter { get; set; }
        public bool hasPrevPage { get; set; }
        public bool hasNextPage { get; set; }
    }
}

