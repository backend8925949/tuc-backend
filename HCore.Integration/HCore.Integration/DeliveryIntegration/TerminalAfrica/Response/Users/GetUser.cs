//==================================================================================
// FileName: GetUser.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Delivery.Object.Response.Users
{
	public class GetUser
	{

        public class Root
        {
            public bool status { get; set; }
            public string? message { get; set; }
            public GetUserData data { get; set; }
        }


        public GetUser()
		{
		}
    }

    public class GetUserCarriers
    {
        public List<string> domestic { get; set; }
        public List<string> international { get; set; }
        public List<string> regional { get; set; }
    }

    public class GetUserData
    {
        public string? _id { get; set; }
        public GetUserCarriers carriers { get; set; }
        public string? company_name { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public string? last_name { get; set; }
        public GetUserMetadata metadata { get; set; }
        public string? phone { get; set; }
        public string? wallet { get; set; }
        public string? user_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
    }

    public class GetUserMetadata
    {
        public string? storeId { get; set; }
    }
}

