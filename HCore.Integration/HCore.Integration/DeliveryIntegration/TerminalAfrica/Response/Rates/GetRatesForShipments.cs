//==================================================================================
// FileName: GetRatesForShipments.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Delivery.Object.Response.Rates
{
	public class GetRatesForShipments
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public List<Datum> data { get; set; }

        public GetRatesForShipments()
		{
		}
    }

    public class Datum
    {
        public double amount { get; set; }
        public List<object> breakdown { get; set; }
        public string? carrier_logo { get; set; }
        public string? carrier_name { get; set; }
        public string? carrier_rate_description { get; set; }
        public string? carrier_reference { get; set; }
        public string? carrier_slug { get; set; }
        public string? currency { get; set; }
        public string? delivery_address { get; set; }
        public double delivery_eta { get; set; }
        public DateTime delivery_date { get; set; }
        public string? delivery_time { get; set; }
        public string? id { get; set; }
        public double insurance_coverage { get; set; }
        public double insurance_fee { get; set; }
        public bool includes_insurance { get; set; }
        public Metadata metadata { get; set; }
        public string? parcel { get; set; }
        public string? pickup_address { get; set; }
        public double pickup_eta { get; set; }
        public string? pickup_time { get; set; }
        public string? rate_id { get; set; }
        public bool used { get; set; }
        public string? user { get; set; }
    }

    public class Metadata
    {
        public string? localProductCode { get; set; }
        public string? productCode { get; set; }
    }
}

