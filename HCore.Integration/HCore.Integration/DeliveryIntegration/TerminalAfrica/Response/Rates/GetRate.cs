//==================================================================================
// FileName: GetRate.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Delivery.Object.Response.Rates
{
	public class GetRate
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public GetRateData data { get; set; }

        public GetRate()
		{
		}
    }

    public class Carriers
    {
        public List<string> domestic { get; set; }
        public List<string> international { get; set; }
        public List<string> regional { get; set; }
    }

    public class GetRateCoordinates
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class GetRateUser
    {
        public string? _id { get; set; }
        public bool admin { get; set; }
        public Carriers carriers { get; set; }
        public string? company_name { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public string? last_name { get; set; }
        public GetRateMetadata metadata { get; set; }
        public string? phone { get; set; }
        public string? public_key { get; set; }
        public string? secret_key { get; set; }
        public string? wallet { get; set; }
        public string? user_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public string? country { get; set; }
        public string? name { get; set; }
        public string? id { get; set; }
    }

    public class GetRateData
    {
        public string? _id { get; set; }
        public double amount { get; set; }
        public List<object> breakdown { get; set; }
        public string? carrier_logo { get; set; }
        public string? carrier_name { get; set; }
        public string? carrier_rate_description { get; set; }
        public string? carrier_reference { get; set; }
        public string? carrier_slug { get; set; }
        public string? currency { get; set; }
        public GetRateDeliveryAddress delivery_address { get; set; }
        public DateTime delivery_date { get; set; }
        public int delivery_eta { get; set; }
        public string? delivery_time { get; set; }
        public string? id { get; set; }
        public int insurance_coverage { get; set; }
        public int insurance_fee { get; set; }
        public bool includes_insurance { get; set; }
        public GetRateMetadata metadata { get; set; }
        public GetRateParcel parcel { get; set; }
        public GetRatePickupAddress pickup_address { get; set; }
        public int pickup_eta { get; set; }
        public string? pickup_time { get; set; }
        public string? rate_id { get; set; }
        public bool used { get; set; }
        public GetRateUser user { get; set; }
        public int __v { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }

    public class GetRateDeliveryAddress
    {
        public string? user { get; set; }
        public string? city { get; set; }
        public GetRateCoordinates coordinates { get; set; }
        public string? country { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public bool is_residential { get; set; }
        public string? last_name { get; set; }
        public string? line1 { get; set; }
        public string? line2 { get; set; }
        public string? phone { get; set; }
        public string? state { get; set; }
        public string? zip { get; set; }
        public string? address_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public GetRateMetadata metadata { get; set; }
    }

    public class GetRateItem
    {
        public string? description { get; set; }
        public string? name { get; set; }
        public string? currency { get; set; }
        public int value { get; set; }
        public int quantity { get; set; }
        public double weight { get; set; }
    }

    public class GetRateMetadata
    {
        public string? localProductCode { get; set; }
        public string? productCode { get; set; }
        public string? storeId { get; set; }
    }

    public class GetRatePackaging
    {
        public string? _id { get; set; }
        public string? user { get; set; }
        public int height { get; set; }
        public int length { get; set; }
        public string? name { get; set; }
        public string? size_unit { get; set; }
        public string? type { get; set; }
        public double weight { get; set; }
        public string? weight_unit { get; set; }
        public int width { get; set; }
        public string? packaging_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public string? id { get; set; }
    }

    public class GetRateParcel
    {
        public string? _id { get; set; }
        public string? description { get; set; }
        public List<GetRateItem> items { get; set; }
        public GetRatePackaging packaging { get; set; }
        public double total_weight { get; set; }
        public string? user { get; set; }
        public string? weight_unit { get; set; }
        public string? parcel_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
    }

    public class GetRatePickupAddress
    {
        public string? user { get; set; }
        public string? city { get; set; }
        public GetRateCoordinates coordinates { get; set; }
        public string? country { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public bool is_residential { get; set; }
        public string? last_name { get; set; }
        public string? line1 { get; set; }
        public string? line2 { get; set; }
        public string? phone { get; set; }
        public string? state { get; set; }
        public string? zip { get; set; }
        public string? address_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
    }
}

