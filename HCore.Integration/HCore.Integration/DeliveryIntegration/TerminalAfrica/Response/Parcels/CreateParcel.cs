//==================================================================================
// FileName: CreateParcel.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Delivery.Object.Response.Parcels
{
	public class CreateParcel
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public CreateParcelData data { get; set; }

        public CreateParcel()
		{
		}
    }

    public class CreateParcelData
    {
        public string? description { get; set; }
        public List<Item> items { get; set; }
        public string? packaging { get; set; }
        public double total_weight { get; set; }
        public string? user { get; set; }
        public string? weight_unit { get; set; }
        public string? _id { get; set; }
        public string? parcel_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public string? id { get; set; }
    }

    public class Item
    {
        public string? description { get; set; }
        public string? name { get; set; }
        public string? currency { get; set; }
        public int value { get; set; }
        public int quantity { get; set; }
        public double weight { get; set; }
    }
}

