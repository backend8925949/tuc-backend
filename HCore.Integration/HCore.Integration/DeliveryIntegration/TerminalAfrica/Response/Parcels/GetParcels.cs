//==================================================================================
// FileName: GetParcels.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Delivery.Object.Response.Parcels
{
	public class GetParcels
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public GetParcelsData data { get; set; }

        public GetParcels()
		{
		}
    }

    public class GetParcelsData
    {
        public List<GetParcelsParcel> parcels { get; set; }
        public GetParcelsPagination pagination { get; set; }
    }

    public class GetParcelsItem
    {
        public string? description { get; set; }
        public string? name { get; set; }
        public string? currency { get; set; }
        public int value { get; set; }
        public int quantity { get; set; }
        public double weight { get; set; }
    }

    public class GetParcelsPagination
    {
        public int page { get; set; }
        public int perPage { get; set; }
        public object prevPage { get; set; }
        public object nextPage { get; set; }
        public int currentPage { get; set; }
        public int total { get; set; }
        public int pageCount { get; set; }
        public int pagingCounter { get; set; }
        public bool hasPrevPage { get; set; }
        public bool hasNextPage { get; set; }
    }

    public class GetParcelsParcel
    {
        public string? _id { get; set; }
        public string? description { get; set; }
        public List<Item> items { get; set; }
        public string? packaging { get; set; }
        public double total_weight { get; set; }
        public string? user { get; set; }
        public string? weight_unit { get; set; }
        public string? parcel_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public string? id { get; set; }
    }
}

