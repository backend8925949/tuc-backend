//==================================================================================
// FileName: GetPackaging.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Delivery.Object.Response.Packaging
{
	public class GetPackaging
    {

        public class Root
        {
            public bool status { get; set; }
            public string? message { get; set; }
            public GetPackagingData data { get; set; }
        }


        public GetPackaging()
		{
		}
    }

    public class GetPackagingData
    {
        public List<GetPackagingPackaging> packaging { get; set; }
        public GetPackagingPagination pagination { get; set; }
    }

    public class GetPackagingPackaging
    {
        public string? _id { get; set; }
        public string? user { get; set; }
        public int height { get; set; }
        public int length { get; set; }
        public string? name { get; set; }
        public string? size_unit { get; set; }
        public string? type { get; set; }
        public double weight { get; set; }
        public string? weight_unit { get; set; }
        public int width { get; set; }
        public string? packaging_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public string? id { get; set; }
    }

    public class GetPackagingPagination
    {
        public int page { get; set; }
        public int perPage { get; set; }
        public object prevPage { get; set; }
        public object nextPage { get; set; }
        public int currentPage { get; set; }
        public int total { get; set; }
        public int pageCount { get; set; }
        public int pagingCounter { get; set; }
        public bool hasPrevPage { get; set; }
        public bool hasNextPage { get; set; }
    }
}

