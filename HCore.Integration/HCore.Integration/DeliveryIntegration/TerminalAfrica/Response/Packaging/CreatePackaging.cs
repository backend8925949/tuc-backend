//==================================================================================
// FileName: CreatePackaging.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Delivery.Object.Response.Packaging
{
	public class CreatePackaging
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public CreatePackagingData data { get; set; }

        public CreatePackaging()
		{
		}
	}

    public class CreatePackagingData
    {
        public string? user { get; set; }
        public double height { get; set; }
        public double length { get; set; }
        public string? name { get; set; }
        public string? size_unit { get; set; }
        public string? type { get; set; }
        public double weight { get; set; }
        public string? weight_unit { get; set; }
        public double width { get; set; }
        public string? _id { get; set; }
        public string? packaging_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public string? id { get; set; }
    }
}

