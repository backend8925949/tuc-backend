//==================================================================================
// FileName: GetCarrier.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Delivery.Object.Response.Carriers
{
    public class GetCarrier
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public GetCarrierData data { get; set; }

        public GetCarrier()
        {
        }
    }

    public class GetCarrierData
    {
        public string? metadata { get; set; }
        public bool active { get; set; }
        public List<string> available_countries { get; set; }
        public Contact contact { get; set; }
        public bool domestic { get; set; }
        public bool international { get; set; }
        public string? logo { get; set; }
        public string? name { get; set; }
        public bool regional { get; set; }
        public bool requires_invoice { get; set; }
        public bool requires_waybill { get; set; }
        public string? slug { get; set; }
        public string? carrier_id { get; set; }
        public int __v { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public object id { get; set; }
    }

    public class Contact
    {
        public string? email { get; set; }
        public string? phone { get; set; }
    }
}

