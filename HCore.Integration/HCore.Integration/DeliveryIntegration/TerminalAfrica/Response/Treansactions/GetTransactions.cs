//==================================================================================
// FileName: GetTransactions.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Delivery.Object.Response.Treansactions
{
    public class GetTransactions
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public GetTransactionsData data { get; set; }
        public GetTransactions()
        {
        }
    }

    public class GetTransactionsData
    {
        public List<GetTransactionsTransaction> transactions { get; set; }
        public GetTransactionsPagination pagination { get; set; }
    }

    public class GetTransactionsPagination
    {
        public int page { get; set; }
        public int perPage { get; set; }
        public int prevPage { get; set; }
        public int nextPage { get; set; }
        public int currentPage { get; set; }
        public int total { get; set; }
        public int pageCount { get; set; }
        public bool hasPrevPage { get; set; }
        public bool hasNextPage { get; set; }
    }
    public class GetTransactionsTransaction
    {
        public double amount { get; set; }
        public string? currency { get; set; }
        public string? description { get; set; }
        public string? flow { get; set; }
        public string? platform { get; set; }
        public string? reference { get; set; }
        public bool reversed { get; set; }
        public string? shipment { get; set; }
        public string? transaction_id { get; set; }
        public string? wallet { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}