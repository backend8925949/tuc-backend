//==================================================================================
// FileName: GetTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace Delivery.Object.Response.Treansactions
{
    public class GetTransaction
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public GetTransactionData data { get; set; }
        public GetTransaction()
        {
        }
    }

    public class GetTransactionData
    {
        public double amount { get; set; }
        public string? currency { get; set; }
        public string? description { get; set; }
        public string? flow { get; set; }
        public string? id { get; set; }
        public string? platform { get; set; }
        public string? reference { get; set; }
        public bool reversed { get; set; }
        public string? shipment { get; set; }
        public string? transaction_id { get; set; }
        public string? wallet { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}

