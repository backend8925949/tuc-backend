//==================================================================================
// FileName: CancelShipment.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Delivery.Object.Response.Shipments
{
    public class CancelShipment
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public CancelShipmentData data { get; set; }

        public CancelShipment()
        {
        }
    }
    public class CancelShipmentData
    {
        public string? _id { get; set; }
        public string? address_to { get; set; }
        public string? address_from { get; set; }
        public string? address_return { get; set; }
        public string? parcel { get; set; }
        public DateTime pickup_date { get; set; }
        public string? status { get; set; }
        public string? user { get; set; }
        public List<CancelShipmentEvent> events { get; set; }
        public string? shipment_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public string? carrier { get; set; }
        public DateTime delivery_arranged { get; set; }
        public DateTime delivery_date { get; set; }
        public CancelShipmentExtras extras { get; set; }
        public string? rate { get; set; }
        public string? id { get; set; }
    }

    public class CancelShipmentEvent
    {
        public DateTime created_at { get; set; }
        public string? description { get; set; }
        public string? location { get; set; }
        public string? status { get; set; }
    }

    public class CancelShipmentExtras
    {
        public string? tracking_url { get; set; }
    }

}

