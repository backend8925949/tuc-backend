//==================================================================================
// FileName: CreateShipment.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Delivery.Object.Response.Shipments
{
    public class CreateShipment
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public CreateShipmentData data { get; set; }

        public CreateShipment()
        {
        }
    }
    #region
    //public class CreateShipmentAddressFrom
    //{
    //    public string? user { get; set; }
    //    public string? city { get; set; }
    //    public CreateShipmentCoordinates coordinates { get; set; }
    //    public string? country { get; set; }
    //    public string? email { get; set; }
    //    public string? first_name { get; set; }
    //    public bool is_residential { get; set; }
    //    public string? last_name { get; set; }
    //    public string? line1 { get; set; }
    //    public string? line2 { get; set; }
    //    public string? phone { get; set; }
    //    public string? state { get; set; }
    //    public string? zip { get; set; }
    //    public string? address_id { get; set; }
    //    public DateTime created_at { get; set; }
    //    public DateTime updated_at { get; set; }
    //    public int __v { get; set; }
    //    public object id { get; set; }
    //}

    //public class CreateShipmentAddressReturn
    //{
    //    public string? user { get; set; }
    //    public string? city { get; set; }
    //    public CreateShipmentCoordinates coordinates { get; set; }
    //    public string? country { get; set; }
    //    public string? email { get; set; }
    //    public string? first_name { get; set; }
    //    public bool is_residential { get; set; }
    //    public string? last_name { get; set; }
    //    public string? line1 { get; set; }
    //    public string? line2 { get; set; }
    //    public string? phone { get; set; }
    //    public string? state { get; set; }
    //    public string? zip { get; set; }
    //    public string? address_id { get; set; }
    //    public DateTime created_at { get; set; }
    //    public DateTime updated_at { get; set; }
    //    public int __v { get; set; }
    //    public object id { get; set; }
    //}

    //public class CreateShipmentAddressTo
    //{
    //    public string? user { get; set; }
    //    public string? city { get; set; }
    //    public CreateShipmentCoordinates coordinates { get; set; }
    //    public string? country { get; set; }
    //    public string? email { get; set; }
    //    public string? first_name { get; set; }
    //    public bool is_residential { get; set; }
    //    public string? last_name { get; set; }
    //    public string? line1 { get; set; }
    //    public string? line2 { get; set; }
    //    public string? phone { get; set; }
    //    public string? state { get; set; }
    //    public string? zip { get; set; }
    //    public string? address_id { get; set; }
    //    public DateTime created_at { get; set; }
    //    public DateTime updated_at { get; set; }
    //    public int __v { get; set; }
    //    public object id { get; set; }
    //}

    //public class CreateShipmentCarriers
    //{
    //    public List<string> domestic { get; set; }
    //    public List<string> international { get; set; }
    //    public List<string> regional { get; set; }
    //}

    //public class CreateShipmentCoordinates
    //{
    //    public double lat { get; set; }
    //    public double lng { get; set; }
    //}

    //public class CreateShipmentData
    //{
    //    public string? _id { get; set; }
    //    public CreateShipmentAddressTo address_to { get; set; }
    //    public CreateShipmentAddressFrom address_from { get; set; }
    //    public CreateShipmentAddressReturn address_return { get; set; }
    //    public CreateShipmentParcel parcel { get; set; }
    //    public object pickup_date { get; set; }
    //    public string? status { get; set; }
    //    public CreateShipmentUser user { get; set; }
    //    public List<object> events { get; set; }
    //    public string? shipment_id { get; set; }
    //    public DateTime created_at { get; set; }
    //    public DateTime updated_at { get; set; }
    //    public int __v { get; set; }
    //}

    //public class CreateShipmentItem
    //{
    //    public string? description { get; set; }
    //    public string? name { get; set; }
    //    public string? currency { get; set; }
    //    public int value { get; set; }
    //    public int quantity { get; set; }
    //    public double weight { get; set; }
    //}

    //public class CreateShipmentMetadata
    //{
    //    public string? storeId { get; set; }
    //}

    //public class CreateShipmentParcel
    //{
    //    public string? metadata { get; set; }
    //    public string? _id { get; set; }
    //    public string? description { get; set; }
    //    public List<CreateShipmentItem> items { get; set; }
    //    public string? packaging { get; set; }
    //    public double total_weight { get; set; }
    //    public string? user { get; set; }
    //    public string? weight_unit { get; set; }
    //    public string? parcel_id { get; set; }
    //    public DateTime created_at { get; set; }
    //    public DateTime updated_at { get; set; }
    //    public int __v { get; set; }
    //    public string? id { get; set; }
    //}

    //public class CreateShipmentUser
    //{
    //    public string? country { get; set; }
    //    public string? _id { get; set; }
    //    public bool admin { get; set; }
    //    public CreateShipmentCarriers carriers { get; set; }
    //    public string? company_name { get; set; }
    //    public string? email { get; set; }
    //    public string? first_name { get; set; }
    //    public string? last_name { get; set; }
    //    public CreateShipmentMetadata metadata { get; set; }
    //    public string? phone { get; set; }
    //    public string? public_key { get; set; }
    //    public string? secret_key { get; set; }
    //    public string? wallet { get; set; }
    //    public string? user_id { get; set; }
    //    public DateTime created_at { get; set; }
    //    public DateTime updated_at { get; set; }
    //    public int __v { get; set; }
    //    public string? name { get; set; }
    //    public string? id { get; set; }
    //}
    #endregion// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class CreateShipmentAddressFrom
    {
        public string? user { get; set; }
        public string? city { get; set; }
        public CreateShipmentCoordinates coordinates { get; set; }
        public string? country { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public bool is_residential { get; set; }
        public string? last_name { get; set; }
        public string? line1 { get; set; }
        public string? line2 { get; set; }
        public string? phone { get; set; }
        public string? place_id { get; set; }
        public string? state { get; set; }
        public string? zip { get; set; }
        public string? address_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
    }

    public class CreateShipmentAddressReturn
    {
        public string? user { get; set; }
        public string? city { get; set; }
        public CreateShipmentCoordinates coordinates { get; set; }
        public string? country { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public bool is_residential { get; set; }
        public string? last_name { get; set; }
        public string? line1 { get; set; }
        public string? line2 { get; set; }
        public string? phone { get; set; }
        public string? place_id { get; set; }
        public string? state { get; set; }
        public string? zip { get; set; }
        public string? address_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
    }

    public class CreateShipmentAddressTo
    {
        public string? user { get; set; }
        public string? city { get; set; }
        public CreateShipmentCoordinates coordinates { get; set; }
        public string? country { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public bool is_residential { get; set; }
        public string? last_name { get; set; }
        public string? line1 { get; set; }
        public string? line2 { get; set; }
        public string? phone { get; set; }
        public string? place_id { get; set; }
        public string? state { get; set; }
        public string? zip { get; set; }
        public string? address_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
    }

    public class CreateShipmentCarriers
    {
        public List<string> domestic { get; set; }
        public List<string> international { get; set; }
        public List<string> regional { get; set; }
    }

    public class CreateShipmentCoordinates
    {
        public double lat { get; set; }
        public double lng { get; set; }
        public string? place_id { get; set; }
    }

    public class CreateShipmentData
    {
        public string? _id { get; set; }
        public CreateShipmentAddressTo address_to { get; set; }
        public CreateShipmentAddressFrom address_from { get; set; }
        public CreateShipmentAddressReturn address_return { get; set; }
        public CreateShipmentParcel parcel { get; set; }
        public object pickup_date { get; set; }
        public string? status { get; set; }
        public CreateShipmentUser user { get; set; }
        public List<object> events { get; set; }
        public string? shipment_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
    }

    public class CreateShipmentItem
    {
        public string? description { get; set; }
        public string? name { get; set; }
        public string? currency { get; set; }
        public int value { get; set; }
        public int quantity { get; set; }
        public double? weight { get; set; }
    }

    public class CreateShipmentMetadata
    {
        public string? storeId { get; set; }
    }

    public class CreateShipmentPackaging
    {
        public string? _id { get; set; }
        public double? height { get; set; }
        public double? length { get; set; }
        public string? name { get; set; }
        public string? size_unit { get; set; }
        public string? type { get; set; }
        public string? user { get; set; }
        public double? weight { get; set; }
        public string? weight_unit { get; set; }
        public double? width { get; set; }
        public string? packaging_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public string? id { get; set; }
    }

    public class CreateShipmentParcel
    {
        public string? _id { get; set; }
        public string? description { get; set; }
        public List<CreateShipmentItem> items { get; set; }
        public CreateShipmentPackaging packaging { get; set; }
        public double? total_weight { get; set; }
        public string? user { get; set; }
        public string? weight_unit { get; set; }
        public string? parcel_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
    }

    public class CreateShipmentUser
    {
        public string? _id { get; set; }
        public bool admin { get; set; }
        public List<object> barred_countries { get; set; }
        public CreateShipmentCarriers carriers { get; set; }
        public string? company_name { get; set; }
        public string? country { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public string? last_name { get; set; }
        public CreateShipmentMetadata metadata { get; set; }
        public string? phone { get; set; }
        public string? plan { get; set; }
        public string? public_key { get; set; }
        public string? secret_key { get; set; }
        public string? wallet { get; set; }
        public string? user_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public string? name { get; set; }
        public string? id { get; set; }
    }


}

