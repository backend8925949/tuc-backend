//==================================================================================
// FileName: ArrangePick_up&DeliveryForShipment.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Delivery.Object.Response.Shipments
{
	public class ArrangePick_up_DeliveryForShipment
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public Data data { get; set; }

        public ArrangePick_up_DeliveryForShipment()
		{
		}
    }

    public class Account
    {
        public string? typeCode { get; set; }
        public string? number { get; set; }
    }

    public class BuyerDetails
    {
        public PostalAddress postalAddress { get; set; }
        public ContactInformation contactInformation { get; set; }
    }

    public class CarrierPickup
    {
        public string? plannedPickupDateAndTime { get; set; }
        public List<Account> accounts { get; set; }
        public CustomerDetails customerDetails { get; set; }
        public List<SpecialInstruction> specialInstructions { get; set; }
        public List<ShipmentDetail> shipmentDetails { get; set; }
    }

    public class Carriers
    {
        public List<string> domestic { get; set; }
        public List<string> international { get; set; }
        public List<string> regional { get; set; }
    }

    public class CarrierShipment
    {
        public string? plannedShippingDateAndTime { get; set; }
        public Pickup pickup { get; set; }
        public string? productCode { get; set; }
        public string? localProductCode { get; set; }
        public List<Account> accounts { get; set; }
        public List<object> valueAddedServices { get; set; }
        public List<CustomerReference> customerReferences { get; set; }
        public OutputImageProperties outputImageProperties { get; set; }
        public CustomerDetails customerDetails { get; set; }
        public Content content { get; set; }
        public bool requestOndemandDeliveryURL { get; set; }
        public List<ShipmentNotification> shipmentNotification { get; set; }
        public bool getOptionalInformation { get; set; }
        public OnDemandDelivery onDemandDelivery { get; set; }
    }

    public class ContactInformation
    {
        public string? companyName { get; set; }
        public string? fullName { get; set; }
        public string? email { get; set; }
        public string? phone { get; set; }
    }

    public class Content
    {
        public string? description { get; set; }
        public List<Package> packages { get; set; }
        public string? declaredValueCurrency { get; set; }
        public int declaredValue { get; set; }
        public string? incoterm { get; set; }
        public bool isCustomsDeclarable { get; set; }
        public string? unitOfMeasurement { get; set; }
    }

    public class Coordinates
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class CustomerDetails
    {
        public ShipperDetails shipperDetails { get; set; }
        public ReceiverDetails receiverDetails { get; set; }
        public BuyerDetails buyerDetails { get; set; }
    }

    public class CustomerReference
    {
        public string? value { get; set; }
        public string? typeCode { get; set; }
    }

    public class Data
    {
        public string? _id { get; set; }
        public string? address_to { get; set; }
        public string? address_from { get; set; }
        public string? address_return { get; set; }
        public string? parcel { get; set; }
        public DateTime pickup_date { get; set; }
        public string? status { get; set; }
        public string? user { get; set; }
        public List<Event> events { get; set; }
        public string? shipment_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public string? carrier { get; set; }
        public DateTime delivery_arranged { get; set; }
        public DateTime delivery_date { get; set; }
        public Extras extras { get; set; }
        public Metadata metadata { get; set; }
        public string? rate { get; set; }
        public string? id { get; set; }
    }

    public class DeliveryAddress
    {
        public string? user { get; set; }
        public string? city { get; set; }
        public Coordinates coordinates { get; set; }
        public string? country { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public bool is_residential { get; set; }
        public string? last_name { get; set; }
        public string? line1 { get; set; }
        public string? line2 { get; set; }
        public string? phone { get; set; }
        public string? state { get; set; }
        public string? zip { get; set; }
        public string? address_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
    }

    public class Dimensions
    {
        public int length { get; set; }
        public int height { get; set; }
        public int width { get; set; }
    }

    public class Document
    {
        public string? imageFormat { get; set; }
        public string? content { get; set; }
        public string? typeCode { get; set; }
        public string? format { get; set; }
        public string? url { get; set; }
    }

    public class Event
    {
        public DateTime created_at { get; set; }
        public string? description { get; set; }
        public string? location { get; set; }
        public string? status { get; set; }
    }

    public class Extras
    {
        public string? carrier_tracking_url { get; set; }
        public string? commercial_invoice_url { get; set; }
        public string? reference { get; set; }
        public ShippingLabel shipping_label { get; set; }
        public string? shipping_label_url { get; set; }
        public string? tracking_number { get; set; }
        public string? tracking_url { get; set; }
    }

    public class ImageOption
    {
        public string? templateName { get; set; }
        public string? typeCode { get; set; }
        public bool isRequested { get; set; }
    }

    public class Item
    {
        public string? description { get; set; }
        public string? name { get; set; }
        public string? currency { get; set; }
        public int value { get; set; }
        public int quantity { get; set; }
        public double weight { get; set; }
    }

    public class Metadata
    {
        public ShipmentPayload shipment_payload { get; set; }
        public CarrierShipment carrier_shipment { get; set; }
        public CarrierPickup carrier_pickup { get; set; }
        public SelectedRate selected_rate { get; set; }
        public string? localProductCode { get; set; }
        public string? productCode { get; set; }
        public string? storeId { get; set; }
    }

    public class OnDemandDelivery
    {
        public string? deliveryOption { get; set; }
        public string? location { get; set; }
        public string? specialInstructions { get; set; }
        public string? authorizerName { get; set; }
    }

    public class OutputImageProperties
    {
        public bool allDocumentsInOneImage { get; set; }
        public string? encodingFormat { get; set; }
        public List<ImageOption> imageOptions { get; set; }
    }

    public class Package
    {
        public int referenceNumber { get; set; }
        public string? trackingNumber { get; set; }
        public string? trackingUrl { get; set; }
        public int weight { get; set; }
        public Dimensions dimensions { get; set; }
    }

    public class Packaging
    {
        public string? _id { get; set; }
        public string? user { get; set; }
        public int height { get; set; }
        public int length { get; set; }
        public string? name { get; set; }
        public string? size_unit { get; set; }
        public string? type { get; set; }
        public double weight { get; set; }
        public string? weight_unit { get; set; }
        public int width { get; set; }
        public string? packaging_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public string? id { get; set; }
    }

    public class Parcel
    {
        public string? _id { get; set; }
        public string? description { get; set; }
        public List<Item> items { get; set; }
        public Packaging packaging { get; set; }
        public double total_weight { get; set; }
        public string? user { get; set; }
        public string? weight_unit { get; set; }
        public string? parcel_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
    }

    public class Pickup
    {
        public bool isRequested { get; set; }
        public List<SpecialInstruction> specialInstructions { get; set; }
        public PickupDetails pickupDetails { get; set; }
    }

    public class PickupAddress
    {
        public string? user { get; set; }
        public string? city { get; set; }
        public Coordinates coordinates { get; set; }
        public string? country { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public bool is_residential { get; set; }
        public string? last_name { get; set; }
        public string? line1 { get; set; }
        public string? line2 { get; set; }
        public string? phone { get; set; }
        public string? state { get; set; }
        public string? zip { get; set; }
        public string? address_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
    }

    public class PickupDetails
    {
        public PostalAddress postalAddress { get; set; }
        public ContactInformation contactInformation { get; set; }
    }

    public class PostalAddress
    {
        public string? postalCode { get; set; }
        public string? cityName { get; set; }
        public string? countryCode { get; set; }
        public string? addressLine1 { get; set; }
        public string? addressLine2 { get; set; }
    }

    public class ReceiverDetails
    {
        public PostalAddress postalAddress { get; set; }
        public ContactInformation contactInformation { get; set; }
    }

    public class SelectedRate
    {
        public string? _id { get; set; }
        public double amount { get; set; }
        public List<object> breakdown { get; set; }
        public string? carrier_logo { get; set; }
        public string? carrier_name { get; set; }
        public string? carrier_rate_description { get; set; }
        public string? carrier_reference { get; set; }
        public string? carrier_slug { get; set; }
        public string? currency { get; set; }
        public DeliveryAddress delivery_address { get; set; }
        public DateTime delivery_date { get; set; }
        public int delivery_eta { get; set; }
        public string? delivery_time { get; set; }
        public string? id { get; set; }
        public int insurance_coverage { get; set; }
        public double insurance_fee { get; set; }
        public bool includes_insurance { get; set; }
        public Metadata metadata { get; set; }
        public Parcel parcel { get; set; }
        public PickupAddress pickup_address { get; set; }
        public int pickup_eta { get; set; }
        public string? pickup_time { get; set; }
        public string? rate_id { get; set; }
        public bool used { get; set; }
        public User user { get; set; }
        public int __v { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }

    public class ShipmentDetail
    {
        public double volumetricWeight { get; set; }
        public string? billingCode { get; set; }
        public string? serviceContentCode { get; set; }
        public CustomerDetails customerDetails { get; set; }
        public string? productCode { get; set; }
        public bool isCustomsDeclarable { get; set; }
        public int declaredValue { get; set; }
        public string? declaredValueCurrency { get; set; }
        public List<Package> packages { get; set; }
        public string? unitOfMeasurement { get; set; }
        public object shipmentTrackingNumber { get; set; }
    }

    public class ShipmentNotification
    {
        public string? typeCode { get; set; }
        public string? receiverId { get; set; }
    }

    public class ShipmentPayload
    {
        public string? shipmentTrackingNumber { get; set; }
        public string? cancelPickupUrl { get; set; }
        public string? trackingUrl { get; set; }
        public string? dispatchConfirmationNumber { get; set; }
        public List<Package> packages { get; set; }
        public List<Document> documents { get; set; }
        public List<ShipmentDetail> shipmentDetails { get; set; }
        public List<string> warnings { get; set; }
    }

    public class ShipperDetails
    {
        public PostalAddress postalAddress { get; set; }
        public ContactInformation contactInformation { get; set; }
    }

    public class ShippingLabel
    {
        public string? imageFormat { get; set; }
        public string? content { get; set; }
        public string? typeCode { get; set; }
        public string? format { get; set; }
        public string? url { get; set; }
    }

    public class SpecialInstruction
    {
        public string? value { get; set; }
    }

    public class User
    {
        public string? _id { get; set; }
        public bool admin { get; set; }
        public Carriers carriers { get; set; }
        public string? company_name { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public string? last_name { get; set; }
        public Metadata metadata { get; set; }
        public string? phone { get; set; }
        public string? public_key { get; set; }
        public string? secret_key { get; set; }
        public string? wallet { get; set; }
        public string? user_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public string? country { get; set; }
        public string? name { get; set; }
        public string? id { get; set; }
    }
}

