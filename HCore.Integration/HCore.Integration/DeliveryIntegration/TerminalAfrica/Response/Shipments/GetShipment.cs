//==================================================================================
// FileName: GetShipment.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Delivery.Object.Response.Shipments
{
	public class GetShipment
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public GetShipmentData data { get; set; }

        public GetShipment()
		{
		}
    }

    public class GetShipmentAddressFrom
    {
        public string? user { get; set; }
        public string? city { get; set; }
        public GetShipmentCoordinates coordinates { get; set; }
        public string? country { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public bool is_residential { get; set; }
        public string? last_name { get; set; }
        public string? line1 { get; set; }
        public string? line2 { get; set; }
        public string? phone { get; set; }
        public string? state { get; set; }
        public string? zip { get; set; }
        public string? address_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
    }

    public class GetShipmentAddressReturn
    {
        public string? user { get; set; }
        public string? city { get; set; }
        public GetShipmentCoordinates coordinates { get; set; }
        public string? country { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public bool is_residential { get; set; }
        public string? last_name { get; set; }
        public string? line1 { get; set; }
        public string? line2 { get; set; }
        public string? phone { get; set; }
        public string? state { get; set; }
        public string? zip { get; set; }
        public string? address_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
    }

    public class GetShipmentAddressTo
    {
        public string? user { get; set; }
        public string? city { get; set; }
        public GetShipmentCoordinates coordinates { get; set; }
        public string? country { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public bool is_residential { get; set; }
        public string? last_name { get; set; }
        public string? line1 { get; set; }
        public string? line2 { get; set; }
        public string? phone { get; set; }
        public string? state { get; set; }
        public string? zip { get; set; }
        public string? address_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public GetShipmentMetadata metadata { get; set; }
        public string? place_id { get; set; }
    }

    public class GetShipmentCarrier
    {
        public string? metadata { get; set; }
        public bool active { get; set; }
        public List<string> available_countries { get; set; }
        public GetShipmentContact contact { get; set; }
        public bool domestic { get; set; }
        public bool international { get; set; }
        public string? logo { get; set; }
        public string? name { get; set; }
        public bool regional { get; set; }
        public bool requires_invoice { get; set; }
        public bool requires_waybill { get; set; }
        public string? slug { get; set; }
        public string? carrier_id { get; set; }
        public int __v { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public object id { get; set; }
    }

    public class GetShipmentCarriers
    {
        public List<string> domestic { get; set; }
        public List<string> international { get; set; }
        public List<string> regional { get; set; }
    }

    public class GetShipmentContact
    {
        public string? email { get; set; }
        public string? phone { get; set; }
    }

    public class GetShipmentCoordinates
    {
        public double lat { get; set; }
        public double lng { get; set; }
        public string? place_id { get; set; }
    }

    public class GetShipmentData
    {
        public string? _id { get; set; }
        public GetShipmentAddressTo address_to { get; set; }
        public GetShipmentAddressFrom address_from { get; set; }
        public GetShipmentAddressReturn address_return { get; set; }
        public Parcel parcel { get; set; }
        public DateTime pickup_date { get; set; }
        public string? status { get; set; }
        public GetShipmentUser user { get; set; }
        public List<GetShipmentEvent> events { get; set; }
        public string? shipment_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public GetShipmentCarrier carrier { get; set; }
        public DateTime delivery_arranged { get; set; }
        public DateTime delivery_date { get; set; }
        public GetShipmentExtras extras { get; set; }
        public GetShipmentMetadata metadata { get; set; }
        public GetShipmentRate rate { get; set; }
    }

    public class GetShipmentEvent
    {
        public DateTime created_at { get; set; }
        public string? description { get; set; }
        public string? location { get; set; }
        public string? status { get; set; }
    }

    public class GetShipmentExtras
    {
        public string? tracking_url { get; set; }
    }

    public class GetShipmentItem
    {
        public string? description { get; set; }
        public string? name { get; set; }
        public string? currency { get; set; }
        public int value { get; set; }
        public int quantity { get; set; }
        public double weight { get; set; }
    }

    public class GetShipmentMetadata
    {
        public string? storeId { get; set; }
        public string? localProductCode { get; set; }
        public string? productCode { get; set; }
    }

    public class GetShipmentParcel
    {
        public string? metadata { get; set; }
        public string? _id { get; set; }
        public string? description { get; set; }
        public List<GetShipmentItem> items { get; set; }
        public string? packaging { get; set; }
        public double total_weight { get; set; }
        public string? user { get; set; }
        public string? weight_unit { get; set; }
        public string? parcel_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public string? id { get; set; }
    }

    public class GetShipmentRate
    {
        public string? _id { get; set; }
        public double amount { get; set; }
        public List<object> breakdown { get; set; }
        public string? carrier_logo { get; set; }
        public string? carrier_name { get; set; }
        public string? carrier_rate_description { get; set; }
        public string? carrier_reference { get; set; }
        public string? carrier_slug { get; set; }
        public string? currency { get; set; }
        public string? delivery_address { get; set; }
        public DateTime delivery_date { get; set; }
        public int delivery_eta { get; set; }
        public string? delivery_time { get; set; }
        public string? id { get; set; }
        public int insurance_coverage { get; set; }
        public int insurance_fee { get; set; }
        public bool includes_insurance { get; set; }
        public GetShipmentMetadata metadata { get; set; }
        public string? parcel { get; set; }
        public string? pickup_address { get; set; }
        public int pickup_eta { get; set; }
        public string? pickup_time { get; set; }
        public string? rate_id { get; set; }
        public bool used { get; set; }
        public string? user { get; set; }
        public int __v { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }

    public class GetShipmentUser
    {
        public List<object> barred_countries { get; set; }
        public string? plan { get; set; }
        public string? _id { get; set; }
        public bool admin { get; set; }
        public GetShipmentCarriers carriers { get; set; }
        public string? company_name { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public string? last_name { get; set; }
        public GetShipmentMetadata metadata { get; set; }
        public string? phone { get; set; }
        public string? public_key { get; set; }
        public string? secret_key { get; set; }
        public string? wallet { get; set; }
        public string? user_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public string? country { get; set; }
        public string? name { get; set; }
        public string? id { get; set; }
    }
}

