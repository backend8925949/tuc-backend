//==================================================================================
// FileName: GetShipments.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Delivery.Object.Response.Shipments
{
	public class GetShipments
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public GetShipmentsData data { get; set; }

        public GetShipments()
		{
		}
    }

    public class GetShipmentsData
    {
        public List<GetShipmentsShipment> shipments { get; set; }
        public GetShipmentsPagination pagination { get; set; }
    }

    public class GetShipmentsPagination
    {
        public int page { get; set; }
        public int perPage { get; set; }
        public object prevPage { get; set; }
        public object nextPage { get; set; }
        public int currentPage { get; set; }
        public int total { get; set; }
        public int pageCount { get; set; }
        public int pagingCounter { get; set; }
        public bool hasPrevPage { get; set; }
        public bool hasNextPage { get; set; }
    }

    public class GetShipmentsShipment
    {
        public string? address_to { get; set; }
        public string? address_from { get; set; }
        public string? address_return { get; set; }
        public string? parcel { get; set; }
        public object pickup_date { get; set; }
        public string? status { get; set; }
        public string? user { get; set; }
        public List<object> events { get; set; }
        public string? shipment_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
    }
}

