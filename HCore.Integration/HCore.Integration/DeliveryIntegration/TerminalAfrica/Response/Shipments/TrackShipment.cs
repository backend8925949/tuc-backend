//==================================================================================
// FileName: TrackShipment.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace Delivery.Object.Response.Shipments
{
	public class TrackShipment
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public TrackShipmentData data { get; set; }

        public TrackShipment()
		{
		}
    }

    public class TrackShipmentAddressFrom
    {
        public string? user { get; set; }
        public string? city { get; set; }
        public TrackShipmentCoordinates coordinates { get; set; }
        public string? country { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public bool is_residential { get; set; }
        public string? last_name { get; set; }
        public string? line1 { get; set; }
        public string? line2 { get; set; }
        public string? phone { get; set; }
        public string? state { get; set; }
        public string? zip { get; set; }
        public string? address_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
    }

    public class TrackShipmentAddressTo
    {
        public string? user { get; set; }
        public string? city { get; set; }
        public TrackShipmentCoordinates coordinates { get; set; }
        public string? country { get; set; }
        public string? email { get; set; }
        public string? first_name { get; set; }
        public bool is_residential { get; set; }
        public string? last_name { get; set; }
        public string? line1 { get; set; }
        public string? line2 { get; set; }
        public string? phone { get; set; }
        public string? state { get; set; }
        public string? zip { get; set; }
        public string? address_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int __v { get; set; }
        public TrackShipmentMetadata metadata { get; set; }
    }

    public class TrackShipmentCoordinates
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class TrackShipmentData
    {
        public object carrier { get; set; }
        public TrackShipmentAddressFrom address_from { get; set; }
        public TrackShipmentAddressTo address_to { get; set; }
        public string? carrier_tracking_number { get; set; }
        public DateTime? delivery_date { get; set; }
        public DateTime? pickup_date { get; set; }
        public List<object> events { get; set; }
        public TrackingStatus tracking_status { get; set; }
        public string? shipment_id { get; set; }
        public string? status { get; set; }
    }

    public class TrackingStatus
    {
        public DateTime created_at { get; set; }
        public string? description { get; set; }
        public string? location { get; set; }
        public string? status { get; set; }
    }

    public class TrackShipmentMetadata
    {
    }
}

