//==================================================================================
// FileName: DealsDelivery.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.IO;
using System.Net;
using System.Text;
using Delivery.Object.Requests;
using Delivery.Object.Requests.Address;
using Delivery.Object.Requests.Carriers;
using Delivery.Object.Requests.Packaging;
using Delivery.Object.Requests.Parcels;
using Delivery.Object.Requests.Rates;
using Delivery.Object.Requests.Shipments;
using Delivery.Object.Requests.Treansactions;
using Delivery.Object.Requests.Users;
using Delivery.Object.Response.Address;
using Delivery.Object.Response.Carriers;
using Delivery.Object.Response.Packaging;
using Delivery.Object.Response.Parcels;
using Delivery.Object.Response.Rates;
using Delivery.Object.Response.Shipments;
using Delivery.Object.Response.Treansactions;
using Delivery.Object.Response.Users;
using HCore.Helper;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;

namespace Delivery.Framework
{
    public class FrameworkDelivery
    {

        public string? ResponseMesssage { get; set; }

        public string? address_id { get; set; }

        private HttpWebRequest CreateWebRequest(string RequestType, string? url, byte[] payload)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = RequestType;
            httpWebRequest.Headers["Authorization"] = "Bearer " + _AppConfig.Shipmonk_SecretKey;

            if (payload != null)
            {
                httpWebRequest.ContentLength = payload.Length;
                Stream _RequestStream = httpWebRequest.GetRequestStream();
                _RequestStream.Write(payload, 0, payload.Length);
                _RequestStream.Close();
            }

            return httpWebRequest;
        }

        public CreateAddress CreateAddress(Create_UpdateAddress _Request)
        {
            if (string.IsNullOrEmpty(_Request.line2))
            {
                _Request.line2 = "";
            }
            if (string.IsNullOrEmpty(_Request.zip))
            {
                _Request.zip = "";
            }
            string request_url = _AppConfig.Shipmonk_Url + "v1/addresses";
            string request_json = JsonConvert.SerializeObject(_Request);
            byte[] bytes = Encoding.ASCII.GetBytes(request_json);

            var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
            try
            {
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {
                    string _Response = streamReader.ReadToEnd();
                    CreateAddress response = JsonConvert.DeserializeObject<CreateAddress>(_Response);
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        address_id = response.data.address_id;
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("CreateAddress", ex, null);
                return null;
            }
        }

        public UpdateAddress UpdateAddress(Create_UpdateAddress _Request, string? AddressId)
        {
            if (string.IsNullOrEmpty(_Request.line2))
            {
                _Request.line2 = "";
            }
            if (string.IsNullOrEmpty(_Request.zip))
            {
                _Request.zip = "";
            }
            string request_url = _AppConfig.Shipmonk_Url + "v1/addresses/" + AddressId;
            string request_json = JsonConvert.SerializeObject(_Request);
            byte[] bytes = Encoding.ASCII.GetBytes(request_json);

            var httpwebrequest = CreateWebRequest("PUT", request_url, bytes);
            try
            {
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {

                    string _Response = streamReader.ReadToEnd();
                    UpdateAddress response = JsonConvert.DeserializeObject<UpdateAddress>(_Response);
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("UpdateAddress", ex, null);
                return null;
            }
        }

        public GetAddresses GetAddresses()
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/addresses";
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {

                    string webresponse = streamReader.ReadToEnd();
                    GetAddresses response = JsonConvert.DeserializeObject<GetAddresses>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetAddresses", ex, null);
                return null;
            }
        }

        public GetAddress GetAddresse(GetAddressRequest _Request)
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/addresses/" + _Request.address_id;
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {

                    string webresponse = streamReader.ReadToEnd();
                    GetAddress response = JsonConvert.DeserializeObject<GetAddress>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetAddresse", ex, null);
                return null;
            }
        }

        public ValidateAddress ValidateAddress(ValidateAddressRequest _Request)
        {
            string request_url = _AppConfig.Shipmonk_Url + "v1/addresses/validate";
            string request_json = JsonConvert.SerializeObject(_Request);
            byte[] bytes = Encoding.ASCII.GetBytes(request_json);

            var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
            try
            {
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {

                    string _Response = streamReader.ReadToEnd();
                    ValidateAddress response = JsonConvert.DeserializeObject<ValidateAddress>(_Response);
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("ValidateAddress", ex, null);
                return null;
            }
        }

        public GetCarriers GetCarriers(GetCarriersRequest _Request)
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/carriers?active=true" + "&type=" + _Request.type + "&perPage=" + _Request.perPage;
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {

                    string webresponse = streamReader.ReadToEnd();
                    Console.WriteLine(webresponse);
                    GetCarriers response = JsonConvert.DeserializeObject<GetCarriers>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetCarriers", ex, null);
                return null;
            }
        }

        public GetCarrier GetCarrier(GetCarrierRequest _Request)
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/carriers/" + _Request.carrier_id;
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {

                    string webresponse = streamReader.ReadToEnd();
                    Console.WriteLine(webresponse);
                    GetCarrier response = JsonConvert.DeserializeObject<GetCarrier>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetCarrier", ex, null);
                return null;
            }
        }

        public CreatePackaging CreatePackaging(CreatePackagingRequest _Request)
        {
            string request_url = _AppConfig.Shipmonk_Url + "v1/packaging";
            string request_json = JsonConvert.SerializeObject(_Request);
            byte[] bytes = Encoding.ASCII.GetBytes(request_json);

            var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
            try
            {
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {
                    string _Response = streamReader.ReadToEnd();
                    CreatePackaging response = JsonConvert.DeserializeObject<CreatePackaging>(_Response);
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("CreatePackaging", ex, null);
                return null;
            }
        }

        public GetPackaging GetPackaging()
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/packaging";
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {

                    string webresponse = streamReader.ReadToEnd();
                    Console.WriteLine(webresponse);
                    GetPackaging response = JsonConvert.DeserializeObject<GetPackaging>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetPackaging", ex, null);
                return null;
            }
        }

        public GetSpecificPackaging GetSpecificPackaging(GetSpecificPackagingRequest _Request)
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/packaging/" + _Request.packaging_id;
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {

                    string webresponse = streamReader.ReadToEnd();
                    Console.WriteLine(webresponse);
                    GetSpecificPackaging response = JsonConvert.DeserializeObject<GetSpecificPackaging>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetSpecificPackaging", ex, null);
                return null;
            }
        }

        public UpdatePackaging UpdatePackaging(UpdatePackagingRequest _Request)
        {
            string request_url = _AppConfig.Shipmonk_Url + "v1/packaging/" + _Request.packaging_id;

            var httpwebrequest = CreateWebRequest("PUT", request_url, null);
            try
            {
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {
                    string _Response = streamReader.ReadToEnd();
                    UpdatePackaging response = JsonConvert.DeserializeObject<UpdatePackaging>(_Response);
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("UpdatePackaging", ex, null);
                return null;
            }
        }

        public CreateParcel CreateParcel(CreateParcelRequest _Request)
        {
            string request_url = _AppConfig.Shipmonk_Url + "v1/parcels";
            string request_json = JsonConvert.SerializeObject(_Request);
            byte[] bytes = Encoding.ASCII.GetBytes(request_json);

            var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
            try
            {
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {

                    string _Response = streamReader.ReadToEnd();
                    CreateParcel response = JsonConvert.DeserializeObject<CreateParcel>(_Response);
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("CreateParcel", ex, null);
                return null;
            }
        }

        public GetParcels GetParcels()
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/parcels";
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {

                    string webresponse = streamReader.ReadToEnd();
                    Console.WriteLine(webresponse);
                    GetParcels response = JsonConvert.DeserializeObject<GetParcels>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetParcels", ex, null);
                return null;
            }
        }

        public GetParcel GetParcel(GetParcelRequest _Request)
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/parcels/" + _Request.parcel_id;
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {
                    string webresponse = streamReader.ReadToEnd();
                    Console.WriteLine(webresponse);
                    GetParcel response = JsonConvert.DeserializeObject<GetParcel>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetParcel", ex, null);
                return null;
            }
        }

        public GetRatesForShipments GetRatesForShipment(GetRatesForShipmentsRequest _Request)
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/rates/shipment" + "?currency=" + _Request.currency + "&delivery_address=" + _Request.delivery_address + "&parcel_id=" + _Request.parcel_id + "&pickup_address=" + _Request.pickup_address;
                if (!string.IsNullOrEmpty(_Request.shipment_id))
                {
                    request_url = _AppConfig.Shipmonk_Url + "v1/rates/shipment" + "?currency=" + _Request.currency + "&delivery_address=" + _Request.delivery_address + "&parcel_id=" + _Request.parcel_id + "&pickup_address=" + _Request.pickup_address + "&shipment_id=" + _Request.shipment_id;
                }
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {
                    string webresponse = streamReader.ReadToEnd();
                    Console.WriteLine(webresponse);
                    GetRatesForShipments response = JsonConvert.DeserializeObject<GetRatesForShipments>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetRatesForShipment", ex, null);
                return null;
            }
        }

        public GetRatesForShipments GetRatesForShipmentCore(GetRatesRequest _Request)
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/rates/shipment?currency=NGN";
                if (!string.IsNullOrEmpty(_Request.shipment_id))
                {
                    request_url += "&shipment_id=" + _Request.shipment_id;
                }
                if (!string.IsNullOrEmpty(_Request.parcel_id))
                {
                    request_url += "&parcel_id=" + _Request.parcel_id;
                }
                if (!string.IsNullOrEmpty(_Request.fromaddress_id))
                {
                    request_url += "&pickup_address=" + _Request.fromaddress_id;
                }
                if (!string.IsNullOrEmpty(_Request.toaddress_id))
                {
                    request_url += "&delivery_address=" + _Request.toaddress_id;
                }
                //"shipment_id=" + _Request.shipment_id + "&parcel_id=" + _Request.parcel_id;
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {
                    string webresponse = streamReader.ReadToEnd();
                    Console.WriteLine(webresponse);
                    GetRatesForShipments response = JsonConvert.DeserializeObject<GetRatesForShipments>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetRatesForShipment", ex, null);
                return null;
            }
        }

        public GetRate GetRate(GetRateRequest _Request)
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/rates/" + _Request.rate_id;
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {
                    string webresponse = streamReader.ReadToEnd();
                    Console.WriteLine(webresponse);
                    GetRate response = JsonConvert.DeserializeObject<GetRate>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetRate", ex, null);
                return null;
            }
        }

        public GetUser GetUser(GetUserRequest _Request)
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/users/" + _Request.user_id;
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {
                    string webresponse = streamReader.ReadToEnd();
                    Console.WriteLine(webresponse);
                    GetUser response = JsonConvert.DeserializeObject<GetUser>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetUser", ex, null);
                return null;
            }
        }

        public GetUseWalletBalance GetUserWalletBalance(GetUserWalletBalanceRequest _Request)
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/users/wallet-balance?user_id=" + _Request.user_id;
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {
                    string webresponse = streamReader.ReadToEnd();
                    Console.WriteLine(webresponse);
                    GetUseWalletBalance response = JsonConvert.DeserializeObject<GetUseWalletBalance>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetUserWalletBalance", ex, null);
                return null;
            }
        }

        public GetUserCarrierss GetUserCarriers()
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/users/carriers";
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {

                    string webresponse = streamReader.ReadToEnd();
                    Console.WriteLine(webresponse);
                    GetUserCarrierss response = JsonConvert.DeserializeObject<GetUserCarrierss>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetUserCarriers", ex, null);
                return null;
            }
        }

        public CreateShipment CreateShipment(CreateShipmentRequest _Request)
        {
            string request_url = _AppConfig.Shipmonk_Url + "v1/shipments";
            string request_json = JsonConvert.SerializeObject(_Request);
            byte[] bytes = Encoding.ASCII.GetBytes(request_json);

            var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
            try
            {
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {

                    string _Response = streamReader.ReadToEnd();
                    CreateShipment response = JsonConvert.DeserializeObject<CreateShipment>(_Response);
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("CreateShipment", ex, null);
                return null;
            }
        }

        public GetShipments GetShipments()
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/shipments";
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {

                    string webresponse = streamReader.ReadToEnd();
                    Console.WriteLine(webresponse);
                    GetShipments response = JsonConvert.DeserializeObject<GetShipments>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetShipments", ex, null);
                return null;
            }
        }

        public GetShipment GetShipment(Get_TrackShipment _Request)
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/shipments/" + _Request.shipment_id;
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {
                    string webresponse = streamReader.ReadToEnd();
                    Console.WriteLine(webresponse);
                    GetShipment response = JsonConvert.DeserializeObject<GetShipment>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetShipment", ex, null);
                return null;
            }
        }

        public TrackShipment TrackShipment(Get_TrackShipment _Request)
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/shipments/track/" + _Request.shipment_id;
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {
                    string webresponse = streamReader.ReadToEnd();
                    Console.WriteLine(webresponse);
                    TrackShipment response = JsonConvert.DeserializeObject<TrackShipment>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("TrackShipment", ex, null);
                return null;
            }
        }

        public ArrangePick_up_DeliveryForShipment ArrangePick_up_DeliveryForShipment(ArrangePick_up_DeliveryForShipmentRequest _Request)
        {
            string request_url = _AppConfig.Shipmonk_Url + "v1/shipments/pickup";
            string request_json = JsonConvert.SerializeObject(_Request);
            byte[] bytes = Encoding.ASCII.GetBytes(request_json);

            var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
            try
            {
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {

                    string _Response = streamReader.ReadToEnd();
                    ArrangePick_up_DeliveryForShipment response = JsonConvert.DeserializeObject<ArrangePick_up_DeliveryForShipment>(_Response);
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("ArrangePick_up_DeliveryForShipment", ex, null);
                return null;
            }
        }

        public CancelShipment CancelShipment(CancelShipmentRequest _Request)
        {
            string request_url = _AppConfig.Shipmonk_Url + "v1/shipments/cancel";
            string request_json = JsonConvert.SerializeObject(_Request);
            byte[] bytes = Encoding.ASCII.GetBytes(request_json);

            var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
            try
            {
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {

                    string _Response = streamReader.ReadToEnd();
                    Console.WriteLine(_Response);
                    CancelShipment response = JsonConvert.DeserializeObject<CancelShipment>(_Response);
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("CancelShipment", ex, null);
                return null;
            }
        }

        public GetTransactions GetTransactions(GetTransactionsRequest _Request)
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/transactions?perPage=" + _Request.perPage + "&wallet=" + _Request.wallet + "&page=" + _Request.page;
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {

                    string webresponse = streamReader.ReadToEnd();
                    Console.WriteLine(webresponse);
                    GetTransactions response = JsonConvert.DeserializeObject<GetTransactions>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetTransactions", ex, null);
                return null;
            }
        }

        public GetTransaction GetTransaction(GetTransactionRequest _Request)
        {
            try
            {
                string request_url = _AppConfig.Shipmonk_Url + "v1/transactions/" + _Request.id;
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse httpwebresponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (var streamReader = new StreamReader(httpwebresponse.GetResponseStream()))
                {
                    string webresponse = streamReader.ReadToEnd();
                    Console.WriteLine(webresponse);
                    GetTransaction response = JsonConvert.DeserializeObject<GetTransaction>(webresponse);
                    if (httpwebresponse.StatusCode == HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetTransaction", ex, null);
                return null;
            }
        }
    }
}

