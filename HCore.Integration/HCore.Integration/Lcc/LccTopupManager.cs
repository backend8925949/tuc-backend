//==================================================================================
// FileName: LccTopupManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;

namespace HCore.Integration.Lcc
{
    public class OLccManager
    {
        public class Request
        {
            public string? reference { get; set; }
            public string? accountNumber { get; set; }
            public long amount { get; set; }
            public string? hash { get; set; }
            public string? transactionReference { get; set; }
        }

        public class Response
        {
            public string? responseCode { get; set; }
            public string? status { get; set; }
            public string? message { get; set; }
            public string? reference { get; set; }
            public string? requestId { get; set; }
            public string? hash { get; set; }
            public InitializeDetails result { get; set; }
        }

        public class InitializeDetails
        {
            public string? balance { get; set; }
            public string? email { get; set; }
            public string? name { get; set; }
            public string? phoneNumber { get; set; }
            public string? accountNumber { get; set; }
            public string? transactionReference { get; set; }
            public string? reference { get; set; }
            public long amount { get; set; }
            public DateTime initializeDate { get; set; }
        }
    }
    public static class LccTopupManager
    {
        public static bool InitializeTransaction(string Reference, string? AccountNumber, double Amount)
        {
            try
            {
                string ApiKey = "139384af76804776b5f93094f77e7782139384af76804776b5f93094f77e7782";
                string ApiSecretKey = "ea41a087289f44998a02963653b531d7ea41a087289f44998a02963653b531d7";
                string ServerUrl = "https://testapi.topuplcc.com/api/v2/initializetopup";
                if (HostEnvironment == HostEnvironmentType.Live)
                {
                    ServerUrl = "https://api.topuplcc.com/api/v2/initializetopup";
                    ApiKey = "4b00ffab812f40a09aa44addd52ea6184b00ffab812f40a09aa44addd52ea618";
                    ApiSecretKey = "4cfd4fb880cb4d39934ada176499dada4cfd4fb880cb4d39934ada176499dada";
                }
                OLccManager.Request _LccRequest = new OLccManager.Request();
                _LccRequest.reference = Reference;
                _LccRequest.accountNumber = AccountNumber;
                _LccRequest.amount = Convert.ToInt64(Amount * 100);
                _LccRequest.hash = EncryptHash(_LccRequest.reference + _LccRequest.accountNumber + _LccRequest.amount, ApiSecretKey);
                string RequestContent = JsonConvert.SerializeObject(_LccRequest);
                HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ServerUrl);
                byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
                _HttpWebRequest.ContentType = "application/json";
                _HttpWebRequest.ContentLength = bytes.Length;
                _HttpWebRequest.Method = "POST";
                _HttpWebRequest.Headers["Authorization"] = "Bearer " + ApiKey;
                Stream _RequestStream = _HttpWebRequest.GetRequestStream();
                _RequestStream.Write(bytes, 0, bytes.Length);
                _RequestStream.Close();
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = _HttpWebResponse.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();
                    OLccManager.Response _RequestBodyContent = JsonConvert.DeserializeObject<OLccManager.Response>(responseStr);
                    if (_RequestBodyContent.responseCode == "00")
                    {
                        return ConfirmTransaction(_RequestBodyContent, Reference, AccountNumber, Amount);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception _Exception)
            {

                return false;

            }
        }
        public static bool ConfirmTransaction(OLccManager.Response _IniResponse, string? Reference, string? AccountNumber, double Amount)
        {
            try
            {
                string ApiKey = "139384af76804776b5f93094f77e7782139384af76804776b5f93094f77e7782";
                string ApiSecretKey = "ea41a087289f44998a02963653b531d7ea41a087289f44998a02963653b531d7";
                string ServerUrl = "https://testapi.topuplcc.com/api/v2/confirmtopup";
                if (HostEnvironment == HostEnvironmentType.Live)
                {
                    ServerUrl = "https://api.topuplcc.com/api/v2/confirmtopup";
                    ApiKey = "4b00ffab812f40a09aa44addd52ea6184b00ffab812f40a09aa44addd52ea618";
                    ApiSecretKey = "4cfd4fb880cb4d39934ada176499dada4cfd4fb880cb4d39934ada176499dada";
                }

                OLccManager.Request _LccRequest = new OLccManager.Request();

                _LccRequest.reference = Reference;
                _LccRequest.accountNumber = AccountNumber;
                _LccRequest.amount = Convert.ToInt64(Amount * 100);
                _LccRequest.transactionReference = _IniResponse.result.transactionReference;
                _LccRequest.hash = EncryptHash(_LccRequest.reference + _LccRequest.accountNumber + _LccRequest.amount + _IniResponse.result.transactionReference, ApiSecretKey);
                string RequestContent = JsonConvert.SerializeObject(_LccRequest);
                HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(ServerUrl);
                byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
                _HttpWebRequest.ContentType = "application/json";
                _HttpWebRequest.ContentLength = bytes.Length;
                _HttpWebRequest.Method = "POST";
                _HttpWebRequest.Headers["Authorization"] = "Bearer " + ApiKey;
                Stream _RequestStream = _HttpWebRequest.GetRequestStream();
                _RequestStream.Write(bytes, 0, bytes.Length);
                _RequestStream.Close();
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = _HttpWebResponse.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();

                    OLccManager.Response _RequestBodyContent = JsonConvert.DeserializeObject<OLccManager.Response>(responseStr);
                    if (_RequestBodyContent.responseCode == "00")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception _Exception)
            {
                return false;
            }
        }
        private static string EncryptHash(string Content, string? SecretKey)
        {
            var hash = new StringBuilder();
            byte[] secretkeyBytes = Encoding.UTF8.GetBytes(SecretKey);
            byte[] inputBytes = Encoding.UTF8.GetBytes(Content);
            using (var hmac = new HMACSHA512(secretkeyBytes))
            {
                byte[] hashValue = hmac.ComputeHash(inputBytes);
                foreach (var theByte in hashValue)
                {
                    hash.Append(theByte.ToString("x2"));
                }
            }
            return hash.ToString();
        }

    }
}
