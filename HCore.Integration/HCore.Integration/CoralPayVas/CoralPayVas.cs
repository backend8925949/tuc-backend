//==================================================================================
// FileName: CoralPayVas.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using static HCore.Helper.HCoreConstant;

namespace HCore.Integration.CoralPayVas
{
    public class OCoralPayVas
    {


        public class AccountCheck
        {
            public class Response
            {
                public string? Status { get; set; }
                public string? StatusMessage { get; set; }
                public string? StatusResponseCode { get; set; }
                public string? RequestContent { get; set; }
                public string? ResponseContent { get; set; }

                public double Amount { get; set; }
                public double MinimumAmount { get; set; }
                public string? OrderId { get; set; }
                public Customer Customer { get; set; }
            }

            public class Customer
            {
                public string? FirstName { get; set; }
                public string? MiddleName { get; set; }
                public string? LastName { get; set; }
                public string? CustomerName { get; set; }
                public string? MeterNumber { get; set; }
                public string? ArrearsBalance { get; set; }
                public bool CanVend { get; set; }
                public string? District { get; set; }
                public string? PhoneNumber { get; set; }
                public string? EmailAddress { get; set; }
            }
        }
        public class AccountPayment
        {
            public class Response
            {
                public string? Status { get; set; }
                public string? StatusMessage { get; set; }
                public string? StatusResponseCode { get; set; }
                public string? RequestContent { get; set; }
                public string? ResponseContent { get; set; }

                public string? PackageName { get; set; }
                public string? PaymentReference { get; set; }
                public string? TransactionId { get; set; }
                public string? VendStatus { get; set; }
                public string? Narration { get; set; }
                public double Amount { get; set; }
                public double ConvenienceFee { get; set; }
                public DateTime? Date { get; set; }
                public Token Token { get; set; }
            }

            public class Token
            {
                public double Amount { get; set; }
                public string? ReceiptNumber { get; set; }
                public string? Tariff { get; set; }
                public string? Units { get; set; }
                public string? UnitsType { get; set; }
                public string? Value { get; set; }
            }
        }
    }
    internal class OCoralPayCore
    {
        public class AccountCheck
        {
            public class Request
            {
                public string? customerId { get; set; }
                public string? billerSlug { get; set; }
                public string? productName { get; set; }
            }
            public class Response
            {
                public bool error { get; set; }
                public string? status { get; set; }
                public string? message { get; set; }
                public string? responseCode { get; set; }
                public ResponseData responseData { get; set; }
            }
            public class ResponseData
            {
                public double amount { get; set; }
                public double minAmount { get; set; }
                public double minPayableAmount { get; set; }
                public string? orderId { get; set; }
                public Customer customer { get; set; }
            }
            public class Customer
            {
                public string? firstName { get; set; }
                public string? middleName { get; set; }
                public string? lastName { get; set; }
                public string? customerName { get; set; }
                public string? meterNumber { get; set; }
                public string? arrearsBalance { get; set; }
                public bool canVend { get; set; }
                public string? district { get; set; }
                public string? phoneNumber { get; set; }
                public string? emailAddress { get; set; }
            }
        }
        public class AccountPayment
        {
            public class Request
            {
                public string? paymentReference { get; set; }
                public string? customerId { get; set; }
                public string? packageSlug { get; set; }
                public string? orderId { get; set; }
                public string? channel { get; set; }
                public double amount { get; set; }
                public string? customerName { get; set; }
                public string? phoneNumber { get; set; }
                public string? email { get; set; }
                public string? accountNumber { get; set; }
                public ResponseData responseData { get; set; }
            }

            public class Response
            {
                public bool error { get; set; }
                public string? status { get; set; }
                public string? message { get; set; }
                public string? responseCode { get; set; }
                public ResponseData responseData { get; set; }
            }
            public class ResponseData
            {
                public string? packageName { get; set; }
                public string? paymentReference { get; set; }
                public string? transactionId { get; set; }
                public string? vendStatus { get; set; }
                public string? narration { get; set; }
                public string? statusCode { get; set; }
                public double amount { get; set; }
                public double convenienceFee { get; set; }
                public double reversalStatus { get; set; }
                public DateTime date { get; set; }
                public TokenData tokenData { get; set; }
            }
            public class TokenData
            {
                public DateTime? daysLastVend { get; set; }
                public TokenInformation stdToken { get; set; }
            }
            public class TokenInformation
            {
                public double amount { get; set; }
                public string? receiptNumber { get; set; }
                public string? tariff { get; set; }
                public string? units { get; set; }
                public string? unitsType { get; set; }
                public string? value { get; set; }
            }
        }
    }
    public class CoralPayVas
    {
        public static OCoralPayVas.AccountCheck.Response CoralPayAccountVerify(string Url, string? AuthUserName, string? AuthPassword, string? AccountNumber, string? billerSlug, string? packageSlug)
        {
            OCoralPayVas.AccountCheck.Response _Response = new OCoralPayVas.AccountCheck.Response();
            _Response.Status = "Error";
            _Response.StatusMessage = "Unable to process your request";
            _Response.StatusResponseCode = "HCP500";
            try
            {
                //DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                //string ServerUrl = "http://204.8.207.124:8080/coralpay-vas/api/";
                //if (HostEnvironment == HostEnvironmentType.Live)
                //{
                //    ServerUrl = "https://vas.coralpay.com/vas-service/api/";
                //}
                OCoralPayCore.AccountCheck.Request _AccountCheckRequest = new OCoralPayCore.AccountCheck.Request();
                _AccountCheckRequest.customerId = AccountNumber;
                _AccountCheckRequest.billerSlug = billerSlug;
                _AccountCheckRequest.productName = packageSlug;
                string RequestContent = JsonConvert.SerializeObject(_AccountCheckRequest);
                HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(Url + "transactions/customer-lookup");
                byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
                _HttpWebRequest.ContentType = "application/json";
                _HttpWebRequest.ContentLength = bytes.Length;
                _HttpWebRequest.Method = "POST";
                _HttpWebRequest.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(AuthUserName + ":" + AuthPassword)));
                Stream _RequestStream = _HttpWebRequest.GetRequestStream();
                _RequestStream.Write(bytes, 0, bytes.Length);
                _RequestStream.Close();
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = _HttpWebResponse.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();
                    OCoralPayCore.AccountCheck.Response _RequestBodyContent = JsonConvert.DeserializeObject<OCoralPayCore.AccountCheck.Response>(responseStr);
                    if (_RequestBodyContent != null)
                    {
                        if (_RequestBodyContent.error == false)
                        {
                            _Response.Status = "Success";
                            _Response.StatusMessage = "Account verified";
                            _Response.StatusResponseCode = "HCP200-" + _RequestBodyContent.responseCode;
                            _Response.RequestContent = RequestContent;
                            _Response.ResponseContent = responseStr;
                            _Response.Amount = _RequestBodyContent.responseData.amount;
                            _Response.MinimumAmount = _RequestBodyContent.responseData.minAmount;
                            if (_RequestBodyContent.responseData.minPayableAmount > 0)
                            {
                                _Response.MinimumAmount = _RequestBodyContent.responseData.minPayableAmount;
                            }
                            _Response.OrderId = _RequestBodyContent.responseData.orderId;
                            if (_RequestBodyContent.responseData.customer != null)
                            {
                                OCoralPayVas.AccountCheck.Customer _Customer = new OCoralPayVas.AccountCheck.Customer();
                                _Customer.FirstName = _RequestBodyContent.responseData.customer.firstName;
                                _Customer.MiddleName = _RequestBodyContent.responseData.customer.middleName;
                                _Customer.LastName = _RequestBodyContent.responseData.customer.lastName;
                                _Customer.CustomerName = _RequestBodyContent.responseData.customer.customerName;
                                _Customer.MeterNumber = _RequestBodyContent.responseData.customer.meterNumber;
                                _Customer.ArrearsBalance = _RequestBodyContent.responseData.customer.arrearsBalance;
                                _Customer.CanVend = _RequestBodyContent.responseData.customer.canVend;
                                _Customer.District = _RequestBodyContent.responseData.customer.district;
                                _Customer.PhoneNumber = _RequestBodyContent.responseData.customer.phoneNumber;
                                _Customer.EmailAddress = _RequestBodyContent.responseData.customer.emailAddress;
                                _Response.Customer = _Customer;
                            }
                            return _Response;
                        }
                        else
                        {
                            _Response.Status = "Error";
                            _Response.StatusMessage = "Operation failed" + _RequestBodyContent.message;
                            _Response.StatusResponseCode = "HCP400-" + _RequestBodyContent.responseCode;
                            _Response.RequestContent = RequestContent;
                            _Response.ResponseContent = responseStr;
                            return _Response;
                        }
                    }
                    else
                    {
                        _Response.RequestContent = RequestContent;
                        _Response.ResponseContent = responseStr;
                        return _Response;
                    }
                }
                else
                {
                    _Response.RequestContent = RequestContent;
                    return _Response;
                }
            }
            catch (Exception _Exception)
            {
                HCore.Helper.HCoreHelper.LogException("CORALPAYVERIFYACCOUNT", _Exception);
                return _Response;
            }
        }

        public static OCoralPayVas.AccountPayment.Response CoralPayPayment(string Url, string? AuthUserName, string? AuthPassword, string? AccountNumber, string? packageSlug, string? paymentReference, double amount, string? orderId, string? customerName, string? phoneNumber, string? email, long AccountId)
        {
            OCoralPayVas.AccountPayment.Response _Response = new OCoralPayVas.AccountPayment.Response();
            _Response.Status = "Error";
            _Response.StatusMessage = "Unable to process your request.";
            _Response.StatusResponseCode = "HCP500";
            try
            {
                OCoralPayCore.AccountPayment.Request _Request = new OCoralPayCore.AccountPayment.Request();
                _Request.paymentReference = paymentReference;
                _Request.customerId = AccountNumber;
                _Request.packageSlug = packageSlug;
                _Request.orderId = orderId;
                _Request.channel = "WEB";
                _Request.amount = amount;
                _Request.customerName = customerName;
                _Request.phoneNumber = phoneNumber;
                _Request.email = email;
                _Request.accountNumber = AccountId.ToString();
                string RequestContent = JsonConvert.SerializeObject(_Request,
                                            new JsonSerializerSettings()
                                            {
                                                NullValueHandling = NullValueHandling.Ignore
                                            });
                HCoreHelper.LogData(LogType.Log, "CoralPayPaymentRequest", "", RequestContent);
                HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(Url + "transactions/process-payment");
                byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
                _HttpWebRequest.ContentType = "application/json";
                _HttpWebRequest.ContentLength = bytes.Length;
                _HttpWebRequest.Method = "POST";
                _HttpWebRequest.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(AuthUserName + ":" + AuthPassword)));
                Stream _RequestStream = _HttpWebRequest.GetRequestStream();
                _RequestStream.Write(bytes, 0, bytes.Length);
                _RequestStream.Close();
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = _HttpWebResponse.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();
                    HCoreHelper.LogData(LogType.Log, "CoralPayPaymentResponse", "", responseStr);

                    OCoralPayCore.AccountPayment.Response _RequestBodyContent = JsonConvert.DeserializeObject<OCoralPayCore.AccountPayment.Response>(responseStr);
                    if (_RequestBodyContent != null)
                    {
                        if (_RequestBodyContent.error == false)
                        {
                            _Response.Status = "Success";
                            _Response.StatusMessage = "Account verified";
                            _Response.StatusResponseCode = "HCP200-" + _RequestBodyContent.responseCode;
                            _Response.RequestContent = RequestContent;
                            _Response.ResponseContent = responseStr;
                            _Response.PackageName = _RequestBodyContent.responseData.packageName;
                            _Response.PaymentReference = _RequestBodyContent.responseData.paymentReference;
                            _Response.TransactionId = _RequestBodyContent.responseData.transactionId;
                            _Response.VendStatus = _RequestBodyContent.responseData.vendStatus;
                            _Response.Narration = _RequestBodyContent.responseData.narration;
                            _Response.Amount = _RequestBodyContent.responseData.amount;
                            _Response.ConvenienceFee = _RequestBodyContent.responseData.convenienceFee;
                            HCoreHelper.LogData(LogType.Log, "CoralPayPaymentResponseX121122", _HttpWebResponse.StatusDescription, responseStr);
                            if (_RequestBodyContent.responseData.tokenData != null && _RequestBodyContent.responseData.tokenData.stdToken != null)
                            {
                                OCoralPayVas.AccountPayment.Token _Token = new OCoralPayVas.AccountPayment.Token();
                                _Token.Amount = _RequestBodyContent.responseData.tokenData.stdToken.amount;
                                _Token.ReceiptNumber = _RequestBodyContent.responseData.tokenData.stdToken.receiptNumber;
                                _Token.Tariff = _RequestBodyContent.responseData.tokenData.stdToken.tariff;
                                _Token.Units = _RequestBodyContent.responseData.tokenData.stdToken.units;
                                _Token.UnitsType = _RequestBodyContent.responseData.tokenData.stdToken.unitsType;
                                _Token.Value = _RequestBodyContent.responseData.tokenData.stdToken.value;
                                _Response.Token = _Token;
                            }
                            return _Response;
                        }
                        else
                        {
                            _Response.Status = "Error";
                            _Response.StatusMessage = "Operation failed" + _RequestBodyContent.message;
                            _Response.StatusResponseCode = "HCP400-" + _RequestBodyContent.responseCode;
                            _Response.RequestContent = RequestContent;
                            _Response.ResponseContent = responseStr;
                            HCoreHelper.LogData(LogType.Log, "CoralPayPaymentResponseX11122", _HttpWebResponse.StatusDescription, responseStr);
                            return _Response;
                        }
                    }
                    else
                    {
                        _Response.RequestContent = RequestContent;
                        _Response.ResponseContent = responseStr;
                        _Response.StatusMessage = "Unable to process your request. No response from provider";
                        HCoreHelper.LogData(LogType.Log, "CoralPayPaymentResponseX111", _HttpWebResponse.StatusDescription, responseStr);
                        return _Response;
                    }
                }
                else
                {
                    Stream responseStream = _HttpWebResponse.GetResponseStream();
                    _Response.StatusMessage = "Unable to process your request. Invalid statuscode " + _HttpWebResponse.StatusDescription;
                    string responseStr = new StreamReader(responseStream).ReadToEnd();
                    HCoreHelper.LogData(LogType.Log, "CoralPayPaymentExResponse", "", responseStr);
                    HCoreHelper.LogData(LogType.Log, "CoralPayPaymentResponseX", _HttpWebResponse.StatusDescription, responseStr);
                    _Response.RequestContent = RequestContent;
                    return _Response;
                }
            }
            catch (Exception _Exception)
            {
                _Response.StatusMessage = "Unable to process your request. Provider error";
                HCoreHelper.LogData(HCoreConstant.LogType.Log, "_ExceptionCorealPay", "", _Exception.Message);
                HCoreHelper.LogException("CoralPayPayment", _Exception);
                return _Response;
            }
        }

        public static OCoralPayVas.AccountPayment.Response CoralPayCancelTransaction(
            string Url, string? AuthUserName, string? AuthPassword, string? AccountNumber,
            string packageSlug, string? paymentReference, double amount,
            string orderId, string? customerName,
            string phoneNumber, string? email, long AccountId)
        {
            OCoralPayVas.AccountPayment.Response _Response = new OCoralPayVas.AccountPayment.Response();
            _Response.Status = "Error";
            _Response.StatusMessage = "Unable to process your request";
            _Response.StatusResponseCode = "HCP500";
            try
            {
                OCoralPayCore.AccountPayment.Request _Request = new OCoralPayCore.AccountPayment.Request();
                _Request.paymentReference = paymentReference;
                _Request.accountNumber = AccountNumber;
                _Request.packageSlug = packageSlug;
                _Request.orderId = orderId;
                _Request.channel = "WEB";
                _Request.amount = amount;
                _Request.customerName = customerName;
                _Request.phoneNumber = phoneNumber;
                _Request.email = email;
                //_Request.accountNumber = AccountId.ToString();
                string RequestContent = JsonConvert.SerializeObject(_Request,
                                            new JsonSerializerSettings()
                                            {
                                                NullValueHandling = NullValueHandling.Ignore
                                            });
                HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(Url + "transactions/notify-payment-reversal");
                HCoreHelper.LogData(LogType.Log, "_VasReversalResponse_Request", AccountId.ToString(), RequestContent);
                byte[] bytes = Encoding.ASCII.GetBytes(RequestContent);
                _HttpWebRequest.ContentType = "application/json";
                _HttpWebRequest.ContentLength = bytes.Length;
                _HttpWebRequest.Method = "POST";
                _HttpWebRequest.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(AuthUserName + ":" + AuthPassword)));
                Stream _RequestStream = _HttpWebRequest.GetRequestStream();
                _RequestStream.Write(bytes, 0, bytes.Length);
                _RequestStream.Close();
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                HCoreHelper.LogData(LogType.Log, "_VasReversalResponse_Response", AccountId.ToString(), _HttpWebResponse.StatusDescription);
                if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = _HttpWebResponse.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();
                    HCoreHelper.LogData(LogType.Log, "_VasReversalResponse_Response", AccountId.ToString(), responseStr);
                    OCoralPayCore.AccountPayment.Response _RequestBodyContent = JsonConvert.DeserializeObject<OCoralPayCore.AccountPayment.Response>(responseStr);
                    if (_RequestBodyContent != null)
                    {
                        if (_RequestBodyContent.error == false)
                        {
                            _Response.Status = "Success";
                            _Response.StatusMessage = "Account verified";
                            _Response.StatusResponseCode = "HCP200-" + _RequestBodyContent.responseCode;
                            _Response.RequestContent = RequestContent;
                            _Response.ResponseContent = responseStr;
                            _Response.PackageName = _RequestBodyContent.responseData.packageName;
                            _Response.PaymentReference = _RequestBodyContent.responseData.paymentReference;
                            _Response.TransactionId = _RequestBodyContent.responseData.transactionId;
                            _Response.VendStatus = _RequestBodyContent.responseData.vendStatus;
                            _Response.Narration = _RequestBodyContent.responseData.narration;
                            _Response.Amount = _RequestBodyContent.responseData.amount;
                            _Response.ConvenienceFee = _RequestBodyContent.responseData.convenienceFee;
                            if (_RequestBodyContent.responseData.tokenData != null && _RequestBodyContent.responseData.tokenData.stdToken != null)
                            {
                                OCoralPayVas.AccountPayment.Token _Token = new OCoralPayVas.AccountPayment.Token();
                                _Token.Amount = _RequestBodyContent.responseData.tokenData.stdToken.amount;
                                _Token.ReceiptNumber = _RequestBodyContent.responseData.tokenData.stdToken.receiptNumber;
                                _Token.Tariff = _RequestBodyContent.responseData.tokenData.stdToken.tariff;
                                _Token.Units = _RequestBodyContent.responseData.tokenData.stdToken.units;
                                _Token.UnitsType = _RequestBodyContent.responseData.tokenData.stdToken.unitsType;
                                _Token.Value = _RequestBodyContent.responseData.tokenData.stdToken.value;
                                _Response.Token = _Token;
                            }
                            return _Response;
                        }
                        else
                        {
                            _Response.Status = "Error";
                            _Response.StatusMessage = "Operation failed" + _RequestBodyContent.message;
                            _Response.StatusResponseCode = "HCP400-" + _RequestBodyContent.responseCode;
                            _Response.RequestContent = RequestContent;
                            _Response.ResponseContent = responseStr;
                            return _Response;
                        }
                    }
                    else
                    {
                        _Response.RequestContent = RequestContent;
                        _Response.ResponseContent = responseStr;
                        return _Response;
                    }
                }
                else
                {
                    _Response.RequestContent = RequestContent;
                    return _Response;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CoralPayCancelTransaction", _Exception);
                return _Response;
            }
        }

        //public static OCoralPayVas.AccountPayment.Response CoralPayTransactionEnquiry(string Url, string? AuthUserName, string? AuthPassword, string? paymentReference)
        //{
        //    OCoralPayVas.AccountPayment.Response _Response = new OCoralPayVas.AccountPayment.Response();
        //    _Response.Status = "Error";
        //    _Response.StatusMessage = "Unable to process your request";
        //    _Response.StatusResponseCode = "HCP500";
        //    try
        //    {
        //        HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(Url + "transactions/payment- lookup/?paymentReference=" + paymentReference);
        //        _HttpWebRequest.ContentType = "text/plain";
        //        _HttpWebRequest.Method = "GET";
        //        _HttpWebRequest.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(AuthUserName + ":" + AuthPassword)));
        //        Stream _RequestStream = _HttpWebRequest.GetRequestStream();
        //        _RequestStream.Close();
        //        HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
        //        HCoreHelper.LogData(LogType.Log, "_VasEnquiryResponse_Response", paymentReference, _HttpWebResponse.StatusDescription);
        //        if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
        //        {
        //            Stream responseStream = _HttpWebResponse.GetResponseStream();
        //            string responseStr = new StreamReader(responseStream).ReadToEnd();
        //            HCoreHelper.LogData(LogType.Log, "_VasEnquiryResponse_Response", paymentReference, responseStr);
        //            OCoralPayCore.TransactionStatus.Response _RequestBodyContent = JsonConvert.DeserializeObject<OCoralPayCore.TransactionStatus.Response>(responseStr);
        //            if (_RequestBodyContent != null)
        //            {
        //                if (_RequestBodyContent.error == false)
        //                {
        //                    _Response.Status = "Success";
        //                    _Response.StatusMessage = "Account verified";
        //                    _Response.StatusResponseCode = "HCP200-" + _RequestBodyContent.responseCode;
        //                    _Response.ResponseContent = responseStr;
        //                    _Response.PaymentReference = _RequestBodyContent.responseData.paymentReference;
        //                    _Response.TransactionId = _RequestBodyContent.responseData.transactionId;
        //                    _Response.VendStatus = _RequestBodyContent.responseData.vendStatus;
        //                    _Response.Narration = _RequestBodyContent.responseData.narration;
        //                    _Response.Amount = _RequestBodyContent.responseData.amount;
        //                    if (_RequestBodyContent.responseData.tokenData != null && _RequestBodyContent.responseData.tokenData.stdToken != null)
        //                    {
        //                        OCoralPayVas.AccountPayment.Token _Token = new OCoralPayVas.AccountPayment.Token();
        //                        _Token.Amount = _RequestBodyContent.responseData.tokenData.stdToken.amount;
        //                        _Token.Units = _RequestBodyContent.responseData.tokenData.stdToken.units;
        //                        _Response.Token = _Token;
        //                    }
        //                    return _Response;
        //                }
        //                else
        //                {
        //                    _Response.Status = "Error";
        //                    _Response.StatusMessage = "Operation failed" + _RequestBodyContent.message;
        //                    _Response.StatusResponseCode = "HCP400-" + _RequestBodyContent.responseCode;
        //                    _Response.ResponseContent = responseStr;
        //                    return _Response;
        //                }
        //            }
        //            else
        //            {
        //                _Response.ResponseContent = responseStr;
        //                return _Response;
        //            }
        //        }
        //        else
        //        {
        //            return _Response;
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException("CoralPayTransactionEnquiry", _Exception);
        //        return _Response;
        //    }
        //}

        #region Transaction Enquiry
        //public CoralPayVas()
        //{
        //    if (HostEnvironment == HostEnvironmentType.Live)
        //    {
        //        SetEndPoints("https://vas.coralpay.com/vas-service/api/");
        //    }
        //    else
        //    {
        //        SetEndPoints("http://204.8.207.124:8080/coralpay-vas/api/");
        //    }
        //}

        //public string? CoralPayUrl { get; private set; }
        //public void SetEndPoints(string url)
        //{
        //    CoralPayUrl = url + "transactions/payment-lookup/";
        //}

        private static HttpWebRequest CreateWebRequest(string RequestType, string? url, byte[] payload, string? username, string? password)
        {
            var _HttpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            _HttpWebRequest.ContentType = "text/plain";
            _HttpWebRequest.Method = RequestType;

            if (HostEnvironment == HostEnvironmentType.Live)
            {
                string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                _HttpWebRequest.Headers.Add("Authorization", "Basic " + svcCredentials);
            }
            else
            {
                string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                _HttpWebRequest.Headers.Add("Authorization", "Basic " + svcCredentials);
            }

            if (payload != null)
            {
                _HttpWebRequest.ContentLength = payload.Length;
                Stream _RequestStream = _HttpWebRequest.GetRequestStream();
                _RequestStream.Write(payload, 0, payload.Length);
                _RequestStream.Close();
            }

            return _HttpWebRequest;
        }

        public static OCoralPayVas.AccountPayment.Response CoralPayTransactionEnquiry(string Url, string? AuthUserName, string? AuthPassword, string? paymentReference)
        {
            OCoralPayVas.AccountPayment.Response _Response = new OCoralPayVas.AccountPayment.Response();
            _Response.Status = "Error";
            _Response.StatusMessage = "Unable to process your request";
            _Response.StatusResponseCode = "HCP500";
            try
            {
                string RequestUrl = Url + "transactions/payment-lookup/?paymentReference=" + paymentReference;
                var httpwebrequest = CreateWebRequest("GET", RequestUrl, null, AuthUserName, AuthPassword);
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

                using (StreamReader streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {
                    Stream responseStream = _HttpWebResponse.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();
                    if (!string.IsNullOrEmpty(responseStr))
                    {
                        HCoreHelper.LogData(LogType.Log, "CORALPAYREQUERY", "", responseStr);
                    }
                    OCoralPayCore.AccountPayment.Response _RequestBodyContent = JsonConvert.DeserializeObject<OCoralPayCore.AccountPayment.Response>(responseStr);
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        if (_RequestBodyContent.error == false)
                        {
                            _Response.Status = "Success";
                            _Response.StatusMessage = "Account verified";
                            _Response.StatusResponseCode = "HCP200-" + _RequestBodyContent.responseCode;
                            _Response.RequestContent = RequestUrl;
                            _Response.ResponseContent = responseStr;
                            _Response.PackageName = _RequestBodyContent.responseData.packageName;
                            _Response.PaymentReference = _RequestBodyContent.responseData.paymentReference;
                            _Response.TransactionId = _RequestBodyContent.responseData.transactionId;
                            _Response.VendStatus = _RequestBodyContent.responseData.vendStatus;
                            _Response.Narration = _RequestBodyContent.responseData.narration;
                            _Response.Amount = _RequestBodyContent.responseData.amount;
                            _Response.ConvenienceFee = _RequestBodyContent.responseData.convenienceFee;
                            if (_RequestBodyContent.responseData.tokenData != null && _RequestBodyContent.responseData.tokenData.stdToken != null)
                            {
                                OCoralPayVas.AccountPayment.Token _Token = new OCoralPayVas.AccountPayment.Token();
                                _Token.Amount = _RequestBodyContent.responseData.tokenData.stdToken.amount;
                                _Token.ReceiptNumber = _RequestBodyContent.responseData.tokenData.stdToken.receiptNumber;
                                _Token.Tariff = _RequestBodyContent.responseData.tokenData.stdToken.tariff;
                                _Token.Units = _RequestBodyContent.responseData.tokenData.stdToken.units;
                                _Token.UnitsType = _RequestBodyContent.responseData.tokenData.stdToken.unitsType;
                                _Token.Value = _RequestBodyContent.responseData.tokenData.stdToken.value;
                                _Response.Token = _Token;
                            }
                            return _Response;
                        }
                        else
                        {
                            _Response.Status = "Error";
                            _Response.StatusMessage = "Operation failed" + _RequestBodyContent.message;
                            _Response.StatusResponseCode = "HCP400-" + _RequestBodyContent.responseCode;
                            _Response.RequestContent = RequestUrl;
                            _Response.ResponseContent = responseStr;
                            return _Response;
                        }
                    }
                    else
                    {
                        _Response.Status = "Error";
                        _Response.StatusMessage = "Unable to verify payment";
                        _Response.StatusResponseCode = "HCP500";
                        return _Response;
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CoralPayTransactionEnquiry", _Exception);
                return _Response;
            }
        }
        #endregion
    }
}
