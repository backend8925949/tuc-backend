//==================================================================================
// FileName: CredPalOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using Newtonsoft.Json;
using RestSharp;

namespace HCore.Integration.CredPal
{
    public static class CredPalOperations
    {
        public static OCredPalOperations.OnboardUser.Response OnboardUser(string Url, string ApiKey, OCredPalOperations.OnboardUser.Request _Request)
        {
            var _RestClient = new RestClient(Url + "/users/onboard");
            //_RestClient.Timeout = -1;
            var _RestClientRequest = new RestRequest();
            _RestClientRequest.Method = Method.Post;
            _RestClientRequest.AddHeader("Authorization", "Bearer " + ApiKey);
            _RestClientRequest.AlwaysMultipartFormData = true;

            if (!string.IsNullOrEmpty(_Request.first_name))
            {
                _RestClientRequest.AddParameter("first_name", _Request.first_name);
            }
            if (!string.IsNullOrEmpty(_Request.last_name))
            {
                _RestClientRequest.AddParameter("last_name", _Request.last_name);
            }
            //
            if (!string.IsNullOrEmpty(_Request.personal_email))
            {
                _RestClientRequest.AddParameter("personal_email", _Request.personal_email);
            }
            if (!string.IsNullOrEmpty(_Request.phone_no))
            {
                _RestClientRequest.AddParameter("phone_no", _Request.phone_no);
            }
            if (!string.IsNullOrEmpty(_Request.work_email))
            {
                _RestClientRequest.AddParameter("work_email", _Request.work_email);
            }
            if (!string.IsNullOrEmpty(_Request.date_of_birth))
            {
                _RestClientRequest.AddParameter("date_of_birth", _Request.date_of_birth);
            }
            if (!string.IsNullOrEmpty(_Request.bvn))
            {
                _RestClientRequest.AddParameter("bvn", _Request.bvn);
            }
            if (!string.IsNullOrEmpty(_Request.address))
            {
                _RestClientRequest.AddParameter("address", _Request.address);
            }
            if (!string.IsNullOrEmpty(_Request.state))
            {
                _RestClientRequest.AddParameter("state", _Request.state);
            }
            if (!string.IsNullOrEmpty(_Request.lga))
            {
                _RestClientRequest.AddParameter("lga", _Request.lga);
            }
            if (!string.IsNullOrEmpty(_Request.plan_name))
            {
                _RestClientRequest.AddParameter("plan_name", _Request.plan_name);
            }
            if (_Request.asset_value > 0)
            {
                _RestClientRequest.AddParameter("asset_value", _Request.asset_value);
            }
            if (_Request.salary_amount > 0)
            {
                _RestClientRequest.AddParameter("salary_amount", _Request.salary_amount);
            }
            if (_Request.salary_day > 0)
            {
                _RestClientRequest.AddParameter("salary_day", _Request.salary_day);
            }

            var _IRestResponse = _RestClient.Execute(_RestClientRequest);
            OCredPalOperations.OnboardUser.Response _RequestBodyContent = JsonConvert.DeserializeObject<OCredPalOperations.OnboardUser.Response>(_IRestResponse.Content);
            return _RequestBodyContent;
        }
        public static OCredPalOperations.AddBank.Response AddBank(string Url, string? ApiKey, OCredPalOperations.AddBank.Request _Request)
        {
            var _RestClient = new RestClient(Url + "/users/onboard/bank-details");
            //_RestClient.Timeout = -1;
            var _RestClientRequest = new RestRequest();
            _RestClientRequest.Method = Method.Post;
            _RestClientRequest.AddHeader("Authorization", "Bearer " + ApiKey);
            _RestClientRequest.AlwaysMultipartFormData = true;

            if (!string.IsNullOrEmpty(_Request.user_email))
            {
                _RestClientRequest.AddParameter("user_email", _Request.user_email);
            }
            if (!string.IsNullOrEmpty(_Request.account_number))
            {
                _RestClientRequest.AddParameter("account_number", _Request.account_number);
            }
            if (!string.IsNullOrEmpty(_Request.bank_code))
            {
                _RestClientRequest.AddParameter("bank_code", _Request.bank_code);
            }
            var _IRestResponse = _RestClient.Execute(_RestClientRequest);
            OCredPalOperations.AddBank.Response _RequestBodyContent = JsonConvert.DeserializeObject<OCredPalOperations.AddBank.Response>(_IRestResponse.Content);
            return _RequestBodyContent;
        }
        public static OCredPalOperations.OfferLetter.Response GetOfferLetter(string Url, string? ApiKey, string? user_email, string? LoanId)
        {
            var _RestClient = new RestClient(Url + "/users/loans/offer-letter?user_email=" + user_email + "&loan_id=" + LoanId);
            //_RestClient.Timeout = -1;
            var _RestClientRequest = new RestRequest();
            _RestClientRequest.Method = Method.Post;
            _RestClientRequest.AddHeader("Authorization", "Bearer " + ApiKey);
            _RestClientRequest.AlwaysMultipartFormData = true;
            var _IRestResponse = _RestClient.Execute(_RestClientRequest);
            OCredPalOperations.OfferLetter.Response _RequestBodyContent = JsonConvert.DeserializeObject<OCredPalOperations.OfferLetter.Response>(_IRestResponse.Content);
            return _RequestBodyContent;
        }
        public static OCredPalOperations.OfferLetter.Response AcceptOfferLetter(string Url, string? ApiKey, string? user_email, string? LoanId)
        {
            var _RestClient = new RestClient(Url + "/users/loans/accept?user_email=" + user_email + "&loan_id=" + LoanId);
            //_RestClient.Timeout = -1;
            var _RestClientRequest = new RestRequest();
            _RestClientRequest.Method = Method.Post;
            _RestClientRequest.AddHeader("Authorization", "Bearer " + ApiKey);
            _RestClientRequest.AlwaysMultipartFormData = true;
            var _IRestResponse = _RestClient.Execute(_RestClientRequest);
            OCredPalOperations.OfferLetter.Response _RequestBodyContent = JsonConvert.DeserializeObject<OCredPalOperations.OfferLetter.Response>(_IRestResponse.Content);
            return _RequestBodyContent;
        }
        public static OCredPalOperations.OfferLetter.Response RejectOfferLetter(string Url, string? ApiKey, string? user_email, string? LoanId)
        {
            var _RestClient = new RestClient(Url + "/users/loans/accept?user_email=" + user_email + "&loan_id=" + LoanId);

            var _RestClientRequest = new RestRequest();
            _RestClientRequest.Method = Method.Post;
            _RestClientRequest.AddHeader("Authorization", "Bearer " + ApiKey);
            _RestClientRequest.AlwaysMultipartFormData = true;
            var _IRestResponse = _RestClient.Execute(_RestClientRequest);
            OCredPalOperations.OfferLetter.Response _RequestBodyContent = JsonConvert.DeserializeObject<OCredPalOperations.OfferLetter.Response>(_IRestResponse.Content);
            return _RequestBodyContent;
        }
        public static OCredPalOperations.Loan.Response RequestLoan(string Url, string? ApiKey, OCredPalOperations.Loan.Request _Request)
        {
            var _RestClient = new RestClient(Url + "/users/loans/requests");

            var _RestClientRequest = new RestRequest();
            _RestClientRequest.Method = Method.Post;
            _RestClientRequest.AddHeader("Authorization", "Bearer " + ApiKey);
            _RestClientRequest.AlwaysMultipartFormData = true;
            if (!string.IsNullOrEmpty(_Request.user_email))
            {
                _RestClientRequest.AddParameter("user_email", _Request.user_email);
            }
            if (!string.IsNullOrEmpty(_Request.type))
            {
                _RestClientRequest.AddParameter("type", _Request.type);
            }
            if (_Request.amount > 0)
            {
                _RestClientRequest.AddParameter("amount", _Request.amount);
            }
            if (_Request.tenure > 0)
            {
                _RestClientRequest.AddParameter("tenure", _Request.tenure);
            }
            var _IRestResponse = _RestClient.Execute(_RestClientRequest);
            OCredPalOperations.Loan.Response _RequestBodyContent = JsonConvert.DeserializeObject<OCredPalOperations.Loan.Response>(_IRestResponse.Content);
            return _RequestBodyContent;
        }
        public static OCredPalOperations.Loan.List.Response GetLoanRequest(string Url, string? ApiKey, string? user_email, string? LoanType)
        {
            var _RestClient = new RestClient(Url + "/users/loans/requests?user_email=" + user_email + "&type=" + LoanType);

            var _RestClientRequest = new RestRequest();
            _RestClientRequest.Method = Method.Post;
            _RestClientRequest.AddHeader("Authorization", "Bearer " + ApiKey);
            _RestClientRequest.AlwaysMultipartFormData = true;
            var _IRestResponse = _RestClient.Execute(_RestClientRequest);
            OCredPalOperations.Loan.List.Response _RequestBodyContent = JsonConvert.DeserializeObject<OCredPalOperations.Loan.List.Response>(_IRestResponse.Content);
            return _RequestBodyContent;
        }
        public static OCredPalOperations.Loan.Details.Response GetLoanRequest(string Url, string? ApiKey, string? user_email, string? LoanType, long LoanId)
        {
            var _RestClient = new RestClient(Url + "/users/loans/requests/one?user_email=" + user_email + "&type=" + LoanType + "&loan_id=" + LoanId);

            var _RestClientRequest = new RestRequest();
            _RestClientRequest.Method = Method.Post;
            _RestClientRequest.AddHeader("Authorization", "Bearer " + ApiKey);
            _RestClientRequest.AlwaysMultipartFormData = true;
            var _IRestResponse = _RestClient.Execute(_RestClientRequest);
            OCredPalOperations.Loan.Details.Response _RequestBodyContent = JsonConvert.DeserializeObject<OCredPalOperations.Loan.Details.Response>(_IRestResponse.Content);
            return _RequestBodyContent;
        }
    }
}
