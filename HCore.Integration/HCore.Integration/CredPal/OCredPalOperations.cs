//==================================================================================
// FileName: OCredPalOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace HCore.Integration.CredPal
{
    public class OCredPalOperations
    {
        public class OnboardUser
        {
            public class Request
            {
                public string? first_name { get; set; }
                public string? last_name { get; set; }
                public string? personal_email { get; set; }
                public string? phone_no { get; set; }
                public string? work_email { get; set; }
                public string? date_of_birth { get; set; }
                public string? bvn { get; set; }
                public string? address { get; set; }
                public string? state { get; set; }
                public string? lga { get; set; }
                public string? plan_name { get; set; } = "Standard";
                public double asset_value { get; set; }
                public double salary_amount { get; set; }
                public int salary_day { get; set; }
            }
            public class Response
            {
                public bool success { get; set; }
                public string? message { get; set; }
                public errors errors { get; set; }
            }
        }
        public class AddBank
        {
            public class Request
            {
                public string? user_email { get; set; }
                public string? account_number { get; set; }
                public string? bank_code { get; set; }
            }
            public class Response
            {
                public bool success { get; set; }
                public string? message { get; set; }
                public errors errors { get; set; }
            }
        }
        public class OfferLetter
        {
            public class Response
            {
                public bool success { get; set; }
                public string? message { get; set; }
                public errors errors { get; set; }
            }
        }
        public class Loan
        {
            public class Request
            {
                public string? user_email { get; set; }
                public double amount { get; set; }
                public string? type { get; set; }
                public int tenure { get; set; }
            }
            public class Response
            {
                public bool success { get; set; }
                public string? message { get; set; }
                public Data data { get; set; }
                public errors errors { get; set; }
            }
            public class Data
            {
                public Customer customer { get; set; }
                public LoanDetails loan { get; set; }
            }
            public class Customer
            {
                public string? first_name { get; set; }
                public string? last_name { get; set; }
                public string? email { get; set; }
                public string? phone_no { get; set; }
            }
            public class LoanDetails
            {
                public string? loan_id { get; set; }
                public string? requested_amount { get; set; }
                public string? loan_amount { get; set; }
                public string? tenure { get; set; }
            }
            public class List
            {
                public class Request
                {
                    public string? user_email { get; set; }
                    public string? type { get; set; }
                    public int Offset { get; set; }
                    public int  Limit { get; set; }
                }
                public class Response
                {
                    public bool success { get; set; }
                    public string? message { get; set; }
                    public List<Data> data { get; set; }
                    public errors errors { get; set; }
                }
                public class Data
                {
                    public long loan_id { get; set; }
                    public double requested_amount { get; set; }
                    public double loan_amount { get; set; }
                    public string? status { get; set; }
                    public int tenure { get; set; }
                    public double interest_rate { get; set; }
                    public DateTime created_at { get; set; }
                }
            }
            public class Details
            {
                public class Request
                {
                    public string? user_email { get; set; }
                    public long loan_id { get; set; }
                    public string? type { get; set; }
                }
                public class Response
                {
                    public bool success { get; set; }
                    public string? message { get; set; }
                    public List<Data> data { get; set; }
                    public errors errors { get; set; }
                }
                public class Data
                {
                    public long loan_id { get; set; }
                    public double requested_amount { get; set; }
                    public double loan_amount { get; set; }
                    public string? status { get; set; }
                    public int tenure { get; set; }
                    public double interest_rate { get; set; }
                    public DateTime created_at { get; set; }
                    public List<Repayments> repayments { get; set; }
                }
                public class Repayments
                {
                    public long repayment_id { get; set; }
                    public double amount { get; set; }
                    public DateTime due_date { get; set; }
                    public double status { get; set; }
                    public double amount_paid { get; set; }
                    public DateTime paid_at { get; set; }
                    public int defaulted { get; set; }
                    public string? reference { get; set; }
                }
            }
        }

        public class errors
        {
            public List<string> first_name { get; set; }
            public List<string> last_name { get; set; }
            public List<string> personal_email { get; set; }
            public List<string> phone_no { get; set; }
            public List<string> work_email { get; set; }
            public List<string> date_of_birth { get; set; }
            public List<string> bvn { get; set; }
            public List<string> address { get; set; }
            public List<string> state { get; set; }
            public List<string> lga { get; set; }
            public List<string> plan_name { get; set; }
            public List<string> asset_value { get; set; }
            public List<string> salary_amount { get; set; }
            public List<string> salary_day { get; set; }
            public List<string> account_number { get; set; }
            public List<string> bank_code { get; set; }
            public List<string> user_email { get; set; }
            public List<string> loan_id { get; set; }
            public List<string> amount { get; set; }
            public List<string> tenure { get; set; }
            public List<string> type { get; set; }
        }
    }
}
