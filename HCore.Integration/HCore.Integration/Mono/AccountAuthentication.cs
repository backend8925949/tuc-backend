//==================================================================================
// FileName: AccountAuthentication.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using HCore.Helper;
using HCore.Integration.Mono;
using HCore.Integration.Mono.Requests;
using HCore.Integration.Mono.Responses;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;

namespace HCore.Integration.Mono
{
    #region Mono Authentication, Account Details, Bank Statements, Income Details
    public class AccountAuthentication
    {
        public AccountAuthentication()
        {
            if (HostEnvironment == HostEnvironmentType.Live)
            {
                SetEndPoints("https://api.withmono.com", "live_sk_uqGfncD64W8LWyST6UsX");
            }
            else
            {
                SetEndPoints("https://api.withmono.com", "live_sk_uqGfncD64W8LWyST6UsX");
                //SetEndPoints("https://api.withmono.com", "test_sk_FHI8F9TPNLyY98Ld4xUX");
            }
        }

        public string? MonoAccountEndPoint { get; private set; }
        public string? MonoAccountsEndPoint { get; private set; }
        public string? MonoIdentityEndPoint { get; private set; }
        public string? MonoAccountsV1EndPoint { get; private set; }
        public string? MonoSecKey { get; private set; }
        // public string? Id { get; set; }
        public string? ResposeMessage { get; private set; }
        private string period = "last1year";
        public string? Period
        {
            get { return period; }
            set { period = value; }
        }
        public void SetEndPoints(string url, String ApiKey)
        {
            MonoAccountEndPoint = url + "/Account/";
            MonoIdentityEndPoint = url + "/accounts/";
            MonoAccountsEndPoint = url + "/accounts/";
            MonoAccountsV1EndPoint = url + "/v1/accounts/";
            MonoSecKey = ApiKey;
        }
        private HttpWebRequest CreateWebRequest(string RequestType, string? url, byte[] payload)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = RequestType;
            httpWebRequest.Headers["mono-sec-key"] = MonoSecKey;

            if (payload != null)
            {
                httpWebRequest.ContentLength = payload.Length;
                Stream _RequestStream = httpWebRequest.GetRequestStream();
                _RequestStream.Write(payload, 0, payload.Length);
                _RequestStream.Close();
            }

            return httpWebRequest;
        }


        public OResponse Authentication(OMono.AuthRequest _Request)
        {
            try
            {
                string request_url = MonoAccountEndPoint + "auth";
                AuthRequest authrequest = new AuthRequest(_Request.code);
                string request_json = JsonConvert.SerializeObject(authrequest);
                byte[] bytes = Encoding.ASCII.GetBytes(request_json);

                var httpwebrequest = CreateWebRequest("POST", request_url, bytes);
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();
                using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {
                    string _Response = streamReader.ReadToEnd();
                    AuthResponse response = JsonConvert.DeserializeObject<AuthResponse>(_Response);
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, response, "BNPL0200", Resource.BNPL0200);
                        #endregion                        
                    }
                    else
                    {
                        var _ResponseItem = new
                        {
                            Message = response.Message
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _ResponseItem, "BNPL0200", Resource.BNPL0200);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Authentication", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, Resource.BNPL0500);
            }
        }

        public OResponse GetAccount(OMono.AccountRequest _Request)
        {
            try
            {
                if (!String.IsNullOrEmpty(_Request.id))
                {
                    string AuthGetAccountUrl = MonoAccountsEndPoint + _Request.id;
                    var AuthGetAccountWebRequest = CreateWebRequest("GET", AuthGetAccountUrl, null);
                    HttpWebResponse _AuthGetAccountHttpWebResponse = (HttpWebResponse)AuthGetAccountWebRequest.GetResponse();
                    using (var AuthGetAccountStreamReader = new StreamReader(_AuthGetAccountHttpWebResponse.GetResponseStream()))
                    {
                        string _AuthGetAccountResponseContent = AuthGetAccountStreamReader.ReadToEnd();
                        AccountResponse _AuthGetAccountResponse = JsonConvert.DeserializeObject<AccountResponse>(_AuthGetAccountResponseContent);
                        if (_AuthGetAccountHttpWebResponse.StatusCode == HttpStatusCode.OK)
                        {
                            var Response = GetUserIdentityCore(_Request.id);
                            _AuthGetAccountResponse.account.BVN = Response.BVN;
                            return HCoreHelper.SendResponse(_Request.userReference, ResponseStatus.Success, _AuthGetAccountResponse, "BNPL0200", Resource.BNPL0200);
                        }
                        else
                        {
                            AuthResponse response = JsonConvert.DeserializeObject<AuthResponse>(_AuthGetAccountResponseContent);
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.userReference, ResponseStatus.Error, response.Message, "BNPL0500", _AuthGetAccountResponse.Message);
                            #endregion
                        }
                    }
                }
                return HCoreHelper.SendResponse(_Request.userReference, ResponseStatus.Error, null, Resource.BNPL0201);
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAccount", _Exception, _Request.userReference);
                return HCoreHelper.SendResponse(_Request.userReference, ResponseStatus.Error, null, Resource.BNPL0500);
            }
        }

        public AccountResponse GetAccountCore(string MonoReferenceNumber)
        {
            try
            {
                if (!String.IsNullOrEmpty(MonoReferenceNumber))
                {
                    string request_url = MonoAccountsEndPoint + MonoReferenceNumber;
                    var httpwebrequest = CreateWebRequest("GET", request_url, null);
                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();
                    using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                    {
                        string _Response = streamReader.ReadToEnd();
                        if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                        {
                            AccountResponse response = JsonConvert.DeserializeObject<AccountResponse>(_Response);
                            var Response = GetUserIdentityCore(MonoReferenceNumber);
                            if (HostEnvironment != HostEnvironmentType.Live)
                            {
                                response.account.BVN = Response.BVN;
                            }
                            if (HostEnvironment == HostEnvironmentType.Live)
                            {
                                response.account.BVN = Response.BVN;
                            }
                            return response;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAccountCore", _Exception, null);
                return null;
            }
        }

        public GetUserIdentityResponse GetUserIdentity(string MonoReferenceNumber)
        {
            try
            {
                if (!String.IsNullOrEmpty(MonoReferenceNumber))
                {
                    string request_url = MonoIdentityEndPoint + MonoReferenceNumber + "/identity";
                    var httpwebrequest = CreateWebRequest("GET", request_url, null);
                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();
                    using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                    {
                        string _Response = streamReader.ReadToEnd();
                        if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                        {
                            GetUserIdentityResponse response = JsonConvert.DeserializeObject<GetUserIdentityResponse>(_Response);
                            //if (HostEnvironment != HostEnvironmentType.Live)
                            //{
                            //    response.BVN = "22146426472";
                            //}
                            return response;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAccount", _Exception, null);
                return null;
            }
        }




        public OResponse HCoreXMonoAuth(OMono.AuthRequest _Request)
        {
            try
            {
                string AuthUrl = MonoAccountEndPoint + "auth";
                AuthRequest AuthRequest = new AuthRequest(_Request.code);
                string AuthRequestJson = JsonConvert.SerializeObject(AuthRequest);
                byte[] AuthRequest_Bytes = Encoding.ASCII.GetBytes(AuthRequestJson);
                var AuthWebRequest = CreateWebRequest("POST", AuthUrl, AuthRequest_Bytes);
                HttpWebResponse _AuthHttpWebResponse = (HttpWebResponse)AuthWebRequest.GetResponse();
                using (var _AuthStreamReader = new StreamReader(_AuthHttpWebResponse.GetResponseStream()))
                {
                    string _AuthStreamReaderResponse = _AuthStreamReader.ReadToEnd();
                    AuthResponse _AuthResponse = JsonConvert.DeserializeObject<AuthResponse>(_AuthStreamReaderResponse);
                    if (_AuthHttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        if (!string.IsNullOrEmpty(_AuthResponse.Id))
                        {
                            string MonoReferenceNumber = _AuthResponse.Id;
                            #region Get Mono Account
                            try
                            {
                                string AuthGetAccountUrl = MonoAccountsEndPoint + MonoReferenceNumber;
                                var AuthGetAccountWebRequest = CreateWebRequest("GET", AuthGetAccountUrl, null);
                                HttpWebResponse _AuthGetAccountHttpWebResponse = (HttpWebResponse)AuthGetAccountWebRequest.GetResponse();
                                using (var AuthGetAccountStreamReader = new StreamReader(_AuthGetAccountHttpWebResponse.GetResponseStream()))
                                {
                                    string _AuthGetAccountResponseContent = AuthGetAccountStreamReader.ReadToEnd();
                                    AccountResponse _AuthGetAccountResponse = JsonConvert.DeserializeObject<AccountResponse>(_AuthGetAccountResponseContent);
                                    if (_AuthGetAccountHttpWebResponse.StatusCode == HttpStatusCode.OK)
                                    {
                                        #region Get Account Identity
                                        try
                                        {
                                            string AuthIdentityUrl = MonoIdentityEndPoint + MonoReferenceNumber + "/identity";
                                            var AuthIdentityHttpWebRequest = CreateWebRequest("GET", AuthIdentityUrl, null);
                                            HttpWebResponse AuthIdentityHttpWebResponse = (HttpWebResponse)AuthIdentityHttpWebRequest.GetResponse();
                                            using (var AuthIdentityStreamReader = new StreamReader(AuthIdentityHttpWebResponse.GetResponseStream()))
                                            {
                                                string AuthIdentityResponseContent = AuthIdentityStreamReader.ReadToEnd();
                                                GetUserIdentityResponse AuthIdentityResponse = JsonConvert.DeserializeObject<GetUserIdentityResponse>(AuthIdentityResponseContent);
                                                if (AuthIdentityHttpWebResponse.StatusCode == HttpStatusCode.OK)
                                                {
                                                    _AuthGetAccountResponse.account.BVN = AuthIdentityResponse.BVN;
                                                    _AuthGetAccountResponse.account.MonoReference = MonoReferenceNumber;
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AuthGetAccountResponse.account, "BNPL0200", "Account validated successfully");
                                                }
                                                else
                                                {
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPL0500", AuthIdentityResponse.Message);
                                                }
                                            }
                                        }
                                        catch (Exception _Exception)
                                        {
                                            HCoreHelper.LogException("Authentication-AuthIdentity", _Exception, _Request.UserReference);
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "", "Unable to authenticate your account. Please try after some time");
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPL0500", _AuthGetAccountResponse.Message);
                                    }
                                }
                            }
                            catch (Exception _Exception)
                            {
                                HCoreHelper.LogException("Authentication-AuthGetAccount", _Exception, _Request.UserReference);
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "", "Unable to authenticate your account. Please try after some time");
                            }
                            #endregion
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "", "Unable to authenticate your bank reference. Please try after some time");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "", _AuthResponse.Message);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Authentication", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "Unable to authenticate your bank account. Please try after some time");
            }
        }



        public GetUserIdentityResponse GetUserIdentityCore(string MonorefernceNumber)
        {
            try
            {
                if (!String.IsNullOrEmpty(MonorefernceNumber))
                {
                    string AuthIdentityUrl = MonoIdentityEndPoint + MonorefernceNumber + "/identity";
                    var AuthIdentityHttpWebRequest = CreateWebRequest("GET", AuthIdentityUrl, null);
                    HttpWebResponse AuthIdentityHttpWebResponse = (HttpWebResponse)AuthIdentityHttpWebRequest.GetResponse();
                    using (var AuthIdentityStreamReader = new StreamReader(AuthIdentityHttpWebResponse.GetResponseStream()))
                    {
                        string AuthIdentityResponseContent = AuthIdentityStreamReader.ReadToEnd();
                        GetUserIdentityResponse response = JsonConvert.DeserializeObject<GetUserIdentityResponse>(AuthIdentityResponseContent);
                        if (AuthIdentityHttpWebResponse.StatusCode == HttpStatusCode.OK)
                        {
                            return response;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetUserIdentityCore", _Exception, null);
                return null;
            }
        }

        public OResponse GetAccountStatement(OMono.AccountRequest _Request)
        {
            try
            {
                if (!String.IsNullOrEmpty(_Request.id))
                {
                    string request_url = MonoAccountsEndPoint + _Request.id + "/statement?period=" + period;
                    var httpwebrequest = CreateWebRequest("GET", request_url, null);
                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

                    using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                    {

                        string _Response = streamReader.ReadToEnd();
                        if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                        {
                            AccountStatementResponse response = JsonConvert.DeserializeObject<AccountStatementResponse>(_Response);
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.userReference, ResponseStatus.Success, response, "BNPL0200", Resource.BNPL0200);
                            #endregion
                        }
                        else
                        {
                            AuthResponse response = JsonConvert.DeserializeObject<AuthResponse>(_Response);
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.userReference, ResponseStatus.Error, response.Message, "BNPL0500", Resource.BNPL0500);
                            #endregion
                        }
                    }
                }
                return HCoreHelper.SendResponse(_Request.userReference, ResponseStatus.Error, null, Resource.BNPL0201);
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("AccountStatementResponse", _Exception, _Request.userReference);
                return HCoreHelper.SendResponse(_Request.userReference, ResponseStatus.Error, null, Resource.BNPL0500);
            }
        }
        public AccountStatementResponse GetAccountStatement(string MonoReferenceNumber)
        {
            try
            {
                if (!String.IsNullOrEmpty(MonoReferenceNumber))
                {
                    string request_url = MonoAccountsEndPoint + MonoReferenceNumber + "/statement?period=" + period;
                    var httpwebrequest = CreateWebRequest("GET", request_url, null);
                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

                    using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                    {
                        string _Response = streamReader.ReadToEnd();
                        if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                        {
                            return JsonConvert.DeserializeObject<AccountStatementResponse>(_Response);
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("AccountStatementResponse", _Exception, null);
                return null;
            }
        }
        public OResponse Income(OMono.AccountRequest _Request)
        {
            try
            {
                if (!String.IsNullOrEmpty(_Request.id))
                {
                    string request_url = MonoAccountsV1EndPoint + _Request.id + "/income";
                    var httpwebrequest = CreateWebRequest("GET", request_url, null);
                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();

                    using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                    {

                        string _Response = streamReader.ReadToEnd();
                        Console.WriteLine(_Response);
                        if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                        {
                            IncomeResponse response = JsonConvert.DeserializeObject<IncomeResponse>(_Response);
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.userReference, ResponseStatus.Success, response, "BNPL0200", Resource.BNPL0200);
                            #endregion
                        }
                        else
                        {
                            AuthResponse response = JsonConvert.DeserializeObject<AuthResponse>(_Response);
                            return HCoreHelper.SendResponse(_Request.userReference, ResponseStatus.Error, response.Message, "BNPL0500", Resource.BNPL0500);
                        }
                    }
                }
                return HCoreHelper.SendResponse(_Request.userReference, ResponseStatus.Error, null, Resource.BNPL0201);
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Income", _Exception, _Request.userReference);
                return HCoreHelper.SendResponse(_Request.userReference, ResponseStatus.Error, null, Resource.BNPL0500);
            }
        }

        public IncomeResponse GetIncome(string MonoReferenceNumber)
        {
            try
            {
                string request_url = MonoAccountsV1EndPoint + MonoReferenceNumber + "/income";
                var httpwebrequest = CreateWebRequest("GET", request_url, null);
                HttpWebResponse _HttpWebResponse = (HttpWebResponse)httpwebrequest.GetResponse();
                using (var streamReader = new StreamReader(_HttpWebResponse.GetResponseStream()))
                {
                    string _Response = streamReader.ReadToEnd();
                    if (_HttpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        IncomeResponse response = JsonConvert.DeserializeObject<IncomeResponse>(_Response);
                        return response;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Income", _Exception, null);
                return null;
            }
        }



    }
    #endregion
}
