//==================================================================================
// FileName: OAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.Integration.Mono
{
    #region Mono Authentication, Account Details, Bank Statements, Income Details
    public class AuthRequest
    {
        public AuthRequest(string Code)
        {
            code = Code;
        }

        public string? code { get; set; }
    }
    public class AuthResponse
    {
        public string? Id { get; set; }
        public string? Message { get; set; }
    }
    public class AccountRequest
    {
        public string? Id { get; set; }
        public string? Mono_Sec_Key { get; set; }
    }
    public class AccountResponse
    {
        public string? Id { get; set; }
        public string? Message { get; set; }
        public Meta meta { get; set; }
        public Account account { get; set; }
    }
    public class Meta
    {
        public string? Data_Status { get; set; }
        public string? Auth_Method { get; set; }
    }
    public class Account
    {
        public string? MonoReference { get; set; }
        public string? Id { get; set; }
        public string? Name { get; set; }
        public string? Currency { get; set; }
        public string? Type { get; set; }
        public string? AccountNumber { get; set; }
        public long? Balance { get; set; }
        public Institution Institution { get; set; }
        public string? BVN { get; set; }
    }
    public class Institution
    {
        public string? Name { get; set; }
        public string? BankCode { get; set; }
        public string? Type { get; set; }
    }

    public class AccountStatementRequest
    {
        public string? Id { get; set; }
        public string? Period { get; set; }
    }
    public class AccountStatementResponse
    {
        public MetaData meta { get; set; }
        public List<AccountStatement> data { get; set; }
    }
    public class AccountStatement
    {
        public string? Id { get; set; }
        public string? Type { get; set; }
        public long Amount { get; set; }
        public string? Narration { get; set; }
        public string? Date { get; set; }
        public long? Balance { get; set; }
    }
    public class MetaData
    {
        public long Count { get; set; }
    }
    public class IncomeRequest
    {
        public string? Id { get; set; }
        public string? Mono_Sec_Key { get; set; }
    }
    public class IncomeResponse
    {
        public long? Average_Income { get; set; }
        public long? Monthly_Income { get; set; }
        public long? Yearly_Income { get; set; }
        public long? Income_Source { get; set; }
    }

    public class OMono
    {
        public class AuthRequest
        {
            public string? code { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class AccountRequest
        {
            public string? id { get; set; }
            public OUserReference userReference { get; set; }
        }
    }
    #endregion
}
