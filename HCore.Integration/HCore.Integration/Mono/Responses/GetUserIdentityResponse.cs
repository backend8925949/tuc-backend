//==================================================================================
// FileName: GetUserIdentityResponse.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.Integration.Mono.Responses
{
    public class GetUserIdentityResponse
    {
        public string? Id { get; set; }
        public string? Message { get; set; }
        public string? FullName { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public string? Gender { get; set; }
        public string? BVN { get; set; }
        public string? MaritalStatus { get; set; }
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }
        public string? Created_At { get; set; }
        public string? Updated_At { get; set; }

        public GetUserIdentityResponse()
        {
        }
    }
}
