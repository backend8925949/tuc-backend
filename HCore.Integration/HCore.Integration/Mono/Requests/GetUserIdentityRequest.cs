//==================================================================================
// FileName: GetUserIdentityRequest.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.Integration.Mono.Requests
{
    public class GetUserIdentityRequest
    {
        public class IdentityRequest
        {
            public string? Id { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public GetUserIdentityRequest()
        {
        }
    }
}
