//==================================================================================
// FileName: ManageThankUCashGatewayV2.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using Akka.Actor;
using HCore.Helper;
using HCore.ThankUCash.Gateway.FrameworkV2;
using HCore.ThankUCash.Gateway.IntegrationV2;
using HCore.ThankUCash.Gateway.ObjectV2;
namespace HCore.ThankUCash.Gateway
{
    public class ManageThankUCashGatewayV2
    {
        FrameworkThankUCashGateway _FrameworkThankUCashGateway;
        FrameworkOperations _FrameworkOperations;
        FrameworkRedeem _FrameworkRedeem;
        //FrameworkReward _FrameworkReward;
        FrameworkSoftcom _FrameworkSoftcom;
        public OResponse SaveProduct(OThankUGateway.Request _Request)
        {
            _FrameworkSoftcom = new FrameworkSoftcom();
            return _FrameworkSoftcom.SaveProduct(_Request);
        }

        public OResponse NotifySoftComTransaction(OThankUGateway.SCNotifyProductPaymentRequest _Request)
        {
            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.NotifySoftComTransaction(_Request);
        }

        public OResponse CoralPay_SaveTransactions(OThankUGateway.CoralPay.Request _Request)
        {
            var _TransactionPostProcessActor = ActorSystem.Create("ActorCoralPayTransactionProcessor");
            var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<ActorCoralPayTransactionProcessor>("ActorCoralPayTransactionProcessor");
            _TransactionPostProcessActorNotify.Tell(_Request);
            return HCoreHelper.SendResponse(null, HCoreConstant.ResponseStatus.Success, null, "200","Request queued");
        }

        //Operations - START
        public OResponse GetBalance(OThankUGateway.Request _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.GetBalance(_Request);
        }
        // Operations - End

        public OResponse ConnectMerchant(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.ConnectMerchant(_Request);
        }
        public OResponse RegisterUser(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.RegisterUser(_Request);
        }
        public OResponse ResetUssdUserPin(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.ResetUssdUserPin(_Request);
        }
        public OResponse UpdateUssdUser(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.UpdateUssdUser(_Request);
        }
        public OResponse RegisterUssdUser(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.RegisterUssdUser(_Request);
        }
        public OResponse RemoveMerchant(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.RemoveMerchant(_Request);
        }

        public OResponse GetConfiguration(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.GetConfiguration(_Request);
        }

        public OResponse CreditReward(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.Reward(_Request);
        }
        //public OResponse RewardNumber(OThankUGateway.Request _Request)
        //{

        //    _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
        //    return _FrameworkThankUCashGateway.RewardNumber(_Request);
        //}
        //public OResponse RedeemNumber(OThankUGateway.Request _Request)
        //{

        //    _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
        //    return _FrameworkThankUCashGateway.RedeemNumber(_Request);
        //}

        public OResponse ValidateAccount(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.ValidateAccount(_Request);
        }
        public OResponse Change(OThankUGateway.Request _Request)
        {
            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.Change(_Request);
        }


        public OResponse InitializeTransaction(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.InitializeTransaction(_Request);
        }
        public OResponse SettleTransaction(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.SettleTransaction(_Request, false, null);
        }

        public OResponse NotifyTransaction(OThankUGateway.NotifyPayment.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.NotifyTransaction(_Request);
        }
        public OResponse CancelTransaction(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.CancelTransaction(_Request);
        }
        public OResponse NotifyFailedTransaction(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.NotifyFailedTransaction(_Request);
        }




        public OResponse NotifySettlements(OThankUGateway.NotifySettlement _Request)
        {
            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.NotifySettlements(_Request);
        }
        public OResponse MerchantReward(OThankUGateway.Request _Request)
        {
            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.MerchantReward(_Request);
        }
        public OResponse MerchantRedeem(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.MerchantRedeem(_Request);
        }
        public OResponse ClaimReward(OThankUGateway.Request _Request)
        {
            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.ClaimReward(_Request);
        }

        //REDEEM - START
        public OResponse Redeem(OThankUGateway.Request _Request)
        {
            _FrameworkRedeem = new FrameworkRedeem();
            return _FrameworkRedeem.Redeem(_Request);
        }
        public OResponse ConfirmRedeem(OThankUGateway.Request _Request)
        {
            _FrameworkRedeem = new FrameworkRedeem();
            return _FrameworkRedeem.ConfirmRedeem(_Request);
        }
        //REDEEM - END





        FrameworkMerchant _FrameworkMerchant;
        public OResponse SaveMerchant(OAccounts.Merchant.Onboarding _Request)
        {
            _FrameworkMerchant = new FrameworkMerchant();
            return _FrameworkMerchant.SaveMerchant(_Request);
        }
        public OResponse SaveStore(OAccounts.Store.Request _Request)
        {
            _FrameworkMerchant = new FrameworkMerchant();
            return _FrameworkMerchant.SaveStore(_Request);
        }
        public OResponse SaveTerminal(OAccounts.Terminal.Request _Request)
        {
            _FrameworkMerchant = new FrameworkMerchant();
            return _FrameworkMerchant.SaveTerminal(_Request);
        }
    }
}
 