//==================================================================================
// FileName: HelperGateway.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.ThankUCash.Gateway.Helper
{
    public static class HelperGateway
    {
       public static class SendGridEmailTemplateIds
        {
            public const string RewardEmail = "d-0e22a726fc3f4fd1aa72cbfee2fee6f1";
            public const string PendingRewardEmail = "d-dd04bdb1ea4e44478a5606e0eb85d5e2";
            public const string RedeemEmail = "d-1635d8f52c664ed285aae77a553d4046";
        }
    }
}
