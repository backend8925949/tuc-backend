//==================================================================================
// FileName: ManageGatewayRewardV1.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.Gateway.Framework;
//using HCore.ThankUCash.Gateway.Integration;
using HCore.ThankUCash.Gateway.Object;
namespace HCore.ThankUCash.Gateway
{
    public class ManageGatewayRewardV1
    {
        FrameworkReward _FrameworkReward;

        public OResponse InitializeTransaction(OThankUGateway.Request _Request)
        {
            _FrameworkReward = new FrameworkReward();
            return _FrameworkReward.InitializeTransaction(_Request);
        }
        public OResponse SettleTransaction(OThankUGateway.Request _Request)
        {
            _FrameworkReward = new FrameworkReward();
            return _FrameworkReward.SettleTransaction(_Request);
        }
        public OResponse NotifyTransaction(OThankUGateway.NotifyPayment.Request _Request)
        {
            _FrameworkReward = new FrameworkReward();
            return _FrameworkReward.NotifyTransaction(_Request);
        }
        //public OResponse NotifySoftComTransaction(OThankUGateway.SCNotifyProductPaymentRequest _Request)
        //{
        //    _FrameworkReward = new FrameworkReward();
        //    return _FrameworkReward.NotifySoftComTransaction(_Request);
        //}
        public OResponse CancelTransaction(OThankUGateway.Request _Request)
        {
            _FrameworkReward = new FrameworkReward();
            return _FrameworkReward.CancelTransaction(_Request);
        }
        public OResponse NotifyFailedTransaction(OThankUGateway.Request _Request)
        {
            _FrameworkReward = new FrameworkReward();
            return _FrameworkReward.NotifyFailedTransaction(_Request);
        }
        public OResponse NotifySettlements(OThankUGateway.NotifySettlement _Request)
        {
            _FrameworkReward = new FrameworkReward();
            return _FrameworkReward.NotifySettlements(_Request);
        }


    }
}
