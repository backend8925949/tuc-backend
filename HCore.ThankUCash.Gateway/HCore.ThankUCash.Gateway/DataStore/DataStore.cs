//==================================================================================
// FileName: DataStore.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Linq;
using static HCore.Helper.HCoreConstant;
using HCore.Helper;
using System.Collections.Generic;
using HCore.Data;
using HCore.Data.Models;
using HCore.ThankUCash.Gateway.Core;

namespace HCore.ThankUCash.Gateway.DataStore
{
    public class DataStore
    {
        internal static DateTime MerchantsSyncDate = new DateTime(2010, 1, 1, 0, 0, 0);
        internal static DateTime StoresSyncDate = new DateTime(2010, 1, 1, 0, 0, 0);
        internal static DateTime CashiersSyncDate = new DateTime(2010, 1, 1, 0, 0, 0);
        internal static DateTime TerminalsSyncDate = new DateTime(2010, 1, 1, 0, 0, 0);
        internal static DateTime PartnersSyncDate = new DateTime(2010, 1, 1, 0, 0, 0);
        internal static DateTime ConfigurationSyncDate = new DateTime(2010, 1, 1, 0, 0, 0);
        internal static DateTime AccountConfigurationsSyncDate = new DateTime(2010, 1, 1, 0, 0, 0);
        internal static DateTime TerminalProductSyncDate = new DateTime(2010, 1, 1, 0, 0, 0);
        public static List<ODataStore.Account.Customer> Customers = new List<ODataStore.Account.Customer>();
        public static List<ODataStore.Account.Merchant> Merchants = new List<ODataStore.Account.Merchant>();
        public static List<ODataStore.Account.Store> Stores = new List<ODataStore.Account.Store>();
        public static List<ODataStore.Account.Cashier> Cashiers = new List<ODataStore.Account.Cashier>();
        public static List<ODataStore.Account.Terminal> Terminals = new List<ODataStore.Account.Terminal>();
        public static List<ODataStore.Account.Partner> Partners = new List<ODataStore.Account.Partner>();
        public static List<ODataStore.Configuration.Base> Configurations = new List<ODataStore.Configuration.Base>();
        public static List<ODataStore.Configuration.Account> AccountConfigurations = new List<ODataStore.Configuration.Account>();
        public static List<ODataStore.TerminalProduct> TerminalProducts = new List<ODataStore.TerminalProduct>();
        internal static void RefreshDataStore()
        {
            try
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    //#region Update Terminal Products
                    //List<ODataStore.TerminalProduct> UpdatedTerminalProducts = (from x in _HCoreContext.TUCTerminalProduct
                    //                                                            where (x.CreateDate > TerminalProductSyncDate || x.ModifyDate > TerminalProductSyncDate)
                    //                                                            select new ODataStore.TerminalProduct
                    //                                                            {
                    //                                                                ReferenceId = x.Id,
                    //                                                                AccountId = x.AccountId,
                    //                                                                ReferenceNumber = x.ReferenceNumber,
                    //                                                                RewardPercentage = x.RewardPercentage,
                    //                                                                StatusId = x.StatusId,
                    //                                                            }).ToList();
                    //if (UpdatedTerminalProducts.Count() > 0)
                    //{
                    //    foreach (var UpdatedTerminalProduct in UpdatedTerminalProducts)
                    //    {
                    //        var ExistingCheck = TerminalProducts.Where(x => x.ReferenceId == UpdatedTerminalProduct.ReferenceId).FirstOrDefault();
                    //        if (ExistingCheck != null)
                    //        {
                    //            ExistingCheck.ReferenceId = UpdatedTerminalProduct.ReferenceId;
                    //            ExistingCheck.AccountId = UpdatedTerminalProduct.AccountId;
                    //            ExistingCheck.ReferenceNumber = UpdatedTerminalProduct.ReferenceNumber;
                    //            ExistingCheck.RewardPercentage = UpdatedTerminalProduct.RewardPercentage;
                    //            ExistingCheck.StatusId = UpdatedTerminalProduct.StatusId;
                    //        }
                    //        else
                    //        {
                    //            TerminalProducts.Add(UpdatedTerminalProduct);
                    //        }
                    //    }
                    //}
                    //TerminalProductSyncDate = HCoreHelper.GetGMTDateTime();
                    //#endregion
                    #region Update Configurations
                    List<ODataStore.Configuration.Base> UpdatedConfigurations = (from x in _HCoreContext.HCCoreCommon
                                                                                 where x.StatusId == HelperStatus.Default.Active
                                                                                 && x.TypeId == HelperType.Configuration
                                                                                 && (x.CreateDate > ConfigurationSyncDate || x.ModifyDate > ConfigurationSyncDate)
                                                                                 select new ODataStore.Configuration.Base
                                                                                 {
                                                                                     ReferenceId = x.Id,
                                                                                     SystemName = x.SystemName,
                                                                                     DefaultValue = x.Value,
                                                                                     TypeCode = x.Helper.SystemName,
                                                                                     TypeName = x.Helper.Name,
                                                                                     ActiveValue = x.InverseParent.Where(a => a.ParentId == x.Id && a.SubParentId == null && a.StatusId == HelperStatus.Default.Active && a.TypeId == HelperType.ConfigurationValue).Select(a => a.Value).FirstOrDefault(),
                                                                                     StatusId = x.StatusId,
                                                                                 }).ToList();
                    if (UpdatedConfigurations.Count() > 0)
                    {
                        foreach (var UpdatedConfiguration in UpdatedConfigurations)
                        {
                            if (string.IsNullOrEmpty(UpdatedConfiguration.ActiveValue))
                            {
                                UpdatedConfiguration.ActiveValue = UpdatedConfiguration.DefaultValue;
                            }
                            var ExistingCheck = Configurations.Where(x => x.ReferenceId == UpdatedConfiguration.ReferenceId).FirstOrDefault();
                            if (ExistingCheck != null)
                            {
                                ExistingCheck.ReferenceId = UpdatedConfiguration.ReferenceId;
                                ExistingCheck.SystemName = UpdatedConfiguration.SystemName;
                                ExistingCheck.DefaultValue = UpdatedConfiguration.DefaultValue;
                                ExistingCheck.TypeCode = UpdatedConfiguration.TypeCode;
                                ExistingCheck.TypeName = UpdatedConfiguration.TypeName;
                                ExistingCheck.ActiveValue = UpdatedConfiguration.ActiveValue;
                                ExistingCheck.StatusId = UpdatedConfiguration.StatusId;
                            }
                            else
                            {
                                Configurations.Add(UpdatedConfiguration);
                            }
                        }
                    }
                    ConfigurationSyncDate = HCoreHelper.GetGMTDateTime();
                    #endregion
                    #region Update Account Configurations
                    List<ODataStore.Configuration.Account> UpdatedAccountConfigurations = (from x in _HCoreContext.HCUAccountParameter
                                                                                           where x.StatusId == HelperStatus.Default.Active
                                                                                        && x.TypeId == HelperType.ConfigurationValue
                                                                                        && x.CommonId != null
                                                                                        && x.AccountId != null
                                                                                        && (x.CreateDate > AccountConfigurationsSyncDate || x.ModifyDate > AccountConfigurationsSyncDate)
                                                                                           orderby x.Id descending
                                                                                           select new ODataStore.Configuration.Account
                                                                                           {
                                                                                               AccountId = (long)x.AccountId,
                                                                                               ConfigurationId = (long)x.CommonId,
                                                                                               Value = x.Value,
                                                                                               StatusId = x.StatusId,
                                                                                               TypeCode = x.Helper.SystemName,
                                                                                               TypeName = x.Helper.Name,
                                                                                           }).ToList();
                    if (UpdatedAccountConfigurations.Count() > 0)
                    {
                        foreach (var UpdatedAccountConfiguration in UpdatedAccountConfigurations)
                        {
                            var ExistingCheck = AccountConfigurations.Where(x => x.AccountId == UpdatedAccountConfiguration.AccountId && x.ConfigurationId == UpdatedAccountConfiguration.ConfigurationId).FirstOrDefault();
                            if (ExistingCheck != null)
                            {
                                ExistingCheck.AccountId = UpdatedAccountConfiguration.AccountId;
                                ExistingCheck.ConfigurationId = UpdatedAccountConfiguration.ConfigurationId;
                                ExistingCheck.Value = UpdatedAccountConfiguration.Value;
                                ExistingCheck.TypeCode = UpdatedAccountConfiguration.TypeCode;
                                ExistingCheck.TypeName = UpdatedAccountConfiguration.TypeName;
                                ExistingCheck.StatusId = UpdatedAccountConfiguration.StatusId;
                            }
                            else
                            {
                                AccountConfigurations.Add(UpdatedAccountConfiguration);
                            }
                        }
                    }
                    AccountConfigurationsSyncDate = HCoreHelper.GetGMTDateTime();
                    #endregion
                    #region Update Merchants
                    List<ODataStore.Account.Merchant> UpdatedMerchants = (from x in _HCoreContext.HCUAccount
                                                                          where x.AccountTypeId == Helpers.UserAccountType.Merchant && (x.CreateDate > MerchantsSyncDate || x.ModifyDate > MerchantsSyncDate)
                                                                          select new ODataStore.Account.Merchant
                                                                          {
                                                                              ReferenceId = x.Id,
                                                                              DisplayName = x.DisplayName,
                                                                              AccountCode = x.AccountCode,
                                                                              OwnerId = x.OwnerId,
                                                                              OwnerAccountTypeId = x.Owner.AccountTypeId,
                                                                              StatusId = x.StatusId,
                                                                              AccountPercentage = x.AccountPercentage,
                                                                              //  Balance = (double)x.HCUAccountBalanceAccount.FirstOrDefault().Balance,
                                                                              Balance = 0,
                                                                              PrimaryCategoryId = x.PrimaryCategoryId,
                                                                              SecondaryCategoryId = x.SecondaryCategoryId,
                                                                          }).ToList();
                    if (UpdatedMerchants.Count() > 0)
                    {
                        foreach (var UpdatedMerchant in UpdatedMerchants)
                        {
                            if (UpdatedMerchant.Balance == null)
                            {
                                UpdatedMerchant.Balance = 0;
                            }
                            var ExistingCheck = Merchants.Where(x => x.ReferenceId == UpdatedMerchant.ReferenceId).FirstOrDefault();
                            if (ExistingCheck != null)
                            {
                                ExistingCheck.DisplayName = UpdatedMerchant.DisplayName;
                                ExistingCheck.AccountCode = UpdatedMerchant.AccountCode;
                                ExistingCheck.StatusId = UpdatedMerchant.StatusId;
                                ExistingCheck.AccountPercentage = UpdatedMerchant.AccountPercentage;
                                ExistingCheck.PrimaryCategoryId = UpdatedMerchant.PrimaryCategoryId;
                                ExistingCheck.SecondaryCategoryId = UpdatedMerchant.SecondaryCategoryId;
                            }
                            else
                            {
                                Merchants.Add(UpdatedMerchant);
                            }
                        }
                    }
                    MerchantsSyncDate = HCoreHelper.GetGMTDateTime();
                    #endregion
                    #region Update Stores
                    List<ODataStore.Account.Store> UpdatedStores = (from x in _HCoreContext.HCUAccount
                                                                    where x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore && (x.CreateDate > StoresSyncDate || x.ModifyDate > StoresSyncDate)
                                                                    select new ODataStore.Account.Store
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        MerchantId = x.OwnerId,
                                                                        DisplayName = x.DisplayName,
                                                                        Address = x.Address,
                                                                        Latitude = x.Latitude,
                                                                        Longitude = x.Longitude,
                                                                        StatusId = x.StatusId,
                                                                    }).ToList();
                    if (UpdatedStores.Count() > 0)
                    {
                        foreach (var UpdatedStore in UpdatedStores)
                        {
                            var ExistingCheck = Stores.Where(x => x.ReferenceId == UpdatedStore.ReferenceId).FirstOrDefault();
                            if (ExistingCheck != null)
                            {
                                ExistingCheck.MerchantId = UpdatedStore.MerchantId;
                                ExistingCheck.DisplayName = UpdatedStore.DisplayName;
                                ExistingCheck.Address = UpdatedStore.Address;
                                ExistingCheck.Latitude = UpdatedStore.Latitude;
                                ExistingCheck.Longitude = UpdatedStore.Longitude;
                                ExistingCheck.StatusId = UpdatedStore.StatusId;
                            }
                            else
                            {
                                Stores.Add(UpdatedStore);
                            }
                        }
                    }
                    StoresSyncDate = HCoreHelper.GetGMTDateTime();
                    #endregion
                    #region Update Cashiers
                    List<ODataStore.Account.Cashier> UpdatedCashiers = (from x in _HCoreContext.HCUAccount
                                                                        where x.AccountTypeId == Helpers.UserAccountType.MerchantCashier && (x.CreateDate > CashiersSyncDate || x.ModifyDate > CashiersSyncDate)
                                                                        select new ODataStore.Account.Cashier
                                                                        {
                                                                            ReferenceId = x.Id,
                                                                            MerchantId = x.Owner.OwnerId,
                                                                            StoreId = x.OwnerId,
                                                                            CashierId = x.DisplayName,
                                                                            StatusId = x.StatusId,
                                                                        }).ToList();
                    if (UpdatedCashiers.Count() > 0)
                    {
                        foreach (var UpdatedCashier in UpdatedCashiers)
                        {
                            var ExistingCheck = Cashiers.Where(x => x.ReferenceId == UpdatedCashier.ReferenceId).FirstOrDefault();
                            if (ExistingCheck != null)
                            {
                                ExistingCheck.MerchantId = UpdatedCashier.MerchantId;
                                ExistingCheck.StoreId = UpdatedCashier.StoreId;
                                ExistingCheck.CashierId = UpdatedCashier.CashierId;
                                ExistingCheck.StatusId = UpdatedCashier.StatusId;
                            }
                            else
                            {
                                Cashiers.Add(UpdatedCashier);
                            }
                        }
                    }
                    CashiersSyncDate = HCoreHelper.GetGMTDateTime();
                    #endregion
                    #region Update Terminals
                    List<ODataStore.Account.Terminal> UpdatedTerminals = (from x in _HCoreContext.TUCTerminal
                                                                          where (x.CreateDate > TerminalsSyncDate || x.ModifyDate > TerminalsSyncDate)
                                                                          select new ODataStore.Account.Terminal
                                                                          {
                                                                              ReferenceId = x.Id,
                                                                              TerminalId = x.DisplayName,
                                                                              BankId = x.AcquirerId,
                                                                              PtspId = x.ProviderId,
                                                                              MerchantId = x.MerchantId,
                                                                              StoreId = x.StoreId,
                                                                              StatusId = (int)x.StatusId,
                                                                          }).ToList();
                    if (UpdatedTerminals.Count() > 0)
                    {
                        foreach (var UpdatedTerminal in UpdatedTerminals)
                        {
                            var ExistingCheck = Terminals.Where(x => x.ReferenceId == UpdatedTerminal.ReferenceId).FirstOrDefault();
                            if (ExistingCheck != null)
                            {
                                ExistingCheck.TerminalId = UpdatedTerminal.TerminalId;
                                ExistingCheck.StoreId = UpdatedTerminal.StoreId;
                                ExistingCheck.BankId = UpdatedTerminal.BankId;
                                ExistingCheck.PtspId = UpdatedTerminal.PtspId;
                                ExistingCheck.MerchantId = UpdatedTerminal.MerchantId;
                                ExistingCheck.StoreId = UpdatedTerminal.StoreId;
                                ExistingCheck.StatusId = UpdatedTerminal.StatusId;
                            }
                            else
                            {
                                Terminals.Add(UpdatedTerminal);
                            }
                        }
                    }
                    TerminalsSyncDate = HCoreHelper.GetGMTDateTime();
                    #endregion
                    #region Update Partners
                    List<ODataStore.Account.Partner> UpdatedPartners = (from x in _HCoreContext.HCUAccount
                                                                        where (x.AccountTypeId == Helpers.UserAccountType.Acquirer || x.AccountTypeId == Helpers.UserAccountType.Partner || x.AccountTypeId == Helpers.UserAccountType.PgAccount || x.AccountTypeId == Helpers.UserAccountType.PosAccount) && (x.CreateDate > PartnersSyncDate || x.ModifyDate > PartnersSyncDate)
                                                                        select new ODataStore.Account.Partner
                                                                        {
                                                                            ReferenceId = x.Id,
                                                                            AccountTypeId = x.AccountTypeId,
                                                                            DisplayName = x.DisplayName,
                                                                            AccountCode = x.AccountCode,
                                                                            StatusId = x.StatusId,
                                                                        }).ToList();
                    if (UpdatedPartners.Count() > 0)
                    {
                        foreach (var UpdatedAccount in UpdatedPartners)
                        {
                            var ExistingCheck = Partners.Where(x => x.ReferenceId == UpdatedAccount.ReferenceId).FirstOrDefault();
                            if (ExistingCheck != null)
                            {
                                ExistingCheck.DisplayName = UpdatedAccount.DisplayName;
                                ExistingCheck.AccountCode = UpdatedAccount.AccountCode;
                                ExistingCheck.StatusId = UpdatedAccount.StatusId;
                            }
                            else
                            {
                                Partners.Add(UpdatedAccount);
                            }
                        }
                    }
                    PartnersSyncDate = HCoreHelper.GetGMTDateTime();
                    #endregion

                    _HCoreContext.Dispose();
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GatewayStore", ex);
            }

        }
    }
}
