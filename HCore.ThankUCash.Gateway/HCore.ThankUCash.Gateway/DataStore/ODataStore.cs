//==================================================================================
// FileName: ODataStore.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.ThankUCash.Gateway.DataStore
{
    public class ODataStore
    {
        public class TerminalProduct
        {
            public long AccountId { get; set; }
            public long ReferenceId { get; set; }
            public object ReferenceNumber { get; set; }
            public double RewardPercentage { get; set; }
            public int StatusId { get; set; }
        }
        public class Configuration
        {
            public class Base
            {
                public long ReferenceId { get; set; }
                public string? SystemName { get; set; }
                public string? DefaultValue { get; set; }
                public string? TypeCode { get; set; }
                public string? TypeName { get; set; }
                public string? ActiveValue { get; set; }
                public int StatusId { get; set; }
            }
            public class Account
            {
                public long AccountId { get; set; }
                public long ConfigurationId { get; set; }
                public string? SystemName { get; set; }
                public string? Value { get; set; }
                public string? TypeCode { get; set; }
                public string? TypeName { get; set; }
                public int StatusId { get; set; }
            }
        }
        public class Account
        {
            public class Customer
            {
                public long AccountId { get; set; }
                public string? Username { get; set; }
                public string? AccountKey { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? Gender { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
                public string? AccountNumber { get; set; }
                public string? Pin { get; set; }
                public long? OwnerId { get; set; }
                public long? OwnerAccountTypeId { get; set; }
                public long? SubOwnerId { get; set; }
                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public long? CreatedByAccountTypeId { get; set; }
                public int StatusId { get; set; }
                public DateTime? SyncDate { get; set; }
            }

            public class Merchant
            {
                public long ReferenceId { get; set; }
                public long? OwnerId { get; set; }
                public long? OwnerAccountTypeId { get; set; }
                public string? DisplayName { get; set; }
                public string? AccountCode { get; set; }
                public int StatusId { get; set; }
                public int? PrimaryCategoryId { get; set; }
                public int? SecondaryCategoryId { get; set; }
                public double? AccountPercentage { get; set; }
                public double? Balance { get; set; }
            }
            public class Store
            {
                public long ReferenceId { get; set; }
                public long? MerchantId { get; set; }
                public string? DisplayName { get; set; }
                public string? Address { get; set; }
                public double? Latitude { get; set; }
                public double? Longitude { get; set; }
                public int StatusId { get; set; }
            }
            public class Cashier
            {
                public long ReferenceId { get; set; }
                public long? MerchantId { get; set; }
                public long? StoreId { get; set; }
                public string? CashierId { get; set; }
                public int StatusId { get; set; }
            }
            public class Terminal
            {
                public long ReferenceId { get; set; }
                public string? TerminalId { get; set; }
                public long? MerchantId { get; set; }
                public long? StoreId { get; set; }
                public long? PtspId { get; set; }
                public long? BankId { get; set; }
                public int StatusId { get; set; }
            }
            public class Partner
            {
                public long ReferenceId { get; set; }
                public long AccountTypeId { get; set; }
                public string? DisplayName { get; set; }
                public string? AccountCode { get; set; }
                public int StatusId { get; set; }
            }
        }
    }
}
