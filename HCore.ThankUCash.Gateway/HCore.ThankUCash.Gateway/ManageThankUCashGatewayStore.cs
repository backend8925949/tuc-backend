//==================================================================================
// FileName: ManageThankUCashGatewayStore.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using Akka.Actor;

namespace HCore.ThankUCash.Gateway
{
    public class ManageThankUCashGatewayStore
    {
        public static void RefreshGatewayDataStore()
        {
            DataStore.DataStore.RefreshDataStore();
        }

        public static void RefreshGatewayDataStoreSync()
        {
            #region TransactionPostProcess
            var _TransactionPostProcessActor = ActorSystem.Create("ActorGatewayDataStore");
            var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<ActorGatewayDataStore>("ActorGatewayDataStore");
            _TransactionPostProcessActorNotify.Tell(1);
            #endregion
        }
    }


    public class ActorGatewayDataStore : ReceiveActor
    {
        public ActorGatewayDataStore()
        {
            Receive<long>(_Request =>
            {
                DataStore.DataStore.RefreshDataStore();
            });
        }
    }
}
