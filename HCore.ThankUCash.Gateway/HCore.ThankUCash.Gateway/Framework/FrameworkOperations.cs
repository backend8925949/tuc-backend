//==================================================================================
// FileName: FrameworkOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.ThankUCash.Gateway.Actor;
using HCore.ThankUCash.Gateway.Core;
using HCore.ThankUCash.Gateway.Object;
using Z.EntityFramework.Plus;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
namespace HCore.ThankUCash.Gateway.Framework
{
    internal class FrameworkOperations
    {
        #region Declare
        HCoreContext _HCoreContext;
        ManageCoreTransaction _ManageCoreTransaction;
        OThankUGateway.Response _GatewayResponse;
        #endregion
        internal OResponse GetConfiguration(OThankUGateway.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.MerchantId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG171, CoreResources.HCG171M);
                }
                else
                {
                    #region Perform Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount)
                        {
                            var MerchantOwnerDetails = _HCoreContext.HCUAccountOwner
                            .Where(x => x.OwnerId == _Request.UserReference.AccountId &&
                            x.Account.AccountCode == _Request.MerchantId &&
                            x.StatusId == HelperStatus.Default.Active)
                            .Select(x => new
                            {
                                MerchantKey = x.Account.Guid,
                            }).FirstOrDefault();
                            if (MerchantOwnerDetails != null)
                            {
                                _GatewayResponse.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", MerchantOwnerDetails.MerchantKey));
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG108");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG103");
                                #endregion
                            }
                        }
                        else
                        {
                            var MerchantOwnerDetails = _HCoreContext.HCUAccount
                            .Where(x => x.AccountCode == _Request.MerchantId &&
                            x.StatusId == HelperStatus.Default.Active)
                            .Select(x => new
                            {
                                MerchantKey = x.Owner.Owner.Guid,
                            }).FirstOrDefault();
                            if (MerchantOwnerDetails != null)
                            {

                                _GatewayResponse.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", MerchantOwnerDetails.MerchantKey));
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG108");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG103");
                                #endregion
                            }
                        }
                    }
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetConfiguration", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG172, CoreResources.HCG172M);
            }
            #endregion
        }
        // OLD FUNCTIONS 
        internal static OGatewayInfo GetGatewayInfo(OThankUGateway.Request _Request)
        {
            using (HCoreContext _HCoreContext = new HCoreContext())
            {
                OGatewayInfo _OGatewayInfo = null;
                switch (_Request.UserReference.AccountTypeId)
                {
                    case UserAccountType.PgAccount:
                    case UserAccountType.Acquirer:
                    case UserAccountType.Partner:
                        _OGatewayInfo = Data.Store.HCoreDataStore.Accounts
                                        .Where(x => x.AccountTypeId == UserAccountType.Merchant &&
                                        x.StatusId == HelperStatus.Default.Active &&
                                        x.AccountCode == _Request.MerchantId)
                                        .Select(x => new OGatewayInfo
                                        {
                                            MerchantId = x.ReferenceId,
                                            MerchantDisplayName = x.DisplayName,
                                            AcquirerId = x.OwnerId,
                                            AcquirerAccountTypeId = x.OwnerAccountTypeId
                                        })
                                        .FirstOrDefault();
                        if (_OGatewayInfo == null)
                        {
                            _OGatewayInfo = _HCoreContext.HCUAccount
                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant &&
                                    x.StatusId == HelperStatus.Default.Active &&
                                    x.AccountCode == _Request.MerchantId)
                                    .Select(x => new OGatewayInfo
                                    {
                                        MerchantId = x.Id,
                                        MerchantDisplayName = x.DisplayName,
                                        AcquirerId = x.OwnerId,
                                        AcquirerAccountTypeId = x.Owner.AccountTypeId
                                    }).FirstOrDefault();
                        }
                        if (_OGatewayInfo != null)
                        {
                            _OGatewayInfo.PsspId = _Request.UserReference.AccountId;
                        }
                        break;
                    case UserAccountType.PosAccount:
                        _OGatewayInfo = Data.Store.HCoreDataStore.Terminals
                                        .Where(x => x.TerminalId == _Request.TerminalId
                                       && x.MerchantId != null
                                       && x.StoreId != null
                                       && x.StatusId == HelperStatus.Default.Active
                                       && x.StoreStatusId == HelperStatus.Default.Active
                                       && x.MerchantStatusId == HelperStatus.Default.Active)
                                         .Select(x => new OGatewayInfo
                                         {
                                             TerminalId = x.ReferenceId,
                                             MerchantId = (long)x.MerchantId,
                                             MerchantDisplayName = x.MerchantDisplayName,
                                             StoreId = (long)x.StoreId,
                                             StoreDisplayName = x.StoreDisplayName,
                                             AcquirerId = x.AcquirerId,
                                             AcquirerAccountTypeId = UserAccountType.Acquirer,
                                         }).FirstOrDefault();
                        if (_OGatewayInfo == null)
                        {
                            //_OGatewayInfo = _HCoreContext.HCUAccount
                            //                                          .Where(x => x.AccountTypeId == UserAccountType.TerminalAccount
                            //                                                 && x.User.Username == _Request.TerminalId
                            //                                                 && x.SubOwnerId != null
                            //                                                 && x.StatusId == HelperStatus.Default.Active
                            //                                                 && x.Owner.StatusId == HelperStatus.Default.Active
                            //                                                 && x.SubOwner.StatusId == HelperStatus.Default.Active
                            //                                                 && x.SubOwner.Owner.StatusId == HelperStatus.Default.Active
                            //                                                )
                            //                                             .Select(x => new OGatewayInfo
                            //                                             {
                            //                                                 TerminalId = x.Id,
                            //                                                 MerchantId = (long)x.SubOwner.OwnerId,
                            //                                                 MerchantDisplayName = x.SubOwner.Owner.DisplayName,
                            //                                                 StoreId = (long)x.SubOwnerId,
                            //                                                 StoreDisplayName = x.SubOwner.DisplayName,
                            //                                                 AcquirerId = x.BankId,
                            //                                                 AcquirerAccountTypeId = UserAccountType.Acquirer,
                            //                                             }).FirstOrDefault();

                            _OGatewayInfo = _HCoreContext.TUCTerminal
                                                                     .Where(x => x.IdentificationNumber == _Request.TerminalId
                                                                            && x.StoreId != null
                                                                            && x.StatusId == HelperStatus.Default.Active
                                                                            && x.Provider.StatusId == HelperStatus.Default.Active
                                                                            && x.Store.StatusId == HelperStatus.Default.Active
                                                                            && x.Merchant.StatusId == HelperStatus.Default.Active
                                                                           )
                                                                        .Select(x => new OGatewayInfo
                                                                        {
                                                                            TerminalId = x.Id,
                                                                            MerchantId = (long)x.MerchantId,
                                                                            MerchantDisplayName = x.Merchant.DisplayName,
                                                                            StoreId = (long)x.StoreId,
                                                                            StoreDisplayName = x.DisplayName,
                                                                            AcquirerId = x.AcquirerId,
                                                                            AcquirerAccountTypeId = UserAccountType.Acquirer,
                                                                        }).FirstOrDefault();
                        }
                        if (_OGatewayInfo != null)
                        {
                            _OGatewayInfo.PtspId = _Request.UserReference.AccountId;
                        }
                        break;
                    case UserAccountType.Merchant:
                        OGatewayInfo MerchantDetails = Data.Store.HCoreDataStore.Accounts
                        .Where(x => x.AccountTypeId == _Request.UserReference.AccountId
                        && x.StatusId == HelperStatus.Default.Active)
                        .Select(x => new OGatewayInfo
                        {
                            MerchantId = x.ReferenceId,
                            MerchantDisplayName = x.DisplayName,
                            AcquirerId = x.OwnerId,
                            AcquirerAccountTypeId = x.OwnerAccountTypeId
                        })
                        .FirstOrDefault();
                        if (MerchantDetails == null)
                        {
                            _OGatewayInfo = _HCoreContext.HCUAccount
                                           .Where(x => x.StatusId == HelperStatus.Default.Active && x.Id == _Request.UserReference.AccountId)
                                           .Select(x => new OGatewayInfo
                                           {
                                               MerchantId = x.Id,
                                               MerchantDisplayName = x.DisplayName,
                                               AcquirerId = x.OwnerId,
                                               AcquirerAccountTypeId = x.Owner.AccountTypeId
                                           }).FirstOrDefault();
                        }
                        break;
                }
                if (_OGatewayInfo != null)
                {
                    if (!string.IsNullOrEmpty(_Request.CashierId))
                    {
                        var CCashierDetails = Data.Store.HCoreDataStore.Accounts.Where(x => x.AccountTypeId == UserAccountType.MerchantCashier
                                               && x.OwnerId == _OGatewayInfo.MerchantId
                                               && x.DisplayName == _Request.CashierId)
                                                    .FirstOrDefault();
                        if (CCashierDetails != null)
                        {
                            _OGatewayInfo.CashierId = CCashierDetails.ReferenceId;
                            _OGatewayInfo.CashierStatusId = (long)CCashierDetails.StatusId;
                        }
                        else
                        {
                            var CashierDetails = _HCoreContext.HCUAccount
                                 .Where(x => x.DisplayName == _Request.CashierId
                                 && x.OwnerId == _OGatewayInfo.MerchantId
                                 && x.AccountTypeId == UserAccountType.MerchantCashier
                                 ).Select(x => new
                                 {
                                     Id = x.Id,
                                     StatusId = x.StatusId,
                                 }).FirstOrDefault();
                            if (CashierDetails != null)
                            {
                                _OGatewayInfo.CashierId = CashierDetails.Id;
                                _OGatewayInfo.CashierStatusId = CashierDetails.StatusId;
                            }
                        }
                    }
                    else
                    {

                        var CCashierDetails = Data.Store.HCoreDataStore.Accounts.Where(x => x.AccountTypeId == UserAccountType.MerchantCashier
                                              && x.OwnerId == _OGatewayInfo.MerchantId
                                              && x.DisplayName == "0001")
                                                   .FirstOrDefault();
                        if (CCashierDetails != null)
                        {
                            _OGatewayInfo.CashierId = CCashierDetails.ReferenceId;
                            _OGatewayInfo.CashierStatusId = (long)CCashierDetails.StatusId;
                        }
                        else
                        {
                            var CashierDetails = _HCoreContext.HCUAccount
                                                 .Where(x => x.DisplayName == "0001"
                                                 && x.OwnerId == _OGatewayInfo.MerchantId
                                                 && x.AccountTypeId == UserAccountType.MerchantCashier
                                                 ).Select(x => new
                                                 {
                                                     Id = x.Id,
                                                     StatusId = x.StatusId,
                                                 }).FirstOrDefault();
                            if (CashierDetails != null)
                            {
                                _OGatewayInfo.CashierId = CashierDetails.Id;
                                _OGatewayInfo.CashierStatusId = CashierDetails.StatusId;
                            }
                        }
                    }
                    if (_OGatewayInfo.AcquirerAccountTypeId != UserAccountType.Merchant
                    && _OGatewayInfo.AcquirerAccountTypeId != UserAccountType.Acquirer)
                    {
                        _OGatewayInfo.AcquirerId = 0;
                    }
                    _HCoreContext.Dispose();
                    if (_OGatewayInfo.AcquirerId == 0)
                    {
                        _OGatewayInfo.AcquirerId = SystemAccounts.ThankUCashMerchant;
                        _OGatewayInfo.AcquirerAccountTypeId = UserAccountType.Merchant;
                    }
                    else if (_Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                    {
                        if (_OGatewayInfo.StoreId != 0)
                        {
                            _OGatewayInfo.TransactionIssuerId = _OGatewayInfo.StoreId;
                            _OGatewayInfo.TransactionIssuerAccountTypeId = UserAccountType.MerchantStore;
                        }
                        else
                        {
                            _OGatewayInfo.TransactionIssuerId = SystemAccounts.ThankUCashMerchant;
                            _OGatewayInfo.TransactionIssuerAccountTypeId = UserAccountType.Merchant;
                        }
                    }
                    return _OGatewayInfo;
                }
                else
                {
                    return null;
                }
            }
        }
        public class De
        {
            public string? DealKey { get; set; }
            public long? UserAccountId { get; set; }
            public string? DisplayName { get; set; }
            public string? Pin { get; set; }
            public long? CreatedById { get; set; }
            public long? CreatedByAccountTypeId { get; set; }
            public long? OwnerId { get; set; }
            public long? OwnerAccountTypeId { get; set; }
            public string? EmailAddress { get; set; }
            public long? AccountStatusId { get; set; }
            public string? MobileNumber { get; set; }
            public string? AccountNumber { get; set; }
            public int StatusId { get; set; }

            public double? DealAmount { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
        }
        internal static OUserInfo GetUserInfo(OThankUGateway.Request _Request, OGatewayInfo _GatewayInfo, bool CreateUser)
        {
            OUserInfo _OUserInfo = new OUserInfo();
            using (HCoreContext _HCoreContext = new HCoreContext())
            {
                if (!string.IsNullOrEmpty(_Request.CardNumber) && !string.IsNullOrEmpty(_Request.TagNumber))
                {
                    if (!string.IsNullOrEmpty(_Request.CardNumber) && !string.IsNullOrEmpty(_Request.TagNumber))
                    {
                        var CardDetails = _HCoreContext.TUCard
                        .Where(x => x.CardNumber == _Request.CardNumber
                            && x.SerialNumber == _Request.TagNumber)
                            .Select(x => new OTUInfo
                            {
                                //CardId = x.Id,
                                UserAccountId = x.ActiveUserAccountId,
                                UserId = x.ActiveUserAccount.UserId,
                                AccountType = x.ActiveUserAccount.AccountTypeId,
                                MobileNumber = x.ActiveUserAccount.MobileNumber,
                                DisplayName = x.ActiveUserAccount.DisplayName,
                                UserPin = x.ActiveUserAccount.AccessPin,
                                CreatedById = x.ActiveUserAccount.CreatedById,
                                CreatedByAccountTypeId = x.ActiveUserAccount.CreatedBy.AccountTypeId,
                                EmailAddress = x.ActiveUserAccount.EmailAddress,
                                OwnerId = x.ActiveUserAccount.OwnerId,
                                OwnerAccountTypeId = x.ActiveUserAccount.Owner.AccountTypeId,
                                CardStatusId = x.StatusId,
                                AccountStatusId = x.ActiveUserAccount.StatusId,
                            }).FirstOrDefault();
                        if (CardDetails != null)
                        {
                            if (CardDetails.CardStatusId == HelperStatus.Card.Assigned)
                            {
                                //_OUserInfo.CardId = CardDetails.CardId;
                                _OUserInfo.DisplayName = CardDetails.DisplayName;
                                _OUserInfo.MobileNumber = CardDetails.MobileNumber;
                                _OUserInfo.EmailAddress = CardDetails.EmailAddress;
                                _OUserInfo.UserAccountId = (long)CardDetails.UserAccountId;
                                _OUserInfo.Pin = CardDetails.UserPin;
                                if (CardDetails.AccountStatusId != null)
                                {
                                    _OUserInfo.AccountStatusId = (long)CardDetails.AccountStatusId;
                                }
                                if (CardDetails.OwnerId != null)
                                {
                                    _OUserInfo.IssuerId = (long)CardDetails.OwnerId;
                                    _OUserInfo.IssuerAccountTypeId = (long)CardDetails.OwnerAccountTypeId;
                                }
                                if (CardDetails.CreatedById != null)
                                {
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                                    {
                                        _OUserInfo.IssuerId = _HCoreContext.HCUAccount.Where(x => x.Id == CardDetails.CreatedById).Select(x => x.Owner.OwnerId).FirstOrDefault() ?? 0;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                    }
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.Acquirer)
                                    {
                                        _OUserInfo.IssuerId = (long)CardDetails.CreatedById;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                                    }
                                }

                                if (_OUserInfo.IssuerId == 0)
                                {
                                    _OUserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                                    _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                }
                                return _OUserInfo;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;

                        }
                    }
                    else if (!string.IsNullOrEmpty(_Request.CardNumber))
                    {
                        var CardDetails = _HCoreContext.TUCard
                        .Where(x => x.CardNumber == _Request.CardNumber)
                            .Select(x => new OTUInfo
                            {
                                //CardId = x.Id,
                                UserAccountId = x.ActiveUserAccountId,
                                UserId = x.ActiveUserAccount.UserId,
                                AccountType = x.ActiveUserAccount.AccountTypeId,
                                MobileNumber = x.ActiveUserAccount.MobileNumber,
                                DisplayName = x.ActiveUserAccount.DisplayName,
                                UserPin = x.ActiveUserAccount.AccessPin,
                                CreatedById = x.ActiveUserAccount.CreatedById,
                                CreatedByAccountTypeId = x.ActiveUserAccount.CreatedBy.AccountTypeId,
                                EmailAddress = x.ActiveUserAccount.EmailAddress,
                                OwnerId = x.ActiveUserAccount.OwnerId,
                                OwnerAccountTypeId = x.ActiveUserAccount.Owner.AccountTypeId,
                                CardStatusId = x.StatusId,
                                AccountStatusId = x.ActiveUserAccount.StatusId,
                            }).FirstOrDefault();
                        if (CardDetails != null)
                        {
                            if (CardDetails.CardStatusId == HelperStatus.Card.Assigned)
                            {
                                //_OUserInfo.CardId = CardDetails.CardId;
                                _OUserInfo.DisplayName = CardDetails.DisplayName;
                                _OUserInfo.MobileNumber = CardDetails.MobileNumber;
                                _OUserInfo.EmailAddress = CardDetails.EmailAddress;
                                _OUserInfo.UserAccountId = (long)CardDetails.UserAccountId;
                                _OUserInfo.Pin = CardDetails.UserPin;
                                if (CardDetails.AccountStatusId != null)
                                {
                                    _OUserInfo.AccountStatusId = (long)CardDetails.AccountStatusId;
                                }
                                if (CardDetails.OwnerId != null)
                                {
                                    _OUserInfo.IssuerId = (long)CardDetails.OwnerId;
                                    _OUserInfo.IssuerAccountTypeId = (long)CardDetails.OwnerAccountTypeId;
                                }
                                if (CardDetails.CreatedById != null)
                                {
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                                    {
                                        _OUserInfo.IssuerId = _HCoreContext.HCUAccount.Where(x => x.Id == CardDetails.CreatedById).Select(x => x.Owner.OwnerId).FirstOrDefault() ?? 0;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                    }
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.Acquirer)
                                    {
                                        _OUserInfo.IssuerId = (long)CardDetails.CreatedById;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                                    }
                                }

                                if (_OUserInfo.IssuerId == 0)
                                {
                                    _OUserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                                    _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                }
                                return _OUserInfo;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;

                        }
                    }
                    else if (!string.IsNullOrEmpty(_Request.TagNumber))
                    {
                        var CardDetails = _HCoreContext.TUCard
                        .Where(x => x.CardNumber == _Request.TagNumber)
                            .Select(x => new OTUInfo
                            {
                                //CardId = x.Id,
                                UserAccountId = x.ActiveUserAccountId,
                                UserId = x.ActiveUserAccount.UserId,
                                AccountType = x.ActiveUserAccount.AccountTypeId,
                                MobileNumber = x.ActiveUserAccount.MobileNumber,
                                DisplayName = x.ActiveUserAccount.DisplayName,
                                UserPin = x.ActiveUserAccount.AccessPin,
                                CreatedById = x.ActiveUserAccount.CreatedById,
                                CreatedByAccountTypeId = x.ActiveUserAccount.CreatedBy.AccountTypeId,
                                EmailAddress = x.ActiveUserAccount.EmailAddress,
                                OwnerId = x.ActiveUserAccount.OwnerId,
                                OwnerAccountTypeId = x.ActiveUserAccount.Owner.AccountTypeId,
                                CardStatusId = x.StatusId,
                                AccountStatusId = x.ActiveUserAccount.StatusId,
                            }).FirstOrDefault();
                        if (CardDetails != null)
                        {
                            if (CardDetails.CardStatusId == HelperStatus.Card.Assigned)
                            {
                                //_OUserInfo.CardId = CardDetails.CardId;
                                _OUserInfo.DisplayName = CardDetails.DisplayName;
                                _OUserInfo.MobileNumber = CardDetails.MobileNumber;
                                _OUserInfo.EmailAddress = CardDetails.EmailAddress;
                                _OUserInfo.UserAccountId = (long)CardDetails.UserAccountId;
                                _OUserInfo.Pin = CardDetails.UserPin;
                                if (CardDetails.AccountStatusId != null)
                                {
                                    _OUserInfo.AccountStatusId = (long)CardDetails.AccountStatusId;
                                }
                                if (CardDetails.OwnerId != null)
                                {
                                    _OUserInfo.IssuerId = (long)CardDetails.OwnerId;
                                    _OUserInfo.IssuerAccountTypeId = (long)CardDetails.OwnerAccountTypeId;
                                }
                                if (CardDetails.CreatedById != null)
                                {
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                                    {
                                        _OUserInfo.IssuerId = _HCoreContext.HCUAccount.Where(x => x.Id == CardDetails.CreatedById).Select(x => x.Owner.OwnerId).FirstOrDefault() ?? 0;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                    }
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.Acquirer)
                                    {
                                        _OUserInfo.IssuerId = (long)CardDetails.CreatedById;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                                    }
                                }

                                if (_OUserInfo.IssuerId == 0)
                                {
                                    _OUserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                                    _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                }
                                return _OUserInfo;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;

                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else if (!string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    if (_Request.MobileNumber.StartsWith("12"))
                    {
                        // Deal
                        var DealCode = _HCoreContext.MDDealCode.Where(x => x.ItemCode == _Request.MobileNumber && x.Deal.AccountId == _GatewayInfo.MerchantId)
                            .Select(x => new De
                            {
                                DealKey = x.Guid,
                                //DealStatusId = x.StatusId,
                                //DealMerchantId = x.Deal.AccountId,
                                DealAmount = x.ItemAmount,
                                UserAccountId = x.Account.Id,
                                DisplayName = x.Account.DisplayName,
                                Pin = x.Account.AccessPin,
                                CreatedById = x.Account.CreatedById,
                                CreatedByAccountTypeId = x.Account.CreatedBy.AccountTypeId,
                                OwnerId = x.Account.OwnerId,
                                OwnerAccountTypeId = x.Account.Owner.AccountTypeId,
                                EmailAddress = x.Account.EmailAddress,
                                AccountStatusId = x.Account.StatusId,
                                MobileNumber = x.Account.MobileNumber,
                                AccountNumber = x.Account.MobileNumber,
                                StatusId = x.StatusId,
                                StartDate = x.StartDate,
                                EndDate = x.EndDate,
                            }).FirstOrDefault();
                        if (DealCode != null)
                        {
                            if (DealCode.StatusId == HelperStatus.DealCodes.Unused)
                            {
                                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                if (CurrentTime < DealCode.StartDate)
                                {
                                    return null;
                                }
                                else if (CurrentTime > DealCode.EndDate)
                                {
                                    return null;
                                }
                                else if (CurrentTime > DealCode.StartDate && CurrentTime < DealCode.EndDate)
                                {

                                }
                                else
                                {
                                    return null;
                                }
                            }
                            _OUserInfo.DealKey = DealCode.DealKey;
                            _OUserInfo.DealAmount = (double)DealCode.DealAmount;
                            _OUserInfo.AccountNumber = DealCode.AccountNumber;
                            _OUserInfo.MobileNumber = DealCode.MobileNumber;
                            _OUserInfo.DisplayName = DealCode.DisplayName;
                            _OUserInfo.UserAccountId = (long)DealCode.UserAccountId;
                            _OUserInfo.Pin = DealCode.Pin;
                            _OUserInfo.EmailAddress = DealCode.EmailAddress;
                            _OUserInfo.AccountStatusId = (long)DealCode.AccountStatusId;
                            if (DealCode.OwnerId != null)
                            {
                                _OUserInfo.IssuerId = (long)DealCode.OwnerId;
                                _OUserInfo.IssuerAccountTypeId = (long)DealCode.OwnerAccountTypeId;
                            }
                            if (DealCode.CreatedById != null)
                            {
                                if (DealCode.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                                {
                                    _OUserInfo.IssuerId = _HCoreContext.HCUAccount.Where(x => x.Id == DealCode.CreatedById).Select(x => x.Owner.OwnerId).FirstOrDefault() ?? 0;
                                    _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                }
                                if (DealCode.CreatedByAccountTypeId == UserAccountType.Acquirer)
                                {
                                    _OUserInfo.IssuerId = (long)DealCode.CreatedById;
                                    _OUserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                                }
                            }
                            if (_OUserInfo.IssuerId == 0)
                            {
                                _OUserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                                _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                            }
                            return _OUserInfo;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    string AppUserCardId = _Request.MobileNumber;
                    _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber, (int)_Request.UserReference.CountryMobileNumberLength);
                    string AppUserLoginId = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                    var UserAccount = _HCoreContext.HCUAccount
                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                && (x.User.Username == AppUserLoginId || x.AccountCode == AppUserCardId))
                                                   .Select(x => new OUserInfo
                                                   {
                                                       UserAccountId = x.Id,
                                                       DisplayName = x.DisplayName,
                                                       Pin = x.AccessPin,
                                                       CreatedById = x.CreatedById,
                                                       CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                                       OwnerId = x.OwnerId,
                                                       OwnerAccountTypeId = x.Owner.AccountTypeId,
                                                       EmailAddress = x.EmailAddress,
                                                       AccountStatusId = x.StatusId,
                                                       MobileNumber = x.MobileNumber,
                                                       AccountNumber = x.MobileNumber,
                                                   }).FirstOrDefault();
                    if (UserAccount != null)
                    {
                        _OUserInfo.AccountNumber = UserAccount.AccountNumber;
                        _OUserInfo.MobileNumber = UserAccount.MobileNumber;
                        _OUserInfo.DisplayName = UserAccount.DisplayName;
                        _OUserInfo.UserAccountId = UserAccount.UserAccountId;
                        _OUserInfo.Pin = UserAccount.Pin;
                        _OUserInfo.EmailAddress = UserAccount.EmailAddress;
                        _OUserInfo.AccountStatusId = UserAccount.AccountStatusId;
                        if (UserAccount.OwnerId != null)
                        {
                            _OUserInfo.IssuerId = (long)UserAccount.OwnerId;
                            _OUserInfo.IssuerAccountTypeId = (long)UserAccount.OwnerAccountTypeId;
                        }

                        if (UserAccount.CreatedById != null)
                        {
                            if (UserAccount.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                            {
                                _OUserInfo.IssuerId = _HCoreContext.HCUAccount.Where(x => x.Id == UserAccount.CreatedById).Select(x => x.Owner.OwnerId).FirstOrDefault() ?? 0;
                                _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                            }
                            if (UserAccount.CreatedByAccountTypeId == UserAccountType.Acquirer)
                            {
                                _OUserInfo.IssuerId = (long)UserAccount.CreatedById;
                                _OUserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                            }
                        }
                        if (_OUserInfo.IssuerId == 0)
                        {
                            _OUserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                            _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                        }
                        return _OUserInfo;
                    }
                    else
                    {
                        if (CreateUser == true && !AppUserCardId.StartsWith("20"))
                        {
                            OAppProfile.Request _AppProfileRequest = new OAppProfile.Request();
                            if (_GatewayInfo != null && _GatewayInfo.MerchantId != 0)
                            {
                                _AppProfileRequest.OwnerId = _GatewayInfo.MerchantId;
                            }
                            if (_GatewayInfo != null && _GatewayInfo.StoreId != 0)
                            {
                                _AppProfileRequest.SubOwnerId = _GatewayInfo.StoreId;
                            }
                            if (_GatewayInfo != null && _GatewayInfo.TerminalId != 0)
                            {
                                _AppProfileRequest.CreatedById = _GatewayInfo.TerminalId;
                            }
                            _AppProfileRequest.MobileNumber = _Request.MobileNumber;
                            _AppProfileRequest.DisplayName = _Request.MobileNumber;
                            _AppProfileRequest.UserReference = _Request.UserReference;
                            ManageCoreUserAccess _ManageCoreUserAccess = new ManageCoreUserAccess();
                            OAppProfile.Response _AppUserCreateResponse = _ManageCoreUserAccess.CreateAppUserAccount(_AppProfileRequest);
                            if (_AppUserCreateResponse.Status == ResponseStatus.Success)
                            {
                                if (_AppUserCreateResponse.StatusId != 0)
                                {
                                    _OUserInfo.AccountStatusId = _AppUserCreateResponse.StatusId;
                                }
                                else
                                {
                                    _OUserInfo.AccountStatusId = HelperStatus.Default.Active;
                                }

                                _OUserInfo.UserAccountId = _AppUserCreateResponse.AccountId;
                                if (_GatewayInfo != null && _GatewayInfo.MerchantId != 0)
                                {
                                    _OUserInfo.IssuerId = _GatewayInfo.MerchantId;
                                    _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                }
                                if (_OUserInfo.IssuerId == 0)
                                {
                                    _OUserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                                    _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                }
                                return _OUserInfo;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;
                        }

                    }
                }
                else
                {
                    return null;
                }
            }
        }
        CoreOperations _CoreOperations;
        internal OResponse GetBalance(OThankUGateway.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.MobileNumber) && string.IsNullOrEmpty(_Request.CardNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109", CoreResources.HCG109);
                    #endregion
                }
                else
                {
                    #region Perform Operations
                    OGatewayInfo _OGatewayInfo = GetGatewayInfo(_Request);
                    if (_OGatewayInfo != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.CashierId))
                        {
                            if (_OGatewayInfo.CashierId == 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG134", CoreResources.HCG134);
                                #endregion
                            }
                            else if (_OGatewayInfo.CashierId != 0 && _OGatewayInfo.CashierStatusId != HelperStatus.Default.Active)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG135", CoreResources.HCG135);
                                #endregion
                            }
                        }
                        OUserInfo _UserInfo = GetUserInfo(_Request, _OGatewayInfo, false);
                        if (_UserInfo != null)
                        {
                            if (_UserInfo.AccountStatusId == HelperStatus.Default.Active)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    double ProductAmount = 0;
                                    #region Get User Products
                                    if (string.IsNullOrEmpty(_UserInfo.DealKey))
                                    {
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var ProductBalance = _HCoreContext.CAProductCode
                                                                  .Where(x => x.AccountId == _UserInfo.UserAccountId
                                                                          && ((x.Product.UsageTypeId == HelperType.ProductUsageType.FullValue && x.AvailableAmount > 0)
                                                                               || (x.Product.UsageTypeId == HelperType.ProductUsageType.PartialValue && x.AvailableAmount > 0))
                                                                          && (x.Product.TypeId == Product.GiftCard || x.Product.TypeId == Product.QuickGiftCard)
                                                                          && x.AvailableAmount > 0
                                                                          && x.StatusId == HelperStatus.ProdutCode.Unused)
                                                                  .OrderByDescending(x => x.AvailableAmount)
                                                                  .Select(x => new
                                                                  {
                                                                      ProductId = x.ProductId,
                                                                      CodeId = x.Id,
                                                                      x.AvailableAmount,
                                                                      UsageTypeId = x.Product.UsageTypeId
                                                                  })
                                                                  .FirstOrDefault();
                                            if (ProductBalance != null)
                                            {
                                                ProductAmount = (double)ProductBalance.AvailableAmount;
                                            }
                                            _HCoreContext.Dispose();
                                        }
                                    }
                                    #endregion
                                    #region Get Balance
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    _GatewayResponse.DealBalance = (long)HCoreHelper.RoundNumber(_UserInfo.DealAmount * 100, 0);
                                    if (string.IsNullOrEmpty(_UserInfo.DealKey))
                                    {
                                        _CoreOperations = new CoreOperations();
                                        long ThankUCashPlusIsEnable = Convert.ToInt64(_CoreOperations.GetConfiguration("thankucashplus", _OGatewayInfo.MerchantId));
                                        long TucGold = Convert.ToInt64(_CoreOperations.GetConfiguration("thankucashgold", _OGatewayInfo.MerchantId));

                                        _GatewayResponse.GiftPointBalance = (long)HCoreHelper.RoundNumber(_ManageCoreTransaction.GetAppUserGiftPointsBalance(_UserInfo.UserAccountId, _OGatewayInfo.MerchantId) * 100, 0);
                                        _GatewayResponse.GiftCardBalance = (long)HCoreHelper.RoundNumber(ProductAmount * 100, 0);
                                        _GatewayResponse.ThankUCashPlusBalance = 0;
                                        _GatewayResponse.TUCGoldBalance = 0;
                                        _GatewayResponse.ThankUCashBalance = 0;

                                        if (ThankUCashPlusIsEnable > 0)
                                        {
                                            _GatewayResponse.ThankUCashPlusBalance = (long)HCoreHelper.RoundNumber(((_ManageCoreTransaction.GetAppUserThankUCashPlusBalance(_UserInfo.UserAccountId, _OGatewayInfo.MerchantId)) * 100), 0);
                                        }
                                        else if (TucGold > 0)
                                        {
                                            _GatewayResponse.TUCGoldBalance = (long)HCoreHelper.RoundNumber(_ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId, _OGatewayInfo.MerchantId, TransactionSource.TUCBlack) * 100, 0);
                                        }
                                        else
                                        {
                                            _GatewayResponse.ThankUCashBalance = (long)HCoreHelper.RoundNumber(_ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId) * 100, 0);
                                        }

                                        if (_UserInfo.DealAmount > 0)
                                        {
                                            _GatewayResponse.Balance = (long)HCoreHelper.RoundNumber(_UserInfo.DealAmount * 100, 0);
                                        }
                                        else
                                        {
                                            _GatewayResponse.Balance = _GatewayResponse.GiftPointBalance + _GatewayResponse.ThankUCashBalance + _GatewayResponse.GiftCardBalance + _GatewayResponse.TUCGoldBalance;
                                        }

                                        long LoyaltyProgramBalance = 0;
                                        OLoyaltyProgram ProgramBalance = _ManageCoreTransaction.GetLoyaltyProgramBalance(_UserInfo.UserAccountId, _OGatewayInfo.MerchantId, (long)_OGatewayInfo.AcquirerId);
                                        if (ProgramBalance != null)
                                        {
                                            LoyaltyProgramBalance = (long)HCoreHelper.RoundNumber((ProgramBalance.Balance * 100), 0);
                                        }
                                        if (LoyaltyProgramBalance > 0)
                                        {
                                            _GatewayResponse.Balance = LoyaltyProgramBalance;
                                        }
                                        else
                                        {
                                            _GatewayResponse.Balance = _GatewayResponse.Balance;
                                        }
                                        if (_GatewayResponse.Balance < 0)
                                        {
                                            _GatewayResponse.Balance = 0;
                                        }
                                    }
                                    else
                                    {
                                        _GatewayResponse.Balance = _GatewayResponse.DealBalance;
                                    }
                                    #endregion
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG113", CoreResources.HCG113);
                                    #endregion
                                }
                            }
                            else
                            {
                                _GatewayResponse.Balance = 0;
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG129", CoreResources.HCG129);
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, "HCG111", CoreResources.HCG111);
                            #endregion
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, "HCG103", CoreResources.HCG103);
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetBalance", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG500, CoreResources.HCG500M);
                #endregion
            }
            #endregion
        }



    }
}
