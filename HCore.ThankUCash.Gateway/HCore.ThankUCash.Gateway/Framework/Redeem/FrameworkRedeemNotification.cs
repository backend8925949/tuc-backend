﻿using System;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.ThankUCash.Gateway.Core;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.ThankUCash.Gateway.Helper.HelperGateway;

namespace HCore.ThankUCash.Gateway.Framework.Redeem
{
    internal class TUCDealResource
    {
        internal const string HCP0122 = "HCP0122";
        internal const string HCP0122M = "Invalid deal code";
        internal const string HCP0123 = "HCP0123";
        internal const string HCP0123M = "Deal code already used";
        internal const string HCP0124 = "HCP0124";
        internal const string HCP0124M = "Deal code expired";
        internal const string HCP0125 = "HC P0125";
        internal const string HCP0125M = "Deal code blocked";
        internal const string HCP0126 = "HCP0126";
        internal const string HCP0126M = "Invalid deal code";
        internal const string HCP0127 = "HCP0127";
        internal const string HCP0127M = "Deal cannot be redeemed at current time. Check for deal redeem shedule";
        internal const string HCP0128 = "HCP0128";
        internal const string HCP0128M = "Deal cannot be redeemed at current time. Check for deal redeem shedule";
        internal const string HCP0129 = "HCP0128";
        internal const string HCP0129M = "Deal cannot be redeemed at current time. Check for deal redeem shedule";
        internal const string HCP0134 = "HCP0134";
        internal const string HCP0134M = "Deal details not found";
        internal const string HCP0135 = "HCP0135";
        internal const string HCP0135M = "Invalid deal merchant";
        internal const string HCP0136 = "HCP0136";
        internal const string HCP0136M = "Invalid deal status";
        internal const string HCP0137 = "HCP0137";
        internal const string HCP0137M = "Deal code Blocked";
        internal const string HCP0138 = "HCP0138";
        internal const string HCP0138M = "Deal code expired";
        internal const string HCP0139 = "HCP0139";
        internal const string HCP0139M = "Code already used";
    }

    internal class TUCLoanResource
    {
        internal const string BNPL0139 = "BNPL0139";
        internal const string BNPL0139M = "Code already used";
    }

    public class ODealOperation
    {
        public class DealCode
        {
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public long DealReferenceId { get; set; }
                public string? DealReferenceKey { get; set; }

                public sbyte IsRedeemed { get; set; }

                public DateTime? UseDate { get; set; }
                public long? UseLocationId { get; set; }
                public string? UseLocationKey { get; set; }
                public string? UseLocationDisplayName { get; set; }
                public string? UseLocationAddress { get; set; }

                public string? ItemCode { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public double? ActualPrice { get; set; }
                public double? SellingPrice { get; set; }
                public double? Amount { get; set; }
                public double? CommissionAmount { get; set; }
                public double? TotalAmount { get; set; }
                public string? Title { get; set; }
                public string? Description { get; set; }
                public string? ImageUrl { get; set; }
                public string? UsageInformation { get; set; }
                public string? Terms { get; set; }
                public long? MerchantReferenceId { get; set; }
                public string? MerchantReferenceKey { get; set; }
                public string? MerchantDisplayName { get; set; }
                public string? MerchantIconUrl { get; set; }
                public string? RedeemInstruction { get; set; }
                public int StatusId { get; set; }
                public DateTime? CreateDate { get; set; }
                public DateTime? DealStartDate { get; set; }
                public DateTime? DealEndDate { get; set; }

                public long? CustomerId { get; set; }
                public string? CustomerKey { get; set; }
                public string? CustomerDisplayName { get; set; }
                public string? CustomerMobileNumber { get; set; }
                public string? CustomerIconUrl { get; set; }
            }
        }
    }
    public class ORedeemNotification
    {
        public long TransactionId { get; set; }
        public string? TransactionKey { get; set; }

        public double RedeemAmount { get; set; }
        public double PurchaseAmount { get; set; }

        public long? CustomerId { get; set; }
        public string? CustomerMobileNumber { get; set; }
        public string? CustomerEmailAddress { get; set; }

        public string? CustomerName { get; set; }

        public long ProductId { get; set; }
        public long ProductCodeId { get; set; }

        public long? MerchantId { get; set; }
        public long? StoreId { get; set; }

        public long? CreatedById { get; set; }


        public long? TransactionSourceId { get; set; }
        public long? ProgramId { get; set; }

        public long? AcquirerId { get; set; }

        public string MerchantDisplayName { get; set; }
    }

    public class FrameworkRedeemNotification
    {
        HCoreContext? _HCoreContext;
        ManageCoreTransaction? _ManageCoreTransaction;
        public void SendRedeemNotification(ORedeemNotification _ORedeemNotification)
        {
            _ManageCoreTransaction = new ManageCoreTransaction();
            if (_ORedeemNotification.ProductId != 0 && _ORedeemNotification.ProductCodeId != 0)
            {
                long UseLocationId = 0;
                long ProductOwnerId = 0;
                using (_HCoreContext = new HCoreContext())
                {
                    var ProductLocationDetails = _HCoreContext.CAProductLocation
                    .Where(x => x.ProductId == _ORedeemNotification.ProductId
                    && x.AccountId == _ORedeemNotification.MerchantId
                    && x.SubAccountId == _ORedeemNotification.StoreId)
                    .Select(x => x.Id).FirstOrDefault();
                    if (ProductLocationDetails != 0)
                    {
                        UseLocationId = ProductLocationDetails;
                    }
                    else
                    {
                        CAProductLocation _CAProductLocation;
                        _CAProductLocation = new CAProductLocation();
                        _CAProductLocation.Guid = HCoreHelper.GenerateGuid();
                        _CAProductLocation.ProductId = (long)_ORedeemNotification.ProductId;
                        _CAProductLocation.AccountId = (long)_ORedeemNotification.MerchantId;
                        _CAProductLocation.SubAccountId = (long)_ORedeemNotification.StoreId;
                        _CAProductLocation.CreateDate = HCoreHelper.GetGMTDateTime();
                        _CAProductLocation.CreatedById = (long)_ORedeemNotification.CreatedById;
                        _CAProductLocation.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.CAProductLocation.Add(_CAProductLocation);
                        _HCoreContext.SaveChanges();
                        UseLocationId = _CAProductLocation.Id;
                    }
                    _HCoreContext.Dispose();
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var ProductDetails = _HCoreContext.CAProduct.Where(x => x.Id == _ORedeemNotification.ProductId).FirstOrDefault();
                    if (ProductDetails != null)
                    {
                        ProductOwnerId = ProductDetails.AccountId;
                        var ProductCodeDetails = _HCoreContext.CAProductCode.Where(x => x.Id == _ORedeemNotification.ProductCodeId).FirstOrDefault();
                        if (ProductCodeDetails != null)
                        {
                            ProductCodeDetails.LastUseLocationId = UseLocationId;
                            ProductCodeDetails.LastUseDate = HCoreHelper.GetGMTDateTime();
                            ProductCodeDetails.UseCount += 1;
                            ProductCodeDetails.TransactionId = _ORedeemNotification.TransactionId;
                            ProductCodeDetails.AvailableAmount -= _ORedeemNotification.RedeemAmount;
                            if (ProductCodeDetails.AvailableAmount >= ProductCodeDetails.AvailableAmount)
                            {
                                ProductCodeDetails.StatusId = HelperStatus.ProdutCode.Used;
                            }
                            ProductDetails.TotalUsed += 1;
                            ProductDetails.TotalUsedAmount += _ORedeemNotification.RedeemAmount;
                            ProductDetails.LastUseLocationId = UseLocationId;
                            ProductDetails.LastUseDate = HCoreHelper.GetGMTDateTime();

                            CAProductUseHistory _CAProductUseHistory;
                            _CAProductUseHistory = new CAProductUseHistory();
                            _CAProductUseHistory.Guid = HCoreHelper.GenerateGuid();
                            _CAProductUseHistory.ProductId = (long)_ORedeemNotification.ProductId;
                            _CAProductUseHistory.AccountId = (long)_ORedeemNotification.CustomerId;
                            _CAProductUseHistory.ProductCodeId = ProductCodeDetails.Id;
                            _CAProductUseHistory.Amount = (long)_ORedeemNotification.RedeemAmount;
                            _CAProductUseHistory.LocationId = UseLocationId;
                            _CAProductUseHistory.CreateDate = HCoreHelper.GetGMTDateTime();
                            _CAProductUseHistory.CreatedById = (long)_ORedeemNotification.CreatedById;
                            _CAProductUseHistory.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.CAProductUseHistory.Add(_CAProductUseHistory);
                            _HCoreContext.SaveChanges();
                        }
                    }
                }
                if (ProductOwnerId != 0)
                {
                    _ManageCoreTransaction.UpdateAccountBalance((long)_ORedeemNotification.CustomerId, TransactionSource.GiftCards, ProductOwnerId);
                }
            }

            _ManageCoreTransaction.UpdateAccountBalance((long)_ORedeemNotification.CustomerId, TransactionSource.TUC);
            if (_ORedeemNotification.TransactionSourceId == TransactionSource.GiftPoints)
            {
                _ManageCoreTransaction.UpdateAccountBalance((long)_ORedeemNotification.CustomerId, TransactionSource.GiftPoints, (long)_ORedeemNotification.MerchantId);
            }

            double Balance = 0;
            if (_ORedeemNotification.ProgramId > 0)
            {
                var ProgramDetails = _ManageCoreTransaction.GetLoyaltyProgramBalance((long)_ORedeemNotification.CustomerId, (long)_ORedeemNotification.MerchantId, (long)_ORedeemNotification.AcquirerId);
                if (ProgramDetails != null)
                {
                    Balance = ProgramDetails.Balance;
                }
            }
            else
            {
                Balance = _ManageCoreTransaction.GetAppUserBalance((long)_ORedeemNotification.CustomerId);
            }

            if (HostEnvironment == HostEnvironmentType.Live || HostEnvironment == HostEnvironmentType.Test)
            {
                string RedeemSms = HCoreHelper.GetConfiguration("redeemsms", _ORedeemNotification.MerchantId);
                if (!string.IsNullOrEmpty(RedeemSms))
                {
                    #region Send SMS
                    string Message = RedeemSms
                                    .Replace("[AMOUNT]", HCoreHelper.RoundNumber(_ORedeemNotification.RedeemAmount, _AppConfig.SystemExitRoundDouble).ToString())
                                    .Replace("[BALANCE]", HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString())
                                    .Replace("[MERCHANT]", _ORedeemNotification.MerchantDisplayName);
                    HCoreHelper.SendSMS(SmsType.Transaction, "234", _ORedeemNotification.CustomerMobileNumber, Message, (long)_ORedeemNotification.CustomerId, _ORedeemNotification.TransactionKey, _ORedeemNotification.TransactionId);
                    #endregion
                }
                else
                {
                    #region Send SMS
                    string Message = "Redeem Alert: You just redeemed N" + HCoreHelper.RoundNumber(_ORedeemNotification.RedeemAmount, _AppConfig.SystemExitRoundDouble).ToString() + " Cash at " + _ORedeemNotification.MerchantDisplayName + ". Your TUC Bal:N" + HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString() + ".  Download TUC App: https://bit.ly/tuc-app. Thank U Very Much";
                    HCoreHelper.SendSMS(SmsType.Transaction, "234", _ORedeemNotification.CustomerMobileNumber, Message, (long)_ORedeemNotification.CustomerId, _ORedeemNotification.TransactionKey, _ORedeemNotification.TransactionId);
                    #endregion
                }
                #region Send Email 
                if (!string.IsNullOrEmpty(_ORedeemNotification.CustomerEmailAddress))
                {
                    var _EmailParameters = new
                    {
                        UserDisplayName = _ORedeemNotification.CustomerName,
                        MerchantName = _ORedeemNotification.MerchantDisplayName,
                        InvoiceAmount = _ORedeemNotification.PurchaseAmount.ToString(),
                        Amount = _ORedeemNotification.RedeemAmount.ToString(),
                        Balance = Balance.ToString(),
                    };
                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RedeemEmail, _ORedeemNotification.CustomerName, _ORedeemNotification.CustomerEmailAddress, _EmailParameters, null);
                }
                #endregion
            }

        }
    }

    public class RedeemNotificationActor : ReceiveActor
    {
        CoreRedeem _CoreRedeem;
        HCoreContext _HCoreContext;
        ManageCoreTransaction _ManageCoreTransaction;
        public RedeemNotificationActor()
        {
            Receive<ORedeemNotification>(_ORedeemNotification =>
            {
                FrameworkRedeemNotification _FrameworkRedeemNotification = new FrameworkRedeemNotification();
                _FrameworkRedeemNotification.SendRedeemNotification(_ORedeemNotification);
            });
        }
    }
}

