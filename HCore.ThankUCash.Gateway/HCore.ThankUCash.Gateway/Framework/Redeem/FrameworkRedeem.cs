﻿using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.ThankUCash.Gateway.Core;
using HCore.ThankUCash.Gateway.Framework;
using HCore.ThankUCash.Gateway.Object;
using Microsoft.EntityFrameworkCore;
using Mono;
using Z.EntityFramework.Plus;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.ThankUCash.Gateway.Helper.HelperGateway;
namespace HCore.ThankUCash.Gateway.Framework.Redeem
{
    public class FrameworkRedeem
    {
        #region Declare
        CoreOperations? _CoreOperations;
        FrameworkThankUCashGateway? _FrameworkThankUCashGateway;
        HCoreContext? _HCoreContext;
        ManageCoreTransaction? _ManageCoreTransaction;
        OThankUGateway.Response? _GatewayResponse;
        OCoreTransaction.Request? _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem>? _TransactionItems;
        #endregion
        internal async Task<OResponse> Redeem(OThankUGateway.Request _Request)
        {
            _GatewayResponse = new OThankUGateway.Response();
            try
            {
                if ((_Request.UserReference?.AccountTypeId == UserAccountType.PosAccount || _Request.UserReference?.AccountTypeId == UserAccountType.PgAccount) && string.IsNullOrEmpty(_Request.MerchantId) && string.IsNullOrEmpty(_Request.TerminalId))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100", CoreResources.HCG100);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109", CoreResources.HCG109);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115", CoreResources.HCG115M);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Pin))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG110");
                    #endregion
                }
                else
                {
                    _CoreOperations = new CoreOperations();
                    OGatewayInfo _OGatewayInfo = _CoreOperations.GetGatewayInfo(_Request);
                    #region Perform Operations
                    if (_OGatewayInfo.Status == ResponseStatus.Success)
                    {
                        OUserInfo _UserInfo = _CoreOperations.GetUserInfo(_Request, _OGatewayInfo, false);
                        if (_UserInfo.Status == ResponseStatus.Success)
                        {
                            switch (_UserInfo.AccountStatusId)
                            {
                                case HelperStatus.Default.Active:
                                    string DPin = HCoreEncrypt.DecryptHash(_UserInfo.Pin);
                                    if (DPin == _Request.Pin)
                                    {
                                        #region DEAL Redeem
                                        if (!string.IsNullOrEmpty(_UserInfo.DealKey))
                                        {
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                var DealCodeDetails = await _HCoreContext.MDDealCode.Where(x => x.Guid == _UserInfo.DealKey)
                                                          .Select(x => new ODealOperation.DealCode.Response
                                                          {
                                                              ReferenceId = x.Id,
                                                              ReferenceKey = x.Guid,

                                                              DealReferenceId = x.DealId,
                                                              DealReferenceKey = x.Deal.Guid,
                                                              ItemCode = x.ItemCode,

                                                              StartDate = x.StartDate,
                                                              EndDate = x.EndDate,

                                                              ActualPrice = x.Deal.ActualPrice,
                                                              SellingPrice = x.Deal.SellingPrice,
                                                              CommissionAmount = x.Deal.CommissionAmount,
                                                              Amount = x.ItemAmount,
                                                              TotalAmount = x.Deal.TotalAmount,

                                                              DealStartDate = x.Deal.StartDate,
                                                              DealEndDate = x.Deal.EndDate,

                                                              Title = x.Deal.Title,
                                                              Description = x.Deal.Description,
                                                              UsageInformation = x.Deal.UsageInformation,
                                                              Terms = x.Deal.Terms,
                                                              ImageUrl = x.Deal.PosterStorage.Path,

                                                              MerchantReferenceId = x.Deal.AccountId,
                                                              MerchantReferenceKey = x.Deal.Account.Guid,
                                                              MerchantDisplayName = x.Deal.Account.DisplayName,
                                                              MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                                                              UseDate = x.LastUseDate,
                                                              UseLocationId = x.LastUseLocationId,
                                                              UseLocationKey = x.LastUseLocation.Guid,
                                                              UseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                              UseLocationAddress = x.LastUseLocation.Address,

                                                              CustomerId = x.Account.Id,
                                                              CustomerKey = x.Account.Guid,
                                                              CustomerDisplayName = x.Account.DisplayName,
                                                              CustomerMobileNumber = x.Account.MobileNumber,
                                                              CustomerIconUrl = x.Account.IconStorage.Path,

                                                              CreateDate = x.CreateDate,
                                                              StatusId = x.StatusId,
                                                              StatusCode = x.Status.SystemName,
                                                              StatusName = x.Status.Name,
                                                          })
                                                          .FirstOrDefaultAsync();
                                                if (DealCodeDetails != null)
                                                {
                                                    if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Unused)
                                                    {
                                                        DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                                        if (CurrentTime < DealCodeDetails.StartDate)
                                                        {
                                                            await _HCoreContext.DisposeAsync();
                                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0127, TUCDealResource.HCP0127M);
                                                        }
                                                        else if (CurrentTime > DealCodeDetails.EndDate)
                                                        {
                                                            await _HCoreContext.DisposeAsync();
                                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0128, TUCDealResource.HCP0128M);
                                                        }
                                                        else if (CurrentTime > DealCodeDetails.StartDate && CurrentTime < DealCodeDetails.EndDate)
                                                        {
                                                            var DealInfo = await _HCoreContext.MDDealCode.Where(x => x.Id == DealCodeDetails.ReferenceId).FirstOrDefaultAsync();
                                                            DealInfo.UseAttempts = 1;
                                                            DealInfo.UseCount = 1;
                                                            DealInfo.LastUseSource = Helpers.DealRedeemSource.Pos;
                                                            DealInfo.LastUseDate = HCoreHelper.GetGMTDateTime();
                                                            if (_OGatewayInfo.StoreId != 0)
                                                            {
                                                                DealInfo.LastUseLocationId = _OGatewayInfo.StoreId;
                                                            }
                                                            if (_OGatewayInfo.TerminalId > 0)
                                                            {
                                                                DealInfo.TerminalId = _OGatewayInfo.TerminalId;
                                                            }
                                                            if (_OGatewayInfo.CashierId > 0)
                                                            {
                                                                DealInfo.CashierId = _OGatewayInfo.CashierId;
                                                            }
                                                            DealInfo.StatusId = HelperStatus.DealCodes.Used;
                                                            await _HCoreContext.SaveChangesAsync();
                                                            using (_HCoreContext = new HCoreContext())
                                                            {
                                                                string? UserNotificationUrl = await _HCoreContext.HCUAccountSession.Where(x => x.AccountId == DealInfo.AccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefaultAsync();
                                                                if (!string.IsNullOrEmpty(UserNotificationUrl))
                                                                {
                                                                    HCoreHelper.SendPushToDevice(UserNotificationUrl, "dealpurchasedetails", "Deal Redeemed", DealCodeDetails.Title + " deal redeemed at " + DealCodeDetails.MerchantDisplayName, "dealpurchasedetails", DealInfo.Id, DealInfo.Guid, "View details", true, null);
                                                                }
                                                            }
                                                            _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
                                                            _GatewayResponse.TransactionDate = HCoreHelper.GetGMTDateTime();
                                                            _GatewayResponse.TransactionReference = _Request.TransactionReference;
                                                            _GatewayResponse.RedeemAmount = (long)((HCoreHelper.RoundNumber((double)DealInfo.ItemAmount, 2)) * 100);
                                                            #region Send Response
                                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG125", CoreResources.HCG125);
                                                            #endregion
                                                        }
                                                        else
                                                        {
                                                            await _HCoreContext.DisposeAsync();
                                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0129, TUCDealResource.HCP0129M);
                                                        }
                                                    }
                                                    else if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Used)
                                                    {
                                                        await _HCoreContext.DisposeAsync();
                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0123, TUCDealResource.HCP0123M);
                                                    }
                                                    else if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Expired)
                                                    {
                                                        await _HCoreContext.DisposeAsync();
                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0124, TUCDealResource.HCP0124M);
                                                    }
                                                    else if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Blocked)
                                                    {
                                                        await _HCoreContext.DisposeAsync();
                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0125, TUCDealResource.HCP0125M);
                                                    }
                                                    else
                                                    {
                                                        await _HCoreContext.DisposeAsync();
                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0126, TUCDealResource.HCP0126M);
                                                    }
                                                }
                                                else
                                                {
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealResource.HCP0122, TUCDealResource.HCP0122M);
                                                }
                                            }
                                        }
                                        #endregion
                                        #region BNPL Redeem
                                        else if (!string.IsNullOrEmpty(_UserInfo.LoanKey))
                                        {
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                var LoanDetails = _HCoreContext?.BNPLAccountLoan.Where(x => x.Guid == _UserInfo.LoanKey)
                                                              .Select(x => new ODealOperation.DealCode.Response
                                                              {
                                                                  ReferenceId = x.Id,
                                                                  ReferenceKey = x.Guid,

                                                                  StartDate = x.StartDate,
                                                                  EndDate = x.EndDate,
                                                                  Amount = x.Amount,
                                                                  TotalAmount = x.TotalAmount,
                                                                  CustomerId = x.Account.Id,
                                                                  CustomerKey = x.Account.Guid,
                                                                  CustomerMobileNumber = x.Account.MobileNumber,
                                                                  CreateDate = x.CreateDate,
                                                                  StatusId = x.StatusId,
                                                                  StatusCode = x.Status.SystemName,
                                                                  StatusName = x.Status.Name,
                                                                  IsRedeemed = x.IsRedeemed,
                                                              })
                                                              .FirstOrDefault();
                                                if (LoanDetails != null)
                                                {
                                                    if (LoanDetails.StatusId == 740 && LoanDetails.IsRedeemed == 0)
                                                    {

                                                        var DealInfo = _HCoreContext.BNPLAccountLoan.Where(x => x.Id == LoanDetails.ReferenceId).FirstOrDefault();
                                                        DealInfo.IsRedeemed = 1;
                                                        DealInfo.RedeemDate = HCoreHelper.GetGMTDateTime();
                                                        if (_OGatewayInfo.StoreId != 0)
                                                        {
                                                            DealInfo.RedeemLocationId = _OGatewayInfo.StoreId;
                                                        }
                                                        if (_OGatewayInfo.TerminalId > 0)
                                                        {
                                                            DealInfo.TerminalId = _OGatewayInfo.TerminalId;
                                                        }
                                                        if (_OGatewayInfo.CashierId > 0)
                                                        {
                                                            DealInfo.CashierId = _OGatewayInfo.CashierId;
                                                        }
                                                        _HCoreContext.SaveChanges();

                                                        _CoreTransactionRequest = new OCoreTransaction.Request();
                                                        _CoreTransactionRequest.CustomerId = (long)LoanDetails.CustomerId;
                                                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                                                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                                        _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
                                                        _CoreTransactionRequest.InvoiceAmount = (double)LoanDetails.TotalAmount;
                                                        _CoreTransactionRequest.ReferenceInvoiceAmount = (double)LoanDetails.TotalAmount;
                                                        _CoreTransactionRequest.ReferenceAmount = (double)LoanDetails.TotalAmount;
                                                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                        {
                                                            UserAccountId = (long)LoanDetails.CustomerId,
                                                            ModeId = TransactionMode.Debit,
                                                            TypeId = TransactionType.TUCBnpl.LoanRedeem,
                                                            SourceId = TransactionSource.TUCBnpl,
                                                            Amount = (double)LoanDetails.TotalAmount,
                                                            TotalAmount = (double)LoanDetails.TotalAmount,
                                                        });
                                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                        {
                                                            UserAccountId = _OGatewayInfo.MerchantId,
                                                            ModeId = TransactionMode.Credit,
                                                            TypeId = TransactionType.TUCBnpl.LoanCredit,
                                                            SourceId = TransactionSource.TUCBnpl,
                                                            Amount = (double)LoanDetails.TotalAmount,
                                                            TotalAmount = (double)LoanDetails.TotalAmount,
                                                        });

                                                        _CoreTransactionRequest.Transactions = _TransactionItems;
                                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                                        {
                                                            using (_HCoreContext = new HCoreContext())
                                                            {
                                                                string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == DealInfo.AccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                                                if (!string.IsNullOrEmpty(UserNotificationUrl))
                                                                {
                                                                    HCoreHelper.SendPushToDevice(UserNotificationUrl, "dealpurchasedetails", "Loan Redeemed", "Loan redeemed at " + _OGatewayInfo.MerchantDisplayName, "loandetails", DealInfo.Id, DealInfo.Guid, "View details", true, null);
                                                                }
                                                            }
                                                            _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
                                                            _GatewayResponse.TransactionDate = HCoreHelper.GetGMTDateTime();
                                                            _GatewayResponse.TransactionReference = _Request.TransactionReference;
                                                            _GatewayResponse.RedeemAmount = (long)((HCoreHelper.RoundNumber((double)DealInfo.Amount, 2)) * 100);
                                                            #region Send Response
                                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG125", CoreResources.HCG125);
                                                            #endregion
                                                        }
                                                        else
                                                        {
                                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealResource.HCP0126, TUCDealResource.HCP0126M);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _HCoreContext.SaveChanges();
                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealResource.HCP0126, TUCDealResource.HCP0126M);
                                                    }
                                                }
                                                else
                                                {
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealResource.HCP0122, TUCDealResource.HCP0122M);
                                                }
                                            }
                                        }
                                        #endregion
                                        #region Loyalty / Points Redeem
                                        else
                                        {
                                            if ((_Request.RedeemAmount / 100) < 1)
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG128", CoreResources.HCG128);
                                                #endregion
                                            }
                                            else if ((_Request.InvoiceAmount / 100) < 1)
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG127", CoreResources.HCG127);
                                                #endregion
                                            }
                                            else if (_Request.RedeemAmount > _Request.InvoiceAmount)
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG117", CoreResources.HCG117);
                                                #endregion
                                            }
                                            else
                                            {
                                                _ManageCoreTransaction = new ManageCoreTransaction();
                                                long TUCGold = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashgold", _OGatewayInfo.MerchantId));
                                                double RedeemAmount = (double)_Request.RedeemAmount / 100;
                                                double InvoiceAmount = (double)_Request.InvoiceAmount / 100;
                                                double UserBalance = 0;
                                                double ProductAmount = 0;
                                                long ProductId = 0;
                                                long ProductTransactionSourceId = 0;
                                                long ProductCodeId = 0;
                                                long RedeemSourceId = 0;
                                                double UserGiftPointBalance = 0;
                                                if (TUCGold > 0)
                                                {
                                                    UserBalance = _ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId, _OGatewayInfo.MerchantId, TransactionSource.TUCBlack);
                                                }
                                                else
                                                {
                                                    UserBalance = _ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId);
                                                }
                                                double LoyaltyProgramBalance = 0;
                                                var ProgramDetails = _ManageCoreTransaction.GetLoyaltyProgramBalance(_UserInfo.UserAccountId, _OGatewayInfo.MerchantId, (long)_OGatewayInfo.AcquirerId);
                                                if (ProgramDetails != null)
                                                {
                                                    LoyaltyProgramBalance = ProgramDetails.Balance;
                                                }
                                                if (LoyaltyProgramBalance >= RedeemAmount)
                                                {
                                                    RedeemSourceId = TransactionSource.TUCBlack;
                                                }
                                                else if (UserBalance >= RedeemAmount)
                                                {
                                                    if (TUCGold > 0)
                                                    {
                                                        RedeemSourceId = TransactionSource.TUCBlack;
                                                    }
                                                    else
                                                    {
                                                        RedeemSourceId = TransactionSource.TUC;
                                                    }
                                                }
                                                else
                                                {
                                                    UserGiftPointBalance = _ManageCoreTransaction.GetAppUserGiftPointsBalance(_UserInfo.UserAccountId, _OGatewayInfo.MerchantId);
                                                    if (UserGiftPointBalance >= RedeemAmount)
                                                    {
                                                        RedeemSourceId = TransactionSource.GiftPoints;
                                                    }
                                                    else
                                                    {
                                                        #region Get User Products
                                                        using (_HCoreContext = new HCoreContext())
                                                        {
                                                            var ProductBalance = _HCoreContext.CAProductCode
                                                                                      .Where(x => x.AccountId == _UserInfo.UserAccountId
                                                                                              && ((x.Product.UsageTypeId == HelperType.ProductUsageType.FullValue && x.AvailableAmount == RedeemAmount)
                                                                                              || (x.Product.UsageTypeId == HelperType.ProductUsageType.PartialValue && x.AvailableAmount >= RedeemAmount))
                                                                                              && (x.Product.TypeId == Product.GiftCard || x.Product.TypeId == Product.QuickGiftCard)
                                                                                              && x.AvailableAmount <= RedeemAmount
                                                                                              && x.AvailableAmount > 0
                                                                                              && x.StatusId == HelperStatus.ProdutCode.Unused)
                                                                          .OrderByDescending(x => x.AvailableAmount)
                                                                                      .Select(x => new
                                                                                      {
                                                                                          ProductId = x.ProductId,
                                                                                          CodeId = x.Id,
                                                                                          x.AvailableAmount,
                                                                                          UsageTypeId = x.Product.UsageTypeId
                                                                                      })
                                                                                      .FirstOrDefault();
                                                            if (ProductBalance != null)
                                                            {
                                                                ProductTransactionSourceId = TransactionSource.GiftCards;
                                                                ProductAmount = (double)ProductBalance.AvailableAmount;
                                                                ProductId = ProductBalance.ProductId;
                                                                ProductCodeId = ProductBalance.CodeId;
                                                            }
                                                            _HCoreContext.Dispose();
                                                        }
                                                        #endregion
                                                    }
                                                }


                                                bool AllowRedeem = false;
                                                double MerchantRewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(_CoreOperations.GetConfiguration("rewardpercentage", _OGatewayInfo.MerchantId)), _AppConfig.SystemRoundPercentage);
                                                if (ProductAmount >= RedeemAmount)
                                                {
                                                    RedeemSourceId = ProductTransactionSourceId;
                                                    if (MerchantRewardPercentage > 0)
                                                    {
                                                        AllowRedeem = false;
                                                    }
                                                }
                                                else if (UserGiftPointBalance >= RedeemAmount)
                                                {
                                                    RedeemSourceId = TransactionSource.GiftPoints;
                                                    if (MerchantRewardPercentage > 0)
                                                    {
                                                        AllowRedeem = true;
                                                    }
                                                }
                                                else if (LoyaltyProgramBalance >= RedeemAmount)
                                                {
                                                    RedeemSourceId = TransactionSource.TUCBlack;
                                                    AllowRedeem = true;
                                                }
                                                else if (UserBalance >= RedeemAmount)
                                                {
                                                    if (TUCGold > 0)
                                                    {
                                                        RedeemSourceId = TransactionSource.TUCBlack;
                                                    }
                                                    else
                                                    {
                                                        RedeemSourceId = TransactionSource.TUC;
                                                    }
                                                    if (MerchantRewardPercentage > 0)
                                                    {
                                                        AllowRedeem = true;
                                                    }
                                                }
                                                else
                                                {
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG120", CoreResources.HCG120);
                                                }

                                                if (AllowRedeem)
                                                {
                                                    string GroupKey = HCoreHelper.GenerateTransactionReference(_OGatewayInfo.MerchantId, _UserInfo.UserAccountId, "TRW");
                                                    _CoreTransactionRequest = new OCoreTransaction.Request();
                                                    _CoreTransactionRequest.CustomerId = _UserInfo.UserAccountId;
                                                    _CoreTransactionRequest.UserReference = _Request.UserReference;
                                                    _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                                    _CoreTransactionRequest.GroupKey = GroupKey;
                                                    _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
                                                    _CoreTransactionRequest.SubParentId = _OGatewayInfo.StoreId;
                                                    _CoreTransactionRequest.InvoiceAmount = InvoiceAmount;
                                                    _CoreTransactionRequest.ReferenceInvoiceAmount = InvoiceAmount;
                                                    _CoreTransactionRequest.ReferenceAmount = RedeemAmount;
                                                    _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
                                                    _CoreTransactionRequest.CashierId = _OGatewayInfo.CashierId;
                                                    _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                                    if (ProgramDetails != null)
                                                    {
                                                        _CoreTransactionRequest.ProgramId = ProgramDetails.ProgramId;
                                                    }
                                                    if (_OGatewayInfo.AcquirerId != null)
                                                    {
                                                        _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
                                                    }
                                                    _CoreTransactionRequest.TerminalId = _OGatewayInfo.TerminalId;
                                                    _TransactionItems = new List<OCoreTransaction.TransactionItem>();

                                                    if (RedeemSourceId == TransactionSource.GiftCards)
                                                    {
                                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                        {
                                                            UserAccountId = _UserInfo.UserAccountId,
                                                            ModeId = TransactionMode.Debit,
                                                            TypeId = TransactionType.Loyalty.TUCRedeem.GiftCardRedeem,
                                                            SourceId = TransactionSource.GiftCards,
                                                            Amount = RedeemAmount,
                                                            TotalAmount = RedeemAmount,
                                                            Comment = ProductCodeId.ToString(),
                                                        });
                                                    }
                                                    if (RedeemSourceId == TransactionSource.TUC)
                                                    {
                                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                        {
                                                            UserAccountId = _UserInfo.UserAccountId,
                                                            ModeId = TransactionMode.Debit,
                                                            TypeId = TransactionType.Loyalty.TUCRedeem.AppRedeem,
                                                            SourceId = TransactionSource.TUC,
                                                            Amount = RedeemAmount,
                                                            TotalAmount = RedeemAmount,
                                                        });
                                                    }
                                                    if (RedeemSourceId == TransactionSource.TUCBlack)
                                                    {
                                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                        {
                                                            UserAccountId = _UserInfo.UserAccountId,
                                                            ModeId = TransactionMode.Debit,
                                                            TypeId = TransactionType.Loyalty.TUCRedeem.AppRedeem,
                                                            SourceId = TransactionSource.TUCBlack,
                                                            Amount = RedeemAmount,
                                                            TotalAmount = RedeemAmount,
                                                        });
                                                    }
                                                    if (RedeemSourceId == TransactionSource.GiftPoints)
                                                    {
                                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                        {
                                                            UserAccountId = _UserInfo.UserAccountId,
                                                            ModeId = TransactionMode.Debit,
                                                            TypeId = TransactionType.Loyalty.TUCRedeem.GiftPointRedeem,
                                                            SourceId = TransactionSource.GiftPoints,
                                                            Amount = RedeemAmount,
                                                            TotalAmount = RedeemAmount,
                                                        });
                                                    }
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = _OGatewayInfo.MerchantId,
                                                        ModeId = TransactionMode.Credit,
                                                        TypeId = TransactionType.Loyalty.TUCRedeem.PosRedeem,
                                                        SourceId = TransactionSource.Settlement,
                                                        Amount = RedeemAmount,
                                                        TotalAmount = RedeemAmount,
                                                    });
                                                    _CoreTransactionRequest.Transactions = _TransactionItems;
                                                    OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                                    if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                                    {

                                                        #region Trigger Notification
                                                        ORedeemNotification _ORedeemNotification = new ORedeemNotification();
                                                        _ORedeemNotification.TransactionId = TransactionResponse.ReferenceId;
                                                        _ORedeemNotification.TransactionKey = TransactionResponse.ReferenceKey;
                                                        _ORedeemNotification.RedeemAmount = RedeemAmount;
                                                        _ORedeemNotification.PurchaseAmount = InvoiceAmount;
                                                        _ORedeemNotification.CustomerId = _UserInfo.UserAccountId;
                                                        _ORedeemNotification.CustomerMobileNumber = _UserInfo.MobileNumber;
                                                        _ORedeemNotification.CustomerEmailAddress = _UserInfo.EmailAddress;
                                                        _ORedeemNotification.CustomerName = _UserInfo.DisplayName;
                                                        _ORedeemNotification.MerchantId = _OGatewayInfo.MerchantId;
                                                        _ORedeemNotification.StoreId = _OGatewayInfo.StoreId;
                                                        _ORedeemNotification.CreatedById = _Request.UserReference?.AccountId;
                                                        _ORedeemNotification.TransactionSourceId = RedeemSourceId;
                                                        _ORedeemNotification.ProgramId = ProgramDetails?.ProgramId;
                                                        _ORedeemNotification.AcquirerId = _OGatewayInfo.AcquirerId;
                                                        _ORedeemNotification.MerchantDisplayName = _OGatewayInfo.MerchantDisplayName;
                                                        var _TransactionPostProcessActor = ActorSystem.Create("RedeemNotificationActor");
                                                        var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<RedeemNotificationActor>("RedeemNotificationActor");
                                                        _TransactionPostProcessActorNotify.Tell(_ORedeemNotification);
                                                        #endregion
                                                        _GatewayResponse.TransactionDate = HCoreHelper.GetGMTDateTime();
                                                        _GatewayResponse.RedeemAmount = (long)((HCoreHelper.RoundNumber(RedeemAmount, 2)) * 100);
                                                        _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
                                                        _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
                                                        _GatewayResponse.TransactionReference = TransactionResponse.GroupKey;
                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG121");
                                                    }
                                                    else
                                                    {
                                                        #region Send Response
                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                                        #endregion
                                                    }
                                                }
                                                else
                                                {
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG120", CoreResources.HCG136);
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG114", CoreResources.HCG114);
                                    }
                                case HelperStatus.Default.Suspended:
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, "HCG131", CoreResources.HCG131);
                                case HelperStatus.Default.Blocked:
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, "HCG132", CoreResources.HCG132);
                                default:
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, "HCG133", CoreResources.HCG133);
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _UserInfo.ResponseCode, _UserInfo.Message);
                            #endregion
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _OGatewayInfo.ResponseCode, _OGatewayInfo.Message);
                    }
                    #endregion

                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogExceptionAsync("Redeem", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG500, CoreResources.HCG500M);
                #endregion
            }
        }
        internal async Task<OResponse> InitializeRedeem(OThankUGateway.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            await Task.Delay(0);
            try
            {
                if ((_Request.UserReference.AccountTypeId == UserAccountType.PosAccount || _Request.UserReference.AccountTypeId == UserAccountType.PgAccount) && string.IsNullOrEmpty(_Request.MerchantId) && string.IsNullOrEmpty(_Request.TerminalId))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100", CoreResources.HCG100);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109", CoreResources.HCG109);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115", CoreResources.HCG115M);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Pin))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG110");
                    #endregion
                }
                else
                {
                    _CoreOperations = new CoreOperations();
                    OGatewayInfo _OGatewayInfo = _CoreOperations.GetGatewayInfo(_Request);
                    #region Perform Operations
                    if (_OGatewayInfo.Status == ResponseStatus.Success)
                    {
                        OUserInfo _UserInfo = _CoreOperations.GetUserInfo(_Request, _OGatewayInfo, false);
                        if (_UserInfo.Status == ResponseStatus.Success)
                        {
                            switch (_UserInfo.AccountStatusId)
                            {
                                case HelperStatus.Default.Active:
                                    if (string.IsNullOrEmpty(_UserInfo.Pin))
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG112", CoreResources.HCG112);
                                        #endregion
                                    }
                                    else
                                    {
                                        string DPin = null;
                                        try
                                        {
                                            DPin = HCoreEncrypt.DecryptHash(_UserInfo.Pin);
                                        }
                                        catch (Exception _Exception)
                                        {
                                            #region  Log Exception
                                            HCoreHelper.LogException(LogLevel.High, "Redeem Pin Error" + _UserInfo.Pin, _Exception, _Request.UserReference);
                                            #endregion
                                        }
                                        if (DPin == _Request.Pin)
                                        {
                                            if (!string.IsNullOrEmpty(_UserInfo.DealKey))
                                            {
                                                using (_HCoreContext = new HCoreContext())
                                                {
                                                    var DealCodeDetails = _HCoreContext.MDDealCode.Where(x => x.Guid == _UserInfo.DealKey)
                                                            .Select(x => new ODealOperation.DealCode.Response
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                ItemCode = x.ItemCode,
                                                                StartDate = x.StartDate,
                                                                EndDate = x.EndDate,
                                                                Amount = x.ItemAmount,
                                                                MerchantReferenceId = x.Deal.AccountId,
                                                                CustomerId = x.Account.Id,
                                                                StatusId = x.StatusId,
                                                            })
                                                            .FirstOrDefault();
                                                    if (DealCodeDetails != null)
                                                    {
                                                        if (DealCodeDetails.MerchantReferenceId == _OGatewayInfo.MerchantId)
                                                        {
                                                            if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Unused)
                                                            {
                                                                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                                                if (CurrentTime < DealCodeDetails.StartDate)
                                                                {
                                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0127, TUCDealResource.HCP0127M);
                                                                }
                                                                else if (CurrentTime > DealCodeDetails.EndDate)
                                                                {
                                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0128, TUCDealResource.HCP0128M);
                                                                }
                                                                else if (CurrentTime > DealCodeDetails.StartDate && CurrentTime < DealCodeDetails.EndDate)
                                                                {
                                                                    _GatewayResponse.InvoiceAmount = (long)((HCoreHelper.RoundNumber((double)DealCodeDetails.Amount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                                                    _GatewayResponse.RedeemAmount = (long)((HCoreHelper.RoundNumber((double)DealCodeDetails.Amount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                                                    _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
                                                                    _GatewayResponse.TransactionReference = "MADDEAL" + _UserInfo.DealKey;
                                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG121");
                                                                }
                                                                else
                                                                {
                                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0129, TUCDealResource.HCP0129M);
                                                                }
                                                            }
                                                            else if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Used)
                                                            {
                                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0139, TUCDealResource.HCP0139M);
                                                            }
                                                            else if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Expired)
                                                            {
                                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0138, TUCDealResource.HCP0138M);
                                                            }
                                                            else if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Blocked)
                                                            {
                                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0137, TUCDealResource.HCP0137M);
                                                            }
                                                            else
                                                            {
                                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0136, TUCDealResource.HCP0136M);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealResource.HCP0135, TUCDealResource.HCP0135M);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealResource.HCP0134, TUCDealResource.HCP0134M);
                                                    }
                                                }
                                            }
                                            else if (!string.IsNullOrEmpty(_UserInfo.LoanKey))
                                            {
                                                using (_HCoreContext = new HCoreContext())
                                                {
                                                    var LoanDetails = _HCoreContext.BNPLAccountLoan.Where(x => x.Guid == _UserInfo.LoanKey)
                                                            .Select(x => new ODealOperation.DealCode.Response
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                ItemCode = x.LoanCode,
                                                                StartDate = x.StartDate,
                                                                EndDate = x.EndDate,
                                                                Amount = x.Amount,
                                                                MerchantReferenceId = x.Merchant.Account.Id,
                                                                CustomerId = x.Account.Account.Id,
                                                                StatusId = x.StatusId,
                                                                IsRedeemed = x.IsRedeemed
                                                            })
                                                            .FirstOrDefault();
                                                    if (LoanDetails != null)
                                                    {
                                                        if (LoanDetails.MerchantReferenceId == _OGatewayInfo.MerchantId)
                                                        {
                                                            if (LoanDetails.StatusId == 740 && LoanDetails.IsRedeemed == 0)
                                                            {
                                                                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();

                                                                _GatewayResponse.InvoiceAmount = (long)(((double)HCoreHelper.RoundNumber((double)LoanDetails.Amount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                                                _GatewayResponse.RedeemAmount = (long)(((double)HCoreHelper.RoundNumber((double)LoanDetails.Amount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                                                _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
                                                                _GatewayResponse.TransactionReference = "BNPL" + _UserInfo.LoanKey;
                                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG121");

                                                            }
                                                            else
                                                            {
                                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, LoanDetails, TUCDealResource.HCP0136, TUCDealResource.HCP0136M);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealResource.HCP0135, TUCDealResource.HCP0135M);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealResource.HCP0134, TUCDealResource.HCP0134M);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if ((_Request.RedeemAmount / 100) < 1)
                                                {
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG128", CoreResources.HCG128);
                                                    #endregion
                                                }
                                                else if ((_Request.InvoiceAmount / 100) < 1)
                                                {
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG127", CoreResources.HCG127);
                                                    #endregion
                                                }

                                                else if (_Request.RedeemAmount > _Request.InvoiceAmount)
                                                {
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG117", CoreResources.HCG117);
                                                    #endregion
                                                }
                                            }

                                            long TUCGold = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashgold", _OGatewayInfo.MerchantId));
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            double RedeemAmount = (double)_Request.RedeemAmount / 100;
                                            double InvoiceAmount = (double)_Request.InvoiceAmount / 100;
                                            double UserBalance = 0;
                                            if (TUCGold > 0)
                                            {
                                                UserBalance = _ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId, _OGatewayInfo.MerchantId, TransactionSource.TUCBlack);
                                            }
                                            else
                                            {
                                                UserBalance = _ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId);
                                            }
                                            double LoyaltyProgramBalance = 0;
                                            var ProgramDetails = _ManageCoreTransaction.GetLoyaltyProgramBalance(_UserInfo.UserAccountId, _OGatewayInfo.MerchantId, (long)_OGatewayInfo.AcquirerId);
                                            if (ProgramDetails != null)
                                            {
                                                LoyaltyProgramBalance = ProgramDetails.Balance;
                                            }
                                            double ProductAmount = 0;
                                            long ProductId = 0;
                                            long ProductTransactionSourceId = 0;
                                            long ProductCodeId = 0;
                                            long RedeemSourceId = 0;
                                            double UserGiftPointBalance = 0;
                                            if (LoyaltyProgramBalance >= RedeemAmount)
                                            {
                                                RedeemSourceId = TransactionSource.TUCBlack;
                                            }
                                            else if (UserBalance >= RedeemAmount)
                                            {
                                                if (TUCGold > 0)
                                                {
                                                    RedeemSourceId = TransactionSource.TUCBlack;
                                                }
                                                else
                                                {
                                                    RedeemSourceId = TransactionSource.TUC;
                                                }
                                            }
                                            else
                                            {
                                                UserGiftPointBalance = _ManageCoreTransaction.GetAppUserGiftPointsBalance(_UserInfo.UserAccountId, _OGatewayInfo.MerchantId);
                                                if (UserGiftPointBalance >= RedeemAmount)
                                                {
                                                    RedeemSourceId = TransactionSource.GiftPoints;
                                                }
                                                else
                                                {
                                                    #region Get User Products
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        var ProductBalance = _HCoreContext.CAProductCode
                                                                                  .Where(x => x.AccountId == _UserInfo.UserAccountId
                                                                                          && ((x.Product.UsageTypeId == HelperType.ProductUsageType.FullValue && x.AvailableAmount == RedeemAmount)
                                                                                          || (x.Product.UsageTypeId == HelperType.ProductUsageType.PartialValue && x.AvailableAmount >= RedeemAmount))
                                                                                          && (x.Product.TypeId == Product.GiftCard || x.Product.TypeId == Product.QuickGiftCard)
                                                                                          && x.AvailableAmount <= RedeemAmount
                                                                                          && x.AvailableAmount > 0
                                                                                          && x.StatusId == HelperStatus.ProdutCode.Unused)
                                                                      .OrderByDescending(x => x.AvailableAmount)
                                                                                  .Select(x => new
                                                                                  {
                                                                                      ProductId = x.ProductId,
                                                                                      CodeId = x.Id,
                                                                                      x.AvailableAmount,
                                                                                      UsageTypeId = x.Product.UsageTypeId
                                                                                  })
                                                                                  .FirstOrDefault();
                                                        if (ProductBalance != null)
                                                        {
                                                            ProductTransactionSourceId = TransactionSource.GiftCards;
                                                            ProductAmount = (double)ProductBalance.AvailableAmount;
                                                            ProductId = ProductBalance.ProductId;
                                                            ProductCodeId = ProductBalance.CodeId;
                                                        }
                                                        _HCoreContext.Dispose();
                                                    }
                                                    #endregion
                                                }
                                            }

                                            bool AllowRedeem = false;
                                            double MerchantRewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(_CoreOperations.GetConfiguration("rewardpercentage", _OGatewayInfo.MerchantId)), _AppConfig.SystemRoundPercentage);
                                            if (ProductAmount >= RedeemAmount)
                                            {
                                                RedeemSourceId = ProductTransactionSourceId;
                                                if (MerchantRewardPercentage > 0)
                                                {
                                                    AllowRedeem = false;
                                                }
                                            }
                                            else if (UserGiftPointBalance >= RedeemAmount)
                                            {
                                                RedeemSourceId = TransactionSource.GiftPoints;
                                                if (MerchantRewardPercentage > 0)
                                                {
                                                    AllowRedeem = true;
                                                }
                                            }
                                            else if (LoyaltyProgramBalance >= RedeemAmount)
                                            {
                                                RedeemSourceId = TransactionSource.TUCBlack;
                                                AllowRedeem = true;
                                            }
                                            else if (UserBalance >= RedeemAmount)
                                            {
                                                if (TUCGold > 0)
                                                {
                                                    RedeemSourceId = TransactionSource.TUCBlack;
                                                }
                                                else
                                                {
                                                    RedeemSourceId = TransactionSource.TUC;
                                                }
                                                if (MerchantRewardPercentage > 0)
                                                {
                                                    AllowRedeem = true;
                                                }
                                            }
                                            else
                                            {
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG120", CoreResources.HCG120);
                                            }
                                            if (AllowRedeem)
                                            {
                                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                                _CoreTransactionRequest.CustomerId = _UserInfo.UserAccountId;
                                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Initialized;
                                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                                _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
                                                _CoreTransactionRequest.SubParentId = _OGatewayInfo.StoreId;
                                                _CoreTransactionRequest.InvoiceAmount = InvoiceAmount;
                                                _CoreTransactionRequest.ReferenceInvoiceAmount = InvoiceAmount;
                                                _CoreTransactionRequest.ReferenceAmount = RedeemAmount;
                                                _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
                                                _CoreTransactionRequest.CashierId = _OGatewayInfo.CashierId;
                                                _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                                if (ProgramDetails != null)
                                                {
                                                    _CoreTransactionRequest.ProgramId = ProgramDetails.ProgramId;
                                                }
                                                if (_OGatewayInfo.AcquirerId != null)
                                                {
                                                    _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
                                                }
                                                _CoreTransactionRequest.TerminalId = _OGatewayInfo.TerminalId;
                                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();

                                                if (RedeemSourceId == TransactionSource.GiftCards)
                                                {
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = _UserInfo.UserAccountId,
                                                        ModeId = TransactionMode.Debit,
                                                        TypeId = TransactionType.Loyalty.TUCRedeem.GiftCardRedeem,
                                                        SourceId = TransactionSource.GiftCards,
                                                        Amount = RedeemAmount,
                                                        TotalAmount = RedeemAmount,
                                                        Comment = ProductCodeId.ToString(),
                                                    });
                                                }
                                                if (RedeemSourceId == TransactionSource.TUC)
                                                {
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = _UserInfo.UserAccountId,
                                                        ModeId = TransactionMode.Debit,
                                                        TypeId = TransactionType.Loyalty.TUCRedeem.AppRedeem,
                                                        SourceId = TransactionSource.TUC,
                                                        Amount = RedeemAmount,
                                                        TotalAmount = RedeemAmount,
                                                    });
                                                }
                                                if (RedeemSourceId == TransactionSource.TUCBlack)
                                                {
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = _UserInfo.UserAccountId,
                                                        ModeId = TransactionMode.Debit,
                                                        TypeId = TransactionType.Loyalty.TUCRedeem.AppRedeem,
                                                        SourceId = TransactionSource.TUCBlack,
                                                        Amount = RedeemAmount,
                                                        TotalAmount = RedeemAmount,
                                                    });
                                                }
                                                if (RedeemSourceId == TransactionSource.GiftPoints)
                                                {
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = _UserInfo.UserAccountId,
                                                        ModeId = TransactionMode.Debit,
                                                        TypeId = TransactionType.Loyalty.TUCRedeem.GiftPointRedeem,
                                                        SourceId = TransactionSource.GiftPoints,
                                                        Amount = RedeemAmount,
                                                        TotalAmount = RedeemAmount,
                                                    });
                                                }
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _OGatewayInfo.MerchantId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionType.Loyalty.TUCRedeem.PosRedeem,
                                                    SourceId = TransactionSource.Settlement,
                                                    Amount = RedeemAmount,
                                                    TotalAmount = RedeemAmount,
                                                });
                                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                                {
                                                    _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
                                                    _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
                                                    _GatewayResponse.TransactionReference = TransactionResponse.GroupKey;
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG121");
                                                }
                                                else
                                                {
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                                    #endregion
                                                }
                                            }
                                            else
                                            {
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG120", CoreResources.HCG136);
                                            }
                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG114", CoreResources.HCG114);
                                        }
                                    }
                                case HelperStatus.Default.Suspended:
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, "HCG131", CoreResources.HCG131);
                                case HelperStatus.Default.Blocked:
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, "HCG132", CoreResources.HCG132);
                                default:
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, "HCG133", CoreResources.HCG133);
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _UserInfo.ResponseCode, _UserInfo.Message);
                            #endregion
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _OGatewayInfo.ResponseCode, _OGatewayInfo.Message);
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "Redeem", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG500, CoreResources.HCG500M);
                #endregion
            }

            #endregion
        }
        internal async Task<OResponse> ConfirmRedeem(OThankUGateway.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            await Task.Delay(0);
            try
            {
                if ((_Request.UserReference.AccountTypeId == UserAccountType.PosAccount || _Request.UserReference.AccountTypeId == UserAccountType.PgAccount) && string.IsNullOrEmpty(_Request.MerchantId) && string.IsNullOrEmpty(_Request.TerminalId))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100", CoreResources.HCG100);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.TransactionReference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG122", CoreResources.HCG122);
                    #endregion
                }
                else
                {
                    OGatewayInfo _OGatewayInfo = FrameworkOperations.GetGatewayInfo(_Request);
                    #region Perform Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_OGatewayInfo != null)
                        {
                            if (_Request.TransactionReference.StartsWith("MADDEAL"))
                            {
                                string DealKey = _Request.TransactionReference.Replace("MADDEAL", "");
                                var DealCodeDetails = _HCoreContext.MDDealCode.Where(x => x.Guid == DealKey)
                                                              .Select(x => new ODealOperation.DealCode.Response
                                                              {
                                                                  ReferenceId = x.Id,
                                                                  ReferenceKey = x.Guid,

                                                                  DealReferenceId = x.DealId,
                                                                  DealReferenceKey = x.Deal.Guid,
                                                                  ItemCode = x.ItemCode,

                                                                  StartDate = x.StartDate,
                                                                  EndDate = x.EndDate,

                                                                  ActualPrice = x.Deal.ActualPrice,
                                                                  SellingPrice = x.Deal.SellingPrice,
                                                                  CommissionAmount = x.Deal.CommissionAmount,
                                                                  Amount = x.ItemAmount,
                                                                  TotalAmount = x.Deal.TotalAmount,

                                                                  DealStartDate = x.Deal.StartDate,
                                                                  DealEndDate = x.Deal.EndDate,

                                                                  Title = x.Deal.Title,
                                                                  Description = x.Deal.Description,
                                                                  UsageInformation = x.Deal.UsageInformation,
                                                                  Terms = x.Deal.Terms,
                                                                  ImageUrl = x.Deal.PosterStorage.Path,

                                                                  MerchantReferenceId = x.Deal.AccountId,
                                                                  MerchantReferenceKey = x.Deal.Account.Guid,
                                                                  MerchantDisplayName = x.Deal.Account.DisplayName,
                                                                  MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                                                                  UseDate = x.LastUseDate,
                                                                  UseLocationId = x.LastUseLocationId,
                                                                  UseLocationKey = x.LastUseLocation.Guid,
                                                                  UseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                                  UseLocationAddress = x.LastUseLocation.Address,

                                                                  CustomerId = x.Account.Id,
                                                                  CustomerKey = x.Account.Guid,
                                                                  CustomerDisplayName = x.Account.DisplayName,
                                                                  CustomerMobileNumber = x.Account.MobileNumber,
                                                                  CustomerIconUrl = x.Account.IconStorage.Path,

                                                                  CreateDate = x.CreateDate,
                                                                  StatusId = x.StatusId,
                                                                  StatusCode = x.Status.SystemName,
                                                                  StatusName = x.Status.Name,
                                                              })
                                                              .FirstOrDefault();
                                if (DealCodeDetails != null)
                                {
                                    if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Unused)
                                    {
                                        DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                        if (CurrentTime < DealCodeDetails.StartDate)
                                        {
                                            _HCoreContext.SaveChanges();
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0127, TUCDealResource.HCP0127M);
                                        }
                                        else if (CurrentTime > DealCodeDetails.EndDate)
                                        {
                                            _HCoreContext.SaveChanges();
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0128, TUCDealResource.HCP0128M);
                                        }
                                        else if (CurrentTime > DealCodeDetails.StartDate && CurrentTime < DealCodeDetails.EndDate)
                                        {
                                            var DealInfo = _HCoreContext.MDDealCode.Where(x => x.Id == DealCodeDetails.ReferenceId).FirstOrDefault();
                                            DealInfo.UseAttempts = 1;
                                            DealInfo.UseCount = 1;
                                            DealInfo.LastUseSource = Helpers.DealRedeemSource.Pos;
                                            DealInfo.LastUseDate = HCoreHelper.GetGMTDateTime();
                                            if (_OGatewayInfo.StoreId != 0)
                                            {
                                                DealInfo.LastUseLocationId = _OGatewayInfo.StoreId;
                                            }
                                            if (_OGatewayInfo.TerminalId > 0)
                                            {
                                                DealInfo.TerminalId = _OGatewayInfo.TerminalId;
                                            }
                                            if (_OGatewayInfo.CashierId > 0)
                                            {
                                                DealInfo.CashierId = _OGatewayInfo.CashierId;
                                            }
                                            DealInfo.StatusId = HelperStatus.DealCodes.Used;
                                            _HCoreContext.SaveChanges();
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == DealInfo.AccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                                if (!string.IsNullOrEmpty(UserNotificationUrl))
                                                {
                                                    HCoreHelper.SendPushToDevice(UserNotificationUrl, "dealpurchasedetails", "Deal Redeemed", DealCodeDetails.Title + " deal redeemed at " + DealCodeDetails.MerchantDisplayName, "dealpurchasedetails", DealInfo.Id, DealInfo.Guid, "View details", true, null);
                                                }
                                            }
                                            _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
                                            _GatewayResponse.TransactionDate = HCoreHelper.GetGMTDateTime();
                                            _GatewayResponse.TransactionReference = _Request.TransactionReference;
                                            _GatewayResponse.RedeemAmount = (long)((HCoreHelper.RoundNumber((double)DealInfo.ItemAmount, 2)) * 100);
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG125", CoreResources.HCG125);
                                            #endregion
                                        }
                                        else
                                        {
                                            _HCoreContext.SaveChanges();
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0129, TUCDealResource.HCP0129M);
                                        }
                                    }
                                    else if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Used)
                                    {
                                        _HCoreContext.SaveChanges();
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0123, TUCDealResource.HCP0123M);
                                    }
                                    else if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Expired)
                                    {
                                        _HCoreContext.SaveChanges();
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0124, TUCDealResource.HCP0124M);
                                    }
                                    else if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Blocked)
                                    {
                                        _HCoreContext.SaveChanges();
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0125, TUCDealResource.HCP0125M);
                                    }
                                    else
                                    {
                                        _HCoreContext.SaveChanges();
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, TUCDealResource.HCP0126, TUCDealResource.HCP0126M);
                                    }
                                }
                                else
                                {
                                    _HCoreContext.SaveChanges();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealResource.HCP0122, TUCDealResource.HCP0122M);
                                }
                            }
                            if (_Request.TransactionReference.StartsWith("BNPL"))
                            {
                                string LoanKey = _Request.TransactionReference.Replace("BNPL", "");
                                var LoanDetails = _HCoreContext.BNPLAccountLoan.Where(x => x.Guid == LoanKey)
                                                              .Select(x => new ODealOperation.DealCode.Response
                                                              {
                                                                  ReferenceId = x.Id,
                                                                  ReferenceKey = x.Guid,

                                                                  StartDate = x.StartDate,
                                                                  EndDate = x.EndDate,
                                                                  Amount = x.Amount,
                                                                  TotalAmount = x.TotalAmount,
                                                                  CustomerId = x.Account.Id,
                                                                  CustomerKey = x.Account.Guid,
                                                                  CustomerMobileNumber = x.Account.MobileNumber,
                                                                  CreateDate = x.CreateDate,
                                                                  StatusId = x.StatusId,
                                                                  StatusCode = x.Status.SystemName,
                                                                  StatusName = x.Status.Name,
                                                                  IsRedeemed = x.IsRedeemed,
                                                              })
                                                              .FirstOrDefault();
                                if (LoanDetails != null)
                                {
                                    if (LoanDetails.StatusId == 740 && LoanDetails.IsRedeemed == 0)
                                    {

                                        var DealInfo = _HCoreContext.BNPLAccountLoan.Where(x => x.Id == LoanDetails.ReferenceId).FirstOrDefault();
                                        DealInfo.IsRedeemed = 1;
                                        DealInfo.RedeemDate = HCoreHelper.GetGMTDateTime();
                                        if (_OGatewayInfo.StoreId != 0)
                                        {
                                            DealInfo.RedeemLocationId = _OGatewayInfo.StoreId;
                                        }
                                        if (_OGatewayInfo.TerminalId > 0)
                                        {
                                            DealInfo.TerminalId = _OGatewayInfo.TerminalId;
                                        }
                                        if (_OGatewayInfo.CashierId > 0)
                                        {
                                            DealInfo.CashierId = _OGatewayInfo.CashierId;
                                        }
                                        _HCoreContext.SaveChanges();

                                        _CoreTransactionRequest = new OCoreTransaction.Request();
                                        _CoreTransactionRequest.CustomerId = (long)LoanDetails.CustomerId;
                                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                        _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
                                        _CoreTransactionRequest.InvoiceAmount = (double)LoanDetails.TotalAmount;
                                        _CoreTransactionRequest.ReferenceInvoiceAmount = (double)LoanDetails.TotalAmount;
                                        _CoreTransactionRequest.ReferenceAmount = (double)LoanDetails.TotalAmount;
                                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = (long)LoanDetails.CustomerId,
                                            ModeId = TransactionMode.Debit,
                                            TypeId = TransactionType.TUCBnpl.LoanRedeem,
                                            SourceId = TransactionSource.TUCBnpl,
                                            Amount = (double)LoanDetails.TotalAmount,
                                            TotalAmount = (double)LoanDetails.TotalAmount,
                                        });
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = _OGatewayInfo.MerchantId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionType.TUCBnpl.LoanCredit,
                                            SourceId = TransactionSource.TUCBnpl,
                                            Amount = (double)LoanDetails.TotalAmount,
                                            TotalAmount = (double)LoanDetails.TotalAmount,
                                        });

                                        _CoreTransactionRequest.Transactions = _TransactionItems;
                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                        {
                                        }

                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == DealInfo.AccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                            if (!string.IsNullOrEmpty(UserNotificationUrl))
                                            {
                                                HCoreHelper.SendPushToDevice(UserNotificationUrl, "dealpurchasedetails", "Loan Redeemed", "Loan redeemed at " + _OGatewayInfo.MerchantDisplayName, "loandetails", DealInfo.Id, DealInfo.Guid, "View details", true, null);
                                            }
                                        }
                                        _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
                                        _GatewayResponse.TransactionDate = HCoreHelper.GetGMTDateTime();
                                        _GatewayResponse.TransactionReference = _Request.TransactionReference;
                                        _GatewayResponse.RedeemAmount = (long)((HCoreHelper.RoundNumber((double)DealInfo.Amount, 2)) * 100);
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG125", CoreResources.HCG125);
                                        #endregion

                                    }
                                    else
                                    {
                                        _HCoreContext.SaveChanges();
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealResource.HCP0126, TUCDealResource.HCP0126M);
                                    }
                                }
                                else
                                {
                                    _HCoreContext.SaveChanges();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealResource.HCP0122, TUCDealResource.HCP0122M);
                                }
                            }

                            var TransactionDetails = _HCoreContext.HCUAccountTransaction.Where(x => x.Guid == _Request.TransactionReference && x.ParentId == _OGatewayInfo.MerchantId).Select(x => new
                            {
                                Id = x.Id,
                                Guid = x.Guid,
                                TotalAmount = x.TotalAmount,
                                PurchaseAmount = x.PurchaseAmount,
                                TransactionDate = x.TransactionDate,
                                AccountNumber = x.AccountNumber,
                                ReferenceNumber = x.ReferenceNumber,
                                ParentId = x.ParentId,
                                UserAccountId = x.AccountId,
                                Status = x.StatusId,
                                CreatedById = x.CreatedBy,
                                UserMobileNumber = x.Account.MobileNumber,
                                UserDisplayName = x.Account.DisplayName,
                                UserEmailAddress = x.Account.EmailAddress,
                                MerchantDisplayName = x.Parent.DisplayName,
                                //MobileNumber = x.Account.MobileNumber,
                                SubParentId = x.SubParentId,
                                UserAccountStatusId = x.Account.StatusId,
                                ReferenceAmount = x.ReferenceAmount,
                                Comment = x.Comment,
                                SourceId = x.SourceId,
                                CashierId = x.CashierId,
                                BankId = x.Terminal.AcquirerId,
                                ProgramId = x.ProgramId
                            }).FirstOrDefault();
                            if (TransactionDetails != null)
                            {
                                if (TransactionDetails.Status == HelperStatus.Transaction.Initialized)
                                {
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    if (TransactionDetails.UserAccountStatusId == HelperStatus.Default.Active)
                                    {
                                        double InvoiceAmount = HCoreHelper.RoundNumber(((double)TransactionDetails.PurchaseAmount), 2);
                                        double RedeemAmount = HCoreHelper.RoundNumber(((double)TransactionDetails.TotalAmount), 2);

                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var TransactionDetailsU = _HCoreContext.HCUAccountTransaction.Where(x => x.GroupKey == _Request.TransactionReference).ToList();
                                            if (TransactionDetailsU != null)
                                            {
                                                foreach (var TransactionDetail in TransactionDetailsU)
                                                {
                                                    TransactionDetail.StatusId = HelperStatus.Transaction.Success;
                                                    TransactionDetail.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                    TransactionDetail.ModifyById = _Request.UserReference.AccountId;
                                                }
                                                _HCoreContext.SaveChanges();
                                            }
                                        }

                                        ORedeemNotification _ORedeemNotification = new ORedeemNotification();
                                        _ORedeemNotification.TransactionId = TransactionDetails.Id;
                                        _ORedeemNotification.TransactionKey = TransactionDetails.Guid;
                                        _ORedeemNotification.RedeemAmount = TransactionDetails.TotalAmount;
                                        _ORedeemNotification.PurchaseAmount = TransactionDetails.PurchaseAmount;
                                        _ORedeemNotification.CustomerId = TransactionDetails.UserAccountId;
                                        _ORedeemNotification.CustomerMobileNumber = TransactionDetails.UserMobileNumber;
                                        _ORedeemNotification.CustomerEmailAddress = TransactionDetails.UserEmailAddress;
                                        _ORedeemNotification.CustomerName = TransactionDetails.UserDisplayName;
                                        _ORedeemNotification.MerchantId = TransactionDetails.ParentId;
                                        _ORedeemNotification.StoreId = TransactionDetails.SubParentId;
                                        _ORedeemNotification.CreatedById = _Request.UserReference.AccountId;
                                        _ORedeemNotification.TransactionSourceId = TransactionDetails.SourceId;
                                        _ORedeemNotification.ProgramId = TransactionDetails.ProgramId;
                                        _ORedeemNotification.AcquirerId = TransactionDetails.BankId;
                                        _ORedeemNotification.MerchantDisplayName = TransactionDetails.MerchantDisplayName;
                                        var _TransactionPostProcessActor = ActorSystem.Create("RedeemNotificationActor");
                                        var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<RedeemNotificationActor>("RedeemNotificationActor");
                                        _TransactionPostProcessActorNotify.Tell(_ORedeemNotification);

                                        _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
                                        _GatewayResponse.TransactionDate = TransactionDetails.TransactionDate;
                                        _GatewayResponse.TransactionReference = _Request.TransactionReference;
                                        _GatewayResponse.RedeemAmount = (long)((HCoreHelper.RoundNumber(RedeemAmount, 2)) * 100);
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG125", CoreResources.HCG125);
                                        #endregion

                                    }
                                    if (TransactionDetails.UserAccountStatusId == HelperStatus.Default.Suspended)
                                    {

                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var TrInfo = _HCoreContext.HCUAccountTransaction.Where(x => x.Id == TransactionDetails.Id).FirstOrDefault();
                                            if (TrInfo != null)
                                            {
                                                TrInfo.ModifyById = SystemAccounts.ThankUCashSystemId;
                                                TrInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                TrInfo.Comment = "Transaction failed as user account was suspended before completing transaction ";
                                                TrInfo.StatusId = HelperStatus.Transaction.Failed;
                                                _HCoreContext.SaveChanges();
                                            }
                                            else
                                            {
                                                _HCoreContext.Dispose();
                                            }
                                        }
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUC205", CoreResources.TUC205);
                                        #endregion
                                    }
                                    else
                                    {
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var TrInfo = _HCoreContext.HCUAccountTransaction.Where(x => x.Id == TransactionDetails.Id).FirstOrDefault();
                                            if (TrInfo != null)
                                            {
                                                TrInfo.ModifyById = SystemAccounts.ThankUCashSystemId;
                                                TrInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                TrInfo.Comment = "Transaction failed as user account was blocked before completing transaction ";
                                                TrInfo.StatusId = HelperStatus.Transaction.Failed;
                                                _HCoreContext.SaveChanges();
                                            }
                                            else
                                            {
                                                _HCoreContext.Dispose();
                                            }
                                        }
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUC204", CoreResources.TUC204);
                                        #endregion
                                    }
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG126");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUC206", CoreResources.TUC206);
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG103");
                            #endregion
                        }
                    }
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "ConfirmRedeem", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
    }
}

