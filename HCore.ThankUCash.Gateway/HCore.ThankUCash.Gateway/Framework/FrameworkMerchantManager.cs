//==================================================================================
// FileName: FrameworkMerchantManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Integration.Mailerlite;
using HCore.Integration.Mailerlite.Requests;
using HCore.ThankUCash.Gateway.Object;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.ThankUCash.Gateway.Framework
{
    internal class FrameworkMerchantManager
    {
        HCoreContext _HCoreContext;
        Random _Random;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        List<HCUAccountParameter> _HCUAccountParameters;
        internal OResponse WebPay_Confirm(OThankUGateway.MerchantRegistration.Request _Request)
        {
            #region Manage Exception
            try
            {
                //if (string.IsNullOrEmpty(_Request.PSSPMerchantId))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1064, TUCCoreResource.CA1064M);
                //}
                //if (string.IsNullOrEmpty(_Request.Name))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1065, TUCCoreResource.CA1065M);
                //}
                //if (string.IsNullOrEmpty(_Request.MobileNumber))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1066, TUCCoreResource.CA1066M);
                //}
                //if (string.IsNullOrEmpty(_Request.EmailAddress))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1067, TUCCoreResource.CA1067M);
                //}
                //if (_Request.Latitude == 0 && _Request.Longitude == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1059, TUCCoreResource.CA1059M);
                //}
                //if (_Request.BranchId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1060, TUCCoreResource.CA1060M);
                //}
                //if (_Request.RmId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1061, TUCCoreResource.CA1061M);
                //}
                //if (string.IsNullOrEmpty(_Request.StatusCode))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CASTATUS, TUCCoreResource.CASTATUSM);
                //}
                //long StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                //if (StatusId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CAINSTATUS, TUCCoreResource.CAINSTATUSM);
                //}
                //OAddressResponse _AddressResponse = HCoreHelper.GetAddressIds(_Request.Business.Address, _Request.UserReference);
                using (_HCoreContext = new HCoreContext())
                {
                    var CheckReference = _HCoreContext.HCUAccount.Where(x => x.ReferenceNumber == _Request.PSSPMerchantId && x.AccountTypeId == UserAccountType.Merchant)
                        .Select(x => new
                        {
                            EmailAddress = x.EmailAddress,
                            AccountCode = x.AccountCode,
                            RewardPercentage = x.AccountPercentage,
                            StatusId = x.StatusId,
                        }).FirstOrDefault();
                    if (CheckReference != null)
                    {
                        _HCoreContext.Dispose();
                        if (_Request.Business.EmailAddress != CheckReference.EmailAddress)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA1037", "Invalid PSSPMerchantId. Account binding failed");
                        }
                        if (CheckReference.StatusId == HelperStatus.Default.Active)
                        {
                            var _AResponse = new
                            {
                                PSSPMerchantId = _Request.PSSPMerchantId,
                                MerchantId = CheckReference.AccountCode,
                                RewardPercentage = CheckReference.RewardPercentage,
                                Status = "Active"
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AResponse, "CA1063", "Account already connected");
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA1037", "Merchant account not active. Contact support to activate account");
                        }
                    }

                    var UserNameCheck = _HCoreContext.HCUAccount.Where(x => x.User.Username == _Request.Business.EmailAddress && x.AccountTypeId == UserAccountType.Merchant).FirstOrDefault();
                    if (UserNameCheck != null)
                    {
                        UserNameCheck.ReferenceNumber = _Request.PSSPMerchantId;
                        _HCoreContext.SaveChanges();
                        var _BResponse = new
                        {
                            PSSPMerchantId = _Request.PSSPMerchantId,
                            MerchantId = UserNameCheck.AccountCode,
                            RewardPercentage = UserNameCheck.AccountPercentage,
                            Status = "Active"
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BResponse, "CA1063", "Account connected successfully");
                        //_HCoreContext.Dispose();
                        //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA1097", "Merchant already exists");
                    }


                    if (!string.IsNullOrEmpty(_Request.Business.MobileNumber))
                    {
                        _Request.Business.MobileNumber = HCoreHelper.FormatMobileNumber("234", _Request.Business.MobileNumber);
                        var UserMobileNumberCheck = _HCoreContext.HCUAccount.Any(x => x.MobileNumber == _Request.Business.MobileNumber && x.AccountTypeId == UserAccountType.Merchant);
                        if (UserMobileNumberCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA1097", "Merchant mobile number already registered");
                        }
                    }
                    if (!string.IsNullOrEmpty(_Request.Business.EmailAddress))
                    {
                        var UserEmailAddressCheck = _HCoreContext.HCUAccount.Any(x => x.EmailAddress == _Request.Business.EmailAddress && x.AccountTypeId == UserAccountType.Merchant);
                        if (UserEmailAddressCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA1315", "Email address already registerd");
                        }
                    }

                    _Random = new Random();
                    //string AccountCode = _Random.Next(100000, 999999).ToString() + _Random.Next(000000000, 999999999).ToString();
                    string ReferenceKey = HCoreHelper.GenerateGuid();
                    string MerchantCode = HCoreHelper.GenerateRandomNumber(15);
                    HCUAccountParameter _HCUAccountParameter;
                    _HCUAccountParameters = new List<HCUAccountParameter>();
                    _HCUAccountParameter = new HCUAccountParameter();
                    //_HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    //_HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                    //_HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage").Select(x => x.Id).FirstOrDefault();
                    //_HCUAccountParameter.Value = _Request.RewardPercentage.ToString();
                    //_HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                    //_HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    //if (_Request.UserReference.AccountId != 0)
                    //{
                    //    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    //}
                    //_HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                    //_HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                    //_HCUAccountParameters.Add(_HCUAccountParameter);
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                    _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewarddeductiontype").Select(x => x.Id).FirstOrDefault();
                    _HCUAccountParameter.Value = "Prepay";
                    _HCUAccountParameter.HelperId = 253;
                    _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                    _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccountParameters.Add(_HCUAccountParameter);

                    string UserName = _Request.Business.EmailAddress;
                    string Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    string UserPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(8));
                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = UserName;
                    _HCUAccountAuth.Password = UserPassword;
                    _HCUAccountAuth.SecondaryPassword = Password;
                    _HCUAccountAuth.SystemPassword = Password;
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    #region Save UserAccount
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                    //if (_Request.Business.Categories != null && _Request.Business.Categories.Count > 0)
                    //{
                    //    _HCUAccountParameters = new List<HCUAccountParameter>();
                    //    foreach (var Category in _Request.Business.Categories)
                    //    {
                    //        HCUAccountParameter _HCUAccountParameterItem = new HCUAccountParameter
                    //        {
                    //            Guid = HCoreHelper.GenerateGuid(),
                    //            TypeId = HelperType.MerchantCategory,
                    //            CommonId = Category.ReferenceId,
                    //            CreateDate = HCoreHelper.GetGMTDateTime(),
                    //            StatusId = HelperStatus.Default.Active,
                    //        };
                    //        _HCUAccount.HCUAccountParameterAccount.Add(_HCUAccountParameterItem);
                    //    }
                    //}
                    _HCUAccount.Guid = ReferenceKey;
                    _HCUAccount.AccountTypeId = UserAccountType.Merchant;
                    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                    _HCUAccount.OwnerId = _Request.UserReference.AccountId;
                    _HCUAccount.DisplayName = _Request.Business.Name;
                    _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                    _HCUAccount.ReferenceNumber = _Request.PSSPMerchantId;
                    _HCUAccount.Name = _Request.Business.Name;
                    _HCUAccount.EmailAddress = _Request.Business.EmailAddress;
                    _HCUAccount.ContactNumber = _Request.Business.MobileNumber;
                    _HCUAccount.AccountPercentage = 3;
                    //_HCUAccount.WebsiteUrl = _Request.WebsiteUrl;
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = MerchantCode;
                    _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.Business.MobileNumber);
                    _HCUAccount.Address = _Request.Business.Address;
                    //_HCUAccount.Latitude = _AddressResponse.Latitude;
                    //_HCUAccount.Longitude = _AddressResponse.Longitude;
                    //_HCUAccount.CountryId = _AddressResponse.CountryId;
                    //if (_AddressResponse.StateId != 0)
                    //{
                    //    _HCUAccount.RegionId = _AddressResponse.StateId;
                    //}
                    //if (_AddressResponse.CityId != 0)
                    //{
                    //    _HCUAccount.CityId = _AddressResponse.CityId;
                    //}
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.StatusId = HelperStatus.Default.Active;
                    _HCUAccount.CountryId = (int?)_Request.UserReference.CountryId;
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.AccountPercentage = 3;
                    _HCUAccount.User = _HCUAccountAuth;
                    #endregion
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    _HCoreContext.SaveChanges();
                    long MerchantId = _HCUAccount.Id;
                    var EmailObject = new
                    {
                        UserName = _HCUAccountAuth.Username,
                        Password = UserPassword,
                        DisplayName = _HCUAccount.DisplayName,
                    };
                    using (_HCoreContext = new HCoreContext())
                    {
                        _HCUAccountAuth = new HCUAccountAuth();
                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(10);
                        _HCUAccountAuth.Password = HCoreHelper.GenerateRandomNumber(6);
                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountAuth.CreatedById = MerchantId;
                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                        #region Save UserAccount
                        _HCUAccount = new HCUAccount();
                        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                        _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                        _HCUAccount.OwnerId = MerchantId;
                        _HCUAccount.DisplayName = _Request.Business.Name;
                        _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                        _HCUAccount.Name = _Request.Business.Name;
                        _HCUAccount.EmailAddress = _Request.Business.EmailAddress;
                        _HCUAccount.ContactNumber = _Request.Business.MobileNumber;
                        _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.Business.MobileNumber);
                        _HCUAccount.Address = _Request.Business.Address;
                        //_HCUAccount.Latitude = _AddressResponse.Latitude;
                        //_HCUAccount.Longitude = _AddressResponse.Longitude;
                        //_HCUAccount.CountryId = _AddressResponse.CountryId;
                        //if (_AddressResponse.StateId != 0)
                        //{
                        //    _HCUAccount.RegionId = _AddressResponse.StateId;
                        //}
                        //if (_AddressResponse.CityId != 0)
                        //{
                        //    _HCUAccount.CityId = _AddressResponse.CityId;
                        //}
                        //_HCUAccount.ReferenceNumber = _Request.PSSPMerchantId;
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                        _HCUAccount.AccountCode = _Random.Next(100000000, 999999999).ToString();
                        if (_Request.UserReference.AppVersionId != 0)
                        {
                            _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                        }
                        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                        _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                        }
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                        _HCUAccount.EmailVerificationStatus = 0;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.NumberVerificationStatus = 0;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.User = _HCUAccountAuth;
                        #endregion
                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                        _HCoreContext.SaveChanges();

                    }
                    HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantWelcomeEmail, _Request.Business.Name, _Request.Business.EmailAddress, EmailObject, _Request.UserReference);
                    //#region Request Verification
                    //_VerificationRequest = new OCoreVerificationManager.Request();
                    //_VerificationRequest.CountryIsd = _Request.UserReference.CountryIsd;
                    //_VerificationRequest.Type = 1;
                    //_VerificationRequest.MobileNumber = _Request.MobileNumber;
                    //_VerificationRequest.UserReference = _Request.UserReference;
                    //_ManageCoreVerification = new ManageCoreVerification();
                    //var VerificationResponse = _ManageCoreVerification.RequestOtp(_VerificationRequest);
                    //#endregion
                    //string Token = "";
                    //if (VerificationResponse.Status == StatusSuccess)
                    //{
                    //    OCoreVerificationManager.Response VerificationResponseItem = (OCoreVerificationManager.Response)VerificationResponse.Result;
                    //    using (_HCoreContext = new HCoreContext())
                    //    {
                    //        var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Id == MerchantId).FirstOrDefault();
                    //        MerchantDetails.ReferralUrl = VerificationResponseItem.RequestToken;
                    //        Token = VerificationResponseItem.RequestToken;
                    //        _HCoreContext.SaveChanges();
                    //    }
                    //}

                    //Operations.FrameworkSubscription _FrameworkSubscription = new Operations.FrameworkSubscription();
                    //_FrameworkSubscription.AddAccountFreeSubscription(MerchantId, UserAccountType.Merchant);

                    //var _Response = new
                    //{
                    //    ReferenceId = MerchantId,
                    //    ReferenceKey = ReferenceKey,
                    //    //Token = Token
                    //};

                    //try
                    //{
                    //    string data = "scope=profile&grant_type=client_credentials";
                    //    using (WebClient _WebClient = new WebClient())
                    //    {
                    //        string TokenUrl = "https://api.interswitchng.com/passport/oauth/token";
                    //        if (HostEnvironment == HostEnvironmentType.Test)
                    //        {
                    //            TokenUrl = "https://apps.qa.interswitchng.com/passport/oauth/token";
                    //        }
                    //        _WebClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    //        _WebClient.Headers.Add("Authorization", "Basic SUtJQTBBQjIxRTZGRjhGMDM2NTBGQjkzQTMxNDQ2NEZGOTMwOTAxM0UzMEI6dGhhbmstdS1jYXNo");
                    //        string result = _WebClient.UploadString(TokenUrl, data);
                    //        if (!string.IsNullOrEmpty(result))
                    //        {
                    //            tokenResponse _TokenResponse = JsonConvert.DeserializeObject<tokenResponse>(result);
                    //            if (_TokenResponse != null)
                    //            {
                    //                string Url = "https://webpay.interswitchng.com/collections/api/v1/tuc/configure";
                    //                if (HostEnvironment == HostEnvironmentType.Test)
                    //                {
                    //                    Url = "https://testwebpay.interswitchng.com/collections/api/v1/tuc/configure";
                    //                }
                    //                var client = new RestSharp.RestClient(Url);
                    //                var request = new RestSharp.RestRequest(RestSharp.Method.POST);
                    //                request.AddHeader("accept", "application/json");
                    //                request.AddHeader("content-type", "application/json");
                    //                string Part = "{\"merchantCode\":\"" + _Request.Reference + "\",\"tucMerchantId\":\"" + MerchantCode + "\"}";
                    //                //request.AddHeader("authorization", "Bearer eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiY2Flc2FyIiwiaXN3LWNvbGxlY3Rpb25zIiwiaXN3LWNvcmUiLCJpc3ctcGF5bWVudGdhdGV3YXkiLCJwYXNzcG9ydCIsInByb2plY3QteC1tZXJjaGFudCJdLCJzY29wZSI6WyJwcm9maWxlIl0sImV4cCI6MTYxODQxNTY2NSwiY2xpZW50X25hbWUiOiJUaGFuayBZb3UgY2FzaCIsImp0aSI6IjIzZGZhY2U1LTc5ZjEtNDkwNC05YTgzLTg2Y2FmNjcwODk5ZSIsImNsaWVudF9pZCI6IklLSUEwQUIyMUU2RkY4RjAzNjUwRkI5M0EzMTQ0NjRGRjkzMDkwMTNFMzBCIn0.SJ3jq6dk_GV7YgSPLaOECf5e2shldOB7DyV-4UgyUrcU7FGSuUpUMPjAb-VanqP14NgQl7aDiQBFA6QGWPxvkoWIfbenftPtpWvt6MBv60k1t6zgVkAkGaj_kphJX7DRteRia1pFJBnkeaxl61hs4vZRJuB1pTc9AJA_hxXCq1YzdAEpTXrF93JHJ0DC6LFZqtb75TFfiFPq_T3CCrX6gyecc-c1yW-0DMxEqhZa_uVOeKTUV-7oZC0EfEnJtfbDQBnyf9Bquzzg4PDX-S9M2qO8V268t4T7jfXzvSwpeiavnGU-d6WM5y1Dd6lmaF3133wif9koFIWxFYQohrxz3w");
                    //                request.AddHeader("authorization", "Bearer " + _TokenResponse.access_token);
                    //                request.AddParameter("application/json", Part, RestSharp.ParameterType.RequestBody);
                    //                RestSharp.IRestResponse response = client.Execute(request);
                    //                HCoreHelper.LogData(HCoreConstant.LogType.Log, "DDSD", Part, JsonConvert.SerializeObject(_Request), response.Content);
                    //            }
                    //        }
                    //    }
                    //}
                    //catch (Exception _Exception)
                    //{
                    //    HCoreHelper.LogException("WEBPAYCON_REQ", _Exception);
                    //}

                    //ManageSubscription _ManageSubscription = new ManageSubscription();
                    //_ManageSubscription.AddAccountFreeSubscription(MerchantId, UserAccountType.Merchant);


                    var _Response = new
                    {
                        PSSPMerchantId = _Request.PSSPMerchantId,
                        MerchantId = MerchantCode,
                        RewardPercentage = 3,
                        Status = "Active"
                    };

                    #region
                    //Add the new Merchant Email to Mailerlite
                    var mailerlite = new MailerliteImple();
                    var emailObj = new CreateListRequest
                    {
                        name="New Merchant Email",
                        emails=new string[] {_Request.Business.EmailAddress}
                    };
                    var jsonResponse = mailerlite.AddEmailToMailerList(emailObj);
                    if(jsonResponse !=null && jsonResponse.IsSuccessful)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "CA1063", "Account created successfully");

                    }
                    #endregion

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "CA1063", "Account created successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("WebPay_Confirm", _Exception, _Request.UserReference, "CA0500", "Unable to process your request. Please try after some time");
            }
            #endregion
        }
    }
}
