//==================================================================================
// FileName: FrameworkCampaignProcessor.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.ThankUCash.Gateway.Core;
using HCore.ThankUCash.Gateway.Object;
using Newtonsoft.Json;
using Z.EntityFramework.Plus;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
namespace HCore.ThankUCash.Gateway.Framework
{
    public class FrameworkCampaignProcessor
    {
        public class BinStatus
        {
            public bool status { get; set; }
            public string? message { get; set; }
            public BinInfo data { get; set; }
        }
        public class BinInfo
        {
            public string? bin { get; set; }
            public string? bin_systemname { get; set; }
            public string? brand { get; set; }
            public string? brand_systemname { get; set; }
            public string? sub_brand { get; set; }
            public string? sub_brand_systemname { get; set; }
            public string? country_code { get; set; }
            public string? country_code_systemname { get; set; }
            public string? country_name { get; set; }
            public string? country_name_systemname { get; set; }
            public string? card_type { get; set; }
            public string? card_type_systemname { get; set; }
            public string? bank { get; set; }
            public string? bank_systemname { get; set; }
        }
        HCoreContext _HCoreContext;
        OCoreTransaction.Request _CoreTransactionRequest;
        ManageCoreTransaction _ManageCoreTransaction;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        HCCoreCommon _HCCoreCommon;
        internal void ProcessOtherBankCardCustomerCampaignReward(OCampaignProcessor _Request) //  OThankUGateway.Request _Request, OGatewayInfo _OGatewayInfo, OUserInfo _UserInfo)
        {
            try
            {
                if (_Request.CardBankId != 0)
                {

                    if (_Request._GatewayInfo.AcquirerId != null)
                    {
                        if (_Request.CardBankId != _Request._GatewayInfo.AcquirerId)
                        {
                            if (_Request._UserRequest.InvoiceAmount > 0)
                            {
                                double InvoiceAmount = (double)_Request._UserRequest.InvoiceAmount / 100;
                                DateTime TodaysDate = HCoreHelper.GetGMTDate();
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var BankCampaign = _HCoreContext.TUCampaign
                                        .Where(x => x.AccountId == _Request._GatewayInfo.AcquirerId
                                        && x.StatusId == HelperStatus.Campaign.Published
                                        && x.Type.SystemName == "othercardcustomers"
                                        && (x.EndDate.Date > TodaysDate || x.EndDate.Date == TodaysDate)
                                        && (x.StartDate < TodaysDate || x.StartDate.Date == TodaysDate))
                                        .Select(x => new
                                        {
                                            UserAccountId = x.AccountId,
                                            ReferenceId = x.Id,
                                            SubTypeCode = x.SubType.SystemName,
                                            SubTypeValue = x.SubTypeValue,

                                            MinimumInvoiceAmount = x.MinimumInvoiceAmount,
                                            MaximumInvoiceAmount = x.MaximumInvoiceAmount,

                                            MinimumRewardAmount = x.MinimumRewardAmount,
                                            MaximumRewardAmount = x.MaximumRewardAmount,

                                            CustomAudience = x.CustomAudience,
                                            Credit = x.Credit,
                                            Debit = x.Debit,
                                            Balance = x.Balance,
                                            SmsText = x.SmsText,
                                        }).FirstOrDefault();

                                    if (BankCampaign != null)
                                    {
                                        double RewardAmount = 0;
                                        if (BankCampaign.SubTypeCode == "fixedamount")
                                        {
                                            // Consider Minimum Invoice Amount
                                            if (BankCampaign.SubTypeValue != null)
                                            {
                                                if (BankCampaign.MinimumInvoiceAmount == 0)
                                                {
                                                    RewardAmount = (double)BankCampaign.SubTypeValue;
                                                }
                                                else if (InvoiceAmount > (BankCampaign.MinimumInvoiceAmount - 1))
                                                {
                                                    RewardAmount = (double)BankCampaign.SubTypeValue;
                                                }
                                                else
                                                {
                                                    RewardAmount = (double)BankCampaign.SubTypeValue;
                                                }
                                            }

                                        }
                                        else if (BankCampaign.SubTypeCode == "amountpercentage")
                                        {
                                            // Consider Minimum Invoice Amount 
                                            // Consider Maximum Reward Amount
                                            double TRewardAmount = HCoreHelper.GetPercentage(InvoiceAmount, (double)BankCampaign.SubTypeValue, _AppConfig.SystemEntryRoundDouble);
                                            if (BankCampaign.MaximumRewardAmount == null)
                                            {
                                                RewardAmount = TRewardAmount;
                                            }
                                            else if (BankCampaign.MaximumRewardAmount == 0)
                                            {
                                                RewardAmount = TRewardAmount;
                                            }
                                            else if (TRewardAmount > (BankCampaign.MaximumRewardAmount + 1))
                                            {
                                                RewardAmount = (double)BankCampaign.MaximumRewardAmount;
                                            }
                                            else
                                            {
                                                RewardAmount = (double)TRewardAmount;
                                            }
                                        }
                                        if (RewardAmount > 0)
                                        {
                                            int TransactionTypeId = TransactionType.BonusReward;
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.CustomerId = _Request._UserInfo.UserAccountId;
                                            _CoreTransactionRequest.UserReference = _Request._UserRequest.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.ParentId = _Request._GatewayInfo.MerchantId;
                                            _CoreTransactionRequest.InvoiceAmount = InvoiceAmount;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = InvoiceAmount;
                                            if (!string.IsNullOrEmpty(_Request._UserRequest.bin))
                                            {
                                                _CoreTransactionRequest.AccountNumber = _Request._UserRequest.bin;
                                            }
                                            else
                                            {
                                                _CoreTransactionRequest.AccountNumber = _Request._UserRequest.SixDigitPan;
                                            }
                                            _CoreTransactionRequest.ReferenceNumber = _Request._UserRequest.ReferenceNumber;
                                            _CoreTransactionRequest.SubParentId = _Request._GatewayInfo.StoreId;
                                            //_CoreTransactionRequest.CardId = _Request._UserInfo.CardId;

                                            if (_Request._GatewayInfo.AcquirerId != null)
                                            {
                                                _CoreTransactionRequest.BankId = (long)_Request._GatewayInfo.AcquirerId;
                                            }
                                            _CoreTransactionRequest.CustomerId = _Request._UserInfo.UserAccountId;
                                            _CoreTransactionRequest.ReferenceAmount = RewardAmount;
                                            _CoreTransactionRequest.CreatedById = _Request._GatewayInfo.TerminalId;
                                            _CoreTransactionRequest.CampaignId = BankCampaign.ReferenceId;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = BankCampaign.UserAccountId,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionTypeId,
                                                SourceId = TransactionSource.Campaign,

                                                Amount = RewardAmount,
                                                Comission = 0,
                                                TotalAmount = RewardAmount,
                                                TransactionDate = _Request._UserRequest.TransactionDate,
                                            });
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _Request._UserInfo.UserAccountId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionTypeId,
                                                SourceId = TransactionSource.TUC,

                                                Amount = RewardAmount,
                                                Comission = 0,
                                                TotalAmount = RewardAmount,
                                                TransactionDate = _Request._UserRequest.TransactionDate,
                                            });
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                            {
                                                //ProcessTransactionResponse(_Request, _OGatewayInfo, _UserInfo, _AmountDistrubutionRequest, TransactionResponse);
                                            }
                                            else
                                            {
                                                #region Send Response
                                                HCoreHelper.SendResponse(_Request._UserRequest.UserReference, ResponseStatus.Error, null, "HCG549");
                                                #endregion
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ProcessCampaign", _Exception);
            }
        }
        HCCoreCountry _HCCoreCountry;
        internal void ProcessBinNumber(OCampaignProcessor _Request)
        {
            try
            {
                if (!string.IsNullOrEmpty(_Request.AccountNumber))
                {
                    if (_Request.AccountNumber.Length >= 6)
                    {
                        long BrandId = 0;
                        long CountryId = 0;
                        long CardTypeId = 0;
                        long BankId = 0;
                        string TAccNumber = _Request.AccountNumber.Substring(0, 6);
                        using (_HCoreContext = new HCoreContext())
                        {
                            #region Send SMS Messagae
                            var smsapiurl = "https://api.paystack.co/decision/bin/" + TAccNumber;
                            HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(smsapiurl);
                            HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                            System.IO.StreamReader _StreamReader = new System.IO.StreamReader(_HttpWebResponse.GetResponseStream());
                            string responseString = _StreamReader.ReadToEnd();
                            _StreamReader.Close();
                            _HttpWebResponse.Close();
                            BinStatus _BinInfo = JsonConvert.DeserializeObject<BinStatus>(responseString);
                            if (_BinInfo.status == true)
                            {
                                if (_BinInfo.data.brand != "Unknown")
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        _BinInfo.data.brand_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.brand);
                                        _BinInfo.data.sub_brand_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.sub_brand);
                                        _BinInfo.data.country_code_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.country_code);
                                        _BinInfo.data.country_name_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.country_name);
                                        _BinInfo.data.card_type_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.card_type);
                                        _BinInfo.data.bank_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.bank);

                                        BrandId = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand && x.SystemName == _BinInfo.data.brand_systemname).Select(x => x.Id).FirstOrDefault();
                                        CountryId = _HCoreContext.HCCoreCountry.Where(x => x.SystemName == _BinInfo.data.country_name_systemname).Select(x => x.Id).FirstOrDefault();
                                        CardTypeId = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardType && x.SystemName == _BinInfo.data.card_type_systemname).Select(x => x.Id).FirstOrDefault();
                                        BankId = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBank && x.SystemName == _BinInfo.data.bank_systemname).Select(x => x.Id).FirstOrDefault();

                                        #region Save Bin Country
                                        if (BrandId == 0)
                                        {
                                            if (!string.IsNullOrEmpty(_BinInfo.data.brand))
                                            {
                                                _HCCoreCommon = new HCCoreCommon();
                                                _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                                                _HCCoreCommon.TypeId = HelperType.BinCardBrand;
                                                _HCCoreCommon.Name = _BinInfo.data.brand;
                                                _HCCoreCommon.SystemName = _BinInfo.data.brand_systemname;
                                                _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                                                _HCCoreCommon.StatusId = HelperStatus.Default.Active;
                                                _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                                            }
                                        }
                                        #endregion
                                        #region Save Bin Country
                                        if (CountryId == 0)
                                        {
                                            if (!string.IsNullOrEmpty(_BinInfo.data.country_name))
                                            {
                                                _HCCoreCountry = new HCCoreCountry();
                                                _HCCoreCountry.Guid = HCoreHelper.GenerateGuid();
                                                _HCCoreCountry.Name = _BinInfo.data.country_name;
                                                _HCCoreCountry.SystemName = _BinInfo.data.country_name_systemname;
                                                _HCCoreCountry.CreateDate = HCoreHelper.GetGMTDateTime();
                                                _HCCoreCountry.CreatedById = 1;
                                                _HCCoreCountry.StatusId = HelperStatus.Default.Active;
                                                _HCoreContext.HCCoreCountry.Add(_HCCoreCountry);
                                                _HCoreContext.SaveChanges();
                                            }
                                        }
                                        #endregion
                                        #region Save Bin Card Type
                                        if (CardTypeId == 0)
                                        {
                                            if (!string.IsNullOrEmpty(_BinInfo.data.card_type))
                                            {
                                                _HCCoreCommon = new HCCoreCommon();
                                                _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                                                _HCCoreCommon.TypeId = HelperType.BinCardType;
                                                _HCCoreCommon.Name = _BinInfo.data.card_type;
                                                _HCCoreCommon.SystemName = _BinInfo.data.card_type_systemname;
                                                _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                                                _HCCoreCommon.StatusId = HelperStatus.Default.Active;
                                                _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                                            }
                                        }
                                        #endregion
                                        #region Save Bin Bank
                                        if (BankId == 0)
                                        {
                                            if (!string.IsNullOrEmpty(_BinInfo.data.bank))
                                            {
                                                _HCCoreCommon = new HCCoreCommon();
                                                _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                                                _HCCoreCommon.TypeId = HelperType.BinCardBank;
                                                _HCCoreCommon.Name = _BinInfo.data.bank;
                                                _HCCoreCommon.SystemName = _BinInfo.data.bank_systemname;
                                                _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                                                _HCCoreCommon.StatusId = HelperStatus.Default.Active;
                                                _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                                            }
                                        }
                                        #endregion
                                        _HCoreContext.SaveChanges();
                                    }
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        long? BankUserAccountId = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBank && x.Id == BankId).Select(x => x.AccountId).FirstOrDefault();
                                        if (BankUserAccountId != null)
                                        {
                                            _Request.CardBankId = (long)BankUserAccountId;
                                            ProcessOtherBankCardCustomerCampaignReward(_Request);
                                            ProcessOwnBankCardCustomerCampaignReward(_Request);
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("PROCESS TRANSACTION - ADD BIN", _Exception);
            }

        }
        private void ProcessOwnBankCardCustomerCampaignReward(OCampaignProcessor _Request) //  OThankUGateway.Request _Request, OGatewayInfo _OGatewayInfo, OUserInfo _UserInfo)
        {
            try
            {
                if (_Request._GatewayInfo.AcquirerId != null)
                {

                    if (_Request._UserRequest.InvoiceAmount > 0)
                    {
                        if (_Request.CardBankId != _Request._GatewayInfo.AcquirerId)
                        {
                            double InvoiceAmount = (double)_Request._UserRequest.InvoiceAmount / 100;
                            DateTime TodaysDate = HCoreHelper.GetGMTDate();
                            using (_HCoreContext = new HCoreContext())
                            {
                                var BankCampaign = _HCoreContext.TUCampaign
                                    .Where(x => x.AccountId == _Request.CardBankId
                                    && x.StatusId == HelperStatus.Campaign.Published
                                    && x.Type.SystemName == "owncardcustomers"
                                    && (x.EndDate.Date > TodaysDate || x.EndDate.Date == TodaysDate)
                                    && (x.StartDate < TodaysDate || x.StartDate.Date == TodaysDate))
                                    .Select(x => new
                                    {
                                        UserAccountId = x.AccountId,
                                        ReferenceId = x.Id,
                                        SubTypeCode = x.SubType.SystemName,
                                        SubTypeValue = x.SubTypeValue,

                                        MinimumInvoiceAmount = x.MinimumInvoiceAmount,
                                        MaximumInvoiceAmount = x.MaximumInvoiceAmount,

                                        MinimumRewardAmount = x.MinimumRewardAmount,
                                        MaximumRewardAmount = x.MaximumRewardAmount,

                                        CustomAudience = x.CustomAudience,
                                        Credit = x.Credit,
                                        Debit = x.Debit,
                                        Balance = x.Balance,
                                        SmsText = x.SmsText,
                                    }).FirstOrDefault();

                                if (BankCampaign != null)
                                {
                                    double RewardAmount = 0;
                                    if (BankCampaign.SubTypeCode == "fixedamount")
                                    {
                                        // Consider Minimum Invoice Amount
                                        if (BankCampaign.SubTypeValue != null)
                                        {


                                            if (BankCampaign.MinimumInvoiceAmount == 0)
                                            {
                                                RewardAmount = (double)BankCampaign.SubTypeValue;
                                            }
                                            else if (InvoiceAmount > (BankCampaign.MinimumInvoiceAmount - 1))
                                            {
                                                RewardAmount = (double)BankCampaign.SubTypeValue;
                                            }
                                            else
                                            {
                                                RewardAmount = (double)BankCampaign.SubTypeValue;
                                            }
                                        }

                                    }
                                    else if (BankCampaign.SubTypeCode == "amountpercentage")
                                    {
                                        // Consider Minimum Invoice Amount 
                                        // Consider Maximum Reward Amount
                                        double TRewardAmount = HCoreHelper.GetPercentage(InvoiceAmount, (double)BankCampaign.SubTypeValue, _AppConfig.SystemEntryRoundDouble);
                                        if (BankCampaign.MaximumRewardAmount == null)
                                        {
                                            RewardAmount = TRewardAmount;
                                        }
                                        else if (BankCampaign.MaximumRewardAmount == 0)
                                        {
                                            RewardAmount = TRewardAmount;
                                        }
                                        else if (TRewardAmount > (BankCampaign.MaximumRewardAmount + 1))
                                        {
                                            RewardAmount = (double)BankCampaign.MaximumRewardAmount;
                                        }
                                        else
                                        {
                                            RewardAmount = TRewardAmount;
                                        }
                                    }
                                    if (RewardAmount > 0)
                                    {
                                        int TransactionTypeId = TransactionType.BonusReward;
                                        _CoreTransactionRequest = new OCoreTransaction.Request();
                                        _CoreTransactionRequest.CustomerId = _Request._UserInfo.UserAccountId;
                                        _CoreTransactionRequest.UserReference = _Request._UserRequest.UserReference;
                                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                        _CoreTransactionRequest.ParentId = _Request._GatewayInfo.MerchantId;
                                        _CoreTransactionRequest.InvoiceAmount = InvoiceAmount;
                                        _CoreTransactionRequest.ReferenceInvoiceAmount = InvoiceAmount;
                                        if (!string.IsNullOrEmpty(_Request._UserRequest.bin))
                                        {
                                            _CoreTransactionRequest.AccountNumber = _Request._UserRequest.bin;
                                        }
                                        else
                                        {
                                            _CoreTransactionRequest.AccountNumber = _Request._UserRequest.SixDigitPan;
                                        }
                                        _CoreTransactionRequest.ReferenceNumber = _Request._UserRequest.ReferenceNumber;
                                        _CoreTransactionRequest.SubParentId = _Request._GatewayInfo.StoreId;
                                        //_CoreTransactionRequest.CardId = _Request._UserInfo.CardId;
                                        if (_Request._GatewayInfo.AcquirerId != null)
                                        {
                                            _CoreTransactionRequest.BankId = (long)_Request._GatewayInfo.AcquirerId;
                                        }

                                        _CoreTransactionRequest.CustomerId = _Request._UserInfo.UserAccountId;
                                        _CoreTransactionRequest.ReferenceAmount = RewardAmount;
                                        _CoreTransactionRequest.CreatedById = _Request._GatewayInfo.TerminalId;
                                        _CoreTransactionRequest.CampaignId = BankCampaign.ReferenceId;
                                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = BankCampaign.UserAccountId,
                                            ModeId = TransactionMode.Debit,
                                            TypeId = TransactionTypeId,
                                            SourceId = TransactionSource.Campaign,

                                            Amount = RewardAmount,
                                            Comission = 0,
                                            TotalAmount = RewardAmount,
                                            TransactionDate = _Request._UserRequest.TransactionDate,
                                        });
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = _Request._UserInfo.UserAccountId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionTypeId,
                                            SourceId = TransactionSource.TUC,

                                            Amount = RewardAmount,
                                            Comission = 0,
                                            TotalAmount = RewardAmount,
                                            TransactionDate = _Request._UserRequest.TransactionDate,
                                        });
                                        _CoreTransactionRequest.Transactions = _TransactionItems;
                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                        {
                                            //ProcessTransactionResponse(_Request, _OGatewayInfo, _UserInfo, _AmountDistrubutionRequest, TransactionResponse);
                                        }
                                        else
                                        {
                                            #region Send Response
                                            HCoreHelper.SendResponse(_Request._UserRequest.UserReference, ResponseStatus.Error, null, "HCG549");
                                            #endregion
                                        }
                                    }
                                }
                            }

                        }


                    }
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ProcessCampaign", _Exception);
            }
        }



    }


}

