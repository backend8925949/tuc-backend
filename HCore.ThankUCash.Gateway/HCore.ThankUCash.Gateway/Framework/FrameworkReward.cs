//==================================================================================
// FileName: FrameworkReward.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using Amazon.Runtime.Internal;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.ThankUCash.Gateway.Actor;
using HCore.ThankUCash.Gateway.Core;
//using HCore.ThankUCash.Gateway.Integration;
using HCore.ThankUCash.Gateway.Object;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.ThankUCash.Gateway.Framework
{
    public class FrameworkReward
    {
        HCoreContext _HCoreContext;
        CoreOperations _CoreOperations;
        OThankUGateway.Response _GatewayResponse;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        ManageCoreTransaction _ManageCoreTransaction;
        //FrameworkSoftcom _FrameworkSoftcom;
        OThankUGateway.Request _GatewayRequest;
        CoreGatewayTransaction _CoreGatewayTransaction;
        cmt_sale_failed _cmt_sale_failed;
        internal OResponse InitializeTransaction(OThankUGateway.Request _Request)
        {
            try
            {
                if (!string.IsNullOrEmpty(_Request.TransactionMode))
                {
                    _Request.TransactionMode = _Request.TransactionMode.Trim().ToLower();
                }
                if (_Request.TransactionMode == PaymentMethod.CashS || _Request.TransactionMode == PaymentMethod.BankS || _Request.TransactionMode == PaymentMethod.UssdS)
                {
                    return SettleTransaction(_Request);
                }
                else if (_Request.TransactionMode == PaymentMethod.CardS)
                {
                    if (string.IsNullOrEmpty(_Request.TerminalId))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG183, CoreResources.HCG183M);
                    }
                    else if (string.IsNullOrEmpty(_Request.MobileNumber))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG109, CoreResources.HCG109M);
                    }
                    else if (_Request.InvoiceAmount < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG116, CoreResources.HCG116M);
                    }
                    else
                    {
                        _CoreOperations = new CoreOperations();
                        OGatewayInfo _OGatewayInfo = _CoreOperations.GetGatewayInfo(_Request);
                        if (_OGatewayInfo.Status == ResponseStatus.Success)
                        {
                            OUserInfo _UserInfo = _CoreOperations.GetUserInfo(_Request, _OGatewayInfo, true);
                            if (_UserInfo.Status == ResponseStatus.Success)
                            {
                                if (_UserInfo.AccountStatusId == HelperStatus.Default.Active)
                                {
                                    OAmountDistribution _AmountDistributionResponse = _CoreOperations.GetAmountDistribution(_Request, _OGatewayInfo, _UserInfo);
                                    _GatewayResponse = new OThankUGateway.Response();
                                    _GatewayResponse.TransactionReference = HCoreHelper.GenerateGuid();
                                    _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
                                    _GatewayResponse.RewardPercentage = _AmountDistributionResponse.RewardPercentage;
                                    _GatewayResponse.RewardAmount = (long)(((double)HCoreHelper.RoundNumber(_AmountDistributionResponse.TUCRewardAmount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                    _GatewayResponse.UserRewardAmount = (long)(((double)HCoreHelper.RoundNumber(_AmountDistributionResponse.TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                    _GatewayResponse.AcquirerAmount = (long)(((double)HCoreHelper.RoundNumber(_AmountDistributionResponse.Acquirer, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                    _GatewayResponse.MerchantAmount = (long)(((double)HCoreHelper.RoundNumber(_AmountDistributionResponse.MerchantAmount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                    _GatewayResponse.CommissionAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionResponse.TUCRewardCommissionAmount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                    if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount)
                                    {
                                        _GatewayResponse.Commission = (long)((HCoreHelper.RoundNumber(_AmountDistributionResponse.Pssp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                        _GatewayResponse.ProviderAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionResponse.Pssp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                    }
                                    else
                                    {
                                        _GatewayResponse.Commission = (long)((HCoreHelper.RoundNumber(_AmountDistributionResponse.Ptsp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                        _GatewayResponse.ProviderAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionResponse.Ptsp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                    }
                                    if (_AmountDistributionResponse.ThankUCashPercentageFromRewardAmount == false)
                                    {
                                        _GatewayResponse.TransactionReference = HCoreHelper.GenerateGuid();
                                        _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
                                        _GatewayResponse.RewardPercentage = _AmountDistributionResponse.RewardPercentage;
                                        _GatewayResponse.RewardAmount = (long)(((double)HCoreHelper.RoundNumber(_AmountDistributionResponse.TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                        _GatewayResponse.UserRewardAmount = (long)(((double)HCoreHelper.RoundNumber(_AmountDistributionResponse.TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                        _GatewayResponse.AcquirerAmount = (long)(((double)HCoreHelper.RoundNumber(_AmountDistributionResponse.Acquirer, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                        _GatewayResponse.MerchantAmount = (long)(((double)HCoreHelper.RoundNumber(_AmountDistributionResponse.InvoiceAmount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                        _GatewayResponse.CommissionAmount = 0;
                                        if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount)
                                        {
                                            _GatewayResponse.Commission = (long)((HCoreHelper.RoundNumber(_AmountDistributionResponse.Pssp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                            _GatewayResponse.ProviderAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionResponse.Pssp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                        }
                                        else
                                        {
                                            _GatewayResponse.Commission = (long)((HCoreHelper.RoundNumber(_AmountDistributionResponse.Ptsp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                            _GatewayResponse.ProviderAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionResponse.Ptsp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                        }
                                    }

                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, CoreResources.HCG502, CoreResources.HCG502M);
                                }
                                else
                                {
                                    _GatewayResponse = new OThankUGateway.Response();
                                    _GatewayResponse.TransactionReference = HCoreHelper.GenerateGuid();
                                    _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
                                    _GatewayResponse.RewardPercentage = 0;
                                    _GatewayResponse.RewardAmount = 0;
                                    _GatewayResponse.UserRewardAmount = 0;
                                    _GatewayResponse.AcquirerAmount = 0;
                                    _GatewayResponse.MerchantAmount = 0;
                                    _GatewayResponse.CommissionAmount = 0;
                                    _GatewayResponse.Commission = 0;
                                    _GatewayResponse.ProviderAmount = 0;
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, CoreResources.HCG502, CoreResources.HCG502M);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _UserInfo.ResponseCode, _UserInfo.Message);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _OGatewayInfo.ResponseCode, _OGatewayInfo.Message);
                        }
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG173, CoreResources.HCG173M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException(LogLevel.High, "InitializeTransaction", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG500, CoreResources.HCG500M);
            }
        }
        internal OResponse SettleTransaction(OThankUGateway.Request _Request)
        {

            _GatewayResponse = new OThankUGateway.Response();
            if (string.IsNullOrEmpty(_Request.TransactionMode))
            {
                if (!string.IsNullOrEmpty(_Request.SixDigitPan))
                {
                    _Request.TransactionMode = PaymentMethod.CardS;
                }
                else if (!string.IsNullOrEmpty(_Request.bin))
                {
                    _Request.TransactionMode = PaymentMethod.CardS;
                }
                else
                {
                    _Request.TransactionMode = PaymentMethod.CashS;
                }
            }
            if (!string.IsNullOrEmpty(_Request.TransactionMode))
            {
                _Request.TransactionMode = _Request.TransactionMode.Trim().ToLower();
            }
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceNumber))
                {
                    _Request.ReferenceNumber = "tuc_" + HCoreHelper.GenerateGuid();
                }
                if (string.IsNullOrEmpty(_Request.TerminalId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG183, CoreResources.HCG183M);
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG109, CoreResources.HCG109M);
                }
                else if (_Request.InvoiceAmount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG116, CoreResources.HCG116M);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG115, CoreResources.HCG115M);
                }
                else if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length < 9)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG145, CoreResources.HCG145M);
                }
                else if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length > 13)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG146, CoreResources.HCG146M);
                }
                else if (!string.IsNullOrEmpty(_Request.MobileNumber) && !System.Text.RegularExpressions.Regex.IsMatch(_Request.MobileNumber, "^[0-9]*$"))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG147, CoreResources.HCG147M);
                }
                else if (_Request.TransactionMode != PaymentMethod.UssdS && _Request.TransactionMode != PaymentMethod.TucWalletS && _Request.TransactionMode != PaymentMethod.CardS && _Request.TransactionMode != PaymentMethod.CashS)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG148, CoreResources.HCG148M);
                }
                else
                {
                    _CoreOperations = new CoreOperations();
                    OGatewayInfo _OGatewayInfo = _CoreOperations.GetGatewayInfo(_Request);
                    if (_OGatewayInfo.Status == ResponseStatus.Success)
                    {
                        //using (_HCoreContext = new HCoreContext())
                        //{
                        //    bool ValidateTransactionReference = _HCoreContext.HCUAccountTransaction.Any(x => x.ReferenceNumber == _Request.ReferenceNumber && x.ProviderId == _Request.UserReference.AccountId);
                        //    _HCoreContext.Dispose();
                        //    if (ValidateTransactionReference)
                        //    {
                        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG194, CoreResources.HCG194M);
                        //    }
                        //}

                        OUserInfo _UserInfo = _CoreOperations.GetUserInfo(_Request, _OGatewayInfo, true);
                        if (_UserInfo.Status == ResponseStatus.Success)
                        {
                            //string GroupKey = HCoreHelper.GenerateGuid();
                            string GroupKey = HCoreHelper.GenerateTransactionReference(_OGatewayInfo.MerchantId, _UserInfo.UserAccountId, "TRG");

                            OAmountDistribution _AmountDistrubutionRequest = _CoreOperations.GetAmountDistribution(_Request, _OGatewayInfo, _UserInfo);
                            _CoreGatewayTransaction = new CoreGatewayTransaction();
                            _CoreGatewayTransaction.ProcessRewardTransaction(GroupKey, _Request, _OGatewayInfo, _UserInfo, _AmountDistrubutionRequest);
                            _GatewayResponse = new OThankUGateway.Response
                            {
                                InvoiceAmount = _Request.InvoiceAmount,
                                ReferenceNumber = _Request.ReferenceNumber,
                                TransactionReference = GroupKey,
                                TransactionDate = HCoreHelper.GetGMTDateTime(),
                                RewardAmount = (long)(HCoreHelper.RoundNumber(_AmountDistrubutionRequest.TUCRewardAmount, _AppConfig.SystemExitRoundDouble) * _AppConfig.SystemMultiplyByDigit),
                                CommissionAmount = (long)(HCoreHelper.RoundNumber(_AmountDistrubutionRequest.TUCRewardCommissionAmount, _AppConfig.SystemExitRoundDouble) * _AppConfig.SystemMultiplyByDigit),
                                UserRewardAmount = (long)(HCoreHelper.RoundNumber(_AmountDistrubutionRequest.TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble) * _AppConfig.SystemMultiplyByDigit),
                                AcquirerAmount = (long)(HCoreHelper.RoundNumber(_AmountDistrubutionRequest.Acquirer, _AppConfig.SystemExitRoundDouble) * _AppConfig.SystemMultiplyByDigit),
                                MerchantAmount = (long)(HCoreHelper.RoundNumber(_AmountDistrubutionRequest.MerchantAmount, _AppConfig.SystemExitRoundDouble) * _AppConfig.SystemMultiplyByDigit)
                            };

                            if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount)
                            {
                                _GatewayResponse.Commission = (long)(HCoreHelper.RoundNumber(_AmountDistrubutionRequest.Pssp, _AppConfig.SystemExitRoundDouble) * _AppConfig.SystemMultiplyByDigit);
                                _GatewayResponse.ProviderAmount = (long)(HCoreHelper.RoundNumber(_AmountDistrubutionRequest.Pssp, _AppConfig.SystemExitRoundDouble) * _AppConfig.SystemMultiplyByDigit);
                            }
                            if (_Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                            {
                                _GatewayResponse.Commission = (long)(HCoreHelper.RoundNumber(_AmountDistrubutionRequest.Ptsp, _AppConfig.SystemExitRoundDouble) * _AppConfig.SystemMultiplyByDigit);
                                _GatewayResponse.ProviderAmount = (long)(HCoreHelper.RoundNumber(_AmountDistrubutionRequest.Ptsp, _AppConfig.SystemExitRoundDouble) * _AppConfig.SystemMultiplyByDigit);
                            }
                            if (_AmountDistrubutionRequest.ThankUCashPercentageFromRewardAmount == false)
                            {
                                _GatewayResponse.RewardAmount = (long)(HCoreHelper.RoundNumber(_AmountDistrubutionRequest.TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble) * _AppConfig.SystemMultiplyByDigit);
                                _GatewayResponse.CommissionAmount = 0;
                                _GatewayResponse.UserRewardAmount = (long)(HCoreHelper.RoundNumber(_AmountDistrubutionRequest.TUCRewardUserAmount, _AppConfig.SystemExitRoundDouble) * _AppConfig.SystemMultiplyByDigit);
                                _GatewayResponse.AcquirerAmount = (long)(HCoreHelper.RoundNumber(_AmountDistrubutionRequest.Acquirer, _AppConfig.SystemExitRoundDouble) * _AppConfig.SystemMultiplyByDigit);
                                _GatewayResponse.MerchantAmount = (long)(HCoreHelper.RoundNumber(_AmountDistrubutionRequest.MerchantAmount, _AppConfig.SystemExitRoundDouble) * _AppConfig.SystemMultiplyByDigit);
                            }
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, CoreResources.HCG125, CoreResources.HCG125M);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _UserInfo.ResponseCode, _UserInfo.Message);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _OGatewayInfo.ResponseCode, _OGatewayInfo.Message);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException(LogLevel.High, "SettleTransaction", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG500, CoreResources.HCG500M);
            }
        }

        //internal OResponse NotifySoftComTransaction(OThankUGateway.SCNotifyProductPaymentRequest _Request)
        //{
        //    try
        //    {
        //        switch (_Request.eventType)
        //        {
        //            case "CREATE_PRODUCT":
        //                _FrameworkSoftcom = new FrameworkSoftcom();
        //                return _FrameworkSoftcom.SoftCon_SaveProduct(_Request);
        //            case "NEW_PAYMENT":
        //                {
        //                    _GatewayResponse = new OThankUGateway.Response();
        //                    #region Manage Exception
        //                    try
        //                    {
        //                        if (_Request.till.terminalID != null && string.IsNullOrEmpty(_Request.till.terminalID))
        //                        {
        //                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100");
        //                        }
        //                        else if (string.IsNullOrEmpty(_Request.payment.billID))
        //                        {
        //                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115");
        //                        }
        //                        else if (_Request.transaction.amount < 1)
        //                        {
        //                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG116");
        //                        }
        //                        else
        //                        {
        //                            string TerminalId = _Request.till.terminalID;
        //                            _GatewayRequest = new OThankUGateway.Request();
        //                            _GatewayRequest.TerminalId = TerminalId;
        //                            _GatewayRequest.MobileNumber = _Request.customer.phone;
        //                            _GatewayRequest.EmailAddress = _Request.customer.email;
        //                            _GatewayRequest.Name = _Request.customer.name;
        //                            _GatewayRequest.ReferenceNumber = _Request.payment.billID;
        //                            _GatewayRequest.TransactionDate = Convert.ToDateTime(_Request.transaction.createdAt);
        //                            _GatewayRequest.InvoiceAmount = (long)_Request.transaction.amount * 100;
        //                            if (_Request.payment != null && _Request.payment.channels != null && _Request.payment.channels.Count() > 0)
        //                            {
        //                                string PaymentMode = _Request.payment.channels[0].ToLower();
        //                                if (PaymentMode == "eyowo_pos")
        //                                {
        //                                    _GatewayRequest.TransactionMode = "card";
        //                                }
        //                                else
        //                                {
        //                                    _GatewayRequest.TransactionMode = "cash";
        //                                }
        //                            }
        //                            _GatewayRequest.UserReference = _Request.UserReference;
        //                            if (_Request.payment.status.ToLower() == "approved")
        //                            {
        //                                return SettleTransaction(_GatewayRequest);
        //                            }
        //                            else
        //                            {
        //                                return NotifyFailedTransaction(_GatewayRequest);

        //                            }
        //                        }
        //                    }
        //                    catch (Exception _Exception)
        //                    {
        //                        #region  Log Exception
        //                        HCoreHelper.LogException(LogLevel.High, "NotifySoftComTransaction", _Exception, _Request.UserReference);
        //                        #endregion
        //                        #region Send Response
        //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
        //                        #endregion
        //                    }
        //                    #endregion
        //                }
        //            default:
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100");
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        HCoreHelper.LogException(LogLevel.High, "NotifySoftComTransaction", _Exception, _Request.UserReference);
        //        #endregion
        //        #region Send Response
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
        //        #endregion
        //    }
        //}

        internal OResponse NotifyTransaction(OThankUGateway.NotifyPayment.Request _Request)
        {
            _GatewayResponse = new OThankUGateway.Response();
            try
            {
                if (_Request.paymentInstrument != null && string.IsNullOrEmpty(_Request.paymentInstrument.userId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG100, CoreResources.HCG100M);
                }
                else if (string.IsNullOrEmpty(_Request.userId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG109, CoreResources.HCG109M);
                }
                else if (string.IsNullOrEmpty(_Request.referenceId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG115, CoreResources.HCG115M);
                }
                else if (_Request.amount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG116, CoreResources.HCG116M);
                }
                else
                {
                    string TerminalId = _Request.paymentInstrument.userId;
                    _GatewayRequest = new OThankUGateway.Request();
                    _GatewayRequest.TerminalId = _Request.paymentInstrument.userId;
                    if (_Request.metadata != null)
                    {
                        _GatewayRequest.CashierId = _Request.metadata.cashier_id;
                    }
                    _GatewayRequest.MobileNumber = _Request.userId;
                    _GatewayRequest.SixDigitPan = _Request.paymentInstrument.masked;
                    _GatewayRequest.ReferenceNumber = _Request.referenceId;
                    _GatewayRequest.TransactionDate = Convert.ToDateTime(_Request.completedAt);
                    _GatewayRequest.InvoiceAmount = (long)_Request.amount * 100;
                    if (!string.IsNullOrEmpty(_Request.paymentInstrument.instrumentType))
                    {
                        _GatewayRequest.TransactionMode = _Request.paymentInstrument.instrumentType;
                    }
                    if (!string.IsNullOrEmpty(_GatewayRequest.TransactionMode))
                    {
                        _GatewayRequest.TransactionMode = _GatewayRequest.TransactionMode.ToLower();
                    }
                    if (_Request.paymentInstrument != null && !string.IsNullOrEmpty(_Request.paymentInstrument.masked))
                    {
                        _GatewayRequest.TransactionMode = PaymentMethod.CardS;
                    }
                    else
                    {
                        _GatewayRequest.TransactionMode = PaymentMethod.CashS;
                    }

                    _GatewayRequest.UserReference = _Request.UserReference;
                    if (_Request.status.ToLower() == "success")
                    {
                        return SettleTransaction(_GatewayRequest);
                    }
                    else
                    {
                        return NotifyFailedTransaction(_GatewayRequest);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException(LogLevel.High, "NotifySoftComTransaction", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG500, CoreResources.HCG500M);
            }
        }
        internal OResponse CancelTransaction(OThankUGateway.Request _Request)
        {
            _GatewayResponse = new OThankUGateway.Response();
            try
            {
                if (string.IsNullOrEmpty(_Request.MerchantId) && string.IsNullOrEmpty(_Request.TerminalId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG100, CoreResources.HCG100M);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG115, CoreResources.HCG115M);
                }
                else if (string.IsNullOrEmpty(_Request.TransactionReference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG122, CoreResources.HCG122M);
                }
                else
                {
                    _CoreOperations = new CoreOperations();
                    OGatewayInfo _OGatewayInfo = _CoreOperations.GetGatewayInfo(_Request);
                    if (_OGatewayInfo != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            bool IsAlreadyCancelled = false;
                            bool IsNotFound = false;
                            var TransactionDetails = _HCoreContext.HCUAccountTransaction.Where(x => x.GroupKey == _Request.TransactionReference && x.ReferenceNumber == _Request.ReferenceNumber).ToList();
                            if (TransactionDetails.Count > 0)
                            {
                                foreach (var Transaction in TransactionDetails)
                                {
                                    if (Transaction.StatusId == HelperStatus.Transaction.Cancelled)
                                    {
                                        IsAlreadyCancelled = true;
                                    }
                                    Transaction.StatusId = HelperStatus.Transaction.Cancelled;
                                    Transaction.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    Transaction.Comment = _Request.Comment;
                                    if (_OGatewayInfo.TerminalId != 0)
                                    {
                                        Transaction.ModifyById = _OGatewayInfo.TerminalId;
                                    }
                                    else
                                    {
                                        Transaction.ModifyById = _Request.UserReference.AccountId;
                                    }
                                }
                            }

                            var _TucSales = _HCoreContext.cmt_sale.Where(x => x.guid == _Request.TransactionReference && x.reference_number == _Request.ReferenceNumber).FirstOrDefault();
                            if (_TucSales != null)
                            {
                                if (_TucSales.status_id == HelperStatus.Transaction.Cancelled)
                                {
                                    IsAlreadyCancelled = true;
                                }
                                _TucSales.status_id = HelperStatus.Transaction.Cancelled;
                                _TucSales.modify_date = HCoreHelper.GetGMTDateTime();
                                _TucSales.status_message = _Request.Comment;
                                _TucSales.modify_by_id = _Request.UserReference.AccountId;
                            }
                            var _TucLoyalty = _HCoreContext.cmt_loyalty.Where(x => x.guid == _Request.TransactionReference && x.reference_number == _Request.ReferenceNumber).FirstOrDefault();
                            if (_TucLoyalty != null)
                            {
                                if (_TucLoyalty.status_id == HelperStatus.Transaction.Cancelled)
                                {
                                    IsAlreadyCancelled = true;
                                }
                                _TucLoyalty.status_id = HelperStatus.Transaction.Cancelled;
                                _TucLoyalty.modify_date = HCoreHelper.GetGMTDateTime();
                                _TucLoyalty.modify_by_id = _Request.UserReference.AccountId;
                            }
                            if (TransactionDetails.Count == 0 && _TucSales == null && _TucLoyalty == null)
                            {
                                IsNotFound = true;
                            }
                            else
                            {
                                IsNotFound = false;
                            }
                            _HCoreContext.SaveChanges();
                            if (IsNotFound)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG193, CoreResources.HCG193M);
                            }
                            else
                            {
                                if (IsAlreadyCancelled)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG192, CoreResources.HCG192M);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, CoreResources.HCG182, CoreResources.HCG182M);
                                }
                            }

                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _OGatewayInfo.ResponseCode, _OGatewayInfo.Message);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "CancelTransaction", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
        }
        internal OResponse NotifyFailedTransaction(OThankUGateway.Request _Request)
        {
            _GatewayResponse = new OThankUGateway.Response();
            try
            {
                if (string.IsNullOrEmpty(_Request.MerchantId) && string.IsNullOrEmpty(_Request.TerminalId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG100, CoreResources.HCG100M);
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG109, CoreResources.HCG109M);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG115, CoreResources.HCG115M);
                }
                else if (_Request.InvoiceAmount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG116, CoreResources.HCG116M);
                }
                else if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length < 10)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG145, CoreResources.HCG145M);
                }
                else if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length > 13)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG146, CoreResources.HCG146M);
                }
                else if (!string.IsNullOrEmpty(_Request.MobileNumber) && !System.Text.RegularExpressions.Regex.IsMatch(_Request.MobileNumber, "^[0-9]*$"))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG147, CoreResources.HCG147M);
                }
                else
                {
                    _CoreOperations = new CoreOperations();
                    OGatewayInfo _OGatewayInfo = _CoreOperations.GetGatewayInfo(_Request);
                    if (_OGatewayInfo.Status == ResponseStatus.Success)
                    {
                        OUserInfo _UserInfo = _CoreOperations.GetUserInfo(_Request, _OGatewayInfo, true);
                        if (_UserInfo.Status == ResponseStatus.Success)
                        {
                            int PaymentModeId = PaymentMethod.Cash;
                            int TransactionTypeId = TransactionType.CardReward;
                            switch (_Request.TransactionMode)
                            {
                                case "cash":
                                    TransactionTypeId = TransactionType.CashReward;
                                    PaymentModeId = PaymentMethod.Cash;
                                    break;
                                case "card":
                                    TransactionTypeId = TransactionType.CardReward;
                                    PaymentModeId = PaymentMethod.Card;
                                    break;
                                default:
                                    TransactionTypeId = TransactionType.CashReward;
                                    PaymentModeId = PaymentMethod.Cash;
                                    if (!string.IsNullOrEmpty(_Request.bin) || !string.IsNullOrEmpty(_Request.SixDigitPan))
                                    {
                                        TransactionTypeId = TransactionType.CardReward;
                                        PaymentModeId = PaymentMethod.Card;
                                    }
                                    break;
                            }
                            using (_HCoreContext = new HCoreContext())
                            {
                                _cmt_sale_failed = new cmt_sale_failed();
                                _cmt_sale_failed.guid = HCoreHelper.GenerateTransactionReference(_OGatewayInfo.MerchantId, _UserInfo.UserAccountId, "TUC");
                                if (_Request.TransactionDate != null)
                                {
                                    _cmt_sale_failed.transaction_date = (DateTime)_Request.TransactionDate;
                                }
                                else
                                {
                                    _cmt_sale_failed.transaction_date = HCoreHelper.GetGMTDateTime();
                                }
                                _cmt_sale_failed.payment_mode_id = PaymentModeId;
                                _cmt_sale_failed.source_id = TransactionSource.TUC;
                                _cmt_sale_failed.type_id = TransactionTypeId;
                                _cmt_sale_failed.invoice_amount = HCoreHelper.RoundNumber(_Request.InvoiceAmount / 100, _AppConfig.SystemExitRoundDouble);
                                _cmt_sale_failed.customer_id = _UserInfo.UserAccountId;
                                _cmt_sale_failed.merchant_id = _OGatewayInfo.MerchantId;
                                if (_OGatewayInfo.StoreId > 0)
                                {
                                    _cmt_sale_failed.store_id = _OGatewayInfo.StoreId;
                                }
                                if (_OGatewayInfo.CashierId > 0)
                                {
                                    _cmt_sale_failed.cashier_id = _OGatewayInfo.CashierId;
                                }
                                if (_OGatewayInfo.TerminalId > 0)
                                {
                                    _cmt_sale_failed.terminal_id = _OGatewayInfo.TerminalId;
                                }
                                _cmt_sale_failed.bin_number = _Request.SixDigitPan;
                                _cmt_sale_failed.reference_number = _Request.ReferenceNumber;
                                _cmt_sale_failed.invoice_number = _Request.InvoiceNumber;
                                _cmt_sale_failed.created_by_id = _Request.UserReference.AccountId;
                                _cmt_sale_failed.status_id = HelperStatus.Transaction.Failed;
                                _cmt_sale_failed.status_message = _Request.ErrorMessage;
                                _HCoreContext.cmt_sale_failed.Add(_cmt_sale_failed);
                                _HCoreContext.SaveChanges();
                                var _Response = new
                                {
                                    TransactionReference = _cmt_sale_failed.guid
                                };
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, CoreResources.HCG181, CoreResources.HCG181M);
                                #endregion
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _UserInfo.ResponseCode, _UserInfo.Message);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _OGatewayInfo.ResponseCode, _OGatewayInfo.Message);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException(LogLevel.High, "NotifyFailedTransaction", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG500, CoreResources.HCG500M);
            }
        }
        internal OResponse NotifySettlements(OThankUGateway.NotifySettlement _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCG11689");
                #endregion

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "InitializeTransaction", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        //private OResponse RewardRedeemTransaction(OThankUGateway.Request _Request, OGatewayInfo _OGatewayInfo, string? GroupKey, double RedeemAmount, double InvoiceAmount, bool AllowCash)
        //{
        //    if (!string.IsNullOrEmpty(_Request.TransactionMode))
        //    {
        //        _Request.TransactionMode = _Request.TransactionMode.ToLower();
        //    }
        //    #region Declare
        //    _GatewayResponse = new OThankUGateway.Response();
        //    #endregion
        //    #region Manage Exception
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_Request.TerminalId))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100");
        //            #endregion
        //        }
        //        else if (string.IsNullOrEmpty(_Request.MobileNumber) && string.IsNullOrEmpty(_Request.CardNumber) && string.IsNullOrEmpty(_Request.TagNumber))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109");
        //            #endregion
        //        }
        //        else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115");
        //            #endregion
        //        }
        //        else if (AllowCash == false && _Request.TransactionMode == "cash")
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115");
        //            #endregion
        //        }
        //        else if (_Request.InvoiceAmount < 1)
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG116");
        //            #endregion
        //        }
        //        else
        //        {
        //            _CoreOperations = new CoreOperations();
        //            OUserInfo _UserInfo = _CoreOperations.GetUserInfo(_Request, _OGatewayInfo, false);
        //            if (_UserInfo.Status == ResponseStatus.Success)
        //            {
        //                using (_HCoreContext = new HCoreContext())
        //                {
        //                    var ValidateTransaction = _HCoreContext.HCUAccountTransaction.Where(x => x.ReferenceNumber == _Request.ReferenceNumber && x.CreatedBy.OwnerId == _Request.UserReference.AccountId).Select(x => x.Id).FirstOrDefault();
        //                    if (ValidateTransaction == 0)
        //                    {
        //                        _HCoreContext.Dispose();
        //                        OAmountDistribution _AmountDistrubutionRequest = _CoreOperations.GetAmountDistribution(_Request, _OGatewayInfo, _UserInfo);
        //                        if (_AmountDistrubutionRequest.RewardAmount > 0)
        //                        {
        //                            long TransactionTypeId = TransactionType.CardReward;
        //                            switch (_Request.TransactionMode)
        //                            {
        //                                case "cash":
        //                                    TransactionTypeId = TransactionType.CashReward;
        //                                    break;
        //                                case "card":
        //                                    TransactionTypeId = TransactionType.CardReward;
        //                                    break;
        //                                case "redeemreward":
        //                                    TransactionTypeId = TransactionType.RedeemReward;
        //                                    break;
        //                                default:
        //                                    TransactionTypeId = TransactionType.CashReward;
        //                                    if (!string.IsNullOrEmpty(_Request.bin) || !string.IsNullOrEmpty(_Request.SixDigitPan))
        //                                    {
        //                                        TransactionTypeId = TransactionType.CardReward;
        //                                    }
        //                                    break;
        //                            }
        //                            _CoreTransactionRequest = new OCoreTransaction.Request();
        //                            _CoreTransactionRequest.CashierId = _OGatewayInfo.CashierId;
        //                            _CoreTransactionRequest.CustomerId = _UserInfo.UserAccountId;
        //                            _CoreTransactionRequest.UserReference = _Request.UserReference;
        //                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
        //                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
        //                            _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
        //                            _CoreTransactionRequest.InvoiceAmount = (InvoiceAmount - RedeemAmount);
        //                            _CoreTransactionRequest.ReferenceInvoiceAmount = InvoiceAmount;
        //                            if (!string.IsNullOrEmpty(_Request.bin))
        //                            {
        //                                _CoreTransactionRequest.AccountNumber = _Request.bin;
        //                            }
        //                            else
        //                            {
        //                                _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
        //                            }
        //                            _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
        //                            _CoreTransactionRequest.SubParentId = _OGatewayInfo.StoreId;
        //                            _CoreTransactionRequest.CardId = _UserInfo.CardId;
        //                            if (_OGatewayInfo.AcquirerId != null)
        //                            {
        //                                _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
        //                            }
        //                            _CoreTransactionRequest.ReferenceAmount = _AmountDistrubutionRequest.RewardAmount;
        //                            _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
        //                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
        //                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                            {
        //                                UserAccountId = _OGatewayInfo.MerchantId,
        //                                ModeId = TransactionMode.Debit,
        //                                TypeId = TransactionTypeId,
        //                                SourceId = TransactionSource.Merchant,

        //                                Amount = _AmountDistrubutionRequest.User,
        //                                Comission = _AmountDistrubutionRequest.RewardCommission,
        //                                TotalAmount = _AmountDistrubutionRequest.RewardAmount,
        //                                TransactionDate = _Request.TransactionDate,
        //                            });
        //                            if (_AmountDistrubutionRequest.IsThankUCashEnabled == true)
        //                            {
        //                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                {
        //                                    UserAccountId = _UserInfo.UserAccountId,
        //                                    ModeId = TransactionMode.Credit,
        //                                    TypeId = TransactionTypeId,
        //                                    SourceId = TransactionSource.ThankUCashPlus,
        //                                    Amount = _AmountDistrubutionRequest.User,
        //                                    TotalAmount = _AmountDistrubutionRequest.User,
        //                                    TransactionDate = _Request.TransactionDate,
        //                                });
        //                            }
        //                            else
        //                            {
        //                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                {
        //                                    UserAccountId = _UserInfo.UserAccountId,
        //                                    ModeId = TransactionMode.Credit,
        //                                    TypeId = TransactionTypeId,
        //                                    SourceId = TransactionSource.TUC,

        //                                    Amount = _AmountDistrubutionRequest.User,
        //                                    TotalAmount = _AmountDistrubutionRequest.User,
        //                                    TransactionDate = _Request.TransactionDate,
        //                                });
        //                            }

        //                            if (_AmountDistrubutionRequest.Ptsp > 0)
        //                            {
        //                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                {
        //                                    UserAccountId = _Request.UserReference.AccountId,
        //                                    ModeId = TransactionMode.Credit,
        //                                    TypeId = TransactionTypeId,
        //                                    SourceId = TransactionSource.Settlement,

        //                                    Amount = _AmountDistrubutionRequest.Ptsp,
        //                                    TotalAmount = _AmountDistrubutionRequest.Ptsp,
        //                                    TransactionDate = _Request.TransactionDate,
        //                                });
        //                            }
        //                            if (_AmountDistrubutionRequest.Acquirer > 0)
        //                            {
        //                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                {
        //                                    UserAccountId = (long)_OGatewayInfo.AcquirerId,
        //                                    ModeId = TransactionMode.Credit,
        //                                    TypeId = TransactionTypeId,
        //                                    SourceId = TransactionSource.Settlement,

        //                                    Amount = _AmountDistrubutionRequest.Acquirer,
        //                                    TotalAmount = _AmountDistrubutionRequest.Acquirer,
        //                                    TransactionDate = _Request.TransactionDate,
        //                                });
        //                                if (_AmountDistrubutionRequest.AllowAcquirerSettlement == "1")
        //                                {
        //                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                    {
        //                                        UserAccountId = (long)_OGatewayInfo.AcquirerId,
        //                                        ModeId = TransactionMode.Debit,
        //                                        TypeId = TransactionTypeId,
        //                                        SourceId = TransactionSource.Settlement,

        //                                        Amount = _AmountDistrubutionRequest.Acquirer,
        //                                        TotalAmount = _AmountDistrubutionRequest.Acquirer,
        //                                        TransactionDate = _Request.TransactionDate,
        //                                    });
        //                                }
        //                            }
        //                            if (_AmountDistrubutionRequest.Issuer > 0)
        //                            {
        //                                if (_UserInfo.IssuerAccountTypeId == UserAccountType.Appuser)
        //                                {
        //                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                    {
        //                                        UserAccountId = _UserInfo.IssuerId,
        //                                        ModeId = TransactionMode.Credit,
        //                                        TypeId = TransactionType.ReferralBonus,
        //                                        SourceId = TransactionSource.TUC,
        //                                        Amount = _AmountDistrubutionRequest.Issuer,
        //                                        TotalAmount = _AmountDistrubutionRequest.Issuer,
        //                                        TransactionDate = _Request.TransactionDate,
        //                                    });
        //                                }
        //                                else
        //                                {
        //                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                    {
        //                                        UserAccountId = _UserInfo.IssuerId,
        //                                        ModeId = TransactionMode.Credit,
        //                                        TypeId = TransactionType.ReferralBonus,
        //                                        SourceId = TransactionSource.Settlement,

        //                                        Amount = _AmountDistrubutionRequest.Issuer,
        //                                        TotalAmount = _AmountDistrubutionRequest.Issuer,
        //                                        TransactionDate = _Request.TransactionDate,
        //                                    });
        //                                }
        //                            }
        //                            if (_AmountDistrubutionRequest.TransactionIssuerTotalAmount > 0)
        //                            {
        //                                if (_OGatewayInfo.TransactionIssuerAccountTypeId == UserAccountType.Appuser)
        //                                {
        //                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                    {
        //                                        UserAccountId = _OGatewayInfo.TransactionIssuerId,
        //                                        ModeId = TransactionMode.Credit,
        //                                        TypeId = TransactionType.TransactionBonus,
        //                                        SourceId = TransactionSource.TUC,

        //                                        Amount = _AmountDistrubutionRequest.TransactionIssuerAmount,
        //                                        Charge = _AmountDistrubutionRequest.TransactionIssuerCharge,
        //                                        TotalAmount = _AmountDistrubutionRequest.TransactionIssuerTotalAmount,
        //                                        TransactionDate = _Request.TransactionDate,
        //                                    });
        //                                }
        //                                else
        //                                {
        //                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                    {
        //                                        UserAccountId = _OGatewayInfo.TransactionIssuerId,
        //                                        ModeId = TransactionMode.Credit,
        //                                        TypeId = TransactionType.TransactionBonus,
        //                                        SourceId = TransactionSource.Settlement,

        //                                        Amount = _AmountDistrubutionRequest.TransactionIssuerAmount,
        //                                        Charge = _AmountDistrubutionRequest.TransactionIssuerCharge,
        //                                        TotalAmount = _AmountDistrubutionRequest.TransactionIssuerTotalAmount,
        //                                        TransactionDate = _Request.TransactionDate,
        //                                    });
        //                                }
        //                            }
        //                            if (_AmountDistrubutionRequest.Ptsa > 0)
        //                            {
        //                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                {
        //                                    UserAccountId = SystemAccounts.SmashLabId,
        //                                    ModeId = TransactionMode.Credit,
        //                                    TypeId = TransactionTypeId,
        //                                    SourceId = TransactionSource.Settlement,

        //                                    Amount = _AmountDistrubutionRequest.Ptsa,
        //                                    TotalAmount = _AmountDistrubutionRequest.Ptsa,
        //                                    TransactionDate = _Request.TransactionDate,
        //                                });
        //                            }
        //                            if (_AmountDistrubutionRequest.ThankUCash > 0)
        //                            {
        //                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                {
        //                                    UserAccountId = SystemAccounts.ThankUCashMerchant,
        //                                    ModeId = TransactionMode.Credit,
        //                                    TypeId = TransactionTypeId,
        //                                    SourceId = TransactionSource.Settlement,

        //                                    Amount = _AmountDistrubutionRequest.ThankUCash,
        //                                    TotalAmount = _AmountDistrubutionRequest.ThankUCash,
        //                                    TransactionDate = _Request.TransactionDate,

        //                                });
        //                            }
        //                            if (_AmountDistrubutionRequest.MerchantReverseAmount > 0)
        //                            {
        //                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                {
        //                                    UserAccountId = _OGatewayInfo.MerchantId,
        //                                    ModeId = TransactionMode.Credit,
        //                                    TypeId = TransactionType.MerchantCredit,
        //                                    SourceId = TransactionSource.Merchant,

        //                                    Amount = _AmountDistrubutionRequest.MerchantReverseAmount,
        //                                    Comission = 0,
        //                                    TotalAmount = _AmountDistrubutionRequest.MerchantReverseAmount,
        //                                    TransactionDate = _Request.TransactionDate,
        //                                });
        //                            }
        //                            _CoreTransactionRequest.Transactions = _TransactionItems;
        //                            _ManageCoreTransaction = new ManageCoreTransaction();
        //                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
        //                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
        //                            {
        //                                _OCampaignProcessor = new OCampaignProcessor();
        //                                _OCampaignProcessor._GatewayInfo = _OGatewayInfo;
        //                                _OCampaignProcessor._UserInfo = _UserInfo;
        //                                _OCampaignProcessor._UserRequest = _Request;
        //                                _OCampaignProcessor.AccountNumber = _CoreTransactionRequest.AccountNumber;
        //                                #region TransactionPostProcess
        //                                var _TransactionPostProcessActor = ActorSystem.Create("AccountOtherCardCampaignProcessorActor");
        //                                var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<AccountOtherCardCampaignProcessorActor>("AccountOtherCardCampaignProcessorActor");
        //                                _TransactionPostProcessActorNotify.Tell(_OCampaignProcessor);
        //                                #endregion
        //                                return _CoreOperations.ProcessTransactionResponse(_Request, _OGatewayInfo, _UserInfo, _AmountDistrubutionRequest, TransactionResponse);
        //                            }
        //                            else
        //                            {
        //                                #region Send Response
        //                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
        //                                #endregion
        //                            }
        //                        }
        //                        else
        //                        {
        //                            string Comment = null;
        //                            if (_UserInfo.AccountStatusId == HelperStatus.Default.Active)
        //                            {
        //                                Comment = "No Reward";
        //                            }
        //                            else if (_UserInfo.AccountStatusId == HelperStatus.Default.Suspended)
        //                            {
        //                                Comment = "User account suspended";
        //                            }
        //                            else
        //                            {
        //                                Comment = "User account blocked";
        //                            }
        //                            long TransactionTypeId = TransactionType.CardReward;
        //                            switch (_Request.TransactionMode)
        //                            {
        //                                case "cash":
        //                                    TransactionTypeId = TransactionType.CashReward;
        //                                    break;
        //                                case "card":
        //                                    TransactionTypeId = TransactionType.CardReward;
        //                                    break;
        //                                case "redeemreward":
        //                                    TransactionTypeId = TransactionType.RedeemReward;
        //                                    break;
        //                                default:
        //                                    TransactionTypeId = TransactionType.CashReward;
        //                                    if (!string.IsNullOrEmpty(_Request.bin) || !string.IsNullOrEmpty(_Request.SixDigitPan))
        //                                    {
        //                                        TransactionTypeId = TransactionType.CardReward;
        //                                    }
        //                                    break;
        //                            }
        //                            _CoreTransactionRequest = new OCoreTransaction.Request();
        //                            _CoreTransactionRequest.CashierId = _OGatewayInfo.CashierId;
        //                            _CoreTransactionRequest.CustomerId = _UserInfo.UserAccountId;
        //                            _CoreTransactionRequest.UserReference = _Request.UserReference;
        //                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
        //                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
        //                            _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
        //                            _CoreTransactionRequest.InvoiceAmount = (InvoiceAmount - RedeemAmount);
        //                            _CoreTransactionRequest.ReferenceInvoiceAmount = InvoiceAmount;
        //                            _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
        //                            _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
        //                            _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
        //                            _CoreTransactionRequest.SubParentId = _OGatewayInfo.StoreId;
        //                            if (!string.IsNullOrEmpty(_Request.bin))
        //                            {
        //                                _CoreTransactionRequest.AccountNumber = _Request.bin;
        //                            }
        //                            else
        //                            {
        //                                _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
        //                            }
        //                            _CoreTransactionRequest.CardId = _UserInfo.CardId;
        //                            if (_OGatewayInfo.AcquirerId != null)
        //                            {
        //                                _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
        //                            }
        //                            _CoreTransactionRequest.ReferenceAmount = _AmountDistrubutionRequest.RewardAmount;
        //                            _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
        //                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
        //                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                            {
        //                                UserAccountId = _OGatewayInfo.MerchantId,
        //                                ModeId = TransactionMode.Debit,
        //                                TypeId = TransactionTypeId,
        //                                SourceId = TransactionSource.Merchant,

        //                                Amount = 0,
        //                                Comission = 0,
        //                                TotalAmount = 0,
        //                                TransactionDate = _Request.TransactionDate,
        //                            });
        //                            if (_AmountDistrubutionRequest.IsThankUCashEnabled == true)
        //                            {
        //                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                {
        //                                    UserAccountId = _UserInfo.UserAccountId,
        //                                    ModeId = TransactionMode.Credit,
        //                                    TypeId = TransactionTypeId,
        //                                    SourceId = TransactionSource.ThankUCashPlus,
        //                                    Amount = 0,
        //                                    TotalAmount = 0,
        //                                    TransactionDate = _Request.TransactionDate,
        //                                    Comment = Comment,
        //                                });
        //                            }
        //                            else
        //                            {
        //                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                {
        //                                    UserAccountId = _UserInfo.UserAccountId,
        //                                    ModeId = TransactionMode.Credit,
        //                                    TypeId = TransactionTypeId,
        //                                    SourceId = TransactionSource.TUC,

        //                                    Amount = 0,
        //                                    TotalAmount = 0,
        //                                    TransactionDate = _Request.TransactionDate,
        //                                    Comment = Comment,
        //                                });
        //                            }
        //                            _CoreTransactionRequest.Transactions = _TransactionItems;
        //                            _ManageCoreTransaction = new ManageCoreTransaction();
        //                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
        //                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
        //                            {
        //                                _OCampaignProcessor = new OCampaignProcessor();
        //                                _OCampaignProcessor._GatewayInfo = _OGatewayInfo;
        //                                _OCampaignProcessor._UserInfo = _UserInfo;
        //                                _OCampaignProcessor._UserRequest = _Request;
        //                                _OCampaignProcessor.AccountNumber = _CoreTransactionRequest.AccountNumber;
        //                                #region TransactionPostProcess
        //                                var _TransactionPostProcessActor = ActorSystem.Create("AccountOtherCardCampaignProcessorActor");
        //                                var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<AccountOtherCardCampaignProcessorActor>("AccountOtherCardCampaignProcessorActor");
        //                                _TransactionPostProcessActorNotify.Tell(_OCampaignProcessor);
        //                                #endregion
        //                                return _CoreOperations.ProcessTransactionResponse(_Request, _OGatewayInfo, _UserInfo, _AmountDistrubutionRequest, TransactionResponse);
        //                            }
        //                            else
        //                            {
        //                                #region Send Response
        //                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
        //                                #endregion
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        _HCoreContext.Dispose();
        //                        #region Send Response
        //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG124");
        //                        #endregion
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _UserInfo.ResponseCode, _UserInfo.Message);
        //                #endregion
        //            }
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        HCoreHelper.LogException(LogLevel.High, "SettleTransaction", _Exception, _Request.UserReference);
        //        #endregion
        //        #region Send Response
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
        //        #endregion
        //    }
        //    #endregion
        //}
    }
}
