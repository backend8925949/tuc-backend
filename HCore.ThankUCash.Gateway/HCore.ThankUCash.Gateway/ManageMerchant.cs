//==================================================================================
// FileName: ManageMerchant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;
using HCore.ThankUCash.Gateway.Framework;
using HCore.ThankUCash.Gateway.Object;

namespace HCore.ThankUCash.Gateway
{
    public class ManageMerchant
    {
        FrameworkMerchantManager _FrameworkMerchantManager;


        public OResponse RegisterMerchant(OThankUGateway.MerchantRegistration.Request _Request)
        {
            _FrameworkMerchantManager = new FrameworkMerchantManager();
            return _FrameworkMerchantManager.WebPay_Confirm(_Request);
        }
    }
}
