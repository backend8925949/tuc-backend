//==================================================================================
// FileName: ManageThankUCashGateway.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using HCore.ThankUCash.Gateway.Crm;
using HCore.ThankUCash.Gateway.Framework;
using HCore.ThankUCash.Gateway.Framework.Redeem;
//using HCore.ThankUCash.Gateway.Integration;
using HCore.ThankUCash.Gateway.Object;

namespace HCore.ThankUCash.Gateway
{
    public class ManageThankUCashGateway
    {
        CrmProduct _CrmProduct;
        FrameworkThankUCashGateway _FrameworkThankUCashGateway;
        FrameworkOperations _FrameworkOperations;
        FrameworkRedeem _FrameworkRedeem;
        //FrameworkRedeemV2 _FrameworkRedeemV2;
        //FrameworkSoftcom _FrameworkSoftcom;
        public OResponse SaveProduct(OThankUGateway.Request _Request)
        {
            _CrmProduct = new CrmProduct();
            return _CrmProduct.SaveProduct(_Request);
        }
        //Operations - START
        public OResponse GetBalance(OThankUGateway.Request _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.GetBalance(_Request);
        }
        // Operations - End
        public OResponse ConnectMerchant(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.ConnectMerchant(_Request);
        }
        public OResponse RegisterUser(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.RegisterUser(_Request);
        }
        public OResponse ResetUssdUserPin(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.ResetUssdUserPin(_Request);
        }
        public OResponse UpdateUssdUser(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.UpdateUssdUser(_Request);
        }
        public OResponse RegisterUssdUser(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.RegisterUssdUser(_Request);
        }
        public OResponse RemoveMerchant(OThankUGateway.Request _Request)
        {
            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.RemoveMerchant(_Request);
        }

        public OResponse GetConfiguration(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.GetConfiguration(_Request);
        }

        public OResponse CreditReward(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.Reward(_Request);
        }

        public OResponse ValidateAccount(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.ValidateAccount(_Request);
        }

        public OResponse NotifySettlements(OThankUGateway.NotifySettlement _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.NotifySettlements(_Request);
        }

        public OResponse MerchantReward(OThankUGateway.Request _Request)
        {
            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.MerchantReward(_Request);
        }
        public OResponse MerchantRedeem(OThankUGateway.Request _Request)
        {

            _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
            return _FrameworkThankUCashGateway.MerchantRedeem(_Request);
        }
        ////REDEEM - START
        //public OResponse Redeem(OThankUGateway.Request _Request)
        //{
        //    _FrameworkRedeem = new FrameworkRedeem();
        //    return _FrameworkRedeem.Redeem(_Request);
        //}
        //public OResponse ConfirmRedeem(OThankUGateway.Request _Request)
        //{
        //    _FrameworkRedeem = new FrameworkRedeem();
        //    return _FrameworkRedeem.ConfirmRedeem(_Request);
        //}
        ////REDEEM - END
        //Async REDEEM - START
        public async Task<OResponse> Redeem(OThankUGateway.Request _Request)
        {
            _FrameworkRedeem = new FrameworkRedeem();
            return await _FrameworkRedeem.Redeem(_Request);
        }

        public async Task<OResponse> InitializeRedeem(OThankUGateway.Request _Request, bool flag = true)
        {
            _FrameworkRedeem = new FrameworkRedeem();
            return await _FrameworkRedeem.InitializeRedeem(_Request);
        }

        public async Task<OResponse> ConfirmRedeem(OThankUGateway.Request _Request)
        {
            _FrameworkRedeem = new FrameworkRedeem();
            return await _FrameworkRedeem.ConfirmRedeem(_Request);
        }
        // Async REDEEM - END

    }
}
