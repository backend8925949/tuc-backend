//==================================================================================
// FileName: OThankUCashGateway.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.ThankUCash.Gateway.Object
{

    internal class OCampaignProcessor
    {
        public OThankUGateway.Request _UserRequest { get; set; }
        public OGatewayInfo _GatewayInfo { get; set; }
        public OUserInfo _UserInfo { get; set; }
        public long CardBankId { get; set; }
        public string? AccountNumber { get; set; }

    }
    public class OProuductManager
    {
        public string? MerchantCode { get; set; }
        public List<OProductManager.Details> Products { get; set; }
        public OUserReference? UserReference { get; set; }
    }
    public class OProductManager
    {
        public class PurchaseItem
        {
            public string? ReferenceId { get; set; }
            public long Price { get; set; }
            public long Quantity { get; set; }
            public long TotalAmount { get; set; }
        }
        public class Details
        {
            public string? ReferenceId { get; set; }
            public string? Sku { get; set; }
            public string? Name { get; set; }
            public long Price { get; set; }
            public long TotalStock { get; set; }
            public long RewardPercentage { get; set; }
            public string? CategoryName { get; set; }
            public string? SubCategoryName { get; set; }
            public string? Status { get; set; } // active | inactive | deleted

            public string? ResponseCode { get; set; }
            public string? ResponseStatus { get; set; }
            public string? ResponseMessage { get; set; }
        }

        public class DetailsResponse
        {
            public string? Sku { get; set; }
            public string? ReferenceId { get; set; }
            public string? StatusCode { get; set; }
            public string? Status { get; set; }
            public string? Message { get; set; }
        }

    }
    internal class OTUInfo
    {
        //public long CardId { get; set; }
        public long? UserAccountId { get; set; }
        public long? UserId { get; set; }
        public long? AccountType { get; set; }
        public string? MobileNumber { get; set; }
        public string? UserPin { get; set; }
        public string? DisplayName { get; set; }
        public string? EmailAddress { get; set; }
        public long? OwnerId { get; set; }
        public long? CardStatusId { get; set; }
        public long? OwnerAccountTypeId { get; set; }
        public long? AccountStatusId { get; set; }
        public long? CreatedById { get; set; }
        public long? CreatedByAccountTypeId { get; set; }
    }
    internal class OAmountDistribution
    {
        public bool IsThankUCashCustomer { get; set; } = false;
        public bool IsThankUCashEnabled { get; set; } = false;
        public bool IsThankUCashGold { get; set; } = false;

        public double InvoiceAmount { get; set; } = 0;
        public double ReferenceInvoiceAmount { get; set; } = 0;

        public double TUCRewardAmount { get; set; } = 0;
        public double TUCRewardCommissionAmount { get; set; } = 0;
        public double TUCRewardUserAmount { get; set; } = 0;



        public double RewardPercentage { get; set; } = 0;
        public double MerchantAmount { get; set; } = 0;

        public double User { get; set; } = 0;
        public double UserPercentage { get; set; } = 0;

        public double Ptsp { get; set; } = 0;
        public double PtspPercentage { get; set; } = 0;

        public double Pssp { get; set; } = 0;
        public double PsspPercentage { get; set; } = 0;

        public double Ptsa { get; set; } = 0;
        public double PtsaPercentage { get; set; } = 0;

        public double Acquirer { get; set; } = 0;
        public double AcquirerPercentage { get; set; } = 0;

        public double Issuer { get; set; } = 0;
        public double IssuerPercentage { get; set; } = 0;

        public double TransactionIssuerAmount { get; set; } = 0; // Transaction Referral Bonus By Cashier || Default is Thank U Cash
        public double TransactionIssuerCharge { get; set; } = 0; // Transaction Referral Bonus By Cashier || Default is Thank U Cash
        public double TransactionIssuerTotalAmount { get; set; } = 0; // Transaction Referral Bonus By Cashier || Default is Thank U Cash
        public double TransactionIssuerPercentage { get; set; } = 0; // Transaction Referral Bonus By Cashier || Default is Thank U Cash

        public double ThankUCash { get; set; } = 0;
        public double ThankUCashPercentage { get; set; } = 0;
        public bool ThankUCashPercentageFromRewardAmount { get; set; } = false;

        public double MerchantReverseAmount { get; set; } = 0;
        public string? AllowAcquirerSettlement { get; set; } = "0";

        public string? ResponseCode { get; set; }
        public string? ResponseMessage { get; set; }

        public int TransactionSourceId { get; set; }
        public int TransactionTypeId { get; set; }
        public int TransactionStatus { get; set; }
        //public bool IsMerchantLowBalance { get; set; }
    }
    internal class OGatewayInfo
    {
        internal HCoreConstant.ResponseStatus Status { get; set; }
        internal string Message { get; set; }
        internal string ResponseCode { get; set; }

        internal long? MerchantReferrerId { get; set; }
        internal long? MerchantReferrerAccountTypeId { get; set; }

        internal long TerminalId { get; set; }
        internal long MerchantId { get; set; }
        internal long? MerchantPrimaryCategoryId { get; set; }
        internal long? MerchantSecondaryCategoryId { get; set; }
        internal long StoreId { get; set; }
        internal long StoreStatusId { get; set; }
        internal long PtspId { get; set; }
        internal long PsspId { get; set; }
        internal long? AcquirerId { get; set; }
        internal long CashierId { get; set; }
        internal long CashierStatusId { get; set; }
        internal long TransactionIssuerId { get; set; }
        internal long TransactionIssuerAccountTypeId { get; set; }
        internal long? AcquirerAccountTypeId { get; set; }
        internal string MerchantDisplayName { get; set; }
        internal string StoreDisplayName { get; set; }


    }
    internal class OUserInfo
    {
        internal HCoreConstant.ResponseStatus Status { get; set; }
        internal string Message { get; set; }
        internal string ResponseCode { get; set; }

        internal long? OwnerId { get; set; }
        internal long? OwnerAccountTypeId { get; set; }

        internal long UserAccountId { get; set; }
        internal string UserAccountKey { get; set; }


        internal string DisplayName { get; set; }
        internal string Name { get; set; }
        internal string MobileNumber { get; set; }
        internal string EmailAddress { get; set; }
        internal string Gender { get; set; }


        internal string DealKey { get; set; }
        internal string LoanKey { get; set; }
        internal string AccountNumber { get; set; }
        internal string Pin { get; set; }
        internal double DealAmount { get; set; }
        internal string FeaturePin { get; set; }
        internal long AccountStatusId { get; set; }

        internal sbyte IsRedeemed { get; set; }

        //internal long? AccountType { get; set; }
        //internal long UserMerchantId { get; set; }
        //internal string UserAccountKey { get; set; }
        internal long IssuerId { get; set; }
        internal long IssuerAccountTypeId { get; set; }

        //internal long CardId { get; set; }

        internal long? CreatedById { get; set; }
        internal long? CreatedByAccountTypeId { get; set; }

        internal double InvoiceAmount { get; set; }
        internal double RewardAmount { get; set; }
        internal double RedeemAmount { get; set; }
    }
    public class OThankUGateway
    {
        public class NotifyPayment
        {
            public class Request
            {

                public string? referenceId { get; set; }                 // Unique transaction id
                public string? clientId { get; set; }                    // GA Id
                public string? userId { get; set; }                      // Mobile Number
                public double amount { get; set; }                      // Invoice Amount
                public double fees { get; set; }                        // Fees
                public string? currency { get; set; }                    // Currency Name 'NGN'
                public string? createdAt { get; set; }                   // Transaction Date / Create Date
                public string? completedAt { get; set; }                 // Transaction Complete Date / Modify Date
                public string? channel { get; set; }                     // Payment Channel  - CARD | ACCOUNT | ACCOUNT_OTP | USSD

                public string? status { get; set; }                      // Transaction Status "SUCCESS"
                public string? statusMessage { get; set; }               // Status message

                public string? notificationStatus { get; set; }          // If notification have been sent to client
                public string? providerReference { get; set; }           // Reference number from payment provider
                public string? provider { get; set; }                    // Payment processing provider to use
                public string? bankCode { get; set; }                    // Payment account back code
                public string? bankAccount { get; set; }                 // Payment account bank account number
                public string? providerInitiated { get; set; }           // Notification status
                public string? platformSettled { get; set; }             // Notification status
                public string? providerResponse { get; set; }            // Notification status
                public string? hash { get; set; }                        // Notification status
                public string? paymentUrl { get; set; }                  // Notification status
                public string? callbackURL { get; set; }                 // Notification status
                public string? callbackMode { get; set; }                // Notification status
                public OMetadata metadata { get; set; }
                public OPaymentInstruement paymentInstrument { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class OMetadata
            {
                public string? merchant_amount { get; set; }               //  Payment instrument internal reference
                public string? fee_percentage { get; set; }                  //  User Mobile Number
                public string? cashier_id { get; set; }        //  Reference payment (combination of payment referenceand application reference )
                public string? commission_amount { get; set; }                  //  Status of the payment instrument , default to true
                public string? user_id { get; set; }          //  CARD , ACCOUNT
                public string? transaction_fee { get; set; }                //  Payment provider name e.g. posvas
                public string? acquirer_amount { get; set; }               //  Date payment would expiry
            }
            public class OPaymentInstruement
            {
                public string? reference { get; set; }               //  Payment instrument internal reference
                public string? userId { get; set; }                  //  User Mobile Number
                public string? paymentReference { get; set; }        //  Reference payment (combination of payment referenceand application reference )
                public string? active { get; set; }                  //  Status of the payment instrument , default to true
                public string? instrumentType { get; set; }          //  CARD , ACCOUNT
                public string? provider { get; set; }                //  Payment provider name e.g. posvas
                public string? expiresAt { get; set; }               //  Date payment would expiry
                public string? reusable { get; set; }                //  If payment instrument can be re used for subsequent transaction
                public string? createdAt { get; set; }               //  Time instrument was first used
                public string? attributes { get; set; }              //  Instrument specific attribute from provider
                public string? masked { get; set; }                  //  Masked Card Number
                public string? status { get; set; }                  //  Current status of the payment
            }
        }
        public class NotifySettlement
        {
            public string? eventd { get; set; }                 // Unique transaction id
            public NotifySettlementData data { get; set; }                 // Unique transaction id
            public OUserReference? UserReference { get; set; }
        }
        public class NotifySettlementData
        {
            public long id { get; set; }
            public string? domain { get; set; }
            public string? date { get; set; }
            public string? currency { get; set; }
            public double total_amount { get; set; }
            public double total_fees { get; set; }
            public double total_processed { get; set; }
            public double total_reward { get; set; }
            public string? created_at { get; set; }
            public object merchants { get; set; }
        }
        public class RequestTemp
        {
            public long AccountId { get; set; }
            public long AccountType { get; set; }
            public string? DisplayName { get; set; }
            public long UserId { get; set; }
            public string? Pin { get; set; }
            public long? CreatedById { get; set; }
            public long? CreatedByAccountTypeId { get; set; }
        }
        public class RequestT
        {

            public string? Name { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? EmailAddress { get; set; }
            public string? Gender { get; set; }

            public string? TransactionMode { get; set; }

            public string? CashierId { get; set; }
            public string? ErrorMessage { get; set; }


            public string? MerchantId { get; set; }
            public string? TerminalId { get; set; }

            public string? MobileNumber { get; set; }
            public string? CardNumber { get; set; }
            public string? TagNumber { get; set; }
            public string? Pin { get; set; }
            public string? SixDigitPan { get; set; }

            public string? ReferenceNumber { get; set; }
            public string? InvoiceAmount { get; set; }

            public long RewardAmount { get; set; }
            public long RedeemAmount { get; set; }
            public long ChangeAmount { get; set; }
            public string? TransactionReference { get; set; }
            public string? TCode { get; set; }

            public string? Comment { get; set; }
            public DateTime? TransactionDate { get; set; }
            public DateTime? DateOfBirth { get; set; }
            public List<OProductManager.Details> Products { get; set; }
            public List<OProductManager.PurchaseItem> Items { get; set; }
            public List<OProduct> sProducts { get; set; }
            public string? ban { get; set; }
            public string? bin { get; set; } //  ITEX sending card number in bin 

            public OUserReference? UserReference { get; set; }
        }

        public class MerchantRegistration
        {
            public class Request
            {
                public string? PSSPMerchantId { get; set; }
                public Business Business { get; set; }
                public Business Contact { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public string? PSSPMerchantId { get; set; }
                public string? MerchantId { get; set; }
                public string? RewardPercentage { get; set; }
                public string? Status { get; set; }
            }

            public class Business
            {
                public string? Name { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
                public string? Address { get; set; }
            }
        }

        public class Request
        {

            public string? Name { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? EmailAddress { get; set; }
            public string? Gender { get; set; }

            public string? TransactionMode { get; set; }

            public string? CashierId { get; set; }
            public string? ErrorMessage { get; set; }


            public string? MerchantId { get; set; }
            public string? StoreId { get; set; }
            public string? TerminalId { get; set; }

            public string? MobileNumber { get; set; }
            public string? CardNumber { get; set; }
            public string? TagNumber { get; set; }
            public string? Pin { get; set; }
            public string? SixDigitPan { get; set; }

            public string? ReferenceNumber { get; set; }
            public long InvoiceAmount { get; set; }
            public string? InvoiceNumber { get; set; }
            public long RewardAmount { get; set; }
            public long RedeemAmount { get; set; }
            public long ChangeAmount { get; set; }
            public string? TransactionReference { get; set; }
            public string? TCode { get; set; }

            public string? Comment { get; set; }
            public DateTime? TransactionDate { get; set; }
            public DateTime? DateOfBirth { get; set; }
            public List<OProductManager.Details> Products { get; set; }
            public List<OProductManager.PurchaseItem> Items { get; set; }
            public List<OProduct> SProducts { get; set; }
            public string? ban { get; set; }
            public string? bin { get; set; } //  ITEX sending card number in bin 
            public OUserReference? UserReference { get; set; }
        }

        public class ProductResponse
        {
            public string? MerchantId { get; set; }
            public List<OProductManager.DetailsResponse> Products { get; set; }
        }

        public class OProduct
        {
            public string? _id { get; set; }
            public string? item { get; set; }
            public long quantity { get; set; }
            public double price { get; set; }
            public string? name { get; set; }
        }

        public class Response
        {
            // Timings
            public DateTime? StartTime { get; set; }
            public DateTime? EndTime { get; set; }
            public DateTime? TransactionDate { get; set; }

            //Amounts ,  Commissions & Distribution
            public long? Commission { get; set; }
            public long? ProviderAmount { get; set; }
            public long? CommissionAmount { get; set; }
            public double? RewardPercentage { get; set; }
            public long? RewardAmount { get; set; }
            public long? UserRewardAmount { get; set; }
            public long? AcquirerAmount { get; set; }
            public long? MerchantAmount { get; set; }
            public long? ChangeAmount { get; set; }
            public long? RedeemAmount { get; set; }
            public long? InvoiceAmount { get; set; }


            // Balance
            public long? Balance { get; set; }
            public long? TUCGoldBalance { get; set; }
            public long? ThankUCashBalance { get; set; }
            public long? DealBalance { get; set; }
            public long? GiftPointBalance { get; set; }
            public long? GiftCardBalance { get; set; }
            public long? ThankUCashPlusBalance { get; set; }
            public long? BalanceValidity { get; set; }

            // Profile
            public string? MobileNumber { get; set; }
            public string? Name { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? EmailAddress { get; set; }
            public string? Gender { get; set; }
            public DateTime? DateOfBirth { get; set; }


            // References
            public string? ReferenceNumber { get; set; }
            public string? TransactionReference { get; set; }

        }
        // SoftCom
        public class SCNotifyProductPaymentRequest
        {
            public string? eventType { get; set; }
            public List<SCPaymentProduct> products { get; set; }
            public SCPaymentCustomer customer { get; set; }
            public SCPaymentTransaction transaction { get; set; }
            public SCPaymentTill till { get; set; }
            public SCPayment payment { get; set; }
            public SCManageProduct Product { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class SCManageProduct
        {
            public bool visibleOnNearby { get; set; }
            public string? _id { get; set; }
            public bool deleted { get; set; }
            public string? business { get; set; }
            public string? name { get; set; }
            public double price { get; set; }
            public string? businessCounter { get; set; }
            public string? counter { get; set; }
            public string? businessSKU { get; set; }
            public long totalStock { get; set; }
            public DateTime createdAt { get; set; }
            public DateTime updatedAt { get; set; }
            public long __v { get; set; }
            public List<SCManageProductStore> stores { get; set; }
        }
        public class SCManageProductStore
        {
            public string? _id { get; set; }
            public string? storeID { get; set; }
            public long stock { get; set; }
            public string? name { get; set; }
            public string? address { get; set; }
            public string? emailaddress { get; set; }
        }
        public class SCPaymentProduct
        {
            public string? _id { get; set; }
            public string? item { get; set; }
            public string? name { get; set; }
            public long quantity { get; set; }
            public double price { get; set; }
        }
        public class SCPaymentCustomer
        {
            public string? id { get; set; }
            public string? name { get; set; }
            public string? phone { get; set; }
            public string? email { get; set; }
        }
        public class SCPaymentTransaction
        {
            public string? reference { get; set; }
            public double amount { get; set; }
            public string? currency { get; set; }
            public DateTime? createdAt { get; set; }
            public DateTime? paidAt { get; set; }
        }
        public class SCPaymentTill
        {
            public string? id { get; set; }
            public string? name { get; set; }
            public string? terminalID { get; set; }
            public string? branch { get; set; }
        }
        public class SCPayment
        {
            public string[] channels { get; set; }
            public string? billID { get; set; }
            public string? status { get; set; }
            public string? chargesApplied { get; set; }
            public SCDiscountOnTheGo discountOnTheGo { get; set; }
            public List<SCTax> till { get; set; }
            public List<SCDiscount> discounts { get; set; }
        }
        public class SCTax
        {
            public string? _id { get; set; }
            public string? taxID { get; set; }
            public double percentage { get; set; }
            public string? name { get; set; }
        }
        public class SCDiscount
        {
            public string? _id { get; set; }
            public string? discountID { get; set; }
            public string? valueType { get; set; }
            public double value { get; set; }
            public string? name { get; set; }
        }
        public class SCDiscountOnTheGo
        {
            public string? _id { get; set; }
            public double amount { get; set; }
            public string? tillID { get; set; }
        }
    }
}
