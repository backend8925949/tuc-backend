//==================================================================================
// FileName: FrameworkMerchant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.ThankUCash.Gateway.ObjectV2;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.ThankUCash.Gateway.FrameworkV2
{
    public class FrameworkMerchant
    {
        #region Declare
        HCoreContext _HCoreContext;
        TUCTerminal _TUCTerminal;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        Random _Random;
        HCUAccountParameter _HCUAccountParameter;
        List<HCUAccountParameter> _HCUAccountParameters;
        #endregion
        internal OResponse SaveMerchant(OAccounts.Merchant.Onboarding _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1068, TUCCoreResource.CA1068M);
                }
                if (string.IsNullOrEmpty(_Request.DisplayName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1064, TUCCoreResource.CA1064M);
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1065, TUCCoreResource.CA1065M);
                }
                if (string.IsNullOrEmpty(_Request.ContactNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1066, TUCCoreResource.CA1066M);
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1067, TUCCoreResource.CA1067M);
                }
                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.Address, _Request.UserReference);
                using (_HCoreContext = new HCoreContext())
                {
                    _Random = new Random();
                    string AccountCode = _Random.Next(100, 999).ToString() + _Random.Next(000000000, 999999999).ToString();
                    _Request.ReferenceKey = HCoreHelper.GenerateGuid();
                    _HCUAccountParameters = new List<HCUAccountParameter>();
                    _HCUAccountParameter = new HCUAccountParameter();
                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                    _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage").Select(x => x.Id).FirstOrDefault();
                    _HCUAccountParameter.Value = _Request.RewardPercentage.ToString();
                    _HCUAccountParameter.StartTime = HCoreHelper.GetGMTDateTime();
                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                    _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccountParameters.Add(_HCUAccountParameter);

                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(6);
                    _HCUAccountAuth.Password = HCoreHelper.GenerateRandomNumber(6);
                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    #region Save UserAccount
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.Guid = _Request.ReferenceKey;
                    _HCUAccount.AccountTypeId = UserAccountType.Merchant;
                    _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                    _HCUAccount.OwnerId = _Request.UserReference.AccountId;
                    _HCUAccount.DisplayName = _Request.DisplayName;
                    if (_Request.OwnerId != 0)
                    {
                        _HCUAccount.OwnerId = _Request.OwnerId;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(_Request.ReferralCode))
                        {
                            long ReferrerId = _HCoreContext.HCUAccount
                                .Where(x => x.ReferralCode == _Request.ReferralCode
                                && (x.AccountTypeId == UserAccountType.Merchant || x.AccountTypeId == UserAccountType.Acquirer || x.AccountTypeId == UserAccountType.PgAccount || x.AccountTypeId == UserAccountType.PosAccount))
                                .Select(x => x.Id).FirstOrDefault();
                            if (ReferrerId > 0)
                            {
                                _HCUAccount.OwnerId = ReferrerId;
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1069, TUCCoreResource.CA1069M);
                            }
                        }
                    }
                    _HCUAccount.Name = _Request.Name;
                    _HCUAccount.EmailAddress = _Request.EmailAddress;
                    _HCUAccount.ContactNumber = _Request.ContactNumber;
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
                    _HCUAccount.ReferralUrl = _Request.ReferenceNumber;
                    if (_Request.ContactPerson != null)
                    {
                        _HCUAccount.FirstName = _Request.ContactPerson.FirstName;
                        _HCUAccount.LastName = _Request.ContactPerson.LastName;
                        _HCUAccount.SecondaryEmailAddress = _Request.ContactPerson.EmailAddress;
                        _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber);
                    }
                    _HCUAccount.Address = _AddressResponse.Address;
                    _HCUAccount.Latitude = _AddressResponse.Latitude;
                    _HCUAccount.Longitude = _AddressResponse.Longitude;
                    _HCUAccount.CountryId = _AddressResponse.CountryId;
                    if (_AddressResponse.StateId != 0)
                    {
                        _HCUAccount.StateId = _AddressResponse.StateId;
                    }
                    if (_AddressResponse.CityId != 0)
                    {
                        _HCUAccount.CityId = _AddressResponse.CityId;
                    }
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    }
                    if (_Request.StatusCode == "active")
                    {
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                    }
                    else
                    {
                        _HCUAccount.StatusId = HelperStatus.Default.Inactive;
                    }
                    _HCUAccount.CountryId = (int?)_Request.UserReference.CountryId;
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.AccountPercentage = _Request.RewardPercentage;
                    _HCUAccount.User = _HCUAccountAuth;
                    _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                    #endregion
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    _HCoreContext.SaveChanges();
                    long MerchantId = _HCUAccount.Id;
                    if (_Request.Stores != null)
                    {
                        foreach (var _Store in _Request.Stores)
                        {
                            OAddressResponse _StoreAddress = HCoreHelper.GetAddressComponent(_Store.Address, _Request.UserReference);
                            using (_HCoreContext = new HCoreContext())
                            {
                                _HCUAccountAuth = new HCUAccountAuth();
                                _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(10);
                                _HCUAccountAuth.Password = HCoreHelper.GenerateRandomNumber(6);
                                _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                                if (_Request.UserReference.AccountId != 0)
                                {
                                    _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                                }
                                _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                #region Save UserAccount
                                _HCUAccount = new HCUAccount();
                                _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                                _HCUAccount.AccountOperationTypeId = AccountOperationType.Offline;
                                _HCUAccount.OwnerId = MerchantId;
                                _HCUAccount.ReferralUrl = _Request.ReferenceNumber;
                                _HCUAccount.DisplayName = _Store.DisplayName;
                                _HCUAccount.Name = _Store.Name;
                                _HCUAccount.EmailAddress = _Store.EmailAddress;
                                _HCUAccount.ContactNumber = _Store.ContactNumber;
                                if (_Store.ContactPerson != null)
                                {
                                    _HCUAccount.FirstName = _Store.ContactPerson.FirstName;
                                    _HCUAccount.LastName = _Store.ContactPerson.LastName;
                                    _HCUAccount.SecondaryEmailAddress = _Store.ContactPerson.EmailAddress;
                                    _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Store.ContactPerson.MobileNumber);
                                }
                                _HCUAccount.Address = _StoreAddress.Address;
                                _HCUAccount.Latitude = _StoreAddress.Latitude;
                                _HCUAccount.Longitude = _StoreAddress.Longitude;
                                _HCUAccount.CountryId = _StoreAddress.CountryId;
                                if (_StoreAddress.StateId != 0)
                                {
                                    _HCUAccount.StateId = _StoreAddress.StateId;
                                }
                                if (_StoreAddress.CityId != 0)
                                {
                                    _HCUAccount.CityId = _StoreAddress.CityId;
                                }
                                _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                                _HCUAccount.AccountCode = _Random.Next(100000000, 999999999).ToString();
                                if (_Request.UserReference.AppVersionId != 0)
                                {
                                    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                                }
                                _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                                _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                                _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                if (_Request.UserReference.AccountId != 0)
                                {
                                    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                }
                                if (_Store.StatusCode == "active")
                                {
                                    _HCUAccount.StatusId = HelperStatus.Default.Active;
                                }
                                else
                                {
                                    _HCUAccount.StatusId = HelperStatus.Default.Inactive;
                                }
                                _HCUAccount.EmailVerificationStatus = 0;
                                _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccount.NumberVerificationStatus = 0;
                                _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccount.User = _HCUAccountAuth;
                                #endregion

                                _Store.ReferenceId = _HCUAccount.Id;
                                _Store.ReferenceKey = _HCUAccount.Guid;
                                if (_Store.Terminals != null)
                                {
                                    foreach (var _Terminal in _Store.Terminals)
                                    {
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            bool TerminalIdCheck = _HCoreContext.TUCTerminal.Any(x => x.IdentificationNumber == _Terminal.TerminalId);
                                            if (!TerminalIdCheck)
                                            {
                                                //_HCUAccountAuth = new HCUAccountAuth();
                                                //_HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                                //_HCUAccountAuth.Username = _Terminal.TerminalId;
                                                //_HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Terminal.TerminalId);
                                                //_HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                                //_HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                                //_HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                                                //if (_Request.UserReference.AccountId != 0)
                                                //{
                                                //    _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                                                //}
                                                //_HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                                //#region Save UserAccount
                                                //_HCUAccount = new HCUAccount();
                                                //_HCUAccount.Guid = HCoreHelper.GenerateGuid();
                                                //_HCUAccount.AccountTypeId = UserAccountType.TerminalAccount;
                                                //_HCUAccount.AccountOperationTypeId = AccountOperationType.Offline;
                                                //_HCUAccount.OwnerId = _Terminal.ProviderId;
                                                //_HCUAccount.BankId = _Request.UserReference.AccountId;
                                                //_HCUAccount.SubOwnerId = _Store.ReferenceId;
                                                //_HCUAccount.DisplayName = _Terminal.TerminalId;
                                                //_HCUAccount.ReferralUrl = _Terminal.ReferenceNumber;
                                                //_HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                                                //_HCUAccount.AccountCode = _Random.Next(100000, 999999).ToString() + _Random.Next(000000000, 999999999).ToString();
                                                //if (_Request.UserReference.AppVersionId != 0)
                                                //{
                                                //    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                                                //}
                                                //_HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                                                //_HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                                                //_HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                                //if (_Request.UserReference.AccountId != 0)
                                                //{
                                                //    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                                //}
                                                //if (_Terminal.StatusCode == "active")
                                                //{
                                                //    _HCUAccount.StatusId = HelperStatus.Default.Active;
                                                //}
                                                //else
                                                //{
                                                //    _HCUAccount.StatusId = HelperStatus.Default.Inactive;
                                                //}
                                                //_HCUAccount.Name = _Terminal.TerminalId;
                                                //_HCUAccount.CountryId = _Request.UserReference.CountryId;
                                                //_HCUAccount.EmailVerificationStatus = 0;
                                                //_HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                                //_HCUAccount.NumberVerificationStatus = 0;
                                                //_HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                                //_HCUAccount.User = _HCUAccountAuth;
                                                //#endregion
                                                _TUCTerminal = new TUCTerminal();
                                                _TUCTerminal.Guid = HCoreHelper.GenerateGuid();
                                                _TUCTerminal.IdentificationNumber = _Terminal.TerminalId;
                                                _TUCTerminal.DisplayName = _Terminal.TerminalId;
                                                _TUCTerminal.SerialNumber = _Terminal.SerialNumber;
                                                //_TUCTerminal.Account = _HCUAccount;
                                                _TUCTerminal.MerchantId = MerchantId;
                                                _TUCTerminal.StoreId = _Store.ReferenceId;
                                                _TUCTerminal.ProviderId = _Request.UserReference.AccountId;
                                                _TUCTerminal.AcquirerId = _Request.UserReference.AccountId;
                                                _TUCTerminal.CreateDate = HCoreHelper.GetGMTDateTime();
                                                _TUCTerminal.CreatedById = _Request.UserReference.AccountId;
                                                _HCoreContext.TUCTerminal.Add(_TUCTerminal);
                                                _HCoreContext.SaveChanges();
                                                _Terminal.ReferenceKey = _TUCTerminal.Guid;
                                                _Terminal.ReferenceId = _TUCTerminal.Id;

                                                
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                    var _Response = new
                    {
                        ReferenceId = _HCUAccount.Id,
                        ReferenceKey = _HCUAccount.Guid,
                    };
                    //ManageSubscription _ManageSubscription = new ManageSubscription();
                    //_ManageSubscription.AddAccountFreeSubscription(MerchantId, UserAccountType.Merchant);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1062, TUCCoreResource.CA1062M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveMerchant", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        internal OResponse SaveStore(OAccounts.Store.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.MerchantReferenceNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1054, TUCCoreResource.CA1054M);
                }

                if (string.IsNullOrEmpty(_Request.DisplayName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1055, TUCCoreResource.CA1055M);
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1056, TUCCoreResource.CA1056M);
                }
                if (string.IsNullOrEmpty(_Request.ContactNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1057, TUCCoreResource.CA1057M);
                }
                OAddressResponse _AddressResponse = HCoreHelper.GetAddressComponent(_Request.Address, _Request.UserReference);
                using (_HCoreContext = new HCoreContext())
                {
                    long MerchantId = _HCoreContext.HCUAccount.Where(x => x.ReferralUrl == _Request.MerchantReferenceNumber).Select(x => x.Id).FirstOrDefault();
                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(10);
                    _HCUAccountAuth.Password = HCoreHelper.GenerateRandomNumber(6);
                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    #region Save UserAccount
                    string TGuid = HCoreHelper.GenerateGuid();
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.Guid = TGuid;
                    _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                    _HCUAccount.AccountOperationTypeId = AccountOperationType.Offline;
                    _HCUAccount.OwnerId = MerchantId;
                    //_HCUAccount.SubOwnerId = _Request.StoreId;
                    _HCUAccount.DisplayName = _Request.DisplayName;
                    _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                    _HCUAccount.Name = _Request.Name;
                    _HCUAccount.EmailAddress = _Request.EmailAddress;
                    _HCUAccount.ContactNumber = _Request.ContactNumber;
                    if (_Request.ContactPerson != null)
                    {
                        _HCUAccount.FirstName = _Request.ContactPerson.FirstName;
                        _HCUAccount.LastName = _Request.ContactPerson.LastName;
                        _HCUAccount.SecondaryEmailAddress = _Request.ContactPerson.EmailAddress;
                        _HCUAccount.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ContactPerson.MobileNumber);
                    }
                    _HCUAccount.Address = _AddressResponse.Address;
                    _HCUAccount.Latitude = _AddressResponse.Latitude;
                    _HCUAccount.Longitude = _AddressResponse.Longitude;
                    _HCUAccount.CountryId = _AddressResponse.CountryId;
                    if (_AddressResponse.StateId != 0)
                    {
                        _HCUAccount.StateId = _AddressResponse.StateId;
                    }
                    if (_AddressResponse.CityId != 0)
                    {
                        _HCUAccount.CityId = _AddressResponse.CityId;
                    }
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                    _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    }
                    _HCUAccount.StatusId = HelperStatus.Default.Active;
                    _HCUAccount.CountryId = (int?)_Request.UserReference.CountryId;
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    _HCUAccount.User = _HCUAccountAuth;
                    #endregion
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    _HCoreContext.SaveChanges();
                    using (_HCoreContext = new HCoreContext())
                    {
                        var TItem = _HCoreContext.HCUAccount.Where(x => x.Guid == TGuid).FirstOrDefault();
                        var _Response = new
                        {
                            ReferenceId = TItem.Id,
                            ReferenceKey = TItem.Guid,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1062, TUCCoreResource.CA1062M);

                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveStore", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
        internal OResponse SaveTerminal(OAccounts.Terminal.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.TerminalId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1038, TUCCoreResource.CA1038M);
                }
                if (string.IsNullOrEmpty(_Request.StoreReferenceNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1042, TUCCoreResource.CA1042);
                }

                //if (_Request.BranchId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1047, TUCCoreResource.CA1047M);
                //}
                //if (string.IsNullOrEmpty(_Request.BranchKey))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1048, TUCCoreResource.CA1048M);
                //}
                //if (_Request.RmId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1049, TUCCoreResource.CA1049M);
                //}
                //if (string.IsNullOrEmpty(_Request.RmKey))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1050, TUCCoreResource.CA1050M);
                //}

                using (_HCoreContext = new HCoreContext())
                {
                    bool TerminalIdCheck = _HCoreContext.TUCTerminal.Any(x => x.IdentificationNumber == _Request.TerminalId);
                    if (TerminalIdCheck)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1051, TUCCoreResource.CA1051M);
                    }
                    var StoreDetails = _HCoreContext.HCUAccount.Where(x => x.ReferralUrl == _Request.StoreReferenceNumber).Select(x => new
                    {
                        StoreId = x.Id,
                        MerchantId = x.OwnerId
                    }).FirstOrDefault();
                    //_HCUAccountAuth = new HCUAccountAuth();
                    //_HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    //_HCUAccountAuth.Username = _Request.TerminalId;
                    //_HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.TerminalId);
                    //_HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    //_HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    //_HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                    //if (_Request.UserReference.AccountId != 0)
                    //{
                    //    _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    //}
                    //_HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    //#region Save UserAccount
                    //_HCUAccount = new HCUAccount();
                    //_HCUAccount.Guid = HCoreHelper.GenerateGuid();
                    //_HCUAccount.AccountTypeId = UserAccountType.TerminalAccount;
                    //_HCUAccount.AccountOperationTypeId = AccountOperationType.Offline;
                    //_HCUAccount.OwnerId = _Request.UserReference.AccountId;
                    //_HCUAccount.SubOwnerId = StoreDetails.StoreId;
                    //_HCUAccount.DisplayName = _Request.TerminalId;
                    //_HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                    //_HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
                    //if (_Request.UserReference.AppVersionId != 0)
                    //{
                    //    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    //}
                    //_HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    //_HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                    //_HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                    //if (_Request.UserReference.AccountId != 0)
                    //{
                    //    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                    //}
                    //_HCUAccount.StatusId = HelperStatus.Default.Active;
                    //_HCUAccount.Name = _Request.TerminalId;
                    //_HCUAccount.CountryId = _Request.UserReference.CountryId;
                    //_HCUAccount.EmailVerificationStatus = 0;
                    //_HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    //_HCUAccount.NumberVerificationStatus = 0;
                    //_HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                    //_HCUAccount.User = _HCUAccountAuth;
                    //#endregion
                    _TUCTerminal = new TUCTerminal();
                    _TUCTerminal.Guid = HCoreHelper.GenerateGuid();
                    _TUCTerminal.DisplayName = _Request.TerminalId;
                    _TUCTerminal.SerialNumber = _Request.SerialNumber;
                    //_TUCTerminal.Account = _HCUAccount;
                    _TUCTerminal.MerchantId = StoreDetails.MerchantId;
                    _TUCTerminal.StoreId = StoreDetails.StoreId;
                    _TUCTerminal.ProviderId = _Request.UserReference.AccountId;
                    _TUCTerminal.AcquirerId = _Request.UserReference.AccountId;
                    _TUCTerminal.CreateDate = HCoreHelper.GetGMTDateTime();
                    _TUCTerminal.CreatedById = _Request.UserReference.AccountId;
                    _HCoreContext.TUCTerminal.Add(_TUCTerminal);
                    _HCoreContext.SaveChanges();
                    var _Response = new
                    {
                        ReferenceId = _TUCTerminal.Id,
                        ReferenceKey = _TUCTerminal.Guid,
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCCoreResource.CA1052, TUCCoreResource.CA1052M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveTerminal", _Exception, _Request.UserReference, TUCCoreResource.CA0500, TUCCoreResource.CA0500M);
            }
            #endregion
        }
    }


    internal class TUCCoreResource
    {
        internal const string CA0500 = "CA0500";
        internal const string CA0500M = "Internal server error occured. Plese try after some time";

        internal const string CA0200 = "CA0200";
        internal const string CA0200M = "Details loaded";

        internal const string CA0404 = "CA0404";
        internal const string CA0404M = "Details not found. It might be removed or not available at the moment";

        internal const string CA0201 = "CA0201";
        internal const string CA0201M = "Details updated successfully";

        internal const string CAREF = "CAREF";
        internal const string CAREFM = "Reference id missing";

        internal const string CAREFKEY = "CAREFKEY";
        internal const string CAREFKEYM = "Reference key missing";

        internal const string CASTATUS = "CASTATUS";
        internal const string CASTATUSM = "Status code required";
        internal const string CAINSTATUS = "CAINSTATUS";
        internal const string CAINSTATUSM = "Invalid status code";

        internal const string CA1000 = "CA1000";
        internal const string CA1000M = "Product name required";
        internal const string CA1001 = "CA1001";
        internal const string CA1001M = "Sku required";
        //internal const string CA1002 = "CA1002";
        //internal const string CA1002M = "Status code required";
        //internal const string CA1003 = "CA1003";
        //internal const string CA1003M = "Invalid status code";
        internal const string CA1004 = "CA1004";
        internal const string CA1004M = "Invalid account details";
        internal const string CA1005 = "CA1005";
        internal const string CA1005M = "Account details missing";
        internal const string CA1006 = "CA1006";
        internal const string CA1006M = "Product details already present";
        internal const string CA1007 = "CA1007";
        internal const string CA1007M = "Product saved successfully";
        internal const string CA1008 = "CA1008";
        internal const string CA1008M = "Product deleted successfully";
        internal const string CA1009 = "CA1009";
        internal const string CA1009M = "Invalid status code";
        internal const string CA1010 = "CA1010";
        internal const string CA1010M = "Name already exists";

        internal const string CA1011 = "CA1011";
        internal const string CA1011M = "Account reference missing";
        internal const string CA1012 = "CA1012";
        internal const string CA1012M = "Account reference key missing";
        internal const string CA1013 = "CA1013";
        internal const string CA1013M = "Manager information missing";
        internal const string CA1014 = "CA1014";
        internal const string CA1014M = "Select existing manager or add new manager information";
        internal const string CA1015 = "CA1015";
        internal const string CA1015M = "Branch name required";
        internal const string CA1016 = "CA1016";
        internal const string CA1016M = "Branch display name required";
        internal const string CA1017 = "CA1017";
        internal const string CA1017M = "Branch code required";
        internal const string CA1018 = "CA1018";
        internal const string CA1018M = "Phone number required";
        internal const string CA1019 = "CA1019";
        internal const string CA1019M = "Email address required";
        internal const string CA1020 = "CA1020";
        internal const string CA1020M = "State information required";
        internal const string CA1021 = "CA1021";
        internal const string CA1021M = "City information required";
        internal const string CA1022 = "CA1022";
        internal const string CA1022M = "Region information required";
        internal const string CA1023 = "CA1023";
        internal const string CA1023M = "Invalid account details";
        internal const string CA1024 = "CA1024";
        internal const string CA1024M = "Branch created successfully";
        internal const string CA1025 = "CA1025";
        internal const string CA1025M = "Branch id required";
        internal const string CA1026 = "CA1026";
        internal const string CA1026M = "Branch key required";
        internal const string CA1027 = "CA1027";
        internal const string CA1027M = "Account role required";
        internal const string CA1028 = "CA1028";
        internal const string CA1028M = "Name required";
        internal const string CA1029 = "CA1029";
        internal const string CA1029M = "Mobile number required";
        internal const string CA1030 = "CA1030";
        internal const string CA1030M = "Email address required";
        internal const string CA1031 = "CA1031";
        internal const string CA1031M = "Account id required";
        internal const string CA1032 = "CA1032";
        internal const string CA1032M = "Account key required";
        internal const string CA1033 = "CA1033";
        internal const string CA1033M = "Name required";
        internal const string CA1034 = "CA1034";
        internal const string CA1034M = "Mobile number required";
        internal const string CA1035 = "CA1035";
        internal const string CA1035M = "Email address required";
        internal const string CA1036 = "CA1036";
        internal const string CA1036M = "Sub account added successfully";
        internal const string CA1037 = "CA1037";
        internal const string CA1037M = "Terminal status updated";
        internal const string CA1038 = "CA1038";
        internal const string CA1038M = "Terminal id required";
        internal const string CA1039 = "CA1039";
        internal const string CA1039M = "Merchant id required";
        internal const string CA1040 = "CA1040";
        internal const string CA1040M = "Merchant key requred";
        internal const string CA1041 = "CA1041";
        internal const string CA1041M = "Store id required";
        internal const string CA1042 = "CA1042";
        internal const string CA1042M = "Store key required";
        internal const string CA1043 = "CA1043";
        internal const string CA1043M = "Acquirer id required";
        internal const string CA1044 = "CA1044";
        internal const string CA1044M = "Acquirer key required";
        internal const string CA1045 = "CA1045";
        internal const string CA1045M = "Provider id required";
        internal const string CA1046 = "CA1046";
        internal const string CA1046M = "Provider key required";
        internal const string CA1047 = "CA1047";
        internal const string CA1047M = "Branch id required";
        internal const string CA1048 = "CA1048";
        internal const string CA1048M = "Branch key required";
        internal const string CA1049 = "CA1049";
        internal const string CA1049M = "Rm id required";
        internal const string CA1050 = "CA1050";
        internal const string CA1050M = "Rm key required";
        internal const string CA1051 = "CA1051";
        internal const string CA1051M = "Terminal id already present";
        internal const string CA1052 = "CA1052";
        internal const string CA1052M = "New terminal added successfully";
        internal const string CA1053 = "CA1053";
        internal const string CA1053M = "Merchant id required";
        internal const string CA1054 = "CA1054";
        internal const string CA1054M = "Merchant key required";
        internal const string CA1055 = "CA1055";
        internal const string CA1055M = "Store display name required";
        internal const string CA1056 = "CA1056";
        internal const string CA1056M = "Store name required";
        internal const string CA1057 = "CA1057";
        internal const string CA1057M = "Store contact number required";
        internal const string CA1058 = "CA1058";
        internal const string CA1058M = "Store address reequired";
        internal const string CA1059 = "CA1059";
        internal const string CA1059M = "Store location required";
        internal const string CA1060 = "CA1060";
        internal const string CA1060M = "Branch Id required";
        internal const string CA1061 = "CA1061";
        internal const string CA1061M = "Rm id required";
        internal const string CA1062 = "CA1062";
        internal const string CA1062M = "New store added successfully";
        internal const string CA1063 = "CA1063";
        internal const string CA1063M = "New account created successfully";
        internal const string CA1064 = "CA1064";
        internal const string CA1064M = "Merchant display name required";
        internal const string CA1065 = "CA1065";
        internal const string CA1065M = "Merchant name required";
        internal const string CA1066 = "CA1066";
        internal const string CA1066M = "Contact number required";
        internal const string CA1067 = "CA1067";
        internal const string CA1067M = "Email address required";
        internal const string CA1068 = "CA1068";
        internal const string CA1068M = "Reference number required";
        internal const string CA1069 = "CA1069";
        internal const string CA1069M = "Invalid Referral Code";
        internal const string CA1070 = "CA1070";
        internal const string CA1070M = "xxxxxxxxx";
        internal const string CA1071 = "CA1071";
        internal const string CA1071M = "xxxxxxxxx";
        internal const string CA1072 = "CA1072";
        internal const string CA1072M = "xxxxxxxxx";
        internal const string CA1073 = "CA1073";
        internal const string CA1073M = "xxxxxxxxx";
        internal const string CA1074 = "CA1074";
        internal const string CA1074M = "xxxxxxxxx";
        internal const string CA1075 = "CA1075";
        internal const string CA1075M = "xxxxxxxxx";
        internal const string CA1076 = "CA1076";
        internal const string CA1076M = "xxxxxxxxx";
        internal const string CA1077 = "CA1077";
        internal const string CA1077M = "xxxxxxxxx";
        internal const string CA1078 = "CA1078";
        internal const string CA1078M = "xxxxxxxxx";
        internal const string CA1079 = "CA1079";
        internal const string CA1079M = "xxxxxxxxx";
        internal const string CA1080 = "CA1080";
        internal const string CA1080M = "xxxxxxxxx";
    }
}
