//==================================================================================
// FileName: FrameworkRedeem.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.ThankUCash.Gateway.Actor;
using HCore.ThankUCash.Gateway.Core;
using HCore.ThankUCash.Gateway.ObjectV2;
using Z.EntityFramework.Plus;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.ThankUCash.Gateway.Helper.HelperGateway;
namespace HCore.ThankUCash.Gateway.FrameworkV2
{
    internal class FrameworkRedeem
    {
        #region Declare
        FrameworkThankUCashGateway _FrameworkThankUCashGateway;
        //FrameworkOperations  _FrameworkOperations;
        HCoreContext _HCoreContext;
        //ManageCoreUserAccess _ManageCoreUserAccess;
        ManageCoreTransaction _ManageCoreTransaction;
        //OUserInfo _OUserInfo;
        OThankUGateway.Response _GatewayResponse;
        //OAppProfile.Request _AppProfileRequest;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        #endregion
        #region Private Operatons
        internal OResponse Redeem(OThankUGateway.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                //long RedeemAvailablityCheck =Convert.ToInt64(HCoreHelper.GetConfiguration("systemredeem"));
                //if (RedeemAvailablityCheck == 0)
                //{
                //     #region Send Response
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG137",CoreResources.HCG137);
                //    #endregion
                //}
                if ((_Request.UserReference.AccountTypeId == UserAccountType.PosAccount || _Request.UserReference.AccountTypeId == UserAccountType.PgAccount) && string.IsNullOrEmpty(_Request.MerchantId) && string.IsNullOrEmpty(_Request.TerminalId))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100",CoreResources.HCG100);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber) && string.IsNullOrEmpty(_Request.CardNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109",CoreResources.HCG109);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115",CoreResources.HCG115);
                    #endregion
                }
                 else if (string.IsNullOrEmpty(_Request.Pin))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG110");
                    #endregion
                }
                else if ((_Request.RedeemAmount / 100) < 1)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG128",CoreResources.HCG128);
                    #endregion
                }
                else if ((_Request.InvoiceAmount / 100) < 1)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG127",CoreResources.HCG127);
                    #endregion
                }
                else if (_Request.RedeemAmount > _Request.InvoiceAmount)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG117",CoreResources.HCG127);
                    #endregion
                }
                else
                {
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length < 10)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG145", CoreResources.HCG145);
                        #endregion
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length > 13)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG146", CoreResources.HCG146);
                        #endregion
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && !System.Text.RegularExpressions.Regex.IsMatch(_Request.MobileNumber, "^[0-9]*$"))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG147", CoreResources.HCG147);
                        #endregion
                    }
                    OGatewayInfo _OGatewayInfo = FrameworkOperations.GetGatewayInfo(_Request);
                    #region Perform Operations
                    if (_OGatewayInfo != null)
                    {
                        //long MerchantRedeemAvailablityCheck =Convert.ToInt64(HCoreHelper.GetConfiguration("merchantredeem",_OGatewayInfo.MerchantId));
                        //if (MerchantRedeemAvailablityCheck == 0)
                        //{
                        //    #region Send Response
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG138",CoreResources.HCG138);
                        //    #endregion
                        //}
                        if (!string.IsNullOrEmpty(_Request.CashierId))
                        {
                            if (_OGatewayInfo.CashierId == 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG134", CoreResources.HCG134);
                                #endregion
                            }
                            else if (_OGatewayInfo.CashierId != 0 && _OGatewayInfo.CashierStatusId != HelperStatus.Default.Active)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG135", CoreResources.HCG135);
                                #endregion
                            }
                        }
                        OUserInfo _UserInfo = FrameworkOperations.GetUserInfo(_Request, _OGatewayInfo, false);
                        if (_UserInfo != null)
                        {
                            switch (_UserInfo.AccountStatusId)
                            {
                                case HelperStatus.Default.Active:
                                    if (string.IsNullOrEmpty(_UserInfo.Pin))
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG112", CoreResources.HCG112);
                                        #endregion
                                    }
                                    else
                                    {
                                        string DPin = HCoreEncrypt.DecryptHash(_UserInfo.Pin);
                                        if (DPin == _Request.Pin)
                                        {
                                           



                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            double RedeemAmount = (double)_Request.RedeemAmount / 100;
                                            double InvoiceAmount = (double)_Request.InvoiceAmount / 100;
                                            double UserBalance = _ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId);
                                            double UserGiftPointBalance = _ManageCoreTransaction.GetAppUserGiftPointsBalance(_UserInfo.UserAccountId, _OGatewayInfo.MerchantId);
                                            double ProductAmount = 0;
                                            long ProductId = 0;
                                            long ProductTransactionSourceId = 0;
                                            long ProductCodeId = 0;
                                            long RedeemSourceId = 0;
                                            #region Get User Products
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                var TransactionDetails = _HCoreContext.HCUAccountTransaction.Where(x => x.ReferenceNumber == _Request.ReferenceNumber ).Select(x => new
                                                {
                                                    Id = x.Id,
                                                }).FirstOrDefault();
                                                if (TransactionDetails != null)
                                                {
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG149", CoreResources.HCG149);
                                                    #endregion
                                                }

                                                var ProductBalance = _HCoreContext.CAProductCode
                                                                          .Where(x => x.AccountId == _UserInfo.UserAccountId
                                                                                  && ((x.Product.UsageTypeId == HelperType.ProductUsageType.FullValue && x.AvailableAmount == RedeemAmount)
                                                                                       || (x.Product.UsageTypeId == HelperType.ProductUsageType.PartialValue && x.AvailableAmount >= RedeemAmount))
                                                                                  && (x.Product.TypeId == Product.GiftCard || x.Product.TypeId == Product.QuickGiftCard)
                                                                                  && x.AvailableAmount <= RedeemAmount
                                                                                  && x.AvailableAmount > 0
                                                                                  && x.StatusId == HelperStatus.ProdutCode.Unused)
                                                                          .Select(x => new
                                                                          {
                                                                              ProductId = x.ProductId,
                                                                              CodeId = x.Id,
                                                                              x.AvailableAmount,
                                                                              UsageTypeId = x.Product.UsageTypeId
                                                                          })
                                                                          .FirstOrDefault();
                                                if (ProductBalance != null)
                                                {
                                                    ProductTransactionSourceId = TransactionSource.GiftCards;
                                                    ProductAmount = (double)ProductBalance.AvailableAmount;
                                                    ProductId = (long)ProductBalance.ProductId;
                                                    ProductCodeId = (long)ProductBalance.CodeId;
                                                }
                                                _HCoreContext.Dispose();
                                            }
                                            #endregion
                                            if (ProductAmount >= RedeemAmount)
                                            {
                                                RedeemSourceId = ProductTransactionSourceId;
                                            }
                                            else if (UserGiftPointBalance >= RedeemAmount)
                                            {
                                                RedeemSourceId = TransactionSource.GiftPoints;
                                            }
                                            else if (UserBalance >= RedeemAmount)
                                            {
                                                RedeemSourceId = TransactionSource.TUC;
                                            }
                                            else
                                            {
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG120", CoreResources.HCG120);
                                            }
                                            double RewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", _OGatewayInfo.MerchantId)), _AppConfig.SystemRoundPercentage);
                                            if (RewardPercentage > 0)
                                            {
                                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                                _CoreTransactionRequest.CustomerId = _UserInfo.UserAccountId;
                                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Initialized;
                                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                                _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
                                                _CoreTransactionRequest.SubParentId = _OGatewayInfo.StoreId;
                                                _CoreTransactionRequest.InvoiceAmount = InvoiceAmount;
                                                _CoreTransactionRequest.ReferenceInvoiceAmount = InvoiceAmount;
                                                _CoreTransactionRequest.ReferenceAmount = RedeemAmount;
                                                _CoreTransactionRequest.CashierId = _OGatewayInfo.CashierId;
                                                _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
                                                _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                                if (_OGatewayInfo.AcquirerId != null)
                                                {
                                                    _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
                                                }
                                                if (_OGatewayInfo.TerminalId != 0)
                                                {
                                                    _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                                }
                                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                                // Debit From User
                                                //_TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                //{
                                                //    UserAccountId = _UserInfo.UserAccountId,
                                                //    ModeId = TransactionMode.Debit,
                                                //    TypeId = TransactionType.OnlineRedeem,
                                                //    SourceId = RedeemSourceId,
                                                //    Amount = RedeemAmount,
                                                //    TotalAmount = RedeemAmount,
                                                //});
                                                if (RedeemSourceId == TransactionSource.GiftCards)
                                                {
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = _UserInfo.UserAccountId,
                                                        ModeId = TransactionMode.Debit,
                                                        TypeId = TransactionType.Loyalty.TUCRedeem.GiftCardRedeem,
                                                        SourceId = TransactionSource.GiftCards,
                                                        Amount = RedeemAmount,
                                                        TotalAmount = RedeemAmount,
                                                        Comment = ProductCodeId.ToString(),
                                                    });
                                                }
                                                if (RedeemSourceId == TransactionSource.TUC)
                                                {
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = _UserInfo.UserAccountId,
                                                        ModeId = TransactionMode.Debit,
                                                        TypeId = TransactionType.Loyalty.TUCRedeem.PosRedeem,
                                                        SourceId = TransactionSource.TUC,
                                                        Amount = RedeemAmount,
                                                        TotalAmount = RedeemAmount,
                                                    });
                                                }
                                                if (RedeemSourceId == TransactionSource.GiftPoints)
                                                {
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = _UserInfo.UserAccountId,
                                                        ModeId = TransactionMode.Debit,
                                                        TypeId = TransactionType.Loyalty.TUCRedeem.GiftPointRedeem,
                                                        SourceId = TransactionSource.GiftPoints,
                                                        Amount = RedeemAmount,
                                                        TotalAmount = RedeemAmount,
                                                    });
                                                }
                                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                                {
                                                    _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
                                                    _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
                                                    _GatewayResponse.TransactionReference = TransactionResponse.GroupKey;
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG121");
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                                    #endregion
                                                }
                                            }
                                            else
                                            {
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG120", CoreResources.HCG136);
                                            }
                                            //#region Get Balance 
                                            //_ManageCoreTransaction = new ManageCoreTransaction();
                                            //double UserAvailableBalance = UserBalance + UserGiftPointBalance + ProductAmount;
                                            //if (RedeemAmount < (UserAvailableBalance + 1))
                                            //{
                                            //    double RewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", _OGatewayInfo.MerchantId)), _AppConfig.SystemRoundPercentage);
                                            //    if (RewardPercentage > 0)
                                            //    {
                                            //        //double MainBalanceDebitAmount = 0;
                                            //        //double GiftCardBalanceDebitAmount = 0;
                                            //        //if (UserGiftCardBalance > 0)
                                            //        //{
                                            //        //    if (RedeemAmount < (UserGiftCardBalance + 1))
                                            //        //    {
                                            //        //        MainBalanceDebitAmount = 0;
                                            //        //        GiftCardBalanceDebitAmount = RedeemAmount;
                                            //        //    }
                                            //        //    else
                                            //        //    {
                                            //        //        GiftCardBalanceDebitAmount = UserGiftCardBalance;
                                            //        //        MainBalanceDebitAmount = (RedeemAmount - GiftCardBalanceDebitAmount);
                                            //        //    }
                                            //        //}
                                            //        //else
                                            //        //{
                                            //        //    GiftCardBalanceDebitAmount = 0;
                                            //        //    MainBalanceDebitAmount = RedeemAmount;
                                            //        //}
                                            //        //if(GiftCardBalanceDebitAmount  <  0);
                                            //        _CoreTransactionRequest = new OCoreTransaction.Request();
                                            //        _CoreTransactionRequest.CustomerId = _UserInfo.UserAccountId;
                                            //        _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            //        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Pending;
                                            //        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            //        _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
                                            //        _CoreTransactionRequest.SubParentId = _OGatewayInfo.StoreId;
                                            //        _CoreTransactionRequest.InvoiceAmount = InvoiceAmount;
                                            //        _CoreTransactionRequest.ReferenceInvoiceAmount = InvoiceAmount;
                                            //        _CoreTransactionRequest.ReferenceAmount = RedeemAmount;
                                            //        _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
                                            //        _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                            //        if (_OGatewayInfo.AcquirerId != null)
                                            //        {
                                            //            _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
                                            //        }
                                            //        if (_OGatewayInfo.TerminalId != 0)
                                            //        {
                                            //            _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                            //        }
                                            //        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            //        // Debit From User
                                            //        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            //        {
                                            //            UserAccountId = _UserInfo.UserAccountId,
                                            //            ModeId = TransactionMode.Debit,
                                            //            TypeId = TransactionType.OnlineRedeem,
                                            //            SourceId = TransactionSource.TUC,

                                            //            Amount = RedeemAmount,
                                            //            TotalAmount = RedeemAmount,
                                            //        });
                                            //        //if (MainBalanceDebitAmount > 0)
                                            //        //{
                                            //        //    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            //        //    {
                                            //        //        UserAccountId = _UserInfo.UserAccountId,
                                            //        //        ModeId = TransactionMode.Debit,
                                            //        //        TypeId = TransactionType.OnlineRedeem,
                                            //        //        SourceId = TransactionSource.TUC,
                                            //        //        Amount = MainBalanceDebitAmount,
                                            //        //        TotalAmount = MainBalanceDebitAmount,
                                            //        //    });
                                            //        //}
                                            //        //if (GiftCardBalanceDebitAmount > 0)
                                            //        //{
                                            //        //    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            //        //    {
                                            //        //        UserAccountId = _UserInfo.UserAccountId,
                                            //        //        ModeId = TransactionMode.Debit,
                                            //        //        TypeId = TransactionType.GiftPointRedeem,
                                            //        //        SourceId = TransactionSource.GiftPoints,
                                            //        //        Amount = GiftCardBalanceDebitAmount,
                                            //        //        TotalAmount = GiftCardBalanceDebitAmount,
                                            //        //    });
                                            //        //}
                                            //        _CoreTransactionRequest.Transactions = _TransactionItems;
                                            //        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            //        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                            //        {
                                            //            _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
                                            //            _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
                                            //            _GatewayResponse.TransactionReference = TransactionResponse.GroupKey;
                                            //            #region Send Response
                                            //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG121");
                                            //            #endregion
                                            //        }
                                            //        else
                                            //        {
                                            //            #region Send Response
                                            //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                            //            #endregion
                                            //        }
                                            //    }
                                            //    else
                                            //    {
                                            //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG120", CoreResources.HCG136);
                                            //    }
                                            //}
                                            //else
                                            //{
                                            //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG120", CoreResources.HCG120);
                                            //}
                                            //#endregion
                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG114", CoreResources.HCG114);
                                        }
                                    }
                                case HelperStatus.Default.Suspended:
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, "HCG131", CoreResources.HCG131);
                                case HelperStatus.Default.Blocked:
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, "HCG132", CoreResources.HCG132);
                                default:
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, "HCG133", CoreResources.HCG133);
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, "HCG111", CoreResources.HCG111);
                            #endregion
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG103", CoreResources.HCG103);
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "Redeem", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG500, CoreResources.HCG500M);
                #endregion
            }
            #endregion
        }
         internal OResponse ConfirmRedeem(OThankUGateway.Request _Request)         {             #region Declare             _GatewayResponse = new OThankUGateway.Response();             #endregion             #region Manage Exception             try             {                 if ((_Request.UserReference.AccountTypeId == UserAccountType.PosAccount || _Request.UserReference.AccountTypeId == UserAccountType.PgAccount) && string.IsNullOrEmpty(_Request.MerchantId) && string.IsNullOrEmpty(_Request.TerminalId))                 {                     #region Send Response                     return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100",CoreResources.HCG100);                     #endregion                 }                 else if (string.IsNullOrEmpty(_Request.TransactionReference))                 {                     #region Send Response                     return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG122", CoreResources.HCG122);                     #endregion                 }                 else                 {                     OGatewayInfo _OGatewayInfo =FrameworkOperations.GetGatewayInfo(_Request);                     #region Perform Operations                     using (_HCoreContext = new HCoreContext())                     {                         if (_OGatewayInfo != null)                         {                             var TransactionDetails = _HCoreContext.HCUAccountTransaction.Where(x => x.Guid == _Request.TransactionReference && x.ParentId == _OGatewayInfo.MerchantId).Select(x => new                             {                                 Id = x.Id,                                 Guid = x.Guid,                                 TotalAmount = x.TotalAmount,                                 PurchaseAmount = x.PurchaseAmount,                                 TransactionDate = x.TransactionDate,                                 AccountNumber = x.AccountNumber,                                 ReferenceNumber = x.ReferenceNumber,                                 ParentId = x.ParentId,                                 UserAccountId = x.AccountId,                                 Status = x.StatusId,                                 CreatedById = x.CreatedBy,                                 UserMobileNumber = x.Account.MobileNumber,                                 UserDisplayName = x.Account.DisplayName,                                 UserEmailAddress = x.Account.EmailAddress,                                 MerchantDisplayName = x.Parent.DisplayName,                                 MobileNumber = x.Account.MobileNumber,                                 SubParentId = x.SubParentId,                                 UserAccountStatusId = x.Account.StatusId,                                 ReferenceAmount = x.ReferenceAmount,                                 Comment = x.Comment,                                 SourceId = x.SourceId,                                 CashierId = x.CashierId,                             }).FirstOrDefault();                              if (TransactionDetails != null)                             {                                 if (TransactionDetails.Status == HelperStatus.Transaction.Initialized)                                 {
                                    _ManageCoreTransaction = new ManageCoreTransaction();                                     if (TransactionDetails.UserAccountStatusId == HelperStatus.Default.Active)                                     {                                         double InvoiceAmount = HCoreHelper.RoundNumber(((double)TransactionDetails.PurchaseAmount), 2);                                         double RedeemAmount = HCoreHelper.RoundNumber(((double)TransactionDetails.TotalAmount), 2);
                                        double ProductAmount = 0;
                                        long ProductId = 0;
                                        long ProductCodeId = 0;
                                        bool IsBalanceStatusOk = false;
                                        string ErrorMessage = "";
                                        string ErrorMessageCode = "";
                                        switch (TransactionDetails.SourceId)
                                        {
                                            case TransactionSource.GiftPoints:
                                                double AccountGiftPointBalance = _ManageCoreTransaction.GetAppUserGiftPointsBalance((long)TransactionDetails.UserAccountId, _OGatewayInfo.MerchantId);
                                                if (AccountGiftPointBalance >= RedeemAmount)
                                                {
                                                    // Process Transaction
                                                    IsBalanceStatusOk = true;
                                                }
                                                else
                                                {
                                                    // Cancel Transaction
                                                    IsBalanceStatusOk = false;
                                                    ErrorMessage = CoreResources.HCG139;
                                                    ErrorMessageCode = "HCG139";
                                                }
                                                break;
                                            case TransactionSource.GiftCards:
                                                if (!string.IsNullOrEmpty(TransactionDetails.Comment))
                                                {
                                                    ProductCodeId = Convert.ToInt64(TransactionDetails.Comment);
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        var ProductCodeDetails = _HCoreContext.CAProductCode
                                                                           .Where(x => x.AccountId == TransactionDetails.UserAccountId
                                                                                   && x.Id == ProductCodeId
                                                                                   && (x.Product.TypeId == Product.GiftCard || x.Product.TypeId == Product.QuickGiftCard)
                                                                                   && x.AvailableAmount <= RedeemAmount
                                                                                   && x.AvailableAmount > 0
                                                                                   && x.StatusId == HelperStatus.ProdutCode.Unused)
                                                                           .Select(x => new
                                                                           {
                                                                               ProductId = x.ProductId,
                                                                               CodeId = x.Id,
                                                                               x.AvailableAmount,
                                                                               UsageTypeId = x.Product.UsageTypeId
                                                                           })
                                                                           .FirstOrDefault();
                                                        if (ProductCodeDetails != null)
                                                        {
                                                            if (RedeemAmount <= ProductCodeDetails.AvailableAmount)
                                                            {
                                                                ProductAmount = (double)ProductCodeDetails.AvailableAmount;
                                                                ProductId = ProductCodeDetails.ProductId;
                                                                IsBalanceStatusOk = true;
                                                            }
                                                            else
                                                            {
                                                                // Cancel Transaction
                                                                IsBalanceStatusOk = false;
                                                                ErrorMessage = CoreResources.HCG140;
                                                                ErrorMessageCode = "HCG140";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            // Cancel Transaction
                                                            IsBalanceStatusOk = false;
                                                            ErrorMessage = CoreResources.HCG142;
                                                            ErrorMessageCode = "HCG142";
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    // Cancel Transaction
                                                    IsBalanceStatusOk = false;
                                                    ErrorMessage = CoreResources.HCG141;
                                                    ErrorMessageCode = "HCG141";
                                                }

                                                break;
                                            case TransactionSource.TUC:
                                                double AccountBalance = _ManageCoreTransaction.GetAppUserBalance((long)TransactionDetails.UserAccountId);
                                                if (AccountBalance >= RedeemAmount)
                                                {
                                                    // Process Transaction
                                                    IsBalanceStatusOk = true;
                                                }
                                                else
                                                {
                                                    // Cancel Transaction
                                                    IsBalanceStatusOk = false;
                                                    ErrorMessage = CoreResources.HCG143;
                                                    ErrorMessageCode = "HCG143";
                                                }

                                                break;
                                            default:
                                                // Cancel Transaction
                                                IsBalanceStatusOk = false;
                                                ErrorMessage = CoreResources.HCG144;
                                                ErrorMessageCode = "HCG144";
                                                break;
                                        }

                                        if (IsBalanceStatusOk)
                                        {
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.CustomerId = (long)TransactionDetails.UserAccountId;
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.ParentTransactionKey = TransactionDetails.Guid;
                                            _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
                                            _CoreTransactionRequest.InvoiceAmount = TransactionDetails.PurchaseAmount;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = TransactionDetails.PurchaseAmount;
                                            _CoreTransactionRequest.AccountNumber = TransactionDetails.AccountNumber;
                                            _CoreTransactionRequest.ReferenceNumber = TransactionDetails.ReferenceNumber;
                                            if (TransactionDetails.CashierId != null)
                                            {
                                                _CoreTransactionRequest.CashierId = (long)TransactionDetails.CashierId;
                                            }

                                            if (_OGatewayInfo.AcquirerId != null)
                                            {
                                                _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
                                            }
                                            _CoreTransactionRequest.ReferenceAmount = RedeemAmount;
                                            if (TransactionDetails.SubParentId != null && TransactionDetails.SubParentId != 0)
                                            {
                                                _CoreTransactionRequest.SubParentId = (long)TransactionDetails.SubParentId;
                                            }
                                            if (_OGatewayInfo.TerminalId != 0)
                                            {
                                                _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                            }
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _OGatewayInfo.MerchantId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.Loyalty.TUCRedeem.PosRedeem,
                                                SourceId = TransactionSource.Settlement,
                                                Amount = RedeemAmount,
                                                TotalAmount = RedeemAmount,
                                            });
                                             _CoreTransactionRequest.Transactions = _TransactionItems;                                             _ManageCoreTransaction = new ManageCoreTransaction();                                             OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);                                             if (TransactionResponse.Status == HelperStatus.Transaction.Success)                                             {                                                 using (_HCoreContext = new HCoreContext())                                                 {                                                     var TransactionDetailsU = _HCoreContext.HCUAccountTransaction.Where(x => x.GroupKey == _Request.TransactionReference).ToList();                                                     if (TransactionDetailsU != null)                                                     {                                                         foreach (var TransactionDetail in TransactionDetailsU)
                                                        {
                                                             TransactionDetail.StatusId = HelperStatus.Transaction.Success;                                                             TransactionDetail.ModifyDate = HCoreHelper.GetGMTDateTime();                                                             TransactionDetail.ModifyById = _Request.UserReference.AccountId;
                                                        }                                                         _HCoreContext.SaveChanges();                                                     }                                                 }
                                                if (ProductId != 0 && ProductCodeId != 0)
                                                {
                                                    long UseLocationId = 0;
                                                    long ProductOwnerId = 0;
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        var ProductLocationDetails = _HCoreContext.CAProductLocation.Where(x => x.ProductId == ProductId && x.AccountId == _OGatewayInfo.MerchantId && x.SubAccountId == _OGatewayInfo.StoreId).Select(x => x.Id).FirstOrDefault();
                                                        if (ProductLocationDetails != 0)
                                                        {
                                                            UseLocationId = ProductLocationDetails;
                                                        }
                                                        else
                                                        {
                                                            CAProductLocation _CAProductLocation;
                                                            _CAProductLocation = new CAProductLocation();
                                                            _CAProductLocation.Guid = HCoreHelper.GenerateGuid();
                                                            _CAProductLocation.ProductId = ProductId;
                                                            _CAProductLocation.AccountId = _OGatewayInfo.MerchantId;
                                                            _CAProductLocation.SubAccountId = _OGatewayInfo.StoreId;
                                                            _CAProductLocation.CreateDate = HCoreHelper.GetGMTDateTime();
                                                            _CAProductLocation.CreatedById = _Request.UserReference.AccountId;
                                                            _CAProductLocation.StatusId = HelperStatus.Default.Active;
                                                            _HCoreContext.CAProductLocation.Add(_CAProductLocation);
                                                            _HCoreContext.SaveChanges();
                                                            UseLocationId = _CAProductLocation.Id;
                                                        }
                                                        _HCoreContext.Dispose();
                                                    }
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        var ProductDetails = _HCoreContext.CAProduct.Where(x => x.Id == ProductId).FirstOrDefault();
                                                        if (ProductDetails != null)
                                                        {
                                                            ProductOwnerId = ProductDetails.AccountId;
                                                            var ProductCodeDetails = _HCoreContext.CAProductCode.Where(x => x.Id == ProductCodeId).FirstOrDefault();
                                                            if (ProductCodeDetails != null)
                                                            {
                                                                ProductCodeDetails.LastUseLocationId = UseLocationId;
                                                                ProductCodeDetails.LastUseDate = HCoreHelper.GetGMTDateTime();
                                                                ProductCodeDetails.UseCount += 1;
                                                                ProductCodeDetails.TransactionId = TransactionDetails.Id;
                                                                ProductCodeDetails.AvailableAmount -= RedeemAmount;
                                                                if (ProductCodeDetails.AvailableAmount >= ProductCodeDetails.AvailableAmount)
                                                                {
                                                                    ProductCodeDetails.StatusId = HelperStatus.ProdutCode.Used;
                                                                }
                                                                ProductDetails.TotalUsed += 1;
                                                                ProductDetails.TotalUsedAmount += RedeemAmount;
                                                                ProductDetails.LastUseLocationId = UseLocationId;
                                                                ProductDetails.LastUseDate = HCoreHelper.GetGMTDateTime();

                                                                CAProductUseHistory _CAProductUseHistory;
                                                                _CAProductUseHistory = new CAProductUseHistory();
                                                                _CAProductUseHistory.Guid = HCoreHelper.GenerateGuid();
                                                                _CAProductUseHistory.ProductId = ProductId;
                                                                _CAProductUseHistory.AccountId = (long)TransactionDetails.UserAccountId;
                                                                _CAProductUseHistory.ProductCodeId = ProductCodeDetails.Id;
                                                                _CAProductUseHistory.Amount = RedeemAmount;
                                                                _CAProductUseHistory.LocationId = UseLocationId;
                                                                _CAProductUseHistory.CreateDate = HCoreHelper.GetGMTDateTime();
                                                                _CAProductUseHistory.CreatedById = _Request.UserReference.AccountId;
                                                                _CAProductUseHistory.StatusId = HelperStatus.Default.Active;
                                                                _HCoreContext.CAProductUseHistory.Add(_CAProductUseHistory);
                                                                _HCoreContext.SaveChanges();
                                                            }
                                                        }
                                                    }
                                                    if (ProductOwnerId != 0)
                                                    {
                                                        _ManageCoreTransaction.UpdateAccountBalance((long)TransactionDetails.UserAccountId, TransactionSource.GiftCards, ProductOwnerId);
                                                    }
                                                }
                                                _ManageCoreTransaction.UpdateAccountBalance((long)TransactionDetails.UserAccountId, TransactionSource.TUC);                                                 if (TransactionDetails.SourceId== TransactionSource.GiftPoints)
                                                {
                                                    _ManageCoreTransaction.UpdateAccountBalance((long)TransactionDetails.UserAccountId, TransactionSource.GiftPoints, (long)TransactionDetails.ParentId);                                                 }

                                                double Balance = _ManageCoreTransaction.GetAppUserBalance((long)TransactionDetails.UserAccountId);                                                 if (HostEnvironment == HostEnvironmentType.Live || HostEnvironment == HostEnvironmentType.Test)                                                 {                                                     string RedeemSms = HCoreHelper.GetConfiguration("redeemsms", TransactionDetails.ParentId);                                                     if (!string.IsNullOrEmpty(RedeemSms))                                                     {                                                         #region Send SMS                                                         string Message = RedeemSms                                                         .Replace("[AMOUNT]", HCoreHelper.RoundNumber(RedeemAmount, _AppConfig.SystemExitRoundDouble).ToString())                                                         .Replace("[BALANCE]", HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString())                                                         .Replace("[MERCHANT]", TransactionDetails.MerchantDisplayName);                                                         HCoreHelper.SendSMS(SmsType.Transaction, "234", TransactionDetails.MobileNumber, Message, (long)TransactionDetails.UserAccountId , TransactionDetails.Guid, TransactionDetails.Id);                                                         #endregion                                                     }                                                     else                                                     {                                                         #region Send SMS                                                         string Message = "Redeem Alert: You just redeemed N" + HCoreHelper.RoundNumber(RedeemAmount, _AppConfig.SystemExitRoundDouble).ToString() + " Cash at " + TransactionDetails.MerchantDisplayName + ". Your TUC Bal:N" + HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString() + ".  Download TUC App: https://bit.ly/tuc-app. Thank U Very Much";                                                         HCoreHelper.SendSMS(SmsType.Transaction, "234", TransactionDetails.MobileNumber, Message, (long)TransactionDetails.UserAccountId, TransactionDetails.Guid, TransactionDetails.Id);
                                                        #endregion                                                     }
                                                    #region Send Email 
                                                    if (!string.IsNullOrEmpty(TransactionDetails.UserEmailAddress))
                                                    {
                                                        var _EmailParameters = new
                                                        {
                                                            UserDisplayName = TransactionDetails.UserDisplayName,
                                                            MerchantName = TransactionDetails.MerchantDisplayName,
                                                            InvoiceAmount = TransactionDetails.PurchaseAmount.ToString(),
                                                            Amount = RedeemAmount.ToString(),
                                                            Balance = Balance.ToString(),
                                                        };
                                                        HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RedeemEmail, TransactionDetails.UserDisplayName, TransactionDetails.UserEmailAddress, _EmailParameters, _Request.UserReference);
                                                    }
                                                    #endregion                                                 }
                                                OThankUGateway.Request _RedeemRewardRequest = new OThankUGateway.Request();
                                                if (!string.IsNullOrEmpty(_Request.TransactionMode))
                                                {
                                                    _RedeemRewardRequest.TransactionMode = _Request.TransactionMode;
                                                }
                                                else
                                                {
                                                    _RedeemRewardRequest.TransactionMode = "redeemreward";
                                                }
                                                _RedeemRewardRequest.MerchantId = _Request.MerchantId;
                                                _RedeemRewardRequest.TerminalId = _Request.TerminalId;
                                                _RedeemRewardRequest.CardNumber = _Request.CardNumber;
                                                _RedeemRewardRequest.TagNumber = _Request.TagNumber;
                                                _RedeemRewardRequest.SixDigitPan = _Request.SixDigitPan;
                                                if (!string.IsNullOrEmpty(_Request.ReferenceNumber))
                                                {
                                                    _RedeemRewardRequest.ReferenceNumber = _Request.ReferenceNumber;
                                                }
                                                else
                                                {
                                                    _RedeemRewardRequest.ReferenceNumber = TransactionDetails.Guid;
                                                }
                                                _RedeemRewardRequest.MobileNumber = TransactionDetails.MobileNumber;
                                                _RedeemRewardRequest.InvoiceAmount = (long)(Math.Round(InvoiceAmount, 2) * 100);
                                                _RedeemRewardRequest.TransactionDate = _Request.TransactionDate;
                                                _RedeemRewardRequest.bin = _Request.bin;
                                                _RedeemRewardRequest.UserReference = _Request.UserReference;
                                                _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
                                                _FrameworkThankUCashGateway.RewardRedeemTransaction(_RedeemRewardRequest, _OGatewayInfo, _Request.TransactionReference, RedeemAmount, TransactionDetails.PurchaseAmount, true);

                                                _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;                                                 _GatewayResponse.TransactionDate = TransactionDetails.TransactionDate;                                                 _GatewayResponse.TransactionReference = _Request.TransactionReference;                                                 _GatewayResponse.RedeemAmount = (long)((HCoreHelper.RoundNumber(RedeemAmount, 2)) * 100);
                                                #region Send Response                                                 return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG125",CoreResources.HCG125);                                                 #endregion
                                            }                                             else                                             {                                                 #region Send Response                                                 return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549",CoreResources.HCG549);                                                 #endregion                                             }
                                        }
                                        else
                                        {
                                            // Cancel Transaction And Send Response
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                var TrInfo = _HCoreContext.HCUAccountTransaction.Where(x => x.Id == TransactionDetails.Id).FirstOrDefault();
                                                if (TrInfo != null)
                                                {
                                                    TrInfo.ModifyById = SystemAccounts.ThankUCashSystemId;
                                                    TrInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                    TrInfo.Comment = ErrorMessageCode +" : "+ ErrorMessage+   " Transaction failed ";
                                                    TrInfo.StatusId = HelperStatus.Transaction.Failed;
                                                    _HCoreContext.SaveChanges();
                                                }
                                                else
                                                {
                                                    _HCoreContext.Dispose();
                                                }
                                            }
                                            #region Send Response                                             return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ErrorMessageCode, ErrorMessage);
                                            #endregion
                                        }
                                    }                                     if (TransactionDetails.UserAccountStatusId == HelperStatus.Default.Suspended)                                     {                                          using (_HCoreContext = new HCoreContext())                                         {                                             var TrInfo = _HCoreContext.HCUAccountTransaction.Where(x => x.Id == TransactionDetails.Id).FirstOrDefault();                                             if (TrInfo != null)                                             {                                                 TrInfo.ModifyById = SystemAccounts.ThankUCashSystemId;                                                 TrInfo.ModifyDate = HCoreHelper.GetGMTDateTime();                                                 TrInfo.Comment = "Transaction failed as user account was suspended before completing transaction ";                                                 TrInfo.StatusId = HelperStatus.Transaction.Failed;                                                 _HCoreContext.SaveChanges();                                             }                                             else                                             {                                                 _HCoreContext.Dispose();                                             }                                         }                                         #region Send Response                                         return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUC205",CoreResources.TUC205);                                         #endregion                                     }                                     else                                     {                                         using (_HCoreContext = new HCoreContext())                                         {                                             var TrInfo = _HCoreContext.HCUAccountTransaction.Where(x => x.Id == TransactionDetails.Id).FirstOrDefault();                                             if (TrInfo != null)                                             {                                                 TrInfo.ModifyById = SystemAccounts.ThankUCashSystemId;                                                 TrInfo.ModifyDate = HCoreHelper.GetGMTDateTime();                                                 TrInfo.Comment = "Transaction failed as user account was blocked before completing transaction ";                                                 TrInfo.StatusId = HelperStatus.Transaction.Failed;                                                 _HCoreContext.SaveChanges();                                             }                                             else                                             {                                                 _HCoreContext.Dispose();                                             }                                         }                                         #region Send Response                                         return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUC204", CoreResources.TUC204);                                         #endregion                                     }                                 }                                 else                                 {                                     #region Send Response                                     return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG126");                                     #endregion                                 }                             }                             else                             {                                 #region Send Response                                 return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUC206",CoreResources.TUC206);                                 #endregion                             }                         }                         else                         {                             #region Send Response                             return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG103");                             #endregion                         }                     }                     #endregion                 }              }             catch (Exception _Exception)             {                 #region  Log Exception                 HCoreHelper.LogException(LogLevel.High, "ConfirmRedeem", _Exception, _Request.UserReference);                 #endregion                 #region Send Response                 return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");                 #endregion             }             #endregion         }
        #endregion

    }
}
